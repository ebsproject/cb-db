"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import configparser

class ConfigParser:

    def __init__(self, filename):
        """Read ini file"""
        self.config = configparser.ConfigParser()
        self.config.read(filename, encoding='utf-8')

    def get_host(self):
        """Get host"""
        return self.config.get('CB-DB','host')

    def get_db_name(self):
        """Get db name"""
        return self.config.get('CB-DB','database')

    def get_port(self):
        """Get port"""
        return self.config.get('CB-DB','port')

    def get_username(self):
        """Get username"""
        return self.config.get('CB-DB','username')

    def get_password(self):
        """Get password"""
        return self.config.get('CB-DB','password')

    def get_schema(self):
        """Get schema"""
        return self.config.get('CB-DB','schema')

if __name__ == '__main__':
    
    """Test"""
    config = ConfigParser('config.ini')
    print (config.get_host())
    
    schema = ConfigParser('schema.ini')
    print (schema.get_schema())

    
   
