/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

-- random values

SELECT
    *
FROM
    crosstab($$
            SELECT
                1 AS row_id,
                row_number() OVER () AS category,
                ge.germplasm_normalized_name
            FROM
                (
                    SELECT
                        DISTINCT 1 + trunc(random() * 1014013)::integer AS id
                    FROM
                        generate_series(1, 1100) g
                ) r
                JOIN germplasm.germplasm AS ge
                    USING (id)
            WHERE
                ge.designation LIKE 'IR%'
            LIMIT
                10
        $$, $$
            SELECT m FROM generate_series(1,10) m
        $$
    ) AS t (
        row_id integer,
        tgnval0 varchar,
        tgnval1 varchar,
        tgnval2 varchar,
        tgnval3 varchar,
        tgnval4 varchar,
        tgnval5 varchar,
        tgnval6 varchar,
        tgnval7 varchar,
        tgnval8 varchar,
        tgnval9 varchar
    )
\gset

SELECT
    100 AS tlimit,
    floor(random() * 10046222)::int AS toffset
\gset

WITH mainQuery AS (
    SELECT
        ROW_NUMBER () OVER () AS "orderNumber",
        COUNT(*) OVER (PARTITION BY "germplasmDbId") AS "packageCount",
        array_length(uniq(sort(ARRAY_AGG("seedDbId") OVER (partition by "germplasmDbId"))), 1) AS "seedCount",
        *
    FROM (
        SELECT
            *
        FROM 
            (
                SELECT
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmType",
                    gn.germplasm_other_names AS "germplasmOtherNames",
                    seed.id AS "seedDbId",
                    seed.program_id AS "programDbId",
                    package.id AS "packageDbId",
                    package.package_code AS "packageCode",
                    package.package_label AS "packageLabel",
                    package.package_quantity AS "quantity",
                    package.package_unit AS "unit",
                    package.package_document AS "packageDocument",
                    (
                        SELECT
                            data_value
                        FROM
                            germplasm.package_trait
                        WHERE
                            is_void=FALSE
                            AND package_id = package.id
                            AND variable_id = 182
                    ) AS "moistureContent",
                    facility.id AS "facilityDbId", 
                    container.id AS "containerDbId",
                    subFacility.id AS "subFacilityDbId",
                    facility.facility_name AS "facilityName",
                    container.facility_name AS "containerName",
                    subFacility.facility_name AS "subFacilityName",
                    creator.person_name AS "creator",
                    modifier.person_name AS "modifier"
                FROM
                    germplasm.germplasm germplasm
                    INNER JOIN 
                        (
                            SELECT
                                STRING_AGG(gn.name_value, '|'), gn.germplasm_id
                            FROM
                                germplasm.germplasm_name gn
                            WHERE
                                gn.is_void = FALSE
                                AND gn.germplasm_normalized_name IN (
                                    $$:tgnval0$$,
                                    $$:tgnval1$$,
                                    $$:tgnval2$$,
                                    $$:tgnval3$$,
                                    $$:tgnval4$$,
                                    $$:tgnval5$$,
                                    $$:tgnval6$$,
                                    $$:tgnval7$$,
                                    $$:tgnval8$$,
                                    $$:tgnval9$$
                                )
                            GROUP BY
                                gn.germplasm_id
                        ) AS gn (germplasm_other_names, germplasm_id)
                        ON gn.germplasm_id = germplasm.id
                    INNER JOIN germplasm.seed seed
                        ON seed.germplasm_id = germplasm.id
                            AND seed.is_void = FALSE
                    INNER JOIN tenant.program program
                        ON program.id = seed.program_id
                            AND program.is_void = FALSE
                    JOIN germplasm.package package
                        ON package.seed_id = seed.id
                            AND package.is_void = FALSE
                    LEFT JOIN place.facility container
                        ON container.id = package.facility_id
                            AND container.is_void = FALSE
                    LEFT JOIN place.facility subFacility
                        ON subFacility.id = container.parent_facility_id
                            AND subFacility.is_void = FALSE
                    LEFT JOIN place.facility facility
                        ON facility.id = container.root_facility_id
                            AND facility.is_void = FALSE
                    JOIN tenant.person creator
                        ON creator.id = package.creator_id
                            AND creator.is_void = FALSE
                    LEFT JOIN tenant.person modifier
                        ON modifier.id = package.modifier_id
                            AND modifier.is_void = FALSE
            ) AS tbl
        ) AS abc
), countQuery AS (
    SELECT
        MAX("orderNumber") AS "totalCount"
    FROM
        mainQuery
)
SELECT
    countQuery."totalCount",
    mainQuery.*
FROM
    countQuery,
    mainQuery
LIMIT
    :tlimit
OFFSET
    :toffset
;
