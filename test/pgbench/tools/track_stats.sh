#!/bin/bash

# watch -n TIME_IN_SECONDS ./track_stats.sh TRANSACTION_NAME [ TARGET_DIRECTORY ] [ SUFFIX ]

# check if target directory is provided
if [ -z "$2" ]; then
    # create sub-folders in current directory
    target_dir=./$1/stats
else
    # create sub-folders in target directory
    target_dir=$2/$1/stats
fi

# create sub-folders for the transaction
mkdir -p $target_dir
mkdir -p $target_dir/htop_stats
mkdir -p $target_dir/docker_stats

# check if suffix is provided
if [ -z "$3" ]; then
    # no suffix, write as regular HTML file
    htop_file="htop_stats-${1}-$(date '+%y%m%d%H%M%S').html"
    docker_stats_file="docker_stats-${1}-$(date '+%y%m%d%H%M%S').html"
else
    # with suffix, append to HTML file if provided
    htop_file="htop_stats-${1}-${3}-$(date '+%y%m%d%H%M%S').html"
    docker_stats_file="docker_stats-${1}-${3}-$(date '+%y%m%d%H%M%S').html"
fi

# save results as HTML file
echo q | htop | aha --black --line-fix > $target_dir/htop_stats/$htop_file
echo q | docker stats --no-stream | aha --black --line-fix > $target_dir/docker_stats/$docker_stats_file
