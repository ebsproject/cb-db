"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

#!/usr/bin/python

import os
import sys
import psycopg2
from pathlib import Path
import re

def read_credentials():
    """ Read database credentials from liquibase properties file """
    config = {}
    
    try:
        # get database credentials from the liquibase properties file
        config_path = Path('../../../build/liquibase/.conf/dock-cb-temp.properties')
        
        # read properties file and save to config dictionary
        with open(config_path, 'r') as file:
            for line in file:
                items = line.split(': ')
                key, value = items[0], items[1][:-1]
                if key == 'url':
                    db_values = value.split(':')
                    config['database'] = value.split('/')[-1]
                    config['host'] = db_values[2].replace('/', '')
                    config['port'] = db_values[3].split('/')[0]
                config[key] = value
    except (Exception) as error:
        print(error)
        sys.exit('Error in retrieving database credentials')
    finally:
        if config is not None:
            return config

def get_database_connection():
    """Establish database connection"""
    
    # retrieve database credentials
    db_config = read_credentials()
    
    # connect to the PostgreSQL server
    return psycopg2.connect(host=db_config['host'], port=db_config['port'], dbname=db_config['database'], user=db_config['username'], password=db_config['password'])

def explain_analyze(filepath, db):
    """Get total cost, planning time, and execution time of query"""
    
    try:
        with open(filepath, 'r') as file:
            # get results from EXPLAIN ANALYZE on the query
            db.execute(f'EXPLAIN (ANALYZE)' + file.read().replace('EXPLAIN ANALYZE', ''))
            results = db.fetchall()
            
            # get total cost
            total_cost = re.search(r'(\.\.)([\d|\.]+)', results[0][0]).group(2)
            
            # initialize variables for planning and execution times
            planning_time = None
            execution_time = None
            
            # search through the results for the times
            for line in results[::-1]:
                if 'Planning Time' in line[0]:
                    # get the planning time in ms
                    planning_time = re.search(r'[\d|\.]+', line[0]).group()
                elif 'Execution Time' in line[0]:
                    # get the execution time in ms
                    execution_time = re.search(r'[\d|\.]+', line[0]).group()
                
                # exit search when both variables are filled up
                if planning_time != None and execution_time != None:
                    break
            
            # return results
            return (total_cost, planning_time, execution_time)
    except (Exception) as error:
        print(error)

def main(path):
    """Run EXPLAIN ANALYZE on a single script or a list of scripts from a directory"""
    
    # create database connection
    db = get_database_connection().cursor()
    
    # print headers
    headers = ['SCRIPT', 'COST', 'PLAN_TIME_MS', 'EXEC_TIME_MS']
    print(f'\t'.join(headers))
    
    # check if path is directory or a SQL file
    if os.path.isdir(path):
        try:
            # run EXPLAIN ANALYZE on every SQL file in the directory
            for filename in sorted(os.listdir(path)):
                # check if file is SQL
                if filename.endswith('.sql'):
                    # run EXPLAIN ANALYZE on the SQL file
                    results = explain_analyze(Path(path) / filename, db)
                    
                    # print the results on the terminal
                    print(f'{filename}\t{results[0]}\t{results[1]}\t{results[2]}')
        except (Exception) as error:
            print(error)
        finally:
            if db is not None:
                db.close()
    else:
        # run EXPLAIN ANALYZE on the single SQL file
        filepath = Path(path)
        results = explain_analyze(filepath, db)
        print(f'{filepath.name}\t{results[0]}\t{results[1]}\t{results[2]}')

if __name__ == '__main__':
    """Run main"""
    
    # python explain_analyze.py FILE_OR_DIRECTORY
    main(sys.argv[1])
