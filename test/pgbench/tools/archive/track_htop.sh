#!/bin/bash

# watch -n TIME_IN_SEC ./track_htop.sh TXN_NAME [ SUFFIX ]

# create directory for the transaction
mkdir -p $1

# check if suffix is provided
if [ -z "$2" ]; then
    # no suffix, write as regular HTML file
    file="htop-${1}-$(date '+%y%m%d%H%M%S').html"
else
    # with suffix, append to HTML file if provided
    file="htop-${1}-${2}-$(date '+%y%m%d%H%M%S').html"
fi

# save results as HTML file
echo q | htop | aha --black --line-fix > $1/$file
