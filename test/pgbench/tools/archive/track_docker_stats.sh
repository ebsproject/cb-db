#!/bin/bash

# watch -n TIME_IN_SEC ./track_docker_stats.sh TXN_NAME [ SUFFIX ]

# create directory for the transaction
mkdir -p $1

# check if suffix is provided
if [ -z "$2" ]; then
    # no suffix, write as regular HTML file
    file="docker_stats-${1}-$(date '+%y%m%d%H%M%S').html"
else
    # with suffix, append to HTML file if provided
    file="docker_stats-${1}-${2}-$(date '+%y%m%d%H%M%S').html"
fi

# save results as HTML file
echo q | docker stats --no-stream | aha --black --line-fix > $1/$file
