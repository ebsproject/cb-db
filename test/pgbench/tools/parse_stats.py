"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

#!/usr/bin/python

import sys
import re

"""Parse results file"""
def main(path):
    # print headers
    headers = ['SCRIPT', 'TOTAL_TXN', 'LAT_AVG', 'TPS', 'STATUS', 'REMARKS']
    print(f'\t'.join(headers))
    
    # read results file
    with open(path, 'r') as file:
        # create a placeholder for each transaction
        transaction = ['NA'] * 5
        errors = []
        
        # check each line
        for line in file:
            # save statistics associated with each line
            if 'transaction type' in line:
                transaction[0] = re.search(r'[\w]+\-[\d|\-]+.sql', line).group()
            elif 'number of transactions actually processed' in line:
                transaction[1] = re.search(r'[\d]+', line).group()
            elif 'latency average' in line:
                transaction[2] = re.search(r'[\d|\.]+', line).group()
            elif 'pgbench: error' in line:
                errors.append(line.split(':')[-1].strip())
            elif 'tps' in line and 'including connections establishing' in line:
                transaction[3] = re.search(r'[\d|\.]+', line).group()
                transaction[4] = 'COMPLETED'
            elif 'pgbench: fatal' in line:
                # encountered error
                transaction[4] = 'INCOMPLETE'
            
            if transaction[4] in ['COMPLETED', 'INCOMPLETE']:
                if len(errors) > 0:
                    transaction.append(';'.join(set(errors)))
                
                if transaction[0] != 'NA':
                    # print results per transaction/file
                    print(f'\t'.join(transaction))
                
                # reset transaction placeholder
                transaction = ['NA'] * 5
                errors = []

"""Run main"""
if __name__ == '__main__':
    # python parse_stats.py PGBENCH_RESULTS_FILE
    main(sys.argv[1])

# example results file
# ➜  extracted git:(feature/DB-593) ✗ pgbench -U postgres -h localhost -p 5432 -c 100 -T 60 -j 10 -n -f GS-001.sql cb                                                                     21.08.18 14:42:34
# transaction type: GS-001.sql
# scaling factor: 1
# query mode: simple
# number of clients: 100
# number of threads: 10
# duration: 60 s
# number of transactions actually processed: 1926
# latency average = 3276.942 ms
# tps = 30.516257 (including connections establishing)
# tps = 30.546459 (excluding connections establishing)
