/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/experiment-packages-search

                SELECT DISTINCT ON ("id")
                    *
                FROM
                    (
                select experiment.experiment_year AS "experimentYear", season.id AS "id", season.season_name AS "text"
                FROM
                    germplasm.package package
                RIGHT JOIN
                    tenant.program program ON program.id = package.program_id
                    AND program.is_void = FALSE
                LEFT JOIN
                    place.facility container ON container.id = package.facility_id
                    AND container.is_void = FALSE
                LEFT JOIN
                    place.facility subFacility ON subFacility.id = container.parent_facility_id
                    AND subFacility.is_void = FALSE
                LEFT JOIN
                    place.facility facility ON facility.id = container.root_facility_id
                    AND facility.is_void = FALSE
                JOIN
                    germplasm.seed seed ON seed.id = package.seed_id
                    AND seed.is_void = FALSE
                LEFT JOIN
                    experiment.location "location" ON "location".id = seed.source_location_id
                    AND location.is_void = FALSE
                INNER JOIN
                    experiment.experiment experiment ON experiment.id = seed.source_experiment_id
                    AND experiment.is_void = FALSE
                INNER JOIN
                    experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
                    AND occurrence.is_void = FALSE
                INNER JOIN
                    tenant.season season ON season.id = experiment.season_id
                    AND season.is_void = FALSE
                INNER JOIN
                    tenant.stage stage ON stage.id = experiment.stage_id
                    AND stage.is_void = FALSE
                INNER JOIN
                    tenant.person creator ON creator.id = package.creator_id
                    AND creator.is_void = FALSE
                LEFT JOIN
                    tenant.person modifier ON modifier.id = package.modifier_id
                    AND modifier.is_void = FALSE
                WHERE
                    package.is_void = FALSE

                    ) AS tbl
                 WHERE ( ((("experimentYear"::text ILIKE '2014')) AND (("id"::text NOT ILIKE 'null'))))ORDER BY "id", "text" ASC
