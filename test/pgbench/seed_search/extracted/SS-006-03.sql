/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/seed-packages-search

                WITH mainQuery AS (
                    SELECT
                        ROW_NUMBER () OVER ( ORDER BY "designation" asc, "sourceHarvestYear" desc, "experimentYear" desc ) AS "orderNumber",

                    COUNT(*) OVER (PARTITION BY "germplasmDbId") AS "packageCount",
                    array_length(uniq(sort(ARRAY_AGG("seedDbId") over (partition by "germplasmDbId"))), 1) AS "seedCount",

                        *
                    FROM (

                SELECT

                    *
                FROM
                    (

                        SELECT

                    germplasm.designation AS "designation",
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmType",
                    gn.germplasm_other_names AS "germplasmOtherNames",

                    seed.seed_name AS "seedName",
                    seed.seed_code AS "seedCode",
                    seed.germplasm_id AS "germplasmDbId",
                    seed.source_experiment_id AS "experimentDbId",
                    seed.source_occurrence_id AS "occurrenceDbId",
                    seed.source_location_id AS "locationDbId",
                    seed.source_entry_id AS "entryDbId",
                    seed.cross_id AS "crossDbId",
                    seed.harvest_source AS "harvestSource",

                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_year AS "experimentYear",
                    experiment.experiment_type AS "experimentType",
                    stage.id AS "stageDbId",
                    stage.stage_name AS "stageName",
                    stage.stage_code AS "stageCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    program.program_code AS "seedManager",
                    "location".location_code AS "location",
                    "location".location_name AS "locationName",
                    season.id AS "seasonDbId",
                    season.season_code AS "seasonCode",
                    "entry".entry_code AS "entryCode",
                    "entry".entry_number AS "entryNumber",
                    plot.plot_code AS "plotCode",
                    plot.plot_number AS "plotNumber",
                    plot.rep AS "replication",

                            seed.id AS "seedDbId",
                            seed.program_id AS "programDbId",
                            (
                                SELECT
                                    EXTRACT (YEAR FROM seed.harvest_date)
                            ) AS "sourceHarvestYear",
                            package.id AS "packageDbId",
                            package.package_code AS "packageCode",
                            package.package_label AS "label",
                            package.package_quantity AS "quantity",
                            package.package_unit AS "unit",
                            package.package_document AS "packageDocument",
                            (
                                SELECT
                                    data_value
                                FROM
                                    germplasm.package_trait
                                WHERE
                                    is_void=FALSE AND
                                    package_id = package.id AND
                                    variable_id = 182
                            ) AS "moistureContent",
                            facility.id AS "facilityDbId",
                            container.id AS "containerDbId",
                            subFacility.id AS "subFacilityDbId",
                            facility.facility_name AS "facilityName",
                            container.facility_name AS "containerName",
                            subFacility.facility_name AS "subFacilityName",
                            creator.person_name AS "creator",
                            modifier.person_name AS "modifier"


                FROM


                    (
                        SELECT
                            STRING_AGG(gn.name_value, '|'),
                            gn.germplasm_id
                        FROM
                            germplasm.germplasm_name gn
                        WHERE
                            gn.is_void = FALSE

                        GROUP BY
                            gn.germplasm_id
                    ) AS gn (germplasm_other_names, germplasm_id)
                    INNER JOIN germplasm.germplasm ON germplasm.id = gn.germplasm_id

                    JOIN germplasm.seed seed ON seed.germplasm_id = germplasm.id AND seed.is_void = FALSE


                        LEFT JOIN
                            experiment.location "location" ON "location".id = seed.source_location_id
                            AND location.is_void = FALSE
                        LEFT JOIN
                            experiment.experiment experiment ON experiment.id = seed.source_experiment_id
                            AND experiment.is_void = FALSE
                        LEFT JOIN
                            experiment.plot plot ON plot.id = seed.source_plot_id
                            AND plot.is_void = FALSE
                        LEFT JOIN
                            experiment.entry "entry" ON "entry".id = seed.source_entry_id
                            AND entry.is_void = false
                        LEFT JOIN
                            experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
                            AND occurrence.is_void = FALSE
                        LEFT JOIN
                            tenant.season season ON season.id = experiment.season_id
                            AND season.is_void = FALSE
                        LEFT JOIN
                            tenant.stage stage ON stage.id = experiment.stage_id
                            AND stage.is_void = FALSE


                    RIGHT JOIN
                        tenant.program program ON program.id = seed.program_id
                        AND program.is_void = FALSE
                    JOIN
                        germplasm.package package ON package.seed_id = seed.id
                        AND package.is_void = FALSE
                    LEFT JOIN
                        place.facility container ON container.id = package.facility_id
                        AND container.is_void = FALSE
                    LEFT JOIN
                        place.facility subFacility ON subFacility.id = container.parent_facility_id
                        AND subFacility.is_void = FALSE
                    LEFT JOIN
                        place.facility facility ON facility.id = container.root_facility_id
                        AND facility.is_void = FALSE
                    JOIN
                        tenant.person creator ON creator.id = package.creator_id
                        AND creator.is_void = FALSE
                    LEFT JOIN
                        tenant.person modifier ON modifier.id = package.modifier_id
                        AND modifier.is_void = FALSE

                WHERE
                    package.is_void = FALSE

                    ) AS tbl
                 WHERE ( ((("programDbId" IN ($$101$$))) AND (("experimentType"::text ILIKE 'Breeding Trial')) AND (("stageDbId" IN ($$101$$,$$103$$)))))


                    ) AS abc
                ),
                countQuery AS (
                    SELECT
                        MAX("orderNumber") AS "totalCount"
                    FROM
                        mainQuery
                )
                SELECT
                    countQuery."totalCount",
                    mainQuery.*
                FROM
                    countQuery,
                    mainQuery
             LIMIT (10) OFFSET (0)
