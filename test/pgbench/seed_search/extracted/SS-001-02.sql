/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/lists-search

        select

            *
        from
            (

        SELECT DISTINCT ON (list.id)
            list.id AS "listDbId",
            list.abbrev,
            list.name,
            list.display_name AS "displayName",
            list.type,
            list.list_sub_type AS "subType",
            list.entity_id AS "entityId",
            list.description,
            list.remarks,
            list.notes,
            list.is_active AS "isActive",
            list.status,
            list.list_usage AS "listUsage",
            list.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            list.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier,
            COALESCE(list_mem.member_count, 0) AS "memberCount",
            (
            (SELECT COUNT(1) FROM (
                SELECT
                jsonb_object_keys(list.access_data->'user') as "userId"
            ) t WHERE "userId" != list.creator_id::text) +
            (SELECT COUNT(1) FROM (
                SELECT
                jsonb_object_keys(list.access_data->'program')
            ) t)
            ) AS "shareCount",
            CASE WHEN (
                pr.person_role_code = 'DATA_OWNER' OR pr.person_role_code = 'DATA_PRODUCER'
                 OR "programRole1".person_role_code = 'DATA_OWNER' OR "programRole1".person_role_code = 'DATA_PRODUCER' OR "programRole2".person_role_code = 'DATA_OWNER' OR "programRole2".person_role_code = 'DATA_PRODUCER') THEN 'read_write'
            ELSE 'read' END AS "permission"

        FROM
            platform.list list
        INNER JOIN
            tenant.person creator ON list.creator_id = creator.id
        LEFT JOIN
            tenant.person_role pr
        ON
            pr.id = (list.access_data->'user'->'488'::text->>'dataRoleId')::integer

        LEFT JOIN
          tenant.person_role "programRole1"
        ON
        "programRole1".id = (list.access_data->'program'->'101'::text->>'dataRoleId')::integer

        LEFT JOIN
          tenant.person_role "programRole2"
        ON
        "programRole1".id = (list.access_data->'program'->'104'::text->>'dataRoleId')::integer

        LEFT JOIN
            tenant.person modifier ON list.modifier_id = modifier.id
        LEFT JOIN (
            SELECT
                COUNT(lm.id) AS member_count,
                lm.list_id
            FROM
                platform.list_member lm
            WHERE
                lm.is_void = FALSE
                AND lm.is_active = TRUE
            GROUP BY
                lm.list_id
        ) AS list_mem (member_count, list_id)
            ON list_mem.list_id = list.id
      WHERE
        list.is_void = FALSE AND
        (list.access_data #> $${user,488}$$ is not null OR list.access_data #> $${program,101}$$ is not null OR list.access_data #> $${program,104}$$ is not null)


      ORDER BY
        list.id


            ) as tbl
         WHERE ( ((("abbrev"::text ILIKE 'FIND_SEEDS_WORKING_LIST_488') ) AND (("type"::text ILIKE 'package') ) AND (("status"::text ILIKE 'draft') ) AND (("isActive"::text ILIKE 'false') )))  limit (10) offset (0)
