/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/seed-packages-search

                WITH mainQuery AS (
                    SELECT
                        ROW_NUMBER () OVER ( ORDER BY "text" ASC ) AS "orderNumber",

                        *
                    FROM (

                SELECT
                    DISTINCT ON ("id")
                    *
                FROM
                    (
                        select program.id AS "programDbId", program.id AS "id", program.program_name AS "text"
                FROM


                 germplasm.seed seed

                    RIGHT JOIN
                        tenant.program program ON program.id = seed.program_id
                        AND program.is_void = FALSE
                    JOIN
                        germplasm.package package ON package.seed_id = seed.id
                        AND package.is_void = FALSE
                    LEFT JOIN
                        place.facility container ON container.id = package.facility_id
                        AND container.is_void = FALSE
                    LEFT JOIN
                        place.facility subFacility ON subFacility.id = container.parent_facility_id
                        AND subFacility.is_void = FALSE
                    LEFT JOIN
                        place.facility facility ON facility.id = container.root_facility_id
                        AND facility.is_void = FALSE
                    JOIN
                        tenant.person creator ON creator.id = package.creator_id
                        AND creator.is_void = FALSE
                    LEFT JOIN
                        tenant.person modifier ON modifier.id = package.modifier_id
                        AND modifier.is_void = FALSE

                WHERE
                    package.is_void = FALSE

                    ) AS tbl
                 WHERE ( ((("programDbId" IN ($$104$$))) AND (("id"::text NOT ILIKE 'null'))))
                ORDER BY "id"

                    ) AS abc
                ),
                countQuery AS (
                    SELECT
                        MAX("orderNumber") AS "totalCount"
                    FROM
                        mainQuery
                )
                SELECT
                    countQuery."totalCount",
                    mainQuery.*
                FROM
                    countQuery,
                    mainQuery
             LIMIT (10) OFFSET (0)
