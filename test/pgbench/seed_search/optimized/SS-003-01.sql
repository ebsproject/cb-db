/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/experiment-packages-search

--EXPLAIN ANALYZE
SELECT
    DISTINCT
    *
FROM (
        SELECT
            occurrence.id AS "id",
            occurrence.occurrence_name AS "text"
        FROM
            experiment.experiment experiment
            INNER JOIN experiment.occurrence AS occurrence
                ON occurrence.experiment_id = experiment.id
            INNER JOIN LATERAL (
                SELECT
                    seed.*
                FROM
                    germplasm.seed seed
                WHERE
                    seed.source_occurrence_id = occurrence.id
                    AND seed.is_void = FALSE
                LIMIT
                    1
            ) seed
                ON TRUE
            INNER JOIN LATERAL (
                SELECT
                    package.*
                FROM
                    germplasm.package package
                WHERE
                    package.seed_id = seed.id
                    AND package.is_void = FALSE
                LIMIT
                    1
            ) package
                ON TRUE
        WHERE
            experiment.is_void = FALSE
    ) AS tbl
WHERE
    "text" LIKE 'IRSEA-AYT-2014-WS%'
ORDER BY
    "id",
    "text" ASC
;
