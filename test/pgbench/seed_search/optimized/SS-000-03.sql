/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/seed-packages-search

--EXPLAIN ANALYZE
WITH mainQuery AS (
    SELECT
        ROW_NUMBER () OVER (ORDER BY "text" ASC) AS "orderNumber",
        *
    FROM (
            SELECT
                DISTINCT ON ("id")
                *
            FROM (
                    SELECT
                        program.id AS "programDbId",
                        program.id AS "id",
                        program.program_name AS "text"
                    FROM
                        tenant.program AS program
                        LEFT JOIN germplasm.seed AS seed
                            ON seed.program_id = program.id
                            AND seed.is_void = FALSE
                        LEFT JOIN germplasm.package package
                            ON package.seed_id = seed.id
                            AND package.is_void = FALSE
                    WHERE
                        program.is_void = FALSE
                ) AS tbl
            WHERE (
                    (
                        (("programDbId" IN ($$104$$)))
                        AND (("id"::text NOT ILIKE 'null'))
                    )
                )
            ORDER BY "id"
        ) AS abc
),
countQuery AS (
    SELECT
        MAX("orderNumber") AS "totalCount"
    FROM
        mainQuery
)
SELECT
    countQuery."totalCount",
    mainQuery.*
FROM
    countQuery,
    mainQuery
LIMIT
    (10)
OFFSET
    (0)
;
