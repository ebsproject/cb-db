/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/experiment-packages-search

--EXPLAIN ANALYZE
SELECT
    DISTINCT
    *
FROM (
        SELECT
            season.id AS "id",
            season.season_name AS "text"
        FROM
            experiment.experiment experiment
            INNER JOIN tenant.season season
                ON season.id = experiment.season_id
                AND season.is_void = FALSE
            INNER JOIN LATERAL (
                SELECT
                    seed.*
                FROM
                    germplasm.seed seed
                WHERE
                    seed.source_experiment_id = experiment.id
                    AND seed.is_void = FALSE
                LIMIT
                    1
            ) seed
                ON TRUE
            INNER JOIN LATERAL (
                SELECT
                    package.*
                FROM
                    germplasm.package package
                WHERE
                    package.seed_id = seed.id
                    AND package.is_void = FALSE
                LIMIT
                    1
            ) package
                ON TRUE
        WHERE
            experiment.is_void = FALSE
    ) AS tbl
ORDER BY
    "id",
    "text" ASC
;
