/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/lists/:id/members-search

--EXPLAIN ANALYZE
SELECT
    *
FROM (
        SELECT
            "listMember".id AS "listMemberDbId",
            "listMember".order_number AS "orderNumber",
            "listMember".creation_timestamp AS "creationTimestamp",
            "listMember".display_value AS "displayValue",
            "listMember".is_active AS "isActive",
            "listMember".remarks,
            experiment.experiment_name AS "experiment",
            experiment.id AS "experimentDbId",
            experiment.experiment_year AS "experimentYear",
            experiment.experiment_type AS "experimentType",
            stage.id AS "experimentStageDbId",
            stage.stage_name AS "experimentStage",
            stage.stage_code AS "experimentStageCode",
            occurrence.occurrence_name AS "experimentOccurrence",
            occurrence.id AS "sourceOccurrenceDbId",
            occurrence.occurrence_name AS "sourceOccurrenceName",
            program.program_code AS "seedManager",
            program.id AS "programDbId",
            seed.id AS "seedDbId",
            seed.seed_name AS "seedName",
            seed.seed_code AS "GID",
            germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
            (
                SELECT
                    STRING_AGG(gn.name_value, '|')
                FROM
                    germplasm.germplasm_name gn
                WHERE
                    gn.is_void = FALSE
                    AND gn.germplasm_id = germplasm.id
                GROUP BY
                    gn.germplasm_id
            ) AS "germplasmOtherNames",
            germplasm.designation,
            germplasm.parentage,
            germplasm.id AS "germplasmDbId",
            "location".id AS "locationDbId",
            "location".location_code AS "location",
            "location".location_name AS "sourceStudyName",
            season.id AS "sourceStudySeasonDbId",
            season.season_code AS "sourceStudySeason",
            "entry".entry_code AS "sourceEntryCode",
            "entry".entry_number AS "seedSourceEntryNumber",
            plot.plot_code AS "seedSourcePlotCode",
            plot.plot_number AS "seedSourcePlotNumber",
            plot.rep AS "replication",
            seed.harvest_date AS "harvestDate",
            (
                SELECT
                    EXTRACT (YEAR FROM seed.harvest_date)
            ) AS "sourceHarvestYear",
            package.id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "label",
            package.package_quantity AS "quantity",
            package.package_unit AS "unit",
            (
                SELECT
                    data_value
                FROM
                    germplasm.package_trait
                WHERE
                    is_void = FALSE
                    AND package_id = package.id
                    AND variable_id = 182
            ) AS "moistureContent",
            facility.facility_name AS "facility",
            container.facility_name AS "container",
            subFacility.facility_name AS "subFacility"
        FROM
            platform.list_member "listMember"
            INNER JOIN germplasm.package package
                ON package.id = "listMember".data_id
                AND package.is_void = FALSE
            LEFT JOIN tenant.program program
                ON program.id = package.program_id
            LEFT JOIN place.facility container
                ON container.id = package.facility_id
            LEFT JOIN place.facility subFacility
                ON subFacility.id = container.parent_facility_id
            LEFT JOIN place.facility facility
                ON facility.id = container.root_facility_id
            JOIN germplasm.seed seed
                ON seed.id = package.seed_id
            JOIN germplasm.germplasm germplasm
                ON germplasm.id = seed.germplasm_id
            LEFT JOIN experiment.location "location"
                ON "location".id = seed.source_location_id
            LEFT JOIN experiment.experiment experiment
                ON experiment.id = seed.source_experiment_id
            LEFT JOIN experiment.plot plot
                ON plot.id = seed.source_plot_id
            LEFT JOIN experiment.entry "entry"
                ON "entry".id = seed.source_entry_id
            LEFT JOIN experiment.occurrence occurrence
                ON occurrence.id = seed.source_occurrence_id
            LEFT JOIN tenant.season season
                ON season.id = experiment.season_id
            LEFT JOIN tenant.stage stage
                ON stage.id = experiment.stage_id
            LEFT JOIN tenant.person creator
                ON creator.id = package.creator_id
            LEFT JOIN tenant.person modifier
                ON modifier.id = package.modifier_id
        WHERE
            "listMember".list_id = 11235
            AND "listMember".is_void = FALSE
        ORDER BY
            "listMember".order_number
    ) AS tbl
WHERE
    (("isActive"::text ILIKE $$true$$))
LIMIT
    (10)
OFFSET
    (0)
;
