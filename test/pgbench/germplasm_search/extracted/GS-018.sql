/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

--# POST /v3/germplasm-search

        select

            *
        from
            (

        SELECT
          DISTINCT ON (germplasm.id)
          germplasm.id AS "germplasmDbId",
          germplasm.designation,
          germplasm.parentage,
          germplasm.generation,
          germplasm.germplasm_state AS "germplasmState",
          germplasm.germplasm_name_type AS "germplasmNameType",
          germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
          germplasm.germplasm_code AS "germplasmCode",
          germplasm.germplasm_type AS "germplasmType",
          (
            SELECT
                DISTINCT ON (germplasm.germplasm_normalized_name)
                STRING_AGG(germplasmName.name_value, ';')
            FROM
                germplasm.germplasm_name AS germplasmName
            WHERE
                germplasm.id = germplasmName.germplasm_id
                AND germplasmName.is_void = FALSE
            GROUP BY
                germplasmName.germplasm_id
          ) AS "otherNames",
          germplasm.germplasm_document AS "germplasmDocument",
          creator.id AS "creatorDbId",
          creator.person_name AS creator,
          modifier.id AS "modifierDbId",
          modifier.person_name AS modifier,
          germplasm.creation_timestamp AS "creationTimestamp",
          germplasm.modification_timestamp AS "modificationTimestamp"

      FROM
        germplasm.germplasm germplasm
      LEFT JOIN
        germplasm.germplasm_name AS germplasmName
      ON
        germplasm.id = germplasmName.germplasm_id
        AND germplasmName.is_void = FALSE
      INNER JOIN tenant.person AS creator
        ON creator.id = germplasm.creator_id
        AND creator.is_void = FALSE
      LEFT JOIN tenant.person AS modifier
        ON modifier.id = germplasm.modifier_id
        AND modifier.is_void = FALSE
      WHERE
        germplasm.is_void = FALSE



            ) as tbl
         WHERE (("germplasmCode"::text ILIKE $$%GE%%$$)) ORDER BY 1 DESC limit (10) offset (0)
