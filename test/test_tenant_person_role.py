"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_person_role_from_file(path):
    list_person_role = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        person_role = row.strip()
        list_person_role.append(person_role)

    return np.sort(np.array(list_person_role))

def get_list_person_role_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_person_role = kwargs['person_role']
    list_person_role = [person_role for person_role in expected_person_role]
    joined_person_role = "','".join(list_person_role)
    
    query = "SELECT person_role_code FROM tenant.person_role WHERE is_void = false AND person_role_code IN ('{0}');".format(joined_person_role)
    cur.execute(query)
    records = cur.fetchall()
    result_person_role = [person_role[0] for person_role in records]

    return np.sort(np.array(result_person_role))

path = 'reference/expected_tenant_person_role.txt'
expected_person_role = get_list_person_role_from_file(path=path)
result_person_role = get_list_person_role_from_db(cursor=get_cursor(), person_role=expected_person_role)
missing_values = np.setdiff1d(expected_person_role, result_person_role)

@pytest.mark.parametrize("a, b, c",[(expected_person_role,result_person_role,missing_values)])
def test_complete_template_tenant_person_role(a,b,c):
    assert np.array_equal(a,b), "There are missing roles "+str(c)