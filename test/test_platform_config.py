"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_config_from_file(path):
    list_config = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        space = row.strip()
        list_config.append(space)
        
    return np.sort(np.array(list_config))

def get_list_config_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_config = kwargs['configs']
    parsed_abbrevs = [parsed_config.strip().split('=')[0] for parsed_config in expected_config]
    
    list_abbrev = [abbrev for abbrev in parsed_abbrevs]
    joined_abbrev = "','".join(list_abbrev)
    query = """SELECT 
                    abbrev||'='||config_value 
                FROM 
                    platform.config 
                WHERE 
                    abbrev in ('{0}') 
                OR 
                    abbrev 
                NOT IN 
                    (
                        'DEFAULT_BORDER_GERMPLASM',
                        'DEFAULT_FILLER_GERMPLASM',
                        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL',
                        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL',
                        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_VAL',
                        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PROCESS_PATH_PROTOCOLS_ACT_VAL',
                        'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL',
                        'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_VAL',
                        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_VAL',
                        'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_VAL',
                        'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_VAL',
                        'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL',
                        'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL',
                        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_VAL',
                        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT_VAL',
                        'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT_VAL',
                        'EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL',
                        'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL',
                        'EXPT_TRIAL_IRRI_PLACE_ACT_VAL',
                        'EXPT_TRIAL_IRRI_PLANTING_PROTOCOLS_ACT_VAL',
                        'EXPT_TRIAL_IRRI_PROCESS_PATH_PROTOCOLS_ACT_VAL',
                        'MANAGE_EXPERIMENT_DATA_IRSEA',
                        'MANAGE_OCCURRENCE_DATA_IRSEA',
                        'HM_HARVEST_USE_CASE_BEHAVIOR'
                    )
            """.format(joined_abbrev)
    
    cur.execute(query)
    records = cur.fetchall()
    result_space = [space[0] for space in records]

    return np.sort(np.array(result_space))

path = 'reference/expected_platform_config.txt'
expected_config = get_list_config_from_file(path=path)
result_config = get_list_config_from_db(cursor=get_cursor(), configs=expected_config)
missing_values = np.setdiff1d(expected_config, result_config)

@pytest.mark.parametrize("a, b, c",[(expected_config,result_config,missing_values)])
def test_complete_template_platform_config(a,b,c):
    # assert np.isin(b,a).all(), "There are missing configs or some config/s do not match "+str(c)
    assert np.array_equal(a,b), "There are missing configs or some config/s that do not match "+str(c)