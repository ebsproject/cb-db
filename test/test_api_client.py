"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_client_from_file(path):
    list_client = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        client = row.strip()
        list_client.append(client)

    return np.sort(np.array(list_client))

def get_list_client_from_db(**kwargs):
    cur = kwargs['cursor']
    query = "SELECT client_id FROM api.client WHERE is_void = 'false' AND client_secret IS NOT NULL AND redirect_uri IS NOT NULL ORDER BY id;"
    cur.execute(query)
    records = cur.fetchall()
    result_client = [client[0] for client in records]

    return np.sort(np.array(result_client))

path = 'reference/expected_api_client.txt'
expected_client = get_list_client_from_file(path=path)
result_client = get_list_client_from_db(cursor=get_cursor(), variables=expected_client)
missing_values = np.setdiff1d(expected_client, result_client)


@pytest.mark.parametrize("a, b, c",[(expected_client,result_client,missing_values)])
def test_complete_template_api_client(a,b,c):
    assert np.array_equal(a,b), "There are missing client IDs "+str(c)
