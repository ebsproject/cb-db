"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_table_from_file(path):
    list_table = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        table = row.strip()
        list_table.append(table)

    return np.sort(np.array(list_table))

def get_list_table_from_db(**kwargs):
    cur = kwargs['cursor']
    query = "SELECT abbrev FROM dictionary.table WHERE is_void = 'false' AND name IS NOT NULL ORDER BY id;"
    cur.execute(query)
    records = cur.fetchall()
    result_table = [table[0] for table in records]

    return np.sort(np.array(result_table))

path = 'reference/expected_dictionary_table.txt'
expected_table = get_list_table_from_file(path=path)
result_table = get_list_table_from_db(cursor=get_cursor(), variables=expected_table)
missing_values = np.setdiff1d(expected_table, result_table)

@pytest.mark.parametrize("a, b, c",[(expected_table,result_table,missing_values)])
def test_complete_template_dictionary_table(a,b,c):
    assert np.array_equal(a,b), "There are missing tables "+str(c)
