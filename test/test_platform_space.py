"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_space_from_file(path):
    list_space = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        space = row.strip()
        list_space.append(space)
        
    return np.sort(np.array(list_space))

def get_list_space_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_space = kwargs['spaces']
    parsed_abbrevs = [parsed_space.strip().split('=')[0] for parsed_space in expected_space]
    
    list_abbrev = [abbrev for abbrev in parsed_abbrevs]
    joined_abbrev = "','".join(list_abbrev)
    query = "SELECT abbrev||'='||menu_data FROM platform.space WHERE abbrev in ('{0}')".format(joined_abbrev)
    
    cur.execute(query)
    records = cur.fetchall()
    result_space = [space[0] for space in records]

    return np.sort(np.array(result_space))

path = 'reference/expected_platform_space.txt'
expected_space = get_list_space_from_file(path=path)
result_space = get_list_space_from_db(cursor=get_cursor(), spaces=expected_space)
missing_values = np.setdiff1d(expected_space, result_space)

@pytest.mark.parametrize("a, b, c",[(expected_space,result_space,missing_values)])
def test_complete_template_platform_space(a,b,c):
    assert np.array_equal(a,b), "Configs do not match "+str(c)
