"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')
schema_config = ConfigParser('config/schema.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()
list_schema = schema_config.get_schema()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_table_from_file(path):
    list_table = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        schema_table = row.strip()
        list_table.append(schema_table)

    return np.sort(np.array(list_table))

def get_list_table_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_schemas = kwargs['schemas']
    joined_schemas = str(expected_schemas.split(',')).replace('[','(').replace(']',')').replace(' ','')
    print (joined_schemas)
    query = "SELECT table_schema||'.'||table_name FROM information_schema.tables WHERE table_schema in {0};".format(joined_schemas)
    cur.execute(query)
    records = cur.fetchall()
    result_tables = [table[0] for table in records]

    return np.sort(np.array(result_tables))

path = 'reference/expected_table.txt'
expected_tables = get_list_table_from_file(path=path)
result_tables = get_list_table_from_db(cursor=get_cursor(), schemas=list_schema)
missing_values = np.setdiff1d(expected_tables, result_tables)

@pytest.mark.parametrize("a, b, c",[(expected_tables,result_tables,missing_values)])
def test_complete_schema_database_table(a,b,c):
    assert np.array_equal(a,b), "There are missing tables "+str(c)
