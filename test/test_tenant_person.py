"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_person_from_file(path):
    list_person = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        person = row.strip()
        list_person.append(person)

    return np.sort(np.array(list_person))

def get_list_person_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_person = kwargs['person']
    list_person = [person for person in expected_person]
    joined_person = "','".join(list_person)
    
    query = "SELECT username FROM tenant.person WHERE is_void = false AND username IN ('{0}');".format(joined_person)
    cur.execute(query)
    records = cur.fetchall()
    result_person = [person[0] for person in records]

    return np.sort(np.array(result_person))

path = 'reference/expected_tenant_person.txt'
expected_person = get_list_person_from_file(path=path)
result_person = get_list_person_from_db(cursor=get_cursor(), person=expected_person)
missing_values = np.setdiff1d(expected_person, result_person)

@pytest.mark.parametrize("a, b, c",[(expected_person,result_person,missing_values)])
def test_complete_template_tenant_person(a,b,c):
    assert np.array_equal(a,b), "There are missing users "+str(c)