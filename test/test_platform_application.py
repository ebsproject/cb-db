"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_application_from_file(path):
    list_application = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        application = row.strip()
        list_application.append(application)

    return np.sort(np.array(list_application))

def get_list_application_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_application = kwargs['applications']
    list_application = [application for application in expected_application]
    joined_application = "','".join(list_application)
   
    query = "SELECT abbrev FROM platform.application WHERE abbrev IN ('{0}') AND is_void = 'false' AND label IS NOT NULL AND action_label IS NOT NULL AND icon IS NOT NULL ORDER BY id;".format(joined_application)
    cur.execute(query)
    records = cur.fetchall()
    result_application = [application[0] for application in records]

    return np.sort(np.array(result_application))

path = 'reference/expected_platform_application.txt'
expected_application = get_list_application_from_file(path=path)
result_application = get_list_application_from_db(cursor=get_cursor(), applications=expected_application)
missing_values = np.setdiff1d(expected_application, result_application)

@pytest.mark.parametrize("a, b, c",[(expected_application,result_application,missing_values)])
def test_complete_template_platform_application(a,b,c):
    assert np.array_equal(a,b), "There are missing applications "+str(c)
