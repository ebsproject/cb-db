"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_module_from_file(path):
    list_module = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        module = row.strip()
        list_module.append(module)

    return np.sort(np.array(list_module))

def get_list_module_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_module = kwargs['module']
    list_module = [module.replace("'","''") for module in expected_module]
    joined_module = "','".join(list_module)
    
    query = "SELECT abbrev FROM platform.module WHERE is_void = false AND abbrev IN ('{0}');".format(joined_module)
    cur.execute(query)
    records = cur.fetchall()
    result_module = [module[0] for module in records]

    return np.sort(np.array(result_module))

path = 'reference/expected_platform_module.txt'
expected_module = get_list_module_from_file(path=path)
result_module = get_list_module_from_db(cursor=get_cursor(), module=expected_module)
missing_values = np.setdiff1d(expected_module, result_module)

@pytest.mark.parametrize("a, b, c",[(expected_module,result_module,missing_values)])
def test_complete_template_platform_module(a,b,c):
    assert np.array_equal(a,b), "There are missing modules "+str(c)