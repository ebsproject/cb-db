"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')
schema_config = ConfigParser('config/schema.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()
list_schema = schema_config.get_schema()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_index_from_file(path):
    list_table = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        schema_table = row.strip()
        list_table.append(schema_table)

    return np.sort(np.array(list_table))

def get_list_index_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_schemas = kwargs['schemas']
    joined_schemas = str(expected_schemas.split(',')).replace('[','(').replace(']',')').replace(' ','')
    query = """
        WITH
        ind_cols AS (
        SELECT
            --n.nspname AS schema_name,
            --t.relname AS table_name,
            i.relname AS index_name
            --a.attname AS column_name,
            --1 + array_position(ix.indkey, a.attnum) AS column_position
        FROM
            pg_catalog.pg_class t
        JOIN 
            pg_catalog.pg_attribute a ON t.oid = a.attrelid 
        JOIN 
            pg_catalog.pg_index ix ON t.oid = ix.indrelid
        JOIN 
            pg_catalog.pg_class i ON a.attnum = ANY(ix.indkey) AND i.oid = ix.indexrelid
        JOIN 
            pg_catalog.pg_namespace n ON n.oid = t.relnamespace
        WHERE 
            t.relkind = 'r'
        AND 
            n.nspname IN {0}
        ORDER BY
            t.relname,
            i.relname,
            array_position(ix.indkey, a.attnum)
        )
        SELECT 
            * 
        FROM 
            ind_cols
        ORDER BY 
            index_name
        ;
    """.format(joined_schemas)

    cur.execute(query)
    records = cur.fetchall()
    result_indexes = [index[0] for index in records]

    return np.sort(np.array(result_indexes))

path = 'reference/expected_index.txt'
expected_indexes = get_list_index_from_file(path=path)
result_indexes = get_list_index_from_db(cursor=get_cursor(), schemas=list_schema)
missing_values = np.setdiff1d(expected_indexes, result_indexes)

@pytest.mark.parametrize("a, b, c",[(expected_indexes,result_indexes,missing_values)])
def test_complete_schema_index(a,b,c):
    assert np.array_equal(a,b), "There are missing indexes "+str(c)
