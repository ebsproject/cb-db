"""
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_schema_from_file(path):
    list_schema = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        schema = row.strip()
        list_schema.append(schema)

    return np.sort(np.array(list_schema))

def get_list_schema_from_db(**kwargs):
    cur = kwargs['cursor']
    query = "SELECT abbrev FROM dictionary.schema WHERE is_void = 'false' AND name IS NOT NULL ORDER BY id;"
    cur.execute(query)
    records = cur.fetchall()
    result_schema = [schema[0] for schema in records]

    return np.sort(np.array(result_schema))

path = 'reference/expected_dictionary_schema.txt'
expected_schema = get_list_schema_from_file(path=path)
result_schema = get_list_schema_from_db(cursor=get_cursor(), variables=expected_schema)
missing_values = np.setdiff1d(expected_schema, result_schema)

@pytest.mark.parametrize("a, b, c",[(expected_schema,result_schema,missing_values)])
def test_complete_template_dictionary_scheme(a,b,c):
    assert np.array_equal(a,b), "There are missing schema "+str(c)
