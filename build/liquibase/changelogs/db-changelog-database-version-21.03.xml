<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
-->
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.3.xsd">

    <!-- DB-57 Add column package_id to experiment.entry -->
    <include file="changesets/21.03/schema/add_column_package_id_to_experiment.entry.sql"/>

    <!-- DB-44 Add new list_type constraints to platform.list -->
    <include file="changesets/21.03/schema/add_new_list_type_constraints_to_platform.list.sql"/>

    <!-- DB-43 Add variable LIST_SUB_TYPE -->
    <include file="changesets/21.03/data/template/add_variable_list_sub_type.sql"/>

    <!-- DB-42 Add column list_sub_type to platform.list -->
    <include file="changesets/21.03/schema/add_column_list_sub_type_to_platform.list.sql"/>

    <!-- DB-40 Create an UNKNOWN record place.geospatial_object table -->
    <include file="changesets/21.03/data/template/add_unkown_record_place.geospatial_object_table.sql"/>

    <!-- DB-46 Update config HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT in platform.config -->
    <include file="changesets/21.03/data/template/update_config_harvest_manager_plot_browser_config_wheat_in_platform.config.sql"/>

    <!-- DB-46 Update config PLOT_TYPE_TRANSPLANTED_UNKNOWN in platform.config -->
    <include file="changesets/21.03/data/template/update_config_plot_type_transplanted_unknown_in_platform.config.sql"/>

    <!-- DB-52 Add new list entities to dictionary.entity -->
    <include file="changesets/21.03/data/template/add_new_list_entities_to_dictionary.entity.sql"/>

    <!-- DB-39 Update scale values for DESIGN -->
    <include file="changesets/21.03/data/template/update_scale_values_for_design.sql"/>

    <!-- DB-46 Update config HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT in platform.config -->
    <include file="changesets/21.03/data/template/update_config_hm_germplasm_name_pattern_rice_default_in_platform.config.sql"/>

    <!-- DB-46 Update config HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE in platform.config -->
    <include file="changesets/21.03/data/template/update_config_harvest_manager_plot_browser_config_rice_in_platform.config.sql"/>

    <!-- DB-34 Delete "Modified Bulk" option in HV_METH_DISC scale values -->
    <include file="changesets/21.03/data/template/delete_modified_bulk_option_in_hv_meth_disc_scale_values.sql"/>

    <!-- DB-58 Update geospatial_coordinates for places with points -->
    <include file="changesets/21.03/data/fixture/update_geospatial_coordinates_for_places_with_points.sql"/>

    <!-- DB-58 Update geospatial_coordinates of geospatial objects with polygons -->
    <include file="changesets/21.03/data/fixture/update_geospatial_coordinates_of_geospatial_objects_with_polygons.sql"/>

    <!-- DB-58 Update geospatial_coordinates of facilities with points -->
    <include file="changesets/21.03/data/fixture/update_geospatial_coordinates_of_facilities_with_points.sql"/>

    <!-- DB-46 Update config PLOT_TYPE_TRANSPLANTED_UNKNOWN in platform.config p2 -->
    <include file="changesets/21.03/data/template/update_config_plot_type_transplanted_unknown_in_platform.config_p2.sql"/>

    <!-- DB-67 Create configurations for experiment-related names p1 -->
    <include file="changesets/21.03/data/template/add_config_experiment_name_config_to_platform.config.sql"/>

    <!-- DB-67 Create configurations for experiment-related names p2 -->
    <include file="changesets/21.03/data/template/add_config_occurence_name_config_to_platform.config.sql"/>

    <!-- DB-46 Update config HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT in platform.config p2 -->
    <include file="changesets/21.03/data/template/update_config_hm_germplasm_name_pattern_rice_default_in_platform.config_p2.sql"/>

    <!-- DB-67 Update config OCCURRENCE_NAME_CONFIG in platform.config -->
    <include file="changesets/21.03/data/template/update_config_occurrence_name_config_to_platform.config.sql"/>

    <!-- DB-46 Update config DEFAULT_DASHBOARD_WIDGETS in platform.config -->
    <include file="changesets/21.03/data/template/update_config_default_dashboard_widgets_config_rice_in_platform.config.sql"/>

    <!-- DB-46 Update config DEFAULT in platform.config -->
    <include file="changesets/21.03/data/template/update_config_default_in_platform.space.sql"/>

    <!-- DB-46 Update config ADMIN in platform.config -->
    <include file="changesets/21.03/data/template/update_config_admin_in_platform.space.sql"/>

    <!-- DB-155 Update package_id column of entry table with reference to packages -->
    <include file="changesets/21.03/data/fixture/update_package_id_column_in_entry_with_packages.sql"/>

    <!-- DB-51 Update experiment name in experiment.experiment -->
    <include file="changesets/21.03/data/fixture/update_experiment_name_in_experiment.experiment.sql"/>

    <!-- DB-51 Update experiment name in experiment.occurrence -->
    <include file="changesets/21.03/data/fixture/update_occurrence_name_in_experiment.occurrence.sql"/>

    <!-- DB-152 Populate Field X and Field Y values in fixture database -->
    <include file="changesets/21.03/data/fixture/populate_field_x_and_field_y_values_in_fixture_database.sql"/>

    <!-- DB-59 Populate breeding trial for AF1 experiment -->
    <include file="changesets/21.03/data/fixture/populate_breeding_trial_for_af_experiment.sql"/>

    <!-- DB-59 Populate breeding trial for AF2 experiment -->
    <include file="changesets/21.03/data/fixture/populate_breeding_trial_for_af2_experiment.sql"/>

    <!-- DB-41 Populate IRSEA breeding trial experiment -->
    <include file="changesets/21.03/data/fixture/populate_irsea_breeding_trial_experiment.sql"/>

    <!-- DB-41 Populate IRSEA generation nursery experiment -->
    <include file="changesets/21.03/data/fixture/populate_irsea_generation_nursery_experiment.sql"/>

    <!-- DB-166 Update config ADMIN in platform.config p2 -->
    <include file="changesets/21.03/data/template/update_config_admin_in_platform.space_p2.sql"/>

    <!-- DB-166 Update config DEFAULT in platform.config p2 -->
    <include file="changesets/21.03/data/template/update_config_default_in_platform.space_p2.sql"/>

    <!-- DB-46 Update config PLOT_TYPE_TRANSPLANTED_UNKNOWN in platform.config p3 -->
    <include file="changesets/21.03/data/template/update_config_plot_type_transplanted_unknown_in_platform.config_p3.sql"/>

</databaseChangeLog>