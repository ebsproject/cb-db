<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xmlns:pro="http://www.liquibase.org/xml/ns/pro"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd
        http://www.liquibase.org/xml/ns/pro
        http://www.liquibase.org/xml/ns/pro/liquibase-pro-4.0.xsd
        http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.0.xsd">

    <!-- CORB-4783 [Child] EM: Add data collection summary report threshold to config -->
    <include file="changesets/23.02/data/template/add_em_threshold.sql" />

    <!-- Release version  | INCLUDE CHANGESETS ABOVE THIS LINE THANK YOU! ################## -->
    <include file="changesets/23.02/tag/23.02.10-tag.xml" />

    <!-- CORB-5028 SS WL: Update configurations to use "ENTRY_CODE" instead of "ENTCODE" variable -->
    <include file="changesets/23.02/data/template/update_ss_params_config.sql" />

    <!-- CORB-5028 SS WL: Update configurations to use "ENTRY_CODE" instead of "ENTCODE" variable -->
    <include file="changesets/23.02/data/template/update_ss_query_results_config.sql" />

    <!-- CORB-5028 SS WL: Update configurations to use "ENTRY_CODE" instead of "ENTCODE" variable -->
    <include file="changesets/23.02/data/template/update_ss_databrowser_columns_config.sql" />

    <!-- CORB-5028 SS WL: Update configurations to use "ENTRY_CODE" instead of "ENTCODE" variable -->
    <include file="changesets/23.02/data/template/update_ss_databrowser_filters_config.sql" />

    <!-- DEVOPS-1716 Populate experiment BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/1_populate_experiment_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate entry_list BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/2_populate_entry_list_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate entry BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/3_populate_entry_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate occurrence BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/4_populate_occurrence_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate plot BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/5_populate_plot_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/6_populate_plot_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/7_populate_plot_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/8_populate_plot_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate experiment_design BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/9_populate_experiment_design_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/10_populate_experiment_design_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/11_populate_experiment_design_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/12_populate_experiment_design_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate geospatial_object BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/13_populate_geospatial_object_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/14_populate_geospatial_object_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/15_populate_geospatial_object_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/16_populate_geospatial_object_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate location BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/17_populate_location_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/18_populate_location_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/19_populate_location_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/20_populate_location_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate location_occurrence_group BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/21_populate_location_occurrence_group_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/22_populate_location_occurrence_group_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/23_populate_location_occurrence_group_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/24_populate_location_occurrence_group_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate tenant_protocol BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/26_populate_protocol_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate experiment_protocol BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/27_populate_experiment_protocol_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate list BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/28_populate_list_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate experiment_data BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/29_populate_experiment_data_ba_exp_1.sql" />

    <!-- DEVOPS-1716 Populate planting_instruction BA_EXP_1 -->
    <include file="changesets/23.01/data/fixture/30_populate_planting_instruction_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/31_populate_planting_instruction_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/32_populate_planting_instruction_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/33_populate_planting_instruction_ba_exp_1.sql" />
    
    <!-- DEVOPS-1716 Populate experiment BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/1_populate_experiment_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate entry_list BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/2_populate_entry_list_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate entry BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/3_populate_entry_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate occurrence BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/4_populate_occurrence_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate plot BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/5_populate_plot_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/6_populate_plot_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/7_populate_plot_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/8_populate_plot_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate experiment_design BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/9_populate_experiment_design_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/10_populate_experiment_design_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/11_populate_experiment_design_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/12_populate_experiment_design_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate geospatial_object BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/13_populate_geospatial_object_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/14_populate_geospatial_object_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/15_populate_geospatial_object_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/16_populate_geospatial_object_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate location BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/17_populate_location_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/18_populate_location_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/19_populate_location_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/20_populate_location_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate location_occurrence_group BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/21_populate_location_occurrence_group_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/22_populate_location_occurrence_group_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/23_populate_location_occurrence_group_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/24_populate_location_occurrence_group_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate tenant_protocol BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/25_populate_protocol_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate experiment_protocol BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/26_populate_experiment_protocol_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate list BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/27_populate_list_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate experiment_data BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/28_populate_experiment_data_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate planting_instruction BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/29_populate_planting_instruction_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/30_populate_planting_instruction_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/31_populate_planting_instruction_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/32_populate_planting_instruction_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate plot_data BA_EXP_1 and BA_EXP_2 -->
    <include file="changesets/23.01/data/fixture/33_populate_plot_data_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/34_populate_plot_data_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/35_populate_plot_data_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/36_populate_plot_data_ba_exp_1.sql" />
    <include file="changesets/23.01/data/fixture/37_populate_plot_data_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/38_populate_plot_data_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/39_populate_plot_data_ba_exp_2.sql" />
    <include file="changesets/23.01/data/fixture/40_populate_plot_data_ba_exp_2.sql" />

    <!-- DEVOPS-1716 Populate experiment BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/1_populate_experiment_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate entry_list BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/2_populate_entry_list_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate entry BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/3_populate_entry_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate occurrence BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/4_populate_occurrence_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate geospatial_object BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/5_populate_geospatial_object_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/6_populate_geospatial_object_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/7_populate_geospatial_object_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/8_populate_geospatial_object_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/9_populate_geospatial_object_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/10_populate_geospatial_object_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate plot BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/11_populate_plot_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/12_populate_plot_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/13_populate_plot_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/14_populate_plot_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/15_populate_plot_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/16_populate_plot_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate experiment_design BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/17_populate_experiment_design_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/18_populate_experiment_design_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/19_populate_experiment_design_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/20_populate_experiment_design_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/21_populate_experiment_design_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/22_populate_experiment_design_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate location BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/23_populate_location_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/24_populate_location_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/25_populate_location_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/26_populate_location_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/27_populate_location_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/28_populate_location_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate location_occurrence_group BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/29_populate_location_occurrence_group_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/30_populate_location_occurrence_group_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/31_populate_location_occurrence_group_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/32_populate_location_occurrence_group_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/33_populate_location_occurrence_group_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/34_populate_location_occurrence_group_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate protocol BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/35_populate_protocol_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate experiment_protocol BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/36_populate_experiment_protocol_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate list BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/37_populate_list_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate experiment_data BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/38_populate_experiment_data_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate planting_instruction BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/39_populate_planting_instruction_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/40_populate_planting_instruction_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/41_populate_planting_instruction_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/42_populate_planting_instruction_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/43_populate_planting_instruction_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/44_populate_planting_instruction_ba_exp_3.sql" />

    <!-- DEVOPS-1716 Populate plot_data BA_EXP_3 -->
    <include file="changesets/23.01/data/fixture/45_populate_plot_data_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/46_populate_plot_data_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/47_populate_plot_data_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/48_populate_plot_data_ba_exp_3.sql" />
    <include file="changesets/23.01/data/fixture/49_populate_plot_data_ba_exp_3.sql" />

    <!-- Release version  | INCLUDE CHANGESETS ABOVE THIS LINE THANK YOU! ################## -->
    <!-- <include file="changesets/23.02/tag/23.02.10-tag.xml" /> -->

    <!-- DEVOPS-1994 Populate Germplasm Maze, Rice and Wheat with special character -->
    <include file="changesets/23.02/data/fixture/populate_germplasm_mrw_special_character.sql" />

    <!-- CORB-5104 EM: Add scale values for Occurrence Status -->
    <include file="changesets/23.02/data/template/add_em_scale_value_for_occurrence_status.sql" />

    <!-- CORB-5116 CB-DB: Void duplicate entry code variable (abbrev is 'ENTCODE') -->
    <include file="changesets/23.02/data/template/void_duplicate_entry_code_variable_entcode.sql" />

    <!-- Release version  | INCLUDE CHANGESETS ABOVE THIS LINE THANK YOU! ################## -->
    <include file="changesets/23.02/tag/23.02.28-tag.xml" />

</databaseChangeLog>
