<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2024 Enterprise Breeding System

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-3.0-or-later
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xmlns:pro="http://www.liquibase.org/xml/ns/pro"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd
        http://www.liquibase.org/xml/ns/pro
        http://www.liquibase.org/xml/ns/pro/liquibase-pro-4.0.xsd
        http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.0.xsd">

    <!-- DEVOPS-1741 Populate germplasm_mta -->
    <include file="changesets/23.01/data/fixture/populate_germplasm_mta.sql" />

    <!-- DEVOPS-1837 Add scale values to OCCURRENCE_STATUS -->
    <include file="changesets/23.01/data/template/add_scale_values_to_occurrence_status.sql" />

    <!-- DEVOPS-1835 Add column other_names to germplasm.germplasm -->
    <include file="changesets/23.01/schema/add_column_other_names_to_germplasm.germplasm.sql" />

    <!-- CORB-4676 HM DB: Set minimum value for number of ears to 0 -->
    <include file="changesets/23.01/data/template/update_hm_maize_data_browser_config_001.sql" />

    <!-- CORB-4790 IM DB: Change all instances of seeds-search to seed-packages-search?dataLevel=all in all IM configs -->
    <include file="changesets/23.01/data/template/update_im_create_package_system_default_config_001.sql" />

    <!-- CORB-4790 IM DB: Change all instances of seeds-search to seed-packages-search?dataLevel=all in all IM configs -->
    <include file="changesets/23.01/data/template/update_im_update_seed_system_default_config_001.sql" />

    <!-- DEVOPS-1836 Update function/trigger which can be used to update the other_names column value -->
    <include file="changesets/23.01/schema/add_trigger_for_other_names.sql" />

    <!-- DEVOPS-1836 Populate germplasm other_names -->
    <!-- <include file="changesets/23.01/data/template/populate_germplasm_other_names.sql" /> -->

    <!-- CORB-4695 As a user, I should be able to use lists that consists of more than 500 items -->
    <include file="changesets/23.01/data/template/update_ss_bg_threshold_config.sql" />

    <!-- CORB-4708 IM Product permissions: allow non-admin to access the Internal Seed Transfer product -->
    <include file="changesets/23.01/data/template/update_add_inventory_manager_access_to_non_admins_in_platform_space.sql" />

    <!-- DEVOPS-1890 Populate other_names column selected from each crop -->
    <include file="changesets/23.01/data/fixture/populate_other_names_column_selected_from_each_crop.sql" />

    <!-- CORB-4820 Tag 'NA' as invalid trait data value when validating transactions -->
    <include file="changesets/23.01/schema/update_function_validate_transaction.sql" />

    <!-- DEVOPS-1834 Populate experiment -->
    <include file="changesets/23.01/data/fixture/1_populate_experiment.sql" />

    <!-- DEVOPS-1834 Populate entry_list -->
    <include file="changesets/23.01/data/fixture/2_populate_entry_list.sql" />

    <!-- DEVOPS-1834 Populate entry -->
    <include file="changesets/23.01/data/fixture/3_populate_entry.sql" />

    <!-- DEVOPS-1834 Populate occurrence -->
    <include file="changesets/23.01/data/fixture/4_populate_occurrence.sql" />

    <!-- DEVOPS-1834 Populate geospatial_object -->
    <include file="changesets/23.01/data/fixture/5_populate_geospatial_object.sql" />

    <!-- DEVOPS-1834 Populate location -->
    <include file="changesets/23.01/data/fixture/6_populate_location.sql" />

    <!-- DEVOPS-1834 Populate location_occurrence_group -->
    <include file="changesets/23.01/data/fixture/7_populate_location_occurrence_group.sql" />

    <!-- DEVOPS-1834 Populate plot -->
    <include file="changesets/23.01/data/fixture/8_populate_plot.sql" />

    <!-- DEVOPS-1834 Populate planting_instruction -->
    <include file="changesets/23.01/data/fixture/9_populate_planting_instruction.sql" />

    <!-- DEVOPS-1834 Populate cross -->
    <include file="changesets/23.01/data/fixture/10_populate_cross.sql" />

    <!-- DEVOPS-1834 Populate cross_parent -->
    <include file="changesets/23.01/data/fixture/11_populate_cross_parent.sql" />

    <!-- DEVOPS-1834 Populate germplasm_relation -->
    <include file="changesets/23.01/data/fixture/12_populate_germplasm_relation.sql" />

    <!-- DEVOPS-1891 Add new transaction status -->
    <include file="changesets/23.01/schema/add_new_transaction_status.sql" />

    <!-- CORB-4614 Add Planting Instructions Manager in default space -->
    <include file="changesets/23.01/data/template/add_pim_in_default_space.sql" />
    
    <!-- CORB-4649 As a user, when using the seed or germplasm search, I should be able to see two new variables so that I have relevant information regarding the seed shipments -->
    <include file="changesets/23.01/data/template/update_ss_new_variables_in_results.sql" />

    <!-- CORB-4840 Add Planting Instructions Manager and Inventory Manager in default space -->
    <include file="changesets/23.01/data/template/add_pim_and_im_in_default_space.sql" />

    <!-- DEVOPS-1922 Curate double transactions for harvest record creation -->
    <include file="changesets/23.01/data/template/curate_double_transactions_for_harvest_record_creation.sql" />

    <!-- CORB-4796 IM DB: Insert background process threshold config for Inventory Manager -->
    <include file="changesets/23.01/data/template/insert_im_bg_threshold_config_001.sql" />

    <!-- DEVOPS-1856 Curate germplasm_relation order_number -->
    <include file="changesets/23.01/data/fixture/curate_germplasm_relation_order_number.sql" />

    <!-- DEVOPS-1908 Curate program_id planting job -->
    <include file="changesets/23.01/data/template/curate_program_id_in_planting_job.sql" />

    <!-- CORB-4676 HM DB: Enable Number of ears as optional input for all remaining methods -->
    <include file="changesets/23.01/data/template/update_hm_maize_data_browser_config_002.sql" />
    
    <!-- DEVOPS-1901 Add variable ASI trait -->
    <include file="changesets/23.01/data/fixture/add_variable_asi_trait.sql" />

    <!-- CORB-4808 Remove package quantity and package unit in view entry columns config -->
    <include file="changesets/23.01/data/template/update_occurrence_view_entry_columns_config.sql" />

    <!-- DEVOPS-1961 Add new transaction status p2 -->
    <include file="changesets/23.01/schema/add_new_transaction_status_p2.sql" />

    <!-- CORB-4833 Tag 'NA' as 'missing' trait data value instead of 'invalid' -->
    <include file="changesets/23.01/schema/update_validate_transaction_function.sql" />

    <!-- DEVOPS-1959 Add site lists -->
    <include file="changesets/23.01/data/fixture/add_new_site_list.sql" />

    <!-- DEVOPS-1958 Populate mta_status and rsht -->
    <include file="changesets/23.01/data/fixture/populate_mta_status_and_rsht.sql" />
    
    <!-- Release version 23.02.01 | INCLUDE CHANGESETS ABOVE THIS LINE THANK YOU! ################## -->
    <include file="changesets/23.01/tag/23.02.01-tag.xml" />

</databaseChangeLog>
