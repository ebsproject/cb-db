/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

DO
$$
BEGIN
    EXECUTE (
        SELECT string_agg(format('GRANT USAGE ON SCHEMA %I TO scheduler', nspname), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA %I TO scheduler', nspname), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT SELECT ON ALL TABLES IN SCHEMA %I TO scheduler', nspname), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA %I TO scheduler', nspname), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT ALL PRIVILEGES ON SCHEMA timetable TO scheduler', nspname), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA timetable TO scheduler'), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg(format('GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA timetable TO scheduler'), '; ')
        FROM   pg_namespace
        WHERE  nspname <> 'information_schema'
        AND    nspname NOT LIKE 'pg\_%'
    );
    
    EXECUTE (
        SELECT string_agg('ALTER TABLE '|| schemaname || '."' || tablename ||'" OWNER TO scheduler', ';')
            FROM pg_tables WHERE schemaname IN ('timetable')
    );
    
    EXECUTE (
        SELECT string_agg('ALTER SEQUENCE '|| sequence_schema || '."' || sequence_name ||'" OWNER TO scheduler', ';')
            FROM information_schema.sequences WHERE sequence_schema IN ('timetable')
    );
    
    EXECUTE (
        SELECT string_agg('ALTER FUNCTION ' || nsp.nspname || '.' || p.proname || '(' || pg_get_function_identity_arguments(p.oid) || ') OWNER TO scheduler', ';')
            FROM pg_proc p
                JOIN pg_namespace nsp ON p.pronamespace = nsp.oid
            WHERE nsp.nspname = 'timetable'
    );
END
$$;
