--liquibase formatted sql

--changeset postgres:create_revert_normalize_text_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1056 Create platform.revert_normalize_text function



-- create function to revert normalized texts
CREATE OR REPLACE FUNCTION platform.revert_normalize_text(input_text text)
    RETURNS text
    LANGUAGE sql
AS $FUNCTION$
    -- (L= any letter; ^= space; N= any numeral, S= any of {-,',[,],+,.})

    -- m) ^/ becomes / and /^ becomes /
        -- l) L­N becomes L^N when there is only one '–' in the name and L is not preceded by a space
        with l as (
            -- k) ^) becomes ) and (^ becomes (
            with k as (
                -- j) REMOVE LEADING OR TRAILING ^
                with j as (
                    -- i) ^^ becomes ^
                    with i as (
                        -- h) ^0N becomes ^N IRTP 00123 becomes IRTP 123
                        with h as (
                            -- g) LL-LL becomes LL^LL KHAO-DAWK-MALI 105 becomes KHAO DAWK MALI 105
                            with g as (
                                -- f) NL becomes N^L EXCEPT SNL B 533A-1 becomes B 533 A-1 but B 533 A-4B is unchanged
                                with f as (
                                    -- e) LN becomes L^N EXCEPT SLN MALI105 becomes MALI 105 but MALI­F4 IS unchanged
                                    with e as (
                                        -- d) L. becomes L^ IR 63 SEL. becomes IR 64 SEL
                                        with d as (
                                            -- c) N( becomes N^( and )N becomes )^N IR64(5A) becomes IR64 (5A)
                                            with c as (
                                                -- b) L( becomes L^( and )L becomes )^L IR64(BPH) becomes IR64 (BPH)
                                                with b as (
                                                    -- a) Capitalize all letters Khao­Dawk­Mali105 becomes KHAO­DAWK­MALI105
                                                    with a as (
                                                        select
                                                            -- a) Capitalize all letters Khao­Dawk­Mali105 becomes KHAO­DAWK­MALI105
                                                            upper(input_text) a_value
                                                    )
                                                    select
                                                        -- *,
                                                        -- b) L( becomes L^( and )L becomes )^L IR64(BPH) becomes IR64 (BPH)
                                                        regexp_replace(regexp_replace(a_value, '([a-zA-Z])(\()', '\1 (', 'g'), '(\))([a-zA-Z])', ') \2', 'g') b_value
                                                    from
                                                        a
                                                )
                                                select
                                                    -- *,
                                                    -- c) N( becomes N^( and )N becomes )^N IR64(5A) becomes IR64 (5A)
                                                    regexp_replace(regexp_replace(b_value, '([0-9])(\()', '\1 (', 'g'), '(\))([0-9])', ') \2', 'g') c_value
                                                from
                                                    b
                                            )
                                            select
                                                -- *,
                                                -- d) L. becomes L^ IR 63 SEL. becomes IR 64 SEL
                                                regexp_replace(c_value, '([a-zA-Z])\.', '\1 ', 'g') d_value
                                            from
                                                c
                                        )
                                        select
                                            -- *,
                                            -- e) LN becomes L^N EXCEPT SLN MALI105 becomes MALI 105 but MALI-F4 IS unchanged
                                            regexp_replace(d_value, '([^\-\''\[\]\+\.]|)([a-zA-Z])([0-9])', '\1\2 \3', 'g') e_value
                                        from
                                            d
                                    )
                                    select
                                        -- *,
                                        -- f) NL becomes N^L EXCEPT SNL B 533A-1 becomes B 533 A-1 but B 533 A-4B is unchanged
                                        regexp_replace(e_value, '([^\-\''\[\]\+\.]|)([0-9])([a-zA-Z])', '\1\2 \3', 'g') f_value
                                    from
                                        e
                                )
                                select
                                    -- *,
                                    -- g) LL-LL becomes LL^LL KHAO-DAWK-MALI 105 becomes KHAO DAWK MALI 105
                                    regexp_replace(f_value, '([a-zA-Z]{2,})?(-)([a-zA-Z]{2,})', '\1 \3', 'g') g_value
                                from
                                    f
                            )
                            select
                                -- *,
                                -- h) ^0N becomes ^N IRTP 00123 becomes IRTP 123
                                (case
                                    when g_value similar to 'IR(\s*)[0-9]{2}(\s*)[a-zA-Z]{1}(\s*)[0-9]+%' then
                                        g_value
                                    else
                                        regexp_replace(g_value, '(\s+)(0+)([0-9])', '\1\3', 'g')
                                end) h_value
                            from
                                g
                        )
                        select
                            -- *,
                            -- i) ^^ becomes ^
                            regexp_replace(h_value, '(\s+)', ' ', 'g') i_value
                        from
                            h
                    )
                    select
                        -- *,
                        -- j) REMOVE LEADING OR TRAILING ^
                        trim(i_value) j_value
                    from
                        i
                )
                select
                    -- *,
                    -- k) ^) becomes ) and (^ becomes (
                    regexp_replace(regexp_replace(j_value, '(\s+)(\))', '\2', 'g'), '(\()(\s+)', '\1', 'g') k_value
                from
                    j
            )
            select
                -- *,
                -- l) L­N becomes L^N when there is only one '–' in the name and L is not preceded by a space
                (case
                    -- when (length(k_value) - length(replace(k_value, '-', ''))) = 1
                    when k_value similar to '[a-zA-Z0-9]+\-[0-9]+'
                        then replace(k_value, '-', ' ')
                    else
                        k_value
                end) l_value
            from
                k
        )
        select
            -- *,
            -- m) ^/ becomes / and /^ becomes /
            regexp_replace(regexp_replace(l_value, '(\s+)(\/)', '\2', 'g'), '(\/)(\s+)', '\1', 'g') m_value
        from
            l
        ;
$FUNCTION$;



-- revert changes
--rollback DROP FUNCTION platform.revert_normalize_text(text);
