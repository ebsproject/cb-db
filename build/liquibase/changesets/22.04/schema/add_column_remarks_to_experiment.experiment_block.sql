--liquibase formatted sql

--changeset postgres:add_column_remarks_to_experiment.experiment_block context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1083 Add column remarks to experiment.experiment_block



ALTER TABLE experiment.experiment_block
    ADD COLUMN remarks text;

COMMENT ON COLUMN experiment.experiment_block.remarks
    IS 'Remarks: Additional remarks about the experiment block [EXPTBLK_REMARKS]';



--rollback ALTER TABLE experiment.experiment_block DROP COLUMN remarks;