--liquibase formatted sql

--changeset postgres:create_detach_text_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Create platform.detach_text function



-- create platform.detach_text function
CREATE FUNCTION
    platform.detach_text(notes text, replace_notes text)
    RETURNS text LANGUAGE sql
AS $_$
    SELECT TRIM(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(notes, replace_notes, '', 'g'), '(.\s*)\1{1,}', '\1', 'g'), '(.+);', '\1', 'g'))
$_$;


-- revert changes
--rollback DROP FUNCTION platform.detach_text(text, text);
