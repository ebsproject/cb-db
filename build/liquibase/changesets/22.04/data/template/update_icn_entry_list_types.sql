--liquibase formatted sql

--changeset postgres:update_icn_entry_list_types context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1124 Update entry_list_type to entry_list



-- update entry_list_type
UPDATE
    experiment.entry_list AS entlist
SET
    entry_list_type = 'entry list',
    notes = platform.append_text(entlist.notes, 'DB-1124 entry_list_type')
WHERE
    entlist.entry_list_type = 'parent list'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry_list AS entlist
--rollback SET
--rollback     entry_list_type = 'parent list',
--rollback     notes = platform.detach_text(entlist.notes, 'DB-1124 entry_list_type')
--rollback WHERE
--rollback     entlist.entry_list_type = 'entry list'
--rollback     AND entlist.notes LIKE '%DB-1124 entry_list_type%'
--rollback ;
