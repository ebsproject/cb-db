--liquibase formatted sql

--changeset postgres:insert_api_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1060 Insert api messages



SELECT api.insert_messages(
    404,
    'ERR',
    'The parent you have requested does not exist.',
    'Resource not found, the entry list you have requested for does not exist.'
);



--rollback DELETE FROM api.messages WHERE message = 'The parent you have requested does not exist.';
--rollback DELETE FROM api.messages WHERE message = 'Resource not found, the entry list you have requested for does not exist.';
