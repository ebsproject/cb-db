--liquibase formatted sql

--changeset postgres:update_normalized_germplasm_names_part_1 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm_name LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized names (part 1)



-- disable triggers
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm_name
    SET
        germplasm_normalized_name = platform.normalize_text(name_value)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 0, 1000000 * 1);
EXECUTE update_normalize_name(1000000 * 1, 1000000 * 2);
EXECUTE update_normalize_name(1000000 * 2, 1000000 * 3);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm_name
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(name_value)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 0, 1000000 * 1);
--rollback EXECUTE update_normalize_name(1000000 * 1, 1000000 * 2);
--rollback EXECUTE update_normalize_name(1000000 * 2, 1000000 * 3);
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



--changeset postgres:update_normalized_germplasm_names_part_2 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm_name WHERE id > 3000000 LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized names (part 2)



-- disable triggers
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm_name
    SET
        germplasm_normalized_name = platform.normalize_text(name_value)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 3, 1000000 * 4);
EXECUTE update_normalize_name(1000000 * 4, 1000000 * 5);
EXECUTE update_normalize_name(1000000 * 5, 1000000 * 6);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm_name
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(name_value)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 3, 1000000 * 4);
--rollback EXECUTE update_normalize_name(1000000 * 4, 1000000 * 5);
--rollback EXECUTE update_normalize_name(1000000 * 5, 1000000 * 6);
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



--changeset postgres:update_normalized_germplasm_names_part_3 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm_name WHERE id > 6000000 LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized names (part 3)



-- disable triggers
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm_name
    SET
        germplasm_normalized_name = platform.normalize_text(name_value)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 6, 1000000 * 7);
EXECUTE update_normalize_name(1000000 * 7, 1000000 * 8);
EXECUTE update_normalize_name(1000000 * 8, 1000000 * 9);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm_name
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(name_value)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 6, 1000000 * 7);
--rollback EXECUTE update_normalize_name(1000000 * 7, 1000000 * 8);
--rollback EXECUTE update_normalize_name(1000000 * 8, 1000000 * 9);
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



--changeset postgres:update_normalized_germplasm_names_part_4 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm_name WHERE id > 9000000 LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized names (part 4)



-- disable triggers
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm_name
    SET
        germplasm_normalized_name = platform.normalize_text(name_value)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 9, 1000000 * 10);
EXECUTE update_normalize_name(1000000 * 10, 1000000 * 11);
EXECUTE update_normalize_name(1000000 * 11, 1000000 * 12);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm_name
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(name_value)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 9, 1000000 * 10);
--rollback EXECUTE update_normalize_name(1000000 * 10, 1000000 * 11);
--rollback EXECUTE update_normalize_name(1000000 * 11, 1000000 * 12);
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



--changeset postgres:update_normalized_germplasm_names_part_5 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm_name WHERE id > 12000000 LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized names (part 5)



-- disable triggers
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm_name
    SET
        germplasm_normalized_name = platform.normalize_text(name_value)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 12, 1000000 * 13);
EXECUTE update_normalize_name(1000000 * 13, 1000000 * 14);
EXECUTE update_normalize_name(1000000 * 14, 1000000 * 15);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm_name
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(name_value)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 12, 1000000 * 13);
--rollback EXECUTE update_normalize_name(1000000 * 13, 1000000 * 14);
--rollback EXECUTE update_normalize_name(1000000 * 14, 1000000 * 15);
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr;
