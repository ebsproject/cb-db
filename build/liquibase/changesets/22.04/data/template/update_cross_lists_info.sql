--liquibase formatted sql

--changeset postgres:populate_experiment_related_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Populate experiment-related info



-- update experiment-related info
UPDATE
    experiment.entry_list AS entlist
SET
    experiment_year = t.experiment_year,
    season_id = t.season_id,
    crop_id = t.crop_id,
    program_id = t.program_id,
    notes = platform.append_text(entlist.notes, 'DB-1082 experiment')
FROM (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            expt.experiment_year,
            expt.season_id,
            expt.crop_id,
            expt.program_id
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
        WHERE
            entlist.is_void = FALSE
            AND expt.is_void = FALSE
            AND expt.experiment_type = 'Intentional Crossing Nursery'
    ) AS t
WHERE
    t.entry_list_id = entlist.id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry_list AS entlist
--rollback SET
--rollback     experiment_year = NULL,
--rollback     season_id = NULL,
--rollback     crop_id = NULL,
--rollback     program_id = NULL,
--rollback     notes = NULL
--rollback WHERE
--rollback     entlist.notes LIKE '%DB-1082 experiment%'
--rollback     OR entlist.notes = ''
--rollback ;



--changeset postgres:populate_site_id_column context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Populate site_id column



-- update site_id column
UPDATE
    experiment.entry_list AS entlist
SET
    site_id = t.site_id,
    notes = platform.append_text(entlist.notes, 'DB-1082 site_id')
FROM (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            occ.id AS occurrence_id,
            (array_agg(occ.site_id))[1] AS site_id
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN experiment.occurrence AS occ
                ON occ.experiment_id = expt.id
        WHERE
            entlist.is_void = FALSE
            AND expt.is_void = FALSE
            AND occ.is_void = FALSE
            AND expt.experiment_type = 'Intentional Crossing Nursery'
        GROUP BY
            expt.id,
            entlist.id,
            occ.id
    ) AS t
WHERE
    t.entry_list_id = entlist.id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry_list AS entlist
--rollback SET
--rollback     site_id = NULL,
--rollback     notes = REPLACE(entlist.notes, ',DB-1082 site_id', '')
--rollback WHERE
--rollback     entlist.notes LIKE '%DB-1082 site_id%'
--rollback ;



--changeset postgres:populate_cross_count_column context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Populate cross_count column



-- update cross_count column
UPDATE
    experiment.entry_list AS entlist
SET
    cross_count = t.cross_count,
    notes = platform.append_text(entlist.notes, 'DB-1082 cross_count')
FROM (
        SELECT
            expt.id AS experiment_id,
            entlist.id AS entry_list_id,
            count(*) AS cross_count
        FROM
            experiment.experiment AS expt
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
        WHERE
            entlist.is_void = FALSE
            AND expt.is_void = FALSE
            AND crs.is_void = FALSE
        GROUP BY
            expt.id,
            entlist.id
    ) AS t
WHERE
    t.entry_list_id = entlist.id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry_list AS entlist
--rollback SET
--rollback     cross_count = NULL,
--rollback     notes = REPLACE(REPLACE(entlist.notes, 'DB-1082 cross_count', ''), ',,', '')
--rollback WHERE
--rollback     entlist.notes LIKE '%DB-1082 cross_count%'
--rollback ;
