--liquibase formatted sql

--changeset postgres:add_scale_value_to_grain_color context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1094 Add scale value to GRAIN_COLOR



-- add scale value to GRAIN_COLOR variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('GRAIN_COLOR_RG', 'RG', 1, 'red grain', 'red', 1, NULL),
        ('GRAIN_COLOR_WG', 'WG', 2, 'white grain', 'white', 1, NULL),
        ('GRAIN_COLOR_BG', 'BG', 3, 'brown grain', 'brown', 1, NULL)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'GRAIN_COLOR'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('GRAIN_COLOR_RG', 'GRAIN_COLOR_WG', 'GRAIN_COLOR_BG')
--rollback ;