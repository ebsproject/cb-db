--liquibase formatted sql

--changeset postgres:update_normalized_germplasm_designations_part_1 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized designations (part 1)



-- disable triggers
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm
    SET
        germplasm_normalized_name = platform.normalize_text(designation)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 0, 1000000 * 1);
EXECUTE update_normalize_name(1000000 * 1, 1000000 * 2);
EXECUTE update_normalize_name(1000000 * 2, 1000000 * 3);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(designation)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 0, 1000000 * 1);
--rollback EXECUTE update_normalize_name(1000000 * 1, 1000000 * 2);
--rollback EXECUTE update_normalize_name(1000000 * 2, 1000000 * 3);
--rollback 
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;



--changeset postgres:update_normalized_germplasm_designations_part_2 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT (CASE WHEN (SELECT 1 FROM germplasm.germplasm WHERE id > 3000000 LIMIT 1) = 1 THEN 1 ELSE 0 END);
--comment: DB-1056 Update normalized designations (part 2)



-- disable triggers
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

-- update germplasm_normalized_name by batch
PREPARE update_normalize_name(integer, integer) AS
    UPDATE
        germplasm.germplasm
    SET
        germplasm_normalized_name = platform.normalize_text(designation)
    WHERE
        id > $1 AND id <= $2
;

EXECUTE update_normalize_name(1000000 * 3, 1000000 * 4);
EXECUTE update_normalize_name(1000000 * 4, 1000000 * 5);
EXECUTE update_normalize_name(1000000 * 5, 1000000 * 6);

DEALLOCATE update_normalize_name;

-- enable triggers
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback PREPARE update_normalize_name(integer, integer) AS
--rollback     UPDATE
--rollback         germplasm.germplasm
--rollback     SET
--rollback         germplasm_normalized_name = platform.revert_normalize_text(designation)
--rollback     WHERE
--rollback         id > $1 AND id <= $2
--rollback ;
--rollback 
--rollback EXECUTE update_normalize_name(1000000 * 3, 1000000 * 4);
--rollback EXECUTE update_normalize_name(1000000 * 4, 1000000 * 5);
--rollback EXECUTE update_normalize_name(1000000 * 5, 1000000 * 6);
--rollback 
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_populate_germplasm_normalized_name_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
