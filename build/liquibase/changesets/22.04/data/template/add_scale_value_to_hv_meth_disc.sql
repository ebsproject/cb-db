--liquibase formatted sql

--changeset postgres:add_scale_value_to_hv_meth_disc context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1093 Add scale value to HV_METH_DISC



-- add scale value to HV_METH_DISC variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('HV_METH_DISC_MAINTAIN_AND_BULK', 'Maintain and Bulk', 22, 'Maintain and Bulk', 'Maintain and Bulk', 1, NULL),
        ('HV_METH_DISC_DH1_INDIVIDUAL_EAR', 'DH1 Individual Ear', 23, 'DH1 Individual Ear', 'DH1 Individual Ear', 1, NULL)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'HV_METH_DISC'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('HV_METH_DISC_MAINTAIN_AND_BULK', 'HV_METH_DISC_DH1_INDIVIDUAL_EAR')
--rollback ;