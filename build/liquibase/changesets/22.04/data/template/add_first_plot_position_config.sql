--liquibase formatted sql

--changeset postgres:add_first_plot_position_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1115 EC EM: Add config for FIRST_PLOT_POSITION_VIEW_DEFAULT in platform.config


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'FIRST_PLOT_POSITION_VIEW_DEFAULT',
    'Default First Plot Position View', 
    $${
        "value": "Top Left"
    }$$,
    1,
    'First plot position in viewing layout',
    1,
    'added by jp.ramos'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'FIRST_PLOT_POSITION_VIEW_DEFAULT';