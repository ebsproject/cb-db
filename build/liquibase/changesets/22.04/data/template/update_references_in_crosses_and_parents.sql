--liquibase formatted sql

--changeset postgres:update_cross_references context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Update references in germplasm.cross table



-- update references in germplasm.cross table
UPDATE
    germplasm.cross AS crs
SET
    entry_list_id = t.entry_list_id,
    notes = platform.append_text(crs.notes, 'DB-1082 cross')
FROM (
        SELECT
            crs.id AS cross_id,
            entlist.id AS entry_list_id
        FROM
            germplasm.cross AS crs
            JOIN LATERAL (
                    -- get first experiment of female parent in a cross
                    SELECT
                        crspar.experiment_id
                    FROM
                        germplasm.cross_parent AS crspar
                    WHERE
                        crspar.cross_id = crs.id
                        AND crspar.parent_role IN ('female', 'female-and-male')
                        AND NOT crspar.is_void
                    LIMIT
                        1
                ) AS crspar
                ON TRUE
            JOIN experiment.experiment AS expt
                ON expt.id = crspar.experiment_id
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
        WHERE
            crs.entry_list_id IS NULL
    ) AS t
WHERE
    crs.id = t.cross_id
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.cross AS crs
--rollback SET
--rollback     entry_list_id = NULL,
--rollback     notes = (CASE platform.detach_text(crs.notes, 'DB-1082 cross') WHEN '' THEN NULL ELSE platform.detach_text(crs.notes, 'DB-1082 cross') END)
--rollback WHERE
--rollback     crs.notes LIKE '%DB-1082 cross%'
--rollback ;



--changeset postgres:update_cross_parent_references context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1082 Update references in germplasm.cross_parent table



-- update references in germplasm.cross_parent table
UPDATE
    germplasm.cross_parent AS crspar
SET
    occurrence_id = t.occurrence_id,
    notes = platform.append_text(crspar.notes, 'DB-1082 cross parent')
FROM (
        SELECT
            crspar.id AS cross_parent_id,
            occ.occurrence_id
        FROM
            germplasm.cross_parent AS crspar
            JOIN experiment.experiment AS expt
                ON expt.id = crspar.experiment_id
            JOIN experiment.entry AS ent
                ON ent.id = crspar.entry_id
            JOIN experiment.entry_list AS entlist
                ON entlist.id = ent.entry_list_id
            JOIN LATERAL (
                    -- get first occurrence of the experiment
                    SELECT
                        occ.id AS occurrence_id
                    FROM
                        experiment.occurrence AS occ
                    WHERE
                        occ.experiment_id = expt.id
                        AND occ.experiment_id = entlist.experiment_id
                        AND NOT occ.is_void
                    LIMIT
                        1
                ) AS occ
                ON TRUE
        WHERE
            crspar.occurrence_id IS NULL
    ) AS t
WHERE
    crspar.id = t.cross_parent_id
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.cross_parent AS crspar
--rollback SET
--rollback     occurrence_id = NULL,
--rollback     notes = (CASE platform.detach_text(crspar.notes, 'DB-1082 cross parent') WHEN '' THEN NULL ELSE platform.detach_text(crspar.notes, 'DB-1082 cross parent') END)
--rollback WHERE
--rollback     crspar.notes LIKE '%DB-1082 cross parent%'
--rollback ;
