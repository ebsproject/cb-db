--liquibase formatted sql

--changeset postgres:update_grain_color_data_level context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1102 Update GRAIN_COLOR data_level to include cross



--update germplasm name config
UPDATE master.variable
SET
	data_level = 'entry, plot, germplasm, cross'
WHERE
    abbrev = 'GRAIN_COLOR'
;

--rollback UPDATE master.variable
--rollback SET
--rollback      data_level = 'entry, plot, germplasm'
--rollback WHERE
--rollback      abbrev = 'GRAIN_COLOR'
--rollback ;