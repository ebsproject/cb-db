--liquibase formatted sql

--changeset postgres:fix_display_name_of_ht12_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1116 EM: Fix display_name of HT12_CONT



UPDATE
    master.variable
SET
    display_name='Plant height of sample 12 in centimeters'
WHERE
    abbrev = 'HT12_CONT'
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     display_name='	Plant height of sample 12 in centimeters'
--rollback WHERE
--rollback     abbrev = 'HT12_CONT'
--rollback ;