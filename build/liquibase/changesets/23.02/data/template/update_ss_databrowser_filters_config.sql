--liquibase formatted sql

--changeset postgres:update_ss_databrowser_filters_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5028 SS WL: Update configurations to use "ENTRY_CODE" instead of "ENTCODE" variable



-- update config
UPDATE platform.config
SET
    config_value = $${
    "name": "Find Seed Data Browser Filters",
    "values": [
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Study Name",
        "order_number": "1",
        "target_table": "operational.study",
        "default_value": "",
        "target_column": "name",
        "allowed_values": "",
        "variable_abbrev": "STUDY_NAME",
        "field_description": "Name of the study or experiment location",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Designation",
        "order_number": "2",
        "target_table": "master.product",
        "default_value": "",
        "target_column": "designation",
        "allowed_values": "",
        "variable_abbrev": "DESIGNATION",
        "field_description": "Preferred name of the germplasm",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Pedigree",
        "order_number": "3",
        "target_table": "master.product",
        "default_value": "",
        "target_column": "parentage",
        "allowed_values": "",
        "variable_abbrev": "PARENTAGE",
        "field_description": "Parentage of the germplasm",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Entry",
        "order_number": "4",
        "target_table": "operational.entry",
        "default_value": "",
        "target_column": "entcode",
        "allowed_values": "",
        "variable_abbrev": "ENTRY_CODE",
        "field_description": "Record of the germplasm in the experiment location",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Plot",
        "order_number": "5",
        "target_table": "operational.plot",
        "default_value": "",
        "target_column": "code",
        "allowed_values": "",
        "variable_abbrev": "PLOT_CODE",
        "field_description": "Plot where the germplasm is planted in the experiment location",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "GID",
        "order_number": "6",
        "target_table": "operational.seed_storage",
        "default_value": "",
        "target_column": "gid",
        "allowed_values": "",
        "variable_abbrev": "GID",
        "field_description": "Germplasm identifier",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Facility",
        "order_number": "7",
        "target_table": "master.facility",
        "default_value": "",
        "target_column": "name",
        "allowed_values": "",
        "variable_abbrev": "FACILITY",
        "field_description": "Location where the seeds are stored in facilities",
        "primary_attribute": "",
        "secondary_attribute": ""
        },
        {
        "disabled": "false",
        "required": "false",
        "field_label": "Container",
        "order_number": "7",
        "target_table": "seed_warehouse.container",
        "default_value": "",
        "target_column": "label",
        "allowed_values": "",
        "variable_abbrev": "CONTAINER",
        "field_description": "Location where the seeds are stored in containers",
        "primary_attribute": "",
        "secondary_attribute": ""
        }
    ]
    }$$
WHERE
    abbrev = 'FIND_SEED_DATA_BROWSER_FILTERS';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback   "name": "Find Seed Data Browser Filters",
--rollback   "values": [
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Study Name",
--rollback       "order_number": "1",
--rollback       "target_table": "operational.study",
--rollback       "default_value": "",
--rollback       "target_column": "name",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "STUDY_NAME",
--rollback       "field_description": "Name of the study or experiment location",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Designation",
--rollback       "order_number": "2",
--rollback       "target_table": "master.product",
--rollback       "default_value": "",
--rollback       "target_column": "designation",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "DESIGNATION",
--rollback       "field_description": "Preferred name of the germplasm",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Pedigree",
--rollback       "order_number": "3",
--rollback       "target_table": "master.product",
--rollback       "default_value": "",
--rollback       "target_column": "parentage",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "PARENTAGE",
--rollback       "field_description": "Parentage of the germplasm",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Entry",
--rollback       "order_number": "4",
--rollback       "target_table": "operational.entry",
--rollback       "default_value": "",
--rollback       "target_column": "entcode",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "ENTCODE",
--rollback       "field_description": "Record of the germplasm in the experiment location",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Plot",
--rollback       "order_number": "5",
--rollback       "target_table": "operational.plot",
--rollback       "default_value": "",
--rollback       "target_column": "code",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "PLOT_CODE",
--rollback       "field_description": "Plot where the germplasm is planted in the experiment location",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "GID",
--rollback       "order_number": "6",
--rollback       "target_table": "operational.seed_storage",
--rollback       "default_value": "",
--rollback       "target_column": "gid",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "GID",
--rollback       "field_description": "Germplasm identifier",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Facility",
--rollback       "order_number": "7",
--rollback       "target_table": "master.facility",
--rollback       "default_value": "",
--rollback       "target_column": "name",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "FACILITY",
--rollback       "field_description": "Location where the seeds are stored in facilities",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     },
--rollback     {
--rollback       "disabled": "false",
--rollback       "required": "false",
--rollback       "field_label": "Container",
--rollback       "order_number": "7",
--rollback       "target_table": "seed_warehouse.container",
--rollback       "default_value": "",
--rollback       "target_column": "label",
--rollback       "allowed_values": "",
--rollback       "variable_abbrev": "CONTAINER",
--rollback       "field_description": "Location where the seeds are stored in containers",
--rollback       "primary_attribute": "",
--rollback       "secondary_attribute": ""
--rollback     }
--rollback   ]
--rollback      }$$
--rollback WHERE
--rollback     abbrev = 'FIND_SEED_DATA_BROWSER_FILTERS';