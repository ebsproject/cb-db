--liquibase formatted sql

--changeset postgres:void_duplicate_entry_code_variable_entcode context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5116 CB-DB: Void duplicate entry code variable (abbrev is 'ENTCODE')



UPDATE
    master.variable
SET
    is_void = TRUE
WHERE
    abbrev = 'ENTCODE';



--rollback UPDATE
--rollback 	master.variable
--rollback SET
--rollback 	is_void = FALSE
--rollback WHERE
--rollback 	abbrev = 'ENTCODE';