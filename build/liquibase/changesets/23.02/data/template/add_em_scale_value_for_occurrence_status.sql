--liquibase formatted sql

--changeset postgres:add_em_scale_value_for_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5104 EM: Add scale values for Occurrence Status



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'OCCURRENCE_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('mapped;trait data collected;draft packing', 'Mapped; Trait Data Collected; Draft Packing', 'Mapped; Trait Data Collected; Draft Packing', 'MAPPED_TRAIT_DATA_COLLECTED_DRAFT_PACKING', 'show'),
        ('mapped;trait data collected;packed', 'Mapped; Trait Data Collected; Packed', 'Mapped; Trait Data Collected; Packed', 'MAPPED_TRAIT_DATA_COLLECTED_PACKED', 'show'),
        ('mapped;trait data collected;packing', 'Mapped; Trait Data Collected; Packing', 'Mapped; Trait Data Collected; Packing', 'MAPPED_TRAIT_DATA_COLLECTED_PACKING', 'show'),
        ('mapped;trait data collected;packing cancelled', 'Mapped; Trait Data Collected; Packing Cancelled', 'Mapped; Trait Data Collected; Packing Cancelled', 'MAPPED_TRAIT_DATA_COLLECTED_PACKING_CANCELLED', 'show'),
        ('mapped;trait data collected;packing on hold', 'Mapped; Trait Data Collected; Packing on Hold', 'Mapped; Trait Data Collected; Packing on Hold', 'MAPPED_TRAIT_DATA_COLLECTED_PACKING_ON_HOLD', 'show'),
        ('mapped;trait data collected;ready for packing', 'Mapped; Trait Data Collected; Ready for Packing', 'Trait Data Collected; Ready for Packing', 'MAPPED_TRAIT_DATA_COLLECTED_READY_FOR_PACKING', 'show'),
        ('mapped;draft packing;trait data collected', 'Mapped; Draft Packing; Trait Data Collected', 'Draft Packing; Trait Data Collected', 'MAPPED_DRAFT_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('mapped;packed;trait data collected', 'Mapped; Packed; Trait Data Collected', 'Mapped; Packed; Trait Data Collected', 'MAPPED_PACKED_TRAIT_DATA_COLLECTED', 'show'),
        ('mapped;packing;trait data collected', 'Mapped; Packing; Trait Data Collected', 'Mapped; Packing; Trait Data Collected', 'MAPPED_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('mapped;packing on hold;trait data collected', 'Mapped; Packing on Hold; Trait Data Collected', 'Mapped; Packing on Hold; Trait Data Collected', 'MAPPED_PACKING_ON_HOLD_TRAIT_DATA_COLLECTED', 'show'),
        ('mapped;packing cancelled;trait data collected', 'Mapped; Packing Cancelled; Trait Data Collected', 'Mapped; Packing Cancelled; Trait Data Collected', 'MAPPED_PACKING_CANCELLED_TRAIT_DATA_COLLECTED', 'show'),
        ('mapped;ready for packing;trait data collected', 'Mapped; Ready for Packing; Trait Data Collected', 'Mapped; Ready for Packing; Trait Data Collected', 'MAPPED_READY_FOR_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;draft packing;trait data collected', 'Planted; Draft Packing; Trait Data Collected', 'Planted; Draft Packing; Trait Data Collected', 'PLANTED_DRAFT_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;trait data collected;draft packing', 'Planted; Trait Data Collected; Draft Packing', 'Planted; Trait Data Collected; Draft Packing', 'PLANTED_TRAIT_DATA_COLLECTED_DRAFT_PACKING', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_DRAFT_PACKING',
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_PACKED',
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_PACKING',
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_PACKING_CANCELLED',
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_PACKING_ON_HOLD',
--rollback      'OCCURRENCE_STATUS_MAPPED_TRAIT_DATA_COLLECTED_READY_FOR_PACKING',
--rollback      'OCCURRENCE_STATUS_MAPPED_DRAFT_PACKING_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_MAPPED_PACKED_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_MAPPED_PACKING_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_MAPPED_PACKING_ON_HOLD_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_MAPPED_PACKING_CANCELLED_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_MAPPED_READY_FOR_PACKING_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_PLANTED_DRAFT_PACKING_TRAIT_DATA_COLLECTED',
--rollback      'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_DRAFT_PACKING'
--rollback     )
--rollback ;