--liquibase formatted sql

--changeset postgres:add_em_threshold context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4783 [Child] EM: Add data collection summary report threshold to config



UPDATE
  platform.config
SET
  config_value = '{
    "exportRecords": {
      "size": "5000",
      "description": "Minimum amount of records to trigger background processing"
    },
    "saveListMembers": {
      "size": "1000",
      "description": "Save list members"
    },
    "generateLocation": {
      "size": "1000",
      "description": "Generate location for an Occurrence"
    },
    "selectOccurrences": {
      "size": "300",
      "description": "Limit for selecting Occurrences across pages"
    },
    "exportCrossRecords": {
      "size": "1000",
      "description": "Minimum amount of Cross records to trigger background processing"
    },
    "uploadPlantingArrays": {
      "size": "500",
      "description": "Upload planting arrays for an Occurrence"
    },
    "exportCsvMappingFiles": {
      "size": "10000",
      "description": "Minimum amount of CSV Mapping file records to trigger background processing"
    },
    "exportDataCollectionSummaries": {
      "size": "1000",
      "description": "Minimum amount of plot records to trigger background processing for Data Collection summaries"
    }
  }'
WHERE
  abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD'
;



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = '{
--rollback   "exportRecords": {
--rollback     "size": "5000",
--rollback     "description": "Minimum amount of records to trigger background processing"
--rollback   },
--rollback   "saveListMembers": {
--rollback     "size": "1000",
--rollback     "description": "Save list members"
--rollback   },
--rollback   "generateLocation": {
--rollback     "size": "1000",
--rollback     "description": "Generate location for an Occurrence"
--rollback   },
--rollback   "selectOccurrences": {
--rollback     "size": "300",
--rollback     "description": "Limit for selecting Occurrences across pages"
--rollback   },
--rollback   "exportCrossRecords": {
--rollback     "size": "1000",
--rollback     "description": "Minimum amount of Cross records to trigger background processing"
--rollback   },
--rollback   "uploadPlantingArrays": {
--rollback     "size": "500",
--rollback     "description": "Upload planting arrays for an Occurrence"
--rollback   },
--rollback   "exportCsvMappingFiles": {
--rollback     "size": "10000",
--rollback     "description": "Minimum amount of CSV Mapping file records to trigger background processing"
--rollback   }
--rollback }'
--rollback WHERE
--rollback 	abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';