
--liquibase formatted sql

--changeset postgres:populate_germplasm_mrw_special_character context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1994 Populate Germplasm Maze, Rice and Wheat with special character



INSERT INTO 
    germplasm.germplasm 
        (designation, parentage, generation, germplasm_state, germplasm_name_type, crop_id, taxonomy_id, creator_id, notes)		
SELECT 
    t.designation,
    t.parentage,
    t.generation,
    t.germplasm_state,
    t.germplasm_name_type,
    c.id  AS crop_id,
    t2.id,
    c.creator_id,
    'DEVOPS-1994 Populate Germplasm'
FROM
    (
        VALUES 
            ('IR*107*(111-1-1-1-1)','?/?','F7','fixed','line_name',1),
            ('IR^107*(111-1-1-1-2))','?/?','F7','fixed','line_name',1),
            ('IR(107)*(111-1-1-1-1)','?/?','F7','fixed','line_name',1),
            ('IR#107*(111-1-1-1-2))','?/?','F7','fixed','line_name',1),
            ('IR@107*(111-1-1-1-1)','?/?','F7','fixed','line_name',1),
            ('IR[107](111-1-1-1-2))','?/?','F7','fixed','line_name',1),
            ('IR-107*(111-1-1-1-1)','?/?','F7','fixed','line_name',1),
            ('IR/107*(111-1-1-1-2))','?/?','F7','fixed','line_name',1),
            ('IR&107*(111-1-1-1-1)','?/?','F7','fixed','line_name',1),
            ('IR:107*(111-1-1-1-2))','?/?','F7','fixed','line_name',1)                                                
    ) AS t(designation,parentage,generation,germplasm_state,germplasm_name_type,creator_id)
JOIN
    tenant.crop c 
ON
    crop_code = 'RICE'
JOIN 
    germplasm.taxonomy t2 
ON
    taxon_id = '4530'
;

INSERT INTO 
    germplasm.germplasm 
        (designation, parentage, generation, germplasm_state, germplasm_name_type, crop_id, taxonomy_id, creator_id, notes)		
SELECT 
    t.designation,
    t.parentage,
    t.generation,
    t.germplasm_state,
    t.germplasm_name_type,
    c.id  AS crop_id,
    t2.id,
    c.creator_id,
    'DEVOPS-1994 Populate Germplasm'
FROM
    (
        VALUES 
            ('TCI141276*0SE-0100TE-3DYR-0E','?/?','F7','fixed','line_name',1),
            ('TCI1412760SE^0100TE-3DYRs-0E','?/?','F7','fixed','line_name',1),
            ('TCI141276(0SE)-0100E-3DYR-0E','?/?','F7','fixed','line_name',1),
            ('CDSS09#Y00449Y-01-1Yf-04Y-0B','?/?','F7','fixed','line_name',1),
            ('CDSS09@Y00449S-09Y-0M-04Y-0B','?/?','F7','fixed','line_name',1),
            ('CMSS08[B00996S]-09B-099Y-20Y','?/?','F7','fixed','line_name',1),
            ('CDSS10Y0-0291S-09M-5Y-06Y-0B','?/?','F7','fixed','line_name',1),
            ('CDSS10Y0/0291S-09Y-2M-06Y-0B','?/?','F7','fixed','line_name',1),
            ('CMSS08B01&003S-099B9Y-22B-0Y','?/?','F7','fixed','line_name',1),
            ('TCI14:1031-0SE-010dE-2DYR-0E','?/?','F7','fixed','line_name',1)                                               
    ) AS t(designation,parentage,generation,germplasm_state,germplasm_name_type,notes)
JOIN
    tenant.crop c 
ON
    crop_code = 'WHEAT'
JOIN 
    germplasm.taxonomy t2 
ON
    taxon_id = '4565'
;

INSERT INTO 
    germplasm.germplasm 
        (designation, parentage, generation, germplasm_state, germplasm_name_type, crop_id, taxonomy_id, creator_id, notes)	
SELECT 
    t.designation,
    t.parentage,
    t.generation,
    t.germplasm_state,
    t.germplasm_name_type,
    c.id  AS crop_id,
    t2.id,
    c.creator_id,
    'DEVOPS-1994 Populate Germplasm'
FROM
    (
        VALUES 
            ('(FHWL42/FHWL120668)*16','?/?','F7','fixed','line_name',1),
            ('(ABLTI0139/CM43)^140-B','?/?','F7','fixed','line_name',1),
            ('TCI141276(0STE)3DYR-0E','?/?','F7','fixed','line_name',1),
            ('(CML494#OFP9)-11-1-B*4','?/?','F7','fixed','line_name',1),
            ('(ABL[I0]39/CML3)@140-B','?/?','F7','fixed','line_name',1),
            ('EEL822/LMC-4d[f@]xcxc3','?/?','F7','fixed','line_name',1),
            ('(HTL437/ABP38)-B-182-B','?/?','F7','fixed','line_name',1),
            ('MPKE18FS/DS&SDFDFSDY03','?/?','F7','fixed','line_name',1),
            ('ABDHFDSFD&DFDSFL120918','?/?','F7','fixed','line_name',1),
            ('(ABLTI9/F:WL12618)@104','?/?','F7','fixed','line_name',1)                                             
    ) AS t(designation,parentage,generation,germplasm_state,germplasm_name_type,notes)
JOIN
    tenant.crop c 
ON
    crop_code = 'MAIZE'
JOIN 
    germplasm.taxonomy t2 
ON
    taxon_id = '4570';


INSERT INTO 
    germplasm.germplasm_name (name_value, germplasm_name_type, germplasm_name_status, creator_id, germplasm_id, notes)
SELECT  
t.name_value, 
t.germplasm_name_type, 
t.germplasm_name_status, 
t.creator_id,
t.germplasm_id,
t.notes
FROM
    (
        VALUES
            ('IR16A27*25A2725A275A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR*107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27^25A2725A272A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR^107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27(25A)225A2725272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR(107)*(111-1-1-1-1)'  and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27#52A2725A2725A72523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR#107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27@5A2725A2725A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR@107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27[5A2]25A2725A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR[107](111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27-5A2725A2725A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR-107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27/5A2725A2725A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR/107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('IR16A27&5A2725A2725A272523323','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR&107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI112S:036-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'IR:107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI121S*036-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'TCI141276*0SE-0100TE-3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12S1^036-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'TCI1412760SE^0100TE-3DYRs-0E' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP(0)6-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'TCI141276(0SE)-0100E-3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP0#36-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CDSS09#Y00449Y-01-1Yf-04Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP0@16-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CDSS09@Y00449S-09Y-0M-04Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP0[61]0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CMSS08[B00996S]-09B-099Y-20Y' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP-36-10E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CDSS10Y0-0291S-09M-5Y-06Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP/36-01E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CDSS10Y0/0291S-09Y-2M-06Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP&316-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'CMSS08B01&003S-099B9Y-22B-0Y' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP:316-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'TCI14:1031-0SE-010dE-2DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('TCI12SP*361-0E-0E-0E-0E-7E-0E','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(FHWL42/FHWL120668)*16' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y^0622S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(ABLTI0139/CM43)^140-B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y(00)2S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'TCI141276(0STE)3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y#0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(CML494#OFP9)-11-1-B*4' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y@0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(ABL[I0]39/CML3)@140-B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y[0]62S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'EEL822/LMC-4d[f@]xcxc3' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y-0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(HTL437/ABP38)-B-182-B' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y/0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'MPKE18FS/DS&SDFDFSDY03' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y&0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = 'ABDHFDSFD&DFDSFL120918' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm'),
            ('CTSS15Y:0062S-29Y-4M-1M-4M-0Y','line_name','active',1,(SELECT id FROM germplasm.germplasm where designation = '(ABLTI9/F:WL12618)@104' and notes = 'DEVOPS-1994 Populate Germplasm'), 'DEVOPS-1994 Populate Germplasm')
    )	t(name_value, germplasm_name_type, germplasm_name_status, creator_id, germplasm_id, notes)
;

INSERT INTO 
    germplasm.seed 
        (seed_code, seed_name, creator_id, is_void, notes, germplasm_id)	
SELECT 
    t.seed_code,
    t.seed_name,
    t.creator_id, 
    t.is_void,
    t.notes,
    t.germplasm_id
    FROM
        (
            VALUES
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('1Seedname*',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR*107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('2Seedname^',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR^107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('3Seedname()',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR(107)*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('4Seedname#',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR#107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('5Seedname@',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR@107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('6Seedname[]',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR[107](111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('7Seedname-',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR-107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('8Seedname/',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR/107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('9Seedname&',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm', (SELECT id FROM germplasm.germplasm where designation = 'IR&107*(111-1-1-1-1)' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('10Seedname:',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'IR:107*(111-1-1-1-2))' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('11Seedname*',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'TCI141276*0SE-0100TE-3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('12Seedname^',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'TCI1412760SE^0100TE-3DYRs-0E' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('13Seedname()',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'TCI141276(0SE)-0100E-3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('14Seedname#',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CDSS09#Y00449Y-01-1Yf-04Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('15Seedname@',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CDSS09@Y00449S-09Y-0M-04Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('16Seedname[]',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CMSS08[B00996S]-09B-099Y-20Y' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('17Seedname-',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CDSS10Y0-0291S-09M-5Y-06Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('18Seedname/',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CDSS10Y0/0291S-09Y-2M-06Y-0B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('19Seedname&',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'CMSS08B01&003S-099B9Y-22B-0Y' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('20Seedname:',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'TCI14:1031-0SE-010dE-2DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('21Seedname*',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(FHWL42/FHWL120668)*16' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('22Seedname^',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(ABLTI0139/CM43)^140-B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('23Seedname()',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'TCI141276(0STE)3DYR-0E' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('24Seedname#',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(CML494#OFP9)-11-1-B*4' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('25Seedname@',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(ABL[I0]39/CML3)@140-B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('26Seedname[]',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'EEL822/LMC-4d[f@]xcxc3' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('27Seedname-',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(HTL437/ABP38)-B-182-B' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('28Seedname/',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'MPKE18FS/DS&SDFDFSDY03' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('29Seedname&',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = 'ABDHFDSFD&DFDSFL120918' and notes = 'DEVOPS-1994 Populate Germplasm' ) ),
                ((SELECT germplasm.generate_code('seed')),(SELECT concat('30Seedname:',md5(random()::text))),1, false, 'DEVOPS-1994 Populate Germplasm',(SELECT id FROM germplasm.germplasm where designation = '(ABLTI9/F:WL12618)@104' and notes = 'DEVOPS-1994 Populate Germplasm' ) )
        ) t (seed_code, seed_name,  creator_id, is_void , notes, germplasm_id)
;
        
INSERT INTO 
    germplasm.package 
        (package_code, package_label, package_quantity, package_unit, package_status, seed_id, creator_id, is_void, notes)	
SELECT 
    t.package_code,
    t.package_label,
    t.package_quantity, 
    t.package_unit,
    t.package_status,
    t.seed_id,
    creator_id,
    is_void,
    'DEVOPS-1994 Populate Germplasm'
    FROM
        (
            VALUES
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel*',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '1Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel^',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '2Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel()',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '3Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel#',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '4Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel@',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '5Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel[',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '6Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel-',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '7Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel/',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '8Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel&',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '9Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel:',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '10Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel*',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '11Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel^',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '12Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel()',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '13Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel#',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '14Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel@',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '15Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel[]',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '16Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel-',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '17Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel/',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '18Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel&',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '19Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel:',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '20Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel*',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '21Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel^',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '22Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel()',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '23Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel#',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '24Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel@',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '25Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel[]',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '26Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel-',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '27Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel/',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '28Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel&',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '29Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm'),
                ((SELECT germplasm.generate_code('package')),(SELECT concat('packagelabel:',md5(random()::text))),CAST(((1500 - 0 + 1) * random() + 0) AS FLOAT),'g','active',(SELECT id FROM germplasm.seed WHERE seed_name LIKE '30Seedname%'),1,false,'DEVOPS-1994 Populate Germplasm')						
        ) t (package_code,package_label,package_quantity,package_unit,package_status,seed_id,creator_id,is_void,notes)
;

-- revert changes
--rollback  DELETE FROM germplasm.package WHERE notes = 'DEVOPS-1994 Populate Germplasm';
--rollback  DELETE FROM germplasm.germplasm_name WHERE notes = 'DEVOPS-1994 Populate Germplasm';
--rollback  DELETE FROM germplasm.seed WHERE notes = 'DEVOPS-1994 Populate Germplasm';
--rollback  DELETE FROM germplasm.germplasm WHERE notes = 'DEVOPS-1994 Populate Germplasm';