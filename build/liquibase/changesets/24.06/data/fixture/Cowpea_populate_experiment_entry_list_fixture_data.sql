--liquibase formatted sql

--changeset postgres:Cowpea_populate_experiment_entry_list_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to experiment.entry_list



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,is_void,notes,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
 VALUES 
((experiment.generate_code('entry_list')),'CWP-SEM-2018-DS-002',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-SEM-2018-DS-002' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-AYT-2022-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-OYT-2021-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-F4-2020-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F4-2020-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-MET0-2023-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-HB-2018-DS-001',NULL,'finalized',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-HB-2018-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',25,2018,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-F1-2018-WS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F1-2018-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-F2-2019-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F2-2019-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'CWP-F3-2019-WS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F3-2019-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1)),
('VOIDED-368','CWP-SEM-2018-DS-001',NULL,'draft',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-SEM-2018-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,'VOIDED','entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='COWPEA' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Cowpea Breeding Program' LIMIT 1));



--rollback DELETE FROM experiment.entry_list
--rollback WHERE entry_list_name IN (
--rollback     'CWP-SEM-2018-DS-002',
--rollback     'CWP-AYT-2022-DS-001',
--rollback     'CWP-OYT-2021-DS-001',
--rollback     'CWP-F4-2020-DS-001',
--rollback     'CWP-MET0-2023-DS-001',
--rollback     'CWP-HB-2018-DS-001',
--rollback     'CWP-F1-2018-WS-001',
--rollback     'CWP-F2-2019-DS-001',
--rollback     'CWP-F3-2019-WS-001',
--rollback     'CWP-SEM-2018-DS-001'
--rollback );