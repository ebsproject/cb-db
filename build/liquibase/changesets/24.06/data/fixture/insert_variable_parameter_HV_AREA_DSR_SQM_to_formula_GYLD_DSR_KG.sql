--liquibase formatted sql

--changeset postgres:insert_variable_parameter_HV_AREA_DSR_SQM_to_formula_GYLD_DSR_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1385 Insert variable HV_AREA_DSR_SQM as a parameter to Formula GYLD_DSR_KG



--insert missing formula_param for formula GYLD_DSR_KG
INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id, order_number)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id,
    2 AS order_number
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('HV_AREA_DSR_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'GYLD_DSR_KG';


--update existing param's order_number
UPDATE master.formula_parameter
SET
order_number = 1
WHERE
param_variable_id in (select id from master.variable v where abbrev ilike '%ADJAYLD_G_CONT%') 
AND 
formula_id in (select id from master.formula f where formula = 'GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10');


-- revert changes
--rollback DELETE FROM
--rollback     master.formula_parameter
--rollback WHERE
--rollback     param_variable_id in (select id from master.variable v where abbrev ilike '%HV_AREA_DSR_SQM%') 
--rollback     AND 
--rollback     formula_id in (select id from master.formula f where formula = 'GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10')
--rollback ;
--rollback 
--rollback UPDATE master.formula_parameter
--rollback SET
--rollback order_number = NULL
--rollback WHERE
--rollback     param_variable_id in (select id from master.variable v where abbrev ilike '%ADJAYLD_G_CONT%') 
--rollback     AND 
--rollback     formula_id in (select id from master.formula f where formula = 'GYLD_DSR_KG=(ADJAYLD_G_CONT/HV_AREA_DSR_SQM)*10')
--rollback ;
