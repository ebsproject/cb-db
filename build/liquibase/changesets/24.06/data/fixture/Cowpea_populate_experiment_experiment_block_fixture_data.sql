--liquibase formatted sql

--changeset postgres:Cowpea_populate_experiment_experiment_block_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to experiment.experiment_block



INSERT INTO experiment.experiment_block
(experiment_block_code,experiment_block_name,experiment_id,parent_experiment_block_id,order_number,block_type,no_of_blocks,no_of_ranges,no_of_cols,no_of_reps,plot_numbering_order,starting_corner,creator_id,is_void,notes)
 VALUES 
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-HB-2018-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-299-299-35','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('VOIDED-332-332-68','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-F1-2018-WS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-365-365-101','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-F2-2019-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-366-366-103','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-F3-2019-WS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-367-367-105','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-F4-2020-DS-001'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-369-369-107','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-SEM-2018-DS-002' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
((SELECT experiment_code||'-EO-1' FROM experiment.experiment WHERE experiment_name = 'CWP-SEM-2018-DS-002'),'Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-SEM-2018-DS-002' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]');



--rollback DELETE FROM experiment.experiment_block
--rollback WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN (
--rollback      'CWP-HB-2018-DS-001',
--rollback      'CWP-F1-2018-WS-001',
--rollback      'CWP-F2-2019-DS-001',
--rollback      'CWP-F3-2019-WS-001',
--rollback      'CWP-F4-2020-DS-001',
--rollback      'CWP-SEM-2018-DS-002'
--rollback      )
--rollback );