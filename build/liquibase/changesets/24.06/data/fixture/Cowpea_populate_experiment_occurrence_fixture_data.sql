--liquibase formatted sql

--changeset postgres:Cowpea_populate_experiment_occurrence_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to experiment.occurrence



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,is_void,notes,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
 VALUES 
((experiment.generate_code('occurrence')),'CWP-SEM-2018-DS-002-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-SEM-2018-DS-002' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,20,20),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' ),NULL,2,NULL,286,572),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' ),NULL,3,NULL,286,572),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,286,572),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' ),NULL,4,NULL,286,572),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-005','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' ),NULL,5,NULL,286,572),
((experiment.generate_code('occurrence')),'CWP-AYT-2022-DS-001-006','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-AYT-2022-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' ),NULL,6,NULL,286,572),
('VOIDED-3611','VOIDED-3611','draft',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),True,'VOIDED-3611',NULL,NULL,NULL,-3611,NULL,0,0),
((experiment.generate_code('occurrence')),'CWP-OYT-2021-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' ),NULL,2,NULL,1406,1750),
('VOIDED-3610','VOIDED-3610','draft',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),True,'VOIDED-3610',NULL,NULL,NULL,-3610,NULL,0,0),
((experiment.generate_code('occurrence')),'CWP-OYT-2021-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,1406,1750),
((experiment.generate_code('occurrence')),'CWP-OYT-2021-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-OYT-2021-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' ),NULL,3,NULL,1406,1750),
((experiment.generate_code('occurrence')),'CWP-F4-2020-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F4-2020-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,375,375),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-002','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' ),NULL,2,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-003','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' ),NULL,3,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-006','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' ),NULL,6,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-004','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' ),NULL,4,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-MET0-2023-DS-001-005','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-MET0-2023-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' ),NULL,5,NULL,56,112),
((experiment.generate_code('occurrence')),'CWP-HB-2018-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-HB-2018-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,10,10),
((experiment.generate_code('occurrence')),'CWP-F1-2018-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F1-2018-WS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'CWP-F2-2019-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F2-2019-DS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'CWP-F3-2019-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='CWP-F3-2019-WS-001' ),NULL,(SELECT id FROM tenant.person WHERE username='admin' ),False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' ),NULL,1,NULL,125,125);



--rollback DELETE FROM experiment.occurrence
--rollback WHERE occurrence_name IN (
--rollback 'CWP-SEM-2018-DS-002-001',
--rollback 'CWP-AYT-2022-DS-001-002',
--rollback 'CWP-AYT-2022-DS-001-003',
--rollback 'CWP-AYT-2022-DS-001-001',
--rollback 'CWP-AYT-2022-DS-001-004',
--rollback 'CWP-AYT-2022-DS-001-005',
--rollback 'CWP-AYT-2022-DS-001-006',
--rollback 'VOIDED-3611',
--rollback 'CWP-OYT-2021-DS-001-002',
--rollback 'VOIDED-3610',
--rollback 'CWP-OYT-2021-DS-001-001',
--rollback 'CWP-OYT-2021-DS-001-003',
--rollback 'CWP-F4-2020-DS-001-001',
--rollback 'CWP-MET0-2023-DS-001-001',
--rollback 'CWP-MET0-2023-DS-001-002',
--rollback 'CWP-MET0-2023-DS-001-003',
--rollback 'CWP-MET0-2023-DS-001-006',
--rollback 'CWP-MET0-2023-DS-001-004',
--rollback 'CWP-MET0-2023-DS-001-005',
--rollback 'CWP-HB-2018-DS-001-001',
--rollback 'CWP-F1-2018-WS-001-001',
--rollback 'CWP-F2-2019-DS-001-001',
--rollback 'CWP-F3-2019-WS-001-001'
--rollback );