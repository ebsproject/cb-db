--liquibase formatted sql

--changeset postgres:Cowpea_populate_experiment_location_occurrence_group_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to experiment.location_occurrence_group



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,creator_id,is_void,notes)
 VALUES 
((SELECT id FROM experiment.location WHERE location_code ='PH_AN_RM-2023-DS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_NE_SM2-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_AN_RM-2021-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-OYT-2021-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2018-DS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-HB-2018-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_NE_SM2-2021-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-OYT-2021-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_NC_MD-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-006' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_BO_UA-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-004' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2019-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-F2-2019-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_IB_SO-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-005' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2018-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-F1-2018-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2023-DS-008' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2018-DS-003' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-SEM-2018-DS-002-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_BO_UA-2023-DS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-004' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2019-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-F3-2019-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_NE_SM2-2023-DS-003' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2020-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-F4-2020-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2021-DS-008' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-OYT-2021-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='IRRIHQ-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_NC_MD-2023-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-006' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_IB_SO-2023-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-MET0-2023-DS-001-005' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_code ='PH_AN_RM-2022-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='CWP-AYT-2022-DS-001-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL);



--rollback DELETE FROM experiment.location_occurrence_group
--rollback WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN 
--rollback     (
--rollback         'CWP-AYT-2022-DS-001-001',
--rollback         'CWP-AYT-2022-DS-001-002',
--rollback         'CWP-AYT-2022-DS-001-003',
--rollback         'CWP-AYT-2022-DS-001-004',
--rollback         'CWP-AYT-2022-DS-001-005',
--rollback         'CWP-AYT-2022-DS-001-006',
--rollback         'CWP-F1-2018-WS-001-001',
--rollback         'CWP-F2-2019-DS-001-001',
--rollback         'CWP-F3-2019-WS-001-001',
--rollback         'CWP-F4-2020-DS-001-001',
--rollback         'CWP-HB-2018-DS-001-001',
--rollback         'CWP-MET0-2023-DS-001-001',
--rollback         'CWP-MET0-2023-DS-001-002',
--rollback         'CWP-MET0-2023-DS-001-003',
--rollback         'CWP-MET0-2023-DS-001-004',
--rollback         'CWP-MET0-2023-DS-001-005',
--rollback         'CWP-MET0-2023-DS-001-006',
--rollback         'CWP-OYT-2021-DS-001-001',
--rollback         'CWP-OYT-2021-DS-001-002',
--rollback         'CWP-OYT-2021-DS-001-003',
--rollback         'CWP-SEM-2018-DS-002-001'
--rollback     )
--rollback );