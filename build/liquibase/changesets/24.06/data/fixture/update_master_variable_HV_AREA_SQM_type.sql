--liquibase formatted sql

--changeset postgres:update_master_variable_HV_AREA_SQM_type context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2123 DC-DB: Update the fixture database HV_AREA_SQM variable type from metadata to observation



UPDATE
    master.variable
SET
    type = 'observation'
WHERE
    abbrev = 'HV_AREA_SQM'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     type = 'metadata'
--rollback WHERE
--rollback     abbrev = 'HV_AREA_SQM'
--rollback ;