--liquibase formatted sql

--changeset postgres:update_tenant_program_code_bw context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1413 Update the fixture database BW program code from BW to BW-CIMMYT

-- update program code of BW
UPDATE
    tenant.program
SET
    program_code = 'BW-CIMMYT'
WHERE
    program_code = 'BW'
;


-- revert changes
--rollback UPDATE
--rollback     tenant.program
--rollback SET
--rollback     program_code = 'BW'
--rollback WHERE
--rollback     program_code = 'BW-CIMMYT'
--rollback ;