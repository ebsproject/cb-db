--liquibase formatted sql

--changeset postgres:Cowpea_populate_place_geospatial_object_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to place.geospatial_object



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,parent_geospatial_object_id,root_geospatial_object_id,creator_id,is_void,notes,coordinates)
 VALUES 
('IRRIHQ-2018-DS-001','IRRIHQ-2018-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2018-DS-002','IRRIHQ-2018-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2018-WS-001','IRRIHQ-2018-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2019-DS-001','IRRIHQ-2019-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2019-WS-001','IRRIHQ-2019-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2020-DS-001','IRRIHQ-2020-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2018-DS-003','IRRIHQ-2018-DS-003','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2021-DS-008','IRRIHQ-2021-DS-008','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_NE_SM2-2021-DS-001','PH_NE_SM2-2021-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_AN_RM-2021-DS-001','PH_AN_RM-2021-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2022-DS-001','IRRIHQ-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_NE_SM2-2022-DS-001','PH_NE_SM2-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_AN_RM-2022-DS-001','PH_AN_RM-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_BO_UA-2022-DS-001','PH_BO_UA-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_IB_SO-2022-DS-001','PH_IB_SO-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_NC_MD-2022-DS-001','PH_NC_MD-2022-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('IRRIHQ-2023-DS-008','IRRIHQ-2023-DS-008','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_NE_SM2-2023-DS-003','PH_NE_SM2-2023-DS-003','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_AN_RM-2023-DS-002','PH_AN_RM-2023-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_BO_UA-2023-DS-002','PH_BO_UA-2023-DS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_IB_SO-2023-DS-001','PH_IB_SO-2023-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('PH_NC_MD-2023-DS-001','PH_NC_MD-2023-DS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL);



--rollback DELETE FROM place.geospatial_object
--rollback WHERE geospatial_object_code IN (
--rollback 'IRRIHQ-2018-DS-001',
--rollback 'IRRIHQ-2018-DS-002',
--rollback 'IRRIHQ-2018-WS-001',
--rollback 'IRRIHQ-2019-DS-001',
--rollback 'IRRIHQ-2019-WS-001',
--rollback 'IRRIHQ-2020-DS-001',
--rollback 'IRRIHQ-2018-DS-003',
--rollback 'IRRIHQ-2021-DS-008',
--rollback 'PH_NE_SM2-2021-DS-001',
--rollback 'PH_AN_RM-2021-DS-001',
--rollback 'IRRIHQ-2022-DS-001',
--rollback 'PH_NE_SM2-2022-DS-001',
--rollback 'PH_AN_RM-2022-DS-001',
--rollback 'PH_BO_UA-2022-DS-001',
--rollback 'PH_IB_SO-2022-DS-001',
--rollback 'PH_NC_MD-2022-DS-001',
--rollback 'IRRIHQ-2023-DS-008',
--rollback 'PH_NE_SM2-2023-DS-003',
--rollback 'PH_AN_RM-2023-DS-002',
--rollback 'PH_BO_UA-2023-DS-002',
--rollback 'PH_IB_SO-2023-DS-001',
--rollback 'PH_NC_MD-2023-DS-001'
--rollback );