--liquibase formatted sql

--changeset postgres:Cowpea_populate_experiment_entry_list_data_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-1522 Populate Cowpea fixture to experiment.entry_list_data


INSERT INTO platform.list 
(abbrev,name,display_name,type,entity_id,creator_id,notes,is_void,record_uuid,list_usage,status,is_active)
VALUES
((SELECT 'TRAIT_PROTOCOL_'||entry_list_code FROM experiment.entry_list WHERE entry_list_name ='CWP-HB-2018-DS-001'),(SELECT entry_list_name||' Trait Protocol ('||entry_list_code||')' FROM experiment.entry_list WHERE entry_list_name ='CWP-HB-2018-DS-001'),(SELECT entry_list_name||'Trait Protocol ('||entry_list_code||')' FROM experiment.entry_list WHERE entry_list_name ='CWP-HB-2018-DS-001'),'trait',(SELECT id FROM dictionary.entity WHERE abbrev = 'TRAIT'), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Created via changesets BDS-1522',False,'697bb5ca-8412-44ce-8f86-2d2750fea265','working list','created',True);


INSERT INTO experiment.entry_list_data
(entry_list_id,variable_id,data_value,data_qc_code,protocol_id,remarks,creator_id,notes,is_void)
 VALUES 
((SELECT id FROM experiment.entry_list WHERE entry_list_name ='CWP-HB-2018-DS-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),(SELECT id from platform.list WHERE abbrev IN (SELECT 'TRAIT_PROTOCOL_'||(SELECT entry_list_code FROM experiment.entry_list WHERE entry_list_name = 'CWP-HB-2018-DS-001'))),'G',NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,False);



--rollback DELETE FROM platform.list
--rollback WHERE name ILIKE 'CWP-HB-2018-DS-001 Trait Protocol (ENTLIST%';
--rollback DELETE FROM experiment.entry_list_data
--rollback WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name ='CWP-HB-2018-DS-001');