--liquibase formatted sql

--changeset postgres:update_harvest_date_and_date_crossed_usage_to_plot context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1591 Update HARVEST_DATE and DATE_CROSSED usage to plot



UPDATE
    master.variable 
SET 
    usage = 'plot'
WHERE
    abbrev 
IN
    (
        'DATE_CROSSED',
        'HARVEST_DATE'
    );



--rollback UPDATE
--rollback     master.variable 
--rollback SET 
--rollback     usage = 'study'
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'DATE_CROSSED',
--rollback         'HARVEST_DATE'
--rollback     );