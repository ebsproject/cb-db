--liquibase formatted sql

--changeset postgres:add_config_intentional_crossing_nursery_harvest_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL to platform.config



INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL', 'Intentional Crossing Nursery Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Intentional Crossing Nursery data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');



--rollback DELETE FROM platform.config WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_VAL';