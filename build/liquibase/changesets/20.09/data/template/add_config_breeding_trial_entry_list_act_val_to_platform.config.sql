--liquibase formatted sql

--changeset postgres:add_config_breeding_trial_entry_list_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config BREEDING_TRIAL_ENTRY_LIST_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL',
        'Breeding Trial Experiment Entry List','
        {
            "Name": "Required and default entry level metadata variables for Breeding Trial data process",
            "Values": [{
                    "default": "entry",
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_TYPE",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                }
            ]
        }
        '::json,
        1, 
        'experiment_creation',
        1,
        'added by j.antonio');

        

--rollback DELETE FROM platform.config WHERE abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL';