--liquibase formatted sql

--changeset postgres:update_config_hm_seed_name_pattern_wheat_default_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config HM_SEED_NAME_PATTERN_WHEAT_DEFAULT p2



UPDATE 
    platform.config
SET
    config_value = 
    $$				
    {
        "bulk": [{
                "type": "field",
                "label": "Source Nursery Site Code",
                "order_number": 0,
                "plotInfoField": "nurserySiteCode"
            },
            {
                "type": "field",
                "label": "Experiment Year - YY",
                "order_number": 1,
                "plotInfoField": "experimentYearYY"
            },
            {
                "type": "field",
                "label": "Source Season Code",
                "order_number": 2,
                "plotInfoField": "seasonCode"
            },
            {
                "type": "delimeter",
                "value": "/",
                "order_number": 3
            },
            {
                "type": "field",
                "label": "Experiment Name",
                "order_number": 4,
                "plotInfoField": "experimentName"
            },
            {
                "type": "delimeter",
                "value": "/",
                "order_number": 5
            },
            {
                "type": "field",
                "label": "Source Entry Number",
                "order_number": 6,
                "plotInfoField": "entryNumber"
            }
        ],
        "default": [{
                "type": "field",
                "label": "Source Nursery Site Code",
                "order_number": 0,
                "plotInfoField": "nurserySiteCode"
            },
            {
                "type": "field",
                "label": "Experiment Year - YY",
                "order_number": 1,
                "plotInfoField": "experimentYearYY"
            },
            {
                "type": "field",
                "label": "Source Season Code",
                "order_number": 2,
                "plotInfoField": "seasonCode"
            },
            {
                "type": "delimeter",
                "value": "/",
                "order_number": 3
            },
            {
                "type": "field",
                "label": "Occurrence Code",
                "order_number": 4,
                "plotInfoField": "occurrenceCode"
            },
            {
                "type": "delimeter",
                "value": "/",
                "order_number": 5
            },
            {
                "type": "field",
                "label": "Source Entry Number",
                "order_number": 6,
                "plotInfoField": "entryNumber"
            }
        ]
    }
    $$
WHERE 
    abbrev = 'HM_SEED_NAME_PATTERN_WHEAT_DEFAULT';



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "bulk": [
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Nursery Site Code",
--rollback                     "order_number": 0,
--rollback                     "plotInfoField": "nurserySiteCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Experiment Year - YY",
--rollback                     "order_number": 1,
--rollback                     "plotInfoField": "experimentYearYY"
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Season Code",
--rollback                     "order_number": 2,
--rollback                     "plotInfoField": "seasonCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "/",
--rollback                     "order_number": 3
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Occurrence Code",
--rollback                     "order_number": 4,
--rollback                     "plotInfoField": "occurrenceCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "/",
--rollback                     "order_number": 5
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Entry Number",
--rollback                     "order_number": 6,
--rollback                     "plotInfoField": "entryNumber"
--rollback                 }
--rollback             ],
--rollback             "default": [
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Nursery Site Code",
--rollback                     "order_number": 0,
--rollback                     "plotInfoField": "nurserySiteCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Experiment Year - YY",
--rollback                     "order_number": 1,
--rollback                     "plotInfoField": "experimentYearYY"
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Season Code",
--rollback                     "order_number": 2,
--rollback                     "plotInfoField": "seasonCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "/",
--rollback                     "order_number": 3
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Occurrence Code",
--rollback                     "order_number": 4,
--rollback                     "plotInfoField": "occurrenceCode"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "/",
--rollback                     "order_number": 5
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "label": "Source Entry Number",
--rollback                     "order_number": 6,
--rollback                     "plotInfoField": "entryNumber"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback 
--rollback WHERE 
--rollback     abbrev = 'HM_SEED_NAME_PATTERN_WHEAT_DEFAULT';