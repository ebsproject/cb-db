--liquibase formatted sql

--changeset postgres:add_variable_person_status context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1318 Add variable PERSON_STATUS



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'PERSON_STATUS' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'PERSON_STATUS' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('PERSON_STATUS','Person Status','Person Status') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'PERSON_STATUS' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'PERSON_STATUS' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Person Status','PERSON_STATUS',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'PERSON_STATUS' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'PERSON_STATUS' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('PERSON_STATUS','categorical','Person Status',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'active',1,'active','active','PERSON_STATUS_ACTIVE');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'inactive',2,'inactive','inactive','PERSON_STATUS_INACTIVE');

	UPDATE master.scale SET min_value='active', max_value='inactive' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Person Status','Person Status','character varying','Person Status','Person Status','True','PERSON_STATUS','study','identification',NULL) 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'PERSON_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'PERSON_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('PERSON_INFO','Person Info') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'PERSON_STATUS');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'PERSON_STATUS');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'PERSON_STATUS');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'PERSON_STATUS');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'PERSON_STATUS');

--rollback DELETE FROM master.variable WHERE abbrev = 'PERSON_STATUS';
