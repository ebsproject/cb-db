--liquibase formatted sql

--changeset postgres:update_harvest_date_and_date_crossed_usage_to_study context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1591 Update HARVEST_DATE and DATE_CROSSED usage to study



UPDATE
    master.variable 
SET 
    usage = 'study'
WHERE
    abbrev 
IN
    (
        'DATE_CROSSED',
        'HARVEST_DATE'
    );
 


--rollback UPDATE
--rollback     master.variable 
--rollback SET 
--rollback     usage = 'plot'
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'DATE_CROSSED',
--rollback         'HARVEST_DATE'
--rollback     );