--liquibase formatted sql

--changeset postgres:update_config_cross_parent_nursery_phase_ii_entry_list_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = '{
   "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase II data process",
   "Values": [
        {
            "disabled": false,
            "required": "required",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "display_name": "Parent Role",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": true,
            "variable_abbrev": "ENTRY_CLASS",
            "is_shown" : false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
    ]
 }'
WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level metadata variables for Cross Parent Nursery Phase I data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": "RICE",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "cropCode",
--rollback                     "target_column": "cropDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "CROP",
--rollback                     "api_resource_sort": "sort=cropCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "crops"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "programCode",
--rollback                     "target_column": "programDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROGRAM",
--rollback                     "api_resource_sort": "sort=programCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "programs"
--rollback                 },
--rollback                 {
--rollback                     "default": "EXPT-XXXX",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_CODE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": "HB",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "stageCode",
--rollback                     "target_column": "stageDbId",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "HB",
--rollback                         "F1"
--rollback                     ],
--rollback                     "variable_abbrev": "STAGE",
--rollback                     "api_resource_sort": "sort=stageCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "POST",
--rollback                     "api_resource_endpoint": "stages-search"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_YEAR",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "seasonCode",
--rollback                     "target_column": "seasonDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "SEASON",
--rollback                     "api_resource_sort": "sort=seasonCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "seasons"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "personName",
--rollback                     "target_column": "stewardDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_STEWARD",
--rollback                     "api_resource_sort": "sort=personName",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "persons",
--rollback                     "secondary_target_column": "personDbId"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "pipelineCode",
--rollback                     "target_column": "pipelineDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PIPELINE",
--rollback                     "api_resource_sort": "sort=pipelineCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "pipelines"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_OBJECTIVE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PLANTING_SEASON",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "projectCode",
--rollback                     "target_column": "projectDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROJECT",
--rollback                     "api_resource_sort": "sort=projectCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "projects"
--rollback                 },
--rollback                 {
--rollback                     "default": "Breeding Crosses",
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "value",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "Breeding Crosses"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_TYPE"
--rollback                 },
--rollback                 {
--rollback                     "default": "Crossing Block",
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "Crossing Block"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_VAL';