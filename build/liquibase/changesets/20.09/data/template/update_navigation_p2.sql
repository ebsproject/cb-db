--liquibase formatted sql

--changeset postgres:update_navigation_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update navigation p2



-- update navigation items
UPDATE 
    platform.space 
SET 
    menu_data = '
    {
        "left_menu_items": [{
            "name": "experiment-creation",
            "label": "Experiment creation",
            "appAbbrev": "EXPERIMENT_CREATION"
            }, {
            "name": "experiment-manager",
            "label": "Experiment manager",
            "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"	
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            },{
                "name": "search",
                "items": [
                {
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },{
                    "name": "search-seeds",
                    "label": "Seeds",
                    "appAbbrev": "FIND_SEEDS"
                },{
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }],
                "label": "Search"
            }
        ]
    }' 
WHERE abbrev = 'DEFAULT';

UPDATE 
    platform.space 
SET 
    menu_data = '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "search",
                "items": [{
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                }, {
                    "name": "search-seeds",
                    "label": "Seeds",
                    "appAbbrev": "FIND_SEEDS"
                }, {
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }],
                "label": "Search"
            }],
            "main_menu_items": [{
                "icon": "folder_special",
                "name": "data-management",
                "items": [{
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                }],
                "label": "Data management"
            }, {
                "icon": "settings",
                "name": "administration",
                "items": [{
                    "name": "administration_users",
                    "label": "Persons",
                    "appAbbrev": "PERSONS"
                }],
                "label": "Administration"
            }]
    }' 
WHERE abbrev = 'ADMIN';



--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = '
--rollback     {
--rollback         "left_menu_items": [{
--rollback             "name": "experiment-creation",
--rollback             "label": "Experiment creation",
--rollback             "appAbbrev": "EXPERIMENT_CREATION"
--rollback             }, {
--rollback             "name": "experiment-manager",
--rollback             "label": "Experiment manager",
--rollback             "appAbbrev": "OCCURRENCES"
--rollback             }, {
--rollback                 "name": "data-collection-qc-quality-control",
--rollback                 "label": "Data Collection",
--rollback                 "appAbbrev": "QUALITY_CONTROL"	
--rollback             }, {
--rollback                 "name": "seeds-harvest-manager",
--rollback                 "label": "Harvest manager",
--rollback                 "appAbbrev": "HARVEST_MANAGER"
--rollback             },{
--rollback                 "name": "search",
--rollback                 "items": [
--rollback                 {
--rollback                     "name": "searchs-germplasm",
--rollback                     "label": "Germplasm",
--rollback                     "appAbbrev": "GERMPLASM_CATALOG"
--rollback                 },{
--rollback                     "name": "search-seeds",
--rollback                     "label": "Seeds",
--rollback                     "appAbbrev": "FIND_SEEDS"
--rollback                 },{
--rollback                     "name": "search-traits",
--rollback                     "label": "Traits",
--rollback                     "appAbbrev": "TRAITS"
--rollback                 }],
--rollback                 "label": "Search"
--rollback             }
--rollback         ]
--rollback     }' 
--rollback WHERE abbrev = 'DEFAULT';
--rollback 
--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = '
--rollback         {
--rollback             "left_menu_items": [{
--rollback                 "name": "experiment-creation",
--rollback                 "label": "Experiment creation",
--rollback                 "appAbbrev": "EXPERIMENT_CREATION"
--rollback             }, {
--rollback                 "name": "experiment-manager",
--rollback                 "label": "Experiment manager",
--rollback                 "appAbbrev": "OCCURRENCES"
--rollback             }, {
--rollback                 "name": "data-collection-qc-quality-control",
--rollback                 "label": "Data Collection",
--rollback                 "appAbbrev": "QUALITY_CONTROL"
--rollback             }, {
--rollback                 "name": "seeds-harvest-manager",
--rollback                 "label": "Harvest manager",
--rollback                 "appAbbrev": "HARVEST_MANAGER"
--rollback             }, {
--rollback                 "name": "search",
--rollback                 "items": [{
--rollback                     "name": "searchs-germplasm",
--rollback                     "label": "Germplasm",
--rollback                     "appAbbrev": "GERMPLASM_CATALOG"
--rollback                 }, {
--rollback                     "name": "search-seeds",
--rollback                     "label": "Seeds",
--rollback                     "appAbbrev": "FIND_SEEDS"
--rollback                 }, {
--rollback                     "name": "search-traits",
--rollback                     "label": "Traits",
--rollback                     "appAbbrev": "TRAITS"
--rollback                 }],
--rollback                 "label": "Search"
--rollback             }],
--rollback             "main_menu_items": [{
--rollback                 "icon": "folder_special",
--rollback                 "name": "data-management",
--rollback                 "items": [{
--rollback                     "name": "data-management_seasons",
--rollback                     "label": "Seasons",
--rollback                     "appAbbrev": "MANAGE_SEASONS"
--rollback                 }],
--rollback                 "label": "Data management"
--rollback             }, {
--rollback                 "icon": "settings",
--rollback                 "name": "administration",
--rollback                 "items": [{
--rollback                     "name": "administration_users",
--rollback                     "label": "Persons",
--rollback                     "appAbbrev": "PERSONS"
--rollback                 }],
--rollback                 "label": "Administration"
--rollback             }]
--rollback     }' 
--rollback WHERE abbrev = 'ADMIN';