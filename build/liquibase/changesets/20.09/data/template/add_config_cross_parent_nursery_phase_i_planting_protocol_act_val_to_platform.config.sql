--liquibase formatted sql

--changeset postgres:add_config_cross_parent_nursery_phase_i_planting_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL', 
        'Cross Parent Nursery Phase I Protocol variables', 
        '{
            "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase I data process",
            "Values": [{
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "ESTABLISHMENT"
                },{
                    "default": false,
                    "variable_abbrev": "PLANTING_TYPE",
                    "disabled": false
                },{
                    "variable_abbrev": "PLOT_TYPE",
                    "required": "required",
                    "disabled": false
                }
            ]
        }'::json, 
        1,
        'experiment_creation',
        1,
        'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL';