--liquibase formatted sql

--changeset postgres:add_breeding_trial_mod_to_platform.item_module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add breeding trial mod to platform.item_module



INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
	id AS item_id,
	CASE
		WHEN abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT_MOD')
		WHEN abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_MOD')
		WHEN abbrev = 'BREEDING_TRIAL_DESIGN_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_DESIGN_ACT_MOD')
		WHEN abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT_MOD')
        WHEN abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT_MOD')
		WHEN abbrev = 'BREEDING_TRIAL_PLACE_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_PLACE_ACT_MOD')
		WHEN abbrev = 'BREEDING_TRIAL_REVIEW_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_REVIEW_ACT_MOD')
		ELSE (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_REVIEW_ACT_MOD') END AS module_id,
	1,
	'added by l.gallardo ' || now()
FROM
	master.item
WHERE
	abbrev 
IN 
    (
        'BREEDING_TRIAL_BASIC_INFO_ACT',
        'BREEDING_TRIAL_ENTRY_LIST_ACT',
        'BREEDING_TRIAL_DESIGN_ACT',
        'BREEDING_TRIAL_EXPT_GROUP_ACT',
        'BREEDING_TRIAL_PROTOCOL_ACT',
        'BREEDING_TRIAL_PLACE_ACT',
        'BREEDING_TRIAL_REVIEW_ACT'
    );

INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
	id AS item_id,
CASE
  WHEN abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD')
  WHEN abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD') END AS module_id,
	1,
	'added by j.antonio ' || now()
FROM
	master.item
WHERE
    abbrev 
IN 
    (
        'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT',
        'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'
    );



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'BREEDING_TRIAL_BASIC_INFO_ACT',
--rollback        'BREEDING_TRIAL_ENTRY_LIST_ACT',
--rollback        'BREEDING_TRIAL_DESIGN_ACT',
--rollback        'BREEDING_TRIAL_EXPT_GROUP_ACT',
--rollback        'BREEDING_TRIAL_PROTOCOL_ACT',
--rollback        'BREEDING_TRIAL_PLACE_ACT',
--rollback        'BREEDING_TRIAL_REVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'BREEDING_TRIAL_BASIC_INFO_ACT_MOD',
--rollback         'BREEDING_TRIAL_ENTRY_LIST_ACT_MOD',
--rollback         'BREEDING_TRIAL_DESIGN_ACT_MOD',
--rollback         'BREEDING_TRIAL_EXPT_GROUP_ACT_MOD', 
--rollback         'BREEDING_TRIAL_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_PLACE_ACT_MOD',
--rollback         'BREEDING_TRIAL_REVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT',
--rollback        'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   );