--liquibase formatted sql

--changeset postgres:remove_newly_added_scale_values_for_hv_meth_disc context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1240 Remove newly added scale values for HV_METH_DISC



DELETE FROM 
    master.scale_value 
WHERE
    abbrev 
IN
    (
        'HV_METH_DISC_MODIFIED_BULK',
        'HV_METH_DISC_SELECTED_BULK',
        'HV_METH_DISC_INDIVIDUAL_PLANT',
        'HV_METH_DISC_INDIVIDUAL_SPIKE_EAR_PANICLE',
        'HV_METH_DISC_HEAD_ROWS',
        'HV_METH_DISC_HEAD_ROW_PURIFICATION',
        'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
    );



--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback 		(
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Modified bulk',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Modified bulk',
--rollback             'Modified bulk',
--rollback             'HV_METH_DISC_MODIFIED_BULK'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Selected bulk',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Selected bulk',
--rollback             'Selected bulk',
--rollback             'HV_METH_DISC_SELECTED_BULK'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Individual plant',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Individual plant',
--rollback             'Individual plant',
--rollback             'HV_METH_DISC_INDIVIDUAL_PLANT'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Individual spike/ear/panicle',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Individual spike/ear/panicle',
--rollback             'Individual spike/ear/panicle',
--rollback             'HV_METH_DISC_INDIVIDUAL_SPIKE_EAR_PANICLE'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Head-rows',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Head-rows',
--rollback             'Head-rows',
--rollback             'HV_METH_DISC_HEAD_ROWS'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Head-row purification',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Head-row purification',
--rollback             'Head-row purification',
--rollback             'HV_METH_DISC_HEAD_ROW_PURIFICATION'
--rollback         );
--rollback 
--rollback INSERT INTO 
--rollback 		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
--rollback 	VALUES 
--rollback         (
--rollback             (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
--rollback             'Single seed numbering',
--rollback             (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
--rollback             'Single seed numbering',
--rollback             'Single seed numbering',
--rollback             'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
--rollback         );