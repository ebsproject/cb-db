--liquibase formatted sql

--changeset postgres:update_name_description_display_name_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update display_name, name, description



UPDATE master.item SET (name, description, display_name) = ('Planting Arrangement', 'Planting Arrangement', 'Planting Arrangement') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT';
UPDATE master.item SET (name, description, display_name) = ('Planting Arrangement', 'Planting Arrangement', 'Planting Arrangement') WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT';
UPDATE master.item SET (name, description, display_name) = ('Entry List', 'Entry List', 'Entry List') WHERE abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT';



--rollback UPDATE master.item SET (name, description, display_name) = ('Design', 'Design', 'Design') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT';
--rollback UPDATE master.item SET (name, description, display_name) = ('Design', 'Design', 'Design') WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT';
--rollback UPDATE master.item SET (name, description, display_name) = ('Specify Entry List', 'Specify Entry List', 'Entry List') WHERE abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT';