--liquibase formatted sql

--changeSET postgres:update_name_description_display_name_in_master.item_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update display_name, name, description p2



UPDATE master.item SET(name, display_name) = ('Cross Parent Nursery (Phase I)','Cross Parent Nursery (Phase I)') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS';

UPDATE master.item SET(name, display_name) = ('Cross Parent Nursery (Phase II)','Cross Parent Nursery (Phase II)') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS';



--rollback UPDATE master.item SET(name, display_name) = ('Cross Parent Nursery Phase I','Cross Parent Nursery Phase I') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS';

--rollback UPDATE master.item SET(name, display_name) = ('Cross Parent Nursery Phase II','Cross Parent Nursery Phase II') WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS';