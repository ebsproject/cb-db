--liquibase formatted sql

--changeset postgres:update_config_cross_parent_nursery_phase_ii_place_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = '{
        "Name": "Required and default place metadata variables for Cross Parent Nursery Phase II data process",
        "Values": [
            {
            "required": "required",
            "disabled": true,
            "target_column": "occurrenceName",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
            "required": "required",
            "disabled": true,
            "target_column": "siteDbId",
            "secondary_target_column":"geospatialObjectDbId",
            "target_value":"geospatialObjectName",
            "api_resource_method" : "POST",
            "api_resource_endpoint" : "geospatial-objects-search",
            "api_resource_filter" : {"geospatialObjectType": "site"},
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "SITE"
            },
            {
            "target_column": "fieldDbId",
            "disabled": true,
            "secondary_target_column":"geospatialObjectDbId",
            "target_value":"scaleName",
            "api_resource_method" : "POST",
            "api_resource_endpoint" : "geospatial-objects-search",
            "api_resource_filter" : {"geospatialObjectType": "field"},
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "FIELD"
            },
            {
            "disabled": true,
            "target_column": "description",
            "secondary_target_column": "",
            "target_value": "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "variable_type" : "identification",
            "variable_abbrev": "DESCRIPTION"
            },
            {
            "disabled": true,
            "allow_new_val": true,
            "target_column": "contactPerson",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "variable_type" : "metadata",
            "variable_abbrev": "CONTCT_PERSON_CONT"
            }
        ]
    }' 
WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default place metadata variables for Cross Parent Nursery Phase II data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "occurrenceName",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "OCCURRENCE_NAME",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "required": "required",
--rollback                     "target_value": "geospatialObjectName",
--rollback                     "target_column": "siteDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "SITE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": {
--rollback                         "geospatialObjectType": "site"
--rollback                     },
--rollback                     "api_resource_method": "POST",
--rollback                     "api_resource_endpoint": "geospatial-objects-search",
--rollback                     "secondary_target_column": "geospatialObjectDbId"
--rollback                 },
--rollback                 {
--rollback                     "target_value": "scaleName",
--rollback                     "target_column": "fieldDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "FIELD",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": {
--rollback                         "geospatialObjectType": "field"
--rollback                     },
--rollback                     "api_resource_method": "POST",
--rollback                     "api_resource_endpoint": "geospatial-objects-search",
--rollback                     "secondary_target_column": "geospatialObjectDbId"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "description",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "target_value": "personName",
--rollback                     "allow_new_val": true,
--rollback                     "target_column": "contactPerson",
--rollback                     "variable_type": "metadata",
--rollback                     "variable_abbrev": "CONTCT_PERSON_CONT",
--rollback                     "api_resource_sort": "sort=personName",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "persons",
--rollback                     "secondary_target_column": "personDbId"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_VAL';