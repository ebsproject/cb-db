--liquibase formatted sql

--changeset postgres:add_cross_parent_nursery_process_path_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add cross parent nursery process path to master.item



INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage) 
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS','Cross Parent Nursery Phase I',40,'Are procedures for first phenotyping – genotyping to select parents for crossing based on the phase of a breeding pipeline.','Cross Parent Nursery Phase I',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT','Specify Parent List',30,'Specify Parent List','Parent List',1,'active','fa fa-list-ol');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT','Design',30,'Design','Design',1,'active','fa fa-files-o');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');

INSERT INTO master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES('CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-files-o');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT','Planting Protocol',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT','Traits Protocol',20,'Traits Protocol','Traits',1,'active','fa fa-braille');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');



--rollback DELETE FROM 
--rollback     master.item 
--rollback WHERE  
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_DATA_PROCESS',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT'
--rollback     );