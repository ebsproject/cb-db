--liquibase formatted sql

--changeset postgres:update_config_breeding_trial_basic_info_act_val_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config BREEDING_TRIAL_BASIC_INFO_ACT_VAL p2



UPDATE 
    platform.config 
SET
    config_value = '{
    "Name": "Required experiment level metadata variables for Breeding Trial data process",
    "Values": [
        {
            "default": false,
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_TYPE"
        },
        {
            "default": "RICE",
            "disabled": true,
            "required": "required",
            "target_column": "cropDbId",
            "target_value" : "cropCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "crops",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=cropCode",
            "variable_type" : "identification",
            "variable_abbrev": "CROP"
        },
        {
            "disabled": true,
            "required": "required",
            "target_column": "programDbId",
            "target_value":"programCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "programs",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=programCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROGRAM"
        },
        {
            "default" : "EXPT-XXXX",
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_CODE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_NAME"
        },
        {
            "disabled": false,
            "required": "required",
            "allowed_values": [
                "IYT",
                "OYT",
                "PYT",
                "AYT",
                "MET0",
                "MET1",
                "MET2",
                "RYT"
            ],
            "target_column": "stageDbId",
            "target_value":"stageCode",
            "api_resource_method": "POST",
            "api_resource_endpoint": "stages-search",
            "api_resource_sort": "sort=stageCode",
            "api_resource_filter" : "",
            "variable_type" : "identification",
            "variable_abbrev": "STAGE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "seasonDbId",
            "target_value":"seasonCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "seasons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=seasonCode",
            "variable_type" : "identification",
            "variable_abbrev": "SEASON"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "stewardDbId",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_STEWARD"
        },
        {
            "disabled": false,
            "target_column": "pipelineDbId",
            "target_value":"pipelineCode",
            "api_resource_method" : "GET",
            "api_resource_endpoint" : "pipelines",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=pipelineCode",
            "variable_type" : "identification",
            "variable_abbrev": "PIPELINE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_OBJECTIVE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "PLANTING_SEASON"
        },
        {
            "disabled": false,
            "target_column": "projectDbId",
            "target_value": "projectCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "projects",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=projectCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROJECT"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"value",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "allowed_values": [
                "Yield Trial",
                "Disease Trial",
                "Physiology Trial",
                "Other Trial"
            ],
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_TYPE"
        },
        {
            "disabled": false,
            "allowed_values": [
                "First Year",
                "Second Year",
                "Re Test",
                "Germplasm Viability"
            ],
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type": "identification",
            "variable_abbrev": "DESCRIPTION"
        },
        {
            "default": false,
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "metadata",
            "variable_abbrev" : "HAS_EXPERIMENT_GROUP"
        }
    ]
}'
WHERE 
    abbrev= 'BREEDING_TRIAL_BASIC_INFO_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level metadata variables for Breeding Trial data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": "RICE",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "cropCode",
--rollback                     "target_column": "cropDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "CROP",
--rollback                     "api_resource_sort": "sort=cropCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "crops"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "programCode",
--rollback                     "target_column": "programDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROGRAM",
--rollback                     "api_resource_sort": "sort=programCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "programs"
--rollback                 },
--rollback                 {
--rollback                     "default": "EXPT-XXXX",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_CODE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "stageCode",
--rollback                     "target_column": "stageDbId",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "IYT",
--rollback                         "OYT",
--rollback                         "PYT",
--rollback                         "AYT",
--rollback                         "MET0",
--rollback                         "MET1",
--rollback                         "MET2",
--rollback                         "RYT"
--rollback                     ],
--rollback                     "variable_abbrev": "STAGE",
--rollback                     "api_resource_sort": "sort=stageCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "POST",
--rollback                     "api_resource_endpoint": "stages-search"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_YEAR",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "seasonCode",
--rollback                     "target_column": "seasonDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "SEASON",
--rollback                     "api_resource_sort": "sort=seasonCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "seasons"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "personName",
--rollback                     "target_column": "stewardDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_STEWARD",
--rollback                     "api_resource_sort": "sort=personName",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "persons",
--rollback                     "secondary_target_column": "personDbId"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "pipelineCode",
--rollback                     "target_column": "pipelineDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PIPELINE",
--rollback                     "api_resource_sort": "sort=pipelineCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "pipelines"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_OBJECTIVE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PLANTING_SEASON",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "projectCode",
--rollback                     "allow_new_val": true,
--rollback                     "target_column": "projectDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROJECT",
--rollback                     "api_resource_sort": "sort=projectCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "projects"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "value",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "Yield Trial",
--rollback                         "Disease Trial",
--rollback                         "Physiology Trial",
--rollback                         "Other Trial"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "First Year",
--rollback                         "Second Year",
--rollback                         "Re Test",
--rollback                         "Germplasm Viability"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "metadata",
--rollback                     "variable_abbrev": "HAS_EXPERIMENT_GROUP",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev= 'BREEDING_TRIAL_BASIC_INFO_ACT_VAL';