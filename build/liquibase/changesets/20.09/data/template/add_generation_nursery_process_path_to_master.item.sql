--liquibase formatted sql

--changeset postgres:add_generation_nursery_process_path_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add generation nursery process path to master.item



INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage) 
VALUES
    ('GENERATION_NURSERY_DATA_PROCESS','Generation Nursery',40,'Are generation nurseries self-pollination occurs naturally and used for selection and advancement of segregating populations and seed increase','Generation Nursery',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_DESIGN_ACT','Planting Arrangement',30,'Planting Arrangement','Planting Arrangement',1,'active','fa fa-files-o');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-files-o');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT','Planting',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_TRAITS_PROTOCOL_ACT','Traits',20,'Traits Protocol','Traits',1,'active','fa fa-braille');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('GENERATION_NURSERY_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');



--rollback DELETE FROM 
--rollback     master.item 
--rollback WHERE  
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'GENERATION_NURSERY_DATA_PROCESS',
--rollback         'GENERATION_NURSERY_BASIC_INFO_ACT',
--rollback         'GENERATION_NURSERY_ENTRY_LIST_ACT',
--rollback         'GENERATION_NURSERY_DESIGN_ACT',
--rollback         'GENERATION_NURSERY_ADD_BLOCKS_ACT',
--rollback         'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT',
--rollback         'GENERATION_NURSERY_MANAGE_BLOCKS_ACT',
--rollback         'GENERATION_NURSERY_OVERVIEW_ACT',
--rollback         'GENERATION_NURSERY_PROTOCOL_ACT',
--rollback         'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT',
--rollback         'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT',
--rollback         'GENERATION_NURSERY_PLACE_ACT',
--rollback         'GENERATION_NURSERY_REVIEW_ACT'
--rollback     );