--liquibase formatted sql

--changeset postgres:update_config_intentional_crossing_nursery_entry_list_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config intentional crossing nursery entry list act val



UPDATE 
    platform.config 
SET
    config_value = 
    '
        {
        "Name": "Required and default entry level metadata variables for Intentional Crossing Nursery data process",
        "Values": [
                {
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_ROLE",
                    "allowed_values": [
                        "ENTRY_ROLE_FEMALE",
                        "ENTRY_ROLE_FEMALE_AND_MALE",
                        "ENTRY_ROLE_MALE"
                    ],
                    "display_name": "Parent Role",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "PARENT_TYPE",
                    "is_shown": "false", 
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entry-data",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "metadata"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                }
            ]
        }
    ' 
WHERE 
    abbrev= 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for Intentional Crossing Nursery data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "display_name": "Parent Role",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "ENTRY_ROLE_FEMALE",
--rollback                         "ENTRY_ROLE_FEMALE_AND_MALE",
--rollback                         "ENTRY_ROLE_MALE"
--rollback                     ],
--rollback                     "variable_abbrev": "ENTRY_ROLE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "metadata",
--rollback                     "variable_abbrev": "PARENT_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entry-data",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev= 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT_VAL';