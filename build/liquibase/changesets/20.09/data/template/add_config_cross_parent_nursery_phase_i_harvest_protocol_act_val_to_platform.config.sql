--liquibase formatted sql

--changeset postgres:add_config_cross_parent_nursery_phase_i_harvest_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_VAL to platform.config



INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_VAL', 'Cross Parent Nursery Phase I Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Cross Parent Nursery Phase I data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');



--rollback DELETE FROM platform.config WHERE abbrev='CROSS_PARENT_NURSERY_PHASE_I_HARVEST_PROTOCOL_ACT_VAL';