--liquibase formatted sql

--changeset postgres:update_config_breeding_trial_entry_list_act_val_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config BREEDING_TRIAL_ENTRY_LIST_ACT_VAL p2



UPDATE 
    platform.config 
SET 
    config_value = '
        {
            "Name": "Required and default entry level metadata variables for Breeding Trial data process",
            "Values": [{
                    "default": "entry",
                    "allowed_values": [
                        "ENTRY_TYPE_CHECK",
                        "ENTRY_TYPE_ENTRY"
                    ],
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_TYPE",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                }
            ]
        }
'::json 
    WHERE abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for Breeding Trial data process",
--rollback             "Values": [{
--rollback                     "default": "entry",
--rollback                     "allowed_values": [
--rollback                         "ENTRY_TYPE_CHECK",
--rollback                         "ENTRY_TYPE_ENTRY"
--rollback                     ],
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "variable_abbrev": "ENTRY_TYPE",
--rollback                     "target_column": "",
--rollback                     "secondary_target_column":"",
--rollback                     "target_value":"",
--rollback                     "api_resource_method" : "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "api_resource_filter" : "",
--rollback                     "api_resource_sort": "", 
--rollback                     "variable_type" : "identification"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "ENTRY_CLASS",
--rollback                     "target_column": "",
--rollback                     "secondary_target_column":"",
--rollback                     "target_value":"",
--rollback                     "api_resource_method" : "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "api_resource_filter" : "",
--rollback                     "api_resource_sort": "", 
--rollback                     "variable_type" : "identification"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "target_column": "",
--rollback                     "secondary_target_column":"",
--rollback                     "target_value":"",
--rollback                     "api_resource_method" : "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "api_resource_filter" : "",
--rollback                     "api_resource_sort": "", 
--rollback                     "variable_type" : "identification"
--rollback                 }
--rollback             ]
--rollback         }
--rollback '::json 
--rollback WHERE 
--rollback     abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL';