--liquibase formatted sql

--changeset postgres:add_variable_direct_seeded context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1240 Add variable DIRECT_SEEDED



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'DIRECT_SEEDED' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'DIRECT_SEEDED' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('DIRECT_SEEDED','Direct Seeded','Direct Seeded') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'DIRECT_SEEDED' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'DIRECT_SEEDED' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Direct Seeded','DIRECT_SEEDED',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'DIRECT_SEEDED' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'DIRECT_SEEDED' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('DIRECT_SEEDED','categorical','Direct Seeded',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'Hand planted',1,'Hand planted','Hand planted','DIRECT_SEEDED_HAND_PLANTED');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'Machine planted',2,'Machine planted','Machine planted','DIRECT_SEEDED_MACHINE_PLANTED');

	UPDATE master.scale SET min_value='Hand planted', max_value='Machine planted' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Direct Seeded','Direct Seeded','character varying','Direct Seeded','Direct Seeded','False','DIRECT_SEEDED','study','system','experiment, study') 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('EXPERIMENT_CREATION','experiment creation') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DIRECT_SEEDED');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DIRECT_SEEDED');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'DIRECT_SEEDED');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'DIRECT_SEEDED');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'DIRECT_SEEDED');

--rollback DELETE FROM master.variable WHERE abbrev = 'DIRECT_SEEDED';
