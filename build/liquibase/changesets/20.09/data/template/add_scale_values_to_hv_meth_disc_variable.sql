--liquibase formatted sql

--changeset postgres:add_scale_values_to_hv_meth_disc_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1240 Add scale values to HV_METH_DISC



INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Modified bulk',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Modified bulk',
            'Modified bulk',
            'HV_METH_DISC_MODIFIED_BULK'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Selected bulk',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Selected bulk',
            'Selected bulk',
            'HV_METH_DISC_SELECTED_BULK'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual plant',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual plant',
            'Individual plant',
            'HV_METH_DISC_INDIVIDUAL_PLANT'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual spike/ear/panicle',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual spike/ear/panicle',
            'Individual spike/ear/panicle',
            'HV_METH_DISC_INDIVIDUAL_SPIKE_EAR_PANICLE'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Head-rows',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Head-rows',
            'Head-rows',
            'HV_METH_DISC_HEAD_ROWS'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Head-row purification',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Head-row purification',
            'Head-row purification',
            'HV_METH_DISC_HEAD_ROW_PURIFICATION'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Single seed numbering',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Single seed numbering',
            'Single seed numbering',
            'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
        );



--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'HV_METH_DISC_MODIFIED_BULK',
--rollback         'HV_METH_DISC_SELECTED_BULK',
--rollback         'HV_METH_DISC_INDIVIDUAL_PLANT',
--rollback         'HV_METH_DISC_INDIVIDUAL_SPIKE_EAR_PANICLE',
--rollback         'HV_METH_DISC_HEAD_ROWS',
--rollback         'HV_METH_DISC_HEAD_ROW_PURIFICATION',
--rollback         'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
--rollback     );