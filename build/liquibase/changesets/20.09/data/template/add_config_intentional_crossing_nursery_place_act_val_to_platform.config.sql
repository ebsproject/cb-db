--liquibase formatted sql

--changeset postgres:add_config_intentional_crossing_nursery_place_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
  (
      'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_VAL', 
      'Intentional Crossing Nursery Site',
      '{
        "Name": "Required and default place metadata variables for Intentional Crossing Nursery data process",
        "Values": [
            {
            "required": "required",
            "target_column": "occurrenceName",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
            "required": "required",
            "target_column": "siteDbId",
            "secondary_target_column":"geospatialObjectDbId",
            "target_value":"geospatialObjectName",
            "api_resource_method" : "POST",
            "api_resource_endpoint" : "geospatial-objects-search",
            "api_resource_filter" : {"geospatialObjectType": "site"},
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "SITE"
            },
            {
            "target_column": "fieldDbId",
            "secondary_target_column":"geospatialObjectDbId",
            "target_value":"scaleName",
            "api_resource_method" : "POST",
            "api_resource_endpoint" : "geospatial-objects-search",
            "api_resource_filter" : {"geospatialObjectType": "field"},
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "FIELD"
            },
            {
            "disabled": false,
            "target_column": "description",
            "secondary_target_column": "",
            "target_value": "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "variable_type" : "identification",
            "variable_abbrev": "DESCRIPTION"
            },
            {
            "allow_new_val": true,
            "target_column": "contactPerson",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "variable_type" : "metadata",
            "variable_abbrev": "CONTCT_PERSON_CONT"
            }
        ]
        }
        '::json, 
        1, 
        'experiment_creation', 
        1,
        'added by j.antonio'
);



--rollback DELETE FROM platform.config WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_VAL';