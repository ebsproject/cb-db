--liquibase formatted sql

--changeset postgres:add_intentional_crossing_nursery_to_platform.item_module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add intenational crossing nursery mod to platform.item_module



INSERT INTO 
    platform.item_module (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT_MOD')
        ELSE (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT','INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT', 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT', 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT','INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT', 'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT','INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT');

INSERT INTO 
    platform.item_module (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT_MOD')
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT_MOD')
        ELSE (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT','INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT');

INSERT INTO 
    platform.item_module (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT_MOD')
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT_MOD')
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT_MOD')
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT','INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT','INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT','INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT');

INSERT INTO 
    platform.item_module (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT_MOD')
  WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT','INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT_MOD', 
--rollback         'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT',
--rollback        'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   );