--liquibase formatted sql

--changeset postgres:void_unnecessary_templates_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Void unnecessary templates p2



UPDATE 
    master.item 
SET 
    (is_void, abbrev, name) = (true, 'VOIDED-'||abbrev, 'VOIDED-'||name) 
WHERE 
    abbrev
IN
    (
        'EXPT_CROSS_POST_PLANNING_DATA_PROCESS', 
        'EXPT_CROSS_PRE_PLANNING_DATA_PROCESS',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS',
        'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS',
        'EXPT_CROSS_PARENT_DATA_PROCESS',
        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS',
        'EXPT_TRIAL_IRRI_DATA_PROCESS',
        'EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS',
        'EXPT_SEED_INCREASE_DATA_PROCESS',
        'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS',
        'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS',
        'EXPT_NURSERY_CB_DATA_PROCESS',
        'EXPT_TRIAL_DATA_PROCESS'
    );



--rollback SELECT NULL;