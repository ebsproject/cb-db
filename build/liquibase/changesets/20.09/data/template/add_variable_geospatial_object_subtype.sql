--liquibase formatted sql

--changeset postgres:add_variable_geospatial_object_subtype context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1318 Add variable GEOSPATIAL_OBJECT_SUBTYPE



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('GEOSPATIAL_OBJECT_SUBTYPE','Geospatial Object Subtype','Geospatial Object Subtype') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Geospatial Object Subtype','GEOSPATIAL_OBJECT_SUBTYPE',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'GEOSPATIAL_OBJECT_SUBTYPE' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('GEOSPATIAL_OBJECT_SUBTYPE','categorical','Geospatial Object Subtype',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'beeding_location',1,'beeding_location','beeding_location','GEOSPATIAL_OBJECT_SUBTYPE_BEEDING_LOCATION');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'block',2,'block','block','GEOSPATIAL_OBJECT_SUBTYPE_BLOCK');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'breeding location',3,'breeding location','breeding location','GEOSPATIAL_OBJECT_SUBTYPE_BREEDING_LOCATION');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'continent',4,'continent','continent','GEOSPATIAL_OBJECT_SUBTYPE_CONTINENT');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'country',5,'country','country','GEOSPATIAL_OBJECT_SUBTYPE_COUNTRY');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'farm',6,'farm','farm','GEOSPATIAL_OBJECT_SUBTYPE_FARM');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'green house',7,'green house','green house','GEOSPATIAL_OBJECT_SUBTYPE_GREEN_HOUSE');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'growth chamber',8,'growth chamber','growth chamber','GEOSPATIAL_OBJECT_SUBTYPE_GROWTH_CHAMBER');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'international agricultural research center',9,'international agricultural research center','international agricultural research center','GEOSPATIAL_OBJECT_SUBTYPE_INTERNATIONAL_AGRICULTURAL_RESEARCH_CENTER');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'met location',10,'met location','met location','GEOSPATIAL_OBJECT_SUBTYPE_MET_LOCATION');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'national agricultural research center',11,'national agricultural research center','national agricultural research center','GEOSPATIAL_OBJECT_SUBTYPE_NATIONAL_AGRICULTURAL_RESEARCH_CENTER');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'plot',12,'plot','plot','GEOSPATIAL_OBJECT_SUBTYPE_PLOT');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'reservoir',13,'reservoir','reservoir','GEOSPATIAL_OBJECT_SUBTYPE_RESERVOIR');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'screen house',14,'screen house','screen house','GEOSPATIAL_OBJECT_SUBTYPE_SCREEN_HOUSE');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'seed bed',15,'seed bed','seed bed','GEOSPATIAL_OBJECT_SUBTYPE_SEED_BED');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'third sub-national division',16,'third sub-national division','third sub-national division','GEOSPATIAL_OBJECT_SUBTYPE_THIRD_SUB_NATIONAL_DIVISION');

	UPDATE master.scale SET min_value='beeding_location', max_value='third sub-national division' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Geospatial Object Subtype','Geospatial Object Subtype','character varying','Geospatial Object Subtype','Geospatial Object Subtype','True','GEOSPATIAL_OBJECT_SUBTYPE','study','identification',NULL) 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'GEOSPATIAL_OBJECT_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'GEOSPATIAL_OBJECT_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('GEOSPATIAL_OBJECT_INFO','Geospatial Object Info') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE');

--rollback DELETE FROM master.variable WHERE abbrev = 'GEOSPATIAL_OBJECT_SUBTYPE';
