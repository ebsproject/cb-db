--liquibase formatted sql

--changeset postgres:add_intentional_crossing_nursery_to_master.item_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add intentional crossing_nursery to master.item_relation



INSERT INTO 
    master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
  (SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS root_id,
  (SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS parent_id,
   id AS child_id,
   CASE 
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT' THEN 1
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT' THEN 2
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT' THEN 3
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT' THEN 4
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT' THEN 5
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT' THEN 6
     WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT' THEN 7
     ELSE 8 END AS order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT','INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT','INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT','INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT','INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT','INTENTIONAL_CROSSING_NURSERY_PLACE_ACT','INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT');

INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT') AS parent_id,
    id AS child_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT' THEN 1
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT'THEN 2
        ELSE 3 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT','INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT');

INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT') AS parent_id,
    id AS child_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT' THEN 1
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT' THEN 2
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT' THEN 3
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT' THEN 4
        ELSE 5 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT','INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT','INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT','INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT');

INSERT INTO
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT') AS parent_id,
    id AS child_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT' THEN 1
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT'THEN 2
        ELSE 3 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT','INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT');



--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'INTENTIONAL_CROSSING_NURSERY_BASIC_INFO_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_ENTRY_LIST_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_PLACE_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_REVIEW_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_CROSSES_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_MANAGE_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DESIGN_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'INTENTIONAL_CROSSING_NURSERY_ADD_BLOCKS_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_ASSIGN_ENTRIES_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_MANAGE_BLOCKS_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_OVERVIEW_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'INTENTIONAL_CROSSING_NURSERY_PLANTING_PROTOCOL_ACT',
--rollback                  'INTENTIONAL_CROSSING_NURSERY_TRAITS_PROTOCOL_ACT'
--rollback             )
--rollback     );