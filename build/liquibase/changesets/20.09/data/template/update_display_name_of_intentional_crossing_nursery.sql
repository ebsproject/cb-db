--liquibase formatted sql

--changeset postgres:update_display_name_of_intentional_crossing_nursery context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update display name of intentional crossing nursery



UPDATE 
    master.item 
SET 
    (name, display_name) = ('Intentional Crossing Nursery','Intentional Crossing Nursery') 
WHERE 
    abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS';

UPDATE 
    master.item 
SET 
    (name, display_name) = ('Breeding Trial','Breeding Trial') 
WHERE 
    abbrev = 'BREEDING_TRIAL_DATA_PROCESS';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name, display_name) = ('Intentional Crossing Nursery','Intentional Crossing Nursery') 
--rollback WHERE 
--rollback     abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS';
--rollback 
--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name, display_name) = ('Breeding Trial Experiment','BreedBreeding Trial Experiment') 
--rollback WHERE 
--rollback     abbrev = 'BREEDING_TRIAL_DATA_PROCESS';
