--liquibase formatted sql

--changeset postgres:add_cross_parent_nursery_p2_mod_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add cross parent nursery p2 mod to platform.module



INSERT INTO 
    platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
VALUES 
  ('CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_MOD','Specify Parent List','Specify Parent List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT_MOD','Manage Crosses','Manage Crosses','experimentCreation','create','manage-crosses',1,'added by j.antonio ' || now(), 'cross list created'),
  ('CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT_MOD','Manage','Manage','experimentCreation','create','manage-crosslist',1,'added by j.antonio ' || now(), 'cross list created'),
  ('CROSS_PARENT_NURSERY_PHASE_II_MATRIX_ACT_MOD','Matrix','Matrix','experimentCreation','create','manage-crossing-matrix',1,'added by j.antonio ' || now(), 'cross list created'),
  ('CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'oiccurrences created'),
  ('CROSS_PARENT_NURSERY_PHASE_II_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');



--rollback DELETE FROM 
--rollback     platform.module
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_MATRIX_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_II_REVIEW_ACT_MOD'
--rollback     );