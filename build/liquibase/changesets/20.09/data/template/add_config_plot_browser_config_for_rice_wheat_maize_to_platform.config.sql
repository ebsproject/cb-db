--liquibase formatted sql

--changeset postgres:add_config_plot_browser_config_for_rice_wheat_maize_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config plot browser config for rice, wheat and maize to platform.config
--validCheckSum: 8:2a809e5acd26fb3e722b2699a7b4c664



INSERT INTO
    platform.config
(
    abbrev,
    name,
    config_value,
    rank,
    usage,
    creation_timestamp,
    creator_id,
    notes,
    is_void
)
VALUES
(
    'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE',
    'Harvest Manager Plot Browser Config for Rice',
$$
    {
        "name" : "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE",
        "values": [
            {
                "germplasm_states" : {
                    "fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod"],
                        "methods" : [
                            "Bulk"
                        ]
                    },
                    "not_fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod","numericVar"],
                        "methods" : [
                            "Bulk",
                            "Bulk fixedline",
                            "Panicle Selection",
                            "Single Plant Selection",
                            "Single Plant Selection and Bulk",
                            "plant-specific",
                            "Plant-Specific and Bulk",
                            "Single Seed Descent"
                        ]
                    }
                }
            }
        ]
    }
$$,
    1,
    'harvest_manager',
    now(),
    1,
    'B4R-7074 create configs - j.bantay 2020-09-16',
    false    
),
(
    'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_MAIZE',
    'Harvest Manager Plot Browser Config for Maize',
$$
    {
        "name" : "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_MAIZE",
        "values": [
            {
                "germplasm_states" : {
                    "fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod"],
                        "methods" : [
                            "Bulk"
                        ]
                    },
                    "not_fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod","numericVar"],
                        "methods" : [
                            "Bulk",
                            "Individual ear"
                        ]
                    }
                }
            }
        ]
    }
$$,
    1,
    'harvest_manager',
    now(),
    1,
    'B4R-7074 create configs - j.bantay 2020-09-16',
    false    
),
(
    'HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT',
    'Harvest Manager Plot Browser Config for Wheat',
$$
    {
        "name" : "HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT",
        "values": [
            {
                "germplasm_states" : {
                    "fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod"],
                        "methods" : [
                            "Bulk"
                        ]
                    },
                    "not_fixed" : {
                        "display_columns" : ["harvestDate","harvestMethod","numericVar"],
                        "methods" : [
                            "Bulk",
                            "Modified bulk",
                            "Single Plant Selection",
                            "Individual spike"
                        ]
                    }
                }
            }
        ]
    }
$$,
    1,
    'harvest_manager',
    now(),
    1,
    'B4R-7074 create configs - j.bantay 2020-09-16',
    false    
);



--rollback DELETE FROM platform.config WHERE abbrev IN ('HARVEST_MANAGER_PLOT_BROWSER_CONFIG_RICE','HARVEST_MANAGER_PLOT_BROWSER_CONFIG_MAIZE','HARVEST_MANAGER_PLOT_BROWSER_CONFIG_WHEAT');