--liquibase formatted sql

--changeset postgres:add_breeding_trial_act_to_master.item_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add breeding trial activity to master.item_relation



INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
	(SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS root_id,
	(SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS parent_id,
	id AS child_id,
	CASE
		WHEN abbrev = 'BREEDING_TRIAL_BASIC_INFO_ACT' then 1
		WHEN abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT'then 2
		WHEN abbrev = 'BREEDING_TRIAL_DESIGN_ACT'then 3
		WHEN abbrev = 'BREEDING_TRIAL_EXPT_GROUP_ACT'then 4
        WHEN abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT'then 5
		WHEN abbrev = 'BREEDING_TRIAL_PLACE_ACT'then 6
		WHEN abbrev = 'BREEDING_TRIAL_REVIEW_ACT'then 7
		ELSE 8 
    END AS order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
FROM
	master.item
WHERE
	abbrev 
IN 
    (
        'BREEDING_TRIAL_BASIC_INFO_ACT',
        'BREEDING_TRIAL_ENTRY_LIST_ACT',
        'BREEDING_TRIAL_DESIGN_ACT',
        'BREEDING_TRIAL_EXPT_GROUP_ACT',
        'BREEDING_TRIAL_PROTOCOL_ACT',
        'BREEDING_TRIAL_PLACE_ACT',
        'BREEDING_TRIAL_REVIEW_ACT'
    );

INSERT INTO
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
	(SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS root_id,
	(SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT') AS parent_id,
	id AS child_id,
    CASE
		WHEN abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT' then 1
		WHEN abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'then 2
		ELSE 3 
    END AS order_number,
	0,
	1,
	1,
	'added by j.antonio ' || now()
FROM
	master.item
WHERE
    abbrev 
IN 
    (
        'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT',
        'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'
    );



--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'BREEDING_TRIAL_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'BREEDING_TRIAL_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'BREEDING_TRIAL_BASIC_INFO_ACT',
--rollback                  'BREEDING_TRIAL_ENTRY_LIST_ACT',
--rollback                  'BREEDING_TRIAL_DESIGN_ACT',
--rollback                  'BREEDING_TRIAL_EXPT_GROUP_ACT',
--rollback                  'BREEDING_TRIAL_PROTOCOL_ACT',
--rollback                  'BREEDING_TRIAL_PLACE_ACT',
--rollback                  'BREEDING_TRIAL_REVIEW_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'BREEDING_TRIAL_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'BREEDING_TRIAL_PROTOCOL_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT',
--rollback                  'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT'
--rollback             )
--rollback     );