--liquibase formatted sql

--changeset postgres:add_breeding_trial_mod_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add breeding trial module to platform.module



INSERT INTO 
    platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
VALUES
    ('BREEDING_TRIAL_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
    ('BREEDING_TRIAL_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
    ('BREEDING_TRIAL_DESIGN_ACT_MOD','Specify Design','Specify Design','experimentCreation','create','specify-design',1,'added by j.antonio ' || now(), 'design generated'),
    ('BREEDING_TRIAL_EXPT_GROUP_ACT_MOD','Specify Experiment Groups','Specify Experiment Groups','experimentCreation','create','specify-experiment-groups',1,'added by j.antonio ' || now(), 'experiment group specified'),
    ('BREEDING_TRIAL_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocos','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('BREEDING_TRIAL_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
    ('BREEDING_TRIAL_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');



--rollback DELETE FROM 
--rollback     platform.module
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'BREEDING_TRIAL_BASIC_INFO_ACT_MOD',
--rollback         'BREEDING_TRIAL_ENTRY_LIST_ACT_MOD',
--rollback         'BREEDING_TRIAL_DESIGN_ACT_MOD',
--rollback         'BREEDING_TRIAL_EXPT_GROUP_ACT_MOD',
--rollback         'BREEDING_TRIAL_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_PLACE_ACT_MOD',
--rollback         'BREEDING_TRIAL_REVIEW_ACT_MOD'
--rollback     );