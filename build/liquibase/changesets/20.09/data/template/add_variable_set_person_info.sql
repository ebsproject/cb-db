--liquibase formatted sql

--changeset postgres:add_variable_set_person_info context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1318 Add variable set PERSON_INFO



INSERT INTO
			master.variable_set (abbrev,name,creator_id) 
		VALUES 
			('PERSON_INFO','Person Info',1);



--rollback DELETE FROM master.variable_set WHERE abbrev='PERSON_INFO';