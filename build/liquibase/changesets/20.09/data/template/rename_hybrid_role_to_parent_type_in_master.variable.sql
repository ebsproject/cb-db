--liquibase formatted sql

--changeset postgres:rename_hybrid_role_to_parent_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1220 Rename HYBRID_ROLE to PARENT_TYPE in master.variable



UPDATE 
    master.variable
SET
    abbrev = 'PARENT_TYPE',
    display_name = 'Parent Type',
    label = 'PARENT TYPE',
    name = 'PARENT TYPE'
WHERE
    abbrev = 'HYBRID_ROLE';



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'HYBRID_ROLE',
--rollback     display_name = 'HYBRID ROLE',
--rollback     label = 'HYBRID ROLE',
--rollback     name = 'HYBRID ROLE'
--rollback WHERE
--rollback     abbrev = 'PARENT_TYPE';