--liquibase formatted sql

--changeset postgres:void_trait_variables_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1626 Void trait variables p2



UPDATE 
    master.variable
SET 
    is_void=TRUE
WHERE
    abbrev
IN
    (
        'HVHILL_CONT',
        'TOTILL_CONT',
        'HARVEST_UNIT',
        'PAN_CROSS'
    );



--rollback UPDATE 
--rollback     master.variable
--rollback SET 
--rollback     is_void=FALSE
--rollback WHERE
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'HVHILL_CONT',
--rollback         'TOTILL_CONT',
--rollback         'HARVEST_UNIT',
--rollback         'PAN_CROSS'
--rollback     );