--liquibase formatted sql

--changeset postgres:add_breeding_trial_process_path_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add breeding trial process path to master.item



INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage)
VALUES
    ('BREEDING_TRIAL_DATA_PROCESS','Breeding Trial Experiment',40,'Are experiment objectives such as yield, disease, physiological, etc.','Breeding Trial Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_DESIGN_ACT','Specify Design',30,'Specify Design','Design',1,'active','fa fa-th');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_EXPT_GROUP_ACT','Specify Experiment Groups',30,'Specify Experiment Groups','Experiment Groups',1,'active','fa fa-bars');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-map-marker');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_PLANTING_PROTOCOL_ACT','Planting Protocol',20,'Planting Protocol','Planting Protocol',1,'active','fa fa-pagelines');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_TRAITS_PROTOCOL_ACT','Traits Protocol',20,'Traits Protocol','Traits Protocol',1,'active','fa fa-braille');

INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker');

INSERT INTO
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('BREEDING_TRIAL_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');



--rollback DELETE FROM 
--rollback     master.item
--rollback WHERE 
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'BREEDING_TRIAL_DATA_PROCESS',
--rollback         'BREEDING_TRIAL_BASIC_INFO_ACT',
--rollback         'BREEDING_TRIAL_ENTRY_LIST_ACT',
--rollback         'BREEDING_TRIAL_DESIGN_ACT',
--rollback         'BREEDING_TRIAL_EXPT_GROUP_ACT',
--rollback         'BREEDING_TRIAL_PROTOCOL_ACT',
--rollback         'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT',
--rollback         'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT',
--rollback         'BREEDING_TRIAL_PLACE_ACT',
--rollback         'BREEDING_TRIAL_REVIEW_ACT'
--rollback     );