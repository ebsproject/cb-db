--liquibase formatted sql

--changeset postgres:add_config_breeding_trial_harvest_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_VAL to platform.config



INSERT INTO platform.config(
    abbrev, name, config_value, rank, usage, creator_id, notes)
    VALUES ('BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_VAL', 'Breeding Trial Harvest Protocol variables', 
      '{
      "Name": "Required experiment level harvest protocol variables for Breeding Trial data process",
      "Values": [{
              "default": false,
              "disabled": false,
              "variable_abbrev": "HV_METH_DISC"
          },{
              "default": false,
              "disabled": false,
              "variable_abbrev": "REMARKS"
          }
      ]
  }'::json, 1, 'experiment_creation', 1,'added by j.antonio');



--rollback DELETE FROM platform.config WHERE abbrev='BREEDING_TRIAL_HARVEST_PROTOCOL_ACT_VAL';