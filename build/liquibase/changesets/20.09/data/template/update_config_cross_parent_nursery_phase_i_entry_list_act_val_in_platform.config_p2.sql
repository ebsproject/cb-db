--liquibase formatted sql

--changeset postgres:update_config_cross_parent_nursery_phase_i_entry_list_act_val_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '{
    "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase I data process",
    "Values": [
        {
            "disabled": false,
            "default": "female-and-male",
            "variable_abbrev": "ENTRY_ROLE",
            "allowed_values": [
                "ENTRY_ROLE_FEMALE",
                "ENTRY_ROLE_FEMALE_AND_MALE",
                "ENTRY_ROLE_MALE"
            ],
            "display_name": "Parent Role",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        },
        {
            "disabled": false,
            "variable_abbrev": "DESCRIPTION",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint": "entries",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification"
        }
    ]
    }'::json 
WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback         '{
--rollback         "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase I data process",
--rollback         "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "default": "female-and-male",
--rollback                     "variable_abbrev": "ENTRY_ROLE",
--rollback                     "allowed_values": [
--rollback                         "ENTRY_ROLE_FEMALE",
--rollback                         "ENTRY_ROLE_FEMALE_AND_MALE",
--rollback                         "ENTRY_ROLE_MALE"
--rollback                     ],
--rollback                     "display_name": "Parent Role",
--rollback                     "target_column": "",
--rollback                     "secondary_target_column":"",
--rollback                     "target_value":"",
--rollback                     "api_resource_method" : "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "api_resource_filter" : "",
--rollback                     "api_resource_sort": "", 
--rollback                     "variable_type" : "identification"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "target_column": "",
--rollback                     "secondary_target_column":"",
--rollback                     "target_value":"",
--rollback                     "api_resource_method" : "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "api_resource_filter" : "",
--rollback                     "api_resource_sort": "", 
--rollback                     "variable_type" : "identification"
--rollback                 }
--rollback             ]
--rollback         }'::json 
--rollback WHERE 
--rollback     abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL';