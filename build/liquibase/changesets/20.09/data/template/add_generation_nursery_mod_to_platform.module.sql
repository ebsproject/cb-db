--liquibase formatted sql

--changeset postgres:add_generation_nursery_mod_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add generation nursery module to platform.module



INSERT INTO 
    platform.module (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
VALUES 
  ('GENERATION_NURSERY_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('GENERATION_NURSERY_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('GENERATION_NURSERY_DESIGN_ACT_MOD','Planting Arrangement','Planting Arrangement','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD','Add Blocks','Add Blocks','experimentCreation','create','add-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD','Assign Entries','Assign Entries','experimentCreation','create','assign-entries',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD','Manage Blocks','Manage Blocks','experimentCreation','create','manage-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_OVERVIEW_ACT_MOD','Overview','Overview','experimentCreation','create','overview',1,'added by j.antonio ' || now(), 'design generated'),
  ('GENERATION_NURSERY_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('GENERATION_NURSERY_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
  ('GENERATION_NURSERY_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');



--rollback DELETE FROM 
--rollback     platform.module
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'GENERATION_NURSERY_BASIC_INFO_ACT_MOD',
--rollback         'GENERATION_NURSERY_ENTRY_LIST_ACT_MOD',
--rollback         'GENERATION_NURSERY_DESIGN_ACT_MOD',
--rollback         'GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD',
--rollback         'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD',
--rollback         'GENERATION_NURSERY_OVERVIEW_ACT_MOD',
--rollback         'GENERATION_NURSERY_PROTOCOL_ACT_MOD',
--rollback         'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'GENERATION_NURSERY_PLACE_ACT_MOD',
--rollback         'GENERATION_NURSERY_REVIEW_ACT_MOD'
--rollback     );