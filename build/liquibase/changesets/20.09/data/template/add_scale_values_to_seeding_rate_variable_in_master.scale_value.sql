--liquibase formatted sql

--changeset postgres:add_scale_values_to_seeding_rate_variable_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1240 Add scale values to SEEDING_RATE variable in master.scale_value



--update variable
UPDATE 
    master.variable
SET
    label = 'Seeding Density',
    name = 'Seeding Density',
    display_name = 'Seeding Density',
    data_type = 'character varying'
WHERE
    abbrev = 'SEEDING_RATE';

--add scale values
INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        'Sparse',
        (SELECT COALESCE(max(order_number)+1,1) FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        'Sparse',
        'Sparse',
        'SEEDING_DENSITY_SPARSE'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        'Normal',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        'Normal',
        'Normal',
        'SEEDING_DENSITY_NORMAL'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        'Dense',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        'Dense',
        'Dense',
        'SEEDING_DENSITY_DENSE'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        '9 g/m2',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        '9 g/m2',
        '9 g/m2',
        'SEEDING_DENSITY_9_G_M2'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        '12 g/m2',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        '12 g/m2',
        '12 g/m2',
        'SEEDING_DENSITY_12_G_M2'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        '200 seeds/m2',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        '200 seeds/m2',
        '200 seeds/m2',
        'SEEDING_DENSITY_200_SEEDS_M2'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        'Single seeded',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        'Single seeded',
        'Single seeded',
        'SEEDING_DENSITY_SINGLE_SEEDED'
    );

INSERT INTO 
    master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='SEEDING_RATE'),
        'Single seed descent',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE')),
        'Single seed descent',
        'Single seed descent',
        'SEEDING_DENSITY_SINGLE_SEED_DESCENT'
    );



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     label = 'SEEDING RATE',
--rollback     name = 'SEEDING RATE',
--rollback     display_name = 'SEEDING RATE',
--rollback     data_type = 'float'
--rollback WHERE
--rollback     abbrev = 'SEEDING_RATE';

--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'SEEDING_RATE');