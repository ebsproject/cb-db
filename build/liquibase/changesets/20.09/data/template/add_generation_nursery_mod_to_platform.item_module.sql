--liquibase formatted sql

--changeset postgres:add_generation_nursery_mod_to_platform.item_module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add generation nursery mod to platform.item_module



INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
        WHEN abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT_MOD')
        WHEN abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT_MOD')
        WHEN abbrev = 'GENERATION_NURSERY_DESIGN_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_DESIGN_ACT_MOD')
      WHEN abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_PROTOCOL_ACT_MOD')
        WHEN abbrev = 'GENERATION_NURSERY_PLACE_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_PLACE_ACT_MOD')
        WHEN abbrev = 'GENERATION_NURSERY_REVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_REVIEW_ACT_MOD')
        ELSE (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_REVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('GENERATION_NURSERY_BASIC_INFO_ACT','GENERATION_NURSERY_ENTRY_LIST_ACT', 'GENERATION_NURSERY_DESIGN_ACT','GENERATION_NURSERY_PROTOCOL_ACT', 'GENERATION_NURSERY_PLACE_ACT','GENERATION_NURSERY_REVIEW_ACT');

INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  WHEN abbrev = 'GENERATION_NURSERY_ADD_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD')
  WHEN abbrev = 'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD')
  WHEN abbrev = 'GENERATION_NURSERY_MANAGE_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD')
  WHEN abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_OVERVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('GENERATION_NURSERY_ADD_BLOCKS_ACT','GENERATION_NURSERY_ASSIGN_ENTRIES_ACT','GENERATION_NURSERY_MANAGE_BLOCKS_ACT','GENERATION_NURSERY_OVERVIEW_ACT');

INSERT INTO platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  WHEN abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD')
  WHEN abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev
IN 
    ('GENERATION_NURSERY_PLANTING_PROTOCOL_ACT','GENERATION_NURSERY_TRAITS_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'GENERATION_NURSERY_BASIC_INFO_ACT',
--rollback        'GENERATION_NURSERY_ENTRY_LIST_ACT',
--rollback        'GENERATION_NURSERY_DESIGN_ACT',
--rollback        'GENERATION_NURSERY_PROTOCOL_ACT',
--rollback        'GENERATION_NURSERY_PLACE_ACT',
--rollback        'GENERATION_NURSERY_REVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'GENERATION_NURSERY_BASIC_INFO_ACT_MOD',
--rollback         'GENERATION_NURSERY_ENTRY_LIST_ACT_MOD',
--rollback         'GENERATION_NURSERY_DESIGN_ACT_MOD',
--rollback         'GENERATION_NURSERY_PROTOCOL_ACT_MOD', 
--rollback         'GENERATION_NURSERY_PLACE_ACT_MOD',
--rollback         'GENERATION_NURSERY_REVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'GENERATION_NURSERY_ADD_BLOCKS_ACT',
--rollback        'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT',
--rollback        'GENERATION_NURSERY_MANAGE_BLOCKS_ACT',
--rollback        'GENERATION_NURSERY_OVERVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'GENERATION_NURSERY_ADD_BLOCKS_ACT_MOD',
--rollback         'GENERATION_NURSERY_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'GENERATION_NURSERY_MANAGE_BLOCKS_ACT_MOD',
--rollback         'GENERATION_NURSERY_OVERVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT',
--rollback        'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'GENERATION_NURSERY_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'GENERATION_NURSERY_TRAITS_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   );