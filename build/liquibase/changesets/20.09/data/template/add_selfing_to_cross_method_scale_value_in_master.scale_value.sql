--liquibase formatted sql

--changeset postgres:add_selfing_to_cross_method_scale_value_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1383 Add selfing to cross method scale value in master.scale_value



INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='CROSS_METHOD'),
        'SELFING',
        (SELECT COALESCE(max(order_number)+1,1) FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'CROSS_METHOD')),
        'Selfing',
        'CROSS_METHOD_SELFING'
    );



--rollback DELETE FROM master.scale_value WHERE abbrev = 'CROSS_METHOD_SELFING';