--liquibase formatted sql

--changeset postgres:update_display_name_of_breeding_trial_protocol context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update display name of breeding trial protocol



UPDATE master.item SET (name,display_name) = ('Traits','Traits') WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT';
UPDATE master.item SET (name,display_name) = ('Planting','Planting') WHERE abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT';



--rollback UPDATE master.item SET (name,display_name) = ('Trial','Trial') WHERE abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT';
--rollback UPDATE master.item SET (name,display_name) = ('Planting','Planting') WHERE abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT';