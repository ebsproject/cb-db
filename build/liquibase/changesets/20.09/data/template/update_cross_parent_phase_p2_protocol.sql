--liquibase formatted sql

--changeset postgres:update_cross_parent_phase_p2_protocol context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update cross parent phase p2 protocol



UPDATE master.item SET is_void = true WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT';

UPDATE platform.module SET is_void = true WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_MOD';

UPDATE platform.module SET  action_id = 'manage-crosses' WHERE  abbrev= 'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT_MOD';



--rollback UPDATE master.item SET is_void = false WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT';

--rollback UPDATE platform.module SET is_void = false WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT_MOD';
 
--rollback UPDATE platform.module SET  action_id = 'manage-crosslist' WHERE  abbrev= 'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT_MOD';