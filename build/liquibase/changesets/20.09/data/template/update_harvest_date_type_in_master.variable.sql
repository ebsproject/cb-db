--liquibase formatted sql

--changeset postgres:update_harvest_date_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-7458 Update harvest_date type in master.variable



UPDATE 
    master.variable
SET
    type='observation'
WHERE
    abbrev='HVDATE_CONT';

UPDATE 
    master.variable
SET
    is_void=true
WHERE
    abbrev='HARVEST_DATE';



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     type='metadata'
--rollback WHERE
--rollback     abbrev='HVDATE_CONT';

--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     is_void=false
--rollback WHERE
--rollback     abbrev='HARVEST_DATE';