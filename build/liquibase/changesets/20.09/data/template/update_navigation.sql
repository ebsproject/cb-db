--liquibase formatted sql

--changeset postgres:update_navigation context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update navigation



-- update navigation items
UPDATE 
    platform.space 
SET 
    menu_data = '
    {
        "left_menu_items": [{
            "name": "experiment-creation",
            "label": "Experiment creation",
            "appAbbrev": "EXPERIMENT_CREATION"
            }, {
            "name": "experiment-manager",
            "label": "Experiment manager",
            "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data Collection",
                "appAbbrev": "QUALITY_CONTROL"	
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            },{
                "name": "search",
                "items": [
                {
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },{
                    "name": "search-seeds",
                    "label": "Seeds",
                    "appAbbrev": "FIND_SEEDS"
                },{
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }],
                "label": "Search"
            }
        ]
    }' 
WHERE abbrev = 'DEFAULT';

UPDATE 
    platform.space 
SET 
    menu_data = '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data Collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "search",
                "items": [{
                    "name": "searchs-germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                }, {
                    "name": "search-seeds",
                    "label": "Seeds",
                    "appAbbrev": "FIND_SEEDS"
                }, {
                    "name": "search-traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }],
                "label": "Search"
            }],
            "main_menu_items": [{
                "icon": "folder_special",
                "name": "data-management",
                "items": [{
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                }],
                "label": "Data management"
            }, {
                "icon": "settings",
                "name": "administration",
                "items": [{
                    "name": "administration_users",
                    "label": "Persons",
                    "appAbbrev": "PERSONS"
                }],
                "label": "Administration"
            }]
    }' 
WHERE abbrev = 'ADMIN';

-- update label of Quality Control and Find Seeds
UPDATE platform.application SET label = 'Data Collection', action_label = 'Data Collection' WHERE abbrev = 'QUALITY_CONTROL';

UPDATE platform.application SET label = 'Seeds' WHERE abbrev = 'FIND_SEEDS';



--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiment creation",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "experiment-manager",
--rollback                     "label": "Experiment manager",
--rollback                     "appAbbrev": "OCCURRENCES"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ],
--rollback             "main_menu_items": [
--rollback                 {
--rollback                     "icon": "folder_special",
--rollback                     "name": "data-management",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-management_germplasm",
--rollback                             "label": "Germplasm",
--rollback                             "appAbbrev": "GERMPLASM_CATALOG"
--rollback                         },
--rollback                         {
--rollback                             "name": "data-management_traits",
--rollback                             "label": "Traits",
--rollback                             "appAbbrev": "TRAITS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data management"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'DEFAULT';
--rollback 
--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiment creation",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "experiment-manager",
--rollback                     "label": "Experiment manager",
--rollback                     "appAbbrev": "OCCURRENCES"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ],
--rollback             "main_menu_items": [
--rollback                 {
--rollback                     "icon": "folder_special",
--rollback                     "name": "data-management",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-management_seasons",
--rollback                             "label": "Seasons",
--rollback                             "appAbbrev": "MANAGE_SEASONS"
--rollback                         },
--rollback                         {
--rollback                             "name": "data-management_germplasm",
--rollback                             "label": "Germplasm",
--rollback                             "appAbbrev": "GERMPLASM_CATALOG"
--rollback                         },
--rollback                         {
--rollback                             "name": "data-management_traits",
--rollback                             "label": "Traits",
--rollback                             "appAbbrev": "TRAITS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data management"
--rollback                 },
--rollback                 {
--rollback                     "icon": "settings",
--rollback                     "name": "administration",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "administration_users",
--rollback                             "label": "Persons",
--rollback                             "appAbbrev": "PERSONS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Administration"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'ADMIN';
--rollback 
--rollback UPDATE platform.application SET label = 'Quality control', action_label = 'Quality control' WHERE abbrev = 'QUALITY_CONTROL';
--rollback 
--rollback UPDATE platform.application SET label = 'Find seeds' WHERE abbrev = 'FIND_SEEDS';