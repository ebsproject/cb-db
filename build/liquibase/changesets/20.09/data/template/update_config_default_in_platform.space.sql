--liquibase formatted sql

--changeset postgres:update_config_default_in_platform.space context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1617 Update config DEFAULT in platform.space



UPDATE 
    platform.space
SET 
    menu_data='{
    "left_menu_items": [
        {
            "name": "experiment-creation",
            "label": "Experiment creation",
            "appAbbrev": "EXPERIMENT_CREATION"
        },
        {
            "name": "experiment-manager",
            "label": "Experiment manager",
            "appAbbrev": "OCCURRENCES"
        },
        {
            "name": "data-collection-qc",
            "items": [
                {
                    "name": "data-collection-qc-quality-control",
                    "label": "Quality control",
                    "appAbbrev": "QUALITY_CONTROL"
                }
            ],
            "label": "Data collection & QC"
        },
        {
            "name": "seeds",
            "items": [
                {
                    "name": "seeds-find-seeds",
                    "label": "Find seeds",
                    "appAbbrev": "FIND_SEEDS"
                },
                {
                    "name": "seeds-harvest-manager",
                    "label": "Harvest manager",
                    "appAbbrev": "HARVEST_MANAGER"
                }
            ],
            "label": "Seeds"
        }
    ],
    "main_menu_items": [
        {
            "icon": "folder_special",
            "name": "data-management",
            "items": [
                {
                    "name": "data-management_germplasm",
                    "label": "Germplasm",
                    "appAbbrev": "GERMPLASM_CATALOG"
                },
                {
                    "name": "data-management_traits",
                    "label": "Traits",
                    "appAbbrev": "TRAITS"
                }
            ],
            "label": "Data management"
        }
    ]
}'
WHERE abbrev='DEFAULT';



--rollback UPDATE 
--rollback     platform.space
--rollback SET 
--rollback     menu_data=
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiment creation",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "experiment-manager",
--rollback                     "label": "Experiment manager",
--rollback                     "appAbbrev": "OCCURRENCES"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev='DEFAULT';