--liquibase formatted sql

--changeset postgres:update_name_and_display_name_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update name and display_name in master.item



UPDATE 
    master.item 
SET 
    (name, display_name) = ('Planting','Planting') 
WHERE 
    abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT';

UPDATE 
    master.item 
SET 
    (name, display_name) = ('Trial','Trial') 
WHERE 
    abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name, display_name) = ('Planting Protocol','Planting Protocol') 
--rollback WHERE 
--rollback     abbrev = 'BREEDING_TRIAL_PLANTING_PROTOCOL_ACT';
--rollback 
--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name, display_name) = ('Traits Protocol','Traits Protocol') 
--rollback WHERE 
--rollback     abbrev = 'BREEDING_TRIAL_TRAITS_PROTOCOL_ACT';