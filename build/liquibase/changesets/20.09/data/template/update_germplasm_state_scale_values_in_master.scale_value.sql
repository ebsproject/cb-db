--liquibase formatted sql

--changeset postgres:update_germplasm_state_scale_values_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1568 Update GERMPLASM_STATE scale values in master.scale_value



UPDATE
    master.scale_value
SET
    is_void = true
WHERE
    scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_STATE')
AND
    value 
IN
    (
        'progeny',
        'fixed_line',
        'hybrid',
        'accession'
    );

INSERT INTO
    master.scale_value
        (scale_id, value, order_number, description, display_name, abbrev, creator_id)
VALUES
    (
        (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_STATE'),
        'fixed',
        1,
        'fixed',
        'fixed',
        'GERMPLASM_STATE_FIXED',
        1
    );

INSERT INTO
    master.scale_value
        (scale_id, value, order_number, description, display_name, abbrev, creator_id)
VALUES
    (
        (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_STATE'),
        'not_fixed',
        2,
        'not_fixed',
        'not_fixed',
        'GERMPLASM_STATE_NOT_FIXED',
        1
    );



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('GERMPLASM_STATE_FIXED','GERMPLASM_STATE_NOT_FIXED');

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     is_void = false
--rollback WHERE
--rollback     scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_STATE')
--rollback AND
--rollback     value 
--rollback IN
--rollback     (
--rollback         'progeny',
--rollback         'fixed_line',
--rollback         'hybrid',
--rollback         'accession'
--rollback     );