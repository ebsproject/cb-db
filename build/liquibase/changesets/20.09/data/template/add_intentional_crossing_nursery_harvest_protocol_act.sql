--liquibase formatted sql

--changeset postgres:add_intentional_crossing_nursery_harvest_protocol_act context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT



--add to item
INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper');

--add to item_relation
INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT') AS parent_id,
    id AS child_id,
    CASE
        WHEN abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT' THEN 3
        ELSE 4 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');

--add to module
INSERT INTO 
    platform.module 
        (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
values
    ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified');

--add to item_module
INSERT INTO platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    (SELECT id FROM platform.module WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD') AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT'
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   ));

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'INTENTIONAL_CROSSING_NURSERY_PROTOCOL_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM platform.module WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT_MOD';

--rollback DELETE FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_HARVEST_PROTOCOL_ACT';