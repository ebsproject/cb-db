--liquibase formatted sql

--changeset postgres:add_config_cross_parent_nursery_phase_i_entry_list_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL', 
        'Cross Parent Nursery Phase I Entry List',
        '{
            "Name": "Required and default entry level metadata variables for Cross Parent Nursery Phase I data process",
            "Values": [
                    {
                        "disabled": false,
                        "variable_abbrev": "ENTRY_ROLE",
                        "allowed_values": [
                            "ENTRY_ROLE_FEMALE",
                            "ENTRY_ROLE_FEMALE_AND_MALE",
                            "ENTRY_ROLE_MALE"
                        ],
                        "display_name": "Parent Role",
                        "target_column": "",
                        "secondary_target_column":"",
                        "target_value":"",
                        "api_resource_method" : "",
                        "api_resource_endpoint": "entries",
                        "api_resource_filter" : "",
                        "api_resource_sort": "", 
                        "variable_type" : "identification"
                    },
                    {
                        "disabled": false,
                        "variable_abbrev": "DESCRIPTION",
                        "target_column": "",
                        "secondary_target_column":"",
                        "target_value":"",
                        "api_resource_method" : "",
                        "api_resource_endpoint": "entries",
                        "api_resource_filter" : "",
                        "api_resource_sort": "", 
                        "variable_type" : "identification"
                    }
                ]
            }'::json, 
            1, 
            'experiment_creation', 
            1,
            'added by j.antonio'
        );



--rollback DELETE FROM platform.config WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_VAL';