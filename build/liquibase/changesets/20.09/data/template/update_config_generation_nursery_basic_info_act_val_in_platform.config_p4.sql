--liquibase formatted sql

--changeset postgres:update_config_generation_nursery_basic_info_act_val_in_platform.config_p4 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config GENERATION_NURSERY_BASIC_INFO_ACT_VAL p4



UPDATE 
    platform.config 
SET 
    config_value = '{
      "Name": "Required experiment level metadata variables for Generation Nursery data process",
      "Values": [
        {
            "default": false,
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_TYPE"
        },
        {
            "disabled": true,
            "required": "required",
            "target_column": "cropDbId",
            "target_value" : "cropCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "crops",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=cropCode",
            "variable_type" : "identification",
            "variable_abbrev": "CROP"
        },
        {
            "disabled": true,
            "required": "required",
            "target_column": "programDbId",
            "target_value":"programCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "programs",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=programCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROGRAM"
        },
    {
            "default" : "EXPT-XXXX",
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_CODE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_NAME"
        },
        {
            "disabled": false,
            "required": "required",
            "allowed_values": [
                "BRE",
                "F1",
                "F2",
                "F3",
                "F4",
                "F5",
                "F6",
                "F7",
                "F8",
                "F9",
                "RGA",
                "SEM"
            ],
            "target_column": "stageDbId",
            "target_value":"stageCode",
            "api_resource_method": "POST",
            "api_resource_endpoint": "stages-search",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=stageCode",
            "variable_type" : "identification",
            "variable_abbrev": "STAGE"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "seasonDbId",
            "target_value":"seasonCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "seasons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=seasonCode",
            "variable_type" : "identification",
            "variable_abbrev": "SEASON"
        },
        {
            "disabled": false,
            "required": "required",
            "target_column": "stewardDbId",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
          "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_STEWARD"
        },
        {
            "disabled": false,
            "target_column": "pipelineDbId",
            "target_value":"pipelineCode",
            "api_resource_method" : "GET",
            "api_resource_endpoint" : "pipelines",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=pipelineCode",
            "variable_type" : "identification",
            "variable_abbrev": "PIPELINE"
        },
        {   
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_OBJECTIVE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "PLANTING_SEASON"
        },
        {
            "disabled": false,
            "allow_new_val": true,
            "target_column": "projectDbId",
            "target_value": "projectCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "projects",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=projectCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROJECT"
        },
        {
            "disabled": false,
            "target_value":"value",
            "secondary_target_column":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "allowed_values": [
                "Seed Increase", 
               "Selection and Advancement", 
               "Rapid Generation Advancement"
            ],
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_TYPE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
        },
        {
            "disabled": false,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type": "identification",
            "variable_abbrev": "DESCRIPTION"
        }
    ]
  }'::json 
WHERE 
    abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = '{
--rollback       "Name": "Required experiment level metadata variables for Generation Nursery data process",
--rollback       "Values": [
--rollback         {
--rollback             "default": false,
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_TYPE"
--rollback         },
--rollback         {
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "target_column": "cropDbId",
--rollback             "target_value" : "cropCode",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "crops",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=cropCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "CROP"
--rollback         },
--rollback         {
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "target_column": "programDbId",
--rollback             "target_value":"programCode",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "programs",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=programCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "PROGRAM"
--rollback         },
--rollback     {
--rollback             "default" : "EXPT-XXXX",
--rollback             "disabled": true,
--rollback             "required": "required",
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_CODE"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_NAME"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "allowed_values": [
--rollback                 "BRE",
--rollback                 "F1",
--rollback                 "F2",
--rollback                 "F3",
--rollback                 "F4",
--rollback                 "F5",
--rollback                 "F6",
--rollback                 "F7",
--rollback                 "F8",
--rollback                 "F9",
--rollback                 "RGA",
--rollback                 "SEM"
--rollback             ],
--rollback             "target_column": "stageDbId",
--rollback             "target_value":"stageCode",
--rollback             "api_resource_method": "POST",
--rollback             "api_resource_endpoint": "stages-search",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=stageCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "STAGE"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_YEAR"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "target_column": "seasonDbId",
--rollback             "target_value":"seasonCode",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "seasons",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=seasonCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "SEASON"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "required": "required",
--rollback             "target_column": "stewardDbId",
--rollback             "secondary_target_column": "personDbId",
--rollback             "target_value":"personName",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "persons",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=personName",
--rollback           "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_STEWARD"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "target_column": "pipelineDbId",
--rollback             "target_value":"pipelineCode",
--rollback             "api_resource_method" : "GET",
--rollback             "api_resource_endpoint" : "pipelines",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=pipelineCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "PIPELINE"
--rollback         },
--rollback         {   
--rollback             "disabled": false,
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_OBJECTIVE"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "PLANTING_SEASON"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "allow_new_val": true,
--rollback             "target_column": "projectDbId",
--rollback             "target_value": "projectCode",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "projects",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "sort=projectCode",
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "PROJECT"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "allowed_values": [
--rollback                "Seed Increase", 
--rollback                "Selection and Advancement", 
--rollback                "Rapid Generation Advancement"
--rollback             ],
--rollback             "variable_abbrev": "EXPERIMENT_SUB_TYPE"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type" : "identification",
--rollback             "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
--rollback         },
--rollback         {
--rollback             "disabled": false,
--rollback             "target_column": "",
--rollback             "secondary_target_column":"",
--rollback             "target_value":"",
--rollback             "api_resource_method" : "",
--rollback             "api_resource_endpoint" : "",
--rollback             "api_resource_filter" : "",
--rollback             "api_resource_sort": "", 
--rollback             "variable_type": "identification",
--rollback             "variable_abbrev": "DESCRIPTION"
--rollback         }
--rollback     ]
--rollback   }'::json 
--rollback WHERE 
--rollback     abbrev = 'GENERATION_NURSERY_BASIC_INFO_ACT_VAL';