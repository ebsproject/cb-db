--liquibase formatted sql

--changeset postgres:update_scale_values_hv_meth_disc context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1240 Update scale values to HV_METH_DISC



DELETE FROM 
    master.scale_value 
WHERE
    abbrev 
IN
    (
        'HV_METH_DISC_MODIFIED_BULK',
        'HV_METH_DISC_SELECTED_BULK',
        'HV_METH_DISC_INDIVIDUAL_PLANT',
        'HV_METH_DISC_INDIVIDUAL_SPIKE_EAR_PANICLE',
        'HV_METH_DISC_HEAD_ROWS',
        'HV_METH_DISC_HEAD_ROW_PURIFICATION',
        'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
    );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Modified bulk',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Modified bulk',
            'Modified bulk',
            'HV_METH_DISC_MODIFIED_BULK'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Selected bulk',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Selected bulk',
            'Selected bulk',
            'HV_METH_DISC_SELECTED_BULK'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual plant',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual plant',
            'Individual plant',
            'HV_METH_DISC_INDIVIDUAL_PLANT'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual spike',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual spike',
            'Individual spike',
            'HV_METH_DISC_INDIVIDUAL_SPIKE'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual ear',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual ear',
            'Individual ear',
            'HV_METH_DISC_INDIVIDUAL_EAR'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Individual panicle',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Individual panicle',
            'Individual panicle',
            'HV_METH_DISC_INDIVIDUAL_PANICLE'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Head-rows',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Head-rows',
            'Head-rows',
            'HV_METH_DISC_HEAD_ROWS'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Head-row purification',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Head-row purification',
            'Head-row purification',
            'HV_METH_DISC_HEAD_ROW_PURIFICATION'
        );

INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
        (
            (SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC'),
            'Single seed numbering',
            (SELECT MAX(order_number)+1 FROM master.scale_value WHERE scale_id=(SELECT scale_id FROM master.variable WHERE abbrev='HV_METH_DISC')),
            'Single seed numbering',
            'Single seed numbering',
            'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
        );



--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'HV_METH_DISC_MODIFIED_BULK',
--rollback         'HV_METH_DISC_SELECTED_BULK',
--rollback         'HV_METH_DISC_INDIVIDUAL_PLANT',
--rollback         'HV_METH_DISC_INDIVIDUAL_SPIKE',
--rollback         'HV_METH_DISC_INDIVIDUAL_EAR',
--rollback         'HV_METH_DISC_INDIVIDUAL_PANICLE',
--rollback         'HV_METH_DISC_HEAD_ROWS',
--rollback         'HV_METH_DISC_HEAD_ROW_PURIFICATION',
--rollback         'HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING'
--rollback     );