--liquibase formatted sql

--changeset postgres:rename_platform_to_line_scale_value_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1220 Rename platform to line scale value in master.scale_value



UPDATE
    master.scale_value
SET
    value = 'Line',
    description = 'Line',
    display_name = 'Line',
    abbrev = 'PARENT_TYPE_LINE'
WHERE
    abbrev = 'HYBRID_ROLE_PLATFORM';

UPDATE
    master.scale_value
SET
    abbrev = 'PARENT_TYPE_TESTER'
WHERE
    abbrev = 'HYBRID_ROLE_TESTER';



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Platform',
--rollback     description = 'Platform',
--rollback     display_name = 'Platform',
--rollback     abbrev = 'HYBRID_ROLE_PLATFORM'
--rollback WHERE
--rollback     abbrev = 'PARENT_TYPE_LINE';

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     abbrev = 'HYBRID_ROLE_TESTER'
--rollback WHERE
--rollback     abbrev = 'PARENT_TYPE_TESTER';