--liquibase formatted sql

--changeset postgres:update_config_cross_parent_nursery_phase_ii_basic_info_act_val_in_platform.config_p4 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT_VAL p4



UPDATE 
    platform.config 
SET 
    config_value = '{
    "Name": "Required experiment level metadata variables for Cross Parent Nursery Phase I data process",
    "Values": [
        {
            "default": false,
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_TYPE"
        },
        {
            "default": "RICE",
            "disabled": true,
            "required": "required",
            "target_column": "cropDbId",
            "target_value" : "cropCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "crops",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=cropCode",
            "variable_type" : "identification",
            "variable_abbrev": "CROP"
        },
        {
            "disabled": true,
            "required": "required",
            "target_column": "programDbId",
            "target_value":"programCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "programs",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=programCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROGRAM"
        },
        {
            "default" : "EXPT-XXXX",
            "disabled": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_CODE"
        },
        {
            "disabled": true,
            "include_form": true,  
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_NAME"
        },
        {
            "default": "HB",
            "include_form": true,
            "disabled": true,
            "required": "required",
            "allowed_values": [
                "HB",
                "F1"
            ],
            "target_column": "stageDbId",
            "target_value":"stageCode",
            "api_resource_method": "POST",
            "api_resource_endpoint": "stages-search",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=stageCode",
            "variable_type" : "identification",
            "variable_abbrev": "STAGE"
        },
        {
            "disabled": true,
            "include_form": true,
            "required": "required",
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "disabled": true,
            "include_form": true,
            "required": "required",
            "target_column": "seasonDbId",
            "target_value":"seasonCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "seasons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=seasonCode",
            "variable_type" : "identification",
            "variable_abbrev": "SEASON"
        },
        {
            "disabled": true,
            "include_form": true,
            "required": "required",
            "target_column": "stewardDbId",
            "secondary_target_column": "personDbId",
            "target_value":"personName",
            "api_resource_method": "GET",
            "api_resource_endpoint": "persons",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=personName",
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_STEWARD"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_column": "pipelineDbId",
            "target_value":"pipelineCode",
            "api_resource_method" : "GET",
            "api_resource_endpoint" : "pipelines",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=pipelineCode",
            "variable_type" : "identification",
            "variable_abbrev": "PIPELINE"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_OBJECTIVE"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "PLANTING_SEASON"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_column": "projectDbId",
            "target_value": "projectCode",
            "api_resource_method": "GET",
            "api_resource_endpoint": "projects",
            "api_resource_filter" : "",
            "api_resource_sort": "sort=projectCode",
            "variable_type" : "identification",
            "variable_abbrev": "PROJECT"
        },
        {
            "disabled": true,
            "default": "Breeding Crosses",
            "include_form": true,
            "target_value":"value",
            "variable_type" : "identification",
            "allowed_values": [
                "Breeding Crosses"
            ],
            "variable_abbrev": "EXPERIMENT_SUB_TYPE"
        },
        {
            "default": "Crossing Block",
            "disabled": true,
            "include_form": true,
            "allowed_values": [
                "Crossing Block"
            ],
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type" : "identification",
            "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_column": "",
            "secondary_target_column":"",
            "target_value":"",
            "api_resource_method" : "",
            "api_resource_endpoint" : "",
            "api_resource_filter" : "",
            "api_resource_sort": "", 
            "variable_type": "identification",
            "variable_abbrev": "DESCRIPTION"
        }
    ]
  }'
WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level metadata variables for Cross Parent Nursery Phase I data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": "RICE",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "cropCode",
--rollback                     "target_column": "cropDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "CROP",
--rollback                     "api_resource_sort": "sort=cropCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "crops"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "programCode",
--rollback                     "target_column": "programDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROGRAM",
--rollback                     "api_resource_sort": "sort=programCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "programs"
--rollback                 },
--rollback                 {
--rollback                     "default": "EXPT-XXXX",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_CODE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "default": "HB",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "stageCode",
--rollback                     "target_column": "stageDbId",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "HB",
--rollback                         "F1"
--rollback                     ],
--rollback                     "variable_abbrev": "STAGE",
--rollback                     "api_resource_sort": "sort=stageCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "POST",
--rollback                     "api_resource_endpoint": "stages-search"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_YEAR",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "seasonCode",
--rollback                     "target_column": "seasonDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "SEASON",
--rollback                     "api_resource_sort": "sort=seasonCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "seasons"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "include_form": true,
--rollback                     "target_value": "personName",
--rollback                     "target_column": "stewardDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_STEWARD",
--rollback                     "api_resource_sort": "sort=personName",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "persons",
--rollback                     "secondary_target_column": "personDbId"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "pipelineCode",
--rollback                     "target_column": "pipelineDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PIPELINE",
--rollback                     "api_resource_sort": "sort=pipelineCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "pipelines"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "EXPERIMENT_OBJECTIVE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PLANTING_SEASON",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "projectCode",
--rollback                     "allow_new_val": true,
--rollback                     "target_column": "projectDbId",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "PROJECT",
--rollback                     "api_resource_sort": "sort=projectCode",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "GET",
--rollback                     "api_resource_endpoint": "projects"
--rollback                 },
--rollback                 {
--rollback                     "default": "Breeding Crosses",
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "value",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "Breeding Crosses"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_TYPE"
--rollback                 },
--rollback                 {
--rollback                     "default": "Crossing Block",
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "Crossing Block"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "include_form": true,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT_VAL';