--liquibase formatted sql

--changeset postgres:add_config_generation_nursery_entry_list_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config GENERATION_NURSERY_ENTRY_LIST_ACT_VAL to platform.config



INSERT INTO 
    platform.config
    (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'GENERATION_NURSERY_ENTRY_LIST_ACT_VAL',
        'Generation Nursery Experiment Entry List',
        '{
            "Name": "Required and default entry level metadata variables for Generation Nursery data process",
            "Values": [{
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                }]
            }'::json, 
            1, 
            'experiment_creation', 
            1,
            'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'GENERATION_NURSERY_ENTRY_LIST_ACT_VAL';