--liquibase formatted sql

--changeset postgres:add_cross_parent_nursery_mod_to_platform.item_module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add cross parent nursery mod to platform.item_module



INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_MOD')
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_MOD')
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT_MOD')
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT_MOD')
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD')
        when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT_MOD')
        ELSE (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT','CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT', 'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT','CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT', 'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT','CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT');

INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT_MOD')
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT_MOD')
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT_MOD')
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT','CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT','CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT','CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT');

INSERT INTO 
    platform.item_module(item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
CASE
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_MOD')
  when abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT_MOD')
  ELSE (SELECT id FROM platform.module WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT','CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT_MOD', 
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT'
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT_MOD'
--rollback 	    )
--rollback   ));

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT',
--rollback        'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT'
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   ));