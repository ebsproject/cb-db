--liquibase formatted sql

--changeset postgres:update_api_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1570 Update api messages



UPDATE api.messages SET message='Required parameters are missing. Ensure that the code field is not empty.' WHERE code=400001;
UPDATE api.messages SET message='You have provided an invalid authorization code.' WHERE code=400002;
UPDATE api.messages SET message='The authentication process was not completed successfully.' WHERE code=401001;
UPDATE api.messages SET message='Failed to successfully validate the token.' WHERE code=401016;

INSERT INTO 
    api.messages 
        (http_status_code, code, type, message, url) 
VALUES
    (401, 401024, 'ERR', 'Unauthorized request. You do not have permissions to perform this operation.', '/responses.html#401024');



--rollback DELETE FROM api.messages WHERE http_status_code=401 AND code=401024;
--rollback UPDATE api.messages SET message='You have provided an invalid value for client_id or redirect uri.' WHERE code=400001;
--rollback UPDATE api.messages SET message='You have provided an invalid email or code while requesting for an authorization token.' WHERE code=400002;
--rollback UPDATE api.messages SET message='The authentication process was not completed successfully. The email/code that you have provided was not found.' WHERE code=401001;
--rollback UPDATE api.messages SET message='Unauthorized request. You do not have permissions to perform this operation.' WHERE code=401016;