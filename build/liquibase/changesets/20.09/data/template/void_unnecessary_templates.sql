--liquibase formatted sql

--changeset postgres:void_unnecessary_templates context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Void unnecessary templates



UPDATE 
    master.item 
SET 
    is_void = true 
WHERE 
    abbrev
IN
    (
        'EXPT_CROSS_POST_PLANNING_DATA_PROCESS', 
        'EXPT_CROSS_PRE_PLANNING_DATA_PROCESS',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS',
        'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS',
        'EXPT_CROSS_PARENT_DATA_PROCESS',
        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS',
        'EXPT_TRIAL_IRRI_DATA_PROCESS',
        'EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS',
        'EXPT_SEED_INCREASE_DATA_PROCESS',
        'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS',
        'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS',
        'EXPT_NURSERY_CB_DATA_PROCESS',
        'EXPT_TRIAL_DATA_PROCESS'
    );



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = false WHERE abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPT_CROSS_POST_PLANNING_DATA_PROCESS', 
--rollback         'EXPT_CROSS_PRE_PLANNING_DATA_PROCESS',
--rollback         'EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS',
--rollback         'EXPT_SEED_INCREASE_IRRI_DATA_PROCESS',
--rollback         'EXPT_CROSS_PARENT_DATA_PROCESS',
--rollback         'EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS',
--rollback         'EXPT_TRIAL_IRRI_DATA_PROCESS',
--rollback         'EXPT_SELECTION_ADVANCEMENT_DATA_PROCESS',
--rollback         'EXPT_SEED_INCREASE_DATA_PROCESS',
--rollback         'EXPT_NURSERY_CROSS_LIST_DATA_PROCESS',
--rollback         'EXPT_NURSERY_PARENT_LIST_DATA_PROCESS',
--rollback         'EXPT_NURSERY_CB_DATA_PROCESS',
--rollback         'EXPT_TRIAL_DATA_PROCESS'
--rollback     );