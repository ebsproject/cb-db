--liquibase formatted sql

--changeset postgres:create_experiment_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Create experiment_code sequence



-- create experiment_code sequence
CREATE SEQUENCE experiment.experiment_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE experiment.experiment_code_seq



--changeset postgres:create_entry_list_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Create entry_list_code sequence



-- create experiment_code sequence
CREATE SEQUENCE experiment.entry_list_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE experiment.entry_list_code_seq



--changeset postgres:create_experiment_code_generator_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Create function to generate experiment codes



-- create experiment_code generator
CREATE OR REPLACE FUNCTION experiment.generate_code(IN entity varchar, IN custom_value varchar DEFAULT NULL, OUT code varchar)
    RETURNS varchar
    LANGUAGE plpgsql
AS $$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('experiment.' || lower(entity) || '_code_seq') INTO seq_id;
    
    IF (upper(entity) = 'EXPERIMENT') THEN
        entity_code := 'EXPT';
    ELSIF (upper(entity) = 'ENTRY_LIST') THEN
        entity_code := 'ENTLIST';
    ELSIF (upper(entity) = 'LOCATION') THEN
        entity_code := 'LOC';
    ELSIF (upper(entity) = 'OCCURRENCE') THEN
        entity_code := 'OCC';
    END IF;
    
    IF (entity_code IS NOT NULL) THEN
        IF (custom_value IS NULL) THEN
            code := entity_code || lpad(seq_id::varchar, 8, '0');
        ELSE
            code := entity_code || custom_value || lpad(seq_id::varchar, 8, '0');
        END IF;
    ELSE
        code := NULL;
    END IF;
END; $$
;



-- revert changes
--rollback DROP FUNCTION experiment.generate_code(IN varchar, IN varchar, OUT varchar);



--changeset postgres:create_occurrence_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Create occurrence_code sequence



-- create occurrence_code sequence
CREATE SEQUENCE experiment.occurrence_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE experiment.occurrence_code_seq



--changeset postgres:create_location_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Create location_code sequence



-- create location_code sequence
CREATE SEQUENCE experiment.location_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE experiment.location_code_seq
