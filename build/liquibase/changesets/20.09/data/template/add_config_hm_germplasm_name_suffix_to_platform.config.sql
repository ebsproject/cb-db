--liquibase formatted sql

--changeset postgres:add_config_hm_germplasm_name_suffix_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config HM_GERMPLASM_NAME_SUFFIX to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_GERMPLASM_NAME_SUFFIX',
        'Germplasm Name Suffix',
    $$			
    {
    "rice": {
        "plotInfoField": ""
    },
    "maize": {
        "plotInfoField": ""
    },

    "wheat": {
        "plotInfoField": "nurserySiteCode"
    }
    }
    $$,
        1,
        'harvest_manager',
        1,
        'B4R 6725 update config - a.caneda 2020-09-07');



--rollback DELETE FROM platform.config WHERE abbrev = 'HM_GERMPLASM_NAME_SUFFIX';