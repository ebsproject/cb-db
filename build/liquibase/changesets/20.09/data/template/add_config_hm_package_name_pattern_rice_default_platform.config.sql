--liquibase formatted sql

--changeset postgres:add_config_hm_package_name_pattern_rice_default_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add config HM_PACKAGE_NAME_PATTERN_RICE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_PACKAGE_NAME_PATTERN_RICE_DEFAULT',
        'Harvest Manager RICE Package Name Pattern-Default',
    $$			
		{
    "pattern": [
      {
        "type": "field",
        "order_number": 0,
        "label":"Seed Name",
        "seedInfoField":"seedName"
      },
		  {
        "type": "delimeter",
        "order_number": 1,
        "value": "-"     
      },
		  {
        "type": "free-text",
        "order_number": 2,
        "value":"PK001"
        
      }
    ]
    }
    $$,
        1,
        'harvest_manager',
        1,
        'B4R 6725 update config - a.caneda 2020-09-07')



--rollback DELETE FROM platform.config WHERE abbrev='HM_PACKAGE_NAME_PATTERN_RICE_DEFAULT';