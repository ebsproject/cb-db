--liquibase formatted sql

--changeset postgres:add_cross_parent_nursery_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add cross parent nursery to platform.module



INSERT INTO 
  platform.module(abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
VALUES 
  ('CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_MOD','Specify Parent List','Specify Parent List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT_MOD','Specify Design','Specify Design','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
  ('CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT_MOD','Add Blocks','Add Blocks','experimentCreation','create','add-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT_MOD','Assign Entries','Assign Entries','experimentCreation','create','assign-entries',1,'added by j.antonio ' || now(), 'design generated'),
  ('CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT_MOD','Manage Blocks','Manage Blocks','experimentCreation','create','manage-blocks',1,'added by j.antonio ' || now(), 'design generated'),
  ('CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT_MOD','Overview','Overview','experimentCreation','create','overview',1,'added by j.antonio ' || now(), 'design generated'),
  ('CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'oiccurrences created'),
  ('CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');



--rollback DELETE FROM
--rollback     platform.module
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_BASIC_INFO_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ENTRY_LIST_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_DESIGN_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ADD_BLOCKS_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_MANAGE_BLOCKS_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_OVERVIEW_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_PLACE_ACT_MOD',
--rollback         'CROSS_PARENT_NURSERY_PHASE_I_REVIEW_ACT_MOD'
--rollback     );