--liquibase formatted sql

--changeset postgres:update_usage_of_planting_arrangement_variable_set context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update usage of planting arrangement variable set



UPDATE 
    master.variable_set 
SET 
    usage = 'application' 
WHERE 
    abbrev = 'PLANTING_ARRANGEMENT';



--rollback UPDATE 
--rollback     master.variable_set 
--rollback SET 
--rollback     usage = NULL 
--rollback WHERE 
--rollback     abbrev = 'PLANTING_ARRANGEMENT';