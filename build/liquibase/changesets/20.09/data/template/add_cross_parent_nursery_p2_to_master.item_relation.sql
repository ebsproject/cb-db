--liquibase formatted sql

--changeset postgres:add_cross_parent_nursery_p2_to_master.item_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add cross parent nursery p2 to master.item_relation



INSERT INTO 
    master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
  (SELECT id FROM master.item WHERE abbrev='CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') AS root_id,
  (SELECT id FROM master.item WHERE abbrev='CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') AS parent_id,
   id AS child_id,
   CASE 
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT' THEN 1
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT' THEN 2
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT' THEN 3
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DESIGN_ACT' THEN 4
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT' THEN 5
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT' THEN 6
     WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_REVIEW_ACT' THEN 7
     ELSE 8 END AS order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT','CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT','CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT','CROSS_PARENT_NURSERY_PHASE_II_DESIGN_ACT','CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT','CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT','CROSS_PARENT_NURSERY_PHASE_II_REVIEW_ACT');

INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT') AS parent_id,
    id AS child_id,
  CASE
        WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MATRIX_ACT' THEN 1
        WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT'THEN 2
        ELSE 3 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_II_MATRIX_ACT','CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT');

INSERT INTO 
    master.item_relation (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT') AS parent_id,
    id AS child_id,
  CASE
        WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT' THEN 1
        WHEN abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT'THEN 2
        ELSE 3 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT','CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT');



--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_BASIC_INFO_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_ENTRY_LIST_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_DESIGN_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_PLACE_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_REVIEW_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_CROSSES_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_MATRIX_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_MANAGE_ACT'
--rollback             )
--rollback     );

--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'CROSS_PARENT_NURSERY_PHASE_II_PROTOCOL_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_PLANTING_PROTOCOL_ACT',
--rollback                  'CROSS_PARENT_NURSERY_PHASE_II_TRAITS_PROTOCOL_ACT'
--rollback             )
--rollback     );