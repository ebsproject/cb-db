--liquibase formatted sql

--changeset postgres:add_variable_set_geospatial_object_info context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1318 Add variable set GEOSPATIAL_OBJECT_INFO



INSERT INTO
			master.variable_set (abbrev,name,creator_id) 
		VALUES 
			('GEOSPATIAL_OBJECT_INFO','Geospatial Object Info',1);



--rollback DELETE FROM master.variable_set WHERE abbrev='GEOSPATIAL_OBJECT_INFO';
        