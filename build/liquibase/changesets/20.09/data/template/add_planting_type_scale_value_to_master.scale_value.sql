--liquibase formatted sql

--changeset postgres:add_planting_type_scale_value_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1239 Add planting type scale value to master.scale_value



INSERT INTO
    master.scale_value
        (scale_id,value,order_number,description,creator_id,scale_value_status,abbrev)
VALUES
    (
        (SELECT scale_id FROM master.variable WHERE abbrev = 'PLANTING_TYPE'),
        'Melgas',
        (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'PLANTING_TYPE')),
        'Melgas',
        1,
        'show',
        'PLANTING_TYPE_MELGAS'
    );



--rollback DELETE FROM master.scale_value WHERE abbrev = 'PLANTING_TYPE_MELGAS' AND scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'PLANTING_TYPE');