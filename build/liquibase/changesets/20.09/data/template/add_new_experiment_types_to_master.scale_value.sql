--liquibase formatted sql

--changeset postgres:add_new_experiment_types_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Add new experiment types to master.scale_value



--void the previously created experiment type scale values
UPDATE 
    master.scale_value 
SET 
    (is_void, value) = (true, 'VOIDED-'||value) 
WHERE 
    abbrev 
IN 
    (
        'EXPERIMENT_TYPE_TRIAL', 
        'EXPERIMENT_TYPE_NURSERY'
    );

--add the new Experiment Types
INSERT INTO 
    master.scale_value 
        (scale_id, order_number, value, description, display_name, abbrev) 
VALUES 
    ((SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_TYPE'),1,'Breeding Trial','Breeding Trial', 'Breeding Trial','EXPERIMENT_TYPE_BREEDING_TRIAL'),
    ((SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_TYPE'),2,'Cross Parent Nursery','Cross Parent Nursery', 'Cross Parent Nursery','EXPERIMENT_TYPE_CROSS_PARENT_NURSERY'),
    ((SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_TYPE'),3,'Generation Nursery','Generation Nursery', 'Generation Nursery','EXPERIMENT_TYPE_GENERATION_NURSERY'),
    ((SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_TYPE'),4,'Intentional Crossing Nursery','Intentional Crossing Nursery', 'Intentional Crossing Nursery','EXPERIMENT_TYPE_INTENTIONAL_CROSSING_NURSERY');



--rollback UPDATE 
--rollback     master.scale_value 
--rollback SET 
--rollback     (is_void, value) = (false, display_name) 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPERIMENT_TYPE_TRIAL', 
--rollback         'EXPERIMENT_TYPE_NURSERY'
--rollback     );

--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE 
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'EXPERIMENT_TYPE_BREEDING_TRIAL',
--rollback         'EXPERIMENT_TYPE_CROSS_PARENT_NURSERY',
--rollback         'EXPERIMENT_TYPE_GENERATION_NURSERY',
--rollback         'EXPERIMENT_TYPE_INTENTIONAL_CROSSING_NURSERY'   
--rollback     );