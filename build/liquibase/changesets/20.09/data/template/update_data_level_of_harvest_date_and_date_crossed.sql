--liquibase formatted sql

--changeset postgres:update_data_level_of_harvest_date_and_date_crossed context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1591 Update data level of harvest date and date crossed



UPDATE
    master.variable 
SET 
    data_level = 'plot'
WHERE
    abbrev 
IN
    (
        'DATE_CROSSED',
        'HARVEST_DATE'
    );



--rollback UPDATE
--rollback     master.variable 
--rollback SET 
--rollback     data_level = 'entry'
--rollback WHERE
--rollback     abbrev = 'DATE_CROSSED';
--rollback 
--rollback UPDATE
--rollback     master.variable 
--rollback SET 
--rollback     data_level = NULL
--rollback WHERE
--rollback     abbrev = 'HARVEST_DATE';