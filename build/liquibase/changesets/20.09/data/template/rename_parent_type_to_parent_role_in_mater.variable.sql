--liquibase formatted sql

--changeset postgres:rename_parent_type_to_parent_role_in_mater.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1220 Rename PARENT_TYPE to PARENT_ROLE in master.variable



UPDATE 
    master.variable
SET
    abbrev = 'PARENT_ROLE',
    display_name = 'Parent Role'
WHERE
    abbrev = 'PARENT_TYPE';



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'PARENT_TYPE',
--rollback     display_name = 'Parent Type'
--rollback WHERE
--rollback     abbrev = 'PARENT_ROLE';