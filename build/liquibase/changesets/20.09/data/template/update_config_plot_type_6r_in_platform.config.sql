--liquibase formatted sql

--changeset postgres:update_config_plot_type_6r_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1281 Update config PLOT_TYPE_6R



UPDATE 
    platform.config 
SET 
    config_value = '{
    "Name": "Required experiment level protocol plot type variables",
    "Values": [{
            "default": 6,
            "disabled": true,
            "required": "required",
            "field_label": "Rows per plot",
            "order_number": 1,
            "variable_abbrev": "ROWS_PER_PLOT_CONT",
            "field_description": "Number of rows per plot"
        },
        {
            "unit": "cm",
            "default": 20,
            "disabled": true,
            "required": "required",
            "field_label": "Dist. bet. rows",
            "order_number": 2,
            "variable_abbrev": "DIST_BET_ROWS",
            "field_description": "Distance between rows"
        },
        {
            "unit": "m",
            "default": 1.2,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Width",
            "order_number": 3,
            "variable_abbrev": "PLOT_WIDTH",
            "field_description": "Plot Width"
        },
        {
            "unit": "m",
            "default": false,
            "computed": "computed",
            "disabled": false,
            "required": "required",
            "field_label": "Plot Length",
            "order_number": 3,
            "variable_abbrev": "PLOT_LN",
            "field_description": "Plot Length"
        },
        {
            "unit": "sqm",
            "default": false,
            "computed": "computed",
            "disabled": true,
            "required": "required",
            "field_label": "Plot Area",
            "order_number": 4,
            "variable_abbrev": "PLOT_AREA_2",
            "field_description": "Plot Area"
        },
        {
            "unit": "m",
            "allow_new_val": true,
            "default": false,
            "disabled": false,
            "required": "required",
            "field_label": "Seeding Rate",
            "order_number": 5,
            "allow_new_val": true,
            "variable_abbrev": "SEEDING_RATE",
            "field_description": "Seeding Rate"
        }
    ]
}'
WHERE abbrev = 'PLOT_TYPE_6R';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level protocol plot type variables",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": 6,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Rows per plot",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "ROWS_PER_PLOT_CONT",
--rollback                     "field_description": "Number of rows per plot"
--rollback                 },
--rollback                 {
--rollback                     "unit": "cm",
--rollback                     "default": 20,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Dist. bet. rows",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "DIST_BET_ROWS",
--rollback                     "field_description": "Distance between rows"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": 1.2,
--rollback                     "computed": "computed",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Plot Width",
--rollback                     "order_number": 3,
--rollback                     "variable_abbrev": "PLOT_WIDTH",
--rollback                     "field_description": "Plot Width"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Plot Length",
--rollback                     "order_number": 3,
--rollback                     "variable_abbrev": "PLOT_LN",
--rollback                     "field_description": "Plot Length"
--rollback                 },
--rollback                 {
--rollback                     "unit": "sqm",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Plot Area",
--rollback                     "order_number": 4,
--rollback                     "variable_abbrev": "PLOT_AREA_2",
--rollback                     "field_description": "Plot Area"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Seeding Rate",
--rollback                     "order_number": 5,
--rollback                     "variable_abbrev": "SEEDING_RATE",
--rollback                     "field_description": "Seeding Rate"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'PLOT_TYPE_6R';