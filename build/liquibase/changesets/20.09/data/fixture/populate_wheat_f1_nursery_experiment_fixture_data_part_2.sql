--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_experiment.experiment_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.experiment



-- populate wheat experiment F1SIMPLEBW-2
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F1') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2014 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2014A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'F1SIMPLEBW-2' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'F1SIMPLEBW-2'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry_list



-- populate wheat entry list F1SIMPLEBW-2_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'F1SIMPLEBW-2_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'F1SIMPLEBW-2') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry



-- populate wheat entries for F1SIMPLEBW-2
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS14Y00151S','Y13-14CBBW95'),
        (2,'CMSS14Y00152S','Y13-14CBBW95'),
        (3,'CMSS14Y00153S','Y13-14CBBW95'),
        (4,'CMSS14Y00154S','Y13-14CBBW96'),
        (5,'CMSS14Y00155S','Y13-14CBBW98'),
        (6,'CMSS14Y00156S','Y13-14CBBW99'),
        (7,'CMSS14Y00157S','Y13-14CBBW99'),
        (8,'CMSS14Y00158S','Y13-14CBBW99'),
        (9,'CMSS14Y00159S','Y13-14CBBW101'),
        (10,'CMSS14Y00160S','Y13-14CBBW101'),
        (11,'CMSS14Y00161S','Y13-14CBBW102'),
        (12,'CMSS14Y00162S','Y13-14CBBW102'),
        (13,'CMSS14Y00163S','Y13-14CBBW103'),
        (14,'CMSS14Y00164S','Y13-14CBBW103'),
        (15,'CMSS14Y00165S','Y13-14CBBW106'),
        (16,'CMSS14Y00166S','Y13-14CBBW106'),
        (17,'CMSS14Y00167S','Y13-14CBBW106'),
        (18,'CMSS14Y00168S','Y13-14CBBW106'),
        (19,'CMSS14Y00169S','Y13-14CBBW107'),
        (20,'CMSS14Y00170S','Y13-14CBBW107'),
        (21,'CMSS14Y00171S','Y13-14CBBW108'),
        (22,'CMSS14Y00172S','Y13-14CBBW108'),
        (23,'CMSS14Y00173S','Y13-14CBBW111'),
        (24,'CMSS14Y00174S','Y13-14CBBW111'),
        (25,'CMSS14Y00175S','Y13-14CBBW111'),
        (26,'CMSS14Y00176S','Y13-14CBBW113'),
        (27,'CMSS14Y00177S','Y13-14CBBW113'),
        (28,'CMSS14Y00178S','Y13-14CBBW113'),
        (29,'CMSS14Y00179S','Y13-14CBBW115'),
        (30,'CMSS14Y00180S','Y13-14CBBW115'),
        (31,'CMSS14Y00181S','Y13-14CBBW116'),
        (32,'CMSS14Y00182S','Y13-14CBBW118'),
        (33,'CMSS14Y00183S','Y13-14CBBW118'),
        (34,'CMSS14Y00184S','Y13-14CBBW129'),
        (35,'CMSS14Y00185S','Y13-14CBBW129'),
        (36,'CMSS14Y00186S','Y13-14CBBW129'),
        (37,'CMSS14Y00187S','Y13-14CBBW129'),
        (38,'CMSS14Y00188S','Y13-14CBBW129'),
        (39,'CMSS14Y00189S','Y13-14CBBW129'),
        (40,'CMSS14Y00190S','Y13-14CBBW129'),
        (41,'CMSS14Y00191S','Y13-14CBBW135'),
        (42,'CMSS14Y00192S','Y13-14CBBW135'),
        (43,'CMSS14Y00193S','Y13-14CBBW135'),
        (44,'CMSS14Y00194S','Y13-14CBBW135'),
        (45,'CMSS14Y00195S','Y13-14CBBW135'),
        (46,'CMSS14Y00196S','Y13-14CBBW135'),
        (47,'CMSS14Y00197S','Y13-14CBBW135'),
        (48,'CMSS14Y00198S','Y13-14CBBW141'),
        (49,'CMSS14Y00199S','Y13-14CBBW141'),
        (50,'CMSS14Y00200S','Y13-14CBBW141'),
        (51,'CMSS14Y00201S','Y13-14CBBW142'),
        (52,'CMSS14Y00202S','Y13-14CBBW142'),
        (53,'CMSS14Y00203S','Y13-14CBBW143'),
        (54,'CMSS14Y00204S','Y13-14CBBW143'),
        (55,'CMSS14Y00205S','Y13-14CBBW143'),
        (56,'CMSS14Y00206S','Y13-14CBBW144'),
        (57,'CMSS14Y00207S','Y13-14CBBW147'),
        (58,'CMSS14Y00208S','Y13-14CBBW147'),
        (59,'CMSS14Y00209S','Y13-14CBBW147'),
        (60,'CMSS14Y00210S','Y13-14CBBW148'),
        (61,'CMSS14Y00211S','Y13-14CBBW150'),
        (62,'CMSS14Y00212S','Y13-14CBBW150'),
        (63,'CMSS14Y00213S','Y13-14CBBW150'),
        (64,'CMSS14Y00214S','Y13-14CBBW151'),
        (65,'CMSS14Y00215S','Y13-14CBBW153'),
        (66,'CMSS14Y00216S','Y13-14CBBW153'),
        (67,'CMSS14Y00217S','Y13-14CBBW155'),
        (68,'CMSS14Y00218S','Y13-14CBBW155'),
        (69,'CMSS14Y00219S','Y13-14CBBW155'),
        (70,'CMSS14Y00220S','Y13-14CBBW156'),
        (71,'CMSS14Y00221S','Y13-14CBBW156'),
        (72,'CMSS14Y00222S','Y13-14CBBW156'),
        (73,'CMSS14Y00223S','Y13-14CBBW160'),
        (74,'CMSS14Y00224S','Y13-14CBBW161'),
        (75,'CMSS14Y00225S','Y13-14CBBW161'),
        (76,'CMSS14Y00226S','Y13-14CBBW167'),
        (77,'CMSS14Y00227S','Y13-14CBBW171'),
        (78,'CMSS14Y00228S','Y13-14CBBW171'),
        (79,'CMSS14Y00229S','Y13-14CBBW171'),
        (80,'CMSS14Y00230S','Y13-14CBBW173'),
        (81,'CMSS14Y00231S','Y13-14CBBW173'),
        (82,'CMSS14Y00232S','Y13-14CBBW174'),
        (83,'CMSS14Y00233S','Y13-14CBBW174'),
        (84,'CMSS14Y00234S','Y13-14CBBW174'),
        (85,'CMSS14Y00235S','Y13-14CBBW174'),
        (86,'CMSS14Y00236S','Y13-14CBBW174'),
        (87,'CMSS14Y00237S','Y13-14CBBW175'),
        (88,'CMSS14Y00238S','Y13-14CBBW175'),
        (89,'CMSS14Y00239S','Y13-14CBBW177'),
        (90,'CMSS14Y00240S','Y13-14CBBW177'),
        (91,'CMSS14Y00241S','Y13-14CBBW178'),
        (92,'CMSS14Y00242S','Y13-14CBBW178'),
        (93,'CMSS14Y00243S','Y13-14CBBW179'),
        (94,'CMSS14Y00244S','Y13-14CBBW179'),
        (95,'CMSS14Y00245S','Y13-14CBBW179'),
        (96,'CMSS14Y00246S','Y13-14CBBW180'),
        (97,'CMSS14Y00247S','Y13-14CBBW180'),
        (98,'CMSS14Y00248S','Y13-14CBBW180'),
        (99,'CMSS14Y00249S','Y13-14CBBW181'),
        (100,'CMSS14Y00250S','Y13-14CBBW181'),
        (101,'CMSS14Y00251S','Y13-14CBBW182'),
        (102,'CMSS14Y00252S','Y13-14CBBW182'),
        (103,'CMSS14Y00253S','Y13-14CBBW184'),
        (104,'CMSS14Y00254S','Y13-14CBBW184'),
        (105,'CMSS14Y00255S','Y13-14CBBW184'),
        (106,'CMSS14Y00256S','Y13-14CBBW185'),
        (107,'CMSS14Y00257S','Y13-14CBBW186'),
        (108,'CMSS14Y00258S','Y13-14CBBW186'),
        (109,'CMSS14Y00259S','Y13-14CBBW187'),
        (110,'CMSS14Y00260S','Y13-14CBBW187'),
        (111,'CMSS14Y00261S','Y13-14CBBW187'),
        (112,'CMSS14Y00262S','Y13-14CBBW188'),
        (113,'CMSS14Y00263S','Y13-14CBBW188'),
        (114,'CMSS14Y00264S','Y13-14CBBW189'),
        (115,'CMSS14Y00265S','Y13-14CBBW189'),
        (116,'CMSS14Y00266S','Y13-14CBBW189'),
        (117,'CMSS14Y00267S','Y13-14CBBW190'),
        (118,'CMSS14Y00268S','Y13-14CBBW190'),
        (119,'CMSS14Y00269S','Y13-14CBBW192'),
        (120,'CMSS14Y00270S','Y13-14CBBW192'),
        (121,'CMSS14Y00271S','Y13-14CBBW195'),
        (122,'CMSS14Y00272S','Y13-14CBBW196'),
        (123,'CMSS14Y00273S','Y13-14CBBW196'),
        (124,'CMSS14Y00274S','Y13-14CBBW199'),
        (125,'CMSS14Y00275S','Y13-14CBBW199'),
        (126,'CMSS14Y00276S','Y13-14CBBW202'),
        (127,'CMSS14Y00277S','Y13-14CBBW202'),
        (128,'CMSS14Y00278S','Y13-14CBBW202'),
        (129,'CMSS14Y00279S','Y13-14CBBW203'),
        (130,'CMSS14Y00280S','Y13-14CBBW203'),
        (131,'CMSS14Y00281S','Y13-14CBBW203'),
        (132,'CMSS14Y00282S','Y13-14CBBW204'),
        (133,'CMSS14Y00283S','Y13-14CBBW204'),
        (134,'CMSS14Y00284S','Y13-14CBBW204'),
        (135,'CMSS14Y00285S','Y13-14CBBW204'),
        (136,'CMSS14Y00286S','Y13-14CBBW204'),
        (137,'CMSS14Y00287S','Y13-14CBBW204'),
        (138,'CMSS14Y00288S','Y13-14CBBW204'),
        (139,'CMSS14Y00289S','Y13-14CBBW204'),
        (140,'CMSS14Y00290S','Y13-14CBBW204'),
        (141,'CMSS14Y00291S','Y13-14CBBW204'),
        (142,'CMSS14Y00292S','Y13-14CBBW204'),
        (143,'CMSS14Y00293S','Y13-14CBBW204'),
        (144,'CMSS14Y00294S','Y13-14CBBW205'),
        (145,'CMSS14Y00295S','Y13-14CBBW206'),
        (146,'CMSS14Y00296S','Y13-14CBBW206'),
        (147,'CMSS14Y00297S','Y13-14CBBW206'),
        (148,'CMSS14Y00298S','Y13-14CBBW207'),
        (149,'CMSS14Y00299S','Y13-14CBBW207'),
        (150,'CMSS14Y00300S','Y13-14CBBW207')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.occurrence



-- populate wheat occurrence for F1SIMPLEBW-2
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'F1SIMPLEBW-2_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'F1SIMPLEBW-2'
    AND geo.geospatial_object_code = 'EB'
    AND person.username = 'a.carlson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'F1SIMPLEBW-2_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in place.geospatial_object



-- populate wheat planting area for F1SIMPLEBW-2
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'F1SIMPLEBW-2_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'F1SIMPLEBW-2_LOC1'
--rollback ;


--changeset postgres:populate_wheat_f1_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location



-- populate wheat location for F1SIMPLEBW-2
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'F1SIMPLEBW-2_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'a.carlson'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'F1SIMPLEBW-2_LOC1'
    AND site.geospatial_object_code = 'EB'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'F1SIMPLEBW-2_LOC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.plot



-- populate wheat plots for F1SIMPLEBW-2
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS14Y00151S'),
        (2,1,2,'CMSS14Y00152S'),
        (3,1,3,'CMSS14Y00153S'),
        (4,1,4,'CMSS14Y00154S'),
        (5,1,5,'CMSS14Y00155S'),
        (6,1,6,'CMSS14Y00156S'),
        (7,1,7,'CMSS14Y00157S'),
        (8,1,8,'CMSS14Y00158S'),
        (9,1,9,'CMSS14Y00159S'),
        (10,1,10,'CMSS14Y00160S'),
        (11,1,11,'CMSS14Y00161S'),
        (12,1,12,'CMSS14Y00162S'),
        (13,1,13,'CMSS14Y00163S'),
        (14,1,14,'CMSS14Y00164S'),
        (15,1,15,'CMSS14Y00165S'),
        (16,1,16,'CMSS14Y00166S'),
        (17,1,17,'CMSS14Y00167S'),
        (18,1,18,'CMSS14Y00168S'),
        (19,1,19,'CMSS14Y00169S'),
        (20,1,20,'CMSS14Y00170S'),
        (21,1,21,'CMSS14Y00171S'),
        (22,1,22,'CMSS14Y00172S'),
        (23,1,23,'CMSS14Y00173S'),
        (24,1,24,'CMSS14Y00174S'),
        (25,1,25,'CMSS14Y00175S'),
        (26,1,26,'CMSS14Y00176S'),
        (27,1,27,'CMSS14Y00177S'),
        (28,1,28,'CMSS14Y00178S'),
        (29,1,29,'CMSS14Y00179S'),
        (30,1,30,'CMSS14Y00180S'),
        (31,1,31,'CMSS14Y00181S'),
        (32,1,32,'CMSS14Y00182S'),
        (33,1,33,'CMSS14Y00183S'),
        (34,1,34,'CMSS14Y00184S'),
        (35,1,35,'CMSS14Y00185S'),
        (36,1,36,'CMSS14Y00186S'),
        (37,1,37,'CMSS14Y00187S'),
        (38,1,38,'CMSS14Y00188S'),
        (39,1,39,'CMSS14Y00189S'),
        (40,1,40,'CMSS14Y00190S'),
        (41,1,41,'CMSS14Y00191S'),
        (42,1,42,'CMSS14Y00192S'),
        (43,1,43,'CMSS14Y00193S'),
        (44,1,44,'CMSS14Y00194S'),
        (45,1,45,'CMSS14Y00195S'),
        (46,1,46,'CMSS14Y00196S'),
        (47,1,47,'CMSS14Y00197S'),
        (48,1,48,'CMSS14Y00198S'),
        (49,1,49,'CMSS14Y00199S'),
        (50,1,50,'CMSS14Y00200S'),
        (51,1,51,'CMSS14Y00201S'),
        (52,1,52,'CMSS14Y00202S'),
        (53,1,53,'CMSS14Y00203S'),
        (54,1,54,'CMSS14Y00204S'),
        (55,1,55,'CMSS14Y00205S'),
        (56,1,56,'CMSS14Y00206S'),
        (57,1,57,'CMSS14Y00207S'),
        (58,1,58,'CMSS14Y00208S'),
        (59,1,59,'CMSS14Y00209S'),
        (60,1,60,'CMSS14Y00210S'),
        (61,1,61,'CMSS14Y00211S'),
        (62,1,62,'CMSS14Y00212S'),
        (63,1,63,'CMSS14Y00213S'),
        (64,1,64,'CMSS14Y00214S'),
        (65,1,65,'CMSS14Y00215S'),
        (66,1,66,'CMSS14Y00216S'),
        (67,1,67,'CMSS14Y00217S'),
        (68,1,68,'CMSS14Y00218S'),
        (69,1,69,'CMSS14Y00219S'),
        (70,1,70,'CMSS14Y00220S'),
        (71,1,71,'CMSS14Y00221S'),
        (72,1,72,'CMSS14Y00222S'),
        (73,1,73,'CMSS14Y00223S'),
        (74,1,74,'CMSS14Y00224S'),
        (75,1,75,'CMSS14Y00225S'),
        (76,1,76,'CMSS14Y00226S'),
        (77,1,77,'CMSS14Y00227S'),
        (78,1,78,'CMSS14Y00228S'),
        (79,1,79,'CMSS14Y00229S'),
        (80,1,80,'CMSS14Y00230S'),
        (81,1,81,'CMSS14Y00231S'),
        (82,1,82,'CMSS14Y00232S'),
        (83,1,83,'CMSS14Y00233S'),
        (84,1,84,'CMSS14Y00234S'),
        (85,1,85,'CMSS14Y00235S'),
        (86,1,86,'CMSS14Y00236S'),
        (87,1,87,'CMSS14Y00237S'),
        (88,1,88,'CMSS14Y00238S'),
        (89,1,89,'CMSS14Y00239S'),
        (90,1,90,'CMSS14Y00240S'),
        (91,1,91,'CMSS14Y00241S'),
        (92,1,92,'CMSS14Y00242S'),
        (93,1,93,'CMSS14Y00243S'),
        (94,1,94,'CMSS14Y00244S'),
        (95,1,95,'CMSS14Y00245S'),
        (96,1,96,'CMSS14Y00246S'),
        (97,1,97,'CMSS14Y00247S'),
        (98,1,98,'CMSS14Y00248S'),
        (99,1,99,'CMSS14Y00249S'),
        (100,1,100,'CMSS14Y00250S'),
        (101,1,101,'CMSS14Y00251S'),
        (102,1,102,'CMSS14Y00252S'),
        (103,1,103,'CMSS14Y00253S'),
        (104,1,104,'CMSS14Y00254S'),
        (105,1,105,'CMSS14Y00255S'),
        (106,1,106,'CMSS14Y00256S'),
        (107,1,107,'CMSS14Y00257S'),
        (108,1,108,'CMSS14Y00258S'),
        (109,1,109,'CMSS14Y00259S'),
        (110,1,110,'CMSS14Y00260S'),
        (111,1,111,'CMSS14Y00261S'),
        (112,1,112,'CMSS14Y00262S'),
        (113,1,113,'CMSS14Y00263S'),
        (114,1,114,'CMSS14Y00264S'),
        (115,1,115,'CMSS14Y00265S'),
        (116,1,116,'CMSS14Y00266S'),
        (117,1,117,'CMSS14Y00267S'),
        (118,1,118,'CMSS14Y00268S'),
        (119,1,119,'CMSS14Y00269S'),
        (120,1,120,'CMSS14Y00270S'),
        (121,1,121,'CMSS14Y00271S'),
        (122,1,122,'CMSS14Y00272S'),
        (123,1,123,'CMSS14Y00273S'),
        (124,1,124,'CMSS14Y00274S'),
        (125,1,125,'CMSS14Y00275S'),
        (126,1,126,'CMSS14Y00276S'),
        (127,1,127,'CMSS14Y00277S'),
        (128,1,128,'CMSS14Y00278S'),
        (129,1,129,'CMSS14Y00279S'),
        (130,1,130,'CMSS14Y00280S'),
        (131,1,131,'CMSS14Y00281S'),
        (132,1,132,'CMSS14Y00282S'),
        (133,1,133,'CMSS14Y00283S'),
        (134,1,134,'CMSS14Y00284S'),
        (135,1,135,'CMSS14Y00285S'),
        (136,1,136,'CMSS14Y00286S'),
        (137,1,137,'CMSS14Y00287S'),
        (138,1,138,'CMSS14Y00288S'),
        (139,1,139,'CMSS14Y00289S'),
        (140,1,140,'CMSS14Y00290S'),
        (141,1,141,'CMSS14Y00291S'),
        (142,1,142,'CMSS14Y00292S'),
        (143,1,143,'CMSS14Y00293S'),
        (144,1,144,'CMSS14Y00294S'),
        (145,1,145,'CMSS14Y00295S'),
        (146,1,146,'CMSS14Y00296S'),
        (147,1,147,'CMSS14Y00297S'),
        (148,1,148,'CMSS14Y00298S'),
        (149,1,149,'CMSS14Y00299S'),
        (150,1,150,'CMSS14Y00300S')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'F1SIMPLEBW-2_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'F1SIMPLEBW-2_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-2_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.planting_instruction



-- populate wheat planting instructions for F1SIMPLEBW-2
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.package_log



-- populate wheat package logs for F1SIMPLEBW-2
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery package_log_id in experiment.planting_instruction



-- populate wheat package log references for F1SIMPLEBW-2
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-2_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross



-- populate wheat crosses for F1SIMPLEBW-2
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    expt.experiment_name = 'F1SIMPLEBW-2'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-2'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross_parent



-- populate wheat cross parents for F1SIMPLEBW-2
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'a.carlson'
        WHERE
            expt.experiment_name = 'F1SIMPLEBW-2'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-2'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.location_occurrence_group_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location_occurrence_group



-- populate wheat location occurrence groups for F1SIMPLEBW-2
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'F1SIMPLEBW-2_LOC1'
    AND occ.occurrence_name = 'F1SIMPLEBW-2_OCC1'
    AND prs.username = 'a.carlson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'F1SIMPLEBW-2_LOC1'
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-2_OCC1'
--rollback ;
