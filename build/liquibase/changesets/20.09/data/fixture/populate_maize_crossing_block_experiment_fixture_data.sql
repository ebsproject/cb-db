--liquibase formatted sql

--changeset postgres:populate_maize_crossing_block_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.experiment



-- populate maize experiment A-2
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
   (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'HB') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2019 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2019B' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'A-2' AS experiment_name,
    'Intentional Crossing Nursery' AS experiment_type,
    'Breeding Crosses' AS experiment_sub_type,
    'Crossing Block' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'elly.donelly') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'elly.donelly') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'A-2'
--rollback ;


--changeset postgres:populate_maize_crossing_block_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry_list



-- populate maize entry list A-2_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'A-2_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'A-2') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'elly.donelly') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'A-2_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry



-- populate maize entries for A-2
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number::integer,
    t.designation AS entry_name,
    'entry' AS entry_type,
    'female-and-male' AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        ('1','ABDHL0221','WE-KIB-17A-48-4'),
        ('2','ABDHL120312','ST-KIB-18B-34-12'),
        ('3','ABDHL120918','ST-KIB-18B-34-5'),
        ('4','ABDHL163943','ST-KIB-18B-48-50'),
        ('5','ABDHL164302','ST-KIB-18B-53-39'),
        ('6','ABDHL164665','ST-KIB-18B-53-41'),
        ('7','ABDHL164672','ST-KIB-18B-53-42'),
        ('8','ABDHL165891','WE-KIB-17B-53-33'),
        ('9','ABLMARSI0022','ST-KIB-18B-34-13'),
        ('10','ABLMARSI0037','ST-KIB-18B-34-6'),
        ('11','ABLTI0137','WE-KIB-17A-48-11'),
        ('12','ABLTI0330','WE-KIB-17A-47-50'),
        ('13','ABSBL10020','WE-KIB-17A-33-6'),
        ('14','ABSBL10060','ST-KIB-18B-34-3'),
        ('15','CML204','WE-KIB-17A-33-14'),
        ('16','CML442','ST-KIB-18B-48-70'),
        ('17','CML444','ST-KIB-18B-48-23'),
        ('18','CML464','ST-KIB-18B-34-2'),
        ('19','CML494','ST-KIB-18B-34-11'),
        ('20','CML536','ST-KIB-18B-46-2'),
        ('21','CML539','ST-KIB-18B-48-18'),
        ('22','CML543','ST-KIB-18B-34-1')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'A-2_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'elly.donelly'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'A-2_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.entry_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry_data



-- populate maize entry data for A-2
INSERT INTO
    experiment.entry_data (
        entry_id, variable_id, data_value, data_qc_code, creator_id
    )
SELECT
    ent.id AS entry_id,
    var.id AS variable_id,
    'Line' AS data_value,
    'N' AS data_qc_code,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PARENT_TYPE'
    INNER JOIN tenant.person AS person
        ON person.username = 'elly.donelly'
WHERE
    entlist.entry_list_name = 'A-2_ENTLIST'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_data AS entdata
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON entlist.id = ent.entry_list_id
--rollback WHERE
--rollback     entdata.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-2_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.occurrence



-- populate maize occurrence for A-2
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'A-2_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    3 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'A-2'
    AND geo.geospatial_object_code = 'KI'
    AND person.username = 'elly.donelly'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'A-2_OCC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in place.geospatial_object



-- populate maize planting area for A-2
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'A-2_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'A-2_LOC1'
--rollback ;


--changeset postgres:populate_maize_crossing_block_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.location



-- populate maize location for A-2
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'A-2_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2019 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'elly.donelly'
    AND season.season_code = 'A'
    AND geo.geospatial_object_name = 'A-2_LOC1'
    AND site.geospatial_object_code = 'KI'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'A-2_LOC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.plot



-- populate maize plots for A-2

INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number::integer AS plot_number,
    'plot' AS plot_type,
    t.rep::integer AS rep,
    1 AS design_x,
    t.plot_number::integer AS design_y,
    1 AS pa_x,
    t.plot_number::integer AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    tenant.person AS person,
    experiment.occurrence AS occ,
    experiment.location AS loc,
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN (
        VALUES
        ('1','1','1','ABDHL0221'),
        ('2','1','2','ABDHL120312'),
        ('3','1','3','ABDHL120918'),
        ('4','1','4','ABDHL163943'),
        ('5','1','5','ABDHL164302'),
        ('6','1','6','ABDHL164665'),
        ('7','1','7','ABDHL164672'),
        ('8','1','8','ABDHL165891'),
        ('9','1','9','ABLMARSI0022'),
        ('10','1','10','ABLMARSI0037'),
        ('11','1','11','ABLTI0137'),
        ('12','1','12','ABLTI0330'),
        ('13','1','13','ABSBL10020'),
        ('14','1','14','ABSBL10060'),
        ('15','1','15','CML204'),
        ('16','1','16','CML442'),
        ('17','1','17','CML444'),
        ('18','1','18','CML464'),
        ('19','1','19','CML494'),
        ('20','1','20','CML536'),
        ('21','1','21','CML539'),
        ('22','1','22','CML543')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
        ON t.entry_number::integer = ent.entry_number::integer
        AND t.designation = ent.entry_name
WHERE
    occ.occurrence_name = 'A-2_OCC1'
    AND loc.location_name = 'A-2_LOC1'
    AND entlist.entry_list_name = 'A-2_ENTLIST'
    AND person.username = 'elly.donelly'
ORDER BY
    t.plot_number
;


-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'A-2_OCC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.planting_instruction



-- populate maize planting instructions for A-2
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'elly.donelly'
WHERE
    entlist.entry_list_name = 'A-2_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-2_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.package_log



-- populate maize package logs for A-2
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'elly.donelly'
WHERE
    entlist.entry_list_name = 'A-2_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-2_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block package_log_id in experiment.planting_instruction



-- populate maize package log references for A-2
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'A-2_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-2_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_maize_crossing_block_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.cross



-- populate maize crosses for A-2
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'SIMPLE CROSS' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'elly.donelly'
WHERE
    expt.experiment_name = 'A-2'
    --AND entf.entry_number BETWEEN 1 AND 50
    --AND entm.entry_number BETWEEN 51 AND 100
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'A-2'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.cross_parent



-- populate maize cross parents for A-2
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'elly.donelly'
        WHERE
            expt.experiment_name = 'A-2'
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'elly.donelly'
            WHERE
                expt.experiment_name = 'A-2'
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'A-2'
--rollback ;