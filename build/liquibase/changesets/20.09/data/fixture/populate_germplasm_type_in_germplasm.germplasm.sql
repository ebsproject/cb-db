--liquibase formatted sql

--changeset postgres:populate_germplasm_type_in_germplasm.germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1385 Populate germplasm_type in germplasm.germplasm



-- disable triggers
ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;


-- populate germplasm_type
UPDATE
    germplasm.germplasm
SET
    germplasm_type = germplasm_state
WHERE
    germplasm_type IS NULL
;


-- enable trigger
ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;
--rollback 
--rollback UPDATE
--rollback     germplasm.germplasm
--rollback SET
--rollback     germplasm_type = NULL
--rollback WHERE
--rollback     germplasm_type IS NOT NULL
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;
