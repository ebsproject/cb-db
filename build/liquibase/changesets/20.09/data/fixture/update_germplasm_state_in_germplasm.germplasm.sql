--liquibase formatted sql

--changeset postgres:update_germplasm_state_in_germplasm.germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1385 Update germplasm_state in germplasm.germplasm



-- disable triggers
ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;


-- update germplasm_state column
UPDATE
    germplasm.germplasm
SET
    germplasm_state = (
        CASE
            WHEN germplasm_state = 'progeny' THEN 'non-fixed'
            WHEN germplasm_state IN ('fixed_line', 'hybrid', 'accession') THEN 'fixed'
        END
    )
WHERE
    germplasm_state IS NOT NULL
;


-- enable trigger
ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;
--rollback 
--rollback UPDATE germplasm.germplasm SET germplasm_state = 'progeny' WHERE germplasm_state = 'non-fixed';
--rollback UPDATE germplasm.germplasm SET germplasm_state = 'hybrid' WHERE id IN (461817);
--rollback UPDATE germplasm.germplasm SET germplasm_state = 'accession' WHERE id IN (91600, 91601, 91602, 91604, 91605, 91609, 91610, 91611, 91614, 91615, 110350, 110378, 110384, 461899, 553721, 1043574, 1346665, 1361470, 1043741);
--rollback UPDATE germplasm.germplasm SET germplasm_state = 'fixed_line' WHERE germplasm_state = 'fixed';
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;