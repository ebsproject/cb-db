--liquibase formatted sql

--changeset postgres:add_a2_cb_experiment_mapping_to_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Add A-2 CB experiment mapping to experiment.location_occurrence_group



INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'A-2_LOC1'
    AND occ.occurrence_name = 'A-2_OCC1'
    AND prs.username = 'elly.donelly'
;



--rollback DELETE FROM experiment.location_occurrence_group WHERE location_id=(SELECT id FROM experiment.location WHERE location_name='A-2_LOC1') 
--rollback AND occurrence_id=(SELECT id FROM experiment.occurrence where occurrence_name='A-2_OCC1');