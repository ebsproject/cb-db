--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_experiment.experiment_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.experiment



-- populate wheat experiment F1SIMPLEBW-5
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F1') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2014 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2014A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'F1SIMPLEBW-5' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'F1SIMPLEBW-5'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry_list



-- populate wheat entry list F1SIMPLEBW-5_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'F1SIMPLEBW-5_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'F1SIMPLEBW-5') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry



-- populate wheat entries for F1SIMPLEBW-5
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS14Y00601S','Y13-14CBBW445'),
        (2,'CMSS14Y00602S','Y13-14CBBW446'),
        (3,'CMSS14Y00603S','Y13-14CBBW446'),
        (4,'CMSS14Y00604S','Y13-14CBBW447'),
        (5,'CMSS14Y00605S','Y13-14CBBW448'),
        (6,'CMSS14Y00606S','Y13-14CBBW449'),
        (7,'CMSS14Y00607S','Y13-14CBBW450'),
        (8,'CMSS14Y00608S','Y13-14CBBW451'),
        (9,'CMSS14Y00609S','Y13-14CBBW452'),
        (10,'CMSS14Y00610S','Y13-14CBBW452'),
        (11,'CMSS14Y00611S','Y13-14CBBW453'),
        (12,'CMSS14Y00612S','Y13-14CBBW453'),
        (13,'CMSS14Y00613S','Y13-14CBBW454'),
        (14,'CMSS14Y00614S','Y13-14CBBW455'),
        (15,'CMSS14Y00615S','Y13-14CBBW456'),
        (16,'CMSS14Y00616S','Y13-14CBBW456'),
        (17,'CMSS14Y00617S','Y13-14CBBW456'),
        (18,'CMSS14Y00618S','Y13-14CBBW456'),
        (19,'CMSS14Y00619S','Y13-14CBBW456'),
        (20,'CMSS14Y00620S','Y13-14CBBW456'),
        (21,'CMSS14Y00621S','Y13-14CBBW457'),
        (22,'CMSS14Y00622S','Y13-14CBBW457'),
        (23,'CMSS14Y00623S','Y13-14CBBW458'),
        (24,'CMSS14Y00624S','Y13-14CBBW459'),
        (25,'CMSS14Y00625S','Y13-14CBBW459'),
        (26,'CMSS14Y00626S','Y13-14CBBW459'),
        (27,'CMSS14Y00627S','Y13-14CBBW460'),
        (28,'CMSS14Y00628S','Y13-14CBBW461'),
        (29,'CMSS14Y00629S','Y13-14CBBW461'),
        (30,'CMSS14Y00630S','Y13-14CBBW462'),
        (31,'CMSS14Y00631S','Y13-14CBBW463'),
        (32,'CMSS14Y00632S','Y13-14CBBW464'),
        (33,'CMSS14Y00633S','Y13-14CBBW465'),
        (34,'CMSS14Y00634S','Y13-14CBBW466'),
        (35,'CMSS14Y00635S','Y13-14CBBW467'),
        (36,'CMSS14Y00636S','Y13-14CBBW468'),
        (37,'CMSS14Y00637S','Y13-14CBBW468'),
        (38,'CMSS14Y00638S','Y13-14CBBW469'),
        (39,'CMSS14Y00639S','Y13-14CBBW470'),
        (40,'CMSS14Y00640S','Y13-14CBBW470'),
        (41,'CMSS14Y00641S','Y13-14CBBW471'),
        (42,'CMSS14Y00642S','Y13-14CBBW472'),
        (43,'CMSS14Y00643S','Y13-14CBBW472'),
        (44,'CMSS14Y00644S','Y13-14CBBW473'),
        (45,'CMSS14Y00645S','Y13-14CBBW474'),
        (46,'CMSS14Y00646S','Y13-14CBBW475'),
        (47,'CMSS14Y00647S','Y13-14CBBW475'),
        (48,'CMSS14Y00648S','Y13-14CBBW476'),
        (49,'CMSS14Y00649S','Y13-14CBBW477'),
        (50,'CMSS14Y00650S','Y13-14CBBW478'),
        (51,'CMSS14Y00651S','Y13-14CBBW478'),
        (52,'CMSS14Y00652S','Y13-14CBBW479'),
        (53,'CMSS14Y00653S','Y13-14CBBW480'),
        (54,'CMSS14Y00654S','Y13-14CBBW481'),
        (55,'CMSS14Y00655S','Y13-14CBBW482'),
        (56,'CMSS14Y00656S','Y13-14CBBW482'),
        (57,'CMSS14Y00657S','Y13-14CBBW483'),
        (58,'CMSS14Y00658S','Y13-14CBBW483'),
        (59,'CMSS14Y00659S','Y13-14CBBW484'),
        (60,'CMSS14Y00660S','Y13-14CBBW484'),
        (61,'CMSS14Y00661S','Y13-14CBBW485'),
        (62,'CMSS14Y00662S','Y13-14CBBW486'),
        (63,'CMSS14Y00663S','Y13-14CBBW487'),
        (64,'CMSS14Y00664S','Y13-14CBBW488'),
        (65,'CMSS14Y00665S','Y13-14CBBW489'),
        (66,'CMSS14Y00666S','Y13-14CBBW490'),
        (67,'CMSS14Y00667S','Y13-14CBBW491'),
        (68,'CMSS14Y00668S','Y13-14CBBW492'),
        (69,'CMSS14Y00669S','Y13-14CBBW492'),
        (70,'CMSS14Y00670S','Y13-14CBBW493'),
        (71,'CMSS14Y00671S','Y13-14CBBW494'),
        (72,'CMSS14Y00672S','Y13-14CBBW495'),
        (73,'CMSS14Y00673S','Y13-14CBBW495'),
        (74,'CMSS14Y00674S','Y13-14CBBW496'),
        (75,'CMSS14Y00675S','Y13-14CBBW497'),
        (76,'CMSS14Y00676S','Y13-14CBBW497'),
        (77,'CMSS14Y00677S','Y13-14CBBW498'),
        (78,'CMSS14Y00678S','Y13-14CBBW499'),
        (79,'CMSS14Y00679S','Y13-14CBBW500'),
        (80,'CMSS14Y00680S','Y13-14CBBW500'),
        (81,'CMSS14Y00681S','Y13-14CBBW501'),
        (82,'CMSS14Y00682S','Y13-14CBBW502'),
        (83,'CMSS14Y00683S','Y13-14CBBW502'),
        (84,'CMSS14Y00684S','Y13-14CBBW503'),
        (85,'CMSS14Y00685S','Y13-14CBBW504'),
        (86,'CMSS14Y00686S','Y13-14CBBW504'),
        (87,'CMSS14Y00687S','Y13-14CBBW505'),
        (88,'CMSS14Y00688S','Y13-14CBBW506'),
        (89,'CMSS14Y00689S','Y13-14CBBW506'),
        (90,'CMSS14Y00690S','Y13-14CBBW507'),
        (91,'CMSS14Y00691S','Y13-14CBBW508'),
        (92,'CMSS14Y00692S','Y13-14CBBW508'),
        (93,'CMSS14Y00693S','Y13-14CBBW508'),
        (94,'CMSS14Y00694S','Y13-14CBBW508'),
        (95,'CMSS14Y00695S','Y13-14CBBW508'),
        (96,'CMSS14Y00696S','Y13-14CBBW508'),
        (97,'CMSS14Y00697S','Y13-14CBBW508'),
        (98,'CMSS14Y00698S','Y13-14CBBW509'),
        (99,'CMSS14Y00699S','Y13-14CBBW509'),
        (100,'CMSS14Y00700S','Y13-14CBBW509'),
        (101,'CMSS14Y00701S','Y13-14CBBW509'),
        (102,'CMSS14Y00702S','Y13-14CBBW509'),
        (103,'CMSS14Y00703S','Y13-14CBBW509'),
        (104,'CMSS14Y00704S','Y13-14CBBW509'),
        (105,'CMSS14Y00705S','Y13-14CBBW510'),
        (106,'CMSS14Y00706S','Y13-14CBBW510'),
        (107,'CMSS14Y00707S','Y13-14CBBW510'),
        (108,'CMSS14Y00708S','Y13-14CBBW510'),
        (109,'CMSS14Y00709S','Y13-14CBBW511'),
        (110,'CMSS14Y00710S','Y13-14CBBW511'),
        (111,'CMSS14Y00711S','Y13-14CBBW511'),
        (112,'CMSS14Y00712S','Y13-14CBBW511'),
        (113,'CMSS14Y00713S','Y13-14CBBW511'),
        (114,'CMSS14Y00714S','Y13-14CBBW511'),
        (115,'CMSS14Y00715S','Y13-14CBBW511'),
        (116,'CMSS14Y00716S','Y13-14CBBW512'),
        (117,'CMSS14Y00717S','Y13-14CBBW512'),
        (118,'CMSS14Y00718S','Y13-14CBBW513'),
        (119,'CMSS14Y00719S','Y13-14CBBW513'),
        (120,'CMSS14Y00720S','Y13-14CBBW513'),
        (121,'CMSS14Y00721S','Y13-14CBBW513'),
        (122,'CMSS14Y00722S','Y13-14CBBW513'),
        (123,'CMSS14Y00723S','Y13-14CBBW514'),
        (124,'CMSS14Y00724S','Y13-14CBBW514'),
        (125,'CMSS14Y00725S','Y13-14CBBW514'),
        (126,'CMSS14Y00726S','Y13-14CBBW514'),
        (127,'CMSS14Y00727S','Y13-14CBBW514'),
        (128,'CMSS14Y00728S','Y13-14CBBW515'),
        (129,'CMSS14Y00729S','Y13-14CBBW515'),
        (130,'CMSS14Y00730S','Y13-14CBBW515'),
        (131,'CMSS14Y00731S','Y13-14CBBW515'),
        (132,'CMSS14Y00732S','Y13-14CBBW515'),
        (133,'CMSS14Y00733S','Y13-14CBBW516'),
        (134,'CMSS14Y00734S','Y13-14CBBW516'),
        (135,'CMSS14Y00735S','Y13-14CBBW516'),
        (136,'CMSS14Y00736S','Y13-14CBBW516'),
        (137,'CMSS14Y00737S','Y13-14CBBW516'),
        (138,'CMSS14Y00738S','Y13-14CBBW517'),
        (139,'CMSS14Y00739S','Y13-14CBBW517'),
        (140,'CMSS14Y00740S','Y13-14CBBW517'),
        (141,'CMSS14Y00741S','Y13-14CBBW532'),
        (142,'CMSS14Y00742S','Y13-14CBBW532'),
        (143,'CMSS14Y00743S','Y13-14CBBW533'),
        (144,'CMSS14Y00744S','Y13-14CBBW533'),
        (145,'CMSS14Y00745S','Y13-14CBBW534'),
        (146,'CMSS14Y00746S','Y13-14CBBW534'),
        (147,'CMSS14Y00747S','Y13-14CBBW535'),
        (148,'CMSS14Y00748S','Y13-14CBBW535'),
        (149,'CMSS14Y00749S','Y13-14CBBW536'),
        (150,'CMSS14Y00750S','Y13-14CBBW536'),
        (151,'CMSS14Y00751S','Y13-14CBBW537'),
        (152,'CMSS14Y00752S','Y13-14CBBW537'),
        (153,'CMSS14Y00753S','Y13-14CBBW538'),
        (154,'CMSS14Y00754S','Y13-14CBBW538'),
        (155,'CMSS14Y00755S','Y13-14CBBW539'),
        (156,'CMSS14Y00756S','Y13-14CBBW539'),
        (157,'CMSS14Y00757S','Y13-14CBBW540'),
        (158,'CMSS14Y00758S','Y13-14CBBW540'),
        (159,'CMSS14Y00759S','Y13-14CBBW541'),
        (160,'CMSS14Y00760S','Y13-14CBBW541'),
        (161,'CMSS14Y00761S','Y13-14CBBW542'),
        (162,'CMSS14Y00762S','Y13-14CBBW542'),
        (163,'CMSS14Y00763S','Y13-14CBBW543'),
        (164,'CMSS14Y00764S','Y13-14CBBW543'),
        (165,'CMSS14Y00765S','Y13-14CBBW544'),
        (166,'CMSS14Y00766S','Y13-14CBBW544'),
        (167,'CMSS14Y00767S','Y13-14CBBW544'),
        (168,'CMSS14Y00768S','Y13-14CBBW545'),
        (169,'CMSS14Y00769S','Y13-14CBBW545'),
        (170,'CMSS14Y00770S','Y13-14CBBW545'),
        (171,'CMSS14Y00771S','Y13-14CBBW546'),
        (172,'CMSS14Y00772S','Y13-14CBBW546'),
        (173,'CMSS14Y00773S','Y13-14CBBW546'),
        (174,'CMSS14Y00774S','Y13-14CBBW547'),
        (175,'CMSS14Y00775S','Y13-14CBBW547'),
        (176,'CMSS14Y00776S','Y13-14CBBW547'),
        (177,'CMSS14Y00777S','Y13-14CBBW548'),
        (178,'CMSS14Y00778S','Y13-14CBBW548'),
        (179,'CMSS14Y00779S','Y13-14CBBW548'),
        (180,'CMSS14Y00780S','Y13-14CBBW549'),
        (181,'CMSS14Y00781S','Y13-14CBBW549'),
        (182,'CMSS14Y00782S','Y13-14CBBW549'),
        (183,'CMSS14Y00783S','Y13-14CBBW550'),
        (184,'CMSS14Y00784S','Y13-14CBBW550'),
        (185,'CMSS14Y00785S','Y13-14CBBW550'),
        (186,'CMSS14Y00786S','Y13-14CBBW551'),
        (187,'CMSS14Y00787S','Y13-14CBBW551'),
        (188,'CMSS14Y00788S','Y13-14CBBW551'),
        (189,'CMSS14Y00789S','Y13-14CBBW552'),
        (190,'CMSS14Y00790S','Y13-14CBBW552'),
        (191,'CMSS14Y00791S','Y13-14CBBW552'),
        (192,'CMSS14Y00792S','Y13-14CBBW557'),
        (193,'CMSS14Y00793S','Y13-14CBBW557'),
        (194,'CMSS14Y00794S','Y13-14CBBW557'),
        (195,'CMSS14Y00795S','Y13-14CBBW557'),
        (196,'CMSS14Y00796S','Y13-14CBBW557'),
        (197,'CMSS14Y00797S','Y13-14CBBW558'),
        (198,'CMSS14Y00798S','Y13-14CBBW558'),
        (199,'CMSS14Y00799S','Y13-14CBBW558'),
        (200,'CMSS14Y00800S','Y13-14CBBW558'),
        (201,'CMSS14Y00801S','Y13-14CBBW558')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.occurrence



-- populate wheat occurrence for F1SIMPLEBW-5
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'F1SIMPLEBW-5_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'F1SIMPLEBW-5'
    AND geo.geospatial_object_code = 'EB'
    AND person.username = 'jennifer.allan'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'F1SIMPLEBW-5_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in place.geospatial_object



-- populate wheat planting area for F1SIMPLEBW-5
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'F1SIMPLEBW-5_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'F1SIMPLEBW-5_LOC1'
--rollback ;


--changeset postgres:populate_wheat_f1_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location



-- populate wheat location for F1SIMPLEBW-5
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'F1SIMPLEBW-5_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'jennifer.allan'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'F1SIMPLEBW-5_LOC1'
    AND site.geospatial_object_code = 'EB'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'F1SIMPLEBW-5_LOC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.plot



-- populate wheat plots for F1SIMPLEBW-5
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS14Y00601S'),
        (2,1,2,'CMSS14Y00602S'),
        (3,1,3,'CMSS14Y00603S'),
        (4,1,4,'CMSS14Y00604S'),
        (5,1,5,'CMSS14Y00605S'),
        (6,1,6,'CMSS14Y00606S'),
        (7,1,7,'CMSS14Y00607S'),
        (8,1,8,'CMSS14Y00608S'),
        (9,1,9,'CMSS14Y00609S'),
        (10,1,10,'CMSS14Y00610S'),
        (11,1,11,'CMSS14Y00611S'),
        (12,1,12,'CMSS14Y00612S'),
        (13,1,13,'CMSS14Y00613S'),
        (14,1,14,'CMSS14Y00614S'),
        (15,1,15,'CMSS14Y00615S'),
        (16,1,16,'CMSS14Y00616S'),
        (17,1,17,'CMSS14Y00617S'),
        (18,1,18,'CMSS14Y00618S'),
        (19,1,19,'CMSS14Y00619S'),
        (20,1,20,'CMSS14Y00620S'),
        (21,1,21,'CMSS14Y00621S'),
        (22,1,22,'CMSS14Y00622S'),
        (23,1,23,'CMSS14Y00623S'),
        (24,1,24,'CMSS14Y00624S'),
        (25,1,25,'CMSS14Y00625S'),
        (26,1,26,'CMSS14Y00626S'),
        (27,1,27,'CMSS14Y00627S'),
        (28,1,28,'CMSS14Y00628S'),
        (29,1,29,'CMSS14Y00629S'),
        (30,1,30,'CMSS14Y00630S'),
        (31,1,31,'CMSS14Y00631S'),
        (32,1,32,'CMSS14Y00632S'),
        (33,1,33,'CMSS14Y00633S'),
        (34,1,34,'CMSS14Y00634S'),
        (35,1,35,'CMSS14Y00635S'),
        (36,1,36,'CMSS14Y00636S'),
        (37,1,37,'CMSS14Y00637S'),
        (38,1,38,'CMSS14Y00638S'),
        (39,1,39,'CMSS14Y00639S'),
        (40,1,40,'CMSS14Y00640S'),
        (41,1,41,'CMSS14Y00641S'),
        (42,1,42,'CMSS14Y00642S'),
        (43,1,43,'CMSS14Y00643S'),
        (44,1,44,'CMSS14Y00644S'),
        (45,1,45,'CMSS14Y00645S'),
        (46,1,46,'CMSS14Y00646S'),
        (47,1,47,'CMSS14Y00647S'),
        (48,1,48,'CMSS14Y00648S'),
        (49,1,49,'CMSS14Y00649S'),
        (50,1,50,'CMSS14Y00650S'),
        (51,1,51,'CMSS14Y00651S'),
        (52,1,52,'CMSS14Y00652S'),
        (53,1,53,'CMSS14Y00653S'),
        (54,1,54,'CMSS14Y00654S'),
        (55,1,55,'CMSS14Y00655S'),
        (56,1,56,'CMSS14Y00656S'),
        (57,1,57,'CMSS14Y00657S'),
        (58,1,58,'CMSS14Y00658S'),
        (59,1,59,'CMSS14Y00659S'),
        (60,1,60,'CMSS14Y00660S'),
        (61,1,61,'CMSS14Y00661S'),
        (62,1,62,'CMSS14Y00662S'),
        (63,1,63,'CMSS14Y00663S'),
        (64,1,64,'CMSS14Y00664S'),
        (65,1,65,'CMSS14Y00665S'),
        (66,1,66,'CMSS14Y00666S'),
        (67,1,67,'CMSS14Y00667S'),
        (68,1,68,'CMSS14Y00668S'),
        (69,1,69,'CMSS14Y00669S'),
        (70,1,70,'CMSS14Y00670S'),
        (71,1,71,'CMSS14Y00671S'),
        (72,1,72,'CMSS14Y00672S'),
        (73,1,73,'CMSS14Y00673S'),
        (74,1,74,'CMSS14Y00674S'),
        (75,1,75,'CMSS14Y00675S'),
        (76,1,76,'CMSS14Y00676S'),
        (77,1,77,'CMSS14Y00677S'),
        (78,1,78,'CMSS14Y00678S'),
        (79,1,79,'CMSS14Y00679S'),
        (80,1,80,'CMSS14Y00680S'),
        (81,1,81,'CMSS14Y00681S'),
        (82,1,82,'CMSS14Y00682S'),
        (83,1,83,'CMSS14Y00683S'),
        (84,1,84,'CMSS14Y00684S'),
        (85,1,85,'CMSS14Y00685S'),
        (86,1,86,'CMSS14Y00686S'),
        (87,1,87,'CMSS14Y00687S'),
        (88,1,88,'CMSS14Y00688S'),
        (89,1,89,'CMSS14Y00689S'),
        (90,1,90,'CMSS14Y00690S'),
        (91,1,91,'CMSS14Y00691S'),
        (92,1,92,'CMSS14Y00692S'),
        (93,1,93,'CMSS14Y00693S'),
        (94,1,94,'CMSS14Y00694S'),
        (95,1,95,'CMSS14Y00695S'),
        (96,1,96,'CMSS14Y00696S'),
        (97,1,97,'CMSS14Y00697S'),
        (98,1,98,'CMSS14Y00698S'),
        (99,1,99,'CMSS14Y00699S'),
        (100,1,100,'CMSS14Y00700S'),
        (101,1,101,'CMSS14Y00701S'),
        (102,1,102,'CMSS14Y00702S'),
        (103,1,103,'CMSS14Y00703S'),
        (104,1,104,'CMSS14Y00704S'),
        (105,1,105,'CMSS14Y00705S'),
        (106,1,106,'CMSS14Y00706S'),
        (107,1,107,'CMSS14Y00707S'),
        (108,1,108,'CMSS14Y00708S'),
        (109,1,109,'CMSS14Y00709S'),
        (110,1,110,'CMSS14Y00710S'),
        (111,1,111,'CMSS14Y00711S'),
        (112,1,112,'CMSS14Y00712S'),
        (113,1,113,'CMSS14Y00713S'),
        (114,1,114,'CMSS14Y00714S'),
        (115,1,115,'CMSS14Y00715S'),
        (116,1,116,'CMSS14Y00716S'),
        (117,1,117,'CMSS14Y00717S'),
        (118,1,118,'CMSS14Y00718S'),
        (119,1,119,'CMSS14Y00719S'),
        (120,1,120,'CMSS14Y00720S'),
        (121,1,121,'CMSS14Y00721S'),
        (122,1,122,'CMSS14Y00722S'),
        (123,1,123,'CMSS14Y00723S'),
        (124,1,124,'CMSS14Y00724S'),
        (125,1,125,'CMSS14Y00725S'),
        (126,1,126,'CMSS14Y00726S'),
        (127,1,127,'CMSS14Y00727S'),
        (128,1,128,'CMSS14Y00728S'),
        (129,1,129,'CMSS14Y00729S'),
        (130,1,130,'CMSS14Y00730S'),
        (131,1,131,'CMSS14Y00731S'),
        (132,1,132,'CMSS14Y00732S'),
        (133,1,133,'CMSS14Y00733S'),
        (134,1,134,'CMSS14Y00734S'),
        (135,1,135,'CMSS14Y00735S'),
        (136,1,136,'CMSS14Y00736S'),
        (137,1,137,'CMSS14Y00737S'),
        (138,1,138,'CMSS14Y00738S'),
        (139,1,139,'CMSS14Y00739S'),
        (140,1,140,'CMSS14Y00740S'),
        (141,1,141,'CMSS14Y00741S'),
        (142,1,142,'CMSS14Y00742S'),
        (143,1,143,'CMSS14Y00743S'),
        (144,1,144,'CMSS14Y00744S'),
        (145,1,145,'CMSS14Y00745S'),
        (146,1,146,'CMSS14Y00746S'),
        (147,1,147,'CMSS14Y00747S'),
        (148,1,148,'CMSS14Y00748S'),
        (149,1,149,'CMSS14Y00749S'),
        (150,1,150,'CMSS14Y00750S'),
        (151,1,151,'CMSS14Y00751S'),
        (152,1,152,'CMSS14Y00752S'),
        (153,1,153,'CMSS14Y00753S'),
        (154,1,154,'CMSS14Y00754S'),
        (155,1,155,'CMSS14Y00755S'),
        (156,1,156,'CMSS14Y00756S'),
        (157,1,157,'CMSS14Y00757S'),
        (158,1,158,'CMSS14Y00758S'),
        (159,1,159,'CMSS14Y00759S'),
        (160,1,160,'CMSS14Y00760S'),
        (161,1,161,'CMSS14Y00761S'),
        (162,1,162,'CMSS14Y00762S'),
        (163,1,163,'CMSS14Y00763S'),
        (164,1,164,'CMSS14Y00764S'),
        (165,1,165,'CMSS14Y00765S'),
        (166,1,166,'CMSS14Y00766S'),
        (167,1,167,'CMSS14Y00767S'),
        (168,1,168,'CMSS14Y00768S'),
        (169,1,169,'CMSS14Y00769S'),
        (170,1,170,'CMSS14Y00770S'),
        (171,1,171,'CMSS14Y00771S'),
        (172,1,172,'CMSS14Y00772S'),
        (173,1,173,'CMSS14Y00773S'),
        (174,1,174,'CMSS14Y00774S'),
        (175,1,175,'CMSS14Y00775S'),
        (176,1,176,'CMSS14Y00776S'),
        (177,1,177,'CMSS14Y00777S'),
        (178,1,178,'CMSS14Y00778S'),
        (179,1,179,'CMSS14Y00779S'),
        (180,1,180,'CMSS14Y00780S'),
        (181,1,181,'CMSS14Y00781S'),
        (182,1,182,'CMSS14Y00782S'),
        (183,1,183,'CMSS14Y00783S'),
        (184,1,184,'CMSS14Y00784S'),
        (185,1,185,'CMSS14Y00785S'),
        (186,1,186,'CMSS14Y00786S'),
        (187,1,187,'CMSS14Y00787S'),
        (188,1,188,'CMSS14Y00788S'),
        (189,1,189,'CMSS14Y00789S'),
        (190,1,190,'CMSS14Y00790S'),
        (191,1,191,'CMSS14Y00791S'),
        (192,1,192,'CMSS14Y00792S'),
        (193,1,193,'CMSS14Y00793S'),
        (194,1,194,'CMSS14Y00794S'),
        (195,1,195,'CMSS14Y00795S'),
        (196,1,196,'CMSS14Y00796S'),
        (197,1,197,'CMSS14Y00797S'),
        (198,1,198,'CMSS14Y00798S'),
        (199,1,199,'CMSS14Y00799S'),
        (200,1,200,'CMSS14Y00800S'),
        (201,1,201,'CMSS14Y00801S')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'F1SIMPLEBW-5_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'F1SIMPLEBW-5_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-5_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.planting_instruction



-- populate wheat planting instructions for F1SIMPLEBW-5
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.package_log



-- populate wheat package logs for F1SIMPLEBW-5
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery package_log_id in experiment.planting_instruction



-- populate wheat package log references for F1SIMPLEBW-5
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-5_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross



-- populate wheat crosses for F1SIMPLEBW-5
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    expt.experiment_name = 'F1SIMPLEBW-5'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-5'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross_parent



-- populate wheat cross parents for F1SIMPLEBW-5
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'jennifer.allan'
        WHERE
            expt.experiment_name = 'F1SIMPLEBW-5'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-5'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.location_occurrence_group_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location_occurrence_group



-- populate wheat location occurrence groups for F1SIMPLEBW-5
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'F1SIMPLEBW-5_LOC1'
    AND occ.occurrence_name = 'F1SIMPLEBW-5_OCC1'
    AND prs.username = 'jennifer.allan'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'F1SIMPLEBW-5_LOC1'
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-5_OCC1'
--rollback ;
