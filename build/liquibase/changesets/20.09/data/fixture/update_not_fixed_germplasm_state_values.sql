--liquibase formatted sql

--changeset postgres:update_not_fixed_germplasm_state_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1385 Update not_fixed germplasm_state values



-- disable triggers
ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
DISABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;


-- update not_fixed germplasm_state values
UPDATE
    germplasm.germplasm
SET
    germplasm_state = 'not_fixed'
WHERE
    germplasm_state = 'non-fixed'
;


-- enable trigger
ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_germplasm_document_tgr
;

ALTER TABLE
    germplasm.germplasm
ENABLE TRIGGER
    germplasm_update_package_document_from_germplasm_tgr
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DISABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;
--rollback 
--rollback UPDATE
--rollback     germplasm.germplasm
--rollback SET
--rollback     germplasm_state = 'non-fixed'
--rollback WHERE
--rollback     germplasm_state = 'not_fixed'
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_germplasm_document_tgr
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ENABLE TRIGGER
--rollback     germplasm_update_package_document_from_germplasm_tgr
--rollback ;
