--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_experiment.experiment_part_4 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.experiment



-- populate wheat experiment F1SIMPLEBW-4
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F1') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2014 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2014A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'F1SIMPLEBW-4' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'k.khadija') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'k.khadija') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'F1SIMPLEBW-4'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry_list



-- populate wheat entry list F1SIMPLEBW-4_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'F1SIMPLEBW-4_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'F1SIMPLEBW-4') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'k.khadija') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry



-- populate wheat entries for F1SIMPLEBW-4
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS14Y00451S','Y13-14CBBW323'),
        (2,'CMSS14Y00452S','Y13-14CBBW323'),
        (3,'CMSS14Y00453S','Y13-14CBBW325'),
        (4,'CMSS14Y00454S','Y13-14CBBW325'),
        (5,'CMSS14Y00455S','Y13-14CBBW326'),
        (6,'CMSS14Y00456S','Y13-14CBBW326'),
        (7,'CMSS14Y00457S','Y13-14CBBW330'),
        (8,'CMSS14Y00458S','Y13-14CBBW333'),
        (9,'CMSS14Y00459S','Y13-14CBBW333'),
        (10,'CMSS14Y00460S','Y13-14CBBW335'),
        (11,'CMSS14Y00461S','Y13-14CBBW335'),
        (12,'CMSS14Y00462S','Y13-14CBBW335'),
        (13,'CMSS14Y00463S','Y13-14CBBW336'),
        (14,'CMSS14Y00464S','Y13-14CBBW336'),
        (15,'CMSS14Y00465S','Y13-14CBBW336'),
        (16,'CMSS14Y00466S','Y13-14CBBW338'),
        (17,'CMSS14Y00467S','Y13-14CBBW339'),
        (18,'CMSS14Y00468S','Y13-14CBBW339'),
        (19,'CMSS14Y00469S','Y13-14CBBW339'),
        (20,'CMSS14Y00470S','Y13-14CBBW341'),
        (21,'CMSS14Y00471S','Y13-14CBBW342'),
        (22,'CMSS14Y00472S','Y13-14CBBW343'),
        (23,'CMSS14Y00473S','Y13-14CBBW344'),
        (24,'CMSS14Y00474S','Y13-14CBBW345'),
        (25,'CMSS14Y00475S','Y13-14CBBW346'),
        (26,'CMSS14Y00476S','Y13-14CBBW346'),
        (27,'CMSS14Y00477S','Y13-14CBBW347'),
        (28,'CMSS14Y00478S','Y13-14CBBW348'),
        (29,'CMSS14Y00479S','Y13-14CBBW349'),
        (30,'CMSS14Y00480S','Y13-14CBBW350'),
        (31,'CMSS14Y00481S','Y13-14CBBW351'),
        (32,'CMSS14Y00482S','Y13-14CBBW352'),
        (33,'CMSS14Y00483S','Y13-14CBBW353'),
        (34,'CMSS14Y00484S','Y13-14CBBW354'),
        (35,'CMSS14Y00485S','Y13-14CBBW355'),
        (36,'CMSS14Y00486S','Y13-14CBBW356'),
        (37,'CMSS14Y00487S','Y13-14CBBW357'),
        (38,'CMSS14Y00488S','Y13-14CBBW358'),
        (39,'CMSS14Y00489S','Y13-14CBBW359'),
        (40,'CMSS14Y00490S','Y13-14CBBW360'),
        (41,'CMSS14Y00491S','Y13-14CBBW361'),
        (42,'CMSS14Y00492S','Y13-14CBBW362'),
        (43,'CMSS14Y00493S','Y13-14CBBW363'),
        (44,'CMSS14Y00494S','Y13-14CBBW364'),
        (45,'CMSS14Y00495S','Y13-14CBBW364'),
        (46,'CMSS14Y00496S','Y13-14CBBW365'),
        (47,'CMSS14Y00497S','Y13-14CBBW366'),
        (48,'CMSS14Y00498S','Y13-14CBBW367'),
        (49,'CMSS14Y00499S','Y13-14CBBW368'),
        (50,'CMSS14Y00500S','Y13-14CBBW369'),
        (51,'CMSS14Y00501S','Y13-14CBBW370'),
        (52,'CMSS14Y00502S','Y13-14CBBW371'),
        (53,'CMSS14Y00503S','Y13-14CBBW372'),
        (54,'CMSS14Y00504S','Y13-14CBBW373'),
        (55,'CMSS14Y00505S','Y13-14CBBW374'),
        (56,'CMSS14Y00506S','Y13-14CBBW375'),
        (57,'CMSS14Y00507S','Y13-14CBBW376'),
        (58,'CMSS14Y00508S','Y13-14CBBW377'),
        (59,'CMSS14Y00509S','Y13-14CBBW378'),
        (60,'CMSS14Y00510S','Y13-14CBBW379'),
        (61,'CMSS14Y00511S','Y13-14CBBW380'),
        (62,'CMSS14Y00512S','Y13-14CBBW380'),
        (63,'CMSS14Y00513S','Y13-14CBBW381'),
        (64,'CMSS14Y00514S','Y13-14CBBW381'),
        (65,'CMSS14Y00515S','Y13-14CBBW382'),
        (66,'CMSS14Y00516S','Y13-14CBBW382'),
        (67,'CMSS14Y00517S','Y13-14CBBW383'),
        (68,'CMSS14Y00518S','Y13-14CBBW384'),
        (69,'CMSS14Y00519S','Y13-14CBBW385'),
        (70,'CMSS14Y00520S','Y13-14CBBW385'),
        (71,'CMSS14Y00521S','Y13-14CBBW386'),
        (72,'CMSS14Y00522S','Y13-14CBBW386'),
        (73,'CMSS14Y00523S','Y13-14CBBW386'),
        (74,'CMSS14Y00524S','Y13-14CBBW387'),
        (75,'CMSS14Y00525S','Y13-14CBBW388'),
        (76,'CMSS14Y00526S','Y13-14CBBW389'),
        (77,'CMSS14Y00527S','Y13-14CBBW390'),
        (78,'CMSS14Y00528S','Y13-14CBBW391'),
        (79,'CMSS14Y00529S','Y13-14CBBW392'),
        (80,'CMSS14Y00530S','Y13-14CBBW392'),
        (81,'CMSS14Y00531S','Y13-14CBBW393'),
        (82,'CMSS14Y00532S','Y13-14CBBW394'),
        (83,'CMSS14Y00533S','Y13-14CBBW395'),
        (84,'CMSS14Y00534S','Y13-14CBBW396'),
        (85,'CMSS14Y00535S','Y13-14CBBW396'),
        (86,'CMSS14Y00536S','Y13-14CBBW397'),
        (87,'CMSS14Y00537S','Y13-14CBBW398'),
        (88,'CMSS14Y00538S','Y13-14CBBW399'),
        (89,'CMSS14Y00539S','Y13-14CBBW400'),
        (90,'CMSS14Y00540S','Y13-14CBBW401'),
        (91,'CMSS14Y00541S','Y13-14CBBW401'),
        (92,'CMSS14Y00542S','Y13-14CBBW401'),
        (93,'CMSS14Y00543S','Y13-14CBBW402'),
        (94,'CMSS14Y00544S','Y13-14CBBW402'),
        (95,'CMSS14Y00545S','Y13-14CBBW403'),
        (96,'CMSS14Y00546S','Y13-14CBBW403'),
        (97,'CMSS14Y00547S','Y13-14CBBW404'),
        (98,'CMSS14Y00548S','Y13-14CBBW404'),
        (99,'CMSS14Y00549S','Y13-14CBBW405'),
        (100,'CMSS14Y00550S','Y13-14CBBW406'),
        (101,'CMSS14Y00551S','Y13-14CBBW406'),
        (102,'CMSS14Y00552S','Y13-14CBBW407'),
        (103,'CMSS14Y00553S','Y13-14CBBW407'),
        (104,'CMSS14Y00554S','Y13-14CBBW408'),
        (105,'CMSS14Y00555S','Y13-14CBBW409'),
        (106,'CMSS14Y00556S','Y13-14CBBW410'),
        (107,'CMSS14Y00557S','Y13-14CBBW411'),
        (108,'CMSS14Y00558S','Y13-14CBBW412'),
        (109,'CMSS14Y00559S','Y13-14CBBW413'),
        (110,'CMSS14Y00560S','Y13-14CBBW414'),
        (111,'CMSS14Y00561S','Y13-14CBBW414'),
        (112,'CMSS14Y00562S','Y13-14CBBW415'),
        (113,'CMSS14Y00563S','Y13-14CBBW415'),
        (114,'CMSS14Y00564S','Y13-14CBBW415'),
        (115,'CMSS14Y00565S','Y13-14CBBW415'),
        (116,'CMSS14Y00566S','Y13-14CBBW415'),
        (117,'CMSS14Y00567S','Y13-14CBBW416'),
        (118,'CMSS14Y00568S','Y13-14CBBW416'),
        (119,'CMSS14Y00569S','Y13-14CBBW417'),
        (120,'CMSS14Y00570S','Y13-14CBBW417'),
        (121,'CMSS14Y00571S','Y13-14CBBW418'),
        (122,'CMSS14Y00572S','Y13-14CBBW419'),
        (123,'CMSS14Y00573S','Y13-14CBBW420'),
        (124,'CMSS14Y00574S','Y13-14CBBW421'),
        (125,'CMSS14Y00575S','Y13-14CBBW422'),
        (126,'CMSS14Y00576S','Y13-14CBBW423'),
        (127,'CMSS14Y00577S','Y13-14CBBW424'),
        (128,'CMSS14Y00578S','Y13-14CBBW425'),
        (129,'CMSS14Y00579S','Y13-14CBBW426'),
        (130,'CMSS14Y00580S','Y13-14CBBW427'),
        (131,'CMSS14Y00581S','Y13-14CBBW428'),
        (132,'CMSS14Y00582S','Y13-14CBBW429'),
        (133,'CMSS14Y00583S','Y13-14CBBW430'),
        (134,'CMSS14Y00584S','Y13-14CBBW431'),
        (135,'CMSS14Y00585S','Y13-14CBBW431'),
        (136,'CMSS14Y00586S','Y13-14CBBW432'),
        (137,'CMSS14Y00587S','Y13-14CBBW432'),
        (138,'CMSS14Y00588S','Y13-14CBBW433'),
        (139,'CMSS14Y00589S','Y13-14CBBW434'),
        (140,'CMSS14Y00590S','Y13-14CBBW434'),
        (141,'CMSS14Y00591S','Y13-14CBBW435'),
        (142,'CMSS14Y00592S','Y13-14CBBW436'),
        (143,'CMSS14Y00593S','Y13-14CBBW437'),
        (144,'CMSS14Y00594S','Y13-14CBBW438'),
        (145,'CMSS14Y00595S','Y13-14CBBW439'),
        (146,'CMSS14Y00596S','Y13-14CBBW440'),
        (147,'CMSS14Y00597S','Y13-14CBBW441'),
        (148,'CMSS14Y00598S','Y13-14CBBW442'),
        (149,'CMSS14Y00599S','Y13-14CBBW443'),
        (150,'CMSS14Y00600S','Y13-14CBBW444')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'k.khadija'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.occurrence



-- populate wheat occurrence for F1SIMPLEBW-4
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'F1SIMPLEBW-4_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'F1SIMPLEBW-4'
    AND geo.geospatial_object_code = 'EB'
    AND person.username = 'k.khadija'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'F1SIMPLEBW-4_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in place.geospatial_object



-- populate wheat planting area for F1SIMPLEBW-4
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'F1SIMPLEBW-4_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'F1SIMPLEBW-4_LOC1'
--rollback ;


--changeset postgres:populate_wheat_f1_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location



-- populate wheat location for F1SIMPLEBW-4
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'F1SIMPLEBW-4_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'k.khadija'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'F1SIMPLEBW-4_LOC1'
    AND site.geospatial_object_code = 'EB'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'F1SIMPLEBW-4_LOC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.plot



-- populate wheat plots for F1SIMPLEBW-4
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS14Y00451S'),
        (2,1,2,'CMSS14Y00452S'),
        (3,1,3,'CMSS14Y00453S'),
        (4,1,4,'CMSS14Y00454S'),
        (5,1,5,'CMSS14Y00455S'),
        (6,1,6,'CMSS14Y00456S'),
        (7,1,7,'CMSS14Y00457S'),
        (8,1,8,'CMSS14Y00458S'),
        (9,1,9,'CMSS14Y00459S'),
        (10,1,10,'CMSS14Y00460S'),
        (11,1,11,'CMSS14Y00461S'),
        (12,1,12,'CMSS14Y00462S'),
        (13,1,13,'CMSS14Y00463S'),
        (14,1,14,'CMSS14Y00464S'),
        (15,1,15,'CMSS14Y00465S'),
        (16,1,16,'CMSS14Y00466S'),
        (17,1,17,'CMSS14Y00467S'),
        (18,1,18,'CMSS14Y00468S'),
        (19,1,19,'CMSS14Y00469S'),
        (20,1,20,'CMSS14Y00470S'),
        (21,1,21,'CMSS14Y00471S'),
        (22,1,22,'CMSS14Y00472S'),
        (23,1,23,'CMSS14Y00473S'),
        (24,1,24,'CMSS14Y00474S'),
        (25,1,25,'CMSS14Y00475S'),
        (26,1,26,'CMSS14Y00476S'),
        (27,1,27,'CMSS14Y00477S'),
        (28,1,28,'CMSS14Y00478S'),
        (29,1,29,'CMSS14Y00479S'),
        (30,1,30,'CMSS14Y00480S'),
        (31,1,31,'CMSS14Y00481S'),
        (32,1,32,'CMSS14Y00482S'),
        (33,1,33,'CMSS14Y00483S'),
        (34,1,34,'CMSS14Y00484S'),
        (35,1,35,'CMSS14Y00485S'),
        (36,1,36,'CMSS14Y00486S'),
        (37,1,37,'CMSS14Y00487S'),
        (38,1,38,'CMSS14Y00488S'),
        (39,1,39,'CMSS14Y00489S'),
        (40,1,40,'CMSS14Y00490S'),
        (41,1,41,'CMSS14Y00491S'),
        (42,1,42,'CMSS14Y00492S'),
        (43,1,43,'CMSS14Y00493S'),
        (44,1,44,'CMSS14Y00494S'),
        (45,1,45,'CMSS14Y00495S'),
        (46,1,46,'CMSS14Y00496S'),
        (47,1,47,'CMSS14Y00497S'),
        (48,1,48,'CMSS14Y00498S'),
        (49,1,49,'CMSS14Y00499S'),
        (50,1,50,'CMSS14Y00500S'),
        (51,1,51,'CMSS14Y00501S'),
        (52,1,52,'CMSS14Y00502S'),
        (53,1,53,'CMSS14Y00503S'),
        (54,1,54,'CMSS14Y00504S'),
        (55,1,55,'CMSS14Y00505S'),
        (56,1,56,'CMSS14Y00506S'),
        (57,1,57,'CMSS14Y00507S'),
        (58,1,58,'CMSS14Y00508S'),
        (59,1,59,'CMSS14Y00509S'),
        (60,1,60,'CMSS14Y00510S'),
        (61,1,61,'CMSS14Y00511S'),
        (62,1,62,'CMSS14Y00512S'),
        (63,1,63,'CMSS14Y00513S'),
        (64,1,64,'CMSS14Y00514S'),
        (65,1,65,'CMSS14Y00515S'),
        (66,1,66,'CMSS14Y00516S'),
        (67,1,67,'CMSS14Y00517S'),
        (68,1,68,'CMSS14Y00518S'),
        (69,1,69,'CMSS14Y00519S'),
        (70,1,70,'CMSS14Y00520S'),
        (71,1,71,'CMSS14Y00521S'),
        (72,1,72,'CMSS14Y00522S'),
        (73,1,73,'CMSS14Y00523S'),
        (74,1,74,'CMSS14Y00524S'),
        (75,1,75,'CMSS14Y00525S'),
        (76,1,76,'CMSS14Y00526S'),
        (77,1,77,'CMSS14Y00527S'),
        (78,1,78,'CMSS14Y00528S'),
        (79,1,79,'CMSS14Y00529S'),
        (80,1,80,'CMSS14Y00530S'),
        (81,1,81,'CMSS14Y00531S'),
        (82,1,82,'CMSS14Y00532S'),
        (83,1,83,'CMSS14Y00533S'),
        (84,1,84,'CMSS14Y00534S'),
        (85,1,85,'CMSS14Y00535S'),
        (86,1,86,'CMSS14Y00536S'),
        (87,1,87,'CMSS14Y00537S'),
        (88,1,88,'CMSS14Y00538S'),
        (89,1,89,'CMSS14Y00539S'),
        (90,1,90,'CMSS14Y00540S'),
        (91,1,91,'CMSS14Y00541S'),
        (92,1,92,'CMSS14Y00542S'),
        (93,1,93,'CMSS14Y00543S'),
        (94,1,94,'CMSS14Y00544S'),
        (95,1,95,'CMSS14Y00545S'),
        (96,1,96,'CMSS14Y00546S'),
        (97,1,97,'CMSS14Y00547S'),
        (98,1,98,'CMSS14Y00548S'),
        (99,1,99,'CMSS14Y00549S'),
        (100,1,100,'CMSS14Y00550S'),
        (101,1,101,'CMSS14Y00551S'),
        (102,1,102,'CMSS14Y00552S'),
        (103,1,103,'CMSS14Y00553S'),
        (104,1,104,'CMSS14Y00554S'),
        (105,1,105,'CMSS14Y00555S'),
        (106,1,106,'CMSS14Y00556S'),
        (107,1,107,'CMSS14Y00557S'),
        (108,1,108,'CMSS14Y00558S'),
        (109,1,109,'CMSS14Y00559S'),
        (110,1,110,'CMSS14Y00560S'),
        (111,1,111,'CMSS14Y00561S'),
        (112,1,112,'CMSS14Y00562S'),
        (113,1,113,'CMSS14Y00563S'),
        (114,1,114,'CMSS14Y00564S'),
        (115,1,115,'CMSS14Y00565S'),
        (116,1,116,'CMSS14Y00566S'),
        (117,1,117,'CMSS14Y00567S'),
        (118,1,118,'CMSS14Y00568S'),
        (119,1,119,'CMSS14Y00569S'),
        (120,1,120,'CMSS14Y00570S'),
        (121,1,121,'CMSS14Y00571S'),
        (122,1,122,'CMSS14Y00572S'),
        (123,1,123,'CMSS14Y00573S'),
        (124,1,124,'CMSS14Y00574S'),
        (125,1,125,'CMSS14Y00575S'),
        (126,1,126,'CMSS14Y00576S'),
        (127,1,127,'CMSS14Y00577S'),
        (128,1,128,'CMSS14Y00578S'),
        (129,1,129,'CMSS14Y00579S'),
        (130,1,130,'CMSS14Y00580S'),
        (131,1,131,'CMSS14Y00581S'),
        (132,1,132,'CMSS14Y00582S'),
        (133,1,133,'CMSS14Y00583S'),
        (134,1,134,'CMSS14Y00584S'),
        (135,1,135,'CMSS14Y00585S'),
        (136,1,136,'CMSS14Y00586S'),
        (137,1,137,'CMSS14Y00587S'),
        (138,1,138,'CMSS14Y00588S'),
        (139,1,139,'CMSS14Y00589S'),
        (140,1,140,'CMSS14Y00590S'),
        (141,1,141,'CMSS14Y00591S'),
        (142,1,142,'CMSS14Y00592S'),
        (143,1,143,'CMSS14Y00593S'),
        (144,1,144,'CMSS14Y00594S'),
        (145,1,145,'CMSS14Y00595S'),
        (146,1,146,'CMSS14Y00596S'),
        (147,1,147,'CMSS14Y00597S'),
        (148,1,148,'CMSS14Y00598S'),
        (149,1,149,'CMSS14Y00599S'),
        (150,1,150,'CMSS14Y00600S')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'F1SIMPLEBW-4_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'F1SIMPLEBW-4_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'k.khadija'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-4_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.planting_instruction



-- populate wheat planting instructions for F1SIMPLEBW-4
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'k.khadija'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.package_log



-- populate wheat package logs for F1SIMPLEBW-4
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'k.khadija'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery package_log_id in experiment.planting_instruction



-- populate wheat package log references for F1SIMPLEBW-4
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-4_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross



-- populate wheat crosses for F1SIMPLEBW-4
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'k.khadija'
WHERE
    expt.experiment_name = 'F1SIMPLEBW-4'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-4'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross_parent



-- populate wheat cross parents for F1SIMPLEBW-4
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'k.khadija'
        WHERE
            expt.experiment_name = 'F1SIMPLEBW-4'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-4'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.location_occurrence_group_part_4 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location_occurrence_group



-- populate wheat location occurrence groups for F1SIMPLEBW-4
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'F1SIMPLEBW-4_LOC1'
    AND occ.occurrence_name = 'F1SIMPLEBW-4_OCC1'
    AND prs.username = 'k.khadija'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'F1SIMPLEBW-4_LOC1'
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-4_OCC1'
--rollback ;
