--liquibase formatted sql

--changeset postgres:add_variable_plot_count_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-147 Add variable PLOT_COUNT_CONT



--add variable
INSERT INTO
    master.variable
        (
            id,abbrev,label,name,data_type,not_null,type,status,
            display_name,ontology_reference,bibliographical_reference,property_id,
            method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
            creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
            usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
            field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
        )
VALUES
    (
        520,
        'PLOT_COUNT_CONT',
        'PLOT_COUNT',
        'plot count',
        'integer',
        FALSE,
        'metadata',
        'active',
        'Plot Count',
        NULL,
        NULL,
        388,
        502,
        499,
        NULL,
        NULL,
        NULL,
        '2014-04-04 14:45:43.113163',
        1,
        '2014-04-04 15:37:09.168561',
        1,
        NULL,
        TRUE,
        NULL,
        NULL,
        'study',
        NULL,
        FALSE,
        NULL,
        FALSE,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    );

SELECT SETVAL('master.variable_id_seq', COALESCE(MAX(id), 1)) FROM master.variable;



--rollback DELETE FROM master.variable WHERE abbrev='PLOT_COUNT_CONT';
--rollback SELECT SETVAL('master.variable_id_seq', COALESCE(MAX(id), 1)) FROM master.variable;


--changeset postgres:delete_variable_plot_count_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Delete variable PLOT_COUNT_CONT


-- delete variable
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
SET session_replication_role = replica;

DELETE FROM master.variable WHERE abbrev = 'PLOT_COUNT_CONT';

--ALTER TABLE master.variable ENABLE TRIGGER ALL;
SET session_replication_role = origin;

ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;



-- revert changes
--rollback INSERT INTO
--rollback     master.variable
--rollback         (
--rollback             id,abbrev,label,name,data_type,not_null,type,status,
--rollback             display_name,ontology_reference,bibliographical_reference,property_id,
--rollback             method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
--rollback             creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
--rollback             usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
--rollback             field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
--rollback         )
--rollback VALUES
--rollback     (
--rollback         520,
--rollback         'PLOT_COUNT_CONT',
--rollback         'PLOT_COUNT',
--rollback         'plot count',
--rollback         'integer',
--rollback         FALSE,
--rollback         'metadata',
--rollback         'active',
--rollback         'Plot Count',
--rollback         NULL,
--rollback         NULL,
--rollback         388,
--rollback         502,
--rollback         499,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         '2014-04-04 14:45:43.113163',
--rollback         1,
--rollback         '2014-04-04 15:37:09.168561',
--rollback         1,
--rollback         NULL,
--rollback         TRUE,
--rollback         NULL,
--rollback         NULL,
--rollback         'study',
--rollback         NULL,
--rollback         FALSE,
--rollback         NULL,
--rollback         FALSE,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL
--rollback     );



--changeset postgres:reinsert_variable_plot_count_cont context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Re-insert variable PLOT_COUNT_CONT



--add variable
INSERT INTO
    master.variable
        (
            id,abbrev,label,name,data_type,not_null,type,status,
            display_name,ontology_reference,bibliographical_reference,property_id,
            method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
            creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
            usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
            field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
        )
VALUES
    (
        520,
        'PLOT_COUNT_CONT',
        'PLOT_COUNT',
        'plot count',
        'integer',
        FALSE,
        'metadata',
        'active',
        'Plot Count',
        NULL,
        NULL,
        388,
        502,
        499,
        NULL,
        NULL,
        NULL,
        '2014-04-04 14:45:43.113163',
        1,
        '2014-04-04 15:37:09.168561',
        1,
        NULL,
        TRUE,
        NULL,
        NULL,
        'study',
        NULL,
        FALSE,
        NULL,
        FALSE,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    );



-- revert changes
--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'PLOT_COUNT_CONT';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
--rollback --ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;
