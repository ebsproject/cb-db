--liquibase formatted sql

--changeset postgres:update_null_program_id_in_germplasm.package context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1627 Update null program_id in germplasm.package



ALTER TABLE germplasm.package DISABLE TRIGGER package_update_package_document_tgr;

UPDATE 
	germplasm.package AS pkg
SET 
	program_id=sd.program_id
FROM
	germplasm.seed AS sd
WHERE
	pkg.seed_id = sd.id
AND
	pkg.program_id IS NULL;
	
ALTER TABLE germplasm.package ENABLE TRIGGER package_update_package_document_tgr;



--rollback SELECT NULL;