--liquibase formatted sql

--changeset postgres:remove_ht_cont_formula_and_formula_parameter context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1382 Remove HT_CONT formula and formula_parameter



UPDATE master.method SET formula_id=NULL WHERE abbrev='HT1_CONT_METHOD';
UPDATE master.method SET formula_id=NULL WHERE abbrev='HT2_CONT_METHOD';
UPDATE master.method SET formula_id=NULL WHERE abbrev='HT3_CONT_METHOD';
UPDATE master.method SET formula_id=NULL WHERE abbrev='HT4_CONT_METHOD';
UPDATE master.method SET formula_id=NULL WHERE abbrev='HT5_CONT_METHOD';
UPDATE master.method SET formula_id=NULL WHERE abbrev='HT6_CONT_METHOD';

UPDATE master.formula SET is_void=true WHERE formula='HT1_CONT = PNL1_CONT + CML1_CONT';
UPDATE master.formula SET is_void=true WHERE formula='HT2_CONT = PNL2_CONT + CML2_CONT';
UPDATE master.formula SET is_void=true WHERE formula='HT3_CONT = PNL3_CONT + CML3_CONT';
UPDATE master.formula SET is_void=true WHERE formula='HT4_CONT = PNL4_CONT + CML4_CONT';
UPDATE master.formula SET is_void=true WHERE formula='HT5_CONT = PNL5_CONT + CML5_CONT';
UPDATE master.formula SET is_void=true WHERE formula='HT6_CONT = PNL6_CONT + CML6_CONT';



--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht1_cont(pnl1_cont, cml1_cont)') WHERE abbrev='HT1_CONT_METHOD';
--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht2_cont(pnl2_cont, cml2_cont)') WHERE abbrev='HT2_CONT_METHOD';
--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht3_cont(pnl3_cont, cml3_cont)') WHERE abbrev='HT3_CONT_METHOD';
--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht4_cont(pnl4_cont, cml4_cont)') WHERE abbrev='HT4_CONT_METHOD';
--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht5_cont(pnl5_cont, cml5_cont)') WHERE abbrev='HT5_CONT_METHOD';
--rollback UPDATE master.method SET formula_id=(SELECT id FROM master.formula WHERE function_name='master.formula_ht6_cont(pnl6_cont, cml6_cont)') WHERE abbrev='HT6_CONT_METHOD';

--rollback UPDATE master.formula SET is_void=false WHERE formula='HT1_CONT = PNL1_CONT + CML1_CONT';
--rollback UPDATE master.formula SET is_void=false WHERE formula='HT2_CONT = PNL2_CONT + CML2_CONT';
--rollback UPDATE master.formula SET is_void=false WHERE formula='HT3_CONT = PNL3_CONT + CML3_CONT';
--rollback UPDATE master.formula SET is_void=false WHERE formula='HT4_CONT = PNL4_CONT + CML4_CONT';
--rollback UPDATE master.formula SET is_void=false WHERE formula='HT5_CONT = PNL5_CONT + CML5_CONT';
--rollback UPDATE master.formula SET is_void=false WHERE formula='HT6_CONT = PNL6_CONT + CML6_CONT';