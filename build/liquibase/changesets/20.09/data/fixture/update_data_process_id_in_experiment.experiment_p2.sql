--liquibase formatted sql

--changeset postgres:update_data_process_id_in_experiment.experiment_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1514 Update data_process_id in experiment.experiment p2



UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0008';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0029';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0064';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0065';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0122';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0159';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0160';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0198';

UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS') WHERE experiment_code='EXPT0102';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS') WHERE experiment_code='EXPT0103';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS') WHERE experiment_code='EXPT0104';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS') WHERE experiment_code='EXPT0176';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS') WHERE experiment_code='EXPT0224';

UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0009';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0013';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0014';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0015';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0016';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0030';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0031';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0032';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0033';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0034';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0043';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0044';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0045';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0066';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0067';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0068';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0089';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0090';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0091';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0092';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0093';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0123';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0124';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0125';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0126';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0127';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0144';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0145';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0146';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0161';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0162';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0199';
UPDATE experiment.experiment SET data_process_id=(SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS') WHERE experiment_code='EXPT0200';



--rollback SELECT NULL;