--liquibase formatted sql

--changeset postgres:populate_maize_f4_nursery_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.experiment



-- populate maize experiment AF4-11
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GMP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F4') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'KE_PROJECT') AS project_id,
    2019 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2019A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'AF4-11' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'alexandra.reid') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'alexandra.reid') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'AF4-11'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.entry_list



-- populate maize entry list AF4-11_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'AF4-11_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'AF4-11') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'alexandra.reid') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'AF4-11_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.entry



-- populate maize entries for AF4-11
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number::integer,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES        
        ('1','(FBM239/RHM52)-B-17-1','ST-KIB-18B-20-191'),
        ('2','(FBM239/RHM52)-B-39-1','ST-KIB-18B-20-192'),
        ('3','(FBM239/RHM52)-B-79-1','ST-KIB-18B-20-194'),
        ('4','(FBM239/RHM52)-B-134-1','ST-KIB-18B-20-199'),
        ('5','(FBM239/LLTR)-B-79-1','ST-KIB-18B-20-201'),
        ('6','(FBM239/LLTR)-B-98-1','ST-KIB-18B-20-203'),
        ('7','(FBM239/RHHB9)-B-109-1','ST-KIB-18B-20-208'),
        ('8','(FBM239/RHHB9)-B-123-1','ST-KIB-18B-20-216'),
        ('9','(FBM239/RHHB9)-B-125-1','ST-KIB-18B-20-218'),
        ('10','(FBM239/RHHB9)-B-126-1','ST-KIB-18B-20-220'),
        ('11','(FBM239/RHHB9)-B-131-1','ST-KIB-18B-20-222'),
        ('12','(FBM239/RHHB9)-B-135-1','ST-KIB-18B-20-224'),
        ('13','(FBM239/RHHB9)-B-158-1','ST-KIB-18B-20-226'),
        ('14','(FBM239/RHHB9)-B-171-1','ST-KIB-18B-20-227'),
        ('15','(FBM239/RHT11)-B-13-1','ST-KIB-18B-20-230'),
        ('16','(FBM239/RHT11)-B-22-1','ST-KIB-18B-20-233'),
        ('17','(FBM239/RHT11)-B-71-1','ST-KIB-18B-20-236'),
        ('18','(FBM239/RHT11)-B-82-1','ST-KIB-18B-20-241'),
        ('19','(FBM239/RHT11)-B-89-1','ST-KIB-18B-20-242'),
        ('20','(FBM239/RHT11)-B-101-1','ST-KIB-18B-20-243'),
        ('21','(FBM239/RHT11)-B-107-1','ST-KIB-18B-20-245'),
        ('22','(FBM239/LLTR)-B-29-1','ST-KIB-18B-20-253'),
        ('23','(FBM239/LLTR)-B-55-1','ST-KIB-18B-20-256'),
        ('24','(FBM239/LLTR)-B-74-1','ST-KIB-18B-20-258'),
        ('25','(FBM239/LLTR)-B-93-1','ST-KIB-18B-20-265'),
        ('26','(FBM239/LLTR)-B-97-1','ST-KIB-18B-20-266')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'AF4-11_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'alexandra.reid'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'AF4-11_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.occurrence



-- populate maize occurrence for AF4-11
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'AF4-11_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'AF4-11'
    AND geo.geospatial_object_code = 'KI'
    AND person.username = 'alexandra.reid'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'AF4-11_OCC1'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in place.geospatial_object



-- populate maize planting area for AF4-11
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'AF4-11_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'AF4-11_LOC1'
--rollback ;


--changeset postgres:populate_maize_f4_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.location



-- populate maize location for AF4-11
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'AF4-11_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'alexandra.reid'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'AF4-11_LOC1'
    AND site.geospatial_object_code = 'KI'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'AF4-11_LOC1'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.plot



-- populate maize plots for AF4-11
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number::integer AS plot_number,
    'plot' AS plot_type,
    t.rep::integer AS rep,
    1 AS design_x,
    t.plot_number::integer AS design_y,
    1 AS pa_x,
    t.plot_number::integer AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        ('1','1','1','(FBM239/RHM52)-B-17-1'),
        ('2','1','2','(FBM239/RHM52)-B-39-1'),
        ('3','1','3','(FBM239/RHM52)-B-79-1'),
        ('4','1','4','(FBM239/RHM52)-B-134-1'),
        ('5','1','5','(FBM239/LLTR)-B-79-1'),
        ('6','1','6','(FBM239/LLTR)-B-98-1'),
        ('7','1','7','(FBM239/RHHB9)-B-109-1'),
        ('8','1','8','(FBM239/RHHB9)-B-123-1'),
        ('9','1','9','(FBM239/RHHB9)-B-125-1'),
        ('10','1','10','(FBM239/RHHB9)-B-126-1'),
        ('11','1','11','(FBM239/RHHB9)-B-131-1'),
        ('12','1','12','(FBM239/RHHB9)-B-135-1'),
        ('13','1','13','(FBM239/RHHB9)-B-158-1'),
        ('14','1','14','(FBM239/RHHB9)-B-171-1'),
        ('15','1','15','(FBM239/RHT11)-B-13-1'),
        ('16','1','16','(FBM239/RHT11)-B-22-1'),
        ('17','1','17','(FBM239/RHT11)-B-71-1'),
        ('18','1','18','(FBM239/RHT11)-B-82-1'),
        ('19','1','19','(FBM239/RHT11)-B-89-1'),
        ('20','1','20','(FBM239/RHT11)-B-101-1'),
        ('21','1','21','(FBM239/RHT11)-B-107-1'),
        ('22','1','22','(FBM239/LLTR)-B-29-1'),
        ('23','1','23','(FBM239/LLTR)-B-55-1'),
        ('24','1','24','(FBM239/LLTR)-B-74-1'),
        ('25','1','25','(FBM239/LLTR)-B-93-1'),
        ('26','1','26','(FBM239/LLTR)-B-97-1')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number::integer = ent.entry_number::integer
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'AF4-11_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'AF4-11_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'AF4-11_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'alexandra.reid'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'AF4-11_OCC1'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.planting_instruction



-- populate maize planting instructions for AF4-11
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'alexandra.reid'
WHERE
    entlist.entry_list_name = 'AF4-11_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'AF4-11_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in germplasm.package_log



-- populate maize package logs for AF4-11
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'alexandra.reid'
WHERE
    entlist.entry_list_name = 'AF4-11_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'AF4-11_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_f4_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery package_log_id in experiment.planting_instruction



-- populate maize package log references for AF4-11
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'AF4-11_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'AF4-11_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_maize_f4_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in germplasm.cross



-- populate maize crosses for AF4-11
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'alexandra.reid'
WHERE
    expt.experiment_name = 'AF4-11'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'AF4-11'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in germplasm.cross_parent



-- populate maize cross parents for AF4-11
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'alexandra.reid'
        WHERE
            expt.experiment_name = 'AF4-11'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'AF4-11'
--rollback ;



--changeset postgres:populate_maize_f4_nursery_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize F4 nursery in experiment.location_occurrence_group



-- populate maize location occurrence groups for AF4-11
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'AF4-11_LOC1'
    AND occ.occurrence_name = 'AF4-11_OCC1'
    AND prs.username = 'alexandra.reid'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'AF4-11_LOC1'
--rollback     AND occ.occurrence_name = 'AF4-11_OCC1'
--rollback ;
