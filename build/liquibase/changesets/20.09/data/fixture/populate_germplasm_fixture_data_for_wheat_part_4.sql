--liquibase formatted sql

--changeset postgres:populate_germplasm.package_for_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1320 Populate germplasm.package for wheat



-- populate wheat packages
INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, creator_id)
SELECT
    germplasm.generate_code('package') AS package_code,
    s.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    s.id AS seed_id,
    '1' AS creator_id
FROM
    germplasm.seed AS s
    INNER JOIN germplasm.germplasm AS g
        ON s.germplasm_id = g.id
    INNER JOIN tenant.crop AS c
        ON g.crop_id = c.id
WHERE
    c.crop_code = 'WHEAT'
;



-- revert changes
--rollback --ALTER TABLE germplasm.package DISABLE TRIGGER ALL;
--rollback 
--rollback DELETE FROM
--rollback     germplasm.package AS p
--rollback USING
--rollback     germplasm.seed AS s
--rollback     INNER JOIN germplasm.germplasm AS g
--rollback         ON s.germplasm_id = g.id
--rollback     INNER JOIN tenant.crop AS c
--rollback         ON g.crop_id = c.id
--rollback WHERE
--rollback     s.id = p.seed_id
--rollback     and c.crop_code = 'WHEAT'
--rollback ;
--rollback 
--rollback --ALTER TABLE germplasm.package ENABLE TRIGGER ALL;
