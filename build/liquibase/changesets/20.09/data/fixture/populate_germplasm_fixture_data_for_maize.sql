--liquibase formatted sql

--changeset postgres:populate_taxonomy_fixture_data_for_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1432 Populate taxonomy fixture data for maize



-- add taxonomy for maize
INSERT INTO
    germplasm.taxonomy (taxon_id, taxonomy_name, crop_id, creator_id)
VALUES 
    (
        4570, 
        'Zea mays L. subsp. mays',
        (SELECT id FROM tenant.crop where crop_code='MAIZE'),
        1
    );



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.taxonomy
--rollback WHERE
--rollback     taxonomy_name = 'Zea mays L. subsp. mays'
--rollback     AND taxon_id = '4570'
--rollback ;



--changeset postgres:populate_germplasm_fixture_data_for_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1432 Populate germplasm fixture data for maize



INSERT INTO
    germplasm.germplasm (designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type,germplasm_normalized_name,crop_id,taxonomy_id,creator_id)
SELECT
    g.*,
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxonomy_name = 'Zea mays L. subsp. mays'
            AND taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
        ('(ABDHL0089/ABDHL120668)@195-B','ABDHL0089/ABDHL120668','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABDHL120668)@195-B'),
        ('(ABDHL0089/ABDHL120668)@19-B','ABDHL0089/ABDHL120668','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABDHL120668)@19-B'),
        ('(ABDHL0089/ABDHL120918)@200-B','ABDHL0089/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABDHL120918)@200-B'),
        ('(ABDHL0089/ABDHL120918)@52-B','ABDHL0089/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABDHL120918)@52-B'),
        ('(ABDHL0089/ABDHL120918)@63-B','ABDHL0089/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABDHL120918)@63-B'),
        ('(ABDHL0089/ABLTI0136)@220-B','ABDHL0089/ABLTI0136','DH_line','DH_line','fixed','pedigree','(ABDHL0089/ABLTI0136)@220-B'),
        ('ABDHL0221','AB262/GKM665','DH_line','DH_line','fixed','line_code','ABDHL0221'),
        ('ABDHL120312','BC491/GKM469','DH_line','DH_line','fixed','line_code','ABDHL120312'),
        ('ABDHL120918','CD831/GKM353','DH_line','DH_line','fixed','line_code','ABDHL120918'),
        ('ABDHL163943','DE708/GKM732','DH_line','DH_line','fixed','line_code','ABDHL163943'),
        ('ABDHL164302','EF305/GKM248','DH_line','DH_line','fixed','line_code','ABDHL164302'),
        ('ABDHL164665','FG451/GKM536','DH_line','DH_line','fixed','line_code','ABDHL164665'),
        ('ABDHL164672','GH375/GKM705','DH_line','DH_line','fixed','line_code','ABDHL164672'),
        ('ABDHL165891','HH536/GKM656','DH_line','DH_line','fixed','line_code','ABDHL165891'),
        ('ABLMARSI0022','JK244/GKM542','inbred_line','inbred_line','fixed','line_code','ABLMARSI0022'),
        ('ABLMARSI0037','KL477/GKM845','inbred_line','inbred_line','fixed','line_code','ABLMARSI0037'),
        ('ABLTI0137','AB246/GKM585','inbred_line','inbred_line','fixed','line_code','ABLTI0137'),
        ('(ABLTI0139/ABDHL120918)@246-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@246-B'),
        ('(ABLTI0139/ABDHL120918)@299-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@299-B'),
        ('(ABLTI0139/ABDHL120918)@373-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@373-B'),
        ('(ABLTI0139/ABDHL120918)@393-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@393-B'),
        ('(ABLTI0139/ABDHL120918)@479-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@479-B'),
        ('(ABLTI0139/ABDHL120918)@553-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@553-B'),
        ('(ABLTI0139/ABDHL120918)@653-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@653-B'),
        ('(ABLTI0139/ABDHL120918)@95-B','ABLTI0139/ABDHL120918','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABDHL120918)@95-B'),
        ('(ABLTI0139/ABLTI0335)@107-B','ABLTI0139/ABLTI0335','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABLTI0335)@107-B'),
        ('(ABLTI0139/ABLTI0335)@14-B','ABLTI0139/ABLTI0335','DH_line','DH_line','fixed','pedigree','(ABLTI0139/ABLTI0335)@14-B'),
        ('(ABLTI0139/CML543)@140-B','ABLTI0139/CML543','DH_line','DH_line','fixed','pedigree','(ABLTI0139/CML543)@140-B'),
        ('ABLTI0330','BC619/GKM437','inbred_line','inbred_line','fixed','line_code','ABLTI0330'),
        ('ABSBL10020','CD637/GKM759','inbred_line','inbred_line','fixed','line_code','ABSBL10020'),
        ('ABSBL10060','DE114/GKM188','inbred_line','inbred_line','fixed','line_code','ABSBL10060'),
        ('CLRCY034','Unknown','DH_line','DH_line','fixed','Line_code','CLRCY034'),
        ('CLYN261','Unknown','DH_line','DH_line','fixed','Line_code','CLYN261'),
        ('CML204','XGG893/YLP564','inbred_line','inbred_line','fixed','CML_code','CML204'),
        ('CML442','XGG379/YLP609','inbred_line','inbred_line','fixed','CML_code','CML442'),
        ('((CML442/KS23-6)-B)@103-B','CML442/KS23-6','DH_line','DH_line','fixed','pedigree','((CML442/KS23-6)-B)@103-B'),
        ('CML444','XGG496/YLP431','inbred_line','inbred_line','fixed','CML_code','CML444'),
        ('CML463','G9A-C6','DH_line','DH_line','fixed','CML_code','CML463'),
        ('CML464','XGG795/YLP891','inbred_line','DH_line','fixed','CML_code','CML464'),
        ('CML494','XGG725/YLP939','inbred_line','inbred_line','fixed','CML_code','CML494'),
        ('(CML494/OFP9)-12-2-1-1-1-B*4','CML494/OFP9','inbred_line','inbred_line','fixed','pedigree','(CML494/OFP9)-12-2-1-1-1-B*4'),
        ('(CML494/OFP9)-12-2-1-1-2-B*5','CML494/OFP9','inbred_line','inbred_line','fixed','pedigree','(CML494/OFP9)-12-2-1-1-2-B*5'),
        ('CML495','PNVABCOSD/NPH-28-1','DH_line','DH_line','fixed','CML_code','CML495'),
        ('CML536','XGG491/YLP342','inbred_line','inbred_line','fixed','CML_code','CML536'),
        ('((CML537/KS523-5)-B)@10-B','CML537/KS523-5','DH_line','DH_line','fixed','pedigree','((CML537/KS523-5)-B)@10-B'),
        ('CML539','XGG523/YLP492','inbred_line','inbred_line','fixed','CML_code','CML539'),
        ('CML543','XGG661/YLP665','inbred_line','inbred_line','fixed','CML_code','CML543'),
        ('CML547','DRB','DH_line','DH_line','fixed','CML_code','CML547'),
        ('EEL822/LMC243','EEL822/LMC243','F1','F1','not_fixed','pedigree','EEL822/LMC243'),
        ('EEL895/IBL444','EEL895/IBL444','F1','F1','not_fixed','pedigree','EEL895/IBL444'),
        ('(FBM239/LLTR)-B-29-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-29-1'),
        ('(FBM239/LLTR)-B-55-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-55-1'),
        ('(FBM239/LLTR)-B-74-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-74-1'),
        ('(FBM239/LLTR)-B-79-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-79-1'),
        ('(FBM239/LLTR)-B-93-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-93-1'),
        ('(FBM239/LLTR)-B-97-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-97-1'),
        ('(FBM239/LLTR)-B-98-1','FBM239/LLTR','F4','segregating_line','not_fixed','pedigree','(FBM239/LLTR)-B-98-1'),
        ('(FBM239/RHHB9)-B-109-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-109-1'),
        ('(FBM239/RHHB9)-B-123-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-123-1'),
        ('(FBM239/RHHB9)-B-125-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-125-1'),
        ('(FBM239/RHHB9)-B-126-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-126-1'),
        ('(FBM239/RHHB9)-B-131-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-131-1'),
        ('(FBM239/RHHB9)-B-135-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-135-1'),
        ('(FBM239/RHHB9)-B-158-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-158-1'),
        ('(FBM239/RHHB9)-B-171-1','FBM239/RHHB9','F4','segregating_line','not_fixed','pedigree','(FBM239/RHHB9)-B-171-1'),
        ('(FBM239/RHM52)-B-134-1','FBM239/RHM52','F4','segregating_line','not_fixed','pedigree','(FBM239/RHM52)-B-134-1'),
        ('(FBM239/RHM52)-B-17-1','FBM239/RHM52','F4','segregating_line','not_fixed','pedigree','(FBM239/RHM52)-B-17-1'),
        ('(FBM239/RHM52)-B-39-1','FBM239/RHM52','F4','segregating_line','not_fixed','pedigree','(FBM239/RHM52)-B-39-1'),
        ('(FBM239/RHM52)-B-79-1','FBM239/RHM52','F4','segregating_line','not_fixed','pedigree','(FBM239/RHM52)-B-79-1'),
        ('(FBM239/RHT11)-B-101-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-101-1'),
        ('(FBM239/RHT11)-B-107-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-107-1'),
        ('(FBM239/RHT11)-B-13-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-13-1'),
        ('(FBM239/RHT11)-B-22-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-22-1'),
        ('(FBM239/RHT11)-B-71-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-71-1'),
        ('(FBM239/RHT11)-B-82-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-82-1'),
        ('(FBM239/RHT11)-B-89-1','FBM239/RHT11','F4','segregating_line','not_fixed','pedigree','(FBM239/RHT11)-B-89-1'),
        ('(HTL437/ABHB9)-B-108-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-108-1-1-B'),
        ('(HTL437/ABHB9)-B-119-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-119-1-1-B'),
        ('(HTL437/ABHB9)-B-120-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-120-1-1-B'),
        ('(HTL437/ABHB9)-B-168-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-168-1-1-B'),
        ('(HTL437/ABHB9)-B-168-1-2-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-168-1-2-B'),
        ('(HTL437/ABHB9)-B-17-1-2-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-17-1-2-B'),
        ('(HTL437/ABHB9)-B-183-1-2-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-183-1-2-B'),
        ('(HTL437/ABHB9)-B-31-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-31-1-1-B'),
        ('(HTL437/ABHB9)-B-31-1-2-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-31-1-2-B'),
        ('(HTL437/ABHB9)-B-3-1-1-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-3-1-1-B'),
        ('(HTL437/ABHB9)-B-73-1-2-B','HTL437/ABHB9','F6','segregating_line','not_fixed','pedigree','(HTL437/ABHB9)-B-73-1-2-B'),
        ('(HTL437/ABP38)-B-157-1-1-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-157-1-1-B'),
        ('(HTL437/ABP38)-B-157-1-2-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-157-1-2-B'),
        ('(HTL437/ABP38)-B-178-1-1-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-178-1-1-B'),
        ('(HTL437/ABP38)-B-178-1-2-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-178-1-2-B'),
        ('(HTL437/ABP38)-B-188-1-2-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-188-1-2-B'),
        ('(HTL437/ABP38)-B-189-1-2-B','HTL437/ABP38','F6','segregating_line','not_fixed','pedigree','(HTL437/ABP38)-B-189-1-2-B'),
        ('(HTL437/ABW52)-B-145-1-1-B','HTL437/ABW52','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52)-B-145-1-1-B'),
        ('(HTL437/ABW52)-B-17-1-2-B','HTL437/ABW52','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52)-B-17-1-2-B'),
        ('(HTL437/ABW52)-B-3-1-1-B','HTL437/ABW52','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52)-B-3-1-1-B'),
        ('(HTL437/ABW52)-B-3-1-2-B','HTL437/ABW52','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52)-B-3-1-2-B'),
        ('(HTL437/ABW52)-B-71-1-1-B','HTL437/ABW52','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52)-B-71-1-1-B'),
        ('(HTL437/ABW52//HTL437)-96-1-1-1-B','HTL437/ABW52//HTL437','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52//HTL437)-96-1-1-1-B'),
        ('(HTL437/ABW52//HTL437)-96-1-1-2-B','HTL437/ABW52//HTL437','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52//HTL437)-96-1-1-2-B'),
        ('(HTL437/ABW52//HTL437)-98-1-1-1-B','HTL437/ABW52//HTL437','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52//HTL437)-98-1-1-1-B'),
        ('(HTL437/ABW52//HTL437)-98-1-1-2-B','HTL437/ABW52//HTL437','F6','segregating_line','not_fixed','pedigree','(HTL437/ABW52//HTL437)-98-1-1-2-B'),
        ('(HTL437/RK132)-B-51-1-2-B','HTL437/RK132','F6','segregating_line','not_fixed','pedigree','(HTL437/RK132)-B-51-1-2-B'),
        ('(HTL437/RK132)-B-86-1-1-B','HTL437/RK132','F6','segregating_line','not_fixed','pedigree','(HTL437/RK132)-B-86-1-1-B'),
        ('(HTL437/RK132)-B-89-1-2-B','HTL437/RK132','F6','segregating_line','not_fixed','pedigree','(HTL437/RK132)-B-89-1-2-B'),
        ('(HTL437/RK132)-B-9-1-1-B','HTL437/RK132','F6','segregating_line','not_fixed','pedigree','(HTL437/RK132)-B-9-1-1-B'),
        ('LMC243/LMC266','LMC243/LMC266','F1','F1','not_fixed','pedigree','LMC243/LMC266'),
        ('LMC266/EEL895','LMC266/EEL895','F1','F1','not_fixed','pedigree','LMC266/EEL895'),
        ('LMC266/LMC269','LMC266/LMC269','F1','F1','not_fixed','pedigree','LMC266/LMC269'),
        ('LMC266/MKL0333','LMC266/MKL0333','F1','F1','not_fixed','pedigree','LMC266/MKL0333'),
        ('(LTL812/ABT11)-B-16-1-1-B','LTL812/ABT11','F6','segregating_line','not_fixed','pedigree','(LTL812/ABT11)-B-16-1-1-B'),
        ('(LTL812/ABT11)-B-16-1-2-B','LTL812/ABT11','F6','segregating_line','not_fixed','pedigree','(LTL812/ABT11)-B-16-1-2-B'),
        ('(LTL812/ABT11)-B-51-1-2-B','LTL812/ABT11','F6','segregating_line','not_fixed','pedigree','(LTL812/ABT11)-B-51-1-2-B'),
        ('(LTL812/ABT11)-B-8-1-2-B','LTL812/ABT11','F6','segregating_line','not_fixed','pedigree','(LTL812/ABT11)-B-8-1-2-B'),
        ('(LTL812/ABW52)-B-113-1-2-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-113-1-2-B'),
        ('(LTL812/ABW52)-B-124-1-2-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-124-1-2-B'),
        ('(LTL812/ABW52)-B-25-1-1-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-25-1-1-B'),
        ('(LTL812/ABW52)-B-65-1-1-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-65-1-1-B'),
        ('(LTL812/ABW52)-B-65-1-2-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-65-1-2-B'),
        ('(LTL812/ABW52)-B-74-1-2-B','LTL812/ABW52','F6','segregating_line','not_fixed','pedigree','(LTL812/ABW52)-B-74-1-2-B'),
        ('(LTL812/RK198)-B-112-1-2-B','LTL812/RK198','F6','inbred_line','fixed','pedigree','(LTL812/RK198)-B-112-1-2-B'),
        ('(LTL812/RK198)-B-53-1-2-B','LTL812/RK198','F6','inbred_line','fixed','pedigree','(LTL812/RK198)-B-53-1-2-B'),
        ('MKL1500041','AHH12/XYL244','DH_line','DH_line','fixed','line_code_1','MKL1500041'),
        ('MKL1500099','AAA1/XXA1','DH_line','DH_line','fixed','line_code_1','MKL1500099'),
        ('MKL1500186','AAA1/XXA2','DH_line','DH_line','fixed','line_code_1','MKL1500186'),
        ('MKL1500215','AAA1/XXA42','DH_line','DH_line','fixed','line_code_1','MKL1500215'),
        ('MKL1500261','AAA1/XXA43','DH_line','DH_line','fixed','line_code_1','MKL1500261'),
        ('MKL150334','AAA1/XXA44','DH_line','DH_line','fixed','line_code_1','MKL150334'),
        ('MKL150339','AAA1/XXA3','DH_line','DH_line','fixed','line_code_1','MKL150339'),
        ('MKL150342','AAA1/XXA4','DH_line','DH_line','fixed','line_code_1','MKL150342'),
        ('MKL150390','AAA1/XXA45','DH_line','DH_line','fixed','line_code_1','MKL150390'),
        ('MKL150399','AAA1/XXA5','DH_line','DH_line','fixed','line_code_1','MKL150399'),
        ('MKL150421','AAA1/XXA6','DH_line','DH_line','fixed','line_code_1','MKL150421'),
        ('MKL150431','AAA1/XXA46','DH_line','DH_line','fixed','line_code_1','MKL150431'),
        ('MKL150694','AAA1/XXA47','DH_line','DH_line','fixed','line_code_1','MKL150694'),
        ('MKL150893','AAA1/XXA48','DH_line','DH_line','fixed','line_code_1','MKL150893'),
        ('MKL150961','AAA1/XXA49','DH_line','DH_line','fixed','line_code_1','MKL150961'),
        ('MKL150968','AAA1/XXA50','DH_line','DH_line','fixed','line_code_1','MKL150968'),
        ('MKL151030','AAA1/XXA51','DH_line','DH_line','fixed','line_code_1','MKL151030'),
        ('MKL151147','AAA1/XXA52','DH_line','DH_line','fixed','line_code_1','MKL151147'),
        ('MKL151294','AAA1/XXA53','DH_line','DH_line','fixed','line_code_1','MKL151294'),
        ('MKL151780','AAA1/XXA7','DH_line','DH_line','fixed','line_code_1','MKL151780'),
        ('MKL151787','AAA1/XXA8','DH_line','DH_line','fixed','line_code_1','MKL151787'),
        ('MKL151822','AAA1/XXA9','DH_line','DH_line','fixed','line_code_1','MKL151822'),
        ('MKL151830','AAA1/XXA54','DH_line','DH_line','fixed','line_code_1','MKL151830'),
        ('MKL151839','AAA1/XXA10','DH_line','DH_line','fixed','line_code_1','MKL151839'),
        ('MKL151841','AAA1/XXA55','DH_line','DH_line','fixed','line_code_1','MKL151841'),
        ('MKL151845','AAA1/XXA56','DH_line','DH_line','fixed','line_code_1','MKL151845'),
        ('MKL151851','AAA1/XXA11','DH_line','DH_line','fixed','line_code_1','MKL151851'),
        ('MKL151920','AAA1/XXA57','DH_line','DH_line','fixed','line_code_1','MKL151920'),
        ('MKL151967','AAA1/XXA58','DH_line','DH_line','fixed','line_code_1','MKL151967'),
        ('MKL151969','AAA1/XXA59','DH_line','DH_line','fixed','line_code_1','MKL151969'),
        ('MKL151972','AAA1/XXA12','DH_line','DH_line','fixed','line_code_1','MKL151972'),
        ('MKL151973','AAA1/XXA60','DH_line','DH_line','fixed','line_code_1','MKL151973'),
        ('MKL152043','AAA1/XXA13','DH_line','DH_line','fixed','line_code_1','MKL152043'),
        ('MKL152076','AAA1/XXA61','DH_line','DH_line','fixed','line_code_1','MKL152076'),
        ('MKL152092','AAA1/XXA62','DH_line','DH_line','fixed','line_code_1','MKL152092'),
        ('MKL152098','AAA1/XXA63','DH_line','DH_line','fixed','line_code_1','MKL152098'),
        ('MKL152140','AAA1/XXA14','DH_line','DH_line','fixed','line_code_1','MKL152140'),
        ('MKL152144','AAA1/XXA15','DH_line','DH_line','fixed','line_code_1','MKL152144'),
        ('MKL152149','AAA1/XXA16','DH_line','DH_line','fixed','line_code_1','MKL152149'),
        ('MKL152366','AAA1/XXA64','DH_line','DH_line','fixed','line_code_1','MKL152366'),
        ('MKL152395','AAA1/XXA17','DH_line','DH_line','fixed','line_code_1','MKL152395'),
        ('MKL152503','AAA1/XXA65','DH_line','DH_line','fixed','line_code_1','MKL152503'),
        ('MKL152554','AAA1/XXA18','DH_line','DH_line','fixed','line_code_1','MKL152554'),
        ('MKL152561','AAA1/XXA19','DH_line','DH_line','fixed','line_code_1','MKL152561'),
        ('MKL152563','AAA1/XXA20','DH_line','DH_line','fixed','line_code_1','MKL152563'),
        ('MKL152579','AAA1/XXA66','DH_line','DH_line','fixed','line_code_1','MKL152579'),
        ('MKL152591','AAA1/XXA21','DH_line','DH_line','fixed','line_code_1','MKL152591'),
        ('MKL152601','AAA1/XXA22','DH_line','DH_line','fixed','line_code_1','MKL152601'),
        ('MKL152616','AAA1/XXA23','DH_line','DH_line','fixed','line_code_1','MKL152616'),
        ('MKL152617','AAA1/XXA24','DH_line','DH_line','fixed','line_code_1','MKL152617'),
        ('MKL152653','AAA1/XXA25','DH_line','DH_line','fixed','line_code_1','MKL152653'),
        ('MKL152658','AAA1/XXA26','DH_line','DH_line','fixed','line_code_1','MKL152658'),
        ('MKL152682','AAA1/XXA67','DH_line','DH_line','fixed','line_code_1','MKL152682'),
        ('MKL152748','AAA1/XXA68','DH_line','DH_line','fixed','line_code_1','MKL152748'),
        ('MKL152769','AAA1/XXA27','DH_line','DH_line','fixed','line_code_1','MKL152769'),
        ('MKL152773','AAA1/XXA69','DH_line','DH_line','fixed','line_code_1','MKL152773'),
        ('MKL152777','AAA1/XXA70','DH_line','DH_line','fixed','line_code_1','MKL152777'),
        ('MKL152778','AAA1/XXA71','DH_line','DH_line','fixed','line_code_1','MKL152778'),
        ('MKL152811','AAA1/XXA72','DH_line','DH_line','fixed','line_code_1','MKL152811'),
        ('MKL152847','AAA1/XXA28','DH_line','DH_line','fixed','line_code_1','MKL152847'),
        ('MKL152857','AAA1/XXA29','DH_line','DH_line','fixed','line_code_1','MKL152857'),
        ('MKL152862','AAA1/XXA73','DH_line','DH_line','fixed','line_code_1','MKL152862'),
        ('MKL152921','AAA1/XXA30','DH_line','DH_line','fixed','line_code_1','MKL152921'),
        ('MKL152929','AAA1/XXA31','DH_line','DH_line','fixed','line_code_1','MKL152929'),
        ('MKL152976','AAA1/XXA32','DH_line','DH_line','fixed','line_code_1','MKL152976'),
        ('MKL152994','AAA1/XXA74','DH_line','DH_line','fixed','line_code_1','MKL152994'),
        ('MKL153050','AAA1/XXA75','DH_line','DH_line','fixed','line_code_1','MKL153050'),
        ('MKL153193','AAA1/XXA33','DH_line','DH_line','fixed','line_code_1','MKL153193'),
        ('MKL153222','AAA1/XXA34','DH_line','DH_line','fixed','line_code_1','MKL153222'),
        ('MKL153223','AAA1/XXA76','DH_line','DH_line','fixed','line_code_1','MKL153223'),
        ('MKL153236','AAA1/XXA77','DH_line','DH_line','fixed','line_code_1','MKL153236'),
        ('MKL153907','AAA1/XXA35','DH_line','DH_line','fixed','line_code_1','MKL153907'),
        ('MKL153908','AAA1/XXA36','DH_line','DH_line','fixed','line_code_1','MKL153908'),
        ('MKL153939','AAA1/XXA37','DH_line','DH_line','fixed','line_code_1','MKL153939'),
        ('MKL153946','AAA1/XXA78','DH_line','DH_line','fixed','line_code_1','MKL153946'),
        ('MKL153984','AAA1/XXA38','DH_line','DH_line','fixed','line_code_1','MKL153984'),
        ('MKL154043','AAA1/XXA39','DH_line','DH_line','fixed','line_code_1','MKL154043'),
        ('MKL154050','AAA1/XXA40','DH_line','DH_line','fixed','line_code_1','MKL154050'),
        ('MKL154091','AAA1/XXA41','DH_line','DH_line','fixed','line_code_1','MKL154091'),
        ('MKL154102','AAA1/XXA79','DH_line','DH_line','fixed','line_code_1','MKL154102')
    ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type,germplasm_normalized_name
    )
;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback DELETE FROM
--rollback     germplasm.germplasm
--rollback WHERE
--rollback     crop_id = (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             tenant.crop
--rollback         WHERE
--rollback             crop_code = 'MAIZE'
--rollback     )
--rollback ;
--rollback 
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;