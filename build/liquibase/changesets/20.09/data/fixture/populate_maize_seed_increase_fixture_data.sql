--liquibase formatted sql

--changeset postgres:populate_maize_seed_increase_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.experiment



-- populate maize experiment A-1
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GMP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'SEM') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'KE_PROJECT') AS project_id,
    2019 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2019A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'A-1' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Seed Increase' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'colin.anderson') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'colin.anderson') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'A-1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.entry_list



-- populate maize entry list A-1_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'A-1_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'A-1') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'colin.anderson') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'A-1_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.entry



-- populate maize entries for A-1
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number::integer,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        ('3','(ABDHL0089/ABDHL120668)@195-B','WE-KIB-17B-22-656'),
        ('2','(ABDHL0089/ABDHL120668)@19-B','WE-KIB-17B-22-472'),
        ('6','(ABDHL0089/ABDHL120918)@200-B','WE-KIB-17B-24-208'),
        ('4','(ABDHL0089/ABDHL120918)@52-B','WE-KIB-17B-24-54'),
        ('5','(ABDHL0089/ABDHL120918)@63-B','WE-KIB-17B-24-65'),
        ('1','(ABDHL0089/ABLTI0136)@220-B','WE-KIB-17B-22-411'),
        ('11','(ABLTI0139/ABDHL120918)@246-B','WE-KIB-17B-26-537'),
        ('17','(ABLTI0139/ABDHL120918)@299-B','WE-KIB-17B-26-593'),
        ('12','(ABLTI0139/ABDHL120918)@373-B','WE-KIB-17B-26-673'),
        ('13','(ABLTI0139/ABDHL120918)@393-B','WE-KIB-17B-26-696'),
        ('14','(ABLTI0139/ABDHL120918)@479-B','WE-KIB-17B-26-786'),
        ('15','(ABLTI0139/ABDHL120918)@553-B','WE-KIB-17B-26-866'),
        ('16','(ABLTI0139/ABDHL120918)@653-B','WE-KIB-17B-26-972'),
        ('10','(ABLTI0139/ABDHL120918)@95-B','WE-KIB-17B-26-374'),
        ('9','(ABLTI0139/ABLTI0335)@107-B','WE-KIB-17B-26-112'),
        ('8','(ABLTI0139/ABLTI0335)@14-B','WE-KIB-17B-26-14'),
        ('7','(ABLTI0139/CML543)@140-B','WE-KIB-17B-23-859'),
        ('22','CLRCY034','WE-KIB-17A-41-36'),
        ('21','CLYN261','WE-KIB-17A-41-38'),
        ('20','((CML442/KS23-6)-B)@103-B','WE-KIB-18A-3-719'),
        ('26','CML463','WE-KIB-18A-7-25'),
        ('25','(CML494/OFP9)-12-2-1-1-1-B*4','WE-KIB-17B-14-5'),
        ('24','(CML494/OFP9)-12-2-1-1-2-B*5','WE-KIB-17B-51-4'),
        ('23','CML495','WE-KIB-17A-71-7'),
        ('19','((CML537/KS523-5)-B)@10-B','WE-KIB-18A-3-758'),
        ('18','CML547','WE-KIB-18A-1-531')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'A-1_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'colin.anderson'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'A-1_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.occurrence



-- populate maize occurrence for A-1
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'A-1_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'A-1'
    AND geo.geospatial_object_code = 'KI'
    AND person.username = 'colin.anderson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'A-1_OCC1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in place.geospatial_object



-- populate maize planting area for A-1
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'A-1_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'A-1_LOC1'
--rollback ;


--changeset postgres:populate_maize_seed_increase_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.location



-- populate maize location for A-1
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'A-1_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2019 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'colin.anderson'
    AND season.season_code = 'A'
    AND geo.geospatial_object_name = 'A-1_LOC1'
    AND site.geospatial_object_code = 'KI'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'A-1_LOC1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.plot



-- populate maize plots for A-1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (3,1,3,'(ABDHL0089/ABDHL120668)@195-B'),
        (2,1,2,'(ABDHL0089/ABDHL120668)@19-B'),
        (6,1,6,'(ABDHL0089/ABDHL120918)@200-B'),
        (4,1,4,'(ABDHL0089/ABDHL120918)@52-B'),
        (5,1,5,'(ABDHL0089/ABDHL120918)@63-B'),
        (1,1,1,'(ABDHL0089/ABLTI0136)@220-B'),
        (11,1,11,'(ABLTI0139/ABDHL120918)@246-B'),
        (17,1,17,'(ABLTI0139/ABDHL120918)@299-B'),
        (12,1,12,'(ABLTI0139/ABDHL120918)@373-B'),
        (13,1,13,'(ABLTI0139/ABDHL120918)@393-B'),
        (14,1,14,'(ABLTI0139/ABDHL120918)@479-B'),
        (15,1,15,'(ABLTI0139/ABDHL120918)@553-B'),
        (16,1,16,'(ABLTI0139/ABDHL120918)@653-B'),
        (10,1,10,'(ABLTI0139/ABDHL120918)@95-B'),
        (9,1,9,'(ABLTI0139/ABLTI0335)@107-B'),
        (8,1,8,'(ABLTI0139/ABLTI0335)@14-B'),
        (7,1,7,'(ABLTI0139/CML543)@140-B'),
        (22,1,22,'CLRCY034'),
        (21,1,21,'CLYN261'),
        (20,1,20,'((CML442/KS23-6)-B)@103-B'),
        (26,1,26,'CML463'),
        (25,1,25,'(CML494/OFP9)-12-2-1-1-1-B*4'),
        (24,1,24,'(CML494/OFP9)-12-2-1-1-2-B*5'),
        (23,1,23,'CML495'),
        (19,1,19,'((CML537/KS523-5)-B)@10-B'),
        (18,1,18,'CML547')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'A-1_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'A-1_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'A-1_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'colin.anderson'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'A-1_OCC1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.planting_instruction



-- populate maize planting instructions for A-1
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'colin.anderson'
WHERE
    entlist.entry_list_name = 'A-1_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-1_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in germplasm.package_log



-- populate maize package logs for A-1
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'colin.anderson'
WHERE
    entlist.entry_list_name = 'A-1_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-1_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_seed_increase_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase package_log_id in experiment.planting_instruction



-- populate maize package log references for A-1
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'A-1_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-1_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_maize_seed_increase_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in germplasm.cross



-- populate maize crosses for A-1
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'colin.anderson'
WHERE
    expt.experiment_name = 'A-1'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'A-1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in germplasm.cross_parent



-- populate maize cross parents for A-1
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'colin.anderson'
        WHERE
            expt.experiment_name = 'A-1'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'A-1'
--rollback ;



--changeset postgres:populate_maize_seed_increase_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize seed increase in experiment.location_occurrence_group



-- populate maize location occurrence groups for A-1
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'A-1_LOC1'
    AND occ.occurrence_name = 'A-1_OCC1'
    AND prs.username = 'colin.anderson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'A-1_LOC1'
--rollback     AND occ.occurrence_name = 'A-1_OCC1'
--rollback ;