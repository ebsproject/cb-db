--liquibase formatted sql

--changeset postgres:populate_place.geospatial_object_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate place.geospatial_object for wheat and maize



-- populate wheat and maize geospatial objects
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        'CO', 'Ciudad Obregon', 'site', 'beeding_location', '1'
    ),
    (
        'EB', 'El Batan', 'site', 'beeding_location', '1'
    ),
    (
        'TO', 'Toluca', 'site', 'beeding_location', '1'
    ),
    (
        'KI', 'Kiboko', 'site', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'CO', 'EB', 'TO', 'KI'
--rollback     )
--rollback ;
