--liquibase formatted sql

--changeset postgres:add_cimmyt_to_tenant.organization context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Add CIMMYT to tenant.organization



-- add CIMMYT organization
INSERT INTO
    tenant.organization (organization_code, organization_name, description, creator_id)
 VALUES 
    ('CIMMYT', 'International Maize and Wheat Improvement Center', 'International Maize and Wheat Improvement Center', '1')
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.organization
--rollback WHERE
--rollback     organization_code = 'CIMMYT'
--rollback ;



--changeset postgres:add_maize_and_wheat_to_tenant.crop context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Add wheat and maize to tenant.crop



-- add wheat and maize crops
INSERT INTO
    tenant.crop (crop_code, crop_name, description, creator_id)
VALUES 
    ('WHEAT', 'Wheat', 'Wheat crop', '1'),
    ('MAIZE', 'Maize', 'Maize crop', '1')
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.crop
--rollback WHERE
--rollback     crop_code IN ('WHEAT', 'MAIZE')
--rollback ;



--changeset postgres:populate_tenant.crop_program_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.crop_program for wheat and maize



-- Add wheat and maize crop programs
INSERT INTO
    tenant.crop_program (crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
VALUES
    (
        'GWP', 'Global Wheat Program', 'Wheat crop program',
        (SELECT id FROM tenant.organization WHERE organization_code = 'CIMMYT'),
        (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT'),
        '1'
    ),
    (
        'GMP', 'Global Maize Program', 'Maize crop program',
        (SELECT id FROM tenant.organization WHERE organization_code = 'CIMMYT'),
        (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE'),
        '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.crop_program
--rollback WHERE
--rollback     crop_program_code IN ('GWP', 'GMP')
--rollback ;



--changeset postgres:populate_tenant.pipeline_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.pipeline for wheat and maize



-- Add wheat and maize pipelines
INSERT INTO
    tenant.pipeline (pipeline_code, pipeline_name, pipeline_status, description, creator_id)
VALUES
    ('GWP_PIPELINE', 'GWP Pipeline', 'active', 'Global Wheat Program Pipeline', '1'),
    ('GMP_PIPELINE', 'GMP Pipeline', 'active', 'Global Maize Program Pipeline', '1')
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.pipeline
--rollback WHERE
--rollback     pipeline_code IN ('GWP_PIPELINE', 'GMP_PIPELINE')
--rollback ;



--changeset postgres:populate_tenant.program_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.program for wheat and maize



-- add wheat and maize programs
INSERT INTO
    tenant.program (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
VALUES 
    (
        'WBP', 'Wheat Breeding Program', 'breeding', 'active', 'Wheat Breeding Program',
        (SELECT id FROM tenant.crop_program WHERE crop_program_code = 'GWP'),
        '1'
    ),
    (
        'MBP', 'Maize Breeding Program', 'breeding', 'active', 'Maize Breeding Program',
        (SELECT id FROM tenant.crop_program WHERE crop_program_code = 'GMP'),
        '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.program
--rollback WHERE
--rollback     program_code IN ('WBP', 'MBP')
--rollback ;



--changeset postgres:populate_tenant.project_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.project for wheat and maize



-- add wheat and maize projects
INSERT INTO
    tenant.project (project_code, project_name, project_status, description, program_id, pipeline_id, leader_id, creator_id)
VALUES
    (
        'WBP_PROJECT', 'WBP Project', 'active', 'Wheat Breeding Program Project',
        (SELECT id FROM tenant.program WHERE program_code = 'WBP'),
        (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE'),
        (SELECT id FROM tenant.person WHERE username = 'nicola.costa'),
        '1'
    ),
    (
        'MBP_PROJECT', 'MBP Project', 'active', 'Maize Breeding Program Project',
        (SELECT id FROM tenant.program WHERE program_code = 'MBP'),
        (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GMP_PIPELINE'),
        (SELECT id FROM tenant.person WHERE username = 'elly.donelly'),
        '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.project
--rollback WHERE
--rollback     project_code IN ('WBP_PROJECT', 'MBP_PROJECT')
--rollback ;



--changeset postgres:populate_tenant.team_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.team for wheat and maize



-- add wheat and maize teams
INSERT INTO
    tenant.team (team_code, team_name, description, creator_id)
VALUES 
    ('WBP_TEAM', 'WBP Team', 'Wheat Breeding Program Team', '1'),
    ('MBP_TEAM', 'MBP Team', 'Maize Breeding Program Team', '1')
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.team
--rollback WHERE
--rollback     team_code IN ('WBP_TEAM', 'MBP_TEAM')
--rollback ;



--changeset postgres:populate_tenant.team_member_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.team_member for wheat and maize



-- add members to wheat team
WITH t_team AS (
    SELECT
        id
    FROM
        tenant.team
    WHERE
        team_code = 'WBP_TEAM'
), t_person AS (
    SELECT
        id,
        username
    FROM
        tenant.person
    WHERE
        username IN (
            'nicola.costa',
            'a.carlson',
            'a.ramos',
            'k.khadija',
            'jennifer.allan'
        )
), t_person_role AS (
    SELECT
        id,
        person_role_code
    FROM
        tenant.person_role
    WHERE
        person_role_code = 'TM'
)
INSERT INTO
    tenant.team_member (team_id, person_id, person_role_id, order_number, creator_id)
SELECT
    t.id AS team_id,
    p.id AS person_id,
    (CASE
        WHEN p.username = 'nicola.costa'
            THEN (SELECT id FROM tenant.person_role WHERE person_role_code = 'TL')
        ELSE
            pr.id
    END) AS person_role_id,
    row_number() OVER () AS order_number,
    '1' AS creator_id
FROM
    t_team AS t,
    t_person AS p,
    t_person_role AS pr
;


-- add members to maize team
WITH t_team AS (
    SELECT
        id
    FROM
        tenant.team
    WHERE
        team_code = 'MBP_TEAM'
), t_person AS (
    SELECT
        id,
        username
    FROM
        tenant.person
    WHERE
        username IN (
            'faith.hamilton',
            'phil.hudson',
            'colin.anderson',
            'alexandra.reid',
            'elly.donelly'
        )
), t_person_role AS (
    SELECT
        id,
        person_role_code
    FROM
        tenant.person_role
    WHERE
        person_role_code = 'TM'
)
INSERT INTO
    tenant.team_member (team_id, person_id, person_role_id, order_number, creator_id)
SELECT
    t.id AS team_id,
    p.id AS person_id,
    (CASE
        WHEN p.username = 'elly.donelly'
            THEN (SELECT id FROM tenant.person_role WHERE person_role_code = 'TL')
        ELSE
            pr.id
    END) AS person_role_id,
    row_number() OVER () AS order_number,
    '1' AS creator_id
FROM
    t_team AS t,
    t_person AS p,
    t_person_role AS pr
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.team_member
--rollback WHERE
--rollback     team_id IN (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             tenant.team
--rollback         WHERE
--rollback             team_code IN ('WBP_TEAM', 'MBP_TEAM')
--rollback     )
--rollback ;



--changeset postgres:populate_tenant.program_team_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.program_team for wheat and maize


-- link wheat and maize programs to teams
INSERT INTO
    tenant.program_team (program_id, team_id, order_number, creator_id)
VALUES
    (
        (SELECT id FROM tenant.program WHERE program_code = 'WBP'),
        (SELECT id FROM tenant.team WHERE team_code = 'WBP_TEAM'),
        1, '1'
    ),
    (
        (SELECT id FROM tenant.program WHERE program_code = 'MBP'),
        (SELECT id FROM tenant.team WHERE team_code = 'MBP_TEAM'),
        1, '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.program_team
--rollback WHERE
--rollback     program_id IN (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             tenant.PROGRAM
--rollback         WHERE
--rollback             program_code IN ('WBP', 'MBP')
--rollback     )
--rollback ;



--changeset postgres:populate_tenant.phase_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.phase for wheat and maize



-- add wheat phases
WITH t_pipeline AS (
    SELECT
        id
    FROM
        tenant.pipeline
    WHERE
        pipeline_code = 'GWP_PIPELINE'
)
INSERT INTO
    tenant.phase (phase_code, phase_name, pipeline_id, creator_id)
SELECT
    t.phase_code,
    t.phase_name,
    p.id AS pipeline_id,
    '1' AS creator_id
FROM (
        VALUES
        ('GWP_PHASE_1', 'GWP Selection and Sorting'),
        ('GWP_PHASE_2', 'GWP Testing and Characterization'),
        ('GWP_PHASE_3', 'GWP Validation and Delivery')
    ) AS t (
        phase_code,
        phase_name
    ),
    t_pipeline AS p
;


-- add maize phases
WITH t_pipeline AS (
    SELECT
        id
    FROM
        tenant.pipeline
    WHERE
        pipeline_code = 'GMP_PIPELINE'
)
INSERT INTO
    tenant.phase (phase_code, phase_name, pipeline_id, creator_id)
SELECT
    t.phase_code,
    t.phase_name,
    p.id AS pipeline_id,
    '1' AS creator_id
FROM (
        VALUES
        ('GMP_PHASE_1', 'GMP Selection and Sorting'),
        ('GMP_PHASE_2', 'GMP Testing and Characterization'),
        ('GMP_PHASE_3', 'GMP Validation and Delivery')
    ) AS t (
        phase_code,
        phase_name
    ),
    t_pipeline AS p
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.phase
--rollback WHERE
--rollback     phase_code IN (
--rollback         'GMP_PHASE_1',
--rollback         'GMP_PHASE_2',
--rollback         'GMP_PHASE_3',
--rollback         'GWP_PHASE_1',
--rollback         'GWP_PHASE_2',
--rollback         'GWP_PHASE_3'
--rollback     )
--rollback ;



--changeset postgres:populate_tenant.scheme_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.scheme for wheat and maize



-- add wheat schemes
WITH t_pipeline AS (
    SELECT
        id
    FROM
        tenant.pipeline
    WHERE
        pipeline_code = 'GWP_PIPELINE'
)
INSERT INTO
    tenant.scheme (scheme_code, scheme_name, pipeline_id, creator_id)
SELECT
    t.scheme_code,
    t.scheme_name,
    p.id AS pipeline_id,
    '1' AS creator_id
FROM (
        VALUES
        ('GWP_SCHEME_1', 'GWP Scheme 1'),
        ('GWP_SCHEME_2', 'GWP Scheme 2'),
        ('GWP_SCHEME_3', 'GWP Scheme 3'),
        ('GWP_SCHEME_4', 'GWP Scheme 4')
    ) AS t (
        scheme_code,
        scheme_name
    ),
    t_pipeline AS p
;


-- add maize schemes
WITH t_pipeline AS (
    SELECT
        id
    FROM
        tenant.pipeline
    WHERE
        pipeline_code = 'GMP_PIPELINE'
)
INSERT INTO
    tenant.scheme (scheme_code, scheme_name, pipeline_id, creator_id)
SELECT
    t.scheme_code,
    t.scheme_name,
    p.id AS pipeline_id,
    '1' AS creator_id
FROM (
        VALUES
        ('GMP_SCHEME_1', 'GMP Scheme 1'),
        ('GMP_SCHEME_2', 'GMP Scheme 2'),
        ('GMP_SCHEME_3', 'GMP Scheme 3'),
        ('GMP_SCHEME_4', 'GMP Scheme 4')
    ) AS t (
        scheme_code,
        scheme_name
    ),
    t_pipeline AS p
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.scheme
--rollback WHERE
--rollback     scheme_code IN (
--rollback         'GMP_SCHEME_1',
--rollback         'GMP_SCHEME_2',
--rollback         'GMP_SCHEME_3',
--rollback         'GMP_SCHEME_4',
--rollback         'GWP_SCHEME_1',
--rollback         'GWP_SCHEME_2',
--rollback         'GWP_SCHEME_3',
--rollback         'GWP_SCHEME_4'
--rollback     )
--rollback ;



--changeset postgres:populate_tenant.season_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Populate tenant.season for wheat and maize



-- add seasons for wheat and maize
INSERT INTO
    tenant.season (season_code, season_name, description, creator_id)
VALUES
    ('A', 'Season A', 'Season A', '1'),
    ('B', 'Season B', 'Season B', '1')
;



-- revert changes
--rollback DELETE FROM tenant.season WHERE season_code in ('A', 'B');
