--liquibase formatted sql

--changeset postgres:update_project_and_pipeline_of_a2_cb_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Update project and pipeline of A-2 CB experiment



UPDATE 
    experiment.experiment
SET
    project_id = (SELECT id FROM tenant.project WHERE project_code = 'KE_PROJECT'),
    planting_season = '2019A',
    pipeline_id = (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GMP_PIPELINE')
WHERE
    experiment_name = 'A-2';



--rollback SELECT NULL;