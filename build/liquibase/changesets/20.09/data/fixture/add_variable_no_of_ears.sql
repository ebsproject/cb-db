--liquibase formatted sql

--changeset postgres:add_variable_no_of_ears context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1462 Add variable NO_OF_EARS



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'NO_OF_EARS' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'NO_OF_EARS' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('NO_OF_EARS','Number of Ears Selected','Number of Ears Selected') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Number of Ears Selected','NO_OF_EARS',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('NO_OF_EARS','continuous','Number of Ears Selected',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Number of Ears Selected','Number of Ears Selected','integer','Number of Ears Selected','Number of Ears Selected','False','NO_OF_EARS','study','observation','plot') 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('HARVEST_SELECTION','Selection for Nurseries') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.variable WHERE abbrev = 'NO_OF_EARS';


--changeset postgres:delete_variable_no_of_ears context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Delete variable NO_OF_EARS

--ALTER TABLE master.variable DISABLE TRIGGER ALL;
SET session_replication_role = replica;

DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

DELETE FROM master.property WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

DELETE FROM master.variable WHERE abbrev = 'NO_OF_EARS';

--ALTER TABLE master.variable ENABLE TRIGGER ALL;
SET session_replication_role = origin;

ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;

-- revert changes


--rollback DO $$
--rollback DECLARE
--rollback     var_property_id int;
--rollback     var_method_id int;
--rollback     var_scale_id int;
--rollback     var_variable_id int;
--rollback     var_variable_set_id int;
--rollback     var_variable_set_member_order_number int;
--rollback     var_count_property_id int;
--rollback     var_count_method_id int;
--rollback     var_count_scale_id int;
--rollback     var_count_variable_set_id int;
--rollback     var_count_variable_set_member_id int;
--rollback BEGIN
--rollback 
--rollback     --PROPERTY
--rollback 
--rollback     SELECT count(id) FROM master."property" WHERE ABBREV = 'NO_OF_EARS' INTO var_count_property_id;
--rollback     IF var_count_property_id > 0 THEN
--rollback         SELECT id FROM master."property" WHERE ABBREV = 'NO_OF_EARS' INTO var_property_id;
--rollback     ELSE
--rollback         INSERT INTO
--rollback             master."property" (abbrev,display_name,name) 
--rollback         VALUES 
--rollback             ('NO_OF_EARS','Number of Ears Selected','Number of Ears Selected') 
--rollback         RETURNING id INTO var_property_id;
--rollback     END IF;
--rollback 
--rollback     --METHOD
--rollback 
--rollback     SELECT count(id) FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_count_method_id;
--rollback     IF var_count_method_id > 0 THEN
--rollback         SELECT id FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_method_id;
--rollback     ELSE
--rollback         INSERT INTO
--rollback             master.method (name,abbrev,formula,description) 
--rollback         VALUES 
--rollback             ('Number of Ears Selected','NO_OF_EARS',NULL,NULL) 
--rollback         RETURNING id INTO var_method_id;
--rollback     END IF;
--rollback 
--rollback     --SCALE
--rollback 
--rollback     SELECT count(id) FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_count_scale_id;
--rollback     IF var_count_scale_id > 0 THEN
--rollback         SELECT id FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_scale_id;
--rollback     ELSE
--rollback         INSERT INTO
--rollback             master.scale (abbrev,type,name,unit,level) 
--rollback         VALUES 
--rollback             ('NO_OF_EARS','continuous','Number of Ears Selected',NULL,'nominal') 
--rollback         RETURNING id INTO var_scale_id;
--rollback     END IF;
--rollback 
--rollback     --VARIABLE
--rollback 
--rollback         INSERT INTO
--rollback             master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
--rollback         VALUES 
--rollback             ('active','Number of Ears Selected','Number of Ears Selected','integer','Number of Ears Selected','Number of Ears Selected','False','NO_OF_EARS','study','observation','plot') 
--rollback         RETURNING id INTO var_variable_id;
--rollback 
--rollback     --UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID
--rollback 
--rollback     UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;
--rollback 
--rollback     --VARIABLE_SET_ID
--rollback 
--rollback     SELECT count(id) FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_count_variable_set_id;
--rollback     IF var_count_variable_set_id > 0 THEN
--rollback         SELECT id FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_variable_set_id;
--rollback     ELSE
--rollback         INSERT INTO
--rollback             master.variable_set (abbrev,name) 
--rollback         VALUES 
--rollback             ('HARVEST_SELECTION','Selection for Nurseries') 
--rollback         RETURNING id INTO var_variable_set_id;
--rollback     END IF;
--rollback 
--rollback     --GET THE LAST ORDER NUMBER
--rollback 
--rollback     SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
--rollback     IF var_count_variable_set_member_id > 0 THEN
--rollback         SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
--rollback     ELSE
--rollback         var_variable_set_member_order_number = 1;
--rollback     END IF;
--rollback 
--rollback     --ADD VARIABLE SET MEMBER
--rollback 
--rollback     INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );
--rollback 
--rollback 
--rollback END;
--rollback $$



--changeset postgres:reinsert_variable_no_of_ears context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Re-insert variable NO_OF_EARS



DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
    var_variable_set_id int;
    var_variable_set_member_order_number int;
    var_count_property_id int;
    var_count_method_id int;
    var_count_scale_id int;
    var_count_variable_set_id int;
    var_count_variable_set_member_id int;
BEGIN

    --PROPERTY

    SELECT count(id) FROM master.property WHERE ABBREV = 'NO_OF_EARS' INTO var_count_property_id;
    IF var_count_property_id > 0 THEN
        SELECT id FROM master.property WHERE ABBREV = 'NO_OF_EARS' INTO var_property_id;
    ELSE
        INSERT INTO
            master.property (abbrev,display_name,name) 
        VALUES 
            ('NO_OF_EARS','Number of Ears Selected','Number of Ears Selected') 
        RETURNING id INTO var_property_id;
    END IF;

    --METHOD

    SELECT count(id) FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_count_method_id;
    IF var_count_method_id > 0 THEN
        SELECT id FROM master.method WHERE ABBREV = 'NO_OF_EARS' INTO var_method_id;
    ELSE
        INSERT INTO
            master.method (name,abbrev,formula,description) 
        VALUES 
            ('Number of Ears Selected','NO_OF_EARS',NULL,NULL) 
        RETURNING id INTO var_method_id;
    END IF;

    --SCALE

    SELECT count(id) FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_count_scale_id;
    IF var_count_scale_id > 0 THEN
        SELECT id FROM master.scale WHERE ABBREV = 'NO_OF_EARS' INTO var_scale_id;
    ELSE
        INSERT INTO
            master.scale (abbrev,type,name,unit,level) 
        VALUES 
            ('NO_OF_EARS','continuous','Number of Ears Selected',NULL,'nominal') 
        RETURNING id INTO var_scale_id;
    END IF;

    --VARIABLE

        INSERT INTO
            master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
        VALUES 
            ('active','Number of Ears Selected','Number of Ears Selected','integer','Number of Ears Selected','Number of Ears Selected','False','NO_OF_EARS','study','observation','plot') 
        RETURNING id INTO var_variable_id;

    --UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

    UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

    --VARIABLE_SET_ID

    SELECT count(id) FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_count_variable_set_id;
    IF var_count_variable_set_id > 0 THEN
        SELECT id FROM master.variable_set WHERE ABBREV = 'HARVEST_SELECTION' INTO var_variable_set_id;
    ELSE
        INSERT INTO
            master.variable_set (abbrev,name) 
        VALUES 
            ('HARVEST_SELECTION','Selection for Nurseries') 
        RETURNING id INTO var_variable_set_id;
    END IF;

    --GET THE LAST ORDER NUMBER

    SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
    IF var_count_variable_set_member_id > 0 THEN
        SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
    ELSE
        var_variable_set_member_order_number = 1;
    END IF;

    --ADD VARIABLE SET MEMBER

    INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'NO_OF_EARS');

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;

--rollback DELETE FROM master.variable WHERE abbrev = 'NO_OF_EARS';

--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;
