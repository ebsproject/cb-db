--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_experiment.experiment_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.experiment



-- populate wheat experiment CBBW-2
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'HB') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2017 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'B') AS season_id,
    '2017B' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'CBBW-2' AS experiment_name,
    'Intentional Crossing Nursery' AS experiment_type,
    'Breeding Crosses' AS experiment_sub_type,
    'Crossing Block' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'entry list created; crosses created; design generated; occurrences created' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'CBBW-2'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_list_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_list



-- populate wheat entry list CBBW-2_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'CBBW-2_ENTLIST' AS entry_list_name,
    'draft' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'CBBW-2') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'a.carlson') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry



-- populate wheat entries for CBBW-2
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    'female-and-male' AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS11B00587S-099M-0SY-14M-0WGY','BV2017CBBW59'),
        (2,'CMSS11B00623T-099TOPY-099M-0SY-17M-0WGY','BV2017CBBW60'),
        (3,'CMSS11B00650T-099TOPY-099M-099NJ-099NJ-20WGY-0B','BV2017CBBW61'),
        (4,'CMSS11B00700T-099TOPY-099M-099NJ-099NJ-12WGY-0B','BV2017CBBW64'),
        (5,'CMSS11B00728T-099TOPY-099M-099NJ-099NJ-20WGY-0B','BV2017CBBW65'),
        (6,'CMSS11B00745T-099TOPY-099M-099NJ-099NJ-40WGY-0B','BV2017CBBW66'),
        (7,'CMSS11B00773T-099TOPY-099M-099NJ-099NJ-25WGY-0B','BV2017CBBW67'),
        (8,'CMSS11B00786T-099TOPY-099M-0SY-31M-0WGY','BV2017CBBW68'),
        (9,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-16WGY-0B','BV2017CBBW69'),
        (10,'CMSS11B00807T-099TOPY-099M-099NJ-099NJ-42WGY-0B','BV2017CBBW70'),
        (11,'CMSS11B00861T-099TOPY-099M-099NJ-099NJ-20WGY-0B','BV2017CBBW71'),
        (12,'CMSS11B00908T-099TOPY-099M-099NJ-099NJ-31WGY-0B','BV2017CBBW72'),
        (13,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-2WGY-0B','BV2017CBBW73'),
        (14,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-34WGY-0B','BV2017CBBW74'),
        (15,'CMSS11B00912T-099TOPY-099M-099NJ-099NJ-1WGY-0B','BV2017PPTBW80'),
        (16,'CMSS11B00913T-099TOPY-099M-099NJ-099NJ-24WGY-0B','BV2017CBBW75'),
        (17,'CMSS11B00930T-099TOPY-099M-099NJ-099NJ-10WGY-0B','BV2017CBBW76'),
        (18,'CMSS11B00959T-099TOPY-099M-0SY-13M-0WGY','BV2017PPTBW82'),
        (19,'CMSS11B00996T-099TOPY-099M-099NJ-099NJ-16WGY-0B','BV2017CBBW77'),
        (20,'CMSS11B01003T-099TOPY-099M-0SY-24M-0WGY','BV2017CBBW78'),
        (21,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-3RGY-0B','BV2017PPTBW91'),
        (22,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-12RGY-0B','BV2017CBBW106'),
        (23,'CMSA11Y00305S-099Y-099M-099NJ-099NJ-1RGY-0B','BV2017CBBW107'),
        (24,'CMSA11Y00311S-099Y-099M-099NJ-099NJ-23RGY-0B','BV2017CBBW108'),
        (25,'CMSS11Y00803T-099TOPM-099Y-099M-099NJ-099NJ-41RGY-0B','BV2017CBBW91'),
        (26,'CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B','BV2017PPTBW85'),
        (27,'CMSS11Y01166T-099TOPM-099Y-099M-099NJ-099NJ-14RGY-0B','BV2017CBBW94'),
        (28,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-2RGY-0B','BV2017CBBW95'),
        (29,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-11RGY-0B','BV2017CBBW96'),
        (30,'CMSS11B00310S-099M-099NJ-099NJ-24RGY-0B','BV2017CBBW99'),
        (31,'CMSS11B00311S-099M-099NJ-099NJ-7RGY-0B','BV2017PPTBW89'),
        (32,'CMSS11B00546S-099M-0SY-4M-0RGY','BV2017CBBW100'),
        (33,'CMSS11B00587S-099M-099NJ-099NJ-15RGY-0B','BV2017CBBW101'),
        (34,'CMSS11B00840T-099TOPY-099M-099NJ-099NJ-14RGY-0B','BV2017PPTBW90'),
        (35,'CMSS11B00973T-099TOPY-099M-099NJ-099NJ-7RGY-0B','BV2017CBBW102'),
        (36,'CMSS11B00983T-099TOPY-099M-099NJ-099NJ-5RGY-0B','BV2017CBBW103'),
        (37,'CMSS11B00993T-099TOPY-099M-099NJ-099NJ-13RGY-0B','BV2017CBBW104'),
        (38,'CMSS11B00995T-099TOPY-099M-099NJ-099NJ-3RGY-0B','BV2017CBBW105'),
        (39,'CMSS11B00344S-099M-099NJ-099NJ-3WGY-0M','BV2017CBBW112'),
        (40,'CMSS11B00381S-099M-099NJ-099NJ-38WGY-0M','BV2017CBBW113'),
        (41,'CMSS11B00383S-099M-099NJ-099NJ-18WGY-0M','BV2017CBBW114'),
        (42,'CMSS11B00454S-099M-099NJ-099NJ-20WGY-0M','BV2017PPTBW97'),
        (43,'CMSS11B00478S-099M-099NJ-099NJ-2WGY-0M','BV2017CBBW116'),
        (44,'CMSS11B00490S-099M-099NJ-099NJ-12WGY-0M','BV2017PPTBW98'),
        (45,'CMSS11B00516S-099M-099NJ-099NJ-12WGY-0M','BV2017CBBW117'),
        (46,'CMSS11B00587S-099M-099NJ-099NJ-9WGY-0M','BV2017CBBW118'),
        (47,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-10WGY-0B','BV2017PPTBW102'),
        (48,'CMSS11B00958T-099TOPY-099M-099NJ-099NJ-11WGY-0B','BV2017CBBW120'),
        (49,'CMSS12Y00044S-099Y-099M-0SY-1M-0WGY','BV2017CBBW121'),
        (50,'CMSS12Y00064S-099Y-099M-0SY-26M-0WGY','BV2017CBBW123')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'CBBW-2_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_data_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_data



-- populate wheat entry data for CBBW-2
INSERT INTO
    experiment.entry_data (
        entry_id, variable_id, data_value, data_qc_code, creator_id
    )
SELECT
    ent.id AS entry_id,
    var.id AS variable_id,
    'Line' AS data_value,
    'N' AS data_qc_code,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PARENT_TYPE'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    entlist.entry_list_name = 'CBBW-2_ENTLIST'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_data AS entdata
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON entlist.id = ent.entry_list_id
--rollback WHERE
--rollback     entdata.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.occurrence_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.occurrence



-- populate wheat occurrence for CBBW-2
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'CBBW-2_OCC1' AS occurrence_name,
    'draft' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    3 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'CBBW-2'
    AND geo.geospatial_object_code = 'CO'
    AND person.username = 'a.carlson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'CBBW-2_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_place.geospatial_object_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in place.geospatial_object



-- populate wheat planting area for CBBW-2
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'CBBW-2_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'CBBW-2_LOC1'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.location_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location



-- populate wheat location for CBBW-2
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'CBBW-2_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'a.carlson'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'CBBW-2_LOC1'
    AND site.geospatial_object_code = 'CO'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'CBBW-2_LOC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.plot_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.plot



-- populate wheat plots for CBBW-2
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS11B00587S-099M-0SY-14M-0WGY'),
        (2,1,2,'CMSS11B00623T-099TOPY-099M-0SY-17M-0WGY'),
        (3,1,3,'CMSS11B00650T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (4,1,4,'CMSS11B00700T-099TOPY-099M-099NJ-099NJ-12WGY-0B'),
        (5,1,5,'CMSS11B00728T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (6,1,6,'CMSS11B00745T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (7,1,7,'CMSS11B00773T-099TOPY-099M-099NJ-099NJ-25WGY-0B'),
        (8,1,8,'CMSS11B00786T-099TOPY-099M-0SY-31M-0WGY'),
        (9,1,9,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (10,1,10,'CMSS11B00807T-099TOPY-099M-099NJ-099NJ-42WGY-0B'),
        (11,1,11,'CMSS11B00861T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (12,1,12,'CMSS11B00908T-099TOPY-099M-099NJ-099NJ-31WGY-0B'),
        (13,1,13,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-2WGY-0B'),
        (14,1,14,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-34WGY-0B'),
        (15,1,15,'CMSS11B00912T-099TOPY-099M-099NJ-099NJ-1WGY-0B'),
        (16,1,16,'CMSS11B00913T-099TOPY-099M-099NJ-099NJ-24WGY-0B'),
        (17,1,17,'CMSS11B00930T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (18,1,18,'CMSS11B00959T-099TOPY-099M-0SY-13M-0WGY'),
        (19,1,19,'CMSS11B00996T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (20,1,20,'CMSS11B01003T-099TOPY-099M-0SY-24M-0WGY'),
        (21,1,21,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (22,1,22,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-12RGY-0B'),
        (23,1,23,'CMSA11Y00305S-099Y-099M-099NJ-099NJ-1RGY-0B'),
        (24,1,24,'CMSA11Y00311S-099Y-099M-099NJ-099NJ-23RGY-0B'),
        (25,1,25,'CMSS11Y00803T-099TOPM-099Y-099M-099NJ-099NJ-41RGY-0B'),
        (26,1,26,'CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B'),
        (27,1,27,'CMSS11Y01166T-099TOPM-099Y-099M-099NJ-099NJ-14RGY-0B'),
        (28,1,28,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-2RGY-0B'),
        (29,1,29,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-11RGY-0B'),
        (30,1,30,'CMSS11B00310S-099M-099NJ-099NJ-24RGY-0B'),
        (31,1,31,'CMSS11B00311S-099M-099NJ-099NJ-7RGY-0B'),
        (32,1,32,'CMSS11B00546S-099M-0SY-4M-0RGY'),
        (33,1,33,'CMSS11B00587S-099M-099NJ-099NJ-15RGY-0B'),
        (34,1,34,'CMSS11B00840T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (35,1,35,'CMSS11B00973T-099TOPY-099M-099NJ-099NJ-7RGY-0B'),
        (36,1,36,'CMSS11B00983T-099TOPY-099M-099NJ-099NJ-5RGY-0B'),
        (37,1,37,'CMSS11B00993T-099TOPY-099M-099NJ-099NJ-13RGY-0B'),
        (38,1,38,'CMSS11B00995T-099TOPY-099M-099NJ-099NJ-3RGY-0B'),
        (39,1,39,'CMSS11B00344S-099M-099NJ-099NJ-3WGY-0M'),
        (40,1,40,'CMSS11B00381S-099M-099NJ-099NJ-38WGY-0M'),
        (41,1,41,'CMSS11B00383S-099M-099NJ-099NJ-18WGY-0M'),
        (42,1,42,'CMSS11B00454S-099M-099NJ-099NJ-20WGY-0M'),
        (43,1,43,'CMSS11B00478S-099M-099NJ-099NJ-2WGY-0M'),
        (44,1,44,'CMSS11B00490S-099M-099NJ-099NJ-12WGY-0M'),
        (45,1,45,'CMSS11B00516S-099M-099NJ-099NJ-12WGY-0M'),
        (46,1,46,'CMSS11B00587S-099M-099NJ-099NJ-9WGY-0M'),
        (47,1,47,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (48,1,48,'CMSS11B00958T-099TOPY-099M-099NJ-099NJ-11WGY-0B'),
        (49,1,49,'CMSS12Y00044S-099Y-099M-0SY-1M-0WGY'),
        (50,1,50,'CMSS12Y00064S-099Y-099M-0SY-26M-0WGY'),
        (51,2,1,'CMSS11B00587S-099M-0SY-14M-0WGY'),
        (52,2,2,'CMSS11B00623T-099TOPY-099M-0SY-17M-0WGY'),
        (53,2,3,'CMSS11B00650T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (54,2,4,'CMSS11B00700T-099TOPY-099M-099NJ-099NJ-12WGY-0B'),
        (55,2,5,'CMSS11B00728T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (56,2,6,'CMSS11B00745T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (57,2,7,'CMSS11B00773T-099TOPY-099M-099NJ-099NJ-25WGY-0B'),
        (58,2,8,'CMSS11B00786T-099TOPY-099M-0SY-31M-0WGY'),
        (59,2,9,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (60,2,10,'CMSS11B00807T-099TOPY-099M-099NJ-099NJ-42WGY-0B'),
        (61,2,11,'CMSS11B00861T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (62,2,12,'CMSS11B00908T-099TOPY-099M-099NJ-099NJ-31WGY-0B'),
        (63,2,13,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-2WGY-0B'),
        (64,2,14,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-34WGY-0B'),
        (65,2,15,'CMSS11B00912T-099TOPY-099M-099NJ-099NJ-1WGY-0B'),
        (66,2,16,'CMSS11B00913T-099TOPY-099M-099NJ-099NJ-24WGY-0B'),
        (67,2,17,'CMSS11B00930T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (68,2,18,'CMSS11B00959T-099TOPY-099M-0SY-13M-0WGY'),
        (69,2,19,'CMSS11B00996T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (70,2,20,'CMSS11B01003T-099TOPY-099M-0SY-24M-0WGY'),
        (71,2,21,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (72,2,22,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-12RGY-0B'),
        (73,2,23,'CMSA11Y00305S-099Y-099M-099NJ-099NJ-1RGY-0B'),
        (74,2,24,'CMSA11Y00311S-099Y-099M-099NJ-099NJ-23RGY-0B'),
        (75,2,25,'CMSS11Y00803T-099TOPM-099Y-099M-099NJ-099NJ-41RGY-0B'),
        (76,2,26,'CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B'),
        (77,2,27,'CMSS11Y01166T-099TOPM-099Y-099M-099NJ-099NJ-14RGY-0B'),
        (78,2,28,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-2RGY-0B'),
        (79,2,29,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-11RGY-0B'),
        (80,2,30,'CMSS11B00310S-099M-099NJ-099NJ-24RGY-0B'),
        (81,2,31,'CMSS11B00311S-099M-099NJ-099NJ-7RGY-0B'),
        (82,2,32,'CMSS11B00546S-099M-0SY-4M-0RGY'),
        (83,2,33,'CMSS11B00587S-099M-099NJ-099NJ-15RGY-0B'),
        (84,2,34,'CMSS11B00840T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (85,2,35,'CMSS11B00973T-099TOPY-099M-099NJ-099NJ-7RGY-0B'),
        (86,2,36,'CMSS11B00983T-099TOPY-099M-099NJ-099NJ-5RGY-0B'),
        (87,2,37,'CMSS11B00993T-099TOPY-099M-099NJ-099NJ-13RGY-0B'),
        (88,2,38,'CMSS11B00995T-099TOPY-099M-099NJ-099NJ-3RGY-0B'),
        (89,2,39,'CMSS11B00344S-099M-099NJ-099NJ-3WGY-0M'),
        (90,2,40,'CMSS11B00381S-099M-099NJ-099NJ-38WGY-0M'),
        (91,2,41,'CMSS11B00383S-099M-099NJ-099NJ-18WGY-0M'),
        (92,2,42,'CMSS11B00454S-099M-099NJ-099NJ-20WGY-0M'),
        (93,2,43,'CMSS11B00478S-099M-099NJ-099NJ-2WGY-0M'),
        (94,2,44,'CMSS11B00490S-099M-099NJ-099NJ-12WGY-0M'),
        (95,2,45,'CMSS11B00516S-099M-099NJ-099NJ-12WGY-0M'),
        (96,2,46,'CMSS11B00587S-099M-099NJ-099NJ-9WGY-0M'),
        (97,2,47,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (98,2,48,'CMSS11B00958T-099TOPY-099M-099NJ-099NJ-11WGY-0B'),
        (99,2,49,'CMSS12Y00044S-099Y-099M-0SY-1M-0WGY'),
        (100,2,50,'CMSS12Y00064S-099Y-099M-0SY-26M-0WGY'),
        (101,3,1,'CMSS11B00587S-099M-0SY-14M-0WGY'),
        (102,3,2,'CMSS11B00623T-099TOPY-099M-0SY-17M-0WGY'),
        (103,3,3,'CMSS11B00650T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (104,3,4,'CMSS11B00700T-099TOPY-099M-099NJ-099NJ-12WGY-0B'),
        (105,3,5,'CMSS11B00728T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (106,3,6,'CMSS11B00745T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (107,3,7,'CMSS11B00773T-099TOPY-099M-099NJ-099NJ-25WGY-0B'),
        (108,3,8,'CMSS11B00786T-099TOPY-099M-0SY-31M-0WGY'),
        (109,3,9,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (110,3,10,'CMSS11B00807T-099TOPY-099M-099NJ-099NJ-42WGY-0B'),
        (111,3,11,'CMSS11B00861T-099TOPY-099M-099NJ-099NJ-20WGY-0B'),
        (112,3,12,'CMSS11B00908T-099TOPY-099M-099NJ-099NJ-31WGY-0B'),
        (113,3,13,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-2WGY-0B'),
        (114,3,14,'CMSS11B00910T-099TOPY-099M-099NJ-099NJ-34WGY-0B'),
        (115,3,15,'CMSS11B00912T-099TOPY-099M-099NJ-099NJ-1WGY-0B'),
        (116,3,16,'CMSS11B00913T-099TOPY-099M-099NJ-099NJ-24WGY-0B'),
        (117,3,17,'CMSS11B00930T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (118,3,18,'CMSS11B00959T-099TOPY-099M-0SY-13M-0WGY'),
        (119,3,19,'CMSS11B00996T-099TOPY-099M-099NJ-099NJ-16WGY-0B'),
        (120,3,20,'CMSS11B01003T-099TOPY-099M-0SY-24M-0WGY'),
        (121,3,21,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (122,3,22,'CMSA11Y00304S-099Y-099M-099NJ-099NJ-12RGY-0B'),
        (123,3,23,'CMSA11Y00305S-099Y-099M-099NJ-099NJ-1RGY-0B'),
        (124,3,24,'CMSA11Y00311S-099Y-099M-099NJ-099NJ-23RGY-0B'),
        (125,3,25,'CMSS11Y00803T-099TOPM-099Y-099M-099NJ-099NJ-41RGY-0B'),
        (126,3,26,'CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B'),
        (127,3,27,'CMSS11Y01166T-099TOPM-099Y-099M-099NJ-099NJ-14RGY-0B'),
        (128,3,28,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-2RGY-0B'),
        (129,3,29,'CMSS11Y01173T-099TOPM-099Y-099M-099NJ-099NJ-11RGY-0B'),
        (130,3,30,'CMSS11B00310S-099M-099NJ-099NJ-24RGY-0B'),
        (131,3,31,'CMSS11B00311S-099M-099NJ-099NJ-7RGY-0B'),
        (132,3,32,'CMSS11B00546S-099M-0SY-4M-0RGY'),
        (133,3,33,'CMSS11B00587S-099M-099NJ-099NJ-15RGY-0B'),
        (134,3,34,'CMSS11B00840T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (135,3,35,'CMSS11B00973T-099TOPY-099M-099NJ-099NJ-7RGY-0B'),
        (136,3,36,'CMSS11B00983T-099TOPY-099M-099NJ-099NJ-5RGY-0B'),
        (137,3,37,'CMSS11B00993T-099TOPY-099M-099NJ-099NJ-13RGY-0B'),
        (138,3,38,'CMSS11B00995T-099TOPY-099M-099NJ-099NJ-3RGY-0B'),
        (139,3,39,'CMSS11B00344S-099M-099NJ-099NJ-3WGY-0M'),
        (140,3,40,'CMSS11B00381S-099M-099NJ-099NJ-38WGY-0M'),
        (141,3,41,'CMSS11B00383S-099M-099NJ-099NJ-18WGY-0M'),
        (142,3,42,'CMSS11B00454S-099M-099NJ-099NJ-20WGY-0M'),
        (143,3,43,'CMSS11B00478S-099M-099NJ-099NJ-2WGY-0M'),
        (144,3,44,'CMSS11B00490S-099M-099NJ-099NJ-12WGY-0M'),
        (145,3,45,'CMSS11B00516S-099M-099NJ-099NJ-12WGY-0M'),
        (146,3,46,'CMSS11B00587S-099M-099NJ-099NJ-9WGY-0M'),
        (147,3,47,'CMSS11B00787T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (148,3,48,'CMSS11B00958T-099TOPY-099M-099NJ-099NJ-11WGY-0B'),
        (149,3,49,'CMSS12Y00044S-099Y-099M-0SY-1M-0WGY'),
        (150,3,50,'CMSS12Y00064S-099Y-099M-0SY-26M-0WGY')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'CBBW-2_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'CBBW-2_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-2_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.planting_instruction_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.planting_instruction



-- populate wheat planting instructions for CBBW-2
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    entlist.entry_list_name = 'CBBW-2_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.package_log_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.package_log



-- populate wheat package logs for CBBW-2
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    entlist.entry_list_name = 'CBBW-2_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_package_log_id_in_experiment.planting_instruction_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block package_log_id in experiment.planting_instruction



-- populate wheat package log references for CBBW-2
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross_part_2 context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross



-- populate wheat crosses for CBBW-2
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'simple cross' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.carlson'
WHERE
    expt.experiment_name = 'CBBW-2'
    AND entf.entry_number BETWEEN 1 AND 25
    AND entm.entry_number BETWEEN 26 AND 50
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-2'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross_parent_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross_parent



-- populate wheat cross parents for CBBW-2
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'a.carlson'
        WHERE
            expt.experiment_name = 'CBBW-2'
            AND ent.entry_number BETWEEN 1 AND 25
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'a.carlson'
            WHERE
                expt.experiment_name = 'CBBW-2'
                AND ent.entry_number BETWEEN 26 AND 50
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'CBBW-2'
--rollback ;



--changeset postgres:update_experiment_statuses_for_wheat_crossing_block_experiment_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update experiment statuses for wheat crossing block experiment



-- update experiment statuses for CBBW-2
UPDATE
    experiment.experiment
SET
    experiment_status = 'planted'
WHERE
    experiment_name = 'CBBW-2'
;

UPDATE
    experiment.entry_list
SET
    entry_list_status = 'created'
WHERE
    entry_list_name = 'CBBW-2_ENTLIST'
;

UPDATE
    experiment.occurrence
SET
    occurrence_status = 'planted'
WHERE
    occurrence_name = 'CBBW-2_OCC1'
;

UPDATE
    experiment.location
SET
    location_status = 'committed'
WHERE
    location_name = 'CBBW-2_LOC1'
;


-- revert changes
--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'entry list created; crosses created; design generated; occurrences created'
--rollback WHERE
--rollback     experiment_name = 'CBBW-2'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_status = 'draft'
--rollback WHERE
--rollback     entry_list_name = 'CBBW-2_ENTLIST'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_status = 'draft'
--rollback WHERE
--rollback     occurrence_name = 'CBBW-2_OCC1'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.location
--rollback SET
--rollback     location_status = 'committed'
--rollback WHERE
--rollback     location_name = 'CBBW-2_LOC1'
--rollback ;



--changeset postgres:populate_pa_coordinates_for_wheat_crossing_block_experiment_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate planting area coordinates for wheat crossing block experiment



-- populate pa coordinates for CBBW-2
UPDATE
    experiment.plot
SET
    pa_x = design_x,
    pa_y = design_y
FROM
    experiment.occurrence AS occ
WHERE
    plot.occurrence_id = occ.id
    AND occ.occurrence_name = 'CBBW-2_OCC1'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     pa_x = NULL,
--rollback     pa_y = NULL
--rollback FROM
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-2_OCC1'
--rollback ;



--changeset postgres:update_cross_method_in_germplasm.cross_for_wheat_crossing_block_experiment_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update cross_method in germplasm.cross for wheat crossing block experiment



-- update cross methods for CBBW-2
UPDATE
    germplasm.cross AS crs
SET
    cross_method = 'SIMPLE CROSS'
FROM
    experiment.experiment AS expt
WHERE
    crs.experiment_id = expt.id
    AND expt.experiment_name = 'CBBW-2'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.cross AS crs
--rollback SET
--rollback     cross_method = 'simple cross'
--rollback FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-2'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.location_occurrence_group_part_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location_occurrence_group



-- populate wheat location occurrence groups for CBBW-2
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'CBBW-2_LOC1'
    AND occ.occurrence_name = 'CBBW-2_OCC1'
    AND prs.username = 'a.carlson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'CBBW-2_LOC1'
--rollback     AND occ.occurrence_name = 'CBBW-2_OCC1'
--rollback ;
