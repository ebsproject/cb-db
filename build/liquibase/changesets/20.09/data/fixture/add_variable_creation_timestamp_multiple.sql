--liquibase formatted sql

--changeset postgres:add_variable_creation_timestamp_multiple context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-147 Add variable CREATION_TIMESTAMP_MULTIPLE



--add variable
INSERT INTO
    master.variable
        (
            id,abbrev,label,name,data_type,not_null,type,status,
            display_name,ontology_reference,bibliographical_reference,property_id,
            method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
            creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
            usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
            field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
        )
VALUES
    (
        1242,
        'CREATION_TIMESTAMP_MULTIPLE',
        'CREATION TIMESTAMP',
        'creation timestamp multiple',
        'json',
        FALSE,
        'metadata',
        'active',
        'Creation Timestamp',
        NULL,
        NULL,
        624,
        939,
        931,
        NULL,
        NULL,
        NULL,
        '2015-01-15 10:06:03.281',
        1,
        '2015-01-19 16:36:43.491',
        1,
        NULL,
        FALSE,
        'Variable used for multiple recording of creation timestamp',
        NULL,
        'application',
        'study',
        FALSE,
        NULL,
        FALSE,
        'timestamp',
        NULL,
        NULL,
        727,
        NULL,
        NULL,
        'object',
        NULL,
        NULL,
        NULL
    );

SELECT SETVAL('master.variable_id_seq', COALESCE(MAX(id), 1)) FROM master.variable;



--rollback DELETE FROM master.variable WHERE abbrev='CREATION_TIMESTAMP_MULTIPLE';
--rollback SELECT SETVAL('master.variable_id_seq', COALESCE(MAX(id), 1)) FROM master.variable;


--changeset postgres:delete_variable_creation_timestamp_multiple context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Delete variable CREATION_TIMESTAMP_MULTIPLE


-- delete variable
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
SET session_replication_role = replica;

DELETE FROM master.variable WHERE abbrev = 'CREATION_TIMESTAMP_MULTIPLE';

--ALTER TABLE master.variable ENABLE TRIGGER ALL;
SET session_replication_role = origin;

ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;



--rollback INSERT INTO
--rollback     master.variable
--rollback         (
--rollback             id,abbrev,label,name,data_type,not_null,type,status,
--rollback             display_name,ontology_reference,bibliographical_reference,property_id,
--rollback             method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
--rollback             creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
--rollback             usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
--rollback             field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
--rollback         )
--rollback VALUES
--rollback     (
--rollback         1242,
--rollback         'CREATION_TIMESTAMP_MULTIPLE',
--rollback         'CREATION TIMESTAMP',
--rollback         'creation timestamp multiple',
--rollback         'json',
--rollback         FALSE,
--rollback         'metadata',
--rollback         'active',
--rollback         'Creation Timestamp',
--rollback         NULL,
--rollback         NULL,
--rollback         624,
--rollback         939,
--rollback         931,
--rollback         NULL,
--rollback         NULL,
--rollback         NULL,
--rollback         '2015-01-15 10:06:03.281',
--rollback         1,
--rollback         '2015-01-19 16:36:43.491',
--rollback         1,
--rollback         NULL,
--rollback         FALSE,
--rollback         'Variable used for multiple recording of creation timestamp',
--rollback         NULL,
--rollback         'application',
--rollback         'study',
--rollback         FALSE,
--rollback         NULL,
--rollback         FALSE,
--rollback         'timestamp',
--rollback         NULL,
--rollback         NULL,
--rollback         727,
--rollback         NULL,
--rollback         NULL,
--rollback         'object',
--rollback         NULL,
--rollback         NULL,
--rollback         NULL
--rollback     );


--changeset postgres:reinsert_variable_creation_timestamp_multiple context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Re-insert variable CREATION_TIMESTAMP_MULTIPLE


-- add variable
INSERT INTO
    master.variable
        (
            id,abbrev,label,name,data_type,not_null,type,status,
            display_name,ontology_reference,bibliographical_reference,property_id,
            method_id,scale_id,variable_set,synonym,remarks,creation_timestamp,
            creator_id,modification_timestamp,modifier_id,notes,is_void,description,default_value,
            usage,data_level,is_column,column_table,is_computed,member_data_type,target_variable_id,
            field_prop,member_variable_id,target_model,class_variable_id,json_type,notif,event_log,target_table
        )
VALUES
    (
        1242,
        'CREATION_TIMESTAMP_MULTIPLE',
        'CREATION TIMESTAMP',
        'creation timestamp multiple',
        'json',
        FALSE,
        'metadata',
        'active',
        'Creation Timestamp',
        NULL,
        NULL,
        624,
        939,
        931,
        NULL,
        NULL,
        NULL,
        '2015-01-15 10:06:03.281',
        1,
        '2015-01-19 16:36:43.491',
        1,
        NULL,
        FALSE,
        'Variable used for multiple recording of creation timestamp',
        NULL,
        'application',
        'study',
        FALSE,
        NULL,
        FALSE,
        'timestamp',
        NULL,
        NULL,
        727,
        NULL,
        NULL,
        'object',
        NULL,
        NULL,
        NULL
    );



--revert changes
--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'CREATION_TIMESTAMP_MULTIPLE';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
--rollback --ALTER TABLE master.variable DISABLE TRIGGER variable_update_variable_document_tgr;
