--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.experiment



-- populate wheat experiment F1SIMPLEBW-1
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F1') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2014 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2014A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'F1SIMPLEBW-1' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'F1SIMPLEBW-1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry_list



-- populate wheat entry list F1SIMPLEBW-1_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'F1SIMPLEBW-1_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'F1SIMPLEBW-1') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry



-- populate wheat entries for F1SIMPLEBW-1
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS14Y00001S','Y13-14CBBW1'),
        (2,'CMSS14Y00002S','Y13-14CBBW1'),
        (3,'CMSS14Y00003S','Y13-14CBBW2'),
        (4,'CMSS14Y00004S','Y13-14CBBW3'),
        (5,'CMSS14Y00005S','Y13-14CBBW3'),
        (6,'CMSS14Y00006S','Y13-14CBBW3'),
        (7,'CMSS14Y00007S','Y13-14CBBW3'),
        (8,'CMSS14Y00008S','Y13-14CBBW3'),
        (9,'CMSS14Y00009S','Y13-14CBBW3'),
        (10,'CMSS14Y00010S','Y13-14CBBW3'),
        (11,'CMSS14Y00011S','Y13-14CBBW3'),
        (12,'CMSS14Y00012S','Y13-14CBBW4'),
        (13,'CMSS14Y00013S','Y13-14CBBW4'),
        (14,'CMSS14Y00014S','Y13-14CBBW4'),
        (15,'CMSS14Y00015S','Y13-14CBBW4'),
        (16,'CMSS14Y00016S','Y13-14CBBW4'),
        (17,'CMSS14Y00017S','Y13-14CBBW4'),
        (18,'CMSS14Y00018S','Y13-14CBBW5'),
        (19,'CMSS14Y00019S','Y13-14CBBW5'),
        (20,'CMSS14Y00020S','Y13-14CBBW7'),
        (21,'CMSS14Y00021S','Y13-14CBBW7'),
        (22,'CMSS14Y00022S','Y13-14CBBW7'),
        (23,'CMSS14Y00023S','Y13-14CBBW7'),
        (24,'CMSS14Y00024S','Y13-14CBBW8'),
        (25,'CMSS14Y00025S','Y13-14CBBW9'),
        (26,'CMSS14Y00026S','Y13-14CBBW11'),
        (27,'CMSS14Y00027S','Y13-14CBBW11'),
        (28,'CMSS14Y00028S','Y13-14CBBW11'),
        (29,'CMSS14Y00029S','Y13-14CBBW11'),
        (30,'CMSS14Y00030S','Y13-14CBBW11'),
        (31,'CMSS14Y00031S','Y13-14CBBW11'),
        (32,'CMSS14Y00032S','Y13-14CBBW11'),
        (33,'CMSS14Y00033S','Y13-14CBBW12'),
        (34,'CMSS14Y00034S','Y13-14CBBW12'),
        (35,'CMSS14Y00035S','Y13-14CBBW12'),
        (36,'CMSS14Y00036S','Y13-14CBBW12'),
        (37,'CMSS14Y00037S','Y13-14CBBW13'),
        (38,'CMSS14Y00038S','Y13-14CBBW13'),
        (39,'CMSS14Y00039S','Y13-14CBBW13'),
        (40,'CMSS14Y00040S','Y13-14CBBW15'),
        (41,'CMSS14Y00041S','Y13-14CBBW18'),
        (42,'CMSS14Y00042S','Y13-14CBBW20'),
        (43,'CMSS14Y00043S','Y13-14CBBW20'),
        (44,'CMSS14Y00044S','Y13-14CBBW20'),
        (45,'CMSS14Y00045S','Y13-14CBBW20'),
        (46,'CMSS14Y00046S','Y13-14CBBW20'),
        (47,'CMSS14Y00047S','Y13-14CBBW27'),
        (48,'CMSS14Y00048S','Y13-14CBBW28'),
        (49,'CMSS14Y00049S','Y13-14CBBW28'),
        (50,'CMSS14Y00050S','Y13-14CBBW28'),
        (51,'CMSS14Y00051S','Y13-14CBBW28'),
        (52,'CMSS14Y00052S','Y13-14CBBW28'),
        (53,'CMSS14Y00053S','Y13-14CBBW29'),
        (54,'CMSS14Y00054S','Y13-14CBBW29'),
        (55,'CMSS14Y00055S','Y13-14CBBW29'),
        (56,'CMSS14Y00056S','Y13-14CBBW29'),
        (57,'CMSS14Y00057S','Y13-14CBBW29'),
        (58,'CMSS14Y00058S','Y13-14CBBW29'),
        (59,'CMSS14Y00059S','Y13-14CBBW29'),
        (60,'CMSS14Y00060S','Y13-14CBBW29'),
        (61,'CMSS14Y00061S','Y13-14CBBW30'),
        (62,'CMSS14Y00062S','Y13-14CBBW30'),
        (63,'CMSS14Y00063S','Y13-14CBBW30'),
        (64,'CMSS14Y00064S','Y13-14CBBW30'),
        (65,'CMSS14Y00065S','Y13-14CBBW30'),
        (66,'CMSS14Y00066S','Y13-14CBBW31'),
        (67,'CMSS14Y00067S','Y13-14CBBW31'),
        (68,'CMSS14Y00068S','Y13-14CBBW31'),
        (69,'CMSS14Y00069S','Y13-14CBBW31'),
        (70,'CMSS14Y00070S','Y13-14CBBW31'),
        (71,'CMSS14Y00071S','Y13-14CBBW31'),
        (72,'CMSS14Y00072S','Y13-14CBBW32'),
        (73,'CMSS14Y00073S','Y13-14CBBW32'),
        (74,'CMSS14Y00074S','Y13-14CBBW32'),
        (75,'CMSS14Y00075S','Y13-14CBBW32'),
        (76,'CMSS14Y00076S','Y13-14CBBW32'),
        (77,'CMSS14Y00077S','Y13-14CBBW32'),
        (78,'CMSS14Y00078S','Y13-14CBBW32'),
        (79,'CMSS14Y00079S','Y13-14CBBW32'),
        (80,'CMSS14Y00080S','Y13-14CBBW32'),
        (81,'CMSS14Y00081S','Y13-14CBBW33'),
        (82,'CMSS14Y00082S','Y13-14CBBW33'),
        (83,'CMSS14Y00083S','Y13-14CBBW33'),
        (84,'CMSS14Y00084S','Y13-14CBBW33'),
        (85,'CMSS14Y00085S','Y13-14CBBW33'),
        (86,'CMSS14Y00086S','Y13-14CBBW33'),
        (87,'CMSS14Y00087S','Y13-14CBBW34'),
        (88,'CMSS14Y00088S','Y13-14CBBW34'),
        (89,'CMSS14Y00089S','Y13-14CBBW34'),
        (90,'CMSS14Y00090S','Y13-14CBBW34'),
        (91,'CMSS14Y00091S','Y13-14CBBW34'),
        (92,'CMSS14Y00092S','Y13-14CBBW34'),
        (93,'CMSS14Y00093S','Y13-14CBBW39'),
        (94,'CMSS14Y00094S','Y13-14CBBW39'),
        (95,'CMSS14Y00095S','Y13-14CBBW41'),
        (96,'CMSS14Y00096S','Y13-14CBBW41'),
        (97,'CMSS14Y00097S','Y13-14CBBW41'),
        (98,'CMSS14Y00098S','Y13-14CBBW43'),
        (99,'CMSS14Y00099S','Y13-14CBBW44'),
        (100,'CMSS14Y00100S','Y13-14CBBW46'),
        (101,'CMSS14Y00101S','Y13-14CBBW50'),
        (102,'CMSS14Y00102S','Y13-14CBBW50'),
        (103,'CMSS14Y00103S','Y13-14CBBW54'),
        (104,'CMSS14Y00104S','Y13-14CBBW54'),
        (105,'CMSS14Y00105S','Y13-14CBBW55'),
        (106,'CMSS14Y00106S','Y13-14CBBW56'),
        (107,'CMSS14Y00107S','Y13-14CBBW56'),
        (108,'CMSS14Y00108S','Y13-14CBBW56'),
        (109,'CMSS14Y00109S','Y13-14CBBW56'),
        (110,'CMSS14Y00110S','Y13-14CBBW63'),
        (111,'CMSS14Y00111S','Y13-14CBBW63'),
        (112,'CMSS14Y00112S','Y13-14CBBW65'),
        (113,'CMSS14Y00113S','Y13-14CBBW65'),
        (114,'CMSS14Y00114S','Y13-14CBBW69'),
        (115,'CMSS14Y00115S','Y13-14CBBW69'),
        (116,'CMSS14Y00116S','Y13-14CBBW69'),
        (117,'CMSS14Y00117S','Y13-14CBBW75'),
        (118,'CMSS14Y00118S','Y13-14CBBW75'),
        (119,'CMSS14Y00119S','Y13-14CBBW78'),
        (120,'CMSS14Y00120S','Y13-14CBBW78'),
        (121,'CMSS14Y00121S','Y13-14CBBW79'),
        (122,'CMSS14Y00122S','Y13-14CBBW79'),
        (123,'CMSS14Y00123S','Y13-14CBBW79'),
        (124,'CMSS14Y00124S','Y13-14CBBW84'),
        (125,'CMSS14Y00125S','Y13-14CBBW84'),
        (126,'CMSS14Y00126S','Y13-14CBBW84'),
        (127,'CMSS14Y00127S','Y13-14CBBW86'),
        (128,'CMSS14Y00128S','Y13-14CBBW86'),
        (129,'CMSS14Y00129S','Y13-14CBBW86'),
        (130,'CMSS14Y00130S','Y13-14CBBW87'),
        (131,'CMSS14Y00131S','Y13-14CBBW88'),
        (132,'CMSS14Y00132S','Y13-14CBBW88'),
        (133,'CMSS14Y00133S','Y13-14CBBW90'),
        (134,'CMSS14Y00134S','Y13-14CBBW90'),
        (135,'CMSS14Y00135S','Y13-14CBBW90'),
        (136,'CMSS14Y00136S','Y13-14CBBW90'),
        (137,'CMSS14Y00137S','Y13-14CBBW90'),
        (138,'CMSS14Y00138S','Y13-14CBBW90'),
        (139,'CMSS14Y00139S','Y13-14CBBW90'),
        (140,'CMSS14Y00140S','Y13-14CBBW90'),
        (141,'CMSS14Y00141S','Y13-14CBBW90'),
        (142,'CMSS14Y00142S','Y13-14CBBW90'),
        (143,'CMSS14Y00143S','Y13-14CBBW90'),
        (144,'CMSS14Y00144S','Y13-14CBBW91'),
        (145,'CMSS14Y00145S','Y13-14CBBW93'),
        (146,'CMSS14Y00146S','Y13-14CBBW93'),
        (147,'CMSS14Y00147S','Y13-14CBBW93'),
        (148,'CMSS14Y00148S','Y13-14CBBW93'),
        (149,'CMSS14Y00149S','Y13-14CBBW94'),
        (150,'CMSS14Y00150S','Y13-14CBBW94')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.occurrence



-- populate wheat occurrence for F1SIMPLEBW-1
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'F1SIMPLEBW-1_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'F1SIMPLEBW-1'
    AND geo.geospatial_object_code = 'EB'
    AND person.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'F1SIMPLEBW-1_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in place.geospatial_object



-- populate wheat planting area for F1SIMPLEBW-1
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'F1SIMPLEBW-1_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'F1SIMPLEBW-1_LOC1'
--rollback ;


--changeset postgres:populate_wheat_f1_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location



-- populate wheat location for F1SIMPLEBW-1
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'F1SIMPLEBW-1_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'nicola.costa'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'F1SIMPLEBW-1_LOC1'
    AND site.geospatial_object_code = 'EB'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'F1SIMPLEBW-1_LOC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.plot



-- populate wheat plots for F1SIMPLEBW-1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS14Y00001S'),
        (2,1,2,'CMSS14Y00002S'),
        (3,1,3,'CMSS14Y00003S'),
        (4,1,4,'CMSS14Y00004S'),
        (5,1,5,'CMSS14Y00005S'),
        (6,1,6,'CMSS14Y00006S'),
        (7,1,7,'CMSS14Y00007S'),
        (8,1,8,'CMSS14Y00008S'),
        (9,1,9,'CMSS14Y00009S'),
        (10,1,10,'CMSS14Y00010S'),
        (11,1,11,'CMSS14Y00011S'),
        (12,1,12,'CMSS14Y00012S'),
        (13,1,13,'CMSS14Y00013S'),
        (14,1,14,'CMSS14Y00014S'),
        (15,1,15,'CMSS14Y00015S'),
        (16,1,16,'CMSS14Y00016S'),
        (17,1,17,'CMSS14Y00017S'),
        (18,1,18,'CMSS14Y00018S'),
        (19,1,19,'CMSS14Y00019S'),
        (20,1,20,'CMSS14Y00020S'),
        (21,1,21,'CMSS14Y00021S'),
        (22,1,22,'CMSS14Y00022S'),
        (23,1,23,'CMSS14Y00023S'),
        (24,1,24,'CMSS14Y00024S'),
        (25,1,25,'CMSS14Y00025S'),
        (26,1,26,'CMSS14Y00026S'),
        (27,1,27,'CMSS14Y00027S'),
        (28,1,28,'CMSS14Y00028S'),
        (29,1,29,'CMSS14Y00029S'),
        (30,1,30,'CMSS14Y00030S'),
        (31,1,31,'CMSS14Y00031S'),
        (32,1,32,'CMSS14Y00032S'),
        (33,1,33,'CMSS14Y00033S'),
        (34,1,34,'CMSS14Y00034S'),
        (35,1,35,'CMSS14Y00035S'),
        (36,1,36,'CMSS14Y00036S'),
        (37,1,37,'CMSS14Y00037S'),
        (38,1,38,'CMSS14Y00038S'),
        (39,1,39,'CMSS14Y00039S'),
        (40,1,40,'CMSS14Y00040S'),
        (41,1,41,'CMSS14Y00041S'),
        (42,1,42,'CMSS14Y00042S'),
        (43,1,43,'CMSS14Y00043S'),
        (44,1,44,'CMSS14Y00044S'),
        (45,1,45,'CMSS14Y00045S'),
        (46,1,46,'CMSS14Y00046S'),
        (47,1,47,'CMSS14Y00047S'),
        (48,1,48,'CMSS14Y00048S'),
        (49,1,49,'CMSS14Y00049S'),
        (50,1,50,'CMSS14Y00050S'),
        (51,1,51,'CMSS14Y00051S'),
        (52,1,52,'CMSS14Y00052S'),
        (53,1,53,'CMSS14Y00053S'),
        (54,1,54,'CMSS14Y00054S'),
        (55,1,55,'CMSS14Y00055S'),
        (56,1,56,'CMSS14Y00056S'),
        (57,1,57,'CMSS14Y00057S'),
        (58,1,58,'CMSS14Y00058S'),
        (59,1,59,'CMSS14Y00059S'),
        (60,1,60,'CMSS14Y00060S'),
        (61,1,61,'CMSS14Y00061S'),
        (62,1,62,'CMSS14Y00062S'),
        (63,1,63,'CMSS14Y00063S'),
        (64,1,64,'CMSS14Y00064S'),
        (65,1,65,'CMSS14Y00065S'),
        (66,1,66,'CMSS14Y00066S'),
        (67,1,67,'CMSS14Y00067S'),
        (68,1,68,'CMSS14Y00068S'),
        (69,1,69,'CMSS14Y00069S'),
        (70,1,70,'CMSS14Y00070S'),
        (71,1,71,'CMSS14Y00071S'),
        (72,1,72,'CMSS14Y00072S'),
        (73,1,73,'CMSS14Y00073S'),
        (74,1,74,'CMSS14Y00074S'),
        (75,1,75,'CMSS14Y00075S'),
        (76,1,76,'CMSS14Y00076S'),
        (77,1,77,'CMSS14Y00077S'),
        (78,1,78,'CMSS14Y00078S'),
        (79,1,79,'CMSS14Y00079S'),
        (80,1,80,'CMSS14Y00080S'),
        (81,1,81,'CMSS14Y00081S'),
        (82,1,82,'CMSS14Y00082S'),
        (83,1,83,'CMSS14Y00083S'),
        (84,1,84,'CMSS14Y00084S'),
        (85,1,85,'CMSS14Y00085S'),
        (86,1,86,'CMSS14Y00086S'),
        (87,1,87,'CMSS14Y00087S'),
        (88,1,88,'CMSS14Y00088S'),
        (89,1,89,'CMSS14Y00089S'),
        (90,1,90,'CMSS14Y00090S'),
        (91,1,91,'CMSS14Y00091S'),
        (92,1,92,'CMSS14Y00092S'),
        (93,1,93,'CMSS14Y00093S'),
        (94,1,94,'CMSS14Y00094S'),
        (95,1,95,'CMSS14Y00095S'),
        (96,1,96,'CMSS14Y00096S'),
        (97,1,97,'CMSS14Y00097S'),
        (98,1,98,'CMSS14Y00098S'),
        (99,1,99,'CMSS14Y00099S'),
        (100,1,100,'CMSS14Y00100S'),
        (101,1,101,'CMSS14Y00101S'),
        (102,1,102,'CMSS14Y00102S'),
        (103,1,103,'CMSS14Y00103S'),
        (104,1,104,'CMSS14Y00104S'),
        (105,1,105,'CMSS14Y00105S'),
        (106,1,106,'CMSS14Y00106S'),
        (107,1,107,'CMSS14Y00107S'),
        (108,1,108,'CMSS14Y00108S'),
        (109,1,109,'CMSS14Y00109S'),
        (110,1,110,'CMSS14Y00110S'),
        (111,1,111,'CMSS14Y00111S'),
        (112,1,112,'CMSS14Y00112S'),
        (113,1,113,'CMSS14Y00113S'),
        (114,1,114,'CMSS14Y00114S'),
        (115,1,115,'CMSS14Y00115S'),
        (116,1,116,'CMSS14Y00116S'),
        (117,1,117,'CMSS14Y00117S'),
        (118,1,118,'CMSS14Y00118S'),
        (119,1,119,'CMSS14Y00119S'),
        (120,1,120,'CMSS14Y00120S'),
        (121,1,121,'CMSS14Y00121S'),
        (122,1,122,'CMSS14Y00122S'),
        (123,1,123,'CMSS14Y00123S'),
        (124,1,124,'CMSS14Y00124S'),
        (125,1,125,'CMSS14Y00125S'),
        (126,1,126,'CMSS14Y00126S'),
        (127,1,127,'CMSS14Y00127S'),
        (128,1,128,'CMSS14Y00128S'),
        (129,1,129,'CMSS14Y00129S'),
        (130,1,130,'CMSS14Y00130S'),
        (131,1,131,'CMSS14Y00131S'),
        (132,1,132,'CMSS14Y00132S'),
        (133,1,133,'CMSS14Y00133S'),
        (134,1,134,'CMSS14Y00134S'),
        (135,1,135,'CMSS14Y00135S'),
        (136,1,136,'CMSS14Y00136S'),
        (137,1,137,'CMSS14Y00137S'),
        (138,1,138,'CMSS14Y00138S'),
        (139,1,139,'CMSS14Y00139S'),
        (140,1,140,'CMSS14Y00140S'),
        (141,1,141,'CMSS14Y00141S'),
        (142,1,142,'CMSS14Y00142S'),
        (143,1,143,'CMSS14Y00143S'),
        (144,1,144,'CMSS14Y00144S'),
        (145,1,145,'CMSS14Y00145S'),
        (146,1,146,'CMSS14Y00146S'),
        (147,1,147,'CMSS14Y00147S'),
        (148,1,148,'CMSS14Y00148S'),
        (149,1,149,'CMSS14Y00149S'),
        (150,1,150,'CMSS14Y00150S')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'F1SIMPLEBW-1_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'F1SIMPLEBW-1_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-1_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.planting_instruction



-- populate wheat planting instructions for F1SIMPLEBW-1
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.package_log



-- populate wheat package logs for F1SIMPLEBW-1
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery package_log_id in experiment.planting_instruction



-- populate wheat package log references for F1SIMPLEBW-1
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-1_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross



-- populate wheat crosses for F1SIMPLEBW-1
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    expt.experiment_name = 'F1SIMPLEBW-1'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross_parent



-- populate wheat cross parents for F1SIMPLEBW-1
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'nicola.costa'
        WHERE
            expt.experiment_name = 'F1SIMPLEBW-1'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location_occurrence_group



-- populate wheat location occurrence groups for F1SIMPLEBW-1
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'F1SIMPLEBW-1_LOC1'
    AND occ.occurrence_name = 'F1SIMPLEBW-1_OCC1'
    AND prs.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'F1SIMPLEBW-1_LOC1'
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-1_OCC1'
--rollback ;



--changeset postgres:update_location_year_in_experiment.location_for_wheat_nursery_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Update location_year in experiment.location for wheat nursery experiment


UPDATE
    experiment.location AS loc
SET
    location_year = '2014',
    season_id = ssn.id
FROM
    tenant.season AS ssn
WHERE
    loc.location_name IN (
        'F1SIMPLEBW-1_LOC1',
        'F1SIMPLEBW-2_LOC1',
        'F1SIMPLEBW-3_LOC1',
        'F1SIMPLEBW-4_LOC1',
        'F1SIMPLEBW-5_LOC1'
    )
    AND ssn.season_code = 'A'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.location AS loc
--rollback SET
--rollback     location_year = '2017',
--rollback     season_id = season.id
--rollback FROM
--rollback     tenant.season
--rollback WHERE
--rollback     loc.location_name IN (
--rollback         'F1SIMPLEBW-1_LOC1',
--rollback         'F1SIMPLEBW-2_LOC1',
--rollback         'F1SIMPLEBW-3_LOC1',
--rollback         'F1SIMPLEBW-4_LOC1',
--rollback         'F1SIMPLEBW-5_LOC1'
--rollback     )
--rollback     AND season.season_code = 'B'
--rollback ;