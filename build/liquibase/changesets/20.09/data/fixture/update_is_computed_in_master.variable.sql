--liquibase formatted sql

--changeset postgres:update_is_computed_in_master.variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1150 Update is_computed in master.variable



UPDATE master.variable SET is_computed=TRUE WHERE abbrev='ADJAYLD_KG_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='ADJAYLD_KG_CONT3';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='ADJAYLD_KG_CONT4';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='ADJAYLD_KG_CONT5';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='ADJYLD3_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='AREA_FACT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='CML_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='DISEASE_INDEX';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='DTH_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='MAT_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='MAT_CONT2';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='MF_HARV_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='PANNO_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='PNL_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='TOTAL_HILL_CONT';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='TOTAL_PLANT_TESTED';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='TPLYLD_CONT_PS';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_0_CONT1';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT2';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON2';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON3';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON5';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON6';
UPDATE master.variable SET is_computed=TRUE WHERE abbrev='YLD_CONT_TON7';



--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='ADJAYLD_KG_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='ADJAYLD_KG_CONT3';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='ADJAYLD_KG_CONT4';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='ADJAYLD_KG_CONT5';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='ADJYLD3_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='AREA_FACT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='CML_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='DISEASE_INDEX';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='DTH_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='MAT_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='MAT_CONT2';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='MF_HARV_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='PANNO_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='PNL_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='TOTAL_HILL_CONT';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='TOTAL_PLANT_TESTED';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='TPLYLD_CONT_PS';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_0_CONT1';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT2';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON2';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON3';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON5';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON6';
--rollback UPDATE master.variable SET is_computed=FALSE WHERE abbrev='YLD_CONT_TON7';



--changeset postgres:reapply_is_computed_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Re-apply is_computed in master.variable



-- update is_computed attribute
UPDATE
    master.variable
SET
    is_computed = TRUE
WHERE
    abbrev IN (
        'ADJAYLD_KG_CONT', 'ADJAYLD_KG_CONT3', 'ADJAYLD_KG_CONT4', 'ADJAYLD_KG_CONT5',
        'ADJYLD3_CONT', 'AREA_FACT', 'CML_CONT', 'DISEASE_INDEX', 'DTH_CONT',
        'MAT_CONT', 'MAT_CONT2', 'MF_HARV_CONT', 'PANNO_CONT', 'PNL_CONT',
        'TOTAL_HILL_CONT', 'TOTAL_PLANT_TESTED', 'TPLYLD_CONT_PS', 'YLD_0_CONT1',
        'YLD_CONT2', 'YLD_CONT_TON', 'YLD_CONT_TON2', 'YLD_CONT_TON3', 'YLD_CONT_TON5',
        'YLD_CONT_TON6', 'YLD_CONT_TON7'
    )
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     is_computed = FALSE
--rollback WHERE
--rollback     abbrev IN (
--rollback         'ADJAYLD_KG_CONT', 'ADJAYLD_KG_CONT3', 'ADJAYLD_KG_CONT4', 'ADJAYLD_KG_CONT5',
--rollback         'ADJYLD3_CONT', 'AREA_FACT', 'CML_CONT', 'DISEASE_INDEX', 'DTH_CONT',
--rollback         'MAT_CONT', 'MAT_CONT2', 'MF_HARV_CONT', 'PANNO_CONT', 'PNL_CONT',
--rollback         'TOTAL_HILL_CONT', 'TOTAL_PLANT_TESTED', 'TPLYLD_CONT_PS', 'YLD_0_CONT1',
--rollback         'YLD_CONT2', 'YLD_CONT_TON', 'YLD_CONT_TON2', 'YLD_CONT_TON3', 'YLD_CONT_TON5',
--rollback         'YLD_CONT_TON6', 'YLD_CONT_TON7'
--rollback     )
--rollback ;
