--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_experiment.experiment_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.experiment



-- populate wheat experiment CBBW-5
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'HB') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2017 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'B') AS season_id,
    '2017B' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'CBBW-5' AS experiment_name,
    'Intentional Crossing Nursery' AS experiment_type,
    'Breeding Crosses' AS experiment_sub_type,
    'Crossing Block' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'entry list created; crosses created; design generated; occurrences created' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'CBBW-5'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_list_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_list



-- populate wheat entry list CBBW-5_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'CBBW-5_ENTLIST' AS entry_list_name,
    'draft' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'CBBW-5') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'jennifer.allan') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry



-- populate wheat entries for CBBW-5
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    'female-and-male' AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS12Y00433S-099Y-099M-099NJ-099NJ-3RGY-0B','BV2017CBBW247'),
        (2,'CMSS12Y00450S-099Y-099M-0SY-42M-0RGY','BV2017PPTBW196'),
        (3,'CMSS12Y00849T-099TOPM-099Y-099M-099NJ-099NJ-8RGY-0B','BV2017CBBW248'),
        (4,'CMSS12Y00962T-099TOPM-099Y-099M-099NJ-099NJ-15RGY-0B','BV2017CBBW250'),
        (5,'CMSS12Y00983T-099TOPM-099Y-099M-0SY-25M-0RGY','BV2017CBBW251'),
        (6,'CMSS12Y01031T-099TOPM-099Y-099M-099NJ-099NJ-19RGY-0B','BV2017CBBW252'),
        (7,'CMSS12Y01178T-099TOPM-099Y-099M-0SY-1M-0RGY','BV2017CBBW253'),
        (8,'CMSS12B00123S-099M-099NJ-099NJ-19RGY-0B','BV2017CBBW254'),
        (9,'CMSS12B00375S-099M-0SY-13M-0RGY','BV2017CBBW256'),
        (10,'CMSS12B00635T-099TOPY-099M-0SY-10M-0RGY','BV2017CBBW257'),
        (11,'CMSS12B00977T-099TOPY-099M-099NJ-099NJ-10RGY-0B','BV2017CBBW259'),
        (12,'CMSS12B00996T-099TOPY-099M-099NJ-099NJ-4RGY-0B','MXI16-17M29HRWSN1121'),
        (13,'CMSS12B01005T-099TOPY-099M-099NJ-099NJ-8RGY-0B','BV2017CBBW262'),
        (14,'CMSS12Y00771T-099TOPM-099Y-099M-099NJ-099NJ-25Y-0WGY','BV2017CBBW297'),
        (15,'CMSS12Y00773T-099TOPM-099Y-099M-099NJ-099NJ-35Y-0WGY','BV2017CBBW298'),
        (16,'CMSS12Y00929T-099TOPM-099Y-099M-099NJ-099NJ-22Y-0WGY','BV2017CBBW301'),
        (17,'CMSS12Y01033T-099TOPM-099Y-099M-099NJ-099NJ-16Y-0WGY','BV2017CBBW305'),
        (18,'CMSS12Y01042T-099TOPM-099Y-099M-099NJ-099NJ-27Y-0WGY','BV2017CBBW307'),
        (19,'CMSS12Y01108T-099TOPM-099Y-099M-099NJ-099NJ-4Y-0WGY','BV2017CBBW309'),
        (20,'CMSS12Y01118T-099TOPM-099Y-099M-099NJ-099NJ-20Y-0WGY','BV2017CBBW311'),
        (21,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-7Y-0WGY','BV2017C52IB37SA30H299'),
        (22,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-19Y-0WGY','BV2017CBBW313'),
        (23,'CMSS12B00147S-099M-099NJ-099NJ-18Y-0WGY','BV2017PPTBW224'),
        (24,'CMSS12B00314S-099M-099NJ-099NJ-6Y-0WGY','BV2017CBBW314'),
        (25,'CMSS12B00411S-099M-099NJ-099NJ-14Y-0WGY','BV2017PPTBW225'),
        (26,'CMSS12B00456S-099M-099NJ-099NJ-16Y-0WGY','BV2017CBBW315'),
        (27,'CMSS12B00626T-099TOPY-099M-099NJ-099NJ-12Y-0WGY','BV2017CBBW317'),
        (28,'CMSS12B00627T-099TOPY-099M-099NJ-099NJ-33Y-0WGY','BV2017PPTBW227'),
        (29,'CMSS12B00630T-099TOPY-099M-099NJ-099NJ-32Y-0WGY','BV2017CBBW322'),
        (30,'CMSS12B00711T-099TOPY-099M-099NJ-099NJ-24Y-0WGY','BV2017CBBW328'),
        (31,'CMSS12B00825T-099TOPY-099M-099NJ-099NJ-7Y-0WGY','BV2017CBBW329'),
        (32,'CMSS13Y00048S-099Y-099M-0SY-24M-0WGY','BV2017CBBW335'),
        (33,'CMSS13Y00066S-099Y-099M-0SY-14M-0WGY','BV2017CBBW336'),
        (34,'CMSS13Y00128S-099Y-099M-0SY-13M-0WGY','BV2017CBBW337'),
        (35,'CMSS13Y00140S-099Y-099M-0SY-7M-0WGY','BV2017CBBW338'),
        (36,'CMSS13Y01104T-099TOPM-099Y-099M-0SY-28M-0WGY','BV2017CBBW347'),
        (37,'CMSS13Y01156T-099TOPM-099Y-099M-0SY-6M-0WGY','BV2017CBBW351'),
        (38,'CMSS13Y01263T-099TOPM-099Y-099M-0SY-21M-0WGY','BV2017CBBW354'),
        (39,'CMSS13Y01427T-099TOPM-099Y-099M-0SY-8M-0WGY','BV2017CBBW364'),
        (40,'CMSS13B00102S-099M-0SY-25M-0WGY','BV2017CBBW368'),
        (41,'CMSS13B00142S-099M-0SY-25M-0WGY','BV2017CBBW370'),
        (42,'CMSS13B00145S-099M-0SY-8M-0WGY','BV2017CBBW372'),
        (43,'CMSS13B00292S-099M-0SY-9M-0WGY','BV2017CBBW373'),
        (44,'CMSS13B00401S-099M-0SY-38M-0WGY','BV2017CBBW375'),
        (45,'CMSS13B00631S-099M-0SY-6M-0WGY','BV2017CBBW378'),
        (46,'CMSS13B00709S-099M-0SY-15M-0WGY','BV2017CBBW382'),
        (47,'CMSS13B00724S-099M-0SY-19M-0WGY','BV2017CBBW383'),
        (48,'CMSS13B00763S-099M-0SY-24M-0WGY','BV2017CBBW385'),
        (49,'CMSS13B01126T-099TOPY-099M-0SY-4M-0WGY','BV2017CBBW390'),
        (50,'CMSS13B01177T-099TOPY-099M-0SY-20M-0WGY','BV2017CBBW392'),
        (51,'CMSS13B01217T-099TOPY-099M-0SY-18M-0WGY','BV2017CBBW393'),
        (52,'CMSS13B01218T-099TOPY-099M-0SY-6M-0WGY','BV2017CBBW394'),
        (53,'CMSS13B01220T-099TOPY-099M-0SY-4M-0WGY','BV2017CBBW395'),
        (54,'CMSS13B01370T-099TOPY-099M-0SY-13M-0WGY','BV2017CBBW399'),
        (55,'CMSS13B01408T-099TOPY-099M-0SY-35M-0WGY','BV2017CBBW403'),
        (56,'CMSS13B01568T-099TOPY-099M-0SY-6M-0WGY','BV2017CBBW406'),
        (57,'CMSS13B01578T-099TOPY-099M-0SY-22M-0WGY','BV2017CBBW409'),
        (58,'CMSS13B01757T-099TOPY-099M-0SY-14M-0WGY','BV2017CBBW412'),
        (59,'CMSS13B01783T-099TOPY-099M-0SY-25M-0WGY','BV2017CBBW413'),
        (60,'-6AUS','BV2017CBBW268'),
        (61,'CMSS07Y00195S-0B-099Y-099M-099Y-16M-0WGY','BV201334ESWYT119'),
        (62,'CMSS06Y00582T-099TOPM-099Y-099ZTM-099Y-099M-10WGY-0B','RIN201233ESWYT106'),
        (63,'CGSS02B00105T-099B-099Y-099M-099Y-099M-1WGY-0B','RIN20094STEMRRSN6035'),
        (64,'CMSS08B00737T-099TOPY-099M-099Y-3M-0WGY','MXI13-14M35ES22SAWHT134'),
        (65,'CMSS08B00764T-099TOPY-099M-099NJ-27WGY-0B','MXI13-14M35ES22SAWHT136'),
        (66,'CMSS08B00771T-099TOPY-099M-099NJ-7WGY-0B','MXI13-14M35ES22SAWHT137')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'CBBW-5_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_data_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_data



-- populate wheat entry data for CBBW-5
INSERT INTO
    experiment.entry_data (
        entry_id, variable_id, data_value, data_qc_code, creator_id
    )
SELECT
    ent.id AS entry_id,
    var.id AS variable_id,
    'Line' AS data_value,
    'N' AS data_qc_code,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PARENT_TYPE'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    entlist.entry_list_name = 'CBBW-5_ENTLIST'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_data AS entdata
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON entlist.id = ent.entry_list_id
--rollback WHERE
--rollback     entdata.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.occurrence_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.occurrence



-- populate wheat occurrence for CBBW-5
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'CBBW-5_OCC1' AS occurrence_name,
    'draft' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    3 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'CBBW-5'
    AND geo.geospatial_object_code = 'CO'
    AND person.username = 'jennifer.allan'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'CBBW-5_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_place.geospatial_object_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in place.geospatial_object



-- populate wheat planting area for CBBW-5
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'CBBW-5_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'CBBW-5_LOC1'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.location_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location



-- populate wheat location for CBBW-5
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'CBBW-5_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'jennifer.allan'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'CBBW-5_LOC1'
    AND site.geospatial_object_code = 'CO'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'CBBW-5_LOC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.plot_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.plot



-- populate wheat plots for CBBW-5
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS12Y00433S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (2,1,2,'CMSS12Y00450S-099Y-099M-0SY-42M-0RGY'),
        (3,1,3,'CMSS12Y00849T-099TOPM-099Y-099M-099NJ-099NJ-8RGY-0B'),
        (4,1,4,'CMSS12Y00962T-099TOPM-099Y-099M-099NJ-099NJ-15RGY-0B'),
        (5,1,5,'CMSS12Y00983T-099TOPM-099Y-099M-0SY-25M-0RGY'),
        (6,1,6,'CMSS12Y01031T-099TOPM-099Y-099M-099NJ-099NJ-19RGY-0B'),
        (7,1,7,'CMSS12Y01178T-099TOPM-099Y-099M-0SY-1M-0RGY'),
        (8,1,8,'CMSS12B00123S-099M-099NJ-099NJ-19RGY-0B'),
        (9,1,9,'CMSS12B00375S-099M-0SY-13M-0RGY'),
        (10,1,10,'CMSS12B00635T-099TOPY-099M-0SY-10M-0RGY'),
        (11,1,11,'CMSS12B00977T-099TOPY-099M-099NJ-099NJ-10RGY-0B'),
        (12,1,12,'CMSS12B00996T-099TOPY-099M-099NJ-099NJ-4RGY-0B'),
        (13,1,13,'CMSS12B01005T-099TOPY-099M-099NJ-099NJ-8RGY-0B'),
        (14,1,14,'CMSS12Y00771T-099TOPM-099Y-099M-099NJ-099NJ-25Y-0WGY'),
        (15,1,15,'CMSS12Y00773T-099TOPM-099Y-099M-099NJ-099NJ-35Y-0WGY'),
        (16,1,16,'CMSS12Y00929T-099TOPM-099Y-099M-099NJ-099NJ-22Y-0WGY'),
        (17,1,17,'CMSS12Y01033T-099TOPM-099Y-099M-099NJ-099NJ-16Y-0WGY'),
        (18,1,18,'CMSS12Y01042T-099TOPM-099Y-099M-099NJ-099NJ-27Y-0WGY'),
        (19,1,19,'CMSS12Y01108T-099TOPM-099Y-099M-099NJ-099NJ-4Y-0WGY'),
        (20,1,20,'CMSS12Y01118T-099TOPM-099Y-099M-099NJ-099NJ-20Y-0WGY'),
        (21,1,21,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-7Y-0WGY'),
        (22,1,22,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-19Y-0WGY'),
        (23,1,23,'CMSS12B00147S-099M-099NJ-099NJ-18Y-0WGY'),
        (24,1,24,'CMSS12B00314S-099M-099NJ-099NJ-6Y-0WGY'),
        (25,1,25,'CMSS12B00411S-099M-099NJ-099NJ-14Y-0WGY'),
        (26,1,26,'CMSS12B00456S-099M-099NJ-099NJ-16Y-0WGY'),
        (27,1,27,'CMSS12B00626T-099TOPY-099M-099NJ-099NJ-12Y-0WGY'),
        (28,1,28,'CMSS12B00627T-099TOPY-099M-099NJ-099NJ-33Y-0WGY'),
        (29,1,29,'CMSS12B00630T-099TOPY-099M-099NJ-099NJ-32Y-0WGY'),
        (30,1,30,'CMSS12B00711T-099TOPY-099M-099NJ-099NJ-24Y-0WGY'),
        (31,1,31,'CMSS12B00825T-099TOPY-099M-099NJ-099NJ-7Y-0WGY'),
        (32,1,32,'CMSS13Y00048S-099Y-099M-0SY-24M-0WGY'),
        (33,1,33,'CMSS13Y00066S-099Y-099M-0SY-14M-0WGY'),
        (34,1,34,'CMSS13Y00128S-099Y-099M-0SY-13M-0WGY'),
        (35,1,35,'CMSS13Y00140S-099Y-099M-0SY-7M-0WGY'),
        (36,1,36,'CMSS13Y01104T-099TOPM-099Y-099M-0SY-28M-0WGY'),
        (37,1,37,'CMSS13Y01156T-099TOPM-099Y-099M-0SY-6M-0WGY'),
        (38,1,38,'CMSS13Y01263T-099TOPM-099Y-099M-0SY-21M-0WGY'),
        (39,1,39,'CMSS13Y01427T-099TOPM-099Y-099M-0SY-8M-0WGY'),
        (40,1,40,'CMSS13B00102S-099M-0SY-25M-0WGY'),
        (41,1,41,'CMSS13B00142S-099M-0SY-25M-0WGY'),
        (42,1,42,'CMSS13B00145S-099M-0SY-8M-0WGY'),
        (43,1,43,'CMSS13B00292S-099M-0SY-9M-0WGY'),
        (44,1,44,'CMSS13B00401S-099M-0SY-38M-0WGY'),
        (45,1,45,'CMSS13B00631S-099M-0SY-6M-0WGY'),
        (46,1,46,'CMSS13B00709S-099M-0SY-15M-0WGY'),
        (47,1,47,'CMSS13B00724S-099M-0SY-19M-0WGY'),
        (48,1,48,'CMSS13B00763S-099M-0SY-24M-0WGY'),
        (49,1,49,'CMSS13B01126T-099TOPY-099M-0SY-4M-0WGY'),
        (50,1,50,'CMSS13B01177T-099TOPY-099M-0SY-20M-0WGY'),
        (51,1,51,'CMSS13B01217T-099TOPY-099M-0SY-18M-0WGY'),
        (52,1,52,'CMSS13B01218T-099TOPY-099M-0SY-6M-0WGY'),
        (53,1,53,'CMSS13B01220T-099TOPY-099M-0SY-4M-0WGY'),
        (54,1,54,'CMSS13B01370T-099TOPY-099M-0SY-13M-0WGY'),
        (55,1,55,'CMSS13B01408T-099TOPY-099M-0SY-35M-0WGY'),
        (56,1,56,'CMSS13B01568T-099TOPY-099M-0SY-6M-0WGY'),
        (57,1,57,'CMSS13B01578T-099TOPY-099M-0SY-22M-0WGY'),
        (58,1,58,'CMSS13B01757T-099TOPY-099M-0SY-14M-0WGY'),
        (59,1,59,'CMSS13B01783T-099TOPY-099M-0SY-25M-0WGY'),
        (60,1,60,'-6AUS'),
        (61,1,61,'CMSS07Y00195S-0B-099Y-099M-099Y-16M-0WGY'),
        (62,1,62,'CMSS06Y00582T-099TOPM-099Y-099ZTM-099Y-099M-10WGY-0B'),
        (63,1,63,'CGSS02B00105T-099B-099Y-099M-099Y-099M-1WGY-0B'),
        (64,1,64,'CMSS08B00737T-099TOPY-099M-099Y-3M-0WGY'),
        (65,1,65,'CMSS08B00764T-099TOPY-099M-099NJ-27WGY-0B'),
        (66,1,66,'CMSS08B00771T-099TOPY-099M-099NJ-7WGY-0B'),
        (67,2,1,'CMSS12Y00433S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (68,2,2,'CMSS12Y00450S-099Y-099M-0SY-42M-0RGY'),
        (69,2,3,'CMSS12Y00849T-099TOPM-099Y-099M-099NJ-099NJ-8RGY-0B'),
        (70,2,4,'CMSS12Y00962T-099TOPM-099Y-099M-099NJ-099NJ-15RGY-0B'),
        (71,2,5,'CMSS12Y00983T-099TOPM-099Y-099M-0SY-25M-0RGY'),
        (72,2,6,'CMSS12Y01031T-099TOPM-099Y-099M-099NJ-099NJ-19RGY-0B'),
        (73,2,7,'CMSS12Y01178T-099TOPM-099Y-099M-0SY-1M-0RGY'),
        (74,2,8,'CMSS12B00123S-099M-099NJ-099NJ-19RGY-0B'),
        (75,2,9,'CMSS12B00375S-099M-0SY-13M-0RGY'),
        (76,2,10,'CMSS12B00635T-099TOPY-099M-0SY-10M-0RGY'),
        (77,2,11,'CMSS12B00977T-099TOPY-099M-099NJ-099NJ-10RGY-0B'),
        (78,2,12,'CMSS12B00996T-099TOPY-099M-099NJ-099NJ-4RGY-0B'),
        (79,2,13,'CMSS12B01005T-099TOPY-099M-099NJ-099NJ-8RGY-0B'),
        (80,2,14,'CMSS12Y00771T-099TOPM-099Y-099M-099NJ-099NJ-25Y-0WGY'),
        (81,2,15,'CMSS12Y00773T-099TOPM-099Y-099M-099NJ-099NJ-35Y-0WGY'),
        (82,2,16,'CMSS12Y00929T-099TOPM-099Y-099M-099NJ-099NJ-22Y-0WGY'),
        (83,2,17,'CMSS12Y01033T-099TOPM-099Y-099M-099NJ-099NJ-16Y-0WGY'),
        (84,2,18,'CMSS12Y01042T-099TOPM-099Y-099M-099NJ-099NJ-27Y-0WGY'),
        (85,2,19,'CMSS12Y01108T-099TOPM-099Y-099M-099NJ-099NJ-4Y-0WGY'),
        (86,2,20,'CMSS12Y01118T-099TOPM-099Y-099M-099NJ-099NJ-20Y-0WGY'),
        (87,2,21,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-7Y-0WGY'),
        (88,2,22,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-19Y-0WGY'),
        (89,2,23,'CMSS12B00147S-099M-099NJ-099NJ-18Y-0WGY'),
        (90,2,24,'CMSS12B00314S-099M-099NJ-099NJ-6Y-0WGY'),
        (91,2,25,'CMSS12B00411S-099M-099NJ-099NJ-14Y-0WGY'),
        (92,2,26,'CMSS12B00456S-099M-099NJ-099NJ-16Y-0WGY'),
        (93,2,27,'CMSS12B00626T-099TOPY-099M-099NJ-099NJ-12Y-0WGY'),
        (94,2,28,'CMSS12B00627T-099TOPY-099M-099NJ-099NJ-33Y-0WGY'),
        (95,2,29,'CMSS12B00630T-099TOPY-099M-099NJ-099NJ-32Y-0WGY'),
        (96,2,30,'CMSS12B00711T-099TOPY-099M-099NJ-099NJ-24Y-0WGY'),
        (97,2,31,'CMSS12B00825T-099TOPY-099M-099NJ-099NJ-7Y-0WGY'),
        (98,2,32,'CMSS13Y00048S-099Y-099M-0SY-24M-0WGY'),
        (99,2,33,'CMSS13Y00066S-099Y-099M-0SY-14M-0WGY'),
        (100,2,34,'CMSS13Y00128S-099Y-099M-0SY-13M-0WGY'),
        (101,2,35,'CMSS13Y00140S-099Y-099M-0SY-7M-0WGY'),
        (102,2,36,'CMSS13Y01104T-099TOPM-099Y-099M-0SY-28M-0WGY'),
        (103,2,37,'CMSS13Y01156T-099TOPM-099Y-099M-0SY-6M-0WGY'),
        (104,2,38,'CMSS13Y01263T-099TOPM-099Y-099M-0SY-21M-0WGY'),
        (105,2,39,'CMSS13Y01427T-099TOPM-099Y-099M-0SY-8M-0WGY'),
        (106,2,40,'CMSS13B00102S-099M-0SY-25M-0WGY'),
        (107,2,41,'CMSS13B00142S-099M-0SY-25M-0WGY'),
        (108,2,42,'CMSS13B00145S-099M-0SY-8M-0WGY'),
        (109,2,43,'CMSS13B00292S-099M-0SY-9M-0WGY'),
        (110,2,44,'CMSS13B00401S-099M-0SY-38M-0WGY'),
        (111,2,45,'CMSS13B00631S-099M-0SY-6M-0WGY'),
        (112,2,46,'CMSS13B00709S-099M-0SY-15M-0WGY'),
        (113,2,47,'CMSS13B00724S-099M-0SY-19M-0WGY'),
        (114,2,48,'CMSS13B00763S-099M-0SY-24M-0WGY'),
        (115,2,49,'CMSS13B01126T-099TOPY-099M-0SY-4M-0WGY'),
        (116,2,50,'CMSS13B01177T-099TOPY-099M-0SY-20M-0WGY'),
        (117,2,51,'CMSS13B01217T-099TOPY-099M-0SY-18M-0WGY'),
        (118,2,52,'CMSS13B01218T-099TOPY-099M-0SY-6M-0WGY'),
        (119,2,53,'CMSS13B01220T-099TOPY-099M-0SY-4M-0WGY'),
        (120,2,54,'CMSS13B01370T-099TOPY-099M-0SY-13M-0WGY'),
        (121,2,55,'CMSS13B01408T-099TOPY-099M-0SY-35M-0WGY'),
        (122,2,56,'CMSS13B01568T-099TOPY-099M-0SY-6M-0WGY'),
        (123,2,57,'CMSS13B01578T-099TOPY-099M-0SY-22M-0WGY'),
        (124,2,58,'CMSS13B01757T-099TOPY-099M-0SY-14M-0WGY'),
        (125,2,59,'CMSS13B01783T-099TOPY-099M-0SY-25M-0WGY'),
        (126,2,60,'-6AUS'),
        (127,2,61,'CMSS07Y00195S-0B-099Y-099M-099Y-16M-0WGY'),
        (128,2,62,'CMSS06Y00582T-099TOPM-099Y-099ZTM-099Y-099M-10WGY-0B'),
        (129,2,63,'CGSS02B00105T-099B-099Y-099M-099Y-099M-1WGY-0B'),
        (130,2,64,'CMSS08B00737T-099TOPY-099M-099Y-3M-0WGY'),
        (131,2,65,'CMSS08B00764T-099TOPY-099M-099NJ-27WGY-0B'),
        (132,2,66,'CMSS08B00771T-099TOPY-099M-099NJ-7WGY-0B'),
        (133,3,1,'CMSS12Y00433S-099Y-099M-099NJ-099NJ-3RGY-0B'),
        (134,3,2,'CMSS12Y00450S-099Y-099M-0SY-42M-0RGY'),
        (135,3,3,'CMSS12Y00849T-099TOPM-099Y-099M-099NJ-099NJ-8RGY-0B'),
        (136,3,4,'CMSS12Y00962T-099TOPM-099Y-099M-099NJ-099NJ-15RGY-0B'),
        (137,3,5,'CMSS12Y00983T-099TOPM-099Y-099M-0SY-25M-0RGY'),
        (138,3,6,'CMSS12Y01031T-099TOPM-099Y-099M-099NJ-099NJ-19RGY-0B'),
        (139,3,7,'CMSS12Y01178T-099TOPM-099Y-099M-0SY-1M-0RGY'),
        (140,3,8,'CMSS12B00123S-099M-099NJ-099NJ-19RGY-0B'),
        (141,3,9,'CMSS12B00375S-099M-0SY-13M-0RGY'),
        (142,3,10,'CMSS12B00635T-099TOPY-099M-0SY-10M-0RGY'),
        (143,3,11,'CMSS12B00977T-099TOPY-099M-099NJ-099NJ-10RGY-0B'),
        (144,3,12,'CMSS12B00996T-099TOPY-099M-099NJ-099NJ-4RGY-0B'),
        (145,3,13,'CMSS12B01005T-099TOPY-099M-099NJ-099NJ-8RGY-0B'),
        (146,3,14,'CMSS12Y00771T-099TOPM-099Y-099M-099NJ-099NJ-25Y-0WGY'),
        (147,3,15,'CMSS12Y00773T-099TOPM-099Y-099M-099NJ-099NJ-35Y-0WGY'),
        (148,3,16,'CMSS12Y00929T-099TOPM-099Y-099M-099NJ-099NJ-22Y-0WGY'),
        (149,3,17,'CMSS12Y01033T-099TOPM-099Y-099M-099NJ-099NJ-16Y-0WGY'),
        (150,3,18,'CMSS12Y01042T-099TOPM-099Y-099M-099NJ-099NJ-27Y-0WGY'),
        (151,3,19,'CMSS12Y01108T-099TOPM-099Y-099M-099NJ-099NJ-4Y-0WGY'),
        (152,3,20,'CMSS12Y01118T-099TOPM-099Y-099M-099NJ-099NJ-20Y-0WGY'),
        (153,3,21,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-7Y-0WGY'),
        (154,3,22,'CMSS12Y01130T-099TOPM-099Y-099M-099NJ-099NJ-19Y-0WGY'),
        (155,3,23,'CMSS12B00147S-099M-099NJ-099NJ-18Y-0WGY'),
        (156,3,24,'CMSS12B00314S-099M-099NJ-099NJ-6Y-0WGY'),
        (157,3,25,'CMSS12B00411S-099M-099NJ-099NJ-14Y-0WGY'),
        (158,3,26,'CMSS12B00456S-099M-099NJ-099NJ-16Y-0WGY'),
        (159,3,27,'CMSS12B00626T-099TOPY-099M-099NJ-099NJ-12Y-0WGY'),
        (160,3,28,'CMSS12B00627T-099TOPY-099M-099NJ-099NJ-33Y-0WGY'),
        (161,3,29,'CMSS12B00630T-099TOPY-099M-099NJ-099NJ-32Y-0WGY'),
        (162,3,30,'CMSS12B00711T-099TOPY-099M-099NJ-099NJ-24Y-0WGY'),
        (163,3,31,'CMSS12B00825T-099TOPY-099M-099NJ-099NJ-7Y-0WGY'),
        (164,3,32,'CMSS13Y00048S-099Y-099M-0SY-24M-0WGY'),
        (165,3,33,'CMSS13Y00066S-099Y-099M-0SY-14M-0WGY'),
        (166,3,34,'CMSS13Y00128S-099Y-099M-0SY-13M-0WGY'),
        (167,3,35,'CMSS13Y00140S-099Y-099M-0SY-7M-0WGY'),
        (168,3,36,'CMSS13Y01104T-099TOPM-099Y-099M-0SY-28M-0WGY'),
        (169,3,37,'CMSS13Y01156T-099TOPM-099Y-099M-0SY-6M-0WGY'),
        (170,3,38,'CMSS13Y01263T-099TOPM-099Y-099M-0SY-21M-0WGY'),
        (171,3,39,'CMSS13Y01427T-099TOPM-099Y-099M-0SY-8M-0WGY'),
        (172,3,40,'CMSS13B00102S-099M-0SY-25M-0WGY'),
        (173,3,41,'CMSS13B00142S-099M-0SY-25M-0WGY'),
        (174,3,42,'CMSS13B00145S-099M-0SY-8M-0WGY'),
        (175,3,43,'CMSS13B00292S-099M-0SY-9M-0WGY'),
        (176,3,44,'CMSS13B00401S-099M-0SY-38M-0WGY'),
        (177,3,45,'CMSS13B00631S-099M-0SY-6M-0WGY'),
        (178,3,46,'CMSS13B00709S-099M-0SY-15M-0WGY'),
        (179,3,47,'CMSS13B00724S-099M-0SY-19M-0WGY'),
        (180,3,48,'CMSS13B00763S-099M-0SY-24M-0WGY'),
        (181,3,49,'CMSS13B01126T-099TOPY-099M-0SY-4M-0WGY'),
        (182,3,50,'CMSS13B01177T-099TOPY-099M-0SY-20M-0WGY'),
        (183,3,51,'CMSS13B01217T-099TOPY-099M-0SY-18M-0WGY'),
        (184,3,52,'CMSS13B01218T-099TOPY-099M-0SY-6M-0WGY'),
        (185,3,53,'CMSS13B01220T-099TOPY-099M-0SY-4M-0WGY'),
        (186,3,54,'CMSS13B01370T-099TOPY-099M-0SY-13M-0WGY'),
        (187,3,55,'CMSS13B01408T-099TOPY-099M-0SY-35M-0WGY'),
        (188,3,56,'CMSS13B01568T-099TOPY-099M-0SY-6M-0WGY'),
        (189,3,57,'CMSS13B01578T-099TOPY-099M-0SY-22M-0WGY'),
        (190,3,58,'CMSS13B01757T-099TOPY-099M-0SY-14M-0WGY'),
        (191,3,59,'CMSS13B01783T-099TOPY-099M-0SY-25M-0WGY'),
        (192,3,60,'-6AUS'),
        (193,3,61,'CMSS07Y00195S-0B-099Y-099M-099Y-16M-0WGY'),
        (194,3,62,'CMSS06Y00582T-099TOPM-099Y-099ZTM-099Y-099M-10WGY-0B'),
        (195,3,63,'CGSS02B00105T-099B-099Y-099M-099Y-099M-1WGY-0B'),
        (196,3,64,'CMSS08B00737T-099TOPY-099M-099Y-3M-0WGY'),
        (197,3,65,'CMSS08B00764T-099TOPY-099M-099NJ-27WGY-0B'),
        (198,3,66,'CMSS08B00771T-099TOPY-099M-099NJ-7WGY-0B')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'CBBW-5_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'CBBW-5_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-5_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.planting_instruction_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.planting_instruction



-- populate wheat planting instructions for CBBW-5
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    entlist.entry_list_name = 'CBBW-5_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.package_log_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.package_log



-- populate wheat package logs for CBBW-5
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    entlist.entry_list_name = 'CBBW-5_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_package_log_id_in_experiment.planting_instruction_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block package_log_id in experiment.planting_instruction



-- populate wheat package log references for CBBW-5
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross_part_5 context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross



-- populate wheat crosses for CBBW-5
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'simple cross' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'jennifer.allan'
WHERE
    expt.experiment_name = 'CBBW-5'
    AND entf.entry_number BETWEEN 1 AND 33
    AND entm.entry_number BETWEEN 34 AND 66
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-5'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross_parent_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross_parent



-- populate wheat cross parents for CBBW-5
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'jennifer.allan'
        WHERE
            expt.experiment_name = 'CBBW-5'
            AND ent.entry_number BETWEEN 1 AND 33
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'jennifer.allan'
            WHERE
                expt.experiment_name = 'CBBW-5'
                AND ent.entry_number BETWEEN 34 AND 66
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'CBBW-5'
--rollback ;



--changeset postgres:update_experiment_statuses_for_wheat_crossing_block_experiment_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update experiment statuses for wheat crossing block experiment



-- update experiment statuses for CBBW-5
UPDATE
    experiment.experiment
SET
    experiment_status = 'planted'
WHERE
    experiment_name = 'CBBW-5'
;

UPDATE
    experiment.entry_list
SET
    entry_list_status = 'created'
WHERE
    entry_list_name = 'CBBW-5_ENTLIST'
;

UPDATE
    experiment.occurrence
SET
    occurrence_status = 'planted'
WHERE
    occurrence_name = 'CBBW-5_OCC1'
;

UPDATE
    experiment.location
SET
    location_status = 'committed'
WHERE
    location_name = 'CBBW-5_LOC1'
;


-- revert changes
--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'entry list created; crosses created; design generated; occurrences created'
--rollback WHERE
--rollback     experiment_name = 'CBBW-5'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_status = 'draft'
--rollback WHERE
--rollback     entry_list_name = 'CBBW-5_ENTLIST'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_status = 'draft'
--rollback WHERE
--rollback     occurrence_name = 'CBBW-5_OCC1'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.location
--rollback SET
--rollback     location_status = 'committed'
--rollback WHERE
--rollback     location_name = 'CBBW-5_LOC1'
--rollback ;



--changeset postgres:populate_pa_coordinates_for_wheat_crossing_block_experiment_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate planting area coordinates for wheat crossing block experiment



-- populate pa coordinates for CBBW-5
UPDATE
    experiment.plot
SET
    pa_x = design_x,
    pa_y = design_y
FROM
    experiment.occurrence AS occ
WHERE
    plot.occurrence_id = occ.id
    AND occ.occurrence_name = 'CBBW-5_OCC1'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     pa_x = NULL,
--rollback     pa_y = NULL
--rollback FROM
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-5_OCC1'
--rollback ;



--changeset postgres:update_cross_method_in_germplasm.cross_for_wheat_crossing_block_experiment_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update cross_method in germplasm.cross for wheat crossing block experiment



-- update cross methods for CBBW-5
UPDATE
    germplasm.cross AS crs
SET
    cross_method = 'SIMPLE CROSS'
FROM
    experiment.experiment AS expt
WHERE
    crs.experiment_id = expt.id
    AND expt.experiment_name = 'CBBW-5'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.cross AS crs
--rollback SET
--rollback     cross_method = 'simple cross'
--rollback FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-5'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.location_occurrence_group_part_5 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location_occurrence_group



-- populate wheat location occurrence groups for CBBW-5
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'CBBW-5_LOC1'
    AND occ.occurrence_name = 'CBBW-5_OCC1'
    AND prs.username = 'jennifer.allan'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'CBBW-5_LOC1'
--rollback     AND occ.occurrence_name = 'CBBW-5_OCC1'
--rollback ;
