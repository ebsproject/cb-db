--liquibase formatted sql

--changeset postgres:populate_maize_crossing_block_in_experiment.experiment_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.experiment



-- populate maize experiment A-3
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
   (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'HB') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2019 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2019B' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'A-3' AS experiment_name,
    'Intentional Crossing Nursery' AS experiment_type,
    'Breeding Crosses' AS experiment_sub_type,
    'Crossing Block' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'phil.hudson') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'phil.hudson') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'A-3'
--rollback ;


--changeset postgres:populate_maize_crossing_block_in_experiment.entry_list_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry_list



-- populate maize entry list A-3_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'A-3_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'A-3') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'phil.hudson') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'A-3_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.entry_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry



-- populate maize entries for A-3
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number::integer,
    t.designation AS entry_name,
    'entry' AS entry_type,
    'female-and-male' AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        ('1','ABDHL0221','WE-KIB-17A-48-4'),
        ('2','ABDHL120312','ST-KIB-18B-34-12'),
        ('3','ABDHL120918','ST-KIB-18B-34-5'),
        ('4','ABLMARSI0022','ST-KIB-18B-34-13'),
        ('5','ABLTI0137','WE-KIB-17A-48-11'),
        ('6','ABLTI0330','WE-KIB-17A-47-50'),
        ('7','ABSBL10060','ST-KIB-18B-34-3'),
        ('8','CML464','ST-KIB-18B-34-2'),
        ('9','ABLMARSI0037','ST-KIB-18B-34-6'),
        ('10','CML543','ST-KIB-18B-34-1')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'A-3_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'phil.hudson'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'A-3_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.entry_data_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.entry_data



-- populate maize entry data for A-3
INSERT INTO
    experiment.entry_data (
        entry_id, variable_id, data_value, data_qc_code, creator_id
    )
SELECT
    ent.id AS entry_id,
    var.id AS variable_id,
    'Line' AS data_value,
    'N' AS data_qc_code,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PARENT_TYPE'
    INNER JOIN tenant.person AS person
        ON person.username = 'phil.hudson'
WHERE
    entlist.entry_list_name = 'A-3_ENTLIST'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_data AS entdata
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON entlist.id = ent.entry_list_id
--rollback WHERE
--rollback     entdata.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-3_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.occurrence_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.occurrence



-- populate maize occurrence for A-3
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'A-3_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    3 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'A-3'
    AND geo.geospatial_object_code = 'KI'
    AND person.username = 'phil.hudson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'A-3_OCC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_place.geospatial_object_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in place.geospatial_object



-- populate maize planting area for A-3
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'A-3_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'A-3_LOC1'
--rollback ;


--changeset postgres:populate_maize_crossing_block_in_experiment.location_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.location



-- populate maize location for A-3
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'A-3_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2019 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'phil.hudson'
    AND season.season_code = 'A'
    AND geo.geospatial_object_name = 'A-3_LOC1'
    AND site.geospatial_object_code = 'KI'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'A-3_LOC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.plot_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.plot



-- populate maize plots for A-3

INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number::integer AS plot_number,
    'plot' AS plot_type,
    t.rep::integer AS rep,
    1 AS design_x,
    t.plot_number::integer AS design_y,
    1 AS pa_x,
    t.plot_number::integer AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    tenant.person AS person,
    experiment.occurrence AS occ,
    experiment.location AS loc,
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN (
        VALUES
        ('1','1','1','ABDHL0221'),
        ('2','1','2','ABDHL120312'),
        ('3','1','3','ABDHL120918'),
        ('4','1','4','ABLMARSI0022'),
        ('5','1','5','ABLTI0137'),
        ('6','1','6','ABLTI0330'),
        ('7','1','7','ABSBL10060'),
        ('8','1','8','CML464'),
        ('9','1','9','ABLMARSI0037'),
        ('10','1','10','CML543')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
        ON t.entry_number::integer = ent.entry_number::integer
        AND t.designation = ent.entry_name
WHERE
    occ.occurrence_name = 'A-3_OCC1'
    AND loc.location_name = 'A-3_LOC1'
    AND entlist.entry_list_name = 'A-3_ENTLIST'
    AND person.username = 'phil.hudson'
ORDER BY
    t.plot_number
;


-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'A-3_OCC1'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.planting_instruction_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.planting_instruction



-- populate maize planting instructions for A-3
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'phil.hudson'
WHERE
    entlist.entry_list_name = 'A-3_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-3_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_germplasm.package_log_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.package_log



-- populate maize package logs for A-3
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'phil.hudson'
WHERE
    entlist.entry_list_name = 'A-3_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-3_ENTLIST'
--rollback ;



--changeset postgres:populate_maize_crossing_block_package_log_id_in_experiment.planting_instruction_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block package_log_id in experiment.planting_instruction



-- populate maize package log references for A-3
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'A-3_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'A-3_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_maize_crossing_block_in_germplasm.cross_part_3 context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.cross



-- populate maize crosses for A-3
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'SIMPLE CROSS' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'phil.hudson'
WHERE
    expt.experiment_name = 'A-3'
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'A-3'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_germplasm.cross_parent_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in germplasm.cross_parent



-- populate maize cross parents for A-3
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'phil.hudson'
        WHERE
            expt.experiment_name = 'A-3'
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'phil.hudson'
            WHERE
                expt.experiment_name = 'A-3'
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'A-3'
--rollback ;



--changeset postgres:populate_maize_crossing_block_in_experiment.location_occurrence_group_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Populate maize crossing block in experiment.location_occurrence_group



-- populate maize location occurrence groups for A-3
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'A-3_LOC1'
    AND occ.occurrence_name = 'A-3_OCC1'
    AND prs.username = 'phil.hudson'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'A-3_LOC1'
--rollback     AND occ.occurrence_name = 'A-3_OCC1'
--rollback ;



--changeset postgres:update_project_and_pipeline_of_a3_cb_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Update project and pipeline of A-3 CB experiment



UPDATE 
    experiment.experiment
SET
    project_id = (SELECT id FROM tenant.project WHERE project_code = 'KE_PROJECT'),
    planting_season = '2019A',
    pipeline_id = (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GMP_PIPELINE')
WHERE
    experiment_name = 'A-3'
;


-- revert changes
--rollback UPDATE 
--rollback     experiment.experiment
--rollback SET
--rollback     project_id = (SELECT id FROM tenant.project WHERE project_code = 'GWP_PIPELINE'),
--rollback     planting_season = '2019B',
--rollback     pipeline_id = (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'BW_PIPELINE')
--rollback WHERE
--rollback     experiment_name = 'A-3'
--rollback ;



--changeset postgres:update_crosses_from_a3_cb_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Update crosses from A-3 CB experiment



WITH t_crosses AS (
    SELECT
        crs.id AS cross_id,
        crs.cross_name
    FROM
        germplasm.cross AS crs
        INNER JOIN experiment.experiment AS expt
            ON crs.experiment_id = expt.id
    WHERE
        expt.experiment_name = 'A-3'
        AND crs.cross_name IN (
            'ABDHL0221/ABDHL0221',
            'ABDHL120312/ABDHL120312',
            'ABDHL120918/ABDHL120918',
            'ABLMARSI0022/ABLMARSI0022',
            'ABLTI0137/ABLTI0137',
            'ABLTI0330/ABLTI0330',
            'ABSBL10060/ABSBL10060',
            'CML464/CML464',
            'ABLMARSI0037/ABLMARSI0037',
            'CML543/CML543'
        )
)
DELETE FROM
    germplasm.cross_parent AS crspar
USING
    t_crosses AS t
WHERE
    crspar.cross_id = t.cross_id
;


WITH t_crosses AS (
    SELECT
        crs.id AS cross_id,
        crs.cross_name
    FROM
        germplasm.cross AS crs
        INNER JOIN experiment.experiment AS expt
            ON crs.experiment_id = expt.id
    WHERE
        expt.experiment_name = 'A-3'
        AND crs.cross_name IN (
            'ABDHL0221/ABDHL0221',
            'ABDHL120312/ABDHL120312',
            'ABDHL120918/ABDHL120918',
            'ABLMARSI0022/ABLMARSI0022',
            'ABLTI0137/ABLTI0137',
            'ABLTI0330/ABLTI0330',
            'ABSBL10060/ABSBL10060',
            'CML464/CML464',
            'ABLMARSI0037/ABLMARSI0037',
            'CML543/CML543'
        )
)
DELETE FROM
    germplasm.cross AS crs
USING
    t_crosses AS t
WHERE
    crs.id = t.cross_id
;



-- revert changes
--rollback INSERT INTO
--rollback    germplasm.cross (
--rollback        cross_name,
--rollback        cross_method,
--rollback        germplasm_id,
--rollback        experiment_id,
--rollback        creator_id
--rollback    )
--rollback SELECT
--rollback     (entf.entry_name || '/' || entm.entry_name) AS cross_name,
--rollback     'SIMPLE CROSS' AS cross_method,
--rollback     NULL AS germplasm_id,
--rollback     expt.id AS experiment_id,
--rollback     person.id AS creator_id
--rollback FROM
--rollback     experiment.experiment AS expt
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON expt.id = entlist.experiment_id
--rollback     INNER JOIN experiment.entry AS entf
--rollback         ON entf.entry_list_id = entlist.id
--rollback     INNER JOIN experiment.entry AS entm
--rollback         ON entm.entry_list_id = entlist.id
--rollback     INNER JOIN tenant.person AS person
--rollback         ON person.username = 'phil.hudson'
--rollback WHERE
--rollback     expt.experiment_name = 'A-3'
--rollback     AND entf.id = entm.id
--rollback ORDER BY
--rollback     entf.entry_number,
--rollback     entm.entry_number
--rollback ;
--rollback 
--rollback 
--rollback INSERT INTO
--rollback    germplasm.cross_parent (
--rollback        cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
--rollback    )
--rollback SELECT
--rollback     t.*
--rollback FROM (
--rollback         SELECT
--rollback             crs.id AS cross_id,
--rollback             ent.germplasm_id,
--rollback             ent.seed_id,
--rollback             'female' AS parent_role,
--rollback             1 AS order_number,
--rollback             expt.id AS experiment_id,
--rollback             ent.id AS entry_id,
--rollback             person.id AS creator_id
--rollback         FROM
--rollback             germplasm.cross AS crs
--rollback             INNER JOIN experiment.experiment AS expt
--rollback                 ON crs.experiment_id = expt.id
--rollback             INNER JOIN experiment.entry_list AS entlist
--rollback                 ON entlist.experiment_id = expt.id
--rollback             INNER JOIN experiment.entry AS ent
--rollback                 ON ent.entry_list_id = entlist.id
--rollback             INNER JOIN tenant.person AS person
--rollback                 ON person.username = 'phil.hudson'
--rollback         WHERE
--rollback             expt.experiment_name = 'A-3'
--rollback             AND crs.cross_name ILIKE ent.entry_name || '/%'
--rollback             AND crs.cross_name IN (
--rollback                 'ABDHL0221/ABDHL0221',
--rollback                 'ABDHL120312/ABDHL120312',
--rollback                 'ABDHL120918/ABDHL120918',
--rollback                 'ABLMARSI0022/ABLMARSI0022',
--rollback                 'ABLTI0137/ABLTI0137',
--rollback                 'ABLTI0330/ABLTI0330',
--rollback                 'ABSBL10060/ABSBL10060',
--rollback                 'CML464/CML464',
--rollback                 'ABLMARSI0037/ABLMARSI0037',
--rollback                 'CML543/CML543'
--rollback             )
--rollback         UNION ALL
--rollback             SELECT
--rollback                 crs.id AS cross_id,
--rollback                 ent.germplasm_id,
--rollback                 ent.seed_id,
--rollback                 'male' AS parent_role,
--rollback                 2 AS order_number,
--rollback                 expt.id AS experiment_id,
--rollback                 ent.id AS entry_id,
--rollback                 person.id AS creator_id
--rollback             FROM
--rollback                 germplasm.cross AS crs
--rollback                 INNER JOIN experiment.experiment AS expt
--rollback                     ON crs.experiment_id = expt.id
--rollback                 INNER JOIN experiment.entry_list AS entlist
--rollback                     ON entlist.experiment_id = expt.id
--rollback                 INNER JOIN experiment.entry AS ent
--rollback                     ON ent.entry_list_id = entlist.id
--rollback                 INNER JOIN tenant.person AS person
--rollback                     ON person.username = 'phil.hudson'
--rollback             WHERE
--rollback                 expt.experiment_name = 'A-3'
--rollback                 AND crs.cross_name ILIKE '%/' || ent.entry_name
--rollback                 AND crs.cross_name IN (
--rollback                     'ABDHL0221/ABDHL0221',
--rollback                     'ABDHL120312/ABDHL120312',
--rollback                     'ABDHL120918/ABDHL120918',
--rollback                     'ABLMARSI0022/ABLMARSI0022',
--rollback                     'ABLTI0137/ABLTI0137',
--rollback                     'ABLTI0330/ABLTI0330',
--rollback                     'ABSBL10060/ABSBL10060',
--rollback                     'CML464/CML464',
--rollback                     'ABLMARSI0037/ABLMARSI0037',
--rollback                     'CML543/CML543'
--rollback                 )
--rollback     ) AS t
--rollback ORDER BY
--rollback     t.cross_id,
--rollback     t.order_number
--rollback ;
