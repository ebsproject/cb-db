--liquibase formatted sql

--changeset postgres:update_experiment_type_for_intentional_crossing_nursery_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1514 Update experiment type for intentional crossing nursery in experiment.experiment



UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0008' and experiment_name='IRSEA-HB-2014-DS-1';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0029' and experiment_name='IRSEA-HB-2014-WS-1';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0064' and experiment_name='IRSEA-HB-2015-DS-2';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0065' and experiment_name='IRSEA-HB-2015-DS-1';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0122' and experiment_name='IRSEA-HB-2015-WS-1';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0159' and experiment_name='IRSEA-HB-2016-DS-2';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0160' and experiment_name='IRSEA-HB-2016-DS-1';
UPDATE experiment.experiment SET experiment_type='Intentional Crossing Nursery' WHERE experiment_code='EXPT0198' and experiment_name='IRSEA-HB-2016-WS-1';



--rollback SELECT NULL;