--liquibase formatted sql

--changeset postgres:update_min_value_and_max_value_of_mc_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1222 Update mc_cont min_value = 0.1 and max_value = 50 in master.scale



-- update column
UPDATE
    master.scale
SET
    min_value = 0.1,
    max_value = 50
WHERE
    id = (SELECT scale_id FROM master.variable WHERE abbrev = 'MC_CONT');



-- revert changes
--rollback UPDATE master.scale
--rollback SET min_value = 1,
--rollback     max_value = 50
--rollback WHERE id = (SELECT scale_id FROM master.variable WHERE abbrev = 'MC_CONT');