--liquibase formatted sql

--changeset postgres:update_germplasm_normalized_names context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1595 Update germplasm_normalized_names


ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;

WITH t AS (
    SELECT
        g.id,
        g.germplasm_id,
        g.name_value,
        g.germplasm_normalized_name,
        platform.normalize_text(g.name_value) AS correct_germplasm_normalized_name,
        g.creation_timestamp,
        g.notes
    FROM
        germplasm.germplasm_name AS g
    WHERE
        platform.normalize_text(g.name_value) <> g.germplasm_normalized_name
)
UPDATE
    germplasm.germplasm_name AS g
SET
    germplasm_normalized_name = t.correct_germplasm_normalized_name
FROM
    t
WHERE
    t.id = g.id
;

ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;


ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

WITH t AS (
    SELECT
        g.id,
        g.designation,
        g.germplasm_normalized_name,
        platform.normalize_text(g.designation) AS correct_germplasm_normalized_name,
        g.creation_timestamp,
        g.notes
    FROM
        germplasm.germplasm AS g
    WHERE
        platform.normalize_text(g.designation) <> g.germplasm_normalized_name
)
UPDATE
    germplasm.germplasm AS g
SET
    germplasm_normalized_name = t.correct_germplasm_normalized_name
FROM
    t
WHERE
    t.id = g.id
;

ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;


-- revert changes
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback WITH t AS (
--rollback     SELECT
--rollback         g.id,
--rollback         g.germplasm_id,
--rollback         g.name_value,
--rollback         g.germplasm_normalized_name,
--rollback         platform.normalize_text(g.name_value) AS correct_germplasm_normalized_name,
--rollback         g.creation_timestamp,
--rollback         g.notes
--rollback     FROM
--rollback         germplasm.germplasm_name AS g
--rollback     WHERE
--rollback         platform.normalize_text(g.name_value) <> g.germplasm_normalized_name
--rollback )
--rollback UPDATE
--rollback     germplasm.germplasm_name AS g
--rollback SET
--rollback     germplasm_normalized_name = t.correct_germplasm_normalized_name
--rollback FROM
--rollback     t
--rollback WHERE
--rollback     t.id = g.id
--rollback ;
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback 
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback WITH t AS (
--rollback     SELECT
--rollback         g.id,
--rollback         g.designation,
--rollback         g.germplasm_normalized_name,
--rollback         platform.normalize_text(g.designation) AS correct_germplasm_normalized_name,
--rollback         g.creation_timestamp,
--rollback         g.notes
--rollback     FROM
--rollback         germplasm.germplasm AS g
--rollback     WHERE
--rollback         platform.normalize_text(g.designation) <> g.germplasm_normalized_name
--rollback )
--rollback UPDATE
--rollback     germplasm.germplasm AS g
--rollback SET
--rollback     germplasm_normalized_name = t.correct_germplasm_normalized_name
--rollback FROM
--rollback     t
--rollback WHERE
--rollback     t.id = g.id
--rollback ;
--rollback 
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
