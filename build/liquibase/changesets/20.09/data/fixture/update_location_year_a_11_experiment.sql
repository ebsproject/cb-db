--liquibase formatted sql

--changeset postgres:update_location_year_a_11_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1549 Update location yeart A-11 experiment



UPDATE 
    experiment.location
SET
    location_year = 2019
WHERE
    location_name='A-11_LOC1';



--rollback UPDATE 
--rollback     experiment.location
--rollback SET
--rollback     location_year = 2017
--rollback WHERE
--rollback     location_name='A-11_LOC1';