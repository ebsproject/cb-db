--liquibase formatted sql

--changeset postgres:update_tenant.program_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Update tenant.program for wheat and maize



-- update wheat and maize programs
UPDATE
    tenant.program
SET
    program_code = 'BW',
    program_name = 'BW Wheat Breeding Program',
    description = 'BW Wheat Breeding Program'
WHERE
    program_code = 'WBP'
;

UPDATE
    tenant.program
SET
    program_code = 'KE',
    program_name = 'KE Maize Breeding Program',
    description = 'KE Maize Breeding Program'
WHERE
    program_code = 'MBP'
;



-- revert changes
--rollback UPDATE
--rollback     tenant.program
--rollback SET
--rollback     program_code = 'WBP',
--rollback     program_name = 'Wheat Breeding Program',
--rollback     description = 'Wheat Breeding Program'
--rollback WHERE
--rollback     program_code = 'BW'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     tenant.program
--rollback SET
--rollback     program_code = 'MBP',
--rollback     program_name = 'Maize Breeding Program',
--rollback     description = 'Maize Breeding Program'
--rollback WHERE
--rollback     program_code = 'KE'
--rollback ;



--changeset postgres:update_tenant.project_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Update tenant.project for wheat and maize



-- update wheat and maize projects
UPDATE
    tenant.project
SET
    project_code = 'BW_PROJECT',
    project_name = 'BW Wheat Breeding Project',
    description = 'BW Wheat Breeding Project'
WHERE
    project_code = 'WBP_PROJECT'
;

UPDATE
    tenant.project
SET
    project_code = 'KE_PROJECT',
    project_name = 'KE Maize Breeding Project',
    description = 'KE Maize Breeding Project'
WHERE
    project_code = 'MBP_PROJECT'
;



-- revert changes
--rollback UPDATE
--rollback     tenant.project
--rollback SET
--rollback     project_code = 'WBP_PROJECT',
--rollback     project_name = 'WBP Project',
--rollback     description = 'Wheat Breeding Program Project'
--rollback WHERE
--rollback     project_code = 'BW_PROJECT'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     tenant.project
--rollback SET
--rollback     project_code = 'MBP_PROJECT',
--rollback     project_name = 'MBP Project',
--rollback     description = 'Maize Breeding Program Project'
--rollback WHERE
--rollback     project_code = 'KE_PROJECT'
--rollback ;



--changeset postgres:update_tenant.team_for_wheat_and_maize context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Update tenant.team for wheat and maize



-- update wheat and maize teams
UPDATE
    tenant.team
SET
    team_code = 'BW_TEAM',
    team_name = 'BW Wheat Breeding Team',
    description = 'BW Wheat Breeding Team'
WHERE
    team_code = 'WBP_TEAM'
;

UPDATE
    tenant.team
SET
    team_code = 'KE_TEAM',
    team_name = 'KE Maize Breeding Team',
    description = 'KE Maize Breeding Team'
WHERE
    team_code = 'MBP_TEAM'
;



-- revert changes
--rollback UPDATE
--rollback     tenant.team
--rollback SET
--rollback     team_code = 'WBP_TEAM',
--rollback     team_name = 'WBP Team',
--rollback     description = 'Wheat Breeding Program Team'
--rollback WHERE
--rollback     team_code = 'BW_TEAM'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     tenant.team
--rollback SET
--rollback     team_code = 'MBP_TEAM',
--rollback     team_name = 'MBP Team',
--rollback     description = 'Maize Breeding Program Team'
--rollback WHERE
--rollback     team_code = 'KE_TEAM'
--rollback ;
