--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_experiment.experiment_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.experiment



-- populate wheat experiment F1SIMPLEBW-3
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'F1') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2014 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'A') AS season_id,
    '2014A' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'F1SIMPLEBW-3' AS experiment_name,
    'Generation Nursery' AS experiment_type,
    NULL AS experiment_sub_type,
    'Selection and Advancement' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'a.ramos') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'a.ramos') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'GENERATION_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'F1SIMPLEBW-3'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry_list



-- populate wheat entry list F1SIMPLEBW-3_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'F1SIMPLEBW-3_ENTLIST' AS entry_list_name,
    'created' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'F1SIMPLEBW-3') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'a.ramos') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.entry



-- populate wheat entries for F1SIMPLEBW-3
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS14Y00301S','Y13-14CBBW207'),
        (2,'CMSS14Y00302S','Y13-14CBBW207'),
        (3,'CMSS14Y00303S','Y13-14CBBW207'),
        (4,'CMSS14Y00304S','Y13-14CBBW207'),
        (5,'CMSS14Y00305S','Y13-14CBBW207'),
        (6,'CMSS14Y00306S','Y13-14CBBW207'),
        (7,'CMSS14Y00307S','Y13-14CBBW207'),
        (8,'CMSS14Y00308S','Y13-14CBBW209'),
        (9,'CMSS14Y00309S','Y13-14CBBW209'),
        (10,'CMSS14Y00310S','Y13-14CBBW209'),
        (11,'CMSS14Y00311S','Y13-14CBBW209'),
        (12,'CMSS14Y00312S','Y13-14CBBW209'),
        (13,'CMSS14Y00313S','Y13-14CBBW209'),
        (14,'CMSS14Y00314S','Y13-14CBBW214'),
        (15,'CMSS14Y00315S','Y13-14CBBW214'),
        (16,'CMSS14Y00316S','Y13-14CBBW214'),
        (17,'CMSS14Y00317S','Y13-14CBBW217'),
        (18,'CMSS14Y00318S','Y13-14CBBW217'),
        (19,'CMSS14Y00319S','Y13-14CBBW218'),
        (20,'CMSS14Y00320S','Y13-14CBBW218'),
        (21,'CMSS14Y00321S','Y13-14CBBW223'),
        (22,'CMSS14Y00322S','Y13-14CBBW223'),
        (23,'CMSS14Y00323S','Y13-14CBBW224'),
        (24,'CMSS14Y00324S','Y13-14CBBW224'),
        (25,'CMSS14Y00325S','Y13-14CBBW225'),
        (26,'CMSS14Y00326S','Y13-14CBBW226'),
        (27,'CMSS14Y00327S','Y13-14CBBW226'),
        (28,'CMSS14Y00328S','Y13-14CBBW226'),
        (29,'CMSS14Y00329S','Y13-14CBBW226'),
        (30,'CMSS14Y00330S','Y13-14CBBW227'),
        (31,'CMSS14Y00331S','Y13-14CBBW227'),
        (32,'CMSS14Y00332S','Y13-14CBBW228'),
        (33,'CMSS14Y00333S','Y13-14CBBW228'),
        (34,'CMSS14Y00334S','Y13-14CBBW229'),
        (35,'CMSS14Y00335S','Y13-14CBBW229'),
        (36,'CMSS14Y00336S','Y13-14CBBW230'),
        (37,'CMSS14Y00337S','Y13-14CBBW230'),
        (38,'CMSS14Y00338S','Y13-14CBBW231'),
        (39,'CMSS14Y00339S','Y13-14CBBW231'),
        (40,'CMSS14Y00340S','Y13-14CBBW233'),
        (41,'CMSS14Y00341S','Y13-14CBBW233'),
        (42,'CMSS14Y00342S','Y13-14CBBW237'),
        (43,'CMSS14Y00343S','Y13-14CBBW240'),
        (44,'CMSS14Y00344S','Y13-14CBBW240'),
        (45,'CMSS14Y00345S','Y13-14CBBW241'),
        (46,'CMSS14Y00346S','Y13-14CBBW241'),
        (47,'CMSS14Y00347S','Y13-14CBBW246'),
        (48,'CMSS14Y00348S','Y13-14CBBW246'),
        (49,'CMSS14Y00349S','Y13-14CBBW247'),
        (50,'CMSS14Y00350S','Y13-14CBBW247'),
        (51,'CMSS14Y00351S','Y13-14CBBW249'),
        (52,'CMSS14Y00352S','Y13-14CBBW249'),
        (53,'CMSS14Y00353S','Y13-14CBBW255'),
        (54,'CMSS14Y00354S','Y13-14CBBW256'),
        (55,'CMSS14Y00355S','Y13-14CBBW259'),
        (56,'CMSS14Y00356S','Y13-14CBBW259'),
        (57,'CMSS14Y00357S','Y13-14CBBW260'),
        (58,'CMSS14Y00358S','Y13-14CBBW260'),
        (59,'CMSS14Y00359S','Y13-14CBBW261'),
        (60,'CMSS14Y00360S','Y13-14CBBW261'),
        (61,'CMSS14Y00361S','Y13-14CBBW261'),
        (62,'CMSS14Y00362S','Y13-14CBBW261'),
        (63,'CMSS14Y00363S','Y13-14CBBW262'),
        (64,'CMSS14Y00364S','Y13-14CBBW262'),
        (65,'CMSS14Y00365S','Y13-14CBBW263'),
        (66,'CMSS14Y00366S','Y13-14CBBW263'),
        (67,'CMSS14Y00367S','Y13-14CBBW264'),
        (68,'CMSS14Y00368S','Y13-14CBBW266'),
        (69,'CMSS14Y00369S','Y13-14CBBW266'),
        (70,'CMSS14Y00370S','Y13-14CBBW267'),
        (71,'CMSS14Y00371S','Y13-14CBBW267'),
        (72,'CMSS14Y00372S','Y13-14CBBW268'),
        (73,'CMSS14Y00373S','Y13-14CBBW268'),
        (74,'CMSS14Y00374S','Y13-14CBBW269'),
        (75,'CMSS14Y00375S','Y13-14CBBW269'),
        (76,'CMSS14Y00376S','Y13-14CBBW270'),
        (77,'CMSS14Y00377S','Y13-14CBBW270'),
        (78,'CMSS14Y00378S','Y13-14CBBW271'),
        (79,'CMSS14Y00379S','Y13-14CBBW271'),
        (80,'CMSS14Y00380S','Y13-14CBBW271'),
        (81,'CMSS14Y00381S','Y13-14CBBW273'),
        (82,'CMSS14Y00382S','Y13-14CBBW273'),
        (83,'CMSS14Y00383S','Y13-14CBBW274'),
        (84,'CMSS14Y00384S','Y13-14CBBW274'),
        (85,'CMSS14Y00385S','Y13-14CBBW275'),
        (86,'CMSS14Y00386S','Y13-14CBBW275'),
        (87,'CMSS14Y00387S','Y13-14CBBW276'),
        (88,'CMSS14Y00388S','Y13-14CBBW276'),
        (89,'CMSS14Y00389S','Y13-14CBBW276'),
        (90,'CMSS14Y00390S','Y13-14CBBW277'),
        (91,'CMSS14Y00391S','Y13-14CBBW277'),
        (92,'CMSS14Y00392S','Y13-14CBBW278'),
        (93,'CMSS14Y00393S','Y13-14CBBW278'),
        (94,'CMSS14Y00394S','Y13-14CBBW280'),
        (95,'CMSS14Y00395S','Y13-14CBBW280'),
        (96,'CMSS14Y00396S','Y13-14CBBW281'),
        (97,'CMSS14Y00397S','Y13-14CBBW281'),
        (98,'CMSS14Y00398S','Y13-14CBBW282'),
        (99,'CMSS14Y00399S','Y13-14CBBW282'),
        (100,'CMSS14Y00400S','Y13-14CBBW285'),
        (101,'CMSS14Y00401S','Y13-14CBBW285'),
        (102,'CMSS14Y00402S','Y13-14CBBW286'),
        (103,'CMSS14Y00403S','Y13-14CBBW286'),
        (104,'CMSS14Y00404S','Y13-14CBBW286'),
        (105,'CMSS14Y00405S','Y13-14CBBW286'),
        (106,'CMSS14Y00406S','Y13-14CBBW286'),
        (107,'CMSS14Y00407S','Y13-14CBBW287'),
        (108,'CMSS14Y00408S','Y13-14CBBW287'),
        (109,'CMSS14Y00409S','Y13-14CBBW287'),
        (110,'CMSS14Y00410S','Y13-14CBBW288'),
        (111,'CMSS14Y00411S','Y13-14CBBW288'),
        (112,'CMSS14Y00412S','Y13-14CBBW288'),
        (113,'CMSS14Y00413S','Y13-14CBBW288'),
        (114,'CMSS14Y00414S','Y13-14CBBW288'),
        (115,'CMSS14Y00415S','Y13-14CBBW288'),
        (116,'CMSS14Y00416S','Y13-14CBBW291'),
        (117,'CMSS14Y00417S','Y13-14CBBW291'),
        (118,'CMSS14Y00418S','Y13-14CBBW291'),
        (119,'CMSS14Y00419S','Y13-14CBBW291'),
        (120,'CMSS14Y00420S','Y13-14CBBW292'),
        (121,'CMSS14Y00421S','Y13-14CBBW292'),
        (122,'CMSS14Y00422S','Y13-14CBBW292'),
        (123,'CMSS14Y00423S','Y13-14CBBW294'),
        (124,'CMSS14Y00424S','Y13-14CBBW296'),
        (125,'CMSS14Y00425S','Y13-14CBBW297'),
        (126,'CMSS14Y00426S','Y13-14CBBW297'),
        (127,'CMSS14Y00427S','Y13-14CBBW299'),
        (128,'CMSS14Y00428S','Y13-14CBBW299'),
        (129,'CMSS14Y00429S','Y13-14CBBW300'),
        (130,'CMSS14Y00430S','Y13-14CBBW300'),
        (131,'CMSS14Y00431S','Y13-14CBBW301'),
        (132,'CMSS14Y00432S','Y13-14CBBW301'),
        (133,'CMSS14Y00433S','Y13-14CBBW304'),
        (134,'CMSS14Y00434S','Y13-14CBBW304'),
        (135,'CMSS14Y00435S','Y13-14CBBW308'),
        (136,'CMSS14Y00436S','Y13-14CBBW309'),
        (137,'CMSS14Y00437S','Y13-14CBBW309'),
        (138,'CMSS14Y00438S','Y13-14CBBW312'),
        (139,'CMSS14Y00439S','Y13-14CBBW312'),
        (140,'CMSS14Y00440S','Y13-14CBBW315'),
        (141,'CMSS14Y00441S','Y13-14CBBW315'),
        (142,'CMSS14Y00442S','Y13-14CBBW315'),
        (143,'CMSS14Y00443S','Y13-14CBBW317'),
        (144,'CMSS14Y00444S','Y13-14CBBW317'),
        (145,'CMSS14Y00445S','Y13-14CBBW318'),
        (146,'CMSS14Y00446S','Y13-14CBBW318'),
        (147,'CMSS14Y00447S','Y13-14CBBW319'),
        (148,'CMSS14Y00448S','Y13-14CBBW319'),
        (149,'CMSS14Y00449S','Y13-14CBBW321'),
        (150,'CMSS14Y00450S','Y13-14CBBW321')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.ramos'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.occurrence



-- populate wheat occurrence for F1SIMPLEBW-3
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'F1SIMPLEBW-3_OCC1' AS occurrence_name,
    'planted' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    1 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'F1SIMPLEBW-3'
    AND geo.geospatial_object_code = 'EB'
    AND person.username = 'a.ramos'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'F1SIMPLEBW-3_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in place.geospatial_object



-- populate wheat planting area for F1SIMPLEBW-3
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'F1SIMPLEBW-3_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'F1SIMPLEBW-3_LOC1'
--rollback ;


--changeset postgres:populate_wheat_f1_nursery_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location



-- populate wheat location for F1SIMPLEBW-3
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'F1SIMPLEBW-3_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'a.ramos'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'F1SIMPLEBW-3_LOC1'
    AND site.geospatial_object_code = 'EB'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'F1SIMPLEBW-3_LOC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.plot



-- populate wheat plots for F1SIMPLEBW-3
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    1 AS pa_x,
    t.plot_number AS pa_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    (
        VALUES
        (1,1,1,'CMSS14Y00301S'),
        (2,1,2,'CMSS14Y00302S'),
        (3,1,3,'CMSS14Y00303S'),
        (4,1,4,'CMSS14Y00304S'),
        (5,1,5,'CMSS14Y00305S'),
        (6,1,6,'CMSS14Y00306S'),
        (7,1,7,'CMSS14Y00307S'),
        (8,1,8,'CMSS14Y00308S'),
        (9,1,9,'CMSS14Y00309S'),
        (10,1,10,'CMSS14Y00310S'),
        (11,1,11,'CMSS14Y00311S'),
        (12,1,12,'CMSS14Y00312S'),
        (13,1,13,'CMSS14Y00313S'),
        (14,1,14,'CMSS14Y00314S'),
        (15,1,15,'CMSS14Y00315S'),
        (16,1,16,'CMSS14Y00316S'),
        (17,1,17,'CMSS14Y00317S'),
        (18,1,18,'CMSS14Y00318S'),
        (19,1,19,'CMSS14Y00319S'),
        (20,1,20,'CMSS14Y00320S'),
        (21,1,21,'CMSS14Y00321S'),
        (22,1,22,'CMSS14Y00322S'),
        (23,1,23,'CMSS14Y00323S'),
        (24,1,24,'CMSS14Y00324S'),
        (25,1,25,'CMSS14Y00325S'),
        (26,1,26,'CMSS14Y00326S'),
        (27,1,27,'CMSS14Y00327S'),
        (28,1,28,'CMSS14Y00328S'),
        (29,1,29,'CMSS14Y00329S'),
        (30,1,30,'CMSS14Y00330S'),
        (31,1,31,'CMSS14Y00331S'),
        (32,1,32,'CMSS14Y00332S'),
        (33,1,33,'CMSS14Y00333S'),
        (34,1,34,'CMSS14Y00334S'),
        (35,1,35,'CMSS14Y00335S'),
        (36,1,36,'CMSS14Y00336S'),
        (37,1,37,'CMSS14Y00337S'),
        (38,1,38,'CMSS14Y00338S'),
        (39,1,39,'CMSS14Y00339S'),
        (40,1,40,'CMSS14Y00340S'),
        (41,1,41,'CMSS14Y00341S'),
        (42,1,42,'CMSS14Y00342S'),
        (43,1,43,'CMSS14Y00343S'),
        (44,1,44,'CMSS14Y00344S'),
        (45,1,45,'CMSS14Y00345S'),
        (46,1,46,'CMSS14Y00346S'),
        (47,1,47,'CMSS14Y00347S'),
        (48,1,48,'CMSS14Y00348S'),
        (49,1,49,'CMSS14Y00349S'),
        (50,1,50,'CMSS14Y00350S'),
        (51,1,51,'CMSS14Y00351S'),
        (52,1,52,'CMSS14Y00352S'),
        (53,1,53,'CMSS14Y00353S'),
        (54,1,54,'CMSS14Y00354S'),
        (55,1,55,'CMSS14Y00355S'),
        (56,1,56,'CMSS14Y00356S'),
        (57,1,57,'CMSS14Y00357S'),
        (58,1,58,'CMSS14Y00358S'),
        (59,1,59,'CMSS14Y00359S'),
        (60,1,60,'CMSS14Y00360S'),
        (61,1,61,'CMSS14Y00361S'),
        (62,1,62,'CMSS14Y00362S'),
        (63,1,63,'CMSS14Y00363S'),
        (64,1,64,'CMSS14Y00364S'),
        (65,1,65,'CMSS14Y00365S'),
        (66,1,66,'CMSS14Y00366S'),
        (67,1,67,'CMSS14Y00367S'),
        (68,1,68,'CMSS14Y00368S'),
        (69,1,69,'CMSS14Y00369S'),
        (70,1,70,'CMSS14Y00370S'),
        (71,1,71,'CMSS14Y00371S'),
        (72,1,72,'CMSS14Y00372S'),
        (73,1,73,'CMSS14Y00373S'),
        (74,1,74,'CMSS14Y00374S'),
        (75,1,75,'CMSS14Y00375S'),
        (76,1,76,'CMSS14Y00376S'),
        (77,1,77,'CMSS14Y00377S'),
        (78,1,78,'CMSS14Y00378S'),
        (79,1,79,'CMSS14Y00379S'),
        (80,1,80,'CMSS14Y00380S'),
        (81,1,81,'CMSS14Y00381S'),
        (82,1,82,'CMSS14Y00382S'),
        (83,1,83,'CMSS14Y00383S'),
        (84,1,84,'CMSS14Y00384S'),
        (85,1,85,'CMSS14Y00385S'),
        (86,1,86,'CMSS14Y00386S'),
        (87,1,87,'CMSS14Y00387S'),
        (88,1,88,'CMSS14Y00388S'),
        (89,1,89,'CMSS14Y00389S'),
        (90,1,90,'CMSS14Y00390S'),
        (91,1,91,'CMSS14Y00391S'),
        (92,1,92,'CMSS14Y00392S'),
        (93,1,93,'CMSS14Y00393S'),
        (94,1,94,'CMSS14Y00394S'),
        (95,1,95,'CMSS14Y00395S'),
        (96,1,96,'CMSS14Y00396S'),
        (97,1,97,'CMSS14Y00397S'),
        (98,1,98,'CMSS14Y00398S'),
        (99,1,99,'CMSS14Y00399S'),
        (100,1,100,'CMSS14Y00400S'),
        (101,1,101,'CMSS14Y00401S'),
        (102,1,102,'CMSS14Y00402S'),
        (103,1,103,'CMSS14Y00403S'),
        (104,1,104,'CMSS14Y00404S'),
        (105,1,105,'CMSS14Y00405S'),
        (106,1,106,'CMSS14Y00406S'),
        (107,1,107,'CMSS14Y00407S'),
        (108,1,108,'CMSS14Y00408S'),
        (109,1,109,'CMSS14Y00409S'),
        (110,1,110,'CMSS14Y00410S'),
        (111,1,111,'CMSS14Y00411S'),
        (112,1,112,'CMSS14Y00412S'),
        (113,1,113,'CMSS14Y00413S'),
        (114,1,114,'CMSS14Y00414S'),
        (115,1,115,'CMSS14Y00415S'),
        (116,1,116,'CMSS14Y00416S'),
        (117,1,117,'CMSS14Y00417S'),
        (118,1,118,'CMSS14Y00418S'),
        (119,1,119,'CMSS14Y00419S'),
        (120,1,120,'CMSS14Y00420S'),
        (121,1,121,'CMSS14Y00421S'),
        (122,1,122,'CMSS14Y00422S'),
        (123,1,123,'CMSS14Y00423S'),
        (124,1,124,'CMSS14Y00424S'),
        (125,1,125,'CMSS14Y00425S'),
        (126,1,126,'CMSS14Y00426S'),
        (127,1,127,'CMSS14Y00427S'),
        (128,1,128,'CMSS14Y00428S'),
        (129,1,129,'CMSS14Y00429S'),
        (130,1,130,'CMSS14Y00430S'),
        (131,1,131,'CMSS14Y00431S'),
        (132,1,132,'CMSS14Y00432S'),
        (133,1,133,'CMSS14Y00433S'),
        (134,1,134,'CMSS14Y00434S'),
        (135,1,135,'CMSS14Y00435S'),
        (136,1,136,'CMSS14Y00436S'),
        (137,1,137,'CMSS14Y00437S'),
        (138,1,138,'CMSS14Y00438S'),
        (139,1,139,'CMSS14Y00439S'),
        (140,1,140,'CMSS14Y00440S'),
        (141,1,141,'CMSS14Y00441S'),
        (142,1,142,'CMSS14Y00442S'),
        (143,1,143,'CMSS14Y00443S'),
        (144,1,144,'CMSS14Y00444S'),
        (145,1,145,'CMSS14Y00445S'),
        (146,1,146,'CMSS14Y00446S'),
        (147,1,147,'CMSS14Y00447S'),
        (148,1,148,'CMSS14Y00448S'),
        (149,1,149,'CMSS14Y00449S'),
        (150,1,150,'CMSS14Y00450S')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'F1SIMPLEBW-3_OCC1'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'F1SIMPLEBW-3_LOC1'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.ramos'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-3_OCC1'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.planting_instruction



-- populate wheat planting instructions for F1SIMPLEBW-3
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.ramos'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.package_log



-- populate wheat package logs for F1SIMPLEBW-3
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'a.ramos'
WHERE
    entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_f1_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery package_log_id in experiment.planting_instruction



-- populate wheat package log references for F1SIMPLEBW-3
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'F1SIMPLEBW-3_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross



-- populate wheat crosses for F1SIMPLEBW-3
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entf.entry_name) AS cross_name,
    'SELFING' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'a.ramos'
WHERE
    expt.experiment_name = 'F1SIMPLEBW-3'
ORDER BY
    entf.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-3'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in germplasm.cross_parent



-- populate wheat cross parents for F1SIMPLEBW-3
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female-and-male' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            experiment.experiment AS expt
            INNER JOIN germplasm.cross AS crs
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'a.ramos'
        WHERE
            expt.experiment_name = 'F1SIMPLEBW-3'
            AND crs.cross_name ILIKE ent.entry_name || '/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'F1SIMPLEBW-3'
--rollback ;



--changeset postgres:populate_wheat_f1_nursery_in_experiment.location_occurrence_group_part_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1550 Populate wheat F1 nursery in experiment.location_occurrence_group



-- populate wheat location occurrence groups for F1SIMPLEBW-3
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'F1SIMPLEBW-3_LOC1'
    AND occ.occurrence_name = 'F1SIMPLEBW-3_OCC1'
    AND prs.username = 'a.ramos'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'F1SIMPLEBW-3_LOC1'
--rollback     AND occ.occurrence_name = 'F1SIMPLEBW-3_OCC1'
--rollback ;
