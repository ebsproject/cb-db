--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.experiment



-- populate wheat experiment CBBW-1
INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'BW') AS program_id,
    (SELECT id FROM tenant.pipeline WHERE pipeline_code = 'GWP_PIPELINE') AS pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'HB') AS stage_id,
    (SELECT id FROM tenant.project WHERE project_code = 'BW_PROJECT') AS project_id,
    2017 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'B') AS season_id,
    '2017B' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'CBBW-1' AS experiment_name,
    'Intentional Crossing Nursery' AS experiment_type,
    'Breeding Crosses' AS experiment_sub_type,
    'Crossing Block' AS experiment_sub_sub_type,
    'Systematic Arrangement' AS experiment_design_type,
    'entry list created; crosses created; design generated; occurrences created' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'CBBW-1'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_list



-- populate wheat entry list CBBW-1_ENTLIST
INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'CBBW-1_ENTLIST' AS entry_list_name,
    'draft' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'CBBW-1') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry



-- populate wheat entries for CBBW-1
INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    'entry' AS entry_type,
    'female-and-male' AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES
        (1,'CMSS07B00580T-099TOPY-099M-099NJ-099NJ-10WGY-0B','BV2017PPTBW7'),
        (2,'CMSS08Y00057S-099Y-099M-099NJ-13WGY-0B','BV2017PPTBW8'),
        (3,'CMSS08Y00278S-099Y-099M-099Y-5M-0WGY','BV2017CBBW5'),
        (4,'CMSS08Y00833T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B','BV2017CBBW6'),
        (5,'CMSS08Y01088T-099M-099Y-099M-099NJ-5WGY-0B','BV2017PPTBW15'),
        (6,'CMSA08Y00378S-050Y-040M-0NJ-2Y-0B','BV2017PPTBW20'),
        (7,'CMSS08B00178S-099M-099Y-15M-0RGY','BV2017PPTBW23'),
        (8,'CMSS08B00181S-099M-099NJ-099NJ-34WGY-0B','BV2017CBBW1'),
        (9,'CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B','BV2017CBBW2'),
        (10,'CMSS08B00259S-099M-099NJ-30RGY-0B','BV2017PPTBW24'),
        (11,'CMSS08B00422S-099M-099NJ-5RGY-0B','BV2017CBBW9'),
        (12,'CMSS08B00633T-099TOPY-099M-099NJ-11WGY-0B','BV2017CBBW3'),
        (13,'CMSS08B00712T-099TOPY-099M-099NJ-099NJ-14RGY-0B','BV2017PPTBW25'),
        (14,'CMSS08B00854T-099TOPY-099M-099Y-12M-0WGY','BV2017PPTBW19'),
        (15,'CMSS08B00866T-099TOPY-099M-099NJ-099NJ-40WGY-0B','BV2017CBBW7'),
        (16,'CMSS08B00880T-099TOPY-099M-099NJ-099NJ-48WGY-0B','BV2017CBBW8'),
        (17,'CMSS08B00954T-099TOPY-099M-099Y-099B-7WGY-0B','BV2017CBBW4'),
        (18,'CMSS08B00987T-099TOPY-099M-099Y-099B-40WGY-0B','BV2017PPTBW26'),
        (19,'CMSS09Y00808T-099TOPM-099Y-099M-099Y-4WGY-0B','BV2017PPTBW28'),
        (20,'CMSA09Y00383T-099B-050Y-050ZTM-0NJ-099NJ-11WGY-0B','BV2017PPTBW31'),
        (21,'CMSA09M00466S-050ZTM-050Y-3WGY-0B','BV2017PPTBW33'),
        (22,'CMSS09B00264S-099M-099Y-10WGY-0B','BV2017PPTBW36'),
        (23,'CMSS09B00382S-099ZTM-099NJ-099NJ-18WGY-0B','BV2017PPTBW37'),
        (24,'CMSA09M00047T-079(SR26POS)Y-050ZTM-0NJ-099NJ-3WGY-0B','BV2017PPTBW38'),
        (25,'CMSS10Y00030S-099Y-099M-11WGY-0B','BV2017PPTBW40'),
        (26,'CMSS10Y00359S-099Y-099M-3WGY-0B','BV2017PPTBW42'),
        (27,'CMSS10Y00374S-099Y-099M-1WGY-0B','BV2017PPTBW43'),
        (28,'CMSS10Y00842T-099TOPM-099Y-099M-1WGY-0B','BV2017PPTBW44'),
        (29,'CMSA08WM00143S-050ZTM-050Y-120ZTM-012Y-02B-0RGY','BV2017CBBW10'),
        (30,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-3RGY-0B','BV2017CBBW11'),
        (31,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-13RGY-0B','BV2017CBBW12'),
        (32,'CMSS09Y00882T-099TOPM-099Y-099ZTM-099NJ-099NJ-5RGY-0B','BV2017CBBW13'),
        (33,'CMSS09Y00883T-099TOPM-099Y-099ZTM-099NJ-099NJ-10RGY-0B','BV2017CBBW14'),
        (34,'CMSA09Y00193T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B','BV2017CBBW15'),
        (35,'CMSA09Y00210T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B','BV2017PPTBW45'),
        (36,'CMSA09M00198T-050Y-050ZTM-0NJ-099NJ-2RGY-0B','BV2017PPTBW46'),
        (37,'CMSA11Y00323S-099Y-099M-099NJ-099NJ-1WGY-0B','BV2017CBBW80'),
        (38,'CMSA11Y00364S-099Y-099M-099NJ-099NJ-7WGY-0B','BV2017CBBW81'),
        (39,'CMSA11Y00384S-099Y-099M-099NJ-099NJ-10WGY-0B','BV2017CBBW82'),
        (40,'CMSA11Y00398S-099Y-099M-099NJ-099NJ-35WGY-0B','BV2017CBBW83'),
        (41,'CMSA11Y00402S-099Y-099M-099NJ-099NJ-18WGY-0B','BV2017CBBW84'),
        (42,'CMSA11Y00417S-099Y-099M-099NJ-099NJ-2WGY-0B','BV2017CBBW85'),
        (43,'CMSA11Y00449S-099Y-099M-099NJ-099NJ-50WGY-0B','BV2017CBBW87'),
        (44,'CMSA11Y00489S-099Y-099M-099NJ-099NJ-5WGY-0B','BV2017CBBW88'),
        (45,'CMSA11Y00507S-099Y-099M-099NJ-099NJ-27WGY-0B','BV2017CBBW89'),
        (46,'CMSS11Y00063S-099Y-099M-0SY-8M-0WGY','BV2017CBBW16'),
        (47,'CMSS11Y00170S-099Y-099M-0SY-11M-0WGY','BV2017CBBW17'),
        (48,'CMSS11Y00172S-099Y-099M-099NJ-099NJ-5WGY-0B','BV2017CBBW18'),
        (49,'CMSS11Y00191S-099Y-099M-099NJ-099NJ-11WGY-0B','BV2017CBBW19'),
        (50,'CMSS11Y00247S-099Y-099M-099NJ-099NJ-35WGY-0B','BV2017CBBW20'),
        (51,'CMSS11Y00302S-099Y-099M-099NJ-099NJ-28WGY-0B','BV2017CBBW21'),
        (52,'CMSS11Y00424S-099Y-099M-099NJ-099NJ-29RGY-0B','BV2017CBBW90'),
        (53,'CMSS11Y00430S-099Y-099M-099NJ-099NJ-11WGY-0B','BV2017CBBW22'),
        (54,'CMSS11Y00722T-099TOPM-099Y-099M-099NJ-099NJ-6WGY-0B','BV2017CBBW23'),
        (55,'CMSS11Y00723T-099TOPM-099Y-099M-099NJ-099NJ-16WGY-0B','BV2017CBBW24'),
        (56,'CMSS11Y00775T-099TOPM-099Y-099M-0SY-20M-0WGY','BV2017CBBW25'),
        (57,'CMSS11Y00822T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B','BV2017CBBW26'),
        (58,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-23WGY-0B','BV2017CBBW27'),
        (59,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-28WGY-0B','BV2017CBBW28'),
        (60,'CMSS11Y00827T-099TOPM-099Y-099M-0SY-11M-0WGY','BV2017CBBW29'),
        (61,'CMSS11Y00831T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B','BV2017CBBW30'),
        (62,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-55WGY-0B','BV2017CBBW31'),
        (63,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-68WGY-0B','BV2017CBBW32'),
        (64,'CMSS11Y00973T-099TOPM-099Y-099M-0SY-29M-0WGY','BV2017CBBW33'),
        (65,'CMSS11Y00977T-099TOPM-099Y-099M-099NJ-099NJ-22WGY-0B','BV2017PPTBW49'),
        (66,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-2WGY-0B','BV2017CBBW34'),
        (67,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B','BV2017CBBW35'),
        (68,'CMSS11Y01006T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B','BV2017CBBW36'),
        (69,'CMSS11Y01008T-099TOPM-099Y-099M-0SY-16M-0WGY','BV2017PPTBW50'),
        (70,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B','BV2017CBBW37'),
        (71,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-41WGY-0B','BV2017CBBW38'),
        (72,'CMSS11Y01081T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B','BV2017CBBW39'),
        (73,'CMSS11Y01091T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B','BV2017CBBW40'),
        (74,'CMSS11Y01117T-099TOPM-099Y-099M-0SY-4M-0WGY','BV2017CBBW41'),
        (75,'CMSS11Y01119T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B','BV2017CBBW42'),
        (76,'CMSS11Y01138T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B','BV2017CBBW43'),
        (77,'CMSS11Y01144T-099TOPM-099Y-099M-099NJ-099NJ-38WGY-0B','BV2017PPTBW52'),
        (78,'CMSS11Y01152T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B','BV2017CBBW44'),
        (79,'CMSS11Y01158T-099TOPM-099Y-099M-0SY-4M-0WGY','BV2017PPTBW54'),
        (80,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B','BV2017CBBW45'),
        (81,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-14WGY-0B','BV2017CBBW46'),
        (82,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B','BV2017PPTBW58'),
        (83,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-12WGY-0B','BV2017PPTBW59'),
        (84,'CMSS11Y01227T-099TOPM-099Y-099M-099NJ-099NJ-1WGY-0B','BV2017PPTBW63'),
        (85,'CMSS11B00079S-099M-099NJ-099NJ-15WGY-0B','BV2017CBBW47'),
        (86,'CMSS11B00147S-099M-099NJ-099NJ-24WGY-0B','BV2017CBBW48'),
        (87,'CMSS11B00167S-099M-099NJ-099NJ-5WGY-0B','BV2017PPTBW66'),
        (88,'CMSS11B00167S-099M-099NJ-099NJ-25WGY-0B','BV2017PPTBW67'),
        (89,'CMSS11B00190S-099M-099NJ-099NJ-50WGY-0B','BV2017PPTBW69'),
        (90,'CMSS11B00192S-099M-099NJ-099NJ-27WGY-0B','BV2017CBBW49'),
        (91,'CMSS11B00199S-099M-099NJ-099NJ-16WGY-0B','BV2017CBBW50'),
        (92,'CMSS11B00199S-099M-099NJ-099NJ-19WGY-0B','BV2017CBBW51'),
        (93,'CMSS11B00211S-099M-099NJ-099NJ-4WGY-0B','BV2017PPTBW71'),
        (94,'CMSS11B00214S-099M-099NJ-099NJ-12WGY-0B','BV2017CBBW52'),
        (95,'CMSS11B00326S-099M-0SY-5M-0WGY','BV2017CBBW53'),
        (96,'CMSS11B00366S-099M-099NJ-099NJ-12WGY-0B','BV2017CBBW54'),
        (97,'CMSS11B00439S-099M-099NJ-099NJ-11WGY-0B','BV2017PPTBW73'),
        (98,'CMSS11B00492S-099M-099NJ-099NJ-11WGY-0B','BV2017CBBW55'),
        (99,'CMSS11B00550S-099M-0SY-36M-0WGY','BV2017CBBW57'),
        (100,'CMSS11B00551S-099M-0SY-16M-0WGY','BV2017CBBW58')
    ) AS t (
        entry_number, designation, seed_name
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'CBBW-1_ENTLIST'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.entry_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.entry_data



-- populate wheat entry data for CBBW-1
INSERT INTO
    experiment.entry_data (
        entry_id, variable_id, data_value, data_qc_code, creator_id
    )
SELECT
    ent.id AS entry_id,
    var.id AS variable_id,
    'Line' AS data_value,
    'N' AS data_qc_code,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PARENT_TYPE'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'CBBW-1_ENTLIST'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_data AS entdata
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON entlist.id = ent.entry_list_id
--rollback WHERE
--rollback     entdata.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.occurrence



-- populate wheat occurrence for CBBW-1
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    experiment.generate_code('occurrence') AS occurrence_code,
    'CBBW-1_OCC1' AS occurrence_name,
    'draft' AS occurrence_status,
    expt.id AS experiment_id,
    geo.id AS site_id,
    3 AS rep_count,
    1 AS occurrence_number,
    person.id AS creator_id
FROM
    experiment.experiment AS expt,
    place.geospatial_object AS geo,
    tenant.person AS person
WHERE
    expt.experiment_name = 'CBBW-1'
    AND geo.geospatial_object_code = 'CO'
    AND person.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name = 'CBBW-1_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in place.geospatial_object



-- populate wheat planting area for CBBW-1
INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id
    )
VALUES
    (
        place.generate_code('geospatial_object'), 'CBBW-1_LOC1', 'planting area', 'beeding_location', '1'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name = 'CBBW-1_LOC1'
--rollback ;


--changeset postgres:populate_wheat_crossing_block_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location



-- populate wheat location for CBBW-1
INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT
    experiment.generate_code('location') AS location_code,
    'CBBW-1_LOC1' AS location_name,
    'committed' AS location_status,
    'planting area' AS location_type,
    2017 AS location_year,
    season.id AS season_id,
    1 AS location_number,
    site.id AS site_id,
    person.id AS steward_id,
    geo.id AS geospatial_object_id,
    person.id AS creator_id
FROM
    tenant.person AS person,
    tenant.season AS season,
    place.geospatial_object AS geo,
    place.geospatial_object AS site
WHERE
    person.username = 'nicola.costa'
    AND season.season_code = 'B'
    AND geo.geospatial_object_name = 'CBBW-1_LOC1'
    AND site.geospatial_object_code = 'CO'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name = 'CBBW-1_LOC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.plot



-- populate wheat plots for CBBW-1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, plot_status, plot_qc_code, creator_id
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_number AS plot_code,
    t.plot_number AS plot_number,
    'plot' AS plot_type,
    t.rep AS rep,
    1 AS design_x,
    t.plot_number AS design_y,
    'active' AS plot_status,
    'G' AS plot_qc_code,
    person.id AS creator_id
FROM
    tenant.person AS person,
    experiment.occurrence AS occ,
    experiment.location AS loc,
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN (
        VALUES
        (1,1,1,'CMSS07B00580T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (2,1,2,'CMSS08Y00057S-099Y-099M-099NJ-13WGY-0B'),
        (3,1,3,'CMSS08Y00278S-099Y-099M-099Y-5M-0WGY'),
        (4,1,4,'CMSS08Y00833T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (5,1,5,'CMSS08Y01088T-099M-099Y-099M-099NJ-5WGY-0B'),
        (6,1,6,'CMSA08Y00378S-050Y-040M-0NJ-2Y-0B'),
        (7,1,7,'CMSS08B00178S-099M-099Y-15M-0RGY'),
        (8,1,8,'CMSS08B00181S-099M-099NJ-099NJ-34WGY-0B'),
        (9,1,9,'CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B'),
        (10,1,10,'CMSS08B00259S-099M-099NJ-30RGY-0B'),
        (11,1,11,'CMSS08B00422S-099M-099NJ-5RGY-0B'),
        (12,1,12,'CMSS08B00633T-099TOPY-099M-099NJ-11WGY-0B'),
        (13,1,13,'CMSS08B00712T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (14,1,14,'CMSS08B00854T-099TOPY-099M-099Y-12M-0WGY'),
        (15,1,15,'CMSS08B00866T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (16,1,16,'CMSS08B00880T-099TOPY-099M-099NJ-099NJ-48WGY-0B'),
        (17,1,17,'CMSS08B00954T-099TOPY-099M-099Y-099B-7WGY-0B'),
        (18,1,18,'CMSS08B00987T-099TOPY-099M-099Y-099B-40WGY-0B'),
        (19,1,19,'CMSS09Y00808T-099TOPM-099Y-099M-099Y-4WGY-0B'),
        (20,1,20,'CMSA09Y00383T-099B-050Y-050ZTM-0NJ-099NJ-11WGY-0B'),
        (21,1,21,'CMSA09M00466S-050ZTM-050Y-3WGY-0B'),
        (22,1,22,'CMSS09B00264S-099M-099Y-10WGY-0B'),
        (23,1,23,'CMSS09B00382S-099ZTM-099NJ-099NJ-18WGY-0B'),
        (24,1,24,'CMSA09M00047T-079(SR26POS)Y-050ZTM-0NJ-099NJ-3WGY-0B'),
        (25,1,25,'CMSS10Y00030S-099Y-099M-11WGY-0B'),
        (26,1,26,'CMSS10Y00359S-099Y-099M-3WGY-0B'),
        (27,1,27,'CMSS10Y00374S-099Y-099M-1WGY-0B'),
        (28,1,28,'CMSS10Y00842T-099TOPM-099Y-099M-1WGY-0B'),
        (29,1,29,'CMSA08WM00143S-050ZTM-050Y-120ZTM-012Y-02B-0RGY'),
        (30,1,30,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-3RGY-0B'),
        (31,1,31,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-13RGY-0B'),
        (32,1,32,'CMSS09Y00882T-099TOPM-099Y-099ZTM-099NJ-099NJ-5RGY-0B'),
        (33,1,33,'CMSS09Y00883T-099TOPM-099Y-099ZTM-099NJ-099NJ-10RGY-0B'),
        (34,1,34,'CMSA09Y00193T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (35,1,35,'CMSA09Y00210T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (36,1,36,'CMSA09M00198T-050Y-050ZTM-0NJ-099NJ-2RGY-0B'),
        (37,1,37,'CMSA11Y00323S-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (38,1,38,'CMSA11Y00364S-099Y-099M-099NJ-099NJ-7WGY-0B'),
        (39,1,39,'CMSA11Y00384S-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (40,1,40,'CMSA11Y00398S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (41,1,41,'CMSA11Y00402S-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (42,1,42,'CMSA11Y00417S-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (43,1,43,'CMSA11Y00449S-099Y-099M-099NJ-099NJ-50WGY-0B'),
        (44,1,44,'CMSA11Y00489S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (45,1,45,'CMSA11Y00507S-099Y-099M-099NJ-099NJ-27WGY-0B'),
        (46,1,46,'CMSS11Y00063S-099Y-099M-0SY-8M-0WGY'),
        (47,1,47,'CMSS11Y00170S-099Y-099M-0SY-11M-0WGY'),
        (48,1,48,'CMSS11Y00172S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (49,1,49,'CMSS11Y00191S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (50,1,50,'CMSS11Y00247S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (51,1,51,'CMSS11Y00302S-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (52,1,52,'CMSS11Y00424S-099Y-099M-099NJ-099NJ-29RGY-0B'),
        (53,1,53,'CMSS11Y00430S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (54,1,54,'CMSS11Y00722T-099TOPM-099Y-099M-099NJ-099NJ-6WGY-0B'),
        (55,1,55,'CMSS11Y00723T-099TOPM-099Y-099M-099NJ-099NJ-16WGY-0B'),
        (56,1,56,'CMSS11Y00775T-099TOPM-099Y-099M-0SY-20M-0WGY'),
        (57,1,57,'CMSS11Y00822T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (58,1,58,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-23WGY-0B'),
        (59,1,59,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (60,1,60,'CMSS11Y00827T-099TOPM-099Y-099M-0SY-11M-0WGY'),
        (61,1,61,'CMSS11Y00831T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (62,1,62,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-55WGY-0B'),
        (63,1,63,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-68WGY-0B'),
        (64,1,64,'CMSS11Y00973T-099TOPM-099Y-099M-0SY-29M-0WGY'),
        (65,1,65,'CMSS11Y00977T-099TOPM-099Y-099M-099NJ-099NJ-22WGY-0B'),
        (66,1,66,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (67,1,67,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (68,1,68,'CMSS11Y01006T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (69,1,69,'CMSS11Y01008T-099TOPM-099Y-099M-0SY-16M-0WGY'),
        (70,1,70,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (71,1,71,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-41WGY-0B'),
        (72,1,72,'CMSS11Y01081T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (73,1,73,'CMSS11Y01091T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (74,1,74,'CMSS11Y01117T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (75,1,75,'CMSS11Y01119T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (76,1,76,'CMSS11Y01138T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (77,1,77,'CMSS11Y01144T-099TOPM-099Y-099M-099NJ-099NJ-38WGY-0B'),
        (78,1,78,'CMSS11Y01152T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (79,1,79,'CMSS11Y01158T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (80,1,80,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (81,1,81,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-14WGY-0B'),
        (82,1,82,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (83,1,83,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-12WGY-0B'),
        (84,1,84,'CMSS11Y01227T-099TOPM-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (85,1,85,'CMSS11B00079S-099M-099NJ-099NJ-15WGY-0B'),
        (86,1,86,'CMSS11B00147S-099M-099NJ-099NJ-24WGY-0B'),
        (87,1,87,'CMSS11B00167S-099M-099NJ-099NJ-5WGY-0B'),
        (88,1,88,'CMSS11B00167S-099M-099NJ-099NJ-25WGY-0B'),
        (89,1,89,'CMSS11B00190S-099M-099NJ-099NJ-50WGY-0B'),
        (90,1,90,'CMSS11B00192S-099M-099NJ-099NJ-27WGY-0B'),
        (91,1,91,'CMSS11B00199S-099M-099NJ-099NJ-16WGY-0B'),
        (92,1,92,'CMSS11B00199S-099M-099NJ-099NJ-19WGY-0B'),
        (93,1,93,'CMSS11B00211S-099M-099NJ-099NJ-4WGY-0B'),
        (94,1,94,'CMSS11B00214S-099M-099NJ-099NJ-12WGY-0B'),
        (95,1,95,'CMSS11B00326S-099M-0SY-5M-0WGY'),
        (96,1,96,'CMSS11B00366S-099M-099NJ-099NJ-12WGY-0B'),
        (97,1,97,'CMSS11B00439S-099M-099NJ-099NJ-11WGY-0B'),
        (98,1,98,'CMSS11B00492S-099M-099NJ-099NJ-11WGY-0B'),
        (99,1,99,'CMSS11B00550S-099M-0SY-36M-0WGY'),
        (100,1,100,'CMSS11B00551S-099M-0SY-16M-0WGY'),
        (101,2,1,'CMSS07B00580T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (102,2,2,'CMSS08Y00057S-099Y-099M-099NJ-13WGY-0B'),
        (103,2,3,'CMSS08Y00278S-099Y-099M-099Y-5M-0WGY'),
        (104,2,4,'CMSS08Y00833T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (105,2,5,'CMSS08Y01088T-099M-099Y-099M-099NJ-5WGY-0B'),
        (106,2,6,'CMSA08Y00378S-050Y-040M-0NJ-2Y-0B'),
        (107,2,7,'CMSS08B00178S-099M-099Y-15M-0RGY'),
        (108,2,8,'CMSS08B00181S-099M-099NJ-099NJ-34WGY-0B'),
        (109,2,9,'CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B'),
        (110,2,10,'CMSS08B00259S-099M-099NJ-30RGY-0B'),
        (111,2,11,'CMSS08B00422S-099M-099NJ-5RGY-0B'),
        (112,2,12,'CMSS08B00633T-099TOPY-099M-099NJ-11WGY-0B'),
        (113,2,13,'CMSS08B00712T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (114,2,14,'CMSS08B00854T-099TOPY-099M-099Y-12M-0WGY'),
        (115,2,15,'CMSS08B00866T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (116,2,16,'CMSS08B00880T-099TOPY-099M-099NJ-099NJ-48WGY-0B'),
        (117,2,17,'CMSS08B00954T-099TOPY-099M-099Y-099B-7WGY-0B'),
        (118,2,18,'CMSS08B00987T-099TOPY-099M-099Y-099B-40WGY-0B'),
        (119,2,19,'CMSS09Y00808T-099TOPM-099Y-099M-099Y-4WGY-0B'),
        (120,2,20,'CMSA09Y00383T-099B-050Y-050ZTM-0NJ-099NJ-11WGY-0B'),
        (121,2,21,'CMSA09M00466S-050ZTM-050Y-3WGY-0B'),
        (122,2,22,'CMSS09B00264S-099M-099Y-10WGY-0B'),
        (123,2,23,'CMSS09B00382S-099ZTM-099NJ-099NJ-18WGY-0B'),
        (124,2,24,'CMSA09M00047T-079(SR26POS)Y-050ZTM-0NJ-099NJ-3WGY-0B'),
        (125,2,25,'CMSS10Y00030S-099Y-099M-11WGY-0B'),
        (126,2,26,'CMSS10Y00359S-099Y-099M-3WGY-0B'),
        (127,2,27,'CMSS10Y00374S-099Y-099M-1WGY-0B'),
        (128,2,28,'CMSS10Y00842T-099TOPM-099Y-099M-1WGY-0B'),
        (129,2,29,'CMSA08WM00143S-050ZTM-050Y-120ZTM-012Y-02B-0RGY'),
        (130,2,30,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-3RGY-0B'),
        (131,2,31,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-13RGY-0B'),
        (132,2,32,'CMSS09Y00882T-099TOPM-099Y-099ZTM-099NJ-099NJ-5RGY-0B'),
        (133,2,33,'CMSS09Y00883T-099TOPM-099Y-099ZTM-099NJ-099NJ-10RGY-0B'),
        (134,2,34,'CMSA09Y00193T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (135,2,35,'CMSA09Y00210T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (136,2,36,'CMSA09M00198T-050Y-050ZTM-0NJ-099NJ-2RGY-0B'),
        (137,2,37,'CMSA11Y00323S-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (138,2,38,'CMSA11Y00364S-099Y-099M-099NJ-099NJ-7WGY-0B'),
        (139,2,39,'CMSA11Y00384S-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (140,2,40,'CMSA11Y00398S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (141,2,41,'CMSA11Y00402S-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (142,2,42,'CMSA11Y00417S-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (143,2,43,'CMSA11Y00449S-099Y-099M-099NJ-099NJ-50WGY-0B'),
        (144,2,44,'CMSA11Y00489S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (145,2,45,'CMSA11Y00507S-099Y-099M-099NJ-099NJ-27WGY-0B'),
        (146,2,46,'CMSS11Y00063S-099Y-099M-0SY-8M-0WGY'),
        (147,2,47,'CMSS11Y00170S-099Y-099M-0SY-11M-0WGY'),
        (148,2,48,'CMSS11Y00172S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (149,2,49,'CMSS11Y00191S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (150,2,50,'CMSS11Y00247S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (151,2,51,'CMSS11Y00302S-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (152,2,52,'CMSS11Y00424S-099Y-099M-099NJ-099NJ-29RGY-0B'),
        (153,2,53,'CMSS11Y00430S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (154,2,54,'CMSS11Y00722T-099TOPM-099Y-099M-099NJ-099NJ-6WGY-0B'),
        (155,2,55,'CMSS11Y00723T-099TOPM-099Y-099M-099NJ-099NJ-16WGY-0B'),
        (156,2,56,'CMSS11Y00775T-099TOPM-099Y-099M-0SY-20M-0WGY'),
        (157,2,57,'CMSS11Y00822T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (158,2,58,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-23WGY-0B'),
        (159,2,59,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (160,2,60,'CMSS11Y00827T-099TOPM-099Y-099M-0SY-11M-0WGY'),
        (161,2,61,'CMSS11Y00831T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (162,2,62,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-55WGY-0B'),
        (163,2,63,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-68WGY-0B'),
        (164,2,64,'CMSS11Y00973T-099TOPM-099Y-099M-0SY-29M-0WGY'),
        (165,2,65,'CMSS11Y00977T-099TOPM-099Y-099M-099NJ-099NJ-22WGY-0B'),
        (166,2,66,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (167,2,67,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (168,2,68,'CMSS11Y01006T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (169,2,69,'CMSS11Y01008T-099TOPM-099Y-099M-0SY-16M-0WGY'),
        (170,2,70,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (171,2,71,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-41WGY-0B'),
        (172,2,72,'CMSS11Y01081T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (173,2,73,'CMSS11Y01091T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (174,2,74,'CMSS11Y01117T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (175,2,75,'CMSS11Y01119T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (176,2,76,'CMSS11Y01138T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (177,2,77,'CMSS11Y01144T-099TOPM-099Y-099M-099NJ-099NJ-38WGY-0B'),
        (178,2,78,'CMSS11Y01152T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (179,2,79,'CMSS11Y01158T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (180,2,80,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (181,2,81,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-14WGY-0B'),
        (182,2,82,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (183,2,83,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-12WGY-0B'),
        (184,2,84,'CMSS11Y01227T-099TOPM-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (185,2,85,'CMSS11B00079S-099M-099NJ-099NJ-15WGY-0B'),
        (186,2,86,'CMSS11B00147S-099M-099NJ-099NJ-24WGY-0B'),
        (187,2,87,'CMSS11B00167S-099M-099NJ-099NJ-5WGY-0B'),
        (188,2,88,'CMSS11B00167S-099M-099NJ-099NJ-25WGY-0B'),
        (189,2,89,'CMSS11B00190S-099M-099NJ-099NJ-50WGY-0B'),
        (190,2,90,'CMSS11B00192S-099M-099NJ-099NJ-27WGY-0B'),
        (191,2,91,'CMSS11B00199S-099M-099NJ-099NJ-16WGY-0B'),
        (192,2,92,'CMSS11B00199S-099M-099NJ-099NJ-19WGY-0B'),
        (193,2,93,'CMSS11B00211S-099M-099NJ-099NJ-4WGY-0B'),
        (194,2,94,'CMSS11B00214S-099M-099NJ-099NJ-12WGY-0B'),
        (195,2,95,'CMSS11B00326S-099M-0SY-5M-0WGY'),
        (196,2,96,'CMSS11B00366S-099M-099NJ-099NJ-12WGY-0B'),
        (197,2,97,'CMSS11B00439S-099M-099NJ-099NJ-11WGY-0B'),
        (198,2,98,'CMSS11B00492S-099M-099NJ-099NJ-11WGY-0B'),
        (199,2,99,'CMSS11B00550S-099M-0SY-36M-0WGY'),
        (200,2,100,'CMSS11B00551S-099M-0SY-16M-0WGY'),
        (201,3,1,'CMSS07B00580T-099TOPY-099M-099NJ-099NJ-10WGY-0B'),
        (202,3,2,'CMSS08Y00057S-099Y-099M-099NJ-13WGY-0B'),
        (203,3,3,'CMSS08Y00278S-099Y-099M-099Y-5M-0WGY'),
        (204,3,4,'CMSS08Y00833T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (205,3,5,'CMSS08Y01088T-099M-099Y-099M-099NJ-5WGY-0B'),
        (206,3,6,'CMSA08Y00378S-050Y-040M-0NJ-2Y-0B'),
        (207,3,7,'CMSS08B00178S-099M-099Y-15M-0RGY'),
        (208,3,8,'CMSS08B00181S-099M-099NJ-099NJ-34WGY-0B'),
        (209,3,9,'CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B'),
        (210,3,10,'CMSS08B00259S-099M-099NJ-30RGY-0B'),
        (211,3,11,'CMSS08B00422S-099M-099NJ-5RGY-0B'),
        (212,3,12,'CMSS08B00633T-099TOPY-099M-099NJ-11WGY-0B'),
        (213,3,13,'CMSS08B00712T-099TOPY-099M-099NJ-099NJ-14RGY-0B'),
        (214,3,14,'CMSS08B00854T-099TOPY-099M-099Y-12M-0WGY'),
        (215,3,15,'CMSS08B00866T-099TOPY-099M-099NJ-099NJ-40WGY-0B'),
        (216,3,16,'CMSS08B00880T-099TOPY-099M-099NJ-099NJ-48WGY-0B'),
        (217,3,17,'CMSS08B00954T-099TOPY-099M-099Y-099B-7WGY-0B'),
        (218,3,18,'CMSS08B00987T-099TOPY-099M-099Y-099B-40WGY-0B'),
        (219,3,19,'CMSS09Y00808T-099TOPM-099Y-099M-099Y-4WGY-0B'),
        (220,3,20,'CMSA09Y00383T-099B-050Y-050ZTM-0NJ-099NJ-11WGY-0B'),
        (221,3,21,'CMSA09M00466S-050ZTM-050Y-3WGY-0B'),
        (222,3,22,'CMSS09B00264S-099M-099Y-10WGY-0B'),
        (223,3,23,'CMSS09B00382S-099ZTM-099NJ-099NJ-18WGY-0B'),
        (224,3,24,'CMSA09M00047T-079(SR26POS)Y-050ZTM-0NJ-099NJ-3WGY-0B'),
        (225,3,25,'CMSS10Y00030S-099Y-099M-11WGY-0B'),
        (226,3,26,'CMSS10Y00359S-099Y-099M-3WGY-0B'),
        (227,3,27,'CMSS10Y00374S-099Y-099M-1WGY-0B'),
        (228,3,28,'CMSS10Y00842T-099TOPM-099Y-099M-1WGY-0B'),
        (229,3,29,'CMSA08WM00143S-050ZTM-050Y-120ZTM-012Y-02B-0RGY'),
        (230,3,30,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-3RGY-0B'),
        (231,3,31,'CMSS09Y00874T-099TOPM-099Y-099ZTM-099NJ-099NJ-13RGY-0B'),
        (232,3,32,'CMSS09Y00882T-099TOPM-099Y-099ZTM-099NJ-099NJ-5RGY-0B'),
        (233,3,33,'CMSS09Y00883T-099TOPM-099Y-099ZTM-099NJ-099NJ-10RGY-0B'),
        (234,3,34,'CMSA09Y00193T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (235,3,35,'CMSA09Y00210T-050M-050Y-050BMX-0NJ-099NJ-1RGY-0B'),
        (236,3,36,'CMSA09M00198T-050Y-050ZTM-0NJ-099NJ-2RGY-0B'),
        (237,3,37,'CMSA11Y00323S-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (238,3,38,'CMSA11Y00364S-099Y-099M-099NJ-099NJ-7WGY-0B'),
        (239,3,39,'CMSA11Y00384S-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (240,3,40,'CMSA11Y00398S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (241,3,41,'CMSA11Y00402S-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (242,3,42,'CMSA11Y00417S-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (243,3,43,'CMSA11Y00449S-099Y-099M-099NJ-099NJ-50WGY-0B'),
        (244,3,44,'CMSA11Y00489S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (245,3,45,'CMSA11Y00507S-099Y-099M-099NJ-099NJ-27WGY-0B'),
        (246,3,46,'CMSS11Y00063S-099Y-099M-0SY-8M-0WGY'),
        (247,3,47,'CMSS11Y00170S-099Y-099M-0SY-11M-0WGY'),
        (248,3,48,'CMSS11Y00172S-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (249,3,49,'CMSS11Y00191S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (250,3,50,'CMSS11Y00247S-099Y-099M-099NJ-099NJ-35WGY-0B'),
        (251,3,51,'CMSS11Y00302S-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (252,3,52,'CMSS11Y00424S-099Y-099M-099NJ-099NJ-29RGY-0B'),
        (253,3,53,'CMSS11Y00430S-099Y-099M-099NJ-099NJ-11WGY-0B'),
        (254,3,54,'CMSS11Y00722T-099TOPM-099Y-099M-099NJ-099NJ-6WGY-0B'),
        (255,3,55,'CMSS11Y00723T-099TOPM-099Y-099M-099NJ-099NJ-16WGY-0B'),
        (256,3,56,'CMSS11Y00775T-099TOPM-099Y-099M-0SY-20M-0WGY'),
        (257,3,57,'CMSS11Y00822T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (258,3,58,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-23WGY-0B'),
        (259,3,59,'CMSS11Y00824T-099TOPM-099Y-099M-099NJ-099NJ-28WGY-0B'),
        (260,3,60,'CMSS11Y00827T-099TOPM-099Y-099M-0SY-11M-0WGY'),
        (261,3,61,'CMSS11Y00831T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (262,3,62,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-55WGY-0B'),
        (263,3,63,'CMSS11Y00892T-099TOPM-099Y-099M-099NJ-099NJ-68WGY-0B'),
        (264,3,64,'CMSS11Y00973T-099TOPM-099Y-099M-0SY-29M-0WGY'),
        (265,3,65,'CMSS11Y00977T-099TOPM-099Y-099M-099NJ-099NJ-22WGY-0B'),
        (266,3,66,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-2WGY-0B'),
        (267,3,67,'CMSS11Y00993T-099TOPM-099Y-099M-099NJ-099NJ-8WGY-0B'),
        (268,3,68,'CMSS11Y01006T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (269,3,69,'CMSS11Y01008T-099TOPM-099Y-099M-0SY-16M-0WGY'),
        (270,3,70,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (271,3,71,'CMSS11Y01036T-099TOPM-099Y-099M-099NJ-099NJ-41WGY-0B'),
        (272,3,72,'CMSS11Y01081T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (273,3,73,'CMSS11Y01091T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (274,3,74,'CMSS11Y01117T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (275,3,75,'CMSS11Y01119T-099TOPM-099Y-099M-099NJ-099NJ-25WGY-0B'),
        (276,3,76,'CMSS11Y01138T-099TOPM-099Y-099M-099NJ-099NJ-18WGY-0B'),
        (277,3,77,'CMSS11Y01144T-099TOPM-099Y-099M-099NJ-099NJ-38WGY-0B'),
        (278,3,78,'CMSS11Y01152T-099TOPM-099Y-099M-099NJ-099NJ-5WGY-0B'),
        (279,3,79,'CMSS11Y01158T-099TOPM-099Y-099M-0SY-4M-0WGY'),
        (280,3,80,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-4WGY-0B'),
        (281,3,81,'CMSS11Y01160T-099TOPM-099Y-099M-099NJ-099NJ-14WGY-0B'),
        (282,3,82,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-10WGY-0B'),
        (283,3,83,'CMSS11Y01216T-099TOPM-099Y-099M-099NJ-099NJ-12WGY-0B'),
        (284,3,84,'CMSS11Y01227T-099TOPM-099Y-099M-099NJ-099NJ-1WGY-0B'),
        (285,3,85,'CMSS11B00079S-099M-099NJ-099NJ-15WGY-0B'),
        (286,3,86,'CMSS11B00147S-099M-099NJ-099NJ-24WGY-0B'),
        (287,3,87,'CMSS11B00167S-099M-099NJ-099NJ-5WGY-0B'),
        (288,3,88,'CMSS11B00167S-099M-099NJ-099NJ-25WGY-0B'),
        (289,3,89,'CMSS11B00190S-099M-099NJ-099NJ-50WGY-0B'),
        (290,3,90,'CMSS11B00192S-099M-099NJ-099NJ-27WGY-0B'),
        (291,3,91,'CMSS11B00199S-099M-099NJ-099NJ-16WGY-0B'),
        (292,3,92,'CMSS11B00199S-099M-099NJ-099NJ-19WGY-0B'),
        (293,3,93,'CMSS11B00211S-099M-099NJ-099NJ-4WGY-0B'),
        (294,3,94,'CMSS11B00214S-099M-099NJ-099NJ-12WGY-0B'),
        (295,3,95,'CMSS11B00326S-099M-0SY-5M-0WGY'),
        (296,3,96,'CMSS11B00366S-099M-099NJ-099NJ-12WGY-0B'),
        (297,3,97,'CMSS11B00439S-099M-099NJ-099NJ-11WGY-0B'),
        (298,3,98,'CMSS11B00492S-099M-099NJ-099NJ-11WGY-0B'),
        (299,3,99,'CMSS11B00550S-099M-0SY-36M-0WGY'),
        (300,3,100,'CMSS11B00551S-099M-0SY-16M-0WGY')
    ) AS t (
        plot_number, rep, entry_number, designation
    )
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
WHERE
    occ.occurrence_name = 'CBBW-1_OCC1'
    AND loc.location_name = 'CBBW-1_LOC1'
    AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
    AND person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-1_OCC1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.planting_instruction



-- populate wheat planting instructions for CBBW-1
INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, package_id, package_log_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    pkg.id AS package_id,
    NULL AS package_log_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
    INNER JOIN experiment.plot AS plot
        ON plot.entry_id = ent.id
    INNER JOIN germplasm.germplasm AS ge
        ON ent.germplasm_id = ge.id
    INNER JOIN germplasm.seed  AS seed
        ON seed.germplasm_id = ge.id
    INNER JOIN germplasm.package AS pkg
        ON pkg.seed_id = seed.id
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'CBBW-1_ENTLIST'
ORDER BY
    plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.package_log context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.package_log



-- populate wheat package logs for CBBW-1
INSERT INTO
   germplasm.package_log (
       package_id, package_quantity, package_unit, package_transaction_type, entity_id, data_id, creator_id
   )
SELECT
    pkg.id AS package_id,
    0 AS package_quantity,
    'g' AS package_unit,
    'withdraw' AS package_transaction_type,
    entity.id AS entity_id,
    ent.id AS data_id,
    person.id AS creator_id
FROM
    experiment.entry AS ent
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'CBBW-1_ENTLIST'
ORDER BY
    ent.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN germplasm.package AS pkg
--rollback         ON ent.seed_id = pkg.seed_id
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback     INNER JOIN dictionary.entity AS entity
--rollback         ON entity.abbrev = 'ENTRY'
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND pkglog.package_transaction_type = 'withdraw'
--rollback     AND pkglog.entity_id = entity.id
--rollback     AND pkglog.data_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_package_log_id_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block package_log_id in experiment.planting_instruction



-- populate wheat package log references for CBBW-1
UPDATE
    experiment.planting_instruction AS plantinst
SET
    package_log_id = pkglog.id
FROM
    germplasm.package_log AS pkglog
    INNER JOIN experiment.entry AS ent
        ON pkglog.data_id = ent.id
    INNER JOIN germplasm.package AS pkg
        ON ent.seed_id = pkg.seed_id
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
    INNER JOIN dictionary.entity AS entity
        ON entity.abbrev = 'ENTRY'
WHERE
    plantinst.entry_id = ent.id
    AND pkglog.package_id = pkg.id
    AND pkglog.package_transaction_type = 'withdraw'
    AND pkglog.entity_id = entity.id
    AND pkglog.data_id = ent.id
    AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plantinst
--rollback SET
--rollback     package_log_id = NULL
--rollback FROM
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;



--liquibase formatted sql

--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross context:fixture  splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross



-- populate wheat crosses for CBBW-1
INSERT INTO
   germplasm.cross (
       cross_name,
       cross_method,
       germplasm_id,
       experiment_id,
       creator_id
   )
SELECT
    (entf.entry_name || '/' || entm.entry_name) AS cross_name,
    'simple cross' AS cross_method,
    NULL AS germplasm_id,
    expt.id AS experiment_id,
    person.id AS creator_id
FROM
    experiment.experiment AS expt
    INNER JOIN experiment.entry_list AS entlist
        ON expt.id = entlist.experiment_id
    INNER JOIN experiment.entry AS entf
        ON entf.entry_list_id = entlist.id
    INNER JOIN experiment.entry AS entm
        ON entm.entry_list_id = entlist.id
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
WHERE
    expt.experiment_name = 'CBBW-1'
    AND entf.entry_number BETWEEN 1 AND 50
    AND entm.entry_number BETWEEN 51 AND 100
ORDER BY
    entf.entry_number,
    entm.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in germplasm.cross_parent



-- populate wheat cross parents for CBBW-1
INSERT INTO
   germplasm.cross_parent (
       cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, creator_id
   )
SELECT
    t.*
FROM (
        SELECT
            crs.id AS cross_id,
            ent.germplasm_id,
            ent.seed_id,
            'female' AS parent_role,
            1 AS order_number,
            expt.id AS experiment_id,
            ent.id AS entry_id,
            person.id AS creator_id
        FROM
            germplasm.CROSS AS crs
            INNER JOIN experiment.experiment AS expt
                ON crs.experiment_id = expt.id
            INNER JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            INNER JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id
            INNER JOIN tenant.person AS person
                ON person.username = 'nicola.costa'
        WHERE
            expt.experiment_name = 'CBBW-1'
            AND ent.entry_number BETWEEN 1 AND 50
            AND crs.cross_name ILIKE ent.entry_name || '/%'
        UNION ALL
            SELECT
                crs.id AS cross_id,
                ent.germplasm_id,
                ent.seed_id,
                'male' AS parent_role,
                2 AS order_number,
                expt.id AS experiment_id,
                ent.id AS entry_id,
                person.id AS creator_id
            FROM
                germplasm.CROSS AS crs
                INNER JOIN experiment.experiment AS expt
                    ON crs.experiment_id = expt.id
                INNER JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                INNER JOIN experiment.entry AS ent
                    ON ent.entry_list_id = entlist.id
                INNER JOIN tenant.person AS person
                    ON person.username = 'nicola.costa'
            WHERE
                expt.experiment_name = 'CBBW-1'
                AND ent.entry_number BETWEEN 51 AND 100
                AND crs.cross_name ILIKE '%/' || ent.entry_name
    ) AS t
ORDER BY
    t.cross_id,
    t.order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     INNER JOIN experiment.experiment AS expt
--rollback         ON crs.experiment_id = expt.id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND expt.experiment_name = 'CBBW-1'
--rollback ;



--changeset postgres:update_experiment_statuses_for_wheat_crossing_block_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update experiment statuses for wheat crossing block experiment



-- update experiment statuses for CBBW-1
UPDATE
    experiment.experiment
SET
    experiment_status = 'planted'
WHERE
    experiment_name = 'CBBW-1'
;

UPDATE
    experiment.entry_list
SET
    entry_list_status = 'created'
WHERE
    entry_list_name = 'CBBW-1_ENTLIST'
;

UPDATE
    experiment.occurrence
SET
    occurrence_status = 'planted'
WHERE
    occurrence_name = 'CBBW-1_OCC1'
;

UPDATE
    experiment.location
SET
    location_status = 'committed'
WHERE
    location_name = 'CBBW-1_LOC1'
;


-- revert changes
--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'entry list created; crosses created; design generated; occurrences created'
--rollback WHERE
--rollback     experiment_name = 'CBBW-1'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_status = 'draft'
--rollback WHERE
--rollback     entry_list_name = 'CBBW-1_ENTLIST'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_status = 'draft'
--rollback WHERE
--rollback     occurrence_name = 'CBBW-1_OCC1'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     experiment.location
--rollback SET
--rollback     location_status = 'committed'
--rollback WHERE
--rollback     location_name = 'CBBW-1_LOC1'
--rollback ;



--changeset postgres:populate_pa_coordinates_for_wheat_crossing_block_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate planting area coordinates for wheat crossing block experiment



-- populate pa coordinates for CBBW-1
UPDATE
    experiment.plot
SET
    pa_x = design_x,
    pa_y = design_y
FROM
    experiment.occurrence AS occ
WHERE
    plot.occurrence_id = occ.id
    AND occ.occurrence_name = 'CBBW-1_OCC1'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot
--rollback SET
--rollback     pa_x = NULL,
--rollback     pa_y = NULL
--rollback FROM
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name = 'CBBW-1_OCC1'
--rollback ;



--changeset postgres:update_cross_method_in_germplasm.cross_for_wheat_crossing_block_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Update cross_method in germplasm.cross for wheat crossing block experiment



-- update cross methods for CBBW-1
UPDATE
    germplasm.cross AS crs
SET
    cross_method = 'SIMPLE CROSS'
FROM
    experiment.experiment AS expt
WHERE
    crs.experiment_id = expt.id
    AND expt.experiment_name = 'CBBW-1'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.cross AS crs
--rollback SET
--rollback     cross_method = 'simple cross'
--rollback FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     crs.experiment_id = expt.id
--rollback     AND expt.experiment_name = 'CBBW-1'
--rollback ;



--changeset postgres:populate_wheat_crossing_block_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-1388 Populate wheat crossing block in experiment.location_occurrence_group



-- populate wheat location occurrence groups for CBBW-1
INSERT INTO
    experiment.location_occurrence_group (
        location_id, occurrence_id, order_number, creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    1 AS order_number,
    prs.id AS creator_id
FROM
    experiment.location AS loc,
    experiment.occurrence AS occ,
    tenant.person AS prs
WHERE
    loc.location_name = 'CBBW-1_LOC1'
    AND occ.occurrence_name = 'CBBW-1_OCC1'
    AND prs.username = 'nicola.costa'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc,
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     logrp.location_id = loc.id
--rollback     AND logrp.occurrence_id = occ.id
--rollback     AND loc.location_name = 'CBBW-1_LOC1'
--rollback     AND occ.occurrence_name = 'CBBW-1_OCC1'
--rollback ;
