--liquibase formatted sql

--changeset postgres:update_usage_check_constraint_in_master.variable context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1591 Update usage check constraint in master.variable



ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application'::character varying::text, 'study'::character varying::text, 'undefined'::character varying::text, 'plot'::character varying::text]));



--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;
--rollback 
--rollback ALTER TABLE master.variable
--rollback     ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application'::character varying::text, 'study'::character varying::text, 'undefined'::character varying::text]));