--liquibase formatted sql

--changeset postgres:drop_not_null_occurrence_id_and_entry_id_in_experiment.plot context:schema splitStatements:false
--comment: EBS-1321 Drop not null occurrence_id and entry_id in experiment.plot



ALTER TABLE
    experiment.plot
ALTER COLUMN
    occurrence_id DROP NOT NULL,
ALTER COLUMN
    entry_id DROP NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.plot
--rollback ALTER COLUMN
--rollback     occurrence_id SET NOT NULL,
--rollback ALTER COLUMN
--rollback     entry_id SET NOT NULL;