--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_in_transaction_id_in_data_terminal.background_process context:schema splitStatements:false
--comment: EBS-1280 Remove not null constraint in transaction_id in data_terminal.background_process



ALTER TABLE
    data_terminal.background_process
ALTER COLUMN
    transaction_id
DROP
    NOT NULL;



--rollback ALTER TABLE data_terminal.background_process
--rollback ALTER COLUMN transaction_id SET NOT NULL;