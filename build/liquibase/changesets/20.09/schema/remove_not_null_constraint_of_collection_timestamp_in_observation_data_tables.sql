--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_of_collection_timestamp_in_observation_data_tables context:schema splitStatements:false
--comment: EBS-1171 Remove not null constraint of collection_timestamp in observation data tables



ALTER TABLE
    experiment.plot_data
ALTER COLUMN
    collection_timestamp DROP NOT NULL;

ALTER TABLE
    experiment.plant_data
ALTER COLUMN
    collection_timestamp DROP NOT NULL;

ALTER TABLE
    experiment.subplot_data
ALTER COLUMN
    collection_timestamp DROP NOT NULL;

ALTER TABLE
    experiment.sample_measurement
ALTER COLUMN
    collection_timestamp DROP NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.plot_data
--rollback ALTER COLUMN
--rollback     collection_timestamp SET NOT NULL;
--rollback ALTER TABLE
--rollback     experiment.plant_data
--rollback ALTER COLUMN
--rollback     collection_timestamp SET NOT NULL;
--rollback ALTER TABLE
--rollback     experiment.subplot_data
--rollback ALTER COLUMN
--rollback     collection_timestamp SET NOT NULL;
--rollback ALTER TABLE
--rollback     experiment.sample_measurement
--rollback ALTER COLUMN
--rollback     collection_timestamp SET NOT NULL;