--liquibase formatted sql

--changeset postgres:temporarily_remove_plot_constraints context:schema splitStatements:false
--comment: EBS-1592 Temporarily remove plot constraints



ALTER TABLE experiment.plot DROP CONSTRAINT plot_location_id_field_x_field_y_unq;

ALTER TABLE experiment.plot DROP CONSTRAINT plot_location_id_pa_x_pa_y_unq;



--rollback ALTER TABLE experiment.plot
--rollback     ADD CONSTRAINT plot_location_id_field_x_field_y_unq UNIQUE (location_id, field_x, field_y);

--rollback COMMENT ON CONSTRAINT plot_location_id_field_x_field_y_unq ON experiment.plot
--rollback     IS 'A plot can be retrieved by its unique location, and field x and field y coordinates.';

--rollback ALTER TABLE experiment.plot
--rollback     ADD CONSTRAINT plot_location_id_pa_x_pa_y_unq UNIQUE (location_id, pa_x, pa_y);
 
--rollback COMMENT ON CONSTRAINT plot_location_id_pa_x_pa_y_unq ON experiment.plot
--rollback     IS 'A plot can be retrieved by its unique location, and planting area x and planting area y coordinates.';