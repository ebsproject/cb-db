--liquibase formatted sql

--changeset postgres:add_column_block_name_to_experiment.experiment_design context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1461 Alter table: experiment.experiment_design + add column block_name



-- add column
ALTER TABLE 
    experiment.experiment_design
ADD COLUMN 
    block_name VARCHAR(64);

COMMENT ON COLUMN experiment.experiment_design.block_name IS 'Experiment Design Block Name: Name of the design block {ColBlock, RowBlock, ...} [EXPTDES_BLKNAME]';



-- revert changes
--rollback ALTER TABLE experiment.experiment_design DROP COLUMN block_name;