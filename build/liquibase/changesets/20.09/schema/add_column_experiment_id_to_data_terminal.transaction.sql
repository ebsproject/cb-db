--liquibase formatted sql

--changeset postgres:add_column_experiment_id_to_data_terminal.transaction context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1280 Add column experiment_id to data_terminal.transaction



ALTER TABLE 
    data_terminal.transaction
ADD COLUMN 
    experiment_id integer,
ADD CONSTRAINT 
    transaction_experiment_id_fk FOREIGN KEY (experiment_id) 
REFERENCES 
    experiment.experiment (id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN 
    data_terminal.transaction.experiment_id
IS 
	'Experiment ID: Reference to the experiment [TRN_EXP_ID]';

CREATE INDEX
	transaction_experiment_id_idx
ON 
	data_terminal.transaction
USING 
	btree (experiment_id);

COMMENT ON INDEX
	data_terminal.transaction_experiment_id_idx
IS
	'A transaction can be retrieved by its experiment id.';



--rollback ALTER TABLE
--rollback     data_terminal.transaction
--rollback DROP COLUMN
--rollback     experiment_id;