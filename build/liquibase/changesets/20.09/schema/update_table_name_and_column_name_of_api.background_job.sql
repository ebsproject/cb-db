--liquibase formatted sql

--changeset postgres:update_table_name_and_column_name_of_api.background_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1425 Update table name and column name of api.background_job table



ALTER TABLE 
    api.background_jobs
RENAME TO
    background_job;

ALTER TABLE
    api.background_job
RENAME COLUMN
    status TO job_status;



--rollback ALTER TABLE
--rollback     api.background_job
--rollback RENAME COLUMN
--rollback     job_status TO status;

--rollback ALTER TABLE 
--rollback     api.background_job
--rollback RENAME TO
--rollback     background_jobs;