--liquibase formatted sql

--changeset postgres:add_column_germplasm_type_to_germplasm.germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1385 Add column germplasm_type to germplasm.germplasm table



-- add column germplasm_type
ALTER TABLE
    germplasm.germplasm
ADD COLUMN
    germplasm_type varchar(32)
;

COMMENT ON COLUMN
    germplasm.germplasm.germplasm_type
IS
    'Germplasm Type: Type of germplasm {progeny, fixed_line, accession, hybrid} [GERM_TYPE]'
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback DROP COLUMN
--rollback     germplasm_type
--rollback ;
