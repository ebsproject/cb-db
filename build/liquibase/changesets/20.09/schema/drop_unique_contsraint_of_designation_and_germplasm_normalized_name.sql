--liquibase formatted sql

--changeset postgres:drop_unique_contsraint_of_designation_and_germplasm_normalized_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1463 Drop unique constraint of designation and germplasm_normalized_name in germplasm.germplasm



-- alter table
ALTER TABLE
	germplasm.germplasm
DROP CONSTRAINT
    germplasm_designation_idx;

ALTER TABLE
	germplasm.germplasm
DROP CONSTRAINT
    germplasm_germplasm_normalized_name_idx;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm ADD CONSTRAINT germplasm_designation_idx UNIQUE (designation);
--rollback COMMENT ON CONSTRAINT germplasm_designation_idx ON germplasm.germplasm IS 'A germplasm can be retrieved by its unique designation.';
--rollback ALTER TABLE germplasm.germplasm ADD CONSTRAINT germplasm_germplasm_normalized_name_idx UNIQUE (germplasm_normalized_name);
--rollback COMMENT ON CONSTRAINT germplasm_germplasm_normalized_name_idx ON germplasm.germplasm IS 'A germplasm can be retrieved by its unique germplasm normalized name.';