--liquibase formatted sql

--changeset postgres:drop_experiment_type_check_constraint_in_experiment.experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1487 Drop experiment_type check constraint in experiment.experiment



ALTER TABLE 
    experiment.experiment 
DROP CONSTRAINT 
    experiment_experiment_type_chk;



--rollback ALTER TABLE 
--rollback     experiment.experiment
--rollback ADD CONSTRAINT 
--rollback     experiment_experiment_type_chk 
--rollback CHECK 
--rollback     (experiment_type::text = ANY (ARRAY['Trial'::text, 'Nursery'::text]));