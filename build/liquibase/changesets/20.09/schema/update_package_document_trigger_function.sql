--liquibase formatted sql

--changeset postgres:update_package_document_trigger_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1423 Update package document trigger function



-- update function
CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        -- generate document
        SELECT  
            concat(
                setweight(to_tsvector(new.package_code),'A'),' ',
                setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
                setweight(to_tsvector(unaccent(gm.designation)),'A'),' ',
                setweight(to_tsvector(gs.seed_code),'B'),' ',
                setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
                setweight(to_tsvector(trunc(new.package_quantity::numeric)::text),'C'),' ',
                setweight(to_tsvector(trunc(new.package_quantity::numeric,1)::text),'C'),' ',
                setweight(to_tsvector(trunc(new.package_quantity::numeric,2)::text),'C'),' ',
                setweight(to_tsvector(trunc(new.package_quantity::numeric,3)::text),'C'),' ',
                setweight(to_tsvector(new.package_unit),'C'),' ',
                setweight(to_tsvector(new.package_status),'C'),' ',
                setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
                setweight(to_tsvector(gs.harvest_method),'CD')
            ) into var_document
        FROM
            germplasm.seed gs
            INNER JOIN germplasm.germplasm gm
                ON gs.germplasm_id = gm.id
        WHERE
            gs.id = new.seed_id
            AND gs.is_void = FALSE
            AND gm.is_void = FALSE;
        
        new.package_document = var_document;
    END IF;
    
    RETURN NEW;
END;
$function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package()
--rollback  RETURNS trigger
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback             SELECT    
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.package_code),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(gm.designation)),'A'),' ',
--rollback                     setweight(to_tsvector(gs.seed_code),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,1)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,2)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,3)::text),'C'),' ',
--rollback                     setweight(to_tsvector(new.package_unit),'C'),' ',
--rollback                     setweight(to_tsvector(new.package_status),'C'),' ',
--rollback                     setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
--rollback                     setweight(to_tsvector(gs.harvest_method),'CD')
--rollback                 ) INTO var_document
--rollback             FROM 
--rollback                 germplasm.package gp
--rollback             INNER JOIN
--rollback                 germplasm.seed gs
--rollback             ON
--rollback                 new.seed_id = gs.id
--rollback             INNER JOIN
--rollback                 germplasm.germplasm gm
--rollback             ON
--rollback                 gs.germplasm_id = gm.id
--rollback             WHERE
--rollback                 gp.id = new.id;
--rollback                 
--rollback         ELSE
--rollback             
--rollback             SELECT    
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.package_code),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(gm.designation)),'A'),' ',
--rollback                     setweight(to_tsvector(gs.seed_code),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,1)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,2)::text),'C'),' ',
--rollback                     setweight(to_tsvector(trunc(gp.package_quantity::numeric,3)::text),'C'),' ',
--rollback                     setweight(to_tsvector(new.package_unit),'C'),' ',
--rollback                     setweight(to_tsvector(new.package_status),'C'),' ',
--rollback                     setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
--rollback                     setweight(to_tsvector(gs.harvest_method),'CD')
--rollback                 ) INTO var_document
--rollback             FROM 
--rollback                 germplasm.package gp
--rollback             INNER JOIN
--rollback                 germplasm.seed gs
--rollback             ON
--rollback                 new.seed_id = gs.id
--rollback             INNER JOIN
--rollback                 germplasm.germplasm gm
--rollback             ON
--rollback                 gs.germplasm_id = gm.id;
--rollback             
--rollback         END IF;
--rollback 
--rollback         new.package_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $function$
--rollback ;
