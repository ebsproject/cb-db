--liquibase formatted sql

--changeset postgres:update_germplasm_document_trigger_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1423 Update germplasm document trigger function



-- update function
CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        -- generate document
        SELECT
            concat(
                setweight(to_tsvector(unaccent(gn.germplasm_normalized_name)),'A'),' ',
                setweight(to_tsvector(unaccent(gn.name_value)),'A'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
                setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
                setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
                setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_type)),'C')
            ) into var_document
        FROM (
                SELECT
                    regexp_replace((array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name)::varchar),'[^a-zA-Y0-9-]',' ','g'),
                    regexp_replace((array_agg(gn.name_value order by gn.name_value)::varchar),'[^a-zA-Y0-9-]',' ','g')
                FROM
                    germplasm.germplasm_name gn
                WHERE
                    gn.germplasm_id = new.id
                    AND gn.is_void = FALSE
            ) AS gn (
                germplasm_normalized_name,
                name_value
            )
        ;
        
        new.germplasm_document = var_document;
    END IF;
    
    RETURN NEW;
END;
$function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
--rollback  RETURNS trigger
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback             SELECT
--rollback                 concat(
--rollback                     
--rollback                     setweight(to_tsvector(unaccent(regexp_replace((array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name)::varchar),'[^a-zA-Y0-9-]',' ','g'))),'A'),' ',
--rollback                     
--rollback                     setweight(to_tsvector(unaccent(regexp_replace((array_agg(gn.name_value order by gn.name_value)::varchar),'[^a-zA-Y0-9-]',' ','g'))),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C')
--rollback                 ) INTO var_document
--rollback             FROM
--rollback                 germplasm.germplasm gm
--rollback             INNER JOIN
--rollback                 germplasm.germplasm_name gn
--rollback             ON
--rollback                 gn.germplasm_id = new.id
--rollback             WHERE
--rollback                 gm.id = new.id
--rollback             GROUP by
--rollback                 gn.germplasm_id,
--rollback                 gm.designation,
--rollback                 gm.parentage,
--rollback                 gm.generation,
--rollback                 gm.germplasm_state,
--rollback                 gm.germplasm_name_type,
--rollback                 gm.germplasm_normalized_name,
--rollback                 new.id;
--rollback 
--rollback         ELSE
--rollback             
--rollback             SELECT
--rollback                 concat(
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C')
--rollback                 ) INTO var_document
--rollback             FROM
--rollback                 germplasm.germplasm gm;
--rollback             
--rollback         END IF;
--rollback 
--rollback         new.germplasm_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $function$
--rollback ;
