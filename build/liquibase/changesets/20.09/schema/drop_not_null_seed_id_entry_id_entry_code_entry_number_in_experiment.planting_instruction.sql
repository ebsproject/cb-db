--liquibase formatted sql

--changeset postgres:drop_not_null_seed_id_entry_id_entry_code_entry_number_in_experiment.planting_instruction context:schema splitStatements:false
--comment: EBS-1384 Drop not null seed_id, entry_id, entry_code and entry_number in experiment.planting_instruction



ALTER TABLE
    experiment.planting_instruction
ALTER COLUMN
    seed_id DROP NOT NULL,
ALTER COLUMN
    entry_id DROP NOT NULL,
ALTER COLUMN
    entry_code DROP NOT NULL,
ALTER COLUMN
    entry_number DROP NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.planting_instruction
--rollback ALTER COLUMN
--rollback     seed_id SET NOT NULL,
--rollback ALTER COLUMN
--rollback     entry_id SET NOT NULL,
--rollback ALTER COLUMN
--rollback     entry_code SET NOT NULL,
--rollback ALTER COLUMN
--rollback     entry_number SET NOT NULL;