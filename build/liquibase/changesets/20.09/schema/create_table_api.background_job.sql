--liquibase formatted sql

--changeset postgres:create_table_api.background_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1425 Create table api.background_job



CREATE TABLE api.background_jobs
(
    id serial,
    worker_name character varying COLLATE pg_catalog."default",
    description character varying COLLATE pg_catalog."default",
    status character varying(20) COLLATE pg_catalog."default",
    message text COLLATE pg_catalog."default",
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    creator_id integer,
    creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
    modifier_id integer,
    modification_timestamp timestamp without time zone,
    is_void boolean NOT NULL DEFAULT false,
    remarks character varying COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    event_log jsonb,
    CONSTRAINT background_jobs_id_pk PRIMARY KEY (id),
    CONSTRAINT background_jobs_creator_id_fkey FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT background_jobs_modifier_id_fkey FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE INDEX background_jobs_creator_id_idx ON api.background_jobs USING btree (creator_id);

CREATE INDEX background_jobs_void_idx ON api.background_jobs USING btree (is_void);

CREATE INDEX background_jobs_modifier_id_idx ON api.background_jobs USING btree (modifier_id);

COMMENT ON COLUMN api.background_jobs.id IS 'Identifier of the record within the table';

COMMENT ON COLUMN api.background_jobs.worker_name IS 'Name of the worker of a particular background job';

COMMENT ON COLUMN api.background_jobs.description IS 'Description of a particular background job';

COMMENT ON COLUMN api.background_jobs.status IS 'Current status of a particular background job';

COMMENT ON COLUMN api.background_jobs.message IS 'Status message of a particular background job';

COMMENT ON COLUMN api.background_jobs.start_time IS 'Time at which a background job is supposed to begin';

COMMENT ON COLUMN api.background_jobs.end_time IS 'Time at which a backtoud job is supposed to end';

COMMENT ON COLUMN api.background_jobs.creation_timestamp IS 'Timestamp when the record was added to the table';

COMMENT ON COLUMN api.background_jobs.creator_id IS 'ID of the user who added the record to the table';

COMMENT ON COLUMN api.background_jobs.modification_timestamp IS 'Timestamp when the record was last modified';

COMMENT ON COLUMN api.background_jobs.modifier_id IS 'ID of the user who last modified the record';

COMMENT ON COLUMN api.background_jobs.notes IS 'Additional details added by an admin; can be technical or advanced details';

COMMENT ON COLUMN api.background_jobs.is_void IS 'Indicator whether the record is deleted (true) or not (false)';

COMMENT ON COLUMN api.background_jobs.event_log IS 'Historical transactions of the record';



--rollback DROP TABLE api.background_jobs;