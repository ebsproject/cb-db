--liquibase formatted sql

--changeset postgres:drop_harvest_status_chk_constraint_in_experiment.plot context:schema splitStatements:false
--comment: EBS-1488 Drop harvest status check constraint in experiment.plot



ALTER TABLE experiment.plot DROP CONSTRAINT plot_harvest_status_chk;



--rollback ALTER TABLE experiment.plot
--rollback     ADD CONSTRAINT plot_harvest_status_chk CHECK (harvest_status::text = ANY (ARRAY['NO_HARVEST'::text, 'READY'::text, 'PROCESSED'::text, 'COMPLETED'::text]));