--liquibase formatted sql

--changeset postgres:add_column_is_seen_in_api.background_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1561 Add column is_seen in api.background_job



ALTER TABLE api.background_job
    ADD COLUMN is_seen boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN api.background_job.is_seen
    IS 'Indicates whether the record is seen by the user';



--rollback ALTER TABLE api.background_job DROP COLUMN is_seen;