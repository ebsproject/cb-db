--liquibase formatted sql

--changeset postgres:create_indexes_for_seed_id_in_experiment.entry_and_germplasm.package context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1620 Create indexes for seed_id columns in experiment.entry and germplasm.package



CREATE INDEX package_seed_id_idx ON germplasm.package (seed_id);

COMMENT ON INDEX germplasm.package_seed_id_idx IS 'An package can be retrieved by its seed.';


CREATE INDEX entry_seed_id_idx ON experiment.entry (seed_id);

COMMENT ON INDEX experiment.entry_seed_id_idx IS 'An entry can be retrieved by its seed.';


-- revert changes
--rollback DROP INDEX germplasm.package_seed_id_idx;
--rollback DROP INDEX experiment.entry_seed_id_idx;
