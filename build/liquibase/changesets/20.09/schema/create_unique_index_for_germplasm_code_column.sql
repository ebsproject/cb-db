--liquibase formatted sql

--changeset postgres:create_unique_index_for_germplasm_code_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1324 Create unique index for germplasm_code



-- create germplasm_code unique index
CREATE UNIQUE INDEX
    germplasm_germplasm_code_unq
ON
    germplasm.germplasm
USING
    btree (germplasm_code)
;



-- revert changes
--rollback DROP INDEX germplasm.germplasm_germplasm_code_unq;
