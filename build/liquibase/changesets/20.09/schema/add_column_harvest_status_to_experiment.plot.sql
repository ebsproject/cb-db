--liquibase formatted sql

--changeset postgres:add_column_harvest_status_to_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1488 Add column harvest_status to experiment.plot



ALTER TABLE 
	experiment.plot 
ADD COLUMN
	harvest_status character varying NOT NULL DEFAULT 'NO_HARVEST',
ADD CONSTRAINT 
    plot_harvest_status_chk
CHECK 
    (harvest_status::text = ANY (ARRAY['NO_HARVEST'::text, 'INCOMPLETE'::text, 'READY'::text, 'PROCESSED'::text, 'COMPLETED'::text]));
	
COMMENT ON COLUMN 
    experiment.plot.harvest_status 
IS 'Harvest status will be used to check if plot has complete set of traits to enable seed creation';



--rollback ALTER TABLE experiment.plot 
--rollback DROP COLUMN harvest_status;