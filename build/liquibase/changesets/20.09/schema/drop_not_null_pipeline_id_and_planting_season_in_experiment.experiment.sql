--liquibase formatted sql

--changeset postgres:drop_not_null_pipeline_id_and_planting_season_in_experiment.experiment context:schema splitStatements:false
--comment: EBS-1323 Drop not null pipeline_id and planting_season in experiment.experiment



ALTER TABLE
    experiment.experiment
ALTER COLUMN
    pipeline_id DROP NOT NULL,
ALTER COLUMN
    planting_season DROP NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.experiment
--rollback ALTER COLUMN
--rollback     pipeline_id SET NOT NULL,
--rollback ALTER COLUMN
--rollback     planting_season SET NOT NULL;