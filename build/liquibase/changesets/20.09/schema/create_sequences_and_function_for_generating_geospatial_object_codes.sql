--liquibase formatted sql

--changeset postgres:create_geospatial_object_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Create geospatial_object_code sequence



-- create geospatial_object_code sequence
CREATE SEQUENCE place.geospatial_object_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE place.geospatial_object_code_seq;



--changeset postgres:create_facility_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Create facility_code sequence



-- create facility_code sequence
CREATE SEQUENCE place.facility_code_seq
    START 1
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE place.facility_code_seq;



--changeset postgres:create_geospatial_object_code_generator_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1387 Create function to generate geospatial object codes



-- create geospatial_object_code generator
CREATE OR REPLACE FUNCTION place.generate_code(IN entity varchar, IN custom_value varchar DEFAULT NULL, OUT code varchar)
    RETURNS varchar
    LANGUAGE plpgsql
AS $$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('place.' || lower(entity) || '_code_seq') INTO seq_id;
    
    IF (upper(entity) = 'GEOSPATIAL_OBJECT') THEN
        entity_code := 'GEO';
    ELSIF (upper(entity) = 'FACILITY') THEN
        entity_code := 'FAC';
    END IF;
    
    IF (entity_code IS NOT NULL) THEN
        IF (custom_value IS NULL) THEN
            code := entity_code || lpad(seq_id::varchar, 12, '0');
        ELSE
            code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
        END IF;
    ELSE
        code := NULL;
    END IF;
END; $$
;



-- revert changes
--rollback DROP FUNCTION place.generate_code(IN varchar, IN varchar, OUT varchar);
