--liquibase formatted sql

--changeset postgres:update_harvest_status_check_constraint_in_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1488 Update harvest status check constraint in experiment.plot



ALTER TABLE experiment.plot DROP CONSTRAINT plot_harvest_status_chk;

ALTER TABLE experiment.plot ADD CONSTRAINT plot_harvest_status_chk CHECK (((harvest_status)::text = ANY (ARRAY['NO_HARVEST'::text, 'READY'::text, 'PROCESSED'::text, 'COMPLETED'::text])))



--rollback ALTER TABLE experiment.plot DROP CONSTRAINT plot_harvest_status_chk;

--rollback ALTER TABLE experiment.plot ADD CONSTRAINT plot_harvest_status_chk CHECK (harvest_status::text = ANY (ARRAY['NO_HARVEST'::text, 'INCOMPLETE'::text, 'READY'::text, 'PROCESSED'::text, 'COMPLETED'::text]));