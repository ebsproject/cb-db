--liquibase formatted sql

--changeset postgres:create_germplasm_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-1324 Create sequence for germplasm_code



-- create sequence for germplasm.seed.seed_code
CREATE SEQUENCE germplasm.germplasm_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE germplasm.germplasm_code_seq;



--changeset postgres:update_code_generator_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-982 Update function for generating germplasm-related codes



-- drop old function
DROP FUNCTION germplasm.generate_code(varchar);


-- create updated function to generate code
CREATE OR REPLACE FUNCTION germplasm.generate_code(IN entity varchar, IN custom_value varchar DEFAULT NULL, OUT code varchar)
    RETURNS varchar
    LANGUAGE plpgsql
AS $$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
    
    IF (entity = 'germplasm') THEN
        entity_code = 'GE';
    ELSIF (entity = 'seed') THEN
        entity_code = 'SEED';
    ELSIF (entity = 'package') THEN
        entity_code = 'PKG';
    END IF;
    
    IF (custom_value IS NULL) THEN
        code := entity_code || lpad(seq_id::varchar, 12, '0');
    ELSE
        code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
    END IF;
END; $$
;



-- revert changes
--rollback DROP FUNCTION germplasm.generate_code(varchar, varchar);
--rollback 
--rollback CREATE OR REPLACE FUNCTION germplasm.generate_code(IN entity varchar, OUT code varchar)
--rollback     RETURNS varchar
--rollback     LANGUAGE plpgsql
--rollback AS $$
--rollback DECLARE
--rollback     seq_id bigint;
--rollback BEGIN
--rollback     SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
--rollback     
--rollback     code := upper(entity) || lpad(seq_id::varchar, 16, '0');
--rollback END; $$
--rollback ;
