--liquibase formatted sql

--changeset postgres:add_experiment_statuses_for_crosses_and_protocols_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-292 Add experiment_statuses for crosses and protocols



INSERT INTO
    master.scale_value (scale_id,value,order_number,description,display_name,creator_id,abbrev)
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS'),
        'Crosses Created',
        '3',
        'Crosses Created',
        'Crosses Created',
        '1',
        'EXPERIMENT_STATUS_CROSSES_CREATED'
    );

INSERT INTO
    master.scale_value (scale_id,value,order_number,description,display_name,creator_id,abbrev)
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS'),
        'Protocols Specified',
        '4',
        'Protocols Specified',
        'Protocols Specified',
        '1',
        'EXPERIMENT_STATUS_PROTOCOLS_SPECIFIED'
    );

UPDATE 
    master.scale_value
SET
    order_number = 5
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Design Generated';

UPDATE 
    master.scale_value
SET
    order_number = 6
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Experiment Group Specified';

UPDATE 
    master.scale_value
SET
    order_number = 7
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Location Rep Studies Created';

UPDATE 
    master.scale_value
SET
    order_number = 8
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Completed';

UPDATE 
    master.scale_value
SET
    order_number = 9
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Active';

UPDATE 
    master.scale_value
SET
    order_number = 10
WHERE 
    scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') 
AND 
    value = 'Created';



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Crosses Created';
--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Protocols Specified';
--rollback UPDATE master.scale_value SET order_number = 3 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Design Generated';
--rollback UPDATE master.scale_value SET order_number = 4 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Experiment Group Specified';
--rollback UPDATE master.scale_value SET order_number = 5 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Location Rep Studies Created';
--rollback UPDATE master.scale_value SET order_number = 6 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Completed';
--rollback UPDATE master.scale_value SET order_number = 7 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Active';
--rollback UPDATE master.scale_value SET order_number = 8 WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_STATUS') AND value = 'Created';