--liquibase formatted sql

--changeset postgres:insert_api_access_token context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-297 Insert API access token



-- insert record
INSERT INTO api.client (id, client_id, client_secret, redirect_uri, creator_id)
VALUES (2, '461','461','https://www.google.com', 1);



--rollback DELETE FROM api.client WHERE id = 2 and client_id = '461';
