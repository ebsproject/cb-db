--liquibase formatted sql

--changeset postgres:insert_traits_query_field_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Insert TRAITS_QUERY_FIELD to platform.config
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM platform.config WHERE abbrev = 'TRAITS_QUERY_FIELDS'



INSERT INTO
    platform.config (abbrev, name, config_value, rank, usage)
VALUES (
    'TRAITS_QUERY_FIELDS',
    'Query fields for trait variable',
    '{
        "name": "Traits Query Fields",
        "fields": {
            "basic": [
                {
                    "label": "Trait Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "type",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "1",
                    "advanced_search": "false",
                    "variable_abbrev": "TRAIT",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "type",
                    "api_resource_method": "POST",
                    "api_resource_output": "type",
                    "api_resource_endpoint": "variables-search?sort=type",
                    "api_resource_body": {"fields":"variable.type","distinctOn":"type","type":"metadata|observation"}
                }
            ],
            "additional": [
                {
                    "label": "Data Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataType",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "2",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_TYPE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataType",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_type",
                    "api_resource_endpoint": "variables-search?sort=data_type",
                    "api_resource_body": {"fields":"variable.data_type","distinctOn":"data_type","data_type":"not ilike %json%"}
                },
                {
                    "label": "Data Level",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataLevel",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data level of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "3",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_LEVEL",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataLevel",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_level",
                    "api_resource_endpoint": "variables-search?sort=data_level",
                    "api_resource_body": {"fields":"variable.data_level","distinctOn":"data_level"}
                },
                {
                    "label": "Usage",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "usage",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The usage of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "4",
                    "advanced_search": "false",
                    "variable_abbrev": "USAGE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "usage",
                    "api_resource_method": "POST",
                    "api_resource_output": "usage",
                    "api_resource_endpoint": "variables-search?sort=usage",
                    "api_resource_body": {"fields":"variable.usage","distinctOn":"usage","usage":"not ilike %application%"}
                }
            ]
        },
        "main_table": "master.variable",
        "search_sort_order": "ASC",
        "search_sort_column": "id",
        "api_resource_method": "POST",
        "api_resource_endpoint": "variables-search",
        "main_table_primary_key": "variableDbId"
    }',
    1,
    'traits_browser'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'TRAITS_QUERY_FIELDS';