--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_cross_post_planning_entry_list_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update entry list template EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for cross post-planning data process",
            "Values": [{
                "disabled": false,
                "variable_abbrev": "DESCRIPTION",
                "api_resource_endpoint": "entries"
            }]
        }
    ' 
WHERE abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for cross post-planning data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION"
--rollback                 }
--rollback             ]
--rollback         }   
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';