--liquibase formatted sql

--changeset postgres:add_package_to_dictionary.table context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-142 Add package to dictionary.table
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM dictionary.table WHERE abbrev = 'PACKAGE'



INSERT INTO 
    dictionary.table (database_id, schema_id, abbrev, name, comment, creator_id)
VALUES 
    (
        (SELECT id FROM dictionary.database WHERE abbrev = 'BIMS_0.10_PROD_OLD_VERSION'), 
        (SELECT id FROM dictionary.schema WHERE abbrev = 'GERMPLASM'),
        'PACKAGE',
        'package',
        'Package',
         1
    );



--rollback DELETE FROM dictionary.table WHERE abbrev = 'PACKAGE';