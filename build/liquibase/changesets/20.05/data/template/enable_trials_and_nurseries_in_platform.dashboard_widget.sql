--liquibase formatted sql

--changeset postgres:enable_trials_and_nurseries_in_platform.dashboard_widget context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-328 Enable TRIALS and NURSERIES in platform.dashboard_widget



UPDATE 
    platform.dashboard_widget
SET 
    is_void = false
WHERE 
    abbrev 
IN
    ('TRIALS', 'NURSERIES');



--rollback UPDATE 
--rollback     platform.dashboard_widget
--rollback SET 
--rollback     is_void = true
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     ('TRIALS', 'NURSERIES');