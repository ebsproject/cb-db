--liquibase formatted sql

--changeset postgres:update_traits_query_fields_config_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update TRAITS_QUERY_FIELDS config in platform.config p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "name": "Traits Query Fields",
            "fields": {
                "basic": [
                    {
                        "label": "Name",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "name",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The name of the trait",
                        "input_field": "input field",
                        "primary_key": "",
                        "order_number": "1",
                        "advanced_search": "false",
                        "variable_abbrev": "NAME",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "name",
                        "api_resource_method": "POST",
                        "api_resource_output": "name",
                        "api_resource_endpoint": "variables-search?sort=name",
                        "api_resource_body": {"fields":"variable.name","distinctOn":"name"}
                    },
                    {
                        "label": "Data Type",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "dataType",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The data type of the trait",
                        "input_field": "selection",
                        "primary_key": "",
                        "order_number": "2",
                        "advanced_search": "false",
                        "variable_abbrev": "DATA_TYPE",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "dataType",
                        "api_resource_method": "POST",
                        "api_resource_output": "data_type",
                        "api_resource_endpoint": "variables-search?sort=data_type",
                        "api_resource_body": {"fields":"variable.data_type|variable.type","distinctOn":"data_type","data_type":"not ilike %json%","type":"observation"}
                    }
                ],
                "additional": [
                    {
                        "label": "Abbrev",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "abbrev",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The abbrev of the trait",
                        "input_field": "input field",
                        "primary_key": "",
                        "order_number": "3",
                        "advanced_search": "false",
                        "variable_abbrev": "ABBREV",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "abbrev",
                        "api_resource_method": "POST",
                        "api_resource_output": "abbrev",
                        "api_resource_endpoint": "variables-search?sort=abbrev",
                        "api_resource_body": {"fields":"variable.abbrev","distinctOn":"abbrev"}
                    },
                    {
                        "label": "Label",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "label",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The label of the trait",
                        "input_field": "input field",
                        "primary_key": "",
                        "order_number": "4",
                        "advanced_search": "false",
                        "variable_abbrev": "LABEL",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "label",
                        "api_resource_method": "POST",
                        "api_resource_output": "label",
                        "api_resource_endpoint": "variables-search?sort=label",
                        "api_resource_body": {"fields":"variable.label","distinctOn":"label"}
                    },
                    {
                        "label": "Data Level",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "dataLevel",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The data level of the trait",
                        "input_field": "selection",
                        "primary_key": "",
                        "order_number": "6",
                        "advanced_search": "false",
                        "variable_abbrev": "DATA_LEVEL",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "dataLevel",
                        "api_resource_method": "POST",
                        "api_resource_output": "data_level",
                        "api_resource_endpoint": "variables-search?sort=data_level",
                        "api_resource_body": {"fields":"variable.data_level|variable.type","distinctOn":"data_level","type":"observation"}
                    },
                    {
                        "label": "Usage",
                        "disabled": "false",
                        "required": "false",
                        "fieldName": "usage",
                        "list_type": "",
                        "input_type": "multiple",
                        "description": "The usage of the trait",
                        "input_field": "selection",
                        "primary_key": "",
                        "order_number": "7",
                        "advanced_search": "false",
                        "variable_abbrev": "USAGE",
                        "allow_filter_list": "false",
                        "list_filter_field": "",
                        "search_sort_order": "ASC",
                        "search_sort_column": "usage",
                        "api_resource_method": "POST",
                        "api_resource_output": "usage",
                        "api_resource_endpoint": "variables-search?sort=usage",
                        "api_resource_body": {"fields":"variable.usage|variable.type","distinctOn":"usage","usage":"not ilike %application%","type":"observation"}
                    }
                ]
            },
            "main_table": "master.variable",
            "search_sort_order": "ASC",
            "search_sort_column": "id",
            "api_resource_method": "POST",
            "api_resource_endpoint": "variables-search",
            "main_table_primary_key": "variableDbId"
        }
    '
WHERE abbrev = 'TRAITS_QUERY_FIELDS';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "Traits Query Fields",
--rollback             "fields": {
--rollback                 "basic": [
--rollback                     {
--rollback                         "label": "Name",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "name",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The name of the trait",
--rollback                         "input_field": "input field",
--rollback                         "primary_key": "",
--rollback                         "order_number": "1",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "NAME",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "fields": "variable.name",
--rollback                             "distinctOn": "name"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "name",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "name",
--rollback                         "api_resource_endpoint": "variables-search?sort=name"
--rollback                     },
--rollback                     {
--rollback                         "label": "Data Type",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "dataType",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The data type of the trait",
--rollback                         "input_field": "selection",
--rollback                         "primary_key": "",
--rollback                         "order_number": "2",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "DATA_TYPE",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "fields": "variable.data_type",
--rollback                             "data_type": "not ilike %json%",
--rollback                             "distinctOn": "data_type"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "dataType",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "data_type",
--rollback                         "api_resource_endpoint": "variables-search?sort=data_type"
--rollback                     }
--rollback                 ],
--rollback                 "additional": [
--rollback                     {
--rollback                         "label": "Abbrev",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "abbrev",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The abbrev of the trait",
--rollback                         "input_field": "input field",
--rollback                         "primary_key": "",
--rollback                         "order_number": "3",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "ABBREV",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "fields": "variable.abbrev",
--rollback                             "distinctOn": "abbrev"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "abbrev",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "abbrev",
--rollback                         "api_resource_endpoint": "variables-search?sort=abbrev"
--rollback                     },
--rollback                     {
--rollback                         "label": "Label",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "label",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The label of the trait",
--rollback                         "input_field": "input field",
--rollback                         "primary_key": "",
--rollback                         "order_number": "4",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "LABEL",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "fields": "variable.label",
--rollback                             "distinctOn": "label"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "label",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "label",
--rollback                         "api_resource_endpoint": "variables-search?sort=label"
--rollback                     },
--rollback                     {
--rollback                         "label": "Data Level",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "dataLevel",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The data level of the trait",
--rollback                         "input_field": "selection",
--rollback                         "primary_key": "",
--rollback                         "order_number": "6",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "DATA_LEVEL",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "fields": "variable.data_level",
--rollback                             "distinctOn": "data_level"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "dataLevel",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "data_level",
--rollback                         "api_resource_endpoint": "variables-search?sort=data_level"
--rollback                     },
--rollback                     {
--rollback                         "label": "Usage",
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "fieldName": "usage",
--rollback                         "list_type": "",
--rollback                         "input_type": "multiple",
--rollback                         "description": "The usage of the trait",
--rollback                         "input_field": "selection",
--rollback                         "primary_key": "",
--rollback                         "order_number": "7",
--rollback                         "advanced_search": "false",
--rollback                         "variable_abbrev": "USAGE",
--rollback                         "allow_filter_list": "false",
--rollback                         "api_resource_body": {
--rollback                             "usage": "not ilike %application%",
--rollback                             "fields": "variable.usage",
--rollback                             "distinctOn": "usage"
--rollback                         },
--rollback                         "list_filter_field": "",
--rollback                         "search_sort_order": "ASC",
--rollback                         "search_sort_column": "usage",
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_output": "usage",
--rollback                         "api_resource_endpoint": "variables-search?sort=usage"
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "main_table": "master.variable",
--rollback             "search_sort_order": "ASC",
--rollback             "search_sort_column": "id",
--rollback             "api_resource_method": "POST",
--rollback             "api_resource_endpoint": "variables-search",
--rollback             "main_table_primary_key": "variableDbId"
--rollback         }   
--rollback     '
--rollback WHERE abbrev = 'TRAITS_QUERY_FIELDS';