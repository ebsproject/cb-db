--liquibase formatted sql

--changeset postgres:insert_traits_query_fields_config context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5965 Insert TRAITS_QUERY_FIELDS config



UPDATE platform.config
SET config_value = '{
        "name": "Traits Query Fields",
        "fields": {
            "basic": [
                {
                    "label": "Trait Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "type",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "1",
                    "advanced_search": "false",
                    "variable_abbrev": "TRAIT",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "type",
                    "api_resource_method": "POST",
                    "api_resource_output": "type",
                    "api_resource_endpoint": "variables-search?sort=type",
                    "api_resource_body": {"fields":"variable.type","distinctOn":"type","type":"metadata|observation"}
                }
            ],
            "additional": [
                {
                    "label": "Data Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataType",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "2",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_TYPE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataType",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_type",
                    "api_resource_endpoint": "variables-search?sort=data_type",
                    "api_resource_body": {"fields":"variable.data_type","distinctOn":"data_type","data_type":"not ilike %json%"}
                },
                {
                    "label": "Data Level",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataLevel",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data level of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "3",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_LEVEL",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataLevel",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_level",
                    "api_resource_endpoint": "variables-search?sort=data_level",
                    "api_resource_body": {"fields":"variable.data_level","distinctOn":"data_level"}
                },
                {
                    "label": "Usage",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "usage",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The usage of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "4",
                    "advanced_search": "false",
                    "variable_abbrev": "USAGE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "usage",
                    "api_resource_method": "POST",
                    "api_resource_output": "usage",
                    "api_resource_endpoint": "variables-search?sort=usage",
                    "api_resource_body": {"fields":"variable.usage","distinctOn":"usage","usage":"not ilike %application%"}
                }
            ]
        },
        "main_table": "master.variable",
        "search_sort_order": "ASC",
        "search_sort_column": "id",
        "api_resource_method": "POST",
        "api_resource_endpoint": "variables-search",
        "main_table_primary_key": "variableDbId"
    }'
WHERE
    abbrev = 'TRAITS_QUERY_FIELDS'
;



--rollback UPDATE platform.config
--rollback SET config_value = '{
--rollback   "name": "Traits Query Fields",
--rollback   "fields": {
--rollback     "basic": [
--rollback       {
--rollback         "label": "Trait Type",
--rollback         "disabled": "false",
--rollback         "required": "false",
--rollback         "fieldName": "type",
--rollback         "list_type": "",
--rollback         "input_type": "multiple",
--rollback         "description": "The type of the trait",
--rollback         "input_field": "selection",
--rollback         "primary_key": "",
--rollback         "order_number": "1",
--rollback         "advanced_search": "false",
--rollback         "variable_abbrev": "TRAIT",
--rollback         "allow_filter_list": "false",
--rollback         "api_resource_body": {
--rollback           "type": "metadata|observation",
--rollback           "fields": "variable.type",
--rollback           "distinctOn": "type"
--rollback         },
--rollback         "list_filter_field": "",
--rollback         "search_sort_order": "ASC",
--rollback         "search_sort_column": "type",
--rollback         "api_resource_method": "POST",
--rollback         "api_resource_output": "type",
--rollback         "api_resource_endpoint": "variables-search?sort=type"
--rollback       }
--rollback     ],
--rollback     "additional": [
--rollback       {
--rollback         "label": "Data Type",
--rollback         "disabled": "false",
--rollback         "required": "false",
--rollback         "fieldName": "dataType",
--rollback         "list_type": "",
--rollback         "input_type": "multiple",
--rollback         "description": "The data type of the trait",
--rollback         "input_field": "selection",
--rollback         "primary_key": "",
--rollback         "order_number": "2",
--rollback         "advanced_search": "false",
--rollback         "variable_abbrev": "DATA_TYPE",
--rollback         "allow_filter_list": "false",
--rollback         "api_resource_body": {
--rollback           "fields": "variable.data_type",
--rollback           "data_type": "not ilike %json%",
--rollback           "distinctOn": "data_type"
--rollback         },
--rollback         "list_filter_field": "",
--rollback         "search_sort_order": "ASC",
--rollback         "search_sort_column": "dataType",
--rollback         "api_resource_method": "POST",
--rollback         "api_resource_output": "data_type",
--rollback         "api_resource_endpoint": "variables-search?sort=data_type"
--rollback       },
--rollback       {
--rollback         "label": "Data Level",
--rollback         "disabled": "false",
--rollback         "required": "false",
--rollback         "fieldName": "dataLevel",
--rollback         "list_type": "",
--rollback         "input_type": "multiple",
--rollback         "description": "The data level of the trait",
--rollback         "input_field": "selection",
--rollback         "primary_key": "",
--rollback         "order_number": "3",
--rollback         "advanced_search": "false",
--rollback         "variable_abbrev": "DATA_LEVEL",
--rollback         "allow_filter_list": "false",
--rollback         "api_resource_body": {
--rollback           "fields": "variable.data_level",
--rollback           "distinctOn": "data_level"
--rollback         },
--rollback         "list_filter_field": "",
--rollback         "search_sort_order": "ASC",
--rollback         "search_sort_column": "dataLevel",
--rollback         "api_resource_method": "POST",
--rollback         "api_resource_output": "data_level",
--rollback         "api_resource_endpoint": "variables-search?sort=data_level"
--rollback       },
--rollback       {
--rollback         "label": "Usage",
--rollback         "disabled": "false",
--rollback         "required": "false",
--rollback         "fieldName": "usage",
--rollback         "list_type": "",
--rollback         "input_type": "multiple",
--rollback         "description": "The usage of the trait",
--rollback         "input_field": "selection",
--rollback         "primary_key": "",
--rollback         "order_number": "4",
--rollback         "advanced_search": "false",
--rollback         "variable_abbrev": "USAGE",
--rollback         "allow_filter_list": "false",
--rollback         "api_resource_body": {
--rollback           "usage": "not ilike %application%",
--rollback           "fields": "variable.usage",
--rollback           "distinctOn": "usage"
--rollback         },
--rollback         "list_filter_field": "",
--rollback         "search_sort_order": "ASC",
--rollback         "search_sort_column": "usage",
--rollback         "api_resource_method": "POST",
--rollback         "api_resource_output": "usage",
--rollback         "api_resource_endpoint": "variables-search?sort=usage"
--rollback       }
--rollback     ]
--rollback   },
--rollback   "main_table": "master.variable",
--rollback   "search_sort_order": "ASC",
--rollback   "search_sort_column": "id",
--rollback   "api_resource_method": "POST",
--rollback   "api_resource_endpoint": "variables-search",
--rollback   "main_table_primary_key": "variableDbId"
--rollback }'
--rollback WHERE
--rollback     abbrev = 'TRAITS_QUERY_FIELDS'
--rollback ;
