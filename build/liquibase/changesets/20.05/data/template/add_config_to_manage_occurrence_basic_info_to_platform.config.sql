--liquibase formatted sql

--changeset postgres:add_config_to_manage_occurrence_basic_info_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Add config to manage occurrence basic info
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM platform.config WHERE abbrev IN ('EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS','DEFAULT_OCCURRENCE_FIELDS');



INSERT INTO
	platform.config (abbrev, name, config_value, rank, usage)
VALUES 
(
	'EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS',
	'Occurrence fields configuration for Trial Experiment Template',
	'
	{
		"basic": {
			"label": "Basic info",
			"fields": [
				{
					"variable_abbrev": "OCCURRENCE_NAME",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "DESCRIPTION",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "CONTCT_PERSON_CONT"
				},
				{
					"variable_abbrev": "ECOSYSTEM"
				}
			]
		}
	}
	',
	1,
	'experiment_manager_occurrence'
);

INSERT INTO
	platform.config (abbrev, name, config_value, rank, usage)
VALUES 
(
	'DEFAULT_OCCURRENCE_FIELDS',
	'Occurrence fields configuration for Default experiment template',
	'
	{
		"basic": {
			"label": "Basic info",
			"fields": [
				{
					"variable_abbrev": "OCCURRENCE_NAME",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "DESCRIPTION",
					"variable_type": "identification"
				},
				{
					"variable_abbrev": "CONTCT_PERSON_CONT"
				},
				{
					"variable_abbrev": "ECOSYSTEM"
				}
			]
		}
	}
	',
	1,
	'experiment_manager_occurrence'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DEFAULT_OCCURRENCE_FIELDS';