--liquibase formatted sql

--changeset postgres:add_variable_distance_bet_plots context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-274 Add variable DISTANCE_BET_PLOTS to master.variable



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('DISTANCE_BET_PLOTS','Distance between plots','Distance between plots') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Distance between plots','DISTANCE_BET_PLOTS',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'DISTANCE_BET_PLOTS' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('DISTANCE_BET_PLOTS','continuous','Distance between plots','m','nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Distance between plots','Distance between plots','float','Distance between plots','Distance between plots','False','DISTANCE_BET_PLOTS','study','metadata','experiment, study') 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'PLANTING_PROTOCOL' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'PLANTING_PROTOCOL' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('PLANTING_PROTOCOL','planting protocol') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback --ALTER TABLE master.scale DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DISTANCE_BET_PLOTS');
--rollback --ALTER TABLE master.scale ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master."property" DISABLE TRIGGER ALL;
--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'DISTANCE_BET_PLOTS');
--rollback --ALTER TABLE master."property" ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.method DISABLE TRIGGER ALL;
--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'DISTANCE_BET_PLOTS');
--rollback --ALTER TABLE master.method ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set_member DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'DISTANCE_BET_PLOTS');
--rollback --ALTER TABLE master.variable_set_member ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'DISTANCE_BET_PLOTS';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
