--liquibase formatted sql

--changeset postgres:update_data_level_value_of_entry_role context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-268 Update data_level value of entry_role variable
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:1 SELECT COUNT(*) FROM master.variable WHERE abbrev = 'ENTRY_ROLE'



UPDATE master.variable SET data_level='entry' WHERE abbrev='ENTRY_ROLE';



--rollback UPDATE master.variable SET data_level=NULL WHERE abbrev='ENTRY_ROLE';
