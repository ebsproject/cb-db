--liquibase formatted sql

--changeset postgres:add_package_to_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-142 Add package to dictionary.entity
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM dictionary.entity WHERE abbrev = 'PACKAGE';



INSERT INTO 
    dictionary.entity (abbrev, name, description, creator_id, table_id)
VALUES 
    ('PACKAGE', 'package', 'Package', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'PACKAGE'));



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'PACKAGE';