--liquibase formatted sql

--changeset postgres:update_navigation_add_traits_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update navigation add TRAITS in platforn.application
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM platform.application WHERE abbrev = 'TRAITS'



--create new application for germplasm
INSERT INTO 
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES 
    ('TRAITS', 'Traits', 'Browse Traits', 'border_color', 1);

--map new application with action (url)
INSERT INTO 
    platform.application_action (application_id, module, controller,creator_id)
SELECT
    app.id,
    'traits',
    'default',
    1
FROM
    platform.application app
WHERE
    app.abbrev = 'TRAITS';

--update navigation
UPDATE
    platform.space
SET
    menu_data = 
    '
        {
            "left_menu_items": [
                {
                    "name": "experiment-creation",
                    "items": [
                        {
                            "name": "experiment-creation-experiments",
                            "label": "Experiments",
                            "appAbbrev": "EXPERIMENT_CREATION"
                        },
                        {
                            "name": "experiment-creation-occurrences",
                            "label": "Occurrences",
                            "appAbbrev": "OCCURRENCES"
                        }
                    ],
                    "label": "Experiments"
                },
                {
                    "name": "data-collection-qc",
                    "items": [
                        {
                            "name": "data-collection-qc-quality-control",
                            "label": "Quality control",
                            "appAbbrev": "QUALITY_CONTROL"
                        }
                    ],
                    "label": "Data collection & QC"
                },
                {
                    "name": "seeds",
                    "items": [
                        {
                            "name": "seeds-find-seeds",
                            "label": "Find seeds",
                            "appAbbrev": "FIND_SEEDS"
                        },
                        {
                            "name": "seeds-harvest-manager",
                            "label": "Harvest manager",
                            "appAbbrev": "HARVEST_MANAGER"
                        }
                    ],
                    "label": "Seeds"
                }
            ],
            "main_menu_items": [
                {
                    "icon": "folder_special",
                    "name": "data-management",
                    "items": [
                        {
                            "name": "data-management_seasons",
                            "label": "Seasons",
                            "appAbbrev": "MANAGE_SEASONS"
                        },
                        {
                            "name": "data-management_germplasm",
                            "label": "Germplasm",
                            "appAbbrev": "GERMPLASM_CATALOG"
                        },
                        {
                            "name": "data-management_traits",
                            "label": "Traits",
                            "appAbbrev": "TRAITS"
                        }
                    ],
                    "label": "Data management"
                },
                {
                    "icon": "settings",
                    "name": "administration",
                    "items": [
                        {
                            "name": "administration_users",
                            "label": "Persons",
                            "appAbbrev": "PERSONS"
                        }
                    ],
                    "label": "Administration"
                }
            ]
        }
    '
WHERE abbrev = 'ADMIN';



--rollback DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev='TRAITS');
--rollback DELETE FROM platform.application WHERE abbrev='TRAITS';
--rollback UPDATE
--rollback     platform.space
--rollback SET
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "experiment-creation-experiments",
--rollback                             "label": "Experiments",
--rollback                             "appAbbrev": "EXPERIMENT_CREATION"
--rollback                         },
--rollback                         {
--rollback                             "name": "experiment-creation-occurrences",
--rollback                             "label": "Occurrences",
--rollback                             "appAbbrev": "OCCURRENCES"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Experiments"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ],
--rollback             "main_menu_items": [
--rollback                 {
--rollback                     "icon": "folder_special",
--rollback                     "name": "data-management",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-management_seasons",
--rollback                             "label": "Seasons",
--rollback                             "appAbbrev": "MANAGE_SEASONS"
--rollback                         },
--rollback                         {
--rollback                             "name": "data-management_germplasm",
--rollback                             "label": "Germplasm",
--rollback                             "appAbbrev": "GERMPLASM_CATALOG"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data management"
--rollback                 },
--rollback                 {
--rollback                     "icon": "settings",
--rollback                     "name": "administration",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "administration_users",
--rollback                             "label": "Persons",
--rollback                             "appAbbrev": "PERSONS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Administration"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'ADMIN';