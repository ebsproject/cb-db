--liquibase formatted sql

--changeset postgres:populate_scale_value_abbrevs_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-267 Populate scale_value abbrevs in master.scale_value



UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_CREATED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Created');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_ACTIVE' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Active');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_COMPLETED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Completed');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_LOCATION_REP_STUDIES_CREATED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Location Rep Studies Created');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_EXPERIMENT_GROUP_SPECIFIED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Experiment Group Specified');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_DESIGN_GENERATED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Design Generated');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_ENTRY_LIST_CREATED' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Entry List Created');
UPDATE master.scale_value SET abbrev='EXPERIMENT_STATUS_DRAFT' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Draft');

UPDATE master.scale_value SET abbrev='ENTRY_TYPE_CROSS' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='cross');
UPDATE master.scale_value SET abbrev='ENTRY_TYPE_PARENT' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='parent');
UPDATE master.scale_value SET abbrev='ENTRY_TYPE_FILLER' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='filler');
UPDATE master.scale_value SET abbrev='ENTRY_TYPE_CONTROL' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='control');
UPDATE master.scale_value SET abbrev='ENTRY_TYPE_CHECK' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='check');
UPDATE master.scale_value SET abbrev='ENTRY_TYPE_ENTRY' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='entry');

UPDATE master.scale_value SET abbrev='ENTRY_ROLE_PERFORMANCE_CHECK' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='performance check');
UPDATE master.scale_value SET abbrev='ENTRY_ROLE_SPATIAL_CHECK' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='spatial check');
UPDATE master.scale_value SET abbrev='ENTRY_ROLE_FEMALE_AND_MALE' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='female-and-male');
UPDATE master.scale_value SET abbrev='ENTRY_ROLE_MALE' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='male');
UPDATE master.scale_value SET abbrev='ENTRY_ROLE_FEMALE' WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='female');



--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Created');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Active');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Completed');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Location Rep Studies Created');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Experiment Group Specified');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Design Generated');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Entry List Created');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS' AND value='Draft');
--rollback 
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='cross');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='parent');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='filler');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='control');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='check');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE' AND value='entry');
--rollback 
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='performance check');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='spatial check');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='female-and-male');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='male');
--rollback UPDATE master.scale_value SET abbrev=NULL WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE' AND value='female');