--liquibase formatted sql

--changeset postgres:remove_duplicate_messages_in_api.messages context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-154 Remove duplicate messages in api.messages
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:2 SELECT COUNT(*) FROM api.messages WHERE code in ('404021','404032')



DELETE FROM api.messages WHERE code='404021';
DELETE FROM api.messages WHERE code='404032';



--rollback INSERT INTO api.messages (http_status_code,code,type,message,creator_id,url) VALUES ('404','404021','ERR','The entry you have requested does not exist.',1,'/responses.html#404021');
--rollback INSERT INTO api.messages (http_status_code,code,type,message,creator_id,url) VALUES ('404','404032','ERR','The field layout you have requested does not exist.',1,'/responses.html#404032');