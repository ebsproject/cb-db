--liquibase formatted sql

--changeset postgres:update_traits_query_fields_config_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5965 Update TRAITS_QUERY_FIELDS config in platform.config



DELETE FROM
    platform.config
WHERE
    abbrev = 'TRAITS_QUERY_FIELDS';

INSERT INTO
    platform.config (abbrev, name, config_value, rank, usage)
VALUES (
    'TRAITS_QUERY_FIELDS',
    'Query fields for trait variable',
    '{
        "name": "Traits Query Fields",
        "fields": {
            "basic": [
                {
                    "label": "Trait Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "type",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "1",
                    "advanced_search": "false",
                    "variable_abbrev": "TRAIT",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "type",
                    "api_resource_method": "POST",
                    "api_resource_output": "type",
                    "api_resource_endpoint": "variables-search?sort=type",
                    "api_resource_body": {"fields":"variable.type","distinctOn":"type","type":"metadata|observation"}
                }
            ],
            "additional": [
                {
                    "label": "Abbrev",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "abbrev",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The abbrev of the trait",
                    "input_field": "input field",
                    "primary_key": "",
                    "order_number": "2",
                    "advanced_search": "false",
                    "variable_abbrev": "ABBREV",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "abbrev",
                    "api_resource_method": "POST",
                    "api_resource_output": "abbrev",
                    "api_resource_endpoint": "variables-search?sort=abbrev",
                    "api_resource_body": {"fields":"variable.abbrev","distinctOn":"abbrev"}
                },
                {
                    "label": "Label",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "label",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The label of the trait",
                    "input_field": "input field",
                    "primary_key": "",
                    "order_number": "3",
                    "advanced_search": "false",
                    "variable_abbrev": "LABEL",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "label",
                    "api_resource_method": "POST",
                    "api_resource_output": "label",
                    "api_resource_endpoint": "variables-search?sort=label",
                    "api_resource_body": {"fields":"variable.label","distinctOn":"label"}
                },
                {
                    "label": "Name",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "name",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The name of the trait",
                    "input_field": "input field",
                    "primary_key": "",
                    "order_number": "4",
                    "advanced_search": "false",
                    "variable_abbrev": "NAME",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "name",
                    "api_resource_method": "POST",
                    "api_resource_output": "name",
                    "api_resource_endpoint": "variables-search?sort=name",
                    "api_resource_body": {"fields":"variable.name","distinctOn":"name"}
                },
                {
                    "label": "Data Type",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataType",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data type of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "5",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_TYPE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataType",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_type",
                    "api_resource_endpoint": "variables-search?sort=data_type",
                    "api_resource_body": {"fields":"variable.data_type","distinctOn":"data_type","data_type":"not ilike %json%"}
                },
                {
                    "label": "Data Level",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "dataLevel",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The data level of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "6",
                    "advanced_search": "false",
                    "variable_abbrev": "DATA_LEVEL",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "dataLevel",
                    "api_resource_method": "POST",
                    "api_resource_output": "data_level",
                    "api_resource_endpoint": "variables-search?sort=data_level",
                    "api_resource_body": {"fields":"variable.data_level","distinctOn":"data_level"}
                },
                {
                    "label": "Usage",
                    "disabled": "false",
                    "required": "false",
                    "fieldName": "usage",
                    "list_type": "",
                    "input_type": "multiple",
                    "description": "The usage of the trait",
                    "input_field": "selection",
                    "primary_key": "",
                    "order_number": "7",
                    "advanced_search": "false",
                    "variable_abbrev": "USAGE",
                    "allow_filter_list": "false",
                    "list_filter_field": "",
                    "search_sort_order": "ASC",
                    "search_sort_column": "usage",
                    "api_resource_method": "POST",
                    "api_resource_output": "usage",
                    "api_resource_endpoint": "variables-search?sort=usage",
                    "api_resource_body": {"fields":"variable.usage","distinctOn":"usage","usage":"not ilike %application%"}
                }
            ]
        },
        "main_table": "master.variable",
        "search_sort_order": "ASC",
        "search_sort_column": "id",
        "api_resource_method": "POST",
        "api_resource_endpoint": "variables-search",
        "main_table_primary_key": "variableDbId"
    }',
    1,
    'traits_browser'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'TRAITS_QUERY_FIELDS';

--rollback INSERT INTO platform.config (abbrev,"name",config_value,"rank","usage",remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,entity_id) VALUES 
--rollback ('TRAITS_QUERY_FIELDS','Query fields for trait variable','{"name": "Traits Query Fields", "fields": {"basic": [{"label": "Trait Type", "disabled": "false", "required": "false", "fieldName": "type", "list_type": "", "input_type": "multiple", "description": "The type of the trait", "input_field": "selection", "primary_key": "", "order_number": "1", "advanced_search": "false", "variable_abbrev": "TRAIT", "allow_filter_list": "false", "api_resource_body": {"type": "metadata|observation", "fields": "variable.type", "distinctOn": "type"}, "list_filter_field": "", "search_sort_order": "ASC", "search_sort_column": "type", "api_resource_method": "POST", "api_resource_output": "type", "api_resource_endpoint": "variables-search?sort=type"}], "additional": [{"label": "Data Type", "disabled": "false", "required": "false", "fieldName": "dataType", "list_type": "", "input_type": "multiple", "description": "The data type of the trait", "input_field": "selection", "primary_key": "", "order_number": "2", "advanced_search": "false", "variable_abbrev": "DATA_TYPE", "allow_filter_list": "false", "api_resource_body": {"fields": "variable.data_type", "data_type": "not ilike %json%", "distinctOn": "data_type"}, "list_filter_field": "", "search_sort_order": "ASC", "search_sort_column": "dataType", "api_resource_method": "POST", "api_resource_output": "data_type", "api_resource_endpoint": "variables-search?sort=data_type"}, {"label": "Data Level", "disabled": "false", "required": "false", "fieldName": "dataLevel", "list_type": "", "input_type": "multiple", "description": "The data level of the trait", "input_field": "selection", "primary_key": "", "order_number": "3", "advanced_search": "false", "variable_abbrev": "DATA_LEVEL", "allow_filter_list": "false", "api_resource_body": {"fields": "variable.data_level", "distinctOn": "data_level"}, "list_filter_field": "", "search_sort_order": "ASC", "search_sort_column": "dataLevel", "api_resource_method": "POST", "api_resource_output": "data_level", "api_resource_endpoint": "variables-search?sort=data_level"}, {"label": "Usage", "disabled": "false", "required": "false", "fieldName": "usage", "list_type": "", "input_type": "multiple", "description": "The usage of the trait", "input_field": "selection", "primary_key": "", "order_number": "4", "advanced_search": "false", "variable_abbrev": "USAGE", "allow_filter_list": "false", "api_resource_body": {"usage": "not ilike %application%", "fields": "variable.usage", "distinctOn": "usage"}, "list_filter_field": "", "search_sort_order": "ASC", "search_sort_column": "usage", "api_resource_method": "POST", "api_resource_output": "usage", "api_resource_endpoint": "variables-search?sort=usage"}]}, "main_table": "master.variable", "search_sort_order": "ASC", "search_sort_column": "id", "api_resource_method": "POST", "api_resource_endpoint": "variables-search", "main_table_primary_key": "variableDbId"}',1,'traits_browser',NULL,'2020-05-18 13:58:37.863099',1,NULL,NULL,NULL,false,NULL);
