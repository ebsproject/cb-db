--liquibase formatted sql

--changeset postgres:remove_unit_in_plot_type_variable_scale context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-348 Remove unit in plot_type variable scale



UPDATE 
    master.scale
SET
    unit = NULL
WHERE
    id 
IN
    (
        SELECT
            scale_id
        FROM
            master.variable
        WHERE
            abbrev = 'PLOT_TYPE'
    )
;



--rollback UPDATE 
--rollback     master.scale
--rollback SET
--rollback     unit = 'm'
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             scale_id
--rollback         FROM
--rollback             master.variable
--rollback         WHERE
--rollback             abbrev = 'PLOT_TYPE'
--rollback     )
--rollback ;