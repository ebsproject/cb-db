--liquibase formatted sql

--changeset postgres:disable_metrics_widget_in_platform.dashboard_widget context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-345 Disable metrics widget in platform.dashboard_widget



UPDATE 
    platform.dashboard_widget 
SET 
    is_void = true 
WHERE 
    abbrev = 'METRICS';



--rollback UPDATE 
--rollback     platform.dashboard_widget 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     abbrev = 'METRICS';