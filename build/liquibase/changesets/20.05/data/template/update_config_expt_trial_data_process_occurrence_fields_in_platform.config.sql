--liquibase formatted sql

--changeset postgres:update_config_expt_trial_data_process_occurrence_fields_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update config EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS in platform.config



UPDATE 
    platform.config
SET 
    config_value = 
    '
        {
            "basic": {
                "label": "Basic info",
                "fields": [
                    {
                        "variable_abbrev": "OCCURRENCE_NAME",
                        "variable_type": "identification",
                        "required": "required"
                    },
                    {
                        "variable_abbrev": "DESCRIPTION",
                        "variable_type": "identification"
                    },
                    {
                        "variable_abbrev": "CONTCT_PERSON_CONT"
                    },
                    {
                        "variable_abbrev": "ECOSYSTEM"
                    }
                ]
            }
        }
    '
WHERE abbrev = 'EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS';



--rollback UPDATE 
--rollback     platform.config
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "basic": {
--rollback                 "label": "Basic info",
--rollback                 "fields": [
--rollback                     {
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "OCCURRENCE_NAME"
--rollback                     },
--rollback                     {
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "DESCRIPTION"
--rollback                     },
--rollback                     {
--rollback                         "variable_abbrev": "CONTCT_PERSON_CONT"
--rollback                     },
--rollback                     {
--rollback                         "variable_abbrev": "ECOSYSTEM"
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'EXPT_TRIAL_DATA_PROCESS_OCCURRENCE_FIELDS';