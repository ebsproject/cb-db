--liquibase formatted sql

--changeset postgres:update_abbrev_experiment_sub_subtype_to_experiment_sub_sub_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-293 Update abbrev experiment_sub_subtype to experiment_sub_sub_type in master.variable



UPDATE
    master.variable
SET
    abbrev = 'EXPERIMENT_SUB_SUB_TYPE',
    label = 'Experiment Sub Sub-type',
    name = 'Experiment Sub Sub-type',
    display_name = 'Experiment Sub Sub-type'
WHERE
    abbrev = 'EXPERIMENT_SUB_SUBTYPE';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'EXPERIMENT_SUB_SUBTYPE',
--rollback     label = 'Experiment Sub Sub-type',
--rollback     name = 'Experiment Sub Sub-type',
--rollback     display_name = 'Experiment Sub Sub-type'
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_SUB_SUB_TYPE';