--liquibase formatted sql

--changeset postgres:remove_special_character_in_geospatial_object_type_and_list_type context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-291 Remove special character in geospatial_object_type and list type



UPDATE 
    place.geospatial_object
SET 
    geospatial_object_type = 'planting area'
WHERE
    geospatial_object_type = 'planting_area';

ALTER TABLE platform.list DROP CONSTRAINT list_type_chk;

UPDATE 
    platform.list
SET 
    type = 'trait protocol'
WHERE
    type = 'trait_protocol';



--rollback UPDATE 
--rollback     place.geospatial_object
--rollback SET 
--rollback     geospatial_object_type = 'planting_area'
--rollback WHERE
--rollback     geospatial_object_type = 'planting area';
--rollback
--rollback ALTER TABLE 
--rollback     platform.list 
--rollback DROP CONSTRAINT 
--rollback     list_type_chk;
--rollback
--rollback UPDATE 
--rollback     platform.list
--rollback SET 
--rollback     type = 'trait_protocol'
--rollback WHERE
--rollback     type = 'trait protocol';
--rollback
--rollback ALTER TABLE 
--rollback     platform.list
--rollback ADD CONSTRAINT 
--rollback     list_type_chk CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'designation'::text, 'study'::text, 'variable'::text, 'trait_protocol'::text, 'package'::text]));