--liquibase formatted sql

--changeset postgres:remove_special_character_of_location_type_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-305 Remove special character of location_type in experiment.location



UPDATE 
    experiment.location
SET 
    location_type = 'planting area'
WHERE
    location_type = 'planting_area';



--rollback UPDATE 
--rollback     experiment.location
--rollback SET 
--rollback     location_type = 'planting_area'
--rollback WHERE
--rollback     location_type = 'planting area';