--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_trial_irri_entry_list_act_val_in_platform.config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update entry list template EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for IRRI Trial data process",
            "Values": [
                {
                    "default": "entry",
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_TYPE",
                    "api_resource_endpoint": "entries"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS",
                    "api_resource_endpoint": "entries"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_endpoint": "entries"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for Selection and Advancement data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": "entry",
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "variable_abbrev": "ENTRY_TYPE"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "ENTRY_CLASS"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_TRIAL_IRRI_ENTRY_LIST_ACT_VAL';