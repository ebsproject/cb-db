--liquibase formatted sql

--changeset postgres:remove_duplicate_elite_line_name_IRRI_235 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-260 Remove duplicate elite line name IRRI 235


-- delete incorrect elite line name
DELETE FROM germplasm.germplasm_name
WHERE id = 1911200
    and name_value = 'IRRI 235';


-- set IRRI 216 as standard
UPDATE germplasm.germplasm_name
SET germplasm_name_status = 'standard'
WHERE id = 1699594
    AND name_value = 'IRRI 216';


-- set IRRI 216 as designation in germplasm.germplasm
UPDATE germplasm.germplasm
SET designation = 'IRRI 216'
WHERE id = 47690;



--rollback UPDATE germplasm.germplasm_name
--rollback SET germplasm_name_status = 'active'
--rollback WHERE id = 1699594
--rollback     AND name_value = 'IRRI 216';


--rollback INSERT INTO germplasm.germplasm_name (id,germplasm_id,name_value,germplasm_name_type,germplasm_name_status,germplasm_normalized_name,creator_id,creation_timestamp,modifier_id,modification_timestamp,is_void,notes,event_log) VALUES 
--rollback (1911200,47690,'IRRI 235','elite_lines','standard','IRRI 235',1,'2020-05-01 20:43:14.281257',NULL,NULL,false,NULL,NULL);


--rollback UPDATE germplasm.germplasm
--rollback SET designation = 'IRRI 235'
--rollback WHERE id = 47690;
