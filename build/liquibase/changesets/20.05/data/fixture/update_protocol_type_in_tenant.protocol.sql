--liquibase formatted sql

--changeset postgres:update_protocol_type_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-343 Update protocol_type in tenant.protocol



UPDATE 
    tenant.protocol 
SET 
    protocol_type = 'trait'
WHERE
    protocol_type = 'trait protocol';



--rollback UPDATE 
--rollback     tenant.protocol 
--rollback SET 
--rollback     protocol_type = 'trait protocol'
--rollback WHERE
--rollback     protocol_type = 'trait';