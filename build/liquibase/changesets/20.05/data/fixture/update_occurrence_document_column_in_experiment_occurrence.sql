--liquibase formatted sql

--changeset postgres:update_occurrence_document_column_in_experiment_occurrence context:fixture splitStatements:false
--comment: EBS-288 Update occurrence_document in experiment.occurrence



UPDATE experiment.occurrence SET occurrence_document = NULL;

CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		eo.id id,
		concat(
			setweight(to_tsvector(eo.occurrence_name), 'A'), ' ',
			setweight(to_tsvector(eo.occurrence_code), 'B'), ' ',
			setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
			setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
			setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
			setweight(to_tsvector((SELECT s.stage_code FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
			setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',   
			setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
			setweight(to_tsvector((SELECT s2.season_code FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
			setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
			setweight(to_tsvector(unaccent((SELECT p.geospatial_object_code FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id)))), 'C'), ' ',
			setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id)))), 'C'), ' ',
			setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_code FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id))), 'CD'), ' ',
			setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id))), 'CD'), ' ',
			setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
			setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
			setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  eo.creator_id))), 'CD'), ' ',
			setweight(to_tsvector(eo.occurrence_status), 'CD')
		) AS doc
	FROM 
		experiment.occurrence eo
	LEFT JOIN 
		experiment.experiment ee
	ON 
		ee.id = eo.experiment_id
	ORDER BY
		eo.id
)
UPDATE experiment.occurrence eo SET occurrence_document = cast(t1.doc AS tsvector) FROM t1 WHERE eo.id = t1.id;



--rollback UPDATE experiment.occurrence SET occurrence_document = NULL;
--rollback 
--rollback CREATE EXTENSION IF NOT EXISTS unaccent;
--rollback 
--rollback WITH t1 AS (
--rollback 	SELECT 
--rollback 		eo.id id,
--rollback 		concat(
--rollback 			setweight(to_tsvector(eo.occurrence_name), 'A'), ' ',
--rollback 			setweight(to_tsvector(eo.occurrence_code), 'B'), ' ',
--rollback 			setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
--rollback 			setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
--rollback 			setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
--rollback 			setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
--rollback 			setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
--rollback 			setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id)))), 'C'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = eo.geospatial_object_id))), 'CD'), ' ',
--rollback 			setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
--rollback 			setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  eo.creator_id))), 'CD'), ' ',
--rollback 			setweight(to_tsvector(eo.occurrence_status), 'CD')
--rollback 		) AS doc
--rollback 	FROM 
--rollback 		experiment.occurrence eo
--rollback 	LEFT JOIN 
--rollback 		experiment.experiment ee
--rollback 	ON 
--rollback 		ee.id = eo.experiment_id
--rollback 	ORDER BY
--rollback 		eo.id
--rollback )
--rollback UPDATE experiment.occurrence eo SET occurrence_document = cast(t1.doc AS tsvector) FROM t1 WHERE eo.id = t1.id;