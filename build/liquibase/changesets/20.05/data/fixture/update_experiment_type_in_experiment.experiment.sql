--liquibase formatted sql

--changeset postgres:update_experiment_type_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-305 Update experiment_type in experiment.experiment



UPDATE 
    experiment.experiment
SET
    experiment_type = 'Nursery'
WHERE
    experiment_type = 'nursery';

UPDATE 
    experiment.experiment
SET
    experiment_type = 'Trial'
WHERE
    experiment_type = 'trial';



--rollback UPDATE 
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_type = 'nursery'
--rollback WHERE
--rollback     experiment_type = 'Nursery';
--rollback 
--rollback UPDATE 
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_type = 'trial'
--rollback WHERE
--rollback     experiment_type = 'Trial';