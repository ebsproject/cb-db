--liquibase formatted sql

--changeset postgres:update_location_document_column_in_experiment.location context:fixture splitStatements:false
--comment: EBS-288 Update location_document in experiment.location



UPDATE experiment.location SET location_document = NULL;

CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		el.id,
		concat(
			setweight(to_tsvector(unaccent(el.location_name)),'A'), ' ',
			setweight(to_tsvector(el.location_code),'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_code FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = el.geospatial_object_id)))), 'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = el.geospatial_object_id)))), 'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_code FROM place.geospatial_object pg WHERE pg.id = el.geospatial_object_id))),'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = el.geospatial_object_id))),'B'), ' ',
			setweight(to_tsvector((select program_name from tenant.program where id = el.program_id)),'C'), ' ',
			setweight(to_tsvector(el.location_year::varchar),'C'), ' ',
			setweight(to_tsvector((SELECT s.season_code FROM tenant.season s WHERE s.id =  el.season_id)),'C'), ' ',
			setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  el.season_id)),'C'), ' ',
			setweight(to_tsvector((SELECT ts.stage_code FROM tenant.stage ts WHERE ts.id = el.stage_id)), 'C'), ' ',
			setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = el.stage_id)), 'C'), ' ',
			setweight(to_tsvector(el.location_type), 'CD'), ' ',
			setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = el.creator_id))),'CD'), ' ',
			setweight(to_tsvector(el.location_status),'CD')
		) AS doc
	FROM 
		experiment.LOCATION el
)

UPDATE experiment.location el SET location_document = cast(t1.doc AS tsvector) FROM t1 WHERE el.id = t1.id;



--rollback UPDATE experiment.location SET location_document = NULL;
--rollback 
--rollback CREATE EXTENSION IF NOT EXISTS unaccent;
--rollback 
--rollback WITH t1 AS (
--rollback 	SELECT 
--rollback 		el.id,
--rollback 		concat(
--rollback 			setweight(to_tsvector(unaccent(el.location_name)),'A'), ' ',
--rollback 			setweight(to_tsvector(el.location_code),'B'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = el.geospatial_object_id)))), 'B'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = el.geospatial_object_id))),'B'), ' ',
--rollback 			setweight(to_tsvector((select program_name from tenant.program where id = el.program_id)),'C'), ' ',
--rollback 			setweight(to_tsvector(el.location_year::varchar),'C'), ' ',
--rollback 			setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  el.season_id)),'C'), ' ',
--rollback 			setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = el.stage_id)), 'C'), ' ',
--rollback 			setweight(to_tsvector(el.location_type), 'CD'), ' ',
--rollback 			setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = el.creator_id))),'CD'), ' ',
--rollback 			setweight(to_tsvector(el.location_status),'CD')
--rollback 		) AS doc
--rollback 	FROM 
--rollback 		experiment.LOCATION el
--rollback )
--rollback 
--rollback UPDATE experiment.location el SET location_document = cast(t1.doc AS tsvector) FROM t1 WHERE el.id = t1.id;