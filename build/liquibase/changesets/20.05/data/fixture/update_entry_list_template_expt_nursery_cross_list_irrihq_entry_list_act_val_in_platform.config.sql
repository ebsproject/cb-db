--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_nursery_cross_list_irrihq_entry_list_act_val_in_platform.config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Update entry list template EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
            "Values": [{
                    "fixed": true,
                    "default": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_ROLE",
                    "allowed_values": [
                        "ENTRY_ROLE_FEMALE",
                        "ENTRY_ROLE_FEMALE_AND_MALE",
                        "ENTRY_ROLE_MALE"
                    ],
                    "api_resource_endpoint": "entries"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_endpoint": "entries"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "fixed": true,
--rollback                     "default": false,
--rollback                     "required": "required",
--rollback                     "variable_abbrev": "ENTRY_ROLE"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';