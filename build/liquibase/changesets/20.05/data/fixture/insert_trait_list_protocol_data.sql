--liquibase formatted sql

--changeset postgres:insert_trait_list_protocol_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-139 Insert trait list protocol data
--validCheckSum: 8:352940d71dccb848261d3d8e099a78a5


-- insert into platform.list
INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1040', 'TRAIT_LIST_EXPT0006', 'TRAIT LIST FOR EXPT0006', 'TRAIT LIST FOR EXPT0006', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1041', 'TRAIT_LIST_EXPT0007', 'TRAIT LIST FOR EXPT0007', 'TRAIT LIST FOR EXPT0007', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1042', 'TRAIT_LIST_EXPT0008', 'TRAIT LIST FOR EXPT0008', 'TRAIT LIST FOR EXPT0008', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1043', 'TRAIT_LIST_EXPT0009', 'TRAIT LIST FOR EXPT0009', 'TRAIT LIST FOR EXPT0009', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1044', 'TRAIT_LIST_EXPT0012', 'TRAIT LIST FOR EXPT0012', 'TRAIT LIST FOR EXPT0012', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1045', 'TRAIT_LIST_EXPT0013', 'TRAIT LIST FOR EXPT0013', 'TRAIT LIST FOR EXPT0013', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1046', 'TRAIT_LIST_EXPT0014', 'TRAIT LIST FOR EXPT0014', 'TRAIT LIST FOR EXPT0014', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1047', 'TRAIT_LIST_EXPT0015', 'TRAIT LIST FOR EXPT0015', 'TRAIT LIST FOR EXPT0015', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1048', 'TRAIT_LIST_EXPT0016', 'TRAIT LIST FOR EXPT0016', 'TRAIT LIST FOR EXPT0016', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1049', 'TRAIT_LIST_EXPT0017', 'TRAIT LIST FOR EXPT0017', 'TRAIT LIST FOR EXPT0017', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1050', 'TRAIT_LIST_EXPT0018', 'TRAIT LIST FOR EXPT0018', 'TRAIT LIST FOR EXPT0018', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1051', 'TRAIT_LIST_EXPT0019', 'TRAIT LIST FOR EXPT0019', 'TRAIT LIST FOR EXPT0019', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1052', 'TRAIT_LIST_EXPT0021', 'TRAIT LIST FOR EXPT0021', 'TRAIT LIST FOR EXPT0021', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1053', 'TRAIT_LIST_EXPT0022', 'TRAIT LIST FOR EXPT0022', 'TRAIT LIST FOR EXPT0022', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1054', 'TRAIT_LIST_EXPT0023', 'TRAIT LIST FOR EXPT0023', 'TRAIT LIST FOR EXPT0023', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1055', 'TRAIT_LIST_EXPT0024', 'TRAIT LIST FOR EXPT0024', 'TRAIT LIST FOR EXPT0024', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1056', 'TRAIT_LIST_EXPT0025', 'TRAIT LIST FOR EXPT0025', 'TRAIT LIST FOR EXPT0025', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1057', 'TRAIT_LIST_EXPT0026', 'TRAIT LIST FOR EXPT0026', 'TRAIT LIST FOR EXPT0026', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1058', 'TRAIT_LIST_EXPT0027', 'TRAIT LIST FOR EXPT0027', 'TRAIT LIST FOR EXPT0027', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1059', 'TRAIT_LIST_EXPT0028', 'TRAIT LIST FOR EXPT0028', 'TRAIT LIST FOR EXPT0028', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1060', 'TRAIT_LIST_EXPT0029', 'TRAIT LIST FOR EXPT0029', 'TRAIT LIST FOR EXPT0029', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1061', 'TRAIT_LIST_EXPT0031', 'TRAIT LIST FOR EXPT0031', 'TRAIT LIST FOR EXPT0031', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1062', 'TRAIT_LIST_EXPT0032', 'TRAIT LIST FOR EXPT0032', 'TRAIT LIST FOR EXPT0032', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1063', 'TRAIT_LIST_EXPT0033', 'TRAIT LIST FOR EXPT0033', 'TRAIT LIST FOR EXPT0033', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1064', 'TRAIT_LIST_EXPT0041', 'TRAIT LIST FOR EXPT0041', 'TRAIT LIST FOR EXPT0041', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1065', 'TRAIT_LIST_EXPT0042', 'TRAIT LIST FOR EXPT0042', 'TRAIT LIST FOR EXPT0042', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1066', 'TRAIT_LIST_EXPT0043', 'TRAIT LIST FOR EXPT0043', 'TRAIT LIST FOR EXPT0043', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1067', 'TRAIT_LIST_EXPT0044', 'TRAIT LIST FOR EXPT0044', 'TRAIT LIST FOR EXPT0044', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1068', 'TRAIT_LIST_EXPT0045', 'TRAIT LIST FOR EXPT0045', 'TRAIT LIST FOR EXPT0045', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1069', 'TRAIT_LIST_EXPT0046', 'TRAIT LIST FOR EXPT0046', 'TRAIT LIST FOR EXPT0046', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1070', 'TRAIT_LIST_EXPT0047', 'TRAIT LIST FOR EXPT0047', 'TRAIT LIST FOR EXPT0047', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1071', 'TRAIT_LIST_EXPT0049', 'TRAIT LIST FOR EXPT0049', 'TRAIT LIST FOR EXPT0049', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1072', 'TRAIT_LIST_EXPT0051', 'TRAIT LIST FOR EXPT0051', 'TRAIT LIST FOR EXPT0051', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1073', 'TRAIT_LIST_EXPT0052', 'TRAIT LIST FOR EXPT0052', 'TRAIT LIST FOR EXPT0052', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1074', 'TRAIT_LIST_EXPT0053', 'TRAIT LIST FOR EXPT0053', 'TRAIT LIST FOR EXPT0053', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1075', 'TRAIT_LIST_EXPT0055', 'TRAIT LIST FOR EXPT0055', 'TRAIT LIST FOR EXPT0055', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1076', 'TRAIT_LIST_EXPT0056', 'TRAIT LIST FOR EXPT0056', 'TRAIT LIST FOR EXPT0056', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1077', 'TRAIT_LIST_EXPT0057', 'TRAIT LIST FOR EXPT0057', 'TRAIT LIST FOR EXPT0057', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1078', 'TRAIT_LIST_EXPT0058', 'TRAIT LIST FOR EXPT0058', 'TRAIT LIST FOR EXPT0058', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1079', 'TRAIT_LIST_EXPT0059', 'TRAIT LIST FOR EXPT0059', 'TRAIT LIST FOR EXPT0059', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1080', 'TRAIT_LIST_EXPT0060', 'TRAIT LIST FOR EXPT0060', 'TRAIT LIST FOR EXPT0060', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1081', 'TRAIT_LIST_EXPT0061', 'TRAIT LIST FOR EXPT0061', 'TRAIT LIST FOR EXPT0061', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1082', 'TRAIT_LIST_EXPT0062', 'TRAIT LIST FOR EXPT0062', 'TRAIT LIST FOR EXPT0062', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1083', 'TRAIT_LIST_EXPT0063', 'TRAIT LIST FOR EXPT0063', 'TRAIT LIST FOR EXPT0063', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1084', 'TRAIT_LIST_EXPT0065', 'TRAIT LIST FOR EXPT0065', 'TRAIT LIST FOR EXPT0065', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1085', 'TRAIT_LIST_EXPT0066', 'TRAIT LIST FOR EXPT0066', 'TRAIT LIST FOR EXPT0066', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1086', 'TRAIT_LIST_EXPT0068', 'TRAIT LIST FOR EXPT0068', 'TRAIT LIST FOR EXPT0068', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1087', 'TRAIT_LIST_EXPT0071', 'TRAIT LIST FOR EXPT0071', 'TRAIT LIST FOR EXPT0071', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1088', 'TRAIT_LIST_EXPT0072', 'TRAIT LIST FOR EXPT0072', 'TRAIT LIST FOR EXPT0072', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1089', 'TRAIT_LIST_EXPT0073', 'TRAIT LIST FOR EXPT0073', 'TRAIT LIST FOR EXPT0073', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1090', 'TRAIT_LIST_EXPT0074', 'TRAIT LIST FOR EXPT0074', 'TRAIT LIST FOR EXPT0074', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1091', 'TRAIT_LIST_EXPT0075', 'TRAIT LIST FOR EXPT0075', 'TRAIT LIST FOR EXPT0075', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1092', 'TRAIT_LIST_EXPT0076', 'TRAIT LIST FOR EXPT0076', 'TRAIT LIST FOR EXPT0076', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1093', 'TRAIT_LIST_EXPT0077', 'TRAIT LIST FOR EXPT0077', 'TRAIT LIST FOR EXPT0077', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1094', 'TRAIT_LIST_EXPT0078', 'TRAIT LIST FOR EXPT0078', 'TRAIT LIST FOR EXPT0078', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1095', 'TRAIT_LIST_EXPT0079', 'TRAIT LIST FOR EXPT0079', 'TRAIT LIST FOR EXPT0079', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1096', 'TRAIT_LIST_EXPT0080', 'TRAIT LIST FOR EXPT0080', 'TRAIT LIST FOR EXPT0080', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1097', 'TRAIT_LIST_EXPT0081', 'TRAIT LIST FOR EXPT0081', 'TRAIT LIST FOR EXPT0081', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1098', 'TRAIT_LIST_EXPT0082', 'TRAIT LIST FOR EXPT0082', 'TRAIT LIST FOR EXPT0082', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1099', 'TRAIT_LIST_EXPT0083', 'TRAIT LIST FOR EXPT0083', 'TRAIT LIST FOR EXPT0083', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1100', 'TRAIT_LIST_EXPT0084', 'TRAIT LIST FOR EXPT0084', 'TRAIT LIST FOR EXPT0084', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1101', 'TRAIT_LIST_EXPT0085', 'TRAIT LIST FOR EXPT0085', 'TRAIT LIST FOR EXPT0085', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1102', 'TRAIT_LIST_EXPT0086', 'TRAIT LIST FOR EXPT0086', 'TRAIT LIST FOR EXPT0086', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1103', 'TRAIT_LIST_EXPT0087', 'TRAIT LIST FOR EXPT0087', 'TRAIT LIST FOR EXPT0087', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1104', 'TRAIT_LIST_EXPT0088', 'TRAIT LIST FOR EXPT0088', 'TRAIT LIST FOR EXPT0088', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1105', 'TRAIT_LIST_EXPT0089', 'TRAIT LIST FOR EXPT0089', 'TRAIT LIST FOR EXPT0089', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1106', 'TRAIT_LIST_EXPT0090', 'TRAIT LIST FOR EXPT0090', 'TRAIT LIST FOR EXPT0090', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1107', 'TRAIT_LIST_EXPT0091', 'TRAIT LIST FOR EXPT0091', 'TRAIT LIST FOR EXPT0091', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1108', 'TRAIT_LIST_EXPT0092', 'TRAIT LIST FOR EXPT0092', 'TRAIT LIST FOR EXPT0092', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1109', 'TRAIT_LIST_EXPT0093', 'TRAIT LIST FOR EXPT0093', 'TRAIT LIST FOR EXPT0093', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1110', 'TRAIT_LIST_EXPT0094', 'TRAIT LIST FOR EXPT0094', 'TRAIT LIST FOR EXPT0094', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1111', 'TRAIT_LIST_EXPT0095', 'TRAIT LIST FOR EXPT0095', 'TRAIT LIST FOR EXPT0095', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1112', 'TRAIT_LIST_EXPT0096', 'TRAIT LIST FOR EXPT0096', 'TRAIT LIST FOR EXPT0096', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1113', 'TRAIT_LIST_EXPT0097', 'TRAIT LIST FOR EXPT0097', 'TRAIT LIST FOR EXPT0097', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1114', 'TRAIT_LIST_EXPT0098', 'TRAIT LIST FOR EXPT0098', 'TRAIT LIST FOR EXPT0098', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1115', 'TRAIT_LIST_EXPT0099', 'TRAIT LIST FOR EXPT0099', 'TRAIT LIST FOR EXPT0099', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1116', 'TRAIT_LIST_EXPT0100', 'TRAIT LIST FOR EXPT0100', 'TRAIT LIST FOR EXPT0100', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1117', 'TRAIT_LIST_EXPT0101', 'TRAIT LIST FOR EXPT0101', 'TRAIT LIST FOR EXPT0101', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1118', 'TRAIT_LIST_EXPT0105', 'TRAIT LIST FOR EXPT0105', 'TRAIT LIST FOR EXPT0105', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1119', 'TRAIT_LIST_EXPT0106', 'TRAIT LIST FOR EXPT0106', 'TRAIT LIST FOR EXPT0106', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1120', 'TRAIT_LIST_EXPT0107', 'TRAIT LIST FOR EXPT0107', 'TRAIT LIST FOR EXPT0107', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1121', 'TRAIT_LIST_EXPT0108', 'TRAIT LIST FOR EXPT0108', 'TRAIT LIST FOR EXPT0108', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1122', 'TRAIT_LIST_EXPT0109', 'TRAIT LIST FOR EXPT0109', 'TRAIT LIST FOR EXPT0109', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1123', 'TRAIT_LIST_EXPT0110', 'TRAIT LIST FOR EXPT0110', 'TRAIT LIST FOR EXPT0110', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1124', 'TRAIT_LIST_EXPT0111', 'TRAIT LIST FOR EXPT0111', 'TRAIT LIST FOR EXPT0111', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1125', 'TRAIT_LIST_EXPT0112', 'TRAIT LIST FOR EXPT0112', 'TRAIT LIST FOR EXPT0112', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1126', 'TRAIT_LIST_EXPT0113', 'TRAIT LIST FOR EXPT0113', 'TRAIT LIST FOR EXPT0113', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1127', 'TRAIT_LIST_EXPT0114', 'TRAIT LIST FOR EXPT0114', 'TRAIT LIST FOR EXPT0114', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1128', 'TRAIT_LIST_EXPT0115', 'TRAIT LIST FOR EXPT0115', 'TRAIT LIST FOR EXPT0115', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1129', 'TRAIT_LIST_EXPT0116', 'TRAIT LIST FOR EXPT0116', 'TRAIT LIST FOR EXPT0116', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1130', 'TRAIT_LIST_EXPT0117', 'TRAIT LIST FOR EXPT0117', 'TRAIT LIST FOR EXPT0117', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1131', 'TRAIT_LIST_EXPT0118', 'TRAIT LIST FOR EXPT0118', 'TRAIT LIST FOR EXPT0118', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1132', 'TRAIT_LIST_EXPT0119', 'TRAIT LIST FOR EXPT0119', 'TRAIT LIST FOR EXPT0119', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1133', 'TRAIT_LIST_EXPT0120', 'TRAIT LIST FOR EXPT0120', 'TRAIT LIST FOR EXPT0120', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1134', 'TRAIT_LIST_EXPT0121', 'TRAIT LIST FOR EXPT0121', 'TRAIT LIST FOR EXPT0121', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1135', 'TRAIT_LIST_EXPT0122', 'TRAIT LIST FOR EXPT0122', 'TRAIT LIST FOR EXPT0122', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1136', 'TRAIT_LIST_EXPT0123', 'TRAIT LIST FOR EXPT0123', 'TRAIT LIST FOR EXPT0123', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1137', 'TRAIT_LIST_EXPT0125', 'TRAIT LIST FOR EXPT0125', 'TRAIT LIST FOR EXPT0125', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1138', 'TRAIT_LIST_EXPT0126', 'TRAIT LIST FOR EXPT0126', 'TRAIT LIST FOR EXPT0126', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1139', 'TRAIT_LIST_EXPT0127', 'TRAIT LIST FOR EXPT0127', 'TRAIT LIST FOR EXPT0127', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1140', 'TRAIT_LIST_EXPT0128', 'TRAIT LIST FOR EXPT0128', 'TRAIT LIST FOR EXPT0128', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1141', 'TRAIT_LIST_EXPT0130', 'TRAIT LIST FOR EXPT0130', 'TRAIT LIST FOR EXPT0130', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1142', 'TRAIT_LIST_EXPT0131', 'TRAIT LIST FOR EXPT0131', 'TRAIT LIST FOR EXPT0131', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1143', 'TRAIT_LIST_EXPT0132', 'TRAIT LIST FOR EXPT0132', 'TRAIT LIST FOR EXPT0132', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1144', 'TRAIT_LIST_EXPT0133', 'TRAIT LIST FOR EXPT0133', 'TRAIT LIST FOR EXPT0133', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1145', 'TRAIT_LIST_EXPT0134', 'TRAIT LIST FOR EXPT0134', 'TRAIT LIST FOR EXPT0134', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1146', 'TRAIT_LIST_EXPT0135', 'TRAIT LIST FOR EXPT0135', 'TRAIT LIST FOR EXPT0135', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1147', 'TRAIT_LIST_EXPT0136', 'TRAIT LIST FOR EXPT0136', 'TRAIT LIST FOR EXPT0136', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1148', 'TRAIT_LIST_EXPT0137', 'TRAIT LIST FOR EXPT0137', 'TRAIT LIST FOR EXPT0137', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1149', 'TRAIT_LIST_EXPT0138', 'TRAIT LIST FOR EXPT0138', 'TRAIT LIST FOR EXPT0138', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1150', 'TRAIT_LIST_EXPT0139', 'TRAIT LIST FOR EXPT0139', 'TRAIT LIST FOR EXPT0139', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1151', 'TRAIT_LIST_EXPT0140', 'TRAIT LIST FOR EXPT0140', 'TRAIT LIST FOR EXPT0140', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1152', 'TRAIT_LIST_EXPT0141', 'TRAIT LIST FOR EXPT0141', 'TRAIT LIST FOR EXPT0141', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1153', 'TRAIT_LIST_EXPT0142', 'TRAIT LIST FOR EXPT0142', 'TRAIT LIST FOR EXPT0142', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1154', 'TRAIT_LIST_EXPT0143', 'TRAIT LIST FOR EXPT0143', 'TRAIT LIST FOR EXPT0143', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1155', 'TRAIT_LIST_EXPT0144', 'TRAIT LIST FOR EXPT0144', 'TRAIT LIST FOR EXPT0144', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1156', 'TRAIT_LIST_EXPT0145', 'TRAIT LIST FOR EXPT0145', 'TRAIT LIST FOR EXPT0145', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1157', 'TRAIT_LIST_EXPT0146', 'TRAIT LIST FOR EXPT0146', 'TRAIT LIST FOR EXPT0146', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1158', 'TRAIT_LIST_EXPT0147', 'TRAIT LIST FOR EXPT0147', 'TRAIT LIST FOR EXPT0147', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1159', 'TRAIT_LIST_EXPT0148', 'TRAIT LIST FOR EXPT0148', 'TRAIT LIST FOR EXPT0148', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1160', 'TRAIT_LIST_EXPT0149', 'TRAIT LIST FOR EXPT0149', 'TRAIT LIST FOR EXPT0149', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1161', 'TRAIT_LIST_EXPT0150', 'TRAIT LIST FOR EXPT0150', 'TRAIT LIST FOR EXPT0150', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1162', 'TRAIT_LIST_EXPT0151', 'TRAIT LIST FOR EXPT0151', 'TRAIT LIST FOR EXPT0151', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1163', 'TRAIT_LIST_EXPT0152', 'TRAIT LIST FOR EXPT0152', 'TRAIT LIST FOR EXPT0152', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1164', 'TRAIT_LIST_EXPT0153', 'TRAIT LIST FOR EXPT0153', 'TRAIT LIST FOR EXPT0153', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1165', 'TRAIT_LIST_EXPT0154', 'TRAIT LIST FOR EXPT0154', 'TRAIT LIST FOR EXPT0154', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1166', 'TRAIT_LIST_EXPT0155', 'TRAIT LIST FOR EXPT0155', 'TRAIT LIST FOR EXPT0155', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1167', 'TRAIT_LIST_EXPT0156', 'TRAIT LIST FOR EXPT0156', 'TRAIT LIST FOR EXPT0156', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1168', 'TRAIT_LIST_EXPT0157', 'TRAIT LIST FOR EXPT0157', 'TRAIT LIST FOR EXPT0157', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1169', 'TRAIT_LIST_EXPT0158', 'TRAIT LIST FOR EXPT0158', 'TRAIT LIST FOR EXPT0158', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1170', 'TRAIT_LIST_EXPT0159', 'TRAIT LIST FOR EXPT0159', 'TRAIT LIST FOR EXPT0159', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1171', 'TRAIT_LIST_EXPT0162', 'TRAIT LIST FOR EXPT0162', 'TRAIT LIST FOR EXPT0162', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1172', 'TRAIT_LIST_EXPT0163', 'TRAIT LIST FOR EXPT0163', 'TRAIT LIST FOR EXPT0163', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1173', 'TRAIT_LIST_EXPT0165', 'TRAIT LIST FOR EXPT0165', 'TRAIT LIST FOR EXPT0165', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1174', 'TRAIT_LIST_EXPT0166', 'TRAIT LIST FOR EXPT0166', 'TRAIT LIST FOR EXPT0166', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1175', 'TRAIT_LIST_EXPT0167', 'TRAIT LIST FOR EXPT0167', 'TRAIT LIST FOR EXPT0167', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1176', 'TRAIT_LIST_EXPT0168', 'TRAIT LIST FOR EXPT0168', 'TRAIT LIST FOR EXPT0168', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1177', 'TRAIT_LIST_EXPT0169', 'TRAIT LIST FOR EXPT0169', 'TRAIT LIST FOR EXPT0169', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1178', 'TRAIT_LIST_EXPT0170', 'TRAIT LIST FOR EXPT0170', 'TRAIT LIST FOR EXPT0170', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1179', 'TRAIT_LIST_EXPT0171', 'TRAIT LIST FOR EXPT0171', 'TRAIT LIST FOR EXPT0171', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1180', 'TRAIT_LIST_EXPT0172', 'TRAIT LIST FOR EXPT0172', 'TRAIT LIST FOR EXPT0172', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1181', 'TRAIT_LIST_EXPT0173', 'TRAIT LIST FOR EXPT0173', 'TRAIT LIST FOR EXPT0173', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1182', 'TRAIT_LIST_EXPT0174', 'TRAIT LIST FOR EXPT0174', 'TRAIT LIST FOR EXPT0174', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1183', 'TRAIT_LIST_EXPT0175', 'TRAIT LIST FOR EXPT0175', 'TRAIT LIST FOR EXPT0175', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1184', 'TRAIT_LIST_EXPT0176', 'TRAIT LIST FOR EXPT0176', 'TRAIT LIST FOR EXPT0176', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1185', 'TRAIT_LIST_EXPT0177', 'TRAIT LIST FOR EXPT0177', 'TRAIT LIST FOR EXPT0177', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1186', 'TRAIT_LIST_EXPT0178', 'TRAIT LIST FOR EXPT0178', 'TRAIT LIST FOR EXPT0178', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1187', 'TRAIT_LIST_EXPT0179', 'TRAIT LIST FOR EXPT0179', 'TRAIT LIST FOR EXPT0179', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1188', 'TRAIT_LIST_EXPT0180', 'TRAIT LIST FOR EXPT0180', 'TRAIT LIST FOR EXPT0180', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1189', 'TRAIT_LIST_EXPT0181', 'TRAIT LIST FOR EXPT0181', 'TRAIT LIST FOR EXPT0181', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1190', 'TRAIT_LIST_EXPT0182', 'TRAIT LIST FOR EXPT0182', 'TRAIT LIST FOR EXPT0182', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1191', 'TRAIT_LIST_EXPT0183', 'TRAIT LIST FOR EXPT0183', 'TRAIT LIST FOR EXPT0183', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1192', 'TRAIT_LIST_EXPT0184', 'TRAIT LIST FOR EXPT0184', 'TRAIT LIST FOR EXPT0184', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1193', 'TRAIT_LIST_EXPT0185', 'TRAIT LIST FOR EXPT0185', 'TRAIT LIST FOR EXPT0185', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1194', 'TRAIT_LIST_EXPT0187', 'TRAIT LIST FOR EXPT0187', 'TRAIT LIST FOR EXPT0187', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1195', 'TRAIT_LIST_EXPT0188', 'TRAIT LIST FOR EXPT0188', 'TRAIT LIST FOR EXPT0188', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1196', 'TRAIT_LIST_EXPT0189', 'TRAIT LIST FOR EXPT0189', 'TRAIT LIST FOR EXPT0189', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1197', 'TRAIT_LIST_EXPT0190', 'TRAIT LIST FOR EXPT0190', 'TRAIT LIST FOR EXPT0190', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1198', 'TRAIT_LIST_EXPT0192', 'TRAIT LIST FOR EXPT0192', 'TRAIT LIST FOR EXPT0192', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1199', 'TRAIT_LIST_EXPT0193', 'TRAIT LIST FOR EXPT0193', 'TRAIT LIST FOR EXPT0193', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1200', 'TRAIT_LIST_EXPT0194', 'TRAIT LIST FOR EXPT0194', 'TRAIT LIST FOR EXPT0194', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1201', 'TRAIT_LIST_EXPT0195', 'TRAIT LIST FOR EXPT0195', 'TRAIT LIST FOR EXPT0195', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1202', 'TRAIT_LIST_EXPT0196', 'TRAIT LIST FOR EXPT0196', 'TRAIT LIST FOR EXPT0196', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1203', 'TRAIT_LIST_EXPT0197', 'TRAIT LIST FOR EXPT0197', 'TRAIT LIST FOR EXPT0197', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1204', 'TRAIT_LIST_EXPT0198', 'TRAIT LIST FOR EXPT0198', 'TRAIT LIST FOR EXPT0198', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1205', 'TRAIT_LIST_EXPT0199', 'TRAIT LIST FOR EXPT0199', 'TRAIT LIST FOR EXPT0199', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1206', 'TRAIT_LIST_EXPT0200', 'TRAIT LIST FOR EXPT0200', 'TRAIT LIST FOR EXPT0200', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1207', 'TRAIT_LIST_EXPT0202', 'TRAIT LIST FOR EXPT0202', 'TRAIT LIST FOR EXPT0202', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1208', 'TRAIT_LIST_EXPT0204', 'TRAIT LIST FOR EXPT0204', 'TRAIT LIST FOR EXPT0204', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1209', 'TRAIT_LIST_EXPT0205', 'TRAIT LIST FOR EXPT0205', 'TRAIT LIST FOR EXPT0205', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1210', 'TRAIT_LIST_EXPT0206', 'TRAIT LIST FOR EXPT0206', 'TRAIT LIST FOR EXPT0206', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1211', 'TRAIT_LIST_EXPT0207', 'TRAIT LIST FOR EXPT0207', 'TRAIT LIST FOR EXPT0207', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1212', 'TRAIT_LIST_EXPT0208', 'TRAIT LIST FOR EXPT0208', 'TRAIT LIST FOR EXPT0208', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1213', 'TRAIT_LIST_EXPT0209', 'TRAIT LIST FOR EXPT0209', 'TRAIT LIST FOR EXPT0209', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1214', 'TRAIT_LIST_EXPT0212', 'TRAIT LIST FOR EXPT0212', 'TRAIT LIST FOR EXPT0212', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1215', 'TRAIT_LIST_EXPT0213', 'TRAIT LIST FOR EXPT0213', 'TRAIT LIST FOR EXPT0213', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1216', 'TRAIT_LIST_EXPT0214', 'TRAIT LIST FOR EXPT0214', 'TRAIT LIST FOR EXPT0214', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1217', 'TRAIT_LIST_EXPT0215', 'TRAIT LIST FOR EXPT0215', 'TRAIT LIST FOR EXPT0215', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1218', 'TRAIT_LIST_EXPT0216', 'TRAIT LIST FOR EXPT0216', 'TRAIT LIST FOR EXPT0216', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1219', 'TRAIT_LIST_EXPT0217', 'TRAIT LIST FOR EXPT0217', 'TRAIT LIST FOR EXPT0217', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1220', 'TRAIT_LIST_EXPT0218', 'TRAIT LIST FOR EXPT0218', 'TRAIT LIST FOR EXPT0218', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1221', 'TRAIT_LIST_EXPT0219', 'TRAIT LIST FOR EXPT0219', 'TRAIT LIST FOR EXPT0219', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1222', 'TRAIT_LIST_EXPT0220', 'TRAIT LIST FOR EXPT0220', 'TRAIT LIST FOR EXPT0220', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1223', 'TRAIT_LIST_EXPT0221', 'TRAIT LIST FOR EXPT0221', 'TRAIT LIST FOR EXPT0221', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1224', 'TRAIT_LIST_EXPT0222', 'TRAIT LIST FOR EXPT0222', 'TRAIT LIST FOR EXPT0222', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1225', 'TRAIT_LIST_EXPT0223', 'TRAIT LIST FOR EXPT0223', 'TRAIT LIST FOR EXPT0223', 'trait protocol', '12', '1', 'created', 'true');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, creator_id, status, is_active)
VALUES ('1226', 'TRAIT_LIST_EXPT0224', 'TRAIT LIST FOR EXPT0224', 'TRAIT LIST FOR EXPT0224', 'trait protocol', '12', '1', 'created', 'true');


-- insert to platform.list_member
INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193893', '1040', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193894', '1040', '21', '2', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193895', '1040', '23', '3', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193896', '1040', '22', '4', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193897', '1040', '24', '5', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193898', '1040', '41', '6', 'BPH GH SES4 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193899', '1040', '157', '7', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193900', '1040', '78', '8', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193901', '1040', '139', '9', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193902', '1040', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193903', '1040', '183', '11', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193904', '1040', '181', '12', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193905', '1040', '180', '13', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193906', '1040', '142', '14', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193907', '1040', '293', '15', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193908', '1040', '413', '16', 'YLD_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193909', '1041', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193910', '1041', '75', '2', 'CL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193911', '1041', '675', '3', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193912', '1041', '676', '4', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193913', '1041', '677', '5', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193914', '1041', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193915', '1041', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193916', '1041', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193917', '1041', '650', '9', 'PnL 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193918', '1041', '651', '10', 'PnL 2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193919', '1041', '652', '11', 'PnL 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193920', '1041', '198', '12', 'PnL CM', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193921', '1041', '287', '13', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193922', '1041', '288', '14', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193923', '1041', '289', '15', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193924', '1041', '285', '16', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193925', '1042', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193926', '1042', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193927', '1042', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193928', '1042', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193929', '1042', '41', '5', 'BPH GH SES4 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193930', '1042', '157', '6', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193931', '1042', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193932', '1042', '139', '8', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193933', '1042', '212', '9', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193934', '1042', '180', '10', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193935', '1042', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193936', '1043', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193937', '1044', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193938', '1044', '21', '2', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193939', '1044', '23', '3', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193940', '1044', '22', '4', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193941', '1044', '24', '5', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193942', '1044', '41', '6', 'BPH GH SES4 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193943', '1044', '157', '7', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193944', '1044', '78', '8', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193945', '1044', '139', '9', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193946', '1044', '212', '10', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193947', '1044', '213', '11', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193948', '1044', '214', '12', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193949', '1044', '215', '13', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193950', '1044', '182', '14', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193951', '1044', '183', '15', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193952', '1044', '181', '16', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193953', '1044', '180', '17', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193954', '1044', '142', '18', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193955', '1044', '293', '19', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193956', '1044', '413', '20', 'YLD_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193957', '1045', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193958', '1045', '75', '2', 'CL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193959', '1045', '675', '3', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193960', '1045', '676', '4', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193961', '1045', '677', '5', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193962', '1045', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193963', '1045', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193964', '1045', '183', '8', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193965', '1045', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193966', '1045', '650', '10', 'PnL 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193967', '1045', '651', '11', 'PnL 2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193968', '1045', '652', '12', 'PnL 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193969', '1045', '198', '13', 'PnL CM', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193970', '1046', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193971', '1046', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193972', '1046', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193973', '1046', '183', '4', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193974', '1047', '639', '1', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193975', '1047', '644', '2', 'SELECTION TYPE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193976', '1048', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193977', '1048', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193978', '1048', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193979', '1048', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193980', '1048', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193981', '1049', '75', '1', 'CL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193982', '1049', '675', '2', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193983', '1049', '676', '3', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193984', '1049', '677', '4', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193985', '1049', '678', '5', 'CL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193986', '1049', '679', '6', 'CL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193987', '1049', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193988', '1049', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193989', '1049', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193990', '1049', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193991', '1049', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193992', '1049', '216', '12', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193993', '1049', '585', '13', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193994', '1049', '180', '14', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193995', '1049', '655', '15', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193996', '1049', '656', '16', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193997', '1049', '657', '17', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193998', '1049', '658', '18', 'PANNO4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('193999', '1049', '659', '19', 'PANNO5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194000', '1049', '429', '20', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194001', '1049', '650', '21', 'PnL 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194002', '1049', '651', '22', 'PnL 2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194003', '1049', '652', '23', 'PnL 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194004', '1049', '653', '24', 'PnL 4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194005', '1049', '654', '25', 'PnL 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194006', '1049', '198', '26', 'PnL CM', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194007', '1050', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194008', '1050', '212', '2', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194009', '1050', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194010', '1050', '214', '4', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194011', '1050', '215', '5', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194012', '1050', '216', '6', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194013', '1050', '180', '7', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194014', '1050', '287', '8', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194015', '1050', '288', '9', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194016', '1050', '289', '10', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194017', '1050', '290', '11', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194018', '1050', '285', '12', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194019', '1051', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194020', '1051', '212', '2', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194021', '1051', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194022', '1051', '214', '4', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194023', '1051', '215', '5', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194024', '1051', '180', '6', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194025', '1052', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194026', '1052', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194027', '1052', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194028', '1052', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194029', '1052', '75', '5', 'CL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194030', '1052', '675', '6', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194031', '1052', '684', '7', 'CL10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194032', '1052', '676', '8', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194033', '1052', '677', '9', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194034', '1052', '678', '10', 'CL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194035', '1052', '679', '11', 'CL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194036', '1052', '680', '12', 'CL6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194037', '1052', '681', '13', 'CL7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194038', '1052', '682', '14', 'CL8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194039', '1052', '683', '15', 'CL9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194040', '1052', '78', '16', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194041', '1052', '180', '17', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194042', '1052', '655', '18', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194043', '1052', '667', '19', 'PANNO10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194044', '1052', '656', '20', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194045', '1052', '657', '21', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194046', '1052', '658', '22', 'PANNO4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194047', '1052', '659', '23', 'PANNO5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194048', '1052', '663', '24', 'PANNO6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194049', '1052', '664', '25', 'PANNO7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194050', '1052', '665', '26', 'PANNO8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194051', '1052', '666', '27', 'PANNO9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194052', '1052', '429', '28', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194053', '1052', '650', '29', 'PnL 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194054', '1052', '651', '30', 'PnL 2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194055', '1052', '652', '31', 'PnL 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194056', '1052', '653', '32', 'PnL 4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194057', '1052', '654', '33', 'PnL 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194058', '1052', '668', '34', 'PnL 6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194059', '1052', '669', '35', 'PnL 7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194060', '1052', '670', '36', 'PnL 8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194061', '1052', '671', '37', 'PnL 9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194062', '1052', '198', '38', 'PnL CM', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194063', '1052', '672', '39', 'PnL10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194064', '1053', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194065', '1053', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194066', '1053', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194067', '1053', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194068', '1053', '41', '5', 'BPH GH SES4 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194069', '1053', '157', '6', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194070', '1053', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194071', '1053', '139', '8', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194072', '1053', '212', '9', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194073', '1053', '213', '10', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194074', '1053', '214', '11', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194075', '1053', '215', '12', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194076', '1053', '216', '13', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194077', '1053', '585', '14', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194078', '1053', '180', '15', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194079', '1054', '75', '1', 'CL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194080', '1054', '675', '2', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194081', '1054', '676', '3', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194082', '1054', '677', '4', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194083', '1054', '678', '5', 'CL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194084', '1054', '679', '6', 'CL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194085', '1054', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194086', '1054', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194087', '1054', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194088', '1054', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194089', '1054', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194090', '1054', '216', '12', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194091', '1054', '585', '13', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194092', '1054', '180', '14', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194093', '1054', '287', '15', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194094', '1054', '288', '16', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194095', '1054', '289', '17', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194096', '1054', '290', '18', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194097', '1054', '291', '19', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194098', '1054', '285', '20', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194099', '1055', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194100', '1055', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194101', '1055', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194102', '1055', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194103', '1056', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194104', '1056', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194105', '1056', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194106', '1057', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194107', '1057', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194108', '1057', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194109', '1057', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194110', '1057', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194111', '1057', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194112', '1057', '177', '7', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194113', '1057', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194114', '1057', '639', '9', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194115', '1058', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194116', '1058', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194117', '1058', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194118', '1058', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194119', '1058', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194120', '1058', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194121', '1058', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194122', '1058', '178', '8', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194123', '1058', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194124', '1058', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194125', '1058', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194126', '1058', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194127', '1058', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194128', '1059', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194129', '1059', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194130', '1059', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194131', '1059', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194132', '1059', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194133', '1059', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194134', '1059', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194135', '1059', '287', '8', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194136', '1059', '288', '9', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194137', '1059', '289', '10', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194138', '1059', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194139', '1059', '413', '12', 'YLD_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194140', '1060', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194141', '1060', '212', '2', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194142', '1060', '632', '3', 'TILLER AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194143', '1061', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194144', '1061', '213', '2', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194145', '1061', '214', '3', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194146', '1061', '215', '4', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194147', '1062', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194148', '1062', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194149', '1062', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194150', '1062', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194151', '1062', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194152', '1062', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194153', '1062', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194154', '1062', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194155', '1063', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194156', '1063', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194157', '1063', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194158', '1064', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194159', '1064', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194160', '1064', '178', '3', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194161', '1064', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194162', '1064', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194163', '1065', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194164', '1065', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194165', '1065', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194166', '1065', '591', '4', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194167', '1066', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194168', '1066', '178', '2', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194169', '1066', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194170', '1066', '235', '4', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194171', '1067', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194172', '1067', '178', '2', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194173', '1067', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194174', '1067', '235', '4', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194175', '1068', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194176', '1068', '21', '2', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194177', '1068', '23', '3', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194178', '1068', '22', '4', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194179', '1068', '24', '5', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194180', '1068', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194181', '1068', '212', '7', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194182', '1068', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194183', '1068', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194184', '1068', '639', '10', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194185', '1068', '235', '11', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194186', '1069', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194187', '1069', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194188', '1069', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194189', '1069', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194190', '1069', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194191', '1069', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194192', '1069', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194193', '1069', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194194', '1069', '178', '9', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194195', '1069', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194196', '1069', '591', '11', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194197', '1069', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194198', '1069', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194199', '1069', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194200', '1069', '290', '15', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194201', '1069', '291', '16', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194202', '1070', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194203', '1070', '213', '2', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194204', '1070', '214', '3', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194205', '1070', '215', '4', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194206', '1070', '216', '5', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194207', '1070', '585', '6', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194208', '1070', '287', '7', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194209', '1070', '288', '8', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194210', '1070', '289', '9', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194211', '1070', '290', '10', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194212', '1070', '291', '11', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194213', '1071', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194214', '1071', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194215', '1071', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194216', '1071', '214', '4', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194217', '1071', '215', '5', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194218', '1072', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194219', '1072', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194220', '1072', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194221', '1072', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194222', '1072', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194223', '1072', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194224', '1072', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194225', '1072', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194226', '1072', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194227', '1072', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194228', '1072', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194229', '1072', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194230', '1072', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194231', '1072', '290', '14', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194232', '1072', '291', '15', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194233', '1073', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194234', '1073', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194235', '1073', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194236', '1073', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194237', '1073', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194238', '1073', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194239', '1073', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194240', '1073', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194241', '1073', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194242', '1073', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194243', '1073', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194244', '1073', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194245', '1073', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194246', '1073', '290', '14', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194247', '1073', '291', '15', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194248', '1074', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194249', '1074', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194250', '1074', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194251', '1074', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194252', '1075', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194253', '1075', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194254', '1075', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194255', '1075', '1571', '4', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194256', '1075', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194257', '1075', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194258', '1075', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194259', '1075', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194260', '1075', '183', '9', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194261', '1075', '180', '10', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194262', '1075', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194263', '1075', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194264', '1075', '285', '13', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194265', '1076', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194266', '1076', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194267', '1076', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194268', '1076', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194269', '1076', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194270', '1077', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194271', '1077', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194272', '1077', '6', '3', 'Amy', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194273', '1077', '1307', '4', 'CLK 0-10 PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194274', '1077', '55', '5', 'CLK 0_9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194275', '1077', '1308', '6', 'CLK 10-25 PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194276', '1077', '1309', '7', 'CLK 25-50 PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194277', '1077', '1310', '8', 'CLK 50-75 PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194278', '1077', '1311', '9', 'CLK GT 75 PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194279', '1077', '1306', '10', 'CLK PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194280', '1077', '78', '11', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194281', '1077', '400', '12', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194282', '1077', '118', '13', 'GELC PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194283', '1077', '122', '14', 'GELT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194284', '1077', '1312', '15', 'GRL STDDEV CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194285', '1077', '1313', '16', 'GRW STDDEV CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194286', '1077', '128', '17', 'GrL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194287', '1077', '134', '18', 'GrW MM', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194288', '1077', '531', '19', 'HDR PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194289', '1077', '213', '20', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194290', '1077', '214', '21', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194291', '1077', '215', '22', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194292', '1077', '182', '23', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194293', '1077', '713', '24', 'MPS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194294', '1077', '142', '25', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194295', '1077', '655', '26', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194296', '1077', '656', '27', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194297', '1077', '657', '28', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194298', '1077', '1317', '29', 'PBR PERC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194299', '1077', '532', '30', 'PERCENT MILLED RICE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194300', '1077', '1303', '31', 'TOT GRN CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194301', '1077', '1316', '32', 'WBR CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194302', '1077', '1319', '33', 'WHDR CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194303', '1077', '1318', '34', 'WMR CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194304', '1077', '1315', '35', 'WPDY CONT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194305', '1078', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194306', '1078', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194307', '1078', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194308', '1078', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194309', '1078', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194310', '1079', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194311', '1079', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194312', '1079', '628', '3', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194313', '1079', '1440', '4', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194314', '1079', '590', '5', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194315', '1079', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194316', '1079', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194317', '1079', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194318', '1079', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194319', '1079', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194320', '1079', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194321', '1079', '216', '12', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194322', '1079', '585', '13', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194323', '1079', '182', '14', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194324', '1079', '591', '15', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194325', '1079', '183', '16', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194326', '1079', '180', '17', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194327', '1079', '142', '18', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194328', '1079', '619', '19', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194329', '1079', '617', '20', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194330', '1079', '618', '21', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194331', '1079', '287', '22', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194332', '1079', '288', '23', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194333', '1079', '289', '24', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194334', '1079', '290', '25', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194335', '1079', '291', '26', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194336', '1079', '285', '27', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194337', '1079', '1143', '28', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194338', '1079', '754', '29', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194339', '1079', '1475', '30', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194340', '1080', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194341', '1080', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194342', '1080', '628', '3', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194343', '1080', '1440', '4', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194344', '1080', '590', '5', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194345', '1080', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194346', '1080', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194347', '1080', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194348', '1080', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194349', '1080', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194350', '1080', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194351', '1080', '216', '12', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194352', '1080', '585', '13', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194353', '1080', '182', '14', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194354', '1080', '591', '15', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194355', '1080', '183', '16', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194356', '1080', '180', '17', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194357', '1080', '142', '18', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194358', '1080', '619', '19', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194359', '1080', '617', '20', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194360', '1080', '618', '21', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194361', '1080', '287', '22', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194362', '1080', '288', '23', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194363', '1080', '289', '24', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194364', '1080', '290', '25', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194365', '1080', '291', '26', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194366', '1080', '285', '27', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194367', '1080', '1143', '28', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194368', '1080', '754', '29', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194369', '1080', '1475', '30', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194370', '1081', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194371', '1081', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194372', '1081', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194373', '1081', '105', '4', 'FSm 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194374', '1081', '182', '5', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194375', '1081', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194376', '1082', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194377', '1082', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194378', '1082', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194379', '1082', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194380', '1083', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194381', '1083', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194382', '1083', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194383', '1083', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194384', '1084', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194385', '1084', '213', '2', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194386', '1084', '287', '3', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194387', '1085', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194388', '1085', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194389', '1085', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194390', '1085', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194391', '1085', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194392', '1085', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194393', '1085', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194394', '1085', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194395', '1085', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194396', '1086', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194397', '1086', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194398', '1086', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194399', '1086', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194400', '1086', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194401', '1086', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194402', '1086', '214', '7', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194403', '1086', '215', '8', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194404', '1086', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194405', '1086', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194406', '1087', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194407', '1087', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194408', '1087', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194409', '1087', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194410', '1087', '182', '5', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194411', '1087', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194412', '1088', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194413', '1088', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194414', '1088', '142', '3', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194415', '1089', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194416', '1089', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194417', '1089', '142', '3', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194418', '1089', '639', '4', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194419', '1090', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194420', '1090', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194421', '1091', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194422', '1091', '675', '2', 'CL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194423', '1091', '676', '3', 'CL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194424', '1091', '677', '4', 'CL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194425', '1091', '678', '5', 'CL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194426', '1091', '679', '6', 'CL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194427', '1091', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194428', '1091', '213', '8', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194429', '1091', '214', '9', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194430', '1091', '215', '10', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194431', '1091', '216', '11', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194432', '1091', '585', '12', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194433', '1091', '182', '13', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194434', '1091', '142', '14', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194435', '1091', '650', '15', 'PnL 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194436', '1091', '651', '16', 'PnL 2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194437', '1091', '652', '17', 'PnL 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194438', '1091', '653', '18', 'PnL 4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194439', '1091', '654', '19', 'PnL 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194440', '1091', '235', '20', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194441', '1091', '287', '21', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194442', '1091', '288', '22', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194443', '1091', '289', '23', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194444', '1091', '290', '24', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194445', '1091', '291', '25', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194446', '1092', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194447', '1092', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194448', '1092', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194449', '1092', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194450', '1092', '639', '5', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194451', '1093', '1436', '1', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194452', '1093', '639', '2', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194453', '1094', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194454', '1094', '1943', '2', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194455', '1094', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194456', '1094', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194457', '1094', '183', '5', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194458', '1094', '181', '6', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194459', '1094', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194460', '1094', '293', '8', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194461', '1094', '1756', '9', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194462', '1095', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194463', '1095', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194464', '1095', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194465', '1095', '105', '4', 'FSm 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194466', '1095', '177', '5', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194467', '1095', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194468', '1095', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194469', '1096', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194470', '1096', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194471', '1096', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194472', '1096', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194473', '1096', '180', '5', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194474', '1096', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194475', '1097', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194476', '1097', '1943', '2', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194477', '1097', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194478', '1097', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194479', '1097', '183', '5', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194480', '1097', '181', '6', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194481', '1097', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194482', '1097', '293', '8', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194483', '1097', '1756', '9', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194484', '1098', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194485', '1098', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194486', '1098', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194487', '1098', '105', '4', 'FSm 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194488', '1098', '177', '5', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194489', '1098', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194490', '1098', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194491', '1099', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194492', '1099', '212', '2', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194493', '1099', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194494', '1099', '182', '4', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194495', '1099', '183', '5', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194496', '1099', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194497', '1100', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194498', '1100', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194499', '1100', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194500', '1100', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194501', '1101', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194502', '1101', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194503', '1101', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194504', '1101', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194505', '1101', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194506', '1101', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194507', '1101', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194508', '1101', '216', '8', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194509', '1101', '585', '9', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194510', '1101', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194511', '1101', '591', '11', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194512', '1101', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194513', '1101', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194514', '1101', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194515', '1101', '290', '15', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194516', '1101', '291', '16', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194517', '1102', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194518', '1102', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194519', '1102', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194520', '1102', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194521', '1102', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194522', '1102', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194523', '1102', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194524', '1102', '216', '8', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194525', '1102', '585', '9', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194526', '1102', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194527', '1102', '591', '11', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194528', '1102', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194529', '1102', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194530', '1102', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194531', '1102', '290', '15', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194532', '1102', '291', '16', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194533', '1103', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194534', '1103', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194535', '1103', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194536', '1103', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194537', '1103', '1163', '5', 'BPH GH SES5 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194538', '1103', '157', '6', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194539', '1103', '139', '7', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194540', '1104', '21', '1', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194541', '1104', '23', '2', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194542', '1104', '22', '3', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194543', '1104', '24', '4', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194544', '1104', '1163', '5', 'BPH GH SES5 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194545', '1104', '157', '6', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194546', '1104', '139', '7', 'GLH 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194547', '1105', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194548', '1105', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194549', '1105', '142', '3', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194550', '1105', '639', '4', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194551', '1106', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194552', '1106', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194553', '1106', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194554', '1106', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194555', '1106', '639', '5', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194556', '1107', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194557', '1107', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194558', '1107', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194559', '1107', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194560', '1108', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194561', '1108', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194562', '1108', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194563', '1108', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194564', '1108', '639', '5', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194565', '1109', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194566', '1109', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194567', '1109', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194568', '1109', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194569', '1109', '182', '5', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194570', '1109', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194571', '1109', '639', '7', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194572', '1110', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194573', '1110', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194574', '1110', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194575', '1110', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194576', '1110', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194577', '1110', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194578', '1110', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194579', '1110', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194580', '1110', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194581', '1110', '288', '10', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194582', '1110', '289', '11', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194583', '1110', '285', '12', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194584', '1110', '1143', '13', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194585', '1111', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194586', '1111', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194587', '1111', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194588', '1111', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194589', '1111', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194590', '1111', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194591', '1111', '214', '7', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194592', '1111', '215', '8', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194593', '1111', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194594', '1111', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194595', '1111', '235', '11', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194596', '1111', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194597', '1111', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194598', '1111', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194599', '1111', '285', '15', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194600', '1112', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194601', '1112', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194602', '1112', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194603', '1112', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194604', '1112', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194605', '1112', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194606', '1112', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194607', '1112', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194608', '1112', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194609', '1112', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194610', '1112', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194611', '1112', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194612', '1112', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194613', '1112', '290', '14', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194614', '1112', '291', '15', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194615', '1112', '285', '16', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194616', '1113', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194617', '1113', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194618', '1113', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194619', '1113', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194620', '1113', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194621', '1113', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194622', '1113', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194623', '1113', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194624', '1113', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194625', '1113', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194626', '1113', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194627', '1113', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194628', '1113', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194629', '1113', '290', '14', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194630', '1113', '291', '15', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194631', '1113', '285', '16', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194632', '1114', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194633', '1114', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194634', '1114', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194635', '1114', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194636', '1114', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194637', '1114', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194638', '1114', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194639', '1114', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194640', '1114', '591', '9', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194641', '1114', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194642', '1114', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194643', '1114', '288', '12', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194644', '1114', '289', '13', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194645', '1114', '285', '14', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194646', '1114', '1143', '15', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194647', '1115', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194648', '1115', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194649', '1115', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194650', '1115', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194651', '1115', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194652', '1115', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194653', '1115', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194654', '1115', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194655', '1115', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194656', '1115', '655', '10', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194657', '1115', '656', '11', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194658', '1115', '657', '12', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194659', '1116', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194660', '1116', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194661', '1116', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194662', '1116', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194663', '1116', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194664', '1116', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194665', '1116', '214', '7', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194666', '1116', '215', '8', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194667', '1116', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194668', '1116', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194669', '1116', '235', '11', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194670', '1116', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194671', '1116', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194672', '1116', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194673', '1116', '285', '15', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194674', '1117', '694', '1', 'SubTol 4DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194675', '1117', '697', '2', 'SubTol 7DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194676', '1118', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194677', '1118', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194678', '1118', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194679', '1118', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194680', '1118', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194681', '1118', '177', '6', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194682', '1118', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194683', '1118', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194684', '1118', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194685', '1118', '285', '10', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194686', '1119', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194687', '1119', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194688', '1119', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194689', '1119', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194690', '1119', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194691', '1119', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194692', '1119', '177', '7', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194693', '1119', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194694', '1119', '181', '9', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194695', '1119', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194696', '1119', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194697', '1119', '1475', '12', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194698', '1120', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194699', '1120', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194700', '1120', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194701', '1120', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194702', '1120', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194703', '1120', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194704', '1120', '1572', '7', 'Ldg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194705', '1120', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194706', '1120', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194707', '1120', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194708', '1120', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194709', '1121', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194710', '1121', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194711', '1121', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194712', '1121', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194713', '1121', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194714', '1121', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194715', '1121', '1572', '7', 'Ldg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194716', '1121', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194717', '1121', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194718', '1121', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194719', '1121', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194720', '1122', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194721', '1122', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194722', '1122', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194723', '1122', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194724', '1122', '293', '5', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194725', '1123', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194726', '1123', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194727', '1123', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194728', '1123', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194729', '1123', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194730', '1123', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194731', '1123', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194732', '1123', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194733', '1123', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194734', '1123', '655', '10', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194735', '1123', '656', '11', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194736', '1123', '657', '12', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194737', '1123', '429', '13', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194738', '1124', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194739', '1124', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194740', '1124', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194741', '1124', '1573', '4', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194742', '1124', '1571', '5', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194743', '1124', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194744', '1124', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194745', '1124', '591', '8', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194746', '1124', '1570', '9', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194747', '1124', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194748', '1124', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194749', '1124', '1470', '12', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194750', '1125', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194751', '1126', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194752', '1126', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194753', '1126', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194754', '1126', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194755', '1126', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194756', '1126', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194757', '1126', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194758', '1126', '1573', '8', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194759', '1126', '1571', '9', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194760', '1126', '78', '10', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194761', '1126', '212', '11', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194762', '1126', '213', '12', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194763', '1126', '591', '13', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194764', '1126', '1570', '14', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194765', '1126', '2136', '15', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194766', '1126', '1470', '16', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194767', '1127', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194768', '1127', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194769', '1127', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194770', '1127', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194771', '1127', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194772', '1127', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194773', '1127', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194774', '1127', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194775', '1127', '177', '9', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194776', '1127', '180', '10', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194777', '1127', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194778', '1127', '619', '12', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194779', '1127', '617', '13', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194780', '1127', '618', '14', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194781', '1127', '287', '15', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194782', '1127', '288', '16', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194783', '1127', '289', '17', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194784', '1127', '290', '18', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194785', '1127', '291', '19', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194786', '1127', '301', '20', 'VVg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194787', '1128', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194788', '1129', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194789', '1129', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194790', '1129', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194791', '1129', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194792', '1129', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194793', '1129', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194794', '1129', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194795', '1129', '1573', '8', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194796', '1129', '1571', '9', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194797', '1129', '78', '10', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194798', '1129', '212', '11', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194799', '1129', '213', '12', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194800', '1129', '591', '13', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194801', '1129', '1570', '14', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194802', '1129', '2136', '15', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194803', '1129', '1470', '16', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194804', '1130', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194805', '1130', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194806', '1130', '212', '3', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194807', '1130', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194808', '1130', '214', '5', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194809', '1130', '215', '6', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194810', '1130', '216', '7', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194811', '1130', '585', '8', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194812', '1130', '177', '9', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194813', '1130', '180', '10', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194814', '1130', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194815', '1130', '619', '12', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194816', '1130', '617', '13', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194817', '1130', '618', '14', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194818', '1130', '287', '15', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194819', '1130', '288', '16', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194820', '1130', '289', '17', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194821', '1130', '290', '18', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194822', '1130', '291', '19', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194823', '1130', '301', '20', 'VVg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194824', '1131', '1573', '1', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194825', '1131', '1571', '2', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194826', '1131', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194827', '1131', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194828', '1131', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194829', '1131', '178', '6', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194830', '1131', '180', '7', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194831', '1131', '1570', '8', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194832', '1131', '225', '9', 'Ri 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194833', '1131', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194834', '1131', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194835', '1132', '1573', '1', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194836', '1132', '1571', '2', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194837', '1132', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194838', '1132', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194839', '1132', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194840', '1132', '178', '6', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194841', '1132', '180', '7', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194842', '1132', '1570', '8', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194843', '1132', '225', '9', 'Ri 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194844', '1132', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194845', '1132', '285', '11', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194846', '1133', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194847', '1133', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194848', '1133', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194849', '1133', '1573', '4', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194850', '1133', '1571', '5', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194851', '1133', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194852', '1133', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194853', '1133', '591', '8', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194854', '1133', '1570', '9', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194855', '1133', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194856', '1133', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194857', '1133', '1470', '12', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194858', '1134', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194859', '1134', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194860', '1134', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194861', '1134', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194862', '1134', '293', '5', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194863', '1135', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194864', '1135', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194865', '1135', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194866', '1135', '287', '4', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194867', '1136', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194868', '1136', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194869', '1136', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194870', '1136', '1431', '4', 'HT10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194871', '1136', '1428', '5', 'HT7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194872', '1136', '1429', '6', 'HT8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194873', '1136', '1430', '7', 'HT9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194874', '1136', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194875', '1136', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194876', '1136', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194877', '1136', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194878', '1136', '216', '12', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194879', '1136', '585', '13', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194880', '1136', '586', '14', 'Ht6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194881', '1136', '177', '15', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194882', '1136', '182', '16', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194883', '1136', '142', '17', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194884', '1136', '639', '18', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194885', '1136', '655', '19', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194886', '1136', '667', '20', 'PANNO10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194887', '1136', '656', '21', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194888', '1136', '657', '22', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194889', '1136', '658', '23', 'PANNO4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194890', '1136', '659', '24', 'PANNO5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194891', '1136', '663', '25', 'PANNO6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194892', '1136', '664', '26', 'PANNO7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194893', '1136', '665', '27', 'PANNO8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194894', '1136', '666', '28', 'PANNO9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194895', '1136', '429', '29', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194896', '1136', '287', '30', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194897', '1136', '1141', '31', 'TILL10', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194898', '1136', '288', '32', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194899', '1136', '289', '33', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194900', '1136', '290', '34', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194901', '1136', '291', '35', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194902', '1136', '292', '36', 'TILL6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194903', '1136', '1138', '37', 'TILL7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194904', '1136', '1139', '38', 'TILL8', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194905', '1136', '1140', '39', 'TILL9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194906', '1136', '285', '40', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194907', '1137', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194908', '1137', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194909', '1137', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194910', '1137', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194911', '1137', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194912', '1137', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194913', '1137', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194914', '1137', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194915', '1137', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194916', '1137', '655', '10', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194917', '1137', '656', '11', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194918', '1137', '657', '12', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194919', '1137', '429', '13', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194920', '1138', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194921', '1138', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194922', '1138', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194923', '1138', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194924', '1138', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194925', '1138', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194926', '1138', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194927', '1138', '177', '8', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194928', '1138', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194929', '1138', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194930', '1138', '655', '11', 'PANNO1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194931', '1138', '656', '12', 'PANNO2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194932', '1138', '657', '13', 'PANNO3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194933', '1138', '429', '14', 'PANNO_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194934', '1139', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194935', '1140', '403', '1', 'DTH', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194936', '1141', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194937', '1141', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194938', '1141', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194939', '1142', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194940', '1142', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194941', '1142', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194942', '1142', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194943', '1142', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194944', '1142', '177', '6', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194945', '1142', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194946', '1142', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194947', '1142', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194948', '1142', '285', '10', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194949', '1143', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194950', '1143', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194951', '1143', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194952', '1143', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194953', '1143', '293', '5', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194954', '1144', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194955', '1144', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194956', '1144', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194957', '1144', '183', '4', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194958', '1144', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194959', '1144', '2136', '6', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194960', '1144', '1756', '7', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194961', '1145', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194962', '1145', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194963', '1145', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194964', '1145', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194965', '1145', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194966', '1145', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194967', '1145', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194968', '1145', '1573', '8', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194969', '1145', '1571', '9', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194970', '1145', '78', '10', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194971', '1145', '213', '11', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194972', '1145', '591', '12', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194973', '1145', '1570', '13', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194974', '1145', '2136', '14', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194975', '1145', '1470', '15', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194976', '1146', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194977', '1146', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194978', '1146', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194979', '1146', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194980', '1146', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194981', '1146', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194982', '1146', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194983', '1146', '1573', '8', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194984', '1146', '1571', '9', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194985', '1146', '78', '10', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194986', '1146', '213', '11', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194987', '1146', '591', '12', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194988', '1146', '1570', '13', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194989', '1146', '2136', '14', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194990', '1146', '1470', '15', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194991', '1147', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194992', '1147', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194993', '1147', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194994', '1147', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194995', '1147', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194996', '1147', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194997', '1147', '177', '7', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194998', '1147', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('194999', '1147', '181', '9', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195000', '1147', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195001', '1147', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195002', '1147', '1475', '12', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195003', '1148', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195004', '1148', '1440', '2', 'AYLD_KG_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195005', '1148', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195006', '1148', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195007', '1148', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195008', '1148', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195009', '1148', '177', '7', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195010', '1148', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195011', '1148', '181', '9', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195012', '1148', '142', '10', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195013', '1148', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195014', '1148', '1475', '12', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195015', '1149', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195016', '1149', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195017', '1149', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195018', '1149', '142', '4', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195019', '1149', '293', '5', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195020', '1150', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195021', '1150', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195022', '1150', '182', '3', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195023', '1150', '183', '4', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195024', '1150', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195025', '1150', '2136', '6', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195026', '1150', '1756', '7', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195027', '1151', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195028', '1152', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195029', '1153', '1754', '1', 'SubTol 14DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195030', '1153', '697', '2', 'SubTol 7DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195031', '1154', '694', '1', 'SubTol 4DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195032', '1154', '697', '2', 'SubTol 7DAD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195033', '1155', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195034', '1155', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195035', '1155', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195036', '1155', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195037', '1155', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195038', '1155', '639', '6', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195039', '1156', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195040', '1156', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195041', '1156', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195042', '1156', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195043', '1156', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195044', '1156', '639', '6', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195045', '1157', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195046', '1157', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195047', '1157', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195048', '1157', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195049', '1157', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195050', '1157', '639', '6', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195051', '1158', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195052', '1158', '182', '2', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195053', '1159', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195054', '1159', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195055', '1159', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195056', '1159', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195057', '1159', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195058', '1159', '177', '6', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195059', '1159', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195060', '1159', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195061', '1159', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195062', '1159', '285', '10', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195063', '1160', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195064', '1160', '1573', '2', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195065', '1160', '1889', '3', 'Corrected AYLD(G)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195066', '1160', '1890', '4', 'Corrected AYLD(G)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195067', '1160', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195068', '1160', '400', '6', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195069', '1160', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195070', '1160', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195071', '1160', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195072', '1160', '235', '10', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195073', '1160', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195074', '1160', '2136', '12', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195075', '1160', '1944', '13', 'YLD_CONT3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195076', '1161', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195077', '1161', '1943', '2', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195078', '1161', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195079', '1161', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195080', '1161', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195081', '1161', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195082', '1161', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195083', '1161', '2136', '8', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195084', '1161', '1756', '9', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195085', '1162', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195086', '1162', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195087', '1162', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195088', '1162', '1241', '4', 'Density', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195089', '1162', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195090', '1162', '400', '6', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195091', '1162', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195092', '1162', '177', '8', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195093', '1162', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195094', '1162', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195095', '1162', '183', '11', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195096', '1162', '181', '12', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195097', '1162', '142', '13', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195098', '1162', '235', '14', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195099', '1162', '1240', '15', 'THRESHING_TIME', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195100', '1162', '287', '16', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195101', '1162', '293', '17', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195102', '1162', '2136', '18', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195103', '1162', '1756', '19', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195104', '1163', '1238', '1', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195105', '1163', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195106', '1163', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195107', '1163', '1437', '4', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195108', '1163', '400', '5', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195109', '1163', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195110', '1163', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195111', '1163', '214', '8', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195112', '1163', '215', '9', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195113', '1163', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195114', '1163', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195115', '1163', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195116', '1163', '288', '13', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195117', '1163', '289', '14', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195118', '1163', '285', '15', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195119', '1164', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195120', '1164', '397', '2', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195121', '1164', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195122', '1164', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195123', '1164', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195124', '1164', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195125', '1164', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195126', '1164', '1573', '8', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195127', '1164', '1571', '9', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195128', '1164', '78', '10', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195129', '1164', '400', '11', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195130', '1164', '212', '12', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195131', '1164', '213', '13', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195132', '1164', '214', '14', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195133', '1164', '215', '15', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195134', '1164', '216', '16', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195135', '1164', '585', '17', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195136', '1164', '178', '18', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195137', '1164', '182', '19', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195138', '1164', '591', '20', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195139', '1164', '1471', '21', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195140', '1164', '180', '22', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195141', '1164', '619', '23', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195142', '1164', '617', '24', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195143', '1164', '618', '25', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195144', '1164', '287', '26', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195145', '1164', '288', '27', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195146', '1164', '289', '28', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195147', '1164', '290', '29', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195148', '1164', '291', '30', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195149', '1164', '285', '31', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195150', '1164', '293', '32', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195151', '1164', '2136', '33', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195152', '1164', '413', '34', 'YLD_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195153', '1164', '1470', '35', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195154', '1165', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195155', '1165', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195156', '1165', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195157', '1165', '1571', '4', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195158', '1165', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195159', '1165', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195160', '1165', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195161', '1165', '214', '8', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195162', '1165', '178', '9', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195163', '1165', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195164', '1165', '183', '11', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195165', '1165', '180', '12', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195166', '1165', '1570', '13', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195167', '1165', '287', '14', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195168', '1165', '288', '15', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195169', '1165', '285', '16', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195170', '1165', '411', '17', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195171', '1165', '2136', '18', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195172', '1165', '1143', '19', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195173', '1165', '754', '20', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195174', '1166', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195175', '1166', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195176', '1166', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195177', '1166', '1571', '4', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195178', '1166', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195179', '1166', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195180', '1166', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195181', '1166', '214', '8', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195182', '1166', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195183', '1166', '183', '10', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195184', '1166', '180', '11', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195185', '1166', '1570', '12', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195186', '1166', '287', '13', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195187', '1166', '288', '14', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195188', '1166', '285', '15', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195189', '1166', '411', '16', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195190', '1166', '2136', '17', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195191', '1166', '1143', '18', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195192', '1166', '754', '19', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195193', '1167', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195194', '1167', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195195', '1167', '21', '3', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195196', '1167', '23', '4', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195197', '1167', '22', '5', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195198', '1167', '24', '6', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195199', '1167', '1573', '7', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195200', '1167', '78', '8', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195201', '1167', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195202', '1167', '591', '10', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195203', '1167', '1471', '11', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195204', '1167', '1570', '12', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195205', '1167', '293', '13', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195206', '1167', '2136', '14', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195207', '1167', '1470', '15', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195208', '1168', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195209', '1168', '1573', '2', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195210', '1168', '1571', '3', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195211', '1168', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195212', '1168', '400', '5', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195213', '1168', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195214', '1168', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195215', '1168', '214', '8', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195216', '1168', '215', '9', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195217', '1168', '216', '10', 'Ht4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195218', '1168', '585', '11', 'Ht5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195219', '1168', '178', '12', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195220', '1168', '182', '13', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195221', '1168', '180', '14', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195222', '1168', '619', '15', 'PHG 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195223', '1168', '617', '16', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195224', '1168', '618', '17', 'PHPN 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195225', '1168', '287', '18', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195226', '1168', '288', '19', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195227', '1168', '289', '20', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195228', '1168', '290', '21', 'TILL4', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195229', '1168', '291', '22', 'TILL5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195230', '1168', '285', '23', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195231', '1168', '2136', '24', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195232', '1168', '413', '25', 'YLD_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195233', '1169', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195234', '1169', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195235', '1169', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195236', '1169', '1571', '4', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195237', '1169', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195238', '1169', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195239', '1169', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195240', '1169', '214', '8', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195241', '1169', '178', '9', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195242', '1169', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195243', '1169', '183', '11', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195244', '1169', '180', '12', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195245', '1169', '1570', '13', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195246', '1169', '287', '14', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195247', '1169', '288', '15', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195248', '1169', '285', '16', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195249', '1169', '411', '17', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195250', '1169', '2136', '18', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195251', '1169', '1143', '19', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195252', '1169', '754', '20', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195253', '1170', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195254', '1170', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195255', '1170', '213', '3', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195256', '1170', '287', '4', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195257', '1171', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195258', '1171', '1437', '2', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195259', '1171', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195260', '1171', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195261', '1171', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195262', '1171', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195263', '1171', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195264', '1171', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195265', '1171', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195266', '1171', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195267', '1171', '288', '11', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195268', '1171', '289', '12', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195269', '1171', '285', '13', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195270', '1172', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195271', '1172', '1555', '2', 'FLW 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195272', '1172', '1436', '3', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195273', '1172', '1556', '4', 'MAT 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195274', '1172', '1557', '5', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195275', '1172', '142', '6', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195276', '1172', '1327', '7', 'PANSEL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195277', '1173', '1555', '1', 'FLW 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195278', '1173', '1436', '2', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195279', '1173', '1556', '3', 'MAT 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195280', '1173', '1557', '4', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195281', '1173', '1327', '5', 'PANSEL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195282', '1174', '1436', '1', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195283', '1174', '1557', '2', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195284', '1175', '1555', '1', 'FLW 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195285', '1175', '1436', '2', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195286', '1175', '1556', '3', 'MAT 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195287', '1175', '1557', '4', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195288', '1175', '1327', '5', 'PANSEL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195289', '1176', '1555', '1', 'FLW 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195290', '1176', '1436', '2', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195291', '1176', '1556', '3', 'MAT 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195292', '1176', '1557', '4', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195293', '1176', '1327', '5', 'PANSEL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195294', '1177', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195295', '1177', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195296', '1177', '177', '3', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195297', '1177', '180', '4', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195298', '1177', '142', '5', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195299', '1177', '293', '6', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195300', '1178', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195301', '1178', '180', '2', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195302', '1178', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195303', '1179', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195304', '1179', '397', '2', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195305', '1179', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195306', '1179', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195307', '1179', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195308', '1179', '180', '6', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195309', '1179', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195310', '1179', '639', '8', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195311', '1179', '293', '9', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195312', '1179', '411', '10', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195313', '1179', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195314', '1179', '414', '12', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195315', '1180', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195316', '1180', '397', '2', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195317', '1180', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195318', '1180', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195319', '1180', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195320', '1180', '180', '6', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195321', '1180', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195322', '1180', '639', '8', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195323', '1180', '293', '9', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195324', '1180', '411', '10', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195325', '1180', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195326', '1180', '414', '12', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195327', '1181', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195328', '1181', '1943', '2', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195329', '1181', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195330', '1181', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195331', '1181', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195332', '1181', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195333', '1181', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195334', '1181', '2136', '8', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195335', '1181', '1756', '9', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195336', '1182', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195337', '1182', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195338', '1182', '21', '3', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195339', '1182', '23', '4', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195340', '1182', '22', '5', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195341', '1182', '24', '6', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195342', '1182', '1573', '7', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195343', '1182', '1241', '8', 'Density', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195344', '1182', '78', '9', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195345', '1182', '213', '10', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195346', '1182', '591', '11', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195347', '1182', '1471', '12', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195348', '1182', '1570', '13', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195349', '1182', '1240', '14', 'THRESHING_TIME', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195350', '1182', '293', '15', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195351', '1182', '2136', '16', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195352', '1182', '1470', '17', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195353', '1183', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195354', '1183', '1943', '2', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195355', '1183', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195356', '1183', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195357', '1183', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195358', '1183', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195359', '1183', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195360', '1183', '2136', '8', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195361', '1183', '1756', '9', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195362', '1184', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195363', '1184', '1437', '2', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195364', '1184', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195365', '1184', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195366', '1184', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195367', '1184', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195368', '1184', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195369', '1184', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195370', '1184', '183', '9', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195371', '1184', '287', '10', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195372', '1184', '288', '11', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195373', '1184', '289', '12', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195374', '1184', '285', '13', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195375', '1185', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195376', '1185', '78', '2', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195377', '1185', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195378', '1185', '213', '4', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195379', '1185', '177', '5', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195380', '1185', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195381', '1185', '142', '7', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195382', '1185', '235', '8', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195383', '1185', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195384', '1186', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195385', '1186', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195386', '1186', '1573', '3', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195387', '1186', '1755', '4', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195388', '1186', '1241', '5', 'Density', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195389', '1186', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195390', '1186', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195391', '1186', '213', '8', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195392', '1186', '177', '9', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195393', '1186', '182', '10', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195394', '1186', '591', '11', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195395', '1186', '183', '12', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195396', '1186', '181', '13', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195397', '1186', '142', '14', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195398', '1186', '235', '15', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195399', '1186', '1240', '16', 'THRESHING_TIME', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195400', '1186', '287', '17', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195401', '1186', '293', '18', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195402', '1186', '2136', '19', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195403', '1186', '1756', '20', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195404', '1187', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195405', '1187', '590', '2', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195406', '1187', '1755', '3', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195407', '1187', '1241', '4', 'Density', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195408', '1187', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195409', '1187', '400', '6', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195410', '1187', '212', '7', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195411', '1187', '213', '8', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195412', '1187', '214', '9', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195413', '1187', '215', '10', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195414', '1187', '182', '11', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195415', '1187', '591', '12', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195416', '1187', '183', '13', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195417', '1187', '181', '14', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195418', '1187', '142', '15', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195419', '1187', '1240', '16', 'THRESHING_TIME', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195420', '1187', '287', '17', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195421', '1187', '288', '18', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195422', '1187', '289', '19', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195423', '1187', '285', '20', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195424', '1187', '293', '21', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195425', '1187', '2136', '22', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195426', '1187', '1756', '23', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195427', '1188', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195428', '1188', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195429', '1188', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195430', '1188', '1755', '4', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195431', '1188', '1241', '5', 'Density', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195432', '1188', '78', '6', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195433', '1188', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195434', '1188', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195435', '1188', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195436', '1188', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195437', '1188', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195438', '1188', '182', '12', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195439', '1188', '591', '13', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195440', '1188', '183', '14', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195441', '1188', '1471', '15', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195442', '1188', '181', '16', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195443', '1188', '142', '17', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195444', '1188', '1240', '18', 'THRESHING_TIME', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195445', '1188', '287', '19', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195446', '1188', '288', '20', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195447', '1188', '289', '21', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195448', '1188', '285', '22', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195449', '1188', '293', '23', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195450', '1188', '2136', '24', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195451', '1188', '1756', '25', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195452', '1189', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195453', '1189', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195454', '1189', '1943', '3', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195455', '1189', '1755', '4', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195456', '1189', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195457', '1189', '212', '6', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195458', '1189', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195459', '1189', '182', '8', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195460', '1189', '183', '9', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195461', '1189', '180', '10', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195462', '1189', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195463', '1189', '293', '12', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195464', '1189', '2136', '13', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195465', '1189', '1756', '14', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195466', '1190', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195467', '1190', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195468', '1190', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195469', '1190', '21', '4', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195470', '1190', '23', '5', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195471', '1190', '22', '6', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195472', '1190', '24', '7', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195473', '1190', '1898', '8', 'Corrected AYLD(Kg)w/MC_Harvester', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195474', '1190', '78', '9', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195475', '1190', '1437', '10', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195476', '1190', '400', '11', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195477', '1190', '212', '12', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195478', '1190', '213', '13', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195479', '1190', '214', '14', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195480', '1190', '215', '15', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195481', '1190', '1899', '16', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195482', '1190', '591', '17', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195483', '1190', '1471', '18', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195484', '1190', '180', '19', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195485', '1190', '1560', '20', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195486', '1190', '142', '21', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195487', '1190', '293', '22', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195488', '1190', '1415', '23', 'Threshing date using CST', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195489', '1190', '2136', '24', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195490', '1191', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195491', '1191', '1209', '2', 'BB FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195492', '1191', '28', '3', 'BLS 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195493', '1191', '157', '4', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195494', '1191', '1573', '5', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195495', '1191', '1943', '6', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195496', '1191', '1755', '7', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195497', '1191', '78', '8', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195498', '1191', '400', '9', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195499', '1191', '1569', '10', 'HT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195500', '1191', '212', '11', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195501', '1191', '213', '12', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195502', '1191', '178', '13', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195503', '1191', '182', '14', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195504', '1191', '183', '15', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195505', '1191', '180', '16', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195506', '1191', '185', '17', 'NBLS 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195507', '1191', '142', '18', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195508', '1191', '235', '19', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195509', '1191', '287', '20', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195510', '1191', '285', '21', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195511', '1192', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195512', '1193', '1765', '1', 'Disease Index Greenhouse Testing', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195513', '1193', '1760', '2', 'Number of plants tested per plot with score 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195514', '1193', '1761', '3', 'Number of plants tested per plot with score 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195515', '1193', '1762', '4', 'Number of plants tested per plot with score 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195516', '1193', '1763', '5', 'Number of plants tested per plot with score 7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195517', '1193', '1764', '6', 'Number of plants tested per plot with score 9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195518', '1193', '1759', '7', 'Total number of plants tested per plot', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195519', '1194', '1169', '1', 'ADJYLD3_MET', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195520', '1194', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195521', '1194', '397', '3', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195522', '1194', '1238', '4', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195523', '1194', '400', '5', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195524', '1194', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195525', '1194', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195526', '1194', '183', '8', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195527', '1194', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195528', '1194', '235', '10', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195529', '1194', '287', '11', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195530', '1194', '285', '12', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195531', '1194', '293', '13', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195532', '1194', '2136', '14', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195533', '1194', '414', '15', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195534', '1194', '1213', '16', 'YLD_TON_MET', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195535', '1195', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195536', '1195', '1573', '2', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195537', '1195', '1571', '3', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195538', '1195', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195539', '1195', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195540', '1195', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195541', '1195', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195542', '1195', '1570', '8', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195543', '1195', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195544', '1195', '288', '10', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195545', '1195', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195546', '1195', '1905', '12', 'YLD_TON7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195547', '1196', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195548', '1196', '1573', '2', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195549', '1196', '1571', '3', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195550', '1196', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195551', '1196', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195552', '1196', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195553', '1196', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195554', '1196', '1570', '8', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195555', '1196', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195556', '1196', '288', '10', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195557', '1196', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195558', '1196', '1905', '12', 'YLD_TON7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195559', '1197', '628', '1', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195560', '1197', '1573', '2', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195561', '1197', '1571', '3', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195562', '1197', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195563', '1197', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195564', '1197', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195565', '1197', '182', '7', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195566', '1197', '1570', '8', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195567', '1197', '287', '9', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195568', '1197', '288', '10', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195569', '1197', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195570', '1197', '1905', '12', 'YLD_TON7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195571', '1198', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195572', '1198', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195573', '1198', '21', '3', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195574', '1198', '23', '4', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195575', '1198', '22', '5', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195576', '1198', '24', '6', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195577', '1198', '1573', '7', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195578', '1198', '1571', '8', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195579', '1198', '78', '9', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195580', '1198', '213', '10', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195581', '1198', '214', '11', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195582', '1198', '178', '12', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195583', '1198', '182', '13', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195584', '1198', '1570', '14', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195585', '1198', '287', '15', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195586', '1198', '288', '16', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195587', '1198', '2136', '17', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195588', '1198', '1475', '18', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195589', '1199', '1169', '1', 'ADJYLD3_MET', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195590', '1199', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195591', '1199', '397', '3', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195592', '1199', '1238', '4', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195593', '1199', '1573', '5', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195594', '1199', '1571', '6', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195595', '1199', '78', '7', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195596', '1199', '400', '8', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195597', '1199', '212', '9', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195598', '1199', '213', '10', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195599', '1199', '214', '11', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195600', '1199', '215', '12', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195601', '1199', '178', '13', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195602', '1199', '182', '14', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195603', '1199', '183', '15', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195604', '1199', '180', '16', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195605', '1199', '142', '17', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195606', '1199', '617', '18', 'PHP 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195607', '1199', '287', '19', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195608', '1199', '288', '20', 'TILL2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195609', '1199', '289', '21', 'TILL3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195610', '1199', '285', '22', 'TILLER_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195611', '1199', '293', '23', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195612', '1199', '2136', '24', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195613', '1199', '414', '25', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195614', '1199', '1143', '26', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195615', '1199', '754', '27', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195616', '1199', '1213', '28', 'YLD_TON_MET', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195617', '1200', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195618', '1200', '1889', '2', 'Corrected AYLD(G)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195619', '1200', '1890', '3', 'Corrected AYLD(G)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195620', '1200', '78', '4', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195621', '1200', '400', '5', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195622', '1200', '182', '6', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195623', '1200', '183', '7', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195624', '1200', '180', '8', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195625', '1200', '142', '9', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195626', '1200', '293', '10', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195627', '1200', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195628', '1200', '1944', '12', 'YLD_CONT3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195629', '1201', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195630', '1202', '1660', '1', 'BPH_GH_SEGRE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195631', '1202', '1659', '2', 'GLH_SEGRE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195632', '1203', '1760', '1', 'Number of plants tested per plot with score 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195633', '1203', '1761', '2', 'Number of plants tested per plot with score 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195634', '1203', '1762', '3', 'Number of plants tested per plot with score 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195635', '1203', '1763', '4', 'Number of plants tested per plot with score 7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195636', '1203', '1764', '5', 'Number of plants tested per plot with score 9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195637', '1203', '1759', '6', 'Total number of plants tested per plot', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195638', '1204', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195639', '1204', '213', '2', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195640', '1204', '287', '3', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195641', '1205', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195642', '1205', '1437', '2', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195643', '1205', '400', '3', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195644', '1205', '212', '4', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195645', '1205', '213', '5', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195646', '1205', '214', '6', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195647', '1205', '215', '7', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195648', '1205', '1899', '8', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195649', '1205', '180', '9', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195650', '1205', '1560', '10', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195651', '1206', '639', '1', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195652', '1207', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195653', '1207', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195654', '1207', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195655', '1208', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195656', '1208', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195657', '1208', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195658', '1209', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195659', '1209', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195660', '1209', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195661', '1210', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195662', '1210', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195663', '1210', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195664', '1211', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195665', '1211', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195666', '1211', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195667', '1212', '400', '1', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195668', '1212', '1899', '2', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195669', '1212', '1560', '3', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195670', '1213', '1555', '1', 'FLW 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195671', '1213', '1436', '2', 'FLW 50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195672', '1213', '1556', '3', 'MAT 1STDATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195673', '1213', '1557', '4', 'MAT_50DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195674', '1213', '1327', '5', 'PANSEL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195675', '1214', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195676', '1214', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195677', '1214', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195678', '1214', '1898', '4', 'Corrected AYLD(Kg)w/MC_Harvester', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195679', '1214', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195680', '1214', '1437', '6', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195681', '1214', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195682', '1214', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195683', '1214', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195684', '1214', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195685', '1214', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195686', '1214', '591', '12', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195687', '1214', '1471', '13', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195688', '1214', '181', '14', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195689', '1214', '180', '15', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195690', '1214', '142', '16', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195691', '1214', '639', '17', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195692', '1214', '293', '18', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195693', '1214', '1415', '19', 'Threshing date using CST', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195694', '1214', '2136', '20', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195695', '1214', '1470', '21', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195696', '1215', '1437', '1', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195697', '1215', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195698', '1215', '639', '3', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195699', '1216', '78', '1', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195700', '1216', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195701', '1216', '142', '3', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195702', '1216', '639', '4', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195703', '1216', '235', '5', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195704', '1217', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195705', '1217', '400', '2', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195706', '1217', '142', '3', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195707', '1217', '639', '4', 'NO OF PLANTS SELECTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195708', '1217', '235', '5', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195709', '1218', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195710', '1218', '397', '2', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195711', '1218', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195712', '1218', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195713', '1218', '212', '5', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195714', '1218', '213', '6', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195715', '1218', '180', '7', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195716', '1218', '142', '8', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195717', '1218', '235', '9', 'RTD FLD 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195718', '1218', '293', '10', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195719', '1218', '2136', '11', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195720', '1218', '414', '12', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195721', '1219', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195722', '1219', '628', '2', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195723', '1219', '21', '3', 'BB1 1-9_PXO 61_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195724', '1219', '23', '4', 'BB1_S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195725', '1219', '22', '5', 'BB2 1-9_PXO 86_FLD', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195726', '1219', '24', '6', 'BB2_S 1/3-S', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195727', '1219', '1573', '7', 'BrSc', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195728', '1219', '1571', '8', 'Dis', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195729', '1219', '78', '9', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195730', '1219', '213', '10', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195731', '1219', '214', '11', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195732', '1219', '178', '12', 'Lg 1-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195733', '1219', '182', '13', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195734', '1219', '1570', '14', 'OT/Seg', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195735', '1219', '2136', '15', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195736', '1219', '1475', '16', 'YLD_TON5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195737', '1220', '2117', '1', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195738', '1220', '397', '2', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195739', '1220', '628', '3', 'AYLD_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195740', '1220', '1889', '4', 'Corrected AYLD(G)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195741', '1220', '1890', '5', 'Corrected AYLD(G)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195742', '1220', '1943', '6', 'Corrected PlotYield(KG)', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195743', '1220', '1755', '7', 'Corrected PlotYield(KG)_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195744', '1220', '78', '8', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195745', '1220', '400', '9', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195746', '1220', '212', '10', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195747', '1220', '213', '11', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195748', '1220', '182', '12', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195749', '1220', '183', '13', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195750', '1220', '181', '14', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195751', '1220', '180', '15', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195752', '1220', '142', '16', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195753', '1220', '293', '17', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195754', '1220', '2136', '18', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195755', '1220', '1944', '19', 'YLD_CONT3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195756', '1220', '1756', '20', 'YLD_TON6', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195757', '1221', '1469', '1', 'ADJUSTED YIELD FROM HARVESTER 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195758', '1221', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195759', '1221', '590', '3', 'AYLD_MECH_KG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195760', '1221', '1898', '4', 'Corrected AYLD(Kg)w/MC_Harvester', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195761', '1221', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195762', '1221', '1437', '6', 'FLW 100DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195763', '1221', '400', '7', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195764', '1221', '212', '8', 'HT_AVG', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195765', '1221', '213', '9', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195766', '1221', '214', '10', 'Ht2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195767', '1221', '215', '11', 'Ht3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195768', '1221', '1899', '12', 'MAT afterFLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195769', '1221', '591', '13', 'MC HARVESTER', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195770', '1221', '1471', '14', 'MF_HARV', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195771', '1221', '181', '15', 'MISS HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195772', '1221', '180', '16', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195773', '1221', '1560', '17', 'Maturity date', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195774', '1221', '142', '18', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195775', '1221', '293', '19', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195776', '1221', '1415', '20', 'Threshing date using CST', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195777', '1221', '2136', '21', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195778', '1221', '1470', '22', 'Yld_TON3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195779', '1222', '1169', '1', 'ADJYLD3_MET', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195780', '1222', '2117', '2', 'AREA FACTOR', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195781', '1222', '397', '3', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195782', '1222', '1238', '4', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195783', '1222', '78', '5', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195784', '1222', '400', '6', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195785', '1222', '213', '7', 'Ht1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195786', '1222', '177', '8', 'Lg PCT', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195787', '1222', '182', '9', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195788', '1222', '183', '10', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195789', '1222', '142', '11', 'NO OF HILLS HARVESTED', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195790', '1222', '287', '12', 'TILL1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195791', '1222', '293', '13', 'TOTAL HILL', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195792', '1222', '411', '14', 'TOTAL_AYLD_PS', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195793', '1222', '2136', '15', 'YLD TON/HA', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195794', '1222', '414', '16', 'YLD_0_CONT1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195795', '1222', '1144', '17', 'YLD_0_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195796', '1222', '1143', '18', 'YLD_CONT2', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195797', '1222', '754', '19', 'YLD_TON', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195798', '1223', '157', '1', 'Bl1 0-9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195799', '1224', '1660', '1', 'BPH_GH_SEGRE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195800', '1224', '1659', '2', 'GLH_SEGRE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195801', '1225', '1765', '1', 'Disease Index Greenhouse Testing', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195802', '1225', '1760', '2', 'Number of plants tested per plot with score 1', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195803', '1225', '1761', '3', 'Number of plants tested per plot with score 3', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195804', '1225', '1762', '4', 'Number of plants tested per plot with score 5', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195805', '1225', '1763', '5', 'Number of plants tested per plot with score 7', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195806', '1225', '1764', '6', 'Number of plants tested per plot with score 9', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195807', '1225', '1759', '7', 'Total number of plants tested per plot', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195808', '1226', '397', '1', 'AYLD_G', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195809', '1226', '1238', '2', 'AYLD_G_MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195810', '1226', '78', '3', 'FLW', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195811', '1226', '400', '4', 'FLW DATE', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195812', '1226', '182', '5', 'MC', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195813', '1226', '183', '6', 'MF', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195814', '1226', '180', '7', 'Mat', '1', 'true');

INSERT INTO platform.list_member (id, list_id, data_id, order_number, display_value, creator_id, is_active)
VALUES ('195815', '1226', '293', '8', 'TOTAL HILL', '1', 'true');


-- insert into tenant.protocol
INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('5', 'TRAIT_LIST_EXPT0006', 'TRAIT LIST FOR EXPT0006', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('6', 'TRAIT_LIST_EXPT0007', 'TRAIT LIST FOR EXPT0007', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('7', 'TRAIT_LIST_EXPT0008', 'TRAIT LIST FOR EXPT0008', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('8', 'TRAIT_LIST_EXPT0009', 'TRAIT LIST FOR EXPT0009', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('9', 'TRAIT_LIST_EXPT0012', 'TRAIT LIST FOR EXPT0012', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('10', 'TRAIT_LIST_EXPT0013', 'TRAIT LIST FOR EXPT0013', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('11', 'TRAIT_LIST_EXPT0014', 'TRAIT LIST FOR EXPT0014', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('12', 'TRAIT_LIST_EXPT0015', 'TRAIT LIST FOR EXPT0015', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('13', 'TRAIT_LIST_EXPT0016', 'TRAIT LIST FOR EXPT0016', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('14', 'TRAIT_LIST_EXPT0017', 'TRAIT LIST FOR EXPT0017', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('15', 'TRAIT_LIST_EXPT0018', 'TRAIT LIST FOR EXPT0018', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('16', 'TRAIT_LIST_EXPT0019', 'TRAIT LIST FOR EXPT0019', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('17', 'TRAIT_LIST_EXPT0021', 'TRAIT LIST FOR EXPT0021', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('18', 'TRAIT_LIST_EXPT0022', 'TRAIT LIST FOR EXPT0022', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('19', 'TRAIT_LIST_EXPT0023', 'TRAIT LIST FOR EXPT0023', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('20', 'TRAIT_LIST_EXPT0024', 'TRAIT LIST FOR EXPT0024', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('21', 'TRAIT_LIST_EXPT0025', 'TRAIT LIST FOR EXPT0025', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('22', 'TRAIT_LIST_EXPT0026', 'TRAIT LIST FOR EXPT0026', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('23', 'TRAIT_LIST_EXPT0027', 'TRAIT LIST FOR EXPT0027', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('24', 'TRAIT_LIST_EXPT0028', 'TRAIT LIST FOR EXPT0028', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('25', 'TRAIT_LIST_EXPT0029', 'TRAIT LIST FOR EXPT0029', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('26', 'TRAIT_LIST_EXPT0031', 'TRAIT LIST FOR EXPT0031', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('27', 'TRAIT_LIST_EXPT0032', 'TRAIT LIST FOR EXPT0032', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('28', 'TRAIT_LIST_EXPT0033', 'TRAIT LIST FOR EXPT0033', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('29', 'TRAIT_LIST_EXPT0041', 'TRAIT LIST FOR EXPT0041', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('30', 'TRAIT_LIST_EXPT0042', 'TRAIT LIST FOR EXPT0042', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('31', 'TRAIT_LIST_EXPT0043', 'TRAIT LIST FOR EXPT0043', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('32', 'TRAIT_LIST_EXPT0044', 'TRAIT LIST FOR EXPT0044', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('33', 'TRAIT_LIST_EXPT0045', 'TRAIT LIST FOR EXPT0045', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('34', 'TRAIT_LIST_EXPT0046', 'TRAIT LIST FOR EXPT0046', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('35', 'TRAIT_LIST_EXPT0047', 'TRAIT LIST FOR EXPT0047', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('36', 'TRAIT_LIST_EXPT0049', 'TRAIT LIST FOR EXPT0049', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('37', 'TRAIT_LIST_EXPT0051', 'TRAIT LIST FOR EXPT0051', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('38', 'TRAIT_LIST_EXPT0052', 'TRAIT LIST FOR EXPT0052', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('39', 'TRAIT_LIST_EXPT0053', 'TRAIT LIST FOR EXPT0053', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('40', 'TRAIT_LIST_EXPT0055', 'TRAIT LIST FOR EXPT0055', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('41', 'TRAIT_LIST_EXPT0056', 'TRAIT LIST FOR EXPT0056', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('42', 'TRAIT_LIST_EXPT0057', 'TRAIT LIST FOR EXPT0057', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('43', 'TRAIT_LIST_EXPT0058', 'TRAIT LIST FOR EXPT0058', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('44', 'TRAIT_LIST_EXPT0059', 'TRAIT LIST FOR EXPT0059', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('45', 'TRAIT_LIST_EXPT0060', 'TRAIT LIST FOR EXPT0060', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('46', 'TRAIT_LIST_EXPT0061', 'TRAIT LIST FOR EXPT0061', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('47', 'TRAIT_LIST_EXPT0062', 'TRAIT LIST FOR EXPT0062', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('48', 'TRAIT_LIST_EXPT0063', 'TRAIT LIST FOR EXPT0063', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('49', 'TRAIT_LIST_EXPT0065', 'TRAIT LIST FOR EXPT0065', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('50', 'TRAIT_LIST_EXPT0066', 'TRAIT LIST FOR EXPT0066', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('51', 'TRAIT_LIST_EXPT0068', 'TRAIT LIST FOR EXPT0068', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('52', 'TRAIT_LIST_EXPT0071', 'TRAIT LIST FOR EXPT0071', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('53', 'TRAIT_LIST_EXPT0072', 'TRAIT LIST FOR EXPT0072', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('54', 'TRAIT_LIST_EXPT0073', 'TRAIT LIST FOR EXPT0073', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('55', 'TRAIT_LIST_EXPT0074', 'TRAIT LIST FOR EXPT0074', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('56', 'TRAIT_LIST_EXPT0075', 'TRAIT LIST FOR EXPT0075', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('57', 'TRAIT_LIST_EXPT0076', 'TRAIT LIST FOR EXPT0076', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('58', 'TRAIT_LIST_EXPT0077', 'TRAIT LIST FOR EXPT0077', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('59', 'TRAIT_LIST_EXPT0078', 'TRAIT LIST FOR EXPT0078', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('60', 'TRAIT_LIST_EXPT0079', 'TRAIT LIST FOR EXPT0079', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('61', 'TRAIT_LIST_EXPT0080', 'TRAIT LIST FOR EXPT0080', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('62', 'TRAIT_LIST_EXPT0081', 'TRAIT LIST FOR EXPT0081', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('63', 'TRAIT_LIST_EXPT0082', 'TRAIT LIST FOR EXPT0082', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('64', 'TRAIT_LIST_EXPT0083', 'TRAIT LIST FOR EXPT0083', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('65', 'TRAIT_LIST_EXPT0084', 'TRAIT LIST FOR EXPT0084', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('66', 'TRAIT_LIST_EXPT0085', 'TRAIT LIST FOR EXPT0085', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('67', 'TRAIT_LIST_EXPT0086', 'TRAIT LIST FOR EXPT0086', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('68', 'TRAIT_LIST_EXPT0087', 'TRAIT LIST FOR EXPT0087', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('69', 'TRAIT_LIST_EXPT0088', 'TRAIT LIST FOR EXPT0088', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('70', 'TRAIT_LIST_EXPT0089', 'TRAIT LIST FOR EXPT0089', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('71', 'TRAIT_LIST_EXPT0090', 'TRAIT LIST FOR EXPT0090', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('72', 'TRAIT_LIST_EXPT0091', 'TRAIT LIST FOR EXPT0091', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('73', 'TRAIT_LIST_EXPT0092', 'TRAIT LIST FOR EXPT0092', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('74', 'TRAIT_LIST_EXPT0093', 'TRAIT LIST FOR EXPT0093', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('75', 'TRAIT_LIST_EXPT0094', 'TRAIT LIST FOR EXPT0094', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('76', 'TRAIT_LIST_EXPT0095', 'TRAIT LIST FOR EXPT0095', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('77', 'TRAIT_LIST_EXPT0096', 'TRAIT LIST FOR EXPT0096', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('78', 'TRAIT_LIST_EXPT0097', 'TRAIT LIST FOR EXPT0097', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('79', 'TRAIT_LIST_EXPT0098', 'TRAIT LIST FOR EXPT0098', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('80', 'TRAIT_LIST_EXPT0099', 'TRAIT LIST FOR EXPT0099', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('81', 'TRAIT_LIST_EXPT0100', 'TRAIT LIST FOR EXPT0100', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('82', 'TRAIT_LIST_EXPT0101', 'TRAIT LIST FOR EXPT0101', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('83', 'TRAIT_LIST_EXPT0105', 'TRAIT LIST FOR EXPT0105', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('84', 'TRAIT_LIST_EXPT0106', 'TRAIT LIST FOR EXPT0106', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('85', 'TRAIT_LIST_EXPT0107', 'TRAIT LIST FOR EXPT0107', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('86', 'TRAIT_LIST_EXPT0108', 'TRAIT LIST FOR EXPT0108', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('87', 'TRAIT_LIST_EXPT0109', 'TRAIT LIST FOR EXPT0109', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('88', 'TRAIT_LIST_EXPT0110', 'TRAIT LIST FOR EXPT0110', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('89', 'TRAIT_LIST_EXPT0111', 'TRAIT LIST FOR EXPT0111', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('90', 'TRAIT_LIST_EXPT0112', 'TRAIT LIST FOR EXPT0112', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('91', 'TRAIT_LIST_EXPT0113', 'TRAIT LIST FOR EXPT0113', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('92', 'TRAIT_LIST_EXPT0114', 'TRAIT LIST FOR EXPT0114', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('93', 'TRAIT_LIST_EXPT0115', 'TRAIT LIST FOR EXPT0115', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('94', 'TRAIT_LIST_EXPT0116', 'TRAIT LIST FOR EXPT0116', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('95', 'TRAIT_LIST_EXPT0117', 'TRAIT LIST FOR EXPT0117', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('96', 'TRAIT_LIST_EXPT0118', 'TRAIT LIST FOR EXPT0118', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('97', 'TRAIT_LIST_EXPT0119', 'TRAIT LIST FOR EXPT0119', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('98', 'TRAIT_LIST_EXPT0120', 'TRAIT LIST FOR EXPT0120', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('99', 'TRAIT_LIST_EXPT0121', 'TRAIT LIST FOR EXPT0121', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('100', 'TRAIT_LIST_EXPT0122', 'TRAIT LIST FOR EXPT0122', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('101', 'TRAIT_LIST_EXPT0123', 'TRAIT LIST FOR EXPT0123', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('102', 'TRAIT_LIST_EXPT0125', 'TRAIT LIST FOR EXPT0125', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('103', 'TRAIT_LIST_EXPT0126', 'TRAIT LIST FOR EXPT0126', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('104', 'TRAIT_LIST_EXPT0127', 'TRAIT LIST FOR EXPT0127', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('105', 'TRAIT_LIST_EXPT0128', 'TRAIT LIST FOR EXPT0128', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('106', 'TRAIT_LIST_EXPT0130', 'TRAIT LIST FOR EXPT0130', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('107', 'TRAIT_LIST_EXPT0131', 'TRAIT LIST FOR EXPT0131', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('108', 'TRAIT_LIST_EXPT0132', 'TRAIT LIST FOR EXPT0132', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('109', 'TRAIT_LIST_EXPT0133', 'TRAIT LIST FOR EXPT0133', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('110', 'TRAIT_LIST_EXPT0134', 'TRAIT LIST FOR EXPT0134', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('111', 'TRAIT_LIST_EXPT0135', 'TRAIT LIST FOR EXPT0135', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('112', 'TRAIT_LIST_EXPT0136', 'TRAIT LIST FOR EXPT0136', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('113', 'TRAIT_LIST_EXPT0137', 'TRAIT LIST FOR EXPT0137', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('114', 'TRAIT_LIST_EXPT0138', 'TRAIT LIST FOR EXPT0138', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('115', 'TRAIT_LIST_EXPT0139', 'TRAIT LIST FOR EXPT0139', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('116', 'TRAIT_LIST_EXPT0140', 'TRAIT LIST FOR EXPT0140', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('117', 'TRAIT_LIST_EXPT0141', 'TRAIT LIST FOR EXPT0141', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('118', 'TRAIT_LIST_EXPT0142', 'TRAIT LIST FOR EXPT0142', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('119', 'TRAIT_LIST_EXPT0143', 'TRAIT LIST FOR EXPT0143', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('120', 'TRAIT_LIST_EXPT0144', 'TRAIT LIST FOR EXPT0144', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('121', 'TRAIT_LIST_EXPT0145', 'TRAIT LIST FOR EXPT0145', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('122', 'TRAIT_LIST_EXPT0146', 'TRAIT LIST FOR EXPT0146', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('123', 'TRAIT_LIST_EXPT0147', 'TRAIT LIST FOR EXPT0147', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('124', 'TRAIT_LIST_EXPT0148', 'TRAIT LIST FOR EXPT0148', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('125', 'TRAIT_LIST_EXPT0149', 'TRAIT LIST FOR EXPT0149', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('126', 'TRAIT_LIST_EXPT0150', 'TRAIT LIST FOR EXPT0150', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('127', 'TRAIT_LIST_EXPT0151', 'TRAIT LIST FOR EXPT0151', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('128', 'TRAIT_LIST_EXPT0152', 'TRAIT LIST FOR EXPT0152', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('129', 'TRAIT_LIST_EXPT0153', 'TRAIT LIST FOR EXPT0153', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('130', 'TRAIT_LIST_EXPT0154', 'TRAIT LIST FOR EXPT0154', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('131', 'TRAIT_LIST_EXPT0155', 'TRAIT LIST FOR EXPT0155', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('132', 'TRAIT_LIST_EXPT0156', 'TRAIT LIST FOR EXPT0156', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('133', 'TRAIT_LIST_EXPT0157', 'TRAIT LIST FOR EXPT0157', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('134', 'TRAIT_LIST_EXPT0158', 'TRAIT LIST FOR EXPT0158', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('135', 'TRAIT_LIST_EXPT0159', 'TRAIT LIST FOR EXPT0159', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('136', 'TRAIT_LIST_EXPT0162', 'TRAIT LIST FOR EXPT0162', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('137', 'TRAIT_LIST_EXPT0163', 'TRAIT LIST FOR EXPT0163', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('138', 'TRAIT_LIST_EXPT0165', 'TRAIT LIST FOR EXPT0165', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('139', 'TRAIT_LIST_EXPT0166', 'TRAIT LIST FOR EXPT0166', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('140', 'TRAIT_LIST_EXPT0167', 'TRAIT LIST FOR EXPT0167', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('141', 'TRAIT_LIST_EXPT0168', 'TRAIT LIST FOR EXPT0168', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('142', 'TRAIT_LIST_EXPT0169', 'TRAIT LIST FOR EXPT0169', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('143', 'TRAIT_LIST_EXPT0170', 'TRAIT LIST FOR EXPT0170', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('144', 'TRAIT_LIST_EXPT0171', 'TRAIT LIST FOR EXPT0171', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('145', 'TRAIT_LIST_EXPT0172', 'TRAIT LIST FOR EXPT0172', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('146', 'TRAIT_LIST_EXPT0173', 'TRAIT LIST FOR EXPT0173', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('147', 'TRAIT_LIST_EXPT0174', 'TRAIT LIST FOR EXPT0174', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('148', 'TRAIT_LIST_EXPT0175', 'TRAIT LIST FOR EXPT0175', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('149', 'TRAIT_LIST_EXPT0176', 'TRAIT LIST FOR EXPT0176', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('150', 'TRAIT_LIST_EXPT0177', 'TRAIT LIST FOR EXPT0177', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('151', 'TRAIT_LIST_EXPT0178', 'TRAIT LIST FOR EXPT0178', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('152', 'TRAIT_LIST_EXPT0179', 'TRAIT LIST FOR EXPT0179', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('153', 'TRAIT_LIST_EXPT0180', 'TRAIT LIST FOR EXPT0180', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('154', 'TRAIT_LIST_EXPT0181', 'TRAIT LIST FOR EXPT0181', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('155', 'TRAIT_LIST_EXPT0182', 'TRAIT LIST FOR EXPT0182', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('156', 'TRAIT_LIST_EXPT0183', 'TRAIT LIST FOR EXPT0183', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('157', 'TRAIT_LIST_EXPT0184', 'TRAIT LIST FOR EXPT0184', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('158', 'TRAIT_LIST_EXPT0185', 'TRAIT LIST FOR EXPT0185', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('159', 'TRAIT_LIST_EXPT0187', 'TRAIT LIST FOR EXPT0187', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('160', 'TRAIT_LIST_EXPT0188', 'TRAIT LIST FOR EXPT0188', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('161', 'TRAIT_LIST_EXPT0189', 'TRAIT LIST FOR EXPT0189', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('162', 'TRAIT_LIST_EXPT0190', 'TRAIT LIST FOR EXPT0190', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('163', 'TRAIT_LIST_EXPT0192', 'TRAIT LIST FOR EXPT0192', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('164', 'TRAIT_LIST_EXPT0193', 'TRAIT LIST FOR EXPT0193', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('165', 'TRAIT_LIST_EXPT0194', 'TRAIT LIST FOR EXPT0194', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('166', 'TRAIT_LIST_EXPT0195', 'TRAIT LIST FOR EXPT0195', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('167', 'TRAIT_LIST_EXPT0196', 'TRAIT LIST FOR EXPT0196', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('168', 'TRAIT_LIST_EXPT0197', 'TRAIT LIST FOR EXPT0197', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('169', 'TRAIT_LIST_EXPT0198', 'TRAIT LIST FOR EXPT0198', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('170', 'TRAIT_LIST_EXPT0199', 'TRAIT LIST FOR EXPT0199', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('171', 'TRAIT_LIST_EXPT0200', 'TRAIT LIST FOR EXPT0200', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('172', 'TRAIT_LIST_EXPT0202', 'TRAIT LIST FOR EXPT0202', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('173', 'TRAIT_LIST_EXPT0204', 'TRAIT LIST FOR EXPT0204', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('174', 'TRAIT_LIST_EXPT0205', 'TRAIT LIST FOR EXPT0205', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('175', 'TRAIT_LIST_EXPT0206', 'TRAIT LIST FOR EXPT0206', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('176', 'TRAIT_LIST_EXPT0207', 'TRAIT LIST FOR EXPT0207', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('177', 'TRAIT_LIST_EXPT0208', 'TRAIT LIST FOR EXPT0208', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('178', 'TRAIT_LIST_EXPT0209', 'TRAIT LIST FOR EXPT0209', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('179', 'TRAIT_LIST_EXPT0212', 'TRAIT LIST FOR EXPT0212', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('180', 'TRAIT_LIST_EXPT0213', 'TRAIT LIST FOR EXPT0213', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('181', 'TRAIT_LIST_EXPT0214', 'TRAIT LIST FOR EXPT0214', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('182', 'TRAIT_LIST_EXPT0215', 'TRAIT LIST FOR EXPT0215', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('183', 'TRAIT_LIST_EXPT0216', 'TRAIT LIST FOR EXPT0216', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('184', 'TRAIT_LIST_EXPT0217', 'TRAIT LIST FOR EXPT0217', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('185', 'TRAIT_LIST_EXPT0218', 'TRAIT LIST FOR EXPT0218', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('186', 'TRAIT_LIST_EXPT0219', 'TRAIT LIST FOR EXPT0219', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('187', 'TRAIT_LIST_EXPT0220', 'TRAIT LIST FOR EXPT0220', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('188', 'TRAIT_LIST_EXPT0221', 'TRAIT LIST FOR EXPT0221', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('189', 'TRAIT_LIST_EXPT0222', 'TRAIT LIST FOR EXPT0222', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('190', 'TRAIT_LIST_EXPT0223', 'TRAIT LIST FOR EXPT0223', 'trait protocol', '101', '1');

INSERT INTO tenant.protocol (id, protocol_code, protocol_name, protocol_type, program_id, creator_id)
VALUES ('191', 'TRAIT_LIST_EXPT0224', 'TRAIT LIST FOR EXPT0224', 'trait protocol', '101', '1');


-- insert into platform.protocol_data
INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('1', '5', '2328', '1040', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('2', '6', '2328', '1041', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('3', '7', '2328', '1042', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('4', '8', '2328', '1043', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('5', '9', '2328', '1044', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('6', '10', '2328', '1045', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('7', '11', '2328', '1046', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('8', '12', '2328', '1047', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('9', '13', '2328', '1048', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('10', '14', '2328', '1049', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('11', '15', '2328', '1050', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('12', '16', '2328', '1051', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('13', '17', '2328', '1052', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('14', '18', '2328', '1053', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('15', '19', '2328', '1054', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('16', '20', '2328', '1055', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('17', '21', '2328', '1056', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('18', '22', '2328', '1057', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('19', '23', '2328', '1058', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('20', '24', '2328', '1059', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('21', '25', '2328', '1060', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('22', '26', '2328', '1061', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('23', '27', '2328', '1062', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('24', '28', '2328', '1063', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('25', '29', '2328', '1064', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('26', '30', '2328', '1065', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('27', '31', '2328', '1066', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('28', '32', '2328', '1067', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('29', '33', '2328', '1068', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('30', '34', '2328', '1069', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('31', '35', '2328', '1070', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('32', '36', '2328', '1071', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('33', '37', '2328', '1072', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('34', '38', '2328', '1073', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('35', '39', '2328', '1074', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('36', '40', '2328', '1075', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('37', '41', '2328', '1076', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('38', '42', '2328', '1077', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('39', '43', '2328', '1078', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('40', '44', '2328', '1079', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('41', '45', '2328', '1080', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('42', '46', '2328', '1081', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('43', '47', '2328', '1082', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('44', '48', '2328', '1083', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('45', '49', '2328', '1084', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('46', '50', '2328', '1085', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('47', '51', '2328', '1086', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('48', '52', '2328', '1087', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('49', '53', '2328', '1088', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('50', '54', '2328', '1089', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('51', '55', '2328', '1090', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('52', '56', '2328', '1091', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('53', '57', '2328', '1092', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('54', '58', '2328', '1093', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('55', '59', '2328', '1094', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('56', '60', '2328', '1095', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('57', '61', '2328', '1096', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('58', '62', '2328', '1097', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('59', '63', '2328', '1098', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('60', '64', '2328', '1099', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('61', '65', '2328', '1100', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('62', '66', '2328', '1101', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('63', '67', '2328', '1102', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('64', '68', '2328', '1103', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('65', '69', '2328', '1104', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('66', '70', '2328', '1105', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('67', '71', '2328', '1106', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('68', '72', '2328', '1107', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('69', '73', '2328', '1108', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('70', '74', '2328', '1109', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('71', '75', '2328', '1110', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('72', '76', '2328', '1111', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('73', '77', '2328', '1112', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('74', '78', '2328', '1113', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('75', '79', '2328', '1114', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('76', '80', '2328', '1115', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('77', '81', '2328', '1116', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('78', '82', '2328', '1117', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('79', '83', '2328', '1118', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('80', '84', '2328', '1119', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('81', '85', '2328', '1120', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('82', '86', '2328', '1121', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('83', '87', '2328', '1122', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('84', '88', '2328', '1123', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('85', '89', '2328', '1124', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('86', '90', '2328', '1125', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('87', '91', '2328', '1126', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('88', '92', '2328', '1127', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('89', '93', '2328', '1128', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('90', '94', '2328', '1129', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('91', '95', '2328', '1130', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('92', '96', '2328', '1131', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('93', '97', '2328', '1132', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('94', '98', '2328', '1133', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('95', '99', '2328', '1134', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('96', '100', '2328', '1135', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('97', '101', '2328', '1136', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('98', '102', '2328', '1137', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('99', '103', '2328', '1138', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('100', '104', '2328', '1139', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('101', '105', '2328', '1140', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('102', '106', '2328', '1141', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('103', '107', '2328', '1142', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('104', '108', '2328', '1143', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('105', '109', '2328', '1144', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('106', '110', '2328', '1145', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('107', '111', '2328', '1146', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('108', '112', '2328', '1147', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('109', '113', '2328', '1148', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('110', '114', '2328', '1149', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('111', '115', '2328', '1150', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('112', '116', '2328', '1151', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('113', '117', '2328', '1152', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('114', '118', '2328', '1153', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('115', '119', '2328', '1154', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('116', '120', '2328', '1155', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('117', '121', '2328', '1156', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('118', '122', '2328', '1157', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('119', '123', '2328', '1158', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('120', '124', '2328', '1159', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('121', '125', '2328', '1160', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('122', '126', '2328', '1161', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('123', '127', '2328', '1162', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('124', '128', '2328', '1163', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('125', '129', '2328', '1164', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('126', '130', '2328', '1165', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('127', '131', '2328', '1166', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('128', '132', '2328', '1167', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('129', '133', '2328', '1168', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('130', '134', '2328', '1169', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('131', '135', '2328', '1170', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('132', '136', '2328', '1171', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('133', '137', '2328', '1172', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('134', '138', '2328', '1173', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('135', '139', '2328', '1174', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('136', '140', '2328', '1175', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('137', '141', '2328', '1176', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('138', '142', '2328', '1177', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('139', '143', '2328', '1178', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('140', '144', '2328', '1179', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('141', '145', '2328', '1180', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('142', '146', '2328', '1181', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('143', '147', '2328', '1182', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('144', '148', '2328', '1183', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('145', '149', '2328', '1184', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('146', '150', '2328', '1185', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('147', '151', '2328', '1186', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('148', '152', '2328', '1187', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('149', '153', '2328', '1188', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('150', '154', '2328', '1189', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('151', '155', '2328', '1190', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('152', '156', '2328', '1191', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('153', '157', '2328', '1192', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('154', '158', '2328', '1193', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('155', '159', '2328', '1194', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('156', '160', '2328', '1195', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('157', '161', '2328', '1196', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('158', '162', '2328', '1197', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('159', '163', '2328', '1198', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('160', '164', '2328', '1199', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('161', '165', '2328', '1200', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('162', '166', '2328', '1201', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('163', '167', '2328', '1202', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('164', '168', '2328', '1203', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('165', '169', '2328', '1204', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('166', '170', '2328', '1205', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('167', '171', '2328', '1206', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('168', '172', '2328', '1207', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('169', '173', '2328', '1208', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('170', '174', '2328', '1209', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('171', '175', '2328', '1210', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('172', '176', '2328', '1211', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('173', '177', '2328', '1212', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('174', '178', '2328', '1213', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('175', '179', '2328', '1214', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('176', '180', '2328', '1215', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('177', '181', '2328', '1216', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('178', '182', '2328', '1217', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('179', '183', '2328', '1218', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('180', '184', '2328', '1219', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('181', '185', '2328', '1220', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('182', '186', '2328', '1221', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('183', '187', '2328', '1222', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('184', '188', '2328', '1223', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('185', '189', '2328', '1224', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('186', '190', '2328', '1225', '1');

INSERT INTO tenant.protocol_data (id, protocol_id, variable_id, data_value, creator_id)
VALUES ('187', '191', '2328', '1226', '1');


-- insert into experiment.experiment_protocol
INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('1', '6', '5', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('2', '7', '6', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('3', '8', '7', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('4', '9', '8', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('5', '12', '9', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('6', '13', '10', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('7', '14', '11', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('8', '15', '12', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('9', '16', '13', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('10', '17', '14', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('11', '18', '15', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('12', '19', '16', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('13', '21', '17', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('14', '22', '18', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('15', '23', '19', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('16', '24', '20', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('17', '25', '21', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('18', '26', '22', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('19', '27', '23', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('20', '28', '24', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('21', '29', '25', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('22', '31', '26', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('23', '32', '27', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('24', '33', '28', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('25', '41', '29', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('26', '42', '30', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('27', '43', '31', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('28', '44', '32', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('29', '45', '33', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('30', '46', '34', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('31', '47', '35', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('32', '49', '36', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('33', '51', '37', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('34', '52', '38', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('35', '53', '39', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('36', '55', '40', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('37', '56', '41', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('38', '57', '42', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('39', '58', '43', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('40', '59', '44', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('41', '60', '45', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('42', '61', '46', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('43', '62', '47', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('44', '63', '48', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('45', '65', '49', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('46', '66', '50', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('47', '68', '51', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('48', '71', '52', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('49', '72', '53', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('50', '73', '54', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('51', '74', '55', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('52', '75', '56', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('53', '76', '57', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('54', '77', '58', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('55', '78', '59', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('56', '79', '60', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('57', '80', '61', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('58', '81', '62', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('59', '82', '63', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('60', '83', '64', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('61', '84', '65', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('62', '85', '66', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('63', '86', '67', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('64', '87', '68', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('65', '88', '69', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('66', '89', '70', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('67', '90', '71', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('68', '91', '72', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('69', '92', '73', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('70', '93', '74', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('71', '94', '75', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('72', '95', '76', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('73', '96', '77', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('74', '97', '78', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('75', '98', '79', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('76', '99', '80', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('77', '100', '81', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('78', '101', '82', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('79', '105', '83', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('80', '106', '84', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('81', '107', '85', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('82', '108', '86', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('83', '109', '87', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('84', '110', '88', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('85', '111', '89', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('86', '112', '90', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('87', '113', '91', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('88', '114', '92', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('89', '115', '93', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('90', '116', '94', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('91', '117', '95', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('92', '118', '96', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('93', '119', '97', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('94', '120', '98', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('95', '121', '99', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('96', '122', '100', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('97', '123', '101', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('98', '125', '102', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('99', '126', '103', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('100', '127', '104', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('101', '128', '105', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('102', '130', '106', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('103', '131', '107', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('104', '132', '108', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('105', '133', '109', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('106', '134', '110', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('107', '135', '111', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('108', '136', '112', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('109', '137', '113', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('110', '138', '114', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('111', '139', '115', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('112', '140', '116', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('113', '141', '117', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('114', '142', '118', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('115', '143', '119', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('116', '144', '120', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('117', '145', '121', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('118', '146', '122', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('119', '147', '123', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('120', '148', '124', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('121', '149', '125', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('122', '150', '126', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('123', '151', '127', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('124', '152', '128', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('125', '153', '129', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('126', '154', '130', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('127', '155', '131', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('128', '156', '132', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('129', '157', '133', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('130', '158', '134', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('131', '159', '135', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('132', '162', '136', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('133', '163', '137', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('134', '165', '138', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('135', '166', '139', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('136', '167', '140', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('137', '168', '141', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('138', '169', '142', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('139', '170', '143', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('140', '171', '144', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('141', '172', '145', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('142', '173', '146', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('143', '174', '147', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('144', '175', '148', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('145', '176', '149', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('146', '177', '150', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('147', '178', '151', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('148', '179', '152', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('149', '180', '153', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('150', '181', '154', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('151', '182', '155', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('152', '183', '156', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('153', '184', '157', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('154', '185', '158', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('155', '187', '159', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('156', '188', '160', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('157', '189', '161', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('158', '190', '162', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('159', '192', '163', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('160', '193', '164', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('161', '194', '165', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('162', '195', '166', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('163', '196', '167', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('164', '197', '168', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('165', '198', '169', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('166', '199', '170', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('167', '200', '171', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('168', '202', '172', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('169', '204', '173', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('170', '205', '174', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('171', '206', '175', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('172', '207', '176', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('173', '208', '177', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('174', '209', '178', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('175', '212', '179', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('176', '213', '180', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('177', '214', '181', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('178', '215', '182', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('179', '216', '183', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('180', '217', '184', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('181', '218', '185', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('182', '219', '186', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('183', '220', '187', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('184', '221', '188', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('185', '222', '189', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('186', '223', '190', '1', '1');

INSERT INTO experiment.experiment_protocol (id, experiment_id, protocol_id, order_number, creator_id)
VALUES ('187', '224', '191', '1', '1');


-- insert into experiment.experiment_data
INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('9', '6', '2328', '1040', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('10', '7', '2328', '1041', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('11', '8', '2328', '1042', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('12', '9', '2328', '1043', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('13', '12', '2328', '1044', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('14', '13', '2328', '1045', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('15', '14', '2328', '1046', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('16', '15', '2328', '1047', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('17', '16', '2328', '1048', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('18', '17', '2328', '1049', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('19', '18', '2328', '1050', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('20', '19', '2328', '1051', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('21', '21', '2328', '1052', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('22', '22', '2328', '1053', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('23', '23', '2328', '1054', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('24', '24', '2328', '1055', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('25', '25', '2328', '1056', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('26', '26', '2328', '1057', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('27', '27', '2328', '1058', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('28', '28', '2328', '1059', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('29', '29', '2328', '1060', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('30', '31', '2328', '1061', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('31', '32', '2328', '1062', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('32', '33', '2328', '1063', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('33', '41', '2328', '1064', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('34', '42', '2328', '1065', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('35', '43', '2328', '1066', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('36', '44', '2328', '1067', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('37', '45', '2328', '1068', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('38', '46', '2328', '1069', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('39', '47', '2328', '1070', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('40', '49', '2328', '1071', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('41', '51', '2328', '1072', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('42', '52', '2328', '1073', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('43', '53', '2328', '1074', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('44', '55', '2328', '1075', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('45', '56', '2328', '1076', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('46', '57', '2328', '1077', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('47', '58', '2328', '1078', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('48', '59', '2328', '1079', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('49', '60', '2328', '1080', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('50', '61', '2328', '1081', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('51', '62', '2328', '1082', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('52', '63', '2328', '1083', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('53', '65', '2328', '1084', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('54', '66', '2328', '1085', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('55', '68', '2328', '1086', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('56', '71', '2328', '1087', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('57', '72', '2328', '1088', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('58', '73', '2328', '1089', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('59', '74', '2328', '1090', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('60', '75', '2328', '1091', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('61', '76', '2328', '1092', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('62', '77', '2328', '1093', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('63', '78', '2328', '1094', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('64', '79', '2328', '1095', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('65', '80', '2328', '1096', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('66', '81', '2328', '1097', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('67', '82', '2328', '1098', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('68', '83', '2328', '1099', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('69', '84', '2328', '1100', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('70', '85', '2328', '1101', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('71', '86', '2328', '1102', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('72', '87', '2328', '1103', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('73', '88', '2328', '1104', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('74', '89', '2328', '1105', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('75', '90', '2328', '1106', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('76', '91', '2328', '1107', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('77', '92', '2328', '1108', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('78', '93', '2328', '1109', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('79', '94', '2328', '1110', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('80', '95', '2328', '1111', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('81', '96', '2328', '1112', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('82', '97', '2328', '1113', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('83', '98', '2328', '1114', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('84', '99', '2328', '1115', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('85', '100', '2328', '1116', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('86', '101', '2328', '1117', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('87', '105', '2328', '1118', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('88', '106', '2328', '1119', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('89', '107', '2328', '1120', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('90', '108', '2328', '1121', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('91', '109', '2328', '1122', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('92', '110', '2328', '1123', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('93', '111', '2328', '1124', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('94', '112', '2328', '1125', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('95', '113', '2328', '1126', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('96', '114', '2328', '1127', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('97', '115', '2328', '1128', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('98', '116', '2328', '1129', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('99', '117', '2328', '1130', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('100', '118', '2328', '1131', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('101', '119', '2328', '1132', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('102', '120', '2328', '1133', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('103', '121', '2328', '1134', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('104', '122', '2328', '1135', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('105', '123', '2328', '1136', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('106', '125', '2328', '1137', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('107', '126', '2328', '1138', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('108', '127', '2328', '1139', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('109', '128', '2328', '1140', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('110', '130', '2328', '1141', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('111', '131', '2328', '1142', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('112', '132', '2328', '1143', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('113', '133', '2328', '1144', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('114', '134', '2328', '1145', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('115', '135', '2328', '1146', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('116', '136', '2328', '1147', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('117', '137', '2328', '1148', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('118', '138', '2328', '1149', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('119', '139', '2328', '1150', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('120', '140', '2328', '1151', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('121', '141', '2328', '1152', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('122', '142', '2328', '1153', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('123', '143', '2328', '1154', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('124', '144', '2328', '1155', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('125', '145', '2328', '1156', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('126', '146', '2328', '1157', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('127', '147', '2328', '1158', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('128', '148', '2328', '1159', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('129', '149', '2328', '1160', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('130', '150', '2328', '1161', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('131', '151', '2328', '1162', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('132', '152', '2328', '1163', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('133', '153', '2328', '1164', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('134', '154', '2328', '1165', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('135', '155', '2328', '1166', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('136', '156', '2328', '1167', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('137', '157', '2328', '1168', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('138', '158', '2328', '1169', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('139', '159', '2328', '1170', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('140', '162', '2328', '1171', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('141', '163', '2328', '1172', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('142', '165', '2328', '1173', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('143', '166', '2328', '1174', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('144', '167', '2328', '1175', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('145', '168', '2328', '1176', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('146', '169', '2328', '1177', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('147', '170', '2328', '1178', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('148', '171', '2328', '1179', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('149', '172', '2328', '1180', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('150', '173', '2328', '1181', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('151', '174', '2328', '1182', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('152', '175', '2328', '1183', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('153', '176', '2328', '1184', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('154', '177', '2328', '1185', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('155', '178', '2328', '1186', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('156', '179', '2328', '1187', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('157', '180', '2328', '1188', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('158', '181', '2328', '1189', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('159', '182', '2328', '1190', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('160', '183', '2328', '1191', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('161', '184', '2328', '1192', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('162', '185', '2328', '1193', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('163', '187', '2328', '1194', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('164', '188', '2328', '1195', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('165', '189', '2328', '1196', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('166', '190', '2328', '1197', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('167', '192', '2328', '1198', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('168', '193', '2328', '1199', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('169', '194', '2328', '1200', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('170', '195', '2328', '1201', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('171', '196', '2328', '1202', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('172', '197', '2328', '1203', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('173', '198', '2328', '1204', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('174', '199', '2328', '1205', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('175', '200', '2328', '1206', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('176', '202', '2328', '1207', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('177', '204', '2328', '1208', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('178', '205', '2328', '1209', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('179', '206', '2328', '1210', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('180', '207', '2328', '1211', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('181', '208', '2328', '1212', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('182', '209', '2328', '1213', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('183', '212', '2328', '1214', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('184', '213', '2328', '1215', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('185', '214', '2328', '1216', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('186', '215', '2328', '1217', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('187', '216', '2328', '1218', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('188', '217', '2328', '1219', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('189', '218', '2328', '1220', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('190', '219', '2328', '1221', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('191', '220', '2328', '1222', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('192', '221', '2328', '1223', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('193', '222', '2328', '1224', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('194', '223', '2328', '1225', 'G', '1');

INSERT INTO experiment.experiment_data (id, experiment_id, variable_id, data_value, data_qc_code, creator_id)
VALUES ('195', '224', '2328', '1226', 'G', '1');


-- update sequences
SELECT SETVAL('platform.list_id_seq', COALESCE(MAX(id), 1)) FROM platform.list;
SELECT SETVAL('platform.list_member_id_seq', COALESCE(MAX(id), 1)) FROM platform.list_member;
SELECT SETVAL('tenant.protocol_data_id_seq', COALESCE(MAX(id), 1)) FROM tenant.protocol_data;
SELECT SETVAL('tenant.protocol_id_seq', COALESCE(MAX(id), 1)) FROM tenant.protocol;
SELECT SETVAL('experiment.experiment_protocol_id_seq', COALESCE(MAX(id), 1)) FROM experiment.experiment_protocol;
SELECT SETVAL('experiment.experiment_data_id_seq', COALESCE(MAX(id), 1)) FROM experiment.experiment_data;



--rollback DELETE FROM experiment.experiment_data WHERE id = '9';
--rollback DELETE FROM experiment.experiment_data WHERE id = '10';
--rollback DELETE FROM experiment.experiment_data WHERE id = '11';
--rollback DELETE FROM experiment.experiment_data WHERE id = '12';
--rollback DELETE FROM experiment.experiment_data WHERE id = '13';
--rollback DELETE FROM experiment.experiment_data WHERE id = '14';
--rollback DELETE FROM experiment.experiment_data WHERE id = '15';
--rollback DELETE FROM experiment.experiment_data WHERE id = '16';
--rollback DELETE FROM experiment.experiment_data WHERE id = '17';
--rollback DELETE FROM experiment.experiment_data WHERE id = '18';
--rollback DELETE FROM experiment.experiment_data WHERE id = '19';
--rollback DELETE FROM experiment.experiment_data WHERE id = '20';
--rollback DELETE FROM experiment.experiment_data WHERE id = '21';
--rollback DELETE FROM experiment.experiment_data WHERE id = '22';
--rollback DELETE FROM experiment.experiment_data WHERE id = '23';
--rollback DELETE FROM experiment.experiment_data WHERE id = '24';
--rollback DELETE FROM experiment.experiment_data WHERE id = '25';
--rollback DELETE FROM experiment.experiment_data WHERE id = '26';
--rollback DELETE FROM experiment.experiment_data WHERE id = '27';
--rollback DELETE FROM experiment.experiment_data WHERE id = '28';
--rollback DELETE FROM experiment.experiment_data WHERE id = '29';
--rollback DELETE FROM experiment.experiment_data WHERE id = '30';
--rollback DELETE FROM experiment.experiment_data WHERE id = '31';
--rollback DELETE FROM experiment.experiment_data WHERE id = '32';
--rollback DELETE FROM experiment.experiment_data WHERE id = '33';
--rollback DELETE FROM experiment.experiment_data WHERE id = '34';
--rollback DELETE FROM experiment.experiment_data WHERE id = '35';
--rollback DELETE FROM experiment.experiment_data WHERE id = '36';
--rollback DELETE FROM experiment.experiment_data WHERE id = '37';
--rollback DELETE FROM experiment.experiment_data WHERE id = '38';
--rollback DELETE FROM experiment.experiment_data WHERE id = '39';
--rollback DELETE FROM experiment.experiment_data WHERE id = '40';
--rollback DELETE FROM experiment.experiment_data WHERE id = '41';
--rollback DELETE FROM experiment.experiment_data WHERE id = '42';
--rollback DELETE FROM experiment.experiment_data WHERE id = '43';
--rollback DELETE FROM experiment.experiment_data WHERE id = '44';
--rollback DELETE FROM experiment.experiment_data WHERE id = '45';
--rollback DELETE FROM experiment.experiment_data WHERE id = '46';
--rollback DELETE FROM experiment.experiment_data WHERE id = '47';
--rollback DELETE FROM experiment.experiment_data WHERE id = '48';
--rollback DELETE FROM experiment.experiment_data WHERE id = '49';
--rollback DELETE FROM experiment.experiment_data WHERE id = '50';
--rollback DELETE FROM experiment.experiment_data WHERE id = '51';
--rollback DELETE FROM experiment.experiment_data WHERE id = '52';
--rollback DELETE FROM experiment.experiment_data WHERE id = '53';
--rollback DELETE FROM experiment.experiment_data WHERE id = '54';
--rollback DELETE FROM experiment.experiment_data WHERE id = '55';
--rollback DELETE FROM experiment.experiment_data WHERE id = '56';
--rollback DELETE FROM experiment.experiment_data WHERE id = '57';
--rollback DELETE FROM experiment.experiment_data WHERE id = '58';
--rollback DELETE FROM experiment.experiment_data WHERE id = '59';
--rollback DELETE FROM experiment.experiment_data WHERE id = '60';
--rollback DELETE FROM experiment.experiment_data WHERE id = '61';
--rollback DELETE FROM experiment.experiment_data WHERE id = '62';
--rollback DELETE FROM experiment.experiment_data WHERE id = '63';
--rollback DELETE FROM experiment.experiment_data WHERE id = '64';
--rollback DELETE FROM experiment.experiment_data WHERE id = '65';
--rollback DELETE FROM experiment.experiment_data WHERE id = '66';
--rollback DELETE FROM experiment.experiment_data WHERE id = '67';
--rollback DELETE FROM experiment.experiment_data WHERE id = '68';
--rollback DELETE FROM experiment.experiment_data WHERE id = '69';
--rollback DELETE FROM experiment.experiment_data WHERE id = '70';
--rollback DELETE FROM experiment.experiment_data WHERE id = '71';
--rollback DELETE FROM experiment.experiment_data WHERE id = '72';
--rollback DELETE FROM experiment.experiment_data WHERE id = '73';
--rollback DELETE FROM experiment.experiment_data WHERE id = '74';
--rollback DELETE FROM experiment.experiment_data WHERE id = '75';
--rollback DELETE FROM experiment.experiment_data WHERE id = '76';
--rollback DELETE FROM experiment.experiment_data WHERE id = '77';
--rollback DELETE FROM experiment.experiment_data WHERE id = '78';
--rollback DELETE FROM experiment.experiment_data WHERE id = '79';
--rollback DELETE FROM experiment.experiment_data WHERE id = '80';
--rollback DELETE FROM experiment.experiment_data WHERE id = '81';
--rollback DELETE FROM experiment.experiment_data WHERE id = '82';
--rollback DELETE FROM experiment.experiment_data WHERE id = '83';
--rollback DELETE FROM experiment.experiment_data WHERE id = '84';
--rollback DELETE FROM experiment.experiment_data WHERE id = '85';
--rollback DELETE FROM experiment.experiment_data WHERE id = '86';
--rollback DELETE FROM experiment.experiment_data WHERE id = '87';
--rollback DELETE FROM experiment.experiment_data WHERE id = '88';
--rollback DELETE FROM experiment.experiment_data WHERE id = '89';
--rollback DELETE FROM experiment.experiment_data WHERE id = '90';
--rollback DELETE FROM experiment.experiment_data WHERE id = '91';
--rollback DELETE FROM experiment.experiment_data WHERE id = '92';
--rollback DELETE FROM experiment.experiment_data WHERE id = '93';
--rollback DELETE FROM experiment.experiment_data WHERE id = '94';
--rollback DELETE FROM experiment.experiment_data WHERE id = '95';
--rollback DELETE FROM experiment.experiment_data WHERE id = '96';
--rollback DELETE FROM experiment.experiment_data WHERE id = '97';
--rollback DELETE FROM experiment.experiment_data WHERE id = '98';
--rollback DELETE FROM experiment.experiment_data WHERE id = '99';
--rollback DELETE FROM experiment.experiment_data WHERE id = '100';
--rollback DELETE FROM experiment.experiment_data WHERE id = '101';
--rollback DELETE FROM experiment.experiment_data WHERE id = '102';
--rollback DELETE FROM experiment.experiment_data WHERE id = '103';
--rollback DELETE FROM experiment.experiment_data WHERE id = '104';
--rollback DELETE FROM experiment.experiment_data WHERE id = '105';
--rollback DELETE FROM experiment.experiment_data WHERE id = '106';
--rollback DELETE FROM experiment.experiment_data WHERE id = '107';
--rollback DELETE FROM experiment.experiment_data WHERE id = '108';
--rollback DELETE FROM experiment.experiment_data WHERE id = '109';
--rollback DELETE FROM experiment.experiment_data WHERE id = '110';
--rollback DELETE FROM experiment.experiment_data WHERE id = '111';
--rollback DELETE FROM experiment.experiment_data WHERE id = '112';
--rollback DELETE FROM experiment.experiment_data WHERE id = '113';
--rollback DELETE FROM experiment.experiment_data WHERE id = '114';
--rollback DELETE FROM experiment.experiment_data WHERE id = '115';
--rollback DELETE FROM experiment.experiment_data WHERE id = '116';
--rollback DELETE FROM experiment.experiment_data WHERE id = '117';
--rollback DELETE FROM experiment.experiment_data WHERE id = '118';
--rollback DELETE FROM experiment.experiment_data WHERE id = '119';
--rollback DELETE FROM experiment.experiment_data WHERE id = '120';
--rollback DELETE FROM experiment.experiment_data WHERE id = '121';
--rollback DELETE FROM experiment.experiment_data WHERE id = '122';
--rollback DELETE FROM experiment.experiment_data WHERE id = '123';
--rollback DELETE FROM experiment.experiment_data WHERE id = '124';
--rollback DELETE FROM experiment.experiment_data WHERE id = '125';
--rollback DELETE FROM experiment.experiment_data WHERE id = '126';
--rollback DELETE FROM experiment.experiment_data WHERE id = '127';
--rollback DELETE FROM experiment.experiment_data WHERE id = '128';
--rollback DELETE FROM experiment.experiment_data WHERE id = '129';
--rollback DELETE FROM experiment.experiment_data WHERE id = '130';
--rollback DELETE FROM experiment.experiment_data WHERE id = '131';
--rollback DELETE FROM experiment.experiment_data WHERE id = '132';
--rollback DELETE FROM experiment.experiment_data WHERE id = '133';
--rollback DELETE FROM experiment.experiment_data WHERE id = '134';
--rollback DELETE FROM experiment.experiment_data WHERE id = '135';
--rollback DELETE FROM experiment.experiment_data WHERE id = '136';
--rollback DELETE FROM experiment.experiment_data WHERE id = '137';
--rollback DELETE FROM experiment.experiment_data WHERE id = '138';
--rollback DELETE FROM experiment.experiment_data WHERE id = '139';
--rollback DELETE FROM experiment.experiment_data WHERE id = '140';
--rollback DELETE FROM experiment.experiment_data WHERE id = '141';
--rollback DELETE FROM experiment.experiment_data WHERE id = '142';
--rollback DELETE FROM experiment.experiment_data WHERE id = '143';
--rollback DELETE FROM experiment.experiment_data WHERE id = '144';
--rollback DELETE FROM experiment.experiment_data WHERE id = '145';
--rollback DELETE FROM experiment.experiment_data WHERE id = '146';
--rollback DELETE FROM experiment.experiment_data WHERE id = '147';
--rollback DELETE FROM experiment.experiment_data WHERE id = '148';
--rollback DELETE FROM experiment.experiment_data WHERE id = '149';
--rollback DELETE FROM experiment.experiment_data WHERE id = '150';
--rollback DELETE FROM experiment.experiment_data WHERE id = '151';
--rollback DELETE FROM experiment.experiment_data WHERE id = '152';
--rollback DELETE FROM experiment.experiment_data WHERE id = '153';
--rollback DELETE FROM experiment.experiment_data WHERE id = '154';
--rollback DELETE FROM experiment.experiment_data WHERE id = '155';
--rollback DELETE FROM experiment.experiment_data WHERE id = '156';
--rollback DELETE FROM experiment.experiment_data WHERE id = '157';
--rollback DELETE FROM experiment.experiment_data WHERE id = '158';
--rollback DELETE FROM experiment.experiment_data WHERE id = '159';
--rollback DELETE FROM experiment.experiment_data WHERE id = '160';
--rollback DELETE FROM experiment.experiment_data WHERE id = '161';
--rollback DELETE FROM experiment.experiment_data WHERE id = '162';
--rollback DELETE FROM experiment.experiment_data WHERE id = '163';
--rollback DELETE FROM experiment.experiment_data WHERE id = '164';
--rollback DELETE FROM experiment.experiment_data WHERE id = '165';
--rollback DELETE FROM experiment.experiment_data WHERE id = '166';
--rollback DELETE FROM experiment.experiment_data WHERE id = '167';
--rollback DELETE FROM experiment.experiment_data WHERE id = '168';
--rollback DELETE FROM experiment.experiment_data WHERE id = '169';
--rollback DELETE FROM experiment.experiment_data WHERE id = '170';
--rollback DELETE FROM experiment.experiment_data WHERE id = '171';
--rollback DELETE FROM experiment.experiment_data WHERE id = '172';
--rollback DELETE FROM experiment.experiment_data WHERE id = '173';
--rollback DELETE FROM experiment.experiment_data WHERE id = '174';
--rollback DELETE FROM experiment.experiment_data WHERE id = '175';
--rollback DELETE FROM experiment.experiment_data WHERE id = '176';
--rollback DELETE FROM experiment.experiment_data WHERE id = '177';
--rollback DELETE FROM experiment.experiment_data WHERE id = '178';
--rollback DELETE FROM experiment.experiment_data WHERE id = '179';
--rollback DELETE FROM experiment.experiment_data WHERE id = '180';
--rollback DELETE FROM experiment.experiment_data WHERE id = '181';
--rollback DELETE FROM experiment.experiment_data WHERE id = '182';
--rollback DELETE FROM experiment.experiment_data WHERE id = '183';
--rollback DELETE FROM experiment.experiment_data WHERE id = '184';
--rollback DELETE FROM experiment.experiment_data WHERE id = '185';
--rollback DELETE FROM experiment.experiment_data WHERE id = '186';
--rollback DELETE FROM experiment.experiment_data WHERE id = '187';
--rollback DELETE FROM experiment.experiment_data WHERE id = '188';
--rollback DELETE FROM experiment.experiment_data WHERE id = '189';
--rollback DELETE FROM experiment.experiment_data WHERE id = '190';
--rollback DELETE FROM experiment.experiment_data WHERE id = '191';
--rollback DELETE FROM experiment.experiment_data WHERE id = '192';
--rollback DELETE FROM experiment.experiment_data WHERE id = '193';
--rollback DELETE FROM experiment.experiment_data WHERE id = '194';
--rollback DELETE FROM experiment.experiment_data WHERE id = '195';

--rollback DELETE FROM experiment.experiment_protocol WHERE id = '1';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '2';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '3';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '4';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '5';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '6';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '7';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '8';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '9';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '10';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '11';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '12';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '13';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '14';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '15';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '16';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '17';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '18';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '19';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '20';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '21';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '22';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '23';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '24';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '25';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '26';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '27';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '28';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '29';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '30';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '31';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '32';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '33';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '34';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '35';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '36';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '37';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '38';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '39';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '40';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '41';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '42';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '43';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '44';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '45';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '46';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '47';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '48';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '49';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '50';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '51';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '52';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '53';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '54';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '55';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '56';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '57';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '58';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '59';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '60';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '61';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '62';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '63';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '64';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '65';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '66';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '67';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '68';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '69';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '70';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '71';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '72';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '73';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '74';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '75';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '76';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '77';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '78';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '79';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '80';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '81';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '82';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '83';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '84';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '85';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '86';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '87';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '88';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '89';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '90';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '91';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '92';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '93';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '94';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '95';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '96';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '97';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '98';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '99';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '100';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '101';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '102';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '103';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '104';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '105';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '106';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '107';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '108';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '109';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '110';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '111';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '112';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '113';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '114';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '115';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '116';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '117';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '118';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '119';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '120';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '121';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '122';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '123';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '124';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '125';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '126';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '127';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '128';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '129';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '130';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '131';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '132';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '133';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '134';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '135';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '136';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '137';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '138';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '139';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '140';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '141';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '142';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '143';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '144';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '145';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '146';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '147';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '148';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '149';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '150';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '151';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '152';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '153';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '154';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '155';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '156';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '157';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '158';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '159';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '160';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '161';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '162';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '163';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '164';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '165';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '166';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '167';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '168';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '169';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '170';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '171';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '172';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '173';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '174';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '175';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '176';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '177';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '178';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '179';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '180';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '181';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '182';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '183';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '184';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '185';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '186';
--rollback DELETE FROM experiment.experiment_protocol WHERE id = '187';

--rollback DELETE FROM tenant.protocol_data WHERE id = '1';
--rollback DELETE FROM tenant.protocol_data WHERE id = '2';
--rollback DELETE FROM tenant.protocol_data WHERE id = '3';
--rollback DELETE FROM tenant.protocol_data WHERE id = '4';
--rollback DELETE FROM tenant.protocol_data WHERE id = '5';
--rollback DELETE FROM tenant.protocol_data WHERE id = '6';
--rollback DELETE FROM tenant.protocol_data WHERE id = '7';
--rollback DELETE FROM tenant.protocol_data WHERE id = '8';
--rollback DELETE FROM tenant.protocol_data WHERE id = '9';
--rollback DELETE FROM tenant.protocol_data WHERE id = '10';
--rollback DELETE FROM tenant.protocol_data WHERE id = '11';
--rollback DELETE FROM tenant.protocol_data WHERE id = '12';
--rollback DELETE FROM tenant.protocol_data WHERE id = '13';
--rollback DELETE FROM tenant.protocol_data WHERE id = '14';
--rollback DELETE FROM tenant.protocol_data WHERE id = '15';
--rollback DELETE FROM tenant.protocol_data WHERE id = '16';
--rollback DELETE FROM tenant.protocol_data WHERE id = '17';
--rollback DELETE FROM tenant.protocol_data WHERE id = '18';
--rollback DELETE FROM tenant.protocol_data WHERE id = '19';
--rollback DELETE FROM tenant.protocol_data WHERE id = '20';
--rollback DELETE FROM tenant.protocol_data WHERE id = '21';
--rollback DELETE FROM tenant.protocol_data WHERE id = '22';
--rollback DELETE FROM tenant.protocol_data WHERE id = '23';
--rollback DELETE FROM tenant.protocol_data WHERE id = '24';
--rollback DELETE FROM tenant.protocol_data WHERE id = '25';
--rollback DELETE FROM tenant.protocol_data WHERE id = '26';
--rollback DELETE FROM tenant.protocol_data WHERE id = '27';
--rollback DELETE FROM tenant.protocol_data WHERE id = '28';
--rollback DELETE FROM tenant.protocol_data WHERE id = '29';
--rollback DELETE FROM tenant.protocol_data WHERE id = '30';
--rollback DELETE FROM tenant.protocol_data WHERE id = '31';
--rollback DELETE FROM tenant.protocol_data WHERE id = '32';
--rollback DELETE FROM tenant.protocol_data WHERE id = '33';
--rollback DELETE FROM tenant.protocol_data WHERE id = '34';
--rollback DELETE FROM tenant.protocol_data WHERE id = '35';
--rollback DELETE FROM tenant.protocol_data WHERE id = '36';
--rollback DELETE FROM tenant.protocol_data WHERE id = '37';
--rollback DELETE FROM tenant.protocol_data WHERE id = '38';
--rollback DELETE FROM tenant.protocol_data WHERE id = '39';
--rollback DELETE FROM tenant.protocol_data WHERE id = '40';
--rollback DELETE FROM tenant.protocol_data WHERE id = '41';
--rollback DELETE FROM tenant.protocol_data WHERE id = '42';
--rollback DELETE FROM tenant.protocol_data WHERE id = '43';
--rollback DELETE FROM tenant.protocol_data WHERE id = '44';
--rollback DELETE FROM tenant.protocol_data WHERE id = '45';
--rollback DELETE FROM tenant.protocol_data WHERE id = '46';
--rollback DELETE FROM tenant.protocol_data WHERE id = '47';
--rollback DELETE FROM tenant.protocol_data WHERE id = '48';
--rollback DELETE FROM tenant.protocol_data WHERE id = '49';
--rollback DELETE FROM tenant.protocol_data WHERE id = '50';
--rollback DELETE FROM tenant.protocol_data WHERE id = '51';
--rollback DELETE FROM tenant.protocol_data WHERE id = '52';
--rollback DELETE FROM tenant.protocol_data WHERE id = '53';
--rollback DELETE FROM tenant.protocol_data WHERE id = '54';
--rollback DELETE FROM tenant.protocol_data WHERE id = '55';
--rollback DELETE FROM tenant.protocol_data WHERE id = '56';
--rollback DELETE FROM tenant.protocol_data WHERE id = '57';
--rollback DELETE FROM tenant.protocol_data WHERE id = '58';
--rollback DELETE FROM tenant.protocol_data WHERE id = '59';
--rollback DELETE FROM tenant.protocol_data WHERE id = '60';
--rollback DELETE FROM tenant.protocol_data WHERE id = '61';
--rollback DELETE FROM tenant.protocol_data WHERE id = '62';
--rollback DELETE FROM tenant.protocol_data WHERE id = '63';
--rollback DELETE FROM tenant.protocol_data WHERE id = '64';
--rollback DELETE FROM tenant.protocol_data WHERE id = '65';
--rollback DELETE FROM tenant.protocol_data WHERE id = '66';
--rollback DELETE FROM tenant.protocol_data WHERE id = '67';
--rollback DELETE FROM tenant.protocol_data WHERE id = '68';
--rollback DELETE FROM tenant.protocol_data WHERE id = '69';
--rollback DELETE FROM tenant.protocol_data WHERE id = '70';
--rollback DELETE FROM tenant.protocol_data WHERE id = '71';
--rollback DELETE FROM tenant.protocol_data WHERE id = '72';
--rollback DELETE FROM tenant.protocol_data WHERE id = '73';
--rollback DELETE FROM tenant.protocol_data WHERE id = '74';
--rollback DELETE FROM tenant.protocol_data WHERE id = '75';
--rollback DELETE FROM tenant.protocol_data WHERE id = '76';
--rollback DELETE FROM tenant.protocol_data WHERE id = '77';
--rollback DELETE FROM tenant.protocol_data WHERE id = '78';
--rollback DELETE FROM tenant.protocol_data WHERE id = '79';
--rollback DELETE FROM tenant.protocol_data WHERE id = '80';
--rollback DELETE FROM tenant.protocol_data WHERE id = '81';
--rollback DELETE FROM tenant.protocol_data WHERE id = '82';
--rollback DELETE FROM tenant.protocol_data WHERE id = '83';
--rollback DELETE FROM tenant.protocol_data WHERE id = '84';
--rollback DELETE FROM tenant.protocol_data WHERE id = '85';
--rollback DELETE FROM tenant.protocol_data WHERE id = '86';
--rollback DELETE FROM tenant.protocol_data WHERE id = '87';
--rollback DELETE FROM tenant.protocol_data WHERE id = '88';
--rollback DELETE FROM tenant.protocol_data WHERE id = '89';
--rollback DELETE FROM tenant.protocol_data WHERE id = '90';
--rollback DELETE FROM tenant.protocol_data WHERE id = '91';
--rollback DELETE FROM tenant.protocol_data WHERE id = '92';
--rollback DELETE FROM tenant.protocol_data WHERE id = '93';
--rollback DELETE FROM tenant.protocol_data WHERE id = '94';
--rollback DELETE FROM tenant.protocol_data WHERE id = '95';
--rollback DELETE FROM tenant.protocol_data WHERE id = '96';
--rollback DELETE FROM tenant.protocol_data WHERE id = '97';
--rollback DELETE FROM tenant.protocol_data WHERE id = '98';
--rollback DELETE FROM tenant.protocol_data WHERE id = '99';
--rollback DELETE FROM tenant.protocol_data WHERE id = '100';
--rollback DELETE FROM tenant.protocol_data WHERE id = '101';
--rollback DELETE FROM tenant.protocol_data WHERE id = '102';
--rollback DELETE FROM tenant.protocol_data WHERE id = '103';
--rollback DELETE FROM tenant.protocol_data WHERE id = '104';
--rollback DELETE FROM tenant.protocol_data WHERE id = '105';
--rollback DELETE FROM tenant.protocol_data WHERE id = '106';
--rollback DELETE FROM tenant.protocol_data WHERE id = '107';
--rollback DELETE FROM tenant.protocol_data WHERE id = '108';
--rollback DELETE FROM tenant.protocol_data WHERE id = '109';
--rollback DELETE FROM tenant.protocol_data WHERE id = '110';
--rollback DELETE FROM tenant.protocol_data WHERE id = '111';
--rollback DELETE FROM tenant.protocol_data WHERE id = '112';
--rollback DELETE FROM tenant.protocol_data WHERE id = '113';
--rollback DELETE FROM tenant.protocol_data WHERE id = '114';
--rollback DELETE FROM tenant.protocol_data WHERE id = '115';
--rollback DELETE FROM tenant.protocol_data WHERE id = '116';
--rollback DELETE FROM tenant.protocol_data WHERE id = '117';
--rollback DELETE FROM tenant.protocol_data WHERE id = '118';
--rollback DELETE FROM tenant.protocol_data WHERE id = '119';
--rollback DELETE FROM tenant.protocol_data WHERE id = '120';
--rollback DELETE FROM tenant.protocol_data WHERE id = '121';
--rollback DELETE FROM tenant.protocol_data WHERE id = '122';
--rollback DELETE FROM tenant.protocol_data WHERE id = '123';
--rollback DELETE FROM tenant.protocol_data WHERE id = '124';
--rollback DELETE FROM tenant.protocol_data WHERE id = '125';
--rollback DELETE FROM tenant.protocol_data WHERE id = '126';
--rollback DELETE FROM tenant.protocol_data WHERE id = '127';
--rollback DELETE FROM tenant.protocol_data WHERE id = '128';
--rollback DELETE FROM tenant.protocol_data WHERE id = '129';
--rollback DELETE FROM tenant.protocol_data WHERE id = '130';
--rollback DELETE FROM tenant.protocol_data WHERE id = '131';
--rollback DELETE FROM tenant.protocol_data WHERE id = '132';
--rollback DELETE FROM tenant.protocol_data WHERE id = '133';
--rollback DELETE FROM tenant.protocol_data WHERE id = '134';
--rollback DELETE FROM tenant.protocol_data WHERE id = '135';
--rollback DELETE FROM tenant.protocol_data WHERE id = '136';
--rollback DELETE FROM tenant.protocol_data WHERE id = '137';
--rollback DELETE FROM tenant.protocol_data WHERE id = '138';
--rollback DELETE FROM tenant.protocol_data WHERE id = '139';
--rollback DELETE FROM tenant.protocol_data WHERE id = '140';
--rollback DELETE FROM tenant.protocol_data WHERE id = '141';
--rollback DELETE FROM tenant.protocol_data WHERE id = '142';
--rollback DELETE FROM tenant.protocol_data WHERE id = '143';
--rollback DELETE FROM tenant.protocol_data WHERE id = '144';
--rollback DELETE FROM tenant.protocol_data WHERE id = '145';
--rollback DELETE FROM tenant.protocol_data WHERE id = '146';
--rollback DELETE FROM tenant.protocol_data WHERE id = '147';
--rollback DELETE FROM tenant.protocol_data WHERE id = '148';
--rollback DELETE FROM tenant.protocol_data WHERE id = '149';
--rollback DELETE FROM tenant.protocol_data WHERE id = '150';
--rollback DELETE FROM tenant.protocol_data WHERE id = '151';
--rollback DELETE FROM tenant.protocol_data WHERE id = '152';
--rollback DELETE FROM tenant.protocol_data WHERE id = '153';
--rollback DELETE FROM tenant.protocol_data WHERE id = '154';
--rollback DELETE FROM tenant.protocol_data WHERE id = '155';
--rollback DELETE FROM tenant.protocol_data WHERE id = '156';
--rollback DELETE FROM tenant.protocol_data WHERE id = '157';
--rollback DELETE FROM tenant.protocol_data WHERE id = '158';
--rollback DELETE FROM tenant.protocol_data WHERE id = '159';
--rollback DELETE FROM tenant.protocol_data WHERE id = '160';
--rollback DELETE FROM tenant.protocol_data WHERE id = '161';
--rollback DELETE FROM tenant.protocol_data WHERE id = '162';
--rollback DELETE FROM tenant.protocol_data WHERE id = '163';
--rollback DELETE FROM tenant.protocol_data WHERE id = '164';
--rollback DELETE FROM tenant.protocol_data WHERE id = '165';
--rollback DELETE FROM tenant.protocol_data WHERE id = '166';
--rollback DELETE FROM tenant.protocol_data WHERE id = '167';
--rollback DELETE FROM tenant.protocol_data WHERE id = '168';
--rollback DELETE FROM tenant.protocol_data WHERE id = '169';
--rollback DELETE FROM tenant.protocol_data WHERE id = '170';
--rollback DELETE FROM tenant.protocol_data WHERE id = '171';
--rollback DELETE FROM tenant.protocol_data WHERE id = '172';
--rollback DELETE FROM tenant.protocol_data WHERE id = '173';
--rollback DELETE FROM tenant.protocol_data WHERE id = '174';
--rollback DELETE FROM tenant.protocol_data WHERE id = '175';
--rollback DELETE FROM tenant.protocol_data WHERE id = '176';
--rollback DELETE FROM tenant.protocol_data WHERE id = '177';
--rollback DELETE FROM tenant.protocol_data WHERE id = '178';
--rollback DELETE FROM tenant.protocol_data WHERE id = '179';
--rollback DELETE FROM tenant.protocol_data WHERE id = '180';
--rollback DELETE FROM tenant.protocol_data WHERE id = '181';
--rollback DELETE FROM tenant.protocol_data WHERE id = '182';
--rollback DELETE FROM tenant.protocol_data WHERE id = '183';
--rollback DELETE FROM tenant.protocol_data WHERE id = '184';
--rollback DELETE FROM tenant.protocol_data WHERE id = '185';
--rollback DELETE FROM tenant.protocol_data WHERE id = '186';
--rollback DELETE FROM tenant.protocol_data WHERE id = '187';

--rollback DELETE FROM tenant.protocol WHERE id = '5';
--rollback DELETE FROM tenant.protocol WHERE id = '6';
--rollback DELETE FROM tenant.protocol WHERE id = '7';
--rollback DELETE FROM tenant.protocol WHERE id = '8';
--rollback DELETE FROM tenant.protocol WHERE id = '9';
--rollback DELETE FROM tenant.protocol WHERE id = '10';
--rollback DELETE FROM tenant.protocol WHERE id = '11';
--rollback DELETE FROM tenant.protocol WHERE id = '12';
--rollback DELETE FROM tenant.protocol WHERE id = '13';
--rollback DELETE FROM tenant.protocol WHERE id = '14';
--rollback DELETE FROM tenant.protocol WHERE id = '15';
--rollback DELETE FROM tenant.protocol WHERE id = '16';
--rollback DELETE FROM tenant.protocol WHERE id = '17';
--rollback DELETE FROM tenant.protocol WHERE id = '18';
--rollback DELETE FROM tenant.protocol WHERE id = '19';
--rollback DELETE FROM tenant.protocol WHERE id = '20';
--rollback DELETE FROM tenant.protocol WHERE id = '21';
--rollback DELETE FROM tenant.protocol WHERE id = '22';
--rollback DELETE FROM tenant.protocol WHERE id = '23';
--rollback DELETE FROM tenant.protocol WHERE id = '24';
--rollback DELETE FROM tenant.protocol WHERE id = '25';
--rollback DELETE FROM tenant.protocol WHERE id = '26';
--rollback DELETE FROM tenant.protocol WHERE id = '27';
--rollback DELETE FROM tenant.protocol WHERE id = '28';
--rollback DELETE FROM tenant.protocol WHERE id = '29';
--rollback DELETE FROM tenant.protocol WHERE id = '30';
--rollback DELETE FROM tenant.protocol WHERE id = '31';
--rollback DELETE FROM tenant.protocol WHERE id = '32';
--rollback DELETE FROM tenant.protocol WHERE id = '33';
--rollback DELETE FROM tenant.protocol WHERE id = '34';
--rollback DELETE FROM tenant.protocol WHERE id = '35';
--rollback DELETE FROM tenant.protocol WHERE id = '36';
--rollback DELETE FROM tenant.protocol WHERE id = '37';
--rollback DELETE FROM tenant.protocol WHERE id = '38';
--rollback DELETE FROM tenant.protocol WHERE id = '39';
--rollback DELETE FROM tenant.protocol WHERE id = '40';
--rollback DELETE FROM tenant.protocol WHERE id = '41';
--rollback DELETE FROM tenant.protocol WHERE id = '42';
--rollback DELETE FROM tenant.protocol WHERE id = '43';
--rollback DELETE FROM tenant.protocol WHERE id = '44';
--rollback DELETE FROM tenant.protocol WHERE id = '45';
--rollback DELETE FROM tenant.protocol WHERE id = '46';
--rollback DELETE FROM tenant.protocol WHERE id = '47';
--rollback DELETE FROM tenant.protocol WHERE id = '48';
--rollback DELETE FROM tenant.protocol WHERE id = '49';
--rollback DELETE FROM tenant.protocol WHERE id = '50';
--rollback DELETE FROM tenant.protocol WHERE id = '51';
--rollback DELETE FROM tenant.protocol WHERE id = '52';
--rollback DELETE FROM tenant.protocol WHERE id = '53';
--rollback DELETE FROM tenant.protocol WHERE id = '54';
--rollback DELETE FROM tenant.protocol WHERE id = '55';
--rollback DELETE FROM tenant.protocol WHERE id = '56';
--rollback DELETE FROM tenant.protocol WHERE id = '57';
--rollback DELETE FROM tenant.protocol WHERE id = '58';
--rollback DELETE FROM tenant.protocol WHERE id = '59';
--rollback DELETE FROM tenant.protocol WHERE id = '60';
--rollback DELETE FROM tenant.protocol WHERE id = '61';
--rollback DELETE FROM tenant.protocol WHERE id = '62';
--rollback DELETE FROM tenant.protocol WHERE id = '63';
--rollback DELETE FROM tenant.protocol WHERE id = '64';
--rollback DELETE FROM tenant.protocol WHERE id = '65';
--rollback DELETE FROM tenant.protocol WHERE id = '66';
--rollback DELETE FROM tenant.protocol WHERE id = '67';
--rollback DELETE FROM tenant.protocol WHERE id = '68';
--rollback DELETE FROM tenant.protocol WHERE id = '69';
--rollback DELETE FROM tenant.protocol WHERE id = '70';
--rollback DELETE FROM tenant.protocol WHERE id = '71';
--rollback DELETE FROM tenant.protocol WHERE id = '72';
--rollback DELETE FROM tenant.protocol WHERE id = '73';
--rollback DELETE FROM tenant.protocol WHERE id = '74';
--rollback DELETE FROM tenant.protocol WHERE id = '75';
--rollback DELETE FROM tenant.protocol WHERE id = '76';
--rollback DELETE FROM tenant.protocol WHERE id = '77';
--rollback DELETE FROM tenant.protocol WHERE id = '78';
--rollback DELETE FROM tenant.protocol WHERE id = '79';
--rollback DELETE FROM tenant.protocol WHERE id = '80';
--rollback DELETE FROM tenant.protocol WHERE id = '81';
--rollback DELETE FROM tenant.protocol WHERE id = '82';
--rollback DELETE FROM tenant.protocol WHERE id = '83';
--rollback DELETE FROM tenant.protocol WHERE id = '84';
--rollback DELETE FROM tenant.protocol WHERE id = '85';
--rollback DELETE FROM tenant.protocol WHERE id = '86';
--rollback DELETE FROM tenant.protocol WHERE id = '87';
--rollback DELETE FROM tenant.protocol WHERE id = '88';
--rollback DELETE FROM tenant.protocol WHERE id = '89';
--rollback DELETE FROM tenant.protocol WHERE id = '90';
--rollback DELETE FROM tenant.protocol WHERE id = '91';
--rollback DELETE FROM tenant.protocol WHERE id = '92';
--rollback DELETE FROM tenant.protocol WHERE id = '93';
--rollback DELETE FROM tenant.protocol WHERE id = '94';
--rollback DELETE FROM tenant.protocol WHERE id = '95';
--rollback DELETE FROM tenant.protocol WHERE id = '96';
--rollback DELETE FROM tenant.protocol WHERE id = '97';
--rollback DELETE FROM tenant.protocol WHERE id = '98';
--rollback DELETE FROM tenant.protocol WHERE id = '99';
--rollback DELETE FROM tenant.protocol WHERE id = '100';
--rollback DELETE FROM tenant.protocol WHERE id = '101';
--rollback DELETE FROM tenant.protocol WHERE id = '102';
--rollback DELETE FROM tenant.protocol WHERE id = '103';
--rollback DELETE FROM tenant.protocol WHERE id = '104';
--rollback DELETE FROM tenant.protocol WHERE id = '105';
--rollback DELETE FROM tenant.protocol WHERE id = '106';
--rollback DELETE FROM tenant.protocol WHERE id = '107';
--rollback DELETE FROM tenant.protocol WHERE id = '108';
--rollback DELETE FROM tenant.protocol WHERE id = '109';
--rollback DELETE FROM tenant.protocol WHERE id = '110';
--rollback DELETE FROM tenant.protocol WHERE id = '111';
--rollback DELETE FROM tenant.protocol WHERE id = '112';
--rollback DELETE FROM tenant.protocol WHERE id = '113';
--rollback DELETE FROM tenant.protocol WHERE id = '114';
--rollback DELETE FROM tenant.protocol WHERE id = '115';
--rollback DELETE FROM tenant.protocol WHERE id = '116';
--rollback DELETE FROM tenant.protocol WHERE id = '117';
--rollback DELETE FROM tenant.protocol WHERE id = '118';
--rollback DELETE FROM tenant.protocol WHERE id = '119';
--rollback DELETE FROM tenant.protocol WHERE id = '120';
--rollback DELETE FROM tenant.protocol WHERE id = '121';
--rollback DELETE FROM tenant.protocol WHERE id = '122';
--rollback DELETE FROM tenant.protocol WHERE id = '123';
--rollback DELETE FROM tenant.protocol WHERE id = '124';
--rollback DELETE FROM tenant.protocol WHERE id = '125';
--rollback DELETE FROM tenant.protocol WHERE id = '126';
--rollback DELETE FROM tenant.protocol WHERE id = '127';
--rollback DELETE FROM tenant.protocol WHERE id = '128';
--rollback DELETE FROM tenant.protocol WHERE id = '129';
--rollback DELETE FROM tenant.protocol WHERE id = '130';
--rollback DELETE FROM tenant.protocol WHERE id = '131';
--rollback DELETE FROM tenant.protocol WHERE id = '132';
--rollback DELETE FROM tenant.protocol WHERE id = '133';
--rollback DELETE FROM tenant.protocol WHERE id = '134';
--rollback DELETE FROM tenant.protocol WHERE id = '135';
--rollback DELETE FROM tenant.protocol WHERE id = '136';
--rollback DELETE FROM tenant.protocol WHERE id = '137';
--rollback DELETE FROM tenant.protocol WHERE id = '138';
--rollback DELETE FROM tenant.protocol WHERE id = '139';
--rollback DELETE FROM tenant.protocol WHERE id = '140';
--rollback DELETE FROM tenant.protocol WHERE id = '141';
--rollback DELETE FROM tenant.protocol WHERE id = '142';
--rollback DELETE FROM tenant.protocol WHERE id = '143';
--rollback DELETE FROM tenant.protocol WHERE id = '144';
--rollback DELETE FROM tenant.protocol WHERE id = '145';
--rollback DELETE FROM tenant.protocol WHERE id = '146';
--rollback DELETE FROM tenant.protocol WHERE id = '147';
--rollback DELETE FROM tenant.protocol WHERE id = '148';
--rollback DELETE FROM tenant.protocol WHERE id = '149';
--rollback DELETE FROM tenant.protocol WHERE id = '150';
--rollback DELETE FROM tenant.protocol WHERE id = '151';
--rollback DELETE FROM tenant.protocol WHERE id = '152';
--rollback DELETE FROM tenant.protocol WHERE id = '153';
--rollback DELETE FROM tenant.protocol WHERE id = '154';
--rollback DELETE FROM tenant.protocol WHERE id = '155';
--rollback DELETE FROM tenant.protocol WHERE id = '156';
--rollback DELETE FROM tenant.protocol WHERE id = '157';
--rollback DELETE FROM tenant.protocol WHERE id = '158';
--rollback DELETE FROM tenant.protocol WHERE id = '159';
--rollback DELETE FROM tenant.protocol WHERE id = '160';
--rollback DELETE FROM tenant.protocol WHERE id = '161';
--rollback DELETE FROM tenant.protocol WHERE id = '162';
--rollback DELETE FROM tenant.protocol WHERE id = '163';
--rollback DELETE FROM tenant.protocol WHERE id = '164';
--rollback DELETE FROM tenant.protocol WHERE id = '165';
--rollback DELETE FROM tenant.protocol WHERE id = '166';
--rollback DELETE FROM tenant.protocol WHERE id = '167';
--rollback DELETE FROM tenant.protocol WHERE id = '168';
--rollback DELETE FROM tenant.protocol WHERE id = '169';
--rollback DELETE FROM tenant.protocol WHERE id = '170';
--rollback DELETE FROM tenant.protocol WHERE id = '171';
--rollback DELETE FROM tenant.protocol WHERE id = '172';
--rollback DELETE FROM tenant.protocol WHERE id = '173';
--rollback DELETE FROM tenant.protocol WHERE id = '174';
--rollback DELETE FROM tenant.protocol WHERE id = '175';
--rollback DELETE FROM tenant.protocol WHERE id = '176';
--rollback DELETE FROM tenant.protocol WHERE id = '177';
--rollback DELETE FROM tenant.protocol WHERE id = '178';
--rollback DELETE FROM tenant.protocol WHERE id = '179';
--rollback DELETE FROM tenant.protocol WHERE id = '180';
--rollback DELETE FROM tenant.protocol WHERE id = '181';
--rollback DELETE FROM tenant.protocol WHERE id = '182';
--rollback DELETE FROM tenant.protocol WHERE id = '183';
--rollback DELETE FROM tenant.protocol WHERE id = '184';
--rollback DELETE FROM tenant.protocol WHERE id = '185';
--rollback DELETE FROM tenant.protocol WHERE id = '186';
--rollback DELETE FROM tenant.protocol WHERE id = '187';
--rollback DELETE FROM tenant.protocol WHERE id = '188';
--rollback DELETE FROM tenant.protocol WHERE id = '189';
--rollback DELETE FROM tenant.protocol WHERE id = '190';
--rollback DELETE FROM tenant.protocol WHERE id = '191';

--rollback DELETE FROM platform.list_member WHERE id = '193893';
--rollback DELETE FROM platform.list_member WHERE id = '193894';
--rollback DELETE FROM platform.list_member WHERE id = '193895';
--rollback DELETE FROM platform.list_member WHERE id = '193896';
--rollback DELETE FROM platform.list_member WHERE id = '193897';
--rollback DELETE FROM platform.list_member WHERE id = '193898';
--rollback DELETE FROM platform.list_member WHERE id = '193899';
--rollback DELETE FROM platform.list_member WHERE id = '193900';
--rollback DELETE FROM platform.list_member WHERE id = '193901';
--rollback DELETE FROM platform.list_member WHERE id = '193902';
--rollback DELETE FROM platform.list_member WHERE id = '193903';
--rollback DELETE FROM platform.list_member WHERE id = '193904';
--rollback DELETE FROM platform.list_member WHERE id = '193905';
--rollback DELETE FROM platform.list_member WHERE id = '193906';
--rollback DELETE FROM platform.list_member WHERE id = '193907';
--rollback DELETE FROM platform.list_member WHERE id = '193908';
--rollback DELETE FROM platform.list_member WHERE id = '193909';
--rollback DELETE FROM platform.list_member WHERE id = '193910';
--rollback DELETE FROM platform.list_member WHERE id = '193911';
--rollback DELETE FROM platform.list_member WHERE id = '193912';
--rollback DELETE FROM platform.list_member WHERE id = '193913';
--rollback DELETE FROM platform.list_member WHERE id = '193914';
--rollback DELETE FROM platform.list_member WHERE id = '193915';
--rollback DELETE FROM platform.list_member WHERE id = '193916';
--rollback DELETE FROM platform.list_member WHERE id = '193917';
--rollback DELETE FROM platform.list_member WHERE id = '193918';
--rollback DELETE FROM platform.list_member WHERE id = '193919';
--rollback DELETE FROM platform.list_member WHERE id = '193920';
--rollback DELETE FROM platform.list_member WHERE id = '193921';
--rollback DELETE FROM platform.list_member WHERE id = '193922';
--rollback DELETE FROM platform.list_member WHERE id = '193923';
--rollback DELETE FROM platform.list_member WHERE id = '193924';
--rollback DELETE FROM platform.list_member WHERE id = '193925';
--rollback DELETE FROM platform.list_member WHERE id = '193926';
--rollback DELETE FROM platform.list_member WHERE id = '193927';
--rollback DELETE FROM platform.list_member WHERE id = '193928';
--rollback DELETE FROM platform.list_member WHERE id = '193929';
--rollback DELETE FROM platform.list_member WHERE id = '193930';
--rollback DELETE FROM platform.list_member WHERE id = '193931';
--rollback DELETE FROM platform.list_member WHERE id = '193932';
--rollback DELETE FROM platform.list_member WHERE id = '193933';
--rollback DELETE FROM platform.list_member WHERE id = '193934';
--rollback DELETE FROM platform.list_member WHERE id = '193935';
--rollback DELETE FROM platform.list_member WHERE id = '193936';
--rollback DELETE FROM platform.list_member WHERE id = '193937';
--rollback DELETE FROM platform.list_member WHERE id = '193938';
--rollback DELETE FROM platform.list_member WHERE id = '193939';
--rollback DELETE FROM platform.list_member WHERE id = '193940';
--rollback DELETE FROM platform.list_member WHERE id = '193941';
--rollback DELETE FROM platform.list_member WHERE id = '193942';
--rollback DELETE FROM platform.list_member WHERE id = '193943';
--rollback DELETE FROM platform.list_member WHERE id = '193944';
--rollback DELETE FROM platform.list_member WHERE id = '193945';
--rollback DELETE FROM platform.list_member WHERE id = '193946';
--rollback DELETE FROM platform.list_member WHERE id = '193947';
--rollback DELETE FROM platform.list_member WHERE id = '193948';
--rollback DELETE FROM platform.list_member WHERE id = '193949';
--rollback DELETE FROM platform.list_member WHERE id = '193950';
--rollback DELETE FROM platform.list_member WHERE id = '193951';
--rollback DELETE FROM platform.list_member WHERE id = '193952';
--rollback DELETE FROM platform.list_member WHERE id = '193953';
--rollback DELETE FROM platform.list_member WHERE id = '193954';
--rollback DELETE FROM platform.list_member WHERE id = '193955';
--rollback DELETE FROM platform.list_member WHERE id = '193956';
--rollback DELETE FROM platform.list_member WHERE id = '193957';
--rollback DELETE FROM platform.list_member WHERE id = '193958';
--rollback DELETE FROM platform.list_member WHERE id = '193959';
--rollback DELETE FROM platform.list_member WHERE id = '193960';
--rollback DELETE FROM platform.list_member WHERE id = '193961';
--rollback DELETE FROM platform.list_member WHERE id = '193962';
--rollback DELETE FROM platform.list_member WHERE id = '193963';
--rollback DELETE FROM platform.list_member WHERE id = '193964';
--rollback DELETE FROM platform.list_member WHERE id = '193965';
--rollback DELETE FROM platform.list_member WHERE id = '193966';
--rollback DELETE FROM platform.list_member WHERE id = '193967';
--rollback DELETE FROM platform.list_member WHERE id = '193968';
--rollback DELETE FROM platform.list_member WHERE id = '193969';
--rollback DELETE FROM platform.list_member WHERE id = '193970';
--rollback DELETE FROM platform.list_member WHERE id = '193971';
--rollback DELETE FROM platform.list_member WHERE id = '193972';
--rollback DELETE FROM platform.list_member WHERE id = '193973';
--rollback DELETE FROM platform.list_member WHERE id = '193974';
--rollback DELETE FROM platform.list_member WHERE id = '193975';
--rollback DELETE FROM platform.list_member WHERE id = '193976';
--rollback DELETE FROM platform.list_member WHERE id = '193977';
--rollback DELETE FROM platform.list_member WHERE id = '193978';
--rollback DELETE FROM platform.list_member WHERE id = '193979';
--rollback DELETE FROM platform.list_member WHERE id = '193980';
--rollback DELETE FROM platform.list_member WHERE id = '193981';
--rollback DELETE FROM platform.list_member WHERE id = '193982';
--rollback DELETE FROM platform.list_member WHERE id = '193983';
--rollback DELETE FROM platform.list_member WHERE id = '193984';
--rollback DELETE FROM platform.list_member WHERE id = '193985';
--rollback DELETE FROM platform.list_member WHERE id = '193986';
--rollback DELETE FROM platform.list_member WHERE id = '193987';
--rollback DELETE FROM platform.list_member WHERE id = '193988';
--rollback DELETE FROM platform.list_member WHERE id = '193989';
--rollback DELETE FROM platform.list_member WHERE id = '193990';
--rollback DELETE FROM platform.list_member WHERE id = '193991';
--rollback DELETE FROM platform.list_member WHERE id = '193992';
--rollback DELETE FROM platform.list_member WHERE id = '193993';
--rollback DELETE FROM platform.list_member WHERE id = '193994';
--rollback DELETE FROM platform.list_member WHERE id = '193995';
--rollback DELETE FROM platform.list_member WHERE id = '193996';
--rollback DELETE FROM platform.list_member WHERE id = '193997';
--rollback DELETE FROM platform.list_member WHERE id = '193998';
--rollback DELETE FROM platform.list_member WHERE id = '193999';
--rollback DELETE FROM platform.list_member WHERE id = '194000';
--rollback DELETE FROM platform.list_member WHERE id = '194001';
--rollback DELETE FROM platform.list_member WHERE id = '194002';
--rollback DELETE FROM platform.list_member WHERE id = '194003';
--rollback DELETE FROM platform.list_member WHERE id = '194004';
--rollback DELETE FROM platform.list_member WHERE id = '194005';
--rollback DELETE FROM platform.list_member WHERE id = '194006';
--rollback DELETE FROM platform.list_member WHERE id = '194007';
--rollback DELETE FROM platform.list_member WHERE id = '194008';
--rollback DELETE FROM platform.list_member WHERE id = '194009';
--rollback DELETE FROM platform.list_member WHERE id = '194010';
--rollback DELETE FROM platform.list_member WHERE id = '194011';
--rollback DELETE FROM platform.list_member WHERE id = '194012';
--rollback DELETE FROM platform.list_member WHERE id = '194013';
--rollback DELETE FROM platform.list_member WHERE id = '194014';
--rollback DELETE FROM platform.list_member WHERE id = '194015';
--rollback DELETE FROM platform.list_member WHERE id = '194016';
--rollback DELETE FROM platform.list_member WHERE id = '194017';
--rollback DELETE FROM platform.list_member WHERE id = '194018';
--rollback DELETE FROM platform.list_member WHERE id = '194019';
--rollback DELETE FROM platform.list_member WHERE id = '194020';
--rollback DELETE FROM platform.list_member WHERE id = '194021';
--rollback DELETE FROM platform.list_member WHERE id = '194022';
--rollback DELETE FROM platform.list_member WHERE id = '194023';
--rollback DELETE FROM platform.list_member WHERE id = '194024';
--rollback DELETE FROM platform.list_member WHERE id = '194025';
--rollback DELETE FROM platform.list_member WHERE id = '194026';
--rollback DELETE FROM platform.list_member WHERE id = '194027';
--rollback DELETE FROM platform.list_member WHERE id = '194028';
--rollback DELETE FROM platform.list_member WHERE id = '194029';
--rollback DELETE FROM platform.list_member WHERE id = '194030';
--rollback DELETE FROM platform.list_member WHERE id = '194031';
--rollback DELETE FROM platform.list_member WHERE id = '194032';
--rollback DELETE FROM platform.list_member WHERE id = '194033';
--rollback DELETE FROM platform.list_member WHERE id = '194034';
--rollback DELETE FROM platform.list_member WHERE id = '194035';
--rollback DELETE FROM platform.list_member WHERE id = '194036';
--rollback DELETE FROM platform.list_member WHERE id = '194037';
--rollback DELETE FROM platform.list_member WHERE id = '194038';
--rollback DELETE FROM platform.list_member WHERE id = '194039';
--rollback DELETE FROM platform.list_member WHERE id = '194040';
--rollback DELETE FROM platform.list_member WHERE id = '194041';
--rollback DELETE FROM platform.list_member WHERE id = '194042';
--rollback DELETE FROM platform.list_member WHERE id = '194043';
--rollback DELETE FROM platform.list_member WHERE id = '194044';
--rollback DELETE FROM platform.list_member WHERE id = '194045';
--rollback DELETE FROM platform.list_member WHERE id = '194046';
--rollback DELETE FROM platform.list_member WHERE id = '194047';
--rollback DELETE FROM platform.list_member WHERE id = '194048';
--rollback DELETE FROM platform.list_member WHERE id = '194049';
--rollback DELETE FROM platform.list_member WHERE id = '194050';
--rollback DELETE FROM platform.list_member WHERE id = '194051';
--rollback DELETE FROM platform.list_member WHERE id = '194052';
--rollback DELETE FROM platform.list_member WHERE id = '194053';
--rollback DELETE FROM platform.list_member WHERE id = '194054';
--rollback DELETE FROM platform.list_member WHERE id = '194055';
--rollback DELETE FROM platform.list_member WHERE id = '194056';
--rollback DELETE FROM platform.list_member WHERE id = '194057';
--rollback DELETE FROM platform.list_member WHERE id = '194058';
--rollback DELETE FROM platform.list_member WHERE id = '194059';
--rollback DELETE FROM platform.list_member WHERE id = '194060';
--rollback DELETE FROM platform.list_member WHERE id = '194061';
--rollback DELETE FROM platform.list_member WHERE id = '194062';
--rollback DELETE FROM platform.list_member WHERE id = '194063';
--rollback DELETE FROM platform.list_member WHERE id = '194064';
--rollback DELETE FROM platform.list_member WHERE id = '194065';
--rollback DELETE FROM platform.list_member WHERE id = '194066';
--rollback DELETE FROM platform.list_member WHERE id = '194067';
--rollback DELETE FROM platform.list_member WHERE id = '194068';
--rollback DELETE FROM platform.list_member WHERE id = '194069';
--rollback DELETE FROM platform.list_member WHERE id = '194070';
--rollback DELETE FROM platform.list_member WHERE id = '194071';
--rollback DELETE FROM platform.list_member WHERE id = '194072';
--rollback DELETE FROM platform.list_member WHERE id = '194073';
--rollback DELETE FROM platform.list_member WHERE id = '194074';
--rollback DELETE FROM platform.list_member WHERE id = '194075';
--rollback DELETE FROM platform.list_member WHERE id = '194076';
--rollback DELETE FROM platform.list_member WHERE id = '194077';
--rollback DELETE FROM platform.list_member WHERE id = '194078';
--rollback DELETE FROM platform.list_member WHERE id = '194079';
--rollback DELETE FROM platform.list_member WHERE id = '194080';
--rollback DELETE FROM platform.list_member WHERE id = '194081';
--rollback DELETE FROM platform.list_member WHERE id = '194082';
--rollback DELETE FROM platform.list_member WHERE id = '194083';
--rollback DELETE FROM platform.list_member WHERE id = '194084';
--rollback DELETE FROM platform.list_member WHERE id = '194085';
--rollback DELETE FROM platform.list_member WHERE id = '194086';
--rollback DELETE FROM platform.list_member WHERE id = '194087';
--rollback DELETE FROM platform.list_member WHERE id = '194088';
--rollback DELETE FROM platform.list_member WHERE id = '194089';
--rollback DELETE FROM platform.list_member WHERE id = '194090';
--rollback DELETE FROM platform.list_member WHERE id = '194091';
--rollback DELETE FROM platform.list_member WHERE id = '194092';
--rollback DELETE FROM platform.list_member WHERE id = '194093';
--rollback DELETE FROM platform.list_member WHERE id = '194094';
--rollback DELETE FROM platform.list_member WHERE id = '194095';
--rollback DELETE FROM platform.list_member WHERE id = '194096';
--rollback DELETE FROM platform.list_member WHERE id = '194097';
--rollback DELETE FROM platform.list_member WHERE id = '194098';
--rollback DELETE FROM platform.list_member WHERE id = '194099';
--rollback DELETE FROM platform.list_member WHERE id = '194100';
--rollback DELETE FROM platform.list_member WHERE id = '194101';
--rollback DELETE FROM platform.list_member WHERE id = '194102';
--rollback DELETE FROM platform.list_member WHERE id = '194103';
--rollback DELETE FROM platform.list_member WHERE id = '194104';
--rollback DELETE FROM platform.list_member WHERE id = '194105';
--rollback DELETE FROM platform.list_member WHERE id = '194106';
--rollback DELETE FROM platform.list_member WHERE id = '194107';
--rollback DELETE FROM platform.list_member WHERE id = '194108';
--rollback DELETE FROM platform.list_member WHERE id = '194109';
--rollback DELETE FROM platform.list_member WHERE id = '194110';
--rollback DELETE FROM platform.list_member WHERE id = '194111';
--rollback DELETE FROM platform.list_member WHERE id = '194112';
--rollback DELETE FROM platform.list_member WHERE id = '194113';
--rollback DELETE FROM platform.list_member WHERE id = '194114';
--rollback DELETE FROM platform.list_member WHERE id = '194115';
--rollback DELETE FROM platform.list_member WHERE id = '194116';
--rollback DELETE FROM platform.list_member WHERE id = '194117';
--rollback DELETE FROM platform.list_member WHERE id = '194118';
--rollback DELETE FROM platform.list_member WHERE id = '194119';
--rollback DELETE FROM platform.list_member WHERE id = '194120';
--rollback DELETE FROM platform.list_member WHERE id = '194121';
--rollback DELETE FROM platform.list_member WHERE id = '194122';
--rollback DELETE FROM platform.list_member WHERE id = '194123';
--rollback DELETE FROM platform.list_member WHERE id = '194124';
--rollback DELETE FROM platform.list_member WHERE id = '194125';
--rollback DELETE FROM platform.list_member WHERE id = '194126';
--rollback DELETE FROM platform.list_member WHERE id = '194127';
--rollback DELETE FROM platform.list_member WHERE id = '194128';
--rollback DELETE FROM platform.list_member WHERE id = '194129';
--rollback DELETE FROM platform.list_member WHERE id = '194130';
--rollback DELETE FROM platform.list_member WHERE id = '194131';
--rollback DELETE FROM platform.list_member WHERE id = '194132';
--rollback DELETE FROM platform.list_member WHERE id = '194133';
--rollback DELETE FROM platform.list_member WHERE id = '194134';
--rollback DELETE FROM platform.list_member WHERE id = '194135';
--rollback DELETE FROM platform.list_member WHERE id = '194136';
--rollback DELETE FROM platform.list_member WHERE id = '194137';
--rollback DELETE FROM platform.list_member WHERE id = '194138';
--rollback DELETE FROM platform.list_member WHERE id = '194139';
--rollback DELETE FROM platform.list_member WHERE id = '194140';
--rollback DELETE FROM platform.list_member WHERE id = '194141';
--rollback DELETE FROM platform.list_member WHERE id = '194142';
--rollback DELETE FROM platform.list_member WHERE id = '194143';
--rollback DELETE FROM platform.list_member WHERE id = '194144';
--rollback DELETE FROM platform.list_member WHERE id = '194145';
--rollback DELETE FROM platform.list_member WHERE id = '194146';
--rollback DELETE FROM platform.list_member WHERE id = '194147';
--rollback DELETE FROM platform.list_member WHERE id = '194148';
--rollback DELETE FROM platform.list_member WHERE id = '194149';
--rollback DELETE FROM platform.list_member WHERE id = '194150';
--rollback DELETE FROM platform.list_member WHERE id = '194151';
--rollback DELETE FROM platform.list_member WHERE id = '194152';
--rollback DELETE FROM platform.list_member WHERE id = '194153';
--rollback DELETE FROM platform.list_member WHERE id = '194154';
--rollback DELETE FROM platform.list_member WHERE id = '194155';
--rollback DELETE FROM platform.list_member WHERE id = '194156';
--rollback DELETE FROM platform.list_member WHERE id = '194157';
--rollback DELETE FROM platform.list_member WHERE id = '194158';
--rollback DELETE FROM platform.list_member WHERE id = '194159';
--rollback DELETE FROM platform.list_member WHERE id = '194160';
--rollback DELETE FROM platform.list_member WHERE id = '194161';
--rollback DELETE FROM platform.list_member WHERE id = '194162';
--rollback DELETE FROM platform.list_member WHERE id = '194163';
--rollback DELETE FROM platform.list_member WHERE id = '194164';
--rollback DELETE FROM platform.list_member WHERE id = '194165';
--rollback DELETE FROM platform.list_member WHERE id = '194166';
--rollback DELETE FROM platform.list_member WHERE id = '194167';
--rollback DELETE FROM platform.list_member WHERE id = '194168';
--rollback DELETE FROM platform.list_member WHERE id = '194169';
--rollback DELETE FROM platform.list_member WHERE id = '194170';
--rollback DELETE FROM platform.list_member WHERE id = '194171';
--rollback DELETE FROM platform.list_member WHERE id = '194172';
--rollback DELETE FROM platform.list_member WHERE id = '194173';
--rollback DELETE FROM platform.list_member WHERE id = '194174';
--rollback DELETE FROM platform.list_member WHERE id = '194175';
--rollback DELETE FROM platform.list_member WHERE id = '194176';
--rollback DELETE FROM platform.list_member WHERE id = '194177';
--rollback DELETE FROM platform.list_member WHERE id = '194178';
--rollback DELETE FROM platform.list_member WHERE id = '194179';
--rollback DELETE FROM platform.list_member WHERE id = '194180';
--rollback DELETE FROM platform.list_member WHERE id = '194181';
--rollback DELETE FROM platform.list_member WHERE id = '194182';
--rollback DELETE FROM platform.list_member WHERE id = '194183';
--rollback DELETE FROM platform.list_member WHERE id = '194184';
--rollback DELETE FROM platform.list_member WHERE id = '194185';
--rollback DELETE FROM platform.list_member WHERE id = '194186';
--rollback DELETE FROM platform.list_member WHERE id = '194187';
--rollback DELETE FROM platform.list_member WHERE id = '194188';
--rollback DELETE FROM platform.list_member WHERE id = '194189';
--rollback DELETE FROM platform.list_member WHERE id = '194190';
--rollback DELETE FROM platform.list_member WHERE id = '194191';
--rollback DELETE FROM platform.list_member WHERE id = '194192';
--rollback DELETE FROM platform.list_member WHERE id = '194193';
--rollback DELETE FROM platform.list_member WHERE id = '194194';
--rollback DELETE FROM platform.list_member WHERE id = '194195';
--rollback DELETE FROM platform.list_member WHERE id = '194196';
--rollback DELETE FROM platform.list_member WHERE id = '194197';
--rollback DELETE FROM platform.list_member WHERE id = '194198';
--rollback DELETE FROM platform.list_member WHERE id = '194199';
--rollback DELETE FROM platform.list_member WHERE id = '194200';
--rollback DELETE FROM platform.list_member WHERE id = '194201';
--rollback DELETE FROM platform.list_member WHERE id = '194202';
--rollback DELETE FROM platform.list_member WHERE id = '194203';
--rollback DELETE FROM platform.list_member WHERE id = '194204';
--rollback DELETE FROM platform.list_member WHERE id = '194205';
--rollback DELETE FROM platform.list_member WHERE id = '194206';
--rollback DELETE FROM platform.list_member WHERE id = '194207';
--rollback DELETE FROM platform.list_member WHERE id = '194208';
--rollback DELETE FROM platform.list_member WHERE id = '194209';
--rollback DELETE FROM platform.list_member WHERE id = '194210';
--rollback DELETE FROM platform.list_member WHERE id = '194211';
--rollback DELETE FROM platform.list_member WHERE id = '194212';
--rollback DELETE FROM platform.list_member WHERE id = '194213';
--rollback DELETE FROM platform.list_member WHERE id = '194214';
--rollback DELETE FROM platform.list_member WHERE id = '194215';
--rollback DELETE FROM platform.list_member WHERE id = '194216';
--rollback DELETE FROM platform.list_member WHERE id = '194217';
--rollback DELETE FROM platform.list_member WHERE id = '194218';
--rollback DELETE FROM platform.list_member WHERE id = '194219';
--rollback DELETE FROM platform.list_member WHERE id = '194220';
--rollback DELETE FROM platform.list_member WHERE id = '194221';
--rollback DELETE FROM platform.list_member WHERE id = '194222';
--rollback DELETE FROM platform.list_member WHERE id = '194223';
--rollback DELETE FROM platform.list_member WHERE id = '194224';
--rollback DELETE FROM platform.list_member WHERE id = '194225';
--rollback DELETE FROM platform.list_member WHERE id = '194226';
--rollback DELETE FROM platform.list_member WHERE id = '194227';
--rollback DELETE FROM platform.list_member WHERE id = '194228';
--rollback DELETE FROM platform.list_member WHERE id = '194229';
--rollback DELETE FROM platform.list_member WHERE id = '194230';
--rollback DELETE FROM platform.list_member WHERE id = '194231';
--rollback DELETE FROM platform.list_member WHERE id = '194232';
--rollback DELETE FROM platform.list_member WHERE id = '194233';
--rollback DELETE FROM platform.list_member WHERE id = '194234';
--rollback DELETE FROM platform.list_member WHERE id = '194235';
--rollback DELETE FROM platform.list_member WHERE id = '194236';
--rollback DELETE FROM platform.list_member WHERE id = '194237';
--rollback DELETE FROM platform.list_member WHERE id = '194238';
--rollback DELETE FROM platform.list_member WHERE id = '194239';
--rollback DELETE FROM platform.list_member WHERE id = '194240';
--rollback DELETE FROM platform.list_member WHERE id = '194241';
--rollback DELETE FROM platform.list_member WHERE id = '194242';
--rollback DELETE FROM platform.list_member WHERE id = '194243';
--rollback DELETE FROM platform.list_member WHERE id = '194244';
--rollback DELETE FROM platform.list_member WHERE id = '194245';
--rollback DELETE FROM platform.list_member WHERE id = '194246';
--rollback DELETE FROM platform.list_member WHERE id = '194247';
--rollback DELETE FROM platform.list_member WHERE id = '194248';
--rollback DELETE FROM platform.list_member WHERE id = '194249';
--rollback DELETE FROM platform.list_member WHERE id = '194250';
--rollback DELETE FROM platform.list_member WHERE id = '194251';
--rollback DELETE FROM platform.list_member WHERE id = '194252';
--rollback DELETE FROM platform.list_member WHERE id = '194253';
--rollback DELETE FROM platform.list_member WHERE id = '194254';
--rollback DELETE FROM platform.list_member WHERE id = '194255';
--rollback DELETE FROM platform.list_member WHERE id = '194256';
--rollback DELETE FROM platform.list_member WHERE id = '194257';
--rollback DELETE FROM platform.list_member WHERE id = '194258';
--rollback DELETE FROM platform.list_member WHERE id = '194259';
--rollback DELETE FROM platform.list_member WHERE id = '194260';
--rollback DELETE FROM platform.list_member WHERE id = '194261';
--rollback DELETE FROM platform.list_member WHERE id = '194262';
--rollback DELETE FROM platform.list_member WHERE id = '194263';
--rollback DELETE FROM platform.list_member WHERE id = '194264';
--rollback DELETE FROM platform.list_member WHERE id = '194265';
--rollback DELETE FROM platform.list_member WHERE id = '194266';
--rollback DELETE FROM platform.list_member WHERE id = '194267';
--rollback DELETE FROM platform.list_member WHERE id = '194268';
--rollback DELETE FROM platform.list_member WHERE id = '194269';
--rollback DELETE FROM platform.list_member WHERE id = '194270';
--rollback DELETE FROM platform.list_member WHERE id = '194271';
--rollback DELETE FROM platform.list_member WHERE id = '194272';
--rollback DELETE FROM platform.list_member WHERE id = '194273';
--rollback DELETE FROM platform.list_member WHERE id = '194274';
--rollback DELETE FROM platform.list_member WHERE id = '194275';
--rollback DELETE FROM platform.list_member WHERE id = '194276';
--rollback DELETE FROM platform.list_member WHERE id = '194277';
--rollback DELETE FROM platform.list_member WHERE id = '194278';
--rollback DELETE FROM platform.list_member WHERE id = '194279';
--rollback DELETE FROM platform.list_member WHERE id = '194280';
--rollback DELETE FROM platform.list_member WHERE id = '194281';
--rollback DELETE FROM platform.list_member WHERE id = '194282';
--rollback DELETE FROM platform.list_member WHERE id = '194283';
--rollback DELETE FROM platform.list_member WHERE id = '194284';
--rollback DELETE FROM platform.list_member WHERE id = '194285';
--rollback DELETE FROM platform.list_member WHERE id = '194286';
--rollback DELETE FROM platform.list_member WHERE id = '194287';
--rollback DELETE FROM platform.list_member WHERE id = '194288';
--rollback DELETE FROM platform.list_member WHERE id = '194289';
--rollback DELETE FROM platform.list_member WHERE id = '194290';
--rollback DELETE FROM platform.list_member WHERE id = '194291';
--rollback DELETE FROM platform.list_member WHERE id = '194292';
--rollback DELETE FROM platform.list_member WHERE id = '194293';
--rollback DELETE FROM platform.list_member WHERE id = '194294';
--rollback DELETE FROM platform.list_member WHERE id = '194295';
--rollback DELETE FROM platform.list_member WHERE id = '194296';
--rollback DELETE FROM platform.list_member WHERE id = '194297';
--rollback DELETE FROM platform.list_member WHERE id = '194298';
--rollback DELETE FROM platform.list_member WHERE id = '194299';
--rollback DELETE FROM platform.list_member WHERE id = '194300';
--rollback DELETE FROM platform.list_member WHERE id = '194301';
--rollback DELETE FROM platform.list_member WHERE id = '194302';
--rollback DELETE FROM platform.list_member WHERE id = '194303';
--rollback DELETE FROM platform.list_member WHERE id = '194304';
--rollback DELETE FROM platform.list_member WHERE id = '194305';
--rollback DELETE FROM platform.list_member WHERE id = '194306';
--rollback DELETE FROM platform.list_member WHERE id = '194307';
--rollback DELETE FROM platform.list_member WHERE id = '194308';
--rollback DELETE FROM platform.list_member WHERE id = '194309';
--rollback DELETE FROM platform.list_member WHERE id = '194310';
--rollback DELETE FROM platform.list_member WHERE id = '194311';
--rollback DELETE FROM platform.list_member WHERE id = '194312';
--rollback DELETE FROM platform.list_member WHERE id = '194313';
--rollback DELETE FROM platform.list_member WHERE id = '194314';
--rollback DELETE FROM platform.list_member WHERE id = '194315';
--rollback DELETE FROM platform.list_member WHERE id = '194316';
--rollback DELETE FROM platform.list_member WHERE id = '194317';
--rollback DELETE FROM platform.list_member WHERE id = '194318';
--rollback DELETE FROM platform.list_member WHERE id = '194319';
--rollback DELETE FROM platform.list_member WHERE id = '194320';
--rollback DELETE FROM platform.list_member WHERE id = '194321';
--rollback DELETE FROM platform.list_member WHERE id = '194322';
--rollback DELETE FROM platform.list_member WHERE id = '194323';
--rollback DELETE FROM platform.list_member WHERE id = '194324';
--rollback DELETE FROM platform.list_member WHERE id = '194325';
--rollback DELETE FROM platform.list_member WHERE id = '194326';
--rollback DELETE FROM platform.list_member WHERE id = '194327';
--rollback DELETE FROM platform.list_member WHERE id = '194328';
--rollback DELETE FROM platform.list_member WHERE id = '194329';
--rollback DELETE FROM platform.list_member WHERE id = '194330';
--rollback DELETE FROM platform.list_member WHERE id = '194331';
--rollback DELETE FROM platform.list_member WHERE id = '194332';
--rollback DELETE FROM platform.list_member WHERE id = '194333';
--rollback DELETE FROM platform.list_member WHERE id = '194334';
--rollback DELETE FROM platform.list_member WHERE id = '194335';
--rollback DELETE FROM platform.list_member WHERE id = '194336';
--rollback DELETE FROM platform.list_member WHERE id = '194337';
--rollback DELETE FROM platform.list_member WHERE id = '194338';
--rollback DELETE FROM platform.list_member WHERE id = '194339';
--rollback DELETE FROM platform.list_member WHERE id = '194340';
--rollback DELETE FROM platform.list_member WHERE id = '194341';
--rollback DELETE FROM platform.list_member WHERE id = '194342';
--rollback DELETE FROM platform.list_member WHERE id = '194343';
--rollback DELETE FROM platform.list_member WHERE id = '194344';
--rollback DELETE FROM platform.list_member WHERE id = '194345';
--rollback DELETE FROM platform.list_member WHERE id = '194346';
--rollback DELETE FROM platform.list_member WHERE id = '194347';
--rollback DELETE FROM platform.list_member WHERE id = '194348';
--rollback DELETE FROM platform.list_member WHERE id = '194349';
--rollback DELETE FROM platform.list_member WHERE id = '194350';
--rollback DELETE FROM platform.list_member WHERE id = '194351';
--rollback DELETE FROM platform.list_member WHERE id = '194352';
--rollback DELETE FROM platform.list_member WHERE id = '194353';
--rollback DELETE FROM platform.list_member WHERE id = '194354';
--rollback DELETE FROM platform.list_member WHERE id = '194355';
--rollback DELETE FROM platform.list_member WHERE id = '194356';
--rollback DELETE FROM platform.list_member WHERE id = '194357';
--rollback DELETE FROM platform.list_member WHERE id = '194358';
--rollback DELETE FROM platform.list_member WHERE id = '194359';
--rollback DELETE FROM platform.list_member WHERE id = '194360';
--rollback DELETE FROM platform.list_member WHERE id = '194361';
--rollback DELETE FROM platform.list_member WHERE id = '194362';
--rollback DELETE FROM platform.list_member WHERE id = '194363';
--rollback DELETE FROM platform.list_member WHERE id = '194364';
--rollback DELETE FROM platform.list_member WHERE id = '194365';
--rollback DELETE FROM platform.list_member WHERE id = '194366';
--rollback DELETE FROM platform.list_member WHERE id = '194367';
--rollback DELETE FROM platform.list_member WHERE id = '194368';
--rollback DELETE FROM platform.list_member WHERE id = '194369';
--rollback DELETE FROM platform.list_member WHERE id = '194370';
--rollback DELETE FROM platform.list_member WHERE id = '194371';
--rollback DELETE FROM platform.list_member WHERE id = '194372';
--rollback DELETE FROM platform.list_member WHERE id = '194373';
--rollback DELETE FROM platform.list_member WHERE id = '194374';
--rollback DELETE FROM platform.list_member WHERE id = '194375';
--rollback DELETE FROM platform.list_member WHERE id = '194376';
--rollback DELETE FROM platform.list_member WHERE id = '194377';
--rollback DELETE FROM platform.list_member WHERE id = '194378';
--rollback DELETE FROM platform.list_member WHERE id = '194379';
--rollback DELETE FROM platform.list_member WHERE id = '194380';
--rollback DELETE FROM platform.list_member WHERE id = '194381';
--rollback DELETE FROM platform.list_member WHERE id = '194382';
--rollback DELETE FROM platform.list_member WHERE id = '194383';
--rollback DELETE FROM platform.list_member WHERE id = '194384';
--rollback DELETE FROM platform.list_member WHERE id = '194385';
--rollback DELETE FROM platform.list_member WHERE id = '194386';
--rollback DELETE FROM platform.list_member WHERE id = '194387';
--rollback DELETE FROM platform.list_member WHERE id = '194388';
--rollback DELETE FROM platform.list_member WHERE id = '194389';
--rollback DELETE FROM platform.list_member WHERE id = '194390';
--rollback DELETE FROM platform.list_member WHERE id = '194391';
--rollback DELETE FROM platform.list_member WHERE id = '194392';
--rollback DELETE FROM platform.list_member WHERE id = '194393';
--rollback DELETE FROM platform.list_member WHERE id = '194394';
--rollback DELETE FROM platform.list_member WHERE id = '194395';
--rollback DELETE FROM platform.list_member WHERE id = '194396';
--rollback DELETE FROM platform.list_member WHERE id = '194397';
--rollback DELETE FROM platform.list_member WHERE id = '194398';
--rollback DELETE FROM platform.list_member WHERE id = '194399';
--rollback DELETE FROM platform.list_member WHERE id = '194400';
--rollback DELETE FROM platform.list_member WHERE id = '194401';
--rollback DELETE FROM platform.list_member WHERE id = '194402';
--rollback DELETE FROM platform.list_member WHERE id = '194403';
--rollback DELETE FROM platform.list_member WHERE id = '194404';
--rollback DELETE FROM platform.list_member WHERE id = '194405';
--rollback DELETE FROM platform.list_member WHERE id = '194406';
--rollback DELETE FROM platform.list_member WHERE id = '194407';
--rollback DELETE FROM platform.list_member WHERE id = '194408';
--rollback DELETE FROM platform.list_member WHERE id = '194409';
--rollback DELETE FROM platform.list_member WHERE id = '194410';
--rollback DELETE FROM platform.list_member WHERE id = '194411';
--rollback DELETE FROM platform.list_member WHERE id = '194412';
--rollback DELETE FROM platform.list_member WHERE id = '194413';
--rollback DELETE FROM platform.list_member WHERE id = '194414';
--rollback DELETE FROM platform.list_member WHERE id = '194415';
--rollback DELETE FROM platform.list_member WHERE id = '194416';
--rollback DELETE FROM platform.list_member WHERE id = '194417';
--rollback DELETE FROM platform.list_member WHERE id = '194418';
--rollback DELETE FROM platform.list_member WHERE id = '194419';
--rollback DELETE FROM platform.list_member WHERE id = '194420';
--rollback DELETE FROM platform.list_member WHERE id = '194421';
--rollback DELETE FROM platform.list_member WHERE id = '194422';
--rollback DELETE FROM platform.list_member WHERE id = '194423';
--rollback DELETE FROM platform.list_member WHERE id = '194424';
--rollback DELETE FROM platform.list_member WHERE id = '194425';
--rollback DELETE FROM platform.list_member WHERE id = '194426';
--rollback DELETE FROM platform.list_member WHERE id = '194427';
--rollback DELETE FROM platform.list_member WHERE id = '194428';
--rollback DELETE FROM platform.list_member WHERE id = '194429';
--rollback DELETE FROM platform.list_member WHERE id = '194430';
--rollback DELETE FROM platform.list_member WHERE id = '194431';
--rollback DELETE FROM platform.list_member WHERE id = '194432';
--rollback DELETE FROM platform.list_member WHERE id = '194433';
--rollback DELETE FROM platform.list_member WHERE id = '194434';
--rollback DELETE FROM platform.list_member WHERE id = '194435';
--rollback DELETE FROM platform.list_member WHERE id = '194436';
--rollback DELETE FROM platform.list_member WHERE id = '194437';
--rollback DELETE FROM platform.list_member WHERE id = '194438';
--rollback DELETE FROM platform.list_member WHERE id = '194439';
--rollback DELETE FROM platform.list_member WHERE id = '194440';
--rollback DELETE FROM platform.list_member WHERE id = '194441';
--rollback DELETE FROM platform.list_member WHERE id = '194442';
--rollback DELETE FROM platform.list_member WHERE id = '194443';
--rollback DELETE FROM platform.list_member WHERE id = '194444';
--rollback DELETE FROM platform.list_member WHERE id = '194445';
--rollback DELETE FROM platform.list_member WHERE id = '194446';
--rollback DELETE FROM platform.list_member WHERE id = '194447';
--rollback DELETE FROM platform.list_member WHERE id = '194448';
--rollback DELETE FROM platform.list_member WHERE id = '194449';
--rollback DELETE FROM platform.list_member WHERE id = '194450';
--rollback DELETE FROM platform.list_member WHERE id = '194451';
--rollback DELETE FROM platform.list_member WHERE id = '194452';
--rollback DELETE FROM platform.list_member WHERE id = '194453';
--rollback DELETE FROM platform.list_member WHERE id = '194454';
--rollback DELETE FROM platform.list_member WHERE id = '194455';
--rollback DELETE FROM platform.list_member WHERE id = '194456';
--rollback DELETE FROM platform.list_member WHERE id = '194457';
--rollback DELETE FROM platform.list_member WHERE id = '194458';
--rollback DELETE FROM platform.list_member WHERE id = '194459';
--rollback DELETE FROM platform.list_member WHERE id = '194460';
--rollback DELETE FROM platform.list_member WHERE id = '194461';
--rollback DELETE FROM platform.list_member WHERE id = '194462';
--rollback DELETE FROM platform.list_member WHERE id = '194463';
--rollback DELETE FROM platform.list_member WHERE id = '194464';
--rollback DELETE FROM platform.list_member WHERE id = '194465';
--rollback DELETE FROM platform.list_member WHERE id = '194466';
--rollback DELETE FROM platform.list_member WHERE id = '194467';
--rollback DELETE FROM platform.list_member WHERE id = '194468';
--rollback DELETE FROM platform.list_member WHERE id = '194469';
--rollback DELETE FROM platform.list_member WHERE id = '194470';
--rollback DELETE FROM platform.list_member WHERE id = '194471';
--rollback DELETE FROM platform.list_member WHERE id = '194472';
--rollback DELETE FROM platform.list_member WHERE id = '194473';
--rollback DELETE FROM platform.list_member WHERE id = '194474';
--rollback DELETE FROM platform.list_member WHERE id = '194475';
--rollback DELETE FROM platform.list_member WHERE id = '194476';
--rollback DELETE FROM platform.list_member WHERE id = '194477';
--rollback DELETE FROM platform.list_member WHERE id = '194478';
--rollback DELETE FROM platform.list_member WHERE id = '194479';
--rollback DELETE FROM platform.list_member WHERE id = '194480';
--rollback DELETE FROM platform.list_member WHERE id = '194481';
--rollback DELETE FROM platform.list_member WHERE id = '194482';
--rollback DELETE FROM platform.list_member WHERE id = '194483';
--rollback DELETE FROM platform.list_member WHERE id = '194484';
--rollback DELETE FROM platform.list_member WHERE id = '194485';
--rollback DELETE FROM platform.list_member WHERE id = '194486';
--rollback DELETE FROM platform.list_member WHERE id = '194487';
--rollback DELETE FROM platform.list_member WHERE id = '194488';
--rollback DELETE FROM platform.list_member WHERE id = '194489';
--rollback DELETE FROM platform.list_member WHERE id = '194490';
--rollback DELETE FROM platform.list_member WHERE id = '194491';
--rollback DELETE FROM platform.list_member WHERE id = '194492';
--rollback DELETE FROM platform.list_member WHERE id = '194493';
--rollback DELETE FROM platform.list_member WHERE id = '194494';
--rollback DELETE FROM platform.list_member WHERE id = '194495';
--rollback DELETE FROM platform.list_member WHERE id = '194496';
--rollback DELETE FROM platform.list_member WHERE id = '194497';
--rollback DELETE FROM platform.list_member WHERE id = '194498';
--rollback DELETE FROM platform.list_member WHERE id = '194499';
--rollback DELETE FROM platform.list_member WHERE id = '194500';
--rollback DELETE FROM platform.list_member WHERE id = '194501';
--rollback DELETE FROM platform.list_member WHERE id = '194502';
--rollback DELETE FROM platform.list_member WHERE id = '194503';
--rollback DELETE FROM platform.list_member WHERE id = '194504';
--rollback DELETE FROM platform.list_member WHERE id = '194505';
--rollback DELETE FROM platform.list_member WHERE id = '194506';
--rollback DELETE FROM platform.list_member WHERE id = '194507';
--rollback DELETE FROM platform.list_member WHERE id = '194508';
--rollback DELETE FROM platform.list_member WHERE id = '194509';
--rollback DELETE FROM platform.list_member WHERE id = '194510';
--rollback DELETE FROM platform.list_member WHERE id = '194511';
--rollback DELETE FROM platform.list_member WHERE id = '194512';
--rollback DELETE FROM platform.list_member WHERE id = '194513';
--rollback DELETE FROM platform.list_member WHERE id = '194514';
--rollback DELETE FROM platform.list_member WHERE id = '194515';
--rollback DELETE FROM platform.list_member WHERE id = '194516';
--rollback DELETE FROM platform.list_member WHERE id = '194517';
--rollback DELETE FROM platform.list_member WHERE id = '194518';
--rollback DELETE FROM platform.list_member WHERE id = '194519';
--rollback DELETE FROM platform.list_member WHERE id = '194520';
--rollback DELETE FROM platform.list_member WHERE id = '194521';
--rollback DELETE FROM platform.list_member WHERE id = '194522';
--rollback DELETE FROM platform.list_member WHERE id = '194523';
--rollback DELETE FROM platform.list_member WHERE id = '194524';
--rollback DELETE FROM platform.list_member WHERE id = '194525';
--rollback DELETE FROM platform.list_member WHERE id = '194526';
--rollback DELETE FROM platform.list_member WHERE id = '194527';
--rollback DELETE FROM platform.list_member WHERE id = '194528';
--rollback DELETE FROM platform.list_member WHERE id = '194529';
--rollback DELETE FROM platform.list_member WHERE id = '194530';
--rollback DELETE FROM platform.list_member WHERE id = '194531';
--rollback DELETE FROM platform.list_member WHERE id = '194532';
--rollback DELETE FROM platform.list_member WHERE id = '194533';
--rollback DELETE FROM platform.list_member WHERE id = '194534';
--rollback DELETE FROM platform.list_member WHERE id = '194535';
--rollback DELETE FROM platform.list_member WHERE id = '194536';
--rollback DELETE FROM platform.list_member WHERE id = '194537';
--rollback DELETE FROM platform.list_member WHERE id = '194538';
--rollback DELETE FROM platform.list_member WHERE id = '194539';
--rollback DELETE FROM platform.list_member WHERE id = '194540';
--rollback DELETE FROM platform.list_member WHERE id = '194541';
--rollback DELETE FROM platform.list_member WHERE id = '194542';
--rollback DELETE FROM platform.list_member WHERE id = '194543';
--rollback DELETE FROM platform.list_member WHERE id = '194544';
--rollback DELETE FROM platform.list_member WHERE id = '194545';
--rollback DELETE FROM platform.list_member WHERE id = '194546';
--rollback DELETE FROM platform.list_member WHERE id = '194547';
--rollback DELETE FROM platform.list_member WHERE id = '194548';
--rollback DELETE FROM platform.list_member WHERE id = '194549';
--rollback DELETE FROM platform.list_member WHERE id = '194550';
--rollback DELETE FROM platform.list_member WHERE id = '194551';
--rollback DELETE FROM platform.list_member WHERE id = '194552';
--rollback DELETE FROM platform.list_member WHERE id = '194553';
--rollback DELETE FROM platform.list_member WHERE id = '194554';
--rollback DELETE FROM platform.list_member WHERE id = '194555';
--rollback DELETE FROM platform.list_member WHERE id = '194556';
--rollback DELETE FROM platform.list_member WHERE id = '194557';
--rollback DELETE FROM platform.list_member WHERE id = '194558';
--rollback DELETE FROM platform.list_member WHERE id = '194559';
--rollback DELETE FROM platform.list_member WHERE id = '194560';
--rollback DELETE FROM platform.list_member WHERE id = '194561';
--rollback DELETE FROM platform.list_member WHERE id = '194562';
--rollback DELETE FROM platform.list_member WHERE id = '194563';
--rollback DELETE FROM platform.list_member WHERE id = '194564';
--rollback DELETE FROM platform.list_member WHERE id = '194565';
--rollback DELETE FROM platform.list_member WHERE id = '194566';
--rollback DELETE FROM platform.list_member WHERE id = '194567';
--rollback DELETE FROM platform.list_member WHERE id = '194568';
--rollback DELETE FROM platform.list_member WHERE id = '194569';
--rollback DELETE FROM platform.list_member WHERE id = '194570';
--rollback DELETE FROM platform.list_member WHERE id = '194571';
--rollback DELETE FROM platform.list_member WHERE id = '194572';
--rollback DELETE FROM platform.list_member WHERE id = '194573';
--rollback DELETE FROM platform.list_member WHERE id = '194574';
--rollback DELETE FROM platform.list_member WHERE id = '194575';
--rollback DELETE FROM platform.list_member WHERE id = '194576';
--rollback DELETE FROM platform.list_member WHERE id = '194577';
--rollback DELETE FROM platform.list_member WHERE id = '194578';
--rollback DELETE FROM platform.list_member WHERE id = '194579';
--rollback DELETE FROM platform.list_member WHERE id = '194580';
--rollback DELETE FROM platform.list_member WHERE id = '194581';
--rollback DELETE FROM platform.list_member WHERE id = '194582';
--rollback DELETE FROM platform.list_member WHERE id = '194583';
--rollback DELETE FROM platform.list_member WHERE id = '194584';
--rollback DELETE FROM platform.list_member WHERE id = '194585';
--rollback DELETE FROM platform.list_member WHERE id = '194586';
--rollback DELETE FROM platform.list_member WHERE id = '194587';
--rollback DELETE FROM platform.list_member WHERE id = '194588';
--rollback DELETE FROM platform.list_member WHERE id = '194589';
--rollback DELETE FROM platform.list_member WHERE id = '194590';
--rollback DELETE FROM platform.list_member WHERE id = '194591';
--rollback DELETE FROM platform.list_member WHERE id = '194592';
--rollback DELETE FROM platform.list_member WHERE id = '194593';
--rollback DELETE FROM platform.list_member WHERE id = '194594';
--rollback DELETE FROM platform.list_member WHERE id = '194595';
--rollback DELETE FROM platform.list_member WHERE id = '194596';
--rollback DELETE FROM platform.list_member WHERE id = '194597';
--rollback DELETE FROM platform.list_member WHERE id = '194598';
--rollback DELETE FROM platform.list_member WHERE id = '194599';
--rollback DELETE FROM platform.list_member WHERE id = '194600';
--rollback DELETE FROM platform.list_member WHERE id = '194601';
--rollback DELETE FROM platform.list_member WHERE id = '194602';
--rollback DELETE FROM platform.list_member WHERE id = '194603';
--rollback DELETE FROM platform.list_member WHERE id = '194604';
--rollback DELETE FROM platform.list_member WHERE id = '194605';
--rollback DELETE FROM platform.list_member WHERE id = '194606';
--rollback DELETE FROM platform.list_member WHERE id = '194607';
--rollback DELETE FROM platform.list_member WHERE id = '194608';
--rollback DELETE FROM platform.list_member WHERE id = '194609';
--rollback DELETE FROM platform.list_member WHERE id = '194610';
--rollback DELETE FROM platform.list_member WHERE id = '194611';
--rollback DELETE FROM platform.list_member WHERE id = '194612';
--rollback DELETE FROM platform.list_member WHERE id = '194613';
--rollback DELETE FROM platform.list_member WHERE id = '194614';
--rollback DELETE FROM platform.list_member WHERE id = '194615';
--rollback DELETE FROM platform.list_member WHERE id = '194616';
--rollback DELETE FROM platform.list_member WHERE id = '194617';
--rollback DELETE FROM platform.list_member WHERE id = '194618';
--rollback DELETE FROM platform.list_member WHERE id = '194619';
--rollback DELETE FROM platform.list_member WHERE id = '194620';
--rollback DELETE FROM platform.list_member WHERE id = '194621';
--rollback DELETE FROM platform.list_member WHERE id = '194622';
--rollback DELETE FROM platform.list_member WHERE id = '194623';
--rollback DELETE FROM platform.list_member WHERE id = '194624';
--rollback DELETE FROM platform.list_member WHERE id = '194625';
--rollback DELETE FROM platform.list_member WHERE id = '194626';
--rollback DELETE FROM platform.list_member WHERE id = '194627';
--rollback DELETE FROM platform.list_member WHERE id = '194628';
--rollback DELETE FROM platform.list_member WHERE id = '194629';
--rollback DELETE FROM platform.list_member WHERE id = '194630';
--rollback DELETE FROM platform.list_member WHERE id = '194631';
--rollback DELETE FROM platform.list_member WHERE id = '194632';
--rollback DELETE FROM platform.list_member WHERE id = '194633';
--rollback DELETE FROM platform.list_member WHERE id = '194634';
--rollback DELETE FROM platform.list_member WHERE id = '194635';
--rollback DELETE FROM platform.list_member WHERE id = '194636';
--rollback DELETE FROM platform.list_member WHERE id = '194637';
--rollback DELETE FROM platform.list_member WHERE id = '194638';
--rollback DELETE FROM platform.list_member WHERE id = '194639';
--rollback DELETE FROM platform.list_member WHERE id = '194640';
--rollback DELETE FROM platform.list_member WHERE id = '194641';
--rollback DELETE FROM platform.list_member WHERE id = '194642';
--rollback DELETE FROM platform.list_member WHERE id = '194643';
--rollback DELETE FROM platform.list_member WHERE id = '194644';
--rollback DELETE FROM platform.list_member WHERE id = '194645';
--rollback DELETE FROM platform.list_member WHERE id = '194646';
--rollback DELETE FROM platform.list_member WHERE id = '194647';
--rollback DELETE FROM platform.list_member WHERE id = '194648';
--rollback DELETE FROM platform.list_member WHERE id = '194649';
--rollback DELETE FROM platform.list_member WHERE id = '194650';
--rollback DELETE FROM platform.list_member WHERE id = '194651';
--rollback DELETE FROM platform.list_member WHERE id = '194652';
--rollback DELETE FROM platform.list_member WHERE id = '194653';
--rollback DELETE FROM platform.list_member WHERE id = '194654';
--rollback DELETE FROM platform.list_member WHERE id = '194655';
--rollback DELETE FROM platform.list_member WHERE id = '194656';
--rollback DELETE FROM platform.list_member WHERE id = '194657';
--rollback DELETE FROM platform.list_member WHERE id = '194658';
--rollback DELETE FROM platform.list_member WHERE id = '194659';
--rollback DELETE FROM platform.list_member WHERE id = '194660';
--rollback DELETE FROM platform.list_member WHERE id = '194661';
--rollback DELETE FROM platform.list_member WHERE id = '194662';
--rollback DELETE FROM platform.list_member WHERE id = '194663';
--rollback DELETE FROM platform.list_member WHERE id = '194664';
--rollback DELETE FROM platform.list_member WHERE id = '194665';
--rollback DELETE FROM platform.list_member WHERE id = '194666';
--rollback DELETE FROM platform.list_member WHERE id = '194667';
--rollback DELETE FROM platform.list_member WHERE id = '194668';
--rollback DELETE FROM platform.list_member WHERE id = '194669';
--rollback DELETE FROM platform.list_member WHERE id = '194670';
--rollback DELETE FROM platform.list_member WHERE id = '194671';
--rollback DELETE FROM platform.list_member WHERE id = '194672';
--rollback DELETE FROM platform.list_member WHERE id = '194673';
--rollback DELETE FROM platform.list_member WHERE id = '194674';
--rollback DELETE FROM platform.list_member WHERE id = '194675';
--rollback DELETE FROM platform.list_member WHERE id = '194676';
--rollback DELETE FROM platform.list_member WHERE id = '194677';
--rollback DELETE FROM platform.list_member WHERE id = '194678';
--rollback DELETE FROM platform.list_member WHERE id = '194679';
--rollback DELETE FROM platform.list_member WHERE id = '194680';
--rollback DELETE FROM platform.list_member WHERE id = '194681';
--rollback DELETE FROM platform.list_member WHERE id = '194682';
--rollback DELETE FROM platform.list_member WHERE id = '194683';
--rollback DELETE FROM platform.list_member WHERE id = '194684';
--rollback DELETE FROM platform.list_member WHERE id = '194685';
--rollback DELETE FROM platform.list_member WHERE id = '194686';
--rollback DELETE FROM platform.list_member WHERE id = '194687';
--rollback DELETE FROM platform.list_member WHERE id = '194688';
--rollback DELETE FROM platform.list_member WHERE id = '194689';
--rollback DELETE FROM platform.list_member WHERE id = '194690';
--rollback DELETE FROM platform.list_member WHERE id = '194691';
--rollback DELETE FROM platform.list_member WHERE id = '194692';
--rollback DELETE FROM platform.list_member WHERE id = '194693';
--rollback DELETE FROM platform.list_member WHERE id = '194694';
--rollback DELETE FROM platform.list_member WHERE id = '194695';
--rollback DELETE FROM platform.list_member WHERE id = '194696';
--rollback DELETE FROM platform.list_member WHERE id = '194697';
--rollback DELETE FROM platform.list_member WHERE id = '194698';
--rollback DELETE FROM platform.list_member WHERE id = '194699';
--rollback DELETE FROM platform.list_member WHERE id = '194700';
--rollback DELETE FROM platform.list_member WHERE id = '194701';
--rollback DELETE FROM platform.list_member WHERE id = '194702';
--rollback DELETE FROM platform.list_member WHERE id = '194703';
--rollback DELETE FROM platform.list_member WHERE id = '194704';
--rollback DELETE FROM platform.list_member WHERE id = '194705';
--rollback DELETE FROM platform.list_member WHERE id = '194706';
--rollback DELETE FROM platform.list_member WHERE id = '194707';
--rollback DELETE FROM platform.list_member WHERE id = '194708';
--rollback DELETE FROM platform.list_member WHERE id = '194709';
--rollback DELETE FROM platform.list_member WHERE id = '194710';
--rollback DELETE FROM platform.list_member WHERE id = '194711';
--rollback DELETE FROM platform.list_member WHERE id = '194712';
--rollback DELETE FROM platform.list_member WHERE id = '194713';
--rollback DELETE FROM platform.list_member WHERE id = '194714';
--rollback DELETE FROM platform.list_member WHERE id = '194715';
--rollback DELETE FROM platform.list_member WHERE id = '194716';
--rollback DELETE FROM platform.list_member WHERE id = '194717';
--rollback DELETE FROM platform.list_member WHERE id = '194718';
--rollback DELETE FROM platform.list_member WHERE id = '194719';
--rollback DELETE FROM platform.list_member WHERE id = '194720';
--rollback DELETE FROM platform.list_member WHERE id = '194721';
--rollback DELETE FROM platform.list_member WHERE id = '194722';
--rollback DELETE FROM platform.list_member WHERE id = '194723';
--rollback DELETE FROM platform.list_member WHERE id = '194724';
--rollback DELETE FROM platform.list_member WHERE id = '194725';
--rollback DELETE FROM platform.list_member WHERE id = '194726';
--rollback DELETE FROM platform.list_member WHERE id = '194727';
--rollback DELETE FROM platform.list_member WHERE id = '194728';
--rollback DELETE FROM platform.list_member WHERE id = '194729';
--rollback DELETE FROM platform.list_member WHERE id = '194730';
--rollback DELETE FROM platform.list_member WHERE id = '194731';
--rollback DELETE FROM platform.list_member WHERE id = '194732';
--rollback DELETE FROM platform.list_member WHERE id = '194733';
--rollback DELETE FROM platform.list_member WHERE id = '194734';
--rollback DELETE FROM platform.list_member WHERE id = '194735';
--rollback DELETE FROM platform.list_member WHERE id = '194736';
--rollback DELETE FROM platform.list_member WHERE id = '194737';
--rollback DELETE FROM platform.list_member WHERE id = '194738';
--rollback DELETE FROM platform.list_member WHERE id = '194739';
--rollback DELETE FROM platform.list_member WHERE id = '194740';
--rollback DELETE FROM platform.list_member WHERE id = '194741';
--rollback DELETE FROM platform.list_member WHERE id = '194742';
--rollback DELETE FROM platform.list_member WHERE id = '194743';
--rollback DELETE FROM platform.list_member WHERE id = '194744';
--rollback DELETE FROM platform.list_member WHERE id = '194745';
--rollback DELETE FROM platform.list_member WHERE id = '194746';
--rollback DELETE FROM platform.list_member WHERE id = '194747';
--rollback DELETE FROM platform.list_member WHERE id = '194748';
--rollback DELETE FROM platform.list_member WHERE id = '194749';
--rollback DELETE FROM platform.list_member WHERE id = '194750';
--rollback DELETE FROM platform.list_member WHERE id = '194751';
--rollback DELETE FROM platform.list_member WHERE id = '194752';
--rollback DELETE FROM platform.list_member WHERE id = '194753';
--rollback DELETE FROM platform.list_member WHERE id = '194754';
--rollback DELETE FROM platform.list_member WHERE id = '194755';
--rollback DELETE FROM platform.list_member WHERE id = '194756';
--rollback DELETE FROM platform.list_member WHERE id = '194757';
--rollback DELETE FROM platform.list_member WHERE id = '194758';
--rollback DELETE FROM platform.list_member WHERE id = '194759';
--rollback DELETE FROM platform.list_member WHERE id = '194760';
--rollback DELETE FROM platform.list_member WHERE id = '194761';
--rollback DELETE FROM platform.list_member WHERE id = '194762';
--rollback DELETE FROM platform.list_member WHERE id = '194763';
--rollback DELETE FROM platform.list_member WHERE id = '194764';
--rollback DELETE FROM platform.list_member WHERE id = '194765';
--rollback DELETE FROM platform.list_member WHERE id = '194766';
--rollback DELETE FROM platform.list_member WHERE id = '194767';
--rollback DELETE FROM platform.list_member WHERE id = '194768';
--rollback DELETE FROM platform.list_member WHERE id = '194769';
--rollback DELETE FROM platform.list_member WHERE id = '194770';
--rollback DELETE FROM platform.list_member WHERE id = '194771';
--rollback DELETE FROM platform.list_member WHERE id = '194772';
--rollback DELETE FROM platform.list_member WHERE id = '194773';
--rollback DELETE FROM platform.list_member WHERE id = '194774';
--rollback DELETE FROM platform.list_member WHERE id = '194775';
--rollback DELETE FROM platform.list_member WHERE id = '194776';
--rollback DELETE FROM platform.list_member WHERE id = '194777';
--rollback DELETE FROM platform.list_member WHERE id = '194778';
--rollback DELETE FROM platform.list_member WHERE id = '194779';
--rollback DELETE FROM platform.list_member WHERE id = '194780';
--rollback DELETE FROM platform.list_member WHERE id = '194781';
--rollback DELETE FROM platform.list_member WHERE id = '194782';
--rollback DELETE FROM platform.list_member WHERE id = '194783';
--rollback DELETE FROM platform.list_member WHERE id = '194784';
--rollback DELETE FROM platform.list_member WHERE id = '194785';
--rollback DELETE FROM platform.list_member WHERE id = '194786';
--rollback DELETE FROM platform.list_member WHERE id = '194787';
--rollback DELETE FROM platform.list_member WHERE id = '194788';
--rollback DELETE FROM platform.list_member WHERE id = '194789';
--rollback DELETE FROM platform.list_member WHERE id = '194790';
--rollback DELETE FROM platform.list_member WHERE id = '194791';
--rollback DELETE FROM platform.list_member WHERE id = '194792';
--rollback DELETE FROM platform.list_member WHERE id = '194793';
--rollback DELETE FROM platform.list_member WHERE id = '194794';
--rollback DELETE FROM platform.list_member WHERE id = '194795';
--rollback DELETE FROM platform.list_member WHERE id = '194796';
--rollback DELETE FROM platform.list_member WHERE id = '194797';
--rollback DELETE FROM platform.list_member WHERE id = '194798';
--rollback DELETE FROM platform.list_member WHERE id = '194799';
--rollback DELETE FROM platform.list_member WHERE id = '194800';
--rollback DELETE FROM platform.list_member WHERE id = '194801';
--rollback DELETE FROM platform.list_member WHERE id = '194802';
--rollback DELETE FROM platform.list_member WHERE id = '194803';
--rollback DELETE FROM platform.list_member WHERE id = '194804';
--rollback DELETE FROM platform.list_member WHERE id = '194805';
--rollback DELETE FROM platform.list_member WHERE id = '194806';
--rollback DELETE FROM platform.list_member WHERE id = '194807';
--rollback DELETE FROM platform.list_member WHERE id = '194808';
--rollback DELETE FROM platform.list_member WHERE id = '194809';
--rollback DELETE FROM platform.list_member WHERE id = '194810';
--rollback DELETE FROM platform.list_member WHERE id = '194811';
--rollback DELETE FROM platform.list_member WHERE id = '194812';
--rollback DELETE FROM platform.list_member WHERE id = '194813';
--rollback DELETE FROM platform.list_member WHERE id = '194814';
--rollback DELETE FROM platform.list_member WHERE id = '194815';
--rollback DELETE FROM platform.list_member WHERE id = '194816';
--rollback DELETE FROM platform.list_member WHERE id = '194817';
--rollback DELETE FROM platform.list_member WHERE id = '194818';
--rollback DELETE FROM platform.list_member WHERE id = '194819';
--rollback DELETE FROM platform.list_member WHERE id = '194820';
--rollback DELETE FROM platform.list_member WHERE id = '194821';
--rollback DELETE FROM platform.list_member WHERE id = '194822';
--rollback DELETE FROM platform.list_member WHERE id = '194823';
--rollback DELETE FROM platform.list_member WHERE id = '194824';
--rollback DELETE FROM platform.list_member WHERE id = '194825';
--rollback DELETE FROM platform.list_member WHERE id = '194826';
--rollback DELETE FROM platform.list_member WHERE id = '194827';
--rollback DELETE FROM platform.list_member WHERE id = '194828';
--rollback DELETE FROM platform.list_member WHERE id = '194829';
--rollback DELETE FROM platform.list_member WHERE id = '194830';
--rollback DELETE FROM platform.list_member WHERE id = '194831';
--rollback DELETE FROM platform.list_member WHERE id = '194832';
--rollback DELETE FROM platform.list_member WHERE id = '194833';
--rollback DELETE FROM platform.list_member WHERE id = '194834';
--rollback DELETE FROM platform.list_member WHERE id = '194835';
--rollback DELETE FROM platform.list_member WHERE id = '194836';
--rollback DELETE FROM platform.list_member WHERE id = '194837';
--rollback DELETE FROM platform.list_member WHERE id = '194838';
--rollback DELETE FROM platform.list_member WHERE id = '194839';
--rollback DELETE FROM platform.list_member WHERE id = '194840';
--rollback DELETE FROM platform.list_member WHERE id = '194841';
--rollback DELETE FROM platform.list_member WHERE id = '194842';
--rollback DELETE FROM platform.list_member WHERE id = '194843';
--rollback DELETE FROM platform.list_member WHERE id = '194844';
--rollback DELETE FROM platform.list_member WHERE id = '194845';
--rollback DELETE FROM platform.list_member WHERE id = '194846';
--rollback DELETE FROM platform.list_member WHERE id = '194847';
--rollback DELETE FROM platform.list_member WHERE id = '194848';
--rollback DELETE FROM platform.list_member WHERE id = '194849';
--rollback DELETE FROM platform.list_member WHERE id = '194850';
--rollback DELETE FROM platform.list_member WHERE id = '194851';
--rollback DELETE FROM platform.list_member WHERE id = '194852';
--rollback DELETE FROM platform.list_member WHERE id = '194853';
--rollback DELETE FROM platform.list_member WHERE id = '194854';
--rollback DELETE FROM platform.list_member WHERE id = '194855';
--rollback DELETE FROM platform.list_member WHERE id = '194856';
--rollback DELETE FROM platform.list_member WHERE id = '194857';
--rollback DELETE FROM platform.list_member WHERE id = '194858';
--rollback DELETE FROM platform.list_member WHERE id = '194859';
--rollback DELETE FROM platform.list_member WHERE id = '194860';
--rollback DELETE FROM platform.list_member WHERE id = '194861';
--rollback DELETE FROM platform.list_member WHERE id = '194862';
--rollback DELETE FROM platform.list_member WHERE id = '194863';
--rollback DELETE FROM platform.list_member WHERE id = '194864';
--rollback DELETE FROM platform.list_member WHERE id = '194865';
--rollback DELETE FROM platform.list_member WHERE id = '194866';
--rollback DELETE FROM platform.list_member WHERE id = '194867';
--rollback DELETE FROM platform.list_member WHERE id = '194868';
--rollback DELETE FROM platform.list_member WHERE id = '194869';
--rollback DELETE FROM platform.list_member WHERE id = '194870';
--rollback DELETE FROM platform.list_member WHERE id = '194871';
--rollback DELETE FROM platform.list_member WHERE id = '194872';
--rollback DELETE FROM platform.list_member WHERE id = '194873';
--rollback DELETE FROM platform.list_member WHERE id = '194874';
--rollback DELETE FROM platform.list_member WHERE id = '194875';
--rollback DELETE FROM platform.list_member WHERE id = '194876';
--rollback DELETE FROM platform.list_member WHERE id = '194877';
--rollback DELETE FROM platform.list_member WHERE id = '194878';
--rollback DELETE FROM platform.list_member WHERE id = '194879';
--rollback DELETE FROM platform.list_member WHERE id = '194880';
--rollback DELETE FROM platform.list_member WHERE id = '194881';
--rollback DELETE FROM platform.list_member WHERE id = '194882';
--rollback DELETE FROM platform.list_member WHERE id = '194883';
--rollback DELETE FROM platform.list_member WHERE id = '194884';
--rollback DELETE FROM platform.list_member WHERE id = '194885';
--rollback DELETE FROM platform.list_member WHERE id = '194886';
--rollback DELETE FROM platform.list_member WHERE id = '194887';
--rollback DELETE FROM platform.list_member WHERE id = '194888';
--rollback DELETE FROM platform.list_member WHERE id = '194889';
--rollback DELETE FROM platform.list_member WHERE id = '194890';
--rollback DELETE FROM platform.list_member WHERE id = '194891';
--rollback DELETE FROM platform.list_member WHERE id = '194892';
--rollback DELETE FROM platform.list_member WHERE id = '194893';
--rollback DELETE FROM platform.list_member WHERE id = '194894';
--rollback DELETE FROM platform.list_member WHERE id = '194895';
--rollback DELETE FROM platform.list_member WHERE id = '194896';
--rollback DELETE FROM platform.list_member WHERE id = '194897';
--rollback DELETE FROM platform.list_member WHERE id = '194898';
--rollback DELETE FROM platform.list_member WHERE id = '194899';
--rollback DELETE FROM platform.list_member WHERE id = '194900';
--rollback DELETE FROM platform.list_member WHERE id = '194901';
--rollback DELETE FROM platform.list_member WHERE id = '194902';
--rollback DELETE FROM platform.list_member WHERE id = '194903';
--rollback DELETE FROM platform.list_member WHERE id = '194904';
--rollback DELETE FROM platform.list_member WHERE id = '194905';
--rollback DELETE FROM platform.list_member WHERE id = '194906';
--rollback DELETE FROM platform.list_member WHERE id = '194907';
--rollback DELETE FROM platform.list_member WHERE id = '194908';
--rollback DELETE FROM platform.list_member WHERE id = '194909';
--rollback DELETE FROM platform.list_member WHERE id = '194910';
--rollback DELETE FROM platform.list_member WHERE id = '194911';
--rollback DELETE FROM platform.list_member WHERE id = '194912';
--rollback DELETE FROM platform.list_member WHERE id = '194913';
--rollback DELETE FROM platform.list_member WHERE id = '194914';
--rollback DELETE FROM platform.list_member WHERE id = '194915';
--rollback DELETE FROM platform.list_member WHERE id = '194916';
--rollback DELETE FROM platform.list_member WHERE id = '194917';
--rollback DELETE FROM platform.list_member WHERE id = '194918';
--rollback DELETE FROM platform.list_member WHERE id = '194919';
--rollback DELETE FROM platform.list_member WHERE id = '194920';
--rollback DELETE FROM platform.list_member WHERE id = '194921';
--rollback DELETE FROM platform.list_member WHERE id = '194922';
--rollback DELETE FROM platform.list_member WHERE id = '194923';
--rollback DELETE FROM platform.list_member WHERE id = '194924';
--rollback DELETE FROM platform.list_member WHERE id = '194925';
--rollback DELETE FROM platform.list_member WHERE id = '194926';
--rollback DELETE FROM platform.list_member WHERE id = '194927';
--rollback DELETE FROM platform.list_member WHERE id = '194928';
--rollback DELETE FROM platform.list_member WHERE id = '194929';
--rollback DELETE FROM platform.list_member WHERE id = '194930';
--rollback DELETE FROM platform.list_member WHERE id = '194931';
--rollback DELETE FROM platform.list_member WHERE id = '194932';
--rollback DELETE FROM platform.list_member WHERE id = '194933';
--rollback DELETE FROM platform.list_member WHERE id = '194934';
--rollback DELETE FROM platform.list_member WHERE id = '194935';
--rollback DELETE FROM platform.list_member WHERE id = '194936';
--rollback DELETE FROM platform.list_member WHERE id = '194937';
--rollback DELETE FROM platform.list_member WHERE id = '194938';
--rollback DELETE FROM platform.list_member WHERE id = '194939';
--rollback DELETE FROM platform.list_member WHERE id = '194940';
--rollback DELETE FROM platform.list_member WHERE id = '194941';
--rollback DELETE FROM platform.list_member WHERE id = '194942';
--rollback DELETE FROM platform.list_member WHERE id = '194943';
--rollback DELETE FROM platform.list_member WHERE id = '194944';
--rollback DELETE FROM platform.list_member WHERE id = '194945';
--rollback DELETE FROM platform.list_member WHERE id = '194946';
--rollback DELETE FROM platform.list_member WHERE id = '194947';
--rollback DELETE FROM platform.list_member WHERE id = '194948';
--rollback DELETE FROM platform.list_member WHERE id = '194949';
--rollback DELETE FROM platform.list_member WHERE id = '194950';
--rollback DELETE FROM platform.list_member WHERE id = '194951';
--rollback DELETE FROM platform.list_member WHERE id = '194952';
--rollback DELETE FROM platform.list_member WHERE id = '194953';
--rollback DELETE FROM platform.list_member WHERE id = '194954';
--rollback DELETE FROM platform.list_member WHERE id = '194955';
--rollback DELETE FROM platform.list_member WHERE id = '194956';
--rollback DELETE FROM platform.list_member WHERE id = '194957';
--rollback DELETE FROM platform.list_member WHERE id = '194958';
--rollback DELETE FROM platform.list_member WHERE id = '194959';
--rollback DELETE FROM platform.list_member WHERE id = '194960';
--rollback DELETE FROM platform.list_member WHERE id = '194961';
--rollback DELETE FROM platform.list_member WHERE id = '194962';
--rollback DELETE FROM platform.list_member WHERE id = '194963';
--rollback DELETE FROM platform.list_member WHERE id = '194964';
--rollback DELETE FROM platform.list_member WHERE id = '194965';
--rollback DELETE FROM platform.list_member WHERE id = '194966';
--rollback DELETE FROM platform.list_member WHERE id = '194967';
--rollback DELETE FROM platform.list_member WHERE id = '194968';
--rollback DELETE FROM platform.list_member WHERE id = '194969';
--rollback DELETE FROM platform.list_member WHERE id = '194970';
--rollback DELETE FROM platform.list_member WHERE id = '194971';
--rollback DELETE FROM platform.list_member WHERE id = '194972';
--rollback DELETE FROM platform.list_member WHERE id = '194973';
--rollback DELETE FROM platform.list_member WHERE id = '194974';
--rollback DELETE FROM platform.list_member WHERE id = '194975';
--rollback DELETE FROM platform.list_member WHERE id = '194976';
--rollback DELETE FROM platform.list_member WHERE id = '194977';
--rollback DELETE FROM platform.list_member WHERE id = '194978';
--rollback DELETE FROM platform.list_member WHERE id = '194979';
--rollback DELETE FROM platform.list_member WHERE id = '194980';
--rollback DELETE FROM platform.list_member WHERE id = '194981';
--rollback DELETE FROM platform.list_member WHERE id = '194982';
--rollback DELETE FROM platform.list_member WHERE id = '194983';
--rollback DELETE FROM platform.list_member WHERE id = '194984';
--rollback DELETE FROM platform.list_member WHERE id = '194985';
--rollback DELETE FROM platform.list_member WHERE id = '194986';
--rollback DELETE FROM platform.list_member WHERE id = '194987';
--rollback DELETE FROM platform.list_member WHERE id = '194988';
--rollback DELETE FROM platform.list_member WHERE id = '194989';
--rollback DELETE FROM platform.list_member WHERE id = '194990';
--rollback DELETE FROM platform.list_member WHERE id = '194991';
--rollback DELETE FROM platform.list_member WHERE id = '194992';
--rollback DELETE FROM platform.list_member WHERE id = '194993';
--rollback DELETE FROM platform.list_member WHERE id = '194994';
--rollback DELETE FROM platform.list_member WHERE id = '194995';
--rollback DELETE FROM platform.list_member WHERE id = '194996';
--rollback DELETE FROM platform.list_member WHERE id = '194997';
--rollback DELETE FROM platform.list_member WHERE id = '194998';
--rollback DELETE FROM platform.list_member WHERE id = '194999';
--rollback DELETE FROM platform.list_member WHERE id = '195000';
--rollback DELETE FROM platform.list_member WHERE id = '195001';
--rollback DELETE FROM platform.list_member WHERE id = '195002';
--rollback DELETE FROM platform.list_member WHERE id = '195003';
--rollback DELETE FROM platform.list_member WHERE id = '195004';
--rollback DELETE FROM platform.list_member WHERE id = '195005';
--rollback DELETE FROM platform.list_member WHERE id = '195006';
--rollback DELETE FROM platform.list_member WHERE id = '195007';
--rollback DELETE FROM platform.list_member WHERE id = '195008';
--rollback DELETE FROM platform.list_member WHERE id = '195009';
--rollback DELETE FROM platform.list_member WHERE id = '195010';
--rollback DELETE FROM platform.list_member WHERE id = '195011';
--rollback DELETE FROM platform.list_member WHERE id = '195012';
--rollback DELETE FROM platform.list_member WHERE id = '195013';
--rollback DELETE FROM platform.list_member WHERE id = '195014';
--rollback DELETE FROM platform.list_member WHERE id = '195015';
--rollback DELETE FROM platform.list_member WHERE id = '195016';
--rollback DELETE FROM platform.list_member WHERE id = '195017';
--rollback DELETE FROM platform.list_member WHERE id = '195018';
--rollback DELETE FROM platform.list_member WHERE id = '195019';
--rollback DELETE FROM platform.list_member WHERE id = '195020';
--rollback DELETE FROM platform.list_member WHERE id = '195021';
--rollback DELETE FROM platform.list_member WHERE id = '195022';
--rollback DELETE FROM platform.list_member WHERE id = '195023';
--rollback DELETE FROM platform.list_member WHERE id = '195024';
--rollback DELETE FROM platform.list_member WHERE id = '195025';
--rollback DELETE FROM platform.list_member WHERE id = '195026';
--rollback DELETE FROM platform.list_member WHERE id = '195027';
--rollback DELETE FROM platform.list_member WHERE id = '195028';
--rollback DELETE FROM platform.list_member WHERE id = '195029';
--rollback DELETE FROM platform.list_member WHERE id = '195030';
--rollback DELETE FROM platform.list_member WHERE id = '195031';
--rollback DELETE FROM platform.list_member WHERE id = '195032';
--rollback DELETE FROM platform.list_member WHERE id = '195033';
--rollback DELETE FROM platform.list_member WHERE id = '195034';
--rollback DELETE FROM platform.list_member WHERE id = '195035';
--rollback DELETE FROM platform.list_member WHERE id = '195036';
--rollback DELETE FROM platform.list_member WHERE id = '195037';
--rollback DELETE FROM platform.list_member WHERE id = '195038';
--rollback DELETE FROM platform.list_member WHERE id = '195039';
--rollback DELETE FROM platform.list_member WHERE id = '195040';
--rollback DELETE FROM platform.list_member WHERE id = '195041';
--rollback DELETE FROM platform.list_member WHERE id = '195042';
--rollback DELETE FROM platform.list_member WHERE id = '195043';
--rollback DELETE FROM platform.list_member WHERE id = '195044';
--rollback DELETE FROM platform.list_member WHERE id = '195045';
--rollback DELETE FROM platform.list_member WHERE id = '195046';
--rollback DELETE FROM platform.list_member WHERE id = '195047';
--rollback DELETE FROM platform.list_member WHERE id = '195048';
--rollback DELETE FROM platform.list_member WHERE id = '195049';
--rollback DELETE FROM platform.list_member WHERE id = '195050';
--rollback DELETE FROM platform.list_member WHERE id = '195051';
--rollback DELETE FROM platform.list_member WHERE id = '195052';
--rollback DELETE FROM platform.list_member WHERE id = '195053';
--rollback DELETE FROM platform.list_member WHERE id = '195054';
--rollback DELETE FROM platform.list_member WHERE id = '195055';
--rollback DELETE FROM platform.list_member WHERE id = '195056';
--rollback DELETE FROM platform.list_member WHERE id = '195057';
--rollback DELETE FROM platform.list_member WHERE id = '195058';
--rollback DELETE FROM platform.list_member WHERE id = '195059';
--rollback DELETE FROM platform.list_member WHERE id = '195060';
--rollback DELETE FROM platform.list_member WHERE id = '195061';
--rollback DELETE FROM platform.list_member WHERE id = '195062';
--rollback DELETE FROM platform.list_member WHERE id = '195063';
--rollback DELETE FROM platform.list_member WHERE id = '195064';
--rollback DELETE FROM platform.list_member WHERE id = '195065';
--rollback DELETE FROM platform.list_member WHERE id = '195066';
--rollback DELETE FROM platform.list_member WHERE id = '195067';
--rollback DELETE FROM platform.list_member WHERE id = '195068';
--rollback DELETE FROM platform.list_member WHERE id = '195069';
--rollback DELETE FROM platform.list_member WHERE id = '195070';
--rollback DELETE FROM platform.list_member WHERE id = '195071';
--rollback DELETE FROM platform.list_member WHERE id = '195072';
--rollback DELETE FROM platform.list_member WHERE id = '195073';
--rollback DELETE FROM platform.list_member WHERE id = '195074';
--rollback DELETE FROM platform.list_member WHERE id = '195075';
--rollback DELETE FROM platform.list_member WHERE id = '195076';
--rollback DELETE FROM platform.list_member WHERE id = '195077';
--rollback DELETE FROM platform.list_member WHERE id = '195078';
--rollback DELETE FROM platform.list_member WHERE id = '195079';
--rollback DELETE FROM platform.list_member WHERE id = '195080';
--rollback DELETE FROM platform.list_member WHERE id = '195081';
--rollback DELETE FROM platform.list_member WHERE id = '195082';
--rollback DELETE FROM platform.list_member WHERE id = '195083';
--rollback DELETE FROM platform.list_member WHERE id = '195084';
--rollback DELETE FROM platform.list_member WHERE id = '195085';
--rollback DELETE FROM platform.list_member WHERE id = '195086';
--rollback DELETE FROM platform.list_member WHERE id = '195087';
--rollback DELETE FROM platform.list_member WHERE id = '195088';
--rollback DELETE FROM platform.list_member WHERE id = '195089';
--rollback DELETE FROM platform.list_member WHERE id = '195090';
--rollback DELETE FROM platform.list_member WHERE id = '195091';
--rollback DELETE FROM platform.list_member WHERE id = '195092';
--rollback DELETE FROM platform.list_member WHERE id = '195093';
--rollback DELETE FROM platform.list_member WHERE id = '195094';
--rollback DELETE FROM platform.list_member WHERE id = '195095';
--rollback DELETE FROM platform.list_member WHERE id = '195096';
--rollback DELETE FROM platform.list_member WHERE id = '195097';
--rollback DELETE FROM platform.list_member WHERE id = '195098';
--rollback DELETE FROM platform.list_member WHERE id = '195099';
--rollback DELETE FROM platform.list_member WHERE id = '195100';
--rollback DELETE FROM platform.list_member WHERE id = '195101';
--rollback DELETE FROM platform.list_member WHERE id = '195102';
--rollback DELETE FROM platform.list_member WHERE id = '195103';
--rollback DELETE FROM platform.list_member WHERE id = '195104';
--rollback DELETE FROM platform.list_member WHERE id = '195105';
--rollback DELETE FROM platform.list_member WHERE id = '195106';
--rollback DELETE FROM platform.list_member WHERE id = '195107';
--rollback DELETE FROM platform.list_member WHERE id = '195108';
--rollback DELETE FROM platform.list_member WHERE id = '195109';
--rollback DELETE FROM platform.list_member WHERE id = '195110';
--rollback DELETE FROM platform.list_member WHERE id = '195111';
--rollback DELETE FROM platform.list_member WHERE id = '195112';
--rollback DELETE FROM platform.list_member WHERE id = '195113';
--rollback DELETE FROM platform.list_member WHERE id = '195114';
--rollback DELETE FROM platform.list_member WHERE id = '195115';
--rollback DELETE FROM platform.list_member WHERE id = '195116';
--rollback DELETE FROM platform.list_member WHERE id = '195117';
--rollback DELETE FROM platform.list_member WHERE id = '195118';
--rollback DELETE FROM platform.list_member WHERE id = '195119';
--rollback DELETE FROM platform.list_member WHERE id = '195120';
--rollback DELETE FROM platform.list_member WHERE id = '195121';
--rollback DELETE FROM platform.list_member WHERE id = '195122';
--rollback DELETE FROM platform.list_member WHERE id = '195123';
--rollback DELETE FROM platform.list_member WHERE id = '195124';
--rollback DELETE FROM platform.list_member WHERE id = '195125';
--rollback DELETE FROM platform.list_member WHERE id = '195126';
--rollback DELETE FROM platform.list_member WHERE id = '195127';
--rollback DELETE FROM platform.list_member WHERE id = '195128';
--rollback DELETE FROM platform.list_member WHERE id = '195129';
--rollback DELETE FROM platform.list_member WHERE id = '195130';
--rollback DELETE FROM platform.list_member WHERE id = '195131';
--rollback DELETE FROM platform.list_member WHERE id = '195132';
--rollback DELETE FROM platform.list_member WHERE id = '195133';
--rollback DELETE FROM platform.list_member WHERE id = '195134';
--rollback DELETE FROM platform.list_member WHERE id = '195135';
--rollback DELETE FROM platform.list_member WHERE id = '195136';
--rollback DELETE FROM platform.list_member WHERE id = '195137';
--rollback DELETE FROM platform.list_member WHERE id = '195138';
--rollback DELETE FROM platform.list_member WHERE id = '195139';
--rollback DELETE FROM platform.list_member WHERE id = '195140';
--rollback DELETE FROM platform.list_member WHERE id = '195141';
--rollback DELETE FROM platform.list_member WHERE id = '195142';
--rollback DELETE FROM platform.list_member WHERE id = '195143';
--rollback DELETE FROM platform.list_member WHERE id = '195144';
--rollback DELETE FROM platform.list_member WHERE id = '195145';
--rollback DELETE FROM platform.list_member WHERE id = '195146';
--rollback DELETE FROM platform.list_member WHERE id = '195147';
--rollback DELETE FROM platform.list_member WHERE id = '195148';
--rollback DELETE FROM platform.list_member WHERE id = '195149';
--rollback DELETE FROM platform.list_member WHERE id = '195150';
--rollback DELETE FROM platform.list_member WHERE id = '195151';
--rollback DELETE FROM platform.list_member WHERE id = '195152';
--rollback DELETE FROM platform.list_member WHERE id = '195153';
--rollback DELETE FROM platform.list_member WHERE id = '195154';
--rollback DELETE FROM platform.list_member WHERE id = '195155';
--rollback DELETE FROM platform.list_member WHERE id = '195156';
--rollback DELETE FROM platform.list_member WHERE id = '195157';
--rollback DELETE FROM platform.list_member WHERE id = '195158';
--rollback DELETE FROM platform.list_member WHERE id = '195159';
--rollback DELETE FROM platform.list_member WHERE id = '195160';
--rollback DELETE FROM platform.list_member WHERE id = '195161';
--rollback DELETE FROM platform.list_member WHERE id = '195162';
--rollback DELETE FROM platform.list_member WHERE id = '195163';
--rollback DELETE FROM platform.list_member WHERE id = '195164';
--rollback DELETE FROM platform.list_member WHERE id = '195165';
--rollback DELETE FROM platform.list_member WHERE id = '195166';
--rollback DELETE FROM platform.list_member WHERE id = '195167';
--rollback DELETE FROM platform.list_member WHERE id = '195168';
--rollback DELETE FROM platform.list_member WHERE id = '195169';
--rollback DELETE FROM platform.list_member WHERE id = '195170';
--rollback DELETE FROM platform.list_member WHERE id = '195171';
--rollback DELETE FROM platform.list_member WHERE id = '195172';
--rollback DELETE FROM platform.list_member WHERE id = '195173';
--rollback DELETE FROM platform.list_member WHERE id = '195174';
--rollback DELETE FROM platform.list_member WHERE id = '195175';
--rollback DELETE FROM platform.list_member WHERE id = '195176';
--rollback DELETE FROM platform.list_member WHERE id = '195177';
--rollback DELETE FROM platform.list_member WHERE id = '195178';
--rollback DELETE FROM platform.list_member WHERE id = '195179';
--rollback DELETE FROM platform.list_member WHERE id = '195180';
--rollback DELETE FROM platform.list_member WHERE id = '195181';
--rollback DELETE FROM platform.list_member WHERE id = '195182';
--rollback DELETE FROM platform.list_member WHERE id = '195183';
--rollback DELETE FROM platform.list_member WHERE id = '195184';
--rollback DELETE FROM platform.list_member WHERE id = '195185';
--rollback DELETE FROM platform.list_member WHERE id = '195186';
--rollback DELETE FROM platform.list_member WHERE id = '195187';
--rollback DELETE FROM platform.list_member WHERE id = '195188';
--rollback DELETE FROM platform.list_member WHERE id = '195189';
--rollback DELETE FROM platform.list_member WHERE id = '195190';
--rollback DELETE FROM platform.list_member WHERE id = '195191';
--rollback DELETE FROM platform.list_member WHERE id = '195192';
--rollback DELETE FROM platform.list_member WHERE id = '195193';
--rollback DELETE FROM platform.list_member WHERE id = '195194';
--rollback DELETE FROM platform.list_member WHERE id = '195195';
--rollback DELETE FROM platform.list_member WHERE id = '195196';
--rollback DELETE FROM platform.list_member WHERE id = '195197';
--rollback DELETE FROM platform.list_member WHERE id = '195198';
--rollback DELETE FROM platform.list_member WHERE id = '195199';
--rollback DELETE FROM platform.list_member WHERE id = '195200';
--rollback DELETE FROM platform.list_member WHERE id = '195201';
--rollback DELETE FROM platform.list_member WHERE id = '195202';
--rollback DELETE FROM platform.list_member WHERE id = '195203';
--rollback DELETE FROM platform.list_member WHERE id = '195204';
--rollback DELETE FROM platform.list_member WHERE id = '195205';
--rollback DELETE FROM platform.list_member WHERE id = '195206';
--rollback DELETE FROM platform.list_member WHERE id = '195207';
--rollback DELETE FROM platform.list_member WHERE id = '195208';
--rollback DELETE FROM platform.list_member WHERE id = '195209';
--rollback DELETE FROM platform.list_member WHERE id = '195210';
--rollback DELETE FROM platform.list_member WHERE id = '195211';
--rollback DELETE FROM platform.list_member WHERE id = '195212';
--rollback DELETE FROM platform.list_member WHERE id = '195213';
--rollback DELETE FROM platform.list_member WHERE id = '195214';
--rollback DELETE FROM platform.list_member WHERE id = '195215';
--rollback DELETE FROM platform.list_member WHERE id = '195216';
--rollback DELETE FROM platform.list_member WHERE id = '195217';
--rollback DELETE FROM platform.list_member WHERE id = '195218';
--rollback DELETE FROM platform.list_member WHERE id = '195219';
--rollback DELETE FROM platform.list_member WHERE id = '195220';
--rollback DELETE FROM platform.list_member WHERE id = '195221';
--rollback DELETE FROM platform.list_member WHERE id = '195222';
--rollback DELETE FROM platform.list_member WHERE id = '195223';
--rollback DELETE FROM platform.list_member WHERE id = '195224';
--rollback DELETE FROM platform.list_member WHERE id = '195225';
--rollback DELETE FROM platform.list_member WHERE id = '195226';
--rollback DELETE FROM platform.list_member WHERE id = '195227';
--rollback DELETE FROM platform.list_member WHERE id = '195228';
--rollback DELETE FROM platform.list_member WHERE id = '195229';
--rollback DELETE FROM platform.list_member WHERE id = '195230';
--rollback DELETE FROM platform.list_member WHERE id = '195231';
--rollback DELETE FROM platform.list_member WHERE id = '195232';
--rollback DELETE FROM platform.list_member WHERE id = '195233';
--rollback DELETE FROM platform.list_member WHERE id = '195234';
--rollback DELETE FROM platform.list_member WHERE id = '195235';
--rollback DELETE FROM platform.list_member WHERE id = '195236';
--rollback DELETE FROM platform.list_member WHERE id = '195237';
--rollback DELETE FROM platform.list_member WHERE id = '195238';
--rollback DELETE FROM platform.list_member WHERE id = '195239';
--rollback DELETE FROM platform.list_member WHERE id = '195240';
--rollback DELETE FROM platform.list_member WHERE id = '195241';
--rollback DELETE FROM platform.list_member WHERE id = '195242';
--rollback DELETE FROM platform.list_member WHERE id = '195243';
--rollback DELETE FROM platform.list_member WHERE id = '195244';
--rollback DELETE FROM platform.list_member WHERE id = '195245';
--rollback DELETE FROM platform.list_member WHERE id = '195246';
--rollback DELETE FROM platform.list_member WHERE id = '195247';
--rollback DELETE FROM platform.list_member WHERE id = '195248';
--rollback DELETE FROM platform.list_member WHERE id = '195249';
--rollback DELETE FROM platform.list_member WHERE id = '195250';
--rollback DELETE FROM platform.list_member WHERE id = '195251';
--rollback DELETE FROM platform.list_member WHERE id = '195252';
--rollback DELETE FROM platform.list_member WHERE id = '195253';
--rollback DELETE FROM platform.list_member WHERE id = '195254';
--rollback DELETE FROM platform.list_member WHERE id = '195255';
--rollback DELETE FROM platform.list_member WHERE id = '195256';
--rollback DELETE FROM platform.list_member WHERE id = '195257';
--rollback DELETE FROM platform.list_member WHERE id = '195258';
--rollback DELETE FROM platform.list_member WHERE id = '195259';
--rollback DELETE FROM platform.list_member WHERE id = '195260';
--rollback DELETE FROM platform.list_member WHERE id = '195261';
--rollback DELETE FROM platform.list_member WHERE id = '195262';
--rollback DELETE FROM platform.list_member WHERE id = '195263';
--rollback DELETE FROM platform.list_member WHERE id = '195264';
--rollback DELETE FROM platform.list_member WHERE id = '195265';
--rollback DELETE FROM platform.list_member WHERE id = '195266';
--rollback DELETE FROM platform.list_member WHERE id = '195267';
--rollback DELETE FROM platform.list_member WHERE id = '195268';
--rollback DELETE FROM platform.list_member WHERE id = '195269';
--rollback DELETE FROM platform.list_member WHERE id = '195270';
--rollback DELETE FROM platform.list_member WHERE id = '195271';
--rollback DELETE FROM platform.list_member WHERE id = '195272';
--rollback DELETE FROM platform.list_member WHERE id = '195273';
--rollback DELETE FROM platform.list_member WHERE id = '195274';
--rollback DELETE FROM platform.list_member WHERE id = '195275';
--rollback DELETE FROM platform.list_member WHERE id = '195276';
--rollback DELETE FROM platform.list_member WHERE id = '195277';
--rollback DELETE FROM platform.list_member WHERE id = '195278';
--rollback DELETE FROM platform.list_member WHERE id = '195279';
--rollback DELETE FROM platform.list_member WHERE id = '195280';
--rollback DELETE FROM platform.list_member WHERE id = '195281';
--rollback DELETE FROM platform.list_member WHERE id = '195282';
--rollback DELETE FROM platform.list_member WHERE id = '195283';
--rollback DELETE FROM platform.list_member WHERE id = '195284';
--rollback DELETE FROM platform.list_member WHERE id = '195285';
--rollback DELETE FROM platform.list_member WHERE id = '195286';
--rollback DELETE FROM platform.list_member WHERE id = '195287';
--rollback DELETE FROM platform.list_member WHERE id = '195288';
--rollback DELETE FROM platform.list_member WHERE id = '195289';
--rollback DELETE FROM platform.list_member WHERE id = '195290';
--rollback DELETE FROM platform.list_member WHERE id = '195291';
--rollback DELETE FROM platform.list_member WHERE id = '195292';
--rollback DELETE FROM platform.list_member WHERE id = '195293';
--rollback DELETE FROM platform.list_member WHERE id = '195294';
--rollback DELETE FROM platform.list_member WHERE id = '195295';
--rollback DELETE FROM platform.list_member WHERE id = '195296';
--rollback DELETE FROM platform.list_member WHERE id = '195297';
--rollback DELETE FROM platform.list_member WHERE id = '195298';
--rollback DELETE FROM platform.list_member WHERE id = '195299';
--rollback DELETE FROM platform.list_member WHERE id = '195300';
--rollback DELETE FROM platform.list_member WHERE id = '195301';
--rollback DELETE FROM platform.list_member WHERE id = '195302';
--rollback DELETE FROM platform.list_member WHERE id = '195303';
--rollback DELETE FROM platform.list_member WHERE id = '195304';
--rollback DELETE FROM platform.list_member WHERE id = '195305';
--rollback DELETE FROM platform.list_member WHERE id = '195306';
--rollback DELETE FROM platform.list_member WHERE id = '195307';
--rollback DELETE FROM platform.list_member WHERE id = '195308';
--rollback DELETE FROM platform.list_member WHERE id = '195309';
--rollback DELETE FROM platform.list_member WHERE id = '195310';
--rollback DELETE FROM platform.list_member WHERE id = '195311';
--rollback DELETE FROM platform.list_member WHERE id = '195312';
--rollback DELETE FROM platform.list_member WHERE id = '195313';
--rollback DELETE FROM platform.list_member WHERE id = '195314';
--rollback DELETE FROM platform.list_member WHERE id = '195315';
--rollback DELETE FROM platform.list_member WHERE id = '195316';
--rollback DELETE FROM platform.list_member WHERE id = '195317';
--rollback DELETE FROM platform.list_member WHERE id = '195318';
--rollback DELETE FROM platform.list_member WHERE id = '195319';
--rollback DELETE FROM platform.list_member WHERE id = '195320';
--rollback DELETE FROM platform.list_member WHERE id = '195321';
--rollback DELETE FROM platform.list_member WHERE id = '195322';
--rollback DELETE FROM platform.list_member WHERE id = '195323';
--rollback DELETE FROM platform.list_member WHERE id = '195324';
--rollback DELETE FROM platform.list_member WHERE id = '195325';
--rollback DELETE FROM platform.list_member WHERE id = '195326';
--rollback DELETE FROM platform.list_member WHERE id = '195327';
--rollback DELETE FROM platform.list_member WHERE id = '195328';
--rollback DELETE FROM platform.list_member WHERE id = '195329';
--rollback DELETE FROM platform.list_member WHERE id = '195330';
--rollback DELETE FROM platform.list_member WHERE id = '195331';
--rollback DELETE FROM platform.list_member WHERE id = '195332';
--rollback DELETE FROM platform.list_member WHERE id = '195333';
--rollback DELETE FROM platform.list_member WHERE id = '195334';
--rollback DELETE FROM platform.list_member WHERE id = '195335';
--rollback DELETE FROM platform.list_member WHERE id = '195336';
--rollback DELETE FROM platform.list_member WHERE id = '195337';
--rollback DELETE FROM platform.list_member WHERE id = '195338';
--rollback DELETE FROM platform.list_member WHERE id = '195339';
--rollback DELETE FROM platform.list_member WHERE id = '195340';
--rollback DELETE FROM platform.list_member WHERE id = '195341';
--rollback DELETE FROM platform.list_member WHERE id = '195342';
--rollback DELETE FROM platform.list_member WHERE id = '195343';
--rollback DELETE FROM platform.list_member WHERE id = '195344';
--rollback DELETE FROM platform.list_member WHERE id = '195345';
--rollback DELETE FROM platform.list_member WHERE id = '195346';
--rollback DELETE FROM platform.list_member WHERE id = '195347';
--rollback DELETE FROM platform.list_member WHERE id = '195348';
--rollback DELETE FROM platform.list_member WHERE id = '195349';
--rollback DELETE FROM platform.list_member WHERE id = '195350';
--rollback DELETE FROM platform.list_member WHERE id = '195351';
--rollback DELETE FROM platform.list_member WHERE id = '195352';
--rollback DELETE FROM platform.list_member WHERE id = '195353';
--rollback DELETE FROM platform.list_member WHERE id = '195354';
--rollback DELETE FROM platform.list_member WHERE id = '195355';
--rollback DELETE FROM platform.list_member WHERE id = '195356';
--rollback DELETE FROM platform.list_member WHERE id = '195357';
--rollback DELETE FROM platform.list_member WHERE id = '195358';
--rollback DELETE FROM platform.list_member WHERE id = '195359';
--rollback DELETE FROM platform.list_member WHERE id = '195360';
--rollback DELETE FROM platform.list_member WHERE id = '195361';
--rollback DELETE FROM platform.list_member WHERE id = '195362';
--rollback DELETE FROM platform.list_member WHERE id = '195363';
--rollback DELETE FROM platform.list_member WHERE id = '195364';
--rollback DELETE FROM platform.list_member WHERE id = '195365';
--rollback DELETE FROM platform.list_member WHERE id = '195366';
--rollback DELETE FROM platform.list_member WHERE id = '195367';
--rollback DELETE FROM platform.list_member WHERE id = '195368';
--rollback DELETE FROM platform.list_member WHERE id = '195369';
--rollback DELETE FROM platform.list_member WHERE id = '195370';
--rollback DELETE FROM platform.list_member WHERE id = '195371';
--rollback DELETE FROM platform.list_member WHERE id = '195372';
--rollback DELETE FROM platform.list_member WHERE id = '195373';
--rollback DELETE FROM platform.list_member WHERE id = '195374';
--rollback DELETE FROM platform.list_member WHERE id = '195375';
--rollback DELETE FROM platform.list_member WHERE id = '195376';
--rollback DELETE FROM platform.list_member WHERE id = '195377';
--rollback DELETE FROM platform.list_member WHERE id = '195378';
--rollback DELETE FROM platform.list_member WHERE id = '195379';
--rollback DELETE FROM platform.list_member WHERE id = '195380';
--rollback DELETE FROM platform.list_member WHERE id = '195381';
--rollback DELETE FROM platform.list_member WHERE id = '195382';
--rollback DELETE FROM platform.list_member WHERE id = '195383';
--rollback DELETE FROM platform.list_member WHERE id = '195384';
--rollback DELETE FROM platform.list_member WHERE id = '195385';
--rollback DELETE FROM platform.list_member WHERE id = '195386';
--rollback DELETE FROM platform.list_member WHERE id = '195387';
--rollback DELETE FROM platform.list_member WHERE id = '195388';
--rollback DELETE FROM platform.list_member WHERE id = '195389';
--rollback DELETE FROM platform.list_member WHERE id = '195390';
--rollback DELETE FROM platform.list_member WHERE id = '195391';
--rollback DELETE FROM platform.list_member WHERE id = '195392';
--rollback DELETE FROM platform.list_member WHERE id = '195393';
--rollback DELETE FROM platform.list_member WHERE id = '195394';
--rollback DELETE FROM platform.list_member WHERE id = '195395';
--rollback DELETE FROM platform.list_member WHERE id = '195396';
--rollback DELETE FROM platform.list_member WHERE id = '195397';
--rollback DELETE FROM platform.list_member WHERE id = '195398';
--rollback DELETE FROM platform.list_member WHERE id = '195399';
--rollback DELETE FROM platform.list_member WHERE id = '195400';
--rollback DELETE FROM platform.list_member WHERE id = '195401';
--rollback DELETE FROM platform.list_member WHERE id = '195402';
--rollback DELETE FROM platform.list_member WHERE id = '195403';
--rollback DELETE FROM platform.list_member WHERE id = '195404';
--rollback DELETE FROM platform.list_member WHERE id = '195405';
--rollback DELETE FROM platform.list_member WHERE id = '195406';
--rollback DELETE FROM platform.list_member WHERE id = '195407';
--rollback DELETE FROM platform.list_member WHERE id = '195408';
--rollback DELETE FROM platform.list_member WHERE id = '195409';
--rollback DELETE FROM platform.list_member WHERE id = '195410';
--rollback DELETE FROM platform.list_member WHERE id = '195411';
--rollback DELETE FROM platform.list_member WHERE id = '195412';
--rollback DELETE FROM platform.list_member WHERE id = '195413';
--rollback DELETE FROM platform.list_member WHERE id = '195414';
--rollback DELETE FROM platform.list_member WHERE id = '195415';
--rollback DELETE FROM platform.list_member WHERE id = '195416';
--rollback DELETE FROM platform.list_member WHERE id = '195417';
--rollback DELETE FROM platform.list_member WHERE id = '195418';
--rollback DELETE FROM platform.list_member WHERE id = '195419';
--rollback DELETE FROM platform.list_member WHERE id = '195420';
--rollback DELETE FROM platform.list_member WHERE id = '195421';
--rollback DELETE FROM platform.list_member WHERE id = '195422';
--rollback DELETE FROM platform.list_member WHERE id = '195423';
--rollback DELETE FROM platform.list_member WHERE id = '195424';
--rollback DELETE FROM platform.list_member WHERE id = '195425';
--rollback DELETE FROM platform.list_member WHERE id = '195426';
--rollback DELETE FROM platform.list_member WHERE id = '195427';
--rollback DELETE FROM platform.list_member WHERE id = '195428';
--rollback DELETE FROM platform.list_member WHERE id = '195429';
--rollback DELETE FROM platform.list_member WHERE id = '195430';
--rollback DELETE FROM platform.list_member WHERE id = '195431';
--rollback DELETE FROM platform.list_member WHERE id = '195432';
--rollback DELETE FROM platform.list_member WHERE id = '195433';
--rollback DELETE FROM platform.list_member WHERE id = '195434';
--rollback DELETE FROM platform.list_member WHERE id = '195435';
--rollback DELETE FROM platform.list_member WHERE id = '195436';
--rollback DELETE FROM platform.list_member WHERE id = '195437';
--rollback DELETE FROM platform.list_member WHERE id = '195438';
--rollback DELETE FROM platform.list_member WHERE id = '195439';
--rollback DELETE FROM platform.list_member WHERE id = '195440';
--rollback DELETE FROM platform.list_member WHERE id = '195441';
--rollback DELETE FROM platform.list_member WHERE id = '195442';
--rollback DELETE FROM platform.list_member WHERE id = '195443';
--rollback DELETE FROM platform.list_member WHERE id = '195444';
--rollback DELETE FROM platform.list_member WHERE id = '195445';
--rollback DELETE FROM platform.list_member WHERE id = '195446';
--rollback DELETE FROM platform.list_member WHERE id = '195447';
--rollback DELETE FROM platform.list_member WHERE id = '195448';
--rollback DELETE FROM platform.list_member WHERE id = '195449';
--rollback DELETE FROM platform.list_member WHERE id = '195450';
--rollback DELETE FROM platform.list_member WHERE id = '195451';
--rollback DELETE FROM platform.list_member WHERE id = '195452';
--rollback DELETE FROM platform.list_member WHERE id = '195453';
--rollback DELETE FROM platform.list_member WHERE id = '195454';
--rollback DELETE FROM platform.list_member WHERE id = '195455';
--rollback DELETE FROM platform.list_member WHERE id = '195456';
--rollback DELETE FROM platform.list_member WHERE id = '195457';
--rollback DELETE FROM platform.list_member WHERE id = '195458';
--rollback DELETE FROM platform.list_member WHERE id = '195459';
--rollback DELETE FROM platform.list_member WHERE id = '195460';
--rollback DELETE FROM platform.list_member WHERE id = '195461';
--rollback DELETE FROM platform.list_member WHERE id = '195462';
--rollback DELETE FROM platform.list_member WHERE id = '195463';
--rollback DELETE FROM platform.list_member WHERE id = '195464';
--rollback DELETE FROM platform.list_member WHERE id = '195465';
--rollback DELETE FROM platform.list_member WHERE id = '195466';
--rollback DELETE FROM platform.list_member WHERE id = '195467';
--rollback DELETE FROM platform.list_member WHERE id = '195468';
--rollback DELETE FROM platform.list_member WHERE id = '195469';
--rollback DELETE FROM platform.list_member WHERE id = '195470';
--rollback DELETE FROM platform.list_member WHERE id = '195471';
--rollback DELETE FROM platform.list_member WHERE id = '195472';
--rollback DELETE FROM platform.list_member WHERE id = '195473';
--rollback DELETE FROM platform.list_member WHERE id = '195474';
--rollback DELETE FROM platform.list_member WHERE id = '195475';
--rollback DELETE FROM platform.list_member WHERE id = '195476';
--rollback DELETE FROM platform.list_member WHERE id = '195477';
--rollback DELETE FROM platform.list_member WHERE id = '195478';
--rollback DELETE FROM platform.list_member WHERE id = '195479';
--rollback DELETE FROM platform.list_member WHERE id = '195480';
--rollback DELETE FROM platform.list_member WHERE id = '195481';
--rollback DELETE FROM platform.list_member WHERE id = '195482';
--rollback DELETE FROM platform.list_member WHERE id = '195483';
--rollback DELETE FROM platform.list_member WHERE id = '195484';
--rollback DELETE FROM platform.list_member WHERE id = '195485';
--rollback DELETE FROM platform.list_member WHERE id = '195486';
--rollback DELETE FROM platform.list_member WHERE id = '195487';
--rollback DELETE FROM platform.list_member WHERE id = '195488';
--rollback DELETE FROM platform.list_member WHERE id = '195489';
--rollback DELETE FROM platform.list_member WHERE id = '195490';
--rollback DELETE FROM platform.list_member WHERE id = '195491';
--rollback DELETE FROM platform.list_member WHERE id = '195492';
--rollback DELETE FROM platform.list_member WHERE id = '195493';
--rollback DELETE FROM platform.list_member WHERE id = '195494';
--rollback DELETE FROM platform.list_member WHERE id = '195495';
--rollback DELETE FROM platform.list_member WHERE id = '195496';
--rollback DELETE FROM platform.list_member WHERE id = '195497';
--rollback DELETE FROM platform.list_member WHERE id = '195498';
--rollback DELETE FROM platform.list_member WHERE id = '195499';
--rollback DELETE FROM platform.list_member WHERE id = '195500';
--rollback DELETE FROM platform.list_member WHERE id = '195501';
--rollback DELETE FROM platform.list_member WHERE id = '195502';
--rollback DELETE FROM platform.list_member WHERE id = '195503';
--rollback DELETE FROM platform.list_member WHERE id = '195504';
--rollback DELETE FROM platform.list_member WHERE id = '195505';
--rollback DELETE FROM platform.list_member WHERE id = '195506';
--rollback DELETE FROM platform.list_member WHERE id = '195507';
--rollback DELETE FROM platform.list_member WHERE id = '195508';
--rollback DELETE FROM platform.list_member WHERE id = '195509';
--rollback DELETE FROM platform.list_member WHERE id = '195510';
--rollback DELETE FROM platform.list_member WHERE id = '195511';
--rollback DELETE FROM platform.list_member WHERE id = '195512';
--rollback DELETE FROM platform.list_member WHERE id = '195513';
--rollback DELETE FROM platform.list_member WHERE id = '195514';
--rollback DELETE FROM platform.list_member WHERE id = '195515';
--rollback DELETE FROM platform.list_member WHERE id = '195516';
--rollback DELETE FROM platform.list_member WHERE id = '195517';
--rollback DELETE FROM platform.list_member WHERE id = '195518';
--rollback DELETE FROM platform.list_member WHERE id = '195519';
--rollback DELETE FROM platform.list_member WHERE id = '195520';
--rollback DELETE FROM platform.list_member WHERE id = '195521';
--rollback DELETE FROM platform.list_member WHERE id = '195522';
--rollback DELETE FROM platform.list_member WHERE id = '195523';
--rollback DELETE FROM platform.list_member WHERE id = '195524';
--rollback DELETE FROM platform.list_member WHERE id = '195525';
--rollback DELETE FROM platform.list_member WHERE id = '195526';
--rollback DELETE FROM platform.list_member WHERE id = '195527';
--rollback DELETE FROM platform.list_member WHERE id = '195528';
--rollback DELETE FROM platform.list_member WHERE id = '195529';
--rollback DELETE FROM platform.list_member WHERE id = '195530';
--rollback DELETE FROM platform.list_member WHERE id = '195531';
--rollback DELETE FROM platform.list_member WHERE id = '195532';
--rollback DELETE FROM platform.list_member WHERE id = '195533';
--rollback DELETE FROM platform.list_member WHERE id = '195534';
--rollback DELETE FROM platform.list_member WHERE id = '195535';
--rollback DELETE FROM platform.list_member WHERE id = '195536';
--rollback DELETE FROM platform.list_member WHERE id = '195537';
--rollback DELETE FROM platform.list_member WHERE id = '195538';
--rollback DELETE FROM platform.list_member WHERE id = '195539';
--rollback DELETE FROM platform.list_member WHERE id = '195540';
--rollback DELETE FROM platform.list_member WHERE id = '195541';
--rollback DELETE FROM platform.list_member WHERE id = '195542';
--rollback DELETE FROM platform.list_member WHERE id = '195543';
--rollback DELETE FROM platform.list_member WHERE id = '195544';
--rollback DELETE FROM platform.list_member WHERE id = '195545';
--rollback DELETE FROM platform.list_member WHERE id = '195546';
--rollback DELETE FROM platform.list_member WHERE id = '195547';
--rollback DELETE FROM platform.list_member WHERE id = '195548';
--rollback DELETE FROM platform.list_member WHERE id = '195549';
--rollback DELETE FROM platform.list_member WHERE id = '195550';
--rollback DELETE FROM platform.list_member WHERE id = '195551';
--rollback DELETE FROM platform.list_member WHERE id = '195552';
--rollback DELETE FROM platform.list_member WHERE id = '195553';
--rollback DELETE FROM platform.list_member WHERE id = '195554';
--rollback DELETE FROM platform.list_member WHERE id = '195555';
--rollback DELETE FROM platform.list_member WHERE id = '195556';
--rollback DELETE FROM platform.list_member WHERE id = '195557';
--rollback DELETE FROM platform.list_member WHERE id = '195558';
--rollback DELETE FROM platform.list_member WHERE id = '195559';
--rollback DELETE FROM platform.list_member WHERE id = '195560';
--rollback DELETE FROM platform.list_member WHERE id = '195561';
--rollback DELETE FROM platform.list_member WHERE id = '195562';
--rollback DELETE FROM platform.list_member WHERE id = '195563';
--rollback DELETE FROM platform.list_member WHERE id = '195564';
--rollback DELETE FROM platform.list_member WHERE id = '195565';
--rollback DELETE FROM platform.list_member WHERE id = '195566';
--rollback DELETE FROM platform.list_member WHERE id = '195567';
--rollback DELETE FROM platform.list_member WHERE id = '195568';
--rollback DELETE FROM platform.list_member WHERE id = '195569';
--rollback DELETE FROM platform.list_member WHERE id = '195570';
--rollback DELETE FROM platform.list_member WHERE id = '195571';
--rollback DELETE FROM platform.list_member WHERE id = '195572';
--rollback DELETE FROM platform.list_member WHERE id = '195573';
--rollback DELETE FROM platform.list_member WHERE id = '195574';
--rollback DELETE FROM platform.list_member WHERE id = '195575';
--rollback DELETE FROM platform.list_member WHERE id = '195576';
--rollback DELETE FROM platform.list_member WHERE id = '195577';
--rollback DELETE FROM platform.list_member WHERE id = '195578';
--rollback DELETE FROM platform.list_member WHERE id = '195579';
--rollback DELETE FROM platform.list_member WHERE id = '195580';
--rollback DELETE FROM platform.list_member WHERE id = '195581';
--rollback DELETE FROM platform.list_member WHERE id = '195582';
--rollback DELETE FROM platform.list_member WHERE id = '195583';
--rollback DELETE FROM platform.list_member WHERE id = '195584';
--rollback DELETE FROM platform.list_member WHERE id = '195585';
--rollback DELETE FROM platform.list_member WHERE id = '195586';
--rollback DELETE FROM platform.list_member WHERE id = '195587';
--rollback DELETE FROM platform.list_member WHERE id = '195588';
--rollback DELETE FROM platform.list_member WHERE id = '195589';
--rollback DELETE FROM platform.list_member WHERE id = '195590';
--rollback DELETE FROM platform.list_member WHERE id = '195591';
--rollback DELETE FROM platform.list_member WHERE id = '195592';
--rollback DELETE FROM platform.list_member WHERE id = '195593';
--rollback DELETE FROM platform.list_member WHERE id = '195594';
--rollback DELETE FROM platform.list_member WHERE id = '195595';
--rollback DELETE FROM platform.list_member WHERE id = '195596';
--rollback DELETE FROM platform.list_member WHERE id = '195597';
--rollback DELETE FROM platform.list_member WHERE id = '195598';
--rollback DELETE FROM platform.list_member WHERE id = '195599';
--rollback DELETE FROM platform.list_member WHERE id = '195600';
--rollback DELETE FROM platform.list_member WHERE id = '195601';
--rollback DELETE FROM platform.list_member WHERE id = '195602';
--rollback DELETE FROM platform.list_member WHERE id = '195603';
--rollback DELETE FROM platform.list_member WHERE id = '195604';
--rollback DELETE FROM platform.list_member WHERE id = '195605';
--rollback DELETE FROM platform.list_member WHERE id = '195606';
--rollback DELETE FROM platform.list_member WHERE id = '195607';
--rollback DELETE FROM platform.list_member WHERE id = '195608';
--rollback DELETE FROM platform.list_member WHERE id = '195609';
--rollback DELETE FROM platform.list_member WHERE id = '195610';
--rollback DELETE FROM platform.list_member WHERE id = '195611';
--rollback DELETE FROM platform.list_member WHERE id = '195612';
--rollback DELETE FROM platform.list_member WHERE id = '195613';
--rollback DELETE FROM platform.list_member WHERE id = '195614';
--rollback DELETE FROM platform.list_member WHERE id = '195615';
--rollback DELETE FROM platform.list_member WHERE id = '195616';
--rollback DELETE FROM platform.list_member WHERE id = '195617';
--rollback DELETE FROM platform.list_member WHERE id = '195618';
--rollback DELETE FROM platform.list_member WHERE id = '195619';
--rollback DELETE FROM platform.list_member WHERE id = '195620';
--rollback DELETE FROM platform.list_member WHERE id = '195621';
--rollback DELETE FROM platform.list_member WHERE id = '195622';
--rollback DELETE FROM platform.list_member WHERE id = '195623';
--rollback DELETE FROM platform.list_member WHERE id = '195624';
--rollback DELETE FROM platform.list_member WHERE id = '195625';
--rollback DELETE FROM platform.list_member WHERE id = '195626';
--rollback DELETE FROM platform.list_member WHERE id = '195627';
--rollback DELETE FROM platform.list_member WHERE id = '195628';
--rollback DELETE FROM platform.list_member WHERE id = '195629';
--rollback DELETE FROM platform.list_member WHERE id = '195630';
--rollback DELETE FROM platform.list_member WHERE id = '195631';
--rollback DELETE FROM platform.list_member WHERE id = '195632';
--rollback DELETE FROM platform.list_member WHERE id = '195633';
--rollback DELETE FROM platform.list_member WHERE id = '195634';
--rollback DELETE FROM platform.list_member WHERE id = '195635';
--rollback DELETE FROM platform.list_member WHERE id = '195636';
--rollback DELETE FROM platform.list_member WHERE id = '195637';
--rollback DELETE FROM platform.list_member WHERE id = '195638';
--rollback DELETE FROM platform.list_member WHERE id = '195639';
--rollback DELETE FROM platform.list_member WHERE id = '195640';
--rollback DELETE FROM platform.list_member WHERE id = '195641';
--rollback DELETE FROM platform.list_member WHERE id = '195642';
--rollback DELETE FROM platform.list_member WHERE id = '195643';
--rollback DELETE FROM platform.list_member WHERE id = '195644';
--rollback DELETE FROM platform.list_member WHERE id = '195645';
--rollback DELETE FROM platform.list_member WHERE id = '195646';
--rollback DELETE FROM platform.list_member WHERE id = '195647';
--rollback DELETE FROM platform.list_member WHERE id = '195648';
--rollback DELETE FROM platform.list_member WHERE id = '195649';
--rollback DELETE FROM platform.list_member WHERE id = '195650';
--rollback DELETE FROM platform.list_member WHERE id = '195651';
--rollback DELETE FROM platform.list_member WHERE id = '195652';
--rollback DELETE FROM platform.list_member WHERE id = '195653';
--rollback DELETE FROM platform.list_member WHERE id = '195654';
--rollback DELETE FROM platform.list_member WHERE id = '195655';
--rollback DELETE FROM platform.list_member WHERE id = '195656';
--rollback DELETE FROM platform.list_member WHERE id = '195657';
--rollback DELETE FROM platform.list_member WHERE id = '195658';
--rollback DELETE FROM platform.list_member WHERE id = '195659';
--rollback DELETE FROM platform.list_member WHERE id = '195660';
--rollback DELETE FROM platform.list_member WHERE id = '195661';
--rollback DELETE FROM platform.list_member WHERE id = '195662';
--rollback DELETE FROM platform.list_member WHERE id = '195663';
--rollback DELETE FROM platform.list_member WHERE id = '195664';
--rollback DELETE FROM platform.list_member WHERE id = '195665';
--rollback DELETE FROM platform.list_member WHERE id = '195666';
--rollback DELETE FROM platform.list_member WHERE id = '195667';
--rollback DELETE FROM platform.list_member WHERE id = '195668';
--rollback DELETE FROM platform.list_member WHERE id = '195669';
--rollback DELETE FROM platform.list_member WHERE id = '195670';
--rollback DELETE FROM platform.list_member WHERE id = '195671';
--rollback DELETE FROM platform.list_member WHERE id = '195672';
--rollback DELETE FROM platform.list_member WHERE id = '195673';
--rollback DELETE FROM platform.list_member WHERE id = '195674';
--rollback DELETE FROM platform.list_member WHERE id = '195675';
--rollback DELETE FROM platform.list_member WHERE id = '195676';
--rollback DELETE FROM platform.list_member WHERE id = '195677';
--rollback DELETE FROM platform.list_member WHERE id = '195678';
--rollback DELETE FROM platform.list_member WHERE id = '195679';
--rollback DELETE FROM platform.list_member WHERE id = '195680';
--rollback DELETE FROM platform.list_member WHERE id = '195681';
--rollback DELETE FROM platform.list_member WHERE id = '195682';
--rollback DELETE FROM platform.list_member WHERE id = '195683';
--rollback DELETE FROM platform.list_member WHERE id = '195684';
--rollback DELETE FROM platform.list_member WHERE id = '195685';
--rollback DELETE FROM platform.list_member WHERE id = '195686';
--rollback DELETE FROM platform.list_member WHERE id = '195687';
--rollback DELETE FROM platform.list_member WHERE id = '195688';
--rollback DELETE FROM platform.list_member WHERE id = '195689';
--rollback DELETE FROM platform.list_member WHERE id = '195690';
--rollback DELETE FROM platform.list_member WHERE id = '195691';
--rollback DELETE FROM platform.list_member WHERE id = '195692';
--rollback DELETE FROM platform.list_member WHERE id = '195693';
--rollback DELETE FROM platform.list_member WHERE id = '195694';
--rollback DELETE FROM platform.list_member WHERE id = '195695';
--rollback DELETE FROM platform.list_member WHERE id = '195696';
--rollback DELETE FROM platform.list_member WHERE id = '195697';
--rollback DELETE FROM platform.list_member WHERE id = '195698';
--rollback DELETE FROM platform.list_member WHERE id = '195699';
--rollback DELETE FROM platform.list_member WHERE id = '195700';
--rollback DELETE FROM platform.list_member WHERE id = '195701';
--rollback DELETE FROM platform.list_member WHERE id = '195702';
--rollback DELETE FROM platform.list_member WHERE id = '195703';
--rollback DELETE FROM platform.list_member WHERE id = '195704';
--rollback DELETE FROM platform.list_member WHERE id = '195705';
--rollback DELETE FROM platform.list_member WHERE id = '195706';
--rollback DELETE FROM platform.list_member WHERE id = '195707';
--rollback DELETE FROM platform.list_member WHERE id = '195708';
--rollback DELETE FROM platform.list_member WHERE id = '195709';
--rollback DELETE FROM platform.list_member WHERE id = '195710';
--rollback DELETE FROM platform.list_member WHERE id = '195711';
--rollback DELETE FROM platform.list_member WHERE id = '195712';
--rollback DELETE FROM platform.list_member WHERE id = '195713';
--rollback DELETE FROM platform.list_member WHERE id = '195714';
--rollback DELETE FROM platform.list_member WHERE id = '195715';
--rollback DELETE FROM platform.list_member WHERE id = '195716';
--rollback DELETE FROM platform.list_member WHERE id = '195717';
--rollback DELETE FROM platform.list_member WHERE id = '195718';
--rollback DELETE FROM platform.list_member WHERE id = '195719';
--rollback DELETE FROM platform.list_member WHERE id = '195720';
--rollback DELETE FROM platform.list_member WHERE id = '195721';
--rollback DELETE FROM platform.list_member WHERE id = '195722';
--rollback DELETE FROM platform.list_member WHERE id = '195723';
--rollback DELETE FROM platform.list_member WHERE id = '195724';
--rollback DELETE FROM platform.list_member WHERE id = '195725';
--rollback DELETE FROM platform.list_member WHERE id = '195726';
--rollback DELETE FROM platform.list_member WHERE id = '195727';
--rollback DELETE FROM platform.list_member WHERE id = '195728';
--rollback DELETE FROM platform.list_member WHERE id = '195729';
--rollback DELETE FROM platform.list_member WHERE id = '195730';
--rollback DELETE FROM platform.list_member WHERE id = '195731';
--rollback DELETE FROM platform.list_member WHERE id = '195732';
--rollback DELETE FROM platform.list_member WHERE id = '195733';
--rollback DELETE FROM platform.list_member WHERE id = '195734';
--rollback DELETE FROM platform.list_member WHERE id = '195735';
--rollback DELETE FROM platform.list_member WHERE id = '195736';
--rollback DELETE FROM platform.list_member WHERE id = '195737';
--rollback DELETE FROM platform.list_member WHERE id = '195738';
--rollback DELETE FROM platform.list_member WHERE id = '195739';
--rollback DELETE FROM platform.list_member WHERE id = '195740';
--rollback DELETE FROM platform.list_member WHERE id = '195741';
--rollback DELETE FROM platform.list_member WHERE id = '195742';
--rollback DELETE FROM platform.list_member WHERE id = '195743';
--rollback DELETE FROM platform.list_member WHERE id = '195744';
--rollback DELETE FROM platform.list_member WHERE id = '195745';
--rollback DELETE FROM platform.list_member WHERE id = '195746';
--rollback DELETE FROM platform.list_member WHERE id = '195747';
--rollback DELETE FROM platform.list_member WHERE id = '195748';
--rollback DELETE FROM platform.list_member WHERE id = '195749';
--rollback DELETE FROM platform.list_member WHERE id = '195750';
--rollback DELETE FROM platform.list_member WHERE id = '195751';
--rollback DELETE FROM platform.list_member WHERE id = '195752';
--rollback DELETE FROM platform.list_member WHERE id = '195753';
--rollback DELETE FROM platform.list_member WHERE id = '195754';
--rollback DELETE FROM platform.list_member WHERE id = '195755';
--rollback DELETE FROM platform.list_member WHERE id = '195756';
--rollback DELETE FROM platform.list_member WHERE id = '195757';
--rollback DELETE FROM platform.list_member WHERE id = '195758';
--rollback DELETE FROM platform.list_member WHERE id = '195759';
--rollback DELETE FROM platform.list_member WHERE id = '195760';
--rollback DELETE FROM platform.list_member WHERE id = '195761';
--rollback DELETE FROM platform.list_member WHERE id = '195762';
--rollback DELETE FROM platform.list_member WHERE id = '195763';
--rollback DELETE FROM platform.list_member WHERE id = '195764';
--rollback DELETE FROM platform.list_member WHERE id = '195765';
--rollback DELETE FROM platform.list_member WHERE id = '195766';
--rollback DELETE FROM platform.list_member WHERE id = '195767';
--rollback DELETE FROM platform.list_member WHERE id = '195768';
--rollback DELETE FROM platform.list_member WHERE id = '195769';
--rollback DELETE FROM platform.list_member WHERE id = '195770';
--rollback DELETE FROM platform.list_member WHERE id = '195771';
--rollback DELETE FROM platform.list_member WHERE id = '195772';
--rollback DELETE FROM platform.list_member WHERE id = '195773';
--rollback DELETE FROM platform.list_member WHERE id = '195774';
--rollback DELETE FROM platform.list_member WHERE id = '195775';
--rollback DELETE FROM platform.list_member WHERE id = '195776';
--rollback DELETE FROM platform.list_member WHERE id = '195777';
--rollback DELETE FROM platform.list_member WHERE id = '195778';
--rollback DELETE FROM platform.list_member WHERE id = '195779';
--rollback DELETE FROM platform.list_member WHERE id = '195780';
--rollback DELETE FROM platform.list_member WHERE id = '195781';
--rollback DELETE FROM platform.list_member WHERE id = '195782';
--rollback DELETE FROM platform.list_member WHERE id = '195783';
--rollback DELETE FROM platform.list_member WHERE id = '195784';
--rollback DELETE FROM platform.list_member WHERE id = '195785';
--rollback DELETE FROM platform.list_member WHERE id = '195786';
--rollback DELETE FROM platform.list_member WHERE id = '195787';
--rollback DELETE FROM platform.list_member WHERE id = '195788';
--rollback DELETE FROM platform.list_member WHERE id = '195789';
--rollback DELETE FROM platform.list_member WHERE id = '195790';
--rollback DELETE FROM platform.list_member WHERE id = '195791';
--rollback DELETE FROM platform.list_member WHERE id = '195792';
--rollback DELETE FROM platform.list_member WHERE id = '195793';
--rollback DELETE FROM platform.list_member WHERE id = '195794';
--rollback DELETE FROM platform.list_member WHERE id = '195795';
--rollback DELETE FROM platform.list_member WHERE id = '195796';
--rollback DELETE FROM platform.list_member WHERE id = '195797';
--rollback DELETE FROM platform.list_member WHERE id = '195798';
--rollback DELETE FROM platform.list_member WHERE id = '195799';
--rollback DELETE FROM platform.list_member WHERE id = '195800';
--rollback DELETE FROM platform.list_member WHERE id = '195801';
--rollback DELETE FROM platform.list_member WHERE id = '195802';
--rollback DELETE FROM platform.list_member WHERE id = '195803';
--rollback DELETE FROM platform.list_member WHERE id = '195804';
--rollback DELETE FROM platform.list_member WHERE id = '195805';
--rollback DELETE FROM platform.list_member WHERE id = '195806';
--rollback DELETE FROM platform.list_member WHERE id = '195807';
--rollback DELETE FROM platform.list_member WHERE id = '195808';
--rollback DELETE FROM platform.list_member WHERE id = '195809';
--rollback DELETE FROM platform.list_member WHERE id = '195810';
--rollback DELETE FROM platform.list_member WHERE id = '195811';
--rollback DELETE FROM platform.list_member WHERE id = '195812';
--rollback DELETE FROM platform.list_member WHERE id = '195813';
--rollback DELETE FROM platform.list_member WHERE id = '195814';
--rollback DELETE FROM platform.list_member WHERE id = '195815';

--rollback DELETE FROM platform.list WHERE id = '1040';
--rollback DELETE FROM platform.list WHERE id = '1041';
--rollback DELETE FROM platform.list WHERE id = '1042';
--rollback DELETE FROM platform.list WHERE id = '1043';
--rollback DELETE FROM platform.list WHERE id = '1044';
--rollback DELETE FROM platform.list WHERE id = '1045';
--rollback DELETE FROM platform.list WHERE id = '1046';
--rollback DELETE FROM platform.list WHERE id = '1047';
--rollback DELETE FROM platform.list WHERE id = '1048';
--rollback DELETE FROM platform.list WHERE id = '1049';
--rollback DELETE FROM platform.list WHERE id = '1050';
--rollback DELETE FROM platform.list WHERE id = '1051';
--rollback DELETE FROM platform.list WHERE id = '1052';
--rollback DELETE FROM platform.list WHERE id = '1053';
--rollback DELETE FROM platform.list WHERE id = '1054';
--rollback DELETE FROM platform.list WHERE id = '1055';
--rollback DELETE FROM platform.list WHERE id = '1056';
--rollback DELETE FROM platform.list WHERE id = '1057';
--rollback DELETE FROM platform.list WHERE id = '1058';
--rollback DELETE FROM platform.list WHERE id = '1059';
--rollback DELETE FROM platform.list WHERE id = '1060';
--rollback DELETE FROM platform.list WHERE id = '1061';
--rollback DELETE FROM platform.list WHERE id = '1062';
--rollback DELETE FROM platform.list WHERE id = '1063';
--rollback DELETE FROM platform.list WHERE id = '1064';
--rollback DELETE FROM platform.list WHERE id = '1065';
--rollback DELETE FROM platform.list WHERE id = '1066';
--rollback DELETE FROM platform.list WHERE id = '1067';
--rollback DELETE FROM platform.list WHERE id = '1068';
--rollback DELETE FROM platform.list WHERE id = '1069';
--rollback DELETE FROM platform.list WHERE id = '1070';
--rollback DELETE FROM platform.list WHERE id = '1071';
--rollback DELETE FROM platform.list WHERE id = '1072';
--rollback DELETE FROM platform.list WHERE id = '1073';
--rollback DELETE FROM platform.list WHERE id = '1074';
--rollback DELETE FROM platform.list WHERE id = '1075';
--rollback DELETE FROM platform.list WHERE id = '1076';
--rollback DELETE FROM platform.list WHERE id = '1077';
--rollback DELETE FROM platform.list WHERE id = '1078';
--rollback DELETE FROM platform.list WHERE id = '1079';
--rollback DELETE FROM platform.list WHERE id = '1080';
--rollback DELETE FROM platform.list WHERE id = '1081';
--rollback DELETE FROM platform.list WHERE id = '1082';
--rollback DELETE FROM platform.list WHERE id = '1083';
--rollback DELETE FROM platform.list WHERE id = '1084';
--rollback DELETE FROM platform.list WHERE id = '1085';
--rollback DELETE FROM platform.list WHERE id = '1086';
--rollback DELETE FROM platform.list WHERE id = '1087';
--rollback DELETE FROM platform.list WHERE id = '1088';
--rollback DELETE FROM platform.list WHERE id = '1089';
--rollback DELETE FROM platform.list WHERE id = '1090';
--rollback DELETE FROM platform.list WHERE id = '1091';
--rollback DELETE FROM platform.list WHERE id = '1092';
--rollback DELETE FROM platform.list WHERE id = '1093';
--rollback DELETE FROM platform.list WHERE id = '1094';
--rollback DELETE FROM platform.list WHERE id = '1095';
--rollback DELETE FROM platform.list WHERE id = '1096';
--rollback DELETE FROM platform.list WHERE id = '1097';
--rollback DELETE FROM platform.list WHERE id = '1098';
--rollback DELETE FROM platform.list WHERE id = '1099';
--rollback DELETE FROM platform.list WHERE id = '1100';
--rollback DELETE FROM platform.list WHERE id = '1101';
--rollback DELETE FROM platform.list WHERE id = '1102';
--rollback DELETE FROM platform.list WHERE id = '1103';
--rollback DELETE FROM platform.list WHERE id = '1104';
--rollback DELETE FROM platform.list WHERE id = '1105';
--rollback DELETE FROM platform.list WHERE id = '1106';
--rollback DELETE FROM platform.list WHERE id = '1107';
--rollback DELETE FROM platform.list WHERE id = '1108';
--rollback DELETE FROM platform.list WHERE id = '1109';
--rollback DELETE FROM platform.list WHERE id = '1110';
--rollback DELETE FROM platform.list WHERE id = '1111';
--rollback DELETE FROM platform.list WHERE id = '1112';
--rollback DELETE FROM platform.list WHERE id = '1113';
--rollback DELETE FROM platform.list WHERE id = '1114';
--rollback DELETE FROM platform.list WHERE id = '1115';
--rollback DELETE FROM platform.list WHERE id = '1116';
--rollback DELETE FROM platform.list WHERE id = '1117';
--rollback DELETE FROM platform.list WHERE id = '1118';
--rollback DELETE FROM platform.list WHERE id = '1119';
--rollback DELETE FROM platform.list WHERE id = '1120';
--rollback DELETE FROM platform.list WHERE id = '1121';
--rollback DELETE FROM platform.list WHERE id = '1122';
--rollback DELETE FROM platform.list WHERE id = '1123';
--rollback DELETE FROM platform.list WHERE id = '1124';
--rollback DELETE FROM platform.list WHERE id = '1125';
--rollback DELETE FROM platform.list WHERE id = '1126';
--rollback DELETE FROM platform.list WHERE id = '1127';
--rollback DELETE FROM platform.list WHERE id = '1128';
--rollback DELETE FROM platform.list WHERE id = '1129';
--rollback DELETE FROM platform.list WHERE id = '1130';
--rollback DELETE FROM platform.list WHERE id = '1131';
--rollback DELETE FROM platform.list WHERE id = '1132';
--rollback DELETE FROM platform.list WHERE id = '1133';
--rollback DELETE FROM platform.list WHERE id = '1134';
--rollback DELETE FROM platform.list WHERE id = '1135';
--rollback DELETE FROM platform.list WHERE id = '1136';
--rollback DELETE FROM platform.list WHERE id = '1137';
--rollback DELETE FROM platform.list WHERE id = '1138';
--rollback DELETE FROM platform.list WHERE id = '1139';
--rollback DELETE FROM platform.list WHERE id = '1140';
--rollback DELETE FROM platform.list WHERE id = '1141';
--rollback DELETE FROM platform.list WHERE id = '1142';
--rollback DELETE FROM platform.list WHERE id = '1143';
--rollback DELETE FROM platform.list WHERE id = '1144';
--rollback DELETE FROM platform.list WHERE id = '1145';
--rollback DELETE FROM platform.list WHERE id = '1146';
--rollback DELETE FROM platform.list WHERE id = '1147';
--rollback DELETE FROM platform.list WHERE id = '1148';
--rollback DELETE FROM platform.list WHERE id = '1149';
--rollback DELETE FROM platform.list WHERE id = '1150';
--rollback DELETE FROM platform.list WHERE id = '1151';
--rollback DELETE FROM platform.list WHERE id = '1152';
--rollback DELETE FROM platform.list WHERE id = '1153';
--rollback DELETE FROM platform.list WHERE id = '1154';
--rollback DELETE FROM platform.list WHERE id = '1155';
--rollback DELETE FROM platform.list WHERE id = '1156';
--rollback DELETE FROM platform.list WHERE id = '1157';
--rollback DELETE FROM platform.list WHERE id = '1158';
--rollback DELETE FROM platform.list WHERE id = '1159';
--rollback DELETE FROM platform.list WHERE id = '1160';
--rollback DELETE FROM platform.list WHERE id = '1161';
--rollback DELETE FROM platform.list WHERE id = '1162';
--rollback DELETE FROM platform.list WHERE id = '1163';
--rollback DELETE FROM platform.list WHERE id = '1164';
--rollback DELETE FROM platform.list WHERE id = '1165';
--rollback DELETE FROM platform.list WHERE id = '1166';
--rollback DELETE FROM platform.list WHERE id = '1167';
--rollback DELETE FROM platform.list WHERE id = '1168';
--rollback DELETE FROM platform.list WHERE id = '1169';
--rollback DELETE FROM platform.list WHERE id = '1170';
--rollback DELETE FROM platform.list WHERE id = '1171';
--rollback DELETE FROM platform.list WHERE id = '1172';
--rollback DELETE FROM platform.list WHERE id = '1173';
--rollback DELETE FROM platform.list WHERE id = '1174';
--rollback DELETE FROM platform.list WHERE id = '1175';
--rollback DELETE FROM platform.list WHERE id = '1176';
--rollback DELETE FROM platform.list WHERE id = '1177';
--rollback DELETE FROM platform.list WHERE id = '1178';
--rollback DELETE FROM platform.list WHERE id = '1179';
--rollback DELETE FROM platform.list WHERE id = '1180';
--rollback DELETE FROM platform.list WHERE id = '1181';
--rollback DELETE FROM platform.list WHERE id = '1182';
--rollback DELETE FROM platform.list WHERE id = '1183';
--rollback DELETE FROM platform.list WHERE id = '1184';
--rollback DELETE FROM platform.list WHERE id = '1185';
--rollback DELETE FROM platform.list WHERE id = '1186';
--rollback DELETE FROM platform.list WHERE id = '1187';
--rollback DELETE FROM platform.list WHERE id = '1188';
--rollback DELETE FROM platform.list WHERE id = '1189';
--rollback DELETE FROM platform.list WHERE id = '1190';
--rollback DELETE FROM platform.list WHERE id = '1191';
--rollback DELETE FROM platform.list WHERE id = '1192';
--rollback DELETE FROM platform.list WHERE id = '1193';
--rollback DELETE FROM platform.list WHERE id = '1194';
--rollback DELETE FROM platform.list WHERE id = '1195';
--rollback DELETE FROM platform.list WHERE id = '1196';
--rollback DELETE FROM platform.list WHERE id = '1197';
--rollback DELETE FROM platform.list WHERE id = '1198';
--rollback DELETE FROM platform.list WHERE id = '1199';
--rollback DELETE FROM platform.list WHERE id = '1200';
--rollback DELETE FROM platform.list WHERE id = '1201';
--rollback DELETE FROM platform.list WHERE id = '1202';
--rollback DELETE FROM platform.list WHERE id = '1203';
--rollback DELETE FROM platform.list WHERE id = '1204';
--rollback DELETE FROM platform.list WHERE id = '1205';
--rollback DELETE FROM platform.list WHERE id = '1206';
--rollback DELETE FROM platform.list WHERE id = '1207';
--rollback DELETE FROM platform.list WHERE id = '1208';
--rollback DELETE FROM platform.list WHERE id = '1209';
--rollback DELETE FROM platform.list WHERE id = '1210';
--rollback DELETE FROM platform.list WHERE id = '1211';
--rollback DELETE FROM platform.list WHERE id = '1212';
--rollback DELETE FROM platform.list WHERE id = '1213';
--rollback DELETE FROM platform.list WHERE id = '1214';
--rollback DELETE FROM platform.list WHERE id = '1215';
--rollback DELETE FROM platform.list WHERE id = '1216';
--rollback DELETE FROM platform.list WHERE id = '1217';
--rollback DELETE FROM platform.list WHERE id = '1218';
--rollback DELETE FROM platform.list WHERE id = '1219';
--rollback DELETE FROM platform.list WHERE id = '1220';
--rollback DELETE FROM platform.list WHERE id = '1221';
--rollback DELETE FROM platform.list WHERE id = '1222';
--rollback DELETE FROM platform.list WHERE id = '1223';
--rollback DELETE FROM platform.list WHERE id = '1224';
--rollback DELETE FROM platform.list WHERE id = '1225';
--rollback DELETE FROM platform.list WHERE id = '1226';

--rollback SELECT SETVAL('platform.list_id_seq', COALESCE(MAX(id), 1)) FROM platform.list;
--rollback SELECT SETVAL('platform.list_member_id_seq', COALESCE(MAX(id), 1)) FROM platform.list_member;
--rollback SELECT SETVAL('tenant.protocol_data_id_seq', COALESCE(MAX(id), 1)) FROM tenant.protocol_data;
--rollback SELECT SETVAL('tenant.protocol_id_seq', COALESCE(MAX(id), 1)) FROM tenant.protocol;
--rollback SELECT SETVAL('experiment.experiment_protocol_id_seq', COALESCE(MAX(id), 1)) FROM experiment.experiment_protocol;
--rollback SELECT SETVAL('experiment.experiment_data_id_seq', COALESCE(MAX(id), 1)) FROM experiment.experiment_data;
