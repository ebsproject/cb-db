--liquibase formatted sql

--changeset postgres:add_column_list_usage_to_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-158 Add column list_usage to platform.list



ALTER TABLE 
	platform.list 
ADD COLUMN 
	list_usage varchar(32);
	
COMMENT ON COLUMN 
    platform.list.list_usage
IS 
	'List Usage: Identified usage of a list [LIST_USAGE]';

CREATE INDEX
	list_list_usage_idx
ON 
	platform.list
USING 
	btree (list_usage);

COMMENT ON INDEX
	platform.list_list_usage_idx
IS
	'A list can be retrieved by its list usage';



--rollback ALTER TABLE platform.list
--rollback DROP COLUMN list_usage;