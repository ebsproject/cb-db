--liquibase formatted sql

--changeset postgres:fix_validate_transaction_function_no_scale_values context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5886 Fix validating transaction records when there is no scale values



CREATE OR REPLACE FUNCTION data_terminal.validate_transaction(
    var_transaction_id integer
)
RETURNS character varying
LANGUAGE plpgsql
AS $function$
DECLARE
    r_variable RECORD;
    r_data_unit RECORD;	
BEGIN
    DELETE FROM data_terminal.transaction_dataset 
    WHERE 
        transaction_id = var_transaction_id
        AND (value is null or trim(value) = '')
        AND is_generated = false;

    --start get all variables in the transaction
    for r_variable in
        SELECT 
            v.id as variable_id,
            v.scale_id,
            v.data_type 
        FROM 
            master.variable v  
        WHERE 
            v.is_void = false 
            and v.id in (
                SELECT
                    distinct variable_id 
                FROM 
                    data_terminal.transaction_dataset 
                WHERE 
                    transaction_id = var_transaction_id
            )
    LOOP

    --start validates the data type
    IF r_variable.data_type = 'integer'
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^\d+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^\d+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;
    

    ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision') 
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^[-+]?[0-9]*\.?[0-9]+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid' 
        WHERE 
            value !~ '^[-+]?[0-9]*\.?[0-9]+$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;
    
    ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' ) 
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;
    
    ELSIF r_variable.data_type = 'date' THEN	
        /* valid date format is YYYY-mm-dd */
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        /*convert format from dd-mm-YYYY into YYYY-mm-dd */
        UPDATE data_terminal.transaction_dataset 
        SET value=
            substring( value from '^\d+\/\d+\/((19|20)\d\d)$') /*year*/ || '-' ||
            substring( value from '^\d+\/(0[1-9]|1[012])\/\d+$') /*month*/ || '-' ||
            substring( value from '^((0[1-9]|[12][0-9]|3[01]))\/(.)+$') /*day*/
        WHERE 
            value ~ '^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$'
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$' 
            and transaction_id = var_transaction_id 
            and variable_id = r_variable.variable_id;

    ELSIF r_variable.data_type = 'time'
    THEN
        UPDATE data_terminal.transaction_dataset 
        SET status= 'new' 
        WHERE 
            value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;

        UPDATE data_terminal.transaction_dataset 
        SET status= 'invalid'  
        WHERE 
            value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;
                
    END IF;

--end validation of the data type

    --for scale values, if variable has scale values
    IF r_variable.scale_id is not NULL THEN
        --start validates the value
        IF EXISTS (SELECT 1 FROM master.scale_value where scale_id = r_variable.scale_id) THEN
            UPDATE data_terminal.transaction_dataset 
                SET status = 'invalid' 
            WHERE 
                value NOT IN (
                    SELECT value 
                    from 
                        master.scale_value 
                    WHERE 
                    scale_id = r_variable.scale_id
                    AND is_void = false
                )
                AND transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id;

            UPDATE data_terminal.transaction_dataset 
                SET status = 'new'
            WHERE 
                value IN (
                    SELECT value 
                    from 
                        master.scale_value 
                    WHERE 
                    scale_id = r_variable.scale_id
                    AND is_void = false
                )
                AND transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id;
        ELSE	-- no scale value, hence, value is valid
             UPDATE data_terminal.transaction_dataset 
                SET status = 'new' 
            WHERE 
                transaction_id = var_transaction_id
                AND variable_id = r_variable.variable_id
                AND status <> 'invalid';

        END IF;
        --end validates the value
    ELSE
    -- no scale value, hence, value is valid
        UPDATE data_terminal.transaction_dataset 
        SET is_data_value_valid = 'new' 
        WHERE 
            transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    END IF;
    
    END LOOP;

    -- start looping through the data unit in the transaction
    for r_data_unit in
        SELECT 
            t.data_unit
        FROM 
            data_terminal.transaction_dataset t
        WHERE 
            t.transaction_id = var_transaction_id
        GROUP by t.data_unit
    LOOP
        IF r_data_unit.data_unit = 'plot_data' THEN
            UPDATE data_terminal.transaction_dataset t
            SET status = 'updated'
            FROM 
                operational.plot_data pd
            WHERE 
                pd.variable_id = t.variable_id
                and t.transaction_id = var_transaction_id
                and pd.plot_id = t.entity_id
                and pd.is_void = false
                and t.entity = 'plot_data'
                and t.value <> pd.value;
    -- end check if the record is already in the operational
        END IF;
-- end looping through the data unit in the transaction	
    END LOOP;

RETURN 'success';
    
END;	
$function$;



--rollback CREATE OR REPLACE FUNCTION data_terminal.validate_transaction(
--rollback 	var_transaction_id integer)
--rollback     RETURNS character varying
--rollback     LANGUAGE 'plpgsql'
--rollback 
--rollback     COST 100
--rollback     VOLATILE 
--rollback AS $BODY$
--rollback DECLARE
--rollback     r_variable RECORD;
--rollback     r_data_unit RECORD;	
--rollback BEGIN
--rollback     DELETE FROM data_terminal.transaction_dataset 
--rollback     WHERE 
--rollback         transaction_id = var_transaction_id
--rollback         AND (value is null or trim(value) = '')
--rollback         AND is_generated = false;
--rollback 
--rollback     
--rollback     for r_variable in
--rollback         SELECT 
--rollback             v.id as variable_id,
--rollback             v.scale_id,
--rollback             v.data_type 
--rollback         FROM 
--rollback             master.variable v  
--rollback         WHERE 
--rollback             v.is_void = false 
--rollback             and v.id in (
--rollback                 SELECT
--rollback                     distinct variable_id 
--rollback                 FROM 
--rollback                     data_terminal.transaction_dataset 
--rollback                 WHERE 
--rollback                     transaction_id = var_transaction_id
--rollback             )
--rollback     LOOP
--rollback 
--rollback     
--rollback     IF r_variable.data_type = 'integer'
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^\d+$' 
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^\d+$' 
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback     
--rollback 
--rollback     ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision') 
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^[-+]?[0-9]*\.?[0-9]+$' 
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid' 
--rollback         WHERE 
--rollback             value !~ '^[-+]?[0-9]*\.?[0-9]+$' 
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback     
--rollback     ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' ) 
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback     
--rollback     ELSIF r_variable.data_type = 'date' THEN	
--rollback         
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback 
--rollback         
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET value=
--rollback             substring( value from '^\d+\/\d+\/((19|20)\d\d)$')  || '-' ||
--rollback             substring( value from '^\d+\/(0[1-9]|1[012])\/\d+$')  || '-' ||
--rollback             substring( value from '^((0[1-9]|[12][0-9]|3[01]))\/(.)+$') 
--rollback         WHERE 
--rollback             value ~ '^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$'
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$' 
--rollback             and transaction_id = var_transaction_id 
--rollback             and variable_id = r_variable.variable_id;
--rollback 
--rollback     ELSIF r_variable.data_type = 'time'
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
--rollback             AND transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
--rollback             AND transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback                 
--rollback     END IF;
--rollback 
--rollback     
--rollback     IF r_variable.scale_id is not NULL THEN
--rollback         
--rollback         IF EXISTS (SELECT 1 FROM master.scale_value where scale_id = r_variable.scale_id) THEN
--rollback             UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'invalid' 
--rollback             WHERE 
--rollback                 value NOT IN (
--rollback                     SELECT value 
--rollback                     from 
--rollback                         master.scale_value 
--rollback                     WHERE 
--rollback                     scale_id = r_variable.scale_id
--rollback                     AND is_void = false
--rollback                 )
--rollback                 AND transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id;
--rollback 
--rollback             UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'new'
--rollback             WHERE 
--rollback                 value IN (
--rollback                     SELECT value 
--rollback                     from 
--rollback                         master.scale_value 
--rollback                     WHERE 
--rollback                     scale_id = r_variable.scale_id
--rollback                     AND is_void = false
--rollback                 )
--rollback                 AND transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id;
--rollback         ELSE	
--rollback              UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'new' 
--rollback             WHERE 
--rollback                 transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id;
--rollback         END IF;
--rollback         
--rollback     ELSE
--rollback     
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET is_data_value_valid = 'new' 
--rollback         WHERE 
--rollback             transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback     END IF;
--rollback     
--rollback     END LOOP;
--rollback 
--rollback 
--rollback     for r_data_unit in
--rollback         SELECT 
--rollback             t.data_unit
--rollback         FROM 
--rollback             data_terminal.transaction_dataset t
--rollback         WHERE 
--rollback             t.transaction_id = var_transaction_id
--rollback         GROUP by t.data_unit
--rollback     LOOP
--rollback         IF r_data_unit.data_unit = 'plot_data' THEN
--rollback             UPDATE data_terminal.transaction_dataset t
--rollback             SET status = 'updated'
--rollback             FROM 
--rollback                 operational.plot_data pd
--rollback             WHERE 
--rollback                 pd.variable_id = t.variable_id
--rollback                 and t.transaction_id = var_transaction_id
--rollback                 and pd.plot_id = t.entity_id
--rollback                 and pd.is_void = false
--rollback                 and t.entity = 'plot_data'
--rollback                 and t.value <> pd.value;
--rollback     
--rollback         END IF;
--rollback 
--rollback     END LOOP;
--rollback 
--rollback RETURN 'success';
--rollback     
--rollback END;	
--rollback $BODY$;