--liquibase formatted sql

--changeset postgres:add_column_remarks_to_germplasm.cross context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-309 Add column remarks to germplasm.cross table



ALTER TABLE 
    germplasm.cross
ADD COLUMN 
    remarks TEXT;

COMMENT ON COLUMN 
    germplasm.cross.remarks 
IS 
    'Remarks: Additional information about the germplasm cross [CROSS_REMARKS]';



--rollback ALTER TABLE germplasm.cross DROP COLUMN remarks;
