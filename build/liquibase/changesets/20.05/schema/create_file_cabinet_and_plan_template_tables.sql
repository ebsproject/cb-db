--liquibase formatted sql

--changeset postgres:create_file_cabinet_and_plan_template_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-298 Create file_cabinet and plan_template tables


-- create file_cabinet table
CREATE TABLE operational.file_cabinet (
	id serial NOT NULL,
	folder_name varchar(256) NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	creator_id int4 NOT NULL,
	modification_timestamp timestamp NULL,
	modifier_id int4 NULL,
	remarks text NULL,
	is_void bool NOT NULL DEFAULT false,
	"source" varchar NOT NULL,
	occurrence_id int4 NULL,
	folder_path varchar NULL,
	files varchar NULL,
	transaction_id int4 NOT NULL,
	CONSTRAINT file_cabinet_id_pkey PRIMARY KEY (id)
);

ALTER TABLE operational.file_cabinet ADD CONSTRAINT file_cabinet_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE operational.file_cabinet ADD CONSTRAINT file_cabinet_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE operational.file_cabinet ADD CONSTRAINT file_cabinet_occurrence_id_fkey FOREIGN KEY (occurrence_id) REFERENCES experiment.occurrence(id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN operational.file_cabinet.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN operational.file_cabinet.folder_name IS 'Name of the result folder';
COMMENT ON COLUMN operational.file_cabinet.creation_timestamp IS 'Timestamp when the record was added to the table';


-- create plan_template table
CREATE TABLE platform.plan_template (
	id serial NOT NULL,
	template_name varchar NULL,
	mandatory_info varchar NULL,
	temp_file_cabinet_id int4 NULL,
	randomization_results jsonb NULL,
	randomization_data_results jsonb NULL,
	randomization_input_file jsonb NULL,
	status varchar NULL,
	remarks text NULL,
	creation_timestamp timestamp NULL,
	creator_id int4 NULL,
	modification_timestamp timestamp NULL,
	modifier_id int4 NULL,
	notes text NULL,
	is_void bool NOT NULL DEFAULT false,
	program_id int4 NULL,
	occurrence_applied jsonb NULL,
	design varchar NULL,
	entry_count int4 NULL,
	plot_count int4 NULL,
	check_count int4 NULL,
	repcount int4 NULL,
	entity_type varchar(32) NULL,
	entity_id int4 NULL,
	CONSTRAINT plan_template_id_pkey PRIMARY KEY (id)
);

CREATE INDEX plan_template_entity_id_idx ON platform.plan_template USING btree (entity_id);
CREATE INDEX plan_template_entity_type_idx ON platform.plan_template USING btree (entity_type);

ALTER TABLE platform.plan_template ADD CONSTRAINT plan_template_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE platform.plan_template ADD CONSTRAINT plan_template_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN platform.plan_template.remarks IS 'additional details about the record';
COMMENT ON COLUMN platform.plan_template.creation_timestamp IS 'timestamp when the record was added to the table';
COMMENT ON COLUMN platform.plan_template.creator_id IS 'id of the user who added the record to the table';
COMMENT ON COLUMN platform.plan_template.modification_timestamp IS 'timestamp when the record was last modified';
COMMENT ON COLUMN platform.plan_template.modifier_id IS 'id of the user who last modified the record';
COMMENT ON COLUMN platform.plan_template.notes IS 'additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN platform.plan_template.is_void IS 'indicator whether the record is deleted (true) or not (false)';
COMMENT ON COLUMN platform.plan_template.entity_type IS 'Entity where the plan template is applied to';
COMMENT ON COLUMN platform.plan_template.entity_id IS 'ID of the entity where the plan template is applied to';



--rollback DROP TABLE operational.file_cabinet;

--rollback DROP TABLE platform.plan_template;
