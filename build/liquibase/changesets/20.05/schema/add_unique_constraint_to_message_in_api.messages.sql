--liquibase formatted sql

--changeset postgres:add_unique_constraint_to_message_in_api.messages context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-138 Add unique constraint to message in api.messages



ALTER TABLE
    api.messages
ADD CONSTRAINT 
    messages_message_unq UNIQUE (message);

COMMENT ON CONSTRAINT messages_message_unq ON api.messages
    IS 'API messages can be retrieved easily using its unique message.';



--rollback ALTER TABLE api.messages
--rollback DROP CONSTRAINT messages_message_unq;