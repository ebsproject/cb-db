--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-229 Create function experiment.update_document_column_for_experiment_occurrence



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

            SELECT 
                concat(
                    setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
                    setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
                    setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
                    setweight(to_tsvector(new.occurrence_status), 'CD')
                ) into var_document
            FROM 
                experiment.occurrence eo
            LEFT JOIN 
                experiment.experiment ee
            ON 
                ee.id = eo.experiment_id
            WHERE
                eo.id = NEW.id;
                
        ELSE

            SELECT 
                concat(
                    setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
                    setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
                    setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
                    setweight(to_tsvector(new.occurrence_status), 'CD')
                ) into var_document
            FROM 
                experiment.occurrence eo
            LEFT JOIN 
                experiment.experiment ee
            ON 
                ee.id = new.experiment_id;
                
        END IF;

        new.occurrence_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_occurrence();