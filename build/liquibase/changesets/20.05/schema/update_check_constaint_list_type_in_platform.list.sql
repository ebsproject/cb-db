--liquibase formatted sql

--changeset postgres:update_check_constaint_list_type_in_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-271 Update list_type_chk constraint in platform.list



ALTER TABLE 
    platform.list 
DROP CONSTRAINT 
    list_type_chk;

ALTER TABLE 
    platform.list
ADD CONSTRAINT 
    list_type_chk CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'designation'::text, 'study'::text, 'variable'::text, 'trait_protocol'::text, 'package'::text]));



--rollback ALTER TABLE 
--rollback     platform.list 
--rollback DROP CONSTRAINT 
--rollback     list_type_chk;
--rollback 
--rollback ALTER TABLE 
--rollback     platform.list
--rollback ADD CONSTRAINT 
--rollback     list_type_chk CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'designation'::text, 'study'::text, 'variable'::text, 'trait_protocol'::text]));
