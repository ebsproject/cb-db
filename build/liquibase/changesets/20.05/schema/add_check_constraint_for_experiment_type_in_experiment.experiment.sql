--liquibase formatted sql

--changeset postgres:add_check_constraint_for_experiment_type_in_experiment.experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-305 Add check constraint for experiment_type in experiment.experiment



ALTER TABLE 
    experiment.experiment
ADD CONSTRAINT 
    experiment_experiment_type_chk CHECK (experiment_type::text = ANY (ARRAY['Trial'::text, 'Nursery'::text]));



--rollback ALTER TABLE 
--rollback     experiment.experiment 
--rollback DROP CONSTRAINT 
--rollback     experiment_experiment_type_chk;