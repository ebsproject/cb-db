--liquibase formatted sql

--changeset postgres:create_table_data_terminal.suppression_rule context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-152 Create table data_terminal.suppression_rule



CREATE TABLE data_terminal.suppression_rule (
    id serial NOT NULL,
    transaction_id integer NOT NULL,
    target_variable_id integer NOT NULL,
    factor_variable_id integer NOT NULL,
    conjunction varchar NOT NULL,
    "operator" varchar NOT NULL,
    value varchar NULL,
    status varchar NOT NULL,
    entity character varying, 
    entity_id integer,
    creator_id integer NOT NULL, 
    creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), 
    modifier_id integer,
    modification_timestamp timestamp without time zone, 
    is_void boolean, 
    remarks character varying,
    notes text,	
    CONSTRAINT suppression_rule_id_pkey PRIMARY KEY (id),
    CONSTRAINT suppression_rule_transaction_id_fkey FOREIGN KEY (transaction_id)
      REFERENCES data_terminal.transaction (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT suppression_rule_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE, 
    CONSTRAINT suppression_rule_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)WITH (
  OIDS=FALSE
);
COMMENT ON TABLE data_terminal.suppression_rule
  IS 'Suppression rules applied in the transaction dataset.';

CREATE INDEX suppression_rule_transaction_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (transaction_id);

CREATE INDEX suppression_rule_target_variable_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (target_variable_id);

CREATE INDEX suppression_rule_factor_variable_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (factor_variable_id);

CREATE INDEX suppression_rule_entity_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (entity_id);

CREATE INDEX suppression_rule_creator_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (creator_id);

CREATE INDEX suppression_rule_modifier_id_idx
  ON data_terminal.suppression_rule
  USING btree
  (modifier_id);

CREATE INDEX suppression_rule_is_void_idx
  ON data_terminal.suppression_rule
  USING btree
  (is_void);

SELECT t.* FROM platform.audit_table('data_terminal.suppression_rule') t;



--rollback DROP TABLE data_terminal.suppression_rule;