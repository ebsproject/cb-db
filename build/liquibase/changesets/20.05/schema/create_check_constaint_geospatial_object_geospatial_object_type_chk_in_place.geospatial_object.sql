--liquibase formatted sql

--changeset postgres:create_check_constaint_geospatial_object_geospatial_object_type_chk_in_place.geospatial_object context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-291 Create check constraint geospatial_object_geospatial_object_type_chk in place.geospatial_object



ALTER TABLE 
    place.geospatial_object
ADD CONSTRAINT 
    geospatial_object_geospatial_object_type_chk CHECK (geospatial_object_type::text = ANY (ARRAY['site'::text, 'planting area'::text, 'field'::text]));



--rollback ALTER TABLE 
--rollback     place.geospatial_object
--rollback DROP CONSTRAINT 
--rollback     geospatial_object_geospatial_object_type_chk;