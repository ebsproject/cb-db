--liquibase formatted sql

--changeset postgres:add_column_block_number_to_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-228 Add column block_number to experiment.plot table



ALTER TABLE 
	experiment.plot 
ADD COLUMN 
	block_number integer;
	
COMMENT ON COLUMN 
    experiment.plot.block_number
IS 
	'Block Number: Number of block in the experiment plot [BLOCK_NUMBER]';



--rollback ALTER TABLE experiment.plot
--rollback DROP COLUMN block_number;
