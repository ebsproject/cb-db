--liquibase formatted sql

--changeset postgres:add_column_config_value_to_master.scale_value context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-258 Add config_value column in the scale value table



ALTER TABLE 
	master.scale_value 
ADD COLUMN 
	config_value jsonb;
	
COMMENT ON COLUMN 
    master.scale_value.config_value
IS 
	'Config Value: Support specific attributes of experiment statuses [CONFIG_VALUE]';



--rollback ALTER TABLE master.scale_value
--rollback DROP COLUMN config_value;


