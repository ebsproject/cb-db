--liquibase formatted sql

--changeset postgres:add_column_status_is_active_to_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-271 Add column status and is_active to platform.list



--add column status
ALTER TABLE 
	platform.list 
ADD COLUMN 
	status varchar(32) NOT NULL DEFAULT 'created';
	
COMMENT ON COLUMN 
    platform.list.status
IS 
	'Status: Identified status of a list [STATUS]';

CREATE INDEX
	list_status_idx
ON 
	platform.list
USING 
	btree (status);

COMMENT ON INDEX
	platform.list_status_idx
IS
	'A list can be retrieved by its status';

--add column is_active
ALTER TABLE 
	platform.list 
ADD COLUMN 
	is_active boolean NOT NULL DEFAULT 'true';
	
COMMENT ON COLUMN 
    platform.list.is_active
IS 
	'Is_Active: Whether the list is actively in use or not [IS_ACTIVE]';

CREATE INDEX
	list_is_active_idx
ON 
	platform.list
USING 
	btree (is_active);

COMMENT ON INDEX
	platform.list_is_active_idx
IS
	'A list can be retrieved by its state';



--rollback ALTER TABLE platform.list
--rollback DROP COLUMN status;
--rollback ALTER TABLE platform.list
--rollback DROP COLUMN is_active;