--liquibase formatted sql

--changeset postgres:update_function_experiment.update_document_column_for_experiment_location context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-229 Update function experiment.update_document_column_for_experiment_location



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

            SELECT 
                concat(
                    setweight(to_tsvector(unaccent(new.location_name)),'A'), ' ',
                    setweight(to_tsvector(new.location_code),'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_code FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_code FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
                    setweight(to_tsvector((select program_name from tenant.program where id = new.program_id)),'C'), ' ',
                    setweight(to_tsvector(new.location_year::varchar),'C'), ' ',
                    setweight(to_tsvector((SELECT s.season_code FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
                    setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
                    setweight(to_tsvector((SELECT ts.stage_code FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
                    setweight(to_tsvector(new.location_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = new.creator_id))),'CD'), ' ',
                    setweight(to_tsvector(new.location_status),'CD')
                ) INTO var_document
            FROM 
                experiment.LOCATION el
            WHERE
                el.id = new.id;
                
        ELSE

            SELECT 
                concat(
                    setweight(to_tsvector(unaccent(new.location_name)),'A'), ' ',
                    setweight(to_tsvector(new.location_code),'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_code FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_code FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
                    setweight(to_tsvector((select program_name from tenant.program where id = new.program_id)),'C'), ' ',
                    setweight(to_tsvector(new.location_year::varchar),'C'), ' ',
                    setweight(to_tsvector((SELECT s.season_code FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
                    setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
                    setweight(to_tsvector((SELECT ts.stage_code FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
                    setweight(to_tsvector(new.location_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = new.creator_id))),'CD'), ' ',
                    setweight(to_tsvector(new.location_status),'CD')
                ) INTO var_document
            FROM 
                experiment.LOCATION el;
            
        END IF;

        new.location_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location()
--rollback     RETURNS trigger
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE NOT LEAKPROOF
--rollback AS $BODY$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback             SELECT 
--rollback                 concat(
--rollback                     setweight(to_tsvector(unaccent(new.location_name)),'A'), ' ',
--rollback                     setweight(to_tsvector(new.location_code),'B'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
--rollback                     setweight(to_tsvector((select program_name from tenant.program where id = new.program_id)),'C'), ' ',
--rollback                     setweight(to_tsvector(new.location_year::varchar),'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(new.location_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = new.creator_id))),'CD'), ' ',
--rollback                     setweight(to_tsvector(new.location_status),'CD')
--rollback                 ) INTO var_document
--rollback             FROM 
--rollback                 experiment.LOCATION el
--rollback             WHERE
--rollback                 el.id = new.id;
--rollback                 
--rollback         ELSE
--rollback 
--rollback             SELECT 
--rollback                 concat(
--rollback                     setweight(to_tsvector(unaccent(new.location_name)),'A'), ' ',
--rollback                     setweight(to_tsvector(new.location_code),'B'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = new.geospatial_object_id)))), 'B'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = new.geospatial_object_id))),'B'), ' ',
--rollback                     setweight(to_tsvector((select program_name from tenant.program where id = new.program_id)),'C'), ' ',
--rollback                     setweight(to_tsvector(new.location_year::varchar),'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  new.season_id)),'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = new.stage_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(new.location_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = new.creator_id))),'CD'), ' ',
--rollback                     setweight(to_tsvector(new.location_status),'CD')
--rollback                 ) INTO var_document
--rollback             FROM 
--rollback                 experiment.LOCATION el;
--rollback             
--rollback         END IF;
--rollback 
--rollback         new.location_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $BODY$;