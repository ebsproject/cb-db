--liquibase formatted sql

--changeset postgres:add_column_remarks_to_data_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-299 Add column remarks to data tables



ALTER TABLE experiment.plot_data
ADD COLUMN remarks TEXT;

ALTER TABLE experiment.subplot_data
ADD COLUMN remarks TEXT;

ALTER TABLE experiment.plant_data
ADD COLUMN remarks TEXT;


COMMENT ON COLUMN experiment.plot_data.remarks IS 'Remarks: Additional information about the plot data [PLOTDATA_REMARKS]';
COMMENT ON COLUMN experiment.subplot_data.remarks IS 'Remarks: Additional information about the subplot data [SUBPLOTDATA_REMARKS]';
COMMENT ON COLUMN experiment.plant_data.remarks IS 'Remarks: Additional information about the plant data [PLANTDATA_REMARKS]';



--rollback ALTER TABLE experiment.plot_data DROP COLUMN remarks;

--rollback ALTER TABLE experiment.subplot_data DROP COLUMN remarks;

--rollback ALTER TABLE experiment.plant_data DROP COLUMN remarks;
