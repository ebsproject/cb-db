--liquibase formatted sql

--changeset postgres:update_function_platform.standardize_product_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-327 Update function platform.standardize_product_name



CREATE OR REPLACE FUNCTION platform.standardize_product_name(
	product_name text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
BEGIN
    return platform.normalize_text(product_name);
END; $BODY$;



--rollback CREATE OR REPLACE FUNCTION platform.standardize_product_name(
--rollback 	product_name text)
--rollback     RETURNS text
--rollback     LANGUAGE 'plpgsql'
--rollback 
--rollback     COST 100
--rollback     VOLATILE 
--rollback AS $BODY$
--rollback BEGIN
--rollback     return z_admin.normalize_text(product_name);
--rollback END; $BODY$;