--liquibase formatted sql

--changeset postgres:add_column_is_method_autofilled_to_germplasm.cross context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-306 Add column is_method_autofilled to germplasm.cross



ALTER TABLE 
	germplasm.cross 
ADD COLUMN 
	is_method_autofilled BOOLEAN NOT NULL DEFAULT false;
	
COMMENT ON COLUMN 
    germplasm.cross.is_method_autofilled
IS 
	'Is Method Autofilled: Detect if the cross method has been populated automatically or not [IS_METHOD_AUTOFILLED]';

CREATE INDEX
	cross_is_method_autofilled_idx
ON 
	germplasm.cross
USING 
	btree (is_method_autofilled);



--rollback ALTER TABLE germplasm.cross
--rollback DROP COLUMN is_method_autofilled;
