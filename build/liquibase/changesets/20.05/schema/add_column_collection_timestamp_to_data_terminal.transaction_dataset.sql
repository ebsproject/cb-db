--liquibase formatted sql

--changeset postgres:add_column_collection_timestamp_to_data_terminal.transaction_dataset context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-270 Add column collection_timestamp to data_terminal.transaction_dataset



ALTER TABLE 
	data_terminal.transaction_dataset
ADD COLUMN 
	collection_timestamp timestamp;
	
COMMENT ON COLUMN 
    data_terminal.transaction_dataset.collection_timestamp
IS 
	'Collection Timestamp: Timestamp when the transaction was made [COLLECTION_TIMESTAMP]';



--rollback ALTER TABLE data_terminal.transaction_dataset
--rollback DROP COLUMN collection_timestamp;


