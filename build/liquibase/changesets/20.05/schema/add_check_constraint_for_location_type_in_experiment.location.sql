--liquibase formatted sql

--changeset postgres:add_check_constraint_for_location_type_in_experiment.location context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-305 Add check constraint for location_type in experiment.location



ALTER TABLE 
    experiment.location
ADD CONSTRAINT 
    location_location_type_chk CHECK (location_type::text = ANY (ARRAY['planting area'::text, 'field'::text, 'site'::text]));



--rollback ALTER TABLE 
--rollback     experiment.location 
--rollback DROP CONSTRAINT 
--rollback     location_location_type_chk;