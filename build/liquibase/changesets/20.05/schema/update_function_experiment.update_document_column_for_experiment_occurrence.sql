--liquibase formatted sql

--changeset postgres:update_function_experiment.update_document_column_for_experiment_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-229 Update function experiment.update_document_column_for_experiment_occurrence



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

            SELECT 
                concat(
                    setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
                    setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT s.stage_code FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',   
                    setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_code FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_code FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_code FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
                    setweight(to_tsvector(new.occurrence_status), 'CD')
                ) into var_document
            FROM 
                experiment.occurrence eo
            LEFT JOIN 
                experiment.experiment ee
            ON 
                ee.id = eo.experiment_id
            WHERE
                eo.id = NEW.id;
                
        ELSE

            SELECT 
                concat(
                    setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
                    setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
                    setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
                    setweight(to_tsvector((SELECT s.stage_code FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',   
                    setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_code FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_code FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_code FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
                    setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
                    setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
                    setweight(to_tsvector(new.occurrence_status), 'CD')
                ) into var_document
            FROM 
                experiment.occurrence eo
            LEFT JOIN 
                experiment.experiment ee
            ON 
                ee.id = new.experiment_id;
                
        END IF;

        new.occurrence_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
--rollback     RETURNS trigger
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE NOT LEAKPROOF
--rollback AS $BODY$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback             SELECT 
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
--rollback                     setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
--rollback                     setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
--rollback                     setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
--rollback                     setweight(to_tsvector(new.occurrence_status), 'CD')
--rollback                 ) into var_document
--rollback             FROM 
--rollback                 experiment.occurrence eo
--rollback             LEFT JOIN 
--rollback                 experiment.experiment ee
--rollback             ON 
--rollback                 ee.id = eo.experiment_id
--rollback             WHERE
--rollback                 eo.id = NEW.id;
--rollback                 
--rollback         ELSE
--rollback 
--rollback             SELECT 
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.occurrence_name), 'A'), ' ',
--rollback                     setweight(to_tsvector(new.occurrence_code), 'B'), ' ',
--rollback                     setweight(to_tsvector((SELECT p.project_name FROM tenant.project p WHERE p.id = ee.project_id)), 'B'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_name), 'B'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_code), 'B'), ' ',
--rollback                     setweight(to_tsvector((SELECT s.stage_name FROM tenant.stage s WHERE s.id = ee.stage_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_year::varchar), 'C'), ' ',
--rollback                     setweight(to_tsvector((SELECT s2.season_name FROM tenant.season s2 WHERE s2.id = ee.season_id)), 'C'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT p.geospatial_object_name FROM place.geospatial_object p WHERE p.id in (SELECT pgo.root_geospatial_object_id FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id)))), 'C'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT pgo.geospatial_object_name FROM place.geospatial_object pgo WHERE pgo.id = new.geospatial_object_id))), 'CD'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_design_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(ee.experiment_type), 'CD'), ' ',
--rollback                     setweight(to_tsvector(unaccent((SELECT p2.person_name FROM tenant.person p2 WHERE p2.id =  new.creator_id))), 'CD'), ' ',
--rollback                     setweight(to_tsvector(new.occurrence_status), 'CD')
--rollback                 ) into var_document
--rollback             FROM 
--rollback                 experiment.occurrence eo
--rollback             LEFT JOIN 
--rollback                 experiment.experiment ee
--rollback             ON 
--rollback                 ee.id = new.experiment_id;
--rollback                 
--rollback         END IF;
--rollback 
--rollback         new.occurrence_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $BODY$;