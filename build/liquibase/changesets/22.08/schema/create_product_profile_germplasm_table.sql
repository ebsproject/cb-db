--liquibase formatted sql

--changeset postgres:create_product_profile_germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1357 Create germplasm.product_profile_germplasm table



-- create table
CREATE TABLE germplasm.product_profile_germplasm (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    product_profile_id integer NOT NULL,
    germplasm_id integer NOT NULL,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT product_profile_germplasm_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT product_profile_germplasm_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT product_profile_germplasm_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE germplasm.product_profile_germplasm
    ADD CONSTRAINT product_profile_germplasm_product_profile_id_fk
    FOREIGN KEY (product_profile_id)
    REFERENCES germplasm.product_profile (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT product_profile_germplasm_product_profile_id_fk
    ON germplasm.product_profile_germplasm
    IS 'A product profile can be associated with one or more germplasm.'
;

ALTER TABLE germplasm.product_profile_germplasm
    ADD CONSTRAINT product_profile_germplasm_germplasm_id_fk
    FOREIGN KEY (germplasm_id)
    REFERENCES germplasm.germplasm (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT product_profile_germplasm_germplasm_id_fk
    ON germplasm.product_profile_germplasm
    IS 'A germplasm can be associated to one or more product profiles.'
;

-- table comment
COMMENT ON TABLE germplasm.product_profile_germplasm
    IS 'Product Profile Germplasm: Product profile associated to one or more germplasm [PPGE]';

-- column comments
COMMENT ON COLUMN germplasm.product_profile_germplasm.product_profile_id
    IS 'Product Profile ID: Reference to the product profile linked to the germplasm [PPGE_PRODPROF_ID]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.germplasm_id
    IS 'Germplasm ID: Reference to the germplasm linked to the product profile [PPGE_GE_ID]';

-- audit column comments
COMMENT ON COLUMN germplasm.product_profile_germplasm.id
    IS 'Product Profile Germplasm ID: Database identifier of the record [PPGE_ID]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.remarks
    IS 'Remarks: Additional notes to record [PPGE_REMARKS]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [PPGE_CTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.creator_id
    IS 'Creator ID: Reference to the person who created the record [PPGE_CPERSON]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [PPGE_MTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [PPGE_MPERSON]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.notes
    IS 'Notes: Technical details about the record [PPGE_NOTES]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [PPGE_ISVOID]';
COMMENT ON COLUMN germplasm.product_profile_germplasm.event_log
    IS 'Event Log: Historical transactions of the record [PPGE_EVENTLOG]';

-- indices
-- create index for regular column
CREATE INDEX product_profile_germplasm_product_profile_id_idx
    ON germplasm.product_profile_germplasm USING btree (product_profile_id)
;

-- create index for regular column
CREATE INDEX product_profile_germplasm_germplasm_id_idx
    ON germplasm.product_profile_germplasm USING btree (germplasm_id)
;

ANALYZE germplasm.product_profile_germplasm;

-- audit indices
CREATE INDEX product_profile_germplasm_creator_id_idx ON germplasm.product_profile_germplasm USING btree (creator_id);
CREATE INDEX product_profile_germplasm_modifier_id_idx ON germplasm.product_profile_germplasm USING btree (modifier_id);
CREATE INDEX product_profile_germplasm_is_void_idx ON germplasm.product_profile_germplasm USING btree (is_void);

-- triggers
CREATE TRIGGER product_profile_germplasm_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.product_profile_germplasm FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.product_profile_germplasm;