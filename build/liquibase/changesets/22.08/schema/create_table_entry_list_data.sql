--liquibase formatted sql

--changeset postgres:create_table_entry_list_data context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1253 Create table experiment.entry_list_data



-- create table
CREATE TABLE experiment.entry_list_data (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    entry_list_id integer NOT NULL,
    variable_id integer NOT NULL,
    data_value character varying NOT NULL,
    data_qc_code character varying(8) NOT NULL,
    protocol_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT entry_list_data_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT entry_list_data_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT entry_list_data_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT entry_list_data_entry_list_id_fk FOREIGN KEY (entry_list_id)
        REFERENCES experiment.entry_list (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT entry_list_data_protocol_id_fk FOREIGN KEY (protocol_id)
        REFERENCES tenant.protocol (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT entry_list_data_variable_id_fk FOREIGN KEY (variable_id)
        REFERENCES master.variable (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- table comment
COMMENT ON TABLE experiment.entry_list_data
    IS 'Entry List Data: Additional information about the entry list [ENTLISTDATA]';

-- column comments
COMMENT ON COLUMN experiment.entry_list_data.id
    IS 'Entry List Data ID: Database identifier of the entry list data [ENTLISTDATA_ID]';
COMMENT ON COLUMN experiment.entry_list_data.entry_list_id
    IS 'Entry List ID: Reference to the entry list that has the additional information [ENTLISTDATA_ENTLIST_ID]';
COMMENT ON COLUMN experiment.entry_list_data.variable_id
    IS 'Variable ID: Reference to the variable of the entry list data [ENTLISTDATA_VAR_ID]';
COMMENT ON COLUMN experiment.entry_list_data.data_value
    IS 'Entry List Data Value: Value of the entry list data [ENTLISTDATA_DATAVAL]';
COMMENT ON COLUMN experiment.entry_list_data.data_qc_code
    IS 'Entry List Data QC Code: Status of the entry list data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [ENTLISTDATA_QCCODE]';
COMMENT ON COLUMN experiment.entry_list_data.protocol_id
    IS 'Protocol ID: Reference to the protocol of the entry list [ENTLISTDATA_PROTOCOL_ID]';

-- audit column comments
COMMENT ON COLUMN experiment.entry_list_data.id
    IS 'Entity Name ID: Database identifier of the record [ENTLISTDATA_ID]';
COMMENT ON COLUMN experiment.entry_list_data.remarks
    IS 'Remarks: Additional notes to record [ENTLISTDATA_REMARKS]';
COMMENT ON COLUMN experiment.entry_list_data.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [ENTLISTDATA_CTSTAMP]';
COMMENT ON COLUMN experiment.entry_list_data.creator_id
    IS 'Creator ID: Reference to the person who created the record [ENTLISTDATA_CPERSON]';
COMMENT ON COLUMN experiment.entry_list_data.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [ENTLISTDATA_MTSTAMP]';
COMMENT ON COLUMN experiment.entry_list_data.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [ENTLISTDATA_MPERSON]';
COMMENT ON COLUMN experiment.entry_list_data.notes
    IS 'Notes: Technical details about the record [ENTLISTDATA_NOTES]';
COMMENT ON COLUMN experiment.entry_list_data.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ENTLISTDATA_ISVOID]';
COMMENT ON COLUMN experiment.entry_list_data.event_log
    IS 'Event Log: Historical transactions of the record [ENTLISTDATA_EVENTLOG]';
COMMENT ON CONSTRAINT entry_list_data_id_pk ON experiment.entry_list_data
    IS 'An entry list data is identified by its unique database ID.';
COMMENT ON CONSTRAINT entry_list_data_entry_list_id_fk ON experiment.entry_list_data
    IS 'An entry list data is an attribute of one entry list. An entry list can be attributed with zero or many entry list data.';
COMMENT ON CONSTRAINT entry_list_data_variable_id_fk ON experiment.entry_list_data
    IS 'An entry list data refers to one variable. A variable can be referred by zero or many entry list data.';

-- indices
CREATE INDEX entry_list_data_entry_list_id_data_qc_code_idx ON experiment.entry_list_data USING btree (entry_list_id, data_qc_code);
CREATE INDEX entry_list_data_entry_list_id_idx ON experiment.entry_list_data USING btree (entry_list_id);
CREATE INDEX entry_list_data_entry_list_id_variable_id_data_value_idx ON experiment.entry_list_data USING btree (entry_list_id, variable_id, data_value);
CREATE INDEX entry_list_data_entry_list_id_variable_id_idx ON experiment.entry_list_data USING btree (entry_list_id, variable_id);
CREATE INDEX entry_list_data_variable_id_idx ON experiment.entry_list_data USING btree (variable_id);

COMMENT ON INDEX experiment.entry_list_data_entry_list_id_data_qc_code_idx
    IS 'An entry list data can be retrieved by its data QC code within an entry list.';
COMMENT ON INDEX experiment.entry_list_data_entry_list_id_idx
    IS 'An entry list data can be retrieved by its unique identifier.';
COMMENT ON INDEX experiment.entry_list_data_entry_list_id_variable_id_idx
    IS 'An entry list data can be retrieved by its variable within an entry list.';
COMMENT ON INDEX experiment.entry_list_data_entry_list_id_variable_id_data_value_idx
    IS 'An entry list data can be retrieved by its variable and data value within an entry list.';
COMMENT ON INDEX experiment.entry_list_data_variable_id_idx
    IS 'An entry list data can be retrieved by its variable.';

-- audit indices
CREATE INDEX table_creator_id_idx ON experiment.entry_list_data USING btree (creator_id);
CREATE INDEX table_modifier_id_idx ON experiment.entry_list_data USING btree (modifier_id);
CREATE INDEX table_is_void_idx ON experiment.entry_list_data USING btree (is_void);

-- triggers
CREATE TRIGGER entry_list_data_event_log_tgr BEFORE INSERT OR UPDATE
    ON experiment.entry_list_data FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



--rollback DROP TABLE experiment.entry_list_data;