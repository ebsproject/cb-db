--liquibase formatted sql

--changeset postgres:grant_update_location_permission_to_scheduler context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM pg_roles WHERE rolname = 'scheduler') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1376 Grant update location permission to scheduler



GRANT UPDATE ON TABLE experiment.location TO scheduler;



-- revert changes
--rollback REVOKE UPDATE ON TABLE experiment.location FROM scheduler;
