--liquibase formatted sql

--changeset postgres:create_plants_with_root_lodging_percentage_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Create RLODGINC_CMP_PCT variable



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, display_name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('RLODGINC_CMP_PCT', 'PLANTS WITH ROOT LODGING PCT', 'percentage of plants with root lodging', 'percentage of plants with root lodging', 'float', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'RLODGINC_CMP_PCT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('RLODGINC_CMP_PCT', 'Percentage of Plants with Root Lodging', 'Percentage of Plants with Root Lodging') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('RLODGINC_CMP_PCT_METHOD', 'percentage of plants with root lodging method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('RLODGINC_CMP_PCT_SCALE', 'PLANTS WITH ROOT LODGING PCT scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'RLODGINC_CMP_PCT'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'RLODGINC_CMP_PCT'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'RLODGINC_CMP_PCT'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'RLODGINC_CMP_PCT'
--rollback ;



--changeset postgres:create_plants_with_root_lodging_percentage_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Create RLODGINC_CMP_PCT formula



INSERT INTO master.formula
    (formula, result_variable_id, method_id, data_level, function_name, database_formula, decimal_place)
SELECT
    'RLODGINC_CMP_PCT = (RLODGINC_CT_PLNTPLOT/(PSTANDHV_CT_PLNTPLOT or PSTANDTH_CT_PLNTPLOT))*100' AS formula,
    var.id AS result_variable_id,
    var.method_id,
    'plot' AS data_level,
    'master.formula_rlodginc_cmp_pct(rlodginc_ct_plntplot, pstandhv_ct_plntplot, pstandth_ct_plntplot)' AS function_name,
    $$
CREATE OR REPLACE FUNCTION
    master.formula_rlodginc_cmp_pct(
        rlodginc_ct_plntplot integer,
        pstandhv_ct_plntplot integer,
        pstandth_ct_plntplot integer
    )
    RETURNS double precision
LANGUAGE plpgsql
AS $function$
DECLARE
    rlodginc_cmp_pct float;
    local_plant_stand_count int;
BEGIN
    IF pstandhv_ct_plntplot IS NULL THEN
        local_plant_stand_count = pstandth_ct_plntplot;
    ELSE
        local_plant_stand_count = pstandhv_ct_plntplot;
    END IF;
    
    IF local_plant_stand_count IS NULL OR local_plant_stand_count = 0 THEN
        rlodginc_cmp_pct = 0;
    ELSE
        rlodginc_cmp_pct = rlodginc_ct_plntplot/local_plant_stand_count::float;
    END IF;
    
    RETURN round(rlodginc_cmp_pct::numeric * 100, 2);
END $function$;
    $$ AS database_formula,
    2 AS decimal_place
FROM
    master.variable AS var
WHERE
    var.abbrev = 'RLODGINC_CMP_PCT'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.formula AS form
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.id = form.result_variable_id
--rollback     AND var.abbrev = 'RLODGINC_CMP_PCT'
--rollback ;



--changeset postgres:create_plants_with_root_lodging_percentage_formula_parameters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Create RLODGINC_CMP_PCT formula parameters



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('RLODGINC_CT_PLNTPLOT', 'PSTANDHV_CT_PLNTPLOT', 'PSTANDTH_CT_PLNTPLOT')
    AND var.abbrev = 'RLODGINC_CMP_PCT'
ORDER BY
    pvar.abbrev = 'RLODGINC_CT_PLNTPLOT' DESC,
    pvar.abbrev = 'PSTANDHV_CT_PLNTPLOT' DESC,
    pvar.abbrev = 'PSTANDTH_CT_PLNTPLOT' DESC
;



-- revert changes
--rollback DELETE FROM
--rollback     master.formula_parameter AS fparam
--rollback USING
--rollback     master.variable AS var,
--rollback     master.formula AS form
--rollback WHERE
--rollback     var.id = form.result_variable_id
--rollback     AND form.id = fparam.formula_id
--rollback     AND var.abbrev = 'RLODGINC_CMP_PCT'
--rollback ;



--changeset postgres:create_plants_with_root_lodging_percentage_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Create master.formula_rlodginc_cmp_pct() function



CREATE OR REPLACE FUNCTION
    master.formula_rlodginc_cmp_pct(
        rlodginc_ct_plntplot integer,
        pstandhv_ct_plntplot integer,
        pstandth_ct_plntplot integer
    )
    RETURNS double precision
LANGUAGE plpgsql
AS $function$
DECLARE
    rlodginc_cmp_pct float;
    local_plant_stand_count int;
BEGIN
    IF pstandhv_ct_plntplot IS NULL THEN
        local_plant_stand_count = pstandth_ct_plntplot;
    ELSE
        local_plant_stand_count = pstandhv_ct_plntplot;
    END IF;
    
    IF local_plant_stand_count IS NULL OR local_plant_stand_count = 0 THEN
        rlodginc_cmp_pct = 0;
    ELSE
        rlodginc_cmp_pct = rlodginc_ct_plntplot/local_plant_stand_count::float;
    END IF;
    
    RETURN round(rlodginc_cmp_pct::numeric * 100, 2);
END $function$;



-- revert changes
--rollback DROP FUNCTION master.formula_rlodginc_cmp_pct(int, int, int);



--changeset postgres:update_root_lodging_percentage_label context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Update root lodging percentage label



UPDATE
    master.variable AS var
SET
    label = t.LABEL
FROM (
        VALUES
        ('RLODGINC_CMP_PCT', 'RLODGINC CMP PCT')
    ) AS t (
        abbrev, label
    )
WHERE
    var.abbrev = t.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     label = t.LABEL
--rollback FROM (
--rollback         VALUES
--rollback         ('RLODGINC_CMP_PCT', 'PLANTS WITH ROOT LODGING PCT')
--rollback     ) AS t (
--rollback         abbrev, label
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;
