--liquibase formatted sql

--changeset postgres:populate_opv_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1360 Populate OPV germplasm



INSERT INTO
    germplasm.germplasm 
        (
            designation,
            parentage,
            generation,
            germplasm_type,
            germplasm_state,
            germplasm_name_type,
            germplasm_normalized_name,
            crop_id,
            taxonomy_id,
            creator_id
        )
SELECT
    g.designation,
    g.parentage,
    g.generation,
    g.germplasm_type,
    g.germplasm_state,
    g.germplasm_name_type,
    platform.normalize_text(g.designation),
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
            ('MPKE18Y01','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18Y02','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18Y03','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18Y04','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18Y05','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE20Y01','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE20Y02','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE20Y03','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE20Y04','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE20Y05','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18W04','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18W06','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18W07','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18W08','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE18W09','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE21W01','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE21W02','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE21W03','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE21W04','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name'),
            ('MPKE21W05','UNKNOWN','UNKNOWN','OPV','not_fixed','cultivar_name')
    ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type
    )
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm
--rollback WHERE 
--rollback     designation 
--rollback IN 
--rollback     (
--rollback          'MPKE18Y01',
--rollback          'MPKE18Y02',
--rollback          'MPKE18Y03',
--rollback          'MPKE18Y04',
--rollback          'MPKE18Y05',
--rollback          'MPKE20Y01',
--rollback          'MPKE20Y02',
--rollback          'MPKE20Y03',
--rollback          'MPKE20Y04',
--rollback          'MPKE20Y05',
--rollback          'MPKE18W04',
--rollback          'MPKE18W06',
--rollback          'MPKE18W07',
--rollback          'MPKE18W08',
--rollback          'MPKE18W09',
--rollback          'MPKE21W01',
--rollback          'MPKE21W02',
--rollback          'MPKE21W03',
--rollback          'MPKE21W04',
--rollback          'MPKE21W05'
--rollback     )
--rollback ;



--changeset postgres:populate_opv_germplasm_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1360 Populate OPV germplasm name



INSERT INTO
    germplasm.germplasm_name 
        (
            germplasm_id, 
            name_value, 
            germplasm_name_type, 
            germplasm_name_status, 
            germplasm_normalized_name, 
            creator_id
        )
SELECT 
    gg.id,
    gg.designation,
    gg.germplasm_name_type,
    'active',
    platform.normalize_text(gg.designation),
    gg.creator_id
FROM
    germplasm.germplasm gg
WHERE
    designation 
IN
    (
        'MPKE18Y01',
        'MPKE18Y02',
        'MPKE18Y03',
        'MPKE18Y04',
        'MPKE18Y05',
        'MPKE20Y01',
        'MPKE20Y02',
        'MPKE20Y03',
        'MPKE20Y04',
        'MPKE20Y05',
        'MPKE18W04',
        'MPKE18W06',
        'MPKE18W07',
        'MPKE18W08',
        'MPKE18W09',
        'MPKE21W01',
        'MPKE21W02',
        'MPKE21W03',
        'MPKE21W04',
        'MPKE21W05'
    )
ORDER BY
    gg.id 
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm_name
--rollback WHERE
--rollback     germplasm_normalized_name
--rollback IN
--rollback     (
--rollback          'MPKE18Y01',
--rollback          'MPKE18Y02',
--rollback          'MPKE18Y03',
--rollback          'MPKE18Y04',
--rollback          'MPKE18Y05',
--rollback          'MPKE20Y01',
--rollback          'MPKE20Y02',
--rollback          'MPKE20Y03',
--rollback          'MPKE20Y04',
--rollback          'MPKE20Y05',
--rollback          'MPKE18W04',
--rollback          'MPKE18W06',
--rollback          'MPKE18W07',
--rollback          'MPKE18W08',
--rollback          'MPKE18W09',
--rollback          'MPKE21W01',
--rollback          'MPKE21W02',
--rollback          'MPKE21W03',
--rollback          'MPKE21W04',
--rollback          'MPKE21W05'
--rollback     )
--rollback ;



--changeset postgres:populate_opv_seed context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1360 Populate OPV seed



INSERT INTO 
    germplasm.seed 
        (
            seed_code, 
            seed_name, 
            germplasm_id,
            program_id,
            creator_id
        )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    -- concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name,
    t.seed_name,
    g.id AS germplasm_id,
    tp.id program_id,
    g.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
        VALUES
            ('MPKE18Y01','KB17B-N01-12'),
            ('MPKE18Y02','KB17B-N01-16'),
            ('MPKE18Y03','KB17B-N01-18'),
            ('MPKE18Y04','KB17B-N01-20'),
            ('MPKE18Y05','KB17B-N01-22'),
            ('MPKE20Y01','KB19B-NOPV-4'),
            ('MPKE20Y02','KB19B-NOPV-6'),
            ('MPKE20Y03','KB19B-NOPV-8'),
            ('MPKE20Y04','KB19B-NOPV-10'),
            ('MPKE20Y05','KB19B-NOPV-12'),
            ('MPKE18W04','KB18A-POP1-2'),
            ('MPKE18W06','KB18A-POP1-4'),
            ('MPKE18W07','KB18A-POP1-6'),
            ('MPKE18W08','KB18A-POP1-8'),
            ('MPKE18W09','KB18A-POP1-10'),
            ('MPKE21W01','KB20B-N12-18'),
            ('MPKE21W02','KB20B-N12-20'),
            ('MPKE21W03','KB20B-N12-22'),
            ('MPKE21W04','KB20B-N12-24'),
            ('MPKE21W05','KB20B-N12-26')
    ) AS t (
        designation, seed_name
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'DW'
;



--rollback DELETE FROM
--rollback 	germplasm.seed
--rollback WHERE seed_name IN (
--rollback    SELECT
--rollback 	     --concat(t.designation,'-',g.id,'-',ROW_NUMBER() OVER ()) AS seed_name
--ROLLBACK       t.seed_name
--rollback    FROM
--rollback       germplasm.germplasm AS g
--rollback    INNER JOIN (
--rollback         VALUES
--rollback              ('MPKE18Y01','KB17B-N01-12'),
--rollback              ('MPKE18Y02','KB17B-N01-16'),
--rollback              ('MPKE18Y03','KB17B-N01-18'),
--rollback              ('MPKE18Y04','KB17B-N01-20'),
--rollback              ('MPKE18Y05','KB17B-N01-22'),
--rollback              ('MPKE20Y01','KB19B-NOPV-4'),
--rollback              ('MPKE20Y02','KB19B-NOPV-6'),
--rollback              ('MPKE20Y03','KB19B-NOPV-8'),
--rollback              ('MPKE20Y04','KB19B-NOPV-10'),
--rollback              ('MPKE20Y05','KB19B-NOPV-12'),
--rollback              ('MPKE18W04','KB18A-POP1-2'),
--rollback              ('MPKE18W06','KB18A-POP1-4'),
--rollback              ('MPKE18W07','KB18A-POP1-6'),
--rollback              ('MPKE18W08','KB18A-POP1-8'),
--rollback              ('MPKE18W09','KB18A-POP1-10'),
--rollback              ('MPKE21W01','KB20B-N12-18'),
--rollback              ('MPKE21W02','KB20B-N12-20'),
--rollback              ('MPKE21W03','KB20B-N12-22'),
--rollback              ('MPKE21W04','KB20B-N12-24'),
--rollback              ('MPKE21W05','KB20B-N12-26')
--rollback     ) AS t (
--rollback         designation, seed_name
--rollback     )
--rollback         ON g.germplasm_normalized_name = t.designation
--rollback );



--changeset postgres:populate_opv_package context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1360 Populate OPV package



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id)
SELECT
     germplasm.generate_code('package') AS package_code,
     t.package_label AS package_label,
     0 AS package_quantity,
     'g' AS package_unit,
     'active' AS package_status,
     gs.id AS seed_id,
     gs.program_id,
     gs.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
        VALUES
            ('MPKE18Y01','KE17B-001-0012'),
            ('MPKE18Y02','KE17B-001-0016'),
            ('MPKE18Y03','KE17B-001-0018'),
            ('MPKE18Y04','KE17B-001-0020'),
            ('MPKE18Y05','KE17B-001-0022'),
            ('MPKE20Y01','KE19B-012-0004'),
            ('MPKE20Y02','KE19B-012-0006'),
            ('MPKE20Y03','KE19B-012-0008'),
            ('MPKE20Y04','KE19B-012-0010'),
            ('MPKE20Y05','KE19B-012-0012'),
            ('MPKE18W04','KE18A-028-0002'),
            ('MPKE18W06','KE18A-028-0004'),
            ('MPKE18W07','KE18A-028-0006'),
            ('MPKE18W08','KE18A-028-0008'),
            ('MPKE18W09','KE18A-028-0010'),
            ('MPKE21W01','KE20B-008-0018'),
            ('MPKE21W02','KE20B-008-0020'),
            ('MPKE21W03','KE20B-008-0022'),
            ('MPKE21W04','KE20B-008-0024'),
            ('MPKE21W05','KE20B-008-0026')
    ) AS t (
        designation, package_label
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'KE'
    INNER JOIN
        germplasm.seed gs
    ON
        gs.germplasm_id = g.id
;
    


--rollback DELETE FROM
--rollback 	germplasm.package
--rollback WHERE package_label IN (
--rollback      'KE17B-001-0012',
--rollback      'KE17B-001-0016',
--rollback      'KE17B-001-0018',
--rollback      'KE17B-001-0020',
--rollback      'KE17B-001-0022',
--rollback      'KE19B-012-0004',
--rollback      'KE19B-012-0006',
--rollback      'KE19B-012-0008',
--rollback      'KE19B-012-0010',
--rollback      'KE19B-012-0012',
--rollback      'KE18A-028-0002',
--rollback      'KE18A-028-0004',
--rollback      'KE18A-028-0006',
--rollback      'KE18A-028-0008',
--rollback      'KE18A-028-0010',
--rollback      'KE20B-008-0018',
--rollback      'KE20B-008-0020',
--rollback      'KE20B-008-0022',
--rollback      'KE20B-008-0024',
--rollback      'KE20B-008-0026'
--rollback );