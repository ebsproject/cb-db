--liquibase formatted sql

--changeset postgres:update_entry_list_code_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1362 Update entry_list_code values



-- drop unique constraint
ALTER TABLE experiment.entry_list
    DROP CONSTRAINT entry_list_entry_list_code_unq
;

-- reset sequence
ALTER SEQUENCE experiment.entry_list_code_seq RESTART;

-- update entry_list_code in order
WITH t_entlist AS (
    SELECT
        entlist.id
    FROM
        experiment.entry_list AS entlist
    ORDER BY
        entlist.id ASC
)
UPDATE
    experiment.entry_list AS entlist
SET
    entry_list_code = experiment.generate_code('entry_list')
FROM
    t_entlist AS t
WHERE
    entlist.id = t.id
;

-- reinstate unique constraint
CREATE UNIQUE INDEX entry_list_entry_list_code_uidx
    ON experiment.entry_list (entry_list_code)
;

ALTER TABLE experiment.entry_list
    ADD CONSTRAINT entry_list_entry_list_code_unq
    UNIQUE USING INDEX entry_list_entry_list_code_uidx
;



-- revert changes
--rollback SELECT NULL
