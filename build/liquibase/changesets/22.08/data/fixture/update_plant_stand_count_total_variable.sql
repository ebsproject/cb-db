--liquibase formatted sql

--changeset postgres:update_pstand_ct_plntplot_total_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Update PSTAND_CT_PLNTPLOT_TOTAL variable



UPDATE
    master.variable
SET
    abbrev = 'RLODGINC_CT_PLNTPLOT',
    label = 'TOTAL PLANTS WITH ROOT LODGING',
    name = 'total number of plants with root lodging',
    display_name = 'total number of plants with root lodging'
WHERE
    abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL',
--rollback     label = 'PLANT STAND COUNT TOTAL',
--rollback     name = 'plant stand count total',
--rollback     display_name = 'plant stand count total'
--rollback WHERE
--rollback     abbrev = 'RLODGINC_CT_PLNTPLOT'
--rollback ;



--changeset postgres:update_pstandhv_ct_plntplot_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Update PSTANDHV_CT_PLNTPLOT variable



UPDATE
    master.variable
SET
    display_name = 'plant stand count at harvest'
WHERE
    abbrev = 'PSTANDHV_CT_PLNTPLOT'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     display_name = NULL
--rollback WHERE
--rollback     abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback ;



--changeset postgres:update_pstandth_ct_plntplot_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Update PSTANDTH_CT_PLNTPLOT variable



UPDATE
    master.variable
SET
    display_name = 'plant stand count at thinning'
WHERE
    abbrev = 'PSTANDTH_CT_PLNTPLOT'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     display_name = NULL
--rollback WHERE
--rollback     abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback ;



--changeset postgres:update_plant_stand_variable_labels context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Update plant stand variable labels



UPDATE
    master.variable AS var
SET
    label = t.LABEL
FROM (
        VALUES
        ('RLODGINC_CT_PLNTPLOT', 'RLODGINC CT PLNTPLOT'),
        ('PSTANDHV_CT_PLNTPLOT', 'PSTANDHV CT PLNTPLOT'),
        ('PSTANDTH_CT_PLNTPLOT', 'PSTANDTH CT PLNTPLOT')
    ) AS t (
        abbrev, label
    )
WHERE
    var.abbrev = t.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     label = t.LABEL
--rollback FROM (
--rollback         VALUES
--rollback         ('RLODGINC_CT_PLNTPLOT', 'TOTAL PLANTS WITH ROOT LODGING'),
--rollback         ('PSTANDHV_CT_PLNTPLOT', 'PLANT STAND COUNT AT HARVEST'),
--rollback         ('PSTANDTH_CT_PLNTPLOT', 'PLANT STAND COUNT AT THINNING')
--rollback     ) AS t (
--rollback         abbrev, label
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;
