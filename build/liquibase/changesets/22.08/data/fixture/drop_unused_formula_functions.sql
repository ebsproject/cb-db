--liquibase formatted sql

--changeset postgres:drop_unused_formula_functions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1377 Drop unused formula functions



DROP FUNCTION IF EXISTS master.formula_adjayld_g_cont;
DROP FUNCTION IF EXISTS master.formula_adjayld_kg_cont;
DROP FUNCTION IF EXISTS master.formula_adjayld_kg_cont3;
DROP FUNCTION IF EXISTS master.formula_adjayld_kg_cont4;
DROP FUNCTION IF EXISTS master.formula_adjayld_kg_cont5;
DROP FUNCTION IF EXISTS master.formula_adjyld3_cont;
DROP FUNCTION IF EXISTS master.formula_area_fact;
DROP FUNCTION IF EXISTS master.formula_cml_cont;
DROP FUNCTION IF EXISTS master.formula_disease_index;
DROP FUNCTION IF EXISTS master.formula_dth_cont;
DROP FUNCTION IF EXISTS master.formula_ht1_cont;
DROP FUNCTION IF EXISTS master.formula_ht2_cont;
DROP FUNCTION IF EXISTS master.formula_ht3_cont;
DROP FUNCTION IF EXISTS master.formula_ht4_cont;
DROP FUNCTION IF EXISTS master.formula_ht5_cont;
DROP FUNCTION IF EXISTS master.formula_ht6_cont;
DROP FUNCTION IF EXISTS master.formula_hv_area_sqm;
DROP FUNCTION IF EXISTS master.formula_mat_cont;
DROP FUNCTION IF EXISTS master.formula_mat_cont2;
DROP FUNCTION IF EXISTS master.formula_mf_cont;
DROP FUNCTION IF EXISTS master.formula_mf_harv_cont;
DROP FUNCTION IF EXISTS master.formula_miss_hill_cont;
DROP FUNCTION IF EXISTS master.formula_panno_cont;
DROP FUNCTION IF EXISTS master.formula_pnl_cont;
DROP FUNCTION IF EXISTS master.formula_til_ave_cont;
DROP FUNCTION IF EXISTS master.formula_total_hill_cont;
DROP FUNCTION IF EXISTS master.formula_total_plant_tested;
DROP FUNCTION IF EXISTS master.formula_tplyld_cont_ps;
DROP FUNCTION IF EXISTS master.formula_yld_0_cont1;
DROP FUNCTION IF EXISTS master.formula_yld_0_cont2;
DROP FUNCTION IF EXISTS master.formula_yld_cont1;
DROP FUNCTION IF EXISTS master.formula_yld_cont2;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton2;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton3;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton5;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton6;
DROP FUNCTION IF EXISTS master.formula_yld_cont_ton7;



-- revert changes
--rollback SELECT NULL;
