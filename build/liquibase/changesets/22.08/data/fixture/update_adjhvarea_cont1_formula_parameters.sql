--liquibase formatted sql

--changeset postgres:update_adjhvarea_cont1_formula_parameters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1377 Update ADJHVAREA_CONT1 formula parameters



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('HVHILL_CONT', 'TOTAL_HILL_CONT')
    AND var.abbrev = 'ADJHVAREA_CONT1'
ORDER BY
    pvar.abbrev
;



--rollback DELETE FROM
--rollback     master.formula_parameter AS fparam
--rollback USING
--rollback     master.variable AS var,
--rollback     master.formula AS form,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     var.id = form.result_variable_id
--rollback     AND form.id = fparam.formula_id
--rollback     AND var.abbrev = 'ADJHVAREA_CONT1'
--rollback     AND fparam.param_variable_id = pvar.id
--rollback     AND pvar.abbrev IN ('HVHILL_CONT', 'TOTAL_HILL_CONT')
--rollback ;
