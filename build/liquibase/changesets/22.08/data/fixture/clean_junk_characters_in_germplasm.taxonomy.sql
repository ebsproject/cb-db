--liquibase formatted sql

--changeset postgres:clean_junk_characters_in_germplasm.taxonomy context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1368 Clean junk characters in germplasm.taxonomy



UPDATE 
    germplasm.taxonomy AS t1
SET
    taxonomy_name = t2.new_name
FROM 
    (
        VALUES
        ('Zea mays L. subsp. mays', 'Zea mays L. subsp. mays'),
        ('× Triticosecale Wittm. ex A. Camus.', 'Triticosecale Wittm. ex A. Camus.')
    ) AS t2 (old_name, new_name) 
WHERE 
    t1.taxonomy_name = t2.old_name
;



--rollback SELECT NULL;