--liquibase formatted sql

--changeset postgres:drop_affected_plants_percentage_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Drop master.formula_affected_plants_percentage function



DROP FUNCTION master.formula_affected_plants_percentage(int, int, int);



-- revert changes
--rollback CREATE OR REPLACE FUNCTION master.formula_affected_plants_percentage(
--rollback     pstandhv_ct_plntplot int,
--rollback     pstandth_ct_plntplot int,
--rollback     pstand_ct_plntplot_total int
--rollback ) returns float as
--rollback $body$
--rollback declare
--rollback     affected_plants_percentage float;
--rollback     local_plant_stand_count int;
--rollback BEGIN
--rollback     IF pstandhv_ct_plntplot IS NULL THEN
--rollback         local_plant_stand_count = pstandth_ct_plntplot;
--rollback     ELSE
--rollback         local_plant_stand_count = pstandhv_ct_plntplot;
--rollback     END IF;
--rollback     
--rollback     IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
--rollback         affected_plants_percentage = 0;
--rollback     ELSE
--rollback         affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
--rollback     END IF;
--rollback     
--rollback     return round(affected_plants_percentage::numeric * 100, 2);
--rollback end
--rollback $body$
--rollback language plpgsql;



--changeset postgres:delete_affected_plants_percentage_formula_parameters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Delete AFFECTED_PLANTS_PERCENTAGE formula parameters



DELETE FROM
    master.formula_parameter AS fparam
USING
    master.variable AS var,
    master.formula AS form
WHERE
    var.id = form.result_variable_id
    AND form.id = fparam.formula_id
    AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
;



-- revert changes
--rollback INSERT INTO master.formula_parameter
--rollback     (formula_id, param_variable_id, data_level, result_variable_id)
--rollback SELECT
--rollback     form.id AS formula_id,
--rollback     pvar.id AS param_variable_id,
--rollback     pvar.data_level,
--rollback     form.result_variable_id
--rollback FROM
--rollback     master.formula AS form
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.id = form.result_variable_id,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     pvar.abbrev IN ('PSTAND_CT_PLNTPLOT_TOTAL', 'PSTANDHV_CT_PLNTPLOT', 'PSTANDTH_CT_PLNTPLOT')
--rollback     AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback ;



--changeset postgres:delete_affected_plants_percentage_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1340 Delete AFFECTED_PLANTS_PERCENTAGE formula



DELETE FROM
    master.formula AS form
USING
    master.variable AS var
WHERE
    var.id = form.result_variable_id
    AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
;



-- revert changes
--rollback INSERT INTO master.formula
--rollback     (formula, result_variable_id, method_id, data_level, function_name, database_formula, decimal_place)
--rollback SELECT
--rollback     'AFFECTED_PLANTS_PERCENTAGE = (PSTAND_CT_PLNTPLOT_TOTAL / (PSTANDHV_CT_PLNTPLOT OR PSTANDTH_CT_PLNTPLOT))::float' AS formula,
--rollback     var.id AS result_variable_id,
--rollback     var.method_id,
--rollback     'plot' AS data_level,
--rollback     'master.formula_affected_plants_percentage(pstand_ct_plntplot_total, pstandhv_ct_plntplot, pstandth_ct_plntplot)' AS function_name,
--rollback     $$
--rollback create or replace function master.formula_affected_plants_percentage(
--rollback     pstand_ct_plntplot_total int,
--rollback     pstandhv_ct_plntplot int,
--rollback     pstandth_ct_plntplot int
--rollback ) returns float as
--rollback $body$
--rollback declare
--rollback     affected_plants_percentage float;
--rollback     local_plant_stand_count int;
--rollback BEGIN
--rollback     IF pstandhv_ct_plntplot IS NULL THEN
--rollback         local_plant_stand_count = pstandth_ct_plntplot;
--rollback     ELSE
--rollback         local_plant_stand_count = pstandhv_ct_plntplot;
--rollback     END IF;
--rollback     
--rollback     IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
--rollback         affected_plants_percentage = 0;
--rollback     ELSE
--rollback         affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
--rollback     END IF;
--rollback     
--rollback     return round(affected_plants_percentage::numeric, 2);
--rollback end
--rollback $body$
--rollback language plpgsql;
--rollback     $$ AS database_formula,
--rollback     2 AS decimal_place
--rollback FROM
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback ;



--changeset postgres:delete_affected_plants_percentage_variable context:fixture splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM experiment.plot_data AS pd INNER JOIN master.variable AS var ON var.id = pd.variable_id WHERE var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1340 Delete AFFECTED_PLANTS_PERCENTAGE variable



DELETE FROM
    master.scale_value AS t
USING
    master.variable AS var
WHERE
    var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
    AND t.scale_id = var.scale_id
;

DELETE FROM
    master.scale AS t
USING
    master.variable AS var
WHERE
    var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
    AND t.id = var.scale_id
;

DELETE FROM
    master.method AS t
USING
    master.variable AS var
WHERE
    var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
    AND t.id = var.method_id
;

DELETE FROM
    master.variable AS t
WHERE
    t.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
;



-- revert changes
--rollback DO $$
--rollback DECLARE
--rollback     var_property_id int;
--rollback     var_method_id int;
--rollback     var_scale_id int;
--rollback     var_variable_id int;
--rollback BEGIN
--rollback     -- variable
--rollback     INSERT INTO
--rollback         master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
--rollback     VALUES 
--rollback         ('AFFECTED_PLANTS_PERCENTAGE', 'AFFECTED PLANTS PERCENTAGE', 'percentage of affected plants', 'float', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
--rollback     RETURNING id INTO var_variable_id;
--rollback 
--rollback     -- property
--rollback     SELECT id FROM master."property" WHERE abbrev = 'AFFECTED_PLANTS_PERCENTAGE' INTO var_property_id;
--rollback     IF var_property_id IS NULL THEN
--rollback         INSERT INTO
--rollback             master."property" (abbrev, name, display_name)
--rollback         VALUES
--rollback             ('AFFECTED_PLANTS_PERCENTAGE', 'Affected Plants Percentage', 'Affected Plants Percentage') 
--rollback         RETURNING id INTO var_property_id;
--rollback     END IF;
--rollback 
--rollback     -- method
--rollback     INSERT INTO
--rollback         master.method (abbrev, name) 
--rollback     VALUES
--rollback         ('AFFECTED_PLANTS_PERCENTAGE_METHOD', 'percentage of affected plants method')
--rollback     RETURNING id INTO var_method_id;
--rollback 
--rollback     -- scale
--rollback     INSERT INTO
--rollback         master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
--rollback     VALUES
--rollback         ('AFFECTED_PLANTS_PERCENTAGE_SCALE', 'AFFECTED PLANTS PERCENTAGE scale', NULL, NULL, NULL, NULL)
--rollback     RETURNING id INTO var_scale_id;
--rollback 
--rollback     -- update references
--rollback     UPDATE
--rollback         master.variable
--rollback     SET
--rollback         property_id = var_property_id,
--rollback         method_id = var_method_id,
--rollback         scale_id = var_scale_id
--rollback     WHERE
--rollback         id = var_variable_id
--rollback     ;
--rollback END; $$;
