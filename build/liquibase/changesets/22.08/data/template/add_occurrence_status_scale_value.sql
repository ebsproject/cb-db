--liquibase formatted sql

--changeset postgres:add_occurrence_status_scale_value context:template splitStatements:false rollbackSplitStatements:false
----preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev = 'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1373 Add planted;trait data collected scale value to OCCURRENCE_STATUS variable



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'OCCURRENCE_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('planted;trait data collected', 'Planted; Trait Data Collected', 'Planted; Trait Data Collected', 'PLANTED_TRAIT_DATA_COLLECTED', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED'
--rollback     )
--rollback ;