--liquibase formatted sql

--changeset postgres:update_hm_germplasm_name_config_rice_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3874 HM DB: Update configuration records for RICE Selfing-not_fixed-plant-specific use case


-- update germplasm name config
UPDATE platform.config
SET
    config_value = '{
        "PLOT": {
            "default": {
                "fixed": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    },
                    "fixed_line": {
                        "single plant seed increase": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                },
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                },
                "not_fixed": {
                    "default": {
                        "bulk": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B",
                                "order_number": 2
                            }
                        ],
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ],
                        "plant-specific": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "field-repeater",
                                "entity": "harvestData",
                                "field_name": "specific_plant",
                                "minimum": "2",
                                "delimiter": "^",
                                "order_number": 2
                            }
                        ],
                        "panicle selection": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ],
                        "single seed descent": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B RGA",
                                "order_number": 2
                            }
                        ],
                        "single plant selection": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "bulk": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "transgenesis": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "max_digits": 3,
                                "leading_zero": "yes",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "genome editing": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "max_digits": 3,
                                "leading_zero": "yes",
                                "order_number": 2
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }'
WHERE
    abbrev = 'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT'



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "fixed": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR XXXXX",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     },
--rollback                     "fixed_line": {
--rollback                         "single plant seed increase": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 },
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR XXXXX",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 },
--rollback                 "not_fixed": {
--rollback                     "default": {
--rollback                         "bulk": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "B",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ],
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR XXXXX",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "plant-specific": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ],
--rollback                         "panicle selection": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ],
--rollback                         "single seed descent": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "B RGA",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ],
--rollback                         "single plant selection": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "bulk": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "transgenesis": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "max_digits": 3,
--rollback                                 "leading_zero": "yes",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "genome editing": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "max_digits": 3,
--rollback                                 "leading_zero": "yes",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state": {
--rollback                     "germplasm_type": {
--rollback                         "harvest_method": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "ABC",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "<entity>",
--rollback                                 "field_name": "<field_name>",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "<schema>",
--rollback                                 "order_number": 4,
--rollback                                 "sequence_name": "<sequence_name>"
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     }'
--rollback WHERE
--rollback     abbrev = 'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT'