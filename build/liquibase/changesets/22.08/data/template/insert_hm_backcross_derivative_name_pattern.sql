--liquibase formatted sql

--changeset postgres:insert_hm_backcross_derivative_name_pattern context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3381 - Add new name config for backcross derivative names in RICE



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_GERMPLASM_BC_DERIV_RICE_DEFAULT',
        'Harvest Manager Rice Germplasm - Backcross Derivative Name Pattern-Default',
$$
{
    "CROSS": {
        "single cross": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ],
                    "bulk": {
                        "with_project_code": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "projectCode",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B",
                                "order_number": 2
                            }
                        ],
                        "without_project_code": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B",
                                "order_number": 2
                            }
                        ]
                    },
                    "single seed numbering": {
                        "with_project_code": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "projectCode",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ],
                        "without_project_code": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                }
            }
        },
        "backcross": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "nonRecurrentParentGermplasm",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        }
                    ],
                    "single seed numbering": [
                        {
                            "type": "field",
                            "entity": "nonRecurrentParentGermplasm",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": ":",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            }
        },
        "default": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            }
        }
    },
    "PLOT": {
        "default": {
            "not_fixed": {
                "default": {
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        }
                    ],
                    "single seed descent": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B RGA",
                            "order_number": 2
                        }
                    ],
                    "single plant selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "panicle selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "plant-specific": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "field-repeater",
                            "entity": "harvestData",
                            "field_name": "specific_plant",
                            "minimum": "2",
                            "delimiter": "^",
                            "order_number": 2
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            },
            "fixed": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                },
                "fixed_line": {
                    "single plant seed increase": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "backcrossDerivativeName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            },
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            }
        }
    },
    "harvest_mode": {
        "cross_method": {
            "germplasm_state": {
                "germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "order_number": 4,
                            "sequence_name": "<sequence_name>"
                        }
                    ]
                }
            }
        }
    }
}
$$,
        1,
        'harvest_manager',
        1,
        'CORB-3381 - Add new name config for backcross derivative names in RICE - j.bantay 2022-08-17');



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_GERMPLASM_BC_DERIV_RICE_DEFAULT';