--liquibase formatted sql

--changeset postgres:update_hm_bg_process_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1372 DB: Update HM background process threshold



UPDATE 
    platform.config 
SET 
    config_value = 
    '{
        "createList": {
            "size": "10000",
            "description": "Create new germplasm, seed, or package list"
        },
        "deleteSeeds": {
            "size": "200",
            "description": "Delete seeds of plots or crosses"
        },
        "previewList": {
            "size": "100",
            "description": "Preview germplasm, seed, or package list"
        },
        "deletePackages": {
            "size": "200",
            "description": "Delete packages and related records"
        },
        "deleteHarvestData": {
            "size": "2000",
            "description": "Delete harvest data of plots or crosses"
        },
        "updateHarvestData": {
            "size": "5000",
            "description": "Update harvest data of plots or crosses"
        },
        "exportPackages": {
            "size": "2000",
            "description": "Export created germplasm, seed, and package records"
        }
    }' 
WHERE abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '{
--rollback         "createList": {
--rollback             "size": "10000",
--rollback             "description": "Create new germplasm, seed, or package list"
--rollback         },
--rollback         "deleteSeeds": {
--rollback             "size": "200",
--rollback             "description": "Delete seeds of plots or crosses"
--rollback         },
--rollback         "previewList": {
--rollback             "size": "100",
--rollback             "description": "Preview germplasm, seed, or package list"
--rollback         },
--rollback         "deletePackages": {
--rollback             "size": "200",
--rollback             "description": "Delete packages and related records"
--rollback         },
--rollback         "deleteHarvestData": {
--rollback             "size": "2000",
--rollback             "description": "Delete harvest data of plots or crosses"
--rollback         },
--rollback         "updateHarvestData": {
--rollback             "size": "5000",
--rollback             "description": "Update harvest data of plots or crosses"
--rollback         }
--rollback     }' 
--rollback WHERE abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';