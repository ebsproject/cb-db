--liquibase formatted sql

--changeset postgres:create_germplasm_type_scale_values context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev IN ('GERMPLASM_TYPE_OPV','GERMPLASM_TYPE_TOP_CROSS_HYBRID','GERMPLASM_TYPE_VARIETAL_CROSS_HYBRID')) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1358 Create scale values for GERMPLASM_TYPE variable



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GERMPLASM_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('OPV', 'open-pollinated variety', 'OPV', 'OPV', 'show'),
        ('top_cross_hybrid', 'a hybrid formed from a fixed line and an open-pollinated variety', 'top_cross_hybrid', 'TOP_CROSS_HYBRID', 'show'),
        ('varietal_cross_hybrid', 'a hybrid formed from two open-pollinated varieties', 'varietal_cross_hybrid', 'VARIETAL_CROSS_HYBRID', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'GERMPLASM_TYPE_OPV','GERMPLASM_TYPE_TOP_CROSS_HYBRID','GERMPLASM_TYPE_VARIETAL_CROSS_HYBRID'
--rollback     )
--rollback ;