--liquibase formatted sql

--changeset postgres:create_table_germplasm.package_log context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-377 Create table package_log


CREATE TABLE germplasm.package_log (
    -- primary key column
    id serial NOT NULL,
    
    -- table columns
    package_id integer NOT NULL,
    package_quantity float NOT NULL,
    package_unit varchar(32) NOT NULL,
    package_transaction_type varchar(32) NOT NULL,
    entity_id integer,
    data_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamp NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamp,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT package_log_id_pk PRIMARY KEY (id),

    -- foreign key constraints
    CONSTRAINT package_log_package_id_fk FOREIGN KEY (package_id)
        REFERENCES germplasm.package (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT package_log_entity_id_fk FOREIGN KEY (entity_id)
        REFERENCES dictionary.entity (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    
    -- audit foreign key constraints
    CONSTRAINT package_log_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT package_log_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- indices
CREATE INDEX package_log_package_id_package_transaction_type_idx
    ON germplasm.package_log
    USING btree (package_id, package_transaction_type);

CREATE INDEX package_log_entity_id_data_id_idx
    ON germplasm.package_log
    USING btree (entity_id, data_id);

CREATE INDEX package_log_is_void_idx
    ON germplasm.package_log
    USING btree (is_void);

CREATE INDEX package_log_creator_id_idx
    ON germplasm.package_log
    USING btree (creator_id);

CREATE INDEX package_log_modifier_id_idx
    ON germplasm.package_log
    USING btree (modifier_id);

-- audit log
SELECT * FROM platform.audit_table(
    in_table_name := 'germplasm.package_log'
);

-- comments
COMMENT ON TABLE germplasm.package_log IS 'Package Log: Transactions recorded in a package [PKGLOG]';

COMMENT ON COLUMN germplasm.package_log.id IS 'Package Log ID: Database identifier of the package [PKGLOG_ID]';

COMMENT ON COLUMN germplasm.package_log.package_id IS 'Package ID: Reference to the package where the transaction is made [PKGLOG_PKG_ID]';
COMMENT ON COLUMN germplasm.package_log.package_quantity IS 'Package Quantity: Weight or number of objects in the package [PKGLOG_QTY]';
COMMENT ON COLUMN germplasm.package_log.package_unit IS 'Package Unit: Unit of quantity in the package [PKGLOG_UNIT]';
COMMENT ON COLUMN germplasm.package_log.package_transaction_type IS 'Package Transaction Type: Type of transaction made in the package {deposit, withdraw, reserve} [PKGLOG_TXNTYPE]';
COMMENT ON COLUMN germplasm.package_log.entity_id IS 'Entity ID: Entity where the transaction is conducted, whether it is deposited, withdrawn, or reserved [PKGLOG_ENTITY_ID]';
COMMENT ON COLUMN germplasm.package_log.data_id IS 'Data ID: ID of the entity where the package log is conducted [PKGLOG_DATA_ID]';

COMMENT ON COLUMN germplasm.package_log.remarks IS 'Remarks: Additional details about the record [PKGLOG_REMARKS]';
COMMENT ON COLUMN germplasm.package_log.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was added to the table [PKGLOG_CTSTAMP]';
COMMENT ON COLUMN germplasm.package_log.creator_id IS 'Creator ID: ID of the user who added the record to the table [PKGLOG_CPERSON]';
COMMENT ON COLUMN germplasm.package_log.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [PKGLOG_MTSTAMP]';
COMMENT ON COLUMN germplasm.package_log.modifier_id IS 'Modifier ID: ID of the user who last modified the record [PKGLOG_MPERSON]';
COMMENT ON COLUMN germplasm.package_log.notes IS 'Notes: Additional details added by an admin; can be technical or advanced details [PKGLOG_NOTES]';
COMMENT ON COLUMN germplasm.package_log.is_void IS 'Is Void: Indicator whether the record is deleted (true) or not (false) [PKGLOG_ISVOID]';
COMMENT ON COLUMN germplasm.package_log.event_log IS 'Event Log: Historical transactions of the record [PKGLOG_EVENTLOG]';
--*/


--rollback DROP TABLE germplasm.package_log;
