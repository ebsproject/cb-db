--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_of_site_id_in_experiment.occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-602 Remove not null constraint of site_id in experiment.occurrence



ALTER TABLE
	experiment.occurrence
ALTER COLUMN
	site_id DROP NOT NULL;



--rollback ALTER TABLE
--rollback 	experiment.occurrence
--rollback ALTER COLUMN
--rollback 	site_id SET NOT NULL;
