--liquibase formatted sql

--changeset postgres:create_function_germplasm.update_document_column_for_germplasm.package_from_germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create function germplasm.update_document_column_for_germplasm_package_from_germplasm



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package_from_germplasm()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE 
            germplasm.package 
        SET 
            modification_timestamp = now() 
        WHERE 
            seed_id
        IN
            (
                SELECT 
                    id
                FROM
                    germplasm.seed
                WHERE
                    germplasm_id = new.id
            );
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION germplasm.update_document_column_for_germplasm_package_from_germplasm();