--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_occurrence_p3 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment_occurrence p3



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
    
        IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN

            IF (TG_OP = 'UPDATE') THEN

                SELECT 
                    concat(
                        setweight(to_tsvector(new.occurrence_code),'A'),' ',
                        setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(tpj.project_code),'B'),' ',
                        setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
                        setweight(to_tsvector(tst.stage_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
                        setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
                        setweight(to_tsvector(ts.season_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
                        setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
                        setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
                        setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
                        setweight(to_tsvector(ee.experiment_type),'D'),' ',
                        setweight(to_tsvector(new.occurrence_status),'D'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'D')
                    ) INTO var_document
                FROM 
                    experiment.occurrence eo
                INNER JOIN 
                    experiment.experiment ee
                ON 
                    new.experiment_id = ee.id 
                LEFT JOIN
                    tenant.project tpj
                ON
                    ee.project_id = tpj.id
                INNER JOIN
                    tenant.stage tst
                ON
                    ee.stage_id = tst.id
                INNER JOIN
                    tenant.season ts
                ON
                    ee.season_id = ts.id
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                LEFT JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON	
                    new.creator_id = tp.id
                WHERE
                    eo.id = new.id;
                    
            ELSE 

                 SELECT 
                    concat(
                        setweight(to_tsvector(new.occurrence_code),'A'),' ',
                        setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(tpj.project_code),'B'),' ',
                        setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
                        setweight(to_tsvector(tst.stage_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
                        setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
                        setweight(to_tsvector(ts.season_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
                        setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
                        setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
                        setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
                        setweight(to_tsvector(ee.experiment_type),'D'),' ',
                        setweight(to_tsvector(new.occurrence_status),'D'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'D')
                    ) INTO var_document
                FROM 
                    experiment.occurrence eo
                INNER JOIN 
                    experiment.experiment ee
                ON 
                    new.experiment_id = ee.id 
                LEFT JOIN
                    tenant.project tpj
                ON
                    ee.project_id = tpj.id
                INNER JOIN
                    tenant.stage tst
                ON
                    ee.stage_id = tst.id
                INNER JOIN
                    tenant.season ts
                ON
                    ee.season_id = ts.id
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                LEFT JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON	
                    new.creator_id = tp.id;

            END IF; 

            new.occurrence_document = var_document;           

        END IF;
    
    RETURN NEW;
END;

$BODY$;



--rollback CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
--rollback     RETURNS trigger
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE NOT LEAKPROOF
--rollback AS $BODY$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback     
--rollback         IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback 
--rollback             IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback                 SELECT 
--rollback                     concat(
--rollback                         setweight(to_tsvector(new.occurrence_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
--rollback                         setweight(to_tsvector(tpj.project_code),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
--rollback                         setweight(to_tsvector(tst.stage_code),'BC'),' ',
--rollback                         setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
--rollback                         setweight(to_tsvector(ts.season_code),'BC'),' ',
--rollback                         setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
--rollback                         setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
--rollback                         setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_type),'D'),' ',
--rollback                         setweight(to_tsvector(new.occurrence_status),'D'),' ',
--rollback                         setweight(to_tsvector(unaccent(tp.person_name)),'D')
--rollback                     ) INTO var_document
--rollback                 FROM 
--rollback                     experiment.occurrence eo
--rollback                 INNER JOIN 
--rollback                     experiment.experiment ee
--rollback                 ON 
--rollback                     new.experiment_id = ee.id 
--rollback                 INNER JOIN
--rollback                     tenant.project tpj
--rollback                 ON
--rollback                     ee.project_id = tpj.id
--rollback                 INNER JOIN
--rollback                     tenant.stage tst
--rollback                 ON
--rollback                     ee.stage_id = tst.id
--rollback                 INNER JOIN
--rollback                     tenant.season ts
--rollback                 ON
--rollback                     ee.season_id = ts.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgs
--rollback                 ON
--rollback                     new.site_id = pgs.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgf
--rollback                 ON
--rollback                     new.field_id = pgf.id
--rollback                 INNER JOIN
--rollback                     tenant.person tp
--rollback                 ON	
--rollback                     new.creator_id = tp.id
--rollback                 WHERE
--rollback                     eo.id = new.id;
--rollback                     
--rollback             ELSE 
--rollback 
--rollback                  SELECT 
--rollback                     concat(
--rollback                         setweight(to_tsvector(new.occurrence_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
--rollback                         setweight(to_tsvector(tpj.project_code),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
--rollback                         setweight(to_tsvector(tst.stage_code),'BC'),' ',
--rollback                         setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
--rollback                         setweight(to_tsvector(ts.season_code),'BC'),' ',
--rollback                         setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
--rollback                         setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
--rollback                         setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_type),'D'),' ',
--rollback                         setweight(to_tsvector(new.occurrence_status),'D'),' ',
--rollback                         setweight(to_tsvector(unaccent(tp.person_name)),'D')
--rollback                     ) INTO var_document
--rollback                 FROM 
--rollback                     experiment.occurrence eo
--rollback                 INNER JOIN 
--rollback                     experiment.experiment ee
--rollback                 ON 
--rollback                     new.experiment_id = ee.id 
--rollback                 INNER JOIN
--rollback                     tenant.project tpj
--rollback                 ON
--rollback                     ee.project_id = tpj.id
--rollback                 INNER JOIN
--rollback                     tenant.stage tst
--rollback                 ON
--rollback                     ee.stage_id = tst.id
--rollback                 INNER JOIN
--rollback                     tenant.season ts
--rollback                 ON
--rollback                     ee.season_id = ts.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgs
--rollback                 ON
--rollback                     new.site_id = pgs.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgf
--rollback                 ON
--rollback                     new.field_id = pgf.id
--rollback                 INNER JOIN
--rollback                     tenant.person tp
--rollback                 ON	
--rollback                     new.creator_id = tp.id;
--rollback 
--rollback             END IF; 
--rollback 
--rollback             new.occurrence_document = var_document;           
--rollback 
--rollback         END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback 
--rollback $BODY$;