--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_location_document_column_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_location_document_column p2



DROP TRIGGER IF EXISTS location_update_location_document_tgr ON experiment.location;

CREATE TRIGGER location_update_location_document_tgr
    BEFORE INSERT OR UPDATE 
    ON experiment.location
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_experiment_location();



--rollback DROP TRIGGER location_update_location_document_tgr ON experiment.location;