--liquibase formatted sql

--changeset postgres:add_column_occurrence_number_to_experiment.occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-583 Add column occurrence_number to experiment.occurrence



-- add column
ALTER TABLE experiment.occurrence
ADD COLUMN occurrence_number integer NOT NULL DEFAULT 1;

COMMENT ON COLUMN experiment.occurrence.occurrence_number
IS 'Occurrence Number: Sequential number of the occurrence within the experiment [OCC_NO]';


-- revert changes
--rollback ALTER TABLE experiment.occurrence
--rollback DROP COLUMN occurrence_number;
