--liquibase formatted sql

--changeset postgres:add_column_package_log_id_to_experiment.planting_instruction context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-380 Add column package_log_id to experiment.planting_instruction



ALTER TABLE experiment.planting_instruction
ADD COLUMN package_log_id INTEGER;

COMMENT ON COLUMN experiment.planting_instruction.package_log_id
IS 'Package Log ID: Reference to the package log of withdrawn materials [PLANTINST_PKGLOG_ID]';



-- revert changes
--rollback ALTER TABLE experiment.planting_instruction
--rollback DROP COLUMN package_log_id;
