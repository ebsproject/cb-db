--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_experiment_document_column_from_stage context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-409 Create trigger tenant.update_experiment_document_column_from_stage 



CREATE TRIGGER stage_update_experiment_document_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.stage
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_experiment_from_stage();



--rollback DROP TRIGGER stage_update_experiment_document_tgr ON tenant.stage;