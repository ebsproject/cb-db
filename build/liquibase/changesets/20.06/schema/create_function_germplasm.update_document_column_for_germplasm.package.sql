--liquibase formatted sql

--changeset postgres:create_function_germplasm.update_document_column_for_germplasm.package context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create function germplasm.update_document_column_for_germplasm_package



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

			SELECT	
				concat(
					setweight(to_tsvector(new.package_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
					setweight(to_tsvector(gs.seed_code),'B'),' ',
					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
					setweight(to_tsvector(new.package_unit),'C'),' ',
					setweight(to_tsvector(new.package_status),'C'),' ',
					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
					setweight(to_tsvector(gs.harvest_method),'CD')
				) INTO var_document
			FROM 
				germplasm.package gp
			INNER JOIN
				germplasm.seed gs
			ON
				new.seed_id = gs.id
			WHERE
				gp.id = new.id;
				
        ELSE
			
			SELECT	
				concat(
					setweight(to_tsvector(new.package_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
					setweight(to_tsvector(gs.seed_code),'B'),' ',
					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
					setweight(to_tsvector(new.package_unit),'C'),' ',
					setweight(to_tsvector(new.package_status),'C'),' ',
					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
					setweight(to_tsvector(gs.harvest_method),'CD')
				) INTO var_document
			FROM 
				germplasm.package gp
			INNER JOIN
				germplasm.seed gs
			ON
				new.seed_id = gs.id;
			
        END IF;

        new.package_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION germplasm.update_document_column_for_germplasm_package();