--liquibase formatted sql

--changeset postgres:update_column_entity_id_in_data_terminal.suppression_rule context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update column entity_id in data_terminal.suppression_rule



ALTER TABLE
    data_terminal.suppression_rule
ALTER COLUMN
    entity_id TYPE text;



--rollback ALTER TABLE
--rollback     data_terminal.suppression_rule
--rollback ALTER COLUMN
--rollback     entity_id TYPE integer USING entity_id::integer;
