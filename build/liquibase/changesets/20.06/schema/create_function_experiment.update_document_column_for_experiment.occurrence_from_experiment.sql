--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment.occurrence_from_experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment.occurrence_from_experiment



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence_from_experiment()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE 
            experiment.occurrence 
        SET 
            modification_timestamp = now() 
        WHERE 
            experiment_id = new.id;
       
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_occurrence_from_experimen() CASCADE;