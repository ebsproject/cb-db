--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_location_document_column_from_creator context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger tenant.update_location_document_column_from_creator



CREATE TRIGGER person_update_location_document_from_creator_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.person
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_location_from_creator();



--rollback DROP TRIGGER person_update_location_document_from_creator_tgr ON tenant.person;