--liquibase formatted sql

--changeset postgres:create_trigger_germplasm.update_germplasm_document_column_from_germplasm_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-408 Create trigger germplasm.update_germplasm_document_column_from_germplasm_name



CREATE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr
    AFTER INSERT OR UPDATE 
	ON germplasm.germplasm_name
    FOR EACH ROW
    EXECUTE PROCEDURE germplasm.update_document_column_for_germplasm_germplasm_from_germp_name();



--rollback DROP TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr ON germplasm.germplasm_name;