--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_experiment_document_column_from_creator context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-409 Create trigger tenant.update_experiment_document_column_from_creator



CREATE TRIGGER person_creator_update_experiment_document_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.person
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_experiment_from_creator();



--rollback DROP TRIGGER person_creator_update_experiment_document_tgr ON tenant.person;