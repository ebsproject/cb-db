--liquibase formatted sql

--changeset postgres:create_function_germplasm.update_document_column_for_germplasm.package_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create function germplasm.update_document_column_for_germplasm_package p2



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

			SELECT	
				concat(
					setweight(to_tsvector(new.package_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
					setweight(to_tsvector(unaccent(gm.designation)),'A'),' ',
					setweight(to_tsvector(gs.seed_code),'B'),' ',
					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
					setweight(to_tsvector(new.package_unit),'C'),' ',
					setweight(to_tsvector(new.package_status),'C'),' ',
					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
					setweight(to_tsvector(gs.harvest_method),'CD')
				) INTO var_document
			FROM 
				germplasm.package gp
			INNER JOIN
				germplasm.seed gs
			ON
				new.seed_id = gs.id
			INNER JOIN
				germplasm.germplasm gm
			ON
				gs.germplasm_id = gm.id
			WHERE
				gp.id = new.id;
				
        ELSE
			
			SELECT	
				concat(
					setweight(to_tsvector(new.package_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
					setweight(to_tsvector(unaccent(gm.designation)),'A'),' ',
					setweight(to_tsvector(gs.seed_code),'B'),' ',
					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
					setweight(to_tsvector(new.package_unit),'C'),' ',
					setweight(to_tsvector(new.package_status),'C'),' ',
					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
					setweight(to_tsvector(gs.harvest_method),'CD')
				) INTO var_document
			FROM 
				germplasm.package gp
			INNER JOIN
				germplasm.seed gs
			ON
				new.seed_id = gs.id
			INNER JOIN
				germplasm.germplasm gm
			ON
				gs.germplasm_id = gm.id;
			
        END IF;

        new.package_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_package()
--rollback     RETURNS trigger
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE NOT LEAKPROOF
--rollback AS $BODY$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback 			SELECT	
--rollback 				concat(
--rollback 					setweight(to_tsvector(new.package_code),'A'),' ',
--rollback 					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
--rollback 					setweight(to_tsvector(gs.seed_code),'B'),' ',
--rollback 					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
--rollback 					setweight(to_tsvector(new.package_unit),'C'),' ',
--rollback 					setweight(to_tsvector(new.package_status),'C'),' ',
--rollback 					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
--rollback 					setweight(to_tsvector(gs.harvest_method),'CD')
--rollback 				) INTO var_document
--rollback 			FROM 
--rollback 				germplasm.package gp
--rollback 			INNER JOIN
--rollback 				germplasm.seed gs
--rollback 			ON
--rollback 				new.seed_id = gs.id
--rollback 			WHERE
--rollback 				gp.id = new.id;
--rollback 				
--rollback         ELSE
--rollback 			
--rollback 			SELECT	
--rollback 				concat(
--rollback 					setweight(to_tsvector(new.package_code),'A'),' ',
--rollback 					setweight(to_tsvector(unaccent(new.package_label)),'A'),' ',
--rollback 					setweight(to_tsvector(gs.seed_code),'B'),' ',
--rollback 					setweight(to_tsvector(unaccent(gs.seed_name)),'B'),' ',
--rollback 					setweight(to_tsvector(new.package_unit),'C'),' ',
--rollback 					setweight(to_tsvector(new.package_status),'C'),' ',
--rollback 					setweight(to_tsvector(gs.harvest_date::text),'CD'),' ',
--rollback 					setweight(to_tsvector(gs.harvest_method),'CD')
--rollback 				) INTO var_document
--rollback 			FROM 
--rollback 				germplasm.package gp
--rollback 			INNER JOIN
--rollback 				germplasm.seed gs
--rollback 			ON
--rollback 				new.seed_id = gs.id;
--rollback 			
--rollback         END IF;
--rollback 
--rollback         new.package_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $BODY$;