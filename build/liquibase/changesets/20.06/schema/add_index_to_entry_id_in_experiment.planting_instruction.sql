--liquibase formatted sql

--changeset postgres:add_index_to_entry_id_in_experiment.planting_instruction context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-380 Add index to entry_id in experiment.planting_instruction



CREATE INDEX planting_instruction_entry_id_idx
ON experiment.planting_instruction (entry_id);

COMMENT ON INDEX experiment.planting_instruction_entry_id_idx
IS 'A planting instruction can be retrieved by its entry.'


-- revert changes
--rollback DROP INDEX experiment.planting_instruction_entry_id_idx;
