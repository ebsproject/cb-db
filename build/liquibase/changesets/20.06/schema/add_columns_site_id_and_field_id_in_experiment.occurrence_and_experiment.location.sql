--liquibase formatted sql

--changeset postgres:add_columns_site_id_and_field_id_in_experiment.occurrence_and_experiment.location.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-389 Add columns site_id and field_id in experiment.occurrence and experiment.location



--add columns site_id and field_id to experimet.occurrence

ALTER TABLE 
    experiment.occurrence
ADD COLUMN 
    site_id integer,
ADD COLUMN
    field_id integer,
ADD CONSTRAINT 
    occurrence_site_id_fk FOREIGN KEY (site_id) 
REFERENCES 
    place.geospatial_object (id) ON UPDATE CASCADE ON DELETE RESTRICT,
ADD CONSTRAINT 
    occurrence_field_id_fk FOREIGN KEY (field_id) 
REFERENCES 
    place.geospatial_object (id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN 
    experiment.occurrence.site_id
IS 
	'Site ID: Reference to the site level geospatial object [OCC_SITE_ID]';

COMMENT ON COLUMN 
    experiment.occurrence.field_id
IS 
	'Field ID: Reference to the field level geospatial object [OCC_FIELD_ID]';

COMMENT ON CONSTRAINT occurrence_site_id_fk
    ON experiment.occurrence
IS 
	'An occurrence attribute refers to a site level geospatial object.';

COMMENT ON CONSTRAINT occurrence_field_id_fk
    ON experiment.occurrence
IS 
	'An occurrence attribute refers to a field level geospatial object';

CREATE INDEX
	occurrence_site_id_idx
ON 
	experiment.occurrence
USING 
	btree (site_id);

COMMENT ON INDEX
	experiment.occurrence_site_id_idx
IS
	'An occurrence can be searched by its site level geospatial object.';

CREATE INDEX
	occurrence_field_id_idx
ON 
	experiment.occurrence
USING 
	btree (field_id);

COMMENT ON INDEX
	experiment.occurrence_field_id_idx
IS
	'An occurrence can be searched by its field level geospatial object';

--add columns site_id and field_id to experimet.location

ALTER TABLE 
    experiment.location
ADD COLUMN 
    site_id integer,
ADD COLUMN
    field_id integer,
ADD CONSTRAINT 
    location_site_id_fk FOREIGN KEY (site_id) 
REFERENCES 
    place.geospatial_object (id) ON UPDATE CASCADE ON DELETE RESTRICT,
ADD CONSTRAINT 
    location_field_id_fk FOREIGN KEY (field_id) 
REFERENCES 
    place.geospatial_object (id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN 
    experiment.location.site_id
IS 
	'Site ID: Reference to the site level geospatial object [LOC_SITE_ID]';

COMMENT ON COLUMN 
    experiment.location.field_id
IS 
	'Field ID: Reference to the field level geospatial object [LOC_FIELD_ID]';

COMMENT ON CONSTRAINT location_site_id_fk
    ON experiment.location
IS 
	'A location attribute refers to a site level geospatial object.';

COMMENT ON CONSTRAINT location_field_id_fk
    ON experiment.location
IS 
	'A location attribute refers to a field level geospatial object.';

CREATE INDEX
	location_site_id_idx
ON 
	experiment.location
USING 
	btree (site_id);

COMMENT ON INDEX
	experiment.location_site_id_idx
IS
	'A location can be searched by its site level geospatial object';

CREATE INDEX
	location_field_id_idx
ON 
	experiment.location
USING 
	btree (field_id);

COMMENT ON INDEX
	experiment.location_field_id_idx
IS
	'A location can be searched by its field level geospatial object';



--rollback ALTER TABLE experiment.occurrence DROP COLUMN site_id, DROP COLUMN field_id;
--rollback ALTER TABLE experiment.location DROP COLUMN site_id, DROP COLUMN field_id;