--liquibase formatted sql

--changeset postgres:update_experiment_status_char_length_in_experiment.experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-731 Update experiment_status character length in experiment.experiment



ALTER TABLE
    experiment.experiment
ALTER COLUMN
    experiment_status
TYPE
    character varying(256);



--rollback ALTER TABLE
--rollback     experiment.experiment
--rollback ALTER COLUMN
--rollback     experiment_status
--rollback TYPE
--rollback     character varying(64);