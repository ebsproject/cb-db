--liquibase formatted sql

--changeset postgres:remove_columns_program_id_and_stage_id_in_experiment.location context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-570 Remove columns program_id and stage_id in experiment.location



ALTER TABLE
	experiment.location
DROP COLUMN
	program_id,
DROP COLUMN
	stage_id;



--rollback 
--rollback ALTER TABLE  
--rollback 	experiment."location"
--rollback ADD COLUMN
--rollback 	program_id integer,
--rollback ADD FOREIGN KEY 
--rollback     (program_id)
--rollback REFERENCES 
--rollback     tenant.program (id);
--rollback     
--rollback COMMENT ON COLUMN 
--rollback     experiment."location".program_id 
--rollback IS 
--rollback 	'Program ID: Reference to the experiment"s program [LOC_PROGRAM_ID]';
--rollback 
--rollback CREATE INDEX
--rollback 	location_program_id_idx
--rollback ON 
--rollback 	experiment."location"
--rollback USING 
--rollback 	btree (program_id);
--rollback 
--rollback 
--rollback ALTER TABLE
--rollback 	experiment."location"
--rollback ADD COLUMN
--rollback 	stage_id integer,
--rollback ADD FOREIGN KEY 
--rollback     (stage_id)
--rollback REFERENCES 
--rollback     tenant.stage (id);
--rollback     
--rollback COMMENT ON COLUMN 
--rollback     experiment."location".stage_id 
--rollback IS 
--rollback 	'Stage ID: Reference to the experiment"s stage [LOC_STAGE_ID]';
--rollback 
--rollback CREATE INDEX
--rollback 	location_stage_id_idx
--rollback ON 
--rollback 	experiment."location"
--rollback USING 
--rollback 	btree (stage_id);