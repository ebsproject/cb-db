--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_in_experiment.experiment_block.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-622 Remove not null constraint in experiment.experiment_block



ALTER TABLE
	experiment.experiment_block
ALTER COLUMN
	no_of_ranges DROP NOT NULL,
ALTER COLUMN
	no_of_cols DROP NOT NULL,
ALTER COLUMN
	no_of_reps DROP NOT NULL,
ALTER COLUMN
	plot_numbering_order DROP NOT NULL,
ALTER COLUMN
	starting_corner DROP NOT NULL;



--rollback ALTER TABLE
--rollback 	experiment.experiment_block
--rollback ALTER COLUMN
--rollback 	no_of_ranges SET NOT NULL,
--rollback ALTER COLUMN
--rollback 	no_of_cols SET NOT NULL,
--rollback ALTER COLUMN
--rollback 	no_of_reps SET NOT NULL,
--rollback ALTER COLUMN
--rollback 	plot_numbering_order SET NOT NULL,
--rollback ALTER COLUMN
--rollback 	starting_corner SET NOT NULL;