--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_occurrence_document_column_from_stage context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_occurrence_document_column_from_stage



CREATE TRIGGER stage_update_occurrence_document_from_stage_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.stage
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_occurrence_from_stage();



--rollback DROP TRIGGER stage_update_occurrence_document_from_stage_tgr ON tenant.stage;