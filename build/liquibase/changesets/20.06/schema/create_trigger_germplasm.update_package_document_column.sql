--liquibase formatted sql

--changeset postgres:create_trigger_germplasm.update_package_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create trigger germplasm.update_package_document_column



CREATE TRIGGER package_update_package_document_tgr
    BEFORE INSERT OR UPDATE 
    ON germplasm.package
    FOR EACH ROW
    EXECUTE PROCEDURE germplasm.update_document_column_for_germplasm_package();



--rollback DROP TRIGGER package_update_package_document_tgr ON germplasm.package;