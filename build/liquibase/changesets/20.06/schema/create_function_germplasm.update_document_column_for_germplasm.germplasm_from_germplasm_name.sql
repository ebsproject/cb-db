--liquibase formatted sql

--changeset postgres:create_function_germplasm.update_document_column_for_germplasm.germplasm_from_germplasm_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-408 Create function germplasm.update_document_column_for_germplasm_germplasm_from_germplasm_name



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm_from_germp_name()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

			IF old.germplasm_id <> new.germplasm_id THEN

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = new.germplasm_id;

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = old.germplasm_id;

			ELSE

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = new.germplasm_id;

			END IF;

         ELSE
			
			UPDATE 
            	germplasm.germplasm
        	SET 
            	modification_timestamp = now() 
        	WHERE 
            	id = new.germplasm_id;

		END IF;
		
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION germplasm.update_document_column_for_germplasm_germplasm_from_germp_name();