--liquibase formatted sql

--changeset postgres:add_column_layout_order_data_to_experiment.experiment_block context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-387 Add column layout_order_data to experiment.experiment_block



ALTER TABLE 
    experiment.experiment_block
ADD COLUMN 
    layout_order_data jsonb;

COMMENT ON COLUMN 
    experiment.experiment_block.layout_order_data
IS 
	'Layout Order Data: Replace temporary table with JSON column [LAYOUT_ORDER_DATA]';

CREATE INDEX
	experiment_block_layout_order_data_idx
ON 
	experiment.experiment_block
USING 
	btree (layout_order_data);

COMMENT ON INDEX
	experiment.experiment_block_layout_order_data_idx
IS
	'Temporary data can be retrieved by layout order data';



--rollback ALTER TABLE experiment.experiment_block
--rollback DROP COLUMN layout_order_data;