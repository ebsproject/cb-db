--liquibase formatted sql

--changeset postgres:add_column_package_document_to_germplasm.package context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-404 Add column package_document to germplasm.package



ALTER TABLE
	germplasm.package 
ADD COLUMN 
	package_document tsvector;

COMMENT ON COLUMN 
    germplasm.package.package_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';



--rollback ALTER TABLE germplasm.package
--rollback DROP COLUMN package_document;