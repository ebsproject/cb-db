--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_occurrence_document_column_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_occurrence_document_column p2



DROP TRIGGER IF EXISTS occurrence_update_occurrence_document_tgr ON experiment.occurrence;

CREATE TRIGGER occurrence_update_occurrence_document_tgr
    BEFORE INSERT OR UPDATE 
    ON experiment.occurrence
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_experiment_occurrence();



--rollback DROP TRIGGER occurrence_update_occurrence_document_tgr ON experiment.occurrence;