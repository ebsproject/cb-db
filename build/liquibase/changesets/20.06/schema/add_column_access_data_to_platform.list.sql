--liquibase formatted sql

--changeset postgres:add_column_access_data_to_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-470 Add column access_data to platform.list



ALTER TABLE
	platform.list 
ADD COLUMN 
	access_data jsonb;

COMMENT ON COLUMN 
    platform.list.access_data 
IS 'Access_Data: Refers to user ability to access or retrieve data within a study or program [LST_ACC_DATA]';



--rollback ALTER TABLE platform.list
--rollback DROP COLUMN access_data;