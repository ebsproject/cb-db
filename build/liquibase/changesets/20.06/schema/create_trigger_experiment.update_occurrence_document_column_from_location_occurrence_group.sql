--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_occurrence_document_column_from_location_occurrence_group context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_occurrence_document_column_from_location_occurrence_group



CREATE TRIGGER location_occurrence_group_update_occurrence_document_tgr
    AFTER INSERT OR UPDATE
    ON experiment.location_occurrence_group
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_occurrence_from_lococcgr();
	


--rollback DROP TRIGGER location_occurrence_group_update_occurrence_document_tgr ON experiment.location_occurrence_group;