--liquibase formatted sql

--changeset postgres:drop_layout_order_idx_in_experiment.experiment_block context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Drop layout_order_idx in experiment.experiment_block



DROP INDEX experiment.experiment_block_layout_order_data_idx;



--rollback CREATE INDEX experiment_block_layout_order_data_idx
--rollback     ON experiment.experiment_block USING btree
--rollback     (layout_order_data ASC NULLS LAST)
--rollback     TABLESPACE pg_default;
--rollback 
--rollback COMMENT ON INDEX experiment.experiment_block_layout_order_data_idx
--rollback     IS 'Temporary data can be retrieved by layout order data';