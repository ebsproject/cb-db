--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_occurrence_document_column_from_season context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_occurrence_document_column_from_season



CREATE TRIGGER season_update_occurrence_document_from_season_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.season
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_occurrence_from_season();



--rollback DROP TRIGGER season_update_occurrence_document_from_season_tgr ON tenant.season;