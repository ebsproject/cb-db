--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_location_document_column_from_experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_location_document_column_from_experiment



CREATE TRIGGER experiment_update_location_document_from_experiment_tgr
    AFTER INSERT OR UPDATE 
	ON experiment.experiment
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_experiment_location_from_experiment();



--rollback DROP TRIGGER experiment_update_location_document_from_experiment_tgr ON experiment.experiment;