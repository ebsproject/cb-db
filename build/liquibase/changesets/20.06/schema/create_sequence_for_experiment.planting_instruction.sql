--liquibase formatted sql

--changeset postgres:create_sequence_for_experiment.planting_instruction context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-655 Create sequence for experiment.planting_instruction



--create sequence
CREATE SEQUENCE experiment.planting_instruction_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

--set default value
ALTER TABLE experiment.planting_instruction
ALTER COLUMN id SET DEFAULT nextval('experiment.planting_instruction_id_seq'::regclass);

--setval
SELECT SETVAL('experiment.planting_instruction_id_seq', COALESCE(MAX(id), 1)) FROM experiment.planting_instruction;



--rollback ALTER TABLE 
--rollback     experiment.planting_instruction
--rollback ALTER COLUMN
--rollback     id DROP DEFAULT;
--rollback DROP SEQUENCE experiment.planting_instruction_id_seq CASCADE



