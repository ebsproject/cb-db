--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_person_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-411 Create trigger person_update_person_document_tgr



CREATE TRIGGER person_update_person_document_tgr
    BEFORE INSERT OR UPDATE 
    ON tenant.person
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_tenant_person();



--rollback DROP TRIGGER person_update_person_document_tgr ON tenant.person;