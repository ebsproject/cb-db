--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment.location_from_experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.experiment.update_document_column_for_experiment.location_from_experiment



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location_from_experiment()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE 
            experiment.location 
        SET 
            modification_timestamp = now() 
        WHERE 
            id 
        IN 
            (
                SELECT 
                    location_id
                FROM
                    experiment.location_occurrence_group
                WHERE
                    occurrence_id 
                IN 
                    (
                        SELECT 
                            id
                        FROM
                            experiment.occurrence
                        WHERE
                            experiment_id
                        IN
                            (
                                SELECT 
                                    id
                                FROM
                                    experiment.experiment
                                WHERE 
                                    id = new.id
                            )
                    )
            );    

    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_location_from_experiment() CASCADE;