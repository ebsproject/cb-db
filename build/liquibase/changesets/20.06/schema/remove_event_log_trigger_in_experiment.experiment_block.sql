--liquibase formatted sql

--changeset postgres:remove_event_log_trigger_in_experiment.experiment_block.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Remove event_log trigger in experiment.experiment_block



DROP TRIGGER experiment_block_event_log_tgr ON experiment.experiment_block CASCADE;



--rollback CREATE TRIGGER experiment_block_event_log_tgr
--rollback     BEFORE INSERT OR UPDATE 
--rollback     ON experiment.experiment_block
--rollback     FOR EACH ROW
--rollback     EXECUTE PROCEDURE platform.log_record_event();
