--liquibase formatted sql

--changeset postgres:create_function_tenant.update_document_column_for_experiment.occurrence_from_stage context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function tenant.update_document_column_for_experiment.occurrence_from_stage



CREATE OR REPLACE FUNCTION tenant.update_document_column_for_experiment_occurrence_from_stage()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE 
            experiment.occurrence 
        SET 
            modification_timestamp = now() 
        WHERE 
            experiment_id 
        IN
            (
                SELECT 
                    id
                FROM
                    experiment.experiment
                WHERE
                    stage_id = new.id
            );
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION tenant.update_document_column_for_experiment_occurrence_from_stage() CASCADE;