--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_location_document_column_from_steward context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger tenant.update_location_document_column_from_steward



CREATE TRIGGER person_update_location_document_from_steward_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.person
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_location_from_steward();



--rollback DROP TRIGGER person_update_location_document_from_steward_tgr ON tenant.person;