--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_location_document_column_from_season context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger tenant.update_location_document_column_from_season



CREATE TRIGGER season_update_location_document_from_season_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.season
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_location_from_season();



--rollback DROP TRIGGER season_update_location_document_from_season_tgr ON tenant.season;