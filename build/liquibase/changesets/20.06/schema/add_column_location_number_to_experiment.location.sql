--liquibase formatted sql

--changeset postgres:add_column_location_number_to_experiment.location context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-583 Add column location_number to experiment.location



-- add column
ALTER TABLE experiment.location
ADD COLUMN location_number integer NOT NULL DEFAULT 1;

COMMENT ON COLUMN experiment.location.location_number
IS 'Location Number: Sequential number of the location within the same year and season [LOC_NO]';


-- revert changes
--rollback ALTER TABLE experiment.location
--rollback DROP COLUMN location_number;
