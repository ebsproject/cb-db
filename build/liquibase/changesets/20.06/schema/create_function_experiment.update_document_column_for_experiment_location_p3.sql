--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_location_p3 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment_location p3



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    
        IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
            IF (TG_OP = 'UPDATE') THEN

                SELECT 
                    concat(
                        setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
                        setweight(to_tsvector(new.location_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(new.location_year::varchar),'C'),' ',
                        setweight(to_tsvector(ts.season_code),'C'),' ',
                        setweight(to_tsvector(ts.season_name),'C'),' ',
                        setweight(to_tsvector(new.location_type),'CD'),' ',
                        setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
                        setweight(to_tsvector(new.location_status),'CD')
                    ) INTO var_document
                FROM 
                    experiment.LOCATION el
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                LEFT JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON
                    new.creator_id = tp.id
                INNER JOIN
                    tenant.person tps
                ON
                    new.steward_id = tps.id
                INNER JOIN
                    tenant.season ts
                ON
                    new.season_id = ts.id
                LEFT JOIN
                    experiment.location_occurrence_group elo
                ON	
                    new.id = elo.location_id
                LEFT JOIN
                    experiment.occurrence eoc
                ON
                    elo.occurrence_id = eoc.id
                LEFT JOIN
                    experiment.experiment ee
                ON
                    eoc.experiment_id = ee.id
                WHERE
                    el.id = new.id;
                
            ELSE

                SELECT 
                    concat(
                        setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
                        setweight(to_tsvector(new.location_code),'A'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(new.location_year::varchar),'C'),' ',
                        setweight(to_tsvector(ts.season_code),'C'),' ',
                        setweight(to_tsvector(ts.season_name),'C'),' ',
                        setweight(to_tsvector(new.location_type),'CD'),' ',
                        setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
                        setweight(to_tsvector(new.location_status),'CD')
                    ) INTO var_document
                FROM 
                    experiment.LOCATION el
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                LEFT JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON
                    new.creator_id = tp.id
                INNER JOIN
                    tenant.person tps
                ON
                    new.steward_id = tps.id
                INNER JOIN
                    tenant.season ts
                ON
                    new.season_id = ts.id;
        
            END IF;
            
            new.location_document = var_document;
        
        END IF;
    
    RETURN NEW;
END;

$BODY$;



--rollback CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location()
--rollback     RETURNS trigger
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE NOT LEAKPROOF
--rollback AS $BODY$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     
--rollback         IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         
--rollback             IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback                 SELECT 
--rollback                     concat(
--rollback                         setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
--rollback                         setweight(to_tsvector(new.location_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
--rollback                         setweight(to_tsvector(ee.experiment_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
--rollback                         setweight(to_tsvector(new.location_year::varchar),'C'),' ',
--rollback                         setweight(to_tsvector(ts.season_code),'C'),' ',
--rollback                         setweight(to_tsvector(ts.season_name),'C'),' ',
--rollback                         setweight(to_tsvector(new.location_type),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(new.location_status),'CD')
--rollback                     ) INTO var_document
--rollback                 FROM 
--rollback                     experiment.LOCATION el
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgs
--rollback                 ON
--rollback                     new.site_id = pgs.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgf
--rollback                 ON
--rollback                     new.field_id = pgf.id
--rollback                 INNER JOIN
--rollback                     tenant.person tp
--rollback                 ON
--rollback                     new.creator_id = tp.id
--rollback                 INNER JOIN
--rollback                     tenant.person tps
--rollback                 ON
--rollback                     new.steward_id = tps.id
--rollback                 INNER JOIN
--rollback                     tenant.season ts
--rollback                 ON
--rollback                     new.season_id = ts.id
--rollback                 INNER JOIN
--rollback                     experiment.location_occurrence_group elo
--rollback                 ON	
--rollback                     new.id = elo.location_id
--rollback                 INNER JOIN
--rollback                     experiment.occurrence eoc
--rollback                 ON
--rollback                     elo.occurrence_id = eoc.id
--rollback                 INNER JOIN
--rollback                     experiment.experiment ee
--rollback                 ON
--rollback                     eoc.experiment_id = ee.id
--rollback                 WHERE
--rollback                     el.id = new.id;
--rollback                 
--rollback             ELSE
--rollback 
--rollback                 SELECT 
--rollback                     concat(
--rollback                         setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
--rollback                         setweight(to_tsvector(new.location_code),'A'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
--rollback                         setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
--rollback                         setweight(to_tsvector(new.location_year::varchar),'C'),' ',
--rollback                         setweight(to_tsvector(ts.season_code),'C'),' ',
--rollback                         setweight(to_tsvector(ts.season_name),'C'),' ',
--rollback                         setweight(to_tsvector(new.location_type),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
--rollback                         setweight(to_tsvector(new.location_status),'CD')
--rollback                     ) INTO var_document
--rollback                 FROM 
--rollback                     experiment.LOCATION el
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgs
--rollback                 ON
--rollback                     new.site_id = pgs.id
--rollback                 INNER JOIN
--rollback                     place.geospatial_object pgf
--rollback                 ON
--rollback                     new.field_id = pgf.id
--rollback                 INNER JOIN
--rollback                     tenant.person tp
--rollback                 ON
--rollback                     new.creator_id = tp.id
--rollback                 INNER JOIN
--rollback                     tenant.person tps
--rollback                 ON
--rollback                     new.steward_id = tps.id
--rollback                 INNER JOIN
--rollback                     tenant.season ts
--rollback                 ON
--rollback                     new.season_id = ts.id;
--rollback         
--rollback             END IF;
--rollback             
--rollback             new.location_document = var_document;
--rollback         
--rollback         END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback 
--rollback $BODY$;