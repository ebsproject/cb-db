--liquibase formatted sql

--changeset postgres:add_not_null_constraint_to_site_id_in_occurrence_and_location context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-389 Add not null constraint to site_id in experiment.occurrence and experiment.location



ALTER TABLE
    experiment.location
ALTER COLUMN
    site_id SET NOT NULL;

ALTER TABLE
    experiment.occurrence
ALTER COLUMN
    site_id SET NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.location
--rollback ALTER COLUMN
--rollback     site_id DROP NOT NULL;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.occurrence
--rollback ALTER COLUMN
--rollback     site_id DROP NOT NULL;