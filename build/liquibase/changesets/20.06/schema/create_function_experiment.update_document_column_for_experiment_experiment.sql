--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-409 Create function experiment.update_document_column_for_experiment_experiment



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_experiment()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

           SELECT
				concat(
					setweight(to_tsvector(new.experiment_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
					setweight(to_tsvector(new.experiment_year::text),'B'),' ',
					setweight(to_tsvector(new.experiment_type),'B'),' ',
					setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
					setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
					setweight(to_tsvector(new.experiment_design_type),'B'),' ',
					setweight(to_tsvector(new.experiment_status),'B'),' ',
					setweight(to_tsvector(new.planting_season),'C'),' ',
					setweight(to_tsvector(ts.season_code),'C'),' ',
					setweight(to_tsvector(ts.season_name),'C'),' ',
					setweight(to_tsvector(tst.stage_code),'C'),' ',
					setweight(to_tsvector(tst.stage_name),'C'),' ',
					setweight(to_tsvector(tp.project_code),'CD'),' ',
					setweight(to_tsvector(tp.project_name),'CD'),' ',
					setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
					setweight(to_tsvector(unaccent(tps.person_name)),'CD')
				) INTO var_document
			FROM
				experiment.experiment ex
			INNER JOIN
				tenant.season ts
			ON
				new.season_id = ts.id
			INNER JOIN
				tenant.stage tst
			ON
				new.stage_id = tst.id
			INNER JOIN
				tenant.project tp
			ON
				new.project_id = tp.id
			INNER JOIN
				tenant.person tpr
			ON
				new.steward_id = tpr.id
			INNER JOIN
				tenant.person tps
			ON 
				new.creator_id = tps.id
			WHERE
				ex.id = new.id;
                
        ELSE
			
			SELECT
				concat(
					setweight(to_tsvector(new.experiment_code),'A'),' ',
					setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
					setweight(to_tsvector(new.experiment_year::text),'B'),' ',
					setweight(to_tsvector(new.experiment_type),'B'),' ',
					setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
					setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
					setweight(to_tsvector(new.experiment_design_type),'B'),' ',
					setweight(to_tsvector(new.experiment_status),'B'),' ',
					setweight(to_tsvector(new.planting_season),'C'),' ',
					setweight(to_tsvector(ts.season_code),'C'),' ',
					setweight(to_tsvector(ts.season_name),'C'),' ',
					setweight(to_tsvector(tst.stage_code),'C'),' ',
					setweight(to_tsvector(tst.stage_name),'C'),' ',
					setweight(to_tsvector(tp.project_code),'CD'),' ',
					setweight(to_tsvector(tp.project_name),'CD'),' ',
					setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
					setweight(to_tsvector(unaccent(tps.person_name)),'CD')
				) INTO var_document
			FROM
				experiment.experiment ex
			INNER JOIN
				tenant.season ts
			ON
				new.season_id = ts.id
			INNER JOIN
				tenant.stage tst
			ON
				new.stage_id = tst.id
			INNER JOIN
				tenant.project tp
			ON
				new.project_id = tp.id
			INNER JOIN
				tenant.person tpr
			ON
				new.steward_id = tpr.id
			INNER JOIN
				tenant.person tps
			ON 
				new.creator_id = tps.id;
            
        END IF;

        new.experiment_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_experiment();