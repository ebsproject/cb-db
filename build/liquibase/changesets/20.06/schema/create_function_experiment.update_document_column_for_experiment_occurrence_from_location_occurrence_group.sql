--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_occurrence_from_location_occurrence_group context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.experiment.update_document_column_for_experiment_occurrence_from_location_occurrence_group



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_occurrence_from_lococcgr()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE experiment.occurrence SET modification_timestamp = now() WHERE id = new.occurrence_id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_occurrence_from_lococcgr() CASCADE;