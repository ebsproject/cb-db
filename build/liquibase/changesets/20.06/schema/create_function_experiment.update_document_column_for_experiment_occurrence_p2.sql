--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_occurrence_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment_occurrence p2



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_occurrence()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
    
        IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN

            IF (TG_OP = 'UPDATE') THEN

                SELECT 
                    concat(
                        setweight(to_tsvector(new.occurrence_code),'A'),' ',
                        setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(tpj.project_code),'B'),' ',
                        setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
                        setweight(to_tsvector(tst.stage_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
                        setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
                        setweight(to_tsvector(ts.season_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
                        setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
                        setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
                        setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
                        setweight(to_tsvector(ee.experiment_type),'D'),' ',
                        setweight(to_tsvector(new.occurrence_status),'D'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'D')
                    ) INTO var_document
                FROM 
                    experiment.occurrence eo
                INNER JOIN 
                    experiment.experiment ee
                ON 
                    new.experiment_id = ee.id 
                INNER JOIN
                    tenant.project tpj
                ON
                    ee.project_id = tpj.id
                INNER JOIN
                    tenant.stage tst
                ON
                    ee.stage_id = tst.id
                INNER JOIN
                    tenant.season ts
                ON
                    ee.season_id = ts.id
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                INNER JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON	
                    new.creator_id = tp.id
                WHERE
                    eo.id = new.id;
                    
            ELSE 

                 SELECT 
                    concat(
                        setweight(to_tsvector(new.occurrence_code),'A'),' ',
                        setweight(to_tsvector(unaccent(new.occurrence_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(tpj.project_code),'B'),' ',
                        setweight(to_tsvector(unaccent(tpj.project_name)),'B'),' ',
                        setweight(to_tsvector(tst.stage_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(tst.stage_name)),'BC'),' ',
                        setweight(to_tsvector(ee.experiment_year::text),'BC'),' ',
                        setweight(to_tsvector(ts.season_code),'BC'),' ',
                        setweight(to_tsvector(unaccent(ts.season_name)),'BC'),' ',
                        setweight(to_tsvector(pgs.geospatial_object_code),'C'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'C'),' ',
                        setweight(to_tsvector(pgf.geospatial_object_code),'CD'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'CD'),' ',
                        setweight(to_tsvector(ee.experiment_design_type),'D'),' ',
                        setweight(to_tsvector(ee.experiment_type),'D'),' ',
                        setweight(to_tsvector(new.occurrence_status),'D'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'D')
                    ) INTO var_document
                FROM 
                    experiment.occurrence eo
                INNER JOIN 
                    experiment.experiment ee
                ON 
                    new.experiment_id = ee.id 
                INNER JOIN
                    tenant.project tpj
                ON
                    ee.project_id = tpj.id
                INNER JOIN
                    tenant.stage tst
                ON
                    ee.stage_id = tst.id
                INNER JOIN
                    tenant.season ts
                ON
                    ee.season_id = ts.id
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                INNER JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON	
                    new.creator_id = tp.id;

            END IF; 

            new.occurrence_document = var_document;           

        END IF;
    
    RETURN NEW;
END;

$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_occurrence() CASCADE;