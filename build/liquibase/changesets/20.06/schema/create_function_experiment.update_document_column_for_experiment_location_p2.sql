--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment_location_p2 context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment_location p2



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    
        IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
            IF (TG_OP = 'UPDATE') THEN

                SELECT 
                    concat(
                        setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
                        setweight(to_tsvector(new.location_code),'A'),' ',
                        setweight(to_tsvector(unaccent(ee.experiment_name)),'A'),' ',
                        setweight(to_tsvector(ee.experiment_code),'A'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(new.location_year::varchar),'C'),' ',
                        setweight(to_tsvector(ts.season_code),'C'),' ',
                        setweight(to_tsvector(ts.season_name),'C'),' ',
                        setweight(to_tsvector(new.location_type),'CD'),' ',
                        setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
                        setweight(to_tsvector(new.location_status),'CD')
                    ) INTO var_document
                FROM 
                    experiment.LOCATION el
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                INNER JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON
                    new.creator_id = tp.id
                INNER JOIN
                    tenant.person tps
                ON
                    new.steward_id = tps.id
                INNER JOIN
                    tenant.season ts
                ON
                    new.season_id = ts.id
                INNER JOIN
                    experiment.location_occurrence_group elo
                ON	
                    new.id = elo.location_id
                INNER JOIN
                    experiment.occurrence eoc
                ON
                    elo.occurrence_id = eoc.id
                INNER JOIN
                    experiment.experiment ee
                ON
                    eoc.experiment_id = ee.id
                WHERE
                    el.id = new.id;
                
            ELSE

                SELECT 
                    concat(
                        setweight(to_tsvector(unaccent(new.location_name)),'A'),' ',
                        setweight(to_tsvector(new.location_code),'A'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgs.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_code)),'B'),' ',
                        setweight(to_tsvector(unaccent(pgf.geospatial_object_name)),'B'),' ',
                        setweight(to_tsvector(new.location_year::varchar),'C'),' ',
                        setweight(to_tsvector(ts.season_code),'C'),' ',
                        setweight(to_tsvector(ts.season_name),'C'),' ',
                        setweight(to_tsvector(new.location_type),'CD'),' ',
                        setweight(to_tsvector(unaccent(tps.person_name)),'CD'),' ',
                        setweight(to_tsvector(unaccent(tp.person_name)),'CD'),' ',
                        setweight(to_tsvector(new.location_status),'CD')
                    ) INTO var_document
                FROM 
                    experiment.LOCATION el
                INNER JOIN
                    place.geospatial_object pgs
                ON
                    new.site_id = pgs.id
                INNER JOIN
                    place.geospatial_object pgf
                ON
                    new.field_id = pgf.id
                INNER JOIN
                    tenant.person tp
                ON
                    new.creator_id = tp.id
                INNER JOIN
                    tenant.person tps
                ON
                    new.steward_id = tps.id
                INNER JOIN
                    tenant.season ts
                ON
                    new.season_id = ts.id;
        
            END IF;
            
            new.location_document = var_document;
        
        END IF;
    
    RETURN NEW;
END;

$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_location() CASCADE;