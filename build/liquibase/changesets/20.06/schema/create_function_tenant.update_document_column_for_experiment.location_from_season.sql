--liquibase formatted sql

--changeset postgres:create_function_tenant.update_document_column_for_experiment.location_from_season context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 tenant.update_document_column_for_experiment.location_from_season



CREATE OR REPLACE FUNCTION tenant.update_document_column_for_experiment_location_from_season()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE experiment.location SET modification_timestamp = now() WHERE season_id = NEW.id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION tenant.update_document_column_for_experiment_location_from_season() CASCADE;