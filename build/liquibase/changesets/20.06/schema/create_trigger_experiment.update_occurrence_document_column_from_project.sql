--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_occurrence_document_column_from_project context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_occurrence_document_column_from_project



CREATE TRIGGER project_update_occurrence_document_from_project_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.project
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_occurrence_from_project();



--rollback DROP TRIGGER project_update_occurrence_document_from_project_tgr ON tenant.project;