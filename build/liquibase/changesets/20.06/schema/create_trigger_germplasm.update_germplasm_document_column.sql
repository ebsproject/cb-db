--liquibase formatted sql

--changeset postgres:create_trigger_germplasm.update_germplasm_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-408 Create trigger germplasm.update_germplasm_document_column



CREATE TRIGGER germplasm_update_germplasm_document_tgr
    BEFORE INSERT OR UPDATE 
    ON germplasm.germplasm
    FOR EACH ROW
    EXECUTE PROCEDURE germplasm.update_document_column_for_germplasm_germplasm();



--rollback DROP TRIGGER germplasm_update_germplasm_document_tgr ON germplasm.germplasm;