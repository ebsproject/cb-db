--liquibase formatted sql

--changeset postgres:create_trigger_germplasm.update_package_document_column_from_germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create trigger germplasm.update_package_document_column_from_germplasm



CREATE TRIGGER germplasm_update_package_document_from_germplasm_tgr
    AFTER INSERT OR UPDATE 
	ON germplasm.germplasm
    FOR EACH ROW
    EXECUTE PROCEDURE germplasm.update_document_column_for_germplasm_package_from_germplasm();



--rollback DROP TRIGGER germplasm_update_package_document_from_germplasm_tgr ON germplasm.germplasm;