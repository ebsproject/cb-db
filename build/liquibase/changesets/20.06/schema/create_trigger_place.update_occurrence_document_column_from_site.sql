--liquibase formatted sql

--changeset postgres:create_trigger_place.update_occurrence_document_column_from_site context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger place.update_occurrence_document_column_from_site



CREATE TRIGGER geospatial_object_update_occurrence_document_from_site_tgr
    AFTER INSERT OR UPDATE 
	ON place.geospatial_object
    FOR EACH ROW
    EXECUTE PROCEDURE place.update_document_column_for_experiment_occurrence_from_site();



--rollback DROP TRIGGER geospatial_object_update_occurrence_document_from_site_tgr ON place.geospatial_object;