--liquibase formatted sql

--changeset postgres:drop_constraint_list_type_check_in_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-472 Drop constraint list_type_chk in platform.list



ALTER TABLE
    platform.list
DROP CONSTRAINT
    list_type_chk;



--rollback ALTER TABLE 
--rollback     platform.list
--rollback ADD CONSTRAINT 
--rollback     list_type_chk 
--rollback CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'designation'::text, 'study'::text, 'variable'::text, 'trait protocol'::text, 'package'::text]));