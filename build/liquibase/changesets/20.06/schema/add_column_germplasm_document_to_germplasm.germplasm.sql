--liquibase formatted sql

--changeset postgres:add_column_germplasm_document_to_germplasm.germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-402 Add column germplasm_document to germplasm.germplasm



ALTER TABLE
	germplasm.germplasm 
ADD COLUMN 
	germplasm_document tsvector;

COMMENT ON COLUMN 
    germplasm.germplasm.germplasm_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';



--rollback ALTER TABLE germplasm.germplasm
--rollback DROP COLUMN germplasm_document;