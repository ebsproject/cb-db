--liquibase formatted sql

--changeset postgres:create_function_place.update_document_column_for_experiment.location_from_site context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function place.update_document_column_for_experiment.location_from_site



CREATE OR REPLACE FUNCTION place.update_document_column_for_experiment_location_from_site()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE experiment.location SET modification_timestamp = now() WHERE site_id = NEW.id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION place.update_document_column_for_experiment_location_from_site() CASCADE;