--liquibase formatted sql

--changeset postgres:drop_table_platform.list_access context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-471 Drop table platform.list_access


-- drop constraints
ALTER TABLE platform.list_access DROP CONSTRAINT list_access_creator_id_fkey;

ALTER TABLE platform.list_access DROP CONSTRAINT list_access_list_id_fkey;

ALTER TABLE platform.list_access DROP CONSTRAINT list_access_modifier_id_fkey;

ALTER TABLE platform.list_access DROP CONSTRAINT list_access_user_id_fkey;


-- drop table
DROP TABLE platform.list_access;



-- revert changes
--rollback CREATE TABLE platform.list_access (
--rollback     id serial NOT NULL,
--rollback     list_id int4 NOT NULL,
--rollback     user_id int4 NOT NULL,
--rollback     "permission" varchar NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--rollback     creator_id int4 NOT NULL,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     event_log jsonb NULL,
--rollback     record_uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
--rollback     CONSTRAINT list_access_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT list_access_permission_chk CHECK (((permission)::text = ANY (ARRAY['read'::text, 'read_write'::text])))
--rollback );
--rollback 
--rollback CREATE INDEX list_access_creator_id_idx ON platform.list_access USING btree (creator_id);
--rollback CREATE INDEX list_access_is_void_idx ON platform.list_access USING btree (is_void);
--rollback CREATE INDEX list_access_list_id_idx ON platform.list_access USING btree (list_id);
--rollback CREATE INDEX list_access_modifier_id_idx ON platform.list_access USING btree (modifier_id);
--rollback CREATE INDEX list_access_record_uuid_idx ON platform.list_access USING btree (record_uuid);
--rollback CREATE INDEX list_access_user_id_idx ON platform.list_access USING btree (user_id);
--rollback 
--rollback ALTER TABLE platform.list_access ADD CONSTRAINT list_access_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--rollback ALTER TABLE platform.list_access ADD CONSTRAINT list_access_list_id_fkey FOREIGN KEY (list_id) REFERENCES platform.list(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--rollback ALTER TABLE platform.list_access ADD CONSTRAINT list_access_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--rollback ALTER TABLE platform.list_access ADD CONSTRAINT list_access_user_id_fkey FOREIGN KEY (user_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--rollback 
--rollback COMMENT ON COLUMN platform.list_access.id IS 'Identifier of the record within the table';
--rollback COMMENT ON COLUMN platform.list_access.list_id IS 'ID of the list. References to platform.list';
--rollback COMMENT ON COLUMN platform.list_access.user_id IS 'ID of the user that the list has been shared to';
--rollback COMMENT ON COLUMN platform.list_access."permission" IS 'Permission given to the user. Can be either read or read_write';
--rollback COMMENT ON COLUMN platform.list_access.remarks IS 'Additional details about the record';
--rollback COMMENT ON COLUMN platform.list_access.creation_timestamp IS 'Timestamp when the record was added to the table';
--rollback COMMENT ON COLUMN platform.list_access.creator_id IS 'ID of the user who added the record to the table';
--rollback COMMENT ON COLUMN platform.list_access.modification_timestamp IS 'Timestamp when the record was last modified';
--rollback COMMENT ON COLUMN platform.list_access.modifier_id IS 'ID of the user who last modified the record';
--rollback COMMENT ON COLUMN platform.list_access.notes IS 'Additional details added by an admin; can be technical or advanced details';
--rollback COMMENT ON COLUMN platform.list_access.is_void IS 'Indicator whether the record is deleted (true) or not (false)';
--rollback COMMENT ON COLUMN platform.list_access.event_log IS 'Historical transactions of the record';
--rollback COMMENT ON COLUMN platform.list_access.record_uuid IS 'Universally unique identifier (UUID) of the record';
