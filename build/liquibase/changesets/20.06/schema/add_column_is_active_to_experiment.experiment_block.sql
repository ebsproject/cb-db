--liquibase formatted sql

--changeset postgres:add_column_is_active_to_experiment.experiment_block context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-620 Add column is_active to experiment.experiment_block



ALTER TABLE 
	experiment.experiment_block 
ADD COLUMN 
	is_active boolean NOT NULL DEFAULT FALSE;
	
COMMENT ON COLUMN 
    experiment.experiment_block.is_active 
IS 
	'Is_Active: Status of the experiment block';



--rollback ALTER TABLE experiment.experiment_block
--rollback DROP COLUMN is_active;