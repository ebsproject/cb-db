--liquibase formatted sql

--changeset postgres:create_trigger_tenant.update_experiment_document_column_from_steward context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-409 Create trigger tenant.update_experiment_document_column_from_steward



CREATE TRIGGER person_steward_update_experiment_document_tgr
    AFTER INSERT OR UPDATE 
	ON tenant.person
    FOR EACH ROW
    EXECUTE PROCEDURE tenant.update_document_column_for_experiment_experiment_from_steward();



--rollback DROP TRIGGER person_steward_update_experiment_document_tgr ON tenant.person;