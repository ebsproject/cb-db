--liquibase formatted sql

--changeset postgres:add_foreign_key_constraint_to_package_id_in_experiment.planting_instruction context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-380 Add foreign key constraint to package_id in experiment.planting_instruction



-- fix data errors
UPDATE experiment.planting_instruction
    SET package_id = NULL
    WHERE package_id IN (
        767137, 767127, 767086, 767123, 767206, 767223, 266905, 767215, 767181, 767056,
        767055, 767131, 767031, 767269, 767120, 767124, 767183, 767225, 767112, 767048,
        767217, 767200, 767088, 767295, 767068, 767283, 767022, 767122, 767017, 767021,
        767204, 767027, 767149, 767192, 767091, 767110, 767148, 767015, 767243, 767081,
        767179, 767032, 767046, 767268, 767153, 767039, 767304, 767156, 767117, 767109,
        767221, 767199
    );


-- add foreign key constraint
ALTER TABLE experiment.planting_instruction
    ADD CONSTRAINT planting_instruction_package_id_fk
    FOREIGN KEY (package_id)
    REFERENCES germplasm.package (id)
    ON UPDATE CASCADE ON DELETE RESTRICT;



-- revert changes
--rollback ALTER TABLE experiment.planting_instruction
--rollback     DROP CONSTRAINT planting_instruction_package_id_fk;

--rollback UPDATE experiment.planting_instruction SET package_id = 266905 WHERE id = 322639;
--rollback UPDATE experiment.planting_instruction SET package_id = 767283 WHERE id = 938232;
--rollback UPDATE experiment.planting_instruction SET package_id = 767295 WHERE id = 938220;
--rollback UPDATE experiment.planting_instruction SET package_id = 767304 WHERE id = 938211;
--rollback UPDATE experiment.planting_instruction SET package_id = 767268 WHERE id = 938247;
--rollback UPDATE experiment.planting_instruction SET package_id = 767269 WHERE id = 938246;
--rollback UPDATE experiment.planting_instruction SET package_id = 767192 WHERE id = 938321;
--rollback UPDATE experiment.planting_instruction SET package_id = 767199 WHERE id = 938314;
--rollback UPDATE experiment.planting_instruction SET package_id = 767200 WHERE id = 938313;
--rollback UPDATE experiment.planting_instruction SET package_id = 767204 WHERE id = 938309;
--rollback UPDATE experiment.planting_instruction SET package_id = 767137 WHERE id = 938377;
--rollback UPDATE experiment.planting_instruction SET package_id = 767148 WHERE id = 938366;
--rollback UPDATE experiment.planting_instruction SET package_id = 767149 WHERE id = 938365;
--rollback UPDATE experiment.planting_instruction SET package_id = 767153 WHERE id = 938361;
--rollback UPDATE experiment.planting_instruction SET package_id = 767081 WHERE id = 938433;
--rollback UPDATE experiment.planting_instruction SET package_id = 767086 WHERE id = 938428;
--rollback UPDATE experiment.planting_instruction SET package_id = 767088 WHERE id = 938426;
--rollback UPDATE experiment.planting_instruction SET package_id = 767091 WHERE id = 938423;
--rollback UPDATE experiment.planting_instruction SET package_id = 767022 WHERE id = 938493;
--rollback UPDATE experiment.planting_instruction SET package_id = 767027 WHERE id = 938488;
--rollback UPDATE experiment.planting_instruction SET package_id = 767031 WHERE id = 938484;
--rollback UPDATE experiment.planting_instruction SET package_id = 767032 WHERE id = 938483;
--rollback UPDATE experiment.planting_instruction SET package_id = 767015 WHERE id = 938783;
--rollback UPDATE experiment.planting_instruction SET package_id = 767017 WHERE id = 938781;
--rollback UPDATE experiment.planting_instruction SET package_id = 767021 WHERE id = 938777;
--rollback UPDATE experiment.planting_instruction SET package_id = 767015 WHERE id = 1107050;
--rollback UPDATE experiment.planting_instruction SET package_id = 767017 WHERE id = 1107051;
--rollback UPDATE experiment.planting_instruction SET package_id = 767021 WHERE id = 1107053;
--rollback UPDATE experiment.planting_instruction SET package_id = 767022 WHERE id = 1107054;
--rollback UPDATE experiment.planting_instruction SET package_id = 767027 WHERE id = 1107056;
--rollback UPDATE experiment.planting_instruction SET package_id = 767068 WHERE id = 1107083;
--rollback UPDATE experiment.planting_instruction SET package_id = 767081 WHERE id = 1107091;
--rollback UPDATE experiment.planting_instruction SET package_id = 767086 WHERE id = 1107095;
--rollback UPDATE experiment.planting_instruction SET package_id = 767088 WHERE id = 1107097;
--rollback UPDATE experiment.planting_instruction SET package_id = 767091 WHERE id = 1107098;
--rollback UPDATE experiment.planting_instruction SET package_id = 767217 WHERE id = 1107139;
--rollback UPDATE experiment.planting_instruction SET package_id = 767221 WHERE id = 1107142;
--rollback UPDATE experiment.planting_instruction SET package_id = 767223 WHERE id = 1107143;
--rollback UPDATE experiment.planting_instruction SET package_id = 767225 WHERE id = 1107144;
--rollback UPDATE experiment.planting_instruction SET package_id = 767243 WHERE id = 1107148;
--rollback UPDATE experiment.planting_instruction SET package_id = 767109 WHERE id = 938405;
--rollback UPDATE experiment.planting_instruction SET package_id = 767110 WHERE id = 938404;
--rollback UPDATE experiment.planting_instruction SET package_id = 767120 WHERE id = 938394;
--rollback UPDATE experiment.planting_instruction SET package_id = 767181 WHERE id = 938332;
--rollback UPDATE experiment.planting_instruction SET package_id = 767243 WHERE id = 938273;
--rollback UPDATE experiment.planting_instruction SET package_id = 767039 WHERE id = 938476;
--rollback UPDATE experiment.planting_instruction SET package_id = 767046 WHERE id = 938469;
--rollback UPDATE experiment.planting_instruction SET package_id = 767048 WHERE id = 938467;
--rollback UPDATE experiment.planting_instruction SET package_id = 767055 WHERE id = 938460;
--rollback UPDATE experiment.planting_instruction SET package_id = 767056 WHERE id = 938459;
--rollback UPDATE experiment.planting_instruction SET package_id = 767068 WHERE id = 938447;
--rollback UPDATE experiment.planting_instruction SET package_id = 767112 WHERE id = 938402;
--rollback UPDATE experiment.planting_instruction SET package_id = 767117 WHERE id = 938397;
--rollback UPDATE experiment.planting_instruction SET package_id = 767122 WHERE id = 938392;
--rollback UPDATE experiment.planting_instruction SET package_id = 767123 WHERE id = 938391;
--rollback UPDATE experiment.planting_instruction SET package_id = 767124 WHERE id = 938390;
--rollback UPDATE experiment.planting_instruction SET package_id = 767127 WHERE id = 938387;
--rollback UPDATE experiment.planting_instruction SET package_id = 767131 WHERE id = 938383;
--rollback UPDATE experiment.planting_instruction SET package_id = 767156 WHERE id = 938358;
--rollback UPDATE experiment.planting_instruction SET package_id = 767179 WHERE id = 938334;
--rollback UPDATE experiment.planting_instruction SET package_id = 767183 WHERE id = 938330;
--rollback UPDATE experiment.planting_instruction SET package_id = 767206 WHERE id = 938307;
--rollback UPDATE experiment.planting_instruction SET package_id = 767215 WHERE id = 938298;
--rollback UPDATE experiment.planting_instruction SET package_id = 767217 WHERE id = 938296;
--rollback UPDATE experiment.planting_instruction SET package_id = 767221 WHERE id = 938292;
--rollback UPDATE experiment.planting_instruction SET package_id = 767223 WHERE id = 938290;
--rollback UPDATE experiment.planting_instruction SET package_id = 767225 WHERE id = 938288;
--rollback UPDATE experiment.planting_instruction SET package_id = 767039 WHERE id = 1107062;
--rollback UPDATE experiment.planting_instruction SET package_id = 767046 WHERE id = 1107069;
--rollback UPDATE experiment.planting_instruction SET package_id = 767156 WHERE id = 1107122;
--rollback UPDATE experiment.planting_instruction SET package_id = 767295 WHERE id = 1107155;
--rollback UPDATE experiment.planting_instruction SET package_id = 767304 WHERE id = 1107156;
--rollback UPDATE experiment.planting_instruction SET package_id = 767031 WHERE id = 1107058;
--rollback UPDATE experiment.planting_instruction SET package_id = 767032 WHERE id = 1107059;
--rollback UPDATE experiment.planting_instruction SET package_id = 767048 WHERE id = 1107071;
--rollback UPDATE experiment.planting_instruction SET package_id = 767055 WHERE id = 1107075;
--rollback UPDATE experiment.planting_instruction SET package_id = 767056 WHERE id = 1107076;
--rollback UPDATE experiment.planting_instruction SET package_id = 767109 WHERE id = 1107103;
--rollback UPDATE experiment.planting_instruction SET package_id = 767110 WHERE id = 1107104;
--rollback UPDATE experiment.planting_instruction SET package_id = 767112 WHERE id = 1107105;
--rollback UPDATE experiment.planting_instruction SET package_id = 767117 WHERE id = 1107108;
--rollback UPDATE experiment.planting_instruction SET package_id = 767120 WHERE id = 1107109;
--rollback UPDATE experiment.planting_instruction SET package_id = 767122 WHERE id = 1107110;
--rollback UPDATE experiment.planting_instruction SET package_id = 767123 WHERE id = 1107111;
--rollback UPDATE experiment.planting_instruction SET package_id = 767124 WHERE id = 1107112;
--rollback UPDATE experiment.planting_instruction SET package_id = 767127 WHERE id = 1107113;
--rollback UPDATE experiment.planting_instruction SET package_id = 767131 WHERE id = 1107115;
--rollback UPDATE experiment.planting_instruction SET package_id = 767137 WHERE id = 1107116;
--rollback UPDATE experiment.planting_instruction SET package_id = 767148 WHERE id = 1107117;
--rollback UPDATE experiment.planting_instruction SET package_id = 767149 WHERE id = 1107118;
--rollback UPDATE experiment.planting_instruction SET package_id = 767153 WHERE id = 1107120;
--rollback UPDATE experiment.planting_instruction SET package_id = 767179 WHERE id = 1107125;
--rollback UPDATE experiment.planting_instruction SET package_id = 767181 WHERE id = 1107126;
--rollback UPDATE experiment.planting_instruction SET package_id = 767183 WHERE id = 1107128;
--rollback UPDATE experiment.planting_instruction SET package_id = 767192 WHERE id = 1107129;
--rollback UPDATE experiment.planting_instruction SET package_id = 767199 WHERE id = 1107131;
--rollback UPDATE experiment.planting_instruction SET package_id = 767200 WHERE id = 1107132;
--rollback UPDATE experiment.planting_instruction SET package_id = 767204 WHERE id = 1107133;
--rollback UPDATE experiment.planting_instruction SET package_id = 767206 WHERE id = 1107135;
--rollback UPDATE experiment.planting_instruction SET package_id = 767215 WHERE id = 1107138;
--rollback UPDATE experiment.planting_instruction SET package_id = 767268 WHERE id = 1107151;
--rollback UPDATE experiment.planting_instruction SET package_id = 767269 WHERE id = 1107152;
--rollback UPDATE experiment.planting_instruction SET package_id = 767283 WHERE id = 1107153;
