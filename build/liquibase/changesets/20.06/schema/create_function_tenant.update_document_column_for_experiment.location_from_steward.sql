--liquibase formatted sql

--changeset postgres:create_function_tenant.update_document_column_for_experiment.location_from_steward context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function tenant.update_document_column_for_experiment.location_from_steward



CREATE OR REPLACE FUNCTION tenant.update_document_column_for_experiment_location_from_steward()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE experiment.location SET modification_timestamp = now() WHERE steward_id = NEW.id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION tenant.update_document_column_for_experiment_location_from_steward() CASCADE;