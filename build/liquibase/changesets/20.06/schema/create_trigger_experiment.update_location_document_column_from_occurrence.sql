--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_location_document_column_from_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create trigger experiment.update_location_document_column_from_occurrence



CREATE TRIGGER occurrence_update_location_document_from_occurrence_tgr
    AFTER INSERT OR UPDATE 
	ON experiment.occurrence
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_experiment_location_from_occurrence();



--rollback DROP TRIGGER occurrence_update_location_document_from_occurrence_tgr ON experiment.occurrence;