--liquibase formatted sql

--changeset postgres:update_constraint_list_type_chk_in_platfrom.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-474 Update constraint list_type_chk in platform.list



ALTER TABLE 
    platform.list
ADD CONSTRAINT 
    list_type_chk 
CHECK (type::text = ANY (ARRAY['seed'::text, 'plot'::text, 'location'::text, 'germplasm'::text, 'study'::text, 'trait'::text, 'trait protocol'::text, 'package'::text]));



--rollback ALTER TABLE
--rollback     platform.list
--rollback DROP CONSTRAINT
--rollback     list_type_chk;