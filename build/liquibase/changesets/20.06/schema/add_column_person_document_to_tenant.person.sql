--liquibase formatted sql

--changeset postgres:add_column_person_document_to_tenant.person context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-405 Add column person_document to tenant.person



ALTER TABLE
	tenant.person 
ADD COLUMN 
	person_document tsvector;

COMMENT ON COLUMN 
    tenant.person.person_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';



--rollback ALTER TABLE tenant.person
--rollback DROP COLUMN person_document;