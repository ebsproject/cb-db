--liquibase formatted sql

--changeset postgres:create_function_tenant.update_document_column_for_tenant.person context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-411 Create function tenant.update_document_column_for_tenant_person



CREATE OR REPLACE FUNCTION tenant.update_document_column_for_tenant_person()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

        	SELECT
				concat(
					setweight(to_tsvector(unaccent(new.person_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.first_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.last_name)),'A'),' ',
					setweight(to_tsvector(new.username),'B'),' ',
					setweight(to_tsvector(new.email),'C'),' ',
					setweight(to_tsvector(new.person_type),'CD'),' ',
					setweight(to_tsvector(new.person_status),'CD')
				) INTO var_document
			FROM 
				tenant.person tp
			WHERE
				tp.id = new.id;
        
        ELSE
			
			SELECT
				concat(
					setweight(to_tsvector(unaccent(new.person_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.first_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.last_name)),'A'),' ',
					setweight(to_tsvector(new.username),'B'),' ',
					setweight(to_tsvector(new.email),'C'),' ',
					setweight(to_tsvector(new.person_type),'CD'),' ',
					setweight(to_tsvector(new.person_status),'CD')
				) INTO var_document
			FROM 
				tenant.person tp;
		
        END IF;

        new.person_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION tenant.update_document_column_for_tenant_person();