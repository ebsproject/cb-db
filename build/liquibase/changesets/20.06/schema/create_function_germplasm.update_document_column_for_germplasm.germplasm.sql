--liquibase formatted sql

--changeset postgres:create_function_germplasm.update_document_column_for_germplasm.germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-408 Create function germplasm.update_document_column_for_germplasm_germplasm



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

			SELECT
				concat(
					--array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name),
					setweight(to_tsvector(unaccent(regexp_replace((array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name)::varchar),'[^a-zA-Y0-9-]',' ','g'))),'A'),' ',
					--array_agg(gn.name_value)
					setweight(to_tsvector(unaccent(regexp_replace((array_agg(gn.name_value order by gn.name_value)::varchar),'[^a-zA-Y0-9-]',' ','g'))),'A'),' ',
					setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
					setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
					setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
					setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
					setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C')
				) INTO var_document
			FROM
				germplasm.germplasm gm
			INNER JOIN
				germplasm.germplasm_name gn
			ON
				gn.germplasm_id = new.id
			WHERE
				gm.id = new.id
			GROUP by
				gn.germplasm_id,
				gm.designation,
				gm.parentage,
				gm.generation,
				gm.germplasm_state,
				gm.germplasm_name_type,
				gm.germplasm_normalized_name,
				new.id;

        ELSE
			
			SELECT
				concat(
					setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
					setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
					setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
					setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
					setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
					setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C')
				) INTO var_document
			FROM
				germplasm.germplasm gm;
			
        END IF;

        new.germplasm_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION germplasm.update_document_column_for_germplasm_germplasm();