--liquibase formatted sql

--changeset postgres:add_column_experiment_document_to_experiment.experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-403 Add column experiment_document to experiment.experiment



ALTER TABLE
	experiment.experiment 
ADD COLUMN 
	experiment_document tsvector;

COMMENT ON COLUMN 
    experiment.experiment.experiment_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';



--rollback ALTER TABLE experiment.experiment
--rollback DROP COLUMN experiment_document;