--liquibase formatted sql

--changeset postgres:create_table_germplasm.package_data context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-380 Create table germplasm.package_data



-- create table
CREATE TABLE germplasm.package_data ( 
    id                   serial  NOT NULL ,
    package_id           integer  NOT NULL ,
    variable_id          integer  NOT NULL ,
    data_value           varchar  NOT NULL ,
    data_qc_code         varchar(8)  NOT NULL ,
    CONSTRAINT pk_package_data_id PRIMARY KEY ( id )
);

ALTER TABLE germplasm.package_data ADD CONSTRAINT package_data_package_id_fk FOREIGN KEY ( package_id ) REFERENCES germplasm.package( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_data_package_id_fk ON germplasm.package_data IS 'A package data refers to one package. A package can have one or many package datas.';

ALTER TABLE germplasm.package_data ADD CONSTRAINT package_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_data_variable_id_fk ON germplasm.package_data IS 'A package data refers to one variable. A variable can be referred by one or many package datas.';

COMMENT ON CONSTRAINT pk_package_data_id ON germplasm.package_data IS 'A package data is identified by its unique database ID.';

CREATE INDEX package_data_package_id_idx ON germplasm.package_data ( package_id );

COMMENT ON INDEX germplasm.package_data_package_id_idx IS 'A package data can be retrieved by its package.';

CREATE INDEX package_data_package_id_variable_id_idx ON germplasm.package_data ( package_id, variable_id );

COMMENT ON INDEX germplasm.package_data_package_id_variable_id_idx IS 'A package data can be retrieved by its variable within a package.';

CREATE INDEX package_data_package_id_variable_id_data_value_idx ON germplasm.package_data ( package_id, variable_id, data_value );

COMMENT ON INDEX germplasm.package_data_package_id_variable_id_data_value_idx IS 'A package data can be retrieved by its variable and data value within a package.';

CREATE INDEX package_data_package_id_data_qc_code_idx ON germplasm.package_data ( package_id, data_qc_code );

COMMENT ON INDEX germplasm.package_data_package_id_data_qc_code_idx IS 'A package data can be retrieved by its data QC code within a package.';

COMMENT ON TABLE germplasm.package_data IS 'Package Data: Data related to the package [PKGDATA]';

COMMENT ON COLUMN germplasm.package_data.id IS 'Package Data ID: Database identifier of the package data [PKGDATA_ID]';

COMMENT ON COLUMN germplasm.package_data.package_id IS 'Package ID: Reference to the package where the data is associated [PKGDATA_PKG_ID]';

COMMENT ON COLUMN germplasm.package_data.variable_id IS 'Variable ID: Reference to the variable of the data [PKGDATA_VAR_ID]';

COMMENT ON COLUMN germplasm.package_data.data_value IS 'Package Data Data Value: Value of the package data [PKGDATA_DATAVAL]';

COMMENT ON COLUMN germplasm.package_data.data_qc_code IS 'Package Data Data QC Code: Quality of the package data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PKGDATA_QCCODE]';

ALTER TABLE IF EXISTS
    germplasm.package_data
ADD COLUMN IF NOT EXISTS
    creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
    creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
    modifier_id integer,
ADD COLUMN IF NOT EXISTS
    modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
    is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
    notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
    event_log jsonb,
ADD FOREIGN KEY
    ( creator_id )
REFERENCES
    tenant.person (id)
ON UPDATE
    CASCADE
ON DELETE
    RESTRICT,
ADD FOREIGN KEY
    ( modifier_id )
REFERENCES
    tenant.person (id)
ON UPDATE
    CASCADE 
ON DELETE
    RESTRICT;

COMMENT ON COLUMN germplasm.package_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [PKGDATA_ISVOID]';
COMMENT ON COLUMN germplasm.package_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [PKGDATA_CTSTAMP]';
COMMENT ON COLUMN germplasm.package_data.creator_id IS 'Creator ID: Reference to the person who created the record [PKGDATA_CPERSON]';
COMMENT ON COLUMN germplasm.package_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [PKGDATA_MTSTAMP]';
COMMENT ON COLUMN germplasm.package_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [PKGDATA_MPERSON]';
COMMENT ON COLUMN germplasm.package_data.notes IS 'Notes: Technical details about the record [PKGDATA_NOTES]';

CREATE INDEX
    package_data_is_void_idx
ON
    germplasm.package_data
USING
    btree ( is_void );

CREATE INDEX
    package_data_creator_id_idx
ON
    germplasm.package_data
USING btree ( creator_id );

CREATE INDEX
    package_data_modifier_id_idx
ON
    germplasm.package_data
USING btree ( modifier_id );

-- audit table
SELECT t.* FROM platform.audit_table('germplasm.package_data') t;



-- revert changes
--rollback DROP TABLE germplasm.package_data;
