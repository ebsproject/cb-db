--liquibase formatted sql

--changeset postgres:create_function_tenant.update_document_column_for_experiment.occurrence_from_creator context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function tenant.update_document_column_for_experiment.occurrence_from_creator



CREATE OR REPLACE FUNCTION tenant.update_document_column_for_experiment_occurrence_from_creator()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		
        UPDATE 
            experiment.occurrence 
        SET 
            modification_timestamp = now() 
        WHERE 
           creator_id = new.id;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION tenant.update_document_column_for_experiment_occurrence_from_creator() CASCADE;