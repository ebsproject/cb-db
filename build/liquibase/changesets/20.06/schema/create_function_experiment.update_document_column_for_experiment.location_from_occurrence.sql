--liquibase formatted sql

--changeset postgres:create_function_experiment.update_document_column_for_experiment.location_from_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Create function experiment.update_document_column_for_experiment.location_from_occurrence



CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_location_from_occurrence()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE 
            experiment.location 
        SET 
            modification_timestamp = now() 
        WHERE 
            id 
        IN 
            (
                SELECT 
                    location_id
                FROM
                    experiment.location_occurrence_group
                WHERE
                    occurrence_id 
                IN 
                    (
                        SELECT 
                            id
                        FROM
                            experiment.occurrence
                        WHERE
                            experiment_id
                        IN
                            (
                                SELECT 
                                    id
                                FROM
                                    experiment.experiment
                                WHERE 
                                    experiment_id = new.experiment_id
                            )
                    )
            );    

    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION experiment.update_document_column_for_experiment_location_from_occurrence() CASCADE;