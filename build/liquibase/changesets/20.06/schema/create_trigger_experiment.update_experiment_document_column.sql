--liquibase formatted sql

--changeset postgres:create_trigger_experiment.update_experiment_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-409 Create trigger experiment.update_experiment_document_column



CREATE TRIGGER experiment_update_experiment_document_tgr
    BEFORE INSERT OR UPDATE 
    ON experiment.experiment
    FOR EACH ROW
    EXECUTE PROCEDURE experiment.update_document_column_for_experiment_experiment();



--rollback DROP TRIGGER experiment_update_experiment_document_tgr ON experiment.experiment;