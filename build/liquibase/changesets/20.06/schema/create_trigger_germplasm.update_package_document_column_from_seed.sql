--liquibase formatted sql

--changeset postgres:create_trigger_germplasm.update_package_document_column_from_seed context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Create trigger germplasm.update_package_document_column_from_seed



CREATE TRIGGER seed_update_package_document_from_seed_tgr
    AFTER INSERT OR UPDATE 
	ON germplasm.seed
    FOR EACH ROW
    EXECUTE PROCEDURE germplasm.update_document_column_for_germplasm_package_from_seed();



--rollback DROP TRIGGER seed_update_package_document_from_seed_tgr ON germplasm.seed;