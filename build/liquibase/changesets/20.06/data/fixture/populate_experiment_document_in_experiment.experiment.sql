--liquibase formatted sql

--changeset postgres:populate_experiment_document_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-403 Populate experiment_document in experiment.experiment



CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT
		ex.id,
		concat(
			setweight(to_tsvector(ex.experiment_code),'A'),' ',
			setweight(to_tsvector(unaccent(ex.experiment_name)),'A'),' ',
			setweight(to_tsvector(ex.experiment_year::text),'B'),' ',
			setweight(to_tsvector(ex.experiment_type),'B'),' ',
			setweight(to_tsvector(ex.experiment_sub_type),'B'),' ',
			setweight(to_tsvector(ex.experiment_sub_sub_type),'B'),' ',
			setweight(to_tsvector(ex.experiment_design_type),'B'),' ',
			setweight(to_tsvector(ex.experiment_status),'B'),' ',
			setweight(to_tsvector(ex.planting_season),'C'),' ',
			setweight(to_tsvector(ts.season_code),'C'),' ',
			setweight(to_tsvector(ts.season_name),'C'),' ',
			setweight(to_tsvector(tst.stage_code),'C'),' ',
			setweight(to_tsvector(tst.stage_name),'C'),' ',
			setweight(to_tsvector(tp.project_code),'CD'),' ',
			setweight(to_tsvector(tp.project_name),'CD'),' ',
			setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
			setweight(to_tsvector(unaccent(tps.person_name)),'CD')
		) AS doc
	FROM
		experiment.experiment ex
	INNER JOIN
		tenant.season ts
	ON
		ex.season_id = ts.id
	INNER JOIN
		tenant.stage tst
	ON
		ex.stage_id = tst.id
	INNER JOIN
		tenant.project tp
	ON
		ex.project_id = tp.id
	INNER JOIN
		tenant.person tpr
	ON
		ex.steward_id = tpr.id
	INNER JOIN
		tenant.person tps
	ON 
		ex.creator_id = tps.id
	ORDER BY
		ex.id
)

UPDATE experiment.experiment ex SET experiment_document = cast(t1.doc AS tsvector) FROM t1 WHERE ex.id = t1.id;



--rollback UPDATE experiment.experiment SET experiment_document = NULL;