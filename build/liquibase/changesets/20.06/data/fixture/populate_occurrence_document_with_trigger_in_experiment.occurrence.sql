--liquibase formatted sql

--changeset postgres:populate_occurrence_document_with_trigger_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Populate occurrence_document in experiment.occurrence



UPDATE 
    experiment.occurrence
SET
    modification_timestamp = now();



--rollback SELECT NULL;