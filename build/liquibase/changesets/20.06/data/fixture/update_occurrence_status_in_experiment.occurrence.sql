--liquibase formatted sql

--changeset postgres:update_occurrence_status_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-391 Update occurrence_status in experiment.occurrence



UPDATE
    experiment.occurrence
SET
    occurrence_status = 'committed'
WHERE
    occurrence_status = 'mapped';



--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_status = 'mapped'
--rollback WHERE
--rollback     occurrence_status = 'committed';