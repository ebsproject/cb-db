--liquibase formatted sql

--changeset postgres:populate_package_document_in_germplasm.package context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-404 Populate package_document in germplasm.package



CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		gp.id,
		concat(
			setweight(to_tsvector(unaccent(gs.seed_code)),'A'),' ',
			setweight(to_tsvector(unaccent(gs.seed_name)),'A'),' ',
			setweight(to_tsvector(gs.harvest_method),'B'),' ',
			setweight(to_tsvector(gs.harvest_date::text),'C'),' ',
			setweight(to_tsvector(unaccent(gs.description)),'CD')
		) AS doc
	FROM 
		germplasm.package gp
	INNER JOIN
		germplasm.seed gs
	ON
		gs.id = gp.seed_id
	ORDER BY
		gp.id
)

UPDATE germplasm.package gp SET package_document = cast(t1.doc AS tsvector) FROM t1 WHERE gp.id = t1.id;



--rollback UPDATE germplasm.package SET package_document = NULL;