--liquibase formatted sql

--changeset postgres:populate_access_data_in_platform.list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-470 Populate access_data in platform.list



DO $$

DECLARE
	var_list_record record;
	var_role_id_data_owner integer;
	var_role_id_data_producer integer;
	var_creator_id integer;
	var_team_id integer;
	var_team_member record;
	var_team_member_id integer;
	var_data_owner_json text;
	var_data_producer_json text;
	var_child_json text;
	var_concatenated_data_producer_json text;
	var_concatenated_user text;
	var_json text;
	
BEGIN

	--get role id of data owner
	SELECT id FROM tenant.person_role WHERE person_role_code = 'DATA_OWNER' INTO var_role_id_data_owner;
	--RAISE NOTICE '!DATA_OWNER ID %', var_role_id_data_owner; 
	
	--get role id of data producer
	SELECT id FROM tenant.person_role WHERE person_role_code = 'DATA_PRODUCER' INTO var_role_id_data_producer;
	--RAISE NOTICE '!DATA_PRODUCER ID %', var_role_id_data_producer; 
	
	FOR var_list_record IN (
		SELECT * FROM platform.list WHERE is_void=FALSE ORDER BY id
	)
	LOOP
	
		--get creator_id
		var_creator_id = var_list_record.creator_id;
		--RAISE NOTICE '!CREATOR ID %', var_creator_id; 
		
		var_data_owner_json = '"'||var_creator_id||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_owner||'}';
		--RAISE NOTICE '!DATA OWNER JSON %', var_data_owner_json; 
		
		--get team_id
		SELECT team_id INTO var_team_id FROM tenant.team_member WHERE person_id = var_creator_id;
		--RAISE NOTICE '!TEAM ID %', var_team_id; 
		
		--get team_member of a team
		FOR var_team_member IN (
			SELECT person_id FROM tenant.team_member WHERE team_id=var_team_id AND person_id NOT IN (var_creator_id) AND is_void=FALSE
		)
		LOOP
			--RAISE NOTICE '--TEAM MEMBER ID %', var_team_member.person_id; 
			var_team_member_id = var_team_member.person_id; 
			var_data_producer_json = '"'||var_team_member_id||'": {"addedBy": 1,"addedOn": "'||now()||'","dataRoleId": '||var_role_id_data_producer||'}';
			
			--RAISE NOTICE '!DATA PRODUCER JSON %', var_data_producer_json; 
			
			var_concatenated_data_producer_json = concat(var_concatenated_data_producer_json,var_data_producer_json,',');
			--RAISE NOTICE '!CONCATENATED DATA PRODUCER JSON %', var_concatenated_data_producer_json; 
			
		END LOOP;
	
		var_concatenated_user = concat(var_data_owner_json,',',var_concatenated_data_producer_json);
		--RAISE NOTICE '!CONCATENATED USER %', var_concatenated_user; 
	
		var_json = concat('{"user":{',left(var_concatenated_user,length(var_concatenated_user)-1),'}}');
		--RAISE NOTICE '!ALL USER % %', var_list_record.id, var_json; 
	
		UPDATE platform.list SET access_data=var_json::json WHERE id = var_list_record.id;
	
		var_concatenated_data_producer_json = '';
		var_concatenated_user = '';
		var_json = '';
		
	END LOOP;

END;
$$



--rollback UPDATE platform.list SET access_data=NULL;