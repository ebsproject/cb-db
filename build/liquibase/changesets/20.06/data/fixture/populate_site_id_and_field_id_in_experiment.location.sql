--liquibase formatted sql

--changeset postgres:populate_site_id_and_field_id_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-390 Populate site_id and field_id in experiment.location



UPDATE experiment.location SET site_id=10001, field_id=71 WHERE id=71;
UPDATE experiment.location SET site_id=10001, field_id=72 WHERE id=72;
UPDATE experiment.location SET site_id=10001, field_id=73 WHERE id=73;
UPDATE experiment.location SET site_id=10001, field_id=74 WHERE id=74;
UPDATE experiment.location SET site_id=10001, field_id=164 WHERE id=164;
UPDATE experiment.location SET site_id=10001, field_id=170 WHERE id=170;
UPDATE experiment.location SET site_id=10001, field_id=205 WHERE id=205;
UPDATE experiment.location SET site_id=10001, field_id=206 WHERE id=206;
UPDATE experiment.location SET site_id=10001, field_id=228 WHERE id=228;
UPDATE experiment.location SET site_id=10001, field_id=230 WHERE id=230;
UPDATE experiment.location SET site_id=10001, field_id=231 WHERE id=231;
UPDATE experiment.location SET site_id=10001, field_id=232 WHERE id=232;
UPDATE experiment.location SET site_id=10001, field_id=233 WHERE id=233;
UPDATE experiment.location SET site_id=10001, field_id=234 WHERE id=234;
UPDATE experiment.location SET site_id=10001, field_id=235 WHERE id=235;
UPDATE experiment.location SET site_id=10001, field_id=236 WHERE id=236;
UPDATE experiment.location SET site_id=10001, field_id=323 WHERE id=323;
UPDATE experiment.location SET site_id=10001, field_id=340 WHERE id=340;
UPDATE experiment.location SET site_id=10001, field_id=359 WHERE id=359;
UPDATE experiment.location SET site_id=10001, field_id=375 WHERE id=375;
UPDATE experiment.location SET site_id=10001, field_id=378 WHERE id=378;
UPDATE experiment.location SET site_id=10001, field_id=385 WHERE id=385;
UPDATE experiment.location SET site_id=10001, field_id=390 WHERE id=390;
UPDATE experiment.location SET site_id=10001, field_id=421 WHERE id=421;
UPDATE experiment.location SET site_id=10001, field_id=463 WHERE id=463;
UPDATE experiment.location SET site_id=10001, field_id=473 WHERE id=473;
UPDATE experiment.location SET site_id=10001, field_id=511 WHERE id=511;
UPDATE experiment.location SET site_id=10001, field_id=524 WHERE id=524;
UPDATE experiment.location SET site_id=10001, field_id=557 WHERE id=557;
UPDATE experiment.location SET site_id=10001, field_id=698 WHERE id=698;
UPDATE experiment.location SET site_id=10001, field_id=700 WHERE id=700;
UPDATE experiment.location SET site_id=10001, field_id=705 WHERE id=705;
UPDATE experiment.location SET site_id=10001, field_id=706 WHERE id=706;
UPDATE experiment.location SET site_id=10001, field_id=714 WHERE id=714;
UPDATE experiment.location SET site_id=10001, field_id=715 WHERE id=715;
UPDATE experiment.location SET site_id=10001, field_id=716 WHERE id=716;
UPDATE experiment.location SET site_id=10001, field_id=720 WHERE id=720;
UPDATE experiment.location SET site_id=10001, field_id=721 WHERE id=721;
UPDATE experiment.location SET site_id=10001, field_id=737 WHERE id=737;
UPDATE experiment.location SET site_id=10001, field_id=804 WHERE id=804;
UPDATE experiment.location SET site_id=10001, field_id=815 WHERE id=815;
UPDATE experiment.location SET site_id=10001, field_id=816 WHERE id=816;
UPDATE experiment.location SET site_id=10017, field_id=821 WHERE id=821;
UPDATE experiment.location SET site_id=10017, field_id=826 WHERE id=826;
UPDATE experiment.location SET site_id=10001, field_id=836 WHERE id=836;
UPDATE experiment.location SET site_id=10001, field_id=843 WHERE id=843;
UPDATE experiment.location SET site_id=10001, field_id=885 WHERE id=885;
UPDATE experiment.location SET site_id=10001, field_id=893 WHERE id=893;
UPDATE experiment.location SET site_id=10001, field_id=894 WHERE id=894;
UPDATE experiment.location SET site_id=10001, field_id=895 WHERE id=895;
UPDATE experiment.location SET site_id=10001, field_id=903 WHERE id=903;
UPDATE experiment.location SET site_id=10001, field_id=907 WHERE id=907;
UPDATE experiment.location SET site_id=10001, field_id=916 WHERE id=916;
UPDATE experiment.location SET site_id=10001, field_id=919 WHERE id=919;
UPDATE experiment.location SET site_id=10001, field_id=1003 WHERE id=1003;
UPDATE experiment.location SET site_id=10001, field_id=1004 WHERE id=1004;
UPDATE experiment.location SET site_id=10001, field_id=1005 WHERE id=1005;
UPDATE experiment.location SET site_id=10001, field_id=1015 WHERE id=1015;
UPDATE experiment.location SET site_id=10001, field_id=1016 WHERE id=1016;
UPDATE experiment.location SET site_id=10001, field_id=1017 WHERE id=1017;
UPDATE experiment.location SET site_id=10001, field_id=1028 WHERE id=1028;
UPDATE experiment.location SET site_id=10001, field_id=1036 WHERE id=1036;
UPDATE experiment.location SET site_id=10001, field_id=1037 WHERE id=1037;
UPDATE experiment.location SET site_id=10001, field_id=1047 WHERE id=1047;
UPDATE experiment.location SET site_id=10001, field_id=1061 WHERE id=1061;
UPDATE experiment.location SET site_id=10001, field_id=1073 WHERE id=1073;
UPDATE experiment.location SET site_id=10001, field_id=1085 WHERE id=1085;
UPDATE experiment.location SET site_id=10001, field_id=1099 WHERE id=1099;
UPDATE experiment.location SET site_id=10001, field_id=1100 WHERE id=1100;
UPDATE experiment.location SET site_id=10001, field_id=1105 WHERE id=1105;
UPDATE experiment.location SET site_id=10001, field_id=1110 WHERE id=1110;
UPDATE experiment.location SET site_id=10001, field_id=1114 WHERE id=1114;
UPDATE experiment.location SET site_id=10001, field_id=1115 WHERE id=1115;
UPDATE experiment.location SET site_id=10001, field_id=1124 WHERE id=1124;
UPDATE experiment.location SET site_id=10001, field_id=1125 WHERE id=1125;
UPDATE experiment.location SET site_id=10001, field_id=1129 WHERE id=1129;
UPDATE experiment.location SET site_id=10001, field_id=1131 WHERE id=1131;
UPDATE experiment.location SET site_id=10003, field_id=1191 WHERE id=1191;
UPDATE experiment.location SET site_id=10005, field_id=1193 WHERE id=1193;
UPDATE experiment.location SET site_id=10005, field_id=1200 WHERE id=1200;
UPDATE experiment.location SET site_id=10003, field_id=1201 WHERE id=1201;
UPDATE experiment.location SET site_id=10002, field_id=1217 WHERE id=1217;
UPDATE experiment.location SET site_id=10002, field_id=1218 WHERE id=1218;
UPDATE experiment.location SET site_id=10001, field_id=1313 WHERE id=1313;
UPDATE experiment.location SET site_id=10001, field_id=1314 WHERE id=1314;
UPDATE experiment.location SET site_id=10001, field_id=1358 WHERE id=1358;
UPDATE experiment.location SET site_id=10001, field_id=1360 WHERE id=1360;
UPDATE experiment.location SET site_id=10001, field_id=1361 WHERE id=1361;
UPDATE experiment.location SET site_id=10001, field_id=1362 WHERE id=1362;
UPDATE experiment.location SET site_id=10010, field_id=1367 WHERE id=1367;
UPDATE experiment.location SET site_id=10009, field_id=1377 WHERE id=1377;
UPDATE experiment.location SET site_id=10002, field_id=1378 WHERE id=1378;
UPDATE experiment.location SET site_id=10009, field_id=1380 WHERE id=1380;
UPDATE experiment.location SET site_id=10002, field_id=1381 WHERE id=1381;
UPDATE experiment.location SET site_id=10017, field_id=1382 WHERE id=1382;
UPDATE experiment.location SET site_id=10017, field_id=1383 WHERE id=1383;
UPDATE experiment.location SET site_id=10017, field_id=1384 WHERE id=1384;
UPDATE experiment.location SET site_id=10012, field_id=1389 WHERE id=1389;
UPDATE experiment.location SET site_id=10017, field_id=1396 WHERE id=1396;
UPDATE experiment.location SET site_id=10012, field_id=1397 WHERE id=1397;
UPDATE experiment.location SET site_id=10001, field_id=1405 WHERE id=1405;
UPDATE experiment.location SET site_id=10017, field_id=1408 WHERE id=1408;
UPDATE experiment.location SET site_id=10001, field_id=1411 WHERE id=1411;
UPDATE experiment.location SET site_id=10001, field_id=1412 WHERE id=1412;
UPDATE experiment.location SET site_id=10001, field_id=1413 WHERE id=1413;
UPDATE experiment.location SET site_id=10001, field_id=1414 WHERE id=1414;
UPDATE experiment.location SET site_id=10010, field_id=1489 WHERE id=1489;
UPDATE experiment.location SET site_id=10001, field_id=1491 WHERE id=1491;
UPDATE experiment.location SET site_id=10001, field_id=1504 WHERE id=1504;
UPDATE experiment.location SET site_id=10001, field_id=1507 WHERE id=1507;
UPDATE experiment.location SET site_id=10001, field_id=1508 WHERE id=1508;
UPDATE experiment.location SET site_id=10001, field_id=1520 WHERE id=1520;
UPDATE experiment.location SET site_id=10001, field_id=1521 WHERE id=1521;
UPDATE experiment.location SET site_id=10001, field_id=1527 WHERE id=1527;
UPDATE experiment.location SET site_id=10001, field_id=1540 WHERE id=1540;
UPDATE experiment.location SET site_id=10001, field_id=1567 WHERE id=1567;
UPDATE experiment.location SET site_id=10001, field_id=1572 WHERE id=1572;
UPDATE experiment.location SET site_id=10001, field_id=1603 WHERE id=1603;
UPDATE experiment.location SET site_id=10001, field_id=1621 WHERE id=1621;
UPDATE experiment.location SET site_id=10001, field_id=1697 WHERE id=1697;
UPDATE experiment.location SET site_id=10001, field_id=1746 WHERE id=1746;
UPDATE experiment.location SET site_id=10001, field_id=1763 WHERE id=1763;
UPDATE experiment.location SET site_id=10001, field_id=1764 WHERE id=1764;
UPDATE experiment.location SET site_id=10001, field_id=1765 WHERE id=1765;
UPDATE experiment.location SET site_id=10001, field_id=1766 WHERE id=1766;
UPDATE experiment.location SET site_id=10001, field_id=1769 WHERE id=1769;
UPDATE experiment.location SET site_id=10001, field_id=1771 WHERE id=1771;
UPDATE experiment.location SET site_id=10001, field_id=1785 WHERE id=1785;
UPDATE experiment.location SET site_id=10001, field_id=1786 WHERE id=1786;
UPDATE experiment.location SET site_id=10001, field_id=1788 WHERE id=1788;
UPDATE experiment.location SET site_id=10001, field_id=1789 WHERE id=1789;
UPDATE experiment.location SET site_id=10001, field_id=1792 WHERE id=1792;
UPDATE experiment.location SET site_id=10002, field_id=1793 WHERE id=1793;
UPDATE experiment.location SET site_id=10009, field_id=1794 WHERE id=1794;
UPDATE experiment.location SET site_id=10002, field_id=1796 WHERE id=1796;
UPDATE experiment.location SET site_id=10001, field_id=1797 WHERE id=1797;
UPDATE experiment.location SET site_id=10001, field_id=1798 WHERE id=1798;
UPDATE experiment.location SET site_id=10017, field_id=1799 WHERE id=1799;
UPDATE experiment.location SET site_id=10017, field_id=1800 WHERE id=1800;
UPDATE experiment.location SET site_id=10017, field_id=1801 WHERE id=1801;
UPDATE experiment.location SET site_id=10001, field_id=1822 WHERE id=1822;
UPDATE experiment.location SET site_id=10001, field_id=1825 WHERE id=1825;
UPDATE experiment.location SET site_id=10001, field_id=1840 WHERE id=1840;
UPDATE experiment.location SET site_id=10001, field_id=1841 WHERE id=1841;
UPDATE experiment.location SET site_id=10001, field_id=1855 WHERE id=1855;
UPDATE experiment.location SET site_id=10001, field_id=1861 WHERE id=1861;
UPDATE experiment.location SET site_id=10009, field_id=1868 WHERE id=1868;
UPDATE experiment.location SET site_id=10002, field_id=1888 WHERE id=1888;
UPDATE experiment.location SET site_id=10017, field_id=1889 WHERE id=1889;
UPDATE experiment.location SET site_id=10001, field_id=1955 WHERE id=1955;
UPDATE experiment.location SET site_id=10001, field_id=1956 WHERE id=1956;
UPDATE experiment.location SET site_id=10005, field_id=1981 WHERE id=1981;
UPDATE experiment.location SET site_id=10005, field_id=1982 WHERE id=1982;
UPDATE experiment.location SET site_id=10003, field_id=1988 WHERE id=1988;
UPDATE experiment.location SET site_id=10003, field_id=1989 WHERE id=1989;
UPDATE experiment.location SET site_id=10002, field_id=1990 WHERE id=1990;
UPDATE experiment.location SET site_id=10002, field_id=1991 WHERE id=1991;
UPDATE experiment.location SET site_id=10001, field_id=2016 WHERE id=2016;
UPDATE experiment.location SET site_id=10001, field_id=2026 WHERE id=2026;
UPDATE experiment.location SET site_id=10001, field_id=2059 WHERE id=2059;
UPDATE experiment.location SET site_id=10001, field_id=2125 WHERE id=2125;
UPDATE experiment.location SET site_id=10001, field_id=2134 WHERE id=2134;
UPDATE experiment.location SET site_id=10001, field_id=2163 WHERE id=2163;
UPDATE experiment.location SET site_id=10001, field_id=2166 WHERE id=2166;
UPDATE experiment.location SET site_id=10001, field_id=2177 WHERE id=2177;
UPDATE experiment.location SET site_id=10001, field_id=2178 WHERE id=2178;
UPDATE experiment.location SET site_id=10001, field_id=2179 WHERE id=2179;
UPDATE experiment.location SET site_id=10001, field_id=2181 WHERE id=2181;
UPDATE experiment.location SET site_id=10001, field_id=2183 WHERE id=2183;
UPDATE experiment.location SET site_id=10001, field_id=2184 WHERE id=2184;
UPDATE experiment.location SET site_id=10001, field_id=2221 WHERE id=2221;
UPDATE experiment.location SET site_id=10001, field_id=2223 WHERE id=2223;
UPDATE experiment.location SET site_id=10001, field_id=2224 WHERE id=2224;
UPDATE experiment.location SET site_id=10001, field_id=2225 WHERE id=2225;
UPDATE experiment.location SET site_id=10001, field_id=2226 WHERE id=2226;
UPDATE experiment.location SET site_id=10001, field_id=2227 WHERE id=2227;
UPDATE experiment.location SET site_id=10001, field_id=2232 WHERE id=2232;
UPDATE experiment.location SET site_id=10001, field_id=2233 WHERE id=2233;
UPDATE experiment.location SET site_id=10001, field_id=2235 WHERE id=2235;
UPDATE experiment.location SET site_id=10001, field_id=2247 WHERE id=2247;
UPDATE experiment.location SET site_id=10001, field_id=2248 WHERE id=2248;
UPDATE experiment.location SET site_id=10001, field_id=2249 WHERE id=2249;
UPDATE experiment.location SET site_id=10001, field_id=2254 WHERE id=2254;
UPDATE experiment.location SET site_id=10001, field_id=2255 WHERE id=2255;
UPDATE experiment.location SET site_id=10010, field_id=2312 WHERE id=2312;
UPDATE experiment.location SET site_id=10002, field_id=2313 WHERE id=2313;
UPDATE experiment.location SET site_id=10002, field_id=2314 WHERE id=2314;
UPDATE experiment.location SET site_id=10001, field_id=2323 WHERE id=2323;
UPDATE experiment.location SET site_id=10001, field_id=2324 WHERE id=2324;
UPDATE experiment.location SET site_id=10001, field_id=2332 WHERE id=2332;
UPDATE experiment.location SET site_id=10001, field_id=2335 WHERE id=2335;
UPDATE experiment.location SET site_id=10001, field_id=2336 WHERE id=2336;
UPDATE experiment.location SET site_id=10001, field_id=2338 WHERE id=2338;
UPDATE experiment.location SET site_id=10001, field_id=2359 WHERE id=2359;
UPDATE experiment.location SET site_id=10002, field_id=2412 WHERE id=2412;
UPDATE experiment.location SET site_id=10001, field_id=2441 WHERE id=2441;
UPDATE experiment.location SET site_id=10001, field_id=2442 WHERE id=2442;
UPDATE experiment.location SET site_id=10001, field_id=2443 WHERE id=2443;
UPDATE experiment.location SET site_id=10001, field_id=2445 WHERE id=2445;
UPDATE experiment.location SET site_id=10001, field_id=2446 WHERE id=2446;
UPDATE experiment.location SET site_id=10001, field_id=2449 WHERE id=2449;
UPDATE experiment.location SET site_id=10003, field_id=2465 WHERE id=2465;
UPDATE experiment.location SET site_id=10003, field_id=2466 WHERE id=2466;
UPDATE experiment.location SET site_id=10002, field_id=2467 WHERE id=2467;
UPDATE experiment.location SET site_id=10002, field_id=2468 WHERE id=2468;
UPDATE experiment.location SET site_id=10001, field_id=2527 WHERE id=2527;
UPDATE experiment.location SET site_id=10001, field_id=2531 WHERE id=2531;
UPDATE experiment.location SET site_id=10001, field_id=2608 WHERE id=2608;
UPDATE experiment.location SET site_id=10001, field_id=2632 WHERE id=2632;
UPDATE experiment.location SET site_id=10001, field_id=2633 WHERE id=2633;
UPDATE experiment.location SET site_id=10001, field_id=2634 WHERE id=2634;
UPDATE experiment.location SET site_id=10001, field_id=2666 WHERE id=2666;
UPDATE experiment.location SET site_id=10001, field_id=2674 WHERE id=2674;
UPDATE experiment.location SET site_id=10001, field_id=2675 WHERE id=2675;
UPDATE experiment.location SET site_id=10001, field_id=2676 WHERE id=2676;
UPDATE experiment.location SET site_id=10002, field_id=2701 WHERE id=2701;
UPDATE experiment.location SET site_id=10003, field_id=2702 WHERE id=2702;
UPDATE experiment.location SET site_id=10001, field_id=2707 WHERE id=2707;
UPDATE experiment.location SET site_id=10002, field_id=2738 WHERE id=2738;
UPDATE experiment.location SET site_id=10002, field_id=2739 WHERE id=2739;
UPDATE experiment.location SET site_id=10017, field_id=2740 WHERE id=2740;
UPDATE experiment.location SET site_id=10017, field_id=2741 WHERE id=2741;
UPDATE experiment.location SET site_id=10001, field_id=2795 WHERE id=2795;
UPDATE experiment.location SET site_id=10001, field_id=2797 WHERE id=2797;
UPDATE experiment.location SET site_id=10001, field_id=2837 WHERE id=2837;
UPDATE experiment.location SET site_id=10001, field_id=2842 WHERE id=2842;
UPDATE experiment.location SET site_id=10001, field_id=2846 WHERE id=2846;
UPDATE experiment.location SET site_id=10001, field_id=2918 WHERE id=2918;
UPDATE experiment.location SET site_id=10001, field_id=2929 WHERE id=2929;
UPDATE experiment.location SET site_id=10001, field_id=2932 WHERE id=2932;
UPDATE experiment.location SET site_id=10001, field_id=2939 WHERE id=2939;
UPDATE experiment.location SET site_id=10001, field_id=2942 WHERE id=2942;
UPDATE experiment.location SET site_id=10001, field_id=3024 WHERE id=3024;
UPDATE experiment.location SET site_id=10001, field_id=3026 WHERE id=3026;
UPDATE experiment.location SET site_id=10001, field_id=3028 WHERE id=3028;
UPDATE experiment.location SET site_id=10001, field_id=3030 WHERE id=3030;
UPDATE experiment.location SET site_id=10001, field_id=3040 WHERE id=3040;
UPDATE experiment.location SET site_id=10001, field_id=3041 WHERE id=3041;
UPDATE experiment.location SET site_id=10001, field_id=3042 WHERE id=3042;
UPDATE experiment.location SET site_id=10001, field_id=3043 WHERE id=3043;
UPDATE experiment.location SET site_id=10001, field_id=3044 WHERE id=3044;
UPDATE experiment.location SET site_id=10001, field_id=3370 WHERE id=3370;
UPDATE experiment.location SET site_id=10001, field_id=3371 WHERE id=3371;
UPDATE experiment.location SET site_id=10001, field_id=3373 WHERE id=3373;
UPDATE experiment.location SET site_id=10001, field_id=3374 WHERE id=3374;
UPDATE experiment.location SET site_id=10001, field_id=3375 WHERE id=3375;
UPDATE experiment.location SET site_id=10001, field_id=3376 WHERE id=3376;



--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=71;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=72;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=73;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=74;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=164;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=170;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=205;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=206;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=228;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=230;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=231;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=232;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=233;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=234;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=235;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=236;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=323;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=340;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=359;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=375;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=378;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=385;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=390;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=421;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=463;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=473;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=511;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=524;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=557;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=698;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=700;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=705;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=706;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=714;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=715;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=716;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=720;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=721;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=737;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=804;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=815;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=816;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=821;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=826;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=836;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=843;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=885;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=893;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=894;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=895;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=903;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=907;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=916;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=919;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1003;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1004;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1005;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1015;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1016;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1017;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1028;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1036;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1037;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1047;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1061;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1073;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1085;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1099;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1100;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1105;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1110;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1114;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1115;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1124;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1125;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1129;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1131;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1191;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1193;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1200;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1201;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1217;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1218;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1313;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1314;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1358;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1360;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1361;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1362;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1367;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1377;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1378;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1380;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1381;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1382;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1383;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1384;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1389;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1396;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1397;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1405;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1408;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1411;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1412;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1413;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1414;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1489;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1491;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1504;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1507;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1508;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1520;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1521;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1527;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1540;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1567;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1572;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1603;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1621;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1697;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1746;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1763;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1764;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1765;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1766;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1769;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1771;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1785;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1786;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1788;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1789;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1792;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1793;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1794;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1796;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1797;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1798;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1799;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1800;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1801;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1822;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1825;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1840;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1841;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1855;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1861;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1868;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1888;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1889;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1955;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1956;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1981;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1982;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1988;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1989;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1990;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=1991;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2016;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2026;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2059;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2125;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2134;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2163;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2166;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2177;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2178;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2179;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2181;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2183;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2184;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2221;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2223;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2224;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2225;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2226;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2227;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2232;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2233;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2235;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2247;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2248;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2249;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2254;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2255;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2312;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2313;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2314;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2323;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2324;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2332;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2335;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2336;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2338;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2359;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2412;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2441;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2442;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2443;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2445;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2446;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2449;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2465;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2466;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2467;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2468;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2527;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2531;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2608;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2632;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2633;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2634;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2666;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2674;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2675;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2676;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2701;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2702;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2707;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2738;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2739;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2740;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2741;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2795;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2797;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2837;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2842;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2846;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2918;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2929;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2932;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2939;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=2942;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3024;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3026;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3028;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3030;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3040;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3041;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3042;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3043;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3044;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3370;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3371;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3373;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3374;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3375;
--rollback UPDATE experiment.location SET site_id=NULL, field_id=NULL WHERE id=3376;