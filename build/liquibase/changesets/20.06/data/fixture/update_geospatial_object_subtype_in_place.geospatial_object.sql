--liquibase formatted sql

--changeset postgres:update_geospatial_object_subtype_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-541 Update geospatial_object_subtype in place.geospatial_object



UPDATE
    place.geospatial_object
SET
    geospatial_object_subtype = 'breeding location'
WHERE
    geospatial_object_subtype = 'beeding_location';



--rollback SELECT NULL;