--liquibase formatted sql

--changeset postgres:populate_location_document_with_trigger_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-571 Populate location_document in experiment.location



UPDATE 
    experiment.location
SET
    modification_timestamp = now();



--rollback SELECT NULL;