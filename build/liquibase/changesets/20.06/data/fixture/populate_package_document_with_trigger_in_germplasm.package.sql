--liquibase formatted sql

--changeset postgres:populate_package_document_with_trigger_in_germplasm.package context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Populate package_document in germplasm.package



UPDATE 
    germplasm.package
SET
    modification_timestamp = now();



--rollback SELECT NULL;