--liquibase formatted sql

--changeset postgres:curate_experiment_design_type_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-721 Curate experiment_design_type in experiment.experiment



UPDATE
    experiment.experiment
SET
    experiment_design_type = 'Systematic Arrangement'
WHERE
    experiment_design_type = 'Systematic arrangement';

UPDATE
    experiment.experiment
SET
    experiment_design_type = 'Pedigree Order'
WHERE
    experiment_design_type = 'Pedigree order';



--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_design_type = 'Systematic arrangement'
--rollback WHERE
--rollback     experiment_design_type = 'Systematic Arrangement';
--rollback 
--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_design_type = 'Pedigree order'
--rollback WHERE
--rollback     experiment_design_type = 'Pedigree Order';

