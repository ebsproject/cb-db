--liquibase formatted sql

--changeset postgres:add_seed_increase_irri_template_to_master.item_relation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Add seed increase IRRI template to master.item_relation



INSERT INTO 
    master.item_relation
    (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS'
    ) AS root_id,
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS'
    ) AS parent_id,
    id as child_id,
    CASE 
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT' THEN 1
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT' THEN 2
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT' THEN 3
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT' THEN 4
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_PLACE_ACT' THEN 5
        WHEN abbrev = 'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT' THEN 6
    ELSE 
        7 
    END AS order_number,
    0,
    1,
    1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    (
        'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT',
        'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT',
        'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT',
        'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT',
        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT',
        'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT'
    );



--rollback DELETE FROM
--rollback     master.item_relation 
--rollback WHERE
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS'
--rollback     )
--rollback AND 
--rollback     parent_id 
--rollback IN
--rollback     (
--rollback          SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SEED_INCREASE_IRRI_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE
--rollback 	        abbrev 
--rollback         IN 
--rollback         (
--rollback             'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT',
--rollback             'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT',
--rollback             'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT',
--rollback             'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT',
--rollback             'EXPT_SEED_INCREASE_IRRI_PLACE_ACT',
--rollback             'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT'
--rollback         )
--rollback );