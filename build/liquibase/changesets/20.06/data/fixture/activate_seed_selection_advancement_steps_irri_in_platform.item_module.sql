--liquibase formatted sql

--changeset postgres:activate_seed_selection_advancement_steps_irri_in_platform.module context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Activate seed selection steps IRRI in platform.item_module



UPDATE 
    platform.item_module 
SET 
    is_void = false 
WHERE
    item_id  
IN 
    (
        SELECT  
            id
        FROM
            master.item
        WHERE
            abbrev
        IN
            (
                'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT'      
            )
    )
AND
    module_id
IN
    (
        SELECT
            id
        FROM
            platform.module
        WHERE
            abbrev
        IN
            (
                'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_MOD',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT_MOD',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_MOD',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT_MOD',
                'EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT_MOD'
            )
    );



--rollback UPDATE 
--rollback     platform.item_module 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE
--rollback     item_id  
--rollback IN 
--rollback     (
--rollback         SELECT  
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT'      
--rollback             )
--rollback     )
--rollback AND
--rollback     module_id
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             platform.module
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_MOD',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT_MOD',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_MOD',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT_MOD',
--rollback                 'EXPT_SELECTION_ADVANCEMENT_IRRI_REVIEW_ACT_MOD'
--rollback             )
--rollback     );