--liquibase formatted sql

--changeset postgres:update_config_expt_selection_advancement_irri_entry_list_act_val_in_platform.config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update config EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for IRRI Selection and Advancement data process",
            "Values": [{
                "disabled": false,
                "variable_abbrev": "DESCRIPTION",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint": "entries",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification"
            }]
        }
    ' 
WHERE abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for IRRI Selection and Advancement data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_endpoint": "entries"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ENTRY_LIST_ACT_VAL';