--liquibase formatted sql

--changeset postgres:remove_created_experiment_status_in_experiment.experiment_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-730 Remove created experiment_status in experiment.experiment



UPDATE
    experiment.experiment
SET
    experiment_status = 'crosses created'
WHERE
    experiment_status = 'created;crosses created';

UPDATE
    experiment.experiment
SET
    experiment_status = 'entry list created;crosses created'
WHERE
    experiment_status = 'created;entry list created;crosses created';


UPDATE
    experiment.experiment
SET
    experiment_status = 'entry list created;design generated;occurrences created'
WHERE
    experiment_status = 'created;entry list created;design generated;occurrences created';



--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;crosses created'
--rollback WHERE
--rollback     experiment_status = 'crosses created';

--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;entry list created;crosses created'
--rollback WHERE
--rollback     experiment_status = 'entry list created;crosses created';

--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;entry list created;design generated;occurrences created'
--rollback WHERE
--rollback     experiment_status = 'entry list created;design generated;occurrences created';