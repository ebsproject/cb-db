--liquibase formatted sql

--changeset postgres:update_list_type_in_platform.list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-472 Update list_type in platform.list



UPDATE
    platform.list
SET
    type = 'germplasm'
WHERE
    type = 'designation';

UPDATE
    platform.list
SET
    type = 'trait'
WHERE
    type = 'variable';



--rollback UPDATE
--rollback     platform.list
--rollback SET
--rollback     type = 'designation'
--rollback WHERE
--rollback     type = 'germplasm';
--rollback 
--rollback UPDATE
--rollback     platform.list
--rollback SET
--rollback     type = 'variable'
--rollback WHERE
--rollback     type = 'trait';