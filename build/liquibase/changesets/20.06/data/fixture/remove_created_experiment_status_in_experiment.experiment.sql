--liquibase formatted sql

--changeset postgres:remove_created_experiment_status_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-730 Remove created experiment_status in experiment.experiment



UPDATE
    experiment.experiment
SET
    experiment_status = 'draft'
WHERE
    experiment_status = 'created;draft';

UPDATE
    experiment.experiment
SET
    experiment_status = 'entry list created'
WHERE
    experiment_status = 'created;entry list created';

UPDATE
    experiment.experiment
SET
    experiment_status = 'entry list created;crosses created;design generated'
WHERE
    experiment_status = 'created;entry list created;crosses created;design generated';

UPDATE
    experiment.experiment
SET
    experiment_status = 'entry list created;design generated'
WHERE
    experiment_status = 'created;entry list created;design generated';



--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;draft'
--rollback WHERE
--rollback     experiment_status = 'draft';

--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;entry list created'
--rollback WHERE
--rollback     experiment_status = 'entry list created';

--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;entry list created;crosses created;design generated'
--rollback WHERE
--rollback     experiment_status = 'entry list created;crosses created;design generated';

--rollback UPDATE
--rollback     experiment.experiment
--rollback SET
--rollback     experiment_status = 'created;entry list created;design generated'
--rollback WHERE
--rollback     experiment_status = 'entry list created;design generated';