--liquibase formatted sql

--changeset postgres:add_selection_advancement_irri_protocol_activity_children_to_master.item_relation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Add selection advancement IRRI protocol activity children to master.item_relation



INSERT INTO 
    master.item_relation
    (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS'
    ) AS root_id,
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT'
    ) AS parent_id,
    id as child_id,
    CASE 
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT' THEN 1
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_TRAITS_PROTOCOLS_ACT' THEN 2
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT' THEN 3
    ELSE 
        4
    END AS order_number,
    0,
    1,
    1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    (
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_TRAITS_PROTOCOLS_ACT',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT'
    );



--rollback DELETE FROM
--rollback     master.item_relation 
--rollback WHERE
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS'
--rollback     )
--rollback AND 
--rollback     parent_id 
--rollback IN
--rollback     (
--rollback          SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_PROTOCOLS_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE
--rollback 	        abbrev 
--rollback         IN 
--rollback         (
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_PLANTING_PROTOCOLS_ACT',
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_TRAITS_PROTOCOLS_ACT',
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT'
--rollback         )
--rollback );