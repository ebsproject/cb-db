--liquibase formatted sql

--changeset postgres:remove_irri_staff_member_names.sql context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-610 Remove IRRI staff member names



--update tenant.person
UPDATE tenant.person SET username='jennifer.allan',first_name='Jennifer',last_name='Allan',person_name='Allan, Jennifer',email='jennifer.allan@gmail.com' WHERE id=27;
UPDATE tenant.person SET username='audrey.mackenzie',first_name='Audrey',last_name='Mackenzie',person_name='Mackenzie, Audrey',email='audrey.mackenzie@gmail.com' WHERE id=30;
UPDATE tenant.person SET username='peter.miller',first_name='Peter',last_name='Miller',person_name='Miller, Peter',email='peter.miller@gmail.com' WHERE id=53;
UPDATE tenant.person SET username='lillian.davidson',first_name='Lillian',last_name='Davidson',person_name='Davidson, Lillian',email='lillian.davidson@gmail.com' WHERE id=67;
UPDATE tenant.person SET username='faith.hamilton',first_name='Faith',last_name='Hamilton',person_name='Hamilton, Faith',email='faith.hamilton@gmail.com' WHERE id=76;
UPDATE tenant.person SET username='phil.hudson',first_name='Phil',last_name='Hudson',person_name='Hudson, Phil',email='phil.hudson@gmail.com' WHERE id=81;
UPDATE tenant.person SET username='colin.anderson',first_name='Colin',last_name='Anderson',person_name='Anderson, Colin',email='colin.anderson@gmail.com' WHERE id=82;
UPDATE tenant.person SET username='donna.knox',first_name='Donna',last_name='Knox',person_name='Knox, Donna',email='donna.knox@gmail.com' WHERE id=84;
UPDATE tenant.person SET username='alexandra.reid',first_name='Alexandra',last_name='Reid',person_name='Reid, Alexandra',email='alexandra.reid@gmail.com' WHERE id=111;
UPDATE tenant.person SET username='charles.butler',first_name='Charles',last_name='Butler',person_name='Butler, Charles',email='charles.butler@gmail.com' WHERE id=117;
UPDATE tenant.person SET username='rose.pullman',first_name='Rose',last_name='Pullman',person_name='Pullman, Rose',email='rose.pullman@gmail.com' WHERE id=124;
UPDATE tenant.person SET username='diana.gill',first_name='Diana',last_name='Gill',person_name='Gill, Diana',email='diana.gill@gmail.com' WHERE id=125;
UPDATE tenant.person SET username='victor.bond',first_name='Victor',last_name='Bond',person_name='Bond, Victor',email='victor.bond@gmail.com' WHERE id=141;
UPDATE tenant.person SET username='owen.kerr',first_name='Owen',last_name='Kerr',person_name='Kerr, Owen',email='owen.kerr@gmail.com' WHERE id=184;
UPDATE tenant.person SET username='anthony.tucker',first_name='Anthony',last_name='Tucker',person_name='Tucker, Anthony',email='anthony.tucker@gmail.com' WHERE id=189;
UPDATE tenant.person SET username='evan.piper',first_name='Evan',last_name='Piper',person_name='Piper, Evan',email='evan.piper@gmail.com' WHERE id=190;
UPDATE tenant.person SET username='sally.gray',first_name='Sally',last_name='Gray',person_name='Gray, Sally',email='sally.gray@gmail.com' WHERE id=191;
UPDATE tenant.person SET username='wendy.glover',first_name='Wendy',last_name='Glover',person_name='Glover, Wendy',email='wendy.glover@gmail.com' WHERE id=192;
UPDATE tenant.person SET username='wanda.hamilton',first_name='Wanda',last_name='Hamilton',person_name='Hamilton, Wanda',email='wanda.hamilton@gmail.com' WHERE id=221;
UPDATE tenant.person SET username='caroline.smith',first_name='Caroline',last_name='Smith',person_name='Smith, Caroline',email='caroline.smith@gmail.com' WHERE id=246;
UPDATE tenant.person SET username='amelia.chapman',first_name='Amelia',last_name='Chapman',person_name='Chapman, Amelia',email='amelia.chapman@gmail.com' WHERE id=249;
UPDATE tenant.person SET username='rebecca.macleod',first_name='Rebecca',last_name='MacLeod',person_name='MacLeod, Rebecca',email='rebecca.macleod@gmail.com' WHERE id=264;
UPDATE tenant.person SET username='stephanie.sutherland',first_name='Stephanie',last_name='Sutherland',person_name='Sutherland, Stephanie',email='stephanie.sutherland@gmail.com' WHERE id=266;
UPDATE tenant.person SET username='kevin.blake',first_name='Kevin',last_name='Blake',person_name='Blake, Kevin',email='kevin.blake@gmail.com' WHERE id=291;
UPDATE tenant.person SET username='alexander.powell',first_name='Alexander',last_name='Powell',person_name='Powell, Alexander',email='alexander.powell@gmail.com' WHERE id=319;
UPDATE tenant.person SET username='robert.pullman',first_name='Robert',last_name='Pullman',person_name='Pullman, Robert',email='robert.pullman@gmail.com' WHERE id=363;
UPDATE tenant.person SET username='james.kerr',first_name='James',last_name='Kerr',person_name='Kerr, James',email='james.kerr@gmail.com' WHERE id=389;
UPDATE tenant.person SET username='alexander.mathis',first_name='Alexander',last_name='Mathis',person_name='Mathis, Alexander',email='alexander.mathis@gmail.com' WHERE id=390;
UPDATE tenant.person SET username='connor.knox',first_name='Connor',last_name='Knox',person_name='Knox, Connor',email='connor.knox@gmail.com' WHERE id=391;
UPDATE tenant.person SET username='charles.hill',first_name='Charles',last_name='Hill',person_name='Hill, Charles',email='charles.hill@gmail.com' WHERE id=413;
UPDATE tenant.person SET username='jack.simpson',first_name='Jack',last_name='Simpson',person_name='Simpson, Jack',email='jack.simpson@gmail.com' WHERE id=417;
UPDATE tenant.person SET username='lillian.taylor',first_name='Lillian',last_name='Taylor',person_name='Taylor, Lillian',email='lillian.taylor@gmail.com' WHERE id=418;
UPDATE tenant.person SET username='megan.dowd',first_name='Megan',last_name='Dowd',person_name='Dowd, Megan',email='megan.dowd@gmail.com' WHERE id=419;
UPDATE tenant.person SET username='grace.bell',first_name='Grace',last_name='Bell',person_name='Bell, Grace',email='grace.bell@gmail.com' WHERE id=448;
UPDATE tenant.person SET username='lily.clark',first_name='Lily',last_name='Clark',person_name='Clark, Lily',email='lily.clark@gmail.com' WHERE id=455;
UPDATE tenant.person SET username='oliver.duncan',first_name='Oliver',last_name='Duncan',person_name='Duncan, Oliver',email='oliver.duncan@gmail.com' WHERE id=458;

--update experiment.occurrence_data
UPDATE experiment.occurrence_data SET data_value='Davidson, Lillian' WHERE data_value='Gannaban, Ritchel';
UPDATE experiment.occurrence_data SET data_value='Anderson, Colin' WHERE data_value='Verdeprado, Holden';
UPDATE experiment.occurrence_data SET data_value='Mackenzie, Audrey' WHERE data_value='Mendoza, Rhulyx';
UPDATE experiment.occurrence_data SET data_value='Allan, Jennifer' WHERE data_value='Santelices, Ronald';
UPDATE experiment.occurrence_data SET data_value='Knox, Donna' WHERE data_value='Shim, Jung-Hyun';
UPDATE experiment.occurrence_data SET data_value='Hudson, Phil' WHERE data_value='Lopena, Vitaliano';
UPDATE experiment.occurrence_data SET data_value='Miller, Peter' WHERE data_value='Bonifacio, Justine';



--rollback UPDATE tenant.person SET username='r.santelices',first_name='Ronald',last_name='Santelices',person_name='Santelices, Ronald',email='r.santelices@irri.org' WHERE id=27;
--rollback UPDATE tenant.person SET username='r.mendoza',first_name='Rhulyx',last_name='Mendoza',person_name='Mendoza, Rhulyx',email='r.mendoza@irri.org' WHERE id=30;
--rollback UPDATE tenant.person SET username='j.bonifacio',first_name='Justine',last_name='Bonifacio',person_name='Bonifacio, Justine',email='j.bonifacio@irri.org' WHERE id=53;
--rollback UPDATE tenant.person SET username='r.gannaban',first_name='Ritchel',last_name='Gannaban',person_name='Gannaban, Ritchel',email='r.gannaban@irri.org' WHERE id=67;
--rollback UPDATE tenant.person SET username='j.pacia',first_name='Jocelyn',last_name='Pacia',person_name='Pacia, Jocelyn',email='j.pacia@irri.org' WHERE id=76;
--rollback UPDATE tenant.person SET username='v.lopena',first_name='Vitaliano',last_name='Lopena',person_name='Lopena, Vitaliano',email='v.lopena@irri.org' WHERE id=81;
--rollback UPDATE tenant.person SET username='h.verdeprado',first_name='Holden',last_name='Verdeprado',person_name='Verdeprado, Holden',email='h.verdeprado@irri.org' WHERE id=82;
--rollback UPDATE tenant.person SET username='j.h.shim',first_name='Jung-Hyun',last_name='Shim',person_name='Shim, Jung-Hyun',email='j.h.shim@irri.org' WHERE id=84;
--rollback UPDATE tenant.person SET username='r.murori',first_name='Rosemary',last_name='Murori',person_name='Murori, Rosemary',email='r.murori@irri.org' WHERE id=111;
--rollback UPDATE tenant.person SET username='a.ndayiragije',first_name='Alexis',last_name='Ndayiragije',person_name='Ndayiragije, Alexis',email='a.ndayiragije@irri.org' WHERE id=117;
--rollback UPDATE tenant.person SET username='j.bizimana',first_name='Jean Berchmans',last_name='Bizimana',person_name='Bizimana, Jean Berchmans',email='j.bizimana@irri.org' WHERE id=124;
--rollback UPDATE tenant.person SET username='j.nduwimana',first_name='Julien',last_name='Nduwimana',person_name='Nduwimana, Julien',email='j.nduwimana@irri.org' WHERE id=125;
--rollback UPDATE tenant.person SET username='j.ignacio',first_name='John Carlos',last_name='Ignacio',person_name='Ignacio, John Carlos',email='j.ignacio@irri.org' WHERE id=141;
--rollback UPDATE tenant.person SET username='p.delacruz',first_name='Princess',last_name='Dela Cruz',person_name='Dela Cruz, Princess',email='p.delacruz@irri.org' WHERE id=184;
--rollback UPDATE tenant.person SET username='m.mkuya',first_name='Mohamed Seleman',last_name='Mkuya',person_name='Mkuya, Mohamed Seleman',email='m.mkuya@irri.org' WHERE id=189;
--rollback UPDATE tenant.person SET username='o.nyongesa',first_name='Oliver',last_name='Nyongesa',person_name='Nyongesa, Oliver',email='o.nyongesa@irri.org' WHERE id=190;
--rollback UPDATE tenant.person SET username='a.matsinhe',first_name='Arlindo',last_name='Matsinhe',person_name='Matsinhe, Arlindo',email='a.matsinhe@irri.org' WHERE id=191;
--rollback UPDATE tenant.person SET username='d.bigirimana',first_name='Donatien',last_name='Bigirimana',person_name='Bigirimana, Donatien',email='d.bigirimana@irri.org' WHERE id=192;
--rollback UPDATE tenant.person SET username='c.venkateshwarlu',first_name='Challa',last_name='Venkateshwarlu',person_name='Venkateshwarlu, Challa',email='c.venkateshwarlu@irri.org' WHERE id=221;
--rollback UPDATE tenant.person SET username='j.cobb',first_name='Joshua Nathaniel',last_name='Cobb',person_name='Cobb, Joshua Nathaniel',email='j.cobb@irri.org' WHERE id=246;
--rollback UPDATE tenant.person SET username='r.b.javier',first_name='Romnick',last_name='Javier',person_name='Javier, Romnick',email='r.b.javier@irri.org' WHERE id=249;
--rollback UPDATE tenant.person SET username='p.s.biswas',first_name='Partha Sarathi',last_name='Biswas',person_name='Biswas, Partha Sarathi',email='p.s.biswas@irri.org' WHERE id=264;
--rollback UPDATE tenant.person SET username='j.arbelaezvelez',first_name='Juan David Arbelaez',last_name='Velez',person_name='Velez, Juan David Arbelaez',email='j.arbelaezvelez@irri.org' WHERE id=266;
--rollback UPDATE tenant.person SET username='k.pranesh',first_name='Pranesh',last_name='K J',person_name='K J, Pranesh',email='k.pranesh@irri.org' WHERE id=291;
--rollback UPDATE tenant.person SET username='r.juma',first_name='Roselyne Uside',last_name='Juma',person_name='Juma, Roselyne Uside',email='r.juma@irri.org' WHERE id=319;
--rollback UPDATE tenant.person SET username='j.p.evangelista',first_name='Joel',last_name='Evangelista',person_name='Evangelista, Joel',email='j.p.evangelista@irri.org' WHERE id=363;
--rollback UPDATE tenant.person SET username='r.kwayu',first_name='Rehema',last_name='Kwayu',person_name='Kwayu, Rehema',email='r.kwayu@irri.org' WHERE id=389;
--rollback UPDATE tenant.person SET username='c.manalang',first_name='Cezar',last_name='Manalang',person_name='Manalang, Cezar',email='c.manalang@irri.org' WHERE id=390;
--rollback UPDATE tenant.person SET username='j.bartholome',first_name='Jerome',last_name='Bartholome',person_name='Bartholome, Jerome',email='j.bartholome@irri.org' WHERE id=391;
--rollback UPDATE tenant.person SET username='s.kariuki',first_name='Simon',last_name='Njau',person_name='Njau, Simon',email='s.kariuki@irri.org' WHERE id=413;
--rollback UPDATE tenant.person SET username='p.saha',first_name='Parth Sarothi',last_name='Saha',person_name='Saha, Parth Sarothi',email='p.saha@irri.org' WHERE id=417;
--rollback UPDATE tenant.person SET username='k.hossain',first_name='Kamal',last_name='Hossain',person_name='Hossain, Kamal',email='k.hossain@irri.org' WHERE id=418;
--rollback UPDATE tenant.person SET username='k.santos',first_name='Kristina Cassandra',last_name='Santos',person_name='Santos, Kristina Cassandra',email='k.santos@irri.org' WHERE id=419;
--rollback UPDATE tenant.person SET username='p.prakash',first_name='Parthiban',last_name='Prakash',person_name='Prakash, Parthiban',email='p.prakash@irri.org' WHERE id=448;
--rollback UPDATE tenant.person SET username='f.niyongabo',first_name='Fulgence',last_name='Niyongabo',person_name='Niyongabo, Fulgence',email='f.niyongabo@irri.org' WHERE id=455;
--rollback UPDATE tenant.person SET username='waseem.hussain',first_name='Waseem',last_name='Hussain',person_name='Hussain, Waseem',email='waseem.hussain@irri.org' WHERE id=458;

--rollback UPDATE experiment.occurrence_data SET data_value='Gannaban, Ritchel' WHERE data_value='Davidson, Lillian';
--rollback UPDATE experiment.occurrence_data SET data_value='Verdeprado, Holden' WHERE data_value='Anderson, Colin';
--rollback UPDATE experiment.occurrence_data SET data_value='Mendoza, Rhulyx' WHERE data_value='Mackenzie, Audrey';
--rollback UPDATE experiment.occurrence_data SET data_value='Santelices, Ronald' WHERE data_value='Allan, Jennifer';
--rollback UPDATE experiment.occurrence_data SET data_value='Shim, Jung-Hyun' WHERE data_value='Knox, Donna';
--rollback UPDATE experiment.occurrence_data SET data_value='Lopena, Vitaliano' WHERE data_value='Hudson, Phil';
--rollback UPDATE experiment.occurrence_data SET data_value='Bonifacio, Justine' WHERE data_value='Miller, Peter';