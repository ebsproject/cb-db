--liquibase formatted sql

--changeset postgres:remove_irri_staff_member_names_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-610 Remove IRRI staff member names p2



--update tenant.person
UPDATE tenant.person SET username='nicola.costa',first_name='Nicola',last_name='Costa',person_name='Costa, Nicola',email='nicola.costa@gmail.com' WHERE id=18;
UPDATE tenant.person SET username='elly.donelly',first_name='Elly',last_name='Donelly',person_name='Donelly, Elly',email='elly.donelly@gmail.com' WHERE id=19;

--update experiment.occurrence_data
UPDATE experiment.occurrence_data SET data_value='Costa, Nicola' WHERE data_value='Lotho, Connie';
UPDATE experiment.occurrence_data SET data_value='Donelly, Elly' WHERE data_value='Parducho, Consie';



--rollback UPDATE tenant.person SET username='m.lotho',first_name='Connie',last_name='Lotho',person_name='Lotho, Connie',email='m.lotho@irri.org' WHERE id=18;
--rollback UPDATE tenant.person SET username='c.parducho',first_name='Consie',last_name='Parducho',person_name='Parducho, Consie',email='c.parducho@irri.org' WHERE id=19;
--rollback UPDATE experiment.occurrence_data SET data_value='Lotho, Connie' WHERE data_value='Costa, Nicola';
--rollback UPDATE experiment.occurrence_data SET data_value='Parducho, Consie' WHERE data_value='Donelly, Elly';