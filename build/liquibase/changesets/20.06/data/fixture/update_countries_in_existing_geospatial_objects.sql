--liquibase formatted sql

--changeset postgres:update_countries_in_existing_geospatial_objects context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-261 Update countries in existing geospatial objects



-- update countries
UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10012';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '15' WHERE id = '10022';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10023';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10024';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10006';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10003';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10005';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10013';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10017';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10018';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '27' WHERE id = '10019';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '6' WHERE id = '10020';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '11' WHERE id = '10021';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10008';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10009';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10011';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10004';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10026';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10044';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10010';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10062';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10007';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10001';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10066';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10058';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10070';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10064';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10052';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10061';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10055';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10054';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10053';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10051';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10049';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10048';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10047';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10046';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10033';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10034';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10032';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10031';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10029';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10002';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10056';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10050';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '12' WHERE id = '10071';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10030';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10073';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '12' WHERE id = '10093';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10075';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10081';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10100';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10096';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10063';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '11' WHERE id = '10097';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10101';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '15' WHERE id = '10098';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10102';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10104';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10105';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10106';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10108';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10110';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10111';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10057';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10112';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10113';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10115';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10103';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10116';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10118';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '9' WHERE id = '10125';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10122';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10038';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10114';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10121';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10123';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '29' WHERE id = '10120';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10124';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '5' WHERE id = '10109';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10095';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '24' WHERE id = '10089';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '24' WHERE id = '10090';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '24' WHERE id = '10088';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10083';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '25' WHERE id = '10145';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '13' WHERE id = '10147';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10156';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10148';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10149';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10150';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10153';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10151';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10210';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10133';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '4' WHERE id = '10128';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10135';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10136';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10127';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10131';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10119';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10059';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10140';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10060';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10130';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10137';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '28' WHERE id = '10132';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10087';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '9' WHERE id = '10126';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '4' WHERE id = '10129';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10134';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10028';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10138';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10139';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10162';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10142';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '20' WHERE id = '10187';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '23' WHERE id = '10144';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10143';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10141';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10154';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10165';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10155';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '13' WHERE id = '10146';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10152';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10170';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10164';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10159';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10160';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10158';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10161';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10173';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10166';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10168';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10181';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10177';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10172';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10171';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10179';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10169';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10176';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10178';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10180';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10183';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10182';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10174';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10157';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10184';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '24' WHERE id = '10189';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '14' WHERE id = '10190';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10208';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10199';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10205';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10192';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10196';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10193';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '20' WHERE id = '10186';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10191';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10197';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10206';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10200';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10202';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10203';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10207';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10215';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10204';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10201';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '20' WHERE id = '10211';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10209';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '20' WHERE id = '10212';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '16' WHERE id = '10219';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10222';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '18' WHERE id = '10221';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '26' WHERE id = '10224';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '7' WHERE id = '10236';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '8' WHERE id = '10218';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '30' WHERE id = '10223';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '3' WHERE id = '10225';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '12' WHERE id = '10091';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10237';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '19' WHERE id = '10227';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '3' WHERE id = '10226';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '10' WHERE id = '10229';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10231';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10230';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10232';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '2' WHERE id = '10072';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10074';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '21' WHERE id = '10086';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10234';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '1' WHERE id = '10235';

UPDATE place.geospatial_object SET parent_geospatial_object_id = '22' WHERE id = '10238';



-- revert changes
--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10012';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10022';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10023';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10024';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10006';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10003';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10005';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10013';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10017';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10018';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10019';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10020';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10021';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10008';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10009';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10011';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10004';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10026';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10044';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10010';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10062';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10007';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10001';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10066';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10058';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10070';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10064';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10052';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10061';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10055';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10054';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10053';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10051';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10049';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10048';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10047';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10046';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10033';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10034';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10032';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10031';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10029';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10002';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10056';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10050';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10071';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10030';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10073';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10093';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10075';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10081';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10100';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10096';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10063';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10097';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10101';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10098';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10102';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10104';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10105';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10106';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10108';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10110';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10111';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10057';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10112';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10113';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10115';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10103';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10116';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10118';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10125';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10122';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10038';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10114';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10121';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10123';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10120';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10124';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10109';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10095';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10089';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10090';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10088';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10083';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10145';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10147';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10156';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10148';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10149';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10150';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10153';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10151';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10210';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10133';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10128';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10135';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10136';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10127';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10131';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10119';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10059';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10140';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10060';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10130';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10137';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10132';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10087';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10126';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10129';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10134';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10028';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10138';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10139';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10162';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10142';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10187';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10144';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10143';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10141';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10154';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10165';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10155';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10146';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10152';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10170';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10164';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10159';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10160';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10158';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10161';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10173';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10166';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10168';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10181';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10177';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10172';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10171';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10179';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10169';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10176';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10178';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10180';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10183';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10182';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10174';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10157';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10184';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10189';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10190';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10208';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10199';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10205';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10192';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10196';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10193';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10186';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10191';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10197';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10206';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10200';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10202';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10203';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10207';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10215';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10204';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10201';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10211';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10209';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10212';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10219';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10222';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10221';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10224';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10236';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10218';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10223';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10225';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10091';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10237';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10227';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10226';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10229';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10231';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10230';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10232';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10072';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10074';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10086';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10234';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10235';

--rollback UPDATE place.geospatial_object SET parent_geospatial_object_id = NULL WHERE id = '10238';
