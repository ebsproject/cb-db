--liquibase formatted sql

--changeset postgres:populate_package_document_with_trigger_in_germplasm.package_p4 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-410 Populate package_document in germplasm.package p4



UPDATE 
    germplasm.package
SET
    modification_timestamp = now();



--rollback SELECT NULL;