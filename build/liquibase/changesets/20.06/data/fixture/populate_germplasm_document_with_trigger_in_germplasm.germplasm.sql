--liquibase formatted sql

--changeset postgres:populate_germplasm_document_with_trigger_in_germplasm.germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-402 Populate germplasm_document in germplasm.germplasm



UPDATE 
    germplasm.germplasm
SET
    modification_timestamp = now();



--rollback SELECT NULL;