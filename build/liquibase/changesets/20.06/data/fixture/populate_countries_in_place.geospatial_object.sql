--liquibase formatted sql

--changeset postgres:populate_countries_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-261 Populate countries in place.geospatial_object



-- populate countries
INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('1', 'AF', 'Afghanistan', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('2', 'BD', 'Bangladesh', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('3', 'BT', 'Bhutan', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('4', 'BR', 'Brazil', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('5', 'BI', 'Burundi', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('6', 'KH', 'Cambodia', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('7', 'CN', 'China', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('8', 'CD', 'Congo (Kinshasa)', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('9', 'EG', 'Egypt', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('10', 'IN', 'India', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('11', 'ID', 'Indonesia', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('12', 'KE', 'Kenya', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('13', 'KP', 'Korea, North', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('14', 'KR', 'Korea, South', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('15', 'LA', 'Laos', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('16', 'MG', 'Madagascar', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('17', 'MW', 'Malawi', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('18', 'MZ', 'Mozambique', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('19', 'MM', 'Myanmar', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('20', 'NP', 'Nepal', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('21', 'PK', 'Pakistan', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('22', 'PH', 'Philippines', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('23', 'RU', 'Russian Federation', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('24', 'LK', 'Sri Lanka', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('25', 'SR', 'Suriname', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('26', 'TZ', 'Tanzania', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('27', 'TH', 'Thailand', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('28', 'UY', 'Uruguay', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('29', 'VN', 'Vietnam', 'site', 'country', '1', '2014-09-01 13:07:09.371958');

INSERT INTO place.geospatial_object (id, geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, creation_timestamp)
    VALUES ('30', 'ZM', 'Zambia', 'site', 'country', '1', '2014-09-01 13:07:09.371958');


-- update sequence
SELECT SETVAL('place.geospatial_object_id_seq', COALESCE(MAX(id), 1)) FROM place.geospatial_object;



-- revert changes
--rollback DELETE FROM place.geospatial_object WHERE id = '1';

--rollback DELETE FROM place.geospatial_object WHERE id = '2';

--rollback DELETE FROM place.geospatial_object WHERE id = '3';

--rollback DELETE FROM place.geospatial_object WHERE id = '4';

--rollback DELETE FROM place.geospatial_object WHERE id = '5';

--rollback DELETE FROM place.geospatial_object WHERE id = '6';

--rollback DELETE FROM place.geospatial_object WHERE id = '7';

--rollback DELETE FROM place.geospatial_object WHERE id = '8';

--rollback DELETE FROM place.geospatial_object WHERE id = '9';

--rollback DELETE FROM place.geospatial_object WHERE id = '10';

--rollback DELETE FROM place.geospatial_object WHERE id = '11';

--rollback DELETE FROM place.geospatial_object WHERE id = '12';

--rollback DELETE FROM place.geospatial_object WHERE id = '13';

--rollback DELETE FROM place.geospatial_object WHERE id = '14';

--rollback DELETE FROM place.geospatial_object WHERE id = '15';

--rollback DELETE FROM place.geospatial_object WHERE id = '16';

--rollback DELETE FROM place.geospatial_object WHERE id = '17';

--rollback DELETE FROM place.geospatial_object WHERE id = '18';

--rollback DELETE FROM place.geospatial_object WHERE id = '19';

--rollback DELETE FROM place.geospatial_object WHERE id = '20';

--rollback DELETE FROM place.geospatial_object WHERE id = '21';

--rollback DELETE FROM place.geospatial_object WHERE id = '22';

--rollback DELETE FROM place.geospatial_object WHERE id = '23';

--rollback DELETE FROM place.geospatial_object WHERE id = '24';

--rollback DELETE FROM place.geospatial_object WHERE id = '25';

--rollback DELETE FROM place.geospatial_object WHERE id = '26';

--rollback DELETE FROM place.geospatial_object WHERE id = '27';

--rollback DELETE FROM place.geospatial_object WHERE id = '28';

--rollback DELETE FROM place.geospatial_object WHERE id = '29';

--rollback DELETE FROM place.geospatial_object WHERE id = '30';

--rollback SELECT SETVAL('place.geospatial_object_id_seq', COALESCE(MAX(id), 1)) FROM place.geospatial_object;