--liquibase formatted sql

--changeset postgres:activate_seed_increase_irri_modules_in_platform.module context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Activate seed increase IRRI modules in platform.module



UPDATE 
    platform.module 
SET 
    is_void = false 
WHERE
    abbrev 
IN 
    (
        'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT_MOD'     
);



--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_DESIGN_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_ENTRY_LIST_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PLANTING_PROTOCOLS_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PROTOCOLS_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_REVIEW_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_TRAITS_PROTOCOLS_ACT_MOD'     
--rollback );