--liquibase formatted sql

--changeset postgres:populate_person_document_in_tenant.person context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-405 Populate person_document in tenant.person



CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT
		tp.id,
		concat(
			setweight(to_tsvector(unaccent(tp.person_name)),'A'),' ',
			setweight(to_tsvector(unaccent(tp.first_name)),'A'),' ',
			setweight(to_tsvector(unaccent(tp.last_name)),'A'),' ',
			setweight(to_tsvector(tp.username),'B'),' ',
			setweight(to_tsvector(tp.email),'C'),' ',
			setweight(to_tsvector(tp.person_type),'CD'),' ',
			setweight(to_tsvector(tp.person_status),'CD')
		) AS doc
	FROM 
		tenant.person tp
	ORDER BY
		tp.id
)

UPDATE tenant.person tp SET person_document = cast(t1.doc AS tsvector) FROM t1 WHERE tp.id = t1.id;



--rollback UPDATE tenant.person SET person_document = NULL;