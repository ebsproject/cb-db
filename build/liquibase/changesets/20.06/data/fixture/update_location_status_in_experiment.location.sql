--liquibase formatted sql

--changeset postgres:update_location_status_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-391 Update locatin_status in experiment.location



UPDATE
    experiment.location
SET
    location_status = 'committed'
WHERE
    location_status = 'completed';



--rollback UPDATE
--rollback     experiment.location
--rollback SET
--rollback     location_status = 'completed'
--rollback WHERE
--rollback     location_status = 'committed';