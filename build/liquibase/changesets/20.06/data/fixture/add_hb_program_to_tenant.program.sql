--liquibase formatted sql

--changeset postgres:add_hb_program_to_tenant.program context:fixture labels:uat splitStatements:false rollbackSplitStatements:false
--comment: EBS-722 Add HB program to tenant.program



INSERT INTO
    tenant.program (program_code,program_name,program_type,program_status,crop_program_id,creator_id)
VALUES 
    (
        'HB',
        'Hybridization Service',
        'CCRD',
        'active',
        1,
        1
    );



--rollback DELETE FROM
--rollback     tenant.program
--rollback WHERE
--rollback     program_code = 'HB';