--liquibase formatted sql

--changeset postgres:populate_location_number_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-583 Populate location_number in experiment.location



-- populate column
UPDATE experiment.location
SET location_number = '1'
WHERE id = '71';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '72';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '73';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '815';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '74';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '231';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '232';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '233';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '816';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '737';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '323';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '340';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '164';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '720';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '170';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '706';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '236';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '234';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '235';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '205';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '206';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '228';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '230';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '511';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '524';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '557';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '1491';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '714';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '1504';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '715';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '716';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '721';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '821';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '826';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '836';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '1508';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '359';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '378';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '1572';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '375';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '390';

UPDATE experiment.location
SET location_number = '19'
WHERE id = '698';

UPDATE experiment.location
SET location_number = '20'
WHERE id = '700';

UPDATE experiment.location
SET location_number = '21'
WHERE id = '705';

UPDATE experiment.location
SET location_number = '22'
WHERE id = '804';

UPDATE experiment.location
SET location_number = '23'
WHERE id = '1507';

UPDATE experiment.location
SET location_number = '24'
WHERE id = '843';

UPDATE experiment.location
SET location_number = '25'
WHERE id = '385';

UPDATE experiment.location
SET location_number = '26'
WHERE id = '421';

UPDATE experiment.location
SET location_number = '27'
WHERE id = '463';

UPDATE experiment.location
SET location_number = '28'
WHERE id = '473';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '1380';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '1382';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '1383';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '1384';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '1389';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '1397';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '1405';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '1408';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '1411';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '1412';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '1413';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '1414';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '1381';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '1527';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '1540';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '2163';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '893';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '1047';

UPDATE experiment.location
SET location_number = '19'
WHERE id = '894';

UPDATE experiment.location
SET location_number = '20'
WHERE id = '1396';

UPDATE experiment.location
SET location_number = '21'
WHERE id = '885';

UPDATE experiment.location
SET location_number = '22'
WHERE id = '895';

UPDATE experiment.location
SET location_number = '23'
WHERE id = '903';

UPDATE experiment.location
SET location_number = '24'
WHERE id = '907';

UPDATE experiment.location
SET location_number = '25'
WHERE id = '916';

UPDATE experiment.location
SET location_number = '26'
WHERE id = '919';

UPDATE experiment.location
SET location_number = '27'
WHERE id = '1003';

UPDATE experiment.location
SET location_number = '28'
WHERE id = '1004';

UPDATE experiment.location
SET location_number = '29'
WHERE id = '1005';

UPDATE experiment.location
SET location_number = '30'
WHERE id = '1015';

UPDATE experiment.location
SET location_number = '31'
WHERE id = '1016';

UPDATE experiment.location
SET location_number = '32'
WHERE id = '1017';

UPDATE experiment.location
SET location_number = '33'
WHERE id = '1028';

UPDATE experiment.location
SET location_number = '34'
WHERE id = '1036';

UPDATE experiment.location
SET location_number = '35'
WHERE id = '1037';

UPDATE experiment.location
SET location_number = '36'
WHERE id = '1061';

UPDATE experiment.location
SET location_number = '37'
WHERE id = '1073';

UPDATE experiment.location
SET location_number = '38'
WHERE id = '1085';

UPDATE experiment.location
SET location_number = '39'
WHERE id = '1105';

UPDATE experiment.location
SET location_number = '40'
WHERE id = '1110';

UPDATE experiment.location
SET location_number = '41'
WHERE id = '1114';

UPDATE experiment.location
SET location_number = '42'
WHERE id = '1115';

UPDATE experiment.location
SET location_number = '43'
WHERE id = '1124';

UPDATE experiment.location
SET location_number = '44'
WHERE id = '1125';

UPDATE experiment.location
SET location_number = '45'
WHERE id = '1129';

UPDATE experiment.location
SET location_number = '46'
WHERE id = '1131';

UPDATE experiment.location
SET location_number = '47'
WHERE id = '1099';

UPDATE experiment.location
SET location_number = '48'
WHERE id = '1191';

UPDATE experiment.location
SET location_number = '49'
WHERE id = '1193';

UPDATE experiment.location
SET location_number = '50'
WHERE id = '1217';

UPDATE experiment.location
SET location_number = '51'
WHERE id = '1100';

UPDATE experiment.location
SET location_number = '52'
WHERE id = '1200';

UPDATE experiment.location
SET location_number = '53'
WHERE id = '1201';

UPDATE experiment.location
SET location_number = '54'
WHERE id = '1218';

UPDATE experiment.location
SET location_number = '55'
WHERE id = '1313';

UPDATE experiment.location
SET location_number = '56'
WHERE id = '1314';

UPDATE experiment.location
SET location_number = '57'
WHERE id = '1358';

UPDATE experiment.location
SET location_number = '58'
WHERE id = '1361';

UPDATE experiment.location
SET location_number = '59'
WHERE id = '1360';

UPDATE experiment.location
SET location_number = '60'
WHERE id = '1362';

UPDATE experiment.location
SET location_number = '61'
WHERE id = '1367';

UPDATE experiment.location
SET location_number = '62'
WHERE id = '1377';

UPDATE experiment.location
SET location_number = '63'
WHERE id = '1378';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '1956';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '1520';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '1746';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '1489';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '1521';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '1567';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '1603';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '1621';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '1697';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '1763';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '1764';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '1765';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '1766';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '1769';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '1785';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '1771';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '1786';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '1788';

UPDATE experiment.location
SET location_number = '19'
WHERE id = '1789';

UPDATE experiment.location
SET location_number = '20'
WHERE id = '1792';

UPDATE experiment.location
SET location_number = '21'
WHERE id = '1793';

UPDATE experiment.location
SET location_number = '22'
WHERE id = '1794';

UPDATE experiment.location
SET location_number = '23'
WHERE id = '1796';

UPDATE experiment.location
SET location_number = '24'
WHERE id = '1797';

UPDATE experiment.location
SET location_number = '25'
WHERE id = '1798';

UPDATE experiment.location
SET location_number = '26'
WHERE id = '1799';

UPDATE experiment.location
SET location_number = '27'
WHERE id = '1800';

UPDATE experiment.location
SET location_number = '28'
WHERE id = '1801';

UPDATE experiment.location
SET location_number = '29'
WHERE id = '1822';

UPDATE experiment.location
SET location_number = '30'
WHERE id = '1825';

UPDATE experiment.location
SET location_number = '31'
WHERE id = '1840';

UPDATE experiment.location
SET location_number = '32'
WHERE id = '1841';

UPDATE experiment.location
SET location_number = '33'
WHERE id = '1855';

UPDATE experiment.location
SET location_number = '34'
WHERE id = '1861';

UPDATE experiment.location
SET location_number = '35'
WHERE id = '1868';

UPDATE experiment.location
SET location_number = '36'
WHERE id = '1888';

UPDATE experiment.location
SET location_number = '37'
WHERE id = '1889';

UPDATE experiment.location
SET location_number = '38'
WHERE id = '1955';

UPDATE experiment.location
SET location_number = '39'
WHERE id = '1981';

UPDATE experiment.location
SET location_number = '40'
WHERE id = '1988';

UPDATE experiment.location
SET location_number = '41'
WHERE id = '1990';

UPDATE experiment.location
SET location_number = '42'
WHERE id = '1982';

UPDATE experiment.location
SET location_number = '43'
WHERE id = '1989';

UPDATE experiment.location
SET location_number = '44'
WHERE id = '1991';

UPDATE experiment.location
SET location_number = '45'
WHERE id = '2026';

UPDATE experiment.location
SET location_number = '46'
WHERE id = '2059';

UPDATE experiment.location
SET location_number = '47'
WHERE id = '2125';

UPDATE experiment.location
SET location_number = '48'
WHERE id = '2177';

UPDATE experiment.location
SET location_number = '49'
WHERE id = '2178';

UPDATE experiment.location
SET location_number = '50'
WHERE id = '2179';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '2255';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '2166';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '2181';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '2183';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '2184';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '2221';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '2223';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '2224';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '2225';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '2226';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '2227';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '2232';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '2233';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '2235';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '2249';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '2254';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '2134';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '2312';

UPDATE experiment.location
SET location_number = '19'
WHERE id = '2313';

UPDATE experiment.location
SET location_number = '20'
WHERE id = '2314';

UPDATE experiment.location
SET location_number = '21'
WHERE id = '2323';

UPDATE experiment.location
SET location_number = '22'
WHERE id = '2324';

UPDATE experiment.location
SET location_number = '23'
WHERE id = '2332';

UPDATE experiment.location
SET location_number = '24'
WHERE id = '2247';

UPDATE experiment.location
SET location_number = '25'
WHERE id = '2335';

UPDATE experiment.location
SET location_number = '26'
WHERE id = '2248';

UPDATE experiment.location
SET location_number = '27'
WHERE id = '2336';

UPDATE experiment.location
SET location_number = '28'
WHERE id = '2359';

UPDATE experiment.location
SET location_number = '29'
WHERE id = '2412';

UPDATE experiment.location
SET location_number = '30'
WHERE id = '2442';

UPDATE experiment.location
SET location_number = '31'
WHERE id = '2465';

UPDATE experiment.location
SET location_number = '32'
WHERE id = '2467';

UPDATE experiment.location
SET location_number = '33'
WHERE id = '2338';

UPDATE experiment.location
SET location_number = '34'
WHERE id = '2466';

UPDATE experiment.location
SET location_number = '35'
WHERE id = '2468';

UPDATE experiment.location
SET location_number = '36'
WHERE id = '2531';

UPDATE experiment.location
SET location_number = '37'
WHERE id = '2016';

UPDATE experiment.location
SET location_number = '1'
WHERE id = '2446';

UPDATE experiment.location
SET location_number = '2'
WHERE id = '2449';

UPDATE experiment.location
SET location_number = '3'
WHERE id = '3041';

UPDATE experiment.location
SET location_number = '4'
WHERE id = '3042';

UPDATE experiment.location
SET location_number = '5'
WHERE id = '3043';

UPDATE experiment.location
SET location_number = '6'
WHERE id = '3044';

UPDATE experiment.location
SET location_number = '7'
WHERE id = '3373';

UPDATE experiment.location
SET location_number = '8'
WHERE id = '3370';

UPDATE experiment.location
SET location_number = '9'
WHERE id = '2527';

UPDATE experiment.location
SET location_number = '10'
WHERE id = '3371';

UPDATE experiment.location
SET location_number = '11'
WHERE id = '2846';

UPDATE experiment.location
SET location_number = '12'
WHERE id = '2608';

UPDATE experiment.location
SET location_number = '13'
WHERE id = '3374';

UPDATE experiment.location
SET location_number = '14'
WHERE id = '3375';

UPDATE experiment.location
SET location_number = '15'
WHERE id = '3376';

UPDATE experiment.location
SET location_number = '16'
WHERE id = '2632';

UPDATE experiment.location
SET location_number = '17'
WHERE id = '2633';

UPDATE experiment.location
SET location_number = '18'
WHERE id = '2634';

UPDATE experiment.location
SET location_number = '19'
WHERE id = '2666';

UPDATE experiment.location
SET location_number = '20'
WHERE id = '2674';

UPDATE experiment.location
SET location_number = '21'
WHERE id = '2675';

UPDATE experiment.location
SET location_number = '22'
WHERE id = '2676';

UPDATE experiment.location
SET location_number = '23'
WHERE id = '2701';

UPDATE experiment.location
SET location_number = '24'
WHERE id = '2702';

UPDATE experiment.location
SET location_number = '25'
WHERE id = '2738';

UPDATE experiment.location
SET location_number = '26'
WHERE id = '2739';

UPDATE experiment.location
SET location_number = '27'
WHERE id = '2740';

UPDATE experiment.location
SET location_number = '28'
WHERE id = '2741';

UPDATE experiment.location
SET location_number = '29'
WHERE id = '2795';

UPDATE experiment.location
SET location_number = '30'
WHERE id = '2797';

UPDATE experiment.location
SET location_number = '31'
WHERE id = '2837';

UPDATE experiment.location
SET location_number = '32'
WHERE id = '2842';

UPDATE experiment.location
SET location_number = '33'
WHERE id = '2918';

UPDATE experiment.location
SET location_number = '34'
WHERE id = '2929';

UPDATE experiment.location
SET location_number = '35'
WHERE id = '2932';

UPDATE experiment.location
SET location_number = '36'
WHERE id = '2939';

UPDATE experiment.location
SET location_number = '37'
WHERE id = '2942';

UPDATE experiment.location
SET location_number = '38'
WHERE id = '3024';

UPDATE experiment.location
SET location_number = '39'
WHERE id = '3026';

UPDATE experiment.location
SET location_number = '40'
WHERE id = '2707';

UPDATE experiment.location
SET location_number = '41'
WHERE id = '3028';

UPDATE experiment.location
SET location_number = '42'
WHERE id = '3030';

UPDATE experiment.location
SET location_number = '43'
WHERE id = '2441';

UPDATE experiment.location
SET location_number = '44'
WHERE id = '3040';

UPDATE experiment.location
SET location_number = '45'
WHERE id = '2443';

UPDATE experiment.location
SET location_number = '46'
WHERE id = '2445';



-- revert changes
--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '71';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '72';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '73';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '815';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '74';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '231';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '232';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '233';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '816';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '737';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '323';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '340';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '164';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '720';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '170';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '706';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '236';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '234';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '235';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '205';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '206';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '228';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '230';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '511';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '524';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '557';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1491';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '714';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1504';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '715';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '716';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '721';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '821';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '826';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '836';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1508';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '359';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '378';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1572';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '375';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '390';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '698';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '700';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '705';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '804';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1507';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '843';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '385';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '421';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '463';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '473';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1380';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1382';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1383';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1384';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1389';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1397';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1405';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1408';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1411';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1412';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1413';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1414';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1381';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1527';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1540';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2163';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '893';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1047';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '894';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1396';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '885';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '895';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '903';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '907';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '916';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '919';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1003';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1004';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1005';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1015';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1016';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1017';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1028';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1036';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1037';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1061';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1073';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1085';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1105';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1110';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1114';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1115';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1124';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1125';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1129';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1131';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1099';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1191';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1193';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1217';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1100';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1200';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1201';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1218';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1313';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1314';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1358';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1361';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1360';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1362';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1367';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1377';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1378';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1956';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1520';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1746';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1489';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1521';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1567';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1603';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1621';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1697';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1763';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1764';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1765';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1766';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1769';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1785';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1771';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1786';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1788';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1789';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1792';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1793';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1794';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1796';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1797';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1798';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1799';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1800';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1801';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1822';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1825';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1840';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1841';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1855';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1861';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1868';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1888';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1889';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1955';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1981';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1988';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1990';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1982';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1989';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '1991';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2026';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2059';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2125';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2177';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2178';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2179';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2255';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2166';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2181';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2183';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2184';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2221';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2223';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2224';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2225';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2226';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2227';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2232';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2233';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2235';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2249';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2254';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2134';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2312';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2313';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2314';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2323';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2324';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2332';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2247';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2335';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2248';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2336';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2359';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2412';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2442';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2465';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2467';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2338';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2466';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2468';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2531';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2016';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2446';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2449';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3041';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3042';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3043';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3044';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3373';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3370';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2527';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3371';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2846';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2608';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3374';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3375';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3376';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2632';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2633';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2634';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2666';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2674';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2675';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2676';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2701';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2702';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2738';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2739';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2740';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2741';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2795';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2797';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2837';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2842';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2918';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2929';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2932';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2939';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2942';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3024';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3026';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2707';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3028';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3030';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2441';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '3040';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2443';

--rollback UPDATE experiment.location
--rollback SET location_number = '1'
--rollback WHERE id = '2445';
