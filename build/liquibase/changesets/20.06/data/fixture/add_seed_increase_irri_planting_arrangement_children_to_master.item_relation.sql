--liquibase formatted sql

--changeset postgres:add_seed_increase_irri_planting_arrangement_children_to_master.item_relation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Add seed increase IRRI planting arrangement children to master.item_relation



INSERT INTO 
    master.item_relation
    (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS'
    ) AS root_id,
    (
        SELECT 
            id 
        FROM 
            master.item 
        WHERE 
            abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT'
    ) AS parent_id,
    id as child_id,
    CASE 
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ADD_BLOCKS_ACT' THEN 1
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_ASSIGN_ENTRIES_ACT' THEN 2
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_MANAGE_BLOCKS_ACT' THEN 3
        WHEN abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_OVERVIEW_ACT' THEN 4
    ELSE 
        5
    END AS order_number,
    0,
    1,
    1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    (
        'EXPT_SELECTION_ADVANCEMENT_IRRI_ADD_BLOCKS_ACT',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_ASSIGN_ENTRIES_ACT',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_MANAGE_BLOCKS_ACT',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_OVERVIEW_ACT'
    );



--rollback DELETE FROM
--rollback     master.item_relation 
--rollback WHERE
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DATA_PROCESS'
--rollback     )
--rollback AND 
--rollback     parent_id 
--rollback IN
--rollback     (
--rollback          SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='EXPT_SELECTION_ADVANCEMENT_IRRI_DESIGN_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE
--rollback 	        abbrev 
--rollback         IN 
--rollback         (
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_ADD_BLOCKS_ACT',
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_ASSIGN_ENTRIES_ACT',
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_MANAGE_BLOCKS_ACT',
--rollback             'EXPT_SELECTION_ADVANCEMENT_IRRI_OVERVIEW_ACT'
--rollback         )
--rollback );