--liquibase formatted sql

--changeset postgres:populate_package_document_in_germplasm.package_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-404 Populate package_document in germplasm.package p2



UPDATE germplasm.package SET package_document = NULL;

CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		gp.id,
		concat(
			setweight(to_tsvector(gp.package_code),'A'),' ',
			setweight(to_tsvector(unaccent(gp.package_label)),'A'),' ',
			setweight(to_tsvector(gs.seed_code),'AB'),' ',
			setweight(to_tsvector(unaccent(gs.seed_name)),'AB'),' ',
			setweight(to_tsvector(gs.harvest_method),'C'),' ',
			setweight(to_tsvector(gs.harvest_date::text),'C'),' ',
			setweight(to_tsvector(gp.package_unit),'CD'),' ',
			setweight(to_tsvector(gp.package_status),'CD'),' ',
			setweight(to_tsvector(unaccent(gs.description)),'CD')
		) AS doc
	FROM 
		germplasm.package gp
	INNER JOIN
		germplasm.seed gs
	ON
		gs.id = gp.seed_id
	ORDER BY
		gp.id
)

UPDATE germplasm.package gp SET package_document = cast(t1.doc AS tsvector) FROM t1 WHERE gp.id = t1.id;



--rollback UPDATE germplasm.package SET package_document = NULL;
--rollback 
--rollback CREATE EXTENSION IF NOT EXISTS unaccent;
--rollback 
--rollback WITH t1 AS (
--rollback 	SELECT 
--rollback 		gp.id,
--rollback 		concat(
--rollback 			setweight(to_tsvector(unaccent(gs.seed_code)),'A'),' ',
--rollback 			setweight(to_tsvector(unaccent(gs.seed_name)),'A'),' ',
--rollback 			setweight(to_tsvector(gs.harvest_method),'B'),' ',
--rollback 			setweight(to_tsvector(gs.harvest_date::text),'C'),' ',
--rollback 			setweight(to_tsvector(unaccent(gs.description)),'CD')
--rollback 		) AS doc
--rollback 	FROM 
--rollback 		germplasm.package gp
--rollback 	INNER JOIN
--rollback 		germplasm.seed gs
--rollback 	ON
--rollback 		gs.id = gp.seed_id
--rollback 	ORDER BY
--rollback 		gp.id
--rollback )
--rollback 
--rollback UPDATE germplasm.package gp SET package_document = cast(t1.doc AS tsvector) FROM t1 WHERE gp.id = t1.id;