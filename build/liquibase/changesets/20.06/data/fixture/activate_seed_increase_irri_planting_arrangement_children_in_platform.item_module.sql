--liquibase formatted sql

--changeset postgres:activate_seed_increase_irri_planting_arrangement_children_in_platform.module context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Activate seed increase IRRI planting arrangement children in platform.item_module



UPDATE 
    platform.item_module 
SET 
    is_void = false 
WHERE
    item_id  
IN 
    (
        SELECT  
            id
        FROM
            master.item
        WHERE
            abbrev
        IN
            (
                'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT',
                'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT',
                'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT',
                'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT'           
            )
    )
AND
    module_id
IN
    (
        SELECT
            id
        FROM
            platform.module
        WHERE
            abbrev
        IN
            (
                'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD',
                'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD',
                'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD',
                'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD'
            )
    );



--rollback UPDATE 
--rollback     platform.item_module 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE
--rollback     item_id  
--rollback IN 
--rollback     (
--rollback         SELECT  
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT',
--rollback                 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT',
--rollback                 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT',
--rollback                 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT'           
--rollback             )
--rollback     )
--rollback AND
--rollback     module_id
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             platform.module
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                 'EXPT_SEED_INCREASE_IRRI_ADD_BLOCKS_ACT_MOD',
--rollback                 'EXPT_SEED_INCREASE_IRRI_ASSIGN_ENTRIES_ACT_MOD',
--rollback                 'EXPT_SEED_INCREASE_IRRI_MANAGE_BLOCKS_ACT_MOD',
--rollback                 'EXPT_SEED_INCREASE_IRRI_OVERVIEW_ACT_MOD'
--rollback             )
--rollback     );
