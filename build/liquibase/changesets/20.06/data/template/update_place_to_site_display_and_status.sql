--liquibase formatted sql

--changeset postgres:update_place_to_site_display_and_status context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update place to site display and status



--update the display
UPDATE 
    master.item 
SET 
    (name,description,display_name) = ('Specify Site', 'Specify Site', 'Site') 
WHERE   
    abbrev 
IN 
    (
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT',
        'EXPT_NURSERY_CB_PLACE_ACT',
        'EXPT_DEFAULT_PLACE_ACT',
        'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT',
        'EXPT_TRIAL_PLACE_ACT',
        'EXPT_NURSERY_PARENT_LIST_PLACE_ACT',
        'EXPT_NURSERY_CROSS_LIST_PLACE_ACT',
        'EXPT_CROSS_PRE_PLANNING_PLACE_ACT',
        'EXPT_SEED_INCREASE_PLACE_ACT',
        'EXPT_CROSS_POST_PLANNING_PLACE_ACT',
        'EXPT_TRIAL_IRRI_PLACE_ACT',
        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT',
        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT'
    );

--update the status
UPDATE 
    platform.module 
SET 
    required_status = 'occurrences created' 
WHERE 
    abbrev 
IN 
    (
        'EXPT_NURSERY_CB_PLACE_ACT_MOD',
        'EXPT_DEFAULT_PLACE_ACT_MOD',
        'EXPT_TRIAL_PLACE_ACT_MOD',
        'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_MOD',
        'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_MOD',
        'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_MOD',
        'EXPT_SEED_INCREASE_PLACE_ACT_MOD',
        'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_MOD',
        'EXPT_CROSS_POST_PLANNING_PLACE_ACT_MOD',
        'EXPT_TRIAL_IRRI_PLACE_ACT_MOD',
        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD'
    );



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name,description,display_name) = ('Specify Place', 'Specify Place', 'Place') 
--rollback WHERE   
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT',
--rollback         'EXPT_NURSERY_CB_PLACE_ACT',
--rollback         'EXPT_DEFAULT_PLACE_ACT',
--rollback         'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT',
--rollback         'EXPT_TRIAL_PLACE_ACT',
--rollback         'EXPT_NURSERY_PARENT_LIST_PLACE_ACT',
--rollback         'EXPT_NURSERY_CROSS_LIST_PLACE_ACT',
--rollback         'EXPT_CROSS_PRE_PLANNING_PLACE_ACT',
--rollback         'EXPT_SEED_INCREASE_PLACE_ACT',
--rollback         'EXPT_CROSS_POST_PLANNING_PLACE_ACT',
--rollback         'EXPT_TRIAL_IRRI_PLACE_ACT',
--rollback         'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT',
--rollback         'EXPT_SEED_INCREASE_IRRI_PLACE_ACT'
--rollback     );
--rollback 
--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     required_status = 'location rep studies created' 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPT_NURSERY_CB_PLACE_ACT_MOD',
--rollback         'EXPT_DEFAULT_PLACE_ACT_MOD',
--rollback         'EXPT_TRIAL_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_MOD',
--rollback         'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_PLACE_ACT_MOD',
--rollback         'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_MOD',
--rollback         'EXPT_CROSS_POST_PLANNING_PLACE_ACT_MOD',
--rollback         'EXPT_TRIAL_IRRI_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
--rollback         'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD'
--rollback     );