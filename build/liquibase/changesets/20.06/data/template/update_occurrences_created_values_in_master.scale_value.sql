--liquibase formatted sql

--changeset postgres:update_occurrences_created_values_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-602 Update occurrences_created values in master.scale_value



UPDATE
    master.scale_value
SET
    value = 'Occurrences Created',
    description = 'Occurrences Created',
    display_name = 'Occurrences Created',
    abbrev = 'EXPERIMENT_STATUS_OCCURRENCES_CREATED'
WHERE
    abbrev = 'EXPERIMENT_STATUS_OCCURRENCE_CREATED';



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Occurrence Created',
--rollback     description = 'Occurrence Created',
--rollback     display_name = 'Occurrence Created',
--rollback     abbrev = 'EXPERIMENT_STATUS_OCCURRENCE_CREATED'
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_STATUS_OCCURRENCES_CREATED';