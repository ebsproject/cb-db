--liquibase formatted sql

--changeset postgres:remove_design_tab_nursery_cross_list_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Remove design tab nursery cross list



UPDATE 
    master.item 
SET 
    is_void = true 
WHERE 
    abbrev = 'EXPT_NURSERY_CROSS_LIST_DESIGN_ACT';


UPDATE 
    master.item 
SET 
    is_void = true 
WHERE 
    abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     abbrev = 'EXPT_NURSERY_CROSS_LIST_DESIGN_ACT';

--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE 
--rollback     abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_DATA_PROCESS';