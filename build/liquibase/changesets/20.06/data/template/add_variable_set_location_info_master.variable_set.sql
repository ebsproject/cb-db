--liquibase formatted sql

--changeset postgres:add_variable_set_location_info_master.variable_set context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-401 Add variable set location info to master.variable_set



INSERT INTO
	master.variable_set (abbrev,name,creator_id) 
VALUES 
	('LOCATION_INFO','Location info',1);



--rollback DELETE FROM master.variable_set WHERE abbrev = 'LOCATION_INFO';