--liquibase formatted sql

--changeset postgres:add_variable_set_occurrence_info_to_master.variable_set context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-602 Add variable set OCCURRENCE_INFO to master.variable_set



INSERT INTO
    master.variable_set (abbrev,name,creator_id) 
VALUES 
    ('OCCURRENCE_INFO','Occurrence info',1);



--rollback DELETE FROM master.variable_set WHERE abbrev = 'OCCURRENCE_INFO';