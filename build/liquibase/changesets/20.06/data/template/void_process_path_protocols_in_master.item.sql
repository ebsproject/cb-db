--liquibase formatted sql

--changeset postgres:void_process_path_protocols_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Void process path protocols in master.item



UPDATE 
    master.item
SET
    is_void = true
WHERE
    abbrev
IN
    (
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT',
        'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT'
    );



--rollback UPDATE 
--rollback     master.item
--rollback SET
--rollback     is_void = false
--rollback WHERE
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'EXPT_SELECTION_ADVANCEMENT_IRRI_PROCESS_PATH_PROTOCOLS_ACT',
--rollback         'EXPT_SEED_INCREASE_IRRI_PROCESS_PATH_PROTOCOLS_ACT'
--rollback     );