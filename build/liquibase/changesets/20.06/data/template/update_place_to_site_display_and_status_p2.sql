--liquibase formatted sql

--changeset postgres:update_place_to_site_display_and_status_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update place to site display and status p2



--update the status
UPDATE 
    platform.module 
SET 
    (name, description,action_id) = ('Specify Occurrences','Specify Occurrences','specify-occurrences')
WHERE 
    abbrev 
IN 
    (
        'EXPT_NURSERY_CB_PLACE_ACT_MOD',
        'EXPT_DEFAULT_PLACE_ACT_MOD',
        'EXPT_TRIAL_PLACE_ACT_MOD',
        'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_MOD',
        'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_MOD',
        'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_MOD',
        'EXPT_SEED_INCREASE_PLACE_ACT_MOD',
        'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_MOD',
        'EXPT_CROSS_POST_PLANNING_PLACE_ACT_MOD',
        'EXPT_TRIAL_IRRI_PLACE_ACT_MOD',
        'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_MOD',
        'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
        'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD'
    );



--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     (name, description,action_id) = ('Specify Locations','Specify Locations','specify-locations')
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'EXPT_NURSERY_CB_PLACE_ACT_MOD',
--rollback         'EXPT_DEFAULT_PLACE_ACT_MOD',
--rollback         'EXPT_TRIAL_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_MOD',
--rollback         'EXPT_CROSS_PRE_PLANNING_PLACE_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_PLACE_ACT_MOD',
--rollback         'EXPT_SELECTION_ADVANCEMENT_PLACE_ACT_MOD',
--rollback         'EXPT_CROSS_POST_PLANNING_PLACE_ACT_MOD',
--rollback         'EXPT_TRIAL_IRRI_PLACE_ACT_MOD',
--rollback         'EXPT_NURSERY_CROSS_LIST_IRRIHQ_PLACE_ACT_MOD',
--rollback         'EXPT_SEED_INCREASE_IRRI_PLACE_ACT_MOD',
--rollback         'EXPT_SELECTION_ADVANCEMENT_IRRI_PLACE_ACT_MOD'
--rollback     );