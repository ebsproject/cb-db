--liquibase formatted sql

--changeset postgres:update_config_expt_nursery_cb_entry_list_act_val_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update config EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default place metadata variables for nursery crossing block data process",
            "Values": [{
                    "fixed": true,
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_ROLE",
                    "allowed_values": [
                        "ENTRY_ROLE_FEMALE",
                        "ENTRY_ROLE_FEMALE_AND_MALE",
                        "ENTRY_ROLE_MALE"
                    ],
                    "display_name": "Parent Role",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                    
                }
            ]
        }
    ' 
WHERE 
    abbrev = 'EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default place metadata variables for nursery crossing block data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "fixed": true,
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "allowed_values": [
--rollback                         "ENTRY_ROLE_FEMALE",
--rollback                         "ENTRY_ROLE_FEMALE_AND_MALE",
--rollback                         "ENTRY_ROLE_MALE"
--rollback                     ],
--rollback                     "variable_abbrev": "ENTRY_ROLE",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "ENTRY_CLASS",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "entries",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL';