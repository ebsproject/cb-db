--liquibase formatted sql

--changeset postgres:update_config_expt_nursery_cross_list_place_act_val_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update config EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL in platform.config p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for nursery parent list data process",
            "Values": [
                {
                "required": "required",
                "target_column": "occurrenceName",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "OCCURRENCE_NAME"
                },
                {
                "required": "required",
                "target_column": "siteDbId",
                "secondary_target_column":"geospatialObjectDbId",
                "target_value":"geospatialObjectName",
                "api_resource_method" : "POST",
                "api_resource_endpoint" : "geospatial-objects-search",
                "api_resource_filter" : {"geospatialObjectType": "site"},
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "SITE"
                },
                {
                "target_column": "fieldDbId",
                "secondary_target_column":"geospatialObjectDbId",
                "target_value":"scaleName",
                "api_resource_method" : "POST",
                "api_resource_endpoint" : "geospatial-objects-search",
                "api_resource_filter" : {"geospatialObjectType": "field"},
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "FIELD"
                },
                {
                "disabled": false,
                "target_column": "description",
                "secondary_target_column": "",
                "target_value": "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "variable_type" : "identification",
                "variable_abbrev": "DESCRIPTION"
                },
                {
                "allow_new_val": true,
                "target_column": "contactPerson",
                "secondary_target_column": "personDbId",
                "target_value":"personName",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=personName",
                "api_resource_method": "GET",
                "api_resource_endpoint": "persons",
                "variable_type" : "metadata",
                "variable_abbrev": "CONTCT_PERSON_CONT"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for nursery parent list data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "target_value": "",
--rollback                     "target_column": "",
--rollback                     "variable_type": "identification",
--rollback                     "variable_abbrev": "DESCRIPTION",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "target_value": "",
--rollback                     "allow_new_val": true,
--rollback                     "target_column": "",
--rollback                     "variable_type": "metadata",
--rollback                     "variable_abbrev": "CONTCT_PERSON_CONT",
--rollback                     "api_resource_sort": "",
--rollback                     "api_resource_filter": "",
--rollback                     "api_resource_method": "",
--rollback                     "api_resource_endpoint": "",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_PLACE_ACT_VAL';