--liquibase formatted sql

--changeset postgres:update_table_id_of_trait_entity_in_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-473 Update table_id of trait entity in dictionary.entity



UPDATE 
    dictionary.entity
SET
    table_id = (
                    SELECT 
                        id 
                    FROM 
                        dictionary.table 
                    WHERE 
                        abbrev = 'VARIABLE'
                )
WHERE
    abbrev = 'TRAIT';



--rollback UPDATE 
--rollback     dictionary.entity
--rollback SET
--rollback     table_id = (
--rollback                     SELECT 
--rollback                         id 
--rollback                     FROM 
--rollback                         dictionary.table 
--rollback                     WHERE 
--rollback                         abbrev = 'TRAIT'
--rollback                 )
--rollback WHERE
--rollback     abbrev = 'TRAIT';