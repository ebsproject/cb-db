--liquibase formatted sql

--changeset postgres:update_config_expt_nursery_parent_list_place_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Update config EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for nursery parent list data process",
            "Values": [
            {
                "disabled": false,
                "target_column": "",
                "secondary_target_column": "",
                "target_value": "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "variable_type" : "identification",
                "variable_abbrev": "DESCRIPTION"
            },
            {
                "allow_new_val": true,
                "target_column": "",
                "secondary_target_column": "",
                "target_value": "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "variable_type" : "metadata",
                "variable_abbrev": "CONTCT_PERSON_CONT"
            }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default place metadata variables for nursery parent list data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Place Remarks"
--rollback                 },
--rollback                 {
--rollback                     "field_label": "Contact person",
--rollback                     "allow_new_val": true,
--rollback                     "variable_abbrev": "CONTCT_PERSON_CONT"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE abbrev = 'EXPT_NURSERY_PARENT_LIST_PLACE_ACT_VAL';