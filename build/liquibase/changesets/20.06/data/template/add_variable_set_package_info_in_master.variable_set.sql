--liquibase formatted sql

--changeset postgres:add_variable_set_package_info_in_master.variable_set context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-300 Add variable set package info to master.variable_set



INSERT INTO
	master.variable_set (abbrev,name,creator_id) 
VALUES 
	('PACKAGE_INFO','Package Info',1) 



--rollback DELETE FROM master.variable_set WHERE abbrev = 'PACKAGE_INFO';