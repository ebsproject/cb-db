--liquibase formatted sql

--changeset postgres:update_trait_schema_and_name_in_dictionary.table context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-657 Update trait schema and name in dictionary.table



UPDATE
    dictionary.table
SET
    schema_id = (
        SELECT
            id
        FROM
            dictionary.schema
        WHERE
            abbrev = 'MASTER'
    ),
    name = 'variable'
WHERE
    abbrev = 'TRAIT';



--rollback UPDATE
--rollback     dictionary.table
--rollback SET
--rollback     schema_id = (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             dictionary.schema
--rollback         WHERE
--rollback             abbrev = 'GERMPLASM'
--rollback     ),
--rollback     name = 'Trait'
--rollback WHERE
--rollback     abbrev = 'TRAIT';