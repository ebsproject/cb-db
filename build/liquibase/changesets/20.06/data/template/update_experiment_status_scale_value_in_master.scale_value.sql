--liquibase formatted sql

--changeset postgres:update_experiment_status_scale_value_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-602 Update EXPERIMENT_STATUS scale value



UPDATE 
    master.scale_value
SET
    (value,description,display_name,abbrev) = ('Occurrence Created','Occurrence Created','Occurrence Created','EXPERIMENT_STATUS_OCCURRENCE_CREATED')
WHERE
    value = 'Location Rep Studies Created'
AND
    scale_id 
IN  
    (
        SELECT 
            scale_id 
        FROm 
            master.variable 
        WHERE 
            abbrev = 'EXPERIMENT_STATUS'
    );



--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     (value,description,display_name,abbrev) = ('Location Rep Studies Created','Location Rep Studies Created','Location Rep Studies Created','EXPERIMENT_STATUS_LOCATION_REP_STUDIES_CREATED')
--rollback WHERE
--rollback     value = 'Occurrence Created'
--rollback AND
--rollback     scale_id 
--rollback IN  
--rollback     (
--rollback         SELECT 
--rollback             scale_id 
--rollback         FROm 
--rollback             master.variable 
--rollback         WHERE 
--rollback             abbrev = 'EXPERIMENT_STATUS'
--rollback     );