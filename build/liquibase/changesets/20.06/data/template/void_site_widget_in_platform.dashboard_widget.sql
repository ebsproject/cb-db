--liquibase formatted sql

--changeset postgres:void_site_widget_in_platform.dashboard_widget context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Void site_widget in platform.dashboard_widget



UPDATE
    platform.dashboard_widget
SET
    is_void = true
WHERE
    abbrev = 'SITES';



--rollback UPDATE
--rollback     platform.dashboard_widget
--rollback SET
--rollback     is_void = false
--rollback WHERE
--rollback     abbrev = 'SITES';