--liquibase formatted sql

--changeset postgres:update_variable_location_name_label_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-401 Update variable LOCATION_NAME label in master.variable



UPDATE 
	master.variable
SET
	label = 'Location name'
WHERE
	abbrev = 'LOCATION_NAME';



--rollback UPDATE 
--rollback 	master.variable
--rollback SET
--rollback 	label = 'Distance between plots'
--rollback WHERE
--rollback 	abbrev = 'LOCATION_NAME';