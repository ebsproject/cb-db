--liquibase formatted sql

--changeset postgres:add_quality_control_config_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-398 Add QUALITY_CONTROL config in platform.config



INSERT INTO 
    platform.config 
    (abbrev, "name", config_value, "rank", "usage", remarks, creation_timestamp, creator_id, notes, is_void) 
VALUES
    (
        'QUALITY_CONTROL_DEFAULTS',
        'Quality Control default values',
        '{"name": "Quality Control","defaults":{"commit_threshold": 1000,"suppress_record_threshold": 100,"suppress_by_rule_threshold": 100}}',
        1, 
        'quality_control', 
        '', 
        now(), 
        1, 
        'added by n.carumba', 
        false
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'QUALITY_CONTROL_DEFAULTS';