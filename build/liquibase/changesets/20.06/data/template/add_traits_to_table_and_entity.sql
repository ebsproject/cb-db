--liquibase formatted sql

--changeset postgres:add_traits_to_table_and_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-473 Add trait to table and entity



INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
VALUES
    (
        (SELECT id FROM dictionary.database WHERE abbrev='BIMS_0.10_PROD_OLD_VERSION'),
        (SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'),
        'TRAIT',
        'Trait',
        'Trait',
        1
    );

INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
VALUES
    (
        'TRAIT',
        'Trait',
        'Trait',
        (SELECT id FROM dictionary.table WHERE abbrev='TRAIT'),
        1
    );



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'TRAIT';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'TRAIT';