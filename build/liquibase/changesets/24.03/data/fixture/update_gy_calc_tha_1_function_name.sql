--liquibase formatted sql

--changeset postgres:update_gy_calc_tha_1_function_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3250 CB-DB: Update function name for gy_calc_tha_1



UPDATE master.formula 
SET
database_formula = 'CREATE OR REPLACE FUNCTION master.formula_gy_calc_tha_1(
                gy_calc_kgha float
            ) RETURNS float AS
            $body$
            BEGIN    
              RETURN gy_calc_kgha/1000;
            END
            $body$
            LANGUAGE plpgsql;'
WHERE formula = 'GY_CALC_THA = GY_CALC_KGHA/1000'



--rollback UPDATE master.formula 
--rollback SET database_formula = 'CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_1(gy_calc_kgha float) RETURNS float AS $body$ BEGIN RETURN GY_CALC_KGHA/1000;END$body$LANGUAGE plpgsql;' WHERE formula IN ('GY_CALC_THA = GY_CALC_KGHA/1000');



--changeset postgres:insert_function_of_gy_calc_tha_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert GY_CALC_THA function usng GY_CALC_KGHA



DROP FUNCTION master.formula_gy_cacl_tha_1;

CREATE OR REPLACE FUNCTION master.formula_gy_calc_tha_1(
    gy_calc_kgha float
) RETURNS float AS
$body$
BEGIN    
  RETURN gy_calc_kgha/1000;
END
$body$
LANGUAGE plpgsql;



--rollback DROP FUNCTION master.formula_gy_calc_tha_1;
--rollback CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_1(
--rollback     gy_calc_kgha float
--rollback ) RETURNS float AS
--rollback $body$
--rollback BEGIN    
--rollback   RETURN GY_CALC_KGHA/1000;
--rollback END
--rollback $body$
--rollback LANGUAGE plpgsql;