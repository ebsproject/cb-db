--liquibase formatted sql

--changeset postgres:insert_variable_SUB_21DAD context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3257 CB-DB: Insert SUB_21DAD variable record



--GY_CALC_KGDA
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('SUB_21DAD', 'SUB_21DAD', 'SUB_21DAD', 'float', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1', 'SUB_21DAD', '24.03')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'SUB_21DAD' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('SUB_21DAD', 'SUB_21DAD', 'SUB_21DAD') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('SUB_21DAD_METHOD', 'SUB_21DAD method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('SUB_21DAD_SCALE', 'SUB_21DAD scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('SUB_21DAD');
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('SUB_21DAD_METHOD');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('SUB_21DAD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('SUB_21DAD_SCALE');   