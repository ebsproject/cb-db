--liquibase formatted sql

--changeset postgres:populate_occurrence_data_for_occurrence_AF2023_AL_T30_V2a-004 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3237 CB-DB: Insert occurrence_data for occurrence AF2023_AL_T30_V2a-004.



INSERT INTO 
    experiment.occurrence_data
        (occurrence_id,variable_id,data_value,data_qc_code,creator_id,protocol_id)
VALUES
((SELECT id FROM experiment.occurrence WHERE occurrence_name = 'AF2023_AL_T30_V2a-004'),(SELECT id FROM master.variable WHERE abbrev = 'TRAIT_PROTOCOL_LIST_ID'),(SELECT id FROM platform.list WHERE abbrev = 'TRAIT_PROTOCOL_EXP0017557-004'),'G',(SELECT id FROM tenant.person WHERE username='admin'),(SELECT id FROM tenant.protocol WHERE protocol_code = 'TRAIT_PROTOCOL_EXP0017557')),
((SELECT id FROM experiment.occurrence WHERE occurrence_name = 'AF2023_AL_T30_V2a-004'),(SELECT id FROM master.variable WHERE abbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'),(SELECT id FROM platform.list WHERE abbrev = 'MANAGEMENT_PROTOCOL_EXP0017557-004'),'G',(SELECT id FROM tenant.person WHERE username='admin'),(SELECT id FROM tenant.protocol WHERE protocol_code = 'MANAGEMENT_PROTOCOL_EXP0017557'))
;



--rollback DELETE FROM
--rollback    experiment.occurrence_data
--rollback WHERE
--rollback    occurrence_id 
--rollback IN
--rollback    (SELECT id FROM experiment.occurrence WHERE occurrence_name = 'AF2023_AL_T30_V2a-004')
--rollback ;