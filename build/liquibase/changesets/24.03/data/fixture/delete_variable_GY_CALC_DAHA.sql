--liquibase formatted sql

--changeset postgres:delete_variable_GY_CALC_DAHA context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3256 CB-DB: Delete variable records of GY_CALC_DAHA



DELETE FROM master.property WHERE abbrev IN
('GY_CALC_DAHA');
DELETE FROM master.formula_parameter where formula_id in (SELECT id FROM master.formula WHERE formula = 'GY_CALC_DAHA = GY_CALC_DAHA/100');
DELETE FROM master.formula WHERE formula IN ('GY_CALC_DAHA = GY_CALC_DAHA/100');
DELETE FROM master.method WHERE abbrev IN
('GY_CALC_DAHA_METHOD');
DELETE FROM master.variable WHERE abbrev IN
('GY_CALC_DAHA');
DELETE FROM master.scale WHERE abbrev IN
('GY_CALC_DAHA_SCALE');
DROP FUNCTION master.formula_gy_calc_daha;         



--rollback DO $$
--rollback DECLARE
--rollback     var_property_id int;
--rollback     var_method_id int;
--rollback     var_scale_id int;
--rollback     var_variable_id int;
--rollback BEGIN
--rollback     -- variable
--rollback     INSERT INTO
--rollback         master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
--rollback     VALUES
--rollback         ('GY_CALC_DAHA', 'GY_CALC_DAHA', 'GY_Calc_daha', 'float', false, 'system', 'experiment, occurrence, location, plot', 'application', 'Grain yield', 'active', '1', 'Grain yield','23.11')
--rollback     RETURNING id INTO var_variable_id;
--rollback 
--rollback     -- property
--rollback     SELECT id FROM master.property WHERE abbrev = 'GY_CALC_DAHA' INTO var_property_id;
--rollback     IF var_property_id IS NULL THEN
--rollback         INSERT INTO
--rollback             master.property (abbrev, name, display_name)
--rollback         VALUES
--rollback             ('GY_CALC_DAHA', 'GY_Calc_daha', 'Grain yield') 
--rollback         RETURNING id INTO var_property_id;
--rollback     END IF;
--rollback 
--rollback     -- method
--rollback     INSERT INTO
--rollback         master.method (abbrev, name) 
--rollback     VALUES
--rollback         ('GY_CALC_DAHA_METHOD', 'GY_Calc_daha method')
--rollback     RETURNING id INTO var_method_id;
--rollback 
--rollback     -- scale
--rollback     INSERT INTO
--rollback         master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
--rollback     VALUES
--rollback         ('GY_CALC_DAHA_SCALE', 'GY_Calc_daha scale', NULL, NULL, NULL, NULL)
--rollback     RETURNING id INTO var_scale_id;
--rollback 
--rollback     -- update references
--rollback     UPDATE
--rollback         master.variable
--rollback     SET
--rollback         property_id = var_property_id,
--rollback         method_id = var_method_id,
--rollback         scale_id = var_scale_id
--rollback     WHERE
--rollback         id = var_variable_id
--rollback     ;
--rollback END; $$;
--rollback 
--rollback INSERT INTO master.formula (formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
--rollback     VALUES ('GY_CALC_DAHA = GY_CALC_DAHA/100', (SELECT id FROM master.variable WHERE abbrev = 'GY_CALC_DAHA'), (SELECT id FROM master.method WHERE abbrev = 'GY_CALC_DAHA_METHOD'), 'plot', '
--rollback     master.formula_gy_calc_daha(gy_calc_daha)', NULL, 'create or replace function master.formula_gy_calc_daha(gy_calc_daha float) returns float as$body$declare gy_calc_daha float;Local_gy_calc_daha float;begin gy_calc_daha = gy_calc_daha/100; return round(gy_calc_daha::numeric, 3); end $body$ language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');
--rollback 
--rollback INSERT INTO master.formula_parameter
--rollback     (formula_id, param_variable_id, data_level, result_variable_id)
--rollback SELECT
--rollback     form.id AS formula_id,
--rollback     pvar.id AS param_variable_id,
--rollback     pvar.data_level,
--rollback     form.result_variable_id
--rollback FROM
--rollback     master.formula AS form
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.id = form.result_variable_id,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     pvar.abbrev IN ('GY_CALC_DAHA') -- must be the same order as defined in the database function
--rollback     AND var.abbrev = 'GY_CALC_DAHA'
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION master.formula_gy_calc_daha(
--rollback 				gy_calc_daha float
--rollback 			) RETURNS float as
--rollback 			$body$
--rollback 			DECLARE
--rollback 			     gy_calc_daha float;
--rollback               local_gy_calc_daha float;
--rollback 			BEGIN
--rollback 				
--rollback gy_calc_daha = gy_calc_daha/100;
--rollback 				
--rollback 				RETURN round(gy_calc_daha::numeric, 3);
--rollback 			END
--rollback 			$body$
--rollback 			language plpgsql;
