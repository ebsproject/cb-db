--liquibase formatted sql

--changeset postgres:insert_other_paramater_of_SUB_SURVIVAL_PCT context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3257 CB-DB: Insert SUB_21DAD as formula_paramteter of SUB_SURVIVAL_PCT



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id, order_number)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id,
    1 AS order_number
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('SUB_21DAD') -- must be the same order as defined in the database function
    AND var.abbrev = 'SUB_SURVIVAL_PCT'
;



--rollback DELETE FROM master.formula_parameter 
--rollback where formula_id in (SELECT id FROM master.formula WHERE formula = 'SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100')
--rollback AND param_variable_id in (SELECT id FROM master.variable where abbrev = 'SUB_21DAD');
