--liquibase formatted sql

--changeset postgres:update_SUB_SURVIVAL_PCT_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3257 CB-DB: Update formula of SUB_SURVIVAL_PCT



UPDATE 
	master.formula
SET 
	database_formula = 
	'CREATE OR REPLACE FUNCTION  master.formula_sub_survival_pct(
			sub_21dad float,
            total_hill_cont float)
		RETURNS double precision
		LANGUAGE plpgsql
	AS $BODY$

		BEGIN
			RETURN sub_21dad/total_hill_cont::float * 100;
		END
				
	$BODY$;'
WHERE formula = 'SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100';



--rollback UPDATE 
--rollback 	master.formula
--rollback SET 
--rollback 	database_formula = 
--rollback 	'create or replace function master.formula_sub_survival_pct( sub_21dad float, total_hill_cont float) returns float as$body$ declare  sub_survival_pct float;  local_sub_21dad float;  local_total_hill_cont float; begin sub_survival_pct = sub_21dad/total_hill_cont::float * 100; return round(sub_survival_pct::numeric, 3); end $body$ language plpgsql;' WHERE formula = 'SUB_SURVIVAL_PCT = SUB_21DAD/TOTAL_HILL_CONT::float * 100';



--changeset postgres:update_SUB_SURVIVAL_PCT_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3257 CB-DB: Update formula function of SUB_SURVIVAL_PCT



CREATE OR REPLACE FUNCTION  master.formula_sub_survival_pct(
			sub_21dad float,
            total_hill_cont float)
		RETURNS double precision
		LANGUAGE plpgsql
	AS $BODY$

		BEGIN
			RETURN sub_21dad/total_hill_cont::float * 100;
		END
				
	$BODY$;



--rollback CREATE OR REPLACE FUNCTION master.formula_sub_survival_pct(
--rollback                 sub_21dad float,
--rollback                 total_hill_cont float
--rollback 			) RETURNS float as
--rollback 			$body$
--rollback 			DECLARE
--rollback 			  	 sub_survival_pct float;
--rollback               local_sub_21dad float;
--rollback               local_total_hill_cont float;
--rollback 			BEGIN
--rollback 				
--rollback sub_survival_pct = sub_21dad/total_hill_cont::float * 100;
--rollback 				
--rollback 				RETURN round(sub_survival_pct::numeric, 3);
--rollback 			END
--rollback 			$body$
--rollback 			language plpgsql;
