--liquibase formatted sql

--changeset postgres:delete_excess_formula_parameter_SHELL_CMP_PCT context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3256 CB-DB: Delete excess formula parameters connected to SHELL_CMP_PCT



DELETE FROM master.formula_parameter WHERE
formula_id IN (SELECT id FROM master.formula v WHERE formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100') AND 
data_level = 'experiment, occurrence, location, plot'



--rollback INSERT INTO master.formula_parameter
--rollback     (formula_id, param_variable_id, data_level, result_variable_id)
--rollback SELECT
--rollback     form.id AS formula_id,
--rollback     pvar.id AS param_variable_id,
--rollback     'experiment, occurrence, location, plot' AS data_level,
--rollback     form.result_variable_id
--rollback FROM
--rollback     master.formula AS form
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.id = form.result_variable_id,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     pvar.abbrev IN ('GW_SUB_M_KG', 'EW_SUB_M_KG') -- must be the same order as defined in the database function
--rollback     AND var.abbrev = 'SHELL_CMP_PCT'
--rollback     AND form.formula = 'SHELL_CMP_PCT=(GW_SUB_M_KG/EW_SUB_M_KG)*100';
--rollback ;
