--liquibase formatted sql

--changeset postgres:delete_excess_formula_parameter_GW_CMP_KG_using_EW_M_KG context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3250 CB-DB: delete excess formula parameter in formula GW_CMP_KG=EW_M_KG*0.8


DELETE FROM master.formula_parameter 
WHERE 
formula_id IN (SELECT id FROM master.formula WHERE formula ilike 'GW_CMP_KG=EW_M_KG*0.8') AND
param_variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'SHELL_CMP_PCT');

DELETE FROM master.formula_parameter fp WHERE 
id IN (SELECT id FROM master.formula_parameter fp WHERE
formula_id IN (SELECT id FROM master.formula WHERE formula ilike 'GW_CMP_KG=EW_M_KG*0.8') AND 
param_variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'EW_M_KG') ORDER BY id ASC LIMIT 1);



--rollback INSERT INTO master.formula_parameter
--rollback     (formula_id, param_variable_id, data_level, result_variable_id)
--rollback SELECT
--rollback     form.id AS formula_id,
--rollback     pvar.id AS param_variable_id,
--rollback     pvar.data_level,
--rollback     form.result_variable_id
--rollback FROM
--rollback     master.formula AS form
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.id = form.result_variable_id,
--rollback     master.variable AS pvar
--rollback WHERE
--rollback     pvar.abbrev IN ('SHELL_CMP_PCT', 'EW_M_KG') -- must be the same order as defined in the database function
--rollback     AND var.abbrev = 'GW_CMP_KG'
--rollback     AND form.formula = 'GW_CMP_KG=EW_M_KG*0.8';
--rollback ;
--rollback
--rollback UPDATE master.formula_parameter
--rollback SET order_number = 1
--rollback WHERE formula_id IN (SELECT id FROM master.formula WHERE formula ilike 'GW_CMP_KG=EW_M_KG*0.8') AND 
--rollback param_variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'EW_M_KG');