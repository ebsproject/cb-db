--liquibase formatted sql

--changeset postgres:update_gy_calc_tha_3_function_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3250 CB-DB: Update function name for gy_calc_tha_3



UPDATE master.formula 
SET
database_formula = 'CREATE OR REPLACE FUNCTION master.formula_gy_calc_tha_3(
                gy_calc_kgda float
            ) RETURNS float as
            $body$
            BEGIN    
              RETURN gy_calc_kgda/100;
            END
            $body$
            LANGUAGE plpgsql;'
WHERE formula = 'GY_CALC_THA = GY_CALC_KGDA/100'



--rollback UPDATE master.formula 
--rollback SET database_formula = 'CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_3(gy_calc_kgda float) RETURNS float as $body$ BEGIN RETURN gy_calc_kgda/100;END$body$LANGUAGE plpgsql;' WHERE formula IN ('GY_CALC_THA = GY_CALC_KGDA/100');



--changeset postgres:insert_function_of_gy_calc_tha_3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3196 CB-DB: Insert GY_CALC_THA function usng GY_CALC_KGDA



DROP FUNCTION master.formula_gy_cacl_tha_3;

CREATE OR REPLACE FUNCTION master.formula_gy_calc_tha_3(
    gy_calc_kgda float
) RETURNS float as
$body$
BEGIN    
  RETURN gy_calc_kgda/100;
END
$body$
LANGUAGE plpgsql;

--rollback DROP FUNCTION master.formula_gy_calc_tha_3;
--rollback CREATE OR REPLACE FUNCTION master.formula_gy_cacl_tha_3(
--rollback     gy_calc_kgda float
--rollback ) RETURNS float as
--rollback $body$
--rollback BEGIN    
--rollback   RETURN gy_calc_kgda/100;
--rollback END
--rollback $body$
--rollback LANGUAGE plpgsql;