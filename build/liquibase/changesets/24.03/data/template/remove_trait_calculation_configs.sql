--liquibase formatted sql

--changeset postgres:remove_trait_calculation_configs.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6993: DC-DB: Create changeset for cleaning up the configs created for trait calculation default list


DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_PLOT_SETTINGS';

DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_PLOT_SETTINGS';

DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_PLOT_SETTINGS';

DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW_PLOT_SETTINGS';

DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_PLOT_SETTINGS';



--rollback DO $$ 
--rollback DECLARE
--rollback     json_array jsonb;
--rollback BEGIN

--rollback     SELECT 
--rollback         jsonb_agg(abbrev) 
--rollback     INTO 
--rollback         json_array
--rollback     FROM (
--rollback         SELECT DISTINCT
--rollback             master.variable.abbrev
--rollback         FROM
--rollback             master.formula_parameter
--rollback         JOIN
--rollback             master.variable ON master.formula_parameter.result_variable_id = master.variable.id
--rollback         WHERE
--rollback             master.formula_parameter.is_void = false
--rollback         AND
--rollback             master.variable.is_void = false
--rollback     ) subquery;

--rollback     INSERT INTO platform.config
--rollback     (
--rollback         abbrev,
--rollback         name,
--rollback         config_value,
--rollback         usage
--rollback     )
--rollback     VALUES
--rollback     (
--rollback         'DC_GLOBAL_PLOT_SETTINGS',
--rollback         'DC Global Plot Settings',
--rollback         jsonb_build_object(
--rollback             'CALCULATED_TRAITS', json_array
--rollback         ),
--rollback         'data_collection'
--rollback     );

--rollback     INSERT INTO platform.config
--rollback     (
--rollback         abbrev,
--rollback         name,
--rollback         config_value,
--rollback         usage
--rollback     )
--rollback     VALUES
--rollback     (
--rollback         'DC_PROGRAM_IRSEA_PLOT_SETTINGS',
--rollback         'DC Program IRSEA Plot Settings',
--rollback         '{
--rollback             "CALCULATED_TRAITS": [
--rollback                "ADJAYLD_G_CONT",
--rollback                "ADJAYLD_KG_CONT",
--rollback                "ADJAYLD_KG_CONT4",
--rollback                "ADJHVAREA_CONT1",
--rollback                "ADJHVAREA_CONT2",
--rollback                "ADJHVAREA_CONT3",
--rollback                "ADJYLD3_CONT",
--rollback                "AREA_FACT",
--rollback                "AYLD_CONT",
--rollback                "DISEASE_INDEX",
--rollback                "GYLD_DSR_KG",
--rollback                "HI_CONT",
--rollback                "HV_AREA_DSR_SQM",
--rollback                "HV_AREA_SQM",
--rollback                "HVHILL_PCT",
--rollback                "MISS_HILL_CONT",
--rollback                "PLOT_AREA_DSR_SQM",
--rollback                "STRWWT_CONT",
--rollback                "STRWWTBM_CONT",
--rollback                "SUB_SURVIVAL_PCT",
--rollback                "TOT_PLTGAP_AREA_SQM",
--rollback                "TOTAL_HILL_CONT",
--rollback                "TOTBM_CONT_KG",
--rollback                "TOTBM_CONT1",
--rollback                "YLD_0_CONT1",
--rollback                "YLD_0_CONT1",
--rollback                "YLD_0_CONT1",
--rollback                "YLD_0_CONT1",
--rollback                "YLD_0_CONT2",
--rollback                "YLD_0_CONT3",
--rollback                "YLD_0_CONT4",
--rollback                "YLD_0_CONT5",
--rollback                "YLD_0_CONT6",
--rollback                "YLD_0_CONT7",
--rollback                "YLD_CONT_TON",
--rollback                "YLD_CONT_TON2",
--rollback                "YLD_CONT_TON5",
--rollback                "YLD_CONT_TON6",
--rollback                "YLD_CONT_TON7",
--rollback                "YLD_CONT1",
--rollback                "YLD_CONT2",
--rollback                "YLD_CONT4",
--rollback                "YLD_DSR_TON"
--rollback            ]
--rollback        }',
--rollback        'data_collection'
--rollback    );

--rollback    INSERT INTO platform.config
--rollback    (
--rollback        abbrev,
--rollback        name,
--rollback        config_value,
--rollback        usage
--rollback    )
--rollback    VALUES
--rollback    (
--rollback        'DC_PROGRAM_KE_PLOT_SETTINGS',
--rollback        'DC Program KE Plot Settings',
--rollback        '{
--rollback            "CALCULATED_TRAITS": [
--rollback                "GY_CMP_THA",
--rollback                "GY_MOI15_CMP_THA",
--rollback                "GY_MOI13_5_CMP_THA",
--rollback                "GW_M_KG",
--rollback                "GW_CMP_KG",
--rollback                "GW_SUB_M_KG",
--rollback                "EW_M_KG",
--rollback                "EW_SUB_M_KG",
--rollback                "SHELL_CMP_PCT",
--rollback                "GMOI_M_PCT",
--rollback                "PLOT_AREA_HARVESTED"
--rollback            ]
--rollback        }',
--rollback        'data_collection'
--rollback    );

--rollback    INSERT INTO platform.config
--rollback    (
--rollback        abbrev,
--rollback        name,
--rollback        config_value,
--rollback        usage
--rollback    )
--rollback    VALUES
--rollback    (
--rollback        'DC_PROGRAM_BW_PLOT_SETTINGS',
--rollback        'DC Program BW Plot Settings',
--rollback        '{
--rollback            "CALCULATED_TRAITS" : [
--rollback                "GY_CALC_KGHA",
--rollback                "GY_CALC_GM2",
--rollback                "GY_CALC_DAHA",
--rollback                "GY_CALC_THA",
--rollback                "PH_M_CM",
--rollback                "PH_M_M",
--rollback                "PLANTING_DATE",
--rollback                "EMER_DATE_YMD",
--rollback                "EMER_DTO_DAY",
--rollback                "BOOT_DATEINIT_YMD",
--rollback                "BOOT_DTOINIT_DAY",
--rollback                "BOOT_DATE_YMD",
--rollback                "BOOT_DTO_DAY",
--rollback                "HD_DATE_YMD",
--rollback                "HD_DTO_DAY",
--rollback                "MAT_DATE_YMD",
--rollback                "MAT_DTO_DAY",
--rollback                "TSPL_DATE_YMD",
--rollback                "TSPL_DTO_DAY"
--rollback            ]
--rollback        }',
--rollback        'data_collection'
--rollback    );

--rollback     INSERT INTO platform.config
--rollback     (
--rollback         abbrev,
--rollback         name,
--rollback         config_value,
--rollback         usage
--rollback     )
--rollback     VALUES
--rollback     (
--rollback         'DC_ROLE_COLLABORATOR_PLOT_SETTINGS',
--rollback         'DC Role Collaborator Plot Settings',
--rollback         jsonb_build_object(
--rollback             'CALCULATED_TRAITS', json_array
--rollback         ),
--rollback         'data_collection'
--rollback     );
--rollback END $$;