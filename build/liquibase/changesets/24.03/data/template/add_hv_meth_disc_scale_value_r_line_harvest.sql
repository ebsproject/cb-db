--liquibase formatted sql

--changeset postgres:add_hv_meth_disc_scale_value_r_line_harvest context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6631 CB HM DB: Add R-line Harvest as new harvest method in HV_METH_DISC scale values



-- add scale value to HV_METH_DISC variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'HV_METH_DISC'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('R-line Harvest', 'R-line Harvest', 'R-line Harvest', 'R_LINE_HARVEST', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'HV_METH_DISC'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HV_METH_DISC_R_LINE_HARVEST'
--rollback     )
--rollback ;