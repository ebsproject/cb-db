--liquibase formatted sql

--changeset postgres:update_gm_update_global_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6962 CB-GM: Update -  download germplasm file using a list is broken



-- update GLOBAL config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"CROSS_NUMBER",
                "display_value":"Cross Number",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"GROWTH_HABIT",
                "display_value":"Growth Habit",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"GRAIN_COLOR",
                "display_value":"Grain Color",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"MTA_STATUS",
                "display_value":"MTA Status",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-mta-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmMtaDbId",
                    "search_filter":"mtaStatus",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"HETEROTIC_GROUP",
                "display_value":"Heterotic Group",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"DH_FIRST_INCREASE_COMPLETED",
                "display_value":"Double Haploid First Increase Completed",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            }
        ]
    }$$
WHERE
    abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';



--rollback  UPDATE platform.config
--rollback  SET
--rollback      config_value = $${
--rollback    "values": [
--rollback      {
--rollback        "name": "Germplasm Name",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm",
--rollback            "seed",
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "DESIGNATION",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "designation",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Germplasm Name Type",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "GERMPLASM_NAME_TYPE",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "nameType",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Parentage",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "PARENTAGE",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "parentage",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Generation",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "GENERATION",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "generation",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Germplasm State",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "GERMPLASM_STATE",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "state",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Taxon ID",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "germplasm"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "TAXON_ID",
--rollback        "entity": "germplasm",
--rollback        "required": "true",
--rollback        "api_field": "taxonomyDbId",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "true",
--rollback        "retrieve_endpoint": "taxonomies",
--rollback        "retrieve_api_value_field": "taxonomyDbId",
--rollback        "retrieve_api_search_field": "taxonId"
--rollback      },
--rollback      {
--rollback        "name": "Seed Name",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "seed"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "SEED_NAME",
--rollback        "entity": "seed",
--rollback        "required": "true",
--rollback        "api_field": "seedName",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Package Label",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "PACKAGE_LABEL",
--rollback        "entity": "package",
--rollback        "required": "true",
--rollback        "api_field": "packageLabel",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      },
--rollback      {
--rollback        "name": "Program",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "PROGRAM",
--rollback        "entity": "package",
--rollback        "required": "true",
--rollback        "api_field": "programDbId",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "true",
--rollback        "retrieve_endpoint": "programs",
--rollback        "retrieve_api_value_field": "programDbId",
--rollback        "retrieve_api_search_field": "programCode"
--rollback      },
--rollback      {
--rollback        "name": "Package Status",
--rollback        "type": "column",
--rollback        "view": {
--rollback          "visible": "true",
--rollback          "entities": [
--rollback            "package"
--rollback          ]
--rollback        },
--rollback        "usage": "required",
--rollback        "abbrev": "PACKAGE_STATUS",
--rollback        "entity": "package",
--rollback        "required": "true",
--rollback        "api_field": "packageStatus",
--rollback        "data_type": "string",
--rollback        "retrieve_db_id": "false",
--rollback        "retrieve_endpoint": "",
--rollback        "retrieve_api_value_field": "",
--rollback        "retrieve_api_search_field": ""
--rollback      }
--rollback    ]
--rollback }$$
--rollback  WHERE
--rollback      abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';
