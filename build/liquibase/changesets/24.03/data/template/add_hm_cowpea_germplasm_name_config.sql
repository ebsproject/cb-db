--liquibase formatted sql

--changeset postgres:add_hm_cowpea_germplasm_name_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-7027 HM DB: Update COWPEA configurations



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_GERMPLASM_COWPEA_DEFAULT',
        'Harvest Manager Cowpea Germplasm Name Pattern-Default',
        $$			
            {
                "PLOT": {
                    "default": {
                        "not_fixed": {
                            "default": {
                                "default": [
                                    {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                    }
                                ],
                                "single plant selection": [
                                    {
                                        "type" : "field",
                                        "entity" : "plot",
                                        "field_name" : "germplasmDesignation",
                                        "order_number":0
                                    },
                                    {
                                        "type" : "delimiter",
                                        "value" : "-",
                                        "order_number" : 1
                                    },
                                    {
                                        "type" : "counter",
                                        "order_number" : 2
                                    }
                                ]
                            }
                        }
                    }
                },
                "CROSS": {
                    "default": {
                        "default": {
                            "default": {
                                "default": [
                                    {
                                    "type": "free-text",
                                    "value": "",
                                    "order_number": 0
                                    }
                                ]
                            }
                        }
                    }
                },
                "harvest_mode": {
                    "cross_method": {
                        "germplasm_state": {
                            "germplasm_type": {
                                "harvest_method": [
                                    {
                                        "type": "free-text",
                                        "value": "ABC",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "<entity>",
                                        "field_name": "<field_name>",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "counter",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "db-sequence",
                                        "schema": "<schema>",
                                        "order_number": 4,
                                        "sequence_name": "<sequence_name>"
                                    },
                                    {
                                        "type": "free-text-repeater",
                                        "value": "ABC",
                                        "minimum": "2",
                                        "delimiter": "*",
                                        "order_number": 5
                                    }
                                ]
                            }
                        }
                    }
                }
                }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6965 Add HM_NAME_PATTERN_GERMPLASM_COWPEA_DEFAULT for Cowpea in platform.config - k.delarosa'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_GERMPLASM_COWPEA_DEFAULT';