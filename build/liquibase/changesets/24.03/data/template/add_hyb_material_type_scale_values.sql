--liquibase formatted sql

--changeset postgres:add_hyb_material_type_scale_values context:template splitStatements:false rollbackSplitStatements:false
--preconditions OnFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM master.scale_value WHERE abbrev IN ('HYB_MATERIAL_TYPE_MAINTAINER','HYB_MATERIAL_TYPE_RESTORER') AND scale_id IN (SELECT id  from master.scale where abbrev = 'HYB_MATERIAL_TYPE_SCALE')
--comment: CORB-6631 CB HM DB: Add HYB_MATERIAL_TYPE scale values



-- add scale value to HYB_MATERIAL_TYPE variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'HYB_MATERIAL_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('M', 'Maintainer', 'Maintainer', 'MAINTAINER', 'show'),
        ('R', 'Restorer', 'Restorer', 'RESTORER', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'HYB_MATERIAL_TYPE'
;



-- revert changes
-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HYB_MATERIAL_TYPE_MAINTAINER',
--rollback         'HYB_MATERIAL_TYPE_RESTORER'
--rollback     )
--rollback ;