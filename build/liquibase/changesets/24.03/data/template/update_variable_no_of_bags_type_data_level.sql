--liquibase formatted sql

--changeset postgres:update_variable_no_of_bags_type_data_level context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6631 HM DB: Update NO_OF_BAGS variable



--update NO_OF_BAGS variable
UPDATE master.variable
SET
	data_level = 'plot,cross',
    type = 'observation'
WHERE
	abbrev = 'NO_OF_BAGS'
;



--rollback UPDATE master.variable
--rollback SET
--rollback 	data_level = 'cross',
--rollback     type = 'metadata'
--rollback WHERE
--rollback 	abbrev = 'NO_OF_BAGS'
--rollback ;