--liquibase formatted sql

--changeset postgres:add_hyb_material_type_variable_and_scale context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6631 CB HM DB: Add new HYB_MATERIAL_TYPE variable and scale



--insert scale
INSERT INTO master.scale 
	(abbrev, name) 
SELECT 'HYB_MATERIAL_TYPE_SCALE', 'Hybrid material type'
WHERE
	NOT EXISTS(
		SELECT abbrev FROM master.scale WHERE abbrev = 'HYB_MATERIAL_TYPE_SCALE'
	);

--insert variable
INSERT INTO master.variable 
	(abbrev, "label", "name", data_type, not_null, "type", status, display_name, ontology_reference, bibliographical_reference, property_id, method_id, scale_id, variable_set, synonym, remarks, creation_timestamp, creator_id, modification_timestamp, modifier_id, notes, is_void, description, default_value, "usage", data_level, is_column, column_table, is_computed, member_data_type, target_variable_id, field_prop, member_variable_id, target_model, class_variable_id, json_type, notif, target_table) 
SELECT 'HYB_MATERIAL_TYPE', 'Hybrid material type', 'Hybrid material type', 'character varying', false, 'observation', 'active', 'Hybrid material type', NULL, NULL, NULL, NULL, scale.id, NULL, NULL, NULL, '2022-06-01 17:02:50.647', 1, '2022-06-01 17:08:00.119', NULL, 'CM-14913 added mlotho20220601', false, 'Material Type (R = Restorer, M = Maintainer)', '', 'occurrence', 'plot', true, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
FROM
	master.scale
WHERE
	scale.abbrev = 'HYB_MATERIAL_TYPE_SCALE'
	AND NOT EXISTS(
		SELECT abbrev FROM master.variable WHERE abbrev = 'HYB_MATERIAL_TYPE'
	);



--rollback DELETE FROM master.variable where abbrev = 'HYB_MATERIAL_TYPE';
--rollback DELETE FROM master.scale where abbrev = 'HYB_MATERIAL_TYPE_SCALE';