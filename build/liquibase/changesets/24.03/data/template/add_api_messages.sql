--liquibase formatted sql

--changeset postgres:add_api_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-7003: CB-API EC: API Updates



select api.insert_messages(
    400,
    'ERR',
    'Invalid format. Pipeline ID must be an integer.',
    'Invalid format. Stage ID must be an integer.',
    'Invalid format. Experiment year must be an integer.',
    'Invalid format. Steward ID must be an integer.',
    'Invalid format. Experiment year must be in this format: YYYY.',
    'Required parameters are missing. Ensure that programDbId, stageDbId, experimentYear, experimentType, seasonDbId, experimentStatus, stewardDbId, and cropDbId fields are not empty.',
    'Invalid format. Experiment plan ID must be an integer.',
    'Invalid format. Data process ID must be an integer.',
    'Invalid request. generateExperimentName can only be used when one of the columns are updated: stageDbId, experimentYear, seasonDbId.'
);



--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Pipeline ID must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Stage ID must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Experiment year must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Steward ID must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Experiment year must be in this format: YYYY.';
--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that programDbId, stageDbId, experimentYear, experimentType, seasonDbId, experimentStatus, stewardDbId, and cropDbId fields are not empty.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Experiment plan ID must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format. Data process ID must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid request. generateExperimentName can only be used when one of the columns are updated: stageDbId, experimentYear, seasonDbId.';