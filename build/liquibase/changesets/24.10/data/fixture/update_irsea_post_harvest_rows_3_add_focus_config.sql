--liquibase formatted sql

--changeset postgres:update_irsea_post_harvest_rows_3_add_focus_config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3725 CB-DC Post harvest: Update IRSEA Post Harvest Rows 3 Config



UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
        "Values": [
            {
                "default": "",
                "field_focus": true,
                "disabled": false,
                "target_value": "AYLD_CONT",
                "target_column": "value",
                "variable_type": "observation",
                "variable_abbrev": "AYLD_CONT",
                "entity_data_type": "trait-data"
            },
            {
                "default": "",
                "devices": [
                    {
                        "os": "Linux",
                        "parity": "None",
                        "baud_rate": 9600,
                        "connector": "/dev/ttyUSB0",
                        "data_bits": "Eight",
                        "stop_bits": 1,
                        "flow_control": "None",
                        "input_buffer_size": 1024,
                        "output_buffer_size": 512
                    },
                    {
                        "os": "Windows",
                        "parity": "None",
                        "baud_rate": 9600,
                        "connector": "COM3",
                        "data_bits": "Eight",
                        "stop_bits": 1,
                        "flow_control": "None",
                        "input_buffer_size": 1024,
                        "output_buffer_size": 512
                    }
                ],
                "disabled": false,
                "target_value": "MC_CONT",
                "target_column": "value",
                "variable_type": "observation",
                "variable_abbrev": "MC_CONT",
                "entity_data_type": "trait-data"
            },
            {
                "default": "",
                "disabled": false,
                "target_value": "REMARKS",
                "target_column": "REMARKS",
                "variable_type": "metadata",
                "variable_abbrev": "REMARKS",
                "entity_data_type": "plot-data"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "target_value": "AYLD_CONT",
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "AYLD_CONT",
--rollback                 "entity_data_type": "trait-data"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "devices": [
--rollback                     {
--rollback                         "os": "Linux",
--rollback                         "parity": "None",
--rollback                         "baud_rate": 9600,
--rollback                         "connector": "/dev/ttyUSB0",
--rollback                         "data_bits": "Eight",
--rollback                         "stop_bits": 1,
--rollback                         "flow_control": "None",
--rollback                         "input_buffer_size": 1024,
--rollback                         "output_buffer_size": 512
--rollback                     },
--rollback                     {
--rollback                         "os": "Windows",
--rollback                         "parity": "None",
--rollback                         "baud_rate": 9600,
--rollback                         "connector": "COM3",
--rollback                         "data_bits": "Eight",
--rollback                         "stop_bits": 1,
--rollback                         "flow_control": "None",
--rollback                         "input_buffer_size": 1024,
--rollback                         "output_buffer_size": 512
--rollback                     }
--rollback                 ],
--rollback                 "disabled": false,
--rollback                 "target_value": "MC_CONT",
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "MC_CONT",
--rollback                 "entity_data_type": "trait-data"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "target_value": "REMARKS",
--rollback                 "target_column": "REMARKS",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "REMARKS",
--rollback                 "entity_data_type": "plot-data"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE 
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';