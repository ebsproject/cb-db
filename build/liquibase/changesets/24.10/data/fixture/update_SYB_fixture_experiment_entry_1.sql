--liquibase formatted sql

--changeset postgres:update_SYB_fixture_experiment_entry_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment:BDS-3516 Update Soybean fixture experiment_entry



UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007360' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007360' LIMIT 1)
WHERE
entry_code = '1' AND 
entry_number = '1' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007361' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007361' LIMIT 1)
WHERE
entry_code = '2' AND 
entry_number = '2' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007362' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007362' LIMIT 1)
WHERE
entry_code = '3' AND 
entry_number = '3' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007367' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007367' LIMIT 1)
WHERE
entry_code = '8' AND 
entry_number = '8' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007368' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007368' LIMIT 1)
WHERE
entry_code = '9' AND 
entry_number = '9' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007369' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007369' LIMIT 1)
WHERE
entry_code = '10' AND 
entry_number = '10' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007363' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007363' LIMIT 1)
WHERE
entry_code = '4' AND 
entry_number = '4' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007364' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007364' LIMIT 1)
WHERE
entry_code = '5' AND 
entry_number = '5' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007365' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007365' LIMIT 1)
WHERE
entry_code = '6' AND 
entry_number = '6' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007366' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007366' LIMIT 1)
WHERE
entry_code = '7' AND 
entry_number = '7' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007408' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007408' LIMIT 1)
WHERE
entry_code = 'VOIDED-1255657' AND 
entry_number = '-1255657' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007360' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007360' LIMIT 1)
WHERE
entry_code = '1' AND 
entry_number = '1' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007361' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007361' LIMIT 1)
WHERE
entry_code = '2' AND 
entry_number = '2' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007362' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007362' LIMIT 1)
WHERE
entry_code = '3' AND 
entry_number = '3' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007363' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007363' LIMIT 1)
WHERE
entry_code = '4' AND 
entry_number = '4' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007364' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007364' LIMIT 1)
WHERE
entry_code = '5' AND 
entry_number = '5' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007365' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007365' LIMIT 1)
WHERE
entry_code = '6' AND 
entry_number = '6' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007366' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007366' LIMIT 1)
WHERE
entry_code = '7' AND 
entry_number = '7' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007367' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007367' LIMIT 1)
WHERE
entry_code = '8' AND 
entry_number = '8' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007368' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007368' LIMIT 1)
WHERE
entry_code = '9' AND 
entry_number = '9' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007369' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007369' LIMIT 1)
WHERE
entry_code = '10' AND 
entry_number = '10' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007370' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007370' LIMIT 1)
WHERE
entry_code = '11' AND 
entry_number = '11' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007371' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007371' LIMIT 1)
WHERE
entry_code = '12' AND 
entry_number = '12' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007372' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007372' LIMIT 1)
WHERE
entry_code = '13' AND 
entry_number = '13' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007373' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007373' LIMIT 1)
WHERE
entry_code = '14' AND 
entry_number = '14' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007374' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007374' LIMIT 1)
WHERE
entry_code = '15' AND 
entry_number = '15' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007375' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007375' LIMIT 1)
WHERE
entry_code = '16' AND 
entry_number = '16' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007376' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007376' LIMIT 1)
WHERE
entry_code = '17' AND 
entry_number = '17' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007377' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007377' LIMIT 1)
WHERE
entry_code = '18' AND 
entry_number = '18' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007378' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007378' LIMIT 1)
WHERE
entry_code = '19' AND 
entry_number = '19' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007379' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007379' LIMIT 1)
WHERE
entry_code = '20' AND 
entry_number = '20' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-SEM-2018-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007380' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007380' LIMIT 1)
WHERE
entry_code = '21' AND 
entry_number = '21' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007381' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007381' LIMIT 1)
WHERE
entry_code = '22' AND 
entry_number = '22' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007382' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007382' LIMIT 1)
WHERE
entry_code = '23' AND 
entry_number = '23' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007383' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007383' LIMIT 1)
WHERE
entry_code = '24' AND 
entry_number = '24' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007384' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007384' LIMIT 1)
WHERE
entry_code = '25' AND 
entry_number = '25' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007385' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007385' LIMIT 1)
WHERE
entry_code = '26' AND 
entry_number = '26' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007386' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007386' LIMIT 1)
WHERE
entry_code = '27' AND 
entry_number = '27' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007387' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007387' LIMIT 1)
WHERE
entry_code = '28' AND 
entry_number = '28' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007388' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007388' LIMIT 1)
WHERE
entry_code = '29' AND 
entry_number = '29' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007389' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007389' LIMIT 1)
WHERE
entry_code = '30' AND 
entry_number = '30' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007390' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007390' LIMIT 1)
WHERE
entry_code = '31' AND 
entry_number = '31' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007391' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007391' LIMIT 1)
WHERE
entry_code = '32' AND 
entry_number = '32' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007392' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007392' LIMIT 1)
WHERE
entry_code = '33' AND 
entry_number = '33' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007393' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007393' LIMIT 1)
WHERE
entry_code = '34' AND 
entry_number = '34' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007394' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007394' LIMIT 1)
WHERE
entry_code = '35' AND 
entry_number = '35' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007395' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007395' LIMIT 1)
WHERE
entry_code = '36' AND 
entry_number = '36' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007396' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007396' LIMIT 1)
WHERE
entry_code = '37' AND 
entry_number = '37' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007397' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007397' LIMIT 1)
WHERE
entry_code = '38' AND 
entry_number = '38' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007398' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007398' LIMIT 1)
WHERE
entry_code = '39' AND 
entry_number = '39' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007399' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007399' LIMIT 1)
WHERE
entry_code = '40' AND 
entry_number = '40' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007400' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007400' LIMIT 1)
WHERE
entry_code = '41' AND 
entry_number = '41' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007401' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007401' LIMIT 1)
WHERE
entry_code = '42' AND 
entry_number = '42' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007402' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007402' LIMIT 1)
WHERE
entry_code = '43' AND 
entry_number = '43' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007403' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007403' LIMIT 1)
WHERE
entry_code = '44' AND 
entry_number = '44' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007404' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007404' LIMIT 1)
WHERE
entry_code = '45' AND 
entry_number = '45' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007405' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007405' LIMIT 1)
WHERE
entry_code = '46' AND 
entry_number = '46' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007406' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007406' LIMIT 1)
WHERE
entry_code = '47' AND 
entry_number = '47' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007407' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007407' LIMIT 1)
WHERE
entry_code = '48' AND 
entry_number = '48' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007408' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007408' LIMIT 1)
WHERE
entry_code = '49' AND 
entry_number = '49' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007409' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007409' LIMIT 1)
WHERE
entry_code = '50' AND 
entry_number = '50' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008202' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008202' LIMIT 1)
WHERE
entry_code = '312' AND 
entry_number = '312' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008203' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008203' LIMIT 1)
WHERE
entry_code = '313' AND 
entry_number = '313' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008205' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008205' LIMIT 1)
WHERE
entry_code = '315' AND 
entry_number = '315' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008162' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008162' LIMIT 1)
WHERE
entry_code = '272' AND 
entry_number = '272' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008163' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008163' LIMIT 1)
WHERE
entry_code = '273' AND 
entry_number = '273' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008164' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008164' LIMIT 1)
WHERE
entry_code = '274' AND 
entry_number = '274' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008165' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008165' LIMIT 1)
WHERE
entry_code = '275' AND 
entry_number = '275' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008166' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008166' LIMIT 1)
WHERE
entry_code = '276' AND 
entry_number = '276' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008167' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008167' LIMIT 1)
WHERE
entry_code = '277' AND 
entry_number = '277' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008168' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008168' LIMIT 1)
WHERE
entry_code = '278' AND 
entry_number = '278' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008169' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008169' LIMIT 1)
WHERE
entry_code = '279' AND 
entry_number = '279' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008170' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008170' LIMIT 1)
WHERE
entry_code = '280' AND 
entry_number = '280' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008171' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008171' LIMIT 1)
WHERE
entry_code = '281' AND 
entry_number = '281' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008172' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008172' LIMIT 1)
WHERE
entry_code = '282' AND 
entry_number = '282' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008173' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008173' LIMIT 1)
WHERE
entry_code = '283' AND 
entry_number = '283' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008174' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008174' LIMIT 1)
WHERE
entry_code = '284' AND 
entry_number = '284' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008175' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008175' LIMIT 1)
WHERE
entry_code = '285' AND 
entry_number = '285' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008176' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008176' LIMIT 1)
WHERE
entry_code = '286' AND 
entry_number = '286' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008177' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008177' LIMIT 1)
WHERE
entry_code = '287' AND 
entry_number = '287' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008178' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008178' LIMIT 1)
WHERE
entry_code = '288' AND 
entry_number = '288' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008179' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008179' LIMIT 1)
WHERE
entry_code = '289' AND 
entry_number = '289' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008180' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008180' LIMIT 1)
WHERE
entry_code = '290' AND 
entry_number = '290' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008181' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008181' LIMIT 1)
WHERE
entry_code = '291' AND 
entry_number = '291' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008182' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008182' LIMIT 1)
WHERE
entry_code = '292' AND 
entry_number = '292' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008183' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008183' LIMIT 1)
WHERE
entry_code = '293' AND 
entry_number = '293' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008184' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008184' LIMIT 1)
WHERE
entry_code = '294' AND 
entry_number = '294' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008185' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008185' LIMIT 1)
WHERE
entry_code = '295' AND 
entry_number = '295' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008186' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008186' LIMIT 1)
WHERE
entry_code = '296' AND 
entry_number = '296' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008187' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008187' LIMIT 1)
WHERE
entry_code = '297' AND 
entry_number = '297' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008188' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008188' LIMIT 1)
WHERE
entry_code = '298' AND 
entry_number = '298' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008189' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008189' LIMIT 1)
WHERE
entry_code = '299' AND 
entry_number = '299' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008190' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008190' LIMIT 1)
WHERE
entry_code = '300' AND 
entry_number = '300' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008191' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008191' LIMIT 1)
WHERE
entry_code = '301' AND 
entry_number = '301' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008192' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008192' LIMIT 1)
WHERE
entry_code = '302' AND 
entry_number = '302' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008193' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008193' LIMIT 1)
WHERE
entry_code = '303' AND 
entry_number = '303' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008194' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008194' LIMIT 1)
WHERE
entry_code = '304' AND 
entry_number = '304' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008195' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008195' LIMIT 1)
WHERE
entry_code = '305' AND 
entry_number = '305' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008196' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008196' LIMIT 1)
WHERE
entry_code = '306' AND 
entry_number = '306' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008197' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008197' LIMIT 1)
WHERE
entry_code = '307' AND 
entry_number = '307' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008198' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008198' LIMIT 1)
WHERE
entry_code = '308' AND 
entry_number = '308' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008199' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008199' LIMIT 1)
WHERE
entry_code = '309' AND 
entry_number = '309' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008200' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008200' LIMIT 1)
WHERE
entry_code = '310' AND 
entry_number = '310' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008201' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008201' LIMIT 1)
WHERE
entry_code = '311' AND 
entry_number = '311' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008204' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008204' LIMIT 1)
WHERE
entry_code = '314' AND 
entry_number = '314' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008206' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008206' LIMIT 1)
WHERE
entry_code = '316' AND 
entry_number = '316' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008207' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008207' LIMIT 1)
WHERE
entry_code = '317' AND 
entry_number = '317' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008208' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008208' LIMIT 1)
WHERE
entry_code = '318' AND 
entry_number = '318' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008209' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008209' LIMIT 1)
WHERE
entry_code = '319' AND 
entry_number = '319' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008212' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008212' LIMIT 1)
WHERE
entry_code = '322' AND 
entry_number = '322' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008213' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008213' LIMIT 1)
WHERE
entry_code = '323' AND 
entry_number = '323' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008214' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008214' LIMIT 1)
WHERE
entry_code = '324' AND 
entry_number = '324' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008534' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008534' LIMIT 1)
WHERE
entry_code = '644' AND 
entry_number = '644' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008535' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008535' LIMIT 1)
WHERE
entry_code = '645' AND 
entry_number = '645' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008210' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008210' LIMIT 1)
WHERE
entry_code = '320' AND 
entry_number = '320' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008211' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008211' LIMIT 1)
WHERE
entry_code = '321' AND 
entry_number = '321' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008215' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008215' LIMIT 1)
WHERE
entry_code = '325' AND 
entry_number = '325' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008216' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008216' LIMIT 1)
WHERE
entry_code = '326' AND 
entry_number = '326' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008217' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008217' LIMIT 1)
WHERE
entry_code = '327' AND 
entry_number = '327' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008218' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008218' LIMIT 1)
WHERE
entry_code = '328' AND 
entry_number = '328' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008219' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008219' LIMIT 1)
WHERE
entry_code = '329' AND 
entry_number = '329' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008220' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008220' LIMIT 1)
WHERE
entry_code = '330' AND 
entry_number = '330' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008221' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008221' LIMIT 1)
WHERE
entry_code = '331' AND 
entry_number = '331' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008222' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008222' LIMIT 1)
WHERE
entry_code = '332' AND 
entry_number = '332' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008223' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008223' LIMIT 1)
WHERE
entry_code = '333' AND 
entry_number = '333' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008224' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008224' LIMIT 1)
WHERE
entry_code = '334' AND 
entry_number = '334' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008225' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008225' LIMIT 1)
WHERE
entry_code = '335' AND 
entry_number = '335' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008226' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008226' LIMIT 1)
WHERE
entry_code = '336' AND 
entry_number = '336' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008227' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008227' LIMIT 1)
WHERE
entry_code = '337' AND 
entry_number = '337' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008228' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008228' LIMIT 1)
WHERE
entry_code = '338' AND 
entry_number = '338' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008229' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008229' LIMIT 1)
WHERE
entry_code = '339' AND 
entry_number = '339' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008230' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008230' LIMIT 1)
WHERE
entry_code = '340' AND 
entry_number = '340' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008231' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008231' LIMIT 1)
WHERE
entry_code = '341' AND 
entry_number = '341' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008232' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008232' LIMIT 1)
WHERE
entry_code = '342' AND 
entry_number = '342' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008233' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008233' LIMIT 1)
WHERE
entry_code = '343' AND 
entry_number = '343' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008234' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008234' LIMIT 1)
WHERE
entry_code = '344' AND 
entry_number = '344' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008235' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008235' LIMIT 1)
WHERE
entry_code = '345' AND 
entry_number = '345' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008236' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008236' LIMIT 1)
WHERE
entry_code = '346' AND 
entry_number = '346' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008237' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008237' LIMIT 1)
WHERE
entry_code = '347' AND 
entry_number = '347' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008238' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008238' LIMIT 1)
WHERE
entry_code = '348' AND 
entry_number = '348' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008239' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008239' LIMIT 1)
WHERE
entry_code = '349' AND 
entry_number = '349' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008240' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008240' LIMIT 1)
WHERE
entry_code = '350' AND 
entry_number = '350' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008241' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008241' LIMIT 1)
WHERE
entry_code = '351' AND 
entry_number = '351' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008242' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008242' LIMIT 1)
WHERE
entry_code = '352' AND 
entry_number = '352' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008243' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008243' LIMIT 1)
WHERE
entry_code = '353' AND 
entry_number = '353' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008244' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008244' LIMIT 1)
WHERE
entry_code = '354' AND 
entry_number = '354' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008245' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008245' LIMIT 1)
WHERE
entry_code = '355' AND 
entry_number = '355' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008246' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008246' LIMIT 1)
WHERE
entry_code = '356' AND 
entry_number = '356' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008247' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008247' LIMIT 1)
WHERE
entry_code = '357' AND 
entry_number = '357' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008248' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008248' LIMIT 1)
WHERE
entry_code = '358' AND 
entry_number = '358' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008249' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008249' LIMIT 1)
WHERE
entry_code = '359' AND 
entry_number = '359' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008250' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008250' LIMIT 1)
WHERE
entry_code = '360' AND 
entry_number = '360' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008251' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008251' LIMIT 1)
WHERE
entry_code = '361' AND 
entry_number = '361' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008252' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008252' LIMIT 1)
WHERE
entry_code = '362' AND 
entry_number = '362' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008253' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008253' LIMIT 1)
WHERE
entry_code = '363' AND 
entry_number = '363' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008254' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008254' LIMIT 1)
WHERE
entry_code = '364' AND 
entry_number = '364' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008255' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008255' LIMIT 1)
WHERE
entry_code = '365' AND 
entry_number = '365' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008256' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008256' LIMIT 1)
WHERE
entry_code = '366' AND 
entry_number = '366' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008257' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008257' LIMIT 1)
WHERE
entry_code = '367' AND 
entry_number = '367' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008258' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008258' LIMIT 1)
WHERE
entry_code = '368' AND 
entry_number = '368' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008259' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008259' LIMIT 1)
WHERE
entry_code = '369' AND 
entry_number = '369' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008260' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008260' LIMIT 1)
WHERE
entry_code = '370' AND 
entry_number = '370' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008261' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008261' LIMIT 1)
WHERE
entry_code = '371' AND 
entry_number = '371' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008262' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008262' LIMIT 1)
WHERE
entry_code = '372' AND 
entry_number = '372' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008263' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008263' LIMIT 1)
WHERE
entry_code = '373' AND 
entry_number = '373' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008264' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008264' LIMIT 1)
WHERE
entry_code = '374' AND 
entry_number = '374' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008265' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008265' LIMIT 1)
WHERE
entry_code = '375' AND 
entry_number = '375' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008266' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008266' LIMIT 1)
WHERE
entry_code = '376' AND 
entry_number = '376' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008267' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008267' LIMIT 1)
WHERE
entry_code = '377' AND 
entry_number = '377' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008268' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008268' LIMIT 1)
WHERE
entry_code = '378' AND 
entry_number = '378' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008269' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008269' LIMIT 1)
WHERE
entry_code = '379' AND 
entry_number = '379' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008270' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008270' LIMIT 1)
WHERE
entry_code = '380' AND 
entry_number = '380' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008271' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008271' LIMIT 1)
WHERE
entry_code = '381' AND 
entry_number = '381' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008272' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008272' LIMIT 1)
WHERE
entry_code = '382' AND 
entry_number = '382' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008273' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008273' LIMIT 1)
WHERE
entry_code = '383' AND 
entry_number = '383' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008274' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008274' LIMIT 1)
WHERE
entry_code = '384' AND 
entry_number = '384' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008275' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008275' LIMIT 1)
WHERE
entry_code = '385' AND 
entry_number = '385' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008276' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008276' LIMIT 1)
WHERE
entry_code = '386' AND 
entry_number = '386' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008277' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008277' LIMIT 1)
WHERE
entry_code = '387' AND 
entry_number = '387' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008278' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008278' LIMIT 1)
WHERE
entry_code = '388' AND 
entry_number = '388' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008279' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008279' LIMIT 1)
WHERE
entry_code = '389' AND 
entry_number = '389' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008280' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008280' LIMIT 1)
WHERE
entry_code = '390' AND 
entry_number = '390' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008281' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008281' LIMIT 1)
WHERE
entry_code = '391' AND 
entry_number = '391' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008282' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008282' LIMIT 1)
WHERE
entry_code = '392' AND 
entry_number = '392' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008283' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008283' LIMIT 1)
WHERE
entry_code = '393' AND 
entry_number = '393' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008284' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008284' LIMIT 1)
WHERE
entry_code = '394' AND 
entry_number = '394' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008285' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008285' LIMIT 1)
WHERE
entry_code = '395' AND 
entry_number = '395' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008286' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008286' LIMIT 1)
WHERE
entry_code = '396' AND 
entry_number = '396' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008287' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008287' LIMIT 1)
WHERE
entry_code = '397' AND 
entry_number = '397' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008288' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008288' LIMIT 1)
WHERE
entry_code = '398' AND 
entry_number = '398' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008289' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008289' LIMIT 1)
WHERE
entry_code = '399' AND 
entry_number = '399' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008290' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008290' LIMIT 1)
WHERE
entry_code = '400' AND 
entry_number = '400' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008291' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008291' LIMIT 1)
WHERE
entry_code = '401' AND 
entry_number = '401' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008292' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008292' LIMIT 1)
WHERE
entry_code = '402' AND 
entry_number = '402' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008293' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008293' LIMIT 1)
WHERE
entry_code = '403' AND 
entry_number = '403' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008294' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008294' LIMIT 1)
WHERE
entry_code = '404' AND 
entry_number = '404' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008295' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008295' LIMIT 1)
WHERE
entry_code = '405' AND 
entry_number = '405' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008296' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008296' LIMIT 1)
WHERE
entry_code = '406' AND 
entry_number = '406' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008297' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008297' LIMIT 1)
WHERE
entry_code = '407' AND 
entry_number = '407' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008298' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008298' LIMIT 1)
WHERE
entry_code = '408' AND 
entry_number = '408' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008299' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008299' LIMIT 1)
WHERE
entry_code = '409' AND 
entry_number = '409' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008300' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008300' LIMIT 1)
WHERE
entry_code = '410' AND 
entry_number = '410' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008301' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008301' LIMIT 1)
WHERE
entry_code = '411' AND 
entry_number = '411' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008302' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008302' LIMIT 1)
WHERE
entry_code = '412' AND 
entry_number = '412' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008303' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008303' LIMIT 1)
WHERE
entry_code = '413' AND 
entry_number = '413' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008304' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008304' LIMIT 1)
WHERE
entry_code = '414' AND 
entry_number = '414' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008305' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008305' LIMIT 1)
WHERE
entry_code = '415' AND 
entry_number = '415' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008306' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008306' LIMIT 1)
WHERE
entry_code = '416' AND 
entry_number = '416' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008307' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008307' LIMIT 1)
WHERE
entry_code = '417' AND 
entry_number = '417' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008308' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008308' LIMIT 1)
WHERE
entry_code = '418' AND 
entry_number = '418' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008309' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008309' LIMIT 1)
WHERE
entry_code = '419' AND 
entry_number = '419' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008310' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008310' LIMIT 1)
WHERE
entry_code = '420' AND 
entry_number = '420' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008311' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008311' LIMIT 1)
WHERE
entry_code = '421' AND 
entry_number = '421' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008312' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008312' LIMIT 1)
WHERE
entry_code = '422' AND 
entry_number = '422' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008313' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008313' LIMIT 1)
WHERE
entry_code = '423' AND 
entry_number = '423' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008314' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008314' LIMIT 1)
WHERE
entry_code = '424' AND 
entry_number = '424' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008315' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008315' LIMIT 1)
WHERE
entry_code = '425' AND 
entry_number = '425' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008316' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008316' LIMIT 1)
WHERE
entry_code = '426' AND 
entry_number = '426' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008317' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008317' LIMIT 1)
WHERE
entry_code = '427' AND 
entry_number = '427' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008318' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008318' LIMIT 1)
WHERE
entry_code = '428' AND 
entry_number = '428' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008319' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008319' LIMIT 1)
WHERE
entry_code = '429' AND 
entry_number = '429' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008320' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008320' LIMIT 1)
WHERE
entry_code = '430' AND 
entry_number = '430' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008321' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008321' LIMIT 1)
WHERE
entry_code = '431' AND 
entry_number = '431' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008322' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008322' LIMIT 1)
WHERE
entry_code = '432' AND 
entry_number = '432' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008323' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008323' LIMIT 1)
WHERE
entry_code = '433' AND 
entry_number = '433' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008324' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008324' LIMIT 1)
WHERE
entry_code = '434' AND 
entry_number = '434' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008325' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008325' LIMIT 1)
WHERE
entry_code = '435' AND 
entry_number = '435' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008326' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008326' LIMIT 1)
WHERE
entry_code = '436' AND 
entry_number = '436' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008327' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008327' LIMIT 1)
WHERE
entry_code = '437' AND 
entry_number = '437' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008328' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008328' LIMIT 1)
WHERE
entry_code = '438' AND 
entry_number = '438' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008329' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008329' LIMIT 1)
WHERE
entry_code = '439' AND 
entry_number = '439' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008330' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008330' LIMIT 1)
WHERE
entry_code = '440' AND 
entry_number = '440' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008331' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008331' LIMIT 1)
WHERE
entry_code = '441' AND 
entry_number = '441' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008332' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008332' LIMIT 1)
WHERE
entry_code = '442' AND 
entry_number = '442' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008333' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008333' LIMIT 1)
WHERE
entry_code = '443' AND 
entry_number = '443' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008334' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008334' LIMIT 1)
WHERE
entry_code = '444' AND 
entry_number = '444' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008335' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008335' LIMIT 1)
WHERE
entry_code = '445' AND 
entry_number = '445' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008336' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008336' LIMIT 1)
WHERE
entry_code = '446' AND 
entry_number = '446' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008337' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008337' LIMIT 1)
WHERE
entry_code = '447' AND 
entry_number = '447' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008338' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008338' LIMIT 1)
WHERE
entry_code = '448' AND 
entry_number = '448' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008339' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008339' LIMIT 1)
WHERE
entry_code = '449' AND 
entry_number = '449' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008340' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008340' LIMIT 1)
WHERE
entry_code = '450' AND 
entry_number = '450' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008341' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008341' LIMIT 1)
WHERE
entry_code = '451' AND 
entry_number = '451' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008342' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008342' LIMIT 1)
WHERE
entry_code = '452' AND 
entry_number = '452' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008343' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008343' LIMIT 1)
WHERE
entry_code = '453' AND 
entry_number = '453' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008344' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008344' LIMIT 1)
WHERE
entry_code = '454' AND 
entry_number = '454' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008345' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008345' LIMIT 1)
WHERE
entry_code = '455' AND 
entry_number = '455' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008346' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008346' LIMIT 1)
WHERE
entry_code = '456' AND 
entry_number = '456' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008347' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008347' LIMIT 1)
WHERE
entry_code = '457' AND 
entry_number = '457' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008348' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008348' LIMIT 1)
WHERE
entry_code = '458' AND 
entry_number = '458' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008349' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008349' LIMIT 1)
WHERE
entry_code = '459' AND 
entry_number = '459' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008350' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008350' LIMIT 1)
WHERE
entry_code = '460' AND 
entry_number = '460' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008351' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008351' LIMIT 1)
WHERE
entry_code = '461' AND 
entry_number = '461' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008352' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008352' LIMIT 1)
WHERE
entry_code = '462' AND 
entry_number = '462' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008353' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008353' LIMIT 1)
WHERE
entry_code = '463' AND 
entry_number = '463' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008354' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008354' LIMIT 1)
WHERE
entry_code = '464' AND 
entry_number = '464' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008355' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008355' LIMIT 1)
WHERE
entry_code = '465' AND 
entry_number = '465' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008356' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008356' LIMIT 1)
WHERE
entry_code = '466' AND 
entry_number = '466' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008357' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008357' LIMIT 1)
WHERE
entry_code = '467' AND 
entry_number = '467' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008358' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008358' LIMIT 1)
WHERE
entry_code = '468' AND 
entry_number = '468' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008359' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008359' LIMIT 1)
WHERE
entry_code = '469' AND 
entry_number = '469' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008360' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008360' LIMIT 1)
WHERE
entry_code = '470' AND 
entry_number = '470' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008361' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008361' LIMIT 1)
WHERE
entry_code = '471' AND 
entry_number = '471' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008362' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008362' LIMIT 1)
WHERE
entry_code = '472' AND 
entry_number = '472' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008363' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008363' LIMIT 1)
WHERE
entry_code = '473' AND 
entry_number = '473' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008364' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008364' LIMIT 1)
WHERE
entry_code = '474' AND 
entry_number = '474' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008365' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008365' LIMIT 1)
WHERE
entry_code = '475' AND 
entry_number = '475' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008366' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008366' LIMIT 1)
WHERE
entry_code = '476' AND 
entry_number = '476' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008367' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008367' LIMIT 1)
WHERE
entry_code = '477' AND 
entry_number = '477' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008368' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008368' LIMIT 1)
WHERE
entry_code = '478' AND 
entry_number = '478' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008369' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008369' LIMIT 1)
WHERE
entry_code = '479' AND 
entry_number = '479' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008370' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008370' LIMIT 1)
WHERE
entry_code = '480' AND 
entry_number = '480' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008371' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008371' LIMIT 1)
WHERE
entry_code = '481' AND 
entry_number = '481' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008372' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008372' LIMIT 1)
WHERE
entry_code = '482' AND 
entry_number = '482' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008373' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008373' LIMIT 1)
WHERE
entry_code = '483' AND 
entry_number = '483' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008374' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008374' LIMIT 1)
WHERE
entry_code = '484' AND 
entry_number = '484' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008375' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008375' LIMIT 1)
WHERE
entry_code = '485' AND 
entry_number = '485' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008376' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008376' LIMIT 1)
WHERE
entry_code = '486' AND 
entry_number = '486' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008377' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008377' LIMIT 1)
WHERE
entry_code = '487' AND 
entry_number = '487' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008378' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008378' LIMIT 1)
WHERE
entry_code = '488' AND 
entry_number = '488' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008379' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008379' LIMIT 1)
WHERE
entry_code = '489' AND 
entry_number = '489' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008380' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008380' LIMIT 1)
WHERE
entry_code = '490' AND 
entry_number = '490' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008381' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008381' LIMIT 1)
WHERE
entry_code = '491' AND 
entry_number = '491' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008382' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008382' LIMIT 1)
WHERE
entry_code = '492' AND 
entry_number = '492' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008383' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008383' LIMIT 1)
WHERE
entry_code = '493' AND 
entry_number = '493' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008384' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008384' LIMIT 1)
WHERE
entry_code = '494' AND 
entry_number = '494' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008385' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008385' LIMIT 1)
WHERE
entry_code = '495' AND 
entry_number = '495' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008386' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008386' LIMIT 1)
WHERE
entry_code = '496' AND 
entry_number = '496' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008387' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008387' LIMIT 1)
WHERE
entry_code = '497' AND 
entry_number = '497' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008388' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008388' LIMIT 1)
WHERE
entry_code = '498' AND 
entry_number = '498' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008389' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008389' LIMIT 1)
WHERE
entry_code = '499' AND 
entry_number = '499' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008390' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008390' LIMIT 1)
WHERE
entry_code = '500' AND 
entry_number = '500' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008391' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008391' LIMIT 1)
WHERE
entry_code = '501' AND 
entry_number = '501' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008392' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008392' LIMIT 1)
WHERE
entry_code = '502' AND 
entry_number = '502' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008393' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008393' LIMIT 1)
WHERE
entry_code = '503' AND 
entry_number = '503' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008394' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008394' LIMIT 1)
WHERE
entry_code = '504' AND 
entry_number = '504' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008395' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008395' LIMIT 1)
WHERE
entry_code = '505' AND 
entry_number = '505' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008396' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008396' LIMIT 1)
WHERE
entry_code = '506' AND 
entry_number = '506' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008397' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008397' LIMIT 1)
WHERE
entry_code = '507' AND 
entry_number = '507' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008398' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008398' LIMIT 1)
WHERE
entry_code = '508' AND 
entry_number = '508' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008399' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008399' LIMIT 1)
WHERE
entry_code = '509' AND 
entry_number = '509' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008400' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008400' LIMIT 1)
WHERE
entry_code = '510' AND 
entry_number = '510' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008401' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008401' LIMIT 1)
WHERE
entry_code = '511' AND 
entry_number = '511' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008402' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008402' LIMIT 1)
WHERE
entry_code = '512' AND 
entry_number = '512' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008403' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008403' LIMIT 1)
WHERE
entry_code = '513' AND 
entry_number = '513' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008404' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008404' LIMIT 1)
WHERE
entry_code = '514' AND 
entry_number = '514' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008405' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008405' LIMIT 1)
WHERE
entry_code = '515' AND 
entry_number = '515' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008406' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008406' LIMIT 1)
WHERE
entry_code = '516' AND 
entry_number = '516' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008407' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008407' LIMIT 1)
WHERE
entry_code = '517' AND 
entry_number = '517' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008408' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008408' LIMIT 1)
WHERE
entry_code = '518' AND 
entry_number = '518' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008409' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008409' LIMIT 1)
WHERE
entry_code = '519' AND 
entry_number = '519' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008410' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008410' LIMIT 1)
WHERE
entry_code = '520' AND 
entry_number = '520' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008411' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008411' LIMIT 1)
WHERE
entry_code = '521' AND 
entry_number = '521' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008412' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008412' LIMIT 1)
WHERE
entry_code = '522' AND 
entry_number = '522' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008413' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008413' LIMIT 1)
WHERE
entry_code = '523' AND 
entry_number = '523' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008414' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008414' LIMIT 1)
WHERE
entry_code = '524' AND 
entry_number = '524' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008415' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008415' LIMIT 1)
WHERE
entry_code = '525' AND 
entry_number = '525' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008416' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008416' LIMIT 1)
WHERE
entry_code = '526' AND 
entry_number = '526' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008417' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008417' LIMIT 1)
WHERE
entry_code = '527' AND 
entry_number = '527' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008418' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008418' LIMIT 1)
WHERE
entry_code = '528' AND 
entry_number = '528' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008419' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008419' LIMIT 1)
WHERE
entry_code = '529' AND 
entry_number = '529' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008420' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008420' LIMIT 1)
WHERE
entry_code = '530' AND 
entry_number = '530' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008421' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008421' LIMIT 1)
WHERE
entry_code = '531' AND 
entry_number = '531' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008422' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008422' LIMIT 1)
WHERE
entry_code = '532' AND 
entry_number = '532' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008423' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008423' LIMIT 1)
WHERE
entry_code = '533' AND 
entry_number = '533' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008424' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008424' LIMIT 1)
WHERE
entry_code = '534' AND 
entry_number = '534' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008425' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008425' LIMIT 1)
WHERE
entry_code = '535' AND 
entry_number = '535' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008426' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008426' LIMIT 1)
WHERE
entry_code = '536' AND 
entry_number = '536' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008427' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008427' LIMIT 1)
WHERE
entry_code = '537' AND 
entry_number = '537' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008428' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008428' LIMIT 1)
WHERE
entry_code = '538' AND 
entry_number = '538' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008429' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008429' LIMIT 1)
WHERE
entry_code = '539' AND 
entry_number = '539' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008430' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008430' LIMIT 1)
WHERE
entry_code = '540' AND 
entry_number = '540' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008431' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008431' LIMIT 1)
WHERE
entry_code = '541' AND 
entry_number = '541' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008432' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008432' LIMIT 1)
WHERE
entry_code = '542' AND 
entry_number = '542' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008433' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008433' LIMIT 1)
WHERE
entry_code = '543' AND 
entry_number = '543' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008434' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008434' LIMIT 1)
WHERE
entry_code = '544' AND 
entry_number = '544' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008435' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008435' LIMIT 1)
WHERE
entry_code = '545' AND 
entry_number = '545' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008436' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008436' LIMIT 1)
WHERE
entry_code = '546' AND 
entry_number = '546' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008437' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008437' LIMIT 1)
WHERE
entry_code = '547' AND 
entry_number = '547' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008438' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008438' LIMIT 1)
WHERE
entry_code = '548' AND 
entry_number = '548' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008439' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008439' LIMIT 1)
WHERE
entry_code = '549' AND 
entry_number = '549' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008440' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008440' LIMIT 1)
WHERE
entry_code = '550' AND 
entry_number = '550' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008441' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008441' LIMIT 1)
WHERE
entry_code = '551' AND 
entry_number = '551' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008442' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008442' LIMIT 1)
WHERE
entry_code = '552' AND 
entry_number = '552' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008443' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008443' LIMIT 1)
WHERE
entry_code = '553' AND 
entry_number = '553' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008444' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008444' LIMIT 1)
WHERE
entry_code = '554' AND 
entry_number = '554' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008445' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008445' LIMIT 1)
WHERE
entry_code = '555' AND 
entry_number = '555' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008446' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008446' LIMIT 1)
WHERE
entry_code = '556' AND 
entry_number = '556' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008447' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008447' LIMIT 1)
WHERE
entry_code = '557' AND 
entry_number = '557' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008448' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008448' LIMIT 1)
WHERE
entry_code = '558' AND 
entry_number = '558' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008449' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008449' LIMIT 1)
WHERE
entry_code = '559' AND 
entry_number = '559' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008450' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008450' LIMIT 1)
WHERE
entry_code = '560' AND 
entry_number = '560' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008451' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008451' LIMIT 1)
WHERE
entry_code = '561' AND 
entry_number = '561' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008452' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008452' LIMIT 1)
WHERE
entry_code = '562' AND 
entry_number = '562' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008453' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008453' LIMIT 1)
WHERE
entry_code = '563' AND 
entry_number = '563' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008454' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008454' LIMIT 1)
WHERE
entry_code = '564' AND 
entry_number = '564' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008455' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008455' LIMIT 1)
WHERE
entry_code = '565' AND 
entry_number = '565' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008456' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008456' LIMIT 1)
WHERE
entry_code = '566' AND 
entry_number = '566' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008457' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008457' LIMIT 1)
WHERE
entry_code = '567' AND 
entry_number = '567' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008458' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008458' LIMIT 1)
WHERE
entry_code = '568' AND 
entry_number = '568' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008459' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008459' LIMIT 1)
WHERE
entry_code = '569' AND 
entry_number = '569' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008460' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008460' LIMIT 1)
WHERE
entry_code = '570' AND 
entry_number = '570' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008461' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008461' LIMIT 1)
WHERE
entry_code = '571' AND 
entry_number = '571' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008462' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008462' LIMIT 1)
WHERE
entry_code = '572' AND 
entry_number = '572' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008463' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008463' LIMIT 1)
WHERE
entry_code = '573' AND 
entry_number = '573' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008464' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008464' LIMIT 1)
WHERE
entry_code = '574' AND 
entry_number = '574' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008465' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008465' LIMIT 1)
WHERE
entry_code = '575' AND 
entry_number = '575' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008466' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008466' LIMIT 1)
WHERE
entry_code = '576' AND 
entry_number = '576' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008467' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008467' LIMIT 1)
WHERE
entry_code = '577' AND 
entry_number = '577' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008468' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008468' LIMIT 1)
WHERE
entry_code = '578' AND 
entry_number = '578' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008469' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008469' LIMIT 1)
WHERE
entry_code = '579' AND 
entry_number = '579' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008470' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008470' LIMIT 1)
WHERE
entry_code = '580' AND 
entry_number = '580' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008471' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008471' LIMIT 1)
WHERE
entry_code = '581' AND 
entry_number = '581' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008472' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008472' LIMIT 1)
WHERE
entry_code = '582' AND 
entry_number = '582' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008473' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008473' LIMIT 1)
WHERE
entry_code = '583' AND 
entry_number = '583' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008474' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008474' LIMIT 1)
WHERE
entry_code = '584' AND 
entry_number = '584' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008475' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008475' LIMIT 1)
WHERE
entry_code = '585' AND 
entry_number = '585' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008476' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008476' LIMIT 1)
WHERE
entry_code = '586' AND 
entry_number = '586' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008477' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008477' LIMIT 1)
WHERE
entry_code = '587' AND 
entry_number = '587' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008478' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008478' LIMIT 1)
WHERE
entry_code = '588' AND 
entry_number = '588' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008479' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008479' LIMIT 1)
WHERE
entry_code = '589' AND 
entry_number = '589' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008480' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008480' LIMIT 1)
WHERE
entry_code = '590' AND 
entry_number = '590' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008481' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008481' LIMIT 1)
WHERE
entry_code = '591' AND 
entry_number = '591' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008482' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008482' LIMIT 1)
WHERE
entry_code = '592' AND 
entry_number = '592' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008483' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008483' LIMIT 1)
WHERE
entry_code = '593' AND 
entry_number = '593' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008484' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008484' LIMIT 1)
WHERE
entry_code = '594' AND 
entry_number = '594' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008485' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008485' LIMIT 1)
WHERE
entry_code = '595' AND 
entry_number = '595' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008486' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008486' LIMIT 1)
WHERE
entry_code = '596' AND 
entry_number = '596' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008487' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008487' LIMIT 1)
WHERE
entry_code = '597' AND 
entry_number = '597' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008488' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008488' LIMIT 1)
WHERE
entry_code = '598' AND 
entry_number = '598' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008489' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008489' LIMIT 1)
WHERE
entry_code = '599' AND 
entry_number = '599' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008490' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008490' LIMIT 1)
WHERE
entry_code = '600' AND 
entry_number = '600' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008491' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008491' LIMIT 1)
WHERE
entry_code = '601' AND 
entry_number = '601' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008492' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008492' LIMIT 1)
WHERE
entry_code = '602' AND 
entry_number = '602' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008493' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008493' LIMIT 1)
WHERE
entry_code = '603' AND 
entry_number = '603' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008494' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008494' LIMIT 1)
WHERE
entry_code = '604' AND 
entry_number = '604' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008495' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008495' LIMIT 1)
WHERE
entry_code = '605' AND 
entry_number = '605' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008496' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008496' LIMIT 1)
WHERE
entry_code = '606' AND 
entry_number = '606' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008497' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008497' LIMIT 1)
WHERE
entry_code = '607' AND 
entry_number = '607' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008498' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008498' LIMIT 1)
WHERE
entry_code = '608' AND 
entry_number = '608' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008499' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008499' LIMIT 1)
WHERE
entry_code = '609' AND 
entry_number = '609' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008500' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008500' LIMIT 1)
WHERE
entry_code = '610' AND 
entry_number = '610' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008501' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008501' LIMIT 1)
WHERE
entry_code = '611' AND 
entry_number = '611' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008502' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008502' LIMIT 1)
WHERE
entry_code = '612' AND 
entry_number = '612' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008503' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008503' LIMIT 1)
WHERE
entry_code = '613' AND 
entry_number = '613' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008504' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008504' LIMIT 1)
WHERE
entry_code = '614' AND 
entry_number = '614' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008505' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008505' LIMIT 1)
WHERE
entry_code = '615' AND 
entry_number = '615' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008506' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008506' LIMIT 1)
WHERE
entry_code = '616' AND 
entry_number = '616' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008507' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008507' LIMIT 1)
WHERE
entry_code = '617' AND 
entry_number = '617' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008508' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008508' LIMIT 1)
WHERE
entry_code = '618' AND 
entry_number = '618' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008509' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008509' LIMIT 1)
WHERE
entry_code = '619' AND 
entry_number = '619' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008510' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008510' LIMIT 1)
WHERE
entry_code = '620' AND 
entry_number = '620' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008511' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008511' LIMIT 1)
WHERE
entry_code = '621' AND 
entry_number = '621' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008512' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008512' LIMIT 1)
WHERE
entry_code = '622' AND 
entry_number = '622' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008513' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008513' LIMIT 1)
WHERE
entry_code = '623' AND 
entry_number = '623' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008514' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008514' LIMIT 1)
WHERE
entry_code = '624' AND 
entry_number = '624' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008515' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008515' LIMIT 1)
WHERE
entry_code = '625' AND 
entry_number = '625' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008516' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008516' LIMIT 1)
WHERE
entry_code = '626' AND 
entry_number = '626' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008517' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008517' LIMIT 1)
WHERE
entry_code = '627' AND 
entry_number = '627' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008518' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008518' LIMIT 1)
WHERE
entry_code = '628' AND 
entry_number = '628' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008519' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008519' LIMIT 1)
WHERE
entry_code = '629' AND 
entry_number = '629' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008520' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008520' LIMIT 1)
WHERE
entry_code = '630' AND 
entry_number = '630' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008521' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008521' LIMIT 1)
WHERE
entry_code = '631' AND 
entry_number = '631' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008522' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008522' LIMIT 1)
WHERE
entry_code = '632' AND 
entry_number = '632' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008523' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008523' LIMIT 1)
WHERE
entry_code = '633' AND 
entry_number = '633' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008524' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008524' LIMIT 1)
WHERE
entry_code = '634' AND 
entry_number = '634' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008525' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008525' LIMIT 1)
WHERE
entry_code = '635' AND 
entry_number = '635' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008526' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008526' LIMIT 1)
WHERE
entry_code = '636' AND 
entry_number = '636' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008527' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008527' LIMIT 1)
WHERE
entry_code = '637' AND 
entry_number = '637' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008528' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008528' LIMIT 1)
WHERE
entry_code = '638' AND 
entry_number = '638' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008529' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008529' LIMIT 1)
WHERE
entry_code = '639' AND 
entry_number = '639' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008530' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008530' LIMIT 1)
WHERE
entry_code = '640' AND 
entry_number = '640' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008531' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008531' LIMIT 1)
WHERE
entry_code = '641' AND 
entry_number = '641' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008532' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008532' LIMIT 1)
WHERE
entry_code = '642' AND 
entry_number = '642' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008533' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008533' LIMIT 1)
WHERE
entry_code = '643' AND 
entry_number = '643' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008591' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008591' LIMIT 1)
WHERE
entry_code = '701' AND 
entry_number = '701' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008536' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008536' LIMIT 1)
WHERE
entry_code = '646' AND 
entry_number = '646' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008537' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008537' LIMIT 1)
WHERE
entry_code = '647' AND 
entry_number = '647' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008538' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008538' LIMIT 1)
WHERE
entry_code = '648' AND 
entry_number = '648' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008539' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008539' LIMIT 1)
WHERE
entry_code = '649' AND 
entry_number = '649' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008540' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008540' LIMIT 1)
WHERE
entry_code = '650' AND 
entry_number = '650' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008541' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008541' LIMIT 1)
WHERE
entry_code = '651' AND 
entry_number = '651' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008542' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008542' LIMIT 1)
WHERE
entry_code = '652' AND 
entry_number = '652' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008543' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008543' LIMIT 1)
WHERE
entry_code = '653' AND 
entry_number = '653' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008544' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008544' LIMIT 1)
WHERE
entry_code = '654' AND 
entry_number = '654' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008545' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008545' LIMIT 1)
WHERE
entry_code = '655' AND 
entry_number = '655' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008546' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008546' LIMIT 1)
WHERE
entry_code = '656' AND 
entry_number = '656' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008547' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008547' LIMIT 1)
WHERE
entry_code = '657' AND 
entry_number = '657' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008548' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008548' LIMIT 1)
WHERE
entry_code = '658' AND 
entry_number = '658' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008549' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008549' LIMIT 1)
WHERE
entry_code = '659' AND 
entry_number = '659' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008550' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008550' LIMIT 1)
WHERE
entry_code = '660' AND 
entry_number = '660' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008551' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008551' LIMIT 1)
WHERE
entry_code = '661' AND 
entry_number = '661' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008552' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008552' LIMIT 1)
WHERE
entry_code = '662' AND 
entry_number = '662' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008553' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008553' LIMIT 1)
WHERE
entry_code = '663' AND 
entry_number = '663' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008554' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008554' LIMIT 1)
WHERE
entry_code = '664' AND 
entry_number = '664' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008555' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008555' LIMIT 1)
WHERE
entry_code = '665' AND 
entry_number = '665' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008556' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008556' LIMIT 1)
WHERE
entry_code = '666' AND 
entry_number = '666' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008557' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008557' LIMIT 1)
WHERE
entry_code = '667' AND 
entry_number = '667' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008558' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008558' LIMIT 1)
WHERE
entry_code = '668' AND 
entry_number = '668' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008559' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008559' LIMIT 1)
WHERE
entry_code = '669' AND 
entry_number = '669' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008560' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008560' LIMIT 1)
WHERE
entry_code = '670' AND 
entry_number = '670' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008561' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008561' LIMIT 1)
WHERE
entry_code = '671' AND 
entry_number = '671' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008562' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008562' LIMIT 1)
WHERE
entry_code = '672' AND 
entry_number = '672' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008563' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008563' LIMIT 1)
WHERE
entry_code = '673' AND 
entry_number = '673' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008564' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008564' LIMIT 1)
WHERE
entry_code = '674' AND 
entry_number = '674' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008565' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008565' LIMIT 1)
WHERE
entry_code = '675' AND 
entry_number = '675' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008566' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008566' LIMIT 1)
WHERE
entry_code = '676' AND 
entry_number = '676' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008567' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008567' LIMIT 1)
WHERE
entry_code = '677' AND 
entry_number = '677' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008568' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008568' LIMIT 1)
WHERE
entry_code = '678' AND 
entry_number = '678' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008569' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008569' LIMIT 1)
WHERE
entry_code = '679' AND 
entry_number = '679' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008570' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008570' LIMIT 1)
WHERE
entry_code = '680' AND 
entry_number = '680' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008571' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008571' LIMIT 1)
WHERE
entry_code = '681' AND 
entry_number = '681' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008572' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008572' LIMIT 1)
WHERE
entry_code = '682' AND 
entry_number = '682' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008573' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008573' LIMIT 1)
WHERE
entry_code = '683' AND 
entry_number = '683' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008574' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008574' LIMIT 1)
WHERE
entry_code = '684' AND 
entry_number = '684' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008575' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008575' LIMIT 1)
WHERE
entry_code = '685' AND 
entry_number = '685' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008576' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008576' LIMIT 1)
WHERE
entry_code = '686' AND 
entry_number = '686' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008577' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008577' LIMIT 1)
WHERE
entry_code = '687' AND 
entry_number = '687' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008578' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008578' LIMIT 1)
WHERE
entry_code = '688' AND 
entry_number = '688' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008579' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008579' LIMIT 1)
WHERE
entry_code = '689' AND 
entry_number = '689' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008580' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008580' LIMIT 1)
WHERE
entry_code = '690' AND 
entry_number = '690' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008581' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008581' LIMIT 1)
WHERE
entry_code = '691' AND 
entry_number = '691' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008582' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008582' LIMIT 1)
WHERE
entry_code = '692' AND 
entry_number = '692' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008583' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008583' LIMIT 1)
WHERE
entry_code = '693' AND 
entry_number = '693' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008584' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008584' LIMIT 1)
WHERE
entry_code = '694' AND 
entry_number = '694' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008585' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008585' LIMIT 1)
WHERE
entry_code = '695' AND 
entry_number = '695' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008586' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008586' LIMIT 1)
WHERE
entry_code = '696' AND 
entry_number = '696' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008587' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008587' LIMIT 1)
WHERE
entry_code = '697' AND 
entry_number = '697' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008588' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008588' LIMIT 1)
WHERE
entry_code = '698' AND 
entry_number = '698' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008589' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008589' LIMIT 1)
WHERE
entry_code = '699' AND 
entry_number = '699' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008590' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008590' LIMIT 1)
WHERE
entry_code = '700' AND 
entry_number = '700' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008641' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008641' LIMIT 1)
WHERE
entry_code = '751' AND 
entry_number = '751' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008592' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008592' LIMIT 1)
WHERE
entry_code = '702' AND 
entry_number = '702' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008593' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008593' LIMIT 1)
WHERE
entry_code = '703' AND 
entry_number = '703' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008594' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008594' LIMIT 1)
WHERE
entry_code = '704' AND 
entry_number = '704' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008595' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008595' LIMIT 1)
WHERE
entry_code = '705' AND 
entry_number = '705' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008596' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008596' LIMIT 1)
WHERE
entry_code = '706' AND 
entry_number = '706' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008597' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008597' LIMIT 1)
WHERE
entry_code = '707' AND 
entry_number = '707' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008598' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008598' LIMIT 1)
WHERE
entry_code = '708' AND 
entry_number = '708' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008599' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008599' LIMIT 1)
WHERE
entry_code = '709' AND 
entry_number = '709' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008600' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008600' LIMIT 1)
WHERE
entry_code = '710' AND 
entry_number = '710' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008601' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008601' LIMIT 1)
WHERE
entry_code = '711' AND 
entry_number = '711' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008602' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008602' LIMIT 1)
WHERE
entry_code = '712' AND 
entry_number = '712' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008603' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008603' LIMIT 1)
WHERE
entry_code = '713' AND 
entry_number = '713' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008604' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008604' LIMIT 1)
WHERE
entry_code = '714' AND 
entry_number = '714' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008605' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008605' LIMIT 1)
WHERE
entry_code = '715' AND 
entry_number = '715' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008606' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008606' LIMIT 1)
WHERE
entry_code = '716' AND 
entry_number = '716' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008607' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008607' LIMIT 1)
WHERE
entry_code = '717' AND 
entry_number = '717' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008608' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008608' LIMIT 1)
WHERE
entry_code = '718' AND 
entry_number = '718' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008609' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008609' LIMIT 1)
WHERE
entry_code = '719' AND 
entry_number = '719' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008610' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008610' LIMIT 1)
WHERE
entry_code = '720' AND 
entry_number = '720' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008611' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008611' LIMIT 1)
WHERE
entry_code = '721' AND 
entry_number = '721' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008612' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008612' LIMIT 1)
WHERE
entry_code = '722' AND 
entry_number = '722' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008613' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008613' LIMIT 1)
WHERE
entry_code = '723' AND 
entry_number = '723' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008614' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008614' LIMIT 1)
WHERE
entry_code = '724' AND 
entry_number = '724' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008615' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008615' LIMIT 1)
WHERE
entry_code = '725' AND 
entry_number = '725' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008616' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008616' LIMIT 1)
WHERE
entry_code = '726' AND 
entry_number = '726' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008617' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008617' LIMIT 1)
WHERE
entry_code = '727' AND 
entry_number = '727' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008618' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008618' LIMIT 1)
WHERE
entry_code = '728' AND 
entry_number = '728' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008619' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008619' LIMIT 1)
WHERE
entry_code = '729' AND 
entry_number = '729' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008620' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008620' LIMIT 1)
WHERE
entry_code = '730' AND 
entry_number = '730' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008621' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008621' LIMIT 1)
WHERE
entry_code = '731' AND 
entry_number = '731' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008622' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008622' LIMIT 1)
WHERE
entry_code = '732' AND 
entry_number = '732' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008623' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008623' LIMIT 1)
WHERE
entry_code = '733' AND 
entry_number = '733' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008624' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008624' LIMIT 1)
WHERE
entry_code = '734' AND 
entry_number = '734' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008625' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008625' LIMIT 1)
WHERE
entry_code = '735' AND 
entry_number = '735' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008626' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008626' LIMIT 1)
WHERE
entry_code = '736' AND 
entry_number = '736' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008627' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008627' LIMIT 1)
WHERE
entry_code = '737' AND 
entry_number = '737' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008628' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008628' LIMIT 1)
WHERE
entry_code = '738' AND 
entry_number = '738' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008629' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008629' LIMIT 1)
WHERE
entry_code = '739' AND 
entry_number = '739' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008630' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008630' LIMIT 1)
WHERE
entry_code = '740' AND 
entry_number = '740' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008631' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008631' LIMIT 1)
WHERE
entry_code = '741' AND 
entry_number = '741' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008632' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008632' LIMIT 1)
WHERE
entry_code = '742' AND 
entry_number = '742' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008633' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008633' LIMIT 1)
WHERE
entry_code = '743' AND 
entry_number = '743' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008634' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008634' LIMIT 1)
WHERE
entry_code = '744' AND 
entry_number = '744' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008635' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008635' LIMIT 1)
WHERE
entry_code = '745' AND 
entry_number = '745' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008636' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008636' LIMIT 1)
WHERE
entry_code = '746' AND 
entry_number = '746' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008637' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008637' LIMIT 1)
WHERE
entry_code = '747' AND 
entry_number = '747' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008638' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008638' LIMIT 1)
WHERE
entry_code = '748' AND 
entry_number = '748' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008639' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008639' LIMIT 1)
WHERE
entry_code = '749' AND 
entry_number = '749' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008640' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008640' LIMIT 1)
WHERE
entry_code = '750' AND 
entry_number = '750' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008642' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008642' LIMIT 1)
WHERE
entry_code = '752' AND 
entry_number = '752' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008643' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008643' LIMIT 1)
WHERE
entry_code = '753' AND 
entry_number = '753' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008644' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008644' LIMIT 1)
WHERE
entry_code = '754' AND 
entry_number = '754' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008645' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008645' LIMIT 1)
WHERE
entry_code = '755' AND 
entry_number = '755' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008646' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008646' LIMIT 1)
WHERE
entry_code = '756' AND 
entry_number = '756' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008647' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008647' LIMIT 1)
WHERE
entry_code = '757' AND 
entry_number = '757' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008648' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008648' LIMIT 1)
WHERE
entry_code = '758' AND 
entry_number = '758' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008649' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008649' LIMIT 1)
WHERE
entry_code = '759' AND 
entry_number = '759' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008650' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008650' LIMIT 1)
WHERE
entry_code = '760' AND 
entry_number = '760' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008651' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008651' LIMIT 1)
WHERE
entry_code = '761' AND 
entry_number = '761' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008652' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008652' LIMIT 1)
WHERE
entry_code = '762' AND 
entry_number = '762' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008653' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008653' LIMIT 1)
WHERE
entry_code = '763' AND 
entry_number = '763' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008654' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008654' LIMIT 1)
WHERE
entry_code = '764' AND 
entry_number = '764' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008655' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008655' LIMIT 1)
WHERE
entry_code = '765' AND 
entry_number = '765' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008656' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008656' LIMIT 1)
WHERE
entry_code = '766' AND 
entry_number = '766' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008657' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008657' LIMIT 1)
WHERE
entry_code = '767' AND 
entry_number = '767' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008658' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008658' LIMIT 1)
WHERE
entry_code = '768' AND 
entry_number = '768' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008659' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008659' LIMIT 1)
WHERE
entry_code = '769' AND 
entry_number = '769' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008660' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008660' LIMIT 1)
WHERE
entry_code = '770' AND 
entry_number = '770' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008661' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008661' LIMIT 1)
WHERE
entry_code = '771' AND 
entry_number = '771' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008662' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008662' LIMIT 1)
WHERE
entry_code = '772' AND 
entry_number = '772' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008663' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008663' LIMIT 1)
WHERE
entry_code = '773' AND 
entry_number = '773' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008664' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008664' LIMIT 1)
WHERE
entry_code = '774' AND 
entry_number = '774' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008665' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008665' LIMIT 1)
WHERE
entry_code = '775' AND 
entry_number = '775' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008666' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008666' LIMIT 1)
WHERE
entry_code = '776' AND 
entry_number = '776' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008667' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008667' LIMIT 1)
WHERE
entry_code = '777' AND 
entry_number = '777' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008668' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008668' LIMIT 1)
WHERE
entry_code = '778' AND 
entry_number = '778' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008669' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008669' LIMIT 1)
WHERE
entry_code = '779' AND 
entry_number = '779' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008670' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008670' LIMIT 1)
WHERE
entry_code = '780' AND 
entry_number = '780' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008671' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008671' LIMIT 1)
WHERE
entry_code = '781' AND 
entry_number = '781' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008672' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008672' LIMIT 1)
WHERE
entry_code = '782' AND 
entry_number = '782' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008673' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008673' LIMIT 1)
WHERE
entry_code = '783' AND 
entry_number = '783' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008674' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008674' LIMIT 1)
WHERE
entry_code = '784' AND 
entry_number = '784' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008675' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008675' LIMIT 1)
WHERE
entry_code = '785' AND 
entry_number = '785' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008676' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008676' LIMIT 1)
WHERE
entry_code = '786' AND 
entry_number = '786' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008677' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008677' LIMIT 1)
WHERE
entry_code = '787' AND 
entry_number = '787' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008678' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008678' LIMIT 1)
WHERE
entry_code = '788' AND 
entry_number = '788' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008679' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008679' LIMIT 1)
WHERE
entry_code = '789' AND 
entry_number = '789' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008680' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008680' LIMIT 1)
WHERE
entry_code = '790' AND 
entry_number = '790' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008681' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008681' LIMIT 1)
WHERE
entry_code = '791' AND 
entry_number = '791' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008682' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008682' LIMIT 1)
WHERE
entry_code = '792' AND 
entry_number = '792' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008683' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008683' LIMIT 1)
WHERE
entry_code = '793' AND 
entry_number = '793' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008684' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008684' LIMIT 1)
WHERE
entry_code = '794' AND 
entry_number = '794' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008685' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008685' LIMIT 1)
WHERE
entry_code = '795' AND 
entry_number = '795' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008686' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008686' LIMIT 1)
WHERE
entry_code = '796' AND 
entry_number = '796' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008687' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008687' LIMIT 1)
WHERE
entry_code = '797' AND 
entry_number = '797' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008688' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008688' LIMIT 1)
WHERE
entry_code = '798' AND 
entry_number = '798' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008689' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008689' LIMIT 1)
WHERE
entry_code = '799' AND 
entry_number = '799' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008690' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008690' LIMIT 1)
WHERE
entry_code = '800' AND 
entry_number = '800' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008691' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008691' LIMIT 1)
WHERE
entry_code = '801' AND 
entry_number = '801' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008692' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008692' LIMIT 1)
WHERE
entry_code = '802' AND 
entry_number = '802' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008693' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008693' LIMIT 1)
WHERE
entry_code = '803' AND 
entry_number = '803' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008694' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008694' LIMIT 1)
WHERE
entry_code = '804' AND 
entry_number = '804' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008695' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008695' LIMIT 1)
WHERE
entry_code = '805' AND 
entry_number = '805' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008696' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008696' LIMIT 1)
WHERE
entry_code = '806' AND 
entry_number = '806' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008697' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008697' LIMIT 1)
WHERE
entry_code = '807' AND 
entry_number = '807' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008698' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008698' LIMIT 1)
WHERE
entry_code = '808' AND 
entry_number = '808' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008699' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008699' LIMIT 1)
WHERE
entry_code = '809' AND 
entry_number = '809' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008700' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008700' LIMIT 1)
WHERE
entry_code = '810' AND 
entry_number = '810' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008701' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008701' LIMIT 1)
WHERE
entry_code = '811' AND 
entry_number = '811' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008702' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008702' LIMIT 1)
WHERE
entry_code = '812' AND 
entry_number = '812' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008703' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008703' LIMIT 1)
WHERE
entry_code = '813' AND 
entry_number = '813' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008704' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008704' LIMIT 1)
WHERE
entry_code = '814' AND 
entry_number = '814' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008705' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008705' LIMIT 1)
WHERE
entry_code = '815' AND 
entry_number = '815' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008706' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008706' LIMIT 1)
WHERE
entry_code = '816' AND 
entry_number = '816' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008707' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008707' LIMIT 1)
WHERE
entry_code = '817' AND 
entry_number = '817' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008708' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008708' LIMIT 1)
WHERE
entry_code = '818' AND 
entry_number = '818' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008709' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008709' LIMIT 1)
WHERE
entry_code = '819' AND 
entry_number = '819' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008710' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008710' LIMIT 1)
WHERE
entry_code = '820' AND 
entry_number = '820' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008711' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008711' LIMIT 1)
WHERE
entry_code = '821' AND 
entry_number = '821' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008712' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008712' LIMIT 1)
WHERE
entry_code = '822' AND 
entry_number = '822' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008713' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008713' LIMIT 1)
WHERE
entry_code = '823' AND 
entry_number = '823' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008714' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008714' LIMIT 1)
WHERE
entry_code = '824' AND 
entry_number = '824' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008715' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008715' LIMIT 1)
WHERE
entry_code = '825' AND 
entry_number = '825' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008716' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008716' LIMIT 1)
WHERE
entry_code = '826' AND 
entry_number = '826' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008717' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008717' LIMIT 1)
WHERE
entry_code = '827' AND 
entry_number = '827' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008718' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008718' LIMIT 1)
WHERE
entry_code = '828' AND 
entry_number = '828' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008719' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008719' LIMIT 1)
WHERE
entry_code = '829' AND 
entry_number = '829' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008720' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008720' LIMIT 1)
WHERE
entry_code = '830' AND 
entry_number = '830' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008721' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008721' LIMIT 1)
WHERE
entry_code = '831' AND 
entry_number = '831' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008722' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008722' LIMIT 1)
WHERE
entry_code = '832' AND 
entry_number = '832' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008723' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008723' LIMIT 1)
WHERE
entry_code = '833' AND 
entry_number = '833' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008724' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008724' LIMIT 1)
WHERE
entry_code = '834' AND 
entry_number = '834' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008725' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008725' LIMIT 1)
WHERE
entry_code = '835' AND 
entry_number = '835' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008726' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008726' LIMIT 1)
WHERE
entry_code = '836' AND 
entry_number = '836' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008727' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008727' LIMIT 1)
WHERE
entry_code = '837' AND 
entry_number = '837' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008728' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008728' LIMIT 1)
WHERE
entry_code = '838' AND 
entry_number = '838' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008729' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008729' LIMIT 1)
WHERE
entry_code = '839' AND 
entry_number = '839' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008730' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008730' LIMIT 1)
WHERE
entry_code = '840' AND 
entry_number = '840' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008731' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008731' LIMIT 1)
WHERE
entry_code = '841' AND 
entry_number = '841' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008732' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008732' LIMIT 1)
WHERE
entry_code = '842' AND 
entry_number = '842' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008733' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008733' LIMIT 1)
WHERE
entry_code = '843' AND 
entry_number = '843' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008856' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008856' LIMIT 1)
WHERE
entry_code = '966' AND 
entry_number = '966' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008857' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008857' LIMIT 1)
WHERE
entry_code = '967' AND 
entry_number = '967' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008858' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008858' LIMIT 1)
WHERE
entry_code = '968' AND 
entry_number = '968' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008859' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008859' LIMIT 1)
WHERE
entry_code = '969' AND 
entry_number = '969' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008860' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008860' LIMIT 1)
WHERE
entry_code = '970' AND 
entry_number = '970' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008861' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008861' LIMIT 1)
WHERE
entry_code = '971' AND 
entry_number = '971' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008734' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008734' LIMIT 1)
WHERE
entry_code = '844' AND 
entry_number = '844' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008735' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008735' LIMIT 1)
WHERE
entry_code = '845' AND 
entry_number = '845' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008736' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008736' LIMIT 1)
WHERE
entry_code = '846' AND 
entry_number = '846' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008737' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008737' LIMIT 1)
WHERE
entry_code = '847' AND 
entry_number = '847' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008738' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008738' LIMIT 1)
WHERE
entry_code = '848' AND 
entry_number = '848' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008739' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008739' LIMIT 1)
WHERE
entry_code = '849' AND 
entry_number = '849' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008740' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008740' LIMIT 1)
WHERE
entry_code = '850' AND 
entry_number = '850' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008741' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008741' LIMIT 1)
WHERE
entry_code = '851' AND 
entry_number = '851' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008742' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008742' LIMIT 1)
WHERE
entry_code = '852' AND 
entry_number = '852' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008743' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008743' LIMIT 1)
WHERE
entry_code = '853' AND 
entry_number = '853' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008744' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008744' LIMIT 1)
WHERE
entry_code = '854' AND 
entry_number = '854' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008745' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008745' LIMIT 1)
WHERE
entry_code = '855' AND 
entry_number = '855' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008746' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008746' LIMIT 1)
WHERE
entry_code = '856' AND 
entry_number = '856' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008747' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008747' LIMIT 1)
WHERE
entry_code = '857' AND 
entry_number = '857' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008748' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008748' LIMIT 1)
WHERE
entry_code = '858' AND 
entry_number = '858' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008749' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008749' LIMIT 1)
WHERE
entry_code = '859' AND 
entry_number = '859' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008750' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008750' LIMIT 1)
WHERE
entry_code = '860' AND 
entry_number = '860' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008751' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008751' LIMIT 1)
WHERE
entry_code = '861' AND 
entry_number = '861' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008752' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008752' LIMIT 1)
WHERE
entry_code = '862' AND 
entry_number = '862' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008753' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008753' LIMIT 1)
WHERE
entry_code = '863' AND 
entry_number = '863' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008754' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008754' LIMIT 1)
WHERE
entry_code = '864' AND 
entry_number = '864' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008755' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008755' LIMIT 1)
WHERE
entry_code = '865' AND 
entry_number = '865' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008756' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008756' LIMIT 1)
WHERE
entry_code = '866' AND 
entry_number = '866' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008757' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008757' LIMIT 1)
WHERE
entry_code = '867' AND 
entry_number = '867' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008758' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008758' LIMIT 1)
WHERE
entry_code = '868' AND 
entry_number = '868' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008759' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008759' LIMIT 1)
WHERE
entry_code = '869' AND 
entry_number = '869' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008760' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008760' LIMIT 1)
WHERE
entry_code = '870' AND 
entry_number = '870' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008761' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008761' LIMIT 1)
WHERE
entry_code = '871' AND 
entry_number = '871' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008762' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008762' LIMIT 1)
WHERE
entry_code = '872' AND 
entry_number = '872' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008763' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008763' LIMIT 1)
WHERE
entry_code = '873' AND 
entry_number = '873' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008764' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008764' LIMIT 1)
WHERE
entry_code = '874' AND 
entry_number = '874' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008765' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008765' LIMIT 1)
WHERE
entry_code = '875' AND 
entry_number = '875' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008766' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008766' LIMIT 1)
WHERE
entry_code = '876' AND 
entry_number = '876' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008767' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008767' LIMIT 1)
WHERE
entry_code = '877' AND 
entry_number = '877' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008768' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008768' LIMIT 1)
WHERE
entry_code = '878' AND 
entry_number = '878' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008769' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008769' LIMIT 1)
WHERE
entry_code = '879' AND 
entry_number = '879' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008770' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008770' LIMIT 1)
WHERE
entry_code = '880' AND 
entry_number = '880' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008771' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008771' LIMIT 1)
WHERE
entry_code = '881' AND 
entry_number = '881' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008772' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008772' LIMIT 1)
WHERE
entry_code = '882' AND 
entry_number = '882' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008773' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008773' LIMIT 1)
WHERE
entry_code = '883' AND 
entry_number = '883' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008774' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008774' LIMIT 1)
WHERE
entry_code = '884' AND 
entry_number = '884' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008775' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008775' LIMIT 1)
WHERE
entry_code = '885' AND 
entry_number = '885' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008776' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008776' LIMIT 1)
WHERE
entry_code = '886' AND 
entry_number = '886' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008777' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008777' LIMIT 1)
WHERE
entry_code = '887' AND 
entry_number = '887' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008778' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008778' LIMIT 1)
WHERE
entry_code = '888' AND 
entry_number = '888' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008779' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008779' LIMIT 1)
WHERE
entry_code = '889' AND 
entry_number = '889' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008780' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008780' LIMIT 1)
WHERE
entry_code = '890' AND 
entry_number = '890' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008781' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008781' LIMIT 1)
WHERE
entry_code = '891' AND 
entry_number = '891' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008782' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008782' LIMIT 1)
WHERE
entry_code = '892' AND 
entry_number = '892' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008783' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008783' LIMIT 1)
WHERE
entry_code = '893' AND 
entry_number = '893' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008784' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008784' LIMIT 1)
WHERE
entry_code = '894' AND 
entry_number = '894' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008785' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008785' LIMIT 1)
WHERE
entry_code = '895' AND 
entry_number = '895' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008786' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008786' LIMIT 1)
WHERE
entry_code = '896' AND 
entry_number = '896' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008787' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008787' LIMIT 1)
WHERE
entry_code = '897' AND 
entry_number = '897' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008788' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008788' LIMIT 1)
WHERE
entry_code = '898' AND 
entry_number = '898' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008789' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008789' LIMIT 1)
WHERE
entry_code = '899' AND 
entry_number = '899' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008790' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008790' LIMIT 1)
WHERE
entry_code = '900' AND 
entry_number = '900' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008791' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008791' LIMIT 1)
WHERE
entry_code = '901' AND 
entry_number = '901' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008792' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008792' LIMIT 1)
WHERE
entry_code = '902' AND 
entry_number = '902' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008793' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008793' LIMIT 1)
WHERE
entry_code = '903' AND 
entry_number = '903' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008794' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008794' LIMIT 1)
WHERE
entry_code = '904' AND 
entry_number = '904' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008795' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008795' LIMIT 1)
WHERE
entry_code = '905' AND 
entry_number = '905' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008796' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008796' LIMIT 1)
WHERE
entry_code = '906' AND 
entry_number = '906' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008797' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008797' LIMIT 1)
WHERE
entry_code = '907' AND 
entry_number = '907' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008798' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008798' LIMIT 1)
WHERE
entry_code = '908' AND 
entry_number = '908' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008799' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008799' LIMIT 1)
WHERE
entry_code = '909' AND 
entry_number = '909' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008800' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008800' LIMIT 1)
WHERE
entry_code = '910' AND 
entry_number = '910' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008801' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008801' LIMIT 1)
WHERE
entry_code = '911' AND 
entry_number = '911' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008802' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008802' LIMIT 1)
WHERE
entry_code = '912' AND 
entry_number = '912' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008803' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008803' LIMIT 1)
WHERE
entry_code = '913' AND 
entry_number = '913' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008804' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008804' LIMIT 1)
WHERE
entry_code = '914' AND 
entry_number = '914' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008805' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008805' LIMIT 1)
WHERE
entry_code = '915' AND 
entry_number = '915' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008806' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008806' LIMIT 1)
WHERE
entry_code = '916' AND 
entry_number = '916' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008807' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008807' LIMIT 1)
WHERE
entry_code = '917' AND 
entry_number = '917' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008808' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008808' LIMIT 1)
WHERE
entry_code = '918' AND 
entry_number = '918' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008809' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008809' LIMIT 1)
WHERE
entry_code = '919' AND 
entry_number = '919' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008810' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008810' LIMIT 1)
WHERE
entry_code = '920' AND 
entry_number = '920' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008811' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008811' LIMIT 1)
WHERE
entry_code = '921' AND 
entry_number = '921' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008812' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008812' LIMIT 1)
WHERE
entry_code = '922' AND 
entry_number = '922' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008813' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008813' LIMIT 1)
WHERE
entry_code = '923' AND 
entry_number = '923' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008814' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008814' LIMIT 1)
WHERE
entry_code = '924' AND 
entry_number = '924' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008815' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008815' LIMIT 1)
WHERE
entry_code = '925' AND 
entry_number = '925' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008816' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008816' LIMIT 1)
WHERE
entry_code = '926' AND 
entry_number = '926' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008817' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008817' LIMIT 1)
WHERE
entry_code = '927' AND 
entry_number = '927' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008818' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008818' LIMIT 1)
WHERE
entry_code = '928' AND 
entry_number = '928' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008819' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008819' LIMIT 1)
WHERE
entry_code = '929' AND 
entry_number = '929' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008820' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008820' LIMIT 1)
WHERE
entry_code = '930' AND 
entry_number = '930' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008821' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008821' LIMIT 1)
WHERE
entry_code = '931' AND 
entry_number = '931' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008822' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008822' LIMIT 1)
WHERE
entry_code = '932' AND 
entry_number = '932' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008823' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008823' LIMIT 1)
WHERE
entry_code = '933' AND 
entry_number = '933' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008824' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008824' LIMIT 1)
WHERE
entry_code = '934' AND 
entry_number = '934' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008825' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008825' LIMIT 1)
WHERE
entry_code = '935' AND 
entry_number = '935' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008826' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008826' LIMIT 1)
WHERE
entry_code = '936' AND 
entry_number = '936' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008827' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008827' LIMIT 1)
WHERE
entry_code = '937' AND 
entry_number = '937' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008828' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008828' LIMIT 1)
WHERE
entry_code = '938' AND 
entry_number = '938' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008829' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008829' LIMIT 1)
WHERE
entry_code = '939' AND 
entry_number = '939' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008830' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008830' LIMIT 1)
WHERE
entry_code = '940' AND 
entry_number = '940' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008831' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008831' LIMIT 1)
WHERE
entry_code = '941' AND 
entry_number = '941' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008832' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008832' LIMIT 1)
WHERE
entry_code = '942' AND 
entry_number = '942' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008833' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008833' LIMIT 1)
WHERE
entry_code = '943' AND 
entry_number = '943' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008834' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008834' LIMIT 1)
WHERE
entry_code = '944' AND 
entry_number = '944' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008835' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008835' LIMIT 1)
WHERE
entry_code = '945' AND 
entry_number = '945' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008836' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008836' LIMIT 1)
WHERE
entry_code = '946' AND 
entry_number = '946' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008837' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008837' LIMIT 1)
WHERE
entry_code = '947' AND 
entry_number = '947' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008838' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008838' LIMIT 1)
WHERE
entry_code = '948' AND 
entry_number = '948' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008839' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008839' LIMIT 1)
WHERE
entry_code = '949' AND 
entry_number = '949' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008840' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008840' LIMIT 1)
WHERE
entry_code = '950' AND 
entry_number = '950' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008841' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008841' LIMIT 1)
WHERE
entry_code = '951' AND 
entry_number = '951' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008842' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008842' LIMIT 1)
WHERE
entry_code = '952' AND 
entry_number = '952' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008843' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008843' LIMIT 1)
WHERE
entry_code = '953' AND 
entry_number = '953' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008844' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008844' LIMIT 1)
WHERE
entry_code = '954' AND 
entry_number = '954' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008845' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008845' LIMIT 1)
WHERE
entry_code = '955' AND 
entry_number = '955' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008846' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008846' LIMIT 1)
WHERE
entry_code = '956' AND 
entry_number = '956' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008847' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008847' LIMIT 1)
WHERE
entry_code = '957' AND 
entry_number = '957' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008848' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008848' LIMIT 1)
WHERE
entry_code = '958' AND 
entry_number = '958' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008849' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008849' LIMIT 1)
WHERE
entry_code = '959' AND 
entry_number = '959' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008850' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008850' LIMIT 1)
WHERE
entry_code = '960' AND 
entry_number = '960' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008851' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008851' LIMIT 1)
WHERE
entry_code = '961' AND 
entry_number = '961' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008852' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008852' LIMIT 1)
WHERE
entry_code = '962' AND 
entry_number = '962' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008853' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008853' LIMIT 1)
WHERE
entry_code = '963' AND 
entry_number = '963' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008854' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008854' LIMIT 1)
WHERE
entry_code = '964' AND 
entry_number = '964' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008855' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008855' LIMIT 1)
WHERE
entry_code = '965' AND 
entry_number = '965' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008916' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008916' LIMIT 1)
WHERE
entry_code = '1026' AND 
entry_number = '1026' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008917' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008917' LIMIT 1)
WHERE
entry_code = '1027' AND 
entry_number = '1027' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008918' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008918' LIMIT 1)
WHERE
entry_code = '1028' AND 
entry_number = '1028' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008919' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008919' LIMIT 1)
WHERE
entry_code = '1029' AND 
entry_number = '1029' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008862' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008862' LIMIT 1)
WHERE
entry_code = '972' AND 
entry_number = '972' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008863' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008863' LIMIT 1)
WHERE
entry_code = '973' AND 
entry_number = '973' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008864' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008864' LIMIT 1)
WHERE
entry_code = '974' AND 
entry_number = '974' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008865' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008865' LIMIT 1)
WHERE
entry_code = '975' AND 
entry_number = '975' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008866' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008866' LIMIT 1)
WHERE
entry_code = '976' AND 
entry_number = '976' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008867' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008867' LIMIT 1)
WHERE
entry_code = '977' AND 
entry_number = '977' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008868' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008868' LIMIT 1)
WHERE
entry_code = '978' AND 
entry_number = '978' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008869' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008869' LIMIT 1)
WHERE
entry_code = '979' AND 
entry_number = '979' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008870' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008870' LIMIT 1)
WHERE
entry_code = '980' AND 
entry_number = '980' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008871' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008871' LIMIT 1)
WHERE
entry_code = '981' AND 
entry_number = '981' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008872' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008872' LIMIT 1)
WHERE
entry_code = '982' AND 
entry_number = '982' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008873' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008873' LIMIT 1)
WHERE
entry_code = '983' AND 
entry_number = '983' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008874' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008874' LIMIT 1)
WHERE
entry_code = '984' AND 
entry_number = '984' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008875' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008875' LIMIT 1)
WHERE
entry_code = '985' AND 
entry_number = '985' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008876' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008876' LIMIT 1)
WHERE
entry_code = '986' AND 
entry_number = '986' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008877' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008877' LIMIT 1)
WHERE
entry_code = '987' AND 
entry_number = '987' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008878' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008878' LIMIT 1)
WHERE
entry_code = '988' AND 
entry_number = '988' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008879' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008879' LIMIT 1)
WHERE
entry_code = '989' AND 
entry_number = '989' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008880' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008880' LIMIT 1)
WHERE
entry_code = '990' AND 
entry_number = '990' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008881' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008881' LIMIT 1)
WHERE
entry_code = '991' AND 
entry_number = '991' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008882' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008882' LIMIT 1)
WHERE
entry_code = '992' AND 
entry_number = '992' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008883' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008883' LIMIT 1)
WHERE
entry_code = '993' AND 
entry_number = '993' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008884' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008884' LIMIT 1)
WHERE
entry_code = '994' AND 
entry_number = '994' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008885' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008885' LIMIT 1)
WHERE
entry_code = '995' AND 
entry_number = '995' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008886' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008886' LIMIT 1)
WHERE
entry_code = '996' AND 
entry_number = '996' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008887' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008887' LIMIT 1)
WHERE
entry_code = '997' AND 
entry_number = '997' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008888' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008888' LIMIT 1)
WHERE
entry_code = '998' AND 
entry_number = '998' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008889' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008889' LIMIT 1)
WHERE
entry_code = '999' AND 
entry_number = '999' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008890' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008890' LIMIT 1)
WHERE
entry_code = '1000' AND 
entry_number = '1000' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008891' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008891' LIMIT 1)
WHERE
entry_code = '1001' AND 
entry_number = '1001' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008892' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008892' LIMIT 1)
WHERE
entry_code = '1002' AND 
entry_number = '1002' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008893' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008893' LIMIT 1)
WHERE
entry_code = '1003' AND 
entry_number = '1003' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008894' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008894' LIMIT 1)
WHERE
entry_code = '1004' AND 
entry_number = '1004' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008895' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008895' LIMIT 1)
WHERE
entry_code = '1005' AND 
entry_number = '1005' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008896' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008896' LIMIT 1)
WHERE
entry_code = '1006' AND 
entry_number = '1006' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008897' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008897' LIMIT 1)
WHERE
entry_code = '1007' AND 
entry_number = '1007' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008898' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008898' LIMIT 1)
WHERE
entry_code = '1008' AND 
entry_number = '1008' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008899' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008899' LIMIT 1)
WHERE
entry_code = '1009' AND 
entry_number = '1009' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008900' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008900' LIMIT 1)
WHERE
entry_code = '1010' AND 
entry_number = '1010' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008901' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008901' LIMIT 1)
WHERE
entry_code = '1011' AND 
entry_number = '1011' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008902' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008902' LIMIT 1)
WHERE
entry_code = '1012' AND 
entry_number = '1012' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008903' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008903' LIMIT 1)
WHERE
entry_code = '1013' AND 
entry_number = '1013' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008904' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008904' LIMIT 1)
WHERE
entry_code = '1014' AND 
entry_number = '1014' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008905' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008905' LIMIT 1)
WHERE
entry_code = '1015' AND 
entry_number = '1015' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008906' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008906' LIMIT 1)
WHERE
entry_code = '1016' AND 
entry_number = '1016' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008907' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008907' LIMIT 1)
WHERE
entry_code = '1017' AND 
entry_number = '1017' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008908' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008908' LIMIT 1)
WHERE
entry_code = '1018' AND 
entry_number = '1018' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008909' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008909' LIMIT 1)
WHERE
entry_code = '1019' AND 
entry_number = '1019' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008910' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008910' LIMIT 1)
WHERE
entry_code = '1020' AND 
entry_number = '1020' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008911' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008911' LIMIT 1)
WHERE
entry_code = '1021' AND 
entry_number = '1021' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008912' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008912' LIMIT 1)
WHERE
entry_code = '1022' AND 
entry_number = '1022' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008913' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008913' LIMIT 1)
WHERE
entry_code = '1023' AND 
entry_number = '1023' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008914' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008914' LIMIT 1)
WHERE
entry_code = '1024' AND 
entry_number = '1024' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008915' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008915' LIMIT 1)
WHERE
entry_code = '1025' AND 
entry_number = '1025' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008960' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008960' LIMIT 1)
WHERE
entry_code = '1070' AND 
entry_number = '1070' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008961' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008961' LIMIT 1)
WHERE
entry_code = '1071' AND 
entry_number = '1071' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008920' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008920' LIMIT 1)
WHERE
entry_code = '1030' AND 
entry_number = '1030' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008921' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008921' LIMIT 1)
WHERE
entry_code = '1031' AND 
entry_number = '1031' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008922' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008922' LIMIT 1)
WHERE
entry_code = '1032' AND 
entry_number = '1032' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008923' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008923' LIMIT 1)
WHERE
entry_code = '1033' AND 
entry_number = '1033' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008924' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008924' LIMIT 1)
WHERE
entry_code = '1034' AND 
entry_number = '1034' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008925' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008925' LIMIT 1)
WHERE
entry_code = '1035' AND 
entry_number = '1035' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008926' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008926' LIMIT 1)
WHERE
entry_code = '1036' AND 
entry_number = '1036' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008927' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008927' LIMIT 1)
WHERE
entry_code = '1037' AND 
entry_number = '1037' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008928' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008928' LIMIT 1)
WHERE
entry_code = '1038' AND 
entry_number = '1038' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008929' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008929' LIMIT 1)
WHERE
entry_code = '1039' AND 
entry_number = '1039' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008930' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008930' LIMIT 1)
WHERE
entry_code = '1040' AND 
entry_number = '1040' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008931' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008931' LIMIT 1)
WHERE
entry_code = '1041' AND 
entry_number = '1041' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008932' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008932' LIMIT 1)
WHERE
entry_code = '1042' AND 
entry_number = '1042' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008933' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008933' LIMIT 1)
WHERE
entry_code = '1043' AND 
entry_number = '1043' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008934' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008934' LIMIT 1)
WHERE
entry_code = '1044' AND 
entry_number = '1044' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008935' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008935' LIMIT 1)
WHERE
entry_code = '1045' AND 
entry_number = '1045' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008936' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008936' LIMIT 1)
WHERE
entry_code = '1046' AND 
entry_number = '1046' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008937' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008937' LIMIT 1)
WHERE
entry_code = '1047' AND 
entry_number = '1047' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008938' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008938' LIMIT 1)
WHERE
entry_code = '1048' AND 
entry_number = '1048' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008939' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008939' LIMIT 1)
WHERE
entry_code = '1049' AND 
entry_number = '1049' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008940' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008940' LIMIT 1)
WHERE
entry_code = '1050' AND 
entry_number = '1050' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008941' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008941' LIMIT 1)
WHERE
entry_code = '1051' AND 
entry_number = '1051' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008942' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008942' LIMIT 1)
WHERE
entry_code = '1052' AND 
entry_number = '1052' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008943' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008943' LIMIT 1)
WHERE
entry_code = '1053' AND 
entry_number = '1053' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008944' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008944' LIMIT 1)
WHERE
entry_code = '1054' AND 
entry_number = '1054' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008945' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008945' LIMIT 1)
WHERE
entry_code = '1055' AND 
entry_number = '1055' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008946' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008946' LIMIT 1)
WHERE
entry_code = '1056' AND 
entry_number = '1056' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008947' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008947' LIMIT 1)
WHERE
entry_code = '1057' AND 
entry_number = '1057' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008948' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008948' LIMIT 1)
WHERE
entry_code = '1058' AND 
entry_number = '1058' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008949' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008949' LIMIT 1)
WHERE
entry_code = '1059' AND 
entry_number = '1059' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008950' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008950' LIMIT 1)
WHERE
entry_code = '1060' AND 
entry_number = '1060' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008951' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008951' LIMIT 1)
WHERE
entry_code = '1061' AND 
entry_number = '1061' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008952' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008952' LIMIT 1)
WHERE
entry_code = '1062' AND 
entry_number = '1062' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008953' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008953' LIMIT 1)
WHERE
entry_code = '1063' AND 
entry_number = '1063' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008954' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008954' LIMIT 1)
WHERE
entry_code = '1064' AND 
entry_number = '1064' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008955' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008955' LIMIT 1)
WHERE
entry_code = '1065' AND 
entry_number = '1065' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008956' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008956' LIMIT 1)
WHERE
entry_code = '1066' AND 
entry_number = '1066' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008957' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008957' LIMIT 1)
WHERE
entry_code = '1067' AND 
entry_number = '1067' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008958' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008958' LIMIT 1)
WHERE
entry_code = '1068' AND 
entry_number = '1068' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008959' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008959' LIMIT 1)
WHERE
entry_code = '1069' AND 
entry_number = '1069' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008973' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008973' LIMIT 1)
WHERE
entry_code = '1083' AND 
entry_number = '1083' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008974' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008974' LIMIT 1)
WHERE
entry_code = '1084' AND 
entry_number = '1084' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008962' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008962' LIMIT 1)
WHERE
entry_code = '1072' AND 
entry_number = '1072' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008963' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008963' LIMIT 1)
WHERE
entry_code = '1073' AND 
entry_number = '1073' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008964' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008964' LIMIT 1)
WHERE
entry_code = '1074' AND 
entry_number = '1074' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008965' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008965' LIMIT 1)
WHERE
entry_code = '1075' AND 
entry_number = '1075' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008966' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008966' LIMIT 1)
WHERE
entry_code = '1076' AND 
entry_number = '1076' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008967' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008967' LIMIT 1)
WHERE
entry_code = '1077' AND 
entry_number = '1077' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008968' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008968' LIMIT 1)
WHERE
entry_code = '1078' AND 
entry_number = '1078' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008969' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008969' LIMIT 1)
WHERE
entry_code = '1079' AND 
entry_number = '1079' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008970' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008970' LIMIT 1)
WHERE
entry_code = '1080' AND 
entry_number = '1080' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008971' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008971' LIMIT 1)
WHERE
entry_code = '1081' AND 
entry_number = '1081' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008972' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008972' LIMIT 1)
WHERE
entry_code = '1082' AND 
entry_number = '1082' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008983' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008983' LIMIT 1)
WHERE
entry_code = '1093' AND 
entry_number = '1093' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008984' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008984' LIMIT 1)
WHERE
entry_code = '1094' AND 
entry_number = '1094' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008985' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008985' LIMIT 1)
WHERE
entry_code = '1095' AND 
entry_number = '1095' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008975' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008975' LIMIT 1)
WHERE
entry_code = '1085' AND 
entry_number = '1085' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008976' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008976' LIMIT 1)
WHERE
entry_code = '1086' AND 
entry_number = '1086' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008977' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008977' LIMIT 1)
WHERE
entry_code = '1087' AND 
entry_number = '1087' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008978' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008978' LIMIT 1)
WHERE
entry_code = '1088' AND 
entry_number = '1088' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008979' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008979' LIMIT 1)
WHERE
entry_code = '1089' AND 
entry_number = '1089' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008980' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008980' LIMIT 1)
WHERE
entry_code = '1090' AND 
entry_number = '1090' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008981' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008981' LIMIT 1)
WHERE
entry_code = '1091' AND 
entry_number = '1091' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008982' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008982' LIMIT 1)
WHERE
entry_code = '1092' AND 
entry_number = '1092' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009006' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009006' LIMIT 1)
WHERE
entry_code = '1116' AND 
entry_number = '1116' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009007' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009007' LIMIT 1)
WHERE
entry_code = '1117' AND 
entry_number = '1117' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009008' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009008' LIMIT 1)
WHERE
entry_code = '1118' AND 
entry_number = '1118' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009009' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009009' LIMIT 1)
WHERE
entry_code = '1119' AND 
entry_number = '1119' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009010' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009010' LIMIT 1)
WHERE
entry_code = '1120' AND 
entry_number = '1120' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009011' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009011' LIMIT 1)
WHERE
entry_code = '1121' AND 
entry_number = '1121' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008986' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008986' LIMIT 1)
WHERE
entry_code = '1096' AND 
entry_number = '1096' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008987' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008987' LIMIT 1)
WHERE
entry_code = '1097' AND 
entry_number = '1097' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008988' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008988' LIMIT 1)
WHERE
entry_code = '1098' AND 
entry_number = '1098' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008989' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008989' LIMIT 1)
WHERE
entry_code = '1099' AND 
entry_number = '1099' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008990' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008990' LIMIT 1)
WHERE
entry_code = '1100' AND 
entry_number = '1100' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008991' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008991' LIMIT 1)
WHERE
entry_code = '1101' AND 
entry_number = '1101' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008992' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008992' LIMIT 1)
WHERE
entry_code = '1102' AND 
entry_number = '1102' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008993' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008993' LIMIT 1)
WHERE
entry_code = '1103' AND 
entry_number = '1103' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008994' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008994' LIMIT 1)
WHERE
entry_code = '1104' AND 
entry_number = '1104' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008995' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008995' LIMIT 1)
WHERE
entry_code = '1105' AND 
entry_number = '1105' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008996' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008996' LIMIT 1)
WHERE
entry_code = '1106' AND 
entry_number = '1106' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008997' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008997' LIMIT 1)
WHERE
entry_code = '1107' AND 
entry_number = '1107' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008998' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008998' LIMIT 1)
WHERE
entry_code = '1108' AND 
entry_number = '1108' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008999' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000008999' LIMIT 1)
WHERE
entry_code = '1109' AND 
entry_number = '1109' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009000' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009000' LIMIT 1)
WHERE
entry_code = '1110' AND 
entry_number = '1110' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009001' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009001' LIMIT 1)
WHERE
entry_code = '1111' AND 
entry_number = '1111' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009002' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009002' LIMIT 1)
WHERE
entry_code = '1112' AND 
entry_number = '1112' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009003' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009003' LIMIT 1)
WHERE
entry_code = '1113' AND 
entry_number = '1113' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009004' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009004' LIMIT 1)
WHERE
entry_code = '1114' AND 
entry_number = '1114' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009005' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009005' LIMIT 1)
WHERE
entry_code = '1115' AND 
entry_number = '1115' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009046' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009046' LIMIT 1)
WHERE
entry_code = '1156' AND 
entry_number = '1156' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009012' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009012' LIMIT 1)
WHERE
entry_code = '1122' AND 
entry_number = '1122' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009013' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009013' LIMIT 1)
WHERE
entry_code = '1123' AND 
entry_number = '1123' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009014' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009014' LIMIT 1)
WHERE
entry_code = '1124' AND 
entry_number = '1124' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009015' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009015' LIMIT 1)
WHERE
entry_code = '1125' AND 
entry_number = '1125' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009016' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009016' LIMIT 1)
WHERE
entry_code = '1126' AND 
entry_number = '1126' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009017' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009017' LIMIT 1)
WHERE
entry_code = '1127' AND 
entry_number = '1127' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009018' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009018' LIMIT 1)
WHERE
entry_code = '1128' AND 
entry_number = '1128' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009019' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009019' LIMIT 1)
WHERE
entry_code = '1129' AND 
entry_number = '1129' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009020' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009020' LIMIT 1)
WHERE
entry_code = '1130' AND 
entry_number = '1130' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009021' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009021' LIMIT 1)
WHERE
entry_code = '1131' AND 
entry_number = '1131' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009022' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009022' LIMIT 1)
WHERE
entry_code = '1132' AND 
entry_number = '1132' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009023' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009023' LIMIT 1)
WHERE
entry_code = '1133' AND 
entry_number = '1133' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009024' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009024' LIMIT 1)
WHERE
entry_code = '1134' AND 
entry_number = '1134' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009025' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009025' LIMIT 1)
WHERE
entry_code = '1135' AND 
entry_number = '1135' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009026' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009026' LIMIT 1)
WHERE
entry_code = '1136' AND 
entry_number = '1136' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009027' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009027' LIMIT 1)
WHERE
entry_code = '1137' AND 
entry_number = '1137' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009028' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009028' LIMIT 1)
WHERE
entry_code = '1138' AND 
entry_number = '1138' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009029' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009029' LIMIT 1)
WHERE
entry_code = '1139' AND 
entry_number = '1139' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009030' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009030' LIMIT 1)
WHERE
entry_code = '1140' AND 
entry_number = '1140' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009031' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009031' LIMIT 1)
WHERE
entry_code = '1141' AND 
entry_number = '1141' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009032' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009032' LIMIT 1)
WHERE
entry_code = '1142' AND 
entry_number = '1142' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009033' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009033' LIMIT 1)
WHERE
entry_code = '1143' AND 
entry_number = '1143' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009034' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009034' LIMIT 1)
WHERE
entry_code = '1144' AND 
entry_number = '1144' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009035' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009035' LIMIT 1)
WHERE
entry_code = '1145' AND 
entry_number = '1145' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009036' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009036' LIMIT 1)
WHERE
entry_code = '1146' AND 
entry_number = '1146' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009037' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009037' LIMIT 1)
WHERE
entry_code = '1147' AND 
entry_number = '1147' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009038' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009038' LIMIT 1)
WHERE
entry_code = '1148' AND 
entry_number = '1148' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009039' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009039' LIMIT 1)
WHERE
entry_code = '1149' AND 
entry_number = '1149' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009040' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009040' LIMIT 1)
WHERE
entry_code = '1150' AND 
entry_number = '1150' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009041' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009041' LIMIT 1)
WHERE
entry_code = '1151' AND 
entry_number = '1151' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009042' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009042' LIMIT 1)
WHERE
entry_code = '1152' AND 
entry_number = '1152' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009043' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009043' LIMIT 1)
WHERE
entry_code = '1153' AND 
entry_number = '1153' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009044' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009044' LIMIT 1)
WHERE
entry_code = '1154' AND 
entry_number = '1154' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009045' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009045' LIMIT 1)
WHERE
entry_code = '1155' AND 
entry_number = '1155' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009087' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009087' LIMIT 1)
WHERE
entry_code = '1197' AND 
entry_number = '1197' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009047' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009047' LIMIT 1)
WHERE
entry_code = '1157' AND 
entry_number = '1157' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009048' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009048' LIMIT 1)
WHERE
entry_code = '1158' AND 
entry_number = '1158' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009049' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009049' LIMIT 1)
WHERE
entry_code = '1159' AND 
entry_number = '1159' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009050' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009050' LIMIT 1)
WHERE
entry_code = '1160' AND 
entry_number = '1160' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009051' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009051' LIMIT 1)
WHERE
entry_code = '1161' AND 
entry_number = '1161' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009052' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009052' LIMIT 1)
WHERE
entry_code = '1162' AND 
entry_number = '1162' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009053' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009053' LIMIT 1)
WHERE
entry_code = '1163' AND 
entry_number = '1163' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009054' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009054' LIMIT 1)
WHERE
entry_code = '1164' AND 
entry_number = '1164' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009055' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009055' LIMIT 1)
WHERE
entry_code = '1165' AND 
entry_number = '1165' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009056' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009056' LIMIT 1)
WHERE
entry_code = '1166' AND 
entry_number = '1166' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009057' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009057' LIMIT 1)
WHERE
entry_code = '1167' AND 
entry_number = '1167' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009058' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009058' LIMIT 1)
WHERE
entry_code = '1168' AND 
entry_number = '1168' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009059' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009059' LIMIT 1)
WHERE
entry_code = '1169' AND 
entry_number = '1169' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009060' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009060' LIMIT 1)
WHERE
entry_code = '1170' AND 
entry_number = '1170' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009061' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009061' LIMIT 1)
WHERE
entry_code = '1171' AND 
entry_number = '1171' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009062' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009062' LIMIT 1)
WHERE
entry_code = '1172' AND 
entry_number = '1172' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009063' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009063' LIMIT 1)
WHERE
entry_code = '1173' AND 
entry_number = '1173' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009064' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009064' LIMIT 1)
WHERE
entry_code = '1174' AND 
entry_number = '1174' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009065' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009065' LIMIT 1)
WHERE
entry_code = '1175' AND 
entry_number = '1175' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009066' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009066' LIMIT 1)
WHERE
entry_code = '1176' AND 
entry_number = '1176' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009067' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009067' LIMIT 1)
WHERE
entry_code = '1177' AND 
entry_number = '1177' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009068' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009068' LIMIT 1)
WHERE
entry_code = '1178' AND 
entry_number = '1178' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009069' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009069' LIMIT 1)
WHERE
entry_code = '1179' AND 
entry_number = '1179' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009070' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009070' LIMIT 1)
WHERE
entry_code = '1180' AND 
entry_number = '1180' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009071' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009071' LIMIT 1)
WHERE
entry_code = '1181' AND 
entry_number = '1181' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009072' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009072' LIMIT 1)
WHERE
entry_code = '1182' AND 
entry_number = '1182' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009073' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009073' LIMIT 1)
WHERE
entry_code = '1183' AND 
entry_number = '1183' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009074' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009074' LIMIT 1)
WHERE
entry_code = '1184' AND 
entry_number = '1184' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009075' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009075' LIMIT 1)
WHERE
entry_code = '1185' AND 
entry_number = '1185' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009076' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009076' LIMIT 1)
WHERE
entry_code = '1186' AND 
entry_number = '1186' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009077' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009077' LIMIT 1)
WHERE
entry_code = '1187' AND 
entry_number = '1187' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009078' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009078' LIMIT 1)
WHERE
entry_code = '1188' AND 
entry_number = '1188' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009079' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009079' LIMIT 1)
WHERE
entry_code = '1189' AND 
entry_number = '1189' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009080' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009080' LIMIT 1)
WHERE
entry_code = '1190' AND 
entry_number = '1190' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009081' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009081' LIMIT 1)
WHERE
entry_code = '1191' AND 
entry_number = '1191' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009082' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009082' LIMIT 1)
WHERE
entry_code = '1192' AND 
entry_number = '1192' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009083' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009083' LIMIT 1)
WHERE
entry_code = '1193' AND 
entry_number = '1193' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009084' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009084' LIMIT 1)
WHERE
entry_code = '1194' AND 
entry_number = '1194' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009085' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009085' LIMIT 1)
WHERE
entry_code = '1195' AND 
entry_number = '1195' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009086' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009086' LIMIT 1)
WHERE
entry_code = '1196' AND 
entry_number = '1196' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009155' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009155' LIMIT 1)
WHERE
entry_code = '1265' AND 
entry_number = '1265' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009156' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009156' LIMIT 1)
WHERE
entry_code = '1266' AND 
entry_number = '1266' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009088' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009088' LIMIT 1)
WHERE
entry_code = '1198' AND 
entry_number = '1198' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009089' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009089' LIMIT 1)
WHERE
entry_code = '1199' AND 
entry_number = '1199' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009090' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009090' LIMIT 1)
WHERE
entry_code = '1200' AND 
entry_number = '1200' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009091' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009091' LIMIT 1)
WHERE
entry_code = '1201' AND 
entry_number = '1201' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009092' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009092' LIMIT 1)
WHERE
entry_code = '1202' AND 
entry_number = '1202' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009093' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009093' LIMIT 1)
WHERE
entry_code = '1203' AND 
entry_number = '1203' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009094' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009094' LIMIT 1)
WHERE
entry_code = '1204' AND 
entry_number = '1204' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009095' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009095' LIMIT 1)
WHERE
entry_code = '1205' AND 
entry_number = '1205' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009096' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009096' LIMIT 1)
WHERE
entry_code = '1206' AND 
entry_number = '1206' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009097' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009097' LIMIT 1)
WHERE
entry_code = '1207' AND 
entry_number = '1207' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009098' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009098' LIMIT 1)
WHERE
entry_code = '1208' AND 
entry_number = '1208' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009099' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009099' LIMIT 1)
WHERE
entry_code = '1209' AND 
entry_number = '1209' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009100' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009100' LIMIT 1)
WHERE
entry_code = '1210' AND 
entry_number = '1210' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009101' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009101' LIMIT 1)
WHERE
entry_code = '1211' AND 
entry_number = '1211' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009102' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009102' LIMIT 1)
WHERE
entry_code = '1212' AND 
entry_number = '1212' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009103' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009103' LIMIT 1)
WHERE
entry_code = '1213' AND 
entry_number = '1213' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009104' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009104' LIMIT 1)
WHERE
entry_code = '1214' AND 
entry_number = '1214' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009105' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009105' LIMIT 1)
WHERE
entry_code = '1215' AND 
entry_number = '1215' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009106' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009106' LIMIT 1)
WHERE
entry_code = '1216' AND 
entry_number = '1216' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009107' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009107' LIMIT 1)
WHERE
entry_code = '1217' AND 
entry_number = '1217' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009108' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009108' LIMIT 1)
WHERE
entry_code = '1218' AND 
entry_number = '1218' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009109' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009109' LIMIT 1)
WHERE
entry_code = '1219' AND 
entry_number = '1219' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009110' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009110' LIMIT 1)
WHERE
entry_code = '1220' AND 
entry_number = '1220' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009111' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009111' LIMIT 1)
WHERE
entry_code = '1221' AND 
entry_number = '1221' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009112' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009112' LIMIT 1)
WHERE
entry_code = '1222' AND 
entry_number = '1222' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009113' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009113' LIMIT 1)
WHERE
entry_code = '1223' AND 
entry_number = '1223' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009114' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009114' LIMIT 1)
WHERE
entry_code = '1224' AND 
entry_number = '1224' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009115' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009115' LIMIT 1)
WHERE
entry_code = '1225' AND 
entry_number = '1225' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009116' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009116' LIMIT 1)
WHERE
entry_code = '1226' AND 
entry_number = '1226' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009117' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009117' LIMIT 1)
WHERE
entry_code = '1227' AND 
entry_number = '1227' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009118' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009118' LIMIT 1)
WHERE
entry_code = '1228' AND 
entry_number = '1228' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009119' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009119' LIMIT 1)
WHERE
entry_code = '1229' AND 
entry_number = '1229' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009120' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009120' LIMIT 1)
WHERE
entry_code = '1230' AND 
entry_number = '1230' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009121' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009121' LIMIT 1)
WHERE
entry_code = '1231' AND 
entry_number = '1231' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009122' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009122' LIMIT 1)
WHERE
entry_code = '1232' AND 
entry_number = '1232' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009123' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009123' LIMIT 1)
WHERE
entry_code = '1233' AND 
entry_number = '1233' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009124' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009124' LIMIT 1)
WHERE
entry_code = '1234' AND 
entry_number = '1234' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009125' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009125' LIMIT 1)
WHERE
entry_code = '1235' AND 
entry_number = '1235' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009126' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009126' LIMIT 1)
WHERE
entry_code = '1236' AND 
entry_number = '1236' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009127' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009127' LIMIT 1)
WHERE
entry_code = '1237' AND 
entry_number = '1237' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009128' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009128' LIMIT 1)
WHERE
entry_code = '1238' AND 
entry_number = '1238' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009129' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009129' LIMIT 1)
WHERE
entry_code = '1239' AND 
entry_number = '1239' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009130' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009130' LIMIT 1)
WHERE
entry_code = '1240' AND 
entry_number = '1240' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009131' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009131' LIMIT 1)
WHERE
entry_code = '1241' AND 
entry_number = '1241' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009132' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009132' LIMIT 1)
WHERE
entry_code = '1242' AND 
entry_number = '1242' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009133' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009133' LIMIT 1)
WHERE
entry_code = '1243' AND 
entry_number = '1243' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009134' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009134' LIMIT 1)
WHERE
entry_code = '1244' AND 
entry_number = '1244' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009135' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009135' LIMIT 1)
WHERE
entry_code = '1245' AND 
entry_number = '1245' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009136' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009136' LIMIT 1)
WHERE
entry_code = '1246' AND 
entry_number = '1246' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009137' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009137' LIMIT 1)
WHERE
entry_code = '1247' AND 
entry_number = '1247' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009138' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009138' LIMIT 1)
WHERE
entry_code = '1248' AND 
entry_number = '1248' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009139' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009139' LIMIT 1)
WHERE
entry_code = '1249' AND 
entry_number = '1249' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009140' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009140' LIMIT 1)
WHERE
entry_code = '1250' AND 
entry_number = '1250' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009141' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009141' LIMIT 1)
WHERE
entry_code = '1251' AND 
entry_number = '1251' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009142' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009142' LIMIT 1)
WHERE
entry_code = '1252' AND 
entry_number = '1252' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009143' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009143' LIMIT 1)
WHERE
entry_code = '1253' AND 
entry_number = '1253' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009144' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009144' LIMIT 1)
WHERE
entry_code = '1254' AND 
entry_number = '1254' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009145' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009145' LIMIT 1)
WHERE
entry_code = '1255' AND 
entry_number = '1255' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009146' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009146' LIMIT 1)
WHERE
entry_code = '1256' AND 
entry_number = '1256' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009147' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009147' LIMIT 1)
WHERE
entry_code = '1257' AND 
entry_number = '1257' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009148' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009148' LIMIT 1)
WHERE
entry_code = '1258' AND 
entry_number = '1258' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009149' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009149' LIMIT 1)
WHERE
entry_code = '1259' AND 
entry_number = '1259' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009150' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009150' LIMIT 1)
WHERE
entry_code = '1260' AND 
entry_number = '1260' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009151' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009151' LIMIT 1)
WHERE
entry_code = '1261' AND 
entry_number = '1261' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009152' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009152' LIMIT 1)
WHERE
entry_code = '1262' AND 
entry_number = '1262' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009153' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009153' LIMIT 1)
WHERE
entry_code = '1263' AND 
entry_number = '1263' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009154' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009154' LIMIT 1)
WHERE
entry_code = '1264' AND 
entry_number = '1264' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009181' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009181' LIMIT 1)
WHERE
entry_code = '1291' AND 
entry_number = '1291' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009182' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009182' LIMIT 1)
WHERE
entry_code = '1292' AND 
entry_number = '1292' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009183' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009183' LIMIT 1)
WHERE
entry_code = '1293' AND 
entry_number = '1293' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009157' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009157' LIMIT 1)
WHERE
entry_code = '1267' AND 
entry_number = '1267' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009158' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009158' LIMIT 1)
WHERE
entry_code = '1268' AND 
entry_number = '1268' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009159' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009159' LIMIT 1)
WHERE
entry_code = '1269' AND 
entry_number = '1269' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009160' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009160' LIMIT 1)
WHERE
entry_code = '1270' AND 
entry_number = '1270' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009161' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009161' LIMIT 1)
WHERE
entry_code = '1271' AND 
entry_number = '1271' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009162' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009162' LIMIT 1)
WHERE
entry_code = '1272' AND 
entry_number = '1272' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009163' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009163' LIMIT 1)
WHERE
entry_code = '1273' AND 
entry_number = '1273' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009164' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009164' LIMIT 1)
WHERE
entry_code = '1274' AND 
entry_number = '1274' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009165' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009165' LIMIT 1)
WHERE
entry_code = '1275' AND 
entry_number = '1275' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009166' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009166' LIMIT 1)
WHERE
entry_code = '1276' AND 
entry_number = '1276' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009167' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009167' LIMIT 1)
WHERE
entry_code = '1277' AND 
entry_number = '1277' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009168' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009168' LIMIT 1)
WHERE
entry_code = '1278' AND 
entry_number = '1278' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009169' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009169' LIMIT 1)
WHERE
entry_code = '1279' AND 
entry_number = '1279' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009170' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009170' LIMIT 1)
WHERE
entry_code = '1280' AND 
entry_number = '1280' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009171' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009171' LIMIT 1)
WHERE
entry_code = '1281' AND 
entry_number = '1281' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009172' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009172' LIMIT 1)
WHERE
entry_code = '1282' AND 
entry_number = '1282' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009173' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009173' LIMIT 1)
WHERE
entry_code = '1283' AND 
entry_number = '1283' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009174' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009174' LIMIT 1)
WHERE
entry_code = '1284' AND 
entry_number = '1284' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009175' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009175' LIMIT 1)
WHERE
entry_code = '1285' AND 
entry_number = '1285' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009176' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009176' LIMIT 1)
WHERE
entry_code = '1286' AND 
entry_number = '1286' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009177' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009177' LIMIT 1)
WHERE
entry_code = '1287' AND 
entry_number = '1287' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009178' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009178' LIMIT 1)
WHERE
entry_code = '1288' AND 
entry_number = '1288' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009179' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009179' LIMIT 1)
WHERE
entry_code = '1289' AND 
entry_number = '1289' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009180' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009180' LIMIT 1)
WHERE
entry_code = '1290' AND 
entry_number = '1290' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009220' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009220' LIMIT 1)
WHERE
entry_code = '1330' AND 
entry_number = '1330' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009221' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009221' LIMIT 1)
WHERE
entry_code = '1331' AND 
entry_number = '1331' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009222' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009222' LIMIT 1)
WHERE
entry_code = '1332' AND 
entry_number = '1332' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009223' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009223' LIMIT 1)
WHERE
entry_code = '1333' AND 
entry_number = '1333' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009184' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009184' LIMIT 1)
WHERE
entry_code = '1294' AND 
entry_number = '1294' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009185' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009185' LIMIT 1)
WHERE
entry_code = '1295' AND 
entry_number = '1295' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009186' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009186' LIMIT 1)
WHERE
entry_code = '1296' AND 
entry_number = '1296' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009187' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009187' LIMIT 1)
WHERE
entry_code = '1297' AND 
entry_number = '1297' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009188' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009188' LIMIT 1)
WHERE
entry_code = '1298' AND 
entry_number = '1298' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009189' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009189' LIMIT 1)
WHERE
entry_code = '1299' AND 
entry_number = '1299' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009190' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009190' LIMIT 1)
WHERE
entry_code = '1300' AND 
entry_number = '1300' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009191' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009191' LIMIT 1)
WHERE
entry_code = '1301' AND 
entry_number = '1301' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009192' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009192' LIMIT 1)
WHERE
entry_code = '1302' AND 
entry_number = '1302' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009193' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009193' LIMIT 1)
WHERE
entry_code = '1303' AND 
entry_number = '1303' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009194' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009194' LIMIT 1)
WHERE
entry_code = '1304' AND 
entry_number = '1304' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009195' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009195' LIMIT 1)
WHERE
entry_code = '1305' AND 
entry_number = '1305' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009196' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009196' LIMIT 1)
WHERE
entry_code = '1306' AND 
entry_number = '1306' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009197' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009197' LIMIT 1)
WHERE
entry_code = '1307' AND 
entry_number = '1307' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009198' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009198' LIMIT 1)
WHERE
entry_code = '1308' AND 
entry_number = '1308' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009199' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009199' LIMIT 1)
WHERE
entry_code = '1309' AND 
entry_number = '1309' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009200' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009200' LIMIT 1)
WHERE
entry_code = '1310' AND 
entry_number = '1310' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009201' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009201' LIMIT 1)
WHERE
entry_code = '1311' AND 
entry_number = '1311' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009202' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009202' LIMIT 1)
WHERE
entry_code = '1312' AND 
entry_number = '1312' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009203' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009203' LIMIT 1)
WHERE
entry_code = '1313' AND 
entry_number = '1313' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009204' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009204' LIMIT 1)
WHERE
entry_code = '1314' AND 
entry_number = '1314' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009205' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009205' LIMIT 1)
WHERE
entry_code = '1315' AND 
entry_number = '1315' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009206' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009206' LIMIT 1)
WHERE
entry_code = '1316' AND 
entry_number = '1316' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009207' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009207' LIMIT 1)
WHERE
entry_code = '1317' AND 
entry_number = '1317' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009208' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009208' LIMIT 1)
WHERE
entry_code = '1318' AND 
entry_number = '1318' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009209' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009209' LIMIT 1)
WHERE
entry_code = '1319' AND 
entry_number = '1319' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009210' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009210' LIMIT 1)
WHERE
entry_code = '1320' AND 
entry_number = '1320' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009211' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009211' LIMIT 1)
WHERE
entry_code = '1321' AND 
entry_number = '1321' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009212' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009212' LIMIT 1)
WHERE
entry_code = '1322' AND 
entry_number = '1322' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009213' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009213' LIMIT 1)
WHERE
entry_code = '1323' AND 
entry_number = '1323' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009214' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009214' LIMIT 1)
WHERE
entry_code = '1324' AND 
entry_number = '1324' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009215' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009215' LIMIT 1)
WHERE
entry_code = '1325' AND 
entry_number = '1325' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009216' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009216' LIMIT 1)
WHERE
entry_code = '1326' AND 
entry_number = '1326' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009217' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009217' LIMIT 1)
WHERE
entry_code = '1327' AND 
entry_number = '1327' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009218' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009218' LIMIT 1)
WHERE
entry_code = '1328' AND 
entry_number = '1328' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009219' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009219' LIMIT 1)
WHERE
entry_code = '1329' AND 
entry_number = '1329' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009287' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009287' LIMIT 1)
WHERE
entry_code = '1397' AND 
entry_number = '1397' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009288' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009288' LIMIT 1)
WHERE
entry_code = '1398' AND 
entry_number = '1398' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009289' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009289' LIMIT 1)
WHERE
entry_code = '1399' AND 
entry_number = '1399' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009290' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009290' LIMIT 1)
WHERE
entry_code = '1400' AND 
entry_number = '1400' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009292' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009292' LIMIT 1)
WHERE
entry_code = '1402' AND 
entry_number = '1402' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009224' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009224' LIMIT 1)
WHERE
entry_code = '1334' AND 
entry_number = '1334' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009225' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009225' LIMIT 1)
WHERE
entry_code = '1335' AND 
entry_number = '1335' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009226' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009226' LIMIT 1)
WHERE
entry_code = '1336' AND 
entry_number = '1336' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009227' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009227' LIMIT 1)
WHERE
entry_code = '1337' AND 
entry_number = '1337' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009228' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009228' LIMIT 1)
WHERE
entry_code = '1338' AND 
entry_number = '1338' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009229' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009229' LIMIT 1)
WHERE
entry_code = '1339' AND 
entry_number = '1339' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009230' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009230' LIMIT 1)
WHERE
entry_code = '1340' AND 
entry_number = '1340' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009231' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009231' LIMIT 1)
WHERE
entry_code = '1341' AND 
entry_number = '1341' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009232' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009232' LIMIT 1)
WHERE
entry_code = '1342' AND 
entry_number = '1342' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009233' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009233' LIMIT 1)
WHERE
entry_code = '1343' AND 
entry_number = '1343' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009234' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009234' LIMIT 1)
WHERE
entry_code = '1344' AND 
entry_number = '1344' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009235' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009235' LIMIT 1)
WHERE
entry_code = '1345' AND 
entry_number = '1345' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009236' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009236' LIMIT 1)
WHERE
entry_code = '1346' AND 
entry_number = '1346' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009237' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009237' LIMIT 1)
WHERE
entry_code = '1347' AND 
entry_number = '1347' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009238' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009238' LIMIT 1)
WHERE
entry_code = '1348' AND 
entry_number = '1348' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009239' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009239' LIMIT 1)
WHERE
entry_code = '1349' AND 
entry_number = '1349' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009240' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009240' LIMIT 1)
WHERE
entry_code = '1350' AND 
entry_number = '1350' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009241' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009241' LIMIT 1)
WHERE
entry_code = '1351' AND 
entry_number = '1351' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009242' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009242' LIMIT 1)
WHERE
entry_code = '1352' AND 
entry_number = '1352' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009243' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009243' LIMIT 1)
WHERE
entry_code = '1353' AND 
entry_number = '1353' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009244' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009244' LIMIT 1)
WHERE
entry_code = '1354' AND 
entry_number = '1354' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009245' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009245' LIMIT 1)
WHERE
entry_code = '1355' AND 
entry_number = '1355' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009246' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009246' LIMIT 1)
WHERE
entry_code = '1356' AND 
entry_number = '1356' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009247' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009247' LIMIT 1)
WHERE
entry_code = '1357' AND 
entry_number = '1357' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009248' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009248' LIMIT 1)
WHERE
entry_code = '1358' AND 
entry_number = '1358' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009249' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009249' LIMIT 1)
WHERE
entry_code = '1359' AND 
entry_number = '1359' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009250' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009250' LIMIT 1)
WHERE
entry_code = '1360' AND 
entry_number = '1360' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009251' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009251' LIMIT 1)
WHERE
entry_code = '1361' AND 
entry_number = '1361' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009252' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009252' LIMIT 1)
WHERE
entry_code = '1362' AND 
entry_number = '1362' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009253' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009253' LIMIT 1)
WHERE
entry_code = '1363' AND 
entry_number = '1363' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009254' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009254' LIMIT 1)
WHERE
entry_code = '1364' AND 
entry_number = '1364' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009255' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009255' LIMIT 1)
WHERE
entry_code = '1365' AND 
entry_number = '1365' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009256' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009256' LIMIT 1)
WHERE
entry_code = '1366' AND 
entry_number = '1366' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009257' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009257' LIMIT 1)
WHERE
entry_code = '1367' AND 
entry_number = '1367' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009258' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009258' LIMIT 1)
WHERE
entry_code = '1368' AND 
entry_number = '1368' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009259' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009259' LIMIT 1)
WHERE
entry_code = '1369' AND 
entry_number = '1369' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009260' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009260' LIMIT 1)
WHERE
entry_code = '1370' AND 
entry_number = '1370' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009261' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009261' LIMIT 1)
WHERE
entry_code = '1371' AND 
entry_number = '1371' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009262' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009262' LIMIT 1)
WHERE
entry_code = '1372' AND 
entry_number = '1372' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009263' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009263' LIMIT 1)
WHERE
entry_code = '1373' AND 
entry_number = '1373' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009264' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009264' LIMIT 1)
WHERE
entry_code = '1374' AND 
entry_number = '1374' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009265' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009265' LIMIT 1)
WHERE
entry_code = '1375' AND 
entry_number = '1375' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009266' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009266' LIMIT 1)
WHERE
entry_code = '1376' AND 
entry_number = '1376' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009267' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009267' LIMIT 1)
WHERE
entry_code = '1377' AND 
entry_number = '1377' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009268' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009268' LIMIT 1)
WHERE
entry_code = '1378' AND 
entry_number = '1378' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009269' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009269' LIMIT 1)
WHERE
entry_code = '1379' AND 
entry_number = '1379' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009270' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009270' LIMIT 1)
WHERE
entry_code = '1380' AND 
entry_number = '1380' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009271' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009271' LIMIT 1)
WHERE
entry_code = '1381' AND 
entry_number = '1381' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009272' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009272' LIMIT 1)
WHERE
entry_code = '1382' AND 
entry_number = '1382' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009273' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009273' LIMIT 1)
WHERE
entry_code = '1383' AND 
entry_number = '1383' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009274' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009274' LIMIT 1)
WHERE
entry_code = '1384' AND 
entry_number = '1384' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009275' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009275' LIMIT 1)
WHERE
entry_code = '1385' AND 
entry_number = '1385' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009276' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009276' LIMIT 1)
WHERE
entry_code = '1386' AND 
entry_number = '1386' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009277' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009277' LIMIT 1)
WHERE
entry_code = '1387' AND 
entry_number = '1387' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009278' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009278' LIMIT 1)
WHERE
entry_code = '1388' AND 
entry_number = '1388' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009279' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009279' LIMIT 1)
WHERE
entry_code = '1389' AND 
entry_number = '1389' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009280' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009280' LIMIT 1)
WHERE
entry_code = '1390' AND 
entry_number = '1390' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009281' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009281' LIMIT 1)
WHERE
entry_code = '1391' AND 
entry_number = '1391' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009282' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009282' LIMIT 1)
WHERE
entry_code = '1392' AND 
entry_number = '1392' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009283' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009283' LIMIT 1)
WHERE
entry_code = '1393' AND 
entry_number = '1393' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009284' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009284' LIMIT 1)
WHERE
entry_code = '1394' AND 
entry_number = '1394' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009285' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009285' LIMIT 1)
WHERE
entry_code = '1395' AND 
entry_number = '1395' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009286' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009286' LIMIT 1)
WHERE
entry_code = '1396' AND 
entry_number = '1396' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009291' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009291' LIMIT 1)
WHERE
entry_code = '1401' AND 
entry_number = '1401' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009293' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009293' LIMIT 1)
WHERE
entry_code = '1403' AND 
entry_number = '1403' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009294' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009294' LIMIT 1)
WHERE
entry_code = '1404' AND 
entry_number = '1404' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009296' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009296' LIMIT 1)
WHERE
entry_code = '1406' AND 
entry_number = '1406' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 

UPDATE experiment.entry
SET
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009295' LIMIT 1),
package_id = (SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000009295' LIMIT 1)
WHERE
entry_code = '1405' AND 
entry_number = '1405' AND 
entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-OYT-2021-DS-001' AND entry_list_status = 'completed' LIMIT 1); 



--rollback SELECT NULL;