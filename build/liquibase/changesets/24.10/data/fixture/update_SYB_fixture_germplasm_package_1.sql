--liquibase formatted sql

--changeset postgres:update_SYB_fixture_germplasm_package_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment:BDS-3516 Update Soybean fixture germplasm_package



UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008570',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008570' LIMIT 1)
WHERE
notes = 'PKG000000008570' AND 
package_label = 'SYB_fixed_0459_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009069',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009069' LIMIT 1)
WHERE
notes = 'PKG000000009069' AND 
package_label = 'SYB_fixed_0958_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009190',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009190' LIMIT 1)
WHERE
notes = 'PKG000000009190' AND 
package_label = 'SYB_fixed_1079_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008729',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008729' LIMIT 1)
WHERE
notes = 'PKG000000008729' AND 
package_label = 'SYB_fixed_0618_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008736',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008736' LIMIT 1)
WHERE
notes = 'PKG000000008736' AND 
package_label = 'SYB_fixed_0625_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009060',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009060' LIMIT 1)
WHERE
notes = 'PKG000000009060' AND 
package_label = 'SYB_fixed_0949_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008745',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008745' LIMIT 1)
WHERE
notes = 'PKG000000008745' AND 
package_label = 'SYB_fixed_0634_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008991',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008991' LIMIT 1)
WHERE
notes = 'PKG000000008991' AND 
package_label = 'SYB_fixed_0880_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009100',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009100' LIMIT 1)
WHERE
notes = 'PKG000000009100' AND 
package_label = 'SYB_fixed_0989_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008182',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008182' LIMIT 1)
WHERE
notes = 'PKG000000008182' AND 
package_label = 'SYB_fixed_0071_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008853',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008853' LIMIT 1)
WHERE
notes = 'PKG000000008853' AND 
package_label = 'SYB_fixed_0742_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008327',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008327' LIMIT 1)
WHERE
notes = 'PKG000000008327' AND 
package_label = 'SYB_fixed_0216_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008363',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008363' LIMIT 1)
WHERE
notes = 'PKG000000008363' AND 
package_label = 'SYB_fixed_0252_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008309',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008309' LIMIT 1)
WHERE
notes = 'PKG000000008309' AND 
package_label = 'SYB_fixed_0198_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008949',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008949' LIMIT 1)
WHERE
notes = 'PKG000000008949' AND 
package_label = 'SYB_fixed_0838_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008681',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008681' LIMIT 1)
WHERE
notes = 'PKG000000008681' AND 
package_label = 'SYB_fixed_0570_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009072',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009072' LIMIT 1)
WHERE
notes = 'PKG000000009072' AND 
package_label = 'SYB_fixed_0961_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008895',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008895' LIMIT 1)
WHERE
notes = 'PKG000000008895' AND 
package_label = 'SYB_fixed_0784_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009150',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009150' LIMIT 1)
WHERE
notes = 'PKG000000009150' AND 
package_label = 'SYB_fixed_1039_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008184',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008184' LIMIT 1)
WHERE
notes = 'PKG000000008184' AND 
package_label = 'SYB_fixed_0073_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008708',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008708' LIMIT 1)
WHERE
notes = 'PKG000000008708' AND 
package_label = 'SYB_fixed_0597_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007400',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007400' LIMIT 1)
WHERE
notes = 'PKG000000007400' AND 
package_label = 'SYB_fixed_0041_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008870',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008870' LIMIT 1)
WHERE
notes = 'PKG000000008870' AND 
package_label = 'SYB_fixed_0759_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008420',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008420' LIMIT 1)
WHERE
notes = 'PKG000000008420' AND 
package_label = 'SYB_fixed_0309_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008452',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008452' LIMIT 1)
WHERE
notes = 'PKG000000008452' AND 
package_label = 'SYB_fixed_0341_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009160',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009160' LIMIT 1)
WHERE
notes = 'PKG000000009160' AND 
package_label = 'SYB_fixed_1049_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008711',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008711' LIMIT 1)
WHERE
notes = 'PKG000000008711' AND 
package_label = 'SYB_fixed_0600_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008223',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008223' LIMIT 1)
WHERE
notes = 'PKG000000008223' AND 
package_label = 'SYB_fixed_0112_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008423',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008423' LIMIT 1)
WHERE
notes = 'PKG000000008423' AND 
package_label = 'SYB_fixed_0312_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008951',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008951' LIMIT 1)
WHERE
notes = 'PKG000000008951' AND 
package_label = 'SYB_fixed_0840_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008836',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008836' LIMIT 1)
WHERE
notes = 'PKG000000008836' AND 
package_label = 'SYB_fixed_0725_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008308',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008308' LIMIT 1)
WHERE
notes = 'PKG000000008308' AND 
package_label = 'SYB_fixed_0197_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008181',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008181' LIMIT 1)
WHERE
notes = 'PKG000000008181' AND 
package_label = 'SYB_fixed_0070_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007361',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007361' LIMIT 1)
WHERE
notes = 'PKG000000007361' AND 
package_label = 'SYB_fixed_0002_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009256',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009256' LIMIT 1)
WHERE
notes = 'PKG000000009256' AND 
package_label = 'SYB_fixed_1145_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008233',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008233' LIMIT 1)
WHERE
notes = 'PKG000000008233' AND 
package_label = 'SYB_fixed_0122_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009238',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009238' LIMIT 1)
WHERE
notes = 'PKG000000009238' AND 
package_label = 'SYB_fixed_1127_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008424',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008424' LIMIT 1)
WHERE
notes = 'PKG000000008424' AND 
package_label = 'SYB_fixed_0313_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009191',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009191' LIMIT 1)
WHERE
notes = 'PKG000000009191' AND 
package_label = 'SYB_fixed_1080_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008526',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008526' LIMIT 1)
WHERE
notes = 'PKG000000008526' AND 
package_label = 'SYB_fixed_0415_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008413',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008413' LIMIT 1)
WHERE
notes = 'PKG000000008413' AND 
package_label = 'SYB_fixed_0302_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008552',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008552' LIMIT 1)
WHERE
notes = 'PKG000000008552' AND 
package_label = 'SYB_fixed_0441_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009257',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009257' LIMIT 1)
WHERE
notes = 'PKG000000009257' AND 
package_label = 'SYB_fixed_1146_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008224',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008224' LIMIT 1)
WHERE
notes = 'PKG000000008224' AND 
package_label = 'SYB_fixed_0113_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008790',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008790' LIMIT 1)
WHERE
notes = 'PKG000000008790' AND 
package_label = 'SYB_fixed_0679_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008740',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008740' LIMIT 1)
WHERE
notes = 'PKG000000008740' AND 
package_label = 'SYB_fixed_0629_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008205',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008205' LIMIT 1)
WHERE
notes = 'PKG000000008205' AND 
package_label = 'SYB_fixed_0094_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007388',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007388' LIMIT 1)
WHERE
notes = 'PKG000000007388' AND 
package_label = 'SYB_fixed_0029_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008226',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008226' LIMIT 1)
WHERE
notes = 'PKG000000008226' AND 
package_label = 'SYB_fixed_0115_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008873',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008873' LIMIT 1)
WHERE
notes = 'PKG000000008873' AND 
package_label = 'SYB_fixed_0762_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008824',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008824' LIMIT 1)
WHERE
notes = 'PKG000000008824' AND 
package_label = 'SYB_fixed_0713_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008950',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008950' LIMIT 1)
WHERE
notes = 'PKG000000008950' AND 
package_label = 'SYB_fixed_0839_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008340',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008340' LIMIT 1)
WHERE
notes = 'PKG000000008340' AND 
package_label = 'SYB_fixed_0229_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008726',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008726' LIMIT 1)
WHERE
notes = 'PKG000000008726' AND 
package_label = 'SYB_fixed_0615_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008784',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008784' LIMIT 1)
WHERE
notes = 'PKG000000008784' AND 
package_label = 'SYB_fixed_0673_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009195',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009195' LIMIT 1)
WHERE
notes = 'PKG000000009195' AND 
package_label = 'SYB_fixed_1084_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008280',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008280' LIMIT 1)
WHERE
notes = 'PKG000000008280' AND 
package_label = 'SYB_fixed_0169_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008167',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008167' LIMIT 1)
WHERE
notes = 'PKG000000008167' AND 
package_label = 'SYB_fixed_0056_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008763',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008763' LIMIT 1)
WHERE
notes = 'PKG000000008763' AND 
package_label = 'SYB_fixed_0652_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008618',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008618' LIMIT 1)
WHERE
notes = 'PKG000000008618' AND 
package_label = 'SYB_fixed_0507_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008522',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008522' LIMIT 1)
WHERE
notes = 'PKG000000008522' AND 
package_label = 'SYB_fixed_0411_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008311',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008311' LIMIT 1)
WHERE
notes = 'PKG000000008311' AND 
package_label = 'SYB_fixed_0200_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008665',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008665' LIMIT 1)
WHERE
notes = 'PKG000000008665' AND 
package_label = 'SYB_fixed_0554_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008337',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008337' LIMIT 1)
WHERE
notes = 'PKG000000008337' AND 
package_label = 'SYB_fixed_0226_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008659',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008659' LIMIT 1)
WHERE
notes = 'PKG000000008659' AND 
package_label = 'SYB_fixed_0548_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008211',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008211' LIMIT 1)
WHERE
notes = 'PKG000000008211' AND 
package_label = 'SYB_fixed_0100_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008847',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008847' LIMIT 1)
WHERE
notes = 'PKG000000008847' AND 
package_label = 'SYB_fixed_0736_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008595',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008595' LIMIT 1)
WHERE
notes = 'PKG000000008595' AND 
package_label = 'SYB_fixed_0484_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009151',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009151' LIMIT 1)
WHERE
notes = 'PKG000000009151' AND 
package_label = 'SYB_fixed_1040_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008472',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008472' LIMIT 1)
WHERE
notes = 'PKG000000008472' AND 
package_label = 'SYB_fixed_0361_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008534',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008534' LIMIT 1)
WHERE
notes = 'PKG000000008534' AND 
package_label = 'SYB_fixed_0423_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008303',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008303' LIMIT 1)
WHERE
notes = 'PKG000000008303' AND 
package_label = 'SYB_fixed_0192_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008697',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008697' LIMIT 1)
WHERE
notes = 'PKG000000008697' AND 
package_label = 'SYB_fixed_0586_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009189',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009189' LIMIT 1)
WHERE
notes = 'PKG000000009189' AND 
package_label = 'SYB_fixed_1078_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008945',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008945' LIMIT 1)
WHERE
notes = 'PKG000000008945' AND 
package_label = 'SYB_fixed_0834_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008910',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008910' LIMIT 1)
WHERE
notes = 'PKG000000008910' AND 
package_label = 'SYB_fixed_0799_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009235',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009235' LIMIT 1)
WHERE
notes = 'PKG000000009235' AND 
package_label = 'SYB_fixed_1124_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007406',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007406' LIMIT 1)
WHERE
notes = 'PKG000000007406' AND 
package_label = 'SYB_fixed_0047_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008723',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008723' LIMIT 1)
WHERE
notes = 'PKG000000008723' AND 
package_label = 'SYB_fixed_0612_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008795',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008795' LIMIT 1)
WHERE
notes = 'PKG000000008795' AND 
package_label = 'SYB_fixed_0684_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008737',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008737' LIMIT 1)
WHERE
notes = 'PKG000000008737' AND 
package_label = 'SYB_fixed_0626_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008757',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008757' LIMIT 1)
WHERE
notes = 'PKG000000008757' AND 
package_label = 'SYB_fixed_0646_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008453',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008453' LIMIT 1)
WHERE
notes = 'PKG000000008453' AND 
package_label = 'SYB_fixed_0342_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008876',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008876' LIMIT 1)
WHERE
notes = 'PKG000000008876' AND 
package_label = 'SYB_fixed_0765_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009187',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009187' LIMIT 1)
WHERE
notes = 'PKG000000009187' AND 
package_label = 'SYB_fixed_1076_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008355',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008355' LIMIT 1)
WHERE
notes = 'PKG000000008355' AND 
package_label = 'SYB_fixed_0244_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008553',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008553' LIMIT 1)
WHERE
notes = 'PKG000000008553' AND 
package_label = 'SYB_fixed_0442_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009032',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009032' LIMIT 1)
WHERE
notes = 'PKG000000009032' AND 
package_label = 'SYB_fixed_0921_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008875',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008875' LIMIT 1)
WHERE
notes = 'PKG000000008875' AND 
package_label = 'SYB_fixed_0764_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008366',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008366' LIMIT 1)
WHERE
notes = 'PKG000000008366' AND 
package_label = 'SYB_fixed_0255_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008198',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008198' LIMIT 1)
WHERE
notes = 'PKG000000008198' AND 
package_label = 'SYB_fixed_0087_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008990',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008990' LIMIT 1)
WHERE
notes = 'PKG000000008990' AND 
package_label = 'SYB_fixed_0879_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008701',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008701' LIMIT 1)
WHERE
notes = 'PKG000000008701' AND 
package_label = 'SYB_fixed_0590_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009285',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009285' LIMIT 1)
WHERE
notes = 'PKG000000009285' AND 
package_label = 'SYB_fixed_1174_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008331',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008331' LIMIT 1)
WHERE
notes = 'PKG000000008331' AND 
package_label = 'SYB_fixed_0220_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008231',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008231' LIMIT 1)
WHERE
notes = 'PKG000000008231' AND 
package_label = 'SYB_fixed_0120_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008433',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008433' LIMIT 1)
WHERE
notes = 'PKG000000008433' AND 
package_label = 'SYB_fixed_0322_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008616',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008616' LIMIT 1)
WHERE
notes = 'PKG000000008616' AND 
package_label = 'SYB_fixed_0505_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008957',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008957' LIMIT 1)
WHERE
notes = 'PKG000000008957' AND 
package_label = 'SYB_fixed_0846_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008774',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008774' LIMIT 1)
WHERE
notes = 'PKG000000008774' AND 
package_label = 'SYB_fixed_0663_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008667',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008667' LIMIT 1)
WHERE
notes = 'PKG000000008667' AND 
package_label = 'SYB_fixed_0556_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009123',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009123' LIMIT 1)
WHERE
notes = 'PKG000000009123' AND 
package_label = 'SYB_fixed_1012_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008385',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008385' LIMIT 1)
WHERE
notes = 'PKG000000008385' AND 
package_label = 'SYB_fixed_0274_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009008',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009008' LIMIT 1)
WHERE
notes = 'PKG000000009008' AND 
package_label = 'SYB_fixed_0897_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009010',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009010' LIMIT 1)
WHERE
notes = 'PKG000000009010' AND 
package_label = 'SYB_fixed_0899_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008290',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008290' LIMIT 1)
WHERE
notes = 'PKG000000008290' AND 
package_label = 'SYB_fixed_0179_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009119',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009119' LIMIT 1)
WHERE
notes = 'PKG000000009119' AND 
package_label = 'SYB_fixed_1008_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009101',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009101' LIMIT 1)
WHERE
notes = 'PKG000000009101' AND 
package_label = 'SYB_fixed_0990_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009236',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009236' LIMIT 1)
WHERE
notes = 'PKG000000009236' AND 
package_label = 'SYB_fixed_1125_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008886',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008886' LIMIT 1)
WHERE
notes = 'PKG000000008886' AND 
package_label = 'SYB_fixed_0775_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008794',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008794' LIMIT 1)
WHERE
notes = 'PKG000000008794' AND 
package_label = 'SYB_fixed_0683_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009233',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009233' LIMIT 1)
WHERE
notes = 'PKG000000009233' AND 
package_label = 'SYB_fixed_1122_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009272',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009272' LIMIT 1)
WHERE
notes = 'PKG000000009272' AND 
package_label = 'SYB_fixed_1161_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008902',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008902' LIMIT 1)
WHERE
notes = 'PKG000000008902' AND 
package_label = 'SYB_fixed_0791_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008192',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008192' LIMIT 1)
WHERE
notes = 'PKG000000008192' AND 
package_label = 'SYB_fixed_0081_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008225',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008225' LIMIT 1)
WHERE
notes = 'PKG000000008225' AND 
package_label = 'SYB_fixed_0114_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008648',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008648' LIMIT 1)
WHERE
notes = 'PKG000000008648' AND 
package_label = 'SYB_fixed_0537_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008403',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008403' LIMIT 1)
WHERE
notes = 'PKG000000008403' AND 
package_label = 'SYB_fixed_0292_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008461',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008461' LIMIT 1)
WHERE
notes = 'PKG000000008461' AND 
package_label = 'SYB_fixed_0350_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009018',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009018' LIMIT 1)
WHERE
notes = 'PKG000000009018' AND 
package_label = 'SYB_fixed_0907_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008508',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008508' LIMIT 1)
WHERE
notes = 'PKG000000008508' AND 
package_label = 'SYB_fixed_0397_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008837',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008837' LIMIT 1)
WHERE
notes = 'PKG000000008837' AND 
package_label = 'SYB_fixed_0726_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008869',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008869' LIMIT 1)
WHERE
notes = 'PKG000000008869' AND 
package_label = 'SYB_fixed_0758_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009029',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009029' LIMIT 1)
WHERE
notes = 'PKG000000009029' AND 
package_label = 'SYB_fixed_0918_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008988',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008988' LIMIT 1)
WHERE
notes = 'PKG000000008988' AND 
package_label = 'SYB_fixed_0877_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009050',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009050' LIMIT 1)
WHERE
notes = 'PKG000000009050' AND 
package_label = 'SYB_fixed_0939_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008872',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008872' LIMIT 1)
WHERE
notes = 'PKG000000008872' AND 
package_label = 'SYB_fixed_0761_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008540',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008540' LIMIT 1)
WHERE
notes = 'PKG000000008540' AND 
package_label = 'SYB_fixed_0429_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008334',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008334' LIMIT 1)
WHERE
notes = 'PKG000000008334' AND 
package_label = 'SYB_fixed_0223_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008476',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008476' LIMIT 1)
WHERE
notes = 'PKG000000008476' AND 
package_label = 'SYB_fixed_0365_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009041',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009041' LIMIT 1)
WHERE
notes = 'PKG000000009041' AND 
package_label = 'SYB_fixed_0930_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008974',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008974' LIMIT 1)
WHERE
notes = 'PKG000000008974' AND 
package_label = 'SYB_fixed_0863_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008936',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008936' LIMIT 1)
WHERE
notes = 'PKG000000008936' AND 
package_label = 'SYB_fixed_0825_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008329',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008329' LIMIT 1)
WHERE
notes = 'PKG000000008329' AND 
package_label = 'SYB_fixed_0218_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008387',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008387' LIMIT 1)
WHERE
notes = 'PKG000000008387' AND 
package_label = 'SYB_fixed_0276_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009193',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009193' LIMIT 1)
WHERE
notes = 'PKG000000009193' AND 
package_label = 'SYB_fixed_1082_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008848',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008848' LIMIT 1)
WHERE
notes = 'PKG000000008848' AND 
package_label = 'SYB_fixed_0737_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009211',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009211' LIMIT 1)
WHERE
notes = 'PKG000000009211' AND 
package_label = 'SYB_fixed_1100_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008168',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008168' LIMIT 1)
WHERE
notes = 'PKG000000008168' AND 
package_label = 'SYB_fixed_0057_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008798',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008798' LIMIT 1)
WHERE
notes = 'PKG000000008798' AND 
package_label = 'SYB_fixed_0687_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008759',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008759' LIMIT 1)
WHERE
notes = 'PKG000000008759' AND 
package_label = 'SYB_fixed_0648_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008293',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008293' LIMIT 1)
WHERE
notes = 'PKG000000008293' AND 
package_label = 'SYB_fixed_0182_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008696',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008696' LIMIT 1)
WHERE
notes = 'PKG000000008696' AND 
package_label = 'SYB_fixed_0585_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008992',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008992' LIMIT 1)
WHERE
notes = 'PKG000000008992' AND 
package_label = 'SYB_fixed_0881_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007404',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007404' LIMIT 1)
WHERE
notes = 'PKG000000007404' AND 
package_label = 'SYB_fixed_0045_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009276',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009276' LIMIT 1)
WHERE
notes = 'PKG000000009276' AND 
package_label = 'SYB_fixed_1165_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008856',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008856' LIMIT 1)
WHERE
notes = 'PKG000000008856' AND 
package_label = 'SYB_fixed_0745_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008789',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008789' LIMIT 1)
WHERE
notes = 'PKG000000008789' AND 
package_label = 'SYB_fixed_0678_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008378',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008378' LIMIT 1)
WHERE
notes = 'PKG000000008378' AND 
package_label = 'SYB_fixed_0267_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008686',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008686' LIMIT 1)
WHERE
notes = 'PKG000000008686' AND 
package_label = 'SYB_fixed_0575_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008247',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008247' LIMIT 1)
WHERE
notes = 'PKG000000008247' AND 
package_label = 'SYB_fixed_0136_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008419',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008419' LIMIT 1)
WHERE
notes = 'PKG000000008419' AND 
package_label = 'SYB_fixed_0308_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008722',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008722' LIMIT 1)
WHERE
notes = 'PKG000000008722' AND 
package_label = 'SYB_fixed_0611_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008496',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008496' LIMIT 1)
WHERE
notes = 'PKG000000008496' AND 
package_label = 'SYB_fixed_0385_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009006',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009006' LIMIT 1)
WHERE
notes = 'PKG000000009006' AND 
package_label = 'SYB_fixed_0895_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008860',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008860' LIMIT 1)
WHERE
notes = 'PKG000000008860' AND 
package_label = 'SYB_fixed_0749_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008249',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008249' LIMIT 1)
WHERE
notes = 'PKG000000008249' AND 
package_label = 'SYB_fixed_0138_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008601',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008601' LIMIT 1)
WHERE
notes = 'PKG000000008601' AND 
package_label = 'SYB_fixed_0490_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009209',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009209' LIMIT 1)
WHERE
notes = 'PKG000000009209' AND 
package_label = 'SYB_fixed_1098_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008304',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008304' LIMIT 1)
WHERE
notes = 'PKG000000008304' AND 
package_label = 'SYB_fixed_0193_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007362',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007362' LIMIT 1)
WHERE
notes = 'PKG000000007362' AND 
package_label = 'SYB_fixed_0003_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008330',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008330' LIMIT 1)
WHERE
notes = 'PKG000000008330' AND 
package_label = 'SYB_fixed_0219_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007378',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007378' LIMIT 1)
WHERE
notes = 'PKG000000007378' AND 
package_label = 'SYB_fixed_0019_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008567',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008567' LIMIT 1)
WHERE
notes = 'PKG000000008567' AND 
package_label = 'SYB_fixed_0456_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009259',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009259' LIMIT 1)
WHERE
notes = 'PKG000000009259' AND 
package_label = 'SYB_fixed_1148_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009114',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009114' LIMIT 1)
WHERE
notes = 'PKG000000009114' AND 
package_label = 'SYB_fixed_1003_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008892',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008892' LIMIT 1)
WHERE
notes = 'PKG000000008892' AND 
package_label = 'SYB_fixed_0781_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009234',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009234' LIMIT 1)
WHERE
notes = 'PKG000000009234' AND 
package_label = 'SYB_fixed_1123_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008900',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008900' LIMIT 1)
WHERE
notes = 'PKG000000008900' AND 
package_label = 'SYB_fixed_0789_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008318',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008318' LIMIT 1)
WHERE
notes = 'PKG000000008318' AND 
package_label = 'SYB_fixed_0207_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008536',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008536' LIMIT 1)
WHERE
notes = 'PKG000000008536' AND 
package_label = 'SYB_fixed_0425_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008537',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008537' LIMIT 1)
WHERE
notes = 'PKG000000008537' AND 
package_label = 'SYB_fixed_0426_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008441',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008441' LIMIT 1)
WHERE
notes = 'PKG000000008441' AND 
package_label = 'SYB_fixed_0330_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009088',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009088' LIMIT 1)
WHERE
notes = 'PKG000000009088' AND 
package_label = 'SYB_fixed_0977_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008880',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008880' LIMIT 1)
WHERE
notes = 'PKG000000008880' AND 
package_label = 'SYB_fixed_0769_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009078',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009078' LIMIT 1)
WHERE
notes = 'PKG000000009078' AND 
package_label = 'SYB_fixed_0967_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008765',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008765' LIMIT 1)
WHERE
notes = 'PKG000000008765' AND 
package_label = 'SYB_fixed_0654_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008219',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008219' LIMIT 1)
WHERE
notes = 'PKG000000008219' AND 
package_label = 'SYB_fixed_0108_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008566',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008566' LIMIT 1)
WHERE
notes = 'PKG000000008566' AND 
package_label = 'SYB_fixed_0455_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008254',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008254' LIMIT 1)
WHERE
notes = 'PKG000000008254' AND 
package_label = 'SYB_fixed_0143_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008714',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008714' LIMIT 1)
WHERE
notes = 'PKG000000008714' AND 
package_label = 'SYB_fixed_0603_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008449',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008449' LIMIT 1)
WHERE
notes = 'PKG000000008449' AND 
package_label = 'SYB_fixed_0338_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009115',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009115' LIMIT 1)
WHERE
notes = 'PKG000000009115' AND 
package_label = 'SYB_fixed_1004_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008963',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008963' LIMIT 1)
WHERE
notes = 'PKG000000008963' AND 
package_label = 'SYB_fixed_0852_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009043',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009043' LIMIT 1)
WHERE
notes = 'PKG000000009043' AND 
package_label = 'SYB_fixed_0932_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008668',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008668' LIMIT 1)
WHERE
notes = 'PKG000000008668' AND 
package_label = 'SYB_fixed_0557_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008589',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008589' LIMIT 1)
WHERE
notes = 'PKG000000008589' AND 
package_label = 'SYB_fixed_0478_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008854',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008854' LIMIT 1)
WHERE
notes = 'PKG000000008854' AND 
package_label = 'SYB_fixed_0743_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008179',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008179' LIMIT 1)
WHERE
notes = 'PKG000000008179' AND 
package_label = 'SYB_fixed_0068_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008894',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008894' LIMIT 1)
WHERE
notes = 'PKG000000008894' AND 
package_label = 'SYB_fixed_0783_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008162',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008162' LIMIT 1)
WHERE
notes = 'PKG000000008162' AND 
package_label = 'SYB_fixed_0051_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008643',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008643' LIMIT 1)
WHERE
notes = 'PKG000000008643' AND 
package_label = 'SYB_fixed_0532_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008208',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008208' LIMIT 1)
WHERE
notes = 'PKG000000008208' AND 
package_label = 'SYB_fixed_0097_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008819',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008819' LIMIT 1)
WHERE
notes = 'PKG000000008819' AND 
package_label = 'SYB_fixed_0708_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009157',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009157' LIMIT 1)
WHERE
notes = 'PKG000000009157' AND 
package_label = 'SYB_fixed_1046_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008323',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008323' LIMIT 1)
WHERE
notes = 'PKG000000008323' AND 
package_label = 'SYB_fixed_0212_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008596',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008596' LIMIT 1)
WHERE
notes = 'PKG000000008596' AND 
package_label = 'SYB_fixed_0485_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008898',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008898' LIMIT 1)
WHERE
notes = 'PKG000000008898' AND 
package_label = 'SYB_fixed_0787_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008591',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008591' LIMIT 1)
WHERE
notes = 'PKG000000008591' AND 
package_label = 'SYB_fixed_0480_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009219',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009219' LIMIT 1)
WHERE
notes = 'PKG000000009219' AND 
package_label = 'SYB_fixed_1108_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008615',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008615' LIMIT 1)
WHERE
notes = 'PKG000000008615' AND 
package_label = 'SYB_fixed_0504_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009125',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009125' LIMIT 1)
WHERE
notes = 'PKG000000009125' AND 
package_label = 'SYB_fixed_1014_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009284',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009284' LIMIT 1)
WHERE
notes = 'PKG000000009284' AND 
package_label = 'SYB_fixed_1173_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008884',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008884' LIMIT 1)
WHERE
notes = 'PKG000000008884' AND 
package_label = 'SYB_fixed_0773_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008276',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008276' LIMIT 1)
WHERE
notes = 'PKG000000008276' AND 
package_label = 'SYB_fixed_0165_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008262',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008262' LIMIT 1)
WHERE
notes = 'PKG000000008262' AND 
package_label = 'SYB_fixed_0151_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009055',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009055' LIMIT 1)
WHERE
notes = 'PKG000000009055' AND 
package_label = 'SYB_fixed_0944_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007405',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007405' LIMIT 1)
WHERE
notes = 'PKG000000007405' AND 
package_label = 'SYB_fixed_0046_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008207',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008207' LIMIT 1)
WHERE
notes = 'PKG000000008207' AND 
package_label = 'SYB_fixed_0096_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009223',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009223' LIMIT 1)
WHERE
notes = 'PKG000000009223' AND 
package_label = 'SYB_fixed_1112_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008437',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008437' LIMIT 1)
WHERE
notes = 'PKG000000008437' AND 
package_label = 'SYB_fixed_0326_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008191',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008191' LIMIT 1)
WHERE
notes = 'PKG000000008191' AND 
package_label = 'SYB_fixed_0080_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008987',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008987' LIMIT 1)
WHERE
notes = 'PKG000000008987' AND 
package_label = 'SYB_fixed_0876_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008811',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008811' LIMIT 1)
WHERE
notes = 'PKG000000008811' AND 
package_label = 'SYB_fixed_0700_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008709',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008709' LIMIT 1)
WHERE
notes = 'PKG000000008709' AND 
package_label = 'SYB_fixed_0598_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008939',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008939' LIMIT 1)
WHERE
notes = 'PKG000000008939' AND 
package_label = 'SYB_fixed_0828_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008361',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008361' LIMIT 1)
WHERE
notes = 'PKG000000008361' AND 
package_label = 'SYB_fixed_0250_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008426',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008426' LIMIT 1)
WHERE
notes = 'PKG000000008426' AND 
package_label = 'SYB_fixed_0315_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009258',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009258' LIMIT 1)
WHERE
notes = 'PKG000000009258' AND 
package_label = 'SYB_fixed_1147_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008897',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008897' LIMIT 1)
WHERE
notes = 'PKG000000008897' AND 
package_label = 'SYB_fixed_0786_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008997',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008997' LIMIT 1)
WHERE
notes = 'PKG000000008997' AND 
package_label = 'SYB_fixed_0886_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008814',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008814' LIMIT 1)
WHERE
notes = 'PKG000000008814' AND 
package_label = 'SYB_fixed_0703_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008298',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008298' LIMIT 1)
WHERE
notes = 'PKG000000008298' AND 
package_label = 'SYB_fixed_0187_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009108',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009108' LIMIT 1)
WHERE
notes = 'PKG000000009108' AND 
package_label = 'SYB_fixed_0997_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008246',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008246' LIMIT 1)
WHERE
notes = 'PKG000000008246' AND 
package_label = 'SYB_fixed_0135_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008588',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008588' LIMIT 1)
WHERE
notes = 'PKG000000008588' AND 
package_label = 'SYB_fixed_0477_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008995',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008995' LIMIT 1)
WHERE
notes = 'PKG000000008995' AND 
package_label = 'SYB_fixed_0884_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009164',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009164' LIMIT 1)
WHERE
notes = 'PKG000000009164' AND 
package_label = 'SYB_fixed_1053_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008434',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008434' LIMIT 1)
WHERE
notes = 'PKG000000008434' AND 
package_label = 'SYB_fixed_0323_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008295',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008295' LIMIT 1)
WHERE
notes = 'PKG000000008295' AND 
package_label = 'SYB_fixed_0184_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008920',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008920' LIMIT 1)
WHERE
notes = 'PKG000000008920' AND 
package_label = 'SYB_fixed_0809_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009020',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009020' LIMIT 1)
WHERE
notes = 'PKG000000009020' AND 
package_label = 'SYB_fixed_0909_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007396',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007396' LIMIT 1)
WHERE
notes = 'PKG000000007396' AND 
package_label = 'SYB_fixed_0037_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008436',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008436' LIMIT 1)
WHERE
notes = 'PKG000000008436' AND 
package_label = 'SYB_fixed_0325_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009196',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009196' LIMIT 1)
WHERE
notes = 'PKG000000009196' AND 
package_label = 'SYB_fixed_1085_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008381',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008381' LIMIT 1)
WHERE
notes = 'PKG000000008381' AND 
package_label = 'SYB_fixed_0270_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008655',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008655' LIMIT 1)
WHERE
notes = 'PKG000000008655' AND 
package_label = 'SYB_fixed_0544_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008351',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008351' LIMIT 1)
WHERE
notes = 'PKG000000008351' AND 
package_label = 'SYB_fixed_0240_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008571',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008571' LIMIT 1)
WHERE
notes = 'PKG000000008571' AND 
package_label = 'SYB_fixed_0460_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008352',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008352' LIMIT 1)
WHERE
notes = 'PKG000000008352' AND 
package_label = 'SYB_fixed_0241_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008333',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008333' LIMIT 1)
WHERE
notes = 'PKG000000008333' AND 
package_label = 'SYB_fixed_0222_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009173',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009173' LIMIT 1)
WHERE
notes = 'PKG000000009173' AND 
package_label = 'SYB_fixed_1062_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009070',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009070' LIMIT 1)
WHERE
notes = 'PKG000000009070' AND 
package_label = 'SYB_fixed_0959_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008846',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008846' LIMIT 1)
WHERE
notes = 'PKG000000008846' AND 
package_label = 'SYB_fixed_0735_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008458',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008458' LIMIT 1)
WHERE
notes = 'PKG000000008458' AND 
package_label = 'SYB_fixed_0347_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008904',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008904' LIMIT 1)
WHERE
notes = 'PKG000000008904' AND 
package_label = 'SYB_fixed_0793_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009090',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009090' LIMIT 1)
WHERE
notes = 'PKG000000009090' AND 
package_label = 'SYB_fixed_0979_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008190',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008190' LIMIT 1)
WHERE
notes = 'PKG000000008190' AND 
package_label = 'SYB_fixed_0079_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008371',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008371' LIMIT 1)
WHERE
notes = 'PKG000000008371' AND 
package_label = 'SYB_fixed_0260_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008874',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008874' LIMIT 1)
WHERE
notes = 'PKG000000008874' AND 
package_label = 'SYB_fixed_0763_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009034',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009034' LIMIT 1)
WHERE
notes = 'PKG000000009034' AND 
package_label = 'SYB_fixed_0923_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008585',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008585' LIMIT 1)
WHERE
notes = 'PKG000000008585' AND 
package_label = 'SYB_fixed_0474_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008564',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008564' LIMIT 1)
WHERE
notes = 'PKG000000008564' AND 
package_label = 'SYB_fixed_0453_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009033',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009033' LIMIT 1)
WHERE
notes = 'PKG000000009033' AND 
package_label = 'SYB_fixed_0922_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008804',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008804' LIMIT 1)
WHERE
notes = 'PKG000000008804' AND 
package_label = 'SYB_fixed_0693_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008563',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008563' LIMIT 1)
WHERE
notes = 'PKG000000008563' AND 
package_label = 'SYB_fixed_0452_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008252',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008252' LIMIT 1)
WHERE
notes = 'PKG000000008252' AND 
package_label = 'SYB_fixed_0141_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008457',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008457' LIMIT 1)
WHERE
notes = 'PKG000000008457' AND 
package_label = 'SYB_fixed_0346_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008721',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008721' LIMIT 1)
WHERE
notes = 'PKG000000008721' AND 
package_label = 'SYB_fixed_0610_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008427',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008427' LIMIT 1)
WHERE
notes = 'PKG000000008427' AND 
package_label = 'SYB_fixed_0316_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008885',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008885' LIMIT 1)
WHERE
notes = 'PKG000000008885' AND 
package_label = 'SYB_fixed_0774_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008961',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008961' LIMIT 1)
WHERE
notes = 'PKG000000008961' AND 
package_label = 'SYB_fixed_0850_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008465',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008465' LIMIT 1)
WHERE
notes = 'PKG000000008465' AND 
package_label = 'SYB_fixed_0354_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008720',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008720' LIMIT 1)
WHERE
notes = 'PKG000000008720' AND 
package_label = 'SYB_fixed_0609_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008270',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008270' LIMIT 1)
WHERE
notes = 'PKG000000008270' AND 
package_label = 'SYB_fixed_0159_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008985',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008985' LIMIT 1)
WHERE
notes = 'PKG000000008985' AND 
package_label = 'SYB_fixed_0874_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009025',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009025' LIMIT 1)
WHERE
notes = 'PKG000000009025' AND 
package_label = 'SYB_fixed_0914_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008398',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008398' LIMIT 1)
WHERE
notes = 'PKG000000008398' AND 
package_label = 'SYB_fixed_0287_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008228',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008228' LIMIT 1)
WHERE
notes = 'PKG000000008228' AND 
package_label = 'SYB_fixed_0117_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009062',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009062' LIMIT 1)
WHERE
notes = 'PKG000000009062' AND 
package_label = 'SYB_fixed_0951_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008829',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008829' LIMIT 1)
WHERE
notes = 'PKG000000008829' AND 
package_label = 'SYB_fixed_0718_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009087',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009087' LIMIT 1)
WHERE
notes = 'PKG000000009087' AND 
package_label = 'SYB_fixed_0976_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008495',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008495' LIMIT 1)
WHERE
notes = 'PKG000000008495' AND 
package_label = 'SYB_fixed_0384_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008408',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008408' LIMIT 1)
WHERE
notes = 'PKG000000008408' AND 
package_label = 'SYB_fixed_0297_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007390',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007390' LIMIT 1)
WHERE
notes = 'PKG000000007390' AND 
package_label = 'SYB_fixed_0031_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008172',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008172' LIMIT 1)
WHERE
notes = 'PKG000000008172' AND 
package_label = 'SYB_fixed_0061_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008783',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008783' LIMIT 1)
WHERE
notes = 'PKG000000008783' AND 
package_label = 'SYB_fixed_0672_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008346',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008346' LIMIT 1)
WHERE
notes = 'PKG000000008346' AND 
package_label = 'SYB_fixed_0235_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008386',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008386' LIMIT 1)
WHERE
notes = 'PKG000000008386' AND 
package_label = 'SYB_fixed_0275_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009093',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009093' LIMIT 1)
WHERE
notes = 'PKG000000009093' AND 
package_label = 'SYB_fixed_0982_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009186',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009186' LIMIT 1)
WHERE
notes = 'PKG000000009186' AND 
package_label = 'SYB_fixed_1075_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008871',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008871' LIMIT 1)
WHERE
notes = 'PKG000000008871' AND 
package_label = 'SYB_fixed_0760_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008338',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008338' LIMIT 1)
WHERE
notes = 'PKG000000008338' AND 
package_label = 'SYB_fixed_0227_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008756',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008756' LIMIT 1)
WHERE
notes = 'PKG000000008756' AND 
package_label = 'SYB_fixed_0645_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009213',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009213' LIMIT 1)
WHERE
notes = 'PKG000000009213' AND 
package_label = 'SYB_fixed_1102_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008944',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008944' LIMIT 1)
WHERE
notes = 'PKG000000008944' AND 
package_label = 'SYB_fixed_0833_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008488',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008488' LIMIT 1)
WHERE
notes = 'PKG000000008488' AND 
package_label = 'SYB_fixed_0377_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008937',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008937' LIMIT 1)
WHERE
notes = 'PKG000000008937' AND 
package_label = 'SYB_fixed_0826_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008480',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008480' LIMIT 1)
WHERE
notes = 'PKG000000008480' AND 
package_label = 'SYB_fixed_0369_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008666',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008666' LIMIT 1)
WHERE
notes = 'PKG000000008666' AND 
package_label = 'SYB_fixed_0555_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009059',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009059' LIMIT 1)
WHERE
notes = 'PKG000000009059' AND 
package_label = 'SYB_fixed_0948_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008925',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008925' LIMIT 1)
WHERE
notes = 'PKG000000008925' AND 
package_label = 'SYB_fixed_0814_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009052',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009052' LIMIT 1)
WHERE
notes = 'PKG000000009052' AND 
package_label = 'SYB_fixed_0941_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008888',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008888' LIMIT 1)
WHERE
notes = 'PKG000000008888' AND 
package_label = 'SYB_fixed_0777_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008489',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008489' LIMIT 1)
WHERE
notes = 'PKG000000008489' AND 
package_label = 'SYB_fixed_0378_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008938',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008938' LIMIT 1)
WHERE
notes = 'PKG000000008938' AND 
package_label = 'SYB_fixed_0827_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008406',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008406' LIMIT 1)
WHERE
notes = 'PKG000000008406' AND 
package_label = 'SYB_fixed_0295_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008674',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008674' LIMIT 1)
WHERE
notes = 'PKG000000008674' AND 
package_label = 'SYB_fixed_0563_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009036',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009036' LIMIT 1)
WHERE
notes = 'PKG000000009036' AND 
package_label = 'SYB_fixed_0925_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009079',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009079' LIMIT 1)
WHERE
notes = 'PKG000000009079' AND 
package_label = 'SYB_fixed_0968_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008971',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008971' LIMIT 1)
WHERE
notes = 'PKG000000008971' AND 
package_label = 'SYB_fixed_0860_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007394',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007394' LIMIT 1)
WHERE
notes = 'PKG000000007394' AND 
package_label = 'SYB_fixed_0035_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008551',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008551' LIMIT 1)
WHERE
notes = 'PKG000000008551' AND 
package_label = 'SYB_fixed_0440_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009149',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009149' LIMIT 1)
WHERE
notes = 'PKG000000009149' AND 
package_label = 'SYB_fixed_1038_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008549',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008549' LIMIT 1)
WHERE
notes = 'PKG000000008549' AND 
package_label = 'SYB_fixed_0438_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008404',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008404' LIMIT 1)
WHERE
notes = 'PKG000000008404' AND 
package_label = 'SYB_fixed_0293_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009039',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009039' LIMIT 1)
WHERE
notes = 'PKG000000009039' AND 
package_label = 'SYB_fixed_0928_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008283',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008283' LIMIT 1)
WHERE
notes = 'PKG000000008283' AND 
package_label = 'SYB_fixed_0172_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009263',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009263' LIMIT 1)
WHERE
notes = 'PKG000000009263' AND 
package_label = 'SYB_fixed_1152_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008700',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008700' LIMIT 1)
WHERE
notes = 'PKG000000008700' AND 
package_label = 'SYB_fixed_0589_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008220',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008220' LIMIT 1)
WHERE
notes = 'PKG000000008220' AND 
package_label = 'SYB_fixed_0109_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008907',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008907' LIMIT 1)
WHERE
notes = 'PKG000000008907' AND 
package_label = 'SYB_fixed_0796_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009107',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009107' LIMIT 1)
WHERE
notes = 'PKG000000009107' AND 
package_label = 'SYB_fixed_0996_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008780',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008780' LIMIT 1)
WHERE
notes = 'PKG000000008780' AND 
package_label = 'SYB_fixed_0669_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008428',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008428' LIMIT 1)
WHERE
notes = 'PKG000000008428' AND 
package_label = 'SYB_fixed_0317_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009064',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009064' LIMIT 1)
WHERE
notes = 'PKG000000009064' AND 
package_label = 'SYB_fixed_0953_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008646',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008646' LIMIT 1)
WHERE
notes = 'PKG000000008646' AND 
package_label = 'SYB_fixed_0535_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008557',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008557' LIMIT 1)
WHERE
notes = 'PKG000000008557' AND 
package_label = 'SYB_fixed_0446_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008312',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008312' LIMIT 1)
WHERE
notes = 'PKG000000008312' AND 
package_label = 'SYB_fixed_0201_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009176',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009176' LIMIT 1)
WHERE
notes = 'PKG000000009176' AND 
package_label = 'SYB_fixed_1065_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008917',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008917' LIMIT 1)
WHERE
notes = 'PKG000000008917' AND 
package_label = 'SYB_fixed_0806_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008464',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008464' LIMIT 1)
WHERE
notes = 'PKG000000008464' AND 
package_label = 'SYB_fixed_0353_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008529',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008529' LIMIT 1)
WHERE
notes = 'PKG000000008529' AND 
package_label = 'SYB_fixed_0418_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008731',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008731' LIMIT 1)
WHERE
notes = 'PKG000000008731' AND 
package_label = 'SYB_fixed_0620_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008214',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008214' LIMIT 1)
WHERE
notes = 'PKG000000008214' AND 
package_label = 'SYB_fixed_0103_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008632',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008632' LIMIT 1)
WHERE
notes = 'PKG000000008632' AND 
package_label = 'SYB_fixed_0521_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008356',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008356' LIMIT 1)
WHERE
notes = 'PKG000000008356' AND 
package_label = 'SYB_fixed_0245_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008978',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008978' LIMIT 1)
WHERE
notes = 'PKG000000008978' AND 
package_label = 'SYB_fixed_0867_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009002',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009002' LIMIT 1)
WHERE
notes = 'PKG000000009002' AND 
package_label = 'SYB_fixed_0891_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008448',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008448' LIMIT 1)
WHERE
notes = 'PKG000000008448' AND 
package_label = 'SYB_fixed_0337_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008750',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008750' LIMIT 1)
WHERE
notes = 'PKG000000008750' AND 
package_label = 'SYB_fixed_0639_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008908',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008908' LIMIT 1)
WHERE
notes = 'PKG000000008908' AND 
package_label = 'SYB_fixed_0797_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008296',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008296' LIMIT 1)
WHERE
notes = 'PKG000000008296' AND 
package_label = 'SYB_fixed_0185_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008286',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008286' LIMIT 1)
WHERE
notes = 'PKG000000008286' AND 
package_label = 'SYB_fixed_0175_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008193',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008193' LIMIT 1)
WHERE
notes = 'PKG000000008193' AND 
package_label = 'SYB_fixed_0082_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008703',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008703' LIMIT 1)
WHERE
notes = 'PKG000000008703' AND 
package_label = 'SYB_fixed_0592_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007364',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007364' LIMIT 1)
WHERE
notes = 'PKG000000007364' AND 
package_label = 'SYB_fixed_0005_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009194',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009194' LIMIT 1)
WHERE
notes = 'PKG000000009194' AND 
package_label = 'SYB_fixed_1083_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007368',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007368' LIMIT 1)
WHERE
notes = 'PKG000000007368' AND 
package_label = 'SYB_fixed_0009_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008548',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008548' LIMIT 1)
WHERE
notes = 'PKG000000008548' AND 
package_label = 'SYB_fixed_0437_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008692',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008692' LIMIT 1)
WHERE
notes = 'PKG000000008692' AND 
package_label = 'SYB_fixed_0581_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009200',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009200' LIMIT 1)
WHERE
notes = 'PKG000000009200' AND 
package_label = 'SYB_fixed_1089_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009104',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009104' LIMIT 1)
WHERE
notes = 'PKG000000009104' AND 
package_label = 'SYB_fixed_0993_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008682',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008682' LIMIT 1)
WHERE
notes = 'PKG000000008682' AND 
package_label = 'SYB_fixed_0571_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008305',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008305' LIMIT 1)
WHERE
notes = 'PKG000000008305' AND 
package_label = 'SYB_fixed_0194_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008393',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008393' LIMIT 1)
WHERE
notes = 'PKG000000008393' AND 
package_label = 'SYB_fixed_0282_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008300',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008300' LIMIT 1)
WHERE
notes = 'PKG000000008300' AND 
package_label = 'SYB_fixed_0189_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009201',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009201' LIMIT 1)
WHERE
notes = 'PKG000000009201' AND 
package_label = 'SYB_fixed_1090_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008212',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008212' LIMIT 1)
WHERE
notes = 'PKG000000008212' AND 
package_label = 'SYB_fixed_0101_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008556',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008556' LIMIT 1)
WHERE
notes = 'PKG000000008556' AND 
package_label = 'SYB_fixed_0445_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008664',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008664' LIMIT 1)
WHERE
notes = 'PKG000000008664' AND 
package_label = 'SYB_fixed_0553_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009085',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009085' LIMIT 1)
WHERE
notes = 'PKG000000009085' AND 
package_label = 'SYB_fixed_0974_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008447',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008447' LIMIT 1)
WHERE
notes = 'PKG000000008447' AND 
package_label = 'SYB_fixed_0336_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008547',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008547' LIMIT 1)
WHERE
notes = 'PKG000000008547' AND 
package_label = 'SYB_fixed_0436_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009121',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009121' LIMIT 1)
WHERE
notes = 'PKG000000009121' AND 
package_label = 'SYB_fixed_1010_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009021',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009021' LIMIT 1)
WHERE
notes = 'PKG000000009021' AND 
package_label = 'SYB_fixed_0910_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008498',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008498' LIMIT 1)
WHERE
notes = 'PKG000000008498' AND 
package_label = 'SYB_fixed_0387_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008891',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008891' LIMIT 1)
WHERE
notes = 'PKG000000008891' AND 
package_label = 'SYB_fixed_0780_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008629',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008629' LIMIT 1)
WHERE
notes = 'PKG000000008629' AND 
package_label = 'SYB_fixed_0518_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009113',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009113' LIMIT 1)
WHERE
notes = 'PKG000000009113' AND 
package_label = 'SYB_fixed_1002_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008689',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008689' LIMIT 1)
WHERE
notes = 'PKG000000008689' AND 
package_label = 'SYB_fixed_0578_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009042',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009042' LIMIT 1)
WHERE
notes = 'PKG000000009042' AND 
package_label = 'SYB_fixed_0931_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008343',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008343' LIMIT 1)
WHERE
notes = 'PKG000000008343' AND 
package_label = 'SYB_fixed_0232_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008999',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008999' LIMIT 1)
WHERE
notes = 'PKG000000008999' AND 
package_label = 'SYB_fixed_0888_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008800',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008800' LIMIT 1)
WHERE
notes = 'PKG000000008800' AND 
package_label = 'SYB_fixed_0689_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008980',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008980' LIMIT 1)
WHERE
notes = 'PKG000000008980' AND 
package_label = 'SYB_fixed_0869_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008919',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008919' LIMIT 1)
WHERE
notes = 'PKG000000008919' AND 
package_label = 'SYB_fixed_0808_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009226',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009226' LIMIT 1)
WHERE
notes = 'PKG000000009226' AND 
package_label = 'SYB_fixed_1115_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008684',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008684' LIMIT 1)
WHERE
notes = 'PKG000000008684' AND 
package_label = 'SYB_fixed_0573_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008762',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008762' LIMIT 1)
WHERE
notes = 'PKG000000008762' AND 
package_label = 'SYB_fixed_0651_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008166',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008166' LIMIT 1)
WHERE
notes = 'PKG000000008166' AND 
package_label = 'SYB_fixed_0055_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009112',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009112' LIMIT 1)
WHERE
notes = 'PKG000000009112' AND 
package_label = 'SYB_fixed_1001_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009242',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009242' LIMIT 1)
WHERE
notes = 'PKG000000009242' AND 
package_label = 'SYB_fixed_1131_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009245',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009245' LIMIT 1)
WHERE
notes = 'PKG000000009245' AND 
package_label = 'SYB_fixed_1134_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008792',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008792' LIMIT 1)
WHERE
notes = 'PKG000000008792' AND 
package_label = 'SYB_fixed_0681_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008484',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008484' LIMIT 1)
WHERE
notes = 'PKG000000008484' AND 
package_label = 'SYB_fixed_0373_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008261',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008261' LIMIT 1)
WHERE
notes = 'PKG000000008261' AND 
package_label = 'SYB_fixed_0150_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008878',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008878' LIMIT 1)
WHERE
notes = 'PKG000000008878' AND 
package_label = 'SYB_fixed_0767_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008576',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008576' LIMIT 1)
WHERE
notes = 'PKG000000008576' AND 
package_label = 'SYB_fixed_0465_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008952',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008952' LIMIT 1)
WHERE
notes = 'PKG000000008952' AND 
package_label = 'SYB_fixed_0841_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009038',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009038' LIMIT 1)
WHERE
notes = 'PKG000000009038' AND 
package_label = 'SYB_fixed_0927_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008287',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008287' LIMIT 1)
WHERE
notes = 'PKG000000008287' AND 
package_label = 'SYB_fixed_0176_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008466',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008466' LIMIT 1)
WHERE
notes = 'PKG000000008466' AND 
package_label = 'SYB_fixed_0355_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008531',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008531' LIMIT 1)
WHERE
notes = 'PKG000000008531' AND 
package_label = 'SYB_fixed_0420_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008369',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008369' LIMIT 1)
WHERE
notes = 'PKG000000008369' AND 
package_label = 'SYB_fixed_0258_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009133',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009133' LIMIT 1)
WHERE
notes = 'PKG000000009133' AND 
package_label = 'SYB_fixed_1022_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009120',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009120' LIMIT 1)
WHERE
notes = 'PKG000000009120' AND 
package_label = 'SYB_fixed_1009_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009024',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009024' LIMIT 1)
WHERE
notes = 'PKG000000009024' AND 
package_label = 'SYB_fixed_0913_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009000',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009000' LIMIT 1)
WHERE
notes = 'PKG000000009000' AND 
package_label = 'SYB_fixed_0889_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008407',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008407' LIMIT 1)
WHERE
notes = 'PKG000000008407' AND 
package_label = 'SYB_fixed_0296_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008852',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008852' LIMIT 1)
WHERE
notes = 'PKG000000008852' AND 
package_label = 'SYB_fixed_0741_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008368',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008368' LIMIT 1)
WHERE
notes = 'PKG000000008368' AND 
package_label = 'SYB_fixed_0257_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008335',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008335' LIMIT 1)
WHERE
notes = 'PKG000000008335' AND 
package_label = 'SYB_fixed_0224_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009035',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009035' LIMIT 1)
WHERE
notes = 'PKG000000009035' AND 
package_label = 'SYB_fixed_0924_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008418',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008418' LIMIT 1)
WHERE
notes = 'PKG000000008418' AND 
package_label = 'SYB_fixed_0307_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008742',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008742' LIMIT 1)
WHERE
notes = 'PKG000000008742' AND 
package_label = 'SYB_fixed_0631_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007385',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007385' LIMIT 1)
WHERE
notes = 'PKG000000007385' AND 
package_label = 'SYB_fixed_0026_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008755',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008755' LIMIT 1)
WHERE
notes = 'PKG000000008755' AND 
package_label = 'SYB_fixed_0644_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008176',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008176' LIMIT 1)
WHERE
notes = 'PKG000000008176' AND 
package_label = 'SYB_fixed_0065_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008922',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008922' LIMIT 1)
WHERE
notes = 'PKG000000008922' AND 
package_label = 'SYB_fixed_0811_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008401',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008401' LIMIT 1)
WHERE
notes = 'PKG000000008401' AND 
package_label = 'SYB_fixed_0290_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008446',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008446' LIMIT 1)
WHERE
notes = 'PKG000000008446' AND 
package_label = 'SYB_fixed_0335_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008542',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008542' LIMIT 1)
WHERE
notes = 'PKG000000008542' AND 
package_label = 'SYB_fixed_0431_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008342',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008342' LIMIT 1)
WHERE
notes = 'PKG000000008342' AND 
package_label = 'SYB_fixed_0231_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008237',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008237' LIMIT 1)
WHERE
notes = 'PKG000000008237' AND 
package_label = 'SYB_fixed_0126_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008867',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008867' LIMIT 1)
WHERE
notes = 'PKG000000008867' AND 
package_label = 'SYB_fixed_0756_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007381',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007381' LIMIT 1)
WHERE
notes = 'PKG000000007381' AND 
package_label = 'SYB_fixed_0022_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008799',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008799' LIMIT 1)
WHERE
notes = 'PKG000000008799' AND 
package_label = 'SYB_fixed_0688_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008364',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008364' LIMIT 1)
WHERE
notes = 'PKG000000008364' AND 
package_label = 'SYB_fixed_0253_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008463',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008463' LIMIT 1)
WHERE
notes = 'PKG000000008463' AND 
package_label = 'SYB_fixed_0352_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008347',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008347' LIMIT 1)
WHERE
notes = 'PKG000000008347' AND 
package_label = 'SYB_fixed_0236_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008887',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008887' LIMIT 1)
WHERE
notes = 'PKG000000008887' AND 
package_label = 'SYB_fixed_0776_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008348',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008348' LIMIT 1)
WHERE
notes = 'PKG000000008348' AND 
package_label = 'SYB_fixed_0237_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008637',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008637' LIMIT 1)
WHERE
notes = 'PKG000000008637' AND 
package_label = 'SYB_fixed_0526_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008467',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008467' LIMIT 1)
WHERE
notes = 'PKG000000008467' AND 
package_label = 'SYB_fixed_0356_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008273',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008273' LIMIT 1)
WHERE
notes = 'PKG000000008273' AND 
package_label = 'SYB_fixed_0162_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008450',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008450' LIMIT 1)
WHERE
notes = 'PKG000000008450' AND 
package_label = 'SYB_fixed_0339_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009185',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009185' LIMIT 1)
WHERE
notes = 'PKG000000009185' AND 
package_label = 'SYB_fixed_1074_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007377',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007377' LIMIT 1)
WHERE
notes = 'PKG000000007377' AND 
package_label = 'SYB_fixed_0018_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008284',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008284' LIMIT 1)
WHERE
notes = 'PKG000000008284' AND 
package_label = 'SYB_fixed_0173_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008521',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008521' LIMIT 1)
WHERE
notes = 'PKG000000008521' AND 
package_label = 'SYB_fixed_0410_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008321',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008321' LIMIT 1)
WHERE
notes = 'PKG000000008321' AND 
package_label = 'SYB_fixed_0210_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009143',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009143' LIMIT 1)
WHERE
notes = 'PKG000000009143' AND 
package_label = 'SYB_fixed_1032_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009134',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009134' LIMIT 1)
WHERE
notes = 'PKG000000009134' AND 
package_label = 'SYB_fixed_1023_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008600',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008600' LIMIT 1)
WHERE
notes = 'PKG000000008600' AND 
package_label = 'SYB_fixed_0489_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008236',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008236' LIMIT 1)
WHERE
notes = 'PKG000000008236' AND 
package_label = 'SYB_fixed_0125_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007366',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007366' LIMIT 1)
WHERE
notes = 'PKG000000007366' AND 
package_label = 'SYB_fixed_0007_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007399',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007399' LIMIT 1)
WHERE
notes = 'PKG000000007399' AND 
package_label = 'SYB_fixed_0040_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008671',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008671' LIMIT 1)
WHERE
notes = 'PKG000000008671' AND 
package_label = 'SYB_fixed_0560_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009084',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009084' LIMIT 1)
WHERE
notes = 'PKG000000009084' AND 
package_label = 'SYB_fixed_0973_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009296',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009296' LIMIT 1)
WHERE
notes = 'PKG000000009296' AND 
package_label = 'SYB_fixed_1185_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008651',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008651' LIMIT 1)
WHERE
notes = 'PKG000000008651' AND 
package_label = 'SYB_fixed_0540_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007363',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007363' LIMIT 1)
WHERE
notes = 'PKG000000007363' AND 
package_label = 'SYB_fixed_0004_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008758',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008758' LIMIT 1)
WHERE
notes = 'PKG000000008758' AND 
package_label = 'SYB_fixed_0647_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008319',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008319' LIMIT 1)
WHERE
notes = 'PKG000000008319' AND 
package_label = 'SYB_fixed_0208_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009028',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009028' LIMIT 1)
WHERE
notes = 'PKG000000009028' AND 
package_label = 'SYB_fixed_0917_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008710',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008710' LIMIT 1)
WHERE
notes = 'PKG000000008710' AND 
package_label = 'SYB_fixed_0599_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008358',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008358' LIMIT 1)
WHERE
notes = 'PKG000000008358' AND 
package_label = 'SYB_fixed_0247_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008285',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008285' LIMIT 1)
WHERE
notes = 'PKG000000008285' AND 
package_label = 'SYB_fixed_0174_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008705',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008705' LIMIT 1)
WHERE
notes = 'PKG000000008705' AND 
package_label = 'SYB_fixed_0594_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008362',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008362' LIMIT 1)
WHERE
notes = 'PKG000000008362' AND 
package_label = 'SYB_fixed_0251_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008929',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008929' LIMIT 1)
WHERE
notes = 'PKG000000008929' AND 
package_label = 'SYB_fixed_0818_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008928',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008928' LIMIT 1)
WHERE
notes = 'PKG000000008928' AND 
package_label = 'SYB_fixed_0817_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008541',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008541' LIMIT 1)
WHERE
notes = 'PKG000000008541' AND 
package_label = 'SYB_fixed_0430_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008279',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008279' LIMIT 1)
WHERE
notes = 'PKG000000008279' AND 
package_label = 'SYB_fixed_0168_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007374',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007374' LIMIT 1)
WHERE
notes = 'PKG000000007374' AND 
package_label = 'SYB_fixed_0015_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008611',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008611' LIMIT 1)
WHERE
notes = 'PKG000000008611' AND 
package_label = 'SYB_fixed_0500_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009141',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009141' LIMIT 1)
WHERE
notes = 'PKG000000009141' AND 
package_label = 'SYB_fixed_1030_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009094',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009094' LIMIT 1)
WHERE
notes = 'PKG000000009094' AND 
package_label = 'SYB_fixed_0983_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008912',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008912' LIMIT 1)
WHERE
notes = 'PKG000000008912' AND 
package_label = 'SYB_fixed_0801_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008791',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008791' LIMIT 1)
WHERE
notes = 'PKG000000008791' AND 
package_label = 'SYB_fixed_0680_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009154',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009154' LIMIT 1)
WHERE
notes = 'PKG000000009154' AND 
package_label = 'SYB_fixed_1043_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008478',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008478' LIMIT 1)
WHERE
notes = 'PKG000000008478' AND 
package_label = 'SYB_fixed_0367_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008621',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008621' LIMIT 1)
WHERE
notes = 'PKG000000008621' AND 
package_label = 'SYB_fixed_0510_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008555',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008555' LIMIT 1)
WHERE
notes = 'PKG000000008555' AND 
package_label = 'SYB_fixed_0444_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008735',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008735' LIMIT 1)
WHERE
notes = 'PKG000000008735' AND 
package_label = 'SYB_fixed_0624_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008515',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008515' LIMIT 1)
WHERE
notes = 'PKG000000008515' AND 
package_label = 'SYB_fixed_0404_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009014',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009014' LIMIT 1)
WHERE
notes = 'PKG000000009014' AND 
package_label = 'SYB_fixed_0903_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009102',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009102' LIMIT 1)
WHERE
notes = 'PKG000000009102' AND 
package_label = 'SYB_fixed_0991_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009268',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009268' LIMIT 1)
WHERE
notes = 'PKG000000009268' AND 
package_label = 'SYB_fixed_1157_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008766',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008766' LIMIT 1)
WHERE
notes = 'PKG000000008766' AND 
package_label = 'SYB_fixed_0655_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007389',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007389' LIMIT 1)
WHERE
notes = 'PKG000000007389' AND 
package_label = 'SYB_fixed_0030_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008825',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008825' LIMIT 1)
WHERE
notes = 'PKG000000008825' AND 
package_label = 'SYB_fixed_0714_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008562',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008562' LIMIT 1)
WHERE
notes = 'PKG000000008562' AND 
package_label = 'SYB_fixed_0451_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009215',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009215' LIMIT 1)
WHERE
notes = 'PKG000000009215' AND 
package_label = 'SYB_fixed_1104_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009156',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009156' LIMIT 1)
WHERE
notes = 'PKG000000009156' AND 
package_label = 'SYB_fixed_1045_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008316',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008316' LIMIT 1)
WHERE
notes = 'PKG000000008316' AND 
package_label = 'SYB_fixed_0205_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008793',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008793' LIMIT 1)
WHERE
notes = 'PKG000000008793' AND 
package_label = 'SYB_fixed_0682_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008336',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008336' LIMIT 1)
WHERE
notes = 'PKG000000008336' AND 
package_label = 'SYB_fixed_0225_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008388',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008388' LIMIT 1)
WHERE
notes = 'PKG000000008388' AND 
package_label = 'SYB_fixed_0277_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008623',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008623' LIMIT 1)
WHERE
notes = 'PKG000000008623' AND 
package_label = 'SYB_fixed_0512_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008609',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008609' LIMIT 1)
WHERE
notes = 'PKG000000008609' AND 
package_label = 'SYB_fixed_0498_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008275',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008275' LIMIT 1)
WHERE
notes = 'PKG000000008275' AND 
package_label = 'SYB_fixed_0164_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008468',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008468' LIMIT 1)
WHERE
notes = 'PKG000000008468' AND 
package_label = 'SYB_fixed_0357_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008390',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008390' LIMIT 1)
WHERE
notes = 'PKG000000008390' AND 
package_label = 'SYB_fixed_0279_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008180',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008180' LIMIT 1)
WHERE
notes = 'PKG000000008180' AND 
package_label = 'SYB_fixed_0069_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008899',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008899' LIMIT 1)
WHERE
notes = 'PKG000000008899' AND 
package_label = 'SYB_fixed_0788_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009109',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009109' LIMIT 1)
WHERE
notes = 'PKG000000009109' AND 
package_label = 'SYB_fixed_0998_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008909',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008909' LIMIT 1)
WHERE
notes = 'PKG000000008909' AND 
package_label = 'SYB_fixed_0798_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009277',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009277' LIMIT 1)
WHERE
notes = 'PKG000000009277' AND 
package_label = 'SYB_fixed_1166_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008864',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008864' LIMIT 1)
WHERE
notes = 'PKG000000008864' AND 
package_label = 'SYB_fixed_0753_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008947',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008947' LIMIT 1)
WHERE
notes = 'PKG000000008947' AND 
package_label = 'SYB_fixed_0836_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008979',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008979' LIMIT 1)
WHERE
notes = 'PKG000000008979' AND 
package_label = 'SYB_fixed_0868_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008265',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008265' LIMIT 1)
WHERE
notes = 'PKG000000008265' AND 
package_label = 'SYB_fixed_0154_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009048',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009048' LIMIT 1)
WHERE
notes = 'PKG000000009048' AND 
package_label = 'SYB_fixed_0937_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008903',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008903' LIMIT 1)
WHERE
notes = 'PKG000000008903' AND 
package_label = 'SYB_fixed_0792_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008786',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008786' LIMIT 1)
WHERE
notes = 'PKG000000008786' AND 
package_label = 'SYB_fixed_0675_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008243',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008243' LIMIT 1)
WHERE
notes = 'PKG000000008243' AND 
package_label = 'SYB_fixed_0132_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008326',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008326' LIMIT 1)
WHERE
notes = 'PKG000000008326' AND 
package_label = 'SYB_fixed_0215_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008821',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008821' LIMIT 1)
WHERE
notes = 'PKG000000008821' AND 
package_label = 'SYB_fixed_0710_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008597',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008597' LIMIT 1)
WHERE
notes = 'PKG000000008597' AND 
package_label = 'SYB_fixed_0486_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008266',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008266' LIMIT 1)
WHERE
notes = 'PKG000000008266' AND 
package_label = 'SYB_fixed_0155_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008787',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008787' LIMIT 1)
WHERE
notes = 'PKG000000008787' AND 
package_label = 'SYB_fixed_0676_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008688',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008688' LIMIT 1)
WHERE
notes = 'PKG000000008688' AND 
package_label = 'SYB_fixed_0577_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008257',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008257' LIMIT 1)
WHERE
notes = 'PKG000000008257' AND 
package_label = 'SYB_fixed_0146_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008317',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008317' LIMIT 1)
WHERE
notes = 'PKG000000008317' AND 
package_label = 'SYB_fixed_0206_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008374',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008374' LIMIT 1)
WHERE
notes = 'PKG000000008374' AND 
package_label = 'SYB_fixed_0263_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008654',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008654' LIMIT 1)
WHERE
notes = 'PKG000000008654' AND 
package_label = 'SYB_fixed_0543_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008550',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008550' LIMIT 1)
WHERE
notes = 'PKG000000008550' AND 
package_label = 'SYB_fixed_0439_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009080',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009080' LIMIT 1)
WHERE
notes = 'PKG000000009080' AND 
package_label = 'SYB_fixed_0969_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008695',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008695' LIMIT 1)
WHERE
notes = 'PKG000000008695' AND 
package_label = 'SYB_fixed_0584_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008850',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008850' LIMIT 1)
WHERE
notes = 'PKG000000008850' AND 
package_label = 'SYB_fixed_0739_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009203',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009203' LIMIT 1)
WHERE
notes = 'PKG000000009203' AND 
package_label = 'SYB_fixed_1092_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008232',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008232' LIMIT 1)
WHERE
notes = 'PKG000000008232' AND 
package_label = 'SYB_fixed_0121_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008445',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008445' LIMIT 1)
WHERE
notes = 'PKG000000008445' AND 
package_label = 'SYB_fixed_0334_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008292',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008292' LIMIT 1)
WHERE
notes = 'PKG000000008292' AND 
package_label = 'SYB_fixed_0181_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008587',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008587' LIMIT 1)
WHERE
notes = 'PKG000000008587' AND 
package_label = 'SYB_fixed_0476_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008429',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008429' LIMIT 1)
WHERE
notes = 'PKG000000008429' AND 
package_label = 'SYB_fixed_0318_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009188',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009188' LIMIT 1)
WHERE
notes = 'PKG000000009188' AND 
package_label = 'SYB_fixed_1077_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008322',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008322' LIMIT 1)
WHERE
notes = 'PKG000000008322' AND 
package_label = 'SYB_fixed_0211_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008676',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008676' LIMIT 1)
WHERE
notes = 'PKG000000008676' AND 
package_label = 'SYB_fixed_0565_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008679',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008679' LIMIT 1)
WHERE
notes = 'PKG000000008679' AND 
package_label = 'SYB_fixed_0568_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008514',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008514' LIMIT 1)
WHERE
notes = 'PKG000000008514' AND 
package_label = 'SYB_fixed_0403_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008580',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008580' LIMIT 1)
WHERE
notes = 'PKG000000008580' AND 
package_label = 'SYB_fixed_0469_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009158',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009158' LIMIT 1)
WHERE
notes = 'PKG000000009158' AND 
package_label = 'SYB_fixed_1047_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009110',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009110' LIMIT 1)
WHERE
notes = 'PKG000000009110' AND 
package_label = 'SYB_fixed_0999_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008400',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008400' LIMIT 1)
WHERE
notes = 'PKG000000008400' AND 
package_label = 'SYB_fixed_0289_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009267',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009267' LIMIT 1)
WHERE
notes = 'PKG000000009267' AND 
package_label = 'SYB_fixed_1156_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009172',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009172' LIMIT 1)
WHERE
notes = 'PKG000000009172' AND 
package_label = 'SYB_fixed_1061_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009270',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009270' LIMIT 1)
WHERE
notes = 'PKG000000009270' AND 
package_label = 'SYB_fixed_1159_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007392',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007392' LIMIT 1)
WHERE
notes = 'PKG000000007392' AND 
package_label = 'SYB_fixed_0033_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008779',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008779' LIMIT 1)
WHERE
notes = 'PKG000000008779' AND 
package_label = 'SYB_fixed_0668_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008953',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008953' LIMIT 1)
WHERE
notes = 'PKG000000008953' AND 
package_label = 'SYB_fixed_0842_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008503',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008503' LIMIT 1)
WHERE
notes = 'PKG000000008503' AND 
package_label = 'SYB_fixed_0392_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008185',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008185' LIMIT 1)
WHERE
notes = 'PKG000000008185' AND 
package_label = 'SYB_fixed_0074_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009210',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009210' LIMIT 1)
WHERE
notes = 'PKG000000009210' AND 
package_label = 'SYB_fixed_1099_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008935',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008935' LIMIT 1)
WHERE
notes = 'PKG000000008935' AND 
package_label = 'SYB_fixed_0824_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008964',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008964' LIMIT 1)
WHERE
notes = 'PKG000000008964' AND 
package_label = 'SYB_fixed_0853_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008227',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008227' LIMIT 1)
WHERE
notes = 'PKG000000008227' AND 
package_label = 'SYB_fixed_0116_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008410',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008410' LIMIT 1)
WHERE
notes = 'PKG000000008410' AND 
package_label = 'SYB_fixed_0299_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008209',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008209' LIMIT 1)
WHERE
notes = 'PKG000000008209' AND 
package_label = 'SYB_fixed_0098_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008288',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008288' LIMIT 1)
WHERE
notes = 'PKG000000008288' AND 
package_label = 'SYB_fixed_0177_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008582',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008582' LIMIT 1)
WHERE
notes = 'PKG000000008582' AND 
package_label = 'SYB_fixed_0471_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008834',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008834' LIMIT 1)
WHERE
notes = 'PKG000000008834' AND 
package_label = 'SYB_fixed_0723_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009287',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009287' LIMIT 1)
WHERE
notes = 'PKG000000009287' AND 
package_label = 'SYB_fixed_1176_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007397',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007397' LIMIT 1)
WHERE
notes = 'PKG000000007397' AND 
package_label = 'SYB_fixed_0038_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008569',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008569' LIMIT 1)
WHERE
notes = 'PKG000000008569' AND 
package_label = 'SYB_fixed_0458_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009099',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009099' LIMIT 1)
WHERE
notes = 'PKG000000009099' AND 
package_label = 'SYB_fixed_0988_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008970',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008970' LIMIT 1)
WHERE
notes = 'PKG000000008970' AND 
package_label = 'SYB_fixed_0859_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008660',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008660' LIMIT 1)
WHERE
notes = 'PKG000000008660' AND 
package_label = 'SYB_fixed_0549_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008197',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008197' LIMIT 1)
WHERE
notes = 'PKG000000008197' AND 
package_label = 'SYB_fixed_0086_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008635',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008635' LIMIT 1)
WHERE
notes = 'PKG000000008635' AND 
package_label = 'SYB_fixed_0524_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008598',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008598' LIMIT 1)
WHERE
notes = 'PKG000000008598' AND 
package_label = 'SYB_fixed_0487_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007409',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007409' LIMIT 1)
WHERE
notes = 'PKG000000007409' AND 
package_label = 'SYB_fixed_0050_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008890',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008890' LIMIT 1)
WHERE
notes = 'PKG000000008890' AND 
package_label = 'SYB_fixed_0779_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009282',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009282' LIMIT 1)
WHERE
notes = 'PKG000000009282' AND 
package_label = 'SYB_fixed_1171_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009131',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009131' LIMIT 1)
WHERE
notes = 'PKG000000009131' AND 
package_label = 'SYB_fixed_1020_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009138',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009138' LIMIT 1)
WHERE
notes = 'PKG000000009138' AND 
package_label = 'SYB_fixed_1027_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008773',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008773' LIMIT 1)
WHERE
notes = 'PKG000000008773' AND 
package_label = 'SYB_fixed_0662_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009289',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009289' LIMIT 1)
WHERE
notes = 'PKG000000009289' AND 
package_label = 'SYB_fixed_1178_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008578',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008578' LIMIT 1)
WHERE
notes = 'PKG000000008578' AND 
package_label = 'SYB_fixed_0467_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008178',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008178' LIMIT 1)
WHERE
notes = 'PKG000000008178' AND 
package_label = 'SYB_fixed_0067_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009224',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009224' LIMIT 1)
WHERE
notes = 'PKG000000009224' AND 
package_label = 'SYB_fixed_1113_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009132',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009132' LIMIT 1)
WHERE
notes = 'PKG000000009132' AND 
package_label = 'SYB_fixed_1021_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008165',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008165' LIMIT 1)
WHERE
notes = 'PKG000000008165' AND 
package_label = 'SYB_fixed_0054_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008816',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008816' LIMIT 1)
WHERE
notes = 'PKG000000008816' AND 
package_label = 'SYB_fixed_0705_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008986',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008986' LIMIT 1)
WHERE
notes = 'PKG000000008986' AND 
package_label = 'SYB_fixed_0875_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008715',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008715' LIMIT 1)
WHERE
notes = 'PKG000000008715' AND 
package_label = 'SYB_fixed_0604_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009281',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009281' LIMIT 1)
WHERE
notes = 'PKG000000009281' AND 
package_label = 'SYB_fixed_1170_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008734',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008734' LIMIT 1)
WHERE
notes = 'PKG000000008734' AND 
package_label = 'SYB_fixed_0623_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008359',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008359' LIMIT 1)
WHERE
notes = 'PKG000000008359' AND 
package_label = 'SYB_fixed_0248_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008345',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008345' LIMIT 1)
WHERE
notes = 'PKG000000008345' AND 
package_label = 'SYB_fixed_0234_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008802',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008802' LIMIT 1)
WHERE
notes = 'PKG000000008802' AND 
package_label = 'SYB_fixed_0691_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008628',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008628' LIMIT 1)
WHERE
notes = 'PKG000000008628' AND 
package_label = 'SYB_fixed_0517_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008538',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008538' LIMIT 1)
WHERE
notes = 'PKG000000008538' AND 
package_label = 'SYB_fixed_0427_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008513',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008513' LIMIT 1)
WHERE
notes = 'PKG000000008513' AND 
package_label = 'SYB_fixed_0402_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008738',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008738' LIMIT 1)
WHERE
notes = 'PKG000000008738' AND 
package_label = 'SYB_fixed_0627_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008394',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008394' LIMIT 1)
WHERE
notes = 'PKG000000008394' AND 
package_label = 'SYB_fixed_0283_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009212',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009212' LIMIT 1)
WHERE
notes = 'PKG000000009212' AND 
package_label = 'SYB_fixed_1101_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008968',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008968' LIMIT 1)
WHERE
notes = 'PKG000000008968' AND 
package_label = 'SYB_fixed_0857_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009155',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009155' LIMIT 1)
WHERE
notes = 'PKG000000009155' AND 
package_label = 'SYB_fixed_1044_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009056',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009056' LIMIT 1)
WHERE
notes = 'PKG000000009056' AND 
package_label = 'SYB_fixed_0945_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008584',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008584' LIMIT 1)
WHERE
notes = 'PKG000000008584' AND 
package_label = 'SYB_fixed_0473_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009202',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009202' LIMIT 1)
WHERE
notes = 'PKG000000009202' AND 
package_label = 'SYB_fixed_1091_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008289',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008289' LIMIT 1)
WHERE
notes = 'PKG000000008289' AND 
package_label = 'SYB_fixed_0178_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009005',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009005' LIMIT 1)
WHERE
notes = 'PKG000000009005' AND 
package_label = 'SYB_fixed_0894_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008879',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008879' LIMIT 1)
WHERE
notes = 'PKG000000008879' AND 
package_label = 'SYB_fixed_0768_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009136',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009136' LIMIT 1)
WHERE
notes = 'PKG000000009136' AND 
package_label = 'SYB_fixed_1025_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008946',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008946' LIMIT 1)
WHERE
notes = 'PKG000000008946' AND 
package_label = 'SYB_fixed_0835_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009037',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009037' LIMIT 1)
WHERE
notes = 'PKG000000009037' AND 
package_label = 'SYB_fixed_0926_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008603',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008603' LIMIT 1)
WHERE
notes = 'PKG000000008603' AND 
package_label = 'SYB_fixed_0492_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008658',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008658' LIMIT 1)
WHERE
notes = 'PKG000000008658' AND 
package_label = 'SYB_fixed_0547_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008377',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008377' LIMIT 1)
WHERE
notes = 'PKG000000008377' AND 
package_label = 'SYB_fixed_0266_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008905',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008905' LIMIT 1)
WHERE
notes = 'PKG000000008905' AND 
package_label = 'SYB_fixed_0794_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009249',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009249' LIMIT 1)
WHERE
notes = 'PKG000000009249' AND 
package_label = 'SYB_fixed_1138_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009117',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009117' LIMIT 1)
WHERE
notes = 'PKG000000009117' AND 
package_label = 'SYB_fixed_1006_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008216',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008216' LIMIT 1)
WHERE
notes = 'PKG000000008216' AND 
package_label = 'SYB_fixed_0105_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008661',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008661' LIMIT 1)
WHERE
notes = 'PKG000000008661' AND 
package_label = 'SYB_fixed_0550_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008590',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008590' LIMIT 1)
WHERE
notes = 'PKG000000008590' AND 
package_label = 'SYB_fixed_0479_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008593',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008593' LIMIT 1)
WHERE
notes = 'PKG000000008593' AND 
package_label = 'SYB_fixed_0482_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008502',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008502' LIMIT 1)
WHERE
notes = 'PKG000000008502' AND 
package_label = 'SYB_fixed_0391_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008545',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008545' LIMIT 1)
WHERE
notes = 'PKG000000008545' AND 
package_label = 'SYB_fixed_0434_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008360',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008360' LIMIT 1)
WHERE
notes = 'PKG000000008360' AND 
package_label = 'SYB_fixed_0249_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008561',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008561' LIMIT 1)
WHERE
notes = 'PKG000000008561' AND 
package_label = 'SYB_fixed_0450_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008678',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008678' LIMIT 1)
WHERE
notes = 'PKG000000008678' AND 
package_label = 'SYB_fixed_0567_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009135',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009135' LIMIT 1)
WHERE
notes = 'PKG000000009135' AND 
package_label = 'SYB_fixed_1024_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008268',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008268' LIMIT 1)
WHERE
notes = 'PKG000000008268' AND 
package_label = 'SYB_fixed_0157_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008719',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008719' LIMIT 1)
WHERE
notes = 'PKG000000008719' AND 
package_label = 'SYB_fixed_0608_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008574',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008574' LIMIT 1)
WHERE
notes = 'PKG000000008574' AND 
package_label = 'SYB_fixed_0463_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009073',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009073' LIMIT 1)
WHERE
notes = 'PKG000000009073' AND 
package_label = 'SYB_fixed_0962_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008245',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008245' LIMIT 1)
WHERE
notes = 'PKG000000008245' AND 
package_label = 'SYB_fixed_0134_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008575',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008575' LIMIT 1)
WHERE
notes = 'PKG000000008575' AND 
package_label = 'SYB_fixed_0464_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008250',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008250' LIMIT 1)
WHERE
notes = 'PKG000000008250' AND 
package_label = 'SYB_fixed_0139_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009089',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009089' LIMIT 1)
WHERE
notes = 'PKG000000009089' AND 
package_label = 'SYB_fixed_0978_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008956',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008956' LIMIT 1)
WHERE
notes = 'PKG000000008956' AND 
package_label = 'SYB_fixed_0845_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009126',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009126' LIMIT 1)
WHERE
notes = 'PKG000000009126' AND 
package_label = 'SYB_fixed_1015_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008500',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008500' LIMIT 1)
WHERE
notes = 'PKG000000008500' AND 
package_label = 'SYB_fixed_0389_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008747',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008747' LIMIT 1)
WHERE
notes = 'PKG000000008747' AND 
package_label = 'SYB_fixed_0636_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009040',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009040' LIMIT 1)
WHERE
notes = 'PKG000000009040' AND 
package_label = 'SYB_fixed_0929_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008641',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008641' LIMIT 1)
WHERE
notes = 'PKG000000008641' AND 
package_label = 'SYB_fixed_0530_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008392',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008392' LIMIT 1)
WHERE
notes = 'PKG000000008392' AND 
package_label = 'SYB_fixed_0281_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008962',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008962' LIMIT 1)
WHERE
notes = 'PKG000000008962' AND 
package_label = 'SYB_fixed_0851_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009083',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009083' LIMIT 1)
WHERE
notes = 'PKG000000009083' AND 
package_label = 'SYB_fixed_0972_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009019',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009019' LIMIT 1)
WHERE
notes = 'PKG000000009019' AND 
package_label = 'SYB_fixed_0908_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007383',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007383' LIMIT 1)
WHERE
notes = 'PKG000000007383' AND 
package_label = 'SYB_fixed_0024_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008998',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008998' LIMIT 1)
WHERE
notes = 'PKG000000008998' AND 
package_label = 'SYB_fixed_0887_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008649',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008649' LIMIT 1)
WHERE
notes = 'PKG000000008649' AND 
package_label = 'SYB_fixed_0538_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008213',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008213' LIMIT 1)
WHERE
notes = 'PKG000000008213' AND 
package_label = 'SYB_fixed_0102_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009057',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009057' LIMIT 1)
WHERE
notes = 'PKG000000009057' AND 
package_label = 'SYB_fixed_0946_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008924',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008924' LIMIT 1)
WHERE
notes = 'PKG000000008924' AND 
package_label = 'SYB_fixed_0813_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008256',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008256' LIMIT 1)
WHERE
notes = 'PKG000000008256' AND 
package_label = 'SYB_fixed_0145_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008803',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008803' LIMIT 1)
WHERE
notes = 'PKG000000008803' AND 
package_label = 'SYB_fixed_0692_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008866',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008866' LIMIT 1)
WHERE
notes = 'PKG000000008866' AND 
package_label = 'SYB_fixed_0755_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009091',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009091' LIMIT 1)
WHERE
notes = 'PKG000000009091' AND 
package_label = 'SYB_fixed_0980_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008862',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008862' LIMIT 1)
WHERE
notes = 'PKG000000008862' AND 
package_label = 'SYB_fixed_0751_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008797',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008797' LIMIT 1)
WHERE
notes = 'PKG000000008797' AND 
package_label = 'SYB_fixed_0686_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008577',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008577' LIMIT 1)
WHERE
notes = 'PKG000000008577' AND 
package_label = 'SYB_fixed_0466_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008380',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008380' LIMIT 1)
WHERE
notes = 'PKG000000008380' AND 
package_label = 'SYB_fixed_0269_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009250',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009250' LIMIT 1)
WHERE
notes = 'PKG000000009250' AND 
package_label = 'SYB_fixed_1139_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008272',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008272' LIMIT 1)
WHERE
notes = 'PKG000000008272' AND 
package_label = 'SYB_fixed_0161_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008921',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008921' LIMIT 1)
WHERE
notes = 'PKG000000008921' AND 
package_label = 'SYB_fixed_0810_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008644',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008644' LIMIT 1)
WHERE
notes = 'PKG000000008644' AND 
package_label = 'SYB_fixed_0533_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008301',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008301' LIMIT 1)
WHERE
notes = 'PKG000000008301' AND 
package_label = 'SYB_fixed_0190_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008781',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008781' LIMIT 1)
WHERE
notes = 'PKG000000008781' AND 
package_label = 'SYB_fixed_0670_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008828',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008828' LIMIT 1)
WHERE
notes = 'PKG000000008828' AND 
package_label = 'SYB_fixed_0717_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008948',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008948' LIMIT 1)
WHERE
notes = 'PKG000000008948' AND 
package_label = 'SYB_fixed_0837_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009183',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009183' LIMIT 1)
WHERE
notes = 'PKG000000009183' AND 
package_label = 'SYB_fixed_1072_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009290',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009290' LIMIT 1)
WHERE
notes = 'PKG000000009290' AND 
package_label = 'SYB_fixed_1179_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008817',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008817' LIMIT 1)
WHERE
notes = 'PKG000000008817' AND 
package_label = 'SYB_fixed_0706_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008194',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008194' LIMIT 1)
WHERE
notes = 'PKG000000008194' AND 
package_label = 'SYB_fixed_0083_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008727',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008727' LIMIT 1)
WHERE
notes = 'PKG000000008727' AND 
package_label = 'SYB_fixed_0616_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008730',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008730' LIMIT 1)
WHERE
notes = 'PKG000000008730' AND 
package_label = 'SYB_fixed_0619_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008229',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008229' LIMIT 1)
WHERE
notes = 'PKG000000008229' AND 
package_label = 'SYB_fixed_0118_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008916',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008916' LIMIT 1)
WHERE
notes = 'PKG000000008916' AND 
package_label = 'SYB_fixed_0805_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008517',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008517' LIMIT 1)
WHERE
notes = 'PKG000000008517' AND 
package_label = 'SYB_fixed_0406_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008713',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008713' LIMIT 1)
WHERE
notes = 'PKG000000008713' AND 
package_label = 'SYB_fixed_0602_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008239',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008239' LIMIT 1)
WHERE
notes = 'PKG000000008239' AND 
package_label = 'SYB_fixed_0128_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008777',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008777' LIMIT 1)
WHERE
notes = 'PKG000000008777' AND 
package_label = 'SYB_fixed_0666_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008444',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008444' LIMIT 1)
WHERE
notes = 'PKG000000008444' AND 
package_label = 'SYB_fixed_0333_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008435',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008435' LIMIT 1)
WHERE
notes = 'PKG000000008435' AND 
package_label = 'SYB_fixed_0324_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008675',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008675' LIMIT 1)
WHERE
notes = 'PKG000000008675' AND 
package_label = 'SYB_fixed_0564_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008494',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008494' LIMIT 1)
WHERE
notes = 'PKG000000008494' AND 
package_label = 'SYB_fixed_0383_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008324',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008324' LIMIT 1)
WHERE
notes = 'PKG000000008324' AND 
package_label = 'SYB_fixed_0213_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009105',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009105' LIMIT 1)
WHERE
notes = 'PKG000000009105' AND 
package_label = 'SYB_fixed_0994_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009227',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009227' LIMIT 1)
WHERE
notes = 'PKG000000009227' AND 
package_label = 'SYB_fixed_1116_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008493',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008493' LIMIT 1)
WHERE
notes = 'PKG000000008493' AND 
package_label = 'SYB_fixed_0382_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009053',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009053' LIMIT 1)
WHERE
notes = 'PKG000000009053' AND 
package_label = 'SYB_fixed_0942_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009017',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009017' LIMIT 1)
WHERE
notes = 'PKG000000009017' AND 
package_label = 'SYB_fixed_0906_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008960',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008960' LIMIT 1)
WHERE
notes = 'PKG000000008960' AND 
package_label = 'SYB_fixed_0849_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008647',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008647' LIMIT 1)
WHERE
notes = 'PKG000000008647' AND 
package_label = 'SYB_fixed_0536_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008926',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008926' LIMIT 1)
WHERE
notes = 'PKG000000008926' AND 
package_label = 'SYB_fixed_0815_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009291',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009291' LIMIT 1)
WHERE
notes = 'PKG000000009291' AND 
package_label = 'SYB_fixed_1180_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009058',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009058' LIMIT 1)
WHERE
notes = 'PKG000000009058' AND 
package_label = 'SYB_fixed_0947_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008302',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008302' LIMIT 1)
WHERE
notes = 'PKG000000008302' AND 
package_label = 'SYB_fixed_0191_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008638',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008638' LIMIT 1)
WHERE
notes = 'PKG000000008638' AND 
package_label = 'SYB_fixed_0527_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009246',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009246' LIMIT 1)
WHERE
notes = 'PKG000000009246' AND 
package_label = 'SYB_fixed_1135_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008823',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008823' LIMIT 1)
WHERE
notes = 'PKG000000008823' AND 
package_label = 'SYB_fixed_0712_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008812',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008812' LIMIT 1)
WHERE
notes = 'PKG000000008812' AND 
package_label = 'SYB_fixed_0701_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008206',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008206' LIMIT 1)
WHERE
notes = 'PKG000000008206' AND 
package_label = 'SYB_fixed_0095_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008395',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008395' LIMIT 1)
WHERE
notes = 'PKG000000008395' AND 
package_label = 'SYB_fixed_0284_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009144',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009144' LIMIT 1)
WHERE
notes = 'PKG000000009144' AND 
package_label = 'SYB_fixed_1033_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008687',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008687' LIMIT 1)
WHERE
notes = 'PKG000000008687' AND 
package_label = 'SYB_fixed_0576_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008565',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008565' LIMIT 1)
WHERE
notes = 'PKG000000008565' AND 
package_label = 'SYB_fixed_0454_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008943',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008943' LIMIT 1)
WHERE
notes = 'PKG000000008943' AND 
package_label = 'SYB_fixed_0832_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008258',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008258' LIMIT 1)
WHERE
notes = 'PKG000000008258' AND 
package_label = 'SYB_fixed_0147_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009254',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009254' LIMIT 1)
WHERE
notes = 'PKG000000009254' AND 
package_label = 'SYB_fixed_1143_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009116',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009116' LIMIT 1)
WHERE
notes = 'PKG000000009116' AND 
package_label = 'SYB_fixed_1005_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009147',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009147' LIMIT 1)
WHERE
notes = 'PKG000000009147' AND 
package_label = 'SYB_fixed_1036_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008810',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008810' LIMIT 1)
WHERE
notes = 'PKG000000008810' AND 
package_label = 'SYB_fixed_0699_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008930',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008930' LIMIT 1)
WHERE
notes = 'PKG000000008930' AND 
package_label = 'SYB_fixed_0819_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009262',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009262' LIMIT 1)
WHERE
notes = 'PKG000000009262' AND 
package_label = 'SYB_fixed_1151_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009146',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009146' LIMIT 1)
WHERE
notes = 'PKG000000009146' AND 
package_label = 'SYB_fixed_1035_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008492',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008492' LIMIT 1)
WHERE
notes = 'PKG000000008492' AND 
package_label = 'SYB_fixed_0381_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008235',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008235' LIMIT 1)
WHERE
notes = 'PKG000000008235' AND 
package_label = 'SYB_fixed_0124_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008760',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008760' LIMIT 1)
WHERE
notes = 'PKG000000008760' AND 
package_label = 'SYB_fixed_0649_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008367',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008367' LIMIT 1)
WHERE
notes = 'PKG000000008367' AND 
package_label = 'SYB_fixed_0256_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008365',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008365' LIMIT 1)
WHERE
notes = 'PKG000000008365' AND 
package_label = 'SYB_fixed_0254_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007375',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007375' LIMIT 1)
WHERE
notes = 'PKG000000007375' AND 
package_label = 'SYB_fixed_0016_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009216',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009216' LIMIT 1)
WHERE
notes = 'PKG000000009216' AND 
package_label = 'SYB_fixed_1105_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009228',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009228' LIMIT 1)
WHERE
notes = 'PKG000000009228' AND 
package_label = 'SYB_fixed_1117_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008707',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008707' LIMIT 1)
WHERE
notes = 'PKG000000008707' AND 
package_label = 'SYB_fixed_0596_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009192',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009192' LIMIT 1)
WHERE
notes = 'PKG000000009192' AND 
package_label = 'SYB_fixed_1081_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008454',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008454' LIMIT 1)
WHERE
notes = 'PKG000000008454' AND 
package_label = 'SYB_fixed_0343_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007395',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007395' LIMIT 1)
WHERE
notes = 'PKG000000007395' AND 
package_label = 'SYB_fixed_0036_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008350',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008350' LIMIT 1)
WHERE
notes = 'PKG000000008350' AND 
package_label = 'SYB_fixed_0239_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009092',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009092' LIMIT 1)
WHERE
notes = 'PKG000000009092' AND 
package_label = 'SYB_fixed_0981_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008749',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008749' LIMIT 1)
WHERE
notes = 'PKG000000008749' AND 
package_label = 'SYB_fixed_0638_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009086',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009086' LIMIT 1)
WHERE
notes = 'PKG000000009086' AND 
package_label = 'SYB_fixed_0975_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008248',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008248' LIMIT 1)
WHERE
notes = 'PKG000000008248' AND 
package_label = 'SYB_fixed_0137_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009292',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009292' LIMIT 1)
WHERE
notes = 'PKG000000009292' AND 
package_label = 'SYB_fixed_1181_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008699',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008699' LIMIT 1)
WHERE
notes = 'PKG000000008699' AND 
package_label = 'SYB_fixed_0588_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009111',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009111' LIMIT 1)
WHERE
notes = 'PKG000000009111' AND 
package_label = 'SYB_fixed_1000_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008604',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008604' LIMIT 1)
WHERE
notes = 'PKG000000008604' AND 
package_label = 'SYB_fixed_0493_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008633',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008633' LIMIT 1)
WHERE
notes = 'PKG000000008633' AND 
package_label = 'SYB_fixed_0522_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008624',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008624' LIMIT 1)
WHERE
notes = 'PKG000000008624' AND 
package_label = 'SYB_fixed_0513_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008391',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008391' LIMIT 1)
WHERE
notes = 'PKG000000008391' AND 
package_label = 'SYB_fixed_0280_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008431',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008431' LIMIT 1)
WHERE
notes = 'PKG000000008431' AND 
package_label = 'SYB_fixed_0320_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008612',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008612' LIMIT 1)
WHERE
notes = 'PKG000000008612' AND 
package_label = 'SYB_fixed_0501_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008993',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008993' LIMIT 1)
WHERE
notes = 'PKG000000008993' AND 
package_label = 'SYB_fixed_0882_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008383',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008383' LIMIT 1)
WHERE
notes = 'PKG000000008383' AND 
package_label = 'SYB_fixed_0272_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008310',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008310' LIMIT 1)
WHERE
notes = 'PKG000000008310' AND 
package_label = 'SYB_fixed_0199_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008631',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008631' LIMIT 1)
WHERE
notes = 'PKG000000008631' AND 
package_label = 'SYB_fixed_0520_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008470',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008470' LIMIT 1)
WHERE
notes = 'PKG000000008470' AND 
package_label = 'SYB_fixed_0359_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008491',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008491' LIMIT 1)
WHERE
notes = 'PKG000000008491' AND 
package_label = 'SYB_fixed_0380_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009232',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009232' LIMIT 1)
WHERE
notes = 'PKG000000009232' AND 
package_label = 'SYB_fixed_1121_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008923',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008923' LIMIT 1)
WHERE
notes = 'PKG000000008923' AND 
package_label = 'SYB_fixed_0812_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009248',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009248' LIMIT 1)
WHERE
notes = 'PKG000000009248' AND 
package_label = 'SYB_fixed_1137_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009295',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009295' LIMIT 1)
WHERE
notes = 'PKG000000009295' AND 
package_label = 'SYB_fixed_1184_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009065',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009065' LIMIT 1)
WHERE
notes = 'PKG000000009065' AND 
package_label = 'SYB_fixed_0954_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009273',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009273' LIMIT 1)
WHERE
notes = 'PKG000000009273' AND 
package_label = 'SYB_fixed_1162_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008487',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008487' LIMIT 1)
WHERE
notes = 'PKG000000008487' AND 
package_label = 'SYB_fixed_0376_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008685',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008685' LIMIT 1)
WHERE
notes = 'PKG000000008685' AND 
package_label = 'SYB_fixed_0574_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008739',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008739' LIMIT 1)
WHERE
notes = 'PKG000000008739' AND 
package_label = 'SYB_fixed_0628_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008940',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008940' LIMIT 1)
WHERE
notes = 'PKG000000008940' AND 
package_label = 'SYB_fixed_0829_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008831',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008831' LIMIT 1)
WHERE
notes = 'PKG000000008831' AND 
package_label = 'SYB_fixed_0720_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009066',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009066' LIMIT 1)
WHERE
notes = 'PKG000000009066' AND 
package_label = 'SYB_fixed_0955_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008375',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008375' LIMIT 1)
WHERE
notes = 'PKG000000008375' AND 
package_label = 'SYB_fixed_0264_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008712',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008712' LIMIT 1)
WHERE
notes = 'PKG000000008712' AND 
package_label = 'SYB_fixed_0601_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009265',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009265' LIMIT 1)
WHERE
notes = 'PKG000000009265' AND 
package_label = 'SYB_fixed_1154_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008169',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008169' LIMIT 1)
WHERE
notes = 'PKG000000008169' AND 
package_label = 'SYB_fixed_0058_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008606',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008606' LIMIT 1)
WHERE
notes = 'PKG000000008606' AND 
package_label = 'SYB_fixed_0495_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008680',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008680' LIMIT 1)
WHERE
notes = 'PKG000000008680' AND 
package_label = 'SYB_fixed_0569_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008849',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008849' LIMIT 1)
WHERE
notes = 'PKG000000008849' AND 
package_label = 'SYB_fixed_0738_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009279',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009279' LIMIT 1)
WHERE
notes = 'PKG000000009279' AND 
package_label = 'SYB_fixed_1168_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009231',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009231' LIMIT 1)
WHERE
notes = 'PKG000000009231' AND 
package_label = 'SYB_fixed_1120_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008915',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008915' LIMIT 1)
WHERE
notes = 'PKG000000008915' AND 
package_label = 'SYB_fixed_0804_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007370',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007370' LIMIT 1)
WHERE
notes = 'PKG000000007370' AND 
package_label = 'SYB_fixed_0011_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009022',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009022' LIMIT 1)
WHERE
notes = 'PKG000000009022' AND 
package_label = 'SYB_fixed_0911_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008768',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008768' LIMIT 1)
WHERE
notes = 'PKG000000008768' AND 
package_label = 'SYB_fixed_0657_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008586',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008586' LIMIT 1)
WHERE
notes = 'PKG000000008586' AND 
package_label = 'SYB_fixed_0475_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008218',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008218' LIMIT 1)
WHERE
notes = 'PKG000000008218' AND 
package_label = 'SYB_fixed_0107_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008605',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008605' LIMIT 1)
WHERE
notes = 'PKG000000008605' AND 
package_label = 'SYB_fixed_0494_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008471',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008471' LIMIT 1)
WHERE
notes = 'PKG000000008471' AND 
package_label = 'SYB_fixed_0360_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008396',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008396' LIMIT 1)
WHERE
notes = 'PKG000000008396' AND 
package_label = 'SYB_fixed_0285_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009106',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009106' LIMIT 1)
WHERE
notes = 'PKG000000009106' AND 
package_label = 'SYB_fixed_0995_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008405',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008405' LIMIT 1)
WHERE
notes = 'PKG000000008405' AND 
package_label = 'SYB_fixed_0294_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009153',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009153' LIMIT 1)
WHERE
notes = 'PKG000000009153' AND 
package_label = 'SYB_fixed_1042_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008725',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008725' LIMIT 1)
WHERE
notes = 'PKG000000008725' AND 
package_label = 'SYB_fixed_0614_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008653',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008653' LIMIT 1)
WHERE
notes = 'PKG000000008653' AND 
package_label = 'SYB_fixed_0542_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008717',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008717' LIMIT 1)
WHERE
notes = 'PKG000000008717' AND 
package_label = 'SYB_fixed_0606_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008269',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008269' LIMIT 1)
WHERE
notes = 'PKG000000008269' AND 
package_label = 'SYB_fixed_0158_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009264',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009264' LIMIT 1)
WHERE
notes = 'PKG000000009264' AND 
package_label = 'SYB_fixed_1153_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009162',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009162' LIMIT 1)
WHERE
notes = 'PKG000000009162' AND 
package_label = 'SYB_fixed_1051_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008163',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008163' LIMIT 1)
WHERE
notes = 'PKG000000008163' AND 
package_label = 'SYB_fixed_0052_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007372',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007372' LIMIT 1)
WHERE
notes = 'PKG000000007372' AND 
package_label = 'SYB_fixed_0013_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009159',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009159' LIMIT 1)
WHERE
notes = 'PKG000000009159' AND 
package_label = 'SYB_fixed_1048_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008744',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008744' LIMIT 1)
WHERE
notes = 'PKG000000008744' AND 
package_label = 'SYB_fixed_0633_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008835',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008835' LIMIT 1)
WHERE
notes = 'PKG000000008835' AND 
package_label = 'SYB_fixed_0724_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008859',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008859' LIMIT 1)
WHERE
notes = 'PKG000000008859' AND 
package_label = 'SYB_fixed_0748_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009128',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009128' LIMIT 1)
WHERE
notes = 'PKG000000009128' AND 
package_label = 'SYB_fixed_1017_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008376',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008376' LIMIT 1)
WHERE
notes = 'PKG000000008376' AND 
package_label = 'SYB_fixed_0265_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008402',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008402' LIMIT 1)
WHERE
notes = 'PKG000000008402' AND 
package_label = 'SYB_fixed_0291_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009011',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009011' LIMIT 1)
WHERE
notes = 'PKG000000009011' AND 
package_label = 'SYB_fixed_0900_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009205',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009205' LIMIT 1)
WHERE
notes = 'PKG000000009205' AND 
package_label = 'SYB_fixed_1094_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008533',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008533' LIMIT 1)
WHERE
notes = 'PKG000000008533' AND 
package_label = 'SYB_fixed_0422_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008341',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008341' LIMIT 1)
WHERE
notes = 'PKG000000008341' AND 
package_label = 'SYB_fixed_0230_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009081',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009081' LIMIT 1)
WHERE
notes = 'PKG000000009081' AND 
package_label = 'SYB_fixed_0970_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008411',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008411' LIMIT 1)
WHERE
notes = 'PKG000000008411' AND 
package_label = 'SYB_fixed_0300_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008527',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008527' LIMIT 1)
WHERE
notes = 'PKG000000008527' AND 
package_label = 'SYB_fixed_0416_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009217',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009217' LIMIT 1)
WHERE
notes = 'PKG000000009217' AND 
package_label = 'SYB_fixed_1106_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008818',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008818' LIMIT 1)
WHERE
notes = 'PKG000000008818' AND 
package_label = 'SYB_fixed_0707_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009253',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009253' LIMIT 1)
WHERE
notes = 'PKG000000009253' AND 
package_label = 'SYB_fixed_1142_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008809',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008809' LIMIT 1)
WHERE
notes = 'PKG000000008809' AND 
package_label = 'SYB_fixed_0698_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009274',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009274' LIMIT 1)
WHERE
notes = 'PKG000000009274' AND 
package_label = 'SYB_fixed_1163_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009293',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009293' LIMIT 1)
WHERE
notes = 'PKG000000009293' AND 
package_label = 'SYB_fixed_1182_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008622',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008622' LIMIT 1)
WHERE
notes = 'PKG000000008622' AND 
package_label = 'SYB_fixed_0511_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009220',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009220' LIMIT 1)
WHERE
notes = 'PKG000000009220' AND 
package_label = 'SYB_fixed_1109_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008594',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008594' LIMIT 1)
WHERE
notes = 'PKG000000008594' AND 
package_label = 'SYB_fixed_0483_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009206',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009206' LIMIT 1)
WHERE
notes = 'PKG000000009206' AND 
package_label = 'SYB_fixed_1095_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008438',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008438' LIMIT 1)
WHERE
notes = 'PKG000000008438' AND 
package_label = 'SYB_fixed_0327_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009237',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009237' LIMIT 1)
WHERE
notes = 'PKG000000009237' AND 
package_label = 'SYB_fixed_1126_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008983',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008983' LIMIT 1)
WHERE
notes = 'PKG000000008983' AND 
package_label = 'SYB_fixed_0872_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009269',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009269' LIMIT 1)
WHERE
notes = 'PKG000000009269' AND 
package_label = 'SYB_fixed_1158_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008177',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008177' LIMIT 1)
WHERE
notes = 'PKG000000008177' AND 
package_label = 'SYB_fixed_0066_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009098',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009098' LIMIT 1)
WHERE
notes = 'PKG000000009098' AND 
package_label = 'SYB_fixed_0987_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009275',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009275' LIMIT 1)
WHERE
notes = 'PKG000000009275' AND 
package_label = 'SYB_fixed_1164_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009161',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009161' LIMIT 1)
WHERE
notes = 'PKG000000009161' AND 
package_label = 'SYB_fixed_1050_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008767',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008767' LIMIT 1)
WHERE
notes = 'PKG000000008767' AND 
package_label = 'SYB_fixed_0656_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007379',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007379' LIMIT 1)
WHERE
notes = 'PKG000000007379' AND 
package_label = 'SYB_fixed_0020_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008482',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008482' LIMIT 1)
WHERE
notes = 'PKG000000008482' AND 
package_label = 'SYB_fixed_0371_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009288',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009288' LIMIT 1)
WHERE
notes = 'PKG000000009288' AND 
package_label = 'SYB_fixed_1177_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008267',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008267' LIMIT 1)
WHERE
notes = 'PKG000000008267' AND 
package_label = 'SYB_fixed_0156_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008841',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008841' LIMIT 1)
WHERE
notes = 'PKG000000008841' AND 
package_label = 'SYB_fixed_0730_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008702',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008702' LIMIT 1)
WHERE
notes = 'PKG000000008702' AND 
package_label = 'SYB_fixed_0591_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009097',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009097' LIMIT 1)
WHERE
notes = 'PKG000000009097' AND 
package_label = 'SYB_fixed_0986_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009171',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009171' LIMIT 1)
WHERE
notes = 'PKG000000009171' AND 
package_label = 'SYB_fixed_1060_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008281',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008281' LIMIT 1)
WHERE
notes = 'PKG000000008281' AND 
package_label = 'SYB_fixed_0170_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008830',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008830' LIMIT 1)
WHERE
notes = 'PKG000000008830' AND 
package_label = 'SYB_fixed_0719_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008746',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008746' LIMIT 1)
WHERE
notes = 'PKG000000008746' AND 
package_label = 'SYB_fixed_0635_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008931',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008931' LIMIT 1)
WHERE
notes = 'PKG000000008931' AND 
package_label = 'SYB_fixed_0820_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008244',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008244' LIMIT 1)
WHERE
notes = 'PKG000000008244' AND 
package_label = 'SYB_fixed_0133_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008861',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008861' LIMIT 1)
WHERE
notes = 'PKG000000008861' AND 
package_label = 'SYB_fixed_0750_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008865',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008865' LIMIT 1)
WHERE
notes = 'PKG000000008865' AND 
package_label = 'SYB_fixed_0754_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008776',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008776' LIMIT 1)
WHERE
notes = 'PKG000000008776' AND 
package_label = 'SYB_fixed_0665_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008313',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008313' LIMIT 1)
WHERE
notes = 'PKG000000008313' AND 
package_label = 'SYB_fixed_0202_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008501',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008501' LIMIT 1)
WHERE
notes = 'PKG000000008501' AND 
package_label = 'SYB_fixed_0390_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007367',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007367' LIMIT 1)
WHERE
notes = 'PKG000000007367' AND 
package_label = 'SYB_fixed_0008_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008914',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008914' LIMIT 1)
WHERE
notes = 'PKG000000008914' AND 
package_label = 'SYB_fixed_0803_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009026',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009026' LIMIT 1)
WHERE
notes = 'PKG000000009026' AND 
package_label = 'SYB_fixed_0915_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009152',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009152' LIMIT 1)
WHERE
notes = 'PKG000000009152' AND 
package_label = 'SYB_fixed_1041_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008485',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008485' LIMIT 1)
WHERE
notes = 'PKG000000008485' AND 
package_label = 'SYB_fixed_0374_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007391',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007391' LIMIT 1)
WHERE
notes = 'PKG000000007391' AND 
package_label = 'SYB_fixed_0032_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008882',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008882' LIMIT 1)
WHERE
notes = 'PKG000000008882' AND 
package_label = 'SYB_fixed_0771_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008877',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008877' LIMIT 1)
WHERE
notes = 'PKG000000008877' AND 
package_label = 'SYB_fixed_0766_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008499',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008499' LIMIT 1)
WHERE
notes = 'PKG000000008499' AND 
package_label = 'SYB_fixed_0388_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008222',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008222' LIMIT 1)
WHERE
notes = 'PKG000000008222' AND 
package_label = 'SYB_fixed_0111_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008652',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008652' LIMIT 1)
WHERE
notes = 'PKG000000008652' AND 
package_label = 'SYB_fixed_0541_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009051',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009051' LIMIT 1)
WHERE
notes = 'PKG000000009051' AND 
package_label = 'SYB_fixed_0940_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008976',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008976' LIMIT 1)
WHERE
notes = 'PKG000000008976' AND 
package_label = 'SYB_fixed_0865_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009243',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009243' LIMIT 1)
WHERE
notes = 'PKG000000009243' AND 
package_label = 'SYB_fixed_1132_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008801',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008801' LIMIT 1)
WHERE
notes = 'PKG000000008801' AND 
package_label = 'SYB_fixed_0690_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008741',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008741' LIMIT 1)
WHERE
notes = 'PKG000000008741' AND 
package_label = 'SYB_fixed_0630_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008278',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008278' LIMIT 1)
WHERE
notes = 'PKG000000008278' AND 
package_label = 'SYB_fixed_0167_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008479',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008479' LIMIT 1)
WHERE
notes = 'PKG000000008479' AND 
package_label = 'SYB_fixed_0368_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008409',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008409' LIMIT 1)
WHERE
notes = 'PKG000000008409' AND 
package_label = 'SYB_fixed_0298_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008456',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008456' LIMIT 1)
WHERE
notes = 'PKG000000008456' AND 
package_label = 'SYB_fixed_0345_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008251',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008251' LIMIT 1)
WHERE
notes = 'PKG000000008251' AND 
package_label = 'SYB_fixed_0140_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008642',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008642' LIMIT 1)
WHERE
notes = 'PKG000000008642' AND 
package_label = 'SYB_fixed_0531_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008843',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008843' LIMIT 1)
WHERE
notes = 'PKG000000008843' AND 
package_label = 'SYB_fixed_0732_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009229',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009229' LIMIT 1)
WHERE
notes = 'PKG000000009229' AND 
package_label = 'SYB_fixed_1118_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008613',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008613' LIMIT 1)
WHERE
notes = 'PKG000000008613' AND 
package_label = 'SYB_fixed_0502_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008507',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008507' LIMIT 1)
WHERE
notes = 'PKG000000008507' AND 
package_label = 'SYB_fixed_0396_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009068',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009068' LIMIT 1)
WHERE
notes = 'PKG000000009068' AND 
package_label = 'SYB_fixed_0957_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008748',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008748' LIMIT 1)
WHERE
notes = 'PKG000000008748' AND 
package_label = 'SYB_fixed_0637_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008955',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008955' LIMIT 1)
WHERE
notes = 'PKG000000008955' AND 
package_label = 'SYB_fixed_0844_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008764',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008764' LIMIT 1)
WHERE
notes = 'PKG000000008764' AND 
package_label = 'SYB_fixed_0653_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008325',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008325' LIMIT 1)
WHERE
notes = 'PKG000000008325' AND 
package_label = 'SYB_fixed_0214_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009163',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009163' LIMIT 1)
WHERE
notes = 'PKG000000009163' AND 
package_label = 'SYB_fixed_1052_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009166',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009166' LIMIT 1)
WHERE
notes = 'PKG000000009166' AND 
package_label = 'SYB_fixed_1055_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008918',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008918' LIMIT 1)
WHERE
notes = 'PKG000000008918' AND 
package_label = 'SYB_fixed_0807_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008901',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008901' LIMIT 1)
WHERE
notes = 'PKG000000008901' AND 
package_label = 'SYB_fixed_0790_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009170',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009170' LIMIT 1)
WHERE
notes = 'PKG000000009170' AND 
package_label = 'SYB_fixed_1059_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008474',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008474' LIMIT 1)
WHERE
notes = 'PKG000000008474' AND 
package_label = 'SYB_fixed_0363_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008320',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008320' LIMIT 1)
WHERE
notes = 'PKG000000008320' AND 
package_label = 'SYB_fixed_0209_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008932',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008932' LIMIT 1)
WHERE
notes = 'PKG000000008932' AND 
package_label = 'SYB_fixed_0821_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009013',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009013' LIMIT 1)
WHERE
notes = 'PKG000000009013' AND 
package_label = 'SYB_fixed_0902_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008959',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008959' LIMIT 1)
WHERE
notes = 'PKG000000008959' AND 
package_label = 'SYB_fixed_0848_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008706',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008706' LIMIT 1)
WHERE
notes = 'PKG000000008706' AND 
package_label = 'SYB_fixed_0595_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009244',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009244' LIMIT 1)
WHERE
notes = 'PKG000000009244' AND 
package_label = 'SYB_fixed_1133_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008530',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008530' LIMIT 1)
WHERE
notes = 'PKG000000008530' AND 
package_label = 'SYB_fixed_0419_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008881',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008881' LIMIT 1)
WHERE
notes = 'PKG000000008881' AND 
package_label = 'SYB_fixed_0770_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008704',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008704' LIMIT 1)
WHERE
notes = 'PKG000000008704' AND 
package_label = 'SYB_fixed_0593_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008889',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008889' LIMIT 1)
WHERE
notes = 'PKG000000008889' AND 
package_label = 'SYB_fixed_0778_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009127',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009127' LIMIT 1)
WHERE
notes = 'PKG000000009127' AND 
package_label = 'SYB_fixed_1016_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008397',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008397' LIMIT 1)
WHERE
notes = 'PKG000000008397' AND 
package_label = 'SYB_fixed_0286_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008164',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008164' LIMIT 1)
WHERE
notes = 'PKG000000008164' AND 
package_label = 'SYB_fixed_0053_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009174',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009174' LIMIT 1)
WHERE
notes = 'PKG000000009174' AND 
package_label = 'SYB_fixed_1063_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008911',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008911' LIMIT 1)
WHERE
notes = 'PKG000000008911' AND 
package_label = 'SYB_fixed_0800_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009222',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009222' LIMIT 1)
WHERE
notes = 'PKG000000009222' AND 
package_label = 'SYB_fixed_1111_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008815',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008815' LIMIT 1)
WHERE
notes = 'PKG000000008815' AND 
package_label = 'SYB_fixed_0704_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008568',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008568' LIMIT 1)
WHERE
notes = 'PKG000000008568' AND 
package_label = 'SYB_fixed_0457_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008694',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008694' LIMIT 1)
WHERE
notes = 'PKG000000008694' AND 
package_label = 'SYB_fixed_0583_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009230',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009230' LIMIT 1)
WHERE
notes = 'PKG000000009230' AND 
package_label = 'SYB_fixed_1119_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008840',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008840' LIMIT 1)
WHERE
notes = 'PKG000000008840' AND 
package_label = 'SYB_fixed_0729_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009178',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009178' LIMIT 1)
WHERE
notes = 'PKG000000009178' AND 
package_label = 'SYB_fixed_1067_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008202',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008202' LIMIT 1)
WHERE
notes = 'PKG000000008202' AND 
package_label = 'SYB_fixed_0091_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008975',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008975' LIMIT 1)
WHERE
notes = 'PKG000000008975' AND 
package_label = 'SYB_fixed_0864_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007376',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007376' LIMIT 1)
WHERE
notes = 'PKG000000007376' AND 
package_label = 'SYB_fixed_0017_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009184',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009184' LIMIT 1)
WHERE
notes = 'PKG000000009184' AND 
package_label = 'SYB_fixed_1073_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007408',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007408' LIMIT 1)
WHERE
notes = 'PKG000000007408' AND 
package_label = 'SYB_fixed_0049_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008994',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008994' LIMIT 1)
WHERE
notes = 'PKG000000008994' AND 
package_label = 'SYB_fixed_0883_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008483',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008483' LIMIT 1)
WHERE
notes = 'PKG000000008483' AND 
package_label = 'SYB_fixed_0372_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008170',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008170' LIMIT 1)
WHERE
notes = 'PKG000000008170' AND 
package_label = 'SYB_fixed_0059_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008525',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008525' LIMIT 1)
WHERE
notes = 'PKG000000008525' AND 
package_label = 'SYB_fixed_0414_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008373',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008373' LIMIT 1)
WHERE
notes = 'PKG000000008373' AND 
package_label = 'SYB_fixed_0262_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008509',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008509' LIMIT 1)
WHERE
notes = 'PKG000000008509' AND 
package_label = 'SYB_fixed_0398_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008422',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008422' LIMIT 1)
WHERE
notes = 'PKG000000008422' AND 
package_label = 'SYB_fixed_0311_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009075',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009075' LIMIT 1)
WHERE
notes = 'PKG000000009075' AND 
package_label = 'SYB_fixed_0964_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008543',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008543' LIMIT 1)
WHERE
notes = 'PKG000000008543' AND 
package_label = 'SYB_fixed_0432_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008669',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008669' LIMIT 1)
WHERE
notes = 'PKG000000008669' AND 
package_label = 'SYB_fixed_0558_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009204',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009204' LIMIT 1)
WHERE
notes = 'PKG000000009204' AND 
package_label = 'SYB_fixed_1093_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008546',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008546' LIMIT 1)
WHERE
notes = 'PKG000000008546' AND 
package_label = 'SYB_fixed_0435_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009082',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009082' LIMIT 1)
WHERE
notes = 'PKG000000009082' AND 
package_label = 'SYB_fixed_0971_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008259',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008259' LIMIT 1)
WHERE
notes = 'PKG000000008259' AND 
package_label = 'SYB_fixed_0148_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008775',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008775' LIMIT 1)
WHERE
notes = 'PKG000000008775' AND 
package_label = 'SYB_fixed_0664_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008754',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008754' LIMIT 1)
WHERE
notes = 'PKG000000008754' AND 
package_label = 'SYB_fixed_0643_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008234',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008234' LIMIT 1)
WHERE
notes = 'PKG000000008234' AND 
package_label = 'SYB_fixed_0123_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008663',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008663' LIMIT 1)
WHERE
notes = 'PKG000000008663' AND 
package_label = 'SYB_fixed_0552_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008592',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008592' LIMIT 1)
WHERE
notes = 'PKG000000008592' AND 
package_label = 'SYB_fixed_0481_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008535',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008535' LIMIT 1)
WHERE
notes = 'PKG000000008535' AND 
package_label = 'SYB_fixed_0424_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009266',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009266' LIMIT 1)
WHERE
notes = 'PKG000000009266' AND 
package_label = 'SYB_fixed_1155_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009179',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009179' LIMIT 1)
WHERE
notes = 'PKG000000009179' AND 
package_label = 'SYB_fixed_1068_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008608',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008608' LIMIT 1)
WHERE
notes = 'PKG000000008608' AND 
package_label = 'SYB_fixed_0497_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007403',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007403' LIMIT 1)
WHERE
notes = 'PKG000000007403' AND 
package_label = 'SYB_fixed_0044_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008315',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008315' LIMIT 1)
WHERE
notes = 'PKG000000008315' AND 
package_label = 'SYB_fixed_0204_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008201',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008201' LIMIT 1)
WHERE
notes = 'PKG000000008201' AND 
package_label = 'SYB_fixed_0090_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008883',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008883' LIMIT 1)
WHERE
notes = 'PKG000000008883' AND 
package_label = 'SYB_fixed_0772_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008187',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008187' LIMIT 1)
WHERE
notes = 'PKG000000008187' AND 
package_label = 'SYB_fixed_0076_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009294',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009294' LIMIT 1)
WHERE
notes = 'PKG000000009294' AND 
package_label = 'SYB_fixed_1183_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008353',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008353' LIMIT 1)
WHERE
notes = 'PKG000000008353' AND 
package_label = 'SYB_fixed_0242_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008203',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008203' LIMIT 1)
WHERE
notes = 'PKG000000008203' AND 
package_label = 'SYB_fixed_0092_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008855',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008855' LIMIT 1)
WHERE
notes = 'PKG000000008855' AND 
package_label = 'SYB_fixed_0744_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008173',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008173' LIMIT 1)
WHERE
notes = 'PKG000000008173' AND 
package_label = 'SYB_fixed_0062_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009076',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009076' LIMIT 1)
WHERE
notes = 'PKG000000009076' AND 
package_label = 'SYB_fixed_0965_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008934',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008934' LIMIT 1)
WHERE
notes = 'PKG000000008934' AND 
package_label = 'SYB_fixed_0823_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008520',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008520' LIMIT 1)
WHERE
notes = 'PKG000000008520' AND 
package_label = 'SYB_fixed_0409_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008927',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008927' LIMIT 1)
WHERE
notes = 'PKG000000008927' AND 
package_label = 'SYB_fixed_0816_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008399',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008399' LIMIT 1)
WHERE
notes = 'PKG000000008399' AND 
package_label = 'SYB_fixed_0288_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007382',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007382' LIMIT 1)
WHERE
notes = 'PKG000000007382' AND 
package_label = 'SYB_fixed_0023_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008196',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008196' LIMIT 1)
WHERE
notes = 'PKG000000008196' AND 
package_label = 'SYB_fixed_0085_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008291',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008291' LIMIT 1)
WHERE
notes = 'PKG000000008291' AND 
package_label = 'SYB_fixed_0180_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008833',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008833' LIMIT 1)
WHERE
notes = 'PKG000000008833' AND 
package_label = 'SYB_fixed_0722_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008171',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008171' LIMIT 1)
WHERE
notes = 'PKG000000008171' AND 
package_label = 'SYB_fixed_0060_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009046',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009046' LIMIT 1)
WHERE
notes = 'PKG000000009046' AND 
package_label = 'SYB_fixed_0935_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008511',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008511' LIMIT 1)
WHERE
notes = 'PKG000000008511' AND 
package_label = 'SYB_fixed_0400_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009009',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009009' LIMIT 1)
WHERE
notes = 'PKG000000009009' AND 
package_label = 'SYB_fixed_0898_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008851',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008851' LIMIT 1)
WHERE
notes = 'PKG000000008851' AND 
package_label = 'SYB_fixed_0740_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008977',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008977' LIMIT 1)
WHERE
notes = 'PKG000000008977' AND 
package_label = 'SYB_fixed_0866_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009182',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009182' LIMIT 1)
WHERE
notes = 'PKG000000009182' AND 
package_label = 'SYB_fixed_1071_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009045',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009045' LIMIT 1)
WHERE
notes = 'PKG000000009045' AND 
package_label = 'SYB_fixed_0934_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008844',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008844' LIMIT 1)
WHERE
notes = 'PKG000000008844' AND 
package_label = 'SYB_fixed_0733_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008796',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008796' LIMIT 1)
WHERE
notes = 'PKG000000008796' AND 
package_label = 'SYB_fixed_0685_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008344',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008344' LIMIT 1)
WHERE
notes = 'PKG000000008344' AND 
package_label = 'SYB_fixed_0233_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008516',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008516' LIMIT 1)
WHERE
notes = 'PKG000000008516' AND 
package_label = 'SYB_fixed_0405_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008460',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008460' LIMIT 1)
WHERE
notes = 'PKG000000008460' AND 
package_label = 'SYB_fixed_0349_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007360',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007360' LIMIT 1)
WHERE
notes = 'PKG000000007360' AND 
package_label = 'SYB_fixed_0001_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007373',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007373' LIMIT 1)
WHERE
notes = 'PKG000000007373' AND 
package_label = 'SYB_fixed_0014_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008981',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008981' LIMIT 1)
WHERE
notes = 'PKG000000008981' AND 
package_label = 'SYB_fixed_0870_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008274',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008274' LIMIT 1)
WHERE
notes = 'PKG000000008274' AND 
package_label = 'SYB_fixed_0163_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007369',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007369' LIMIT 1)
WHERE
notes = 'PKG000000007369' AND 
package_label = 'SYB_fixed_0010_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008751',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008751' LIMIT 1)
WHERE
notes = 'PKG000000008751' AND 
package_label = 'SYB_fixed_0640_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008412',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008412' LIMIT 1)
WHERE
notes = 'PKG000000008412' AND 
package_label = 'SYB_fixed_0301_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008820',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008820' LIMIT 1)
WHERE
notes = 'PKG000000008820' AND 
package_label = 'SYB_fixed_0709_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009103',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009103' LIMIT 1)
WHERE
notes = 'PKG000000009103' AND 
package_label = 'SYB_fixed_0992_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008984',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008984' LIMIT 1)
WHERE
notes = 'PKG000000008984' AND 
package_label = 'SYB_fixed_0873_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009124',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009124' LIMIT 1)
WHERE
notes = 'PKG000000009124' AND 
package_label = 'SYB_fixed_1013_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008657',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008657' LIMIT 1)
WHERE
notes = 'PKG000000008657' AND 
package_label = 'SYB_fixed_0546_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008941',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008941' LIMIT 1)
WHERE
notes = 'PKG000000008941' AND 
package_label = 'SYB_fixed_0830_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008954',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008954' LIMIT 1)
WHERE
notes = 'PKG000000008954' AND 
package_label = 'SYB_fixed_0843_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008523',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008523' LIMIT 1)
WHERE
notes = 'PKG000000008523' AND 
package_label = 'SYB_fixed_0412_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009007',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009007' LIMIT 1)
WHERE
notes = 'PKG000000009007' AND 
package_label = 'SYB_fixed_0896_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008264',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008264' LIMIT 1)
WHERE
notes = 'PKG000000008264' AND 
package_label = 'SYB_fixed_0153_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008512',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008512' LIMIT 1)
WHERE
notes = 'PKG000000008512' AND 
package_label = 'SYB_fixed_0401_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008421',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008421' LIMIT 1)
WHERE
notes = 'PKG000000008421' AND 
package_label = 'SYB_fixed_0310_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008425',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008425' LIMIT 1)
WHERE
notes = 'PKG000000008425' AND 
package_label = 'SYB_fixed_0314_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009140',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009140' LIMIT 1)
WHERE
notes = 'PKG000000009140' AND 
package_label = 'SYB_fixed_1029_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008372',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008372' LIMIT 1)
WHERE
notes = 'PKG000000008372' AND 
package_label = 'SYB_fixed_0261_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008282',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008282' LIMIT 1)
WHERE
notes = 'PKG000000008282' AND 
package_label = 'SYB_fixed_0171_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008297',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008297' LIMIT 1)
WHERE
notes = 'PKG000000008297' AND 
package_label = 'SYB_fixed_0186_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009286',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009286' LIMIT 1)
WHERE
notes = 'PKG000000009286' AND 
package_label = 'SYB_fixed_1175_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008339',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008339' LIMIT 1)
WHERE
notes = 'PKG000000008339' AND 
package_label = 'SYB_fixed_0228_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008240',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008240' LIMIT 1)
WHERE
notes = 'PKG000000008240' AND 
package_label = 'SYB_fixed_0129_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007380',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007380' LIMIT 1)
WHERE
notes = 'PKG000000007380' AND 
package_label = 'SYB_fixed_0021_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008942',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008942' LIMIT 1)
WHERE
notes = 'PKG000000008942' AND 
package_label = 'SYB_fixed_0831_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008260',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008260' LIMIT 1)
WHERE
notes = 'PKG000000008260' AND 
package_label = 'SYB_fixed_0149_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009071',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009071' LIMIT 1)
WHERE
notes = 'PKG000000009071' AND 
package_label = 'SYB_fixed_0960_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008518',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008518' LIMIT 1)
WHERE
notes = 'PKG000000008518' AND 
package_label = 'SYB_fixed_0407_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009177',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009177' LIMIT 1)
WHERE
notes = 'PKG000000009177' AND 
package_label = 'SYB_fixed_1066_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008241',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008241' LIMIT 1)
WHERE
notes = 'PKG000000008241' AND 
package_label = 'SYB_fixed_0130_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007387',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007387' LIMIT 1)
WHERE
notes = 'PKG000000007387' AND 
package_label = 'SYB_fixed_0028_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008965',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008965' LIMIT 1)
WHERE
notes = 'PKG000000008965' AND 
package_label = 'SYB_fixed_0854_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008842',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008842' LIMIT 1)
WHERE
notes = 'PKG000000008842' AND 
package_label = 'SYB_fixed_0731_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008414',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008414' LIMIT 1)
WHERE
notes = 'PKG000000008414' AND 
package_label = 'SYB_fixed_0303_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008656',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008656' LIMIT 1)
WHERE
notes = 'PKG000000008656' AND 
package_label = 'SYB_fixed_0545_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008349',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008349' LIMIT 1)
WHERE
notes = 'PKG000000008349' AND 
package_label = 'SYB_fixed_0238_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008807',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008807' LIMIT 1)
WHERE
notes = 'PKG000000008807' AND 
package_label = 'SYB_fixed_0696_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008432',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008432' LIMIT 1)
WHERE
notes = 'PKG000000008432' AND 
package_label = 'SYB_fixed_0321_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008805',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008805' LIMIT 1)
WHERE
notes = 'PKG000000008805' AND 
package_label = 'SYB_fixed_0694_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009165',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009165' LIMIT 1)
WHERE
notes = 'PKG000000009165' AND 
package_label = 'SYB_fixed_1054_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009137',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009137' LIMIT 1)
WHERE
notes = 'PKG000000009137' AND 
package_label = 'SYB_fixed_1026_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009095',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009095' LIMIT 1)
WHERE
notes = 'PKG000000009095' AND 
package_label = 'SYB_fixed_0984_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009280',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009280' LIMIT 1)
WHERE
notes = 'PKG000000009280' AND 
package_label = 'SYB_fixed_1169_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008200',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008200' LIMIT 1)
WHERE
notes = 'PKG000000008200' AND 
package_label = 'SYB_fixed_0089_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008204',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008204' LIMIT 1)
WHERE
notes = 'PKG000000008204' AND 
package_label = 'SYB_fixed_0093_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008451',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008451' LIMIT 1)
WHERE
notes = 'PKG000000008451' AND 
package_label = 'SYB_fixed_0340_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008475',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008475' LIMIT 1)
WHERE
notes = 'PKG000000008475' AND 
package_label = 'SYB_fixed_0364_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009240',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009240' LIMIT 1)
WHERE
notes = 'PKG000000009240' AND 
package_label = 'SYB_fixed_1129_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009063',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009063' LIMIT 1)
WHERE
notes = 'PKG000000009063' AND 
package_label = 'SYB_fixed_0952_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008913',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008913' LIMIT 1)
WHERE
notes = 'PKG000000008913' AND 
package_label = 'SYB_fixed_0802_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008670',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008670' LIMIT 1)
WHERE
notes = 'PKG000000008670' AND 
package_label = 'SYB_fixed_0559_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008230',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008230' LIMIT 1)
WHERE
notes = 'PKG000000008230' AND 
package_label = 'SYB_fixed_0119_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008505',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008505' LIMIT 1)
WHERE
notes = 'PKG000000008505' AND 
package_label = 'SYB_fixed_0394_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008973',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008973' LIMIT 1)
WHERE
notes = 'PKG000000008973' AND 
package_label = 'SYB_fixed_0862_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009181',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009181' LIMIT 1)
WHERE
notes = 'PKG000000009181' AND 
package_label = 'SYB_fixed_1070_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008573',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008573' LIMIT 1)
WHERE
notes = 'PKG000000008573' AND 
package_label = 'SYB_fixed_0462_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008544',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008544' LIMIT 1)
WHERE
notes = 'PKG000000008544' AND 
package_label = 'SYB_fixed_0433_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009251',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009251' LIMIT 1)
WHERE
notes = 'PKG000000009251' AND 
package_label = 'SYB_fixed_1140_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008863',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008863' LIMIT 1)
WHERE
notes = 'PKG000000008863' AND 
package_label = 'SYB_fixed_0752_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008415',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008415' LIMIT 1)
WHERE
notes = 'PKG000000008415' AND 
package_label = 'SYB_fixed_0304_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007384',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007384' LIMIT 1)
WHERE
notes = 'PKG000000007384' AND 
package_label = 'SYB_fixed_0025_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007401',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007401' LIMIT 1)
WHERE
notes = 'PKG000000007401' AND 
package_label = 'SYB_fixed_0042_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009139',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009139' LIMIT 1)
WHERE
notes = 'PKG000000009139' AND 
package_label = 'SYB_fixed_1028_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008481',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008481' LIMIT 1)
WHERE
notes = 'PKG000000008481' AND 
package_label = 'SYB_fixed_0370_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008673',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008673' LIMIT 1)
WHERE
notes = 'PKG000000008673' AND 
package_label = 'SYB_fixed_0562_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009096',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009096' LIMIT 1)
WHERE
notes = 'PKG000000009096' AND 
package_label = 'SYB_fixed_0985_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008263',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008263' LIMIT 1)
WHERE
notes = 'PKG000000008263' AND 
package_label = 'SYB_fixed_0152_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009283',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009283' LIMIT 1)
WHERE
notes = 'PKG000000009283' AND 
package_label = 'SYB_fixed_1172_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009130',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009130' LIMIT 1)
WHERE
notes = 'PKG000000009130' AND 
package_label = 'SYB_fixed_1019_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008277',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008277' LIMIT 1)
WHERE
notes = 'PKG000000008277' AND 
package_label = 'SYB_fixed_0166_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009221',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009221' LIMIT 1)
WHERE
notes = 'PKG000000009221' AND 
package_label = 'SYB_fixed_1110_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008982',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008982' LIMIT 1)
WHERE
notes = 'PKG000000008982' AND 
package_label = 'SYB_fixed_0871_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008845',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008845' LIMIT 1)
WHERE
notes = 'PKG000000008845' AND 
package_label = 'SYB_fixed_0734_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008417',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008417' LIMIT 1)
WHERE
notes = 'PKG000000008417' AND 
package_label = 'SYB_fixed_0306_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008459',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008459' LIMIT 1)
WHERE
notes = 'PKG000000008459' AND 
package_label = 'SYB_fixed_0348_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008473',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008473' LIMIT 1)
WHERE
notes = 'PKG000000008473' AND 
package_label = 'SYB_fixed_0362_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009239',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009239' LIMIT 1)
WHERE
notes = 'PKG000000009239' AND 
package_label = 'SYB_fixed_1128_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008761',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008761' LIMIT 1)
WHERE
notes = 'PKG000000008761' AND 
package_label = 'SYB_fixed_0650_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008826',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008826' LIMIT 1)
WHERE
notes = 'PKG000000008826' AND 
package_label = 'SYB_fixed_0715_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008778',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008778' LIMIT 1)
WHERE
notes = 'PKG000000008778' AND 
package_label = 'SYB_fixed_0667_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008650',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008650' LIMIT 1)
WHERE
notes = 'PKG000000008650' AND 
package_label = 'SYB_fixed_0539_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008620',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008620' LIMIT 1)
WHERE
notes = 'PKG000000008620' AND 
package_label = 'SYB_fixed_0509_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009260',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009260' LIMIT 1)
WHERE
notes = 'PKG000000009260' AND 
package_label = 'SYB_fixed_1149_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008718',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008718' LIMIT 1)
WHERE
notes = 'PKG000000008718' AND 
package_label = 'SYB_fixed_0607_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008599',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008599' LIMIT 1)
WHERE
notes = 'PKG000000008599' AND 
package_label = 'SYB_fixed_0488_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007398',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007398' LIMIT 1)
WHERE
notes = 'PKG000000007398' AND 
package_label = 'SYB_fixed_0039_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008610',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008610' LIMIT 1)
WHERE
notes = 'PKG000000008610' AND 
package_label = 'SYB_fixed_0499_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008972',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008972' LIMIT 1)
WHERE
notes = 'PKG000000008972' AND 
package_label = 'SYB_fixed_0861_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008357',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008357' LIMIT 1)
WHERE
notes = 'PKG000000008357' AND 
package_label = 'SYB_fixed_0246_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009044',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009044' LIMIT 1)
WHERE
notes = 'PKG000000009044' AND 
package_label = 'SYB_fixed_0933_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009012',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009012' LIMIT 1)
WHERE
notes = 'PKG000000009012' AND 
package_label = 'SYB_fixed_0901_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009030',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009030' LIMIT 1)
WHERE
notes = 'PKG000000009030' AND 
package_label = 'SYB_fixed_0919_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009218',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009218' LIMIT 1)
WHERE
notes = 'PKG000000009218' AND 
package_label = 'SYB_fixed_1107_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008822',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008822' LIMIT 1)
WHERE
notes = 'PKG000000008822' AND 
package_label = 'SYB_fixed_0711_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009207',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009207' LIMIT 1)
WHERE
notes = 'PKG000000009207' AND 
package_label = 'SYB_fixed_1096_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008806',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008806' LIMIT 1)
WHERE
notes = 'PKG000000008806' AND 
package_label = 'SYB_fixed_0695_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008443',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008443' LIMIT 1)
WHERE
notes = 'PKG000000008443' AND 
package_label = 'SYB_fixed_0332_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009278',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009278' LIMIT 1)
WHERE
notes = 'PKG000000009278' AND 
package_label = 'SYB_fixed_1167_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008813',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008813' LIMIT 1)
WHERE
notes = 'PKG000000008813' AND 
package_label = 'SYB_fixed_0702_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009054',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009054' LIMIT 1)
WHERE
notes = 'PKG000000009054' AND 
package_label = 'SYB_fixed_0943_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008455',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008455' LIMIT 1)
WHERE
notes = 'PKG000000008455' AND 
package_label = 'SYB_fixed_0344_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009142',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009142' LIMIT 1)
WHERE
notes = 'PKG000000009142' AND 
package_label = 'SYB_fixed_1031_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007407',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007407' LIMIT 1)
WHERE
notes = 'PKG000000007407' AND 
package_label = 'SYB_fixed_0048_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008519',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008519' LIMIT 1)
WHERE
notes = 'PKG000000008519' AND 
package_label = 'SYB_fixed_0408_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009261',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009261' LIMIT 1)
WHERE
notes = 'PKG000000009261' AND 
package_label = 'SYB_fixed_1150_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008382',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008382' LIMIT 1)
WHERE
notes = 'PKG000000008382' AND 
package_label = 'SYB_fixed_0271_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008839',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008839' LIMIT 1)
WHERE
notes = 'PKG000000008839' AND 
package_label = 'SYB_fixed_0728_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008370',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008370' LIMIT 1)
WHERE
notes = 'PKG000000008370' AND 
package_label = 'SYB_fixed_0259_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008967',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008967' LIMIT 1)
WHERE
notes = 'PKG000000008967' AND 
package_label = 'SYB_fixed_0856_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008636',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008636' LIMIT 1)
WHERE
notes = 'PKG000000008636' AND 
package_label = 'SYB_fixed_0525_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008782',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008782' LIMIT 1)
WHERE
notes = 'PKG000000008782' AND 
package_label = 'SYB_fixed_0671_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009077',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009077' LIMIT 1)
WHERE
notes = 'PKG000000009077' AND 
package_label = 'SYB_fixed_0966_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008299',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008299' LIMIT 1)
WHERE
notes = 'PKG000000008299' AND 
package_label = 'SYB_fixed_0188_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008332',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008332' LIMIT 1)
WHERE
notes = 'PKG000000008332' AND 
package_label = 'SYB_fixed_0221_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008497',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008497' LIMIT 1)
WHERE
notes = 'PKG000000008497' AND 
package_label = 'SYB_fixed_0386_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008788',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008788' LIMIT 1)
WHERE
notes = 'PKG000000008788' AND 
package_label = 'SYB_fixed_0677_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008868',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008868' LIMIT 1)
WHERE
notes = 'PKG000000008868' AND 
package_label = 'SYB_fixed_0757_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008769',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008769' LIMIT 1)
WHERE
notes = 'PKG000000008769' AND 
package_label = 'SYB_fixed_0658_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008662',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008662' LIMIT 1)
WHERE
notes = 'PKG000000008662' AND 
package_label = 'SYB_fixed_0551_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008554',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008554' LIMIT 1)
WHERE
notes = 'PKG000000008554' AND 
package_label = 'SYB_fixed_0443_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009175',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009175' LIMIT 1)
WHERE
notes = 'PKG000000009175' AND 
package_label = 'SYB_fixed_1064_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008486',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008486' LIMIT 1)
WHERE
notes = 'PKG000000008486' AND 
package_label = 'SYB_fixed_0375_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008743',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008743' LIMIT 1)
WHERE
notes = 'PKG000000008743' AND 
package_label = 'SYB_fixed_0632_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008693',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008693' LIMIT 1)
WHERE
notes = 'PKG000000008693' AND 
package_label = 'SYB_fixed_0582_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007371',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007371' LIMIT 1)
WHERE
notes = 'PKG000000007371' AND 
package_label = 'SYB_fixed_0012_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008752',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008752' LIMIT 1)
WHERE
notes = 'PKG000000008752' AND 
package_label = 'SYB_fixed_0641_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008195',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008195' LIMIT 1)
WHERE
notes = 'PKG000000008195' AND 
package_label = 'SYB_fixed_0084_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008645',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008645' LIMIT 1)
WHERE
notes = 'PKG000000008645' AND 
package_label = 'SYB_fixed_0534_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008210',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008210' LIMIT 1)
WHERE
notes = 'PKG000000008210' AND 
package_label = 'SYB_fixed_0099_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008524',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008524' LIMIT 1)
WHERE
notes = 'PKG000000008524' AND 
package_label = 'SYB_fixed_0413_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008617',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008617' LIMIT 1)
WHERE
notes = 'PKG000000008617' AND 
package_label = 'SYB_fixed_0506_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009225',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009225' LIMIT 1)
WHERE
notes = 'PKG000000009225' AND 
package_label = 'SYB_fixed_1114_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008933',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008933' LIMIT 1)
WHERE
notes = 'PKG000000008933' AND 
package_label = 'SYB_fixed_0822_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008640',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008640' LIMIT 1)
WHERE
notes = 'PKG000000008640' AND 
package_label = 'SYB_fixed_0529_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008627',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008627' LIMIT 1)
WHERE
notes = 'PKG000000008627' AND 
package_label = 'SYB_fixed_0516_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008183',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008183' LIMIT 1)
WHERE
notes = 'PKG000000008183' AND 
package_label = 'SYB_fixed_0072_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008504',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008504' LIMIT 1)
WHERE
notes = 'PKG000000008504' AND 
package_label = 'SYB_fixed_0393_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008199',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008199' LIMIT 1)
WHERE
notes = 'PKG000000008199' AND 
package_label = 'SYB_fixed_0088_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008958',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008958' LIMIT 1)
WHERE
notes = 'PKG000000008958' AND 
package_label = 'SYB_fixed_0847_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008639',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008639' LIMIT 1)
WHERE
notes = 'PKG000000008639' AND 
package_label = 'SYB_fixed_0528_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008469',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008469' LIMIT 1)
WHERE
notes = 'PKG000000008469' AND 
package_label = 'SYB_fixed_0358_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008724',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008724' LIMIT 1)
WHERE
notes = 'PKG000000008724' AND 
package_label = 'SYB_fixed_0613_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008579',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008579' LIMIT 1)
WHERE
notes = 'PKG000000008579' AND 
package_label = 'SYB_fixed_0468_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008732',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008732' LIMIT 1)
WHERE
notes = 'PKG000000008732' AND 
package_label = 'SYB_fixed_0621_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007386',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007386' LIMIT 1)
WHERE
notes = 'PKG000000007386' AND 
package_label = 'SYB_fixed_0027_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009252',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009252' LIMIT 1)
WHERE
notes = 'PKG000000009252' AND 
package_label = 'SYB_fixed_1141_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009241',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009241' LIMIT 1)
WHERE
notes = 'PKG000000009241' AND 
package_label = 'SYB_fixed_1130_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009001',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009001' LIMIT 1)
WHERE
notes = 'PKG000000009001' AND 
package_label = 'SYB_fixed_0890_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009061',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009061' LIMIT 1)
WHERE
notes = 'PKG000000009061' AND 
package_label = 'SYB_fixed_0950_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008221',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008221' LIMIT 1)
WHERE
notes = 'PKG000000008221' AND 
package_label = 'SYB_fixed_0110_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008354',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008354' LIMIT 1)
WHERE
notes = 'PKG000000008354' AND 
package_label = 'SYB_fixed_0243_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009197',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009197' LIMIT 1)
WHERE
notes = 'PKG000000009197' AND 
package_label = 'SYB_fixed_1086_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009015',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009015' LIMIT 1)
WHERE
notes = 'PKG000000009015' AND 
package_label = 'SYB_fixed_0904_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008572',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008572' LIMIT 1)
WHERE
notes = 'PKG000000008572' AND 
package_label = 'SYB_fixed_0461_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008672',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008672' LIMIT 1)
WHERE
notes = 'PKG000000008672' AND 
package_label = 'SYB_fixed_0561_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008626',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008626' LIMIT 1)
WHERE
notes = 'PKG000000008626' AND 
package_label = 'SYB_fixed_0515_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008996',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008996' LIMIT 1)
WHERE
notes = 'PKG000000008996' AND 
package_label = 'SYB_fixed_0885_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008440',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008440' LIMIT 1)
WHERE
notes = 'PKG000000008440' AND 
package_label = 'SYB_fixed_0329_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009118',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009118' LIMIT 1)
WHERE
notes = 'PKG000000009118' AND 
package_label = 'SYB_fixed_1007_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008893',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008893' LIMIT 1)
WHERE
notes = 'PKG000000008893' AND 
package_label = 'SYB_fixed_0782_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008583',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008583' LIMIT 1)
WHERE
notes = 'PKG000000008583' AND 
package_label = 'SYB_fixed_0472_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008238',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008238' LIMIT 1)
WHERE
notes = 'PKG000000008238' AND 
package_label = 'SYB_fixed_0127_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009148',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009148' LIMIT 1)
WHERE
notes = 'PKG000000009148' AND 
package_label = 'SYB_fixed_1037_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008753',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008753' LIMIT 1)
WHERE
notes = 'PKG000000008753' AND 
package_label = 'SYB_fixed_0642_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008442',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008442' LIMIT 1)
WHERE
notes = 'PKG000000008442' AND 
package_label = 'SYB_fixed_0331_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008328',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008328' LIMIT 1)
WHERE
notes = 'PKG000000008328' AND 
package_label = 'SYB_fixed_0217_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009168',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009168' LIMIT 1)
WHERE
notes = 'PKG000000009168' AND 
package_label = 'SYB_fixed_1057_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008416',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008416' LIMIT 1)
WHERE
notes = 'PKG000000008416' AND 
package_label = 'SYB_fixed_0305_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009074',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009074' LIMIT 1)
WHERE
notes = 'PKG000000009074' AND 
package_label = 'SYB_fixed_0963_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008785',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008785' LIMIT 1)
WHERE
notes = 'PKG000000008785' AND 
package_label = 'SYB_fixed_0674_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008614',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008614' LIMIT 1)
WHERE
notes = 'PKG000000008614' AND 
package_label = 'SYB_fixed_0503_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008989',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008989' LIMIT 1)
WHERE
notes = 'PKG000000008989' AND 
package_label = 'SYB_fixed_0878_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008217',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008217' LIMIT 1)
WHERE
notes = 'PKG000000008217' AND 
package_label = 'SYB_fixed_0106_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008857',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008857' LIMIT 1)
WHERE
notes = 'PKG000000008857' AND 
package_label = 'SYB_fixed_0746_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008559',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008559' LIMIT 1)
WHERE
notes = 'PKG000000008559' AND 
package_label = 'SYB_fixed_0448_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009003',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009003' LIMIT 1)
WHERE
notes = 'PKG000000009003' AND 
package_label = 'SYB_fixed_0892_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008215',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008215' LIMIT 1)
WHERE
notes = 'PKG000000008215' AND 
package_label = 'SYB_fixed_0104_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008506',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008506' LIMIT 1)
WHERE
notes = 'PKG000000008506' AND 
package_label = 'SYB_fixed_0395_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008510',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008510' LIMIT 1)
WHERE
notes = 'PKG000000008510' AND 
package_label = 'SYB_fixed_0399_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008528',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008528' LIMIT 1)
WHERE
notes = 'PKG000000008528' AND 
package_label = 'SYB_fixed_0417_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008307',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008307' LIMIT 1)
WHERE
notes = 'PKG000000008307' AND 
package_label = 'SYB_fixed_0196_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009027',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009027' LIMIT 1)
WHERE
notes = 'PKG000000009027' AND 
package_label = 'SYB_fixed_0916_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008607',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008607' LIMIT 1)
WHERE
notes = 'PKG000000008607' AND 
package_label = 'SYB_fixed_0496_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008770',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008770' LIMIT 1)
WHERE
notes = 'PKG000000008770' AND 
package_label = 'SYB_fixed_0659_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008430',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008430' LIMIT 1)
WHERE
notes = 'PKG000000008430' AND 
package_label = 'SYB_fixed_0319_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009169',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009169' LIMIT 1)
WHERE
notes = 'PKG000000009169' AND 
package_label = 'SYB_fixed_1058_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009067',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009067' LIMIT 1)
WHERE
notes = 'PKG000000009067' AND 
package_label = 'SYB_fixed_0956_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008532',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008532' LIMIT 1)
WHERE
notes = 'PKG000000008532' AND 
package_label = 'SYB_fixed_0421_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008384',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008384' LIMIT 1)
WHERE
notes = 'PKG000000008384' AND 
package_label = 'SYB_fixed_0273_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009031',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009031' LIMIT 1)
WHERE
notes = 'PKG000000009031' AND 
package_label = 'SYB_fixed_0920_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008174',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008174' LIMIT 1)
WHERE
notes = 'PKG000000008174' AND 
package_label = 'SYB_fixed_0063_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008186',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008186' LIMIT 1)
WHERE
notes = 'PKG000000008186' AND 
package_label = 'SYB_fixed_0075_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009208',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009208' LIMIT 1)
WHERE
notes = 'PKG000000009208' AND 
package_label = 'SYB_fixed_1097_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008728',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008728' LIMIT 1)
WHERE
notes = 'PKG000000008728' AND 
package_label = 'SYB_fixed_0617_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008772',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008772' LIMIT 1)
WHERE
notes = 'PKG000000008772' AND 
package_label = 'SYB_fixed_0661_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009122',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009122' LIMIT 1)
WHERE
notes = 'PKG000000009122' AND 
package_label = 'SYB_fixed_1011_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008188',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008188' LIMIT 1)
WHERE
notes = 'PKG000000008188' AND 
package_label = 'SYB_fixed_0077_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007365',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007365' LIMIT 1)
WHERE
notes = 'PKG000000007365' AND 
package_label = 'SYB_fixed_0006_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008771',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008771' LIMIT 1)
WHERE
notes = 'PKG000000008771' AND 
package_label = 'SYB_fixed_0660_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008306',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008306' LIMIT 1)
WHERE
notes = 'PKG000000008306' AND 
package_label = 'SYB_fixed_0195_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009255',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009255' LIMIT 1)
WHERE
notes = 'PKG000000009255' AND 
package_label = 'SYB_fixed_1144_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008858',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008858' LIMIT 1)
WHERE
notes = 'PKG000000008858' AND 
package_label = 'SYB_fixed_0747_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008690',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008690' LIMIT 1)
WHERE
notes = 'PKG000000008690' AND 
package_label = 'SYB_fixed_0579_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008827',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008827' LIMIT 1)
WHERE
notes = 'PKG000000008827' AND 
package_label = 'SYB_fixed_0716_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009049',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009049' LIMIT 1)
WHERE
notes = 'PKG000000009049' AND 
package_label = 'SYB_fixed_0938_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008896',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008896' LIMIT 1)
WHERE
notes = 'PKG000000008896' AND 
package_label = 'SYB_fixed_0785_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008634',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008634' LIMIT 1)
WHERE
notes = 'PKG000000008634' AND 
package_label = 'SYB_fixed_0523_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008906',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008906' LIMIT 1)
WHERE
notes = 'PKG000000008906' AND 
package_label = 'SYB_fixed_0795_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008969',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008969' LIMIT 1)
WHERE
notes = 'PKG000000008969' AND 
package_label = 'SYB_fixed_0858_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008808',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008808' LIMIT 1)
WHERE
notes = 'PKG000000008808' AND 
package_label = 'SYB_fixed_0697_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008683',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008683' LIMIT 1)
WHERE
notes = 'PKG000000008683' AND 
package_label = 'SYB_fixed_0572_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008389',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008389' LIMIT 1)
WHERE
notes = 'PKG000000008389' AND 
package_label = 'SYB_fixed_0278_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008477',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008477' LIMIT 1)
WHERE
notes = 'PKG000000008477' AND 
package_label = 'SYB_fixed_0366_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009167',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009167' LIMIT 1)
WHERE
notes = 'PKG000000009167' AND 
package_label = 'SYB_fixed_1056_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008255',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008255' LIMIT 1)
WHERE
notes = 'PKG000000008255' AND 
package_label = 'SYB_fixed_0144_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008294',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008294' LIMIT 1)
WHERE
notes = 'PKG000000008294' AND 
package_label = 'SYB_fixed_0183_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008832',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008832' LIMIT 1)
WHERE
notes = 'PKG000000008832' AND 
package_label = 'SYB_fixed_0721_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008581',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008581' LIMIT 1)
WHERE
notes = 'PKG000000008581' AND 
package_label = 'SYB_fixed_0470_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008733',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008733' LIMIT 1)
WHERE
notes = 'PKG000000008733' AND 
package_label = 'SYB_fixed_0622_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008677',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008677' LIMIT 1)
WHERE
notes = 'PKG000000008677' AND 
package_label = 'SYB_fixed_0566_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009023',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009023' LIMIT 1)
WHERE
notes = 'PKG000000009023' AND 
package_label = 'SYB_fixed_0912_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008462',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008462' LIMIT 1)
WHERE
notes = 'PKG000000008462' AND 
package_label = 'SYB_fixed_0351_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008242',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008242' LIMIT 1)
WHERE
notes = 'PKG000000008242' AND 
package_label = 'SYB_fixed_0131_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008558',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008558' LIMIT 1)
WHERE
notes = 'PKG000000008558' AND 
package_label = 'SYB_fixed_0447_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009129',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009129' LIMIT 1)
WHERE
notes = 'PKG000000009129' AND 
package_label = 'SYB_fixed_1018_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009271',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009271' LIMIT 1)
WHERE
notes = 'PKG000000009271' AND 
package_label = 'SYB_fixed_1160_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008691',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008691' LIMIT 1)
WHERE
notes = 'PKG000000008691' AND 
package_label = 'SYB_fixed_0580_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008698',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008698' LIMIT 1)
WHERE
notes = 'PKG000000008698' AND 
package_label = 'SYB_fixed_0587_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008439',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008439' LIMIT 1)
WHERE
notes = 'PKG000000008439' AND 
package_label = 'SYB_fixed_0328_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009247',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009247' LIMIT 1)
WHERE
notes = 'PKG000000009247' AND 
package_label = 'SYB_fixed_1136_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009180',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009180' LIMIT 1)
WHERE
notes = 'PKG000000009180' AND 
package_label = 'SYB_fixed_1069_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009214',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009214' LIMIT 1)
WHERE
notes = 'PKG000000009214' AND 
package_label = 'SYB_fixed_1103_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008539',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008539' LIMIT 1)
WHERE
notes = 'PKG000000008539' AND 
package_label = 'SYB_fixed_0428_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009016',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009016' LIMIT 1)
WHERE
notes = 'PKG000000009016' AND 
package_label = 'SYB_fixed_0905_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007393',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007393' LIMIT 1)
WHERE
notes = 'PKG000000007393' AND 
package_label = 'SYB_fixed_0034_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009047',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009047' LIMIT 1)
WHERE
notes = 'PKG000000009047' AND 
package_label = 'SYB_fixed_0936_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008838',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008838' LIMIT 1)
WHERE
notes = 'PKG000000008838' AND 
package_label = 'SYB_fixed_0727_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008602',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008602' LIMIT 1)
WHERE
notes = 'PKG000000008602' AND 
package_label = 'SYB_fixed_0491_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008716',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008716' LIMIT 1)
WHERE
notes = 'PKG000000008716' AND 
package_label = 'SYB_fixed_0605_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008619',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008619' LIMIT 1)
WHERE
notes = 'PKG000000008619' AND 
package_label = 'SYB_fixed_0508_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008630',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008630' LIMIT 1)
WHERE
notes = 'PKG000000008630' AND 
package_label = 'SYB_fixed_0519_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009198',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009198' LIMIT 1)
WHERE
notes = 'PKG000000009198' AND 
package_label = 'SYB_fixed_1087_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009004',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009004' LIMIT 1)
WHERE
notes = 'PKG000000009004' AND 
package_label = 'SYB_fixed_0893_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008271',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008271' LIMIT 1)
WHERE
notes = 'PKG000000008271' AND 
package_label = 'SYB_fixed_0160_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008625',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008625' LIMIT 1)
WHERE
notes = 'PKG000000008625' AND 
package_label = 'SYB_fixed_0514_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008175',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008175' LIMIT 1)
WHERE
notes = 'PKG000000008175' AND 
package_label = 'SYB_fixed_0064_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008189',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008189' LIMIT 1)
WHERE
notes = 'PKG000000008189' AND 
package_label = 'SYB_fixed_0078_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008379',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008379' LIMIT 1)
WHERE
notes = 'PKG000000008379' AND 
package_label = 'SYB_fixed_0268_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008560',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008560' LIMIT 1)
WHERE
notes = 'PKG000000008560' AND 
package_label = 'SYB_fixed_0449_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008253',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008253' LIMIT 1)
WHERE
notes = 'PKG000000008253' AND 
package_label = 'SYB_fixed_0142_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008966',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008966' LIMIT 1)
WHERE
notes = 'PKG000000008966' AND 
package_label = 'SYB_fixed_0855_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008314',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008314' LIMIT 1)
WHERE
notes = 'PKG000000008314' AND 
package_label = 'SYB_fixed_0203_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000008490',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000008490' LIMIT 1)
WHERE
notes = 'PKG000000008490' AND 
package_label = 'SYB_fixed_0379_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009145',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009145' LIMIT 1)
WHERE
notes = 'PKG000000009145' AND 
package_label = 'SYB_fixed_1034_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000009199',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000009199' LIMIT 1)
WHERE
notes = 'PKG000000009199' AND 
package_label = 'SYB_fixed_1088_PACKAGE-001'; 

UPDATE germplasm.package
SET
notes = 'SYB-PKG000000007402',
seed_id = (SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007402' LIMIT 1)
WHERE
notes = 'PKG000000007402' AND 
package_label = 'SYB_fixed_0043_PACKAGE-001'; 



--rollback SELECT NULL;