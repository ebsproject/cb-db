--liquibase formatted sql

--changeset postgres:update_SYB_fixture_germplasm_seed_1  context:fixture splitStatements:false rollbackSplitStatements:false
--comment:BDS-3516 Update Soybean fixture germplasm_seed



UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008827'
WHERE
notes = 'SEED000000008827' AND 
seed_name = 'SYB_fixed_0716_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008180'
WHERE
notes = 'SEED000000008180' AND 
seed_name = 'SYB_fixed_0069_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008281'
WHERE
notes = 'SEED000000008281' AND 
seed_name = 'SYB_fixed_0170_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009121'
WHERE
notes = 'SEED000000009121' AND 
seed_name = 'SYB_fixed_1010_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009096'
WHERE
notes = 'SEED000000009096' AND 
seed_name = 'SYB_fixed_0985_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007393'
WHERE
notes = 'SEED000000007393' AND 
seed_name = 'SYB_fixed_0034_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008486'
WHERE
notes = 'SEED000000008486' AND 
seed_name = 'SYB_fixed_0375_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009164'
WHERE
notes = 'SEED000000009164' AND 
seed_name = 'SYB_fixed_1053_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008579'
WHERE
notes = 'SEED000000008579' AND 
seed_name = 'SYB_fixed_0468_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008469'
WHERE
notes = 'SEED000000008469' AND 
seed_name = 'SYB_fixed_0358_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009177'
WHERE
notes = 'SEED000000009177' AND 
seed_name = 'SYB_fixed_1066_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008658'
WHERE
notes = 'SEED000000008658' AND 
seed_name = 'SYB_fixed_0547_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008932'
WHERE
notes = 'SEED000000008932' AND 
seed_name = 'SYB_fixed_0821_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008670'
WHERE
notes = 'SEED000000008670' AND 
seed_name = 'SYB_fixed_0559_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007384'
WHERE
notes = 'SEED000000007384' AND 
seed_name = 'SYB_fixed_0025_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008811'
WHERE
notes = 'SEED000000008811' AND 
seed_name = 'SYB_fixed_0700_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008515'
WHERE
notes = 'SEED000000008515' AND 
seed_name = 'SYB_fixed_0404_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008625'
WHERE
notes = 'SEED000000008625' AND 
seed_name = 'SYB_fixed_0514_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008538'
WHERE
notes = 'SEED000000008538' AND 
seed_name = 'SYB_fixed_0427_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008708'
WHERE
notes = 'SEED000000008708' AND 
seed_name = 'SYB_fixed_0597_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008944'
WHERE
notes = 'SEED000000008944' AND 
seed_name = 'SYB_fixed_0833_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008578'
WHERE
notes = 'SEED000000008578' AND 
seed_name = 'SYB_fixed_0467_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008277'
WHERE
notes = 'SEED000000008277' AND 
seed_name = 'SYB_fixed_0166_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008375'
WHERE
notes = 'SEED000000008375' AND 
seed_name = 'SYB_fixed_0264_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008418'
WHERE
notes = 'SEED000000008418' AND 
seed_name = 'SYB_fixed_0307_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008822'
WHERE
notes = 'SEED000000008822' AND 
seed_name = 'SYB_fixed_0711_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008702'
WHERE
notes = 'SEED000000008702' AND 
seed_name = 'SYB_fixed_0591_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008485'
WHERE
notes = 'SEED000000008485' AND 
seed_name = 'SYB_fixed_0374_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008236'
WHERE
notes = 'SEED000000008236' AND 
seed_name = 'SYB_fixed_0125_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008977'
WHERE
notes = 'SEED000000008977' AND 
seed_name = 'SYB_fixed_0866_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008934'
WHERE
notes = 'SEED000000008934' AND 
seed_name = 'SYB_fixed_0823_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008373'
WHERE
notes = 'SEED000000008373' AND 
seed_name = 'SYB_fixed_0262_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008651'
WHERE
notes = 'SEED000000008651' AND 
seed_name = 'SYB_fixed_0540_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008354'
WHERE
notes = 'SEED000000008354' AND 
seed_name = 'SYB_fixed_0243_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008732'
WHERE
notes = 'SEED000000008732' AND 
seed_name = 'SYB_fixed_0621_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008293'
WHERE
notes = 'SEED000000008293' AND 
seed_name = 'SYB_fixed_0182_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008836'
WHERE
notes = 'SEED000000008836' AND 
seed_name = 'SYB_fixed_0725_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008867'
WHERE
notes = 'SEED000000008867' AND 
seed_name = 'SYB_fixed_0756_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008387'
WHERE
notes = 'SEED000000008387' AND 
seed_name = 'SYB_fixed_0276_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009171'
WHERE
notes = 'SEED000000009171' AND 
seed_name = 'SYB_fixed_1060_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008600'
WHERE
notes = 'SEED000000008600' AND 
seed_name = 'SYB_fixed_0489_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008326'
WHERE
notes = 'SEED000000008326' AND 
seed_name = 'SYB_fixed_0215_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008580'
WHERE
notes = 'SEED000000008580' AND 
seed_name = 'SYB_fixed_0469_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009151'
WHERE
notes = 'SEED000000009151' AND 
seed_name = 'SYB_fixed_1040_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008289'
WHERE
notes = 'SEED000000008289' AND 
seed_name = 'SYB_fixed_0178_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008342'
WHERE
notes = 'SEED000000008342' AND 
seed_name = 'SYB_fixed_0231_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008310'
WHERE
notes = 'SEED000000008310' AND 
seed_name = 'SYB_fixed_0199_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008908'
WHERE
notes = 'SEED000000008908' AND 
seed_name = 'SYB_fixed_0797_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008333'
WHERE
notes = 'SEED000000008333' AND 
seed_name = 'SYB_fixed_0222_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008931'
WHERE
notes = 'SEED000000008931' AND 
seed_name = 'SYB_fixed_0820_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008357'
WHERE
notes = 'SEED000000008357' AND 
seed_name = 'SYB_fixed_0246_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008311'
WHERE
notes = 'SEED000000008311' AND 
seed_name = 'SYB_fixed_0200_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008163'
WHERE
notes = 'SEED000000008163' AND 
seed_name = 'SYB_fixed_0052_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008389'
WHERE
notes = 'SEED000000008389' AND 
seed_name = 'SYB_fixed_0278_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008227'
WHERE
notes = 'SEED000000008227' AND 
seed_name = 'SYB_fixed_0116_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009110'
WHERE
notes = 'SEED000000009110' AND 
seed_name = 'SYB_fixed_0999_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008171'
WHERE
notes = 'SEED000000008171' AND 
seed_name = 'SYB_fixed_0060_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008894'
WHERE
notes = 'SEED000000008894' AND 
seed_name = 'SYB_fixed_0783_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008582'
WHERE
notes = 'SEED000000008582' AND 
seed_name = 'SYB_fixed_0471_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008291'
WHERE
notes = 'SEED000000008291' AND 
seed_name = 'SYB_fixed_0180_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008598'
WHERE
notes = 'SEED000000008598' AND 
seed_name = 'SYB_fixed_0487_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009099'
WHERE
notes = 'SEED000000009099' AND 
seed_name = 'SYB_fixed_0988_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009105'
WHERE
notes = 'SEED000000009105' AND 
seed_name = 'SYB_fixed_0994_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008799'
WHERE
notes = 'SEED000000008799' AND 
seed_name = 'SYB_fixed_0688_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008810'
WHERE
notes = 'SEED000000008810' AND 
seed_name = 'SYB_fixed_0699_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008768'
WHERE
notes = 'SEED000000008768' AND 
seed_name = 'SYB_fixed_0657_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008473'
WHERE
notes = 'SEED000000008473' AND 
seed_name = 'SYB_fixed_0362_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009006'
WHERE
notes = 'SEED000000009006' AND 
seed_name = 'SYB_fixed_0895_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007362'
WHERE
notes = 'SEED000000007362' AND 
seed_name = 'SYB_fixed_0003_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007368'
WHERE
notes = 'SEED000000007368' AND 
seed_name = 'SYB_fixed_0009_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009009'
WHERE
notes = 'SEED000000009009' AND 
seed_name = 'SYB_fixed_0898_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008420'
WHERE
notes = 'SEED000000008420' AND 
seed_name = 'SYB_fixed_0309_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008976'
WHERE
notes = 'SEED000000008976' AND 
seed_name = 'SYB_fixed_0865_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009225'
WHERE
notes = 'SEED000000009225' AND 
seed_name = 'SYB_fixed_1114_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008176'
WHERE
notes = 'SEED000000008176' AND 
seed_name = 'SYB_fixed_0065_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008508'
WHERE
notes = 'SEED000000008508' AND 
seed_name = 'SYB_fixed_0397_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008857'
WHERE
notes = 'SEED000000008857' AND 
seed_name = 'SYB_fixed_0746_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009203'
WHERE
notes = 'SEED000000009203' AND 
seed_name = 'SYB_fixed_1092_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009115'
WHERE
notes = 'SEED000000009115' AND 
seed_name = 'SYB_fixed_1004_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009195'
WHERE
notes = 'SEED000000009195' AND 
seed_name = 'SYB_fixed_1084_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008984'
WHERE
notes = 'SEED000000008984' AND 
seed_name = 'SYB_fixed_0873_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008970'
WHERE
notes = 'SEED000000008970' AND 
seed_name = 'SYB_fixed_0859_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008177'
WHERE
notes = 'SEED000000008177' AND 
seed_name = 'SYB_fixed_0066_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008714'
WHERE
notes = 'SEED000000008714' AND 
seed_name = 'SYB_fixed_0603_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009081'
WHERE
notes = 'SEED000000009081' AND 
seed_name = 'SYB_fixed_0970_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008270'
WHERE
notes = 'SEED000000008270' AND 
seed_name = 'SYB_fixed_0159_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008891'
WHERE
notes = 'SEED000000008891' AND 
seed_name = 'SYB_fixed_0780_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008785'
WHERE
notes = 'SEED000000008785' AND 
seed_name = 'SYB_fixed_0674_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008433'
WHERE
notes = 'SEED000000008433' AND 
seed_name = 'SYB_fixed_0322_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009217'
WHERE
notes = 'SEED000000009217' AND 
seed_name = 'SYB_fixed_1106_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008256'
WHERE
notes = 'SEED000000008256' AND 
seed_name = 'SYB_fixed_0145_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008806'
WHERE
notes = 'SEED000000008806' AND 
seed_name = 'SYB_fixed_0695_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008314'
WHERE
notes = 'SEED000000008314' AND 
seed_name = 'SYB_fixed_0203_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008392'
WHERE
notes = 'SEED000000008392' AND 
seed_name = 'SYB_fixed_0281_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008522'
WHERE
notes = 'SEED000000008522' AND 
seed_name = 'SYB_fixed_0411_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009138'
WHERE
notes = 'SEED000000009138' AND 
seed_name = 'SYB_fixed_1027_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009133'
WHERE
notes = 'SEED000000009133' AND 
seed_name = 'SYB_fixed_1022_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009126'
WHERE
notes = 'SEED000000009126' AND 
seed_name = 'SYB_fixed_1015_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008327'
WHERE
notes = 'SEED000000008327' AND 
seed_name = 'SYB_fixed_0216_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008259'
WHERE
notes = 'SEED000000008259' AND 
seed_name = 'SYB_fixed_0148_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008425'
WHERE
notes = 'SEED000000008425' AND 
seed_name = 'SYB_fixed_0314_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008918'
WHERE
notes = 'SEED000000008918' AND 
seed_name = 'SYB_fixed_0807_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008814'
WHERE
notes = 'SEED000000008814' AND 
seed_name = 'SYB_fixed_0703_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007380'
WHERE
notes = 'SEED000000007380' AND 
seed_name = 'SYB_fixed_0021_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008537'
WHERE
notes = 'SEED000000008537' AND 
seed_name = 'SYB_fixed_0426_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008730'
WHERE
notes = 'SEED000000008730' AND 
seed_name = 'SYB_fixed_0619_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009102'
WHERE
notes = 'SEED000000009102' AND 
seed_name = 'SYB_fixed_0991_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009282'
WHERE
notes = 'SEED000000009282' AND 
seed_name = 'SYB_fixed_1171_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009037'
WHERE
notes = 'SEED000000009037' AND 
seed_name = 'SYB_fixed_0926_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009067'
WHERE
notes = 'SEED000000009067' AND 
seed_name = 'SYB_fixed_0956_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008468'
WHERE
notes = 'SEED000000008468' AND 
seed_name = 'SYB_fixed_0357_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008610'
WHERE
notes = 'SEED000000008610' AND 
seed_name = 'SYB_fixed_0499_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008181'
WHERE
notes = 'SEED000000008181' AND 
seed_name = 'SYB_fixed_0070_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008999'
WHERE
notes = 'SEED000000008999' AND 
seed_name = 'SYB_fixed_0888_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009078'
WHERE
notes = 'SEED000000009078' AND 
seed_name = 'SYB_fixed_0967_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008668'
WHERE
notes = 'SEED000000008668' AND 
seed_name = 'SYB_fixed_0557_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008251'
WHERE
notes = 'SEED000000008251' AND 
seed_name = 'SYB_fixed_0140_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009285'
WHERE
notes = 'SEED000000009285' AND 
seed_name = 'SYB_fixed_1174_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008592'
WHERE
notes = 'SEED000000008592' AND 
seed_name = 'SYB_fixed_0481_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009152'
WHERE
notes = 'SEED000000009152' AND 
seed_name = 'SYB_fixed_1041_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008929'
WHERE
notes = 'SEED000000008929' AND 
seed_name = 'SYB_fixed_0818_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008952'
WHERE
notes = 'SEED000000008952' AND 
seed_name = 'SYB_fixed_0841_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009263'
WHERE
notes = 'SEED000000009263' AND 
seed_name = 'SYB_fixed_1152_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008182'
WHERE
notes = 'SEED000000008182' AND 
seed_name = 'SYB_fixed_0071_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009112'
WHERE
notes = 'SEED000000009112' AND 
seed_name = 'SYB_fixed_1001_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008507'
WHERE
notes = 'SEED000000008507' AND 
seed_name = 'SYB_fixed_0396_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008669'
WHERE
notes = 'SEED000000008669' AND 
seed_name = 'SYB_fixed_0558_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008220'
WHERE
notes = 'SEED000000008220' AND 
seed_name = 'SYB_fixed_0109_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008575'
WHERE
notes = 'SEED000000008575' AND 
seed_name = 'SYB_fixed_0464_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008328'
WHERE
notes = 'SEED000000008328' AND 
seed_name = 'SYB_fixed_0217_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008196'
WHERE
notes = 'SEED000000008196' AND 
seed_name = 'SYB_fixed_0085_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008496'
WHERE
notes = 'SEED000000008496' AND 
seed_name = 'SYB_fixed_0385_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009104'
WHERE
notes = 'SEED000000009104' AND 
seed_name = 'SYB_fixed_0993_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008536'
WHERE
notes = 'SEED000000008536' AND 
seed_name = 'SYB_fixed_0425_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008621'
WHERE
notes = 'SEED000000008621' AND 
seed_name = 'SYB_fixed_0510_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008793'
WHERE
notes = 'SEED000000008793' AND 
seed_name = 'SYB_fixed_0682_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008953'
WHERE
notes = 'SEED000000008953' AND 
seed_name = 'SYB_fixed_0842_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008993'
WHERE
notes = 'SEED000000008993' AND 
seed_name = 'SYB_fixed_0882_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008781'
WHERE
notes = 'SEED000000008781' AND 
seed_name = 'SYB_fixed_0670_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008424'
WHERE
notes = 'SEED000000008424' AND 
seed_name = 'SYB_fixed_0313_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008713'
WHERE
notes = 'SEED000000008713' AND 
seed_name = 'SYB_fixed_0602_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009185'
WHERE
notes = 'SEED000000009185' AND 
seed_name = 'SYB_fixed_1074_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008437'
WHERE
notes = 'SEED000000008437' AND 
seed_name = 'SYB_fixed_0326_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008555'
WHERE
notes = 'SEED000000008555' AND 
seed_name = 'SYB_fixed_0444_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008475'
WHERE
notes = 'SEED000000008475' AND 
seed_name = 'SYB_fixed_0364_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008676'
WHERE
notes = 'SEED000000008676' AND 
seed_name = 'SYB_fixed_0565_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008853'
WHERE
notes = 'SEED000000008853' AND 
seed_name = 'SYB_fixed_0742_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008616'
WHERE
notes = 'SEED000000008616' AND 
seed_name = 'SYB_fixed_0505_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008472'
WHERE
notes = 'SEED000000008472' AND 
seed_name = 'SYB_fixed_0361_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008762'
WHERE
notes = 'SEED000000008762' AND 
seed_name = 'SYB_fixed_0651_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008279'
WHERE
notes = 'SEED000000008279' AND 
seed_name = 'SYB_fixed_0168_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007367'
WHERE
notes = 'SEED000000007367' AND 
seed_name = 'SYB_fixed_0008_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008272'
WHERE
notes = 'SEED000000008272' AND 
seed_name = 'SYB_fixed_0161_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009107'
WHERE
notes = 'SEED000000009107' AND 
seed_name = 'SYB_fixed_0996_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008667'
WHERE
notes = 'SEED000000008667' AND 
seed_name = 'SYB_fixed_0556_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008567'
WHERE
notes = 'SEED000000008567' AND 
seed_name = 'SYB_fixed_0456_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009142'
WHERE
notes = 'SEED000000009142' AND 
seed_name = 'SYB_fixed_1031_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008816'
WHERE
notes = 'SEED000000008816' AND 
seed_name = 'SYB_fixed_0705_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008183'
WHERE
notes = 'SEED000000008183' AND 
seed_name = 'SYB_fixed_0072_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008294'
WHERE
notes = 'SEED000000008294' AND 
seed_name = 'SYB_fixed_0183_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008728'
WHERE
notes = 'SEED000000008728' AND 
seed_name = 'SYB_fixed_0617_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009001'
WHERE
notes = 'SEED000000009001' AND 
seed_name = 'SYB_fixed_0890_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009074'
WHERE
notes = 'SEED000000009074' AND 
seed_name = 'SYB_fixed_0963_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009172'
WHERE
notes = 'SEED000000009172' AND 
seed_name = 'SYB_fixed_1061_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008439'
WHERE
notes = 'SEED000000008439' AND 
seed_name = 'SYB_fixed_0328_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009198'
WHERE
notes = 'SEED000000009198' AND 
seed_name = 'SYB_fixed_1087_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008866'
WHERE
notes = 'SEED000000008866' AND 
seed_name = 'SYB_fixed_0755_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008557'
WHERE
notes = 'SEED000000008557' AND 
seed_name = 'SYB_fixed_0446_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008467'
WHERE
notes = 'SEED000000008467' AND 
seed_name = 'SYB_fixed_0356_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008638'
WHERE
notes = 'SEED000000008638' AND 
seed_name = 'SYB_fixed_0527_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008398'
WHERE
notes = 'SEED000000008398' AND 
seed_name = 'SYB_fixed_0287_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009089'
WHERE
notes = 'SEED000000009089' AND 
seed_name = 'SYB_fixed_0978_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009026'
WHERE
notes = 'SEED000000009026' AND 
seed_name = 'SYB_fixed_0915_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009230'
WHERE
notes = 'SEED000000009230' AND 
seed_name = 'SYB_fixed_1119_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008901'
WHERE
notes = 'SEED000000008901' AND 
seed_name = 'SYB_fixed_0790_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008516'
WHERE
notes = 'SEED000000008516' AND 
seed_name = 'SYB_fixed_0405_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008500'
WHERE
notes = 'SEED000000008500' AND 
seed_name = 'SYB_fixed_0389_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009007'
WHERE
notes = 'SEED000000009007' AND 
seed_name = 'SYB_fixed_0896_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008440'
WHERE
notes = 'SEED000000008440' AND 
seed_name = 'SYB_fixed_0329_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009291'
WHERE
notes = 'SEED000000009291' AND 
seed_name = 'SYB_fixed_1180_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008885'
WHERE
notes = 'SEED000000008885' AND 
seed_name = 'SYB_fixed_0774_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008313'
WHERE
notes = 'SEED000000008313' AND 
seed_name = 'SYB_fixed_0202_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008233'
WHERE
notes = 'SEED000000008233' AND 
seed_name = 'SYB_fixed_0122_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008407'
WHERE
notes = 'SEED000000008407' AND 
seed_name = 'SYB_fixed_0296_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008692'
WHERE
notes = 'SEED000000008692' AND 
seed_name = 'SYB_fixed_0581_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008719'
WHERE
notes = 'SEED000000008719' AND 
seed_name = 'SYB_fixed_0608_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008225'
WHERE
notes = 'SEED000000008225' AND 
seed_name = 'SYB_fixed_0114_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008988'
WHERE
notes = 'SEED000000008988' AND 
seed_name = 'SYB_fixed_0877_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008197'
WHERE
notes = 'SEED000000008197' AND 
seed_name = 'SYB_fixed_0086_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008351'
WHERE
notes = 'SEED000000008351' AND 
seed_name = 'SYB_fixed_0240_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008334'
WHERE
notes = 'SEED000000008334' AND 
seed_name = 'SYB_fixed_0223_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008739'
WHERE
notes = 'SEED000000008739' AND 
seed_name = 'SYB_fixed_0628_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009202'
WHERE
notes = 'SEED000000009202' AND 
seed_name = 'SYB_fixed_1091_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009136'
WHERE
notes = 'SEED000000009136' AND 
seed_name = 'SYB_fixed_1025_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008948'
WHERE
notes = 'SEED000000008948' AND 
seed_name = 'SYB_fixed_0837_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008904'
WHERE
notes = 'SEED000000008904' AND 
seed_name = 'SYB_fixed_0793_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008513'
WHERE
notes = 'SEED000000008513' AND 
seed_name = 'SYB_fixed_0402_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009049'
WHERE
notes = 'SEED000000009049' AND 
seed_name = 'SYB_fixed_0938_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008505'
WHERE
notes = 'SEED000000008505' AND 
seed_name = 'SYB_fixed_0394_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008455'
WHERE
notes = 'SEED000000008455' AND 
seed_name = 'SYB_fixed_0344_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007361'
WHERE
notes = 'SEED000000007361' AND 
seed_name = 'SYB_fixed_0002_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008647'
WHERE
notes = 'SEED000000008647' AND 
seed_name = 'SYB_fixed_0536_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008212'
WHERE
notes = 'SEED000000008212' AND 
seed_name = 'SYB_fixed_0101_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009205'
WHERE
notes = 'SEED000000009205' AND 
seed_name = 'SYB_fixed_1094_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008510'
WHERE
notes = 'SEED000000008510' AND 
seed_name = 'SYB_fixed_0399_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008707'
WHERE
notes = 'SEED000000008707' AND 
seed_name = 'SYB_fixed_0596_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008630'
WHERE
notes = 'SEED000000008630' AND 
seed_name = 'SYB_fixed_0519_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009094'
WHERE
notes = 'SEED000000009094' AND 
seed_name = 'SYB_fixed_0983_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009000'
WHERE
notes = 'SEED000000009000' AND 
seed_name = 'SYB_fixed_0889_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008556'
WHERE
notes = 'SEED000000008556' AND 
seed_name = 'SYB_fixed_0445_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008777'
WHERE
notes = 'SEED000000008777' AND 
seed_name = 'SYB_fixed_0666_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008665'
WHERE
notes = 'SEED000000008665' AND 
seed_name = 'SYB_fixed_0554_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008385'
WHERE
notes = 'SEED000000008385' AND 
seed_name = 'SYB_fixed_0274_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008687'
WHERE
notes = 'SEED000000008687' AND 
seed_name = 'SYB_fixed_0576_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009250'
WHERE
notes = 'SEED000000009250' AND 
seed_name = 'SYB_fixed_1139_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008851'
WHERE
notes = 'SEED000000008851' AND 
seed_name = 'SYB_fixed_0740_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008759'
WHERE
notes = 'SEED000000008759' AND 
seed_name = 'SYB_fixed_0648_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008332'
WHERE
notes = 'SEED000000008332' AND 
seed_name = 'SYB_fixed_0221_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008335'
WHERE
notes = 'SEED000000008335' AND 
seed_name = 'SYB_fixed_0224_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008465'
WHERE
notes = 'SEED000000008465' AND 
seed_name = 'SYB_fixed_0354_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008696'
WHERE
notes = 'SEED000000008696' AND 
seed_name = 'SYB_fixed_0585_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008198'
WHERE
notes = 'SEED000000008198' AND 
seed_name = 'SYB_fixed_0087_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009128'
WHERE
notes = 'SEED000000009128' AND 
seed_name = 'SYB_fixed_1017_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009111'
WHERE
notes = 'SEED000000009111' AND 
seed_name = 'SYB_fixed_1000_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008699'
WHERE
notes = 'SEED000000008699' AND 
seed_name = 'SYB_fixed_0588_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008864'
WHERE
notes = 'SEED000000008864' AND 
seed_name = 'SYB_fixed_0753_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008231'
WHERE
notes = 'SEED000000008231' AND 
seed_name = 'SYB_fixed_0120_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009040'
WHERE
notes = 'SEED000000009040' AND 
seed_name = 'SYB_fixed_0929_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008321'
WHERE
notes = 'SEED000000008321' AND 
seed_name = 'SYB_fixed_0210_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008791'
WHERE
notes = 'SEED000000008791' AND 
seed_name = 'SYB_fixed_0680_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008179'
WHERE
notes = 'SEED000000008179' AND 
seed_name = 'SYB_fixed_0068_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008429'
WHERE
notes = 'SEED000000008429' AND 
seed_name = 'SYB_fixed_0318_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009003'
WHERE
notes = 'SEED000000009003' AND 
seed_name = 'SYB_fixed_0892_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008937'
WHERE
notes = 'SEED000000008937' AND 
seed_name = 'SYB_fixed_0826_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008543'
WHERE
notes = 'SEED000000008543' AND 
seed_name = 'SYB_fixed_0432_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008689'
WHERE
notes = 'SEED000000008689' AND 
seed_name = 'SYB_fixed_0578_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008411'
WHERE
notes = 'SEED000000008411' AND 
seed_name = 'SYB_fixed_0300_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008526'
WHERE
notes = 'SEED000000008526' AND 
seed_name = 'SYB_fixed_0415_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008819'
WHERE
notes = 'SEED000000008819' AND 
seed_name = 'SYB_fixed_0708_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008869'
WHERE
notes = 'SEED000000008869' AND 
seed_name = 'SYB_fixed_0758_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009139'
WHERE
notes = 'SEED000000009139' AND 
seed_name = 'SYB_fixed_1028_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008298'
WHERE
notes = 'SEED000000008298' AND 
seed_name = 'SYB_fixed_0187_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008879'
WHERE
notes = 'SEED000000008879' AND 
seed_name = 'SYB_fixed_0768_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008319'
WHERE
notes = 'SEED000000008319' AND 
seed_name = 'SYB_fixed_0208_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008466'
WHERE
notes = 'SEED000000008466' AND 
seed_name = 'SYB_fixed_0355_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008870'
WHERE
notes = 'SEED000000008870' AND 
seed_name = 'SYB_fixed_0759_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009153'
WHERE
notes = 'SEED000000009153' AND 
seed_name = 'SYB_fixed_1042_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008893'
WHERE
notes = 'SEED000000008893' AND 
seed_name = 'SYB_fixed_0782_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008229'
WHERE
notes = 'SEED000000008229' AND 
seed_name = 'SYB_fixed_0118_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008906'
WHERE
notes = 'SEED000000008906' AND 
seed_name = 'SYB_fixed_0795_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009180'
WHERE
notes = 'SEED000000009180' AND 
seed_name = 'SYB_fixed_1069_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008423'
WHERE
notes = 'SEED000000008423' AND 
seed_name = 'SYB_fixed_0312_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008287'
WHERE
notes = 'SEED000000008287' AND 
seed_name = 'SYB_fixed_0176_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008742'
WHERE
notes = 'SEED000000008742' AND 
seed_name = 'SYB_fixed_0631_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007363'
WHERE
notes = 'SEED000000007363' AND 
seed_name = 'SYB_fixed_0004_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008876'
WHERE
notes = 'SEED000000008876' AND 
seed_name = 'SYB_fixed_0765_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007370'
WHERE
notes = 'SEED000000007370' AND 
seed_name = 'SYB_fixed_0011_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008353'
WHERE
notes = 'SEED000000008353' AND 
seed_name = 'SYB_fixed_0242_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008274'
WHERE
notes = 'SEED000000008274' AND 
seed_name = 'SYB_fixed_0163_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008297'
WHERE
notes = 'SEED000000008297' AND 
seed_name = 'SYB_fixed_0186_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008266'
WHERE
notes = 'SEED000000008266' AND 
seed_name = 'SYB_fixed_0155_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008774'
WHERE
notes = 'SEED000000008774' AND 
seed_name = 'SYB_fixed_0663_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008649'
WHERE
notes = 'SEED000000008649' AND 
seed_name = 'SYB_fixed_0538_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008206'
WHERE
notes = 'SEED000000008206' AND 
seed_name = 'SYB_fixed_0095_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008666'
WHERE
notes = 'SEED000000008666' AND 
seed_name = 'SYB_fixed_0555_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009054'
WHERE
notes = 'SEED000000009054' AND 
seed_name = 'SYB_fixed_0943_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009236'
WHERE
notes = 'SEED000000009236' AND 
seed_name = 'SYB_fixed_1125_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009070'
WHERE
notes = 'SEED000000009070' AND 
seed_name = 'SYB_fixed_0959_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008362'
WHERE
notes = 'SEED000000008362' AND 
seed_name = 'SYB_fixed_0251_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009055'
WHERE
notes = 'SEED000000009055' AND 
seed_name = 'SYB_fixed_0944_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008787'
WHERE
notes = 'SEED000000008787' AND 
seed_name = 'SYB_fixed_0676_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009148'
WHERE
notes = 'SEED000000009148' AND 
seed_name = 'SYB_fixed_1037_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009252'
WHERE
notes = 'SEED000000009252' AND 
seed_name = 'SYB_fixed_1141_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009127'
WHERE
notes = 'SEED000000009127' AND 
seed_name = 'SYB_fixed_1016_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008404'
WHERE
notes = 'SEED000000008404' AND 
seed_name = 'SYB_fixed_0293_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008628'
WHERE
notes = 'SEED000000008628' AND 
seed_name = 'SYB_fixed_0517_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008634'
WHERE
notes = 'SEED000000008634' AND 
seed_name = 'SYB_fixed_0523_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008520'
WHERE
notes = 'SEED000000008520' AND 
seed_name = 'SYB_fixed_0409_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009258'
WHERE
notes = 'SEED000000009258' AND 
seed_name = 'SYB_fixed_1147_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007399'
WHERE
notes = 'SEED000000007399' AND 
seed_name = 'SYB_fixed_0040_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009279'
WHERE
notes = 'SEED000000009279' AND 
seed_name = 'SYB_fixed_1168_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009243'
WHERE
notes = 'SEED000000009243' AND 
seed_name = 'SYB_fixed_1132_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008347'
WHERE
notes = 'SEED000000008347' AND 
seed_name = 'SYB_fixed_0236_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008386'
WHERE
notes = 'SEED000000008386' AND 
seed_name = 'SYB_fixed_0275_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008471'
WHERE
notes = 'SEED000000008471' AND 
seed_name = 'SYB_fixed_0360_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008788'
WHERE
notes = 'SEED000000008788' AND 
seed_name = 'SYB_fixed_0677_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009051'
WHERE
notes = 'SEED000000009051' AND 
seed_name = 'SYB_fixed_0940_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008571'
WHERE
notes = 'SEED000000008571' AND 
seed_name = 'SYB_fixed_0460_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008711'
WHERE
notes = 'SEED000000008711' AND 
seed_name = 'SYB_fixed_0600_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008991'
WHERE
notes = 'SEED000000008991' AND 
seed_name = 'SYB_fixed_0880_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008950'
WHERE
notes = 'SEED000000008950' AND 
seed_name = 'SYB_fixed_0839_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009122'
WHERE
notes = 'SEED000000009122' AND 
seed_name = 'SYB_fixed_1011_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008173'
WHERE
notes = 'SEED000000008173' AND 
seed_name = 'SYB_fixed_0062_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009189'
WHERE
notes = 'SEED000000009189' AND 
seed_name = 'SYB_fixed_1078_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008663'
WHERE
notes = 'SEED000000008663' AND 
seed_name = 'SYB_fixed_0552_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008798'
WHERE
notes = 'SEED000000008798' AND 
seed_name = 'SYB_fixed_0687_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007375'
WHERE
notes = 'SEED000000007375' AND 
seed_name = 'SYB_fixed_0016_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008444'
WHERE
notes = 'SEED000000008444' AND 
seed_name = 'SYB_fixed_0333_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008554'
WHERE
notes = 'SEED000000008554' AND 
seed_name = 'SYB_fixed_0443_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008358'
WHERE
notes = 'SEED000000008358' AND 
seed_name = 'SYB_fixed_0247_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008703'
WHERE
notes = 'SEED000000008703' AND 
seed_name = 'SYB_fixed_0592_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009098'
WHERE
notes = 'SEED000000009098' AND 
seed_name = 'SYB_fixed_0987_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008221'
WHERE
notes = 'SEED000000008221' AND 
seed_name = 'SYB_fixed_0110_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009095'
WHERE
notes = 'SEED000000009095' AND 
seed_name = 'SYB_fixed_0984_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008832'
WHERE
notes = 'SEED000000008832' AND 
seed_name = 'SYB_fixed_0721_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008755'
WHERE
notes = 'SEED000000008755' AND 
seed_name = 'SYB_fixed_0644_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008371'
WHERE
notes = 'SEED000000008371' AND 
seed_name = 'SYB_fixed_0260_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008712'
WHERE
notes = 'SEED000000008712' AND 
seed_name = 'SYB_fixed_0601_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007366'
WHERE
notes = 'SEED000000007366' AND 
seed_name = 'SYB_fixed_0007_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009186'
WHERE
notes = 'SEED000000009186' AND 
seed_name = 'SYB_fixed_1075_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008250'
WHERE
notes = 'SEED000000008250' AND 
seed_name = 'SYB_fixed_0139_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008234'
WHERE
notes = 'SEED000000008234' AND 
seed_name = 'SYB_fixed_0123_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008805'
WHERE
notes = 'SEED000000008805' AND 
seed_name = 'SYB_fixed_0694_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008838'
WHERE
notes = 'SEED000000008838' AND 
seed_name = 'SYB_fixed_0727_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008491'
WHERE
notes = 'SEED000000008491' AND 
seed_name = 'SYB_fixed_0380_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008612'
WHERE
notes = 'SEED000000008612' AND 
seed_name = 'SYB_fixed_0501_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008242'
WHERE
notes = 'SEED000000008242' AND 
seed_name = 'SYB_fixed_0131_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008784'
WHERE
notes = 'SEED000000008784' AND 
seed_name = 'SYB_fixed_0673_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008461'
WHERE
notes = 'SEED000000008461' AND 
seed_name = 'SYB_fixed_0350_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008593'
WHERE
notes = 'SEED000000008593' AND 
seed_name = 'SYB_fixed_0482_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008417'
WHERE
notes = 'SEED000000008417' AND 
seed_name = 'SYB_fixed_0306_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009191'
WHERE
notes = 'SEED000000009191' AND 
seed_name = 'SYB_fixed_1080_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007378'
WHERE
notes = 'SEED000000007378' AND 
seed_name = 'SYB_fixed_0019_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008492'
WHERE
notes = 'SEED000000008492' AND 
seed_name = 'SYB_fixed_0381_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009187'
WHERE
notes = 'SEED000000009187' AND 
seed_name = 'SYB_fixed_1076_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009190'
WHERE
notes = 'SEED000000009190' AND 
seed_name = 'SYB_fixed_1079_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009061'
WHERE
notes = 'SEED000000009061' AND 
seed_name = 'SYB_fixed_0950_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008878'
WHERE
notes = 'SEED000000008878' AND 
seed_name = 'SYB_fixed_0767_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008817'
WHERE
notes = 'SEED000000008817' AND 
seed_name = 'SYB_fixed_0706_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009124'
WHERE
notes = 'SEED000000009124' AND 
seed_name = 'SYB_fixed_1013_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008261'
WHERE
notes = 'SEED000000008261' AND 
seed_name = 'SYB_fixed_0150_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007391'
WHERE
notes = 'SEED000000007391' AND 
seed_name = 'SYB_fixed_0032_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008823'
WHERE
notes = 'SEED000000008823' AND 
seed_name = 'SYB_fixed_0712_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009090'
WHERE
notes = 'SEED000000009090' AND 
seed_name = 'SYB_fixed_0979_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008750'
WHERE
notes = 'SEED000000008750' AND 
seed_name = 'SYB_fixed_0639_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008566'
WHERE
notes = 'SEED000000008566' AND 
seed_name = 'SYB_fixed_0455_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009025'
WHERE
notes = 'SEED000000009025' AND 
seed_name = 'SYB_fixed_0914_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009158'
WHERE
notes = 'SEED000000009158' AND 
seed_name = 'SYB_fixed_1047_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008470'
WHERE
notes = 'SEED000000008470' AND 
seed_name = 'SYB_fixed_0359_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009257'
WHERE
notes = 'SEED000000009257' AND 
seed_name = 'SYB_fixed_1146_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009204'
WHERE
notes = 'SEED000000009204' AND 
seed_name = 'SYB_fixed_1093_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008576'
WHERE
notes = 'SEED000000008576' AND 
seed_name = 'SYB_fixed_0465_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009206'
WHERE
notes = 'SEED000000009206' AND 
seed_name = 'SYB_fixed_1095_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008210'
WHERE
notes = 'SEED000000008210' AND 
seed_name = 'SYB_fixed_0099_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008779'
WHERE
notes = 'SEED000000008779' AND 
seed_name = 'SYB_fixed_0668_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007409'
WHERE
notes = 'SEED000000007409' AND 
seed_name = 'SYB_fixed_0050_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008304'
WHERE
notes = 'SEED000000008304' AND 
seed_name = 'SYB_fixed_0193_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008583'
WHERE
notes = 'SEED000000008583' AND 
seed_name = 'SYB_fixed_0472_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008859'
WHERE
notes = 'SEED000000008859' AND 
seed_name = 'SYB_fixed_0748_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008271'
WHERE
notes = 'SEED000000008271' AND 
seed_name = 'SYB_fixed_0160_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009048'
WHERE
notes = 'SEED000000009048' AND 
seed_name = 'SYB_fixed_0937_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009060'
WHERE
notes = 'SEED000000009060' AND 
seed_name = 'SYB_fixed_0949_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008873'
WHERE
notes = 'SEED000000008873' AND 
seed_name = 'SYB_fixed_0762_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009278'
WHERE
notes = 'SEED000000009278' AND 
seed_name = 'SYB_fixed_1167_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008674'
WHERE
notes = 'SEED000000008674' AND 
seed_name = 'SYB_fixed_0563_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008825'
WHERE
notes = 'SEED000000008825' AND 
seed_name = 'SYB_fixed_0714_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008860'
WHERE
notes = 'SEED000000008860' AND 
seed_name = 'SYB_fixed_0749_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008841'
WHERE
notes = 'SEED000000008841' AND 
seed_name = 'SYB_fixed_0730_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008169'
WHERE
notes = 'SEED000000008169' AND 
seed_name = 'SYB_fixed_0058_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008725'
WHERE
notes = 'SEED000000008725' AND 
seed_name = 'SYB_fixed_0614_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008370'
WHERE
notes = 'SEED000000008370' AND 
seed_name = 'SYB_fixed_0259_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008848'
WHERE
notes = 'SEED000000008848' AND 
seed_name = 'SYB_fixed_0737_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009277'
WHERE
notes = 'SEED000000009277' AND 
seed_name = 'SYB_fixed_1166_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008994'
WHERE
notes = 'SEED000000008994' AND 
seed_name = 'SYB_fixed_0883_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008415'
WHERE
notes = 'SEED000000008415' AND 
seed_name = 'SYB_fixed_0304_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008315'
WHERE
notes = 'SEED000000008315' AND 
seed_name = 'SYB_fixed_0204_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009033'
WHERE
notes = 'SEED000000009033' AND 
seed_name = 'SYB_fixed_0922_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009201'
WHERE
notes = 'SEED000000009201' AND 
seed_name = 'SYB_fixed_1090_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007396'
WHERE
notes = 'SEED000000007396' AND 
seed_name = 'SYB_fixed_0037_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008436'
WHERE
notes = 'SEED000000008436' AND 
seed_name = 'SYB_fixed_0325_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008462'
WHERE
notes = 'SEED000000008462' AND 
seed_name = 'SYB_fixed_0351_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009221'
WHERE
notes = 'SEED000000009221' AND 
seed_name = 'SYB_fixed_1110_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008967'
WHERE
notes = 'SEED000000008967' AND 
seed_name = 'SYB_fixed_0856_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008943'
WHERE
notes = 'SEED000000008943' AND 
seed_name = 'SYB_fixed_0832_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008568'
WHERE
notes = 'SEED000000008568' AND 
seed_name = 'SYB_fixed_0457_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008661'
WHERE
notes = 'SEED000000008661' AND 
seed_name = 'SYB_fixed_0550_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009276'
WHERE
notes = 'SEED000000009276' AND 
seed_name = 'SYB_fixed_1165_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009013'
WHERE
notes = 'SEED000000009013' AND 
seed_name = 'SYB_fixed_0902_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008617'
WHERE
notes = 'SEED000000008617' AND 
seed_name = 'SYB_fixed_0506_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008535'
WHERE
notes = 'SEED000000008535' AND 
seed_name = 'SYB_fixed_0424_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008697'
WHERE
notes = 'SEED000000008697' AND 
seed_name = 'SYB_fixed_0586_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008330'
WHERE
notes = 'SEED000000008330' AND 
seed_name = 'SYB_fixed_0219_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009240'
WHERE
notes = 'SEED000000009240' AND 
seed_name = 'SYB_fixed_1129_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008504'
WHERE
notes = 'SEED000000008504' AND 
seed_name = 'SYB_fixed_0393_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008925'
WHERE
notes = 'SEED000000008925' AND 
seed_name = 'SYB_fixed_0814_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008588'
WHERE
notes = 'SEED000000008588' AND 
seed_name = 'SYB_fixed_0477_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008639'
WHERE
notes = 'SEED000000008639' AND 
seed_name = 'SYB_fixed_0528_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008305'
WHERE
notes = 'SEED000000008305' AND 
seed_name = 'SYB_fixed_0194_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009162'
WHERE
notes = 'SEED000000009162' AND 
seed_name = 'SYB_fixed_1051_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008201'
WHERE
notes = 'SEED000000008201' AND 
seed_name = 'SYB_fixed_0090_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008219'
WHERE
notes = 'SEED000000008219' AND 
seed_name = 'SYB_fixed_0108_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008484'
WHERE
notes = 'SEED000000008484' AND 
seed_name = 'SYB_fixed_0373_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008783'
WHERE
notes = 'SEED000000008783' AND 
seed_name = 'SYB_fixed_0672_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008435'
WHERE
notes = 'SEED000000008435' AND 
seed_name = 'SYB_fixed_0324_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008228'
WHERE
notes = 'SEED000000008228' AND 
seed_name = 'SYB_fixed_0117_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008683'
WHERE
notes = 'SEED000000008683' AND 
seed_name = 'SYB_fixed_0572_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008368'
WHERE
notes = 'SEED000000008368' AND 
seed_name = 'SYB_fixed_0257_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007402'
WHERE
notes = 'SEED000000007402' AND 
seed_name = 'SYB_fixed_0043_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008804'
WHERE
notes = 'SEED000000008804' AND 
seed_name = 'SYB_fixed_0693_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008399'
WHERE
notes = 'SEED000000008399' AND 
seed_name = 'SYB_fixed_0288_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009077'
WHERE
notes = 'SEED000000009077' AND 
seed_name = 'SYB_fixed_0966_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009076'
WHERE
notes = 'SEED000000009076' AND 
seed_name = 'SYB_fixed_0965_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009057'
WHERE
notes = 'SEED000000009057' AND 
seed_name = 'SYB_fixed_0946_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008253'
WHERE
notes = 'SEED000000008253' AND 
seed_name = 'SYB_fixed_0142_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009014'
WHERE
notes = 'SEED000000009014' AND 
seed_name = 'SYB_fixed_0903_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009027'
WHERE
notes = 'SEED000000009027' AND 
seed_name = 'SYB_fixed_0916_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009021'
WHERE
notes = 'SEED000000009021' AND 
seed_name = 'SYB_fixed_0910_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008654'
WHERE
notes = 'SEED000000008654' AND 
seed_name = 'SYB_fixed_0543_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008463'
WHERE
notes = 'SEED000000008463' AND 
seed_name = 'SYB_fixed_0352_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009269'
WHERE
notes = 'SEED000000009269' AND 
seed_name = 'SYB_fixed_1158_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008602'
WHERE
notes = 'SEED000000008602' AND 
seed_name = 'SYB_fixed_0491_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008735'
WHERE
notes = 'SEED000000008735' AND 
seed_name = 'SYB_fixed_0624_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008840'
WHERE
notes = 'SEED000000008840' AND 
seed_name = 'SYB_fixed_0729_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008902'
WHERE
notes = 'SEED000000008902' AND 
seed_name = 'SYB_fixed_0791_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008317'
WHERE
notes = 'SEED000000008317' AND 
seed_name = 'SYB_fixed_0206_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007386'
WHERE
notes = 'SEED000000007386' AND 
seed_name = 'SYB_fixed_0027_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008559'
WHERE
notes = 'SEED000000008559' AND 
seed_name = 'SYB_fixed_0448_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008587'
WHERE
notes = 'SEED000000008587' AND 
seed_name = 'SYB_fixed_0476_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008741'
WHERE
notes = 'SEED000000008741' AND 
seed_name = 'SYB_fixed_0630_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008379'
WHERE
notes = 'SEED000000008379' AND 
seed_name = 'SYB_fixed_0268_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008961'
WHERE
notes = 'SEED000000008961' AND 
seed_name = 'SYB_fixed_0850_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008613'
WHERE
notes = 'SEED000000008613' AND 
seed_name = 'SYB_fixed_0502_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009199'
WHERE
notes = 'SEED000000009199' AND 
seed_name = 'SYB_fixed_1088_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008213'
WHERE
notes = 'SEED000000008213' AND 
seed_name = 'SYB_fixed_0102_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008914'
WHERE
notes = 'SEED000000008914' AND 
seed_name = 'SYB_fixed_0803_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008705'
WHERE
notes = 'SEED000000008705' AND 
seed_name = 'SYB_fixed_0594_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008185'
WHERE
notes = 'SEED000000008185' AND 
seed_name = 'SYB_fixed_0074_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009019'
WHERE
notes = 'SEED000000009019' AND 
seed_name = 'SYB_fixed_0908_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008673'
WHERE
notes = 'SEED000000008673' AND 
seed_name = 'SYB_fixed_0562_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008235'
WHERE
notes = 'SEED000000008235' AND 
seed_name = 'SYB_fixed_0124_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008384'
WHERE
notes = 'SEED000000008384' AND 
seed_name = 'SYB_fixed_0273_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007374'
WHERE
notes = 'SEED000000007374' AND 
seed_name = 'SYB_fixed_0015_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008479'
WHERE
notes = 'SEED000000008479' AND 
seed_name = 'SYB_fixed_0368_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008861'
WHERE
notes = 'SEED000000008861' AND 
seed_name = 'SYB_fixed_0750_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008748'
WHERE
notes = 'SEED000000008748' AND 
seed_name = 'SYB_fixed_0637_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008796'
WHERE
notes = 'SEED000000008796' AND 
seed_name = 'SYB_fixed_0685_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008232'
WHERE
notes = 'SEED000000008232' AND 
seed_name = 'SYB_fixed_0121_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009044'
WHERE
notes = 'SEED000000009044' AND 
seed_name = 'SYB_fixed_0933_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009280'
WHERE
notes = 'SEED000000009280' AND 
seed_name = 'SYB_fixed_1169_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008933'
WHERE
notes = 'SEED000000008933' AND 
seed_name = 'SYB_fixed_0822_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009156'
WHERE
notes = 'SEED000000009156' AND 
seed_name = 'SYB_fixed_1045_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009058'
WHERE
notes = 'SEED000000009058' AND 
seed_name = 'SYB_fixed_0947_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008525'
WHERE
notes = 'SEED000000008525' AND 
seed_name = 'SYB_fixed_0414_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009246'
WHERE
notes = 'SEED000000009246' AND 
seed_name = 'SYB_fixed_1135_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009141'
WHERE
notes = 'SEED000000009141' AND 
seed_name = 'SYB_fixed_1030_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008981'
WHERE
notes = 'SEED000000008981' AND 
seed_name = 'SYB_fixed_0870_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009065'
WHERE
notes = 'SEED000000009065' AND 
seed_name = 'SYB_fixed_0954_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008780'
WHERE
notes = 'SEED000000008780' AND 
seed_name = 'SYB_fixed_0669_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008907'
WHERE
notes = 'SEED000000008907' AND 
seed_name = 'SYB_fixed_0796_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008331'
WHERE
notes = 'SEED000000008331' AND 
seed_name = 'SYB_fixed_0220_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008480'
WHERE
notes = 'SEED000000008480' AND 
seed_name = 'SYB_fixed_0369_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008966'
WHERE
notes = 'SEED000000008966' AND 
seed_name = 'SYB_fixed_0855_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008200'
WHERE
notes = 'SEED000000008200' AND 
seed_name = 'SYB_fixed_0089_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009154'
WHERE
notes = 'SEED000000009154' AND 
seed_name = 'SYB_fixed_1043_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008481'
WHERE
notes = 'SEED000000008481' AND 
seed_name = 'SYB_fixed_0370_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008680'
WHERE
notes = 'SEED000000008680' AND 
seed_name = 'SYB_fixed_0569_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008453'
WHERE
notes = 'SEED000000008453' AND 
seed_name = 'SYB_fixed_0342_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008910'
WHERE
notes = 'SEED000000008910' AND 
seed_name = 'SYB_fixed_0799_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008217'
WHERE
notes = 'SEED000000008217' AND 
seed_name = 'SYB_fixed_0106_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008168'
WHERE
notes = 'SEED000000008168' AND 
seed_name = 'SYB_fixed_0057_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008895'
WHERE
notes = 'SEED000000008895' AND 
seed_name = 'SYB_fixed_0784_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008223'
WHERE
notes = 'SEED000000008223' AND 
seed_name = 'SYB_fixed_0112_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009032'
WHERE
notes = 'SEED000000009032' AND 
seed_name = 'SYB_fixed_0921_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008341'
WHERE
notes = 'SEED000000008341' AND 
seed_name = 'SYB_fixed_0230_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008447'
WHERE
notes = 'SEED000000008447' AND 
seed_name = 'SYB_fixed_0336_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008957'
WHERE
notes = 'SEED000000008957' AND 
seed_name = 'SYB_fixed_0846_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008204'
WHERE
notes = 'SEED000000008204' AND 
seed_name = 'SYB_fixed_0093_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009029'
WHERE
notes = 'SEED000000009029' AND 
seed_name = 'SYB_fixed_0918_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008824'
WHERE
notes = 'SEED000000008824' AND 
seed_name = 'SYB_fixed_0713_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008570'
WHERE
notes = 'SEED000000008570' AND 
seed_name = 'SYB_fixed_0459_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008875'
WHERE
notes = 'SEED000000008875' AND 
seed_name = 'SYB_fixed_0764_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008265'
WHERE
notes = 'SEED000000008265' AND 
seed_name = 'SYB_fixed_0154_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009093'
WHERE
notes = 'SEED000000009093' AND 
seed_name = 'SYB_fixed_0982_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009017'
WHERE
notes = 'SEED000000009017' AND 
seed_name = 'SYB_fixed_0906_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008701'
WHERE
notes = 'SEED000000008701' AND 
seed_name = 'SYB_fixed_0590_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008552'
WHERE
notes = 'SEED000000008552' AND 
seed_name = 'SYB_fixed_0441_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009117'
WHERE
notes = 'SEED000000009117' AND 
seed_name = 'SYB_fixed_1006_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008402'
WHERE
notes = 'SEED000000008402' AND 
seed_name = 'SYB_fixed_0291_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009146'
WHERE
notes = 'SEED000000009146' AND 
seed_name = 'SYB_fixed_1035_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008830'
WHERE
notes = 'SEED000000008830' AND 
seed_name = 'SYB_fixed_0719_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008604'
WHERE
notes = 'SEED000000008604' AND 
seed_name = 'SYB_fixed_0493_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009228'
WHERE
notes = 'SEED000000009228' AND 
seed_name = 'SYB_fixed_1117_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009174'
WHERE
notes = 'SEED000000009174' AND 
seed_name = 'SYB_fixed_1063_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008790'
WHERE
notes = 'SEED000000008790' AND 
seed_name = 'SYB_fixed_0679_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008831'
WHERE
notes = 'SEED000000008831' AND 
seed_name = 'SYB_fixed_0720_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009209'
WHERE
notes = 'SEED000000009209' AND 
seed_name = 'SYB_fixed_1098_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008854'
WHERE
notes = 'SEED000000008854' AND 
seed_name = 'SYB_fixed_0743_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009066'
WHERE
notes = 'SEED000000009066' AND 
seed_name = 'SYB_fixed_0955_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008273'
WHERE
notes = 'SEED000000008273' AND 
seed_name = 'SYB_fixed_0162_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008278'
WHERE
notes = 'SEED000000008278' AND 
seed_name = 'SYB_fixed_0167_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008715'
WHERE
notes = 'SEED000000008715' AND 
seed_name = 'SYB_fixed_0604_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008727'
WHERE
notes = 'SEED000000008727' AND 
seed_name = 'SYB_fixed_0616_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009036'
WHERE
notes = 'SEED000000009036' AND 
seed_name = 'SYB_fixed_0925_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009262'
WHERE
notes = 'SEED000000009262' AND 
seed_name = 'SYB_fixed_1151_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008329'
WHERE
notes = 'SEED000000008329' AND 
seed_name = 'SYB_fixed_0218_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008599'
WHERE
notes = 'SEED000000008599' AND 
seed_name = 'SYB_fixed_0488_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009023'
WHERE
notes = 'SEED000000009023' AND 
seed_name = 'SYB_fixed_0912_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008930'
WHERE
notes = 'SEED000000008930' AND 
seed_name = 'SYB_fixed_0819_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008292'
WHERE
notes = 'SEED000000008292' AND 
seed_name = 'SYB_fixed_0181_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008290'
WHERE
notes = 'SEED000000008290' AND 
seed_name = 'SYB_fixed_0179_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007364'
WHERE
notes = 'SEED000000007364' AND 
seed_name = 'SYB_fixed_0005_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008263'
WHERE
notes = 'SEED000000008263' AND 
seed_name = 'SYB_fixed_0152_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008820'
WHERE
notes = 'SEED000000008820' AND 
seed_name = 'SYB_fixed_0709_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008849'
WHERE
notes = 'SEED000000008849' AND 
seed_name = 'SYB_fixed_0738_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009108'
WHERE
notes = 'SEED000000009108' AND 
seed_name = 'SYB_fixed_0997_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007397'
WHERE
notes = 'SEED000000007397' AND 
seed_name = 'SYB_fixed_0038_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009072'
WHERE
notes = 'SEED000000009072' AND 
seed_name = 'SYB_fixed_0961_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009167'
WHERE
notes = 'SEED000000009167' AND 
seed_name = 'SYB_fixed_1056_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008807'
WHERE
notes = 'SEED000000008807' AND 
seed_name = 'SYB_fixed_0696_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009042'
WHERE
notes = 'SEED000000009042' AND 
seed_name = 'SYB_fixed_0931_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008978'
WHERE
notes = 'SEED000000008978' AND 
seed_name = 'SYB_fixed_0867_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008704'
WHERE
notes = 'SEED000000008704' AND 
seed_name = 'SYB_fixed_0593_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008890'
WHERE
notes = 'SEED000000008890' AND 
seed_name = 'SYB_fixed_0779_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008923'
WHERE
notes = 'SEED000000008923' AND 
seed_name = 'SYB_fixed_0812_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007376'
WHERE
notes = 'SEED000000007376' AND 
seed_name = 'SYB_fixed_0017_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008324'
WHERE
notes = 'SEED000000008324' AND 
seed_name = 'SYB_fixed_0213_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009132'
WHERE
notes = 'SEED000000009132' AND 
seed_name = 'SYB_fixed_1021_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009182'
WHERE
notes = 'SEED000000009182' AND 
seed_name = 'SYB_fixed_1071_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008622'
WHERE
notes = 'SEED000000008622' AND 
seed_name = 'SYB_fixed_0511_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008608'
WHERE
notes = 'SEED000000008608' AND 
seed_name = 'SYB_fixed_0497_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009144'
WHERE
notes = 'SEED000000009144' AND 
seed_name = 'SYB_fixed_1033_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009008'
WHERE
notes = 'SEED000000009008' AND 
seed_name = 'SYB_fixed_0897_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009030'
WHERE
notes = 'SEED000000009030' AND 
seed_name = 'SYB_fixed_0919_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008829'
WHERE
notes = 'SEED000000008829' AND 
seed_name = 'SYB_fixed_0718_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008211'
WHERE
notes = 'SEED000000008211' AND 
seed_name = 'SYB_fixed_0100_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008942'
WHERE
notes = 'SEED000000008942' AND 
seed_name = 'SYB_fixed_0831_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009194'
WHERE
notes = 'SEED000000009194' AND 
seed_name = 'SYB_fixed_1083_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008499'
WHERE
notes = 'SEED000000008499' AND 
seed_name = 'SYB_fixed_0388_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008800'
WHERE
notes = 'SEED000000008800' AND 
seed_name = 'SYB_fixed_0689_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008512'
WHERE
notes = 'SEED000000008512' AND 
seed_name = 'SYB_fixed_0401_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008203'
WHERE
notes = 'SEED000000008203' AND 
seed_name = 'SYB_fixed_0092_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008336'
WHERE
notes = 'SEED000000008336' AND 
seed_name = 'SYB_fixed_0225_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008924'
WHERE
notes = 'SEED000000008924' AND 
seed_name = 'SYB_fixed_0813_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008502'
WHERE
notes = 'SEED000000008502' AND 
seed_name = 'SYB_fixed_0391_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008635'
WHERE
notes = 'SEED000000008635' AND 
seed_name = 'SYB_fixed_0524_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009210'
WHERE
notes = 'SEED000000009210' AND 
seed_name = 'SYB_fixed_1099_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008457'
WHERE
notes = 'SEED000000008457' AND 
seed_name = 'SYB_fixed_0346_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008361'
WHERE
notes = 'SEED000000008361' AND 
seed_name = 'SYB_fixed_0250_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008252'
WHERE
notes = 'SEED000000008252' AND 
seed_name = 'SYB_fixed_0141_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008657'
WHERE
notes = 'SEED000000008657' AND 
seed_name = 'SYB_fixed_0546_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008761'
WHERE
notes = 'SEED000000008761' AND 
seed_name = 'SYB_fixed_0650_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008979'
WHERE
notes = 'SEED000000008979' AND 
seed_name = 'SYB_fixed_0868_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008548'
WHERE
notes = 'SEED000000008548' AND 
seed_name = 'SYB_fixed_0437_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009294'
WHERE
notes = 'SEED000000009294' AND 
seed_name = 'SYB_fixed_1183_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008652'
WHERE
notes = 'SEED000000008652' AND 
seed_name = 'SYB_fixed_0541_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008456'
WHERE
notes = 'SEED000000008456' AND 
seed_name = 'SYB_fixed_0345_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008288'
WHERE
notes = 'SEED000000008288' AND 
seed_name = 'SYB_fixed_0177_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009260'
WHERE
notes = 'SEED000000009260' AND 
seed_name = 'SYB_fixed_1149_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008874'
WHERE
notes = 'SEED000000008874' AND 
seed_name = 'SYB_fixed_0763_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009079'
WHERE
notes = 'SEED000000009079' AND 
seed_name = 'SYB_fixed_0968_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008812'
WHERE
notes = 'SEED000000008812' AND 
seed_name = 'SYB_fixed_0701_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007385'
WHERE
notes = 'SEED000000007385' AND 
seed_name = 'SYB_fixed_0026_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007390'
WHERE
notes = 'SEED000000007390' AND 
seed_name = 'SYB_fixed_0031_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008813'
WHERE
notes = 'SEED000000008813' AND 
seed_name = 'SYB_fixed_0702_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009119'
WHERE
notes = 'SEED000000009119' AND 
seed_name = 'SYB_fixed_1008_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008734'
WHERE
notes = 'SEED000000008734' AND 
seed_name = 'SYB_fixed_0623_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008540'
WHERE
notes = 'SEED000000008540' AND 
seed_name = 'SYB_fixed_0429_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008488'
WHERE
notes = 'SEED000000008488' AND 
seed_name = 'SYB_fixed_0377_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008717'
WHERE
notes = 'SEED000000008717' AND 
seed_name = 'SYB_fixed_0606_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008956'
WHERE
notes = 'SEED000000008956' AND 
seed_name = 'SYB_fixed_0845_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008629'
WHERE
notes = 'SEED000000008629' AND 
seed_name = 'SYB_fixed_0518_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009192'
WHERE
notes = 'SEED000000009192' AND 
seed_name = 'SYB_fixed_1081_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008633'
WHERE
notes = 'SEED000000008633' AND 
seed_name = 'SYB_fixed_0522_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008740'
WHERE
notes = 'SEED000000008740' AND 
seed_name = 'SYB_fixed_0629_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009244'
WHERE
notes = 'SEED000000009244' AND 
seed_name = 'SYB_fixed_1133_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008239'
WHERE
notes = 'SEED000000008239' AND 
seed_name = 'SYB_fixed_0128_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008523'
WHERE
notes = 'SEED000000008523' AND 
seed_name = 'SYB_fixed_0412_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008985'
WHERE
notes = 'SEED000000008985' AND 
seed_name = 'SYB_fixed_0874_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008551'
WHERE
notes = 'SEED000000008551' AND 
seed_name = 'SYB_fixed_0440_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008527'
WHERE
notes = 'SEED000000008527' AND 
seed_name = 'SYB_fixed_0416_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008406'
WHERE
notes = 'SEED000000008406' AND 
seed_name = 'SYB_fixed_0295_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008770'
WHERE
notes = 'SEED000000008770' AND 
seed_name = 'SYB_fixed_0659_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007373'
WHERE
notes = 'SEED000000007373' AND 
seed_name = 'SYB_fixed_0014_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009197'
WHERE
notes = 'SEED000000009197' AND 
seed_name = 'SYB_fixed_1086_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008960'
WHERE
notes = 'SEED000000008960' AND 
seed_name = 'SYB_fixed_0849_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008983'
WHERE
notes = 'SEED000000008983' AND 
seed_name = 'SYB_fixed_0872_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008258'
WHERE
notes = 'SEED000000008258' AND 
seed_name = 'SYB_fixed_0147_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009020'
WHERE
notes = 'SEED000000009020' AND 
seed_name = 'SYB_fixed_0909_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009043'
WHERE
notes = 'SEED000000009043' AND 
seed_name = 'SYB_fixed_0932_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009097'
WHERE
notes = 'SEED000000009097' AND 
seed_name = 'SYB_fixed_0986_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008295'
WHERE
notes = 'SEED000000008295' AND 
seed_name = 'SYB_fixed_0184_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008166'
WHERE
notes = 'SEED000000008166' AND 
seed_name = 'SYB_fixed_0055_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009220'
WHERE
notes = 'SEED000000009220' AND 
seed_name = 'SYB_fixed_1109_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008671'
WHERE
notes = 'SEED000000008671' AND 
seed_name = 'SYB_fixed_0560_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009267'
WHERE
notes = 'SEED000000009267' AND 
seed_name = 'SYB_fixed_1156_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008356'
WHERE
notes = 'SEED000000008356' AND 
seed_name = 'SYB_fixed_0245_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008448'
WHERE
notes = 'SEED000000008448' AND 
seed_name = 'SYB_fixed_0337_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009120'
WHERE
notes = 'SEED000000009120' AND 
seed_name = 'SYB_fixed_1009_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008645'
WHERE
notes = 'SEED000000008645' AND 
seed_name = 'SYB_fixed_0534_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008506'
WHERE
notes = 'SEED000000008506' AND 
seed_name = 'SYB_fixed_0395_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008544'
WHERE
notes = 'SEED000000008544' AND 
seed_name = 'SYB_fixed_0433_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007381'
WHERE
notes = 'SEED000000007381' AND 
seed_name = 'SYB_fixed_0022_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008594'
WHERE
notes = 'SEED000000008594' AND 
seed_name = 'SYB_fixed_0483_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008302'
WHERE
notes = 'SEED000000008302' AND 
seed_name = 'SYB_fixed_0191_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008818'
WHERE
notes = 'SEED000000008818' AND 
seed_name = 'SYB_fixed_0707_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008186'
WHERE
notes = 'SEED000000008186' AND 
seed_name = 'SYB_fixed_0075_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009143'
WHERE
notes = 'SEED000000009143' AND 
seed_name = 'SYB_fixed_1032_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009080'
WHERE
notes = 'SEED000000009080' AND 
seed_name = 'SYB_fixed_0969_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008688'
WHERE
notes = 'SEED000000008688' AND 
seed_name = 'SYB_fixed_0577_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009271'
WHERE
notes = 'SEED000000009271' AND 
seed_name = 'SYB_fixed_1160_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008886'
WHERE
notes = 'SEED000000008886' AND 
seed_name = 'SYB_fixed_0775_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009135'
WHERE
notes = 'SEED000000009135' AND 
seed_name = 'SYB_fixed_1024_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008609'
WHERE
notes = 'SEED000000008609' AND 
seed_name = 'SYB_fixed_0498_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008720'
WHERE
notes = 'SEED000000008720' AND 
seed_name = 'SYB_fixed_0609_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008369'
WHERE
notes = 'SEED000000008369' AND 
seed_name = 'SYB_fixed_0258_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007388'
WHERE
notes = 'SEED000000007388' AND 
seed_name = 'SYB_fixed_0029_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009248'
WHERE
notes = 'SEED000000009248' AND 
seed_name = 'SYB_fixed_1137_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009064'
WHERE
notes = 'SEED000000009064' AND 
seed_name = 'SYB_fixed_0953_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008868'
WHERE
notes = 'SEED000000008868' AND 
seed_name = 'SYB_fixed_0757_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008497'
WHERE
notes = 'SEED000000008497' AND 
seed_name = 'SYB_fixed_0386_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008611'
WHERE
notes = 'SEED000000008611' AND 
seed_name = 'SYB_fixed_0500_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008641'
WHERE
notes = 'SEED000000008641' AND 
seed_name = 'SYB_fixed_0530_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008572'
WHERE
notes = 'SEED000000008572' AND 
seed_name = 'SYB_fixed_0461_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008887'
WHERE
notes = 'SEED000000008887' AND 
seed_name = 'SYB_fixed_0776_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009085'
WHERE
notes = 'SEED000000009085' AND 
seed_name = 'SYB_fixed_0974_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008679'
WHERE
notes = 'SEED000000008679' AND 
seed_name = 'SYB_fixed_0568_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009039'
WHERE
notes = 'SEED000000009039' AND 
seed_name = 'SYB_fixed_0928_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008843'
WHERE
notes = 'SEED000000008843' AND 
seed_name = 'SYB_fixed_0732_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009219'
WHERE
notes = 'SEED000000009219' AND 
seed_name = 'SYB_fixed_1108_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008246'
WHERE
notes = 'SEED000000008246' AND 
seed_name = 'SYB_fixed_0135_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009249'
WHERE
notes = 'SEED000000009249' AND 
seed_name = 'SYB_fixed_1138_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008244'
WHERE
notes = 'SEED000000008244' AND 
seed_name = 'SYB_fixed_0133_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008839'
WHERE
notes = 'SEED000000008839' AND 
seed_name = 'SYB_fixed_0728_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009287'
WHERE
notes = 'SEED000000009287' AND 
seed_name = 'SYB_fixed_1176_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008577'
WHERE
notes = 'SEED000000008577' AND 
seed_name = 'SYB_fixed_0466_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008175'
WHERE
notes = 'SEED000000008175' AND 
seed_name = 'SYB_fixed_0064_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008922'
WHERE
notes = 'SEED000000008922' AND 
seed_name = 'SYB_fixed_0811_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008754'
WHERE
notes = 'SEED000000008754' AND 
seed_name = 'SYB_fixed_0643_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008199'
WHERE
notes = 'SEED000000008199' AND 
seed_name = 'SYB_fixed_0088_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008390'
WHERE
notes = 'SEED000000008390' AND 
seed_name = 'SYB_fixed_0279_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009284'
WHERE
notes = 'SEED000000009284' AND 
seed_name = 'SYB_fixed_1173_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008372'
WHERE
notes = 'SEED000000008372' AND 
seed_name = 'SYB_fixed_0261_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008903'
WHERE
notes = 'SEED000000008903' AND 
seed_name = 'SYB_fixed_0792_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008459'
WHERE
notes = 'SEED000000008459' AND 
seed_name = 'SYB_fixed_0348_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009193'
WHERE
notes = 'SEED000000009193' AND 
seed_name = 'SYB_fixed_1082_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009178'
WHERE
notes = 'SEED000000009178' AND 
seed_name = 'SYB_fixed_1067_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008992'
WHERE
notes = 'SEED000000008992' AND 
seed_name = 'SYB_fixed_0881_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008855'
WHERE
notes = 'SEED000000008855' AND 
seed_name = 'SYB_fixed_0744_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008345'
WHERE
notes = 'SEED000000008345' AND 
seed_name = 'SYB_fixed_0234_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008501'
WHERE
notes = 'SEED000000008501' AND 
seed_name = 'SYB_fixed_0390_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008303'
WHERE
notes = 'SEED000000008303' AND 
seed_name = 'SYB_fixed_0192_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008340'
WHERE
notes = 'SEED000000008340' AND 
seed_name = 'SYB_fixed_0229_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009050'
WHERE
notes = 'SEED000000009050' AND 
seed_name = 'SYB_fixed_0939_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008194'
WHERE
notes = 'SEED000000008194' AND 
seed_name = 'SYB_fixed_0083_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008982'
WHERE
notes = 'SEED000000008982' AND 
seed_name = 'SYB_fixed_0871_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008363'
WHERE
notes = 'SEED000000008363' AND 
seed_name = 'SYB_fixed_0252_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008595'
WHERE
notes = 'SEED000000008595' AND 
seed_name = 'SYB_fixed_0484_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009052'
WHERE
notes = 'SEED000000009052' AND 
seed_name = 'SYB_fixed_0941_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008631'
WHERE
notes = 'SEED000000008631' AND 
seed_name = 'SYB_fixed_0520_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008452'
WHERE
notes = 'SEED000000008452' AND 
seed_name = 'SYB_fixed_0341_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008821'
WHERE
notes = 'SEED000000008821' AND 
seed_name = 'SYB_fixed_0710_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008753'
WHERE
notes = 'SEED000000008753' AND 
seed_name = 'SYB_fixed_0642_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008837'
WHERE
notes = 'SEED000000008837' AND 
seed_name = 'SYB_fixed_0726_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008443'
WHERE
notes = 'SEED000000008443' AND 
seed_name = 'SYB_fixed_0332_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008562'
WHERE
notes = 'SEED000000008562' AND 
seed_name = 'SYB_fixed_0451_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008172'
WHERE
notes = 'SEED000000008172' AND 
seed_name = 'SYB_fixed_0061_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008752'
WHERE
notes = 'SEED000000008752' AND 
seed_name = 'SYB_fixed_0641_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009234'
WHERE
notes = 'SEED000000009234' AND 
seed_name = 'SYB_fixed_1123_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009034'
WHERE
notes = 'SEED000000009034' AND 
seed_name = 'SYB_fixed_0923_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008460'
WHERE
notes = 'SEED000000008460' AND 
seed_name = 'SYB_fixed_0349_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008191'
WHERE
notes = 'SEED000000008191' AND 
seed_name = 'SYB_fixed_0080_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009175'
WHERE
notes = 'SEED000000009175' AND 
seed_name = 'SYB_fixed_1064_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008773'
WHERE
notes = 'SEED000000008773' AND 
seed_name = 'SYB_fixed_0662_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008346'
WHERE
notes = 'SEED000000008346' AND 
seed_name = 'SYB_fixed_0235_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008744'
WHERE
notes = 'SEED000000008744' AND 
seed_name = 'SYB_fixed_0633_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008301'
WHERE
notes = 'SEED000000008301' AND 
seed_name = 'SYB_fixed_0190_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008660'
WHERE
notes = 'SEED000000008660' AND 
seed_name = 'SYB_fixed_0549_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008815'
WHERE
notes = 'SEED000000008815' AND 
seed_name = 'SYB_fixed_0704_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008532'
WHERE
notes = 'SEED000000008532' AND 
seed_name = 'SYB_fixed_0421_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008238'
WHERE
notes = 'SEED000000008238' AND 
seed_name = 'SYB_fixed_0127_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008422'
WHERE
notes = 'SEED000000008422' AND 
seed_name = 'SYB_fixed_0311_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008378'
WHERE
notes = 'SEED000000008378' AND 
seed_name = 'SYB_fixed_0267_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008410'
WHERE
notes = 'SEED000000008410' AND 
seed_name = 'SYB_fixed_0299_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008808'
WHERE
notes = 'SEED000000008808' AND 
seed_name = 'SYB_fixed_0697_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008241'
WHERE
notes = 'SEED000000008241' AND 
seed_name = 'SYB_fixed_0130_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008494'
WHERE
notes = 'SEED000000008494' AND 
seed_name = 'SYB_fixed_0383_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008700'
WHERE
notes = 'SEED000000008700' AND 
seed_name = 'SYB_fixed_0589_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008383'
WHERE
notes = 'SEED000000008383' AND 
seed_name = 'SYB_fixed_0272_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008738'
WHERE
notes = 'SEED000000008738' AND 
seed_name = 'SYB_fixed_0627_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009059'
WHERE
notes = 'SEED000000009059' AND 
seed_name = 'SYB_fixed_0948_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009231'
WHERE
notes = 'SEED000000009231' AND 
seed_name = 'SYB_fixed_1120_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008643'
WHERE
notes = 'SEED000000008643' AND 
seed_name = 'SYB_fixed_0532_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008283'
WHERE
notes = 'SEED000000008283' AND 
seed_name = 'SYB_fixed_0172_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008165'
WHERE
notes = 'SEED000000008165' AND 
seed_name = 'SYB_fixed_0054_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008833'
WHERE
notes = 'SEED000000008833' AND 
seed_name = 'SYB_fixed_0722_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008945'
WHERE
notes = 'SEED000000008945' AND 
seed_name = 'SYB_fixed_0834_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008377'
WHERE
notes = 'SEED000000008377' AND 
seed_name = 'SYB_fixed_0266_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008524'
WHERE
notes = 'SEED000000008524' AND 
seed_name = 'SYB_fixed_0413_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009002'
WHERE
notes = 'SEED000000009002' AND 
seed_name = 'SYB_fixed_0891_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009130'
WHERE
notes = 'SEED000000009130' AND 
seed_name = 'SYB_fixed_1019_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008431'
WHERE
notes = 'SEED000000008431' AND 
seed_name = 'SYB_fixed_0320_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008178'
WHERE
notes = 'SEED000000008178' AND 
seed_name = 'SYB_fixed_0067_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008366'
WHERE
notes = 'SEED000000008366' AND 
seed_name = 'SYB_fixed_0255_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008905'
WHERE
notes = 'SEED000000008905' AND 
seed_name = 'SYB_fixed_0794_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008698'
WHERE
notes = 'SEED000000008698' AND 
seed_name = 'SYB_fixed_0587_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008432'
WHERE
notes = 'SEED000000008432' AND 
seed_name = 'SYB_fixed_0321_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008915'
WHERE
notes = 'SEED000000008915' AND 
seed_name = 'SYB_fixed_0804_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008897'
WHERE
notes = 'SEED000000008897' AND 
seed_name = 'SYB_fixed_0786_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008414'
WHERE
notes = 'SEED000000008414' AND 
seed_name = 'SYB_fixed_0303_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008951'
WHERE
notes = 'SEED000000008951' AND 
seed_name = 'SYB_fixed_0840_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008275'
WHERE
notes = 'SEED000000008275' AND 
seed_name = 'SYB_fixed_0164_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008483'
WHERE
notes = 'SEED000000008483' AND 
seed_name = 'SYB_fixed_0372_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009125'
WHERE
notes = 'SEED000000009125' AND 
seed_name = 'SYB_fixed_1014_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009018'
WHERE
notes = 'SEED000000009018' AND 
seed_name = 'SYB_fixed_0907_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009165'
WHERE
notes = 'SEED000000009165' AND 
seed_name = 'SYB_fixed_1054_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007365'
WHERE
notes = 'SEED000000007365' AND 
seed_name = 'SYB_fixed_0006_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008343'
WHERE
notes = 'SEED000000008343' AND 
seed_name = 'SYB_fixed_0232_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009214'
WHERE
notes = 'SEED000000009214' AND 
seed_name = 'SYB_fixed_1103_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008364'
WHERE
notes = 'SEED000000008364' AND 
seed_name = 'SYB_fixed_0253_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007360'
WHERE
notes = 'SEED000000007360' AND 
seed_name = 'SYB_fixed_0001_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007410'
WHERE
notes = 'SEED000000007410' AND 
seed_name = '301678462'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007411'
WHERE
notes = 'SEED000000007411' AND 
seed_name = '301678495'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007412'
WHERE
notes = 'SEED000000007412' AND 
seed_name = '301678496'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007413'
WHERE
notes = 'SEED000000007413' AND 
seed_name = '301678528'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008255'
WHERE
notes = 'SEED000000008255' AND 
seed_name = 'SYB_fixed_0144_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008590'
WHERE
notes = 'SEED000000008590' AND 
seed_name = 'SYB_fixed_0479_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009275'
WHERE
notes = 'SEED000000009275' AND 
seed_name = 'SYB_fixed_1164_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008653'
WHERE
notes = 'SEED000000008653' AND 
seed_name = 'SYB_fixed_0542_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008938'
WHERE
notes = 'SEED000000008938' AND 
seed_name = 'SYB_fixed_0827_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008438'
WHERE
notes = 'SEED000000008438' AND 
seed_name = 'SYB_fixed_0327_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008224'
WHERE
notes = 'SEED000000008224' AND 
seed_name = 'SYB_fixed_0113_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008284'
WHERE
notes = 'SEED000000008284' AND 
seed_name = 'SYB_fixed_0173_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008636'
WHERE
notes = 'SEED000000008636' AND 
seed_name = 'SYB_fixed_0525_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008997'
WHERE
notes = 'SEED000000008997' AND 
seed_name = 'SYB_fixed_0886_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009166'
WHERE
notes = 'SEED000000009166' AND 
seed_name = 'SYB_fixed_1055_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008240'
WHERE
notes = 'SEED000000008240' AND 
seed_name = 'SYB_fixed_0129_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008764'
WHERE
notes = 'SEED000000008764' AND 
seed_name = 'SYB_fixed_0653_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008202'
WHERE
notes = 'SEED000000008202' AND 
seed_name = 'SYB_fixed_0091_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009200'
WHERE
notes = 'SEED000000009200' AND 
seed_name = 'SYB_fixed_1089_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009075'
WHERE
notes = 'SEED000000009075' AND 
seed_name = 'SYB_fixed_0964_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008756'
WHERE
notes = 'SEED000000008756' AND 
seed_name = 'SYB_fixed_0645_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008607'
WHERE
notes = 'SEED000000008607' AND 
seed_name = 'SYB_fixed_0496_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008718'
WHERE
notes = 'SEED000000008718' AND 
seed_name = 'SYB_fixed_0607_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008845'
WHERE
notes = 'SEED000000008845' AND 
seed_name = 'SYB_fixed_0734_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009157'
WHERE
notes = 'SEED000000009157' AND 
seed_name = 'SYB_fixed_1046_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008729'
WHERE
notes = 'SEED000000008729' AND 
seed_name = 'SYB_fixed_0618_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009253'
WHERE
notes = 'SEED000000009253' AND 
seed_name = 'SYB_fixed_1142_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008709'
WHERE
notes = 'SEED000000008709' AND 
seed_name = 'SYB_fixed_0598_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009207'
WHERE
notes = 'SEED000000009207' AND 
seed_name = 'SYB_fixed_1096_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009161'
WHERE
notes = 'SEED000000009161' AND 
seed_name = 'SYB_fixed_1050_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008534'
WHERE
notes = 'SEED000000008534' AND 
seed_name = 'SYB_fixed_0423_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008307'
WHERE
notes = 'SEED000000008307' AND 
seed_name = 'SYB_fixed_0196_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008916'
WHERE
notes = 'SEED000000008916' AND 
seed_name = 'SYB_fixed_0805_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008430'
WHERE
notes = 'SEED000000008430' AND 
seed_name = 'SYB_fixed_0319_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009168'
WHERE
notes = 'SEED000000009168' AND 
seed_name = 'SYB_fixed_1057_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009239'
WHERE
notes = 'SEED000000009239' AND 
seed_name = 'SYB_fixed_1128_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009229'
WHERE
notes = 'SEED000000009229' AND 
seed_name = 'SYB_fixed_1118_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008648'
WHERE
notes = 'SEED000000008648' AND 
seed_name = 'SYB_fixed_0537_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008209'
WHERE
notes = 'SEED000000008209' AND 
seed_name = 'SYB_fixed_0098_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008529'
WHERE
notes = 'SEED000000008529' AND 
seed_name = 'SYB_fixed_0418_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007372'
WHERE
notes = 'SEED000000007372' AND 
seed_name = 'SYB_fixed_0013_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008947'
WHERE
notes = 'SEED000000008947' AND 
seed_name = 'SYB_fixed_0836_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008917'
WHERE
notes = 'SEED000000008917' AND 
seed_name = 'SYB_fixed_0806_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008675'
WHERE
notes = 'SEED000000008675' AND 
seed_name = 'SYB_fixed_0564_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008789'
WHERE
notes = 'SEED000000008789' AND 
seed_name = 'SYB_fixed_0678_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009273'
WHERE
notes = 'SEED000000009273' AND 
seed_name = 'SYB_fixed_1162_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008597'
WHERE
notes = 'SEED000000008597' AND 
seed_name = 'SYB_fixed_0486_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008268'
WHERE
notes = 'SEED000000008268' AND 
seed_name = 'SYB_fixed_0157_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008174'
WHERE
notes = 'SEED000000008174' AND 
seed_name = 'SYB_fixed_0063_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008495'
WHERE
notes = 'SEED000000008495' AND 
seed_name = 'SYB_fixed_0384_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008521'
WHERE
notes = 'SEED000000008521' AND 
seed_name = 'SYB_fixed_0410_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007394'
WHERE
notes = 'SEED000000007394' AND 
seed_name = 'SYB_fixed_0035_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008955'
WHERE
notes = 'SEED000000008955' AND 
seed_name = 'SYB_fixed_0844_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007369'
WHERE
notes = 'SEED000000007369' AND 
seed_name = 'SYB_fixed_0010_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007392'
WHERE
notes = 'SEED000000007392' AND 
seed_name = 'SYB_fixed_0033_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009259'
WHERE
notes = 'SEED000000009259' AND 
seed_name = 'SYB_fixed_1148_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008542'
WHERE
notes = 'SEED000000008542' AND 
seed_name = 'SYB_fixed_0431_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008614'
WHERE
notes = 'SEED000000008614' AND 
seed_name = 'SYB_fixed_0503_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008722'
WHERE
notes = 'SEED000000008722' AND 
seed_name = 'SYB_fixed_0611_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009226'
WHERE
notes = 'SEED000000009226' AND 
seed_name = 'SYB_fixed_1115_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008646'
WHERE
notes = 'SEED000000008646' AND 
seed_name = 'SYB_fixed_0535_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008280'
WHERE
notes = 'SEED000000008280' AND 
seed_name = 'SYB_fixed_0169_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008926'
WHERE
notes = 'SEED000000008926' AND 
seed_name = 'SYB_fixed_0815_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008216'
WHERE
notes = 'SEED000000008216' AND 
seed_name = 'SYB_fixed_0105_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008282'
WHERE
notes = 'SEED000000008282' AND 
seed_name = 'SYB_fixed_0171_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008441'
WHERE
notes = 'SEED000000008441' AND 
seed_name = 'SYB_fixed_0330_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008973'
WHERE
notes = 'SEED000000008973' AND 
seed_name = 'SYB_fixed_0862_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008380'
WHERE
notes = 'SEED000000008380' AND 
seed_name = 'SYB_fixed_0269_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009176'
WHERE
notes = 'SEED000000009176' AND 
seed_name = 'SYB_fixed_1065_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008606'
WHERE
notes = 'SEED000000008606' AND 
seed_name = 'SYB_fixed_0495_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008968'
WHERE
notes = 'SEED000000008968' AND 
seed_name = 'SYB_fixed_0857_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008187'
WHERE
notes = 'SEED000000008187' AND 
seed_name = 'SYB_fixed_0076_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009092'
WHERE
notes = 'SEED000000009092' AND 
seed_name = 'SYB_fixed_0981_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009181'
WHERE
notes = 'SEED000000009181' AND 
seed_name = 'SYB_fixed_1070_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008381'
WHERE
notes = 'SEED000000008381' AND 
seed_name = 'SYB_fixed_0270_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008405'
WHERE
notes = 'SEED000000008405' AND 
seed_name = 'SYB_fixed_0294_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009149'
WHERE
notes = 'SEED000000009149' AND 
seed_name = 'SYB_fixed_1038_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009083'
WHERE
notes = 'SEED000000009083' AND 
seed_name = 'SYB_fixed_0972_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008771'
WHERE
notes = 'SEED000000008771' AND 
seed_name = 'SYB_fixed_0660_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008637'
WHERE
notes = 'SEED000000008637' AND 
seed_name = 'SYB_fixed_0526_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009010'
WHERE
notes = 'SEED000000009010' AND 
seed_name = 'SYB_fixed_0899_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008782'
WHERE
notes = 'SEED000000008782' AND 
seed_name = 'SYB_fixed_0671_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008549'
WHERE
notes = 'SEED000000008549' AND 
seed_name = 'SYB_fixed_0438_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008899'
WHERE
notes = 'SEED000000008899' AND 
seed_name = 'SYB_fixed_0788_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007383'
WHERE
notes = 'SEED000000007383' AND 
seed_name = 'SYB_fixed_0024_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008913'
WHERE
notes = 'SEED000000008913' AND 
seed_name = 'SYB_fixed_0802_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008428'
WHERE
notes = 'SEED000000008428' AND 
seed_name = 'SYB_fixed_0317_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008880'
WHERE
notes = 'SEED000000008880' AND 
seed_name = 'SYB_fixed_0769_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007404'
WHERE
notes = 'SEED000000007404' AND 
seed_name = 'SYB_fixed_0045_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007405'
WHERE
notes = 'SEED000000007405' AND 
seed_name = 'SYB_fixed_0046_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008745'
WHERE
notes = 'SEED000000008745' AND 
seed_name = 'SYB_fixed_0634_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008230'
WHERE
notes = 'SEED000000008230' AND 
seed_name = 'SYB_fixed_0119_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009169'
WHERE
notes = 'SEED000000009169' AND 
seed_name = 'SYB_fixed_1058_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007389'
WHERE
notes = 'SEED000000007389' AND 
seed_name = 'SYB_fixed_0030_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008828'
WHERE
notes = 'SEED000000008828' AND 
seed_name = 'SYB_fixed_0717_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009005'
WHERE
notes = 'SEED000000009005' AND 
seed_name = 'SYB_fixed_0894_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008842'
WHERE
notes = 'SEED000000008842' AND 
seed_name = 'SYB_fixed_0731_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008245'
WHERE
notes = 'SEED000000008245' AND 
seed_name = 'SYB_fixed_0134_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008765'
WHERE
notes = 'SEED000000008765' AND 
seed_name = 'SYB_fixed_0654_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008664'
WHERE
notes = 'SEED000000008664' AND 
seed_name = 'SYB_fixed_0553_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008964'
WHERE
notes = 'SEED000000008964' AND 
seed_name = 'SYB_fixed_0853_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008775'
WHERE
notes = 'SEED000000008775' AND 
seed_name = 'SYB_fixed_0664_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009145'
WHERE
notes = 'SEED000000009145' AND 
seed_name = 'SYB_fixed_1034_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009264'
WHERE
notes = 'SEED000000009264' AND 
seed_name = 'SYB_fixed_1153_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008322'
WHERE
notes = 'SEED000000008322' AND 
seed_name = 'SYB_fixed_0211_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008286'
WHERE
notes = 'SEED000000008286' AND 
seed_name = 'SYB_fixed_0175_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009015'
WHERE
notes = 'SEED000000009015' AND 
seed_name = 'SYB_fixed_0904_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008300'
WHERE
notes = 'SEED000000008300' AND 
seed_name = 'SYB_fixed_0189_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008847'
WHERE
notes = 'SEED000000008847' AND 
seed_name = 'SYB_fixed_0736_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008990'
WHERE
notes = 'SEED000000008990' AND 
seed_name = 'SYB_fixed_0879_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008337'
WHERE
notes = 'SEED000000008337' AND 
seed_name = 'SYB_fixed_0226_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008299'
WHERE
notes = 'SEED000000008299' AND 
seed_name = 'SYB_fixed_0188_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008940'
WHERE
notes = 'SEED000000008940' AND 
seed_name = 'SYB_fixed_0829_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008909'
WHERE
notes = 'SEED000000008909' AND 
seed_name = 'SYB_fixed_0798_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009062'
WHERE
notes = 'SEED000000009062' AND 
seed_name = 'SYB_fixed_0951_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009118'
WHERE
notes = 'SEED000000009118' AND 
seed_name = 'SYB_fixed_1007_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008888'
WHERE
notes = 'SEED000000008888' AND 
seed_name = 'SYB_fixed_0777_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008248'
WHERE
notes = 'SEED000000008248' AND 
seed_name = 'SYB_fixed_0137_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007379'
WHERE
notes = 'SEED000000007379' AND 
seed_name = 'SYB_fixed_0020_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007398'
WHERE
notes = 'SEED000000007398' AND 
seed_name = 'SYB_fixed_0039_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009163'
WHERE
notes = 'SEED000000009163' AND 
seed_name = 'SYB_fixed_1052_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008208'
WHERE
notes = 'SEED000000008208' AND 
seed_name = 'SYB_fixed_0097_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009087'
WHERE
notes = 'SEED000000009087' AND 
seed_name = 'SYB_fixed_0976_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008921'
WHERE
notes = 'SEED000000008921' AND 
seed_name = 'SYB_fixed_0810_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008519'
WHERE
notes = 'SEED000000008519' AND 
seed_name = 'SYB_fixed_0408_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009147'
WHERE
notes = 'SEED000000009147' AND 
seed_name = 'SYB_fixed_1036_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008490'
WHERE
notes = 'SEED000000008490' AND 
seed_name = 'SYB_fixed_0379_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008170'
WHERE
notes = 'SEED000000008170' AND 
seed_name = 'SYB_fixed_0059_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008974'
WHERE
notes = 'SEED000000008974' AND 
seed_name = 'SYB_fixed_0863_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008260'
WHERE
notes = 'SEED000000008260' AND 
seed_name = 'SYB_fixed_0149_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008731'
WHERE
notes = 'SEED000000008731' AND 
seed_name = 'SYB_fixed_0620_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008963'
WHERE
notes = 'SEED000000008963' AND 
seed_name = 'SYB_fixed_0852_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009069'
WHERE
notes = 'SEED000000009069' AND 
seed_name = 'SYB_fixed_0958_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008254'
WHERE
notes = 'SEED000000008254' AND 
seed_name = 'SYB_fixed_0143_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008941'
WHERE
notes = 'SEED000000008941' AND 
seed_name = 'SYB_fixed_0830_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008632'
WHERE
notes = 'SEED000000008632' AND 
seed_name = 'SYB_fixed_0521_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008493'
WHERE
notes = 'SEED000000008493' AND 
seed_name = 'SYB_fixed_0382_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009024'
WHERE
notes = 'SEED000000009024' AND 
seed_name = 'SYB_fixed_0913_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009261'
WHERE
notes = 'SEED000000009261' AND 
seed_name = 'SYB_fixed_1150_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008539'
WHERE
notes = 'SEED000000008539' AND 
seed_name = 'SYB_fixed_0428_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009011'
WHERE
notes = 'SEED000000009011' AND 
seed_name = 'SYB_fixed_0900_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008517'
WHERE
notes = 'SEED000000008517' AND 
seed_name = 'SYB_fixed_0406_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009286'
WHERE
notes = 'SEED000000009286' AND 
seed_name = 'SYB_fixed_1175_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008349'
WHERE
notes = 'SEED000000008349' AND 
seed_name = 'SYB_fixed_0238_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008678'
WHERE
notes = 'SEED000000008678' AND 
seed_name = 'SYB_fixed_0567_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008226'
WHERE
notes = 'SEED000000008226' AND 
seed_name = 'SYB_fixed_0115_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007406'
WHERE
notes = 'SEED000000007406' AND 
seed_name = 'SYB_fixed_0047_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008975'
WHERE
notes = 'SEED000000008975' AND 
seed_name = 'SYB_fixed_0864_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009256'
WHERE
notes = 'SEED000000009256' AND 
seed_name = 'SYB_fixed_1145_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008184'
WHERE
notes = 'SEED000000008184' AND 
seed_name = 'SYB_fixed_0073_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008724'
WHERE
notes = 'SEED000000008724' AND 
seed_name = 'SYB_fixed_0613_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008710'
WHERE
notes = 'SEED000000008710' AND 
seed_name = 'SYB_fixed_0599_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009068'
WHERE
notes = 'SEED000000009068' AND 
seed_name = 'SYB_fixed_0957_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009232'
WHERE
notes = 'SEED000000009232' AND 
seed_name = 'SYB_fixed_1121_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008695'
WHERE
notes = 'SEED000000008695' AND 
seed_name = 'SYB_fixed_0584_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008760'
WHERE
notes = 'SEED000000008760' AND 
seed_name = 'SYB_fixed_0649_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009071'
WHERE
notes = 'SEED000000009071' AND 
seed_name = 'SYB_fixed_0960_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009073'
WHERE
notes = 'SEED000000009073' AND 
seed_name = 'SYB_fixed_0962_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008935'
WHERE
notes = 'SEED000000008935' AND 
seed_name = 'SYB_fixed_0824_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009289'
WHERE
notes = 'SEED000000009289' AND 
seed_name = 'SYB_fixed_1178_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008659'
WHERE
notes = 'SEED000000008659' AND 
seed_name = 'SYB_fixed_0548_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008746'
WHERE
notes = 'SEED000000008746' AND 
seed_name = 'SYB_fixed_0635_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008482'
WHERE
notes = 'SEED000000008482' AND 
seed_name = 'SYB_fixed_0371_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008446'
WHERE
notes = 'SEED000000008446' AND 
seed_name = 'SYB_fixed_0335_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009116'
WHERE
notes = 'SEED000000009116' AND 
seed_name = 'SYB_fixed_1005_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009245'
WHERE
notes = 'SEED000000009245' AND 
seed_name = 'SYB_fixed_1134_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009038'
WHERE
notes = 'SEED000000009038' AND 
seed_name = 'SYB_fixed_0927_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008189'
WHERE
notes = 'SEED000000008189' AND 
seed_name = 'SYB_fixed_0078_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008856'
WHERE
notes = 'SEED000000008856' AND 
seed_name = 'SYB_fixed_0745_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008749'
WHERE
notes = 'SEED000000008749' AND 
seed_name = 'SYB_fixed_0638_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008946'
WHERE
notes = 'SEED000000008946' AND 
seed_name = 'SYB_fixed_0835_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008962'
WHERE
notes = 'SEED000000008962' AND 
seed_name = 'SYB_fixed_0851_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008971'
WHERE
notes = 'SEED000000008971' AND 
seed_name = 'SYB_fixed_0860_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008376'
WHERE
notes = 'SEED000000008376' AND 
seed_name = 'SYB_fixed_0265_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008618'
WHERE
notes = 'SEED000000008618' AND 
seed_name = 'SYB_fixed_0507_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009223'
WHERE
notes = 'SEED000000009223' AND 
seed_name = 'SYB_fixed_1112_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008476'
WHERE
notes = 'SEED000000008476' AND 
seed_name = 'SYB_fixed_0365_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008514'
WHERE
notes = 'SEED000000008514' AND 
seed_name = 'SYB_fixed_0403_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009227'
WHERE
notes = 'SEED000000009227' AND 
seed_name = 'SYB_fixed_1116_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009131'
WHERE
notes = 'SEED000000009131' AND 
seed_name = 'SYB_fixed_1020_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008222'
WHERE
notes = 'SEED000000008222' AND 
seed_name = 'SYB_fixed_0111_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008498'
WHERE
notes = 'SEED000000008498' AND 
seed_name = 'SYB_fixed_0387_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008249'
WHERE
notes = 'SEED000000008249' AND 
seed_name = 'SYB_fixed_0138_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008996'
WHERE
notes = 'SEED000000008996' AND 
seed_name = 'SYB_fixed_0885_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008644'
WHERE
notes = 'SEED000000008644' AND 
seed_name = 'SYB_fixed_0533_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008243'
WHERE
notes = 'SEED000000008243' AND 
seed_name = 'SYB_fixed_0132_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008541'
WHERE
notes = 'SEED000000008541' AND 
seed_name = 'SYB_fixed_0430_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008706'
WHERE
notes = 'SEED000000008706' AND 
seed_name = 'SYB_fixed_0595_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008344'
WHERE
notes = 'SEED000000008344' AND 
seed_name = 'SYB_fixed_0233_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008736'
WHERE
notes = 'SEED000000008736' AND 
seed_name = 'SYB_fixed_0625_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008393'
WHERE
notes = 'SEED000000008393' AND 
seed_name = 'SYB_fixed_0282_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008416'
WHERE
notes = 'SEED000000008416' AND 
seed_name = 'SYB_fixed_0305_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009046'
WHERE
notes = 'SEED000000009046' AND 
seed_name = 'SYB_fixed_0935_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009106'
WHERE
notes = 'SEED000000009106' AND 
seed_name = 'SYB_fixed_0995_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009188'
WHERE
notes = 'SEED000000009188' AND 
seed_name = 'SYB_fixed_1077_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008792'
WHERE
notes = 'SEED000000008792' AND 
seed_name = 'SYB_fixed_0681_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009283'
WHERE
notes = 'SEED000000009283' AND 
seed_name = 'SYB_fixed_1172_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008912'
WHERE
notes = 'SEED000000008912' AND 
seed_name = 'SYB_fixed_0801_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008898'
WHERE
notes = 'SEED000000008898' AND 
seed_name = 'SYB_fixed_0787_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008560'
WHERE
notes = 'SEED000000008560' AND 
seed_name = 'SYB_fixed_0449_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008451'
WHERE
notes = 'SEED000000008451' AND 
seed_name = 'SYB_fixed_0340_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009270'
WHERE
notes = 'SEED000000009270' AND 
seed_name = 'SYB_fixed_1159_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007377'
WHERE
notes = 'SEED000000007377' AND 
seed_name = 'SYB_fixed_0018_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008449'
WHERE
notes = 'SEED000000008449' AND 
seed_name = 'SYB_fixed_0338_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009213'
WHERE
notes = 'SEED000000009213' AND 
seed_name = 'SYB_fixed_1102_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009047'
WHERE
notes = 'SEED000000009047' AND 
seed_name = 'SYB_fixed_0936_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008312'
WHERE
notes = 'SEED000000008312' AND 
seed_name = 'SYB_fixed_0201_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008164'
WHERE
notes = 'SEED000000008164' AND 
seed_name = 'SYB_fixed_0053_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008691'
WHERE
notes = 'SEED000000008691' AND 
seed_name = 'SYB_fixed_0580_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008620'
WHERE
notes = 'SEED000000008620' AND 
seed_name = 'SYB_fixed_0509_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008747'
WHERE
notes = 'SEED000000008747' AND 
seed_name = 'SYB_fixed_0636_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008794'
WHERE
notes = 'SEED000000008794' AND 
seed_name = 'SYB_fixed_0683_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008716'
WHERE
notes = 'SEED000000008716' AND 
seed_name = 'SYB_fixed_0605_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007382'
WHERE
notes = 'SEED000000007382' AND 
seed_name = 'SYB_fixed_0023_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009216'
WHERE
notes = 'SEED000000009216' AND 
seed_name = 'SYB_fixed_1105_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008264'
WHERE
notes = 'SEED000000008264' AND 
seed_name = 'SYB_fixed_0153_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008852'
WHERE
notes = 'SEED000000008852' AND 
seed_name = 'SYB_fixed_0741_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008478'
WHERE
notes = 'SEED000000008478' AND 
seed_name = 'SYB_fixed_0367_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008348'
WHERE
notes = 'SEED000000008348' AND 
seed_name = 'SYB_fixed_0237_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008603'
WHERE
notes = 'SEED000000008603' AND 
seed_name = 'SYB_fixed_0492_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008662'
WHERE
notes = 'SEED000000008662' AND 
seed_name = 'SYB_fixed_0551_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008382'
WHERE
notes = 'SEED000000008382' AND 
seed_name = 'SYB_fixed_0271_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008684'
WHERE
notes = 'SEED000000008684' AND 
seed_name = 'SYB_fixed_0573_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009218'
WHERE
notes = 'SEED000000009218' AND 
seed_name = 'SYB_fixed_1107_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009274'
WHERE
notes = 'SEED000000009274' AND 
seed_name = 'SYB_fixed_1163_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008958'
WHERE
notes = 'SEED000000008958' AND 
seed_name = 'SYB_fixed_0847_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009183'
WHERE
notes = 'SEED000000009183' AND 
seed_name = 'SYB_fixed_1072_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008626'
WHERE
notes = 'SEED000000008626' AND 
seed_name = 'SYB_fixed_0515_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009290'
WHERE
notes = 'SEED000000009290' AND 
seed_name = 'SYB_fixed_1179_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008802'
WHERE
notes = 'SEED000000008802' AND 
seed_name = 'SYB_fixed_0691_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008489'
WHERE
notes = 'SEED000000008489' AND 
seed_name = 'SYB_fixed_0378_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008721'
WHERE
notes = 'SEED000000008721' AND 
seed_name = 'SYB_fixed_0610_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008565'
WHERE
notes = 'SEED000000008565' AND 
seed_name = 'SYB_fixed_0454_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008686'
WHERE
notes = 'SEED000000008686' AND 
seed_name = 'SYB_fixed_0575_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008889'
WHERE
notes = 'SEED000000008889' AND 
seed_name = 'SYB_fixed_0778_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008766'
WHERE
notes = 'SEED000000008766' AND 
seed_name = 'SYB_fixed_0655_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008892'
WHERE
notes = 'SEED000000008892' AND 
seed_name = 'SYB_fixed_0781_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009028'
WHERE
notes = 'SEED000000009028' AND 
seed_name = 'SYB_fixed_0917_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008801'
WHERE
notes = 'SEED000000008801' AND 
seed_name = 'SYB_fixed_0690_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009265'
WHERE
notes = 'SEED000000009265' AND 
seed_name = 'SYB_fixed_1154_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008464'
WHERE
notes = 'SEED000000008464' AND 
seed_name = 'SYB_fixed_0353_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009114'
WHERE
notes = 'SEED000000009114' AND 
seed_name = 'SYB_fixed_1003_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009241'
WHERE
notes = 'SEED000000009241' AND 
seed_name = 'SYB_fixed_1130_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008928'
WHERE
notes = 'SEED000000008928' AND 
seed_name = 'SYB_fixed_0817_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008723'
WHERE
notes = 'SEED000000008723' AND 
seed_name = 'SYB_fixed_0612_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009150'
WHERE
notes = 'SEED000000009150' AND 
seed_name = 'SYB_fixed_1039_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008553'
WHERE
notes = 'SEED000000008553' AND 
seed_name = 'SYB_fixed_0442_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008758'
WHERE
notes = 'SEED000000008758' AND 
seed_name = 'SYB_fixed_0647_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009129'
WHERE
notes = 'SEED000000009129' AND 
seed_name = 'SYB_fixed_1018_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009247'
WHERE
notes = 'SEED000000009247' AND 
seed_name = 'SYB_fixed_1136_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008900'
WHERE
notes = 'SEED000000008900' AND 
seed_name = 'SYB_fixed_0789_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008574'
WHERE
notes = 'SEED000000008574' AND 
seed_name = 'SYB_fixed_0463_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008936'
WHERE
notes = 'SEED000000008936' AND 
seed_name = 'SYB_fixed_0825_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008573'
WHERE
notes = 'SEED000000008573' AND 
seed_name = 'SYB_fixed_0462_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009082'
WHERE
notes = 'SEED000000009082' AND 
seed_name = 'SYB_fixed_0971_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009212'
WHERE
notes = 'SEED000000009212' AND 
seed_name = 'SYB_fixed_1101_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008550'
WHERE
notes = 'SEED000000008550' AND 
seed_name = 'SYB_fixed_0439_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008195'
WHERE
notes = 'SEED000000008195' AND 
seed_name = 'SYB_fixed_0084_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008884'
WHERE
notes = 'SEED000000008884' AND 
seed_name = 'SYB_fixed_0773_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009137'
WHERE
notes = 'SEED000000009137' AND 
seed_name = 'SYB_fixed_1026_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008681'
WHERE
notes = 'SEED000000008681' AND 
seed_name = 'SYB_fixed_0570_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008218'
WHERE
notes = 'SEED000000008218' AND 
seed_name = 'SYB_fixed_0107_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008409'
WHERE
notes = 'SEED000000008409' AND 
seed_name = 'SYB_fixed_0298_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008426'
WHERE
notes = 'SEED000000008426' AND 
seed_name = 'SYB_fixed_0315_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008769'
WHERE
notes = 'SEED000000008769' AND 
seed_name = 'SYB_fixed_0658_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009109'
WHERE
notes = 'SEED000000009109' AND 
seed_name = 'SYB_fixed_0998_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008826'
WHERE
notes = 'SEED000000008826' AND 
seed_name = 'SYB_fixed_0715_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008561'
WHERE
notes = 'SEED000000008561' AND 
seed_name = 'SYB_fixed_0450_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008247'
WHERE
notes = 'SEED000000008247' AND 
seed_name = 'SYB_fixed_0136_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008596'
WHERE
notes = 'SEED000000008596' AND 
seed_name = 'SYB_fixed_0485_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008546'
WHERE
notes = 'SEED000000008546' AND 
seed_name = 'SYB_fixed_0435_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009091'
WHERE
notes = 'SEED000000009091' AND 
seed_name = 'SYB_fixed_0980_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009173'
WHERE
notes = 'SEED000000009173' AND 
seed_name = 'SYB_fixed_1062_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008316'
WHERE
notes = 'SEED000000008316' AND 
seed_name = 'SYB_fixed_0205_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008531'
WHERE
notes = 'SEED000000008531' AND 
seed_name = 'SYB_fixed_0420_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008190'
WHERE
notes = 'SEED000000008190' AND 
seed_name = 'SYB_fixed_0079_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008323'
WHERE
notes = 'SEED000000008323' AND 
seed_name = 'SYB_fixed_0212_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008474'
WHERE
notes = 'SEED000000008474' AND 
seed_name = 'SYB_fixed_0363_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008763'
WHERE
notes = 'SEED000000008763' AND 
seed_name = 'SYB_fixed_0652_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009045'
WHERE
notes = 'SEED000000009045' AND 
seed_name = 'SYB_fixed_0934_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008965'
WHERE
notes = 'SEED000000008965' AND 
seed_name = 'SYB_fixed_0854_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008694'
WHERE
notes = 'SEED000000008694' AND 
seed_name = 'SYB_fixed_0583_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009281'
WHERE
notes = 'SEED000000009281' AND 
seed_name = 'SYB_fixed_1170_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008919'
WHERE
notes = 'SEED000000008919' AND 
seed_name = 'SYB_fixed_0808_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009035'
WHERE
notes = 'SEED000000009035' AND 
seed_name = 'SYB_fixed_0924_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008871'
WHERE
notes = 'SEED000000008871' AND 
seed_name = 'SYB_fixed_0760_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008215'
WHERE
notes = 'SEED000000008215' AND 
seed_name = 'SYB_fixed_0104_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009086'
WHERE
notes = 'SEED000000009086' AND 
seed_name = 'SYB_fixed_0975_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008693'
WHERE
notes = 'SEED000000008693' AND 
seed_name = 'SYB_fixed_0582_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008601'
WHERE
notes = 'SEED000000008601' AND 
seed_name = 'SYB_fixed_0490_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008360'
WHERE
notes = 'SEED000000008360' AND 
seed_name = 'SYB_fixed_0249_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008591'
WHERE
notes = 'SEED000000008591' AND 
seed_name = 'SYB_fixed_0480_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009251'
WHERE
notes = 'SEED000000009251' AND 
seed_name = 'SYB_fixed_1140_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008877'
WHERE
notes = 'SEED000000008877' AND 
seed_name = 'SYB_fixed_0766_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008627'
WHERE
notes = 'SEED000000008627' AND 
seed_name = 'SYB_fixed_0516_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008285'
WHERE
notes = 'SEED000000008285' AND 
seed_name = 'SYB_fixed_0174_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008352'
WHERE
notes = 'SEED000000008352' AND 
seed_name = 'SYB_fixed_0241_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009242'
WHERE
notes = 'SEED000000009242' AND 
seed_name = 'SYB_fixed_1131_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008408'
WHERE
notes = 'SEED000000008408' AND 
seed_name = 'SYB_fixed_0297_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007403'
WHERE
notes = 'SEED000000007403' AND 
seed_name = 'SYB_fixed_0044_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008503'
WHERE
notes = 'SEED000000008503' AND 
seed_name = 'SYB_fixed_0392_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008427'
WHERE
notes = 'SEED000000008427' AND 
seed_name = 'SYB_fixed_0316_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009170'
WHERE
notes = 'SEED000000009170' AND 
seed_name = 'SYB_fixed_1059_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007387'
WHERE
notes = 'SEED000000007387' AND 
seed_name = 'SYB_fixed_0028_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008269'
WHERE
notes = 'SEED000000008269' AND 
seed_name = 'SYB_fixed_0158_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008872'
WHERE
notes = 'SEED000000008872' AND 
seed_name = 'SYB_fixed_0761_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008623'
WHERE
notes = 'SEED000000008623' AND 
seed_name = 'SYB_fixed_0512_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008959'
WHERE
notes = 'SEED000000008959' AND 
seed_name = 'SYB_fixed_0848_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008511'
WHERE
notes = 'SEED000000008511' AND 
seed_name = 'SYB_fixed_0400_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008442'
WHERE
notes = 'SEED000000008442' AND 
seed_name = 'SYB_fixed_0331_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008454'
WHERE
notes = 'SEED000000008454' AND 
seed_name = 'SYB_fixed_0343_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008980'
WHERE
notes = 'SEED000000008980' AND 
seed_name = 'SYB_fixed_0869_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008865'
WHERE
notes = 'SEED000000008865' AND 
seed_name = 'SYB_fixed_0754_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008412'
WHERE
notes = 'SEED000000008412' AND 
seed_name = 'SYB_fixed_0301_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008995'
WHERE
notes = 'SEED000000008995' AND 
seed_name = 'SYB_fixed_0884_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009031'
WHERE
notes = 'SEED000000009031' AND 
seed_name = 'SYB_fixed_0920_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008339'
WHERE
notes = 'SEED000000008339' AND 
seed_name = 'SYB_fixed_0228_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009266'
WHERE
notes = 'SEED000000009266' AND 
seed_name = 'SYB_fixed_1155_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008450'
WHERE
notes = 'SEED000000008450' AND 
seed_name = 'SYB_fixed_0339_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008207'
WHERE
notes = 'SEED000000008207' AND 
seed_name = 'SYB_fixed_0096_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008737'
WHERE
notes = 'SEED000000008737' AND 
seed_name = 'SYB_fixed_0626_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008528'
WHERE
notes = 'SEED000000008528' AND 
seed_name = 'SYB_fixed_0417_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008325'
WHERE
notes = 'SEED000000008325' AND 
seed_name = 'SYB_fixed_0214_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009100'
WHERE
notes = 'SEED000000009100' AND 
seed_name = 'SYB_fixed_0989_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008767'
WHERE
notes = 'SEED000000008767' AND 
seed_name = 'SYB_fixed_0656_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008772'
WHERE
notes = 'SEED000000008772' AND 
seed_name = 'SYB_fixed_0661_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009159'
WHERE
notes = 'SEED000000009159' AND 
seed_name = 'SYB_fixed_1048_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008309'
WHERE
notes = 'SEED000000008309' AND 
seed_name = 'SYB_fixed_0198_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008751'
WHERE
notes = 'SEED000000008751' AND 
seed_name = 'SYB_fixed_0640_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008835'
WHERE
notes = 'SEED000000008835' AND 
seed_name = 'SYB_fixed_0724_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008690'
WHERE
notes = 'SEED000000008690' AND 
seed_name = 'SYB_fixed_0579_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008188'
WHERE
notes = 'SEED000000008188' AND 
seed_name = 'SYB_fixed_0077_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009208'
WHERE
notes = 'SEED000000009208' AND 
seed_name = 'SYB_fixed_1097_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008642'
WHERE
notes = 'SEED000000008642' AND 
seed_name = 'SYB_fixed_0531_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008863'
WHERE
notes = 'SEED000000008863' AND 
seed_name = 'SYB_fixed_0752_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007395'
WHERE
notes = 'SEED000000007395' AND 
seed_name = 'SYB_fixed_0036_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007371'
WHERE
notes = 'SEED000000007371' AND 
seed_name = 'SYB_fixed_0012_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009103'
WHERE
notes = 'SEED000000009103' AND 
seed_name = 'SYB_fixed_0992_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008394'
WHERE
notes = 'SEED000000008394' AND 
seed_name = 'SYB_fixed_0283_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008262'
WHERE
notes = 'SEED000000008262' AND 
seed_name = 'SYB_fixed_0151_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009196'
WHERE
notes = 'SEED000000009196' AND 
seed_name = 'SYB_fixed_1085_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008650'
WHERE
notes = 'SEED000000008650' AND 
seed_name = 'SYB_fixed_0539_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008581'
WHERE
notes = 'SEED000000008581' AND 
seed_name = 'SYB_fixed_0470_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007407'
WHERE
notes = 'SEED000000007407' AND 
seed_name = 'SYB_fixed_0048_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008615'
WHERE
notes = 'SEED000000008615' AND 
seed_name = 'SYB_fixed_0504_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008257'
WHERE
notes = 'SEED000000008257' AND 
seed_name = 'SYB_fixed_0146_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008355'
WHERE
notes = 'SEED000000008355' AND 
seed_name = 'SYB_fixed_0244_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009140'
WHERE
notes = 'SEED000000009140' AND 
seed_name = 'SYB_fixed_1029_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008743'
WHERE
notes = 'SEED000000008743' AND 
seed_name = 'SYB_fixed_0632_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008396'
WHERE
notes = 'SEED000000008396' AND 
seed_name = 'SYB_fixed_0285_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008972'
WHERE
notes = 'SEED000000008972' AND 
seed_name = 'SYB_fixed_0861_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009293'
WHERE
notes = 'SEED000000009293' AND 
seed_name = 'SYB_fixed_1182_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008883'
WHERE
notes = 'SEED000000008883' AND 
seed_name = 'SYB_fixed_0772_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008776'
WHERE
notes = 'SEED000000008776' AND 
seed_name = 'SYB_fixed_0665_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008920'
WHERE
notes = 'SEED000000008920' AND 
seed_name = 'SYB_fixed_0809_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009255'
WHERE
notes = 'SEED000000009255' AND 
seed_name = 'SYB_fixed_1144_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008397'
WHERE
notes = 'SEED000000008397' AND 
seed_name = 'SYB_fixed_0286_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008162'
WHERE
notes = 'SEED000000008162' AND 
seed_name = 'SYB_fixed_0051_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008939'
WHERE
notes = 'SEED000000008939' AND 
seed_name = 'SYB_fixed_0828_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008954'
WHERE
notes = 'SEED000000008954' AND 
seed_name = 'SYB_fixed_0843_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008205'
WHERE
notes = 'SEED000000008205' AND 
seed_name = 'SYB_fixed_0094_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008558'
WHERE
notes = 'SEED000000008558' AND 
seed_name = 'SYB_fixed_0447_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007401'
WHERE
notes = 'SEED000000007401' AND 
seed_name = 'SYB_fixed_0042_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008388'
WHERE
notes = 'SEED000000008388' AND 
seed_name = 'SYB_fixed_0277_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008308'
WHERE
notes = 'SEED000000008308' AND 
seed_name = 'SYB_fixed_0197_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008989'
WHERE
notes = 'SEED000000008989' AND 
seed_name = 'SYB_fixed_0878_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009233'
WHERE
notes = 'SEED000000009233' AND 
seed_name = 'SYB_fixed_1122_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008306'
WHERE
notes = 'SEED000000008306' AND 
seed_name = 'SYB_fixed_0195_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008672'
WHERE
notes = 'SEED000000008672' AND 
seed_name = 'SYB_fixed_0561_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007400'
WHERE
notes = 'SEED000000007400' AND 
seed_name = 'SYB_fixed_0041_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008276'
WHERE
notes = 'SEED000000008276' AND 
seed_name = 'SYB_fixed_0165_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008518'
WHERE
notes = 'SEED000000008518' AND 
seed_name = 'SYB_fixed_0407_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009288'
WHERE
notes = 'SEED000000009288' AND 
seed_name = 'SYB_fixed_1177_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008589'
WHERE
notes = 'SEED000000008589' AND 
seed_name = 'SYB_fixed_0478_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008640'
WHERE
notes = 'SEED000000008640' AND 
seed_name = 'SYB_fixed_0529_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008685'
WHERE
notes = 'SEED000000008685' AND 
seed_name = 'SYB_fixed_0574_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008400'
WHERE
notes = 'SEED000000008400' AND 
seed_name = 'SYB_fixed_0289_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009235'
WHERE
notes = 'SEED000000009235' AND 
seed_name = 'SYB_fixed_1124_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008927'
WHERE
notes = 'SEED000000008927' AND 
seed_name = 'SYB_fixed_0816_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009155'
WHERE
notes = 'SEED000000009155' AND 
seed_name = 'SYB_fixed_1044_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008421'
WHERE
notes = 'SEED000000008421' AND 
seed_name = 'SYB_fixed_0310_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008584'
WHERE
notes = 'SEED000000008584' AND 
seed_name = 'SYB_fixed_0473_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009237'
WHERE
notes = 'SEED000000009237' AND 
seed_name = 'SYB_fixed_1126_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008214'
WHERE
notes = 'SEED000000008214' AND 
seed_name = 'SYB_fixed_0103_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008395'
WHERE
notes = 'SEED000000008395' AND 
seed_name = 'SYB_fixed_0284_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008949'
WHERE
notes = 'SEED000000008949' AND 
seed_name = 'SYB_fixed_0838_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008844'
WHERE
notes = 'SEED000000008844' AND 
seed_name = 'SYB_fixed_0733_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008167'
WHERE
notes = 'SEED000000008167' AND 
seed_name = 'SYB_fixed_0056_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008530'
WHERE
notes = 'SEED000000008530' AND 
seed_name = 'SYB_fixed_0419_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009238'
WHERE
notes = 'SEED000000009238' AND 
seed_name = 'SYB_fixed_1127_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009268'
WHERE
notes = 'SEED000000009268' AND 
seed_name = 'SYB_fixed_1157_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008795'
WHERE
notes = 'SEED000000008795' AND 
seed_name = 'SYB_fixed_0684_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008487'
WHERE
notes = 'SEED000000008487' AND 
seed_name = 'SYB_fixed_0376_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008726'
WHERE
notes = 'SEED000000008726' AND 
seed_name = 'SYB_fixed_0615_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008987'
WHERE
notes = 'SEED000000008987' AND 
seed_name = 'SYB_fixed_0876_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008624'
WHERE
notes = 'SEED000000008624' AND 
seed_name = 'SYB_fixed_0513_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008881'
WHERE
notes = 'SEED000000008881' AND 
seed_name = 'SYB_fixed_0770_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008862'
WHERE
notes = 'SEED000000008862' AND 
seed_name = 'SYB_fixed_0751_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008563'
WHERE
notes = 'SEED000000008563' AND 
seed_name = 'SYB_fixed_0452_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009211'
WHERE
notes = 'SEED000000009211' AND 
seed_name = 'SYB_fixed_1100_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008477'
WHERE
notes = 'SEED000000008477' AND 
seed_name = 'SYB_fixed_0366_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008846'
WHERE
notes = 'SEED000000008846' AND 
seed_name = 'SYB_fixed_0735_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008809'
WHERE
notes = 'SEED000000008809' AND 
seed_name = 'SYB_fixed_0698_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009101'
WHERE
notes = 'SEED000000009101' AND 
seed_name = 'SYB_fixed_0990_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008896'
WHERE
notes = 'SEED000000008896' AND 
seed_name = 'SYB_fixed_0785_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008391'
WHERE
notes = 'SEED000000008391' AND 
seed_name = 'SYB_fixed_0280_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008267'
WHERE
notes = 'SEED000000008267' AND 
seed_name = 'SYB_fixed_0156_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009215'
WHERE
notes = 'SEED000000009215' AND 
seed_name = 'SYB_fixed_1104_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008192'
WHERE
notes = 'SEED000000008192' AND 
seed_name = 'SYB_fixed_0081_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008350'
WHERE
notes = 'SEED000000008350' AND 
seed_name = 'SYB_fixed_0239_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009041'
WHERE
notes = 'SEED000000009041' AND 
seed_name = 'SYB_fixed_0930_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008803'
WHERE
notes = 'SEED000000008803' AND 
seed_name = 'SYB_fixed_0692_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009056'
WHERE
notes = 'SEED000000009056' AND 
seed_name = 'SYB_fixed_0945_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009012'
WHERE
notes = 'SEED000000009012' AND 
seed_name = 'SYB_fixed_0901_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009292'
WHERE
notes = 'SEED000000009292' AND 
seed_name = 'SYB_fixed_1181_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009004'
WHERE
notes = 'SEED000000009004' AND 
seed_name = 'SYB_fixed_0893_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009160'
WHERE
notes = 'SEED000000009160' AND 
seed_name = 'SYB_fixed_1049_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008998'
WHERE
notes = 'SEED000000008998' AND 
seed_name = 'SYB_fixed_0887_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000007408'
WHERE
notes = 'SEED000000007408' AND 
seed_name = 'SYB_fixed_0049_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008911'
WHERE
notes = 'SEED000000008911' AND 
seed_name = 'SYB_fixed_0800_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008605'
WHERE
notes = 'SEED000000008605' AND 
seed_name = 'SYB_fixed_0494_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009222'
WHERE
notes = 'SEED000000009222' AND 
seed_name = 'SYB_fixed_1111_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009084'
WHERE
notes = 'SEED000000009084' AND 
seed_name = 'SYB_fixed_0973_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008677'
WHERE
notes = 'SEED000000008677' AND 
seed_name = 'SYB_fixed_0566_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008547'
WHERE
notes = 'SEED000000008547' AND 
seed_name = 'SYB_fixed_0436_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008655'
WHERE
notes = 'SEED000000008655' AND 
seed_name = 'SYB_fixed_0544_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008619'
WHERE
notes = 'SEED000000008619' AND 
seed_name = 'SYB_fixed_0508_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009123'
WHERE
notes = 'SEED000000009123' AND 
seed_name = 'SYB_fixed_1012_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008757'
WHERE
notes = 'SEED000000008757' AND 
seed_name = 'SYB_fixed_0646_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008778'
WHERE
notes = 'SEED000000008778' AND 
seed_name = 'SYB_fixed_0667_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009184'
WHERE
notes = 'SEED000000009184' AND 
seed_name = 'SYB_fixed_1073_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009113'
WHERE
notes = 'SEED000000009113' AND 
seed_name = 'SYB_fixed_1002_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008458'
WHERE
notes = 'SEED000000008458' AND 
seed_name = 'SYB_fixed_0347_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008320'
WHERE
notes = 'SEED000000008320' AND 
seed_name = 'SYB_fixed_0209_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008656'
WHERE
notes = 'SEED000000008656' AND 
seed_name = 'SYB_fixed_0545_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008193'
WHERE
notes = 'SEED000000008193' AND 
seed_name = 'SYB_fixed_0082_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008733'
WHERE
notes = 'SEED000000008733' AND 
seed_name = 'SYB_fixed_0622_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008403'
WHERE
notes = 'SEED000000008403' AND 
seed_name = 'SYB_fixed_0292_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009063'
WHERE
notes = 'SEED000000009063' AND 
seed_name = 'SYB_fixed_0952_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008365'
WHERE
notes = 'SEED000000008365' AND 
seed_name = 'SYB_fixed_0254_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008434'
WHERE
notes = 'SEED000000008434' AND 
seed_name = 'SYB_fixed_0323_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008296'
WHERE
notes = 'SEED000000008296' AND 
seed_name = 'SYB_fixed_0185_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008509'
WHERE
notes = 'SEED000000008509' AND 
seed_name = 'SYB_fixed_0398_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009134'
WHERE
notes = 'SEED000000009134' AND 
seed_name = 'SYB_fixed_1023_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008374'
WHERE
notes = 'SEED000000008374' AND 
seed_name = 'SYB_fixed_0263_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008419'
WHERE
notes = 'SEED000000008419' AND 
seed_name = 'SYB_fixed_0308_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008533'
WHERE
notes = 'SEED000000008533' AND 
seed_name = 'SYB_fixed_0422_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008969'
WHERE
notes = 'SEED000000008969' AND 
seed_name = 'SYB_fixed_0858_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008569'
WHERE
notes = 'SEED000000008569' AND 
seed_name = 'SYB_fixed_0458_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008858'
WHERE
notes = 'SEED000000008858' AND 
seed_name = 'SYB_fixed_0747_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008338'
WHERE
notes = 'SEED000000008338' AND 
seed_name = 'SYB_fixed_0227_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008545'
WHERE
notes = 'SEED000000008545' AND 
seed_name = 'SYB_fixed_0434_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008797'
WHERE
notes = 'SEED000000008797' AND 
seed_name = 'SYB_fixed_0686_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009022'
WHERE
notes = 'SEED000000009022' AND 
seed_name = 'SYB_fixed_0911_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008786'
WHERE
notes = 'SEED000000008786' AND 
seed_name = 'SYB_fixed_0675_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008882'
WHERE
notes = 'SEED000000008882' AND 
seed_name = 'SYB_fixed_0771_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008586'
WHERE
notes = 'SEED000000008586' AND 
seed_name = 'SYB_fixed_0475_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008682'
WHERE
notes = 'SEED000000008682' AND 
seed_name = 'SYB_fixed_0571_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009179'
WHERE
notes = 'SEED000000009179' AND 
seed_name = 'SYB_fixed_1068_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009295'
WHERE
notes = 'SEED000000009295' AND 
seed_name = 'SYB_fixed_1184_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008413'
WHERE
notes = 'SEED000000008413' AND 
seed_name = 'SYB_fixed_0302_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009016'
WHERE
notes = 'SEED000000009016' AND 
seed_name = 'SYB_fixed_0905_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008367'
WHERE
notes = 'SEED000000008367' AND 
seed_name = 'SYB_fixed_0256_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008834'
WHERE
notes = 'SEED000000008834' AND 
seed_name = 'SYB_fixed_0723_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008401'
WHERE
notes = 'SEED000000008401' AND 
seed_name = 'SYB_fixed_0290_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009224'
WHERE
notes = 'SEED000000009224' AND 
seed_name = 'SYB_fixed_1113_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008850'
WHERE
notes = 'SEED000000008850' AND 
seed_name = 'SYB_fixed_0739_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009088'
WHERE
notes = 'SEED000000009088' AND 
seed_name = 'SYB_fixed_0977_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008237'
WHERE
notes = 'SEED000000008237' AND 
seed_name = 'SYB_fixed_0126_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008318'
WHERE
notes = 'SEED000000008318' AND 
seed_name = 'SYB_fixed_0207_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008585'
WHERE
notes = 'SEED000000008585' AND 
seed_name = 'SYB_fixed_0474_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009272'
WHERE
notes = 'SEED000000009272' AND 
seed_name = 'SYB_fixed_1161_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008986'
WHERE
notes = 'SEED000000008986' AND 
seed_name = 'SYB_fixed_0875_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009254'
WHERE
notes = 'SEED000000009254' AND 
seed_name = 'SYB_fixed_1143_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009053'
WHERE
notes = 'SEED000000009053' AND 
seed_name = 'SYB_fixed_0942_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008359'
WHERE
notes = 'SEED000000008359' AND 
seed_name = 'SYB_fixed_0248_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008445'
WHERE
notes = 'SEED000000008445' AND 
seed_name = 'SYB_fixed_0334_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000009296'
WHERE
notes = 'SEED000000009296' AND 
seed_name = 'SYB_fixed_1185_SEED-001'; 

UPDATE germplasm.seed
SET
notes ='SYB-SEED000000008564'
WHERE
notes = 'SEED000000008564' AND 
seed_name = 'SYB_fixed_0453_SEED-001'; 



--rollback SELECT NULL;