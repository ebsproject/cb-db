--liquibase formatted sql

--changeset postgres:update_SYB_fixture_germplasm_cross_parent_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment:BDS-3516 Update Soybean fixture germplasm_cross_parent



UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-90' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-3-B|IT18IRRIHQ-10-B-5-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-97' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-1-1|IT18IRRIHQ-20-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-94' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-2-B|IT18IRRIHQ-10-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0716_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0716|SYB_fixed_0716' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0716' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0069_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0069|SYB_fixed_0069' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0069' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0170_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0170|SYB_fixed_0170' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0170' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-391' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0170|SYB_fixed_0170' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0170' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-81' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0170|SYB_fixed_0170' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0170' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1010|SYB_fixed_1010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0985_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0985|SYB_fixed_0985' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0985' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0034_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0034|SYB_fixed_0034' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0034' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-143' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-3-2-B|IT18IRRIHQ-15-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0375_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0375|SYB_fixed_0375' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0375' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-596' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0375|SYB_fixed_0375' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0375' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1053_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1053|SYB_fixed_1053' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1053' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0468_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0468|SYB_fixed_0468' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0468' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-5/10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25|IT18IRRIHQ-25' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0358_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0358|SYB_fixed_0358' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0358' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-579' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0358|SYB_fixed_0358' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0358' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1066_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1066|SYB_fixed_1066' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1066' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0547_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0547|SYB_fixed_0547' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0547' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0821_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0821|SYB_fixed_0821' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0821' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B|IT18IRRIHQ-6-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0559_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0559|SYB_fixed_0559' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0559' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0025_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0025|SYB_fixed_0025' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0025' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0700_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0700|SYB_fixed_0700' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0700' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0404_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0404|SYB_fixed_0404' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0404' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0514_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0514|SYB_fixed_0514' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0514' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-2-2|IT18IRRIHQ-3-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-60' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-3-B|IT18IRRIHQ-7-B-2-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-88' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-2-2|IT18IRRIHQ-18-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0427_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0427|SYB_fixed_0427' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0427' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0597_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0597|SYB_fixed_0597' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0597' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0833_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0833|SYB_fixed_0833' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0833' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0467_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0467|SYB_fixed_0467' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0467' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0166_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0166|SYB_fixed_0166' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0166' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-387' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0166|SYB_fixed_0166' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0166' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0264_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0264|SYB_fixed_0264' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0264' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-485' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0264|SYB_fixed_0264' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0264' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0307_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0307|SYB_fixed_0307' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0307' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-528' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0307|SYB_fixed_0307' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0307' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B|IT18IRRIHQ-15-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0711_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0711|SYB_fixed_0711' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0711' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-130' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-3-1-B|IT18IRRIHQ-14-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0591_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0591|SYB_fixed_0591' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0591' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0374_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0374|SYB_fixed_0374' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0374' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-595' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0374|SYB_fixed_0374' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0374' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-285' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0374|SYB_fixed_0374' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0374' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0125_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0125|SYB_fixed_0125' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0125' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-346' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0125|SYB_fixed_0125' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0125' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-3|IT18IRRIHQ-18-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0866_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0866|SYB_fixed_0866' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0866' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0823_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0823|SYB_fixed_0823' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0823' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0262_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0262|SYB_fixed_0262' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0262' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-483' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0262|SYB_fixed_0262' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0262' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0540_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0540|SYB_fixed_0540' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0540' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0243_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0243|SYB_fixed_0243' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0243' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-464' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0243|SYB_fixed_0243' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0243' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-93' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-4-1|IT18IRRIHQ-19-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0621_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0621|SYB_fixed_0621' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0621' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0182_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0182|SYB_fixed_0182' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0182' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-403' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0182|SYB_fixed_0182' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0182' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-93' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0182|SYB_fixed_0182' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0182' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0725_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0725|SYB_fixed_0725' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0725' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0756_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0756|SYB_fixed_0756' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0756' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0276_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0276|SYB_fixed_0276' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0276' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-497' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0276|SYB_fixed_0276' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0276' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-90' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-4-1|IT18IRRIHQ-18-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1060_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1060|SYB_fixed_1060' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1060' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-5|IT18IRRIHQ-11-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0489_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0489|SYB_fixed_0489' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0489' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0215_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0215|SYB_fixed_0215' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0215' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-436' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0215|SYB_fixed_0215' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0215' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0469_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0469|SYB_fixed_0469' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0469' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1040_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1040|SYB_fixed_1040' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1040' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0178_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0178|SYB_fixed_0178' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0178' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-399' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0178|SYB_fixed_0178' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0178' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-1-1-B|IT18IRRIHQ-2-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-3|IT18IRRIHQ-12-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0231_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0231|SYB_fixed_0231' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0231' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-452' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0231|SYB_fixed_0231' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0231' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0199_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0199|SYB_fixed_0199' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0199' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-420' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0199|SYB_fixed_0199' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0199' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0797_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0797|SYB_fixed_0797' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0797' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-3-1|IT18IRRIHQ-5-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0222_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0222|SYB_fixed_0222' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0222' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-443' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0222|SYB_fixed_0222' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0222' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-133' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0222|SYB_fixed_0222' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0222' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-2|IT18IRRIHQ-15-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0820_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0820|SYB_fixed_0820' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0820' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-64' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-2-1|IT18IRRIHQ-13-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-42' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-4-2|IT18IRRIHQ-9-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0246_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0246|SYB_fixed_0246' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0246' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-467' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0246|SYB_fixed_0246' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0246' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-157' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-5-1-B|IT18IRRIHQ-17-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-71' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-4-1|IT18IRRIHQ-15-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0200_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0200|SYB_fixed_0200' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0200' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-421' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0200|SYB_fixed_0200' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0200' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0052_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0052|SYB_fixed_0052' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0052' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-2|IT18IRRIHQ-17-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0278_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0278|SYB_fixed_0278' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0278' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-499' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0278|SYB_fixed_0278' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0278' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0116_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0116|SYB_fixed_0116' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0116' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-337' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0116|SYB_fixed_0116' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0116' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0999_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0999|SYB_fixed_0999' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0999' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0060_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0060|SYB_fixed_0060' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0060' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-1|IT18IRRIHQ-12-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-1/9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4|IT18IRRIHQ-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-43' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-5-1|IT18IRRIHQ-9-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-165' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-4-1-B|IT18IRRIHQ-17-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0783_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0783|SYB_fixed_0783' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0783' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0471_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0471|SYB_fixed_0471' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0471' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-221' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-5-1-B|IT18IRRIHQ-25-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0180_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0180|SYB_fixed_0180' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0180' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-401' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0180|SYB_fixed_0180' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0180' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0487_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0487|SYB_fixed_0487' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0487' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0988_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0988|SYB_fixed_0988' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0988' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-58' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-4-2|IT18IRRIHQ-12-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0994_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0994|SYB_fixed_0994' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0994' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0688_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0688|SYB_fixed_0688' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0688' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0699_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0699|SYB_fixed_0699' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0699' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0657_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0657|SYB_fixed_0657' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0657' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-1|IT18IRRIHQ-2-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-41' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-2-2|IT18IRRIHQ-9-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0362_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0362|SYB_fixed_0362' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0362' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-583' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0362|SYB_fixed_0362' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0362' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-132' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-1-2-B|IT18IRRIHQ-14-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0895_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0895|SYB_fixed_0895' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0895' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0003' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0003' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0009|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0009|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0898_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0898|SYB_fixed_0898' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0898' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0309_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0309|SYB_fixed_0309' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0309' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-530' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0309|SYB_fixed_0309' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0309' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0865_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0865|SYB_fixed_0865' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0865' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1114_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1114|SYB_fixed_1114' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1114' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0065_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0065|SYB_fixed_0065' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0065' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0397_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0397|SYB_fixed_0397' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0397' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-1|IT18IRRIHQ-9-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0746_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0746|SYB_fixed_0746' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0746' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1092_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1092|SYB_fixed_1092' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1092' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1004|SYB_fixed_1004' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-1-2|IT18IRRIHQ-2-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-194' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-3-2-B|IT18IRRIHQ-20-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1084_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1084|SYB_fixed_1084' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1084' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0873_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0873|SYB_fixed_0873' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0873' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0859_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0859|SYB_fixed_0859' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0859' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-83' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-2-2|IT18IRRIHQ-17-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0066_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0066|SYB_fixed_0066' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0066' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0603_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0603|SYB_fixed_0603' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0603' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-5|IT18IRRIHQ-4-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-173' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-3-1-B|IT18IRRIHQ-18-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-93' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-4-2|IT18IRRIHQ-19-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0970_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0970|SYB_fixed_0970' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0970' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-73' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-4-1-B|IT18IRRIHQ-8-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-138' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-4-1-B|IT18IRRIHQ-15-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0159_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0159|SYB_fixed_0159' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0159' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-380' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0159|SYB_fixed_0159' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0159' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-70' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0159|SYB_fixed_0159' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0159' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-61' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-3-2|IT18IRRIHQ-13-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-88' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-4-2-B|IT18IRRIHQ-10-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-53' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-2-1|IT18IRRIHQ-11-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-4|IT18IRRIHQ-13-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0780_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0780|SYB_fixed_0780' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0780' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0674_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0674|SYB_fixed_0674' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0674' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-46' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-4|IT18IRRIHQ-10-B-2-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-85' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-4-B|IT18IRRIHQ-10-B-2-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-66' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-4-2-B|IT18IRRIHQ-7-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0322_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0322|SYB_fixed_0322' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0322' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-543' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0322|SYB_fixed_0322' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0322' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1106_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1106|SYB_fixed_1106' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1106' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-1|IT18IRRIHQ-18-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0145_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0145|SYB_fixed_0145' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0145' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-366' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0145|SYB_fixed_0145' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0145' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0695_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0695|SYB_fixed_0695' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0695' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0203_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0203|SYB_fixed_0203' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0203' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-424' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0203|SYB_fixed_0203' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0203' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0281_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0281|SYB_fixed_0281' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0281' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-502' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0281|SYB_fixed_0281' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0281' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-192' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0281|SYB_fixed_0281' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0281' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0411_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0411|SYB_fixed_0411' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0411' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1027_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1027|SYB_fixed_1027' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1027' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-5|IT18IRRIHQ-21-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1022_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1022|SYB_fixed_1022' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1022' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1015_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1015|SYB_fixed_1015' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1015' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0216_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0216|SYB_fixed_0216' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0216' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-437' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0216|SYB_fixed_0216' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0216' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-127' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0216|SYB_fixed_0216' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0216' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0148_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0148|SYB_fixed_0148' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0148' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-369' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0148|SYB_fixed_0148' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0148' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-59' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0148|SYB_fixed_0148' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0148' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0314_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0314|SYB_fixed_0314' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0314' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-535' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0314|SYB_fixed_0314' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0314' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0807_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0807|SYB_fixed_0807' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0807' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0703_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0703|SYB_fixed_0703' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0703' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-2-1|IT18IRRIHQ-3-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0021_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0021|SYB_fixed_0021' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0021' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0426_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0426|SYB_fixed_0426' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0426' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0619_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0619|SYB_fixed_0619' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0619' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0991_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0991|SYB_fixed_0991' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0991' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-4-1|IT18IRRIHQ-3-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-84' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-1-B|IT18IRRIHQ-10-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1171_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1171|SYB_fixed_1171' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1171' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0926_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0926|SYB_fixed_0926' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0926' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-134' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-2-1-B|IT18IRRIHQ-14-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-3-B|IT18IRRIHQ-2-B-5-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0956_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0956|SYB_fixed_0956' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0956' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0357_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0357|SYB_fixed_0357' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0357' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-578' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0357|SYB_fixed_0357' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0357' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-268' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0357|SYB_fixed_0357' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0357' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0499_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0499|SYB_fixed_0499' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0499' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-72' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-1-2|IT18IRRIHQ-15-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0070_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0070|SYB_fixed_0070' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0070' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B|IT18IRRIHQ-10-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B|IT18IRRIHQ-10-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B|IT18IRRIHQ-10-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0888_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0888|SYB_fixed_0888' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0888' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0967_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0967|SYB_fixed_0967' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0967' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-98' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-2-2|IT18IRRIHQ-20-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0557_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0557|SYB_fixed_0557' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0557' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0140_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0140|SYB_fixed_0140' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0140' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-361' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0140|SYB_fixed_0140' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0140' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1174_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1174|SYB_fixed_1174' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1174' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0481_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0481|SYB_fixed_0481' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0481' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1041_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1041|SYB_fixed_1041' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1041' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0818_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0818|SYB_fixed_0818' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0818' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-142' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-2-2-B|IT18IRRIHQ-15-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0841_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0841|SYB_fixed_0841' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0841' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1152_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1152|SYB_fixed_1152' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1152' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0071_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0071|SYB_fixed_0071' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0071' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-58' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-4-1|IT18IRRIHQ-12-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-139' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-1-1-B|IT18IRRIHQ-15-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-91' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-2-B|IT18IRRIHQ-10-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1001|SYB_fixed_1001' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0396_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0396|SYB_fixed_0396' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0396' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0558_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0558|SYB_fixed_0558' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0558' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0109_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0109|SYB_fixed_0109' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0109' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-330' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0109|SYB_fixed_0109' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0109' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0464_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0464|SYB_fixed_0464' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0464' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0217_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0217|SYB_fixed_0217' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0217' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-438' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0217|SYB_fixed_0217' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0217' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0085_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0085|SYB_fixed_0085' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0085' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-215' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-2-1-B|IT18IRRIHQ-24-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0385_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0385|SYB_fixed_0385' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0385' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-3|IT18IRRIHQ-2-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-49' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-1|IT18IRRIHQ-10-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0993_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0993|SYB_fixed_0993' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0993' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0425_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0425|SYB_fixed_0425' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0425' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0510_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0510|SYB_fixed_0510' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0510' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0682_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0682|SYB_fixed_0682' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0682' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0842_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0842|SYB_fixed_0842' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0842' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-170' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-1-2-B|IT18IRRIHQ-18-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-188' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-4-2-B|IT18IRRIHQ-20-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-29' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-3-2|IT18IRRIHQ-6-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-47' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-5-1-B|IT18IRRIHQ-5-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-39' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-4-1|IT18IRRIHQ-8-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0882_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0882|SYB_fixed_0882' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0882' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0670_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0670|SYB_fixed_0670' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0670' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-1-2-B|IT18IRRIHQ-2-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0313_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0313|SYB_fixed_0313' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0313' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-534' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0313|SYB_fixed_0313' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0313' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-5|IT18IRRIHQ-25-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-3/8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13|IT18IRRIHQ-13' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-82' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-3-1-B|IT18IRRIHQ-9-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0602_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0602|SYB_fixed_0602' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0602' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1074_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1074|SYB_fixed_1074' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1074' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0326_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0326|SYB_fixed_0326' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0326' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-547' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0326|SYB_fixed_0326' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0326' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0444_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0444|SYB_fixed_0444' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0444' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0364_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0364|SYB_fixed_0364' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0364' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-585' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0364|SYB_fixed_0364' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0364' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0565_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0565|SYB_fixed_0565' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0565' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-63' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-1-1|IT18IRRIHQ-13-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-96' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-4-2|IT18IRRIHQ-20-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0742_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0742|SYB_fixed_0742' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0742' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0505_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0505|SYB_fixed_0505' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0505' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-81' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-5-2|IT18IRRIHQ-17-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0361_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0361|SYB_fixed_0361' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0361' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-582' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0361|SYB_fixed_0361' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0361' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-94' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-3-2|IT18IRRIHQ-19-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5|IT18IRRIHQ-2-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0651_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0651|SYB_fixed_0651' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0651' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0168_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0168|SYB_fixed_0168' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0168' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-389' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0168|SYB_fixed_0168' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0168' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-79' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0168|SYB_fixed_0168' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0168' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0008|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0008|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-3|IT18IRRIHQ-3-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0161_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0161|SYB_fixed_0161' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0161' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-382' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0161|SYB_fixed_0161' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0161' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0996_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0996|SYB_fixed_0996' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0996' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0556_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0556|SYB_fixed_0556' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0556' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0456_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0456|SYB_fixed_0456' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0456' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1031_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1031|SYB_fixed_1031' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1031' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-1|IT18IRRIHQ-8-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0705_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0705|SYB_fixed_0705' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0705' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0072_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0072|SYB_fixed_0072' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0072' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0183_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0183|SYB_fixed_0183' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0183' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-404' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0183|SYB_fixed_0183' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0183' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0617_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0617|SYB_fixed_0617' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0617' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0890_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0890|SYB_fixed_0890' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0890' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-4|IT18IRRIHQ-15-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-121' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-1-1-B|IT18IRRIHQ-13-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0963_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0963|SYB_fixed_0963' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0963' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B|IT18IRRIHQ-11-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-3-1|IT18IRRIHQ-2-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-3|IT18IRRIHQ-6-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1061_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1061|SYB_fixed_1061' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1061' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0328_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0328|SYB_fixed_0328' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0328' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-549' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0328|SYB_fixed_0328' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0328' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-191' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-2-1-B|IT18IRRIHQ-20-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1087_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1087|SYB_fixed_1087' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1087' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0755_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0755|SYB_fixed_0755' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0755' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0446_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0446|SYB_fixed_0446' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0446' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0356_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0356|SYB_fixed_0356' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0356' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-577' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0356|SYB_fixed_0356' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0356' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0527_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0527|SYB_fixed_0527' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0527' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0287_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0287|SYB_fixed_0287' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0287' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-508' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0287|SYB_fixed_0287' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0287' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-2/8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8|IT18IRRIHQ-8' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-2|IT18IRRIHQ-11-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-48' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-3|IT18IRRIHQ-10-B-5-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0978_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0978|SYB_fixed_0978' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0978' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0915_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0915|SYB_fixed_0915' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0915' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-2/7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7|IT18IRRIHQ-7' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-30' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-3|IT18IRRIHQ-6-B-1-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-84' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-3-2|IT18IRRIHQ-17-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-49' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-2-1-B|IT18IRRIHQ-6-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-30' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-2|IT18IRRIHQ-6-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-100' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-5-1|IT18IRRIHQ-20-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1119_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1119|SYB_fixed_1119' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1119' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-206' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-5-1-B|IT18IRRIHQ-22-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-3|IT18IRRIHQ-2-B-5-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0790_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0790|SYB_fixed_0790' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0790' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0405_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0405|SYB_fixed_0405' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0405' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0389_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0389|SYB_fixed_0389' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0389' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0896_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0896|SYB_fixed_0896' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0896' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0329_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0329|SYB_fixed_0329' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0329' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-550' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0329|SYB_fixed_0329' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0329' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1180_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1180|SYB_fixed_1180' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1180' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0774_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0774|SYB_fixed_0774' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0774' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0202_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0202|SYB_fixed_0202' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0202' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-423' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0202|SYB_fixed_0202' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0202' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0122_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0122|SYB_fixed_0122' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0122' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-343' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0122|SYB_fixed_0122' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0122' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-33' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0122|SYB_fixed_0122' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0122' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0296_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0296|SYB_fixed_0296' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0296' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-517' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0296|SYB_fixed_0296' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0296' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-207' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0296|SYB_fixed_0296' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0296' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0581_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0581|SYB_fixed_0581' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0581' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0608_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0608|SYB_fixed_0608' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0608' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-4|IT18IRRIHQ-16-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B|IT18IRRIHQ-12-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-2|IT18IRRIHQ-4-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0114_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0114|SYB_fixed_0114' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0114' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-335' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0114|SYB_fixed_0114' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0114' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0877_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0877|SYB_fixed_0877' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0877' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0086_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0086|SYB_fixed_0086' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0086' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-4|IT18IRRIHQ-7-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0240_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0240|SYB_fixed_0240' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0240' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-461' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0240|SYB_fixed_0240' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0240' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0223_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0223|SYB_fixed_0223' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0223' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-444' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0223|SYB_fixed_0223' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0223' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-216' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-5-1-B|IT18IRRIHQ-24-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0628_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0628|SYB_fixed_0628' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0628' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-2|IT18IRRIHQ-4-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1091_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1091|SYB_fixed_1091' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1091' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-4|IT18IRRIHQ-14-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1025_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1025|SYB_fixed_1025' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1025' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0837_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0837|SYB_fixed_0837' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0837' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-42' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-2-1-B|IT18IRRIHQ-5-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0793_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0793|SYB_fixed_0793' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0793' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0402_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0402|SYB_fixed_0402' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0402' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0938_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0938|SYB_fixed_0938' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0938' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-2-1|IT18IRRIHQ-1-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-124' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-3-1|IT18IRRIHQ-25-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-4-2|IT18IRRIHQ-5-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0394_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0394|SYB_fixed_0394' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0394' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-113' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-3-2-B|IT18IRRIHQ-12-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-31' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-2-B|IT18IRRIHQ-4-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0344_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0344|SYB_fixed_0344' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0344' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-565' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0344|SYB_fixed_0344' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0344' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B|IT18IRRIHQ-7-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0002' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0002' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-36' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-1-1|IT18IRRIHQ-8-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0536_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0536|SYB_fixed_0536' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0536' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-87' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-1-1|IT18IRRIHQ-18-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0101_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0101|SYB_fixed_0101' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0101' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-322' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0101|SYB_fixed_0101' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0101' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1094_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1094|SYB_fixed_1094' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1094' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0399_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0399|SYB_fixed_0399' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0399' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-55' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-4-1|IT18IRRIHQ-11-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0596_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0596|SYB_fixed_0596' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0596' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0519_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0519|SYB_fixed_0519' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0519' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0983_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0983|SYB_fixed_0983' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0983' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-185' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-2-1-B|IT18IRRIHQ-19-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-212' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-1-1-B|IT18IRRIHQ-24-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0889_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0889|SYB_fixed_0889' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0889' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0445_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0445|SYB_fixed_0445' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0445' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0666_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0666|SYB_fixed_0666' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0666' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-34' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-1-B|IT18IRRIHQ-4-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B|IT18IRRIHQ-25-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0554_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0554|SYB_fixed_0554' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0554' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0274_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0274|SYB_fixed_0274' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0274' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-495' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0274|SYB_fixed_0274' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0274' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0576_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0576|SYB_fixed_0576' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0576' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1139_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1139|SYB_fixed_1139' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1139' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B|IT18IRRIHQ-22-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-5/6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21|IT18IRRIHQ-21' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0740_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0740|SYB_fixed_0740' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0740' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0648_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0648|SYB_fixed_0648' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0648' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0221_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0221|SYB_fixed_0221' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0221' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-442' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0221|SYB_fixed_0221' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0221' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-47' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-4-2|IT18IRRIHQ-10-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-39' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-5-2-B|IT18IRRIHQ-4-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0224_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0224|SYB_fixed_0224' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0224' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-445' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0224|SYB_fixed_0224' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0224' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0354_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0354|SYB_fixed_0354' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0354' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-575' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0354|SYB_fixed_0354' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0354' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-265' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0354|SYB_fixed_0354' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0354' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0585_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0585|SYB_fixed_0585' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0585' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0087_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0087|SYB_fixed_0087' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0087' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1017_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1017|SYB_fixed_1017' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1017' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B|IT18IRRIHQ-20-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1000_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1000|SYB_fixed_1000' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1000' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0588_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0588|SYB_fixed_0588' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0588' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0753_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0753|SYB_fixed_0753' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0753' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0120_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0120|SYB_fixed_0120' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0120' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-341' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0120|SYB_fixed_0120' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0120' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-99' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-1-1-B|IT18IRRIHQ-11-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-3|IT18IRRIHQ-19-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0929_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0929|SYB_fixed_0929' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0929' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-54' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-3-2|IT18IRRIHQ-11-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-4|IT18IRRIHQ-3-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0210_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0210|SYB_fixed_0210' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0210' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-431' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0210|SYB_fixed_0210' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0210' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-60' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-2-2|IT18IRRIHQ-12-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0680_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0680|SYB_fixed_0680' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0680' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-2|IT18IRRIHQ-18-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0068_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0068|SYB_fixed_0068' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0068' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0318_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0318|SYB_fixed_0318' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0318' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-539' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0318|SYB_fixed_0318' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0318' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-104' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-3-2-B|IT18IRRIHQ-11-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0892_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0892|SYB_fixed_0892' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0892' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0826_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0826|SYB_fixed_0826' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0826' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-106' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-4-1|IT18IRRIHQ-22-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0432_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0432|SYB_fixed_0432' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0432' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0578_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0578|SYB_fixed_0578' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0578' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0300_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0300|SYB_fixed_0300' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0300' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-521' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0300|SYB_fixed_0300' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0300' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-3|IT18IRRIHQ-4-B-3-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0415_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0415|SYB_fixed_0415' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0415' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-95' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-2-1|IT18IRRIHQ-19-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0708_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0708|SYB_fixed_0708' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0708' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-2|IT18IRRIHQ-5-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0758_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0758|SYB_fixed_0758' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0758' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1028_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1028|SYB_fixed_1028' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1028' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-68' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-1-2|IT18IRRIHQ-14-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0187_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0187|SYB_fixed_0187' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0187' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-408' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0187|SYB_fixed_0187' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0187' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0768_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0768|SYB_fixed_0768' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0768' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0208_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0208|SYB_fixed_0208' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0208' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-429' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0208|SYB_fixed_0208' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0208' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0355_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0355|SYB_fixed_0355' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0355' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-576' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0355|SYB_fixed_0355' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0355' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0759_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0759|SYB_fixed_0759' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0759' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1042_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1042|SYB_fixed_1042' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1042' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0782_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0782|SYB_fixed_0782' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0782' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0118_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0118|SYB_fixed_0118' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0118' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-339' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0118|SYB_fixed_0118' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0118' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-29' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0118|SYB_fixed_0118' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0118' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-1|IT18IRRIHQ-25-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0795_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0795|SYB_fixed_0795' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0795' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-2-B|IT18IRRIHQ-2-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1069_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1069|SYB_fixed_1069' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1069' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-117' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-4-1|IT18IRRIHQ-24-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0312_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0312|SYB_fixed_0312' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0312' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-533' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0312|SYB_fixed_0312' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0312' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0176_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0176|SYB_fixed_0176' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0176' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-397' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0176|SYB_fixed_0176' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0176' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0631_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0631|SYB_fixed_0631' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0631' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0004' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0004_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0004' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0765_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0765|SYB_fixed_0765' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0765' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-1|IT18IRRIHQ-3-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0011_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0011|SYB_fixed_0011' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0011' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0011|SYB_fixed_0011' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0011' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0242_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0242|SYB_fixed_0242' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0242' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-463' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0242|SYB_fixed_0242' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0242' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-4/9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19|IT18IRRIHQ-19' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-4|IT18IRRIHQ-19-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0163_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0163|SYB_fixed_0163' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0163' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-384' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0163|SYB_fixed_0163' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0163' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-5|IT18IRRIHQ-5-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0186_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0186|SYB_fixed_0186' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0186' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-407' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0186|SYB_fixed_0186' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0186' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-38' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-3-1|IT18IRRIHQ-8-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-65' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-4-2|IT18IRRIHQ-13-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0155_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0155|SYB_fixed_0155' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0155' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-376' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0155|SYB_fixed_0155' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0155' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0663_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0663|SYB_fixed_0663' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0663' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-79' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-5-2-B|IT18IRRIHQ-9-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2|IT18IRRIHQ-10-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0538_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0538|SYB_fixed_0538' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0538' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0095_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0095|SYB_fixed_0095' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0095' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-316' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0095|SYB_fixed_0095' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0095' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0555_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0555|SYB_fixed_0555' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0555' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0943_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0943|SYB_fixed_0943' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0943' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1125_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1125|SYB_fixed_1125' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1125' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-115' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-4-1|IT18IRRIHQ-23-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-122' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-4-1|IT18IRRIHQ-25-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-115' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-2-1-B|IT18IRRIHQ-12-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-2|IT18IRRIHQ-22-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0959_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0959|SYB_fixed_0959' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0959' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0251_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0251|SYB_fixed_0251' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0251' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-472' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0251|SYB_fixed_0251' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0251' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-2-B|IT18IRRIHQ-2-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0944_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0944|SYB_fixed_0944' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0944' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0676_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0676|SYB_fixed_0676' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0676' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1037_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1037|SYB_fixed_1037' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1037' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1141_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1141|SYB_fixed_1141' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1141' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1016_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1016|SYB_fixed_1016' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1016' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-92' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-5-1|IT18IRRIHQ-19-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-4-1-B|IT18IRRIHQ-3-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-5|IT18IRRIHQ-1-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-79' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-3-2|IT18IRRIHQ-16-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0293_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0293|SYB_fixed_0293' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0293' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-514' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0293|SYB_fixed_0293' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0293' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-4|IT18IRRIHQ-4-B-1-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-3|IT18IRRIHQ-21-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0517_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0517|SYB_fixed_0517' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0517' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0523_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0523|SYB_fixed_0523' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0523' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0409_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0409|SYB_fixed_0409' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0409' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1147_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1147|SYB_fixed_1147' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1147' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0040_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0040|SYB_fixed_0040' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0040' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1168_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1168|SYB_fixed_1168' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1168' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1132_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1132|SYB_fixed_1132' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1132' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0236_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0236|SYB_fixed_0236' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0236' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-457' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0236|SYB_fixed_0236' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0236' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3|IT18IRRIHQ-1-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0275_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0275|SYB_fixed_0275' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0275' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-496' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0275|SYB_fixed_0275' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0275' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-38' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-5-1-B|IT18IRRIHQ-4-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-43' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-2-2-B|IT18IRRIHQ-5-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0360_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0360|SYB_fixed_0360' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0360' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-581' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0360|SYB_fixed_0360' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0360' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0677_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0677|SYB_fixed_0677' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0677' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-61' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-3-1|IT18IRRIHQ-13-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-150' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-1-1-B|IT18IRRIHQ-16-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0940_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0940|SYB_fixed_0940' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0940' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-5|IT18IRRIHQ-15-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0460_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0460|SYB_fixed_0460' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0460' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-57' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-1-B|IT18IRRIHQ-6-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0600_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0600|SYB_fixed_0600' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0600' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-1|IT18IRRIHQ-4-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0880_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0880|SYB_fixed_0880' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0880' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-2|IT18IRRIHQ-1-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0839_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0839|SYB_fixed_0839' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0839' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-49' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-3|IT18IRRIHQ-10-B-1-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1011_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1011|SYB_fixed_1011' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1011' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0062_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0062|SYB_fixed_0062' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0062' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1078_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1078|SYB_fixed_1078' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1078' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-62' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-5-1|IT18IRRIHQ-13-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0552_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0552|SYB_fixed_0552' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0552' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0687_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0687|SYB_fixed_0687' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0687' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0016_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0016|SYB_fixed_0016' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0016' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0016|SYB_fixed_0016' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0016' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B|IT18IRRIHQ-18-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0333_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0333|SYB_fixed_0333' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0333' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-554' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0333|SYB_fixed_0333' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0333' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B|IT18IRRIHQ-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B|IT18IRRIHQ-9-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B|IT18IRRIHQ-10-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0443_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0443|SYB_fixed_0443' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0443' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-1-B|IT18IRRIHQ-2-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-2/6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6|IT18IRRIHQ-6' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0247_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0247|SYB_fixed_0247' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0247' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-468' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0247|SYB_fixed_0247' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0247' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-158' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0247|SYB_fixed_0247' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0247' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0592_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0592|SYB_fixed_0592' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0592' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0987_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0987|SYB_fixed_0987' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0987' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0110_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0110|SYB_fixed_0110' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0110' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-331' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0110|SYB_fixed_0110' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0110' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0984_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0984|SYB_fixed_0984' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0984' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0721_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0721|SYB_fixed_0721' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0721' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0644_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0644|SYB_fixed_0644' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0644' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0260_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0260|SYB_fixed_0260' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0260' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-481' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0260|SYB_fixed_0260' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0260' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-119' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-2-1|IT18IRRIHQ-24-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-190' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-1-2-B|IT18IRRIHQ-20-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0601_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0601|SYB_fixed_0601' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0601' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-3|IT18IRRIHQ-9-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0007|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0007|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-99' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-3-1|IT18IRRIHQ-20-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1075_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1075|SYB_fixed_1075' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1075' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0139_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0139|SYB_fixed_0139' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0139' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-360' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0139|SYB_fixed_0139' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0139' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0123_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0123|SYB_fixed_0123' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0123' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-344' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0123|SYB_fixed_0123' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0123' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-34' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0123|SYB_fixed_0123' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0123' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0694_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0694|SYB_fixed_0694' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0694' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0727_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0727|SYB_fixed_0727' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0727' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0380_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0380|SYB_fixed_0380' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0380' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0501_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0501|SYB_fixed_0501' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0501' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-68' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-1-1|IT18IRRIHQ-14-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0131_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0131|SYB_fixed_0131' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0131' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-352' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0131|SYB_fixed_0131' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0131' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0673_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0673|SYB_fixed_0673' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0673' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0350_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0350|SYB_fixed_0350' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0350' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-571' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0350|SYB_fixed_0350' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0350' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-56' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-1-2|IT18IRRIHQ-12-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0482_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0482|SYB_fixed_0482' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0482' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-48' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-1-1-B|IT18IRRIHQ-5-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-5-2-B|IT18IRRIHQ-1-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-66' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-5-2|IT18IRRIHQ-14-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0306_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0306|SYB_fixed_0306' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0306' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-527' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0306|SYB_fixed_0306' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0306' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1080_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1080|SYB_fixed_1080' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1080' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0019_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0019|SYB_fixed_0019' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0019' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0019|SYB_fixed_0019' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0019' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0381_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0381|SYB_fixed_0381' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0381' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-33' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-3-B|IT18IRRIHQ-4-B-3-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1076_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1076|SYB_fixed_1076' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1076' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-4-2|IT18IRRIHQ-1-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1079_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1079|SYB_fixed_1079' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1079' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-4/6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16|IT18IRRIHQ-16' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0950_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0950|SYB_fixed_0950' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0950' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-180' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-5-2-B|IT18IRRIHQ-19-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0767_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0767|SYB_fixed_0767' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0767' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0706_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0706|SYB_fixed_0706' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0706' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1013_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1013|SYB_fixed_1013' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1013' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-36' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-2-B|IT18IRRIHQ-4-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0150_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0150|SYB_fixed_0150' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0150' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-371' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0150|SYB_fixed_0150' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0150' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-86' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-3-B|IT18IRRIHQ-10-B-2-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0032_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0032|SYB_fixed_0032' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0032' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0712_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0712|SYB_fixed_0712' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0712' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0979_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0979|SYB_fixed_0979' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0979' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0639_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0639|SYB_fixed_0639' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0639' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0455_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0455|SYB_fixed_0455' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0455' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0914_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0914|SYB_fixed_0914' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0914' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1047_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1047|SYB_fixed_1047' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1047' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0359_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0359|SYB_fixed_0359' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0359' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-580' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0359|SYB_fixed_0359' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0359' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1146_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1146|SYB_fixed_1146' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1146' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1093_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1093|SYB_fixed_1093' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1093' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-4-B|IT18IRRIHQ-2-B-4-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-47' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-4-1|IT18IRRIHQ-10-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0465_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0465|SYB_fixed_0465' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0465' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-159' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-1-1-B|IT18IRRIHQ-17-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1095_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1095|SYB_fixed_1095' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1095' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1|IT18IRRIHQ-6-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0099_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0099|SYB_fixed_0099' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0099' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-320' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0099|SYB_fixed_0099' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0099' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0099|SYB_fixed_0099' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0099' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-3-1|IT18IRRIHQ-3-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-2|IT18IRRIHQ-14-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B|IT18IRRIHQ-17-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0668_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0668|SYB_fixed_0668' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0668' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0050_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0050|SYB_fixed_0050' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0050' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-97' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-5-2-B|IT18IRRIHQ-11-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-4|IT18IRRIHQ-12-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0193_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0193|SYB_fixed_0193' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0193' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-414' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0193|SYB_fixed_0193' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0193' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-3|IT18IRRIHQ-15-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0472_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0472|SYB_fixed_0472' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0472' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0748_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0748|SYB_fixed_0748' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0748' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0160_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0160|SYB_fixed_0160' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0160' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-381' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0160|SYB_fixed_0160' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0160' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-71' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0160|SYB_fixed_0160' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0160' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0937_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0937|SYB_fixed_0937' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0937' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0949_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0949|SYB_fixed_0949' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0949' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1|IT18IRRIHQ-4-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0762_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0762|SYB_fixed_0762' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0762' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1167_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1167|SYB_fixed_1167' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1167' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-4|IT18IRRIHQ-17-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B|IT18IRRIHQ-24-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0563_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0563|SYB_fixed_0563' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0563' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-30' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-1|IT18IRRIHQ-6-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0714_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0714|SYB_fixed_0714' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0714' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-114' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-3-1-B|IT18IRRIHQ-12-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-32' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-3-2|IT18IRRIHQ-7-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0749_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0749|SYB_fixed_0749' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0749' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0730_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0730|SYB_fixed_0730' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0730' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0058_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0058|SYB_fixed_0058' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0058' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0614_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0614|SYB_fixed_0614' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0614' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-213' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-4-1-B|IT18IRRIHQ-24-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-203' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-1-1-B|IT18IRRIHQ-22-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0259_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0259|SYB_fixed_0259' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0259' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-480' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0259|SYB_fixed_0259' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0259' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0737_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0737|SYB_fixed_0737' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0737' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1166_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1166|SYB_fixed_1166' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1166' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0883_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0883|SYB_fixed_0883' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0883' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0304_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0304|SYB_fixed_0304' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0304' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-525' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0304|SYB_fixed_0304' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0304' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-4-1-B|IT18IRRIHQ-1-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0204_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0204|SYB_fixed_0204' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0204' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-425' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0204|SYB_fixed_0204' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0204' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0922_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0922|SYB_fixed_0922' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0922' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1090_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1090|SYB_fixed_1090' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1090' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0037_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0037|SYB_fixed_0037' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0037' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0325_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0325|SYB_fixed_0325' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0325' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-546' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0325|SYB_fixed_0325' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0325' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0351_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0351|SYB_fixed_0351' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0351' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-572' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0351|SYB_fixed_0351' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0351' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1110_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1110|SYB_fixed_1110' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1110' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-2|IT18IRRIHQ-12-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-3|IT18IRRIHQ-11-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0856_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0856|SYB_fixed_0856' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0856' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-3|IT18IRRIHQ-20-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0832_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0832|SYB_fixed_0832' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0832' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-136' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-4-2-B|IT18IRRIHQ-14-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0457_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0457|SYB_fixed_0457' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0457' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0550_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0550|SYB_fixed_0550' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0550' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1165_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1165|SYB_fixed_1165' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1165' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-89' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-3-2|IT18IRRIHQ-18-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0902_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0902|SYB_fixed_0902' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0902' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0506_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0506|SYB_fixed_0506' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0506' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0424_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0424|SYB_fixed_0424' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0424' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0586_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0586|SYB_fixed_0586' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0586' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0219_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0219|SYB_fixed_0219' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0219' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-440' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0219|SYB_fixed_0219' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0219' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-119' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-5-2-B|IT18IRRIHQ-13-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1129_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1129|SYB_fixed_1129' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1129' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-1|IT18IRRIHQ-2-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0393_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0393|SYB_fixed_0393' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0393' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0814_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0814|SYB_fixed_0814' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0814' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0477_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0477|SYB_fixed_0477' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0477' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-3|IT18IRRIHQ-8-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-31' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-2|IT18IRRIHQ-7-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0528_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0528|SYB_fixed_0528' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0528' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-30' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-4|IT18IRRIHQ-6-B-1-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B|IT18IRRIHQ-8-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-2|IT18IRRIHQ-16-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-99' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-3-2|IT18IRRIHQ-20-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-33' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-4-2|IT18IRRIHQ-7-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0194_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0194|SYB_fixed_0194' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0194' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-415' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0194|SYB_fixed_0194' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0194' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1051_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1051|SYB_fixed_1051' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1051' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-89' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-4-1-B|IT18IRRIHQ-10-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0090_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0090|SYB_fixed_0090' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0090' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-311' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0090|SYB_fixed_0090' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0090' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0108_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0108|SYB_fixed_0108' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0108' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-329' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0108|SYB_fixed_0108' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0108' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0373_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0373|SYB_fixed_0373' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0373' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-594' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0373|SYB_fixed_0373' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0373' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-284' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0373|SYB_fixed_0373' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0373' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0672_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0672|SYB_fixed_0672' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0672' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-44' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-3-2|IT18IRRIHQ-9-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0324_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0324|SYB_fixed_0324' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0324' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-545' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0324|SYB_fixed_0324' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0324' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-78' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-2-2|IT18IRRIHQ-16-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0117_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0117|SYB_fixed_0117' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0117' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-338' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0117|SYB_fixed_0117' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0117' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-28' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0117|SYB_fixed_0117' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0117' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0572_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0572|SYB_fixed_0572' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0572' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-177' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-1-1-B|IT18IRRIHQ-19-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0257_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0257|SYB_fixed_0257' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0257' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-478' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0257|SYB_fixed_0257' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0257' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-168' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0257|SYB_fixed_0257' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0257' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0043_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0043|SYB_fixed_0043' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0043' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0693_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0693|SYB_fixed_0693' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0693' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0288_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0288|SYB_fixed_0288' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0288' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-509' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0288|SYB_fixed_0288' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0288' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0966_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0966|SYB_fixed_0966' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0966' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-52' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-1-2|IT18IRRIHQ-11-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0965_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0965|SYB_fixed_0965' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0965' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0946_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0946|SYB_fixed_0946' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0946' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0142_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0142|SYB_fixed_0142' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0142' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-363' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0142|SYB_fixed_0142' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0142' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-125' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-4-2-B|IT18IRRIHQ-13-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-107' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-1-1-B|IT18IRRIHQ-12-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-2|IT18IRRIHQ-20-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-75' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-5-1|IT18IRRIHQ-15-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0903_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0903|SYB_fixed_0903' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0903' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0916_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0916|SYB_fixed_0916' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0916' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0910_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0910|SYB_fixed_0910' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0910' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-2-2|IT18IRRIHQ-4-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0543_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0543|SYB_fixed_0543' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0543' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-166' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-4-2-B|IT18IRRIHQ-17-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-2|IT18IRRIHQ-6-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0352_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0352|SYB_fixed_0352' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0352' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-573' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0352|SYB_fixed_0352' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0352' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-263' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0352|SYB_fixed_0352' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0352' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-5|IT18IRRIHQ-9-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-31' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-3|IT18IRRIHQ-7-B-2-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1158_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1158|SYB_fixed_1158' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1158' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-32' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-2-B|IT18IRRIHQ-4-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0491_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0491|SYB_fixed_0491' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0491' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-3|IT18IRRIHQ-14-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0624_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0624|SYB_fixed_0624' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0624' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0729_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0729|SYB_fixed_0729' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0729' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0791_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0791|SYB_fixed_0791' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0791' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-88' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-2-1|IT18IRRIHQ-18-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0206_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0206|SYB_fixed_0206' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0206' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-427' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0206|SYB_fixed_0206' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0206' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-2/10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10|IT18IRRIHQ-10' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0027_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0027|SYB_fixed_0027' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0027' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-112' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-1-1|IT18IRRIHQ-23-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0448_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0448|SYB_fixed_0448' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0448' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-69' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-2-1|IT18IRRIHQ-14-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0476_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0476|SYB_fixed_0476' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0476' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-95' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-1-B|IT18IRRIHQ-10-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0630_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0630|SYB_fixed_0630' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0630' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0268_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0268|SYB_fixed_0268' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0268' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-489' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0268|SYB_fixed_0268' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0268' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0850_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0850|SYB_fixed_0850' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0850' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0502_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0502|SYB_fixed_0502' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0502' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1088_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1088|SYB_fixed_1088' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1088' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-1|IT18IRRIHQ-15-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0102_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0102|SYB_fixed_0102' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0102' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-323' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0102|SYB_fixed_0102' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0102' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0803_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0803|SYB_fixed_0803' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0803' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-52' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-1-1|IT18IRRIHQ-11-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0594_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0594|SYB_fixed_0594' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0594' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0074_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0074|SYB_fixed_0074' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0074' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-29' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-3-1|IT18IRRIHQ-6-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B|IT18IRRIHQ-16-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-2|IT18IRRIHQ-2-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-5/8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23|IT18IRRIHQ-23' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-210' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-3-1-B|IT18IRRIHQ-23-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0908_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0908|SYB_fixed_0908' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0908' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0562_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0562|SYB_fixed_0562' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0562' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0124_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0124|SYB_fixed_0124' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0124' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-345' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0124|SYB_fixed_0124' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0124' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-35' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0124|SYB_fixed_0124' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0124' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0273_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0273|SYB_fixed_0273' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0273' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-494' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0273|SYB_fixed_0273' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0273' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0015_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0015|SYB_fixed_0015' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0015' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0015|SYB_fixed_0015' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0015' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0368_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0368|SYB_fixed_0368' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0368' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-589' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0368|SYB_fixed_0368' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0368' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0750_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0750|SYB_fixed_0750' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0750' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-162' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-2-2-B|IT18IRRIHQ-17-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0637_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0637|SYB_fixed_0637' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0637' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0685_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0685|SYB_fixed_0685' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0685' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0121_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0121|SYB_fixed_0121' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0121' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-342' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0121|SYB_fixed_0121' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0121' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-32' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0121|SYB_fixed_0121' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0121' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0933_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0933|SYB_fixed_0933' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0933' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1169_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1169|SYB_fixed_1169' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1169' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0822_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0822|SYB_fixed_0822' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0822' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-56' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-2-B|IT18IRRIHQ-6-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1045_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1045|SYB_fixed_1045' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1045' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0947_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0947|SYB_fixed_0947' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0947' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-29' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-1-B|IT18IRRIHQ-4-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0414_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0414|SYB_fixed_0414' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0414' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1135_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1135|SYB_fixed_1135' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1135' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-92' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-1-B|IT18IRRIHQ-10-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1030_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1030|SYB_fixed_1030' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1030' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-1/6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1|IT18IRRIHQ-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0870_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0870|SYB_fixed_0870' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0870' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-1|IT18IRRIHQ-17-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0954_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0954|SYB_fixed_0954' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0954' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0669_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0669|SYB_fixed_0669' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0669' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0796_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0796|SYB_fixed_0796' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0796' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-1|IT18IRRIHQ-13-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-51' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-5-2|IT18IRRIHQ-11-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0220_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0220|SYB_fixed_0220' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0220' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-441' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0220|SYB_fixed_0220' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0220' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0369_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0369|SYB_fixed_0369' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0369' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-590' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0369|SYB_fixed_0369' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0369' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0855_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0855|SYB_fixed_0855' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0855' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0089_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0089|SYB_fixed_0089' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0089' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-65' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-3-1-B|IT18IRRIHQ-7-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1043_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1043|SYB_fixed_1043' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1043' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-2-1-B|IT18IRRIHQ-3-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-4|IT18IRRIHQ-1-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0370_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0370|SYB_fixed_0370' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0370' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-591' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0370|SYB_fixed_0370' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0370' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0569_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0569|SYB_fixed_0569' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0569' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-92' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-5-2|IT18IRRIHQ-19-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0342_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0342|SYB_fixed_0342' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0342' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-563' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0342|SYB_fixed_0342' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0342' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-219' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-1-1-B|IT18IRRIHQ-25-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0799_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0799|SYB_fixed_0799' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0799' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-46' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-4-2-B|IT18IRRIHQ-5-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0106_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0106|SYB_fixed_0106' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0106' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-327' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0106|SYB_fixed_0106' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0106' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0057_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0057|SYB_fixed_0057' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0057' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-2|IT18IRRIHQ-24-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-146' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-5-2-B|IT18IRRIHQ-15-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0784_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0784|SYB_fixed_0784' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0784' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-1|IT18IRRIHQ-2-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B|IT18IRRIHQ-13-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0112_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0112|SYB_fixed_0112' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0112' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-333' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0112|SYB_fixed_0112' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0112' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0112|SYB_fixed_0112' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0112' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0921_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0921|SYB_fixed_0921' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0921' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0230_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0230|SYB_fixed_0230' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0230' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-451' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0230|SYB_fixed_0230' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0230' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-141' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0230|SYB_fixed_0230' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0230' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-40' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-5-1|IT18IRRIHQ-8-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-35' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-3-B|IT18IRRIHQ-4-B-4-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0336_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0336|SYB_fixed_0336' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0336' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-557' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0336|SYB_fixed_0336' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0336' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0846_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0846|SYB_fixed_0846' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0846' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0093_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0093|SYB_fixed_0093' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0093' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-314' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0093|SYB_fixed_0093' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0093' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0918_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0918|SYB_fixed_0918' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0918' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-3|IT18IRRIHQ-5-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0713_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0713|SYB_fixed_0713' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0713' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0459_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0459|SYB_fixed_0459' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0459' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4|IT18IRRIHQ-4-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-91' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-1-1|IT18IRRIHQ-19-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0764_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0764|SYB_fixed_0764' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0764' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0154_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0154|SYB_fixed_0154' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0154' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-375' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0154|SYB_fixed_0154' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0154' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0982_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0982|SYB_fixed_0982' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0982' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0906_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0906|SYB_fixed_0906' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0906' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-195' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-5-1-B|IT18IRRIHQ-20-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0590_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0590|SYB_fixed_0590' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0590' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0441_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0441|SYB_fixed_0441' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0441' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1006|SYB_fixed_1006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0291_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0291|SYB_fixed_0291' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0291' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-512' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0291|SYB_fixed_0291' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0291' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1035_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1035|SYB_fixed_1035' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1035' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0719_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0719|SYB_fixed_0719' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0719' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0493_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0493|SYB_fixed_0493' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0493' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-4|IT18IRRIHQ-8-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1117_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1117|SYB_fixed_1117' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1117' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1063_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1063|SYB_fixed_1063' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1063' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-133' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-2-2-B|IT18IRRIHQ-14-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0679_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0679|SYB_fixed_0679' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0679' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0720_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0720|SYB_fixed_0720' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0720' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-73' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-2-2|IT18IRRIHQ-15-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-3|IT18IRRIHQ-22-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1098_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1098|SYB_fixed_1098' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1098' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0743_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0743|SYB_fixed_0743' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0743' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0955_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0955|SYB_fixed_0955' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0955' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0162_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0162|SYB_fixed_0162' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0162' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-383' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0162|SYB_fixed_0162' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0162' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-5-1-B|IT18IRRIHQ-1-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-1|IT18IRRIHQ-19-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0167_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0167|SYB_fixed_0167' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0167' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-388' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0167|SYB_fixed_0167' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0167' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-78' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0167|SYB_fixed_0167' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0167' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0604_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0604|SYB_fixed_0604' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0604' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B|IT18IRRIHQ-23-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0616_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0616|SYB_fixed_0616' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0616' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0925_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0925|SYB_fixed_0925' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0925' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1151_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1151|SYB_fixed_1151' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1151' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0218_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0218|SYB_fixed_0218' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0218' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-439' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0218|SYB_fixed_0218' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0218' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-123' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-2-1-B|IT18IRRIHQ-13-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0488_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0488|SYB_fixed_0488' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0488' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-204' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-2-1-B|IT18IRRIHQ-22-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0912_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0912|SYB_fixed_0912' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0912' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-2|IT18IRRIHQ-8-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0819_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0819|SYB_fixed_0819' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0819' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0181_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0181|SYB_fixed_0181' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0181' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-402' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0181|SYB_fixed_0181' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0181' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0179_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0179|SYB_fixed_0179' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0179' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-400' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0179|SYB_fixed_0179' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0179' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-90' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0179|SYB_fixed_0179' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0179' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B|IT18IRRIHQ-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0005' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0005' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-40' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-2-2-B|IT18IRRIHQ-4-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0152_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0152|SYB_fixed_0152' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0152' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-373' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0152|SYB_fixed_0152' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0152' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-63' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0152|SYB_fixed_0152' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0152' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0709_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0709|SYB_fixed_0709' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0709' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-4-1|IT18IRRIHQ-5-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0738_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0738|SYB_fixed_0738' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0738' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0997_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0997|SYB_fixed_0997' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0997' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-3|IT18IRRIHQ-4-B-1-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0038_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0038|SYB_fixed_0038' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0038' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0961_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0961|SYB_fixed_0961' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0961' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1056_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1056|SYB_fixed_1056' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1056' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-214' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-3-1-B|IT18IRRIHQ-24-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-1-1|IT18IRRIHQ-2-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-5|IT18IRRIHQ-6-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-80' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-4-1|IT18IRRIHQ-16-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-4|IT18IRRIHQ-25-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0696_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0696|SYB_fixed_0696' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0696' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0931_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0931|SYB_fixed_0931' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0931' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0867_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0867|SYB_fixed_0867' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0867' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-52' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-3-B|IT18IRRIHQ-6-B-4-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0593_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0593|SYB_fixed_0593' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0593' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0779_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0779|SYB_fixed_0779' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0779' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-69' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-1-1-B|IT18IRRIHQ-7-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0812_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0812|SYB_fixed_0812' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0812' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0017_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0017|SYB_fixed_0017' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0017' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0017|SYB_fixed_0017' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0017' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0213_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0213|SYB_fixed_0213' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0213' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-434' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0213|SYB_fixed_0213' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0213' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1021_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1021|SYB_fixed_1021' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1021' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1071_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1071|SYB_fixed_1071' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1071' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0511_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0511|SYB_fixed_0511' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0511' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-1-B|IT18IRRIHQ-2-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0497_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0497|SYB_fixed_0497' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0497' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-3/9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14|IT18IRRIHQ-14' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1033_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1033|SYB_fixed_1033' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1033' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0897_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0897|SYB_fixed_0897' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0897' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0919_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0919|SYB_fixed_0919' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0919' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-31' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-4|IT18IRRIHQ-7-B-2-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0718_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0718|SYB_fixed_0718' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0718' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-3|IT18IRRIHQ-4-B-4-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-71' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-4-2|IT18IRRIHQ-15-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0100_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0100|SYB_fixed_0100' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0100' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-321' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0100|SYB_fixed_0100' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0100' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0831_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0831|SYB_fixed_0831' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0831' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1083_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1083|SYB_fixed_1083' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1083' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0388_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0388|SYB_fixed_0388' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0388' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-128' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-5-2-B|IT18IRRIHQ-14-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0689_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0689|SYB_fixed_0689' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0689' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-1|IT18IRRIHQ-20-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0401_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0401|SYB_fixed_0401' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0401' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-64' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-2-2|IT18IRRIHQ-13-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-167' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-5-1-B|IT18IRRIHQ-18-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0092_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0092|SYB_fixed_0092' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0092' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-313' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0092|SYB_fixed_0092' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0092' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0225_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0225|SYB_fixed_0225' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0225' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-446' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0225|SYB_fixed_0225' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0225' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0813_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0813|SYB_fixed_0813' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0813' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-77' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-1-2|IT18IRRIHQ-16-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0391_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0391|SYB_fixed_0391' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0391' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0524_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0524|SYB_fixed_0524' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0524' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1099_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1099|SYB_fixed_1099' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1099' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0346_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0346|SYB_fixed_0346' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0346' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-567' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0346|SYB_fixed_0346' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0346' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-49' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-2|IT18IRRIHQ-10-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0250_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0250|SYB_fixed_0250' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0250' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-471' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0250|SYB_fixed_0250' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0250' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0141_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0141|SYB_fixed_0141' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0141' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-362' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0141|SYB_fixed_0141' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0141' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-52' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0141|SYB_fixed_0141' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0141' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-2-1|IT18IRRIHQ-4-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-3|IT18IRRIHQ-23-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-34' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-5-1|IT18IRRIHQ-7-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0546_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0546|SYB_fixed_0546' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0546' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-74' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-3-1|IT18IRRIHQ-15-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0650_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0650|SYB_fixed_0650' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0650' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0868_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0868|SYB_fixed_0868' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0868' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0437_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0437|SYB_fixed_0437' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0437' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1183_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1183|SYB_fixed_1183' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1183' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-74' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-3-2|IT18IRRIHQ-15-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0541_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0541|SYB_fixed_0541' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0541' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-178' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-1-2-B|IT18IRRIHQ-19-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-87' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-1-2|IT18IRRIHQ-18-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-127' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-5-1-B|IT18IRRIHQ-14-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-148' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-5-1-B|IT18IRRIHQ-16-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-1|IT18IRRIHQ-1-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-200' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-3-1-B|IT18IRRIHQ-21-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0345_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0345|SYB_fixed_0345' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0345' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-566' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0345|SYB_fixed_0345' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0345' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0177_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0177|SYB_fixed_0177' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0177' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-398' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0177|SYB_fixed_0177' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0177' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1149_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1149|SYB_fixed_1149' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1149' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-192' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-2-2-B|IT18IRRIHQ-20-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0763_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0763|SYB_fixed_0763' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0763' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0968_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0968|SYB_fixed_0968' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0968' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0701_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0701|SYB_fixed_0701' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0701' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0026_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0026|SYB_fixed_0026' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0026' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0031_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0031|SYB_fixed_0031' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0031' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-122' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-1-2-B|IT18IRRIHQ-13-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0702_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0702|SYB_fixed_0702' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0702' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1008_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1008|SYB_fixed_1008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0623_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0623|SYB_fixed_0623' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0623' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0429_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0429|SYB_fixed_0429' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0429' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0377_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0377|SYB_fixed_0377' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0377' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0606_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0606|SYB_fixed_0606' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0606' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0845_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0845|SYB_fixed_0845' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0845' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-144' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-3-1-B|IT18IRRIHQ-15-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0518_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0518|SYB_fixed_0518' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0518' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1081_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1081|SYB_fixed_1081' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1081' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0522_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0522|SYB_fixed_0522' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0522' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0629_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0629|SYB_fixed_0629' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0629' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-5|IT18IRRIHQ-14-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-4|IT18IRRIHQ-24-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1133_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1133|SYB_fixed_1133' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1133' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0128_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0128|SYB_fixed_0128' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0128' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-349' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0128|SYB_fixed_0128' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0128' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0412_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0412|SYB_fixed_0412' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0412' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0874_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0874|SYB_fixed_0874' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0874' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-217' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-2-1-B|IT18IRRIHQ-25-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0440_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0440|SYB_fixed_0440' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0440' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0416_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0416|SYB_fixed_0416' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0416' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0295_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0295|SYB_fixed_0295' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0295' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-516' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0295|SYB_fixed_0295' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0295' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0659_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0659|SYB_fixed_0659' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0659' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0014_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0014|SYB_fixed_0014' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0014' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0014|SYB_fixed_0014' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0014' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1086_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1086|SYB_fixed_1086' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1086' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0849_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0849|SYB_fixed_0849' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0849' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0872_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0872|SYB_fixed_0872' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0872' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-75' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-2-1-B|IT18IRRIHQ-9-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0147_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0147|SYB_fixed_0147' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0147' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-368' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0147|SYB_fixed_0147' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0147' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0909_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0909|SYB_fixed_0909' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0909' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-30' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-4-B|IT18IRRIHQ-4-B-1-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0932_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0932|SYB_fixed_0932' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0932' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0986_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0986|SYB_fixed_0986' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0986' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-4-2-B|IT18IRRIHQ-1-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0184_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0184|SYB_fixed_0184' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0184' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-405' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0184|SYB_fixed_0184' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0184' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0055_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0055|SYB_fixed_0055' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0055' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-80' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-5-1-B|IT18IRRIHQ-9-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1109_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1109|SYB_fixed_1109' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1109' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0560_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0560|SYB_fixed_0560' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0560' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-121' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-2-1|IT18IRRIHQ-25-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1156_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1156|SYB_fixed_1156' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1156' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-175' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-4-1-B|IT18IRRIHQ-18-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0245_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0245|SYB_fixed_0245' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0245' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-466' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0245|SYB_fixed_0245' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0245' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-5|IT18IRRIHQ-24-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0337_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0337|SYB_fixed_0337' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0337' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-558' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0337|SYB_fixed_0337' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0337' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-5-1|IT18IRRIHQ-4-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1009_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1009|SYB_fixed_1009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0534_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0534|SYB_fixed_0534' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0534' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-201' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-4-1-B|IT18IRRIHQ-21-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-147' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-5-2-B|IT18IRRIHQ-16-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0395_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0395|SYB_fixed_0395' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0395' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0433_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0433|SYB_fixed_0433' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0433' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0022_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0022|SYB_fixed_0022' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0022' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0483_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0483|SYB_fixed_0483' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0483' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-129' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-3-2-B|IT18IRRIHQ-14-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0191_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0191|SYB_fixed_0191' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0191' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-412' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0191|SYB_fixed_0191' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0191' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0707_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0707|SYB_fixed_0707' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0707' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0075_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0075|SYB_fixed_0075' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0075' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1032_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1032|SYB_fixed_1032' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1032' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0969_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0969|SYB_fixed_0969' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0969' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0577_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0577|SYB_fixed_0577' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0577' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-72' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-3-1-B|IT18IRRIHQ-8-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1160_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1160|SYB_fixed_1160' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1160' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-2|IT18IRRIHQ-2-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0775_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0775|SYB_fixed_0775' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0775' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-5-1|IT18IRRIHQ-3-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-37' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-1-B|IT18IRRIHQ-4-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-116' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-2-2-B|IT18IRRIHQ-12-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-67' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-4-1-B|IT18IRRIHQ-7-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-174' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-3-2-B|IT18IRRIHQ-18-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1024_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1024|SYB_fixed_1024' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1024' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0498_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0498|SYB_fixed_0498' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0498' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0609_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0609|SYB_fixed_0609' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0609' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0258_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0258|SYB_fixed_0258' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0258' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-479' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0258|SYB_fixed_0258' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0258' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0029_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0029|SYB_fixed_0029' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0029' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1137_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1137|SYB_fixed_1137' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1137' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0953_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0953|SYB_fixed_0953' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0953' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0757_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0757|SYB_fixed_0757' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0757' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0386_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0386|SYB_fixed_0386' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0386' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0500_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0500|SYB_fixed_0500' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0500' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0530_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0530|SYB_fixed_0530' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0530' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-2-2-B|IT18IRRIHQ-3-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0461_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0461|SYB_fixed_0461' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0461' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0776_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0776|SYB_fixed_0776' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0776' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-1-2|IT18IRRIHQ-1-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0974_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0974|SYB_fixed_0974' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0974' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0568_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0568|SYB_fixed_0568' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0568' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-73' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-2-1|IT18IRRIHQ-15-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0928_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0928|SYB_fixed_0928' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0928' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-155' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-4-2-B|IT18IRRIHQ-16-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0732_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0732|SYB_fixed_0732' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0732' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1108_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1108|SYB_fixed_1108' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1108' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-211' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-4-1-B|IT18IRRIHQ-23-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-63' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-1-2|IT18IRRIHQ-13-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0135_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0135|SYB_fixed_0135' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0135' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-356' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0135|SYB_fixed_0135' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0135' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-102' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-1-1|IT18IRRIHQ-21-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1138_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1138|SYB_fixed_1138' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1138' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0133_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0133|SYB_fixed_0133' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0133' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-354' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0133|SYB_fixed_0133' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0133' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0728_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0728|SYB_fixed_0728' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0728' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1176_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1176|SYB_fixed_1176' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1176' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-112' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-4-2-B|IT18IRRIHQ-12-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-64' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-3-2-B|IT18IRRIHQ-7-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0466_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0466|SYB_fixed_0466' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0466' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0064_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0064|SYB_fixed_0064' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0064' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0811_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0811|SYB_fixed_0811' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0811' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0643_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0643|SYB_fixed_0643' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0643' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0088_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0088|SYB_fixed_0088' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0088' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-4|IT18IRRIHQ-21-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0279_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0279|SYB_fixed_0279' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0279' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-500' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0279|SYB_fixed_0279' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0279' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-190' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0279|SYB_fixed_0279' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0279' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1173_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1173|SYB_fixed_1173' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1173' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0261_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0261|SYB_fixed_0261' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0261' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-482' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0261|SYB_fixed_0261' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0261' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-5|IT18IRRIHQ-16-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0792_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0792|SYB_fixed_0792' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0792' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-59' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-4-B|IT18IRRIHQ-6-B-1-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0348_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0348|SYB_fixed_0348' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0348' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-569' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0348|SYB_fixed_0348' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0348' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-4-2|IT18IRRIHQ-4-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1082_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1082|SYB_fixed_1082' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1082' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-209' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-2-1-B|IT18IRRIHQ-23-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1067_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1067|SYB_fixed_1067' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1067' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0881_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0881|SYB_fixed_0881' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0881' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0744_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0744|SYB_fixed_0744' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0744' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0234_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0234|SYB_fixed_0234' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0234' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-455' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0234|SYB_fixed_0234' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0234' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0390_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0390|SYB_fixed_0390' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0390' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-220' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-3-1-B|IT18IRRIHQ-25-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0192_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0192|SYB_fixed_0192' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0192' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-413' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0192|SYB_fixed_0192' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0192' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-76' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-2-2-B|IT18IRRIHQ-9-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-41' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-2-1-B|IT18IRRIHQ-4-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4|IT18IRRIHQ-2-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0229_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0229|SYB_fixed_0229' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0229' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-450' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0229|SYB_fixed_0229' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0229' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0939_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0939|SYB_fixed_0939' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0939' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-44' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-3-1|IT18IRRIHQ-9-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-105' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-4-1|IT18IRRIHQ-21-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0083_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0083|SYB_fixed_0083' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0083' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0871_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0871|SYB_fixed_0871' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0871' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0252_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0252|SYB_fixed_0252' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0252' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-473' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0252|SYB_fixed_0252' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0252' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0484_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0484|SYB_fixed_0484' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0484' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0941_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0941|SYB_fixed_0941' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0941' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0520_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0520|SYB_fixed_0520' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0520' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0341_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0341|SYB_fixed_0341' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0341' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-562' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0341|SYB_fixed_0341' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0341' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-252' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0341|SYB_fixed_0341' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0341' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0710_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0710|SYB_fixed_0710' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0710' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0642_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0642|SYB_fixed_0642' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0642' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0726_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0726|SYB_fixed_0726' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0726' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-104' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-3-1|IT18IRRIHQ-21-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0332_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0332|SYB_fixed_0332' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0332' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-553' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0332|SYB_fixed_0332' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0332' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-86' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-5-2|IT18IRRIHQ-18-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0451_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0451|SYB_fixed_0451' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0451' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0061_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0061|SYB_fixed_0061' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0061' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0641_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0641|SYB_fixed_0641' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0641' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-5|IT18IRRIHQ-7-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1123_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1123|SYB_fixed_1123' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1123' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0923_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0923|SYB_fixed_0923' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0923' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-141' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-2-1-B|IT18IRRIHQ-15-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-59' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-3-1|IT18IRRIHQ-12-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0349_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0349|SYB_fixed_0349' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0349' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-570' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0349|SYB_fixed_0349' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0349' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-260' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0349|SYB_fixed_0349' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0349' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0080_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0080|SYB_fixed_0080' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0080' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1064_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1064|SYB_fixed_1064' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1064' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0662_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0662|SYB_fixed_0662' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0662' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-109' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-5-2-B|IT18IRRIHQ-12-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0235_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0235|SYB_fixed_0235' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0235' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-456' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0235|SYB_fixed_0235' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0235' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-146' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0235|SYB_fixed_0235' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0235' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0633_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0633|SYB_fixed_0633' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0633' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-70' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-4-1|IT18IRRIHQ-14-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0190_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0190|SYB_fixed_0190' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0190' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-411' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0190|SYB_fixed_0190' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0190' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-101' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0190|SYB_fixed_0190' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0190' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0549_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0549|SYB_fixed_0549' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0549' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0704_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0704|SYB_fixed_0704' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0704' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0421_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0421|SYB_fixed_0421' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0421' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0127_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0127|SYB_fixed_0127' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0127' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-348' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0127|SYB_fixed_0127' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0127' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0311_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0311|SYB_fixed_0311' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0311' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-532' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0311|SYB_fixed_0311' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0311' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0267_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0267|SYB_fixed_0267' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0267' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-488' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0267|SYB_fixed_0267' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0267' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-48' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-1|IT18IRRIHQ-10-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0299_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0299|SYB_fixed_0299' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0299' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-520' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0299|SYB_fixed_0299' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0299' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0697_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0697|SYB_fixed_0697' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0697' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0130_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0130|SYB_fixed_0130' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0130' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-351' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0130|SYB_fixed_0130' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0130' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-41' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0130|SYB_fixed_0130' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0130' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0383_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0383|SYB_fixed_0383' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0383' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0589_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0589|SYB_fixed_0589' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0589' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0272_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0272|SYB_fixed_0272' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0272' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-493' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0272|SYB_fixed_0272' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0272' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-183' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0272|SYB_fixed_0272' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0272' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-65' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-4-1|IT18IRRIHQ-13-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-53' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-2-2|IT18IRRIHQ-11-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0627_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0627|SYB_fixed_0627' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0627' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0948_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0948|SYB_fixed_0948' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0948' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1120_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1120|SYB_fixed_1120' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1120' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0532_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0532|SYB_fixed_0532' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0532' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0172_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0172|SYB_fixed_0172' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0172' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-393' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0172|SYB_fixed_0172' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0172' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-50' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-5-1-B|IT18IRRIHQ-6-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-1-1|IT18IRRIHQ-5-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0054_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0054|SYB_fixed_0054' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0054' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0722_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0722|SYB_fixed_0722' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0722' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0834_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0834|SYB_fixed_0834' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0834' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0266_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0266|SYB_fixed_0266' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0266' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-487' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0266|SYB_fixed_0266' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0266' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-177' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0266|SYB_fixed_0266' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0266' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0413_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0413|SYB_fixed_0413' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0413' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-1|IT18IRRIHQ-5-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0891_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0891|SYB_fixed_0891' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0891' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-3/6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11|IT18IRRIHQ-11' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-4|IT18IRRIHQ-23-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1019_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1019|SYB_fixed_1019' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1019' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-2-2|IT18IRRIHQ-5-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0320_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0320|SYB_fixed_0320' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0320' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-541' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0320|SYB_fixed_0320' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0320' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0067_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0067|SYB_fixed_0067' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0067' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0255_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0255|SYB_fixed_0255' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0255' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-476' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0255|SYB_fixed_0255' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0255' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0794_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0794|SYB_fixed_0794' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0794' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0587_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0587|SYB_fixed_0587' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0587' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0321_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0321|SYB_fixed_0321' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0321' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-542' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0321|SYB_fixed_0321' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0321' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0804_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0804|SYB_fixed_0804' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0804' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0786_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0786|SYB_fixed_0786' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0786' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0303_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0303|SYB_fixed_0303' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0303' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-524' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0303|SYB_fixed_0303' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0303' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0840_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0840|SYB_fixed_0840' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0840' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-55' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-3-1-B|IT18IRRIHQ-6-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0164_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0164|SYB_fixed_0164' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0164' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-385' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0164|SYB_fixed_0164' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0164' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-75' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0164|SYB_fixed_0164' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0164' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0372_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0372|SYB_fixed_0372' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0372' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-593' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0372|SYB_fixed_0372' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0372' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1014_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1014|SYB_fixed_1014' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1014' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0907_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0907|SYB_fixed_0907' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0907' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1054_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1054|SYB_fixed_1054' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1054' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0006|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0006_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0006|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0232_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0232|SYB_fixed_0232' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0232' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-453' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0232|SYB_fixed_0232' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0232' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1103_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1103|SYB_fixed_1103' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1103' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0253_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0253|SYB_fixed_0253' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0253' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-474' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0253|SYB_fixed_0253' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0253' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0006' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0008' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0009' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0001' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0001_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0001' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0144_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0144|SYB_fixed_0144' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0144' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-365' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0144|SYB_fixed_0144' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0144' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0479_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0479|SYB_fixed_0479' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0479' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1164_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1164|SYB_fixed_1164' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1164' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0542_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0542|SYB_fixed_0542' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0542' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0827_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0827|SYB_fixed_0827' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0827' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0327_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0327|SYB_fixed_0327' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0327' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-548' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0327|SYB_fixed_0327' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0327' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0113_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0113|SYB_fixed_0113' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0113' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-334' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0113|SYB_fixed_0113' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0113' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0173_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0173|SYB_fixed_0173' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0173' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-394' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0173|SYB_fixed_0173' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0173' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-57' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-5-1|IT18IRRIHQ-12-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0525_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0525|SYB_fixed_0525' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0525' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0886_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0886|SYB_fixed_0886' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0886' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1055_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1055|SYB_fixed_1055' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1055' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-46' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-3|IT18IRRIHQ-10-B-2-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0129_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0129|SYB_fixed_0129' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0129' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-350' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0129|SYB_fixed_0129' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0129' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-40' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0129|SYB_fixed_0129' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0129' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0653_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0653|SYB_fixed_0653' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0653' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-153' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-3-1-B|IT18IRRIHQ-16-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0091_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0091|SYB_fixed_0091' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0091' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-312' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0091|SYB_fixed_0091' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0091' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0091|SYB_fixed_0091' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0091' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1089_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1089|SYB_fixed_1089' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1089' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-4/8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18|IT18IRRIHQ-18' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0964_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0964|SYB_fixed_0964' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0964' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-183' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-3-1-B|IT18IRRIHQ-19-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0645_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0645|SYB_fixed_0645' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0645' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-15' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-3-B|IT18IRRIHQ-2-B-4-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0496_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0496|SYB_fixed_0496' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0496' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0607_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0607|SYB_fixed_0607' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0607' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0734_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0734|SYB_fixed_0734' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0734' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-158' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-5-2-B|IT18IRRIHQ-17-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1046_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1046|SYB_fixed_1046' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1046' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0618_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0618|SYB_fixed_0618' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0618' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1142_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1142|SYB_fixed_1142' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1142' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0598_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0598|SYB_fixed_0598' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0598' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1096_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1096|SYB_fixed_1096' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1096' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-2|IT18IRRIHQ-3-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1050_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1050|SYB_fixed_1050' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1050' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-80' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-4-2|IT18IRRIHQ-16-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0423_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0423|SYB_fixed_0423' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0423' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0196_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0196|SYB_fixed_0196' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0196' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-417' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0196|SYB_fixed_0196' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0196' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0805_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0805|SYB_fixed_0805' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0805' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0319_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0319|SYB_fixed_0319' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0319' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-540' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0319|SYB_fixed_0319' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0319' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-1|IT18IRRIHQ-4-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1057_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1057|SYB_fixed_1057' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1057' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1128_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1128|SYB_fixed_1128' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1128' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1118_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1118|SYB_fixed_1118' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1118' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0537_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0537|SYB_fixed_0537' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0537' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0098_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0098|SYB_fixed_0098' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0098' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-319' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0098|SYB_fixed_0098' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0098' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0418_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0418|SYB_fixed_0418' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0418' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0013_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0013|SYB_fixed_0013' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0013' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0013|SYB_fixed_0013' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0013' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0836_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0836|SYB_fixed_0836' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0836' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0806_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0806|SYB_fixed_0806' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0806' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0564_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0564|SYB_fixed_0564' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0564' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0678_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0678|SYB_fixed_0678' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0678' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1162_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1162|SYB_fixed_1162' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1162' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-181' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-4-1-B|IT18IRRIHQ-19-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0486_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0486|SYB_fixed_0486' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0486' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-45' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-4-1-B|IT18IRRIHQ-5-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0157_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0157|SYB_fixed_0157' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0157' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-378' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0157|SYB_fixed_0157' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0157' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0063_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0063|SYB_fixed_0063' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0063' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0384_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0384|SYB_fixed_0384' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0384' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0410_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0410|SYB_fixed_0410' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0410' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0035_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0035|SYB_fixed_0035' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0035' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0844_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0844|SYB_fixed_0844' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0844' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-4|IT18IRRIHQ-5-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-1|IT18IRRIHQ-14-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2|IT18IRRIHQ-7-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0001|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0002|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0003|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0004|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0005|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0010_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0010|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0010|SYB_fixed_0010' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-28' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-1|IT18IRRIHQ-6-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-5-2|IT18IRRIHQ-4-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0033_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0033|SYB_fixed_0033' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0033' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1148_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1148|SYB_fixed_1148' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1148' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-69' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-2-2|IT18IRRIHQ-14-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0431_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0431|SYB_fixed_0431' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0431' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0503_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0503|SYB_fixed_0503' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0503' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-140' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-1-2-B|IT18IRRIHQ-15-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0611_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0611|SYB_fixed_0611' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0611' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1115_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1115|SYB_fixed_1115' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1115' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0535_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0535|SYB_fixed_0535' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0535' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-2-1|IT18IRRIHQ-2-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0169_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0169|SYB_fixed_0169' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0169' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-390' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0169|SYB_fixed_0169' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0169' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0815_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0815|SYB_fixed_0815' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0815' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-42' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-4-1|IT18IRRIHQ-9-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0105_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0105|SYB_fixed_0105' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0105' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-326' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0105|SYB_fixed_0105' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0105' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0171_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0171|SYB_fixed_0171' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0171' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-392' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0171|SYB_fixed_0171' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0171' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-61' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-4-B|IT18IRRIHQ-7-B-2-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-78' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-4-2-B|IT18IRRIHQ-9-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0330_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0330|SYB_fixed_0330' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0330' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-551' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0330|SYB_fixed_0330' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0330' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-5|IT18IRRIHQ-20-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0862_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0862|SYB_fixed_0862' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0862' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-81' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-3-2-B|IT18IRRIHQ-9-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0269_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0269|SYB_fixed_0269' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0269' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-490' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0269|SYB_fixed_0269' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0269' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-156' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-4-1-B|IT18IRRIHQ-16-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1065_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1065|SYB_fixed_1065' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1065' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-27' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-3-1-B|IT18IRRIHQ-3-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0495_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0495|SYB_fixed_0495' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0495' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0857_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0857|SYB_fixed_0857' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0857' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-3|IT18IRRIHQ-13-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0076_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0076|SYB_fixed_0076' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0076' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-41' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-2-1|IT18IRRIHQ-9-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0981_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0981|SYB_fixed_0981' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0981' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1070_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1070|SYB_fixed_1070' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1070' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0270_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0270|SYB_fixed_0270' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0270' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-491' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0270|SYB_fixed_0270' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0270' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0294_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0294|SYB_fixed_0294' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0294' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-515' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0294|SYB_fixed_0294' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0294' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-205' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0294|SYB_fixed_0294' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0294' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1038_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1038|SYB_fixed_1038' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1038' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1|IT18IRRIHQ-10-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0972_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0972|SYB_fixed_0972' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0972' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0660_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0660|SYB_fixed_0660' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0660' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0526_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0526|SYB_fixed_0526' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0526' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0899_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0899|SYB_fixed_0899' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0899' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-51' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-1-B|IT18IRRIHQ-6-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-79' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-3-1|IT18IRRIHQ-16-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-124' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-2-2-B|IT18IRRIHQ-13-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0671_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0671|SYB_fixed_0671' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0671' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-1-2-B|IT18IRRIHQ-1-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-105' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-4-2-B|IT18IRRIHQ-11-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0438_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0438|SYB_fixed_0438' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0438' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0788_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0788|SYB_fixed_0788' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0788' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-187' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-4-1-B|IT18IRRIHQ-20-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-2/9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9|IT18IRRIHQ-9' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-62' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-2-B|IT18IRRIHQ-7-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0024_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0024|SYB_fixed_0024' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0024' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-3|IT18IRRIHQ-25-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0802_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0802|SYB_fixed_0802' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0802' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0317_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0317|SYB_fixed_0317' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0317' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-538' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0317|SYB_fixed_0317' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0317' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0769_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0769|SYB_fixed_0769' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0769' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-28' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-3-B|IT18IRRIHQ-4-B-1-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B|IT18IRRIHQ-4-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0045_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0045|SYB_fixed_0045' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0045' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0046_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0046|SYB_fixed_0046' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0046' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-169' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-1-1-B|IT18IRRIHQ-18-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0634_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0634|SYB_fixed_0634' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0634' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0119_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0119|SYB_fixed_0119' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0119' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-340' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0119|SYB_fixed_0119' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0119' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1058_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1058|SYB_fixed_1058' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1058' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-5|IT18IRRIHQ-22-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0030_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0030|SYB_fixed_0030' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0030' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0717_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0717|SYB_fixed_0717' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0717' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0894_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0894|SYB_fixed_0894' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0894' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-75' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-5-2|IT18IRRIHQ-15-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0731_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0731|SYB_fixed_0731' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0731' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0134_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0134|SYB_fixed_0134' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0134' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-355' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0134|SYB_fixed_0134' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0134' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-154' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-3-2-B|IT18IRRIHQ-16-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0654_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0654|SYB_fixed_0654' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0654' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0553_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0553|SYB_fixed_0553' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0553' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0853_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0853|SYB_fixed_0853' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0853' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0664_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0664|SYB_fixed_0664' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0664' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-101' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-2-2-B|IT18IRRIHQ-11-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-54' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-3-1|IT18IRRIHQ-11-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1034_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1034|SYB_fixed_1034' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1034' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1153_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1153|SYB_fixed_1153' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1153' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-51' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-5-1|IT18IRRIHQ-11-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0211_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0211|SYB_fixed_0211' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0211' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-432' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0211|SYB_fixed_0211' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0211' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-118' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-3-2-B|IT18IRRIHQ-13-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0175_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0175|SYB_fixed_0175' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0175' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-396' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0175|SYB_fixed_0175' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0175' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0904_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0904|SYB_fixed_0904' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0904' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-108' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-2-1|IT18IRRIHQ-22-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0189_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0189|SYB_fixed_0189' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0189' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-410' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0189|SYB_fixed_0189' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0189' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-108' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-1-2-B|IT18IRRIHQ-12-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0736_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0736|SYB_fixed_0736' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0736' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0879_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0879|SYB_fixed_0879' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0879' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0226_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0226|SYB_fixed_0226' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0226' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-447' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0226|SYB_fixed_0226' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0226' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-5|IT18IRRIHQ-12-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-1-B|IT18IRRIHQ-1-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0188_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0188|SYB_fixed_0188' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0188' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-409' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0188|SYB_fixed_0188' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0188' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0829_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0829|SYB_fixed_0829' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0829' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-184' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-3-2-B|IT18IRRIHQ-19-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0798_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0798|SYB_fixed_0798' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0798' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0951_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0951|SYB_fixed_0951' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0951' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1007_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1007|SYB_fixed_1007' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-2|IT18IRRIHQ-23-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-1/10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5|IT18IRRIHQ-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-53' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-2-B|IT18IRRIHQ-6-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0777_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0777|SYB_fixed_0777' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0777' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0137_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0137|SYB_fixed_0137' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0137' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-358' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0137|SYB_fixed_0137' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0137' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0020_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0020|SYB_fixed_0020' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0020' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0020|SYB_fixed_0020' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0020' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0039_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0039|SYB_fixed_0039' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0039' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1052_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1052|SYB_fixed_1052' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1052' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0097_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0097|SYB_fixed_0097' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0097' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-318' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0097|SYB_fixed_0097' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0097' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0976_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0976|SYB_fixed_0976' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0976' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-114' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-3-1|IT18IRRIHQ-23-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0810_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0810|SYB_fixed_0810' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0810' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0408_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0408|SYB_fixed_0408' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0408' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1036_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1036|SYB_fixed_1036' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1036' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0379_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0379|SYB_fixed_0379' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0379' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-118' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-3-1|IT18IRRIHQ-24-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-33' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-4-1|IT18IRRIHQ-7-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-4-1|IT18IRRIHQ-1-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-100' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-5-2|IT18IRRIHQ-20-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0059_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0059|SYB_fixed_0059' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0059' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0863_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0863|SYB_fixed_0863' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0863' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0149_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0149|SYB_fixed_0149' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0149' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-370' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0149|SYB_fixed_0149' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0149' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-86' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-5-1|IT18IRRIHQ-18-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0620_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0620|SYB_fixed_0620' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0620' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0852_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0852|SYB_fixed_0852' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0852' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-3|IT18IRRIHQ-7-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0958_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0958|SYB_fixed_0958' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0958' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0143_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0143|SYB_fixed_0143' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0143' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-364' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0143|SYB_fixed_0143' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0143' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-54' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0143|SYB_fixed_0143' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0143' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0830_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0830|SYB_fixed_0830' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0830' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-81' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-5-1|IT18IRRIHQ-17-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0521_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0521|SYB_fixed_0521' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0521' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-5|IT18IRRIHQ-8-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0382_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0382|SYB_fixed_0382' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0382' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0913_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0913|SYB_fixed_0913' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0913' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1150_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1150|SYB_fixed_1150' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1150' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0428_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0428|SYB_fixed_0428' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0428' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0900_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0900|SYB_fixed_0900' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0900' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-76' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-5-2|IT18IRRIHQ-16-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0406_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0406|SYB_fixed_0406' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0406' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1175_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1175|SYB_fixed_1175' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1175' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0238_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0238|SYB_fixed_0238' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0238' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-459' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0238|SYB_fixed_0238' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0238' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-218' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-4-1-B|IT18IRRIHQ-25-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-67' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-3-1|IT18IRRIHQ-14-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-110' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-5-1|IT18IRRIHQ-22-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0567_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0567|SYB_fixed_0567' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0567' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0115_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0115|SYB_fixed_0115' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0115' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-336' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0115|SYB_fixed_0115' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0115' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-43' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-5-2|IT18IRRIHQ-9-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0047_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0047|SYB_fixed_0047' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0047' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0864_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0864|SYB_fixed_0864' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0864' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1145_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1145|SYB_fixed_1145' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1145' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0073_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0073|SYB_fixed_0073' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0073' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0613_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0613|SYB_fixed_0613' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0613' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0599_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0599|SYB_fixed_0599' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0599' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0957_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0957|SYB_fixed_0957' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0957' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1121_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1121|SYB_fixed_1121' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1121' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0584_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0584|SYB_fixed_0584' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0584' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0649_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0649|SYB_fixed_0649' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0649' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0960_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0960|SYB_fixed_0960' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0960' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-1-2|IT18IRRIHQ-4-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0962_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0962|SYB_fixed_0962' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0962' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0824_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0824|SYB_fixed_0824' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0824' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1178_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1178|SYB_fixed_1178' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1178' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-2-1-B|IT18IRRIHQ-2-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0548_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0548|SYB_fixed_0548' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0548' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-66' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-5-1|IT18IRRIHQ-14-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0635_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0635|SYB_fixed_0635' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0635' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0371_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0371|SYB_fixed_0371' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0371' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-592' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0371|SYB_fixed_0371' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0371' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-282' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0371|SYB_fixed_0371' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0371' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0335_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0335|SYB_fixed_0335' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0335' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-556' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0335|SYB_fixed_0335' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0335' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-246' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0335|SYB_fixed_0335' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0335' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5|IT18IRRIHQ-10-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1005_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1005|SYB_fixed_1005' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-54' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-3-2-B|IT18IRRIHQ-6-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-196' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-5-2-B|IT18IRRIHQ-20-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1134_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1134|SYB_fixed_1134' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1134' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-98' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-2-1|IT18IRRIHQ-20-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0927_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0927|SYB_fixed_0927' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0927' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0078_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0078|SYB_fixed_0078' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0078' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0745_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0745|SYB_fixed_0745' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0745' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0638_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0638|SYB_fixed_0638' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0638' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-4|IT18IRRIHQ-2-B-4-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-100' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-1-2-B|IT18IRRIHQ-11-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-117' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-3-1-B|IT18IRRIHQ-13-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0835_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0835|SYB_fixed_0835' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0835' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-93' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-1-3-B|IT18IRRIHQ-10-B-1-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-1-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-35' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-1-1|IT18IRRIHQ-7-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-4|IT18IRRIHQ-11-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0851_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0851|SYB_fixed_0851' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0851' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0860_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0860|SYB_fixed_0860' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0860' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0265_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0265|SYB_fixed_0265' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0265' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-486' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0265|SYB_fixed_0265' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0265' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0507_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0507|SYB_fixed_0507' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0507' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-189' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-1-1-B|IT18IRRIHQ-20-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1112_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1112|SYB_fixed_1112' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1112' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0365_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0365|SYB_fixed_0365' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0365' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-586' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0365|SYB_fixed_0365' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0365' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-276' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0365|SYB_fixed_0365' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0365' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-2-1|IT18IRRIHQ-5-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-4|IT18IRRIHQ-10-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0403_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0403|SYB_fixed_0403' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0403' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1116_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1116|SYB_fixed_1116' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1116' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-94' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-3-1|IT18IRRIHQ-19-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1020_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1020|SYB_fixed_1020' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1020' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0111_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0111|SYB_fixed_0111' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0111' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-332' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0111|SYB_fixed_0111' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0111' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0387_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0387|SYB_fixed_0387' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0387' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0138_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0138|SYB_fixed_0138' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0138' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-359' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0138|SYB_fixed_0138' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0138' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0885_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0885|SYB_fixed_0885' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0885' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-76' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-5-1|IT18IRRIHQ-16-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0533_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0533|SYB_fixed_0533' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0533' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0132_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0132|SYB_fixed_0132' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0132' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-353' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0132|SYB_fixed_0132' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0132' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-113' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-2-1|IT18IRRIHQ-23-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0430_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0430|SYB_fixed_0430' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0430' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0595_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0595|SYB_fixed_0595' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0595' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-172' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-2-2-B|IT18IRRIHQ-18-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0233_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0233|SYB_fixed_0233' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0233' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-454' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0233|SYB_fixed_0233' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0233' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0625_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0625|SYB_fixed_0625' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0625' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0282_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0282|SYB_fixed_0282' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0282' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-503' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0282|SYB_fixed_0282' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0282' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0305_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0305|SYB_fixed_0305' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0305' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-526' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0305|SYB_fixed_0305' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0305' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0935_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0935|SYB_fixed_0935' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0935' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-14' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B|IT18IRRIHQ-14-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0995_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0995|SYB_fixed_0995' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0995' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-145' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-5-1-B|IT18IRRIHQ-15-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1077_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1077|SYB_fixed_1077' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1077' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0681_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0681|SYB_fixed_0681' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0681' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-84' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-3-1|IT18IRRIHQ-17-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1172_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1172|SYB_fixed_1172' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1172' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0801_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0801|SYB_fixed_0801' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0801' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-28' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-3|IT18IRRIHQ-6-B-4-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-2|IT18IRRIHQ-13-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0787_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0787|SYB_fixed_0787' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0787' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-3|IT18IRRIHQ-1-B-3-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-152' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-2-2-B|IT18IRRIHQ-16-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0449_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0449|SYB_fixed_0449' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0449' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-197' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-5-1-B|IT18IRRIHQ-21-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0340_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0340|SYB_fixed_0340' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0340' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-561' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0340|SYB_fixed_0340' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0340' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1159_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1159|SYB_fixed_1159' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1159' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0018_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0018|SYB_fixed_0018' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0018' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0018|SYB_fixed_0018' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0018' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0338_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0338|SYB_fixed_0338' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0338' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-559' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0338|SYB_fixed_0338' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0338' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1102_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1102|SYB_fixed_1102' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1102' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0936_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0936|SYB_fixed_0936' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0936' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0201_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0201|SYB_fixed_0201' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0201' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-422' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0201|SYB_fixed_0201' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0201' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-112' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0201|SYB_fixed_0201' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0201' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-4/10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20|IT18IRRIHQ-20' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-37' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-2-1|IT18IRRIHQ-8-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0053_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0053|SYB_fixed_0053' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0053' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-31' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-1|IT18IRRIHQ-7-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0580_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0580|SYB_fixed_0580' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0580' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-3|IT18IRRIHQ-17-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0509_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0509|SYB_fixed_0509' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0509' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-161' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-2-1-B|IT18IRRIHQ-17-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-1|IT18IRRIHQ-1-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0636_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0636|SYB_fixed_0636' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0636' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-1-1|IT18IRRIHQ-3-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0683_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0683|SYB_fixed_0683' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0683' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0605_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0605|SYB_fixed_0605' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0605' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B|IT18IRRIHQ-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B|IT18IRRIHQ-9-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B|IT18IRRIHQ-10-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0023_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0023|SYB_fixed_0023' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0023' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1105_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1105|SYB_fixed_1105' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1105' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0153_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0153|SYB_fixed_0153' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0153' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-374' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0153|SYB_fixed_0153' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0153' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-64' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0153|SYB_fixed_0153' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0153' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-62' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-5-2|IT18IRRIHQ-13-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-85' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-4-1|IT18IRRIHQ-17-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0741_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0741|SYB_fixed_0741' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0741' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0367_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0367|SYB_fixed_0367' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0367' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-588' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0367|SYB_fixed_0367' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0367' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-278' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0367|SYB_fixed_0367' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0367' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-46' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-1|IT18IRRIHQ-10-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-107' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-1-1|IT18IRRIHQ-22-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-151' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-2-1-B|IT18IRRIHQ-16-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0237_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0237|SYB_fixed_0237' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0237' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-458' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0237|SYB_fixed_0237' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0237' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0492_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0492|SYB_fixed_0492' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0492' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0551_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0551|SYB_fixed_0551' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0551' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-111' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-5-1|IT18IRRIHQ-23-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0271_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0271|SYB_fixed_0271' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0271' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-492' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0271|SYB_fixed_0271' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0271' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-97' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-1-2|IT18IRRIHQ-20-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-68' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-5-1-B|IT18IRRIHQ-7-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-160' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-1-2-B|IT18IRRIHQ-17-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0573_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0573|SYB_fixed_0573' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0573' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1107_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1107|SYB_fixed_1107' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1107' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1163_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1163|SYB_fixed_1163' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1163' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0847_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0847|SYB_fixed_0847' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0847' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-5|IT18IRRIHQ-19-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1072_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1072|SYB_fixed_1072' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1072' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B|IT18IRRIHQ-19-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0515_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0515|SYB_fixed_0515' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0515' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1179_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1179|SYB_fixed_1179' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1179' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-26' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-2-1|IT18IRRIHQ-6-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0691_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0691|SYB_fixed_0691' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0691' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0378_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0378|SYB_fixed_0378' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0378' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0610_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0610|SYB_fixed_0610' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0610' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0454_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0454|SYB_fixed_0454' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0454' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-95' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-2-2|IT18IRRIHQ-19-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-5|IT18IRRIHQ-18-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-85' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-4-2|IT18IRRIHQ-17-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0575_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0575|SYB_fixed_0575' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0575' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-4/7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17|IT18IRRIHQ-17' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-96' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-3-1-B|IT18IRRIHQ-10-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0778_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0778|SYB_fixed_0778' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0778' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0655_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0655|SYB_fixed_0655' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0655' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0781_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0781|SYB_fixed_0781' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0781' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-18' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-4|IT18IRRIHQ-18-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0917_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0917|SYB_fixed_0917' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0917' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-135' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-4-1-B|IT18IRRIHQ-14-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0690_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0690|SYB_fixed_0690' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0690' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1154_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1154|SYB_fixed_1154' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1154' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-77' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-1-1|IT18IRRIHQ-16-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0353_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0353|SYB_fixed_0353' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0353' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-574' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0353|SYB_fixed_0353' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0353' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-1|IT18IRRIHQ-7-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1003_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1003|SYB_fixed_1003' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-58' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-1-3-B|IT18IRRIHQ-6-B-1-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-1-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-193' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-3-1-B|IT18IRRIHQ-20-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1130_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1130|SYB_fixed_1130' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1130' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-1|IT18IRRIHQ-21-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0817_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0817|SYB_fixed_0817' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0817' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0612_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0612|SYB_fixed_0612' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0612' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-72' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-1-1|IT18IRRIHQ-15-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1039_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1039|SYB_fixed_1039' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1039' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0442_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0442|SYB_fixed_0442' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0442' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0647_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0647|SYB_fixed_0647' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0647' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1018_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1018|SYB_fixed_1018' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1018' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1136_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1136|SYB_fixed_1136' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1136' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0789_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0789|SYB_fixed_0789' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0789' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0463_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0463|SYB_fixed_0463' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0463' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0825_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0825|SYB_fixed_0825' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0825' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-125' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-5-1|IT18IRRIHQ-25-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0462_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0462|SYB_fixed_0462' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0462' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0971_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0971|SYB_fixed_0971' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0971' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1101_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1101|SYB_fixed_1101' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1101' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0439_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0439|SYB_fixed_0439' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0439' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0084_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0084|SYB_fixed_0084' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0084' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-1|IT18IRRIHQ-24-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0773_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0773|SYB_fixed_0773' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0773' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1026_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1026|SYB_fixed_1026' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1026' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0570_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0570|SYB_fixed_0570' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0570' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0107_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0107|SYB_fixed_0107' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0107' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-328' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0107|SYB_fixed_0107' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0107' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-182' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-4-2-B|IT18IRRIHQ-19-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0298_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0298|SYB_fixed_0298' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0298' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-519' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0298|SYB_fixed_0298' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0298' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0315_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0315|SYB_fixed_0315' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0315' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-536' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0315|SYB_fixed_0315' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0315' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0658_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0658|SYB_fixed_0658' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0658' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0998_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0998|SYB_fixed_0998' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0998' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0715_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0715|SYB_fixed_0715' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0715' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-57' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-5-2|IT18IRRIHQ-12-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0450_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0450|SYB_fixed_0450' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0450' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-5|IT18IRRIHQ-3-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-137' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15-B-4-2-B|IT18IRRIHQ-15-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0136_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0136|SYB_fixed_0136' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0136' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-357' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0136|SYB_fixed_0136' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0136' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-47' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0136|SYB_fixed_0136' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0136' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0485_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0485|SYB_fixed_0485' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0485' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0435_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0435|SYB_fixed_0435' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0435' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-123' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-1-1|IT18IRRIHQ-25-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-109' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-3-1|IT18IRRIHQ-22-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0980_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0980|SYB_fixed_0980' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0980' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-120' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-5-1-B|IT18IRRIHQ-13-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1062_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1062|SYB_fixed_1062' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1062' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B|IT18IRRIHQ-21-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0205_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0205|SYB_fixed_0205' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0205' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-426' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0205|SYB_fixed_0205' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0205' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0420_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0420|SYB_fixed_0420' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0420' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0079_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0079|SYB_fixed_0079' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0079' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0212_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0212|SYB_fixed_0212' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0212' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-433' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0212|SYB_fixed_0212' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0212' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-123' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0212|SYB_fixed_0212' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0212' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-6' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4|IT18IRRIHQ-6-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0363_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0363|SYB_fixed_0363' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0363' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-584' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0363|SYB_fixed_0363' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0363' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-87' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-2-B|IT18IRRIHQ-10-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0652_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0652|SYB_fixed_0652' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0652' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0934_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0934|SYB_fixed_0934' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0934' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0854_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0854|SYB_fixed_0854' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0854' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-131' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-1-1-B|IT18IRRIHQ-14-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0583_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0583|SYB_fixed_0583' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0583' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1170_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1170|SYB_fixed_1170' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1170' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0808_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0808|SYB_fixed_0808' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0808' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0924_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0924|SYB_fixed_0924' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0924' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0760_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0760|SYB_fixed_0760' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0760' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-71' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-2-1-B|IT18IRRIHQ-8-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0104_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0104|SYB_fixed_0104' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0104' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-325' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0104|SYB_fixed_0104' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0104' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0975_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0975|SYB_fixed_0975' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0975' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-63' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-2-1-B|IT18IRRIHQ-7-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0582_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0582|SYB_fixed_0582' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0582' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-4|IT18IRRIHQ-22-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-208' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-1-1-B|IT18IRRIHQ-23-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0490_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0490|SYB_fixed_0490' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0490' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0249_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0249|SYB_fixed_0249' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0249' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-470' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0249|SYB_fixed_0249' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0249' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0480_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0480|SYB_fixed_0480' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0480' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1140_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1140|SYB_fixed_1140' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1140' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0766_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0766|SYB_fixed_0766' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0766' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0516_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0516|SYB_fixed_0516' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0516' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0174_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0174|SYB_fixed_0174' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0174' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-395' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0174|SYB_fixed_0174' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0174' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0241_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0241|SYB_fixed_0241' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0241' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-462' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0241|SYB_fixed_0241' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0241' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1131_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1131|SYB_fixed_1131' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1131' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0297_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0297|SYB_fixed_0297' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0297' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-518' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0297|SYB_fixed_0297' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0297' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0044_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0044|SYB_fixed_0044' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0044' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0392_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0392|SYB_fixed_0392' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0392' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0316_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0316|SYB_fixed_0316' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0316' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-537' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0316|SYB_fixed_0316' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0316' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1059_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1059|SYB_fixed_1059' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1059' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0028_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0028|SYB_fixed_0028' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0028' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0158_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0158|SYB_fixed_0158' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0158' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-379' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0158|SYB_fixed_0158' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0158' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0761_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0761|SYB_fixed_0761' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0761' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0512_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0512|SYB_fixed_0512' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0512' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0848_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0848|SYB_fixed_0848' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0848' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0400_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0400|SYB_fixed_0400' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0400' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-5|IT18IRRIHQ-17-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-5-1|IT18IRRIHQ-5-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0331_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0331|SYB_fixed_0331' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0331' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-552' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0331|SYB_fixed_0331' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0331' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-55' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-4-2|IT18IRRIHQ-11-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-2|IT18IRRIHQ-1-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0343_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0343|SYB_fixed_0343' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0343' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-564' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0343|SYB_fixed_0343' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0343' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0869_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0869|SYB_fixed_0869' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0869' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0754_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0754|SYB_fixed_0754' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0754' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0301_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0301|SYB_fixed_0301' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0301' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-522' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0301|SYB_fixed_0301' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0301' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0884_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0884|SYB_fixed_0884' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0884' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0920_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0920|SYB_fixed_0920' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0920' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0228_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0228|SYB_fixed_0228' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0228' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-449' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0228|SYB_fixed_0228' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0228' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1155_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1155|SYB_fixed_1155' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1155' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0339_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0339|SYB_fixed_0339' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0339' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-560' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0339|SYB_fixed_0339' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0339' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0096_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0096|SYB_fixed_0096' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0096' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-317' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0096|SYB_fixed_0096' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0096' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0626_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0626|SYB_fixed_0626' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0626' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0417_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0417|SYB_fixed_0417' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0417' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0214_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0214|SYB_fixed_0214' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0214' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-435' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0214|SYB_fixed_0214' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0214' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0989_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0989|SYB_fixed_0989' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0989' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-2-B|IT18IRRIHQ-1-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0656_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0656|SYB_fixed_0656' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0656' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0661_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0661|SYB_fixed_0661' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0661' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-50' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-3-1|IT18IRRIHQ-10-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1048_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1048|SYB_fixed_1048' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1048' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-1/7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2|IT18IRRIHQ-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-74' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-5-1-B|IT18IRRIHQ-8-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0198_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0198|SYB_fixed_0198' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0198' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-419' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0198|SYB_fixed_0198' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0198' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0640_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0640|SYB_fixed_0640' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0640' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-4-3|IT18IRRIHQ-2-B-4-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-4-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0724_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0724|SYB_fixed_0724' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0724' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-1|IT18IRRIHQ-16-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-19' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-2|IT18IRRIHQ-19-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0579_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0579|SYB_fixed_0579' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0579' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-82' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-1-1|IT18IRRIHQ-17-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0077_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0077|SYB_fixed_0077' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0077' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-186' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-2-2-B|IT18IRRIHQ-19-B-2-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-2-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1097_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1097|SYB_fixed_1097' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1097' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-70' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-8-B-1-1-B|IT18IRRIHQ-8-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-8-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0531_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0531|SYB_fixed_0531' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0531' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-3/10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-15|IT18IRRIHQ-15' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-15' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0752_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0752|SYB_fixed_0752' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0752' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-45' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-1-1|IT18IRRIHQ-9-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0036_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0036|SYB_fixed_0036' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0036' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0012_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0012|SYB_fixed_0012' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0012' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-SEM-2018-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0012|SYB_fixed_0012' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0012' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0992_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0992|SYB_fixed_0992' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0992' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0283_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0283|SYB_fixed_0283' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0283' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-504' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0283|SYB_fixed_0283' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0283' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0151_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0151|SYB_fixed_0151' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0151' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-372' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0151|SYB_fixed_0151' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0151' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-32' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-7-B-3-1|IT18IRRIHQ-7-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-7-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-1-1-B|IT18IRRIHQ-1-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1085_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1085|SYB_fixed_1085' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1085' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0539_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0539|SYB_fixed_0539' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0539' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0470_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0470|SYB_fixed_0470' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0470' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0048_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0048|SYB_fixed_0048' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0048' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-103' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-2-1|IT18IRRIHQ-21-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0504_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0504|SYB_fixed_0504' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0504' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-103' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-3-1-B|IT18IRRIHQ-11-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0146_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0146|SYB_fixed_0146' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0146' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-367' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0146|SYB_fixed_0146' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0146' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-57' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0146|SYB_fixed_0146' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0146' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0244_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0244|SYB_fixed_0244' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0244' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-465' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0244|SYB_fixed_0244' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0244' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1029_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1029|SYB_fixed_1029' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1029' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0632_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0632|SYB_fixed_0632' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0632' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0285_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0285|SYB_fixed_0285' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0285' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-506' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0285|SYB_fixed_0285' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0285' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-163' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-3-2-B|IT18IRRIHQ-17-B-3-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-3-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0861_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0861|SYB_fixed_0861' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0861' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-126' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-4-1-B|IT18IRRIHQ-13-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1182_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1182|SYB_fixed_1182' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1182' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-48' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-5-2|IT18IRRIHQ-10-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0772_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0772|SYB_fixed_0772' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0772' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0665_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0665|SYB_fixed_0665' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0665' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0809_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0809|SYB_fixed_0809' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0809' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1144_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1144|SYB_fixed_1144' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1144' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0286_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0286|SYB_fixed_0286' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0286' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-507' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0286|SYB_fixed_0286' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0286' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-202' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-4-1-B|IT18IRRIHQ-22-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-20' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-4|IT18IRRIHQ-20-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0051_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0051|SYB_fixed_0051' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0051' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0828_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0828|SYB_fixed_0828' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0828' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0843_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0843|SYB_fixed_0843' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0843' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-83' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-2-1|IT18IRRIHQ-17-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0094_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0094|SYB_fixed_0094' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0094' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-315' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0094|SYB_fixed_0094' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0094' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-164' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-3-1-B|IT18IRRIHQ-17-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0447_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0447|SYB_fixed_0447' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0447' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-59' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-3-2|IT18IRRIHQ-12-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0042_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0042|SYB_fixed_0042' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0042' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-82' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-17-B-1-2|IT18IRRIHQ-17-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-17-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0277_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0277|SYB_fixed_0277' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0277' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-498' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0277|SYB_fixed_0277' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0277' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-205' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-3-1-B|IT18IRRIHQ-22-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0197_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0197|SYB_fixed_0197' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0197' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-418' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0197|SYB_fixed_0197' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0197' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0878_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0878|SYB_fixed_0878' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0878' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1122_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1122|SYB_fixed_1122' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1122' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-83' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-1-1-B|IT18IRRIHQ-9-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0195_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0195|SYB_fixed_0195' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0195' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-416' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0195|SYB_fixed_0195' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0195' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0561_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0561|SYB_fixed_0561' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0561' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0041_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0041|SYB_fixed_0041' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0041' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-44' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B-3-1-B|IT18IRRIHQ-5-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0165_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0165|SYB_fixed_0165' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0165' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-386' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0165|SYB_fixed_0165' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0165' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-76' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0165|SYB_fixed_0165' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0165' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0407_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0407|SYB_fixed_0407' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0407' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1177_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1177|SYB_fixed_1177' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1177' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-102' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-2-1-B|IT18IRRIHQ-11-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0478_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0478|SYB_fixed_0478' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0478' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0529_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0529|SYB_fixed_0529' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0529' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-5|IT18IRRIHQ-23-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0574_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0574|SYB_fixed_0574' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0574' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-1/8' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3|IT18IRRIHQ-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-2-1-B|IT18IRRIHQ-1-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0289_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0289|SYB_fixed_0289' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0289' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-510' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0289|SYB_fixed_0289' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0289' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-2|IT18IRRIHQ-9-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1124_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1124|SYB_fixed_1124' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1124' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0816_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0816|SYB_fixed_0816' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0816' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1044_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1044|SYB_fixed_1044' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1044' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-13' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-13-B-5|IT18IRRIHQ-13-B-5' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-13-B-5' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0310_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0310|SYB_fixed_0310' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0310' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-531' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0310|SYB_fixed_0310' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0310' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0473_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0473|SYB_fixed_0473' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0473' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1126_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1126|SYB_fixed_1126' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1126' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0103_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0103|SYB_fixed_0103' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0103' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-324' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0103|SYB_fixed_0103' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0103' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0284_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0284|SYB_fixed_0284' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0284' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-505' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0284|SYB_fixed_0284' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0284' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0838_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0838|SYB_fixed_0838' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0838' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0733_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0733|SYB_fixed_0733' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0733' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-1' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-3-3-B|IT18IRRIHQ-1-B-3-3-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-3-3-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-16' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-3|IT18IRRIHQ-16-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0056_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0056|SYB_fixed_0056' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0056' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-90' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-4-2|IT18IRRIHQ-18-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-56' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-1-1|IT18IRRIHQ-12-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0419_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0419|SYB_fixed_0419' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0419' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-5/9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24|IT18IRRIHQ-24' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-46' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-2-2|IT18IRRIHQ-10-B-2-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-2-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1127_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1127|SYB_fixed_1127' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1127' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1157_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1157|SYB_fixed_1157' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1157' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-22' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22-B-1|IT18IRRIHQ-22-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-17' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3-1|IT18IRRIHQ-4-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0684_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0684|SYB_fixed_0684' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0684' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-70' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-4-2|IT18IRRIHQ-14-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-5-2|IT18IRRIHQ-1-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0376_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0376|SYB_fixed_0376' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0376' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0615_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0615|SYB_fixed_0615' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0615' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0876_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0876|SYB_fixed_0876' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0876' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0513_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0513|SYB_fixed_0513' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0513' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0770_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0770|SYB_fixed_0770' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0770' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0751_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0751|SYB_fixed_0751' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0751' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-120' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-5-1|IT18IRRIHQ-24-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0452_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0452|SYB_fixed_0452' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0452' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-171' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-2-1-B|IT18IRRIHQ-18-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-110' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-5-1-B|IT18IRRIHQ-12-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1100_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1100|SYB_fixed_1100' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1100' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-27' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-5-1|IT18IRRIHQ-6-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-2' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-1-1|IT18IRRIHQ-1-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0366_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0366|SYB_fixed_0366' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0366' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-587' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0366|SYB_fixed_0366' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0366' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-91' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-1-2|IT18IRRIHQ-19-B-1-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-1-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0735_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0735|SYB_fixed_0735' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0735' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0698_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0698|SYB_fixed_0698' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0698' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0990_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0990|SYB_fixed_0990' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0990' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-111' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-4-1-B|IT18IRRIHQ-12-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-179' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-19-B-5-1-B|IT18IRRIHQ-19-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-19-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-3' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B-5-1|IT18IRRIHQ-1-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-1-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-3|IT18IRRIHQ-24-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0785_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0785|SYB_fixed_0785' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0785' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0280_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0280|SYB_fixed_0280' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0280' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-501' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0280|SYB_fixed_0280' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0280' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0156_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0156|SYB_fixed_0156' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0156' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-377' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0156|SYB_fixed_0156' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0156' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1104_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1104|SYB_fixed_1104' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1104' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-78' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-2-1|IT18IRRIHQ-16-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-5/7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-22|IT18IRRIHQ-22' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-22' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0081_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0081|SYB_fixed_0081' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0081' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0239_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0239|SYB_fixed_0239' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0239' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-460' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0239|SYB_fixed_0239' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0239' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0930_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0930|SYB_fixed_0930' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0930' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0692_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0692|SYB_fixed_0692' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0692' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-5-2|IT18IRRIHQ-2-B-5-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-5-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0945_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0945|SYB_fixed_0945' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0945' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-198' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-1-1-B|IT18IRRIHQ-21-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0901_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0901|SYB_fixed_0901' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0901' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1181_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1181|SYB_fixed_1181' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1181' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0893_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0893|SYB_fixed_0893' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0893' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1049_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1049|SYB_fixed_1049' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1049' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0887_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0887|SYB_fixed_0887' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0887' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0049_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0049|SYB_fixed_0049' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0049' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0800_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0800|SYB_fixed_0800' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0800' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0494_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0494|SYB_fixed_0494' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0494' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-5' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-5-B|IT18IRRIHQ-5-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-5-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1111_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1111|SYB_fixed_1111' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1111' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0973_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0973|SYB_fixed_0973' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0973' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0566_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0566|SYB_fixed_0566' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0566' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-11' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-1|IT18IRRIHQ-11-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-4' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-4-B-3|IT18IRRIHQ-4-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-4-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-106' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-4-1-B|IT18IRRIHQ-11-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-98' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-11-B-5-1-B|IT18IRRIHQ-11-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-11-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-101' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-5-1|IT18IRRIHQ-21-B-5-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-5-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-24' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-1-1-B|IT18IRRIHQ-3-B-1-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-1-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0436_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0436|SYB_fixed_0436' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0436' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0544_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0544|SYB_fixed_0544' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0544' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0508_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0508|SYB_fixed_0508' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0508' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1012_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1012|SYB_fixed_1012' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1012' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0646_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0646|SYB_fixed_0646' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0646' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0667_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0667|SYB_fixed_0667' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0667' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1073_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1073|SYB_fixed_1073' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1073' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1002_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1002|SYB_fixed_1002' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0347_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0347|SYB_fixed_0347' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0347' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-568' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0347|SYB_fixed_0347' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0347' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-23' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-1|IT18IRRIHQ-23-B-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0209_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0209|SYB_fixed_0209' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0209' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-430' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0209|SYB_fixed_0209' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0209' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18DS-SYB-HB-2018-DS-001-3/7' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12|IT18IRRIHQ-12' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0545_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0545|SYB_fixed_0545' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0545' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0082_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0082|SYB_fixed_0082' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0082' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0622_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0622|SYB_fixed_0622' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0622' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0292_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0292|SYB_fixed_0292' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0292' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-513' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0292|SYB_fixed_0292' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0292' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0952_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0952|SYB_fixed_0952' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0952' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0254_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0254|SYB_fixed_0254' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0254' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-475' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0254|SYB_fixed_0254' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0254' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-176' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-4-2-B|IT18IRRIHQ-18-B-4-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-4-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0323_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0323|SYB_fixed_0323' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0323' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-544' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0323|SYB_fixed_0323' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0323' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-96' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-20-B-4-1|IT18IRRIHQ-20-B-4-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-20-B-4-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0185_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0185|SYB_fixed_0185' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0185' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-406' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0185|SYB_fixed_0185' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0185' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-96' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0185|SYB_fixed_0185' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0185' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0398_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0398|SYB_fixed_0398' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0398' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-67' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-14-B-3-2|IT18IRRIHQ-14-B-3-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-14-B-3-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1023_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1023|SYB_fixed_1023' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1023' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0263_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0263|SYB_fixed_0263' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0263' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-484' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0263|SYB_fixed_0263' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0263' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0308_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0308|SYB_fixed_0308' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0308' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-529' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0308|SYB_fixed_0308' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0308' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0422_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0422|SYB_fixed_0422' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0422' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0858_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0858|SYB_fixed_0858' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0858' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0458_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0458|SYB_fixed_0458' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0458' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-28' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-6-B-4-2|IT18IRRIHQ-6-B-4-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-6-B-4-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0747_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0747|SYB_fixed_0747' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0747' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-21' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-2|IT18IRRIHQ-21-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-4|IT18IRRIHQ-9-B-4' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-4' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0227_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0227|SYB_fixed_0227' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0227' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-448' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0227|SYB_fixed_0227' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0227' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0434_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0434|SYB_fixed_0434' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0434' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0686_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0686|SYB_fixed_0686' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0686' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-89' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-3-1|IT18IRRIHQ-18-B-3-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-3-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-116' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-24-B-1-1|IT18IRRIHQ-24-B-1-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-24-B-1-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0911_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0911|SYB_fixed_0911' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0911' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0675_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0675|SYB_fixed_0675' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0675' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0771_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0771|SYB_fixed_0771' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0771' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0475_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0475|SYB_fixed_0475' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0475' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0571_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0571|SYB_fixed_0571' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0571' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1068_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1068|SYB_fixed_1068' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1068' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1184_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1184|SYB_fixed_1184' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1184' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B|IT18IRRIHQ-9-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-1-B|IT18IRRIHQ-9-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ18WS-SYB-F1-2018-WS-001-9' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B|IT18IRRIHQ-9-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0302_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0302|SYB_fixed_0302' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0302' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-523' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0302|SYB_fixed_0302' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0302' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-207' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-23-B-5-1-B|IT18IRRIHQ-23-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-23-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0905_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0905|SYB_fixed_0905' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0905' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-199' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-21-B-2-1-B|IT18IRRIHQ-21-B-2-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-21-B-2-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0256_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0256|SYB_fixed_0256' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0256' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-477' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0256|SYB_fixed_0256' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0256' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0723_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0723|SYB_fixed_0723' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0723' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0290_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0290|SYB_fixed_0290' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0290' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-511' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0290|SYB_fixed_0290' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0290' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-201' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0290|SYB_fixed_0290' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0290' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-77' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-9-B-4-1-B|IT18IRRIHQ-9-B-4-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-9-B-4-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-12' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-2-B-3-1-B|IT18IRRIHQ-2-B-3-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-2-B-3-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1113_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1113|SYB_fixed_1113' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1113' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0739_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0739|SYB_fixed_0739' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0739' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0977_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0977|SYB_fixed_0977' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0977' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-168' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-18-B-5-2-B|IT18IRRIHQ-18-B-5-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-18-B-5-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-25' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-25-B-2|IT18IRRIHQ-25-B-2' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-25-B-2' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0126_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0126|SYB_fixed_0126' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0126' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-347' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0126|SYB_fixed_0126' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0126' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0207_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0207|SYB_fixed_0207' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0207' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-428' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0207|SYB_fixed_0207' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0207' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0474_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0474|SYB_fixed_0474' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0474' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1161_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1161|SYB_fixed_1161' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1161' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0875_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0875|SYB_fixed_0875' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0875' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19WS-SYB-F3-2019-WS-001-60' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-12-B-2-1|IT18IRRIHQ-12-B-2-1' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-12-B-2-1' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1143_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1143|SYB_fixed_1143' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1143' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-26' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-3-B-5-1-B|IT18IRRIHQ-3-B-5-1-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-3-B-5-1-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0942_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0942|SYB_fixed_0942' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0942' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ19DS-SYB-F2-2019-DS-001-10' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-10-B-3|IT18IRRIHQ-10-B-3' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-10-B-3' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0248_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0248|SYB_fixed_0248' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0248' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-469' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0248|SYB_fixed_0248' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0248' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ22DS-SYB-AYT-2022-DS-001-001-159' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0248|SYB_fixed_0248' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0248' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0334_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0334|SYB_fixed_0334' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0334' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ21DS-SYB-OYT-2021-DS-001-001-555' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0334|SYB_fixed_0334' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0334' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_1185_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_1185|SYB_fixed_1185' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_1185' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='IRRIHQ20DS-SYB-F4-2020-DS-001-149' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='IT18IRRIHQ-16-B-1-2-B|IT18IRRIHQ-16-B-1-2-B' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='IT18IRRIHQ-16-B-1-2-B' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 

UPDATE germplasm.cross_parent
SET
seed_id = (SELECT id FROM germplasm.seed WHERE seed_name ='SYB_fixed_0453_SEED-001' LIMIT 1)
WHERE
cross_id IN (SELECT id FROM germplasm.cross WHERE cross_name ='SYB_fixed_0453|SYB_fixed_0453' LIMIT 1) AND 
germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0453' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1); 



--rollback SELECT NULL;