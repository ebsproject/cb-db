--liquibase formatted sql

--changeset postgres:update_global_post_harvest_rows_3_add_focus_config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3725 CB-DC Post harvest: Update Global Post Harvest Rows 3 Config



UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Default configuration for the third row in the post-harvest data collection page",
        "Values": [
            {
                "default": "",
                "disabled": false,
                "field_focus": true,
                "target_value": "AYLD_CONT",
                "target_column": "value",
                "variable_type": "observation",
                "variable_abbrev": "AYLD_CONT",
                "entity_data_type": "trait-data"
            },
            {
                "default": "",
                "disabled": false,
                "target_value": "REMARKS",
                "target_column": "REMARKS",
                "variable_type": "metadata",
                "variable_abbrev": "REMARKS",
                "entity_data_type": "plot-data"
            }
        ]
    }$$
WHERE
    abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback          "Name": "Default configuration for the third row in the post-harvest data collection page",
--rollback          "Values": [
--rollback              {
--rollback                  "default": "",
--rollback                  "disabled": false,
--rollback                  "target_value": "REMARKS",
--rollback                   "target_column": "REMARKS",
--rollback                   "variable_type": "metadata",
--rollback                   "variable_abbrev": "REMARKS",
--rollback                  "entity_data_type": "plot-data"
--rollback                }
--rollback          ]
--rollback      }$$
--rollback WHERE 
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS';