--liquibase formatted sql

--changeset postgres:update_master.formula_plot_area_dsr_sqm_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3769 CB-DB-Fixture: Remove excess function parameter for PLOT_AREA_DSR_SQM formula



DROP FUNCTION IF EXISTS master.formula_plot_area_dsr_sqm(double precision, double precision, double precision, double precision);

-- FUNCTION: master.formula_plot_area_dsr_sqm(double precision, double precision, integer)

CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
	dist_bet_rows double precision,
	row_length_cont double precision,
	rows_per_plot_cont integer)
    RETURNS double precision
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
	DECLARE
		plot_area_dsr_sqm float;
	BEGIN

		plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;

		RETURN round(plot_area_dsr_sqm::numeric, 3);
	END

$BODY$;



--rollback DROP FUNCTION IF EXISTS master.formula_plot_area_dsr_sqm(double precision, double precision, integer);
--rollback
--rollback CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
--rollback 	plot_area_dsr_sqm double precision,
--rollback 	dist_bet_rows double precision,
--rollback 	row_length_cont double precision,
--rollback 	rows_per_plot_cont double precision)
--rollback     RETURNS double precision
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE PARALLEL UNSAFE
--rollback AS $BODY$
--rollback 	DECLARE
--rollback 		plot_area_dsr_sqm float;
--rollback 		local_dist_bet_rows float;
--rollback 		local_length_cont float;
--rollback 		local_rows_per_plot_cont float;
--rollback 	BEGIN
--rollback 
--rollback 		plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;
--rollback 
--rollback 		RETURN round(plot_area_dsr_sqm::numeric, 3);
--rollback 	END
--rollback 
--rollback $BODY$;
