--liquibase formatted sql

--changeset postgres:update_hm_harvest_data_requirements_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3717 CB-HM: Update HARVEST DATA CONFIGURATION - include all known use cases for RICE, WHEAT, MAIZE, COWPEA, and SOYBEAN 



UPDATE 
    platform.config
SET
    config_value = $$
        [
            {
                "cropCode": "*",
                "state": "unknown",
                "config": {
                    "harvestData": [ ],
                    "harvestMethods": { }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "selfing",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "selfing",
                "state": "fixed",
                "type": "fixed_line",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT_SEED_INCREASE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            } 
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "selfing",
                "state": "*",
                "type": [ 
                    "genome_edited", "transgenic" 
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "selfing",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_SEED_DESCENT": [],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION_AND_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "PANICLE_SELECTION": [
                            {
                                "variableAbbrev": "PANNO_SEL",
                                "apiFieldName": "noOfPanicle",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "PLANT_SPECIFIC": [
                            {
                                "variableAbbrev": "SPECIFIC_PLANT",
                                "apiFieldName": "specificPlantNo",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "comma_sep_int"
                                }
                            }
                        ],
                        "PLANT_SPECIFIC_AND_BULK": [
                            {
                                "variableAbbrev": "SPECIFIC_PLANT",
                                "apiFieldName": "specificPlantNo",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "comma_sep_int"
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "stageCode": "TCV",
                "crossMethod": "selfing",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "R_LINE_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "stageCode": "TCV",
                "crossMethod": "selfing",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": { }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": [
                    "single cross", "backcross"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "HEAD_SINGLE_SEED_NUMBERING": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": [
                    "double cross", "three-way cross",
                    "complex cross"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": [
                    "transgenesis", "genome editing"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "HEAD_SINGLE_SEED_NUMBERING": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "hybrid formation",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "H_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "CMS multiplication",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "A_LINE_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethod": "test cross",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "TEST_CROSS_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethod": "selfing",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SELECTED_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "INDIVIDUAL_SPIKE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethod": "selfing",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "GRAIN_COLOR",
                            "apiFieldName": "grainColor",
                            "required": false,
                            "inputType": "select2",
                            "inputOptions": {
                                "useScaleValues": true
                            }
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SELECTED_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "INDIVIDUAL_SPIKE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethod": [
                    "single cross", "double cross", "top cross", "backcross", "hybrid formation"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethod": "selfing",
                "state": [ 
                    "fixed",
                    "not_fixed"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethod": "selfing",
                "state": "fixed",
                "type": "haploid",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "DH1_INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethod": "selfing",
                "state": "not_fixed",
                "type": [
                    "landrace", "composite", "synthetic", "population"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "MAINTAIN_AND_BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethod": [
                    "single cross", "double cross", "three-way cross", 
                    "backcross", "hybrid formation", "maternal haploid induction"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethod": "selfing",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethod": "selfing",
                "state": "not_fixed",
                "generation": [
                    "contains F1", "contains F4", "contains F5", "contains F6"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethod": "selfing",
                "state": "not_fixed",
                "generation": [
                    "contains F2", "contains F3"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethod": [
                    "bi-parental cross", "backcross"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            }
        ]
    $$
WHERE
    abbrev = 'HM_HARVEST_DATA_REQUIREMENTS_CONFIG'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback         [
--rollback             {
--rollback                 "cropCode": "*",
--rollback                 "state": "unknown",
--rollback                 "config": {
--rollback                     "harvestData": [ ],
--rollback                     "harvestMethods": { }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "type": "fixed_line",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_PLANT_SEED_INCREASE": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "*",
--rollback                 "type": [ 
--rollback                     "genome_edited", "transgenic" 
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_SEED_DESCENT": [],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "SINGLE_PLANT_SELECTION_AND_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PANICLE_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "PANNO_SEL",
--rollback                                 "apiFieldName": "noOfPanicle",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PLANT_SPECIFIC": [
--rollback                             {
--rollback                                 "variableAbbrev": "SPECIFIC_PLANT",
--rollback                                 "apiFieldName": "specificPlantNo",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "comma_sep_int"
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PLANT_SPECIFIC_AND_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "SPECIFIC_PLANT",
--rollback                                 "apiFieldName": "specificPlantNo",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "comma_sep_int"
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "stageCode": "TCV",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "R_LINE_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "stageCode": "TCV",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": { }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "single cross", "backcross"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "HEAD_SINGLE_SEED_NUMBERING": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "double cross", "three-way cross",
--rollback                     "complex cross"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "transgenesis", "genome editing"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "HEAD_SINGLE_SEED_NUMBERING": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "hybrid formation",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "H_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "CMS multiplication",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "A_LINE_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "test cross",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "TEST_CROSS_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethod": [
--rollback                     "single cross", "double cross", "top cross", "backcross", "hybrid formation"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 } 
--rollback             }
--rollback         ]
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'HM_HARVEST_DATA_REQUIREMENTS_CONFIG'
--rollback ;