--liquibase formatted sql

--changeset postgres:update_bt_experiment_entry_list_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3635 CB-EC: Entry list > Add spatial check option for entry role values



UPDATE 
    platform.config
SET
    config_value = $${
        "Name": "Required and default entry level metadata variables for Breeding Trial data process",
        "Values": [
            {
                "default": "test",
                "disabled": false,
                "required": "required",
                "target_value": "",
                "target_column": "",
                "variable_type": "identification",
                "allowed_values": ["ENTRY_TYPE_CHECK", "ENTRY_TYPE_TEST"],
                "variable_abbrev": "ENTRY_TYPE",
                "api_resource_sort": "",
                "api_resource_filter": "",
                "api_resource_method": "",
                "api_resource_endpoint": "entries",
                "secondary_target_column": ""
            },
            {
                "disabled": false,
                "target_value": "",
                "target_column": "",
                "variable_type": "identification",
                "variable_abbrev": "ENTRY_CLASS",
                "api_resource_sort": "",
                "api_resource_filter": "",
                "api_resource_method": "",
                "api_resource_endpoint": "entries",
                "secondary_target_column": ""
            },
            {
                "disabled": false,
                "target_value": "",
                "target_column": "",
                "variable_type": "identification",
                "allowed_values": [
                    "ENTRY_ROLE_PERFORMANCE_CHECK",
                    "ENTRY_ROLE_REPEATED_CHECK"   ,
                    "ENTRY_ROLE_LOCAL_CHECK",
                    "ENTRY_ROLE_SPATIAL_CHECK"
                ],
                "variable_abbrev": "ENTRY_ROLE",
                "api_resource_sort": "",
                "api_resource_filter": "",
                "api_resource_method": "",
                "api_resource_endpoint": "entries",
                "secondary_target_column": ""
            },
            {
                "disabled": false,
                "target_value": "",
                "target_column": "",
                "variable_type": "identification",
                "variable_abbrev": "DESCRIPTION",
                "api_resource_sort": "",
                "api_resource_filter": "",
                "api_resource_method": "",
                "api_resource_endpoint": "entries",
                "secondary_target_column": ""
            }
        ]
    }$$
WHERE
    abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Required and default entry level metadata variables for Breeding Trial data process",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "test",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "",
--rollback                 "target_column": "",
--rollback                 "variable_type": "identification",
--rollback                 "allowed_values": ["ENTRY_TYPE_CHECK", "ENTRY_TYPE_TEST"],
--rollback                 "variable_abbrev": "ENTRY_TYPE",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_filter": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "entries",
--rollback                 "secondary_target_column": ""
--rollback             },
--rollback             {
--rollback                 "disabled": false,
--rollback                 "target_value": "",
--rollback                 "target_column": "",
--rollback                 "variable_type": "identification",
--rollback                 "variable_abbrev": "ENTRY_CLASS",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_filter": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "entries",
--rollback                 "secondary_target_column": ""
--rollback             },
--rollback             {
--rollback                 "disabled": false,
--rollback                 "target_value": "",
--rollback                 "target_column": "",
--rollback                 "variable_type": "identification",
--rollback                 "allowed_values": [
--rollback                     "ENTRY_ROLE_PERFORMANCE_CHECK",
--rollback                     "ENTRY_ROLE_REPEATED_CHECK"   ,
--rollback                     "ENTRY_ROLE_LOCAL_CHECK"
--rollback                 ],
--rollback                 "variable_abbrev": "ENTRY_ROLE",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_filter": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "entries",
--rollback                 "secondary_target_column": ""
--rollback             },
--rollback             {
--rollback                 "disabled": false,
--rollback                 "target_value": "",
--rollback                 "target_column": "",
--rollback                 "variable_type": "identification",
--rollback                 "variable_abbrev": "DESCRIPTION",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_filter": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "entries",
--rollback                 "secondary_target_column": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'BREEDING_TRIAL_ENTRY_LIST_ACT_VAL'
--rollback ;