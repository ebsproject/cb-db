--liquibase formatted sql

--changeset postgres:update_post_harvest_view_settings_2410 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2535 CB-DC Post harvest: Update Post Harvest View Settings 24.10 #2



UPDATE
    platform.config
SET
    config_value = $${
        "headers": [
            {
                "display_name": "Occurrence Name",
                "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
                "display_name": "Germplasm Name",
                "variable_abbrev": "GERMPLASM_NAME"
            },
            {
                "display_name": "Actual Grain Yield In Grams",
                "variable_abbrev": "AYLD_CONT"
            },
            {
                "display_name": "Moisture Content %",
                "variable_abbrev": "MC_CONT"
            }
        ],
        "numberOfPreviewItems": 5
    }$$
WHERE
    abbrev = 'DC_GLOBAL_POST_HARVEST_VIEW_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "headers": [
            {
                "display_name": "Germplasm Name",
                "variable_abbrev": "GERMPLASM_NAME"
            },
            {
                "display_name": "Actual Grain Yield In Grams",
                "variable_abbrev": "AYLD_CONT"
            },
            {
                "display_name": "Moisture Content %",
                "variable_abbrev": "MC_CONT"
            }
        ],
        "numberOfPreviewItems": 5
    }$$
WHERE
    abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_VIEW_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "headers": [
            {
                "display_name": "Occurrence Name",
                "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
                "display_name": "Germplasm Name",
                "variable_abbrev": "GERMPLASM_NAME"
            },
            {
                "display_name": "Actual Grain Yield In Grams",
                "variable_abbrev": "AYLD_CONT"
            },
            {
                "display_name": "Moisture Content %",
                "variable_abbrev": "MC_CONT"
            }
        ],
        "numberOfPreviewItems": 5
    }$$
WHERE
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_VIEW_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "headers": [
            {
                "display_name": "Occurrence Name",
                "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
                "display_name": "Germplasm Name",
                "variable_abbrev": "GERMPLASM_NAME"
            },
            {
                "display_name": "Actual Grain Yield In Grams",
                "variable_abbrev": "AYLD_CONT"
            }
        ],
        "numberOfPreviewItems": 5
    }$$
WHERE
    abbrev = 'DC_PROGRAM_KE_POST_HARVEST_VIEW_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "headers": [
            {
                "display_name": "Occurrence Name",
                "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
                "display_name": "Germplasm Name",
                "variable_abbrev": "GERMPLASM_NAME"
            },
            {
                "display_name": "Moisture Content %",
                "variable_abbrev": "MC_CONT"
            }
        ],
        "numberOfPreviewItems": 5
    }$$
WHERE
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_VIEW_SETTINGS';

--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "headers": [
--rollback             {
--rollback                 "display_name": "Occurrence Name",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Germplasm Name",
--rollback                 "variable_abbrev": "GERMPLASM_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Plot Number",
--rollback                 "variable_abbrev": "PLOTNO"
--rollback             },
--rollback             {
--rollback                 "display_name": "Actual Grain Yield In Grams",
--rollback                 "variable_abbrev": "AYLD_CONT"
--rollback             },
--rollback             {
--rollback                 "display_name": "Moisture Content %",
--rollback                 "variable_abbrev": "MC_CONT"
--rollback             }
--rollback         ],
--rollback         "numberOfPreviewItems": 5
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_VIEW_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "headers": [
--rollback             {
--rollback                 "display_name": "Occurrence Name",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Germplasm Name",
--rollback                 "variable_abbrev": "GERMPLASM_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Plot Number",
--rollback                 "variable_abbrev": "PLOTNO"
--rollback             },
--rollback             {
--rollback                 "display_name": "Actual Grain Yield In Grams",
--rollback                 "variable_abbrev": "AYLD_CONT"
--rollback             },
--rollback             {
--rollback                 "display_name": "Moisture Content %",
--rollback                 "variable_abbrev": "MC_CONT"
--rollback             }
--rollback         ],
--rollback         "numberOfPreviewItems": 5
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_VIEW_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "headers": [
--rollback             {
--rollback                 "display_name": "Occurrence Name",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Germplasm Name",
--rollback                 "variable_abbrev": "GERMPLASM_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Plot Number",
--rollback                 "variable_abbrev": "PLOTNO"
--rollback             },
--rollback             {
--rollback                 "display_name": "Actual Grain Yield In Grams",
--rollback                 "variable_abbrev": "AYLD_CONT"
--rollback             }
--rollback         ],
--rollback         "numberOfPreviewItems": 5
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_VIEW_SETTINGS';
--rollback
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "headers": [
--rollback             {
--rollback                 "display_name": "Occurrence Name",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Germplasm Name",
--rollback                 "variable_abbrev": "GERMPLASM_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Plot Number",
--rollback                 "variable_abbrev": "PLOTNO"
--rollback             }
--rollback         ],
--rollback         "numberOfPreviewItems": 5
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_KE_POST_HARVEST_VIEW_SETTINGS';
--rollback
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "headers": [
--rollback             {
--rollback                 "display_name": "Occurrence Name",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback                 "display_name": "Germplasm Name",
--rollback                 "variable_abbrev": "GERMPLASM_NAME"
--rollback             }
--rollback         ],
--rollback         "numberOfPreviewItems": 5
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_VIEW_SETTINGS';