--liquibase formatted sql

--changeset postgres:add_scale_value_to_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5768 Add scale values to OCCURRENCE_STATUS



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'OCCURRENCE_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('trait data quality checked', 'Trait data quality checked', 'Trait Data Quality Checked', 'TRAIT_DATA_QUALITY_CHECKED', 'show'),
        ('design generation in progress', 'Design Generation In Progress', 'Design Generation In Progress', 'DESIGN_GENERATION_IN_PROGRESS', 'show'),
        ('design generation done', 'Design Generation Done', 'Design Generation Done', 'DESIGN_GENERATION_DONE', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     value in ('trait data quality checked', 'design generation in progress', 'design generation done') AND scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'OCCURRENCE_STATUS')