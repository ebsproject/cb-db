--liquibase formatted sql

--changeset postgres:void_data_collection_transaction_application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5659 Void data collection transaction in application action



UPDATE 
    platform.application_action 
SET 
    is_void = TRUE 
WHERE 
    controller='transaction' 
    AND module='dataCollection';



-- revert changes
--rollback UPDATE 
--rollback     platform.application_action 
--rollback SET 
--rollback     is_void = FALSE 
--rollback WHERE 
--rollback     controller='transaction' 
--rollback     AND module='dataCollection';