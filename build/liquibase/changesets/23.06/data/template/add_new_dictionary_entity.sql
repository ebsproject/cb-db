--liquibase formatted sql

--changeset postgres:add_new_dictionary_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5727 GM Database: Add ‘entity’ column to germplasm.file_upload table



INSERT INTO 
    dictionary.entity (abbrev, name, creator_id, table_id)
VALUES
    ('GERMPLASM_ATTRIBUTE', 'germplasm', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'GERMPLASM')),
    ('GERMPLASM_RELATION', 'germplasm', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'GERMPLASM'));



--rollback DELETE FROM dictionary.entity WHERE abbrev IN ('GERMPLASM_ATTRIBUTE','GERMPLASM_RELATION');



