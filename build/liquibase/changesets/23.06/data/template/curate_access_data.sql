--liquibase formatted sql

--changeset postgres:curate_access_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2555 Curate access_data



-- disable triggers
ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
;

ALTER TABLE experiment.occurrence
    DISABLE TRIGGER occurrence_update_occurrence_document_tgr
;

--curate id 25
WITH t1 AS (
    SELECT 
        id,
        access_data old_access_data, 
        REPLACE(access_data::varchar, '"dataRoleId": 25', '"permission": "write"') new_access_data
    FROM 
        experiment.occurrence o 
    WHERE 
        access_data::varchar ILIKE '%"dataRoleId": 25%'
)
UPDATE 
    experiment.occurrence o
SET
    access_data = t1.new_access_data::json FROM t1
WHERE 
    o.id = t1.id
;
    
--curate id 26
WITH t1 AS (
    SELECT 
        id,
        access_data old_access_data, 
        REPLACE(access_data::varchar, '"dataRoleId": 26', '"permission": "write"') new_access_data
    FROM 
        experiment.occurrence o 
    WHERE 
        access_data::varchar ILIKE '%"dataRoleId": 26%'
)
UPDATE 
    experiment.occurrence o
SET
    access_data = t1.new_access_data::json FROM t1
WHERE 
    o.id = t1.id
;

--curate id 27
WITH t1 AS (
    SELECT 
        id,
        access_data old_access_data, 
        REPLACE(access_data::varchar, '"dataRoleId": 27', '"permission": "read"') new_access_data
    FROM 
        experiment.occurrence o 
    WHERE 
        access_data::varchar ILIKE '%"dataRoleId": 27%'
)
UPDATE 
    experiment.occurrence o
SET
    access_data = t1.new_access_data::json FROM t1
WHERE 
    o.id = t1.id
;

-- enable triggers
ALTER TABLE experiment.occurrence
    ENABLE TRIGGER occurrence_update_location_document_from_occurrence_tgr
;

ALTER TABLE experiment.occurrence
    ENABLE TRIGGER occurrence_update_occurrence_document_tgr
;



-- revert changes
--rollback SELECT NULL;