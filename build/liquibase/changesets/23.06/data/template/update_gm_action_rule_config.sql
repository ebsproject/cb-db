--liquibase formatted sql

--changeset postgres:update_gm_action_rule_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5683 CB GM: As a user, I should be able to view the conflicts based on the germplasm_names and germplasm_attributes both in the report and in the resolve conflicts browser



UPDATE platform.config
SET
    config_value = $${
        "values": [{
            "CONFLICT_MARKERS": {
                "MATCH_VALUE": {
                    "DESIGNATION": {
                        "table": "germplasm",
                        "column": "designation",
                        "schema": "germplasm",
                        "target": {
                        "table": "germplasm_name",
                        "column": "name_value",
                        "filter": [
                            {
                            "value": "standard",
                            "column": "germplasm_name_status"
                            }
                        ],
                        "process": "filter"
                        },
                        "condition": []
                    },
                    "OTHER_NAMES": {
                        "table": "germplasm",
                        "column": "other_names",
                        "schema": "germplasm",
                        "target": {
                        "table": "germplasm_name",
                        "column": "name_value",
                        "filter": [],
                        "process": "aggregate"
                        },
                        "condition": []
                    }
                },
                "SINGLE_RECORD": {
                    "GRAIN_COLOR": {
                        "table": "germplasm_attribute",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_attribute",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                            {
                                "value": "GRAIN_COLOR",
                                "column": "variableAbbrev",
                                "data_value": "attributeDataValue"
                            }
                        ]
                    },
                    "CROSS_NUMBER": {
                        "table": "germplasm_attribute",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_attribute",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                            {
                                "value": "CROSS_NUMBER",
                                "column": "variableAbbrev",
                                "data_value": "attributeDataValue"
                            }
                        ]
                    },
                    "GROWTH_HABIT": {
                        "table": "germplasm_attribute",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_attribute",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                        {
                            "value": "GROWTH_HABIT",
                            "column": "variableAbbrev",
                            "data_value": "attributeDataValue"
                        }
                        ]
                    },
                    "GERMPLASM_NAME": {
                        "table": "germplasm_name",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_name",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                            {
                                "value": "standard",
                                "column": "germplasmNameStatus"
                            },
                            {
                                "value": "bcid,pedigree,selection_history,species_name",
                                "column": "germplasmNameType",
                                "data_value": "nameValue"
                            }
                        ]
                    },
                    "HETEROTIC_GROUP": {
                        "table": "germplasm_attribute",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_attribute",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                            {
                                "value": "HETEROTIC_GROUP",
                                "column": "variableAbbrev",
                                "data_value": "attributeDataValue"
                            }
                        ]
                    },
                    "DH_FIRST_INCREASE_COMPLETED": {
                        "table": "germplasm_attribute",
                        "column": "id",
                        "schema": "germplasm",
                        "target": {
                            "table": "germplasm_attribute",
                            "column": "id",
                            "filter": [],
                            "process": ""
                        },
                        "condition": [
                            {
                                "value": "DH_FIRST_INCREASE_COMPLETED",
                                "column": "variableAbbrev",
                                "data_value": "attributeDataValue"
                            }
                        ]
                    }
                }
            }
        }]
    }$$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_ACTION_RULE_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback     $${
--rollback      "values":[
--rollback          {
--rollback              "CONFLICT_MARKERS":{
--rollback                  "MATCH_VALUE":{
--rollback                      "DESIGNATION":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm",
--rollback                          "column": "designation",
--rollback                          "target":{
--rollback                              "column":"name_value",
--rollback                              "table":"germplasm_name",
--rollback                              "process":"filter",
--rollback                              "filter":[
--rollback                                  {
--rollback                                      "column":"germplasm_name_status",
--rollback                                      "value":"standard"
--rollback                                  }
--rollback                              ]
--rollback                          },
--rollback                          "condition":[]
--rollback                      },
--rollback                      "OTHER_NAMES":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm",
--rollback                          "column": "other_names",
--rollback                          "target": {
--rollback                              "column":"name_value",
--rollback                              "table":"germplasm_name",
--rollback                              "process":"aggregate",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": []
--rollback                      }
--rollback                  },
--rollback                  "SINGLE_RECORD":{
--rollback                      "GERMPLASM_NAME":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_name",
--rollback                          "column": "id",
--rollback                          "target":{
--rollback                              "column":"id",
--rollback                              "table":"germplasm_name",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"germplasmNameStatus",
--rollback                                  "value":"standard"
--rollback                              },
--rollback                              {
--rollback                                  "column":"germplasmNameType",
--rollback                                  "value":"pedigree, selection_history"
--rollback                              }
--rollback                          ]
--rollback                      },
--rollback                      "GRAIN_COLOR":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_attribute",
--rollback                          "column": "id",
--rollback                          "target": {
--rollback                              "column":"id",
--rollback                              "table":"germplasm_attribute",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"variableAbbrev",
--rollback                                  "value":"GRAIN_COLOR"
--rollback                              }
--rollback                          ]
--rollback                      },
--rollback                      "GROWTH_HABIT":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_attribute",
--rollback                          "column": "id",
--rollback                          "target": {
--rollback                              "column":"id",
--rollback                              "table":"germplasm_attribute",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"variableAbbrev",
--rollback                                  "value":"GROWTH_HABIT",
--rollback                                  "data_value":"attributeDataValue"
--rollback                              }
--rollback                          ]
--rollback                      },
--rollback                      "CROSS_NUMBER":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_attribute",
--rollback                          "column": "id",
--rollback                          "target": {
--rollback                              "column":"id",
--rollback                              "table":"germplasm_attribute",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"variableAbbrev",
--rollback                                  "value":"CROSS_NUMBER",
--rollback                                  "data_value":"attributeDataValue"
--rollback                              }
--rollback                          ]
--rollback                      },
--rollback                      "HETEROTIC_GROUP":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_attribute",
--rollback                          "column": "id",
--rollback                          "target": {
--rollback                              "column":"id",
--rollback                              "table":"germplasm_attribute",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"variableAbbrev",
--rollback                                  "value":"HETEROTIC_GROUP",
--rollback                                  "data_value":"attributeDataValue"
--rollback                              }
--rollback                          ]
--rollback                      },
--rollback                      "DH_FIRST_INCREASE_COMPLETED":{
--rollback                          "schema": "germplasm",
--rollback                          "table": "germplasm_attribute",
--rollback                          "column": "id",
--rollback                          "target": {
--rollback                              "column":"id",
--rollback                              "table":"germplasm_attribute",
--rollback                              "process":"",
--rollback                              "filter":[]
--rollback                          },
--rollback                          "condition": [
--rollback                              {
--rollback                                  "column":"variableAbbrev",
--rollback                                  "value":"DH_FIRST_INCREASE_COMPLETED",
--rollback                                  "data_value":"attributeDataValue"
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              }
--rollback          }
--rollback      ]
--rollback  }$$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_ACTION_RULE_SYSTEM_DEFAULT';