--liquibase formatted sql

--changeset postgres:insert_gm_update_variables_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5638 As a CB ADMIN, I should be able to specify germplasm attribute config, so that I can specify the variables that can be used as germplasm attributes



-- insert GLOBAL config
INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG',
        'Germplasm Manager - Germplasm Manager UPDATE tool GLOBAL variable configuration record',
        $$
        {
            "values": [
                {
                    "abbrev":"GERMPLASM_CODE",
                    "display_value":"Germplasm Code",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"germplasmCode",
                        "additional_filters":{}
                    },
                    "usage": "required",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"DESIGNATION",
                    "display_value":"Designation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"designation",
                        "additional_filters":{}
                    },
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"PARENTAGE",
                    "display_value":"Parentage",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GENERATION",
                    "display_value":"Generation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_STATE",
                    "display_value":"Germplasm State",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_TYPE",
                    "display_value":"Germplasm Type",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                }
            ]
        }
        $$,
        'germplasm_manager'
    );

-- insert CROP config
INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'GM_CROP_WHEAT_GERMPLASM_UPDATE_CONFIG',
        'Germplasm Manager - Germplasm Manager UPDATE tool WHEAT variable configuration record',
        $$
        {
            "values": [
                {
                    "abbrev":"GERMPLASM_CODE",
                    "display_value":"Germplasm Code",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"germplasmCode",
                        "additional_filters":{}
                    },
                    "usage": "required",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"DESIGNATION",
                    "display_value":"Designation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"designation",
                        "additional_filters":{}
                    },
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"PARENTAGE",
                    "display_value":"Parentage",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GENERATION",
                    "display_value":"Generation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_STATE",
                    "display_value":"Germplasm State",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_TYPE",
                    "display_value":"Germplasm Type",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"CROSS_NUMBER",
                    "display_value":"Cross Number",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"GET",
                        "search_endpoint":"germplasm/{id}/attributes",
                        "search_params":"",
                        "search_api_field":"",
                        "search_filter":"",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GROWTH_HABIT",
                    "display_value":"Growth Habit",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"GET",
                        "search_endpoint":"germplasm/{id}/attributes",
                        "search_params":"",
                        "search_api_field":"",
                        "search_filter":"",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GRAIN_COLOR",
                    "display_value":"Grain Color",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"GET",
                        "search_endpoint":"germplasm/{id}/attributes",
                        "search_params":"",
                        "search_api_field":"",
                        "search_filter":"",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"MTA_STATUS",
                    "display_value":"MTA Status",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-mta-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmMtaDbId",
                        "search_filter":"mtaStatus",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                }
            ]
        }
        $$,
        'germplasm_manager'
    );

INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG',
        'Germplasm Manager - Germplasm Manager UPDATE tool MAIZE variable configuration record',
        $$
        {
            "values": [
                {
                    "abbrev":"GERMPLASM_CODE",
                    "display_value":"Germplasm Code",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"germplasmCode",
                        "additional_filters":{}
                    },
                    "usage": "required",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"DESIGNATION",
                    "display_value":"Designation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"designation",
                        "additional_filters":{}
                    },
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"PARENTAGE",
                    "display_value":"Parentage",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GENERATION",
                    "display_value":"Generation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_STATE",
                    "display_value":"Germplasm State",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_TYPE",
                    "display_value":"Germplasm Type",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"HETEROTIC_GROUP",
                    "display_value":"Heterotic Group",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"GET",
                        "search_endpoint":"germplasm/{id}/attributes",
                        "search_params":"",
                        "search_api_field":"",
                        "search_filter":"",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"DH_FIRST_INCREASE_COMPLETED",
                    "display_value":"Double Haploid First Increase Completed",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"GET",
                        "search_endpoint":"germplasm/{id}/attributes",
                        "search_params":"",
                        "search_api_field":"",
                        "search_filter":"",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"MTA_STATUS",
                    "display_value":"MTA Status",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-mta-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmMtaDbId",
                        "search_filter":"mtaStatus",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                }
            ]
        }
        $$,
        'germplasm_manager'
    );

INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'GM_CROP_RICE_GERMPLASM_UPDATE_CONFIG',
        'Germplasm Manager - Germplasm Manager UPDATE tool RICE variable configuration record',
        $$
        {
            "values": [
                {
                    "abbrev":"GERMPLASM_CODE",
                    "display_value":"Germplasm Code",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"germplasmCode",
                        "additional_filters":{}
                    },
                    "usage": "required",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"DESIGNATION",
                    "display_value":"Designation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmDbId",
                        "search_filter":"designation",
                        "additional_filters":{}
                    },
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"PARENTAGE",
                    "display_value":"Parentage",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GENERATION",
                    "display_value":"Generation",
                    "type":"basic",
                    "entity":"germplasm",
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional",
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_STATE",
                    "display_value":"Germplasm State",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"GERMPLASM_TYPE",
                    "display_value":"Germplasm Type",
                    "type":"basic", 
                    "entity":"germplasm", 
                    "retrieve_db_id": "false",
                    "api_resource":{},
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                },
                {
                    "abbrev":"MTA_STATUS",
                    "display_value":"MTA Status",
                    "type":"attribute", 
                    "entity":"germplasm_attribute", 
                    "retrieve_db_id": "true",
                    "api_resource":{
                        "http_method":"POST",
                        "search_endpoint":"germplasm-mta-search",
                        "search_params":"limit=1",
                        "search_api_field":"germplasmMtaDbId",
                        "search_filter":"mtaStatus",
                        "additional_filters":{}
                    },
                    "usage": "optional", 
                    "in_export": "true",
                    "in_upload": "true",
                    "in_browser": "true",
                    "view":{
                        "visible": "true",
                        "entities": [
                            "germplasm"
                        ]
                    }
                }
            ]
        }
        $$,
        'germplasm_manager'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';
--rollback DELETE FROM platform.config WHERE abbrev = 'GM_CROP_WHEAT_GERMPLASM_UPDATE_CONFIG';
--rollback DELETE FROM platform.config WHERE abbrev = 'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG';
--rollback DELETE FROM platform.config WHERE abbrev = 'GM_CROP_RICE_GERMPLASM_UPDATE_CONFIG';
