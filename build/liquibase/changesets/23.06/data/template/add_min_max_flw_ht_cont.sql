--liquibase formatted sql

--changeset postgres:update_scale_values_of_generation_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2543 Add min/max values in the template for calculated traits/variables


UPDATE master.scale SET min_value = '0', max_value = '200' where id = (select scale_id from master.variable where abbrev = 'HT_CONT');
UPDATE master.scale SET min_value = '0', max_value = '100' where id = (select scale_id from master.variable where abbrev = 'FLW_CONT');


--rollback UPDATE master.scale SET min_value = null, max_value = null where id = (select scale_id from master.variable where abbrev = 'HT_CONT');
--rollback UPDATE master.scale SET min_value = null, max_value = null where id = (select scale_id from master.variable where abbrev = 'FLW_CONT');
