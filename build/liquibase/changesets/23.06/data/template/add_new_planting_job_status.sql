--liquibase formatted sql

--changeset postgres:add_new_planting_job_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5773 CB DB: Create changeset for adding new packing/planting job status



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'PLANTING_JOB_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('submission in progress', 'Submission in Progress', 'Submission in Progress', 'SUBMISSION_IN_PROGRESS', 'show'),
        ('cancellation in progress', 'Cancellation in Progress', 'Cancellation in Progress', 'CANCELLATION_IN_PROGRESS', 'show')
        -- TODO: Add more rows for more scale values
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'PLANTING_JOB_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'PLANTING_JOB_STATUS_SUBMISSION_IN_PROGRESS',
--rollback         'PLANTING_JOB_STATUS_CANCELLATION_IN_PROGRESS'
--rollback     )
--rollback ;