--liquibase formatted sql

--changeset postgres:add_variable_and_scale_value_transaction_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2554: Add a new variable and scale values for TRANSACTION_STATUS


DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('TRANSACTION_STATUS','Transaction Status','Transaction Status') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Transaction Status','TRANSACTION_STATUS',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'TRANSACTION_STATUS' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('TRANSACTION_STATUS','categorical','Transaction Status',NULL,NULL) 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'in queue',1,'In Queue','In Queue',1,false,'TRANSACTION_STATUS_IN_QUEUE');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'uploading in progress',2,'Uploading in Progress','Uploading in Progress',1,false,'TRANSACTION_STATUS_UPLOADING_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev)
	VALUES 
		(var_scale_id,'uploaded',3,'Uploaded','Uploaded',1,false,'TRANSACTION_STATUS_UPLOADED');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'suppression in progress',4,'Suppression in Progress','Suppression in Progress',1,false,'TRANSACTION_STATUS_SUPPRESSION_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'undo suppression in progress',5,'Undo Suppression in Progress','Undo Suppression in Progress',1,false,'TRANSACTION_STATUS_UNDO_SUPRESSION_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'validation in progress',6,'Validation in Progress','Validation in Progress',1,false,'TRANSACTION_STATUS_VALIDATION_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'validated',7,'Validated','Validated',1,false,'TRANSACTION_STATUS_VALIDATED');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'committing in progress',8,'Committing in Progress','Committing in Progress',1,false,'TRANSACTION_STATUS_COMMITTING_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'committed',9,'Committed','Committed',1,false,'TRANSACTION_STATUS_COMMITTED');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'deletion in progress',10,'Deletion in Progress','Deletion in Progress',1,false,'TRANSACTION_STATUS_DELETION_IN_PROGRESS');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, creator_id, is_void, abbrev) 
	VALUES 
		(var_scale_id,'error in background process',11,'Error in Background Process','Error in Background Process',1,false,'TRANSACTION_STATUS_ERROR_IN_BACKGROUND_PROCESS');	

	UPDATE master.scale SET min_value='in queue', max_value='error in background process' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Transaction Status','Transaction Status','character varying','Shows status of a transaction','Transaction Status','False','TRANSACTION_STATUS','application','metadata',NULL) 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

 SELECT count(id) FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_count_variable_set_id;
 IF var_count_variable_set_id > 0 THEN
 	SELECT id FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_variable_set_id;
 ELSE
 	INSERT INTO
 		master.variable_set (abbrev,name) 
 	VALUES 
 		('EXPERIMENT_CREATION','Experiment Creation') 
 	RETURNING id INTO var_variable_set_id;
 END IF;

 --GET THE LAST ORDER NUMBER

 SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
 IF var_count_variable_set_member_id > 0 THEN
 	SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
 ELSE
 	var_variable_set_member_order_number = 1;
 END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$

--rollback --ALTER TABLE master.scale_value DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS');
--rollback --ALTER TABLE master.scale_value ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.scale DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS');
--rollback --ALTER TABLE master.scale ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master."property" DISABLE TRIGGER ALL;
--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS');
--rollback --ALTER TABLE master."property" ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.method DISABLE TRIGGER ALL;
--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS');
--rollback --ALTER TABLE master.method ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set_member DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS');
--rollback --ALTER TABLE master.variable_set_member ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set WHERE abbrev = 'EXPERIMENT_CREATION';
--rollback --ALTER TABLE master.variable_set ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'TRANSACTION_STATUS';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
