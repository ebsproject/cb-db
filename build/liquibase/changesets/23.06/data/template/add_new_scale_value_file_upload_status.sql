--liquibase formatted sql

--changeset postgres:add_new_scale_value_to_file_upload_status context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5571: GM - Add one new scale value for FILE_UPLOAD_ACTION variable



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES ('resolving conflict', 'Resolving Conflict', 'Resolving Conflict', 'RESOLVING_CONFLICT', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
;

-- update germplasm file status constraint
ALTER TABLE
    germplasm.file_upload
DROP CONSTRAINT IF EXISTS
    file_upload_file_status_chk;
	
ALTER TABLE 
    germplasm.file_upload
ADD CONSTRAINT 
    file_upload_file_status_chk CHECK (
        file_status::text = ANY (
            ARRAY['in queue'::text, 'created'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text, 'merge ready'::text, 'merging in progress'::text, 'merge failed'::text, 'resolving conflict'::text]
    ));



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback      'GERMPLASM_FILE_UPLOAD_STATUS_RESOLVING_CONFLICT'
--rollback     )
--rollback ;
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback DROP CONSTRAINT IF EXISTS
--rollback     file_upload_file_status_chk;
--rollback 	
--rollback ALTER TABLE 
--rollback     germplasm.file_upload
--rollback ADD CONSTRAINT 
--rollback     file_upload_file_status_chk CHECK (
--rollback         file_status::text = ANY (
--rollback             ARRAY['in queue'::text, 'created'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text, 'merge ready'::text, 'merging in progress'::text, 'merge failed'::text]
--rollback     ));