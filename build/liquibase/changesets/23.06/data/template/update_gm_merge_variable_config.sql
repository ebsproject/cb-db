--liquibase formatted sql

--changeset postgres:update_gm_merge_variable_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5670 As a user, I should be able to view the successfully completed merge germplasm transaction.



UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
            "name": "Germplasm Code Keep",
            "label": "Germplasm Code of retained germplasm",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_KEEP"
            },
            {
            "name": "Germplasm Code Merge",
            "label": "Germplasm Code of germplasm to be merged",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_MERGE"
            },
            {
            "name": "Designation Keep",
            "label": "Designation of retained germplasm",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_KEEP",
            "view": {
                    "visible": "true",
                    "entities": [ "germplasm" ]
                }
            },
            {
            "name": "Designation Merge",
            "label": "Designation of germplasm to be merged",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_MERGE"
            }
        ]
    }
    $$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';

UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
            "name": "Germplasm Code Keep",
            "label": "Germplasm Code of retained germplasm",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_KEEP"
            },
            {
            "name": "Germplasm Code Merge",
            "label": "Germplasm Code of germplasm to be merged",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_MERGE"
            },
            {
            "name": "Designation Keep",
            "label": "Designation of retained germplasm",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_KEEP",
            "view": {
                    "visible": "true",
                    "entities": [ "germplasm" ]
                }
            },
            {
            "name": "Designation Merge",
            "label": "Designation of germplasm to be merged",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_MERGE"
            }
        ]
    }
    $$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_CONFIG_RICE_DEFAULT';

UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
            "name": "Germplasm Code Keep",
            "label": "Germplasm Code of retained germplasm",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_KEEP"
            },
            {
            "name": "Germplasm Code Merge",
            "label": "Germplasm Code of germplasm to be merged",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_MERGE"
            },
            {
            "name": "Designation Keep",
            "label": "Designation of retained germplasm",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_KEEP",
            "view": {
                    "visible": "true",
                    "entities": [ "germplasm" ]
                }
            },
            {
            "name": "Designation Merge",
            "label": "Designation of germplasm to be merged",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_MERGE"
            }
        ]
    }
    $$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_CONFIG_WHEAT_DEFAULT';

UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
            "name": "Germplasm Code Keep",
            "label": "Germplasm Code of retained germplasm",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_KEEP"
            },
            {
            "name": "Germplasm Code Merge",
            "label": "Germplasm Code of germplasm to be merged",
            "abbrev": "GERMPLASM_CODE",
            "entity": "germplasm",
            "required": "true",
            "column_header": "GERMPLASM_CODE_MERGE"
            },
            {
            "name": "Designation Keep",
            "label": "Designation of retained germplasm",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_KEEP",
            "view": {
                    "visible": "true",
                    "entities": [ "germplasm" ]
                }
            },
            {
            "name": "Designation Merge",
            "label": "Designation of germplasm to be merged",
            "abbrev": "DESIGNATION",
            "entity": "germplasm",
            "required": "false",
            "column_header": "DESIGNATION_MERGE"
            }
        ]
    }
    $$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_CONFIG_MAIZE_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback   "values": [
--rollback     {
--rollback       "name": "Germplasm Code Keep",
--rollback       "label": "Germplasm Code of retained germplasm",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Germplasm Code Merge",
--rollback       "label": "Germplasm Code of germplasm to be merged",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_MERGE"
--rollback     },
--rollback     {
--rollback       "name": "Designation Keep",
--rollback       "label": "Designation of retained germplasm",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Designation Merge",
--rollback       "label": "Designation of germplasm to be merged",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_MERGE"
--rollback     }
--rollback   ]
--rollback }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';

--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback   "values": [
--rollback     {
--rollback       "name": "Germplasm Code Keep",
--rollback       "label": "Germplasm Code of retained germplasm",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Germplasm Code Merge",
--rollback       "label": "Germplasm Code of germplasm to be merged",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_MERGE"
--rollback     },
--rollback     {
--rollback       "name": "Designation Keep",
--rollback       "label": "Designation of retained germplasm",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Designation Merge",
--rollback       "label": "Designation of germplasm to be merged",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_MERGE"
--rollback     }
--rollback   ]
--rollback }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_CONFIG_RICE_DEFAULT';

--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback   "values": [
--rollback     {
--rollback       "name": "Germplasm Code Keep",
--rollback       "label": "Germplasm Code of retained germplasm",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Germplasm Code Merge",
--rollback       "label": "Germplasm Code of germplasm to be merged",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_MERGE"
--rollback     },
--rollback     {
--rollback       "name": "Designation Keep",
--rollback       "label": "Designation of retained germplasm",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Designation Merge",
--rollback       "label": "Designation of germplasm to be merged",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_MERGE"
--rollback     }
--rollback   ]
--rollback }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_CONFIG_WHEAT_DEFAULT';

--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback   "values": [
--rollback     {
--rollback       "name": "Germplasm Code Keep",
--rollback       "label": "Germplasm Code of retained germplasm",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Germplasm Code Merge",
--rollback       "label": "Germplasm Code of germplasm to be merged",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "germplasm",
--rollback       "required": "true",
--rollback       "column_header": "GERMPLASM_CODE_MERGE"
--rollback     },
--rollback     {
--rollback       "name": "Designation Keep",
--rollback       "label": "Designation of retained germplasm",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_KEEP"
--rollback     },
--rollback     {
--rollback       "name": "Designation Merge",
--rollback       "label": "Designation of germplasm to be merged",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "germplasm",
--rollback       "required": "false",
--rollback       "column_header": "DESIGNATION_MERGE"
--rollback     }
--rollback   ]
--rollback }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_CONFIG_MAIZE_DEFAULT';