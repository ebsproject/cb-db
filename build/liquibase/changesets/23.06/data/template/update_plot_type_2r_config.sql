--liquibase formatted sql

--changeset postgres:update_plot_type_2r_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5658 Update config PLOT_TYPE_2R in platform.config




UPDATE
    platform.config
SET
    config_value =
    '
    {
      "Name": "Required experiment level protocol plot type variables",
      "Values": [
        {
          "default": 2,
          "disabled": true,
          "required": "required",
          "variable_abbrev": "ROWS_PER_PLOT_CONT"
        },
        {
          "unit": "cm",
          "default": 20,
          "disabled": true,
          "required": "required",
          "variable_abbrev": "DIST_BET_ROWS"
        },
        {
          "default": 0.4,
          "computed": "computed",
          "disabled": true,
          "required": "required",
          "variable_abbrev": "PLOT_WIDTH"
        },
        {
          "unit": "m",
          "default": 5,
          "computed": "computed",
          "disabled": false,
          "required": "required",
          "variable_abbrev": "PLOT_LN"
        },
        {
          "unit": "sqm",
          "default": false,
          "computed": "computed",
          "disabled": true,
          "required": "required",
          "variable_abbrev": "PLOT_AREA_2"
        },
        {
          "default": false,
          "disabled": false,
          "required": false,
          "variable_abbrev": "ALLEY_LENGTH"
        },
        {
          "unit": "m",
          "default": false,
          "disabled": false,
          "required": "required",
          "allow_new_val": true,
          "variable_abbrev": "SEEDING_RATE"
        },
        {
          "default": false,
          "disabled": false,
          "variable_abbrev": "PLANTING_INSTRUCTIONS"
        }
      ]
    }
    '
WHERE
    abbrev = 'PLOT_TYPE_2R';

--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value =
--rollback     '
--rollback     {
--rollback       "Name": "Required experiment level protocol plot type variables",
--rollback       "Values": [
--rollback         {
--rollback           "default": 2,
--rollback           "disabled": true,
--rollback           "required": "required",
--rollback           "variable_abbrev": "ROWS_PER_PLOT_CONT"
--rollback         },
--rollback         {
--rollback           "unit": "cm",
--rollback           "default": 20,
--rollback           "disabled": true,
--rollback           "required": "required",
--rollback           "variable_abbrev": "DIST_BET_ROWS"
--rollback         },
--rollback         {
--rollback           "default": 0.4,
--rollback           "computed": "computed",
--rollback           "disabled": true,
--rollback           "required": "required",
--rollback           "variable_abbrev": "PLOT_WIDTH"
--rollback         },
--rollback         {
--rollback           "unit": "m",
--rollback           "default": false,
--rollback           "computed": "computed",
--rollback           "disabled": false,
--rollback           "required": "required",
--rollback           "variable_abbrev": "PLOT_LN"
--rollback         },
--rollback         {
--rollback           "unit": "sqm",
--rollback           "default": false,
--rollback           "computed": "computed",
--rollback           "disabled": true,
--rollback           "required": "required",
--rollback           "variable_abbrev": "PLOT_AREA_2"
--rollback         },
--rollback         {
--rollback           "default": false,
--rollback           "disabled": false,
--rollback           "required": false,
--rollback           "variable_abbrev": "ALLEY_LENGTH"
--rollback         },
--rollback         {
--rollback           "unit": "m",
--rollback           "default": false,
--rollback           "disabled": false,
--rollback           "required": "required",
--rollback           "allow_new_val": true,
--rollback           "variable_abbrev": "SEEDING_RATE"
--rollback         },
--rollback         {
--rollback           "default": false,
--rollback           "disabled": false,
--rollback           "variable_abbrev": "PLANTING_INSTRUCTIONS"
--rollback         }
--rollback       ]
--rollback     }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'PLOT_TYPE_2R';