--liquibase formatted sql

--changeset postgres:populate_planting_instruction_entry_id_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BA-1874 Populate planting_instruction entry_id values



-- populate planting_instruction.entry_id
WITH t AS (
    SELECT
        pinst.id AS planting_instruction_id,
        plot.id AS plot_id,
        ent.id AS entry_id,
        pinst.entry_id AS planting_instruction_entry_id,
        plot.entry_id AS plot_entry_id
    FROM
        experiment.planting_instruction AS pinst
        INNER JOIN experiment.plot AS plot
            ON plot.id = pinst.plot_id
        INNER JOIN experiment.occurrence AS occ
            ON occ.id = plot.occurrence_id
        INNER JOIN experiment.location_occurrence_group AS logrp
            ON logrp.occurrence_id = plot.occurrence_id
        INNER JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        INNER JOIN experiment.entry_list AS entlist
            ON entlist.experiment_id = expt.id
        INNER JOIN experiment.entry AS ent
            ON ent.entry_list_id = entlist.id
            AND ent.entry_number = pinst.entry_number
            AND ent.entry_code = pinst.entry_code
    WHERE
        pinst.entry_id IS NULL
)
UPDATE
    experiment.planting_instruction AS pinst
SET
    entry_id = t.entry_id,
    notes = platform.append_text(pinst.notes, 'BA-1874 Populate entry_id')
FROM
    t
WHERE
    pinst.entry_id IS NULL
    AND t.planting_instruction_id = pinst.id
    AND t.entry_id IS NOT NULL
;

-- revert changes
--rollback UPDATE experiment.planting_instruction SET entry_id = NULL WHERE notes LIKE '%BA-1874%';


--changeset postgres:populate_plot_entry_id_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BA-1874 Populate plot entry_id values

-- populate plot.entry_id
WITH t AS (
    SELECT
        pinst.id AS planting_instruction_id,
        plot.id AS plot_id,
        ent.id AS entry_id,
        pinst.entry_id AS planting_instruction_entry_id,
        plot.entry_id AS plot_entry_id
    FROM
        experiment.planting_instruction AS pinst
        INNER JOIN experiment.plot AS plot
            ON plot.id = pinst.plot_id
        INNER JOIN experiment.occurrence AS occ
            ON occ.id = plot.occurrence_id
        INNER JOIN experiment.location_occurrence_group AS logrp
            ON logrp.occurrence_id = plot.occurrence_id
        INNER JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        INNER JOIN experiment.entry_list AS entlist
            ON entlist.experiment_id = expt.id
        INNER JOIN experiment.entry AS ent
            ON ent.entry_list_id = entlist.id
            AND ent.entry_number = pinst.entry_number
            AND ent.entry_code = pinst.entry_code
    WHERE
        plot.entry_id IS NULL
)
UPDATE
    experiment.plot AS plot
SET
    entry_id = t.entry_id,
    notes = platform.append_text(plot.notes, 'BA-1874 Populate entry_id')
FROM
    t
WHERE
    plot.entry_id IS NULL
    AND t.plot_id = plot.id
    AND t.entry_id IS NOT NULL
;



-- revert changes
--rollback UPDATE experiment.plot SET entry_id = NULL WHERE notes LIKE '%BA-1874%';