--liquibase formatted sql

--changeset postgres:add_c_collation_indices_designation_sname_plabel_001 context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2562 CORB-5770 - Add C collation b-tree indices for designation (germplasm), seed_name, and package_label columns


-- Index: germplasm_designation_c_idx
CREATE INDEX IF NOT EXISTS germplasm_designation_c_idx
    ON germplasm.germplasm USING btree
    (designation COLLATE pg_catalog."C" ASC NULLS LAST)
    TABLESPACE pg_default;
    
COMMENT ON INDEX germplasm.germplasm_designation_c_idx
    IS 'DEVOPS-2562 - Add germplasm.designation btree C-collation index.';


-- Index: seed_seed_name_c_idx
CREATE INDEX IF NOT EXISTS seed_seed_name_c_idx
    ON germplasm.seed USING btree
    (seed_name COLLATE pg_catalog."C" ASC NULLS LAST)
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.seed_seed_name_c_idx
    IS 'DEVOPS-2562 - Add seed.seed_name btree C-collation index.';

-- Index: package_package_label_c_idx
CREATE INDEX IF NOT EXISTS package_package_label_c_idx
    ON germplasm."package" USING btree
    (package_label COLLATE pg_catalog."C" ASC NULLS LAST)
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.package_package_label_c_idx
    IS 'DEVOPS-2562 - Add package.package_label btree C-collation index.';



-- revert changes
--rollback DROP INDEX IF EXISTS germplasm.germplasm_designation_c_idx;
--rollback DROP INDEX IF EXISTS germplasm.seed_seed_name_c_idx;
--rollback DROP INDEX IF EXISTS germplasm.package_package_label_c_idx;
