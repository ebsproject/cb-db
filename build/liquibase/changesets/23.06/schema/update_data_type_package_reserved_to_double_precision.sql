--liquibase formatted sql

--changeset postgres:update_data_type_package_reserved_to_double_precision context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-5802 CB DB: Alter package_reserved's data type from integer to double precision



ALTER TABLE 
    germplasm.package 
ALTER COLUMN 
    package_reserved 
    TYPE double precision USING package_reserved::double precision
;



-- revert changes
--rollback ALTER TABLE 
--rollback     germplasm.package 
--rollback ALTER COLUMN 
--rollback     package_reserved 
--rollback     TYPE double precision USING package_reserved::double precision
--rollback ;