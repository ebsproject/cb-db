--liquibase formatted sql

--changeset postgres:add_entity_id_column_to_germplasm_file_upload context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-5727 GM Database: Add ‘entity’ column to germplasm.file_upload table



-- add entity_id column
ALTER TABLE
    germplasm.file_upload
ADD COLUMN
    entity_id integer;

-- add comment on new column
COMMENT ON COLUMN germplasm.file_upload.entity_id
    IS 'Entity ID: Unique identifier of referenced entity record.';

-- add index to new column
CREATE INDEX file_upload_entity_id_idx
    ON germplasm.file_upload USING btree (entity_id);

-- add foreign key constraint
ALTER TABLE germplasm.file_upload
ADD CONSTRAINT file_upload_entity_id_fk
    FOREIGN KEY (entity_id)
    REFERENCES dictionary.entity (id)
    ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON CONSTRAINT file_upload_entity_id_fk ON germplasm.file_upload
    IS 'An entity ID can refer an existing entity record in the dictionary.';



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback DROP COLUMN
--rollback     entity_id
--rollback ;