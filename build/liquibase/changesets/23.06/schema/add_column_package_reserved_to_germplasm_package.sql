--liquibase formatted sql

--changeset postgres:add_column_package_reserved_to_germplasm_package context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2545 Add column package_reserved to germplasm.package



ALTER TABLE
    germplasm.package
ADD COLUMN
    package_reserved integer
;

COMMENT ON COLUMN germplasm.package.package_reserved
    IS 'Package Reserved: Reserved quantity of the package [PKG_PACKAGE_RESERVED]';



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.package
--rollback DROP COLUMN
--rollback     package_reserved
--rollback ;