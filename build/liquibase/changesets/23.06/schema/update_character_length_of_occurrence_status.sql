--liquibase formatted sql

--changeset postgres:update_character_length_of_occurrence_status context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2560 Update character length of occurrence status



ALTER TABLE experiment.occurrence
ALTER COLUMN occurrence_status TYPE varchar(256);



-- revert changes
--rollback ALTER TABLE experiment.occurrence
--rollback ALTER COLUMN occurrence_status TYPE varchar(64);