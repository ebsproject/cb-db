--liquibase formatted sql

--changeset postgres:add_column_package_log_id_to_experiment_planting_job_entry context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2546 Add column package_log_id to experiment.planting_job_entry



ALTER TABLE
    experiment.planting_job_entry
ADD COLUMN
    package_log_id integer
;

COMMENT ON COLUMN experiment.planting_job_entry.package_log_id
    IS 'Package Log ID: Unique reference to a linked package log record [PJOBENT_ENT_PACKAGE_LOG_ID]';

-- add index to new column
CREATE INDEX planting_job_entry_package_log_id_idx
    ON experiment.planting_job_entry USING btree (package_log_id);

-- add foreign key constraint
ALTER TABLE experiment.planting_job_entry
ADD CONSTRAINT planting_job_entry_package_log_id_fk
    FOREIGN KEY (package_log_id)
    REFERENCES germplasm.package_log (id)
    ON UPDATE CASCADE ON DELETE RESTRICT;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.planting_job_entry
--rollback DROP COLUMN
--rollback     package_log_id
--rollback ;