--liquibase formatted sql

--changeset postgres:update_data_type_occurrence_applied_to_varchar context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2579 Update occurrence_applied data type from jsonb to varchar



ALTER TABLE 
    platform.plan_template 
ALTER COLUMN 
    occurrence_applied 
    TYPE varchar USING occurrence_applied::varchar
;



-- revert changes
--rollback ALTER TABLE 
--rollback     platform.plan_template 
--rollback ALTER COLUMN 
--rollback     occurrence_applied 
--rollback     TYPE jsonb USING occurrence_applied::jsonb
--rollback ;