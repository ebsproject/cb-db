--liquibase formatted sql

--changeset postgres:drop_column_entry_list_id_to_data_terminal_transaction context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2282 Drop column entry_list_id to data_terminal.transaction



-- drop column
ALTER TABLE
    data_terminal.transaction
DROP COLUMN
    entry_list_id
;



-- revert changes
--rollback ALTER TABLE
--rollback     data_terminal.transaction
--rollback ADD COLUMN
--rollback     entry_list_id integer
--rollback ;
--rollback 
--rollback COMMENT ON COLUMN data_terminal.transaction.entry_list_id
--rollback     IS 'Entry List ID: Reference to the entry list that has the additional information [TRNS_ENTLIST_ID] Name: Description [TRNS_ENTLIST_ID]';
--rollback 
--rollback -- add foreign constraint
--rollback ALTER TABLE data_terminal.transaction
--rollback     ADD CONSTRAINT transaction_entry_list_id_fk
--rollback     FOREIGN KEY (entry_list_id)
--rollback     REFERENCES experiment.entry_list (id)
--rollback     ON UPDATE CASCADE ON DELETE RESTRICT
--rollback ;