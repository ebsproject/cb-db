--liquibase formatted sql

--changeset postgres:add_column_entry_list_id_to_data_terminal_transaction context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2282 Add column entry_list_id to data_terminal.transaction



-- add column
ALTER TABLE
    data_terminal.transaction
ADD COLUMN
    entry_list_id integer
;

COMMENT ON COLUMN data_terminal.transaction.entry_list_id
    IS 'Entry List ID: Reference to the entry list that has the additional information [TRNS_ENTLIST_ID] Name: Description [TRNS_ENTLIST_ID]';

-- add foreign constraint
ALTER TABLE data_terminal.transaction
    ADD CONSTRAINT transaction_entry_list_id_fk
    FOREIGN KEY (entry_list_id)
    REFERENCES experiment.entry_list (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;



-- revert changes
--rollback ALTER TABLE
--rollback     data_terminal.transaction
--rollback DROP COLUMN
--rollback     entry_list_id
--rollback ;