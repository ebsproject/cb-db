--liquibase formatted sql

--changeset postgres:4_populate_package_maize_synthetic_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed and package with newly created germplasm



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id)
SELECT 	
    germplasm.generate_code('package') AS package_code,
    gs.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    gs.id AS seed_id,
    gs.program_id,
    gs.creator_id
FROM
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
WHERE 
    gg.designation
IN
    (
        'DEVOPS-2888-MAIZE_SYNT-4742',
        'DEVOPS-2888-MAIZE_SYNT-1782',
        'DEVOPS-2888-MAIZE_SYNT-3316',
        'DEVOPS-2888-MAIZE_SYNT-8326',
        'DEVOPS-2888-MAIZE_SYNT-8694',
        'DEVOPS-2888-MAIZE_SYNT-5176',
        'DEVOPS-2888-MAIZE_SYNT-2520',
        'DEVOPS-2888-MAIZE_SYNT-5589',
        'DEVOPS-2888-MAIZE_SYNT-6869',
        'DEVOPS-2888-MAIZE_SYNT-7670',
        'DEVOPS-2888-MAIZE_SYNT-6499',
        'DEVOPS-2888-MAIZE_SYNT-2607',
        'DEVOPS-2888-MAIZE_SYNT-6370',
        'DEVOPS-2888-MAIZE_SYNT-6009',
        'DEVOPS-2888-MAIZE_SYNT-5400',
        'DEVOPS-2888-MAIZE_SYNT-4415',
        'DEVOPS-2888-MAIZE_SYNT-7179',
        'DEVOPS-2888-MAIZE_SYNT-253',
        'DEVOPS-2888-MAIZE_SYNT-1049',
        'DEVOPS-2888-MAIZE_SYNT-7603',
        'DEVOPS-2888-MAIZE_SYNT-5588',
        'DEVOPS-2888-MAIZE_SYNT-1469',
        'DEVOPS-2888-MAIZE_SYNT-7230',
        'DEVOPS-2888-MAIZE_SYNT-466',
        'DEVOPS-2888-MAIZE_SYNT-3258'
    )
ORDER BY
    gs.id
;

    

--rollback DELETE FROM
--rollback 	germplasm.package
--rollback WHERE package_label ilike 'DEVOPS-2888-MAIZE_SYNT%'