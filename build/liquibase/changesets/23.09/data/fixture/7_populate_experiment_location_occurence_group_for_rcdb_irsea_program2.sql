--liquibase formatted sql

--changeset postgres:7_populate_experiment_location_occurence_group_for_rcdb_irsea_program2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    experiment.location_occurrence_group
        (location_id,occurrence_id,order_number,creator_id)
SELECT
    location_id,occurrence_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.location WHERE location_code='PH_II_AU-2023-WS-002'),(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047919-002'),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (location_id,occurrence_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.location_occurrence_group
--rollback WHERE
--rollback    location_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.location WHERE location_code='PH_II_AU-2023-WS-002'
--rollback    )
--rollback ;