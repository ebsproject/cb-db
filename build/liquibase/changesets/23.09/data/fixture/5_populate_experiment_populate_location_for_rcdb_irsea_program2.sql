--liquibase formatted sql

--changeset postgres:5_populate_experiment_populate_location_for_rcdb_irsea_program2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    experiment.location
        (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
SELECT
    location_code,location_name,location_status,location_type,description,steward_id,location_planting_date::date,location_harvest_date::date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id::int,location_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('PH_II_AU-2023-WS-002','PH_II_AU-2023-WS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),2023,(SELECT id FROM tenant.season WHERE season_code='WS'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_II_AU'),NULL,2,NULL,0,248))
     t (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
;



--rollback UPDATE experiment.plot p
--rollback SET    location_id = NULL
--rollback WHERE  location_id = (SELECT id
--rollback                       FROM   experiment.location
--rollback                       WHERE  location_code = 'PH_II_AU-2023-WS-002'); 
--rollback 
--rollback DELETE FROM
--rollback    experiment.location
--rollback WHERE
--rollback    location_code
--rollback IN
--rollback    (
--rollback        'PH_II_AU-2023-WS-002'
--rollback    )
--rollback ;