--liquibase formatted sql

--changeset postgres:1_populate_experiment_experiment_for_rcdb_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    experiment.experiment
        (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
SELECT
    program_id,pipeline_id::int,stage_id,project_id::int,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id::int,creator_id,data_process_id,crop_id,remarks
FROM
    (
        VALUES
            ((SELECT id FROM tenant.program WHERE program_code='IRSEA'),NULL,(SELECT id FROM tenant.stage WHERE stage_code='OYT'),NULL,2023,(SELECT id FROM tenant.season WHERE season_code='WS'),NULL,'EXP0047919','BA2023_AugRCBD_T206',NULL,'Breeding Trial',NULL,NULL,'Augmented RCBD','planted',NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM master.item WHERE abbrev='BREEDING_TRIAL_DATA_PROCESS'),(SELECT id FROM tenant.crop WHERE crop_code='RICE'),NULL))
     t (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
;



--rollback DELETE FROM
--rollback    experiment.experiment
--rollback WHERE
--rollback    experiment_code
--rollback IN
--rollback    (
--rollback        'EXP0047919'
--rollback    )
--rollback ;