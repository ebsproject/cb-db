--liquibase formatted sql

--changeset postgres:2_populate_germplasm_name_maize_landrace_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Populate germplasm_name with newly created germplasm data



INSERT INTO
    germplasm.germplasm_name 
        (
            germplasm_id, 
            name_value, 
            germplasm_name_type, 
            germplasm_name_status, 
            germplasm_normalized_name, 
            creator_id
        )
SELECT 
    gg.id,
    gg.designation,
    gg.germplasm_name_type,
    'standard',
    platform.normalize_text(gg.designation),
    gg.creator_id
FROM
    germplasm.germplasm gg
WHERE
    designation 
 IN (
     'DEVOPS-2888-MAIZE_LAND-121',
     'DEVOPS-2888-MAIZE_LAND-6991',
     'DEVOPS-2888-MAIZE_LAND-3690',
     'DEVOPS-2888-MAIZE_LAND-3867',
     'DEVOPS-2888-MAIZE_LAND-5776',
     'DEVOPS-2888-MAIZE_LAND-6168',
     'DEVOPS-2888-MAIZE_LAND-9226',
     'DEVOPS-2888-MAIZE_LAND-3481',
     'DEVOPS-2888-MAIZE_LAND-1431',
     'DEVOPS-2888-MAIZE_LAND-6228',
     'DEVOPS-2888-MAIZE_LAND-5773',
     'DEVOPS-2888-MAIZE_LAND-9595',
     'DEVOPS-2888-MAIZE_LAND-5143',
     'DEVOPS-2888-MAIZE_LAND-2887',
     'DEVOPS-2888-MAIZE_LAND-9046',
     'DEVOPS-2888-MAIZE_LAND-5366',
     'DEVOPS-2888-MAIZE_LAND-9496',
     'DEVOPS-2888-MAIZE_LAND-5328',
     'DEVOPS-2888-MAIZE_LAND-2846',
     'DEVOPS-2888-MAIZE_LAND-6620',
     'DEVOPS-2888-MAIZE_LAND-1995',
     'DEVOPS-2888-MAIZE_LAND-8302',
     'DEVOPS-2888-MAIZE_LAND-7837',
     'DEVOPS-2888-MAIZE_LAND-8164',
     'DEVOPS-2888-MAIZE_LAND-9547'
    )

ORDER BY
    gg.id 
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm_name
--rollback WHERE
--rollback     germplasm_normalized_name 
--rollback IN (
--rollback     'DEVOPS-2888-MAIZE_LAND-121',
--rollback     'DEVOPS-2888-MAIZE_LAND-6991',
--rollback     'DEVOPS-2888-MAIZE_LAND-3690',
--rollback     'DEVOPS-2888-MAIZE_LAND-3867',
--rollback     'DEVOPS-2888-MAIZE_LAND-5776',
--rollback     'DEVOPS-2888-MAIZE_LAND-6168',
--rollback     'DEVOPS-2888-MAIZE_LAND-9226',
--rollback     'DEVOPS-2888-MAIZE_LAND-3481',
--rollback     'DEVOPS-2888-MAIZE_LAND-1431',
--rollback     'DEVOPS-2888-MAIZE_LAND-6228',
--rollback     'DEVOPS-2888-MAIZE_LAND-5773',
--rollback     'DEVOPS-2888-MAIZE_LAND-9595',
--rollback     'DEVOPS-2888-MAIZE_LAND-5143',
--rollback     'DEVOPS-2888-MAIZE_LAND-2887',
--rollback     'DEVOPS-2888-MAIZE_LAND-9046',
--rollback     'DEVOPS-2888-MAIZE_LAND-5366',
--rollback     'DEVOPS-2888-MAIZE_LAND-9496',
--rollback     'DEVOPS-2888-MAIZE_LAND-5328',
--rollback     'DEVOPS-2888-MAIZE_LAND-2846',
--rollback     'DEVOPS-2888-MAIZE_LAND-6620',
--rollback     'DEVOPS-2888-MAIZE_LAND-1995',
--rollback     'DEVOPS-2888-MAIZE_LAND-8302',
--rollback     'DEVOPS-2888-MAIZE_LAND-7837',
--rollback     'DEVOPS-2888-MAIZE_LAND-8164',
--rollback     'DEVOPS-2888-MAIZE_LAND-9547'
--rollback     )
--rollback       
--rollback ;