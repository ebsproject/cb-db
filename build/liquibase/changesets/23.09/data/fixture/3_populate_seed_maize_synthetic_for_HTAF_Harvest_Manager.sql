--liquibase formatted sql


--changeset postgres:3_populate_seed_maize_synthetic_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO 
    germplasm.seed 
        (
            seed_code, 
            seed_name, 
            germplasm_id,
            program_id,
            creator_id
        )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name,
    g.id AS germplasm_id,
    tp.id program_id,
    g.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
            VALUES
            ('DEVOPS-2888-MAIZE_SYNT-4742'),
            ('DEVOPS-2888-MAIZE_SYNT-1782'),
            ('DEVOPS-2888-MAIZE_SYNT-3316'),
            ('DEVOPS-2888-MAIZE_SYNT-8326'),
            ('DEVOPS-2888-MAIZE_SYNT-8694'),
            ('DEVOPS-2888-MAIZE_SYNT-5176'),
            ('DEVOPS-2888-MAIZE_SYNT-2520'),
            ('DEVOPS-2888-MAIZE_SYNT-5589'),
            ('DEVOPS-2888-MAIZE_SYNT-6869'),
            ('DEVOPS-2888-MAIZE_SYNT-7670'),
            ('DEVOPS-2888-MAIZE_SYNT-6499'),
            ('DEVOPS-2888-MAIZE_SYNT-2607'),
            ('DEVOPS-2888-MAIZE_SYNT-6370'),
            ('DEVOPS-2888-MAIZE_SYNT-6009'),
            ('DEVOPS-2888-MAIZE_SYNT-5400'),
            ('DEVOPS-2888-MAIZE_SYNT-4415'),
            ('DEVOPS-2888-MAIZE_SYNT-7179'),
            ('DEVOPS-2888-MAIZE_SYNT-253'),
            ('DEVOPS-2888-MAIZE_SYNT-1049'),
            ('DEVOPS-2888-MAIZE_SYNT-7603'),
            ('DEVOPS-2888-MAIZE_SYNT-5588'),
            ('DEVOPS-2888-MAIZE_SYNT-1469'),
            ('DEVOPS-2888-MAIZE_SYNT-7230'),
            ('DEVOPS-2888-MAIZE_SYNT-466'),
            ('DEVOPS-2888-MAIZE_SYNT-3258')
            ) AS t (
        designation
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'KE'
;



--rollback DELETE FROM
--rollback 	germplasm.seed
--rollback WHERE seed_name ilike 'DEVOPS-2888-MAIZE_SYNT%'