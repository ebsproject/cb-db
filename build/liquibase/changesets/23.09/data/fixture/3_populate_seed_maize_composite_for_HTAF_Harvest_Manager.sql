--liquibase formatted sql


--changeset postgres:3_populate_seed_maize_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO 
    germplasm.seed 
        (
            seed_code, 
            seed_name, 
            germplasm_id,
            program_id,
            creator_id
        )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name,
    g.id AS germplasm_id,
    tp.id program_id,
    g.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
        VALUES
            ('DEVOPS-2888-MAIZE_COMP-2595'),
            ('DEVOPS-2888-MAIZE_COMP-7414'),
            ('DEVOPS-2888-MAIZE_COMP-4012'),
            ('DEVOPS-2888-MAIZE_COMP-5813'),
            ('DEVOPS-2888-MAIZE_COMP-9176'),
            ('DEVOPS-2888-MAIZE_COMP-2646'),
            ('DEVOPS-2888-MAIZE_COMP-5394'),
            ('DEVOPS-2888-MAIZE_COMP-5674'),
            ('DEVOPS-2888-MAIZE_COMP-9118'),
            ('DEVOPS-2888-MAIZE_COMP-6593'),
            ('DEVOPS-2888-MAIZE_COMP-1387'),
            ('DEVOPS-2888-MAIZE_COMP-8781'),
            ('DEVOPS-2888-MAIZE_COMP-3849'),
            ('DEVOPS-2888-MAIZE_COMP-1514'),
            ('DEVOPS-2888-MAIZE_COMP-8422'),
            ('DEVOPS-2888-MAIZE_COMP-4472'),
            ('DEVOPS-2888-MAIZE_COMP-4647'),
            ('DEVOPS-2888-MAIZE_COMP-1902'),
            ('DEVOPS-2888-MAIZE_COMP-874'),
            ('DEVOPS-2888-MAIZE_COMP-2077'),
            ('DEVOPS-2888-MAIZE_COMP-8118'),
            ('DEVOPS-2888-MAIZE_COMP-4354'),
            ('DEVOPS-2888-MAIZE_COMP-681'),
            ('DEVOPS-2888-MAIZE_COMP-4995'),
            ('DEVOPS-2888-MAIZE_COMP-972')
            ) AS t (
        designation
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'KE'
;



--rollback DELETE FROM
--rollback 	germplasm.seed
--rollback WHERE seed_name ilike 'DEVOPS-2888-MAIZE_COMP%'