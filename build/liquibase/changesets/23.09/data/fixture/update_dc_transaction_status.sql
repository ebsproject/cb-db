--liquibase formatted sql

--changeset postgres:update_dc_transaction_status context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CORB-6372 CB-DB: Curate current DC transaction statuses



UPDATE data_terminal.transaction
SET status = 'uploaded'
WHERE status = 'validated';

UPDATE data_terminal.transaction
SET status = 'uploading in progress'
WHERE status = 'validation in progress';



--rollback UPDATE data_terminal.transaction
--rollback SET status = 'validated'
--rollback WHERE status = 'uploaded';
--rollback
--rollback UPDATE data_terminal.transaction
--rollback SET status = 'validation in progress'
--rollback WHERE status = 'uploading in progress';