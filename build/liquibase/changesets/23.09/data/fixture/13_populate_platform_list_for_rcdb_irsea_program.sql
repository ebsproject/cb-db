--liquibase formatted sql

--changeset postgres:13_populate_platform_list_for_rcdb_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0047919','BA_AugRCBD_2023 Trait Protocol (EXP0047919)','BA_AugRCBD_2023 Trait Protocol (EXP0047919)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'72796667-06a3-4cbc-9fdf-03fe67f2869b','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0047919','BA_AugRCBD_2023 Management Protocol (EXP0047919)','BA_AugRCBD_2023 Management Protocol (EXP0047919)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'daf6fd3b-3030-41d8-af51-93dc69522cc3','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0047919-001','BA2023_AugRCBD_T206-001 Trait Protocol (EXP0047919-001)','BA2023_AugRCBD_T206-001 Trait Protocol (EXP0047919-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'81681a0e-0a8a-4be8-9a29-e8f45eb9be8d','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0047919-001','BA2023_AugRCBD_T206-001 Management Protocol (EXP0047919-001)','BA2023_AugRCBD_T206-001 Management Protocol (EXP0047919-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'c8fd25bc-caec-40d5-b926-cab64d4f77fa','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0047919-002','BA2023_AugRCBD_T206-002 Trait Protocol (EXP0047919-002)','BA2023_AugRCBD_T206-002 Trait Protocol (EXP0047919-002)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'331b48f6-01c3-44f4-b09f-7c4f0c30b371','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0047919-002','BA2023_AugRCBD_T206-002 Management Protocol (EXP0047919-002)','BA2023_AugRCBD_T206-002 Management Protocol (EXP0047919-002)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'529500ad-7bc3-4108-b138-fd83f0f49dec','working list','created',True,NULL),
            ('TRAIT_PROTOCOL_EXP0047919-003','BA2023_AugRCBD_T206-003 Trait Protocol (EXP0047919-003)','BA2023_AugRCBD_T206-003 Trait Protocol (EXP0047919-003)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'52c14b7a-26e5-4f3b-b939-9dc393963c57','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0047919-003','BA2023_AugRCBD_T206-003 Management Protocol (EXP0047919-003)','BA2023_AugRCBD_T206-003 Management Protocol (EXP0047919-003)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'e63b2514-0398-4b39-af0b-ec1be62265c0','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0047919',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919',
--rollback        'TRAIT_PROTOCOL_EXP0047919-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919-001',
--rollback        'TRAIT_PROTOCOL_EXP0047919-002',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919-002',
--rollback        'TRAIT_PROTOCOL_EXP0047919-003',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919-003',
--rollback        'TRAIT_PROTOCOL_EXP0047919-004',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919-004'
--rollback    )
--rollback ;