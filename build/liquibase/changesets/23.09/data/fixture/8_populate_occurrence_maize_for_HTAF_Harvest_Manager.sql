--liquibase formatted sql

--changeset postgres:8_populate_occurrence_maize_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate Occurrence Maize for HTAF Harvest Manager



INSERT INTO
    experiment.occurrence(
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        entry_count,
        plot_count,
        creator_id
    )
SELECT 
    experiment.generate_code('occurrence') AS occurrence_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',exp.experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS occurrence_name,
    'planted' AS occurrence_status,
    exp.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    1 AS rep_count,
    1 AS occurrence_number,
    100 as entry_count,
    100 as plot_count,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name='KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)')
--rollback ;

