--liquibase formatted sql

--changeset postgres:7_populate_entry_maize_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate entry Maize for HTAF Harvest Manager



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, package_id, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT 
    ROW_NUMBER() OVER() AS entry_code,
    ROW_NUMBER() OVER() AS entry_number,
    gg.designation AS entry_name,
    'test' AS entry_type,
    NULL AS entry_class,
    gp.id AS package_id,
    NULL AS entry_role,
    'active' AS entry_status,
    el.id AS entry_list_id,
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    ee.creator_id AS creator_id,
    FALSE AS is_void
FROM 
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
INNER JOIN
    germplasm.package gp
ON
    gp.seed_id = gs.id
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = ee.id
WHERE
    gg.designation
IN
    (
        'DEVOPS-2888-MAIZE_COMP-972',
        'DEVOPS-2888-MAIZE_COMP-4995',
        'DEVOPS-2888-MAIZE_COMP-681',
        'DEVOPS-2888-MAIZE_COMP-4354',
        'DEVOPS-2888-MAIZE_COMP-8118',
        'DEVOPS-2888-MAIZE_COMP-2077',
        'DEVOPS-2888-MAIZE_COMP-874',
        'DEVOPS-2888-MAIZE_COMP-1902',
        'DEVOPS-2888-MAIZE_COMP-4647',
        'DEVOPS-2888-MAIZE_COMP-4472',
        'DEVOPS-2888-MAIZE_COMP-8422',
        'DEVOPS-2888-MAIZE_COMP-1514',
        'DEVOPS-2888-MAIZE_COMP-3849',
        'DEVOPS-2888-MAIZE_COMP-8781',
        'DEVOPS-2888-MAIZE_COMP-1387',
        'DEVOPS-2888-MAIZE_COMP-6593',
        'DEVOPS-2888-MAIZE_COMP-9118',
        'DEVOPS-2888-MAIZE_COMP-5674',
        'DEVOPS-2888-MAIZE_COMP-5394',
        'DEVOPS-2888-MAIZE_COMP-2646',
        'DEVOPS-2888-MAIZE_COMP-9176',
        'DEVOPS-2888-MAIZE_COMP-5813',
        'DEVOPS-2888-MAIZE_COMP-4012',
        'DEVOPS-2888-MAIZE_COMP-7414',
        'DEVOPS-2888-MAIZE_COMP-2595',
        'DEVOPS-2888-MAIZE_LAND-121',
        'DEVOPS-2888-MAIZE_LAND-6991',
        'DEVOPS-2888-MAIZE_LAND-3690',
        'DEVOPS-2888-MAIZE_LAND-3867',
        'DEVOPS-2888-MAIZE_LAND-5776',
        'DEVOPS-2888-MAIZE_LAND-6168',
        'DEVOPS-2888-MAIZE_LAND-9226',
        'DEVOPS-2888-MAIZE_LAND-3481',
        'DEVOPS-2888-MAIZE_LAND-1431',
        'DEVOPS-2888-MAIZE_LAND-6228',
        'DEVOPS-2888-MAIZE_LAND-5773',
        'DEVOPS-2888-MAIZE_LAND-9595',
        'DEVOPS-2888-MAIZE_LAND-5143',
        'DEVOPS-2888-MAIZE_LAND-2887',
        'DEVOPS-2888-MAIZE_LAND-9046',
        'DEVOPS-2888-MAIZE_LAND-5366',
        'DEVOPS-2888-MAIZE_LAND-9496',
        'DEVOPS-2888-MAIZE_LAND-5328',
        'DEVOPS-2888-MAIZE_LAND-2846',
        'DEVOPS-2888-MAIZE_LAND-6620',
        'DEVOPS-2888-MAIZE_LAND-1995',
        'DEVOPS-2888-MAIZE_LAND-8302',
        'DEVOPS-2888-MAIZE_LAND-7837',
        'DEVOPS-2888-MAIZE_LAND-8164',
        'DEVOPS-2888-MAIZE_LAND-9547',
        'DEVOPS-2888-MAIZE_POPU-9628',
        'DEVOPS-2888-MAIZE_POPU-8582',
        'DEVOPS-2888-MAIZE_POPU-9149',
        'DEVOPS-2888-MAIZE_POPU-2084',
        'DEVOPS-2888-MAIZE_POPU-7171',
        'DEVOPS-2888-MAIZE_POPU-6260',
        'DEVOPS-2888-MAIZE_POPU-5423',
        'DEVOPS-2888-MAIZE_POPU-1261',
        'DEVOPS-2888-MAIZE_POPU-462',
        'DEVOPS-2888-MAIZE_POPU-7385',
        'DEVOPS-2888-MAIZE_POPU-6589',
        'DEVOPS-2888-MAIZE_POPU-6935',
        'DEVOPS-2888-MAIZE_POPU-9377',
        'DEVOPS-2888-MAIZE_POPU-2320',
        'DEVOPS-2888-MAIZE_POPU-6559',
        'DEVOPS-2888-MAIZE_POPU-2160',
        'DEVOPS-2888-MAIZE_POPU-7746',
        'DEVOPS-2888-MAIZE_POPU-6036',
        'DEVOPS-2888-MAIZE_POPU-9549',
        'DEVOPS-2888-MAIZE_POPU-2141',
        'DEVOPS-2888-MAIZE_POPU-2',
        'DEVOPS-2888-MAIZE_POPU-9797',
        'DEVOPS-2888-MAIZE_POPU-8983',
        'DEVOPS-2888-MAIZE_POPU-4208',
        'DEVOPS-2888-MAIZE_POPU-4454',
        'DEVOPS-2888-MAIZE_SYNT-4742',
        'DEVOPS-2888-MAIZE_SYNT-1782',
        'DEVOPS-2888-MAIZE_SYNT-3316',
        'DEVOPS-2888-MAIZE_SYNT-8326',
        'DEVOPS-2888-MAIZE_SYNT-8694',
        'DEVOPS-2888-MAIZE_SYNT-5176',
        'DEVOPS-2888-MAIZE_SYNT-2520',
        'DEVOPS-2888-MAIZE_SYNT-5589',
        'DEVOPS-2888-MAIZE_SYNT-6869',
        'DEVOPS-2888-MAIZE_SYNT-7670',
        'DEVOPS-2888-MAIZE_SYNT-6499',
        'DEVOPS-2888-MAIZE_SYNT-2607',
        'DEVOPS-2888-MAIZE_SYNT-6370',
        'DEVOPS-2888-MAIZE_SYNT-6009',
        'DEVOPS-2888-MAIZE_SYNT-5400',
        'DEVOPS-2888-MAIZE_SYNT-4415',
        'DEVOPS-2888-MAIZE_SYNT-7179',
        'DEVOPS-2888-MAIZE_SYNT-253',
        'DEVOPS-2888-MAIZE_SYNT-1049',
        'DEVOPS-2888-MAIZE_SYNT-7603',
        'DEVOPS-2888-MAIZE_SYNT-5588',
        'DEVOPS-2888-MAIZE_SYNT-1469',
        'DEVOPS-2888-MAIZE_SYNT-7230',
        'DEVOPS-2888-MAIZE_SYNT-466',
        'DEVOPS-2888-MAIZE_SYNT-3258'
    );



-- revert changes
--rollback DELETE FROM
--rollback 	experiment.entry AS e
--rollback USING
--rollback 	experiment.entry_list AS el
--rollback INNER JOIN
--rollback 	experiment.experiment AS ee
--rollback ON
--rollback 	el.experiment_id = ee.id
--rollback WHERE
--rollback  	ee.experiment_name='KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
--rollback AND
--rollback 	e.entry_list_id = el.id;