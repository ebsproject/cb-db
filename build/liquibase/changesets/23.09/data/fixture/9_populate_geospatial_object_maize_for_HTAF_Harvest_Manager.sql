--liquibase formatted sql

--changeset postgres:9_populate_geospatial_object_maize_for_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate Geospatial Object Maize for HTAF Harvest Manager



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS geospatial_object_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name,
    'planting area' AS geospatial_object_type,
    'breeding location' AS geospatial_object_subtype,
    exp.creator_id AS creator_id,
    field.id AS parent_geospatial_object_id,
    site.id AS root_geospatial_object_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object go
--rollback WHERE
--rollback geospatial_object_code ilike '%KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)%';