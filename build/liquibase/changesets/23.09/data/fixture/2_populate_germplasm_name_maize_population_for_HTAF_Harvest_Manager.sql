--liquibase formatted sql

--changeset postgres:2_populate_germplasm_name_maize_population_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Populate germplasm_name with newly created germplasm data



INSERT INTO
    germplasm.germplasm_name 
        (
            germplasm_id, 
            name_value, 
            germplasm_name_type, 
            germplasm_name_status, 
            germplasm_normalized_name, 
            creator_id
        )
SELECT 
    gg.id,
    gg.designation,
    gg.germplasm_name_type,
    'standard',
    platform.normalize_text(gg.designation),
    gg.creator_id
FROM
    germplasm.germplasm gg
WHERE
    designation 
 IN (
     'DEVOPS-2888-MAIZE_POPU-9628',
     'DEVOPS-2888-MAIZE_POPU-8582',
     'DEVOPS-2888-MAIZE_POPU-9149',
     'DEVOPS-2888-MAIZE_POPU-2084',
     'DEVOPS-2888-MAIZE_POPU-7171',
     'DEVOPS-2888-MAIZE_POPU-6260',
     'DEVOPS-2888-MAIZE_POPU-5423',
     'DEVOPS-2888-MAIZE_POPU-1261',
     'DEVOPS-2888-MAIZE_POPU-462',
     'DEVOPS-2888-MAIZE_POPU-7385',
     'DEVOPS-2888-MAIZE_POPU-6589',
     'DEVOPS-2888-MAIZE_POPU-6935',
     'DEVOPS-2888-MAIZE_POPU-9377',
     'DEVOPS-2888-MAIZE_POPU-2320',
     'DEVOPS-2888-MAIZE_POPU-6559',
     'DEVOPS-2888-MAIZE_POPU-2160',
     'DEVOPS-2888-MAIZE_POPU-7746',
     'DEVOPS-2888-MAIZE_POPU-6036',
     'DEVOPS-2888-MAIZE_POPU-9549',
     'DEVOPS-2888-MAIZE_POPU-2141',
     'DEVOPS-2888-MAIZE_POPU-2',
     'DEVOPS-2888-MAIZE_POPU-9797',
     'DEVOPS-2888-MAIZE_POPU-8983',
     'DEVOPS-2888-MAIZE_POPU-4208',
     'DEVOPS-2888-MAIZE_POPU-4454'
    )

ORDER BY
    gg.id 
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm_name
--rollback WHERE
--rollback     germplasm_normalized_name 
--rollback IN (
--rollback     'DEVOPS-2888-MAIZE_POPU-9628',
--rollback     'DEVOPS-2888-MAIZE_POPU-8582',
--rollback     'DEVOPS-2888-MAIZE_POPU-9149',
--rollback     'DEVOPS-2888-MAIZE_POPU-2084',
--rollback     'DEVOPS-2888-MAIZE_POPU-7171',
--rollback     'DEVOPS-2888-MAIZE_POPU-6260',
--rollback     'DEVOPS-2888-MAIZE_POPU-5423',
--rollback     'DEVOPS-2888-MAIZE_POPU-1261',
--rollback     'DEVOPS-2888-MAIZE_POPU-462',
--rollback     'DEVOPS-2888-MAIZE_POPU-7385',
--rollback     'DEVOPS-2888-MAIZE_POPU-6589',
--rollback     'DEVOPS-2888-MAIZE_POPU-6935',
--rollback     'DEVOPS-2888-MAIZE_POPU-9377',
--rollback     'DEVOPS-2888-MAIZE_POPU-2320',
--rollback     'DEVOPS-2888-MAIZE_POPU-6559',
--rollback     'DEVOPS-2888-MAIZE_POPU-2160',
--rollback     'DEVOPS-2888-MAIZE_POPU-7746',
--rollback     'DEVOPS-2888-MAIZE_POPU-6036',
--rollback     'DEVOPS-2888-MAIZE_POPU-9549',
--rollback     'DEVOPS-2888-MAIZE_POPU-2141',
--rollback     'DEVOPS-2888-MAIZE_POPU-2',
--rollback     'DEVOPS-2888-MAIZE_POPU-9797',
--rollback     'DEVOPS-2888-MAIZE_POPU-8983',
--rollback     'DEVOPS-2888-MAIZE_POPU-4208',
--rollback     'DEVOPS-2888-MAIZE_POPU-4454'
--rollback     )
--rollback       
--rollback ;