--liquibase formatted sql

--changeset postgres:4_populate_package_maize_population_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id)
SELECT 	
    germplasm.generate_code('package') AS package_code,
    gs.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    gs.id AS seed_id,
    gs.program_id,
    gs.creator_id
FROM
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
WHERE 
    gg.designation
IN
    (
        'DEVOPS-2888-MAIZE_POPU-9628',
        'DEVOPS-2888-MAIZE_POPU-8582',
        'DEVOPS-2888-MAIZE_POPU-9149',
        'DEVOPS-2888-MAIZE_POPU-2084',
        'DEVOPS-2888-MAIZE_POPU-7171',
        'DEVOPS-2888-MAIZE_POPU-6260',
        'DEVOPS-2888-MAIZE_POPU-5423',
        'DEVOPS-2888-MAIZE_POPU-1261',
        'DEVOPS-2888-MAIZE_POPU-462',
        'DEVOPS-2888-MAIZE_POPU-7385',
        'DEVOPS-2888-MAIZE_POPU-6589',
        'DEVOPS-2888-MAIZE_POPU-6935',
        'DEVOPS-2888-MAIZE_POPU-9377',
        'DEVOPS-2888-MAIZE_POPU-2320',
        'DEVOPS-2888-MAIZE_POPU-6559',
        'DEVOPS-2888-MAIZE_POPU-2160',
        'DEVOPS-2888-MAIZE_POPU-7746',
        'DEVOPS-2888-MAIZE_POPU-6036',
        'DEVOPS-2888-MAIZE_POPU-9549',
        'DEVOPS-2888-MAIZE_POPU-2141',
        'DEVOPS-2888-MAIZE_POPU-2',
        'DEVOPS-2888-MAIZE_POPU-9797',
        'DEVOPS-2888-MAIZE_POPU-8983',
        'DEVOPS-2888-MAIZE_POPU-4208',
        'DEVOPS-2888-MAIZE_POPU-4454'
    )
ORDER BY
    gs.id
;

    

--rollback DELETE FROM
--rollback 	germplasm.package
--rollback WHERE package_label ilike 'DEVOPS-2888-MAIZE_POPU%'