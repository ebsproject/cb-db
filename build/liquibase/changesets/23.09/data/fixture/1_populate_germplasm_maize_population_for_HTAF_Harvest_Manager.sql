--liquibase formatted sql

--changeset postgres:1_populate_germplasm_maize_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Add new germplasms with newly created scale value "population"



INSERT INTO
    germplasm.germplasm 
        (
            designation,
            parentage,
            generation,
            germplasm_type,
            germplasm_state,
            germplasm_name_type,
            germplasm_normalized_name,
            crop_id,
            taxonomy_id,
            creator_id
        )
SELECT
    g.designation,
    g.parentage,
    g.generation,
    g.germplasm_type,
    g.germplasm_state,
    g.germplasm_name_type,
    platform.normalize_text(g.designation),
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxonomy_name = 'Zea mays L. subsp. mays'
            AND taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
            ('DEVOPS-2888-MAIZE_POPU-9628','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-8582','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-9149','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-2084','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-7171','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-6260','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-5423','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-1261','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-462','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-7385','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-6589','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-6935','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-9377','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-2320','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-6559','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-2160','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-7746','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-6036','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-9549','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-2141','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-2','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-9797','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-8983','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-4208','?/?','F5','population','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_POPU-4454','?/?','F5','population','fixed','selection_history')
        ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type
    )
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm
--rollback WHERE 
--rollback     designation 
--rollback IN 
--rollback     (
--rollback     'DEVOPS-2888-MAIZE_POPU-9628',
--rollback     'DEVOPS-2888-MAIZE_POPU-8582',
--rollback     'DEVOPS-2888-MAIZE_POPU-9149',
--rollback     'DEVOPS-2888-MAIZE_POPU-2084',
--rollback     'DEVOPS-2888-MAIZE_POPU-7171',
--rollback     'DEVOPS-2888-MAIZE_POPU-6260',
--rollback     'DEVOPS-2888-MAIZE_POPU-5423',
--rollback     'DEVOPS-2888-MAIZE_POPU-1261',
--rollback     'DEVOPS-2888-MAIZE_POPU-462',
--rollback     'DEVOPS-2888-MAIZE_POPU-7385',
--rollback     'DEVOPS-2888-MAIZE_POPU-6589',
--rollback     'DEVOPS-2888-MAIZE_POPU-6935',
--rollback     'DEVOPS-2888-MAIZE_POPU-9377',
--rollback     'DEVOPS-2888-MAIZE_POPU-2320',
--rollback     'DEVOPS-2888-MAIZE_POPU-6559',
--rollback     'DEVOPS-2888-MAIZE_POPU-2160',
--rollback     'DEVOPS-2888-MAIZE_POPU-7746',
--rollback     'DEVOPS-2888-MAIZE_POPU-6036',
--rollback     'DEVOPS-2888-MAIZE_POPU-9549',
--rollback     'DEVOPS-2888-MAIZE_POPU-2141',
--rollback     'DEVOPS-2888-MAIZE_POPU-2',
--rollback     'DEVOPS-2888-MAIZE_POPU-9797',
--rollback     'DEVOPS-2888-MAIZE_POPU-8983',
--rollback     'DEVOPS-2888-MAIZE_POPU-4208',
--rollback     'DEVOPS-2888-MAIZE_POPU-4454'
--rollback     )