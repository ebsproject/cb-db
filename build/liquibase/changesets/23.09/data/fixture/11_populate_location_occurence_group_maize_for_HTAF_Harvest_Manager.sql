--liquibase formatted sql

--changeset postgres:11_populate_location_occurence_group_maize_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate Location Occurrence Group Maize for HTAF Harvest Manager



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment exp
INNER JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    experiment.occurrence occ
ON
    occ.experiment_id = exp.id
INNER JOIN
    experiment.location loc
ON
    loc.location_code ilike '%KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)%'
WHERE
    exp.experiment_name = 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
ORDER BY
    occ.id
;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     occurrence_id 
--rollback IN 
--rollback     (
--rollback      SELECT
--rollback          occ.id AS occurrence_id
--rollback      FROM
--rollback          experiment.experiment exp
--rollback      INNER JOIN
--rollback          tenant.season tss
--rollback      ON
--rollback          tss.id = exp.season_id
--rollback      INNER JOIN
--rollback          tenant.stage ts
--rollback      ON
--rollback          ts.id = exp.stage_id
--rollback      INNER JOIN
--rollback          tenant.program tp
--rollback      ON
--rollback          exp.program_id = tp.id
--rollback      INNER JOIN
--rollback          experiment.occurrence occ
--rollback      ON
--rollback          occ.experiment_id = exp.id
--rollback      INNER JOIN
--rollback          experiment.location loc
--rollback      ON
--rollback          loc.location_code ilike '%KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)%'
--rollback      WHERE
--rollback          exp.experiment_name = 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
--rollback      ORDER BY
--rollback          occ.id
--rollback     )
--rollback ;
