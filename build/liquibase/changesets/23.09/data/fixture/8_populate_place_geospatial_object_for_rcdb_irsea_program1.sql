--liquibase formatted sql

--changeset postgres:8_populate_place_geospatial_object_for_rcdb_irsea_program1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    place.geospatial_object
        (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
SELECT
    geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates::polygon,altitude::float,description,creator_id
FROM
    (
        VALUES
            ('IRRIHQ-2023-WS-005','IRRIHQ-2023-WS-005','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin')))
     t (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
;

UPDATE place.geospatial_object SET parent_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ') WHERE geospatial_object_code='IRRIHQ-2023-WS-005';
UPDATE place.geospatial_object SET root_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH') WHERE geospatial_object_code='IRRIHQ-2023-WS-005';



--rollback DELETE FROM
--rollback    place.geospatial_object
--rollback WHERE
--rollback    geospatial_object_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2023-WS-005'
--rollback    )
--rollback ;