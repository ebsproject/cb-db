--liquibase formatted sql

--changeset postgres:6_populate_entry_list_maize_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate entry list Maize for HTAF Harvest Manager



INSERT INTO 
    experiment.entry_list
        (
            entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,
            entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id
        )
SELECT
    entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,
    entry_list_type,cross_count,experiment_year::int,season_id::int,site_id::int,crop_id,program_id
FROM
    (
        VALUES
            (
                (experiment.generate_code('entry_list')),
                'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY) Entry List',
                NULL,
                'completed',
                (SELECT id FROM experiment.experiment WHERE experiment_name='KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'),
                (SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),
                'entry list',
                0,
                NULL,
                NULL,
                NULL,
                (SELECT id FROM tenant.crop WHERE crop_code='MAIZE'),
                (SELECT id FROM tenant.program WHERE program_code='KE')
            )
    )
     t (entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
;



--rollback DELETE FROM
--rollback    experiment.entry_list
--rollback WHERE
--rollback    entry_list_name
--rollback IN
--rollback    (
--rollback         'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY) Entry List'
--rollback    )
--rollback ;