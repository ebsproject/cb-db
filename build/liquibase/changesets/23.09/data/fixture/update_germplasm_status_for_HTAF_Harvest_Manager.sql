--liquibase formatted sql

--changeset postgres:update_germplasm_status_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 update germplasm state to 'not_fixed' to newly added germplasm



update germplasm.germplasm
SET 
germplasm_state = 'not_fixed'
WHERE designation ilike 'DEVOPS-2888-MAIZE_%'



-- revert changes
--rollback update germplasm.germplasm
--rollback SET 
--rollback germplasm_state = 'fixed'
--rollback WHERE designation ilike 'DEVOPS-2888-MAIZE_%'