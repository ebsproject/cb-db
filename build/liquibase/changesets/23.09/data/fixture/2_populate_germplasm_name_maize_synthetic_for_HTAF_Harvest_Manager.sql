--liquibase formatted sql

--changeset postgres:2_populate_germplasm_name_maize_synthetic_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Populate germplasm_name with newly created germplasm data



INSERT INTO
    germplasm.germplasm_name 
        (
            germplasm_id, 
            name_value, 
            germplasm_name_type, 
            germplasm_name_status, 
            germplasm_normalized_name, 
            creator_id
        )
SELECT 
    gg.id,
    gg.designation,
    gg.germplasm_name_type,
    'standard',
    platform.normalize_text(gg.designation),
    gg.creator_id
FROM
    germplasm.germplasm gg
WHERE
    designation 
 IN (
     'DEVOPS-2888-MAIZE_SYNT-4742',
     'DEVOPS-2888-MAIZE_SYNT-1782',
     'DEVOPS-2888-MAIZE_SYNT-3316',
     'DEVOPS-2888-MAIZE_SYNT-8326',
     'DEVOPS-2888-MAIZE_SYNT-8694',
     'DEVOPS-2888-MAIZE_SYNT-5176',
     'DEVOPS-2888-MAIZE_SYNT-2520',
     'DEVOPS-2888-MAIZE_SYNT-5589',
     'DEVOPS-2888-MAIZE_SYNT-6869',
     'DEVOPS-2888-MAIZE_SYNT-7670',
     'DEVOPS-2888-MAIZE_SYNT-6499',
     'DEVOPS-2888-MAIZE_SYNT-2607',
     'DEVOPS-2888-MAIZE_SYNT-6370',
     'DEVOPS-2888-MAIZE_SYNT-6009',
     'DEVOPS-2888-MAIZE_SYNT-5400',
     'DEVOPS-2888-MAIZE_SYNT-4415',
     'DEVOPS-2888-MAIZE_SYNT-7179',
     'DEVOPS-2888-MAIZE_SYNT-253',
     'DEVOPS-2888-MAIZE_SYNT-1049',
     'DEVOPS-2888-MAIZE_SYNT-7603',
     'DEVOPS-2888-MAIZE_SYNT-5588',
     'DEVOPS-2888-MAIZE_SYNT-1469',
     'DEVOPS-2888-MAIZE_SYNT-7230',
     'DEVOPS-2888-MAIZE_SYNT-466',
     'DEVOPS-2888-MAIZE_SYNT-3258'
    )

ORDER BY
    gg.id 
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm_name
--rollback WHERE
--rollback     germplasm_normalized_name 
--rollback IN (
--rollback     'DEVOPS-2888-MAIZE_SYNT-4742',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1782',
--rollback     'DEVOPS-2888-MAIZE_SYNT-3316',
--rollback     'DEVOPS-2888-MAIZE_SYNT-8326',
--rollback     'DEVOPS-2888-MAIZE_SYNT-8694',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5176',
--rollback     'DEVOPS-2888-MAIZE_SYNT-2520',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5589',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6869',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7670',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6499',
--rollback     'DEVOPS-2888-MAIZE_SYNT-2607',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6370',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6009',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5400',
--rollback     'DEVOPS-2888-MAIZE_SYNT-4415',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7179',
--rollback     'DEVOPS-2888-MAIZE_SYNT-253',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1049',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7603',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5588',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1469',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7230',
--rollback     'DEVOPS-2888-MAIZE_SYNT-466',
--rollback     'DEVOPS-2888-MAIZE_SYNT-3258'
--rollback     )
--rollback       
--rollback ;