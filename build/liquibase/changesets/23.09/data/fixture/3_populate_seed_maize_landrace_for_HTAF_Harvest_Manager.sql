--liquibase formatted sql


--changeset postgres:3_populate_seed_maize_landrace_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO 
    germplasm.seed 
        (
            seed_code, 
            seed_name, 
            germplasm_id,
            program_id,
            creator_id
        )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name,
    g.id AS germplasm_id,
    tp.id program_id,
    g.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
        VALUES
            ('DEVOPS-2888-MAIZE_LAND-121'),
            ('DEVOPS-2888-MAIZE_LAND-6991'),
            ('DEVOPS-2888-MAIZE_LAND-3690'),
            ('DEVOPS-2888-MAIZE_LAND-3867'),
            ('DEVOPS-2888-MAIZE_LAND-5776'),
            ('DEVOPS-2888-MAIZE_LAND-6168'),
            ('DEVOPS-2888-MAIZE_LAND-9226'),
            ('DEVOPS-2888-MAIZE_LAND-3481'),
            ('DEVOPS-2888-MAIZE_LAND-1431'),
            ('DEVOPS-2888-MAIZE_LAND-6228'),
            ('DEVOPS-2888-MAIZE_LAND-5773'),
            ('DEVOPS-2888-MAIZE_LAND-9595'),
            ('DEVOPS-2888-MAIZE_LAND-5143'),
            ('DEVOPS-2888-MAIZE_LAND-2887'),
            ('DEVOPS-2888-MAIZE_LAND-9046'),
            ('DEVOPS-2888-MAIZE_LAND-5366'),
            ('DEVOPS-2888-MAIZE_LAND-9496'),
            ('DEVOPS-2888-MAIZE_LAND-5328'),
            ('DEVOPS-2888-MAIZE_LAND-2846'),
            ('DEVOPS-2888-MAIZE_LAND-6620'),
            ('DEVOPS-2888-MAIZE_LAND-1995'),
            ('DEVOPS-2888-MAIZE_LAND-8302'),
            ('DEVOPS-2888-MAIZE_LAND-7837'),
            ('DEVOPS-2888-MAIZE_LAND-8164'),
            ('DEVOPS-2888-MAIZE_LAND-9547')
            ) AS t (
        designation
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'KE'
;



--rollback DELETE FROM
--rollback 	germplasm.seed
--rollback WHERE seed_name ilike 'DEVOPS-2888-MAIZE_LAND%'