--liquibase formatted sql

--changeset postgres:1_populate_germplasm_maize_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Add new germplasms with newly created scale value "composite"



INSERT INTO
    germplasm.germplasm 
        (
            designation,
            parentage,
            generation,
            germplasm_type,
            germplasm_state,
            germplasm_name_type,
            germplasm_normalized_name,
            crop_id,
            taxonomy_id,
            creator_id
        )
SELECT
    g.designation,
    g.parentage,
    g.generation,
    g.germplasm_type,
    g.germplasm_state,
    g.germplasm_name_type,
    platform.normalize_text(g.designation),
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxonomy_name = 'Zea mays L. subsp. mays'
            AND taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
            ('DEVOPS-2888-MAIZE_COMP-972','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-4995','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-681','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-4354','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-8118','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-2077','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-874','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-1902','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-4647','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-4472','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-8422','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-1514','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-3849','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-8781','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-1387','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-6593','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-9118','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-5674','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-5394','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-2646','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-9176','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-5813','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-4012','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-7414','?/?','F5','composite','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_COMP-2595','?/?','F5','composite','fixed','selection_history')
        ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type
    )
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm
--rollback WHERE 
--rollback     designation 
--rollback IN 
--rollback     (
--rollback     'DEVOPS-2888-MAIZE_COMP-972',
--rollback     'DEVOPS-2888-MAIZE_COMP-4995',
--rollback     'DEVOPS-2888-MAIZE_COMP-681',
--rollback     'DEVOPS-2888-MAIZE_COMP-4354',
--rollback     'DEVOPS-2888-MAIZE_COMP-8118',
--rollback     'DEVOPS-2888-MAIZE_COMP-2077',
--rollback     'DEVOPS-2888-MAIZE_COMP-874',
--rollback     'DEVOPS-2888-MAIZE_COMP-1902',
--rollback     'DEVOPS-2888-MAIZE_COMP-4647',
--rollback     'DEVOPS-2888-MAIZE_COMP-4472',
--rollback     'DEVOPS-2888-MAIZE_COMP-8422',
--rollback     'DEVOPS-2888-MAIZE_COMP-1514',
--rollback     'DEVOPS-2888-MAIZE_COMP-3849',
--rollback     'DEVOPS-2888-MAIZE_COMP-8781',
--rollback     'DEVOPS-2888-MAIZE_COMP-1387',
--rollback     'DEVOPS-2888-MAIZE_COMP-6593',
--rollback     'DEVOPS-2888-MAIZE_COMP-9118',
--rollback     'DEVOPS-2888-MAIZE_COMP-5674',
--rollback     'DEVOPS-2888-MAIZE_COMP-5394',
--rollback     'DEVOPS-2888-MAIZE_COMP-2646',
--rollback     'DEVOPS-2888-MAIZE_COMP-9176',
--rollback     'DEVOPS-2888-MAIZE_COMP-5813',
--rollback     'DEVOPS-2888-MAIZE_COMP-4012',
--rollback     'DEVOPS-2888-MAIZE_COMP-7414',
--rollback     'DEVOPS-2888-MAIZE_COMP-2595'
--rollback    )