--liquibase formatted sql

--changeset postgres:4_populate_experiment_populate_occurence_for_rcdb_irsea_program2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    experiment.occurrence
        (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
SELECT
    occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id::int,creator_id,rep_count,site_id,field_id::int,occurrence_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('EXP0047919-002','BA2023_AugRCBD_T206-002','planted;trait data collected;trait data quality checked',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'),NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH_II_AU'),NULL,2,NULL,206,248))
     t (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.occurrence
--rollback WHERE
--rollback    occurrence_code
--rollback IN
--rollback    (
--rollback        'EXP0047919-002'
--rollback    )
--rollback ;