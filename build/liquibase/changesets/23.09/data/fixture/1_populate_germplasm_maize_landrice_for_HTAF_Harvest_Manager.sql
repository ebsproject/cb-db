--liquibase formatted sql

--changeset postgres:1_populate_germplasm_maize_landrice_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Add new germplasms with newly created scale value "landrace"



INSERT INTO
    germplasm.germplasm 
        (
            designation,
            parentage,
            generation,
            germplasm_type,
            germplasm_state,
            germplasm_name_type,
            germplasm_normalized_name,
            crop_id,
            taxonomy_id,
            creator_id
        )
SELECT
    g.designation,
    g.parentage,
    g.generation,
    g.germplasm_type,
    g.germplasm_state,
    g.germplasm_name_type,
    platform.normalize_text(g.designation),
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxonomy_name = 'Zea mays L. subsp. mays'
            AND taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
            ('DEVOPS-2888-MAIZE_LAND-121','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-6991','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-3690','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-3867','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-5776','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-6168','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-9226','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-3481','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-1431','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-6228','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-5773','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-9595','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-5143','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-2887','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-9046','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-5366','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-9496','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-5328','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-2846','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-6620','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-1995','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-8302','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-7837','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-8164','?/?','F5','landrace','fixed','selection_history'),
            ('DEVOPS-2888-MAIZE_LAND-9547','?/?','F5','landrace','fixed','selection_history')
        ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type
    )
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm
--rollback WHERE 
--rollback     designation 
--rollback IN 
--rollback     (
--rollback    'DEVOPS-2888-MAIZE_LAND-121',
--rollback    'DEVOPS-2888-MAIZE_LAND-6991',
--rollback    'DEVOPS-2888-MAIZE_LAND-3690',
--rollback    'DEVOPS-2888-MAIZE_LAND-3867',
--rollback    'DEVOPS-2888-MAIZE_LAND-5776',
--rollback    'DEVOPS-2888-MAIZE_LAND-6168',
--rollback    'DEVOPS-2888-MAIZE_LAND-9226',
--rollback    'DEVOPS-2888-MAIZE_LAND-3481',
--rollback    'DEVOPS-2888-MAIZE_LAND-1431',
--rollback    'DEVOPS-2888-MAIZE_LAND-6228',
--rollback    'DEVOPS-2888-MAIZE_LAND-5773',
--rollback    'DEVOPS-2888-MAIZE_LAND-9595',
--rollback    'DEVOPS-2888-MAIZE_LAND-5143',
--rollback    'DEVOPS-2888-MAIZE_LAND-2887',
--rollback    'DEVOPS-2888-MAIZE_LAND-9046',
--rollback    'DEVOPS-2888-MAIZE_LAND-5366',
--rollback    'DEVOPS-2888-MAIZE_LAND-9496',
--rollback    'DEVOPS-2888-MAIZE_LAND-5328',
--rollback    'DEVOPS-2888-MAIZE_LAND-2846',
--rollback    'DEVOPS-2888-MAIZE_LAND-6620',
--rollback    'DEVOPS-2888-MAIZE_LAND-1995',
--rollback    'DEVOPS-2888-MAIZE_LAND-8302',
--rollback    'DEVOPS-2888-MAIZE_LAND-7837',
--rollback    'DEVOPS-2888-MAIZE_LAND-8164',
--rollback    'DEVOPS-2888-MAIZE_LAND-9547'
--rollback    )