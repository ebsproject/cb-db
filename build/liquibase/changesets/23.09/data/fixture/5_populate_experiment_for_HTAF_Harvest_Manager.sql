--liquibase formatted sql

--changeset postgres:5_populate_experiment_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2889 Populate experiment Maize for HTAF Harvest Manager



INSERT INTO 
    experiment.experiment
        (
            program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,
            planting_season,experiment_code,experiment_name,experiment_objective,
            experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,
            experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks
        )
SELECT
    program_id,pipeline_id::int,stage_id,project_id::int,experiment_year,season_id,planting_season,experiment_code,
    experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,
    experiment_status,description,steward_id,experiment_plan_id::int,creator_id,data_process_id,crop_id,remarks
FROM
    (
        VALUES
            (
                (SELECT id FROM tenant.program WHERE program_code='KE'),
                NULL,
                (SELECT id FROM tenant.stage WHERE stage_code='AYT'),
                NULL,
                2023,
                (SELECT id FROM tenant.season WHERE season_code='DS'),
                NULL,
                (experiment.generate_code('experiment')),
                'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)',
                NULL,
                'Generation Nursery',
                NULL,
                NULL,
                'Alpha-Lattice',
                'created',
                NULL,
                (SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),
                NULL,
                (SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),
                (SELECT id FROM master.item WHERE abbrev='GENERATION_NURSERY_DATA_PROCESS'),
                (SELECT id FROM tenant.crop WHERE crop_name='Maize'),
                NULL
            )
    )
     t (program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,experiment_plan_id,creator_id,data_process_id,crop_id,remarks)
;



--rollback DELETE FROM
--rollback    experiment.experiment
--rollback WHERE
--rollback    experiment_name
--rollback IN
--rollback    (
--rollback     	 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)'
--rollback    )
--rollback ;