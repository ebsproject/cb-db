--liquibase formatted sql

--changeset postgres:4_populate_package_maize_landrace_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id)
SELECT 	
    germplasm.generate_code('package') AS package_code,
    gs.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    gs.id AS seed_id,
    gs.program_id,
    gs.creator_id
FROM
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
WHERE 
    gg.designation
IN
    (
        'DEVOPS-2888-MAIZE_LAND-121',
        'DEVOPS-2888-MAIZE_LAND-6991',
        'DEVOPS-2888-MAIZE_LAND-3690',
        'DEVOPS-2888-MAIZE_LAND-3867',
        'DEVOPS-2888-MAIZE_LAND-5776',
        'DEVOPS-2888-MAIZE_LAND-6168',
        'DEVOPS-2888-MAIZE_LAND-9226',
        'DEVOPS-2888-MAIZE_LAND-3481',
        'DEVOPS-2888-MAIZE_LAND-1431',
        'DEVOPS-2888-MAIZE_LAND-6228',
        'DEVOPS-2888-MAIZE_LAND-5773',
        'DEVOPS-2888-MAIZE_LAND-9595',
        'DEVOPS-2888-MAIZE_LAND-5143',
        'DEVOPS-2888-MAIZE_LAND-2887',
        'DEVOPS-2888-MAIZE_LAND-9046',
        'DEVOPS-2888-MAIZE_LAND-5366',
        'DEVOPS-2888-MAIZE_LAND-9496',
        'DEVOPS-2888-MAIZE_LAND-5328',
        'DEVOPS-2888-MAIZE_LAND-2846',
        'DEVOPS-2888-MAIZE_LAND-6620',
        'DEVOPS-2888-MAIZE_LAND-1995',
        'DEVOPS-2888-MAIZE_LAND-8302',
        'DEVOPS-2888-MAIZE_LAND-7837',
        'DEVOPS-2888-MAIZE_LAND-8164',
        'DEVOPS-2888-MAIZE_LAND-9547'
    )
ORDER BY
    gs.id
;

    

--rollback DELETE FROM
--rollback 	germplasm.package
--rollback WHERE package_label ilike 'DEVOPS-2888-MAIZE_LAND%'