--liquibase formatted sql


--changeset postgres:3_populate_seed_maize_population_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO 
    germplasm.seed 
        (
            seed_code, 
            seed_name, 
            germplasm_id,
            program_id,
            creator_id
        )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    concat(t.designation,'-',g.id,'-', ROW_NUMBER() OVER ()) AS seed_name,
    g.id AS germplasm_id,
    tp.id program_id,
    g.creator_id
FROM
    germplasm.germplasm AS g
    INNER JOIN (
        VALUES
            ('DEVOPS-2888-MAIZE_POPU-9628'),
            ('DEVOPS-2888-MAIZE_POPU-8582'),
            ('DEVOPS-2888-MAIZE_POPU-9149'),
            ('DEVOPS-2888-MAIZE_POPU-2084'),
            ('DEVOPS-2888-MAIZE_POPU-7171'),
            ('DEVOPS-2888-MAIZE_POPU-6260'),
            ('DEVOPS-2888-MAIZE_POPU-5423'),
            ('DEVOPS-2888-MAIZE_POPU-1261'),
            ('DEVOPS-2888-MAIZE_POPU-462'),
            ('DEVOPS-2888-MAIZE_POPU-7385'),
            ('DEVOPS-2888-MAIZE_POPU-6589'),
            ('DEVOPS-2888-MAIZE_POPU-6935'),
            ('DEVOPS-2888-MAIZE_POPU-9377'),
            ('DEVOPS-2888-MAIZE_POPU-2320'),
            ('DEVOPS-2888-MAIZE_POPU-6559'),
            ('DEVOPS-2888-MAIZE_POPU-2160'),
            ('DEVOPS-2888-MAIZE_POPU-7746'),
            ('DEVOPS-2888-MAIZE_POPU-6036'),
            ('DEVOPS-2888-MAIZE_POPU-9549'),
            ('DEVOPS-2888-MAIZE_POPU-2141'),
            ('DEVOPS-2888-MAIZE_POPU-2'),
            ('DEVOPS-2888-MAIZE_POPU-9797'),
            ('DEVOPS-2888-MAIZE_POPU-8983'),
            ('DEVOPS-2888-MAIZE_POPU-4208'),
            ('DEVOPS-2888-MAIZE_POPU-4454')
            ) AS t (
        designation
    )
        ON g.germplasm_normalized_name = t.designation
    INNER JOIN
        tenant.program tp
    ON
        tp.program_code = 'KE'
;



--rollback DELETE FROM
--rollback 	germplasm.seed
--rollback WHERE seed_name ilike 'DEVOPS-2888-MAIZE_POPU%'