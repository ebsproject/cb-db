--liquibase formatted sql

--changeset postgres:10_populate_tenant_protocol_for_rcdb_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0047919','Trait Protocol EXP0047919','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('PLANTING_PROTOCOL_EXP0047919','Planting Protocol EXP0047919','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('HARVEST_PROTOCOL_EXP0047919','Harvest Protocol EXP0047919','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0047919','Management Protocol EXP0047919','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0047919',
--rollback        'PLANTING_PROTOCOL_EXP0047919',
--rollback        'HARVEST_PROTOCOL_EXP0047919',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047919'
--rollback    )
--rollback ;