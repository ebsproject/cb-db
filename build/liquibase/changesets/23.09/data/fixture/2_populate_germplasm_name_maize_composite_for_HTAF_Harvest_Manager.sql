--liquibase formatted sql

--changeset postgres:2_populate_germplasm_name_maize_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Populate germplasm_name with newly created germplasm data



INSERT INTO
    germplasm.germplasm_name 
        (
            germplasm_id, 
            name_value, 
            germplasm_name_type, 
            germplasm_name_status, 
            germplasm_normalized_name, 
            creator_id
        )
SELECT 
    gg.id,
    gg.designation,
    gg.germplasm_name_type,
    'standard',
    platform.normalize_text(gg.designation),
    gg.creator_id
FROM
    germplasm.germplasm gg
WHERE
    designation 
 IN (
     'DEVOPS-2888-MAIZE_COMP-2595',
     'DEVOPS-2888-MAIZE_COMP-7414',
     'DEVOPS-2888-MAIZE_COMP-4012',
     'DEVOPS-2888-MAIZE_COMP-5813',
     'DEVOPS-2888-MAIZE_COMP-9176',
     'DEVOPS-2888-MAIZE_COMP-2646',
     'DEVOPS-2888-MAIZE_COMP-5394',
     'DEVOPS-2888-MAIZE_COMP-5674',
     'DEVOPS-2888-MAIZE_COMP-9118',
     'DEVOPS-2888-MAIZE_COMP-6593',
     'DEVOPS-2888-MAIZE_COMP-1387',
     'DEVOPS-2888-MAIZE_COMP-8781',
     'DEVOPS-2888-MAIZE_COMP-3849',
     'DEVOPS-2888-MAIZE_COMP-1514',
     'DEVOPS-2888-MAIZE_COMP-8422',
     'DEVOPS-2888-MAIZE_COMP-4472',
     'DEVOPS-2888-MAIZE_COMP-4647',
     'DEVOPS-2888-MAIZE_COMP-1902',
     'DEVOPS-2888-MAIZE_COMP-874',
     'DEVOPS-2888-MAIZE_COMP-2077',
     'DEVOPS-2888-MAIZE_COMP-8118',
     'DEVOPS-2888-MAIZE_COMP-4354',
     'DEVOPS-2888-MAIZE_COMP-681',
     'DEVOPS-2888-MAIZE_COMP-4995',
     'DEVOPS-2888-MAIZE_COMP-972'
    )

ORDER BY
    gg.id 
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm_name
--rollback WHERE
--rollback     germplasm_normalized_name 
--rollback IN (
--rollback     'DEVOPS-2888-MAIZE_COMP-2595',
--rollback     'DEVOPS-2888-MAIZE_COMP-7414',
--rollback     'DEVOPS-2888-MAIZE_COMP-4012',
--rollback     'DEVOPS-2888-MAIZE_COMP-5813',
--rollback     'DEVOPS-2888-MAIZE_COMP-9176',
--rollback     'DEVOPS-2888-MAIZE_COMP-2646',
--rollback     'DEVOPS-2888-MAIZE_COMP-5394',
--rollback     'DEVOPS-2888-MAIZE_COMP-5674',
--rollback     'DEVOPS-2888-MAIZE_COMP-9118',
--rollback     'DEVOPS-2888-MAIZE_COMP-6593',
--rollback     'DEVOPS-2888-MAIZE_COMP-1387',
--rollback     'DEVOPS-2888-MAIZE_COMP-8781',
--rollback     'DEVOPS-2888-MAIZE_COMP-3849',
--rollback     'DEVOPS-2888-MAIZE_COMP-1514',
--rollback     'DEVOPS-2888-MAIZE_COMP-8422',
--rollback     'DEVOPS-2888-MAIZE_COMP-4472',
--rollback     'DEVOPS-2888-MAIZE_COMP-4647',
--rollback     'DEVOPS-2888-MAIZE_COMP-1902',
--rollback     'DEVOPS-2888-MAIZE_COMP-874',
--rollback     'DEVOPS-2888-MAIZE_COMP-2077',
--rollback     'DEVOPS-2888-MAIZE_COMP-8118',
--rollback     'DEVOPS-2888-MAIZE_COMP-4354',
--rollback     'DEVOPS-2888-MAIZE_COMP-681',
--rollback     'DEVOPS-2888-MAIZE_COMP-4995',
--rollback     'DEVOPS-2888-MAIZE_COMP-972'
--rollback     )
--rollback       
--rollback ;