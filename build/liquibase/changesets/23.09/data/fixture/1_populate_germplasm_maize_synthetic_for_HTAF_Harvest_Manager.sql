--liquibase formatted sql

--changeset postgres:1_populate_germplasm_synthetic_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2888 Add new germplasms with newly created scale value "synthetic"



INSERT INTO
    germplasm.germplasm 
        (
            designation,
            parentage,
            generation,
            germplasm_type,
            germplasm_state,
            germplasm_name_type,
            germplasm_normalized_name,
            crop_id,
            taxonomy_id,
            creator_id
        )
SELECT
    g.designation,
    g.parentage,
    g.generation,
    g.germplasm_type,
    g.germplasm_state,
    g.germplasm_name_type,
    platform.normalize_text(g.designation),
    c.crop_id,
    t.taxonomy_id,
    '1' AS creator_id
FROM
    (
        SELECT
            id
        FROM
            tenant.crop
        WHERE
            crop_code = 'MAIZE'
    ) AS c (
        crop_id
    ), (
        SELECT
            id
        FROM
            germplasm.taxonomy
        WHERE
            taxonomy_name = 'Zea mays L. subsp. mays'
            AND taxon_id = '4570'
    ) AS t (
        taxonomy_id
    ), (
        VALUES
    ('DEVOPS-2888-MAIZE_SYNT-4742','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-1782','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-3316','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-8326','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-8694','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-5176','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-2520','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-5589','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-6869','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-7670','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-6499','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-2607','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-6370','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-6009','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-5400','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-4415','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-7179','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-253','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-1049','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-7603','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-5588','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-1469','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-7230','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-466','?/?','F5','synthetic','fixed','selection_history'),
    ('DEVOPS-2888-MAIZE_SYNT-3258','?/?','F5','synthetic','fixed','selection_history')
        ) AS g (
        designation,parentage,generation,germplasm_type,germplasm_state,germplasm_name_type
    )
;



--rollback DELETE FROM 
--rollback     germplasm.germplasm
--rollback WHERE 
--rollback     designation 
--rollback IN 
--rollback     (
--rollback     'DEVOPS-2888-MAIZE_SYNT-4742',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1782',
--rollback     'DEVOPS-2888-MAIZE_SYNT-3316',
--rollback     'DEVOPS-2888-MAIZE_SYNT-8326',
--rollback     'DEVOPS-2888-MAIZE_SYNT-8694',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5176',
--rollback     'DEVOPS-2888-MAIZE_SYNT-2520',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5589',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6869',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7670',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6499',
--rollback     'DEVOPS-2888-MAIZE_SYNT-2607',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6370',
--rollback     'DEVOPS-2888-MAIZE_SYNT-6009',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5400',
--rollback     'DEVOPS-2888-MAIZE_SYNT-4415',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7179',
--rollback     'DEVOPS-2888-MAIZE_SYNT-253',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1049',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7603',
--rollback     'DEVOPS-2888-MAIZE_SYNT-5588',
--rollback     'DEVOPS-2888-MAIZE_SYNT-1469',
--rollback     'DEVOPS-2888-MAIZE_SYNT-7230',
--rollback     'DEVOPS-2888-MAIZE_SYNT-466',
--rollback     'DEVOPS-2888-MAIZE_SYNT-3258'
--rollback     )