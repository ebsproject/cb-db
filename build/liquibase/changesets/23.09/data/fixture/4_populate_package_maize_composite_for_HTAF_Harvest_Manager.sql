--liquibase formatted sql

--changeset postgres:4_populate_package_maize_composite_for_HTAF_Harvest_Manager context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2890 Populate seed with newly created germplasm



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id)
SELECT 	
    germplasm.generate_code('package') AS package_code,
    gs.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    gs.id AS seed_id,
    gs.program_id,
    gs.creator_id
FROM
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
WHERE 
    gg.designation
IN
    (
        'DEVOPS-2888-MAIZE_COMP-972',
        'DEVOPS-2888-MAIZE_COMP-4995',
        'DEVOPS-2888-MAIZE_COMP-681',
        'DEVOPS-2888-MAIZE_COMP-4354',
        'DEVOPS-2888-MAIZE_COMP-8118',
        'DEVOPS-2888-MAIZE_COMP-2077',
        'DEVOPS-2888-MAIZE_COMP-874',
        'DEVOPS-2888-MAIZE_COMP-1902',
        'DEVOPS-2888-MAIZE_COMP-4647',
        'DEVOPS-2888-MAIZE_COMP-4472',
        'DEVOPS-2888-MAIZE_COMP-8422',
        'DEVOPS-2888-MAIZE_COMP-1514',
        'DEVOPS-2888-MAIZE_COMP-3849',
        'DEVOPS-2888-MAIZE_COMP-8781',
        'DEVOPS-2888-MAIZE_COMP-1387',
        'DEVOPS-2888-MAIZE_COMP-6593',
        'DEVOPS-2888-MAIZE_COMP-9118',
        'DEVOPS-2888-MAIZE_COMP-5674',
        'DEVOPS-2888-MAIZE_COMP-5394',
        'DEVOPS-2888-MAIZE_COMP-2646',
        'DEVOPS-2888-MAIZE_COMP-9176',
        'DEVOPS-2888-MAIZE_COMP-5813',
        'DEVOPS-2888-MAIZE_COMP-4012',
        'DEVOPS-2888-MAIZE_COMP-7414',
        'DEVOPS-2888-MAIZE_COMP-2595'
    )
ORDER BY
    gs.id
;

    

--rollback DELETE FROM
--rollback 	germplasm.package
--rollback WHERE package_label ilike 'DEVOPS-2888-MAIZE_COMP%'