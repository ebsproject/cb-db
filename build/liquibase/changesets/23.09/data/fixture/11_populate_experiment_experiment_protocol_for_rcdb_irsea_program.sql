--liquibase formatted sql

--changeset postgres:11_populate_experiment_experiment_protocol_for_rcdb_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2718 CB-DB: Create liquibase changesets for experiments created in BA-147



INSERT INTO 
    experiment.experiment_protocol
        (experiment_id,protocol_id,order_number,creator_id)
SELECT
    experiment_id,protocol_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'),(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0047919'),3,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'),(SELECT id FROM tenant.protocol WHERE protocol_code='PLANTING_PROTOCOL_EXP0047919'),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'),(SELECT id FROM tenant.protocol WHERE protocol_code='HARVEST_PROTOCOL_EXP0047919'),5,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'),(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0047919'),4,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,protocol_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_protocol
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047919'
--rollback    )
--rollback ;