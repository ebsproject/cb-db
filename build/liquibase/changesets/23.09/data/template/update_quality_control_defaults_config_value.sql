--liquibase formatted sql

--changeset postgres:update_quality_control_defaults_config_value context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6317 CB-DB: Add threshold for downloading of plot dataset



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "name": "Quality Control",
            "defaults": {
                "commit_threshold": 1000,
                "suppress_record_threshold": 100,
                "suppress_by_rule_threshold": 100,
                "download_dataset_threshold": 1000
            }
        }
    ' 
WHERE 
    abbrev = 'QUALITY_CONTROL_DEFAULTS';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "Quality Control",
--rollback             "defaults": {
--rollback                 "commit_threshold": 1000,
--rollback                 "suppress_record_threshold": 100,
--rollback                 "suppress_by_rule_threshold": 100
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'QUALITY_CONTROL_DEFAULTS';