--liquibase formatted sql

--changeset postgres:update cm collaborator role germplasm attribute config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes



-- update cm collaborator role germplasm attribute config
UPDATE
    platform.config
SET
    abbrev = 'CM_ROLE_COLLABORATOR_GERMPLASM_ATTRIBUTE_SETTINGS'
WHERE
    abbrev = 'CM_ROLE_COLLABORATOR_ATTRIBUTE_SETTINGS'



--rollback UPDATE
--rollback    platform.config
--rollback SET
--rollback    abbrev = 'CM_ROLE_COLLABORATOR_ATTRIBUTE_SETTINGS'
--rollback WHERE
--rollback    abbrev = 'CM_ROLE_COLLABORATOR_GERMPLASM_ATTRIBUTE_SETTINGS'