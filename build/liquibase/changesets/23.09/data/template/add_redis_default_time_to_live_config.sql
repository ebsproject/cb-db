--liquibase formatted sql

--changeset postgres:add_redis_default_time_to_live_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6290 CB-EM: POC on applying Redis to EM View Occurrence Plots with collected trait data



INSERT INTO platform.config
    (
        creator_id,
        abbrev,
        name,
        config_value,
        rank,
        usage
    )
VALUES
    (
        1,
        'REDIS_DEFAULT_TIME_TO_LIVE',
        'Default Time-to-Live value for Redis keys in seconds',
        '{"value": 600}',
        1,
        'application'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'REDIS_DEFAULT_TIME_TO_LIVE';
