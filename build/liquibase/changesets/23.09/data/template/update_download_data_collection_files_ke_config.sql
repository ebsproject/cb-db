--liquibase formatted sql

--changeset postgres:update_download_data_collection_files_ke_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6316 CB DB: Update configuration for Download for FieldBook



UPDATE
    platform.config
SET
    config_value = '{
        "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
            {
            "abbrev": "PLOT_CODE",
            "attribute": "plotCode"
            },
            {
            "abbrev": "PLOTNO",
            "attribute": "plotNumber"
            },
            {
            "abbrev": "REP",
            "attribute": "rep"
            },
            {
            "abbrev": "BLOCK_NO_CONT",
            "attribute": "blockNumber"
            },
            {
            "abbrev": "ROW_BLOCK_NUMBER",
            "attribute": "rowBlockNumber"
            },
            {
            "abbrev": "COL_BLOCK_NUMBER",
            "attribute": "colBlockNumber"
            },
            {
            "abbrev": "ENTNO",
            "attribute": "entryNumber"
            },
            {
            "abbrev": "DESIGNATION",
            "attribute": "entryName"
            },
            {
            "abbrev": "PARENTAGE",
            "attribute": "parentage"
            },
            {
            "abbrev": "FIELD_X",
            "attribute": "fieldX"
            },
            {
            "abbrev": "FIELD_Y",
            "attribute": "fieldY"
            },
            {
            "abbrev": "OCCURRENCE_ID",
            "attribute": "occurrenceDbId"
            },
            {
            "abbrev": "OCCURRENCE_NAME",
            "attribute": "occurrenceName"
            },
            {
            "abbrev": "OCCURRENCE_CODE",
            "attribute": "occurrenceCode"
            },
            {
            "abbrev": "LOCATION_ID",
            "attribute": "locationDbId"
            },
            {
            "abbrev": "LOCATION_NAME",
            "attribute": "locationName"
            },
            {
            "abbrev": "LOCATION_CODE",
            "attribute": "locationCode"
            }
        ]}'
WHERE
    abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_KE';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = '{
--rollback         "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
--rollback             {
--rollback             "abbrev": "PLOT_CODE",
--rollback             "attribute": "plotCode"
--rollback             },
--rollback             {
--rollback             "abbrev": "PLOTNO",
--rollback             "attribute": "plotNumber"
--rollback             },
--rollback             {
--rollback             "abbrev": "REP",
--rollback             "attribute": "rep"
--rollback             },
--rollback             {
--rollback             "abbrev": "BLOCK_NO_CONT",
--rollback             "attribute": "blockNumber"
--rollback             },
--rollback             {
--rollback             "abbrev": "ENTNO",
--rollback             "attribute": "entryNumber"
--rollback             },
--rollback             {
--rollback             "abbrev": "DESIGNATION",
--rollback             "attribute": "entryName"
--rollback             },
--rollback             {
--rollback             "abbrev": "PARENTAGE",
--rollback             "attribute": "parentage"
--rollback             },
--rollback             {
--rollback             "abbrev": "FIELD_X",
--rollback             "attribute": "fieldX"
--rollback             },
--rollback             {
--rollback             "abbrev": "FIELD_Y",
--rollback             "attribute": "fieldY"
--rollback             },
--rollback             {
--rollback             "abbrev": "OCCURRENCE_ID",
--rollback             "attribute": "occurrenceDbId"
--rollback             },
--rollback             {
--rollback             "abbrev": "OCCURRENCE_NAME",
--rollback             "attribute": "occurrenceName"
--rollback             },
--rollback             {
--rollback             "abbrev": "OCCURRENCE_CODE",
--rollback             "attribute": "occurrenceCode"
--rollback             },
--rollback             {
--rollback             "abbrev": "LOCATION_ID",
--rollback             "attribute": "locationDbId"
--rollback             },
--rollback             {
--rollback             "abbrev": "LOCATION_NAME",
--rollback             "attribute": "locationName"
--rollback             },
--rollback             {
--rollback             "abbrev": "LOCATION_CODE",
--rollback             "attribute": "locationCode"
--rollback             }
--rollback         ]}'
--rollback WHERE
--rollback     abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_KE';