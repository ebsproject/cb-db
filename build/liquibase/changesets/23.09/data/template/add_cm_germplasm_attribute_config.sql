--liquibase formatted sql

--changeset postgres:add_cm_germplasm_attribute_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes



-- add cm germplasm attribute config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    -- GLOBAL
    (
        'CM_GLOBAL_GERMPLASM_ATTRIBUTE_SETTINGS',
        'Cross Manager Export Global Configuration for Germplasm Attribute',
        $$
            {
                "Export": {
                    "attributes": [
                        "GERMPLASM_CODE",
                        "GERMPLASM_NAME",
                        "GRAIN_COLOR",
                        "CROSS_NUMBER",
                        "GROWTH_HABIT"
                    ]
                }
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes'
    ),
    -- ROLE_COLLABORATOR
    (
        'CM_ROLE_COLLABORATOR_ATTRIBUTE_SETTINGS',
        'Cross Manager Export Role Collaborator Configuration for Germplasm Attribute',
        $$
            {
                "Export": {
                    "attributes": [
                        "GERMPLASM_CODE",
                        "GERMPLASM_NAME"
                    ]
                }
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes'
    ),
    -- PROGRAM_KE
    (
        'CM_PROGRAM_KE_GERMPLASM_ATTRIBUTE_SETTINGS',
        'Cross Manager Export Program KE Configuration for Germplasm Attribute',
        $$
            {
                "Export": {
                    "attributes": [
                        "GERMPLASM_CODE",
                        "GERMPLASM_NAME",
                        "GRAIN_COLOR",
                        "HETEROTIC_GROUP"
                    ]
                }
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes'
    ),
    -- PROGRAM_IRSEA
    (
        'CM_PROGRAM_IRSEA_GERMPLASM_ATTRIBUTE_SETTINGS',
        'Cross Manager Export Program IRSEA Configuration for Germplasm Attribute',
        $$
            {
                "Export": {
                    "attributes": [
                        "GERMPLASM_CODE",
                        "GERMPLASM_NAME",
                        "CROSS_NUMBER"
                    ]
                }
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-6318 > CM-DB: Create changeset for configuration for exporting of germplasm attributes'
    );

--rollback DELETE FROM platform.config WHERE abbrev='CM_GLOBAL_GERMPLASM_ATTRIBUTE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev='CM_ROLE_COLLABORATOR_ATTRIBUTE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev='CM_PROGRAM_KE_GERMPLASM_ATTRIBUTE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev='CM_PROGRAM_IRSEA_GERMPLASM_ATTRIBUTE_SETTINGS';