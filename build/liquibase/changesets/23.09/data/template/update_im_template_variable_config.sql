--liquibase formatted sql

--changeset postgres:update_im_template_variable_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6358 IM: Replace Designation, Germplasm, and Entry Name with Germplasm Name 



-- update ss wl variable configuration
UPDATE
	platform.config
SET
	config_value = $${
    "values": [
        {
            "name": "Germplasm Code",
            "type": "column",
            "view": {
                "visible": "false",
                "entities": []
            },
            "usage": "required",
            "abbrev": "GERMPLASM_CODE",
            "entity": "seed",
            "required": "true",
            "api_field": "germplasmDbId",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "germplasmCode",
            "skip_creation": "false",
            "retrieve_db_id": "true",
            "url_parameters": "limit=1",
            "db_id_api_field": "germplasmDbId",
            "search_endpoint": "germplasm-search",
            "additional_filters": {}
        },
        {
            "name": "Germplasm Name",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "seed",
                "package"
                ]
            },
            "usage": "optional",
            "abbrev": "GERMPLASM_NAME",
            "entity": "seed",
            "required": "false",
            "api_field": "",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "nameValue",
            "skip_creation": "true",
            "retrieve_db_id": "true",
            "url_parameters": "limit=1",
            "db_id_api_field": "germplasmDbId",
            "search_endpoint": "germplasm-names-search",
            "additional_filters": {}
        },
        {
            "name": "Package Label",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PACKAGE_LABEL",
            "entity": "package",
            "required": "true",
            "api_field": "packageLabel",
            "data_type": "string",
            "http_method": "",
            "value_filter": "",
            "skip_creation": "false",
            "retrieve_db_id": "false",
            "url_parameters": "",
            "db_id_api_field": "",
            "search_endpoint": "",
            "additional_filters": {}
        },
        {
            "name": "Program",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PROGRAM",
            "entity": "package",
            "required": "true",
            "api_field": "programDbId",
            "data_type": "string",
            "http_method": "POST",
            "value_filter": "programCode",
            "skip_creation": "false",
            "retrieve_db_id": "true",
            "url_parameters": "limit=1",
            "db_id_api_field": "programDbId",
            "search_endpoint": "programs-search",
            "additional_filters": {}
        },
        {
            "name": "Package Status",
            "type": "column",
            "view": {
                "visible": "true",
                "entities": [
                "package"
                ]
            },
            "usage": "required",
            "abbrev": "PACKAGE_STATUS",
            "entity": "package",
            "required": "true",
            "api_field": "packageStatus",
            "data_type": "string",
            "http_method": "",
            "value_filter": "",
            "skip_creation": "false",
            "retrieve_db_id": "false",
            "url_parameters": "",
            "db_id_api_field": "",
            "search_endpoint": "",
            "additional_filters": {}
        }
    ]
    }$$
WHERE
	abbrev = 'IM_CREATE_SEED_PACKAGE_SYSTEM_DEFAULT';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $$
--rollback {
--rollback   "values": [
--rollback     {
--rollback       "name": "Germplasm Code",
--rollback       "type": "column",
--rollback       "view": {
--rollback         "visible": "false",
--rollback         "entities": []
--rollback       },
--rollback       "usage": "required",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "entity": "seed",
--rollback       "required": "true",
--rollback       "api_field": "germplasmDbId",
--rollback       "data_type": "string",
--rollback       "http_method": "POST",
--rollback       "value_filter": "germplasmCode",
--rollback       "skip_creation": "false",
--rollback       "retrieve_db_id": "true",
--rollback       "url_parameters": "limit=1",
--rollback       "db_id_api_field": "germplasmDbId",
--rollback       "search_endpoint": "germplasm-search",
--rollback       "additional_filters": {}
--rollback     },
--rollback     {
--rollback       "name": "Germplasm Name",
--rollback       "type": "column",
--rollback       "view": {
--rollback         "visible": "true",
--rollback         "entities": [
--rollback           "seed",
--rollback           "package"
--rollback         ]
--rollback       },
--rollback       "usage": "optional",
--rollback       "abbrev": "DESIGNATION",
--rollback       "entity": "seed",
--rollback       "required": "false",
--rollback       "api_field": "",
--rollback       "data_type": "string",
--rollback       "http_method": "POST",
--rollback       "value_filter": "nameValue",
--rollback       "skip_creation": "true",
--rollback       "retrieve_db_id": "true",
--rollback       "url_parameters": "limit=1",
--rollback       "db_id_api_field": "germplasmDbId",
--rollback       "search_endpoint": "germplasm-names-search",
--rollback       "additional_filters": {}
--rollback     },
--rollback     {
--rollback       "name": "Package Label",
--rollback       "type": "column",
--rollback       "view": {
--rollback         "visible": "true",
--rollback         "entities": [
--rollback           "package"
--rollback         ]
--rollback       },
--rollback       "usage": "required",
--rollback       "abbrev": "PACKAGE_LABEL",
--rollback       "entity": "package",
--rollback       "required": "true",
--rollback       "api_field": "packageLabel",
--rollback       "data_type": "string",
--rollback       "http_method": "",
--rollback       "value_filter": "",
--rollback       "skip_creation": "false",
--rollback       "retrieve_db_id": "false",
--rollback       "url_parameters": "",
--rollback       "db_id_api_field": "",
--rollback       "search_endpoint": "",
--rollback       "additional_filters": {}
--rollback     },
--rollback     {
--rollback       "name": "Program",
--rollback       "type": "column",
--rollback       "view": {
--rollback         "visible": "true",
--rollback         "entities": [
--rollback           "package"
--rollback         ]
--rollback       },
--rollback       "usage": "required",
--rollback       "abbrev": "PROGRAM",
--rollback       "entity": "package",
--rollback       "required": "true",
--rollback       "api_field": "programDbId",
--rollback       "data_type": "string",
--rollback       "http_method": "POST",
--rollback       "value_filter": "programCode",
--rollback       "skip_creation": "false",
--rollback       "retrieve_db_id": "true",
--rollback       "url_parameters": "limit=1",
--rollback       "db_id_api_field": "programDbId",
--rollback       "search_endpoint": "programs-search",
--rollback       "additional_filters": {}
--rollback     },
--rollback     {
--rollback       "name": "Package Status",
--rollback       "type": "column",
--rollback       "view": {
--rollback         "visible": "true",
--rollback         "entities": [
--rollback           "package"
--rollback         ]
--rollback       },
--rollback       "usage": "required",
--rollback       "abbrev": "PACKAGE_STATUS",
--rollback       "entity": "package",
--rollback       "required": "true",
--rollback       "api_field": "packageStatus",
--rollback       "data_type": "string",
--rollback       "http_method": "",
--rollback       "value_filter": "",
--rollback       "skip_creation": "false",
--rollback       "retrieve_db_id": "false",
--rollback       "url_parameters": "",
--rollback       "db_id_api_field": "",
--rollback       "search_endpoint": "",
--rollback       "additional_filters": {}
--rollback     }
--rollback   ]
--rollback }
--rollback 	$$
--rollback WHERE
--rollback 	abbrev = 'IM_CREATE_SEED_PACKAGE_SYSTEM_DEFAULT';