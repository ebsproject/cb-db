--liquibase formatted sql

--changeset postgres:add_variable_across_env_design context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6281 Add variable ACROSS_ENV_DESIGN



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, display_name, creator_id) 
    VALUES 
        ('ACROSS_ENV_DESIGN', 'ACROSS ENVIRONMENT DESIGN', 'Across Environment Design', 'character varying', false, 'system', null, 'application', NULL, 'active', 'Across Environment Design', '1')
    RETURNING id INTO var_variable_id;

     -- property
    SELECT id FROM master.property WHERE abbrev = 'ACROSS_ENV_DESIGN' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('ACROSS_ENV_DESIGN', 'Across Environment Design', 'Across Environment Design') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('ACROSS_ENV_DESIGN', 'Across Environment Design')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('ACROSS_ENV_DESIGN', 'Across Environment Design', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- scale_value (uncomment if variable has scale values; else, delete this code)
    INSERT INTO
        master.scale_value (
            scale_id, order_number, value, description, abbrev, scale_value_status
        )
    SELECT
        var_scale_id AS scale_id,
        ROW_NUMBER() OVER (PARTITION BY t.variable) AS order_number,
        t.value,
        t.description,
        t.variable || '_' || t.abbrev AS abbrev,
        t.scale_value_status
    FROM (
            VALUES
            ('ACROSS_ENV_DESIGN', 'none', 'None', 'NONE', 'show'),
            ('ACROSS_ENV_DESIGN', 'random', 'Random', 'RANDOM', 'show')
            -- TODO: Add more scale values here if needed
        ) AS t (
            variable, value, description, abbrev, scale_value_status
        )
    ;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'ACROSS_ENV_DESIGN'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'ACROSS_ENV_DESIGN'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'ACROSS_ENV_DESIGN'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'ACROSS_ENV_DESIGN'
--rollback ;