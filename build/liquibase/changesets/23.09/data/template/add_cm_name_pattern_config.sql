--liquibase formatted sql

--changeset postgres:add_cm_name_pattern_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3915 M8 CM: Generate entry list name similar to experiment name generator



-- add cm bg process thresholds config
INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'CM_NAME_PATTERN_ENTRY_LIST_NAME',
        'Cross Manager Name Pattern for Entry List Name',
        $$
            {
                "pattern": [
                    {
                        "value": "SITE_CODE",
                        "is_variable": true
                    },
                    {
                        "value": "-",
                        "is_variable": false
                    },
                    {
                        "value": "SEASON_CODE",
                        "is_variable": true
                    },
                    {
                        "value": "-",
                        "is_variable": false
                    },
                    {
                        "value": "YEAR",
                        "is_variable": true
                    },
                    {
                        "value": "-",
                        "is_variable": false
                    },
                    {
                        "value": "COUNTER",
                        "digits": "3",
                        "max_value": "SITE|SEASON|YEAR",
                        "zero_padding": "leading"
                    }
                ]
            }
        $$,
        1,
        'cross_manager',
        1,
        'CORB-3915 M8 CM: Generate entry list name similar to experiment name generator'
    );



--rollback DELETE FROM platform.config WHERE abbrev='CM_NAME_PATTERN_ENTRY_LIST_NAME';