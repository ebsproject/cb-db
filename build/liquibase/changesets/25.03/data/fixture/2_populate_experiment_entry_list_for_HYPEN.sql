--liquibase formatted sql

--changeset postgres:populate_experiment_entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267: CB DB - Populate experiment_entry_list for HYPEN



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,is_void,notes,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
 VALUES 
(experiment.generate_code('entry_list'),'HEI-TEST-MM24WS-12','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-12' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1)),
(experiment.generate_code('entry_list'),'IRSEA-OYT-2025-WS-006','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-04-UPDATED' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1)),
(experiment.generate_code('entry_list'),'HEI-TEST-MM24WS-13','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-13' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1)),
(experiment.generate_code('entry_list'),'IRSEA-BRE-2025-CS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-14' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1));



--rollback DELETE FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN (
--rollback 'HEI-TEST-MM24WS-12',
--rollback 'HEI-TEST-MM24WS-04-UPDATED',
--rollback 'HEI-TEST-MM24WS-13',
--rollback 'HEI-TEST-MM24WS-14'
--rollback ));