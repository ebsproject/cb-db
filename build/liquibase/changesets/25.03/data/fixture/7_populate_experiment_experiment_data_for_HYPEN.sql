--liquibase formatted sql

--changeset postgres:populate_experiment_experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267: CB DB - Populate experiment_experiment_data for HYPEN



INSERT INTO experiment.experiment_data
(experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id,is_void,notes)
 VALUES 
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-12' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW' LIMIT 1),'Bottom Left','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-12' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),'311460','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-12' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),'311461','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-04-UPDATED' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW' LIMIT 1),'Bottom Left','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-04-UPDATED' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),'311464','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-04-UPDATED' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),'311465','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-13' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW' LIMIT 1),'Bottom Left','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-13' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),'313451','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-13' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),'313452','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-14' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW' LIMIT 1),'Top Left','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-14' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),'313474','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-14' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),'313475','N',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),False,NULL);



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN ('HEI-TEST-MM24WS-12','HEI-TEST-MM24WS-13','HEI-TEST-MM24WS-14','HEI-TEST-MM24WS-04-UPDATED'))