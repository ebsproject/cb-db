--liquibase formatted sql

--changeset postgres:7_populate_experiment_location_occurrence_group_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate experiment.location_occurrence_group referencing BATestData1_Aug_NS_2025



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,creator_id,is_void,notes)
 VALUES 
((SELECT id FROM experiment.location WHERE location_name ='IRRIHQ-2025-WS-009' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_NE_SM2-2025-WS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-002' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_AN_RM-2025-WS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-003' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_BO_UA-2025-WS-002' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-004' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_IB_SO-2025-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-005' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_NC_MD-2025-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-006' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_II_AU-2025-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-007' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='PH_QZ_IF-2025-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='BATestData1_Aug_NS_2025-008' LIMIT 1),1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL);



--rollback DELETE FROM experiment.location_occurrence_group WHERE occurrence_id IN ( SELECT id FROM experiment.occurrence WHERE occurrence_name IN (
--rollback 'BATestData1_Aug_NS_2025-001',
--rollback 'BATestData1_Aug_NS_2025-002',
--rollback 'BATestData1_Aug_NS_2025-003', 
--rollback 'BATestData1_Aug_NS_2025-004', 
--rollback 'BATestData1_Aug_NS_2025-005', 
--rollback 'BATestData1_Aug_NS_2025-006', 
--rollback 'BATestData1_Aug_NS_2025-007', 
--rollback 'BATestData1_Aug_NS_2025-008' 
--rollback ));