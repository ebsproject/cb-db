--liquibase formatted sql

--changeset postgres:12_populate_experiment_experiment_data_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate experiment.experiment_data referencing BATestData1_Aug_NS_2025



INSERT INTO experiment.experiment_data
(experiment_id,variable_id,data_value,data_qc_code,protocol_id,is_void,notes,creator_id)
 VALUES 
((SELECT id FROM experiment.experiment WHERE experiment_name ='BATestData1_Aug_NS_2025' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),311218,'N',NULL,False,NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1)),
((SELECT id FROM experiment.experiment WHERE experiment_name ='BATestData1_Aug_NS_2025' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),311219,'N',NULL,False,NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1));



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name ='BATestData1_Aug_NS_2025' LIMIT 1)