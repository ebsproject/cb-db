--liquibase formatted sql

--changeset postgres:11_populate_management_list_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267 CB DB - populate trait management list for HYPEN



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'MANAGEMENT_PROTOCOL_' || occur.occurrence_code AS abbrev,													
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'variable' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'VARIABLE') AS entity_id,
	(SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-5267)' notes,
	'management protocol' AS list_sub_type
FROM 
	experiment.occurrence occur 
WHERE
	occur.experiment_id IN (SELECT id from experiment.experiment where experiment_name in ('HEI-TEST-MM24WS-12','HEI-TEST-MM24WS-04-UPDATED','HEI-TEST-MM24WS-13','HEI-TEST-MM24WS-14'));



--rollback DELETE FROM
--rollback     platform.list
--rollback WHERE
--rollback notes = 'Inserted via liquibase changeset (BDS-5267)'




