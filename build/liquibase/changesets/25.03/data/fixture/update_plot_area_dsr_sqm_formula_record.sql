--liquibase formatted sql

--changeset postgres:update_master.formula_plot_area_dsr_sqm_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5210 CB-DB-Fixture: Update incorrect PLOT_AREA_DSR_SQM formula record



UPDATE
	master.formula
SET
	formula = 'PLOT_AREA_DSR_SQM = (DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT',
	database_formula = '
		CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
			dist_bet_rows double precision,
			row_length_cont double precision,
			rows_per_plot_cont double precision)
			RETURNS double precision
			LANGUAGE ''plpgsql''
			COST 100
			VOLATILE PARALLEL UNSAFE
		AS $BODY$
			DECLARE
				plot_area_dsr_sqm float;
			BEGIN

				plot_area_dsr_sqm = (dist_bet_rows/100) * row_length_cont * rows_per_plot_cont;

				RETURN round(plot_area_dsr_sqm::numeric, 3);
			END

		$BODY$;
	'
WHERE formula = 'PLOT_AREA_DSR_SQM = DIST_BET_ROWS * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT'



--rollback UPDATE
--rollback 	master.formula
--rollback SET
--rollback 	formula = 'PLOT_AREA_DSR_SQM = DIST_BET_ROWS * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT',
--rollback 	database_formula = '
--rollback 		create or replace function master.formula_plot_area_dsr_sqm(
--rollback 			dist_bet_rows float,
--rollback 			row_length_cont  float,
--rollback 			rows_per_plot_cont float
--rollback 		) returns float as
--rollback 		$body$
--rollback 		declare
--rollback 			plot_area_dsr_sqm float;
--rollback 			local_dist_bet_rows float;
--rollback 			local_length_cont float;
--rollback 			local_rows_per_plot_cont float;
--rollback 		begin
--rollback 			
--rollback 		plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;
--rollback 			
--rollback 			return round(plot_area_dsr_sqm::numeric, 3);
--rollback 		end
--rollback 		$body$
--rollback 		language plpgsql;
--rollback 	'
--rollback WHERE formula = 'PLOT_AREA_DSR_SQM = (DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT'

