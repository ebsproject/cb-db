--liquibase formatted sql

--changeset postgres:11_populate_trait_list_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate trait management list referencing BATestData1_Aug_NS_2025



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'TRAIT_PROTOCOL_' || occur.occurrence_code AS abbrev,													
	occur.occurrence_name|| ' Trait Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Trait Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'trait' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'TRAIT') AS entity_id,
	(SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-5003)' notes,
	'trait protocol' AS list_sub_type
FROM 
	experiment.occurrence occur 
WHERE
	occur.experiment_id IN (SELECT id from experiment.experiment where experiment_name in ('BATestData1_Aug_NS_2025'));



--rollback DELETE FROM
--rollback     platform.list
--rollback WHERE
--rollback name ILIKE 'BATestData1_Aug_NS_2025'
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-5003)'
