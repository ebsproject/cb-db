--liquibase formatted sql

--changeset postgres:1_populate_experiment_experiment_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate experiment.experiment referencing BATestData1_Aug_NS_2025



INSERT INTO experiment.experiment
(program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,creator_id,experiment_plan_id,is_void,notes,data_process_id,crop_id,remarks)
 VALUES 
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),(SELECT id FROM tenant.pipeline WHERE pipeline_code ='BTP_PIPE' LIMIT 1),(SELECT id FROM tenant.stage WHERE stage_code ='OYT' LIMIT 1),NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,experiment.generate_code('experiment'),'BATestData1_Aug_NS_2025',NULL,'Breeding Trial',NULL,NULL,NULL,'created',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'upload');



--rollback DELETE FROM experiment.experiment WHERE experiment_name = 'BATestData1_Aug_NS_2025';