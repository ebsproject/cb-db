--liquibase formatted sql

--changeset postgres:populate_experiment_occurrence_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267: CB DB - Populate experiment_occurrence_data for HYPEN



INSERT INTO experiment.occurrence_data
(occurrence_id,variable_id,data_value,data_qc_code,creator_id,is_void,notes,protocol_id)
 VALUES 
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-13-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),313459,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-13') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-13-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),313460,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-13') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-12-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),311462,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-12') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-12-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),311463,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-12') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-14-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),313486,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-14') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-14-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),313487,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-14') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-04-UPDATED-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID' LIMIT 1),311466,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'MANAGEMENT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-04-UPDATED') LIMIT 1)),
((SELECT id FROM experiment.occurrence WHERE occurrence_name ='HEI-TEST-MM24WS-04-UPDATED-001' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),311467,'G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,(SELECT id FROM tenant.protocol WHERE protocol_code= (SELECT 'TRAIT_PROTOCOL_'||experiment_code FROM experiment.experiment WHERE experiment_name = 'HEI-TEST-MM24WS-04-UPDATED') LIMIT 1));



--rollback DELETE FROM experiment.occurrence_data WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN (
--rollback 'HEI-TEST-MM24WS-13-001',
--rollback 'HEI-TEST-MM24WS-12-001',
--rollback 'HEI-TEST-MM24WS-14-001',
--rollback 'HEI-TEST-MM24WS-04-UPDATED-001'
--rollback ));