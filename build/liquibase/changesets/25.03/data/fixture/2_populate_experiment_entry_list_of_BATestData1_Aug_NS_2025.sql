--liquibase formatted sql

--changeset postgres:2_populate_experiment_entry_list_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate experiment.entry_list referencing BATestData1_Aug_NS_2025



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,is_void,notes,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
 VALUES 
(experiment.generate_code('entry_list'),'IRSEA-OYT-2025-WS-004','new','completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='BATestData1_Aug_NS_2025' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1));



--rollback DELETE FROM experiment.entry_list WHERE entry_list_name = 'IRSEA-OYT-2025-WS-004';