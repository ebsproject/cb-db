--liquibase formatted sql

--changeset postgres:10_populate_trait_protocols_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267 CB DB - populate trait protocol for HYPEN



INSERT INTO  tenant.protocol 
	(protocol_code,protocol_name,protocol_type,description,program_id,creator_id,notes)
SELECT
	    'TRAIT_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Trait Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'trait' AS protocol_type,
	    null AS description,
	    ex.program_id,
	    (SELECT id FROM tenant.person WHERE username ='admin') AS creator_id,
		'Inserted via liquibase changeset (BDS-5267)' notes
	FROM
	   	experiment.experiment ex
   	WHERE
		ex.experiment_name in ('HEI-TEST-MM24WS-12','HEI-TEST-MM24WS-04-UPDATED','HEI-TEST-MM24WS-13','HEI-TEST-MM24WS-14');



--rollback DELETE FROM
--rollback     tenant.protocol
--rollback WHERE
--rollback 		protocol_code ILIKE 'TRAIT_PROTOCOL_%'
--rollback AND
--rollback 		notes = 'Inserted via liquibase changeset (BDS-5267)';