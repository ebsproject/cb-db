--liquibase formatted sql

--changeset postgres:populate_experiment_occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267: CB DB - Populate experiment_occurrence for HYPEN



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,creator_id,geospatial_object_id,is_void,notes,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count,access_data,occurrence_design_type)
 VALUES 
((SELECT expt.experiment_code||'-1' FROM experiment.experiment expt WHERE expt.experiment_name='HEI-TEST-MM24WS-12'),'HEI-TEST-MM24WS-12-001','created',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-12' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),1,NULL,52,104,'{"person": {"1": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}, "program": {"101": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}}','Alpha-Lattice'),
((SELECT expt.experiment_code||'-1' FROM experiment.experiment expt WHERE expt.experiment_name='HEI-TEST-MM24WS-04-UPDATED'),'HEI-TEST-MM24WS-04-UPDATED-001','created',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-04-UPDATED' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),1,NULL,160,320,'{"person": {"1": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}, "program": {"101": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}}','Alpha-Lattice'),
((SELECT expt.experiment_code||'-1' FROM experiment.experiment expt WHERE expt.experiment_name='HEI-TEST-MM24WS-13'),'HEI-TEST-MM24WS-13-001','created',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-13' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),1,NULL,100,200,'{"person": {"1": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}, "program": {"101": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}}','Alpha-Lattice'),
((SELECT expt.experiment_code||'-1' FROM experiment.experiment expt WHERE expt.experiment_name='HEI-TEST-MM24WS-14'),'HEI-TEST-MM24WS-14-001','created',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='HEI-TEST-MM24WS-14' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,1.0,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),1,NULL,131,131,'{"person": {"1": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}, "program": {"101": {"addedBy": 1, "addedOn": "2025-03-03 15:42:57.323432+08", "permission": "write"}}}','Entry Order');



--rollback  DELETE FROM experiment.occurrence WHERE occurrence_name IN (
--rollback 'HEI-TEST-MM24WS-12-001',
--rollback 'HEI-TEST-MM24WS-04-UPDATED-001',
--rollback 'HEI-TEST-MM24WS-13-001',
--rollback 'HEI-TEST-MM24WS-14-001'
--rollback );       