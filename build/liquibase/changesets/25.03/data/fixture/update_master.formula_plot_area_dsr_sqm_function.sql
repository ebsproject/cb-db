--liquibase formatted sql

--changeset postgres:update_master.formula_plot_area_dsr_sqm_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5210 CB-DB-Fixture: Update incorrect PLOT_AREA_DSR_SQM formula function



-- FUNCTION: master.formula_plot_area_dsr_sqm(double precision, double precision, double precision)

DROP FUNCTION IF EXISTS master.formula_plot_area_dsr_sqm(double precision, double precision, integer);

CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
	dist_bet_rows double precision,
	row_length_cont double precision,
	rows_per_plot_cont double precision)
    RETURNS double precision
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
	DECLARE
		plot_area_dsr_sqm float;
	BEGIN

		plot_area_dsr_sqm = (dist_bet_rows/100) * row_length_cont * rows_per_plot_cont;

		RETURN round(plot_area_dsr_sqm::numeric, 3);
	END

$BODY$;



--rollback DROP FUNCTION IF EXISTS master.formula_plot_area_dsr_sqm(double precision, double precision, double precision);
--rollback 
--rollback CREATE OR REPLACE FUNCTION master.formula_plot_area_dsr_sqm(
--rollback 	dist_bet_rows double precision,
--rollback 	row_length_cont double precision,
--rollback 	rows_per_plot_cont integer)
--rollback     RETURNS double precision
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE PARALLEL UNSAFE
--rollback AS $BODY$
--rollback 	DECLARE
--rollback 		plot_area_dsr_sqm float;
--rollback 	BEGIN
--rollback 
--rollback 		plot_area_dsr_sqm = dist_bet_rows * row_length_cont * rows_per_plot_cont;
--rollback 
--rollback 		RETURN round(plot_area_dsr_sqm::numeric, 3);
--rollback 	END
--rollback 
--rollback $BODY$;
