--liquibase formatted sql

--changeset postgres:5_populate_place_geospatial_object_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate place.geospatial_object referencing BATestData1_Aug_NS_2025



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,parent_geospatial_object_id,root_geospatial_object_id,creator_id,is_void,notes,coordinates)
 VALUES 
('IRRIHQ-2025-WS-009','IRRIHQ-2025-WS-009','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_NE_SM2-2025-WS-002','PH_NE_SM2-2025-WS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PHILRICE_2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_AN_RM-2025-WS-002','PH_AN_RM-2025-WS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_BO_UA-2025-WS-002','PH_BO_UA-2025-WS-002','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_IB_SO-2025-WS-001','PH_IB_SO-2025-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_NC_MD-2025-WS-001','PH_NC_MD-2025-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_II_AU-2025-WS-001','PH_II_AU-2025-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_II_AU' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL),
('PH_QZ_IF-2025-WS-001','PH_QZ_IF-2025-WS-001','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_QZ_IF' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,NULL);



--rollback DELETE FROM place.geospatial_object WHERE geospatial_object_code IN (
--rollback 'IRRIHQ-2025-WS-009',
--rollback 'PH_NE_SM2-2025-WS-002',
--rollback 'PH_AN_RM-2025-WS-002',
--rollback 'PH_BO_UA-2025-WS-002',
--rollback 'PH_IB_SO-2025-WS-001',
--rollback 'PH_NC_MD-2025-WS-001',
--rollback 'PH_II_AU-2025-WS-001',
--rollback 'PH_QZ_IF-2025-WS-001'
--rollback );