--liquibase formatted sql

--changeset postgres:1_populate_1_germplasm_germplasm_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate germplasm.germplasm referencing BATestData1_Aug_NS_2025



INSERT INTO germplasm.germplasm
(designation,parentage,generation,germplasm_state,germplasm_name_type,crop_id,taxonomy_id,product_profile_id,germplasm_normalized_name,creator_id,is_void,notes,germplasm_type,other_names,germplasm_uuid)
 VALUES 
('BH06:01','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:01',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'ba72fb66-e966-49c3-9ce9-9dc8ad980d4a'),
('BH06:02','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:02',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'cd032fad-0581-4a8a-a5ea-765aab2e0133'),
('BH06:03','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:03',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'1429b0e0-790c-4494-b168-02e671e0b475'),
('BH06:04','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:04',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'a306b40e-2ebe-4035-90d0-265e72d760be'),
('BH06:05','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:05',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'5164f7d0-d332-41bd-ab48-10c0cbab19e7'),
('BH06:06','A/B','F6','fixed','line_name',(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),(SELECT id FROM germplasm.taxonomy WHERE taxonomy_name='Oryza sativa' LIMIT 1),NULL,'BH06:06',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,'progeny',NULL,'65d2c809-f69b-4ca6-bc4f-008871cfd3a5');



--rollback DELETE FROM germplasm.germplasm WHERE designation IN (
--rollback 'BH06:01',
--rollback 'BH06:02',
--rollback 'BH06:03',
--rollback 'BH06:04',
--rollback 'BH06:05',
--rollback 'BH06:06'
--rollback );