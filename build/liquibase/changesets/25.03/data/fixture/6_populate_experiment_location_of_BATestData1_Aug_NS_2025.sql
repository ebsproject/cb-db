--liquibase formatted sql

--changeset postgres:6_populate_experiment_location_of_BATestData1_Aug_NS_2025 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5003 CB DB - populate experiment.location referencing BATestData1_Aug_NS_2025



INSERT INTO experiment.location
(location_code,location_name,location_status,location_type,description,steward_id,geospatial_object_id,creator_id,is_void,notes,location_year,season_id,site_id,field_id,remarks,entry_count,plot_count)
 VALUES 
('IRRIHQ-2025-WS-009','IRRIHQ-2025-WS-009','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ-2025-WS-009' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRI_FARMS' LIMIT 1),9,0,176),
('PH_NE_SM2-2025-WS-002','PH_NE_SM2-2025-WS-002','mapped','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2-2025-WS-002' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NE_SM2' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PHILRICE_2' LIMIT 1),2,0,176),
('PH_AN_RM-2025-WS-002','PH_AN_RM-2025-WS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM-2025-WS-002' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_AN_RM' LIMIT 1),NULL,2,0,176),
('PH_BO_UA-2025-WS-002','PH_BO_UA-2025-WS-002','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA-2025-WS-002' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_BO_UA' LIMIT 1),NULL,2,0,178),
('PH_IB_SO-2025-WS-001','PH_IB_SO-2025-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO-2025-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_IB_SO' LIMIT 1),NULL,1,0,177),
('PH_NC_MD-2025-WS-001','PH_NC_MD-2025-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD-2025-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_NC_MD' LIMIT 1),NULL,1,0,177),
('PH_II_AU-2025-WS-001','PH_II_AU-2025-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_II_AU-2025-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_II_AU' LIMIT 1),NULL,1,0,177),
('PH_QZ_IF-2025-WS-001','PH_QZ_IF-2025-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_QZ_IF-2025-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),False,NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH_QZ_IF' LIMIT 1),NULL,1,0,177);



--rollback DELETE FROM experiment.location WHERE location_code IN (
--rollback 'IRRIHQ-2025-WS-009',
--rollback 'PH_NE_SM2-2025-WS-002',
--rollback 'PH_AN_RM-2025-WS-002',
--rollback 'PH_BO_UA-2025-WS-002',
--rollback 'PH_IB_SO-2025-WS-001',
--rollback 'PH_NC_MD-2025-WS-001',
--rollback 'PH_II_AU-2025-WS-001',
--rollback 'PH_QZ_IF-2025-WS-001'
--rollback );