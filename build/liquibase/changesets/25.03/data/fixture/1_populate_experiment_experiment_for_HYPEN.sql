--liquibase formatted sql

--changeset postgres:populate_experiment_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-5267: CB DB - Populate experiment_experiment for HYPEN



INSERT INTO experiment.experiment
(program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,creator_id,experiment_plan_id,is_void,notes,data_process_id,crop_id,remarks)
 VALUES 
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='PYT' LIMIT 1),NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,experiment.generate_code('experiment'),'HEI-TEST-MM24WS-12',NULL,'Breeding Trial',NULL,NULL,NULL,'created',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,'updated entries',(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'generate'),
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='OYT' LIMIT 1),NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,experiment.generate_code('experiment'),'HEI-TEST-MM24WS-04-UPDATED',NULL,'Breeding Trial',NULL,NULL,NULL,'created',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,NULL,(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'generate'),
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='OYT' LIMIT 1),NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),NULL,experiment.generate_code('experiment'),'HEI-TEST-MM24WS-13',NULL,'Breeding Trial',NULL,NULL,NULL,'created',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,'updated entries',(SELECT id FROM master.item WHERE abbrev ='BREEDING_TRIAL_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),'generate'),
((SELECT id FROM tenant.program WHERE program_code ='IRSEA' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='BRE' LIMIT 1),NULL,2025,(SELECT id FROM tenant.season WHERE season_code ='CS' LIMIT 1),NULL,experiment.generate_code('experiment'),'HEI-TEST-MM24WS-14',NULL,'Observation',NULL,NULL,'Entry Order','created',NULL,(SELECT id FROM tenant.person WHERE email ='admin@ebsproject.org' LIMIT 1),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' LIMIT 1),NULL,False,'updated entries',(SELECT id FROM master.item WHERE abbrev ='OBSERVATION_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='RICE' LIMIT 1),NULL);



--rollback DELETE FROM experiment.experiment WHERE experiment_name IN (
--rollback 'HEI-TEST-MM24WS-12',
--rollback 'HEI-TEST-MM24WS-04-UPDATED',
--rollback 'HEI-TEST-MM24WS-13',
--rollback 'HEI-TEST-MM24WS-14'
--rollback );