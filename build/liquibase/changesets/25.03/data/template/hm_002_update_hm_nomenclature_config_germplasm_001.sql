--liquibase formatted sql

--changeset postgres:hm_002_update_hm_nomenclature_config_germplasm_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-29 BDS-5243 - CB-HM: Update Nomenclature Config for Germplasm (Maize use cases)



UPDATE 
    platform.config
SET
    config_value = $$
        [
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": [
                    "not_fixed", "fixed"
                ],
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "B",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "SINGLE_SEED_DESCENT",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "B RGA",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "harvestMethodAbbrev": [
                    "SINGLE_PLANT_SELECTION",
                    "PANICLE_SELECTION",
                    "SINGLE_PLANT_SEED_INCREASE"
                ],
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "harvestMethodAbbrev": "PLANT_SPECIFIC",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "specific",
                            "options": {
                                "minimum": 2,
                                "delimiter": "^",
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "stageCode": "TCV",
                "state": "fixed",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "freeText",
                            "value": " R",
                            "orderNumber": 1
                        }
                    ]
                }
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "!= SELFING",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "updated",
                            "fieldName": "crossName",
                            "orderNumber": 1
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "contains F1",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "freeText",
                            "value": "(",
                            "orderNumber": 0
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": ")",
                            "orderNumber": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 3
                        },
                        {
                            "type": "freeText",
                            "subType": "repeater",
                            "value": "B",
                            "options": {
                                "minimum": 2,
                                "delimiter": "*"
                            },
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "subType": "repeater",
                            "value": "B",
                            "options": {
                                "minimum": 2,
                                "delimiter": "*"
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "contains F1",
                "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                "config": {
                    "pattern": [
                        {
                            "type": "freeText",
                            "value": "(",
                            "orderNumber": 0
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": ")",
                            "orderNumber": 2
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 3
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "type": "doubled_haploid",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "B",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "type": "haploid",
                "harvestMethodAbbrev": "DH1_INDIVIDUAL_EAR",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "retrieved",
                            "fieldName": "parentGermplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "@",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "0",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "F1TOP",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "0TOP",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "noOfPlant": "!= null",
                "harvestMethodAbbrev": "SELECTED_BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "0",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "noOfPlant",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 4
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 5
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "SELECTED_BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "099",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "F1TOP",
                "noOfPlant": "!= null",
                "harvestMethodAbbrev": "SELECTED_BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "0",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "noOfPlant",
                            "orderNumber": 3
                        },
                        {
                            "type": "freeText",
                            "value": "TOP",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "F1TOP",
                "harvestMethodAbbrev": "SELECTED_BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "099TOP",
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "grainColor",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": [
                    "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
                ],
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 3
                        }
                    ]
                }
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "F1TOP",
                "harvestMethodAbbrev": [
                    "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
                ],
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        },
                        {
                            "type": "freeText",
                            "value": "TOP",
                            "orderNumber": 3
                        },
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "fieldOriginSiteCode",
                            "orderNumber": 4
                        }
                    ]
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "contains F1",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "F2",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "1",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "SINGLE_PLANT",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": [
                    "CHICKPEA", "GROUNDNUT", "SORGHUM",
                    "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "BULK",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "freeText",
                            "value": "B",
                            "orderNumber": 2
                        }
                    ]
                }
            },
            {
                "cropCode": [
                    "CHICKPEA", "GROUNDNUT", "SORGHUM",
                    "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "harvestMethodAbbrev": "SINGLE_PLANT",
                "config": {
                    "pattern": [
                        {
                            "type": "recordInfo",
                            "subType": "planted",
                            "fieldName": "germplasmDesignation",
                            "orderNumber": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "orderNumber": 1
                        },
                        {
                            "type": "counter",
                            "subType": "autoIncrement",
                            "options": {
                                "schema": "germplasm",
                                "table": "germplasm_name",
                                "field": "name_value",
                                "zeroPadded": false,
                                "maxDigits": null
                            },
                            "orderNumber": 2
                        }
                    ]
                }
            }
        ]
    $$
WHERE
    abbrev = 'HM_NOMENCLATURE_CONFIG_GERMPLASM'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback         [
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": [
--rollback                     "not_fixed", "fixed"
--rollback                 ],
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "B",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "SINGLE_SEED_DESCENT",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "B RGA",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "harvestMethodAbbrev": [
--rollback                     "SINGLE_PLANT_SELECTION",
--rollback                     "PANICLE_SELECTION",
--rollback                     "SINGLE_PLANT_SEED_INCREASE"
--rollback                 ],
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "harvestMethodAbbrev": "PLANT_SPECIFIC",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "specific",
--rollback                             "options": {
--rollback                                 "minimum": 2,
--rollback                                 "delimiter": "^",
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "stageCode": "TCV",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": " R",
--rollback                             "orderNumber": 1
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethodAbbrev": "!= SELFING",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "updated",
--rollback                             "fieldName": "crossName",
--rollback                             "orderNumber": 1
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "contains F1",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "(",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": ")",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "subType": "repeater",
--rollback                             "value": "B",
--rollback                             "options": {
--rollback                                 "minimum": 2,
--rollback                                 "delimiter": "*"
--rollback                             },
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "subType": "repeater",
--rollback                             "value": "B",
--rollback                             "options": {
--rollback                                 "minimum": 2,
--rollback                                 "delimiter": "*"
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "contains F1",
--rollback                 "harvestMethodAbbrev": "INDIVIDUAL_EAR",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "(",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": ")",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "INDIVIDUAL_EAR",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "fixed",
--rollback                 "type": "doubled_haploid",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "B",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "0",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "F1TOP",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "0TOP",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "noOfPlant": "!= null",
--rollback                 "harvestMethodAbbrev": "SELECTED_BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "0",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "noOfPlant",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 4
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 5
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "SELECTED_BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "099",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "F1TOP",
--rollback                 "noOfPlant": "!= null",
--rollback                 "harvestMethodAbbrev": "SELECTED_BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "0",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "noOfPlant",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "TOP",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "F1TOP",
--rollback                 "harvestMethodAbbrev": "SELECTED_BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "099TOP",
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "grainColor",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": [
--rollback                     "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
--rollback                 ],
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 3
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "F1TOP",
--rollback                 "harvestMethodAbbrev": [
--rollback                     "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
--rollback                 ],
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "TOP",
--rollback                             "orderNumber": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "fieldOriginSiteCode",
--rollback                             "orderNumber": 4
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "generation": "contains F1",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "F2",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "1",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "SINGLE_PLANT",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "CHICKPEA", "GROUNDNUT", "SORGHUM",
--rollback                     "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
--rollback                 ],
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "BULK",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "freeText",
--rollback                             "value": "B",
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "CHICKPEA", "GROUNDNUT", "SORGHUM",
--rollback                     "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
--rollback                 ],
--rollback                 "crossMethodAbbrev": "SELFING",
--rollback                 "state": "not_fixed",
--rollback                 "harvestMethodAbbrev": "SINGLE_PLANT",
--rollback                 "config": {
--rollback                     "pattern": [
--rollback                         {
--rollback                             "type": "recordInfo",
--rollback                             "subType": "planted",
--rollback                             "fieldName": "germplasmDesignation",
--rollback                             "orderNumber": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "orderNumber": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "counter",
--rollback                             "subType": "autoIncrement",
--rollback                             "options": {
--rollback                                 "schema": "germplasm",
--rollback                                 "table": "germplasm_name",
--rollback                                 "field": "name_value",
--rollback                                 "zeroPadded": false,
--rollback                                 "maxDigits": null
--rollback                             },
--rollback                             "orderNumber": 2
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         ]
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'HM_NOMENCLATURE_CONFIG_GERMPLASM'
--rollback ;