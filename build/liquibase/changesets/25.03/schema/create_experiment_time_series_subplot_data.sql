--liquibase formatted sql

--changeset postgres:create_experiment_timeseries_subplot_data context:schema splitStatements:false
--comment: BDS-5269 Create timeseries subplot data table

CREATE TABLE IF NOT EXISTS experiment.timeseries_subplot_data (
	id serial NOT NULL,
	subplot_id int4 NOT NULL,
	variable_id int4 NOT NULL,
	data_value varchar NULL,
	data_qc_code varchar(8) NOT NULL,
	transaction_id int4 NULL,
	collection_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	creation_timestamp timestamp DEFAULT now() NOT NULL,
	modifier_id int4 NULL,
	modification_timestamp timestamp NULL,
	is_void boolean DEFAULT false NOT NULL,
	notes text NULL,
	event_log jsonb NULL,
	remarks text NULL,
	CONSTRAINT timeseries_subplot_data_id_pkey PRIMARY KEY (id),
	CONSTRAINT timeseries_subplot_data_creator_id_fkey FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT timeseries_subplot_data_modifier_id_fkey FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT timeseries_subplot_data_subplot_id_fk FOREIGN KEY (subplot_id)
        REFERENCES experiment.subplot (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT timeseries_subplot_data_variable_id_fk FOREIGN KEY (variable_id)
        REFERENCES master.variable (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_creator_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (creator_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_is_void_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (is_void ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_modifier_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (modifier_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_subplot_id_data_qc_code_idx
    ON experiment.timeseries_subplot_data USING btree 
    (subplot_id ASC NULLS LAST, data_qc_code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_subplot_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (subplot_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_subplot_id_variable_id_data_value_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (subplot_id ASC NULLS LAST, variable_id ASC NULLS LAST, data_value COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_subplot_id_variable_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (subplot_id ASC NULLS LAST, variable_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_variable_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (variable_id ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX IF NOT EXISTS timeseries_subplot_data_transaction_id_idx 
    ON experiment.timeseries_subplot_data USING btree 
    (transaction_id ASC NULLS LAST)
    TABLESPACE pg_default;


COMMENT ON TABLE experiment.timeseries_subplot_data
    IS 'Time series Subplot Data: Time series data collected from a subplot [TIMESERIES_SUBPLOT_DATA]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.id
    IS 'Time Series Subplot Data ID: Database identifier of the times series subplot data [TIMESERIES_SUBPLOT_DATA_ID]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.subplot_id
    IS 'Subplot ID: Reference to the subplot where the data was collected [TIMESERIES_SUBPLOT_DATA_SUBPLOT_ID]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.variable_id
    IS 'Variable ID: Reference to the variable of the subplot data [TIMESERIES_SUBPLOT_DATA_VAR_ID]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.data_value
    IS 'Subplot Data Value: Value of the data collected from the subplot [TIMESERIES_SUBPLOT_DATA_DATAVAL]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.data_qc_code
    IS 'Subplot Data QC Code: Status of the subplot data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PLOTDATA_QCCODE]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.transaction_id
    IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [TIMESERIES_SUBPLOT_DATA_TXN_ID]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.collection_timestamp
    IS 'Collection Timestamp: Timestamp when the subplot data was collected [TIMESERIES_SUBPLOT_DATA_COLLTSTAMP]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.creator_id
    IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.notes
    IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.timeseries_subplot_data.remarks
    IS 'Remarks: Additional information about the time series subplot data [TIMESERIES_SUBPLOT_DATA_REMARKS]';
COMMENT ON CONSTRAINT timeseries_subplot_data_id_pkey ON experiment.timeseries_subplot_data
    IS 'A times series subplot data is identified by its unique database identifier.';
COMMENT ON CONSTRAINT timeseries_subplot_data_subplot_id_fk ON experiment.timeseries_subplot_data
    IS 'A time series subplot data is collected from one subplot. A subplot can be collected with zero or many subplot data.';
COMMENT ON CONSTRAINT timeseries_subplot_data_variable_id_fk ON experiment.timeseries_subplot_data
    IS 'A time series subplot data refers to one variable. A variable can be referred by zero or many subplot data.';
COMMENT ON INDEX experiment.timeseries_subplot_data_subplot_id_data_qc_code_idx 
    IS 'A subplot data can be retrieved by its data QC code within a subplot.';
COMMENT ON INDEX experiment.timeseries_subplot_data_subplot_id_variable_id_data_value_idx 
    IS 'A subplot data can be retrieved by its variable and data value within a subplot.';
COMMENT ON INDEX experiment.timeseries_subplot_data_subplot_id_variable_id_idx 
    IS 'A subplot data can be retrieved by its variable within a subplot.';

CREATE TRIGGER timeseries_subplot_data_event_log_tgr
    BEFORE INSERT OR UPDATE 
    ON experiment.timeseries_subplot_data
    FOR EACH ROW
    EXECUTE FUNCTION platform.log_record_event();

--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_creator_id_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_is_void_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_modifier_id_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_subplot_id_data_qc_code_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_subplot_id_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_subplot_id_variable_id_data_value_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_subplot_id_variable_id_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_variable_id_idx;
--rollback DROP INDEX IF EXISTS experiment.timeseries_subplot_data_transaction_id_idx;
--rollback DROP TABLE IF EXISTS experiment.timeseries_subplot_data  CASCADE;