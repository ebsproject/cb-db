--liquibase formatted sql

--changeset postgres:update_detach_text_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Update platform.detach_text function



-- create platform.detach_text function
CREATE OR REPLACE FUNCTION
    platform.detach_text(notes text, replace_notes text)
    RETURNS text
AS $_$
DECLARE
    var_text text;
BEGIN
    -- remove specified text
    var_text = TRIM(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(notes, replace_notes, '', 'g'), '(.\s*)\1{1,}', '\1', 'g'), '(.+);', '\1', 'g'));
    
    -- set to null if new text is empty string
    IF TRIM(var_text) = '' THEN
        var_text = NULL;
    END IF;
    
    RETURN var_text;
END $_$
LANGUAGE plpgsql;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION
--rollback     platform.detach_text(notes text, replace_notes text)
--rollback     RETURNS text LANGUAGE sql
--rollback AS $_$
--rollback     SELECT TRIM(REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(notes, replace_notes, '', 'g'), '(.\s*)\1{1,}', '\1', 'g'), '(.+);', '\1', 'g'))
--rollback $_$;
