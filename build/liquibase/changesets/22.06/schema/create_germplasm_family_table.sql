--liquibase formatted sql

--changeset postgres:create_germplasm_family_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1059 Create germplasm.family table



-- create table
CREATE TABLE germplasm.family (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    family_code varchar(128) NOT NULL,
    family_name varchar(256) NOT NULL,
    description text,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT family_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT family_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT family_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
CREATE UNIQUE INDEX family_family_code_uidx
    ON germplasm.family (family_code)
;

ALTER TABLE germplasm.family
    ADD CONSTRAINT family_family_code_ukey
    UNIQUE USING INDEX family_family_code_uidx
;

-- table comment
COMMENT ON TABLE germplasm.family
    IS 'Family: Relationships of germplasm in the genealogy [FAM]';

-- column comments
COMMENT ON COLUMN germplasm.family.family_code
    IS 'Family Code: System identifier of the family [FAM_CODE]';
COMMENT ON COLUMN germplasm.family.family_name
    IS 'Family Name: Name of the family [FAM_NAME]';
COMMENT ON COLUMN germplasm.family.description
    IS 'Description: Additional details about the family [FAM_DESC]';

-- audit column comments
COMMENT ON COLUMN germplasm.family.id
    IS 'Family ID: Database identifier of the record [FAM_ID]';
COMMENT ON COLUMN germplasm.family.remarks
    IS 'Remarks: Additional notes to record [FAM_REMARKS]';
COMMENT ON COLUMN germplasm.family.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [FAM_CTSTAMP]';
COMMENT ON COLUMN germplasm.family.creator_id
    IS 'Creator ID: Reference to the person who created the record [FAM_CPERSON]';
COMMENT ON COLUMN germplasm.family.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [FAM_MTSTAMP]';
COMMENT ON COLUMN germplasm.family.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [FAM_MPERSON]';
COMMENT ON COLUMN germplasm.family.notes
    IS 'Notes: Technical details about the record [FAM_NOTES]';
COMMENT ON COLUMN germplasm.family.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [FAM_ISVOID]';
COMMENT ON COLUMN germplasm.family.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [FAM_EVENTLOG]';

-- indices
CREATE INDEX family_family_code_idx
    ON germplasm.family USING btree (family_code)
;

CREATE INDEX family_family_name_idx
    ON germplasm.family USING btree (family_name)
;

-- audit indices
CREATE INDEX family_creator_id_idx ON germplasm.family USING btree (creator_id);
CREATE INDEX family_modifier_id_idx ON germplasm.family USING btree (modifier_id);
CREATE INDEX family_is_void_idx ON germplasm.family USING btree (is_void);

-- triggers
CREATE TRIGGER family_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.family FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.family;
