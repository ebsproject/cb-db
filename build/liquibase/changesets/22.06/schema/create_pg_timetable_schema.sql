--liquibase formatted sql

--changeset postgres:create_pg_timetable_schema context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM information_schema.schemata WHERE catalog_name = current_database() AND schema_name = 'timetable') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1197 Create pg_timetable schema



--
-- Name: timetable; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA timetable;


--
-- Name: command_kind; Type: TYPE; Schema: timetable; Owner: -
--

CREATE TYPE timetable.command_kind AS ENUM (
    'SQL',
    'PROGRAM',
    'BUILTIN'
);


--
-- Name: cron; Type: DOMAIN; Schema: timetable; Owner: -
--

CREATE DOMAIN timetable.cron AS text
    CONSTRAINT cron_check CHECK ((((substr(VALUE, 1, 6) = ANY (ARRAY['@every'::text, '@after'::text])) AND ((substr(VALUE, 7))::interval IS NOT NULL)) OR (VALUE = '@reboot'::text) OR (VALUE ~ '^(((\d+,)+\d+|(\d+(\/|-)\d+)|(\*(\/|-)\d+)|\d+|\*) +){4}(((\d+,)+\d+|(\d+(\/|-)\d+)|(\*(\/|-)\d+)|\d+|\*) ?)$'::text)));


--
-- Name: DOMAIN cron; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON DOMAIN timetable.cron IS 'Extended CRON-style notation with support of interval values';


--
-- Name: log_type; Type: TYPE; Schema: timetable; Owner: -
--

CREATE TYPE timetable.log_type AS ENUM (
    'DEBUG',
    'NOTICE',
    'INFO',
    'ERROR',
    'PANIC',
    'USER'
);


--
-- Name: active_chain; Type: TABLE; Schema: timetable; Owner: -
--

CREATE UNLOGGED TABLE timetable.active_chain (
    chain_id bigint NOT NULL,
    client_name text NOT NULL,
    started_at timestamp with time zone DEFAULT now()
);


--
-- Name: TABLE active_chain; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.active_chain IS 'Stores information about active chains within session';


--
-- Name: active_session; Type: TABLE; Schema: timetable; Owner: -
--

CREATE UNLOGGED TABLE timetable.active_session (
    client_pid bigint NOT NULL,
    client_name text NOT NULL,
    server_pid bigint NOT NULL,
    started_at timestamp with time zone DEFAULT now()
);


--
-- Name: TABLE active_session; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.active_session IS 'Stores information about active sessions';


--
-- Name: chain; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.chain (
    chain_id bigint NOT NULL,
    chain_name text NOT NULL,
    run_at timetable.cron,
    max_instances integer,
    timeout integer DEFAULT 0,
    live boolean DEFAULT false,
    self_destruct boolean DEFAULT false,
    exclusive_execution boolean DEFAULT false,
    client_name text
);


--
-- Name: task; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.task (
    task_id bigint NOT NULL,
    chain_id bigint,
    task_order double precision NOT NULL,
    task_name text,
    kind timetable.command_kind DEFAULT 'SQL'::timetable.command_kind NOT NULL,
    command text NOT NULL,
    run_as text,
    database_connection text,
    ignore_error boolean DEFAULT false NOT NULL,
    autonomous boolean DEFAULT false NOT NULL,
    timeout integer DEFAULT 0
);


--
-- Name: migration; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.migration (
    id bigint NOT NULL,
    version text NOT NULL
);


--
-- Name: parameter; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.parameter (
    task_id bigint NOT NULL,
    order_id integer NOT NULL,
    value jsonb,
    CONSTRAINT parameter_order_id_check CHECK ((order_id > 0))
);


--
-- Name: execution_log; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.execution_log (
    chain_id bigint,
    task_id bigint,
    last_run timestamp with time zone DEFAULT now(),
    finished timestamp with time zone,
    pid bigint,
    returncode integer,
    kind timetable.command_kind,
    command text,
    output text,
    client_name text NOT NULL
);


--
-- Name: get_client_name(integer); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.get_client_name(integer) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT client_name FROM timetable.active_session WHERE server_pid = $1 LIMIT 1
$_$;


--
-- Name: log; Type: TABLE; Schema: timetable; Owner: -
--

CREATE TABLE timetable.log (
    ts timestamp with time zone DEFAULT now(),
    pid integer NOT NULL,
    log_level timetable.log_type NOT NULL,
    client_name text DEFAULT timetable.get_client_name(pg_backend_pid()),
    message text,
    message_data jsonb
);


--
-- Name: _validate_json_schema_type(text, jsonb); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable._validate_json_schema_type(type text, data jsonb) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
  IF type = 'integer' THEN
    IF jsonb_typeof(data) != 'number' THEN
      RETURN false;
    END IF;
    IF trunc(data::text::numeric) != data::text::numeric THEN
      RETURN false;
    END IF;
  ELSE
    IF type != jsonb_typeof(data) THEN
      RETURN false;
    END IF;
  END IF;
  RETURN true;
END;
$$;


--
-- Name: add_job(text, timetable.cron, text, jsonb, timetable.command_kind, text, integer, boolean, boolean, boolean, boolean); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.add_job(job_name text, job_schedule timetable.cron, job_command text, job_parameters jsonb DEFAULT NULL::jsonb, job_kind timetable.command_kind DEFAULT 'SQL'::timetable.command_kind, job_client_name text DEFAULT NULL::text, job_max_instances integer DEFAULT NULL::integer, job_live boolean DEFAULT true, job_self_destruct boolean DEFAULT false, job_ignore_errors boolean DEFAULT true, job_exclusive boolean DEFAULT false) RETURNS bigint
    LANGUAGE sql
    AS $$
    WITH 
        cte_chain (v_chain_id) AS (
            INSERT INTO timetable.chain (chain_name, run_at, max_instances, live, self_destruct, client_name, exclusive_execution) 
            VALUES (job_name, job_schedule,job_max_instances, job_live, job_self_destruct, job_client_name, job_exclusive)
            RETURNING chain_id
        ),
        cte_task(v_task_id) AS (
            INSERT INTO timetable.task (chain_id, task_order, kind, command, ignore_error, autonomous)
            SELECT v_chain_id, 10, job_kind, job_command, job_ignore_errors, TRUE
            FROM cte_chain
            RETURNING task_id
        ),
        cte_param AS (
            INSERT INTO timetable.parameter (task_id, order_id, value)
            SELECT v_task_id, 1, job_parameters FROM cte_task, cte_chain
        )
        SELECT v_chain_id FROM cte_chain
$$;


--
-- Name: FUNCTION add_job(job_name text, job_schedule timetable.cron, job_command text, job_parameters jsonb, job_kind timetable.command_kind, job_client_name text, job_max_instances integer, job_live boolean, job_self_destruct boolean, job_ignore_errors boolean, job_exclusive boolean); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.add_job(job_name text, job_schedule timetable.cron, job_command text, job_parameters jsonb, job_kind timetable.command_kind, job_client_name text, job_max_instances integer, job_live boolean, job_self_destruct boolean, job_ignore_errors boolean, job_exclusive boolean) IS 'Add one-task chain (aka job) to the system';


--
-- Name: add_task(timetable.command_kind, text, bigint, double precision); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.add_task(kind timetable.command_kind, command text, parent_id bigint, order_delta double precision DEFAULT 10) RETURNS bigint
    LANGUAGE sql
    AS $_$
    INSERT INTO timetable.task (chain_id, task_order, kind, command) 
    SELECT chain_id, task_order + $4, $1, $2 FROM timetable.task WHERE task_id = $3
    RETURNING task_id
$_$;


--
-- Name: FUNCTION add_task(kind timetable.command_kind, command text, parent_id bigint, order_delta double precision); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.add_task(kind timetable.command_kind, command text, parent_id bigint, order_delta double precision) IS 'Add a task to the same chain as the task with parent_id';


--
-- Name: cron_months(timestamp with time zone, integer[]); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.cron_months(from_ts timestamp with time zone, allowed_months integer[]) RETURNS SETOF timestamp with time zone
    LANGUAGE sql STRICT
    AS $$
    WITH
    am(am) AS (SELECT UNNEST(allowed_months)),
    genm(ts) AS ( --generated months
        SELECT date_trunc('month', ts)
        FROM pg_catalog.generate_series(from_ts, from_ts + INTERVAL '1 year', INTERVAL '1 month') g(ts)
    )
    SELECT ts FROM genm JOIN am ON date_part('month', genm.ts) = am.am
$$;


--
-- Name: cron_days(timestamp with time zone, integer[], integer[], integer[]); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.cron_days(from_ts timestamp with time zone, allowed_months integer[], allowed_days integer[], allowed_week_days integer[]) RETURNS SETOF timestamp with time zone
    LANGUAGE sql STRICT
    AS $$
    WITH
    ad(ad) AS (SELECT UNNEST(allowed_days)),
    am(am) AS (SELECT * FROM timetable.cron_months(from_ts, allowed_months)),
    gend(ts) AS ( --generated days
        SELECT date_trunc('day', ts)
        FROM am,
            pg_catalog.generate_series(am.am, am.am + INTERVAL '1 month'
                - INTERVAL '1 day',  -- don't include the same day of the next month
                INTERVAL '1 day') g(ts)
    )
    SELECT ts
    FROM gend JOIN ad ON date_part('day', gend.ts) = ad.ad
    WHERE extract(dow from ts)=ANY(allowed_week_days)
$$;


--
-- Name: cron_split_to_arrays(text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.cron_split_to_arrays(cron text, OUT mins integer[], OUT hours integer[], OUT days integer[], OUT months integer[], OUT dow integer[]) RETURNS record
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    a_element text[];
    i_index integer;
    a_tmp text[];
    tmp_item text;
    a_range int[];
    a_split text[];
    a_res integer[];
    allowed_range integer[];
    max_val integer;
    min_val integer;
BEGIN
    a_element := regexp_split_to_array(cron, '\s+');
    FOR i_index IN 1..5 LOOP
        a_res := NULL;
        a_tmp := string_to_array(a_element[i_index],',');
        CASE i_index -- 1 - mins, 2 - hours, 3 - days, 4 - weeks, 5 - DOWs
            WHEN 1 THEN allowed_range := '{0,59}';
            WHEN 2 THEN allowed_range := '{0,23}';
            WHEN 3 THEN allowed_range := '{1,31}';
            WHEN 4 THEN allowed_range := '{1,12}';
        ELSE
            allowed_range := '{0,7}';
        END CASE;
        FOREACH  tmp_item IN ARRAY a_tmp LOOP
            IF tmp_item ~ '^[0-9]+$' THEN -- normal integer
                a_res := array_append(a_res, tmp_item::int);
            ELSIF tmp_item ~ '^[*]+$' THEN -- '*' any value
                a_range := array(select generate_series(allowed_range[1], allowed_range[2]));
                a_res := array_cat(a_res, a_range);
            ELSIF tmp_item ~ '^[0-9]+[-][0-9]+$' THEN -- '-' range of values
                a_range := regexp_split_to_array(tmp_item, '-');
                a_range := array(select generate_series(a_range[1], a_range[2]));
                a_res := array_cat(a_res, a_range);
            ELSIF tmp_item ~ '^[0-9]+[\/][0-9]+$' THEN -- '/' step values
                a_range := regexp_split_to_array(tmp_item, '/');
                a_range := array(select generate_series(a_range[1], allowed_range[2], a_range[2]));
                a_res := array_cat(a_res, a_range);
            ELSIF tmp_item ~ '^[0-9-]+[\/][0-9]+$' THEN -- '-' range of values and '/' step values
                a_split := regexp_split_to_array(tmp_item, '/');
                a_range := regexp_split_to_array(a_split[1], '-');
                a_range := array(select generate_series(a_range[1], a_range[2], a_split[2]::int));
                a_res := array_cat(a_res, a_range);
            ELSIF tmp_item ~ '^[*]+[\/][0-9]+$' THEN -- '*' any value and '/' step values
                a_split := regexp_split_to_array(tmp_item, '/');
                a_range := array(select generate_series(allowed_range[1], allowed_range[2], a_split[2]::int));
                a_res := array_cat(a_res, a_range);
            ELSE
                RAISE EXCEPTION 'Value ("%") not recognized', a_element[i_index]
                    USING HINT = 'fields separated by space or tab.'+
                       'Values allowed: numbers (value list with ","), '+
                    'any value with "*", range of value with "-" and step values with "/"!';
            END IF;
        END LOOP;
        SELECT
           ARRAY_AGG(x.val), MIN(x.val), MAX(x.val) INTO a_res, min_val, max_val
        FROM (
            SELECT DISTINCT UNNEST(a_res) AS val ORDER BY val) AS x;
        IF max_val > allowed_range[2] OR min_val < allowed_range[1] THEN
            RAISE EXCEPTION '% is out of range: %', a_res, allowed_range;
        END IF;
        CASE i_index
                  WHEN 1 THEN mins := a_res;
            WHEN 2 THEN hours := a_res;
            WHEN 3 THEN days := a_res;
            WHEN 4 THEN months := a_res;
        ELSE
            dow := a_res;
        END CASE;
    END LOOP;
    RETURN;
END;
$_$;


--
-- Name: cron_times(integer[], integer[]); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.cron_times(allowed_hours integer[], allowed_minutes integer[]) RETURNS SETOF time without time zone
    LANGUAGE sql STRICT
    AS $$
    WITH
    ah(ah) AS (SELECT UNNEST(allowed_hours)),
    am(am) AS (SELECT UNNEST(allowed_minutes))
    SELECT make_time(ah.ah, am.am, 0) FROM ah CROSS JOIN am
$$;


--
-- Name: cron_runs(timestamp with time zone, text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.cron_runs(from_ts timestamp with time zone, cron text) RETURNS SETOF timestamp with time zone
    LANGUAGE sql STRICT
    AS $$
    SELECT cd + ct
    FROM
        timetable.cron_split_to_arrays(cron) a,
        timetable.cron_times(a.hours, a.mins) ct CROSS JOIN
        timetable.cron_days(from_ts, a.months, a.days, a.dow) cd
    WHERE cd + ct > from_ts
    ORDER BY 1 ASC;
$$;


--
-- Name: delete_job(text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.delete_job(job_name text) RETURNS boolean
    LANGUAGE sql
    AS $_$
    WITH del_chain AS (DELETE FROM timetable.chain WHERE chain.chain_name = $1 RETURNING chain_id)
    SELECT EXISTS(SELECT 1 FROM del_chain)
$_$;


--
-- Name: FUNCTION delete_job(job_name text); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.delete_job(job_name text) IS 'Delete the chain and its tasks from the system';


--
-- Name: delete_task(bigint); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.delete_task(task_id bigint) RETURNS boolean
    LANGUAGE sql
    AS $_$
    WITH del_task AS (DELETE FROM timetable.task WHERE task_id = $1 RETURNING task_id)
    SELECT EXISTS(SELECT 1 FROM del_task)
$_$;


--
-- Name: FUNCTION delete_task(task_id bigint); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.delete_task(task_id bigint) IS 'Delete the task from a chain';


--
-- Name: is_cron_in_time(timetable.cron, timestamp with time zone); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.is_cron_in_time(run_at timetable.cron, ts timestamp with time zone) RETURNS boolean
    LANGUAGE sql
    AS $$
    SELECT
    CASE WHEN run_at IS NULL THEN
        TRUE
    ELSE
        date_part('month', ts) = ANY(a.months)
        AND (date_part('dow', ts) = ANY(a.dow) OR date_part('isodow', ts) = ANY(a.dow))
        AND date_part('day', ts) = ANY(a.days)
        AND date_part('hour', ts) = ANY(a.hours)
        AND date_part('minute', ts) = ANY(a.mins)
    END
    FROM
        timetable.cron_split_to_arrays(run_at) a
$$;


--
-- Name: move_task_down(bigint); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.move_task_down(task_id bigint) RETURNS boolean
    LANGUAGE sql
    AS $_$
    WITH current_task (ct_chain_id, ct_id, ct_order) AS (
        SELECT chain_id, task_id, task_order FROM timetable.task WHERE task_id = $1
    ),
    tasks(t_id, t_new_order) AS (
        SELECT task_id, COALESCE(LAG(task_order) OVER w, LEAD(task_order) OVER w)
        FROM timetable.task t, current_task ct
        WHERE chain_id = ct_chain_id AND (task_order > ct_order OR task_id = ct_id)
        WINDOW w AS (PARTITION BY chain_id ORDER BY ABS(task_order - ct_order))
        LIMIT 2
    ),
    upd AS (
        UPDATE timetable.task t SET task_order = t_new_order
        FROM tasks WHERE tasks.t_id = t.task_id AND tasks.t_new_order IS NOT NULL
        RETURNING true
    )
    SELECT COUNT(*) > 0 FROM upd
$_$;


--
-- Name: FUNCTION move_task_down(task_id bigint); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.move_task_down(task_id bigint) IS 'Switch the order of the task execution with a following task within the chain';


--
-- Name: move_task_up(bigint); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.move_task_up(task_id bigint) RETURNS boolean
    LANGUAGE sql
    AS $_$
    WITH current_task (ct_chain_id, ct_id, ct_order) AS (
        SELECT chain_id, task_id, task_order FROM timetable.task WHERE task_id = $1
    ),
    tasks(t_id, t_new_order) AS (
        SELECT task_id, COALESCE(LAG(task_order) OVER w, LEAD(task_order) OVER w)
        FROM timetable.task t, current_task ct
        WHERE chain_id = ct_chain_id AND (task_order < ct_order OR task_id = ct_id)
        WINDOW w AS (PARTITION BY chain_id ORDER BY ABS(task_order - ct_order))
        LIMIT 2
    ),
    upd AS (
        UPDATE timetable.task t SET task_order = t_new_order
        FROM tasks WHERE tasks.t_id = t.task_id AND tasks.t_new_order IS NOT NULL
        RETURNING true
    )
    SELECT COUNT(*) > 0 FROM upd
$_$;


--
-- Name: FUNCTION move_task_up(task_id bigint); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.move_task_up(task_id bigint) IS 'Switch the order of the task execution with a previous task within the chain';


--
-- Name: next_run(timetable.cron); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.next_run(cron timetable.cron) RETURNS timestamp with time zone
    LANGUAGE sql STRICT
    AS $$
    SELECT * FROM timetable.cron_runs(now(), cron) LIMIT 1
$$;


--
-- Name: notify_chain_start(bigint, text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.notify_chain_start(chain_id bigint, worker_name text) RETURNS void
    LANGUAGE sql
    AS $$
    SELECT pg_notify(
        worker_name, 
        format('{"ConfigID": %s, "Command": "START", "Ts": %s}', 
        chain_id, 
        EXTRACT(epoch FROM clock_timestamp())::bigint)
    )
$$;


--
-- Name: FUNCTION notify_chain_start(chain_id bigint, worker_name text); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.notify_chain_start(chain_id bigint, worker_name text) IS 'Send notification to the worker to start the chain';


--
-- Name: notify_chain_stop(bigint, text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.notify_chain_stop(chain_id bigint, worker_name text) RETURNS void
    LANGUAGE sql
    AS $$ 
    SELECT pg_notify(
        worker_name, 
        format('{"ConfigID": %s, "Command": "STOP", "Ts": %s}', 
            chain_id, 
            EXTRACT(epoch FROM clock_timestamp())::bigint)
        )
$$;


--
-- Name: FUNCTION notify_chain_stop(chain_id bigint, worker_name text); Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON FUNCTION timetable.notify_chain_stop(chain_id bigint, worker_name text) IS 'Send notification to the worker to stop the chain';


--
-- Name: try_lock_client_name(bigint, text); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.try_lock_client_name(worker_pid bigint, worker_name text) RETURNS boolean
    LANGUAGE plpgsql STRICT
    AS $$
BEGIN
    IF pg_is_in_recovery() THEN
        RAISE NOTICE 'Cannot obtain lock on a replica. Please, use the primary node';
        RETURN FALSE;
    END IF;
    -- remove disconnected sessions
    DELETE
        FROM timetable.active_session
        WHERE server_pid NOT IN (
            SELECT pid
            FROM pg_catalog.pg_stat_activity
            WHERE application_name = 'pg_timetable'
        );
    DELETE 
        FROM timetable.active_chain 
        WHERE client_name NOT IN (
            SELECT client_name FROM timetable.active_session
        );
    -- check if there any active sessions with the client name but different client pid
    PERFORM 1
        FROM timetable.active_session s
        WHERE
            s.client_pid <> worker_pid
            AND s.client_name = worker_name
        LIMIT 1;
    IF FOUND THEN
        RAISE NOTICE 'Another client is already connected to server with name: %', worker_name;
        RETURN FALSE;
    END IF;
    -- insert current session information
    INSERT INTO timetable.active_session(client_pid, client_name, server_pid) VALUES (worker_pid, worker_name, pg_backend_pid());
    RETURN TRUE;
END;
$$;


--
-- Name: validate_json_schema(jsonb, jsonb, jsonb); Type: FUNCTION; Schema: timetable; Owner: -
--

CREATE FUNCTION timetable.validate_json_schema(schema jsonb, data jsonb, root_schema jsonb DEFAULT NULL::jsonb) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
DECLARE
  prop text;
  item jsonb;
  path text[];
  types text[];
  pattern text;
  props text[];
BEGIN

  IF root_schema IS NULL THEN
    root_schema = schema;
  END IF;

  IF schema ? 'type' THEN
    IF jsonb_typeof(schema->'type') = 'array' THEN
      types = ARRAY(SELECT jsonb_array_elements_text(schema->'type'));
    ELSE
      types = ARRAY[schema->>'type'];
    END IF;
    IF (SELECT NOT bool_or(timetable._validate_json_schema_type(type, data)) FROM unnest(types) type) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'properties' THEN
    FOR prop IN SELECT jsonb_object_keys(schema->'properties') LOOP
      IF data ? prop AND NOT timetable.validate_json_schema(schema->'properties'->prop, data->prop, root_schema) THEN
        RETURN false;
      END IF;
    END LOOP;
  END IF;

  IF schema ? 'required' AND jsonb_typeof(data) = 'object' THEN
    IF NOT ARRAY(SELECT jsonb_object_keys(data)) @>
           ARRAY(SELECT jsonb_array_elements_text(schema->'required')) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'items' AND jsonb_typeof(data) = 'array' THEN
    IF jsonb_typeof(schema->'items') = 'object' THEN
      FOR item IN SELECT jsonb_array_elements(data) LOOP
        IF NOT timetable.validate_json_schema(schema->'items', item, root_schema) THEN
          RETURN false;
        END IF;
      END LOOP;
    ELSE
      IF NOT (
        SELECT bool_and(i > jsonb_array_length(schema->'items') OR timetable.validate_json_schema(schema->'items'->(i::int - 1), elem, root_schema))
        FROM jsonb_array_elements(data) WITH ORDINALITY AS t(elem, i)
      ) THEN
        RETURN false;
      END IF;
    END IF;
  END IF;

  IF jsonb_typeof(schema->'additionalItems') = 'boolean' and NOT (schema->'additionalItems')::text::boolean AND jsonb_typeof(schema->'items') = 'array' THEN
    IF jsonb_array_length(data) > jsonb_array_length(schema->'items') THEN
      RETURN false;
    END IF;
  END IF;

  IF jsonb_typeof(schema->'additionalItems') = 'object' THEN
    IF NOT (
        SELECT bool_and(timetable.validate_json_schema(schema->'additionalItems', elem, root_schema))
        FROM jsonb_array_elements(data) WITH ORDINALITY AS t(elem, i)
        WHERE i > jsonb_array_length(schema->'items')
      ) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'minimum' AND jsonb_typeof(data) = 'number' THEN
    IF data::text::numeric < (schema->>'minimum')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'maximum' AND jsonb_typeof(data) = 'number' THEN
    IF data::text::numeric > (schema->>'maximum')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF COALESCE((schema->'exclusiveMinimum')::text::bool, FALSE) THEN
    IF data::text::numeric = (schema->>'minimum')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF COALESCE((schema->'exclusiveMaximum')::text::bool, FALSE) THEN
    IF data::text::numeric = (schema->>'maximum')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'anyOf' THEN
    IF NOT (SELECT bool_or(timetable.validate_json_schema(sub_schema, data, root_schema)) FROM jsonb_array_elements(schema->'anyOf') sub_schema) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'allOf' THEN
    IF NOT (SELECT bool_and(timetable.validate_json_schema(sub_schema, data, root_schema)) FROM jsonb_array_elements(schema->'allOf') sub_schema) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'oneOf' THEN
    IF 1 != (SELECT COUNT(*) FROM jsonb_array_elements(schema->'oneOf') sub_schema WHERE timetable.validate_json_schema(sub_schema, data, root_schema)) THEN
      RETURN false;
    END IF;
  END IF;

  IF COALESCE((schema->'uniqueItems')::text::boolean, false) THEN
    IF (SELECT COUNT(*) FROM jsonb_array_elements(data)) != (SELECT count(DISTINCT val) FROM jsonb_array_elements(data) val) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'additionalProperties' AND jsonb_typeof(data) = 'object' THEN
    props := ARRAY(
      SELECT key
      FROM jsonb_object_keys(data) key
      WHERE key NOT IN (SELECT jsonb_object_keys(schema->'properties'))
        AND NOT EXISTS (SELECT * FROM jsonb_object_keys(schema->'patternProperties') pat WHERE key ~ pat)
    );
    IF jsonb_typeof(schema->'additionalProperties') = 'boolean' THEN
      IF NOT (schema->'additionalProperties')::text::boolean AND jsonb_typeof(data) = 'object' AND NOT props <@ ARRAY(SELECT jsonb_object_keys(schema->'properties')) THEN
        RETURN false;
      END IF;
    ELSEIF NOT (
      SELECT bool_and(timetable.validate_json_schema(schema->'additionalProperties', data->key, root_schema))
      FROM unnest(props) key
    ) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? '$ref' THEN
    path := ARRAY(
      SELECT regexp_replace(regexp_replace(path_part, '~1', '/'), '~0', '~')
      FROM UNNEST(regexp_split_to_array(schema->>'$ref', '/')) path_part
    );
    -- ASSERT path[1] = '#', 'only refs anchored at the root are supported';
    IF NOT timetable.validate_json_schema(root_schema #> path[2:array_length(path, 1)], data, root_schema) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'enum' THEN
    IF NOT EXISTS (SELECT * FROM jsonb_array_elements(schema->'enum') val WHERE val = data) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'minLength' AND jsonb_typeof(data) = 'string' THEN
    IF char_length(data #>> '{}') < (schema->>'minLength')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'maxLength' AND jsonb_typeof(data) = 'string' THEN
    IF char_length(data #>> '{}') > (schema->>'maxLength')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'not' THEN
    IF timetable.validate_json_schema(schema->'not', data, root_schema) THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'maxProperties' AND jsonb_typeof(data) = 'object' THEN
    IF (SELECT count(*) FROM jsonb_object_keys(data)) > (schema->>'maxProperties')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'minProperties' AND jsonb_typeof(data) = 'object' THEN
    IF (SELECT count(*) FROM jsonb_object_keys(data)) < (schema->>'minProperties')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'maxItems' AND jsonb_typeof(data) = 'array' THEN
    IF (SELECT count(*) FROM jsonb_array_elements(data)) > (schema->>'maxItems')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'minItems' AND jsonb_typeof(data) = 'array' THEN
    IF (SELECT count(*) FROM jsonb_array_elements(data)) < (schema->>'minItems')::numeric THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'dependencies' THEN
    FOR prop IN SELECT jsonb_object_keys(schema->'dependencies') LOOP
      IF data ? prop THEN
        IF jsonb_typeof(schema->'dependencies'->prop) = 'array' THEN
          IF NOT (SELECT bool_and(data ? dep) FROM jsonb_array_elements_text(schema->'dependencies'->prop) dep) THEN
            RETURN false;
          END IF;
        ELSE
          IF NOT timetable.validate_json_schema(schema->'dependencies'->prop, data, root_schema) THEN
            RETURN false;
          END IF;
        END IF;
      END IF;
    END LOOP;
  END IF;

  IF schema ? 'pattern' AND jsonb_typeof(data) = 'string' THEN
    IF (data #>> '{}') !~ (schema->>'pattern') THEN
      RETURN false;
    END IF;
  END IF;

  IF schema ? 'patternProperties' AND jsonb_typeof(data) = 'object' THEN
    FOR prop IN SELECT jsonb_object_keys(data) LOOP
      FOR pattern IN SELECT jsonb_object_keys(schema->'patternProperties') LOOP
        RAISE NOTICE 'prop %s, pattern %, schema %', prop, pattern, schema->'patternProperties'->pattern;
        IF prop ~ pattern AND NOT timetable.validate_json_schema(schema->'patternProperties'->pattern, data->prop, root_schema) THEN
          RETURN false;
        END IF;
      END LOOP;
    END LOOP;
  END IF;

  IF schema ? 'multipleOf' AND jsonb_typeof(data) = 'number' THEN
    IF data::text::numeric % (schema->>'multipleOf')::numeric != 0 THEN
      RETURN false;
    END IF;
  END IF;

  RETURN true;
END;
$_$;


SET default_tablespace = '';

SET default_table_access_method = heap;


--
-- Name: TABLE chain; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.chain IS 'Stores information about chains schedule';


--
-- Name: COLUMN chain.run_at; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.run_at IS 'Extended CRON-style time notation the chain has to be run at';


--
-- Name: COLUMN chain.max_instances; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.max_instances IS 'Number of instances (clients) this chain can run in parallel';


--
-- Name: COLUMN chain.timeout; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.timeout IS 'Abort any chain that takes more than the specified number of milliseconds';


--
-- Name: COLUMN chain.live; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.live IS 'Indication that the chain is ready to run, set to FALSE to pause execution';


--
-- Name: COLUMN chain.self_destruct; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.self_destruct IS 'Indication that this chain will delete itself after successful run';


--
-- Name: COLUMN chain.exclusive_execution; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.exclusive_execution IS 'All parallel chains should be paused while executing this chain';


--
-- Name: COLUMN chain.client_name; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.chain.client_name IS 'Only client with this name is allowed to run this chain, set to NULL to allow any client';


--
-- Name: chain_chain_id_seq; Type: SEQUENCE; Schema: timetable; Owner: -
--

CREATE SEQUENCE timetable.chain_chain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: chain_chain_id_seq; Type: SEQUENCE OWNED BY; Schema: timetable; Owner: -
--

ALTER SEQUENCE timetable.chain_chain_id_seq OWNED BY timetable.chain.chain_id;


--
-- Name: TABLE execution_log; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.execution_log IS 'Stores log entries of executed tasks and chains';


--
-- Name: TABLE log; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.log IS 'Stores log entries of active sessions';


--
-- Name: TABLE parameter; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.parameter IS 'Stores parameters passed as arguments to a chain task';


--
-- Name: TABLE task; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON TABLE timetable.task IS 'Holds information about chain elements aka tasks';


--
-- Name: COLUMN task.chain_id; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.chain_id IS 'Link to the chain, if NULL task considered to be disabled';


--
-- Name: COLUMN task.task_order; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.task_order IS 'Indicates the order of task within a chain';


--
-- Name: COLUMN task.kind; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.kind IS 'Indicates whether "command" is SQL, built-in function or an external program';


--
-- Name: COLUMN task.command; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.command IS 'Contains either an SQL command, or command string to be executed';


--
-- Name: COLUMN task.run_as; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.run_as IS 'Role name to run task as. Uses SET ROLE for SQL commands';


--
-- Name: COLUMN task.ignore_error; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.ignore_error IS 'Indicates whether a next task in a chain can be executed regardless of the success of the current one';


--
-- Name: COLUMN task.timeout; Type: COMMENT; Schema: timetable; Owner: -
--

COMMENT ON COLUMN timetable.task.timeout IS 'Abort any task within a chain that takes more than the specified number of milliseconds';


--
-- Name: task_task_id_seq; Type: SEQUENCE; Schema: timetable; Owner: -
--

CREATE SEQUENCE timetable.task_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_task_id_seq; Type: SEQUENCE OWNED BY; Schema: timetable; Owner: -
--

ALTER SEQUENCE timetable.task_task_id_seq OWNED BY timetable.task.task_id;


--
-- Name: chain chain_id; Type: DEFAULT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.chain ALTER COLUMN chain_id SET DEFAULT nextval('timetable.chain_chain_id_seq'::regclass);


--
-- Name: task task_id; Type: DEFAULT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.task ALTER COLUMN task_id SET DEFAULT nextval('timetable.task_task_id_seq'::regclass);


--
-- Name: chain chain_chain_name_key; Type: CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.chain
    ADD CONSTRAINT chain_chain_name_key UNIQUE (chain_name);


--
-- Name: chain chain_pkey; Type: CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.chain
    ADD CONSTRAINT chain_pkey PRIMARY KEY (chain_id);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (id);


--
-- Name: parameter parameter_pkey; Type: CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.parameter
    ADD CONSTRAINT parameter_pkey PRIMARY KEY (task_id, order_id);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (task_id);


--
-- Name: parameter parameter_task_id_fkey; Type: FK CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.parameter
    ADD CONSTRAINT parameter_task_id_fkey FOREIGN KEY (task_id) REFERENCES timetable.task(task_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: task task_chain_id_fkey; Type: FK CONSTRAINT; Schema: timetable; Owner: -
--

ALTER TABLE ONLY timetable.task
    ADD CONSTRAINT task_chain_id_fkey FOREIGN KEY (chain_id) REFERENCES timetable.chain(chain_id) ON UPDATE CASCADE ON DELETE CASCADE;



-- revert changes
--rollback DROP SCHEMA timetable CASCADE;
