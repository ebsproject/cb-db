--liquibase formatted sql

--changeset postgres:update_germplasm_generate_code_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1059 Update germplasm.generate_code() function



-- include family to the code generation function
CREATE OR REPLACE FUNCTION germplasm.generate_code (
        entity character varying,
        prefix character varying DEFAULT NULL::character varying,
        custom_value character varying DEFAULT NULL::character varying,
        OUT code character varying
    )
    RETURNS character varying
    LANGUAGE plpgsql
AS $function$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
    
    IF (prefix IS NULL) THEN
        IF (entity = 'germplasm') THEN
            entity_code = 'GE';
        ELSIF (entity = 'seed') THEN
            entity_code = 'SEED';
        ELSIF (entity = 'package') THEN
            entity_code = 'PKG';
        ELSIF (entity = 'family') THEN
            entity_code = 'FAM';
        END IF;
    ELSE
        entity_code = upper(regexp_replace(prefix, '\s', ''));
    END IF;
    
    IF (custom_value IS NULL) THEN
        code := entity_code || lpad(seq_id::varchar, 12, '0');
    ELSE
        code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
    END IF;
END; $function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION germplasm.generate_code (
--rollback         entity character varying,
--rollback         prefix character varying DEFAULT NULL::character varying,
--rollback         custom_value character varying DEFAULT NULL::character varying,
--rollback         OUT code character varying
--rollback     )
--rollback     RETURNS character varying
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     seq_id bigint;
--rollback     entity_code varchar;
--rollback BEGIN
--rollback     SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
--rollback     
--rollback     IF (prefix IS NULL) THEN
--rollback         IF (entity = 'germplasm') THEN
--rollback             entity_code = 'GE';
--rollback         ELSIF (entity = 'seed') THEN
--rollback             entity_code = 'SEED';
--rollback         ELSIF (entity = 'package') THEN
--rollback             entity_code = 'PKG';
--rollback         END IF;
--rollback     ELSE
--rollback         entity_code = upper(regexp_replace(prefix, '\s', ''));
--rollback     END IF;
--rollback     
--rollback     IF (custom_value IS NULL) THEN
--rollback         code := entity_code || lpad(seq_id::varchar, 12, '0');
--rollback     ELSE
--rollback         code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
--rollback     END IF;
--rollback END; $function$
--rollback ;
