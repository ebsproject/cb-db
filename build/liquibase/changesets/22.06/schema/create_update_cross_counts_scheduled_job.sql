--liquibase formatted sql

--changeset postgres:create_update_cross_counts_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1197 Create experiment.update_cross_counts() function



-- delete existing function if any
DROP FUNCTION IF EXISTS
    experiment.update_cross_counts()
;

-- create function
CREATE OR REPLACE FUNCTION
    experiment.update_cross_counts()
RETURNS
    VOID
AS $BODY$
DECLARE
BEGIN
    UPDATE
        experiment.entry_list AS entlist
    SET
        cross_count = t.cross_count
    FROM (
            SELECT
                expt.id AS experiment_id,
                entlist.id AS entry_list_id,
                count(*) AS cross_count
            FROM
                experiment.experiment AS expt
                JOIN experiment.entry_list AS entlist
                    ON entlist.experiment_id = expt.id
                JOIN germplasm.cross AS crs
                    ON crs.experiment_id = expt.id
            WHERE
                entlist.is_void = FALSE
                AND expt.is_void = FALSE
                AND crs.is_void = FALSE
            GROUP BY
                expt.id,
                entlist.id
        ) AS t
    WHERE
        t.entry_list_id = entlist.id
    ;
END; $BODY$
LANGUAGE
    PLPGSQL
;



-- revert changes
--rollback DROP FUNCTION IF EXISTS
--rollback     experiment.update_cross_counts()
--rollback ;



--changeset postgres:grant_privileges_to_objects context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1197 Grant privileges to objects


-- grant privileges to objects used in the function
GRANT UPDATE ON TABLE experiment.entry_list TO scheduler;
GRANT EXECUTE ON FUNCTION experiment.update_cross_counts() TO scheduler;



-- revert changes
--rollback REVOKE UPDATE ON TABLE experiment.entry_list FROM scheduler;
--rollback REVOKE EXECUTE ON FUNCTION experiment.update_cross_counts() FROM scheduler;



--changeset postgres:add_update_cross_counts_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1197 Add experiment.update_cross_counts job



-- run every 11pm during workdays
SELECT timetable.add_job('experiment.update_cross_counts', '0 11 * * 1,2,3,4,5', 'SELECT experiment.update_cross_counts()');



-- revert changes
--rollback SELECT timetable.delete_job('experiment.update_cross_counts');
