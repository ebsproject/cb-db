--liquibase formatted sql

--changeset postgres:create_indices_for_germplasm_parentage_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1259 Create indices for germplasm parentage column



-- create index for equal searches (=)
CREATE INDEX germplasm_parentage_idx
    ON germplasm.germplasm USING btree (parentage)
;

-- create index for partial searches (like)
CREATE INDEX germplasm_parentage_gin_trgm_idx
    ON germplasm.germplasm USING gin (parentage gin_trgm_ops)
;



-- revert changes
--rollback DROP INDEX germplasm.germplasm_parentage_idx;
--rollback DROP INDEX germplasm.germplasm_parentage_gin_trgm_idx;
