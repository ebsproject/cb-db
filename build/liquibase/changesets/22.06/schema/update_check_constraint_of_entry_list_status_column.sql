--liquibase formatted sql

--changeset postgres:update_check_constraint_to_entry_list_status_column context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM experiment.entry_list WHERE entry_list_status = 'created' LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1252 Update check constraint of entry_list_status column
--validCheckSum: 8:f640fbb05f77d97e552050783b074f7e



-- replace check constraint
ALTER TABLE experiment.entry_list
    ADD CONSTRAINT entry_list_entry_list_status_chk
    CHECK (entry_list_status = ANY(ARRAY['draft', 'completed', 'finalized', 'parents added', 'parent list specified', 'crosses added', 'cross list specified', 'deletion in progress']::text[]))
;



-- revert changes
--rollback ALTER TABLE experiment.entry_list
--rollback     DROP CONSTRAINT entry_list_entry_list_status_chk
--rollback ;
