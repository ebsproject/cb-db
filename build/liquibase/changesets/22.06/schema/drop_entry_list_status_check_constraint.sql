--liquibase formatted sql

--changeset postgres:drop_entry_list_status_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM public.databasechangelog WHERE id = 'update_entry_list_status_column_values' AND comments LIKE 'DB-1252%') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1287 Drop entry_list_status check constraint



-- drop temporarily
ALTER TABLE experiment.entry_list
    DROP CONSTRAINT entry_list_entry_list_status_chk
;



-- revert changes
--rollback ALTER TABLE experiment.entry_list
--rollback     ADD CONSTRAINT entry_list_entry_list_status_chk
--rollback     CHECK (entry_list_status = ANY(ARRAY['draft', 'completed', 'finalized']::text[]))
--rollback ;
