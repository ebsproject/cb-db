--liquibase formatted sql

--changeset postgres:create_experiment_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create experiment_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX experiment_experiment_name_upper_idx
    ON experiment.experiment USING btree (UPPER(experiment_name))
;

-- create gin index for partial like/ilike-searched column
CREATE INDEX experiment_experiment_name_gin_trgm_idx
    ON experiment.experiment USING gin (experiment_name gin_trgm_ops)
;

-- update table's query plan
ANALYZE experiment.experiment;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX experiment.experiment_experiment_name_upper_idx;
--rollback 
--rollback -- drop gin index for partial like/ilike-searched column
--rollback DROP INDEX experiment.experiment_experiment_name_gin_trgm_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE experiment.experiment;



--changeset postgres:create_entry_list_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create entry_list_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX entry_list_entry_list_name_upper_idx
    ON experiment.entry_list USING btree (UPPER(entry_list_name))
;

-- create gin index for partial like/ilike-searched column
CREATE INDEX entry_list_entry_list_name_gin_trgm_idx
    ON experiment.entry_list USING gin (entry_list_name gin_trgm_ops)
;

-- update table's query plan
ANALYZE experiment.entry_list;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX experiment.entry_list_entry_list_name_upper_idx;
--rollback 
--rollback -- drop gin index for partial like/ilike-searched column
--rollback DROP INDEX experiment.entry_list_entry_list_name_gin_trgm_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE experiment.entry_list;



--changeset postgres:create_occurrence_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create occurrence_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX occurrence_occurrence_name_upper_idx
    ON experiment.occurrence USING btree (UPPER(occurrence_name))
;

-- create gin index for partial like/ilike-searched column
CREATE INDEX occurrence_occurrence_name_gin_trgm_idx
    ON experiment.occurrence USING gin (occurrence_name gin_trgm_ops)
;

-- update table's query plan
ANALYZE experiment.occurrence;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX experiment.occurrence_occurrence_name_upper_idx;
--rollback 
--rollback -- drop gin index for partial like/ilike-searched column
--rollback DROP INDEX experiment.occurrence_occurrence_name_gin_trgm_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE experiment.occurrence;



--changeset postgres:create_location_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create location_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX location_location_name_upper_idx
    ON experiment.location USING btree (UPPER(location_name))
;

-- create gin index for partial like/ilike-searched column
CREATE INDEX location_location_name_gin_trgm_idx
    ON experiment.location USING gin (location_name gin_trgm_ops)
;

-- update table's query plan
ANALYZE experiment.location;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX experiment.location_location_name_upper_idx;
--rollback 
--rollback -- drop gin index for partial like/ilike-searched column
--rollback DROP INDEX experiment.location_location_name_gin_trgm_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE experiment.location;
