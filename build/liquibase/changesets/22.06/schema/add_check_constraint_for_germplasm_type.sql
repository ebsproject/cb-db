--liquibase formatted sql

--changeset postgres:add_check_constraint_for_germplasm_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1232 Add check constraint for germplasm_type in germplasm.germplasm



ALTER TABLE germplasm.germplasm
    ADD CONSTRAINT germplasm_germplasm_type_chk 
    CHECK (germplasm_type::text = ANY (ARRAY['accession'::text, 'backcross'::text, 'CMS_line'::text, 'doubled_haploid'::text, 'F1'::text, 'F1F'::text, 'F1TOP'::text, 'fixed_inbred_line'::text, 'fixed_line'::text, 'gene_construct'::text, 'genome_edited'::text, 'haploid'::text, 'hybrid'::text, 'inbred_line'::text, 'progeny'::text, 'segregating'::text, 'transgenic'::text, 'experimental_variety'::text, 'landrace'::text, 'population'::text, 'synthetic'::text, 'unknown'::text]));



--rollback ALTER TABLE 
--rollback     germplasm.germplasm 
--rollback DROP CONSTRAINT 
--rollback     germplasm_germplasm_type_chk;