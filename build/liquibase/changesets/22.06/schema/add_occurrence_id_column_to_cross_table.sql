--liquibase formatted sql

--changeset postgres:add_occurrence_id_column_cross_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1251 Add occurrence_id column to germplasm.cross table



-- add occurrence_id column
ALTER TABLE
    germplasm.cross
ADD COLUMN
    occurrence_id integer
;

COMMENT ON COLUMN germplasm.cross.occurrence_id
    IS 'Occurrence ID: Reference to the occurrence where the cross is made (where the female parent is planted) [CROSS_OCC_ID]';



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.cross
--rollback DROP COLUMN
--rollback     occurrence_id
--rollback ;



--changeset postgres:add_occurrence_id_foreign_key_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1251 Add occurrence_id foreign key constraint



ALTER TABLE germplasm.cross
    ADD CONSTRAINT cross_occurrence_id_fk
    FOREIGN KEY (occurrence_id)
    REFERENCES experiment.occurrence (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT cross_occurrence_id_fk
    ON germplasm.cross
    IS 'A cross may be planted in zero or one occurrence. An occurrence can have zero or more crosses.'
;



-- revert changes
--rollback ALTER TABLE germplasm.cross
--rollback     DROP CONSTRAINT cross_occurrence_id_fk
--rollback ;



--changeset postgres:add_index_to_occurrence_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1251 Add index to occurrence_id column



CREATE INDEX cross_occurrence_id_idx
    ON germplasm.cross USING btree (occurrence_id)
;



-- revert changes
--rollback DROP INDEX germplasm.cross_occurrence_id_idx;
