--liquibase formatted sql

--changeset postgres:create_germplasm_family_member_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1059 Create germplasm.family_member table



-- create table
CREATE TABLE germplasm.family_member (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    family_id integer NOT NULL,
    germplasm_id integer NOT NULL,
    order_number integer NOT NULL DEFAULT 0,
    cross_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT family_member_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT family_member_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT family_member_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE germplasm.family_member
    ADD CONSTRAINT family_member_family_id_fk
    FOREIGN KEY (family_id)
    REFERENCES germplasm.family (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT family_member_family_id_fk
    ON germplasm.family_member
    IS 'A family can have zero or more family members (germplasm). A family member (germplasm) belongs to only one family.'
;

ALTER TABLE germplasm.family_member
    ADD CONSTRAINT family_member_germplasm_id_fk
    FOREIGN KEY (germplasm_id)
    REFERENCES germplasm.germplasm (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT family_member_germplasm_id_fk
    ON germplasm.family_member
    IS 'A family member (germplasm) belongs to only one family. A family can have zero or more family members (germplasm).'
;

CREATE UNIQUE INDEX family_member_family_id_germplasm_id_uidx
    ON germplasm.family_member (family_id, germplasm_id)
;

ALTER TABLE germplasm.family_member
    ADD CONSTRAINT family_member_family_id_germplasm_id_ukey
    UNIQUE USING INDEX family_member_family_id_germplasm_id_uidx
;

CREATE UNIQUE INDEX family_member_family_id_order_number_uidx
    ON germplasm.family_member (family_id, order_number)
;

ALTER TABLE germplasm.family_member
    ADD CONSTRAINT family_member_family_id_order_number_ukey
    UNIQUE USING INDEX family_member_family_id_order_number_uidx
;

ALTER TABLE germplasm.family_member
    ADD CONSTRAINT family_member_cross_id_fk
    FOREIGN KEY (cross_id)
    REFERENCES germplasm.family (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT family_member_cross_id_fk
    ON germplasm.family_member
    IS 'A family member may refer to zero or one cross. A cross may have zero or more family members.'
;

CREATE INDEX family_member_cross_id_idx
    ON germplasm.family_member USING btree (cross_id)
;

-- table comment
COMMENT ON TABLE germplasm.family_member
    IS 'Family Member: Germplasm members of a family [FAMMEM]';

-- column comments
COMMENT ON COLUMN germplasm.family_member.family_id
    IS 'Family ID: Reference to the family where the germplasm belongs [FAMMEM_FAM_ID]';
COMMENT ON COLUMN germplasm.family_member.germplasm_id
    IS 'Germplasm ID: Reference to the germplasm belonging to the family [FAMMEM_GE_ID]';
COMMENT ON COLUMN germplasm.family_member.order_number
    IS 'Order Number: Ordering of the germplasm within the family, if applicable [FAMMEM_ORDERNO]';

-- audit column comments
COMMENT ON COLUMN germplasm.family_member.id
    IS 'Family Member ID: Database identifier of the record [FAMMEM_ID]';
COMMENT ON COLUMN germplasm.family_member.remarks
    IS 'Remarks: Additional notes to record [FAMMEM_REMARKS]';
COMMENT ON COLUMN germplasm.family_member.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [FAMMEM_CTSTAMP]';
COMMENT ON COLUMN germplasm.family_member.creator_id
    IS 'Creator ID: Reference to the person who created the record [FAMMEM_CPERSON]';
COMMENT ON COLUMN germplasm.family_member.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [FAMMEM_MTSTAMP]';
COMMENT ON COLUMN germplasm.family_member.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [FAMMEM_MPERSON]';
COMMENT ON COLUMN germplasm.family_member.notes
    IS 'Notes: Technical details about the record [FAMMEM_NOTES]';
COMMENT ON COLUMN germplasm.family_member.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [FAMMEM_ISVOID]';
COMMENT ON COLUMN germplasm.family_member.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [FAMMEM_EVENTLOG]';

-- indices
CREATE INDEX family_member_family_id_idx
    ON germplasm.family_member USING btree (family_id)
;

CREATE INDEX family_member_family_id_germplasm_id_idx
    ON germplasm.family_member USING btree (family_id, germplasm_id)
;

-- audit indices
CREATE INDEX family_member_creator_id_idx ON germplasm.family_member USING btree (creator_id);
CREATE INDEX family_member_modifier_id_idx ON germplasm.family_member USING btree (modifier_id);
CREATE INDEX family_member_is_void_idx ON germplasm.family_member USING btree (is_void);

-- triggers
CREATE TRIGGER family_member_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.family_member FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.family_member;
