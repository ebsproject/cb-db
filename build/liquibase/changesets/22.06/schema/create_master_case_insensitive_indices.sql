--liquibase formatted sql

--changeset postgres:create_variable_label_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create variable label case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX variable_label_upper_idx
    ON master.variable USING btree (UPPER(label))
;

-- update table's query plan
ANALYZE master.variable;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX master.variable_label_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE master.variable;



--changeset postgres:create_variable_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create variable name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX variable_name_upper_idx
    ON master.variable USING btree (UPPER(name))
;

-- update table's query plan
ANALYZE master.variable;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX master.variable_name_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE master.variable;
