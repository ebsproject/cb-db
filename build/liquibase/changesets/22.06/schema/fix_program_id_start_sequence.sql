--liquibase formatted sql

--changeset postgres:fix_program_id_start_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1279 Fix tenant.program id starting sequence value



SELECT SETVAL('tenant.program_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.program;



-- revert changes
--rollback SELECT NULL;
