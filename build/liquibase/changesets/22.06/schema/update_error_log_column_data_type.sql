--liquibase formatted sql

--changeset postgres:update_error_log_column_data_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1255 Update error_log column data type



-- update data type
ALTER TABLE
    germplasm.file_upload
ALTER COLUMN
    error_log
    SET DATA TYPE jsonb
    USING error_log::jsonb
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback ALTER COLUMN
--rollback     error_log
--rollback     SET DATA TYPE text
--rollback ;
