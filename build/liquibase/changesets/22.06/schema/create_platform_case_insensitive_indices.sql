--liquibase formatted sql

--changeset postgres:create_list_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create list name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX list_name_upper_idx
    ON platform.list USING btree (UPPER(name))
;

-- update table's query plan
ANALYZE platform.list;


-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX platform.list_name_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE platform.list;



--changeset postgres:create_list_display_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create list display_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX list_display_name_upper_idx
    ON platform.list USING btree (UPPER(display_name))
;

-- update table's query plan
ANALYZE platform.list;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX platform.list_display_name_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE platform.list;
