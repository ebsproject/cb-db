--liquibase formatted sql

--changeset postgres:create_seed_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create seed_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX seed_seed_name_upper_idx
    ON germplasm.seed USING btree (UPPER(seed_name))
;

-- update table's query plan
ANALYZE germplasm.seed;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX germplasm.seed_seed_name_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE germplasm.seed;



--changeset postgres:create_package_label_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create package_label case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX package_package_label_upper_idx
    ON germplasm.package USING btree (UPPER(package_label))
;

-- update table's query plan
ANALYZE germplasm.package;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX germplasm.package_package_label_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE germplasm.package;
