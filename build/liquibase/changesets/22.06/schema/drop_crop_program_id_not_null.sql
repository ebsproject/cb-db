--liquibase formatted sql

--changeset postgres:drop_crop_program_id_not_null context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1279 Drop crop_program_id not null constraint in tenant.program



ALTER TABLE
    tenant.program
ALTER COLUMN
    crop_program_id
    DROP NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     tenant.program
--rollback ALTER COLUMN
--rollback     crop_program_id
--rollback     SET NOT NULL
--rollback ;
