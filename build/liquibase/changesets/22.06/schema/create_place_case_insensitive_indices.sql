--liquibase formatted sql

--changeset postgres:create_geospatial_object_name_case_insensitive_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1240 Create geospatial_object_name case-insensitive index



-- create index for case-insensitive sorting
CREATE INDEX geospatial_object_geospatial_object_name_upper_idx
    ON place.geospatial_object USING btree (UPPER(geospatial_object_name))
;

-- update table's query plan
ANALYZE place.geospatial_object;



-- revert changes
--rollback -- drop index for case-insensitive sorting
--rollback DROP INDEX place.geospatial_object_geospatial_object_name_upper_idx;
--rollback 
--rollback -- update table's query plan
--rollback ANALYZE place.geospatial_object;
