--liquibase formatted sql

--changeset postgres:update_variable_data_type_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1255 Update variable data_type check constraint



-- replace check constraint
ALTER TABLE master.variable
    DROP CONSTRAINT variable_data_type_chk
;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_data_type_chk
    CHECK (data_type = ANY(ARRAY['bigint', 'boolean', 'character varying', 'date', 'float', 'inet', 'integer', 'json', 'jsonb', 'name', 'numeric', 'oid', 'polygon', 'smallint', 'text', 'time', 'timestamp']))
;



-- revert changes
--rollback ALTER TABLE master.variable
--rollback     DROP CONSTRAINT variable_data_type_chk
--rollback ;
--rollback 
--rollback ALTER TABLE master.variable
--rollback     ADD CONSTRAINT variable_data_type_chk
--rollback     CHECK (data_type = ANY(ARRAY['bigint', 'boolean', 'character varying', 'date', 'float', 'inet', 'integer', 'json', 'name', 'numeric', 'oid', 'polygon', 'smallint', 'text', 'time', 'timestamp']))
--rollback ;
