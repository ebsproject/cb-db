--liquibase formatted sql

--changeset postgres:populate_winter_wheat_experiments context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate Winter Wheat experiments



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'WW') AS program_id,
    NULL pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'AYT') AS stage_id,
    NULL project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'DS') AS season_id,
    'DS' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'WW-AYT-2021-DS-001' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'RCBD' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'WW-AYT-2021-DS-001'
--rollback ;



--changeset postgres:populate_protocol_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate protocol for Winter Wheat experiment



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
SELECT
    CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code,
    CONCAT(INITCAP(ptc.protocol_type),' Protocol ',UPPER(exp.experiment_name)) AS protocol_name,
    ptc.protocol_type AS protocol_type,
    exp.program_id AS program_id,
    exp.creator_id AS creator_id
FROM 
(
    SELECT 
        experiment_name,
        program_id,
        creator_id
    FROM
        experiment.experiment
    WHERE
        experiment_name = 'WW-AYT-2021-DS-001'
) AS exp,
(
    VALUES
        ('trait'),
        ('management'),
        ('planting'),
        ('harvest')
) AS ptc (protocol_type)



--rollback DELETE FROM 
--rollback     tenant.protocol 
--rollback WHERE 
--rollback     protocol_code 
--rollback IN 
--rollback     (
--rollback         SELECT
--rollback             CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code
--rollback         FROM 
--rollback         (
--rollback             SELECT 
--rollback                 experiment_name,
--rollback                 program_id,
--rollback                 creator_id
--rollback             FROM
--rollback                 experiment.experiment
--rollback             WHERE
--rollback                 experiment_name = 'WW-AYT-2021-DS-001'
--rollback         ) AS exp,
--rollback         (
--rollback             VALUES
--rollback                 ('trait'),
--rollback                 ('management'),
--rollback                 ('planting'),
--rollback                 ('harvest')
--rollback         ) AS ptc (protocol_type)
--rollback 
--rollback     )
--rollback ;



--changeset postgres:populate_experiment_data_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment_data for Winter Wheat experiment



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    mv.id AS variable_id,
    exp_data.data_value AS data_value,
    'N' AS data_qc_code,
    tp.id AS protocol_id,
    exp.creator_id AS creator_id
FROM	
    experiment.experiment AS exp,
    (
        VALUES
            ('FIRST_PLOT_POSITION_VIEW','Top Left',NULL),
            ('TRAIT_PROTOCOL_LIST_ID','1393','trait'),
            ('MANAGEMENT_PROTOCOL_LIST_ID','1394','management'),
            ('ESTABLISHMENT','transplanted','planting'),
            ('PLANTING_TYPE','Flat','planting'),
            ('PLOT_TYPE','6R','planting'),
            ('ROWS_PER_PLOT_CONT','6','planting'),
            ('DIST_BET_ROWS','20','planting'),
            ('PLOT_WIDTH','1.2','planting'),
            ('PLOT_LN','20','planting'),
            ('PLOT_AREA_2','24','planting'),
            ('ALLEY_LENGTH','1','planting'),
            ('SEEDING_RATE','Normal','planting'),
            ('PROTOCOL_TARGET_LEVEL','occurrence',NULL),
            ('HV_METH_DISC','Bulk','harvest')
    ) AS exp_data (variable_abbrev, data_value,protocol_type)
    INNER JOIN
        master.variable mv
    ON
        mv.abbrev = exp_data.variable_abbrev
    LEFT JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(exp_data.protocol_type),'_PROTOCOL_',UPPER('WW-AYT-2021-DS-001')))
WHERE
    exp.experiment_name='WW-AYT-2021-DS-001'
;



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='WW-AYT-2021-DS-001');



--changeset postgres:populate_experiment_protocol_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment_protocol for Winter Wheat exp



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
SELECT
    exp.id AS experiment_id,
    tp.id AS protocol_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM 	
    experiment.experiment AS exp,
    (
        VALUES
            ('trait'),
            ('management'),
            ('planting'),
            ('harvest')
    ) AS t (protocol_type)
    INNER JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(t.protocol_type),'_PROTOCOL_',UPPER('WW-AYT-2021-DS-001')))
WHERE	
    exp.experiment_name='WW-AYT-2021-DS-001'
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='WW-AYT-2021-DS-001');



--changeset postgres:populate_entry_list_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate entry_list for Winter Wheat exp



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT 
	experiment.generate_code('entry_list') AS entry_list_code,
  	CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS entry_list_name,
	'completed' AS entry_list_status,
	exp.id AS experiment_id,
	exp.creator_id AS creator_id,
	FALSE AS is_void,
	'entry list' AS entry_list_type
FROM
	experiment.experiment AS exp
JOIN
	tenant.stage ts
ON
	ts.id = exp.stage_id
JOIN
	tenant.season tss
ON
	tss.id = exp.season_id
JOIN
	tenant.program tp
ON
	exp.program_id = tp.id
WHERE
	experiment_name='WW-AYT-2021-DS-001'
;    
    


-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='WW-AYT-2021-DS-001');



--changeset postgres:populate_entry_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate entry for Winter Wheat exp



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, package_id, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT 
    ROW_NUMBER() OVER() AS entry_code,
    ROW_NUMBER() OVER() AS entry_number,
    gg.designation AS entry_name,
    'test' AS entry_type,
    NULL AS entry_class,
    gp.id AS package_id,
    NULL AS entry_role,
    'active' AS entry_status,
    el.id AS entry_list_id,
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    ee.creator_id AS creator_id,
    FALSE AS is_void
FROM 
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
INNER JOIN
    germplasm.package gp
ON
    gp.seed_id = gs.id
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'WW-AYT-2021-DS-001'
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = ee.id
WHERE
    gg.designation
IN
    (
        'TCI141011-0SE-0100TE-2DYR-0E',
        'TCI141015-0SE-0100TE-1DYR-0E',
        'TCI141016-0SE-0100TE-3DYR-0E',
        'TCI141024-0SE-0100TE-7DYR-0E',
        'TCI141028-0SE-0100TE-3DYR-0E',
        'TCI141031-0SE-0100TE-2DYR-0E',
        'TCI141047-0SE-0100TE-1DYR-0E',
        'TCI141047-0SE-0100TE-3DYR-0E',
        'TCI141050-0SE-0100TE-3DYR-0E',
        'TCI141051-0SE-0100TE-1DYR-0E',
        'TCI141051-0SE-0100TE-2DYR-0E',
        'TCI141051-0SE-0100TE-4DYR-0E',
        'TCI141052-0SE-0100TE-3DYR-0E',
        'TCI141052-0SE-0100TE-4DYR-0E',
        'TCI141052-0SE-0100TE-5DYR-0E',
        'TCI141052-0SE-0100TE-6DYR-0E',
        'TCI141052-0SE-0100TE-7DYR-0E',
        'TCI141052-0SE-0100TE-8DYR-0E',
        'TCI141062-0SE-0100TE-2DYR-0E',
        'TCI141070-0SE-0100TE-1DYR-0E',
        'TCI141071-0SE-0100TE-1DYR-0E',
        'TCI141072-0SE-0100TE-4DYR-0E',
        'TCI141073-0SE-0100TE-1DYR-0E',
        'TCI141073-0SE-0100TE-2DYR-0E',
        'TCI141074-0SE-0100TE-2DYR-0E',
        'TCI141076-0SE-0100TE-1DYR-0E',
        'TCI141076-0SE-0100TE-2DYR-0E',
        'TCI141076-0SE-0100TE-3DYR-0E',
        'TCI141078-0SE-0100TE-1DYR-0E',
        'TCI141078-0SE-0100TE-2DYR-0E',
        'TCI141078-0SE-0100TE-3DYR-0E',
        'TCI141079-0SE-0100TE-3DYR-0E',
        'TCI141079-0SE-0100TE-4DYR-0E',
        'TCI141079-0SE-0100TE-5DYR-0E',
        'TCI141086-0SE-0100TE-1DYR-0E',
        'TCI141086-0SE-0100TE-3DYR-0E',
        'TCI141086-0SE-0100TE-5DYR-0E',
        'TCI141086-0SE-0100TE-7DYR-0E',
        'TCI141086-0SE-0100TE-8DYR-0E',
        'TCI141086-0SE-0100TE-11DYR-0E',
        'TCI141086-0SE-0100TE-12DYR-0E',
        'TCI141086-0SE-0100TE-13DYR-0E',
        'TCI141092-0SE-0100TE-6DYR-0E',
        'TCI141095-0SE-0100TE-2DYR-0E',
        'TCI141096-0SE-0100TE-1DYR-0E',
        'TCI141096-0SE-0100TE-2DYR-0E',
        'TCI141096-0SE-0100TE-3DYR-0E',
        'TCI141099-0SE-0100TE-3DYR-0E',
        'TCI141099-0SE-0100TE-4DYR-0E',
        'TCI141101-0SE-0100TE-2DYR-0E',
        'TCI141101-0SE-0100TE-8DYR-0E',
        'TCI141105-0SE-0100TE-6DYR-0E',
        'TCI141105-0SE-0100TE-7DYR-0E',
        'TCI141106-0SE-0100TE-1DYR-0E',
        'TCI141106-0SE-0100TE-3DYR-0E',
        'TCI141106-0SE-0100TE-4DYR-0E',
        'TCI141106-0SE-0100TE-5DYR-0E',
        'TCI141111-0SE-0100TE-1DYR-0E',
        'TCI141111-0SE-0100TE-2DYR-0E',
        'TCI141113-0SE-0100TE-12DYR-0E',
        'TCI141113-0SE-0100TE-13DYR-0E',
        'TCI141115-0SE-0100TE-1DYR-0E',
        'TCI141115-0SE-0100TE-3DYR-0E',
        'TCI141119-0SE-0100TE-2DYR-0E',
        'TCI141121-0SE-0100TE-4DYR-0E',
        'TCI141121-0SE-0100TE-5DYR-0E',
        'TCI141125-0SE-0100TE-3DYR-0E',
        'TCI141125-0SE-0100TE-5DYR-0E',
        'TCI141125-0SE-0100TE-6DYR-0E',
        'TCI141130-0SE-0100TE-2DYR-0E',
        'TCI141130-0SE-0100TE-3DYR-0E',
        'TCI141130-0SE-0100TE-5DYR-0E',
        'TCI141134-0SE-0100TE-1DYR-0E',
        'TCI141134-0SE-0100TE-2DYR-0E',
        'TCI141134-0SE-0100TE-5DYR-0E',
        'TCI141134-0SE-0100TE-6DYR-0E',
        'TCI141134-0SE-0100TE-9DYR-0E',
        'TCI141136-0SE-0100TE-1DYR-0E',
        'TCI141136-0SE-0100TE-2DYR-0E',
        'TCI141136-0SE-0100TE-3DYR-0E',
        'TCI141136-0SE-0100TE-4DYR-0E',
        'TCI141136-0SE-0100TE-5DYR-0E',
        'TCI141136-0SE-0100TE-6DYR-0E',
        'TCI141136-0SE-0100TE-7DYR-0E',
        'TCI141143-0SE-0100TE-2DYR-0E',
        'TCI141145-0SE-0100TE-1DYR-0E',
        'TCI141145-0SE-0100TE-2DYR-0E',
        'TCI141145-0SE-0100TE-3DYR-0E',
        'TCI141145-0SE-0100TE-5DYR-0E',
        'TCI141146-0SE-0100TE-1DYR-0E',
        'TCI141146-0SE-0100TE-2DYR-0E',
        'TCI141146-0SE-0100TE-4DYR-0E',
        'TCI141146-0SE-0100TE-5DYR-0E',
        'TCI141146-0SE-0100TE-6DYR-0E',
        'TCI141146-0SE-0100TE-7DYR-0E',
        'TCI141146-0SE-0100TE-8DYR-0E',
        'TCI141146-0SE-0100TE-10DYR-0E',
        'TCI141146-0SE-0100TE-11DYR-0E',
        'TCI141146-0SE-0100TE-12DYR-0E',
        'TCI141146-0SE-0100TE-13DYR-0E',
        'TCI141146-0SE-0100TE-14DYR-0E',
        'TCI141167-0SE-0100TE-5DYR-0E',
        'TCI141170-0SE-0100TE-1DYR-0E',
        'TCI141174-0SE-0100TE-9DYR-0E',
        'TCI141176-0SE-0100TE-3DYR-0E',
        'TCI141178-0SE-0100TE-1DYR-0E',
        'TCI141178-0SE-0100TE-2DYR-0E',
        'TCI141185-0SE-0100TE-2DYR-0E',
        'TCI141185-0SE-0100TE-9DYR-0E',
        'TCI141185-0SE-0100TE-10DYR-0E',
        'TCI141188-0SE-0100TE-1DYR-0E',
        'TCI141188-0SE-0100TE-2DYR-0E',
        'TCI141188-0SE-0100TE-3DYR-0E',
        'TCI141188-0SE-0100TE-8DYR-0E',
        'TCI141188-0SE-0100TE-9DYR-0E',
        'TCI141188-0SE-0100TE-10DYR-0E',
        'TCI141194-0SE-0100TE-3DYR-0E',
        'TCI141195-0SE-0100TE-3DYR-0E',
        'TCI141196-0SE-0100TE-2DYR-0E',
        'TCI141196-0SE-0100TE-3DYR-0E',
        'TCI141196-0SE-0100TE-4DYR-0E',
        'TCI141197-0SE-0100TE-1DYR-0E',
        'TCI141197-0SE-0100TE-2DYR-0E',
        'TCI141197-0SE-0100TE-3DYR-0E',
        'TCI141197-0SE-0100TE-4DYR-0E',
        'TCI141197-0SE-0100TE-5DYR-0E',
        'TCI141197-0SE-0100TE-6DYR-0E',
        'TCI141197-0SE-0100TE-7DYR-0E',
        'TCI141197-0SE-0100TE-8DYR-0E',
        'TCI141198-0SE-0100TE-1DYR-0E',
        'TCI141198-0SE-0100TE-2DYR-0E',
        'TCI141198-0SE-0100TE-3DYR-0E',
        'TCI141198-0SE-0100TE-4DYR-0E',
        'TCI141198-0SE-0100TE-5DYR-0E',
        'TCI141198-0SE-0100TE-8DYR-0E',
        'TCI141198-0SE-0100TE-10DYR-0E',
        'TCI141198-0SE-0100TE-11DYR-0E',
        'TCI141201-0SE-0100TE-5DYR-0E',
        'TCI141202-0SE-0100TE-1DYR-0E',
        'TCI141202-0SE-0100TE-2DYR-0E',
        'TCI141202-0SE-0100TE-6DYR-0E',
        'TCI141204-0SE-0100TE-3DYR-0E',
        'TCI141205-0SE-0100TE-5DYR-0E',
        'TCI141205-0SE-0100TE-6DYR-0E',
        'TCI141205-0SE-0100TE-7DYR-0E',
        'TCI141208-0SE-0100TE-1DYR-0E',
        'TCI141208-0SE-0100TE-2DYR-0E',
        'TCI141208-0SE-0100TE-4DYR-0E',
        'TCI141211-0SE-0100TE-1DYR-0E',
        'TCI141211-0SE-0100TE-2DYR-0E',
        'TCI141211-0SE-0100TE-3DYR-0E',
        'TCI141211-0SE-0100TE-6DYR-0E',
        'TCI141216-0SE-0100TE-8DYR-0E',
        'TCI141216-0SE-0100TE-10DYR-0E',
        'TCI141218-0SE-0100TE-1DYR-0E',
        'TCI141218-0SE-0100TE-2DYR-0E',
        'TCI141218-0SE-0100TE-3DYR-0E',
        'TCI141218-0SE-0100TE-4DYR-0E',
        'TCI141218-0SE-0100TE-6DYR-0E',
        'TCI141218-0SE-0100TE-8DYR-0E',
        'TCI141218-0SE-0100TE-10DYR-0E',
        'TCI141218-0SE-0100TE-11DYR-0E',
        'TCI141218-0SE-0100TE-12DYR-0E',
        'TCI141218-0SE-0100TE-13DYR-0E',
        'TCI141219-0SE-0100TE-6DYR-0E',
        'TCI141219-0SE-0100TE-7DYR-0E',
        'TCI141220-0SE-0100TE-5DYR-0E',
        'TCI141221-0SE-0100TE-1DYR-0E',
        'TCI141221-0SE-0100TE-2DYR-0E',
        'TCI141221-0SE-0100TE-3DYR-0E',
        'TCI141225-0SE-0100TE-3DYR-0E',
        'TCI141226-0SE-0100TE-1DYR-0E',
        'TCI141226-0SE-0100TE-3DYR-0E',
        'TCI141226-0SE-0100TE-4DYR-0E',
        'TCI141234-0SE-0100TE-1DYR-0E',
        'TCI141234-0SE-0100TE-2DYR-0E',
        'TCI141234-0SE-0100TE-3DYR-0E',
        'TCI141234-0SE-0100TE-4DYR-0E',
        'TCI141235-0SE-0100TE-2DYR-0E',
        'TCI141236-0SE-0100TE-1DYR-0E',
        'TCI141236-0SE-0100TE-2DYR-0E',
        'TCI141236-0SE-0100TE-3DYR-0E',
        'TCI141236-0SE-0100TE-4DYR-0E',
        'TCI141236-0SE-0100TE-5DYR-0E',
        'TCI141236-0SE-0100TE-7DYR-0E',
        'TCI141236-0SE-0100TE-8DYR-0E',
        'TCI141238-0SE-0100TE-1DYR-0E',
        'TCI141238-0SE-0100TE-4DYR-0E',
        'TCI141239-0SE-0100TE-1DYR-0E',
        'TCI141239-0SE-0100TE-2DYR-0E',
        'TCI141240-0SE-0100TE-1DYR-0E',
        'TCI141240-0SE-0100TE-3DYR-0E',
        'TCI141240-0SE-0100TE-6DYR-0E',
        'TCI141242-0SE-0100TE-2DYR-0E',
        'TCI141242-0SE-0100TE-3DYR-0E',
        'TCI141242-0SE-0100TE-4DYR-0E',
        'TCI141242-0SE-0100TE-5DYR-0E',
        'TCI141242-0SE-0100TE-7DYR-0E',
        'TCI141244-0SE-0100TE-7DYR-0E',
        'TCI141244-0SE-0100TE-8DYR-0E',
        'TCI141244-0SE-0100TE-9DYR-0E',
        'TCI141245-0SE-0100TE-1DYR-0E',
        'TCI141245-0SE-0100TE-2DYR-0E',
        'TCI141245-0SE-0100TE-3DYR-0E',
        'TCI141245-0SE-0100TE-4DYR-0E',
        'TCI141251-0SE-0100TE-1DYR-0E',
        'TCI141254-0SE-0100TE-2DYR-0E',
        'TCI141256-0SE-0100TE-2DYR-0E',
        'TCI141256-0SE-0100TE-3DYR-0E',
        'TCI141256-0SE-0100TE-4DYR-0E',
        'TCI141257-0SE-0100TE-1DYR-0E',
        'TCI141262-0SE-0100TE-2DYR-0E',
        'TCI141262-0SE-0100TE-6DYR-0E',
        'TCI141263-0SE-0100TE-2DYR-0E',
        'TCI141266-0SE-0100TE-2DYR-0E',
        'TCI141266-0SE-0100TE-3DYR-0E',
        'TCI141268-0SE-0100TE-3DYR-0E',
        'TCI141268-0SE-0100TE-4DYR-0E',
        'TCI141268-0SE-0100TE-6DYR-0E',
        'TCI141270-0SE-0100TE-1DYR-0E',
        'TCI141270-0SE-0100TE-2DYR-0E',
        'TCI141271-0SE-0100TE-3DYR-0E',
        'TCI141272-0SE-0100TE-1DYR-0E',
        'TCI141272-0SE-0100TE-4DYR-0E',
        'TCI141274-0SE-0100TE-5DYR-0E',
        'TCI141274-0SE-0100TE-6DYR-0E',
        'TCI141274-0SE-0100TE-7DYR-0E',
        'TCI141274-0SE-0100TE-9DYR-0E',
        'TCI141274-0SE-0100TE-10DYR-0E',
        'TCI141275-0SE-0100TE-2DYR-0E',
        'TCI141275-0SE-0100TE-4DYR-0E',
        'TCI141276-0SE-0100TE-1DYR-0E',
        'TCI141276-0SE-0100TE-2DYR-0E',
        'TCI141276-0SE-0100TE-3DYR-0E',
        'TCI141276-0SE-0100TE-4DYR-0E',
        'TCI141276-0SE-0100TE-5DYR-0E',
        'TCI141276-0SE-0100TE-6DYR-0E',
        'TCI141277-0SE-0100TE-1DYR-0E',
        'TCI141277-0SE-0100TE-2DYR-0E',
        'TCI141277-0SE-0100TE-5DYR-0E',
        'TCI141277-0SE-0100TE-6DYR-0E',
        'TCI141281-0SE-0100TE-2DYR-0E',
        'TCI141281-0SE-0100TE-4DYR-0E',
        'TCI141281-0SE-0100TE-6DYR-0E',
        'TCI141290-0SE-0100TE-1DYR-0E',
        'TCI141290-0SE-0100TE-2DYR-0E',
        'TCI141290-0SE-0100TE-3DYR-0E',
        'TCI141290-0SE-0100TE-4DYR-0E',
        'TCI141303-0SE-0100TE-1DYR-0E',
        'TCI141303-0SE-0100TE-2DYR-0E',
        'TCI141303-0SE-0100TE-3DYR-0E',
        'TCI141303-0SE-0100TE-4DYR-0E',
        'TCI141307-0SE-0100TE-2DYR-0E',
        'TCI141307-0SE-0100TE-3DYR-0E',
        'TCI141307-0SE-0100TE-4DYR-0E',
        'TCI141307-0SE-0100TE-5DYR-0E',
        'TCI141307-0SE-0100TE-8DYR-0E',
        'TCI141307-0SE-0100TE-9DYR-0E',
        'TCI141307-0SE-0100TE-12DYR-0E',
        'TCI141308-0SE-0100TE-1DYR-0E',
        'TCI141308-0SE-0100TE-2DYR-0E',
        'TCI141308-0SE-0100TE-3DYR-0E',
        'TCI141308-0SE-0100TE-5DYR-0E',
        'TCI141308-0SE-0100TE-6DYR-0E',
        'TCI141308-0SE-0100TE-7DYR-0E',
        'TCI141308-0SE-0100TE-8DYR-0E',
        'TCI141314-0SE-0100TE-1DYR-0E',
        'TCI141314-0SE-0100TE-2DYR-0E',
        'TCI141314-0SE-0100TE-5DYR-0E',
        'TCI141314-0SE-0100TE-8DYR-0E',
        'TCI141314-0SE-0100TE-9DYR-0E',
        'TCI141314-0SE-0100TE-10DYR-0E',
        'TCI141331-0SE-0100TE-3DYR-0E',
        'TCI141331-0SE-0100TE-4DYR-0E',
        'TCI141331-0SE-0100TE-9DYR-0E',
        'TCI141331-0SE-0100TE-10DYR-0E',
        'TCI141331-0SE-0100TE-11DYR-0E',
        'TCI141331-0SE-0100TE-12DYR-0E',
        'TCI141367-0SE-0100TE-1DYR-0E',
        'TCI141367-0SE-0100TE-3DYR-0E',
        'TCI141367-0SE-0100TE-4DYR-0E',
        'TCI141367-0SE-0100TE-5DYR-0E',
        'TCI141369-0SE-0100TE-2DYR-0E',
        'TCI141369-0SE-0100TE-3DYR-0E',
        'TCI141371-0SE-0100TE-5DYR-0E',
        'TCI142007-0SE-0100TE-3DYR-0E',
        'TCI142007-0SE-0100TE-4DYR-0E',
        'TCI142008-0SE-0100TE-2DYR-0E',
        'TCI142008-0SE-0100TE-4DYR-0E',
        'TCI142010-0SE-0100TE-1DYR-0E',
        'TCI142011-0SE-0100TE-2DYR-0E',
        'TCI142016-0SE-0100TE-1DYR-0E',
        'TCI142017-0SE-0100TE-4DYR-0E',
        'TCI142020-0SE-0100TE-3DYR-0E',
        'TCI142022-0SE-0100TE-1DYR-0E',
        'TCI142022-0SE-0100TE-2DYR-0E',
        'TCI142024-0SE-0100TE-3DYR-0E',
        'TCI142026-0SE-0100TE-2DYR-0E',
        'TCI142026-0SE-0100TE-4DYR-0E',
        'TCI142026-0SE-0100TE-5DYR-0E',
        'TCI142026-0SE-0100TE-6DYR-0E',
        'TCI142026-0SE-0100TE-7DYR-0E',
        'TCI142044-0SE-0100TE-1DYR-0E',
        'TCI142044-0SE-0100TE-2DYR-0E',
        'TCI142046-0SE-0100TE-1DYR-0E',
        'TCI142046-0SE-0100TE-3DYR-0E',
        'TCI142047-0SE-0100TE-1DYR-0E',
        'TCI142047-0SE-0100TE-3DYR-0E',
        'TCI142047-0SE-0100TE-4DYR-0E',
        'TCI142047-0SE-0100TE-5DYR-0E',
        'TCI142047-0SE-0100TE-11DYR-0E',
        'TCI142047-0SE-0100TE-12DYR-0E',
        'TCI142048-0SE-0100TE-3DYR-0E',
        'TCI142048-0SE-0100TE-4DYR-0E',
        'TCI142053-0SE-0100TE-1DYR-0E',
        'TCI142053-0SE-0100TE-3DYR-0E',
        'TCI142059-0SE-0100TE-3DYR-0E',
        'TCI142059-0SE-0100TE-4DYR-0E',
        'TCI142066-0SE-0100TE-1DYR-0E',
        'TCI142066-0SE-0100TE-2DYR-0E',
        'TCI142068-0SE-0100TE-2DYR-0E',
        'TCI142068-0SE-0100TE-3DYR-0E',
        'TCI142088-0SE-0100TE-2DYR-0E',
        'TCI142088-0SE-0100TE-3DYR-0E',
        'TCI142088-0SE-0100TE-4DYR-0E',
        'TCI142088-0SE-0100TE-6DYR-0E',
        'TCI142088-0SE-0100TE-7DYR-0E',
        'TCI142090-0SE-0100TE-6DYR-0E',
        'TCI142090-0SE-0100TE-7DYR-0E',
        'TCI142090-0SE-0100TE-8DYR-0E',
        'TCI142090-0SE-0100TE-9DYR-0E',
        'TCI142090-0SE-0100TE-10DYR-0E',
        'TCI142091-0SE-0100TE-2DYR-0E',
        'TCI142091-0SE-0100TE-4DYR-0E',
        'TCI142091-0SE-0100TE-5DYR-0E',
        'TCI142007-0SE-0100TE-2DYR-0E',
        'TCI142091-0SE-0100TE-7DYR-0E',
        'TCI142091-0SE-0100TE-8DYR-0E',
        'TCI142091-0SE-0100TE-9DYR-0E',
        'TCI142091-0SE-0100TE-10DYR-0E',
        'TCI142092-0SE-0100TE-1DYR-0E',
        'TCI142092-0SE-0100TE-5DYR-0E',
        'TCI142093-0SE-0100TE-1DYR-0E',
        'TCI142096-0SE-0100TE-1DYR-0E',
        'TCI142096-0SE-0100TE-2DYR-0E',
        'TCI142096-0SE-0100TE-4DYR-0E',
        'TCI142096-0SE-0100TE-5DYR-0E',
        'TCI142098-0SE-0100TE-3DYR-0E',
        'TCI142098-0SE-0100TE-4DYR-0E',
        'TCI142098-0SE-0100TE-5DYR-0E',
        'TCI142098-0SE-0100TE-6DYR-0E',
        'TCI142098-0SE-0100TE-8DYR-0E',
        'TCI142098-0SE-0100TE-9DYR-0E',
        'TCI142098-0SE-0100TE-11DYR-0E',
        'TCI142098-0SE-0100TE-12DYR-0E',
        'TCI142103-0SE-0100TE-1DYR-0E',
        'TCI142103-0SE-0100TE-3DYR-0E',
        'TCI142103-0SE-0100TE-4DYR-0E',
        'TCI142106-0SE-0100TE-1DYR-0E',
        'TCI142106-0SE-0100TE-3DYR-0E',
        'TCI142106-0SE-0100TE-6DYR-0E',
        'TCI142106-0SE-0100TE-8DYR-0E',
        'TCI142111-0SE-0100TE-1DYR-0E',
        'TCI142111-0SE-0100TE-2DYR-0E',
        'TCI142111-0SE-0100TE-4DYR-0E',
        'TCI142111-0SE-0100TE-12DYR-0E',
        'TCI142112-0SE-0100TE-1DYR-0E',
        'TCI142112-0SE-0100TE-6DYR-0E',
        'TCI142112-0SE-0100TE-9DYR-0E',
        'TCI142112-0SE-0100TE-11DYR-0E',
        'TCI142112-0SE-0100TE-12DYR-0E',
        'TCI142112-0SE-0100TE-15DYR-0E',
        'TCI142112-0SE-0100TE-17DYR-0E',
        'TCI142113-0SE-0100TE-2DYR-0E',
        'TCI142113-0SE-0100TE-3DYR-0E',
        'TCI142113-0SE-0100TE-4DYR-0E',
        'TCI142113-0SE-0100TE-5DYR-0E',
        'TCI142116-0SE-0100TE-1DYR-0E',
        'TCI142118-0SE-0100TE-1DYR-0E',
        'TCI142118-0SE-0100TE-3DYR-0E',
        'TCI142118-0SE-0100TE-4DYR-0E',
        'TCI142119-0SE-0100TE-3DYR-0E',
        'TCI142119-0SE-0100TE-7DYR-0E',
        'TCI142122-0SE-0100TE-4DYR-0E',
        'TCI142125-0SE-0100TE-3DYR-0E',
        'TCI142125-0SE-0100TE-6DYR-0E',
        'TCI142125-0SE-0100TE-8DYR-0E',
        'TCI142126-0SE-0100TE-5DYR-0E',
        'TCI142126-0SE-0100TE-8DYR-0E',
        'TCI142126-0SE-0100TE-9DYR-0E',
        'TCI142127-0SE-0100TE-1DYR-0E',
        'TCI142127-0SE-0100TE-2DYR-0E',
        'TCI142127-0SE-0100TE-3DYR-0E',
        'TCI142127-0SE-0100TE-4DYR-0E',
        'TCI142127-0SE-0100TE-6DYR-0E',
        'TCI142127-0SE-0100TE-7DYR-0E',
        'TCI142127-0SE-0100TE-8DYR-0E',
        'TCI142127-0SE-0100TE-9DYR-0E',
        'TCI142130-0SE-0100TE-2DYR-0E',
        'TCI142131-0SE-0100TE-6DYR-0E',
        'TCI142136-0SE-0100TE-1DYR-0E',
        'TCI142136-0SE-0100TE-5DYR-0E',
        'TCI142136-0SE-0100TE-7DYR-0E',
        'TCI142137-0SE-0100TE-3DYR-0E',
        'TCI142137-0SE-0100TE-5DYR-0E',
        'TCI142137-0SE-0100TE-6DYR-0E',
        'TCI142137-0SE-0100TE-9DYR-0E',
        'TCI142143-0SE-0100TE-11DYR-0E',
        'TCI142147-0SE-0100TE-1DYR-0E',
        'TCI142147-0SE-0100TE-3DYR-0E',
        'TCI142147-0SE-0100TE-4DYR-0E',
        'TCI142147-0SE-0100TE-5DYR-0E',
        'TCI142147-0SE-0100TE-6DYR-0E',
        'TCI142164-0SE-0100TE-3DYR-0E',
        'TCI142164-0SE-0100TE-5DYR-0E',
        'TCI142164-0SE-0100TE-6DYR-0E',
        'TCI142169-0SE-0100TE-1DYR-0E',
        'TCI142169-0SE-0100TE-2DYR-0E',
        'TCI142169-0SE-0100TE-5DYR-0E',
        'TCI142177-0SE-0100TE-2DYR-0E',
        'TCI142177-0SE-0100TE-4DYR-0E',
        'TCI142177-0SE-0100TE-5DYR-0E',
        'TCI142183-0SE-0100TE-2DYR-0E',
        'TCI142183-0SE-0100TE-4DYR-0E',
        'TCI142183-0SE-0100TE-5DYR-0E',
        'TCI142196-0SE-0100TE-1DYR-0E',
        'TCI142199-0SE-0100TE-2DYR-0E',
        'TCI142199-0SE-0100TE-4DYR-0E',
        'TCI142199-0SE-0100TE-5DYR-0E',
        'TCI142199-0SE-0100TE-6DYR-0E',
        'TCI142199-0SE-0100TE-7DYR-0E',
        'TCI142205-0SE-0100TE-1DYR-0E',
        'TCI142207-0SE-0100TE-1DYR-0E',
        'TCI142207-0SE-0100TE-3DYR-0E',
        'TCI141068-0SE-0100TE-1DYR-0E',
        'TCI141068-0SE-0100TE-3DYR-0E',
        'TCI141391-0SE-0100TE-1DYR-0E',
        'TCI141391-0SE-0100TE-3DYR-0E',
        'TCI141008-0SE-0100TE-2DYR-0E',
        'TCI141008-0SE-0100TE-4DYR-0E',
        'TCI141008-0SE-0100TE-5DYR-0E',
        'TCI141103-0SE-0100TE-1DYR-0E',
        'TCI141103-0SE-0100TE-2DYR-0E',
        'TCI141103-0SE-0100TE-3DYR-0E',
        'TCI141103-0SE-0100TE-8DYR-0E',
        'TCI141103-0SE-0100TE-9DYR-0E',
        'TCI141103-0SE-0100TE-13DYR-0E',
        'TCI141109-0SE-0100TE-1DYR-0E',
        'TCI141109-0SE-0100TE-3DYR-0E',
        'TCI141109-0SE-0100TE-4DYR-0E',
        'TCI141109-0SE-0100TE-5DYR-0E',
        'TCI141109-0SE-0100TE-6DYR-0E',
        'TCI141109-0SE-0100TE-8DYR-0E',
        'TCI141109-0SE-0100TE-9DYR-0E',
        'TCI141109-0SE-0100TE-10DYR-0E',
        'TCI141109-0SE-0100TE-12DYR-0E',
        'TCI141132-0SE-0100TE-2DYR-0E',
        'TCI141132-0SE-0100TE-3DYR-0E',
        'TCI141132-0SE-0100TE-5DYR-0E',
        'TCI141132-0SE-0100TE-6DYR-0E',
        'TCI141132-0SE-0100TE-7DYR-0E',
        'TCI141132-0SE-0100TE-8DYR-0E',
        'TCI141144-0SE-0100TE-3DYR-0E',
        'TCI141144-0SE-0100TE-8DYR-0E',
        'TCI141144-0SE-0100TE-10DYR-0E',
        'TCI141144-0SE-0100TE-14DYR-0E',
        'TCI141147-0SE-0100TE-2DYR-0E',
        'TCI141147-0SE-0100TE-3DYR-0E',
        'TCI141147-0SE-0100TE-5DYR-0E',
        'TCI141151-0SE-0100TE-3DYR-0E',
        'TCI141151-0SE-0100TE-4DYR-0E',
        'TCI141151-0SE-0100TE-5DYR-0E',
        'TCI141151-0SE-0100TE-6DYR-0E',
        'TCI141151-0SE-0100TE-7DYR-0E',
        'TCI141151-0SE-0100TE-8DYR-0E',
        'TCI141151-0SE-0100TE-12DYR-0E',
        'TCI141151-0SE-0100TE-13DYR-0E',
        'TCI141151-0SE-0100TE-14DYR-0E',
        'TCI141152-0SE-0100TE-1DYR-0E',
        'TCI141152-0SE-0100TE-4DYR-0E',
        'TCI141152-0SE-0100TE-5DYR-0E',
        'TCI141160-0SE-0100TE-1DYR-0E',
        'TCI141160-0SE-0100TE-3DYR-0E',
        'TCI141160-0SE-0100TE-4DYR-0E',
        'TCI141160-0SE-0100TE-5DYR-0E',
        'TCI141160-0SE-0100TE-8DYR-0E',
        'TCI141160-0SE-0100TE-9DYR-0E',
        'TCI141172-0SE-0100TE-3DYR-0E',
        'TCI141172-0SE-0100TE-4DYR-0E',
        'TCI141172-0SE-0100TE-5DYR-0E',
        'TCI141287-0SE-0100TE-2DYR-0E',
        'TCI141287-0SE-0100TE-5DYR-0E',
        'TCI141287-0SE-0100TE-6DYR-0E',
        'TCI141287-0SE-0100TE-7DYR-0E',
        'TCI141287-0SE-0100TE-9DYR-0E',
        'TCI141292-0SE-0100TE-1DYR-0E',
        'TCI141292-0SE-0100TE-2DYR-0E',
        'TCI141292-0SE-0100TE-3DYR-0E',
        'TCI141292-0SE-0100TE-5DYR-0E',
        'TCI141292-0SE-0100TE-6DYR-0E',
        'TCI141292-0SE-0100TE-7DYR-0E',
        'TCI141292-0SE-0100TE-9DYR-0E',
        'TCI141292-0SE-0100TE-10DYR-0E',
        'TCI141292-0SE-0100TE-11DYR-0E',
        'TCI141292-0SE-0100TE-13DYR-0E',
        'TCI141292-0SE-0100TE-14DYR-0E',
        'TCI141292-0SE-0100TE-15DYR-0E',
        'TCI141292-0SE-0100TE-16DYR-0E',
        'TCI141292-0SE-0100TE-17DYR-0E',
        'TCI141292-0SE-0100TE-19DYR-0E',
        'TCI141293-0SE-0100TE-1DYR-0E',
        'TCI141293-0SE-0100TE-2DYR-0E',
        'TCI141293-0SE-0100TE-3DYR-0E',
        'TCI141293-0SE-0100TE-4DYR-0E',
        'TCI141293-0SE-0100TE-5DYR-0E',
        'TCI141293-0SE-0100TE-6DYR-0E',
        'TCI141293-0SE-0100TE-9DYR-0E',
        'TCI141293-0SE-0100TE-10DYR-0E',
        'TCI141301-0SE-0100TE-3DYR-0E',
        'TCI141301-0SE-0100TE-4DYR-0E',
        'TCI141301-0SE-0100TE-5DYR-0E',
        'TCI141301-0SE-0100TE-6DYR-0E',
        'TCI141301-0SE-0100TE-8DYR-0E',
        'TCI141301-0SE-0100TE-13DYR-0E',
        'TCI141301-0SE-0100TE-15DYR-0E',
        'TCI141309-0SE-0100TE-3DYR-0E',
        'TCI141309-0SE-0100TE-4DYR-0E',
        'TCI141313-0SE-0100TE-1DYR-0E',
        'TCI141313-0SE-0100TE-2DYR-0E',
        'TCI141313-0SE-0100TE-3DYR-0E',
        'TCI141317-0SE-0100TE-2DYR-0E',
        'TCI141318-0SE-0100TE-3DYR-0E',
        'TCI141321-0SE-0100TE-1DYR-0E',
        'TCI141321-0SE-0100TE-2DYR-0E',
        'TCI141321-0SE-0100TE-4DYR-0E',
        'TCI141321-0SE-0100TE-5DYR-0E',
        'TCI141321-0SE-0100TE-6DYR-0E',
        'TCI141325-0SE-0100TE-3DYR-0E',
        'TCI141325-0SE-0100TE-6DYR-0E',
        'TCI141325-0SE-0100TE-7DYR-0E',
        'TCI141325-0SE-0100TE-8DYR-0E',
        'TCI141325-0SE-0100TE-9DYR-0E',
        'TCI141327-0SE-0100TE-1DYR-0E',
        'TCI141327-0SE-0100TE-2DYR-0E',
        'TCI141327-0SE-0100TE-3DYR-0E',
        'TCI141327-0SE-0100TE-6DYR-0E',
        'TCI141327-0SE-0100TE-7DYR-0E',
        'TCI141327-0SE-0100TE-8DYR-0E',
        'TCI141327-0SE-0100TE-9DYR-0E',
        'TCI141327-0SE-0100TE-10DYR-0E',
        'TCI141328-0SE-0100TE-1DYR-0E',
        'TCI141328-0SE-0100TE-2DYR-0E',
        'TCI141328-0SE-0100TE-3DYR-0E',
        'TCI141328-0SE-0100TE-4DYR-0E',
        'TCI141328-0SE-0100TE-5DYR-0E',
        'TCI141328-0SE-0100TE-7DYR-0E',
        'TCI141328-0SE-0100TE-10DYR-0E',
        'TCI141328-0SE-0100TE-11DYR-0E',
        'TCI141328-0SE-0100TE-12DYR-0E',
        'TCI141328-0SE-0100TE-13DYR-0E',
        'TCI141328-0SE-0100TE-15DYR-0E',
        'TCI141328-0SE-0100TE-17DYR-0E',
        'TCI141328-0SE-0100TE-18DYR-0E',
        'TCI141328-0SE-0100TE-19DYR-0E',
        'TCI141333-0SE-0100TE-3DYR-0E',
        'TCI141333-0SE-0100TE-4DYR-0E',
        'TCI141335-0SE-0100TE-1DYR-0E',
        'TCI141335-0SE-0100TE-2DYR-0E',
        'TCI141335-0SE-0100TE-4DYR-0E',
        'TCI141337-0SE-0100TE-4DYR-0E',
        'TCI141337-0SE-0100TE-5DYR-0E',
        'TCI141339-0SE-0100TE-1DYR-0E',
        'TCI141339-0SE-0100TE-2DYR-0E',
        'TCI141339-0SE-0100TE-3DYR-0E',
        'TCI141339-0SE-0100TE-4DYR-0E',
        'TCI141339-0SE-0100TE-5DYR-0E',
        'TCI141345-0SE-0100TE-1DYR-0E',
        'TCI141345-0SE-0100TE-8DYR-0E',
        'TCI141368-0SE-0100TE-8DYR-0E',
        'TCI141368-0SE-0100TE-9DYR-0E',
        'TCI141382-0SE-0100TE-2DYR-0E',
        'TCI141382-0SE-0100TE-4DYR-0E',
        'TCI141382-0SE-0100TE-5DYR-0E',
        'TCI141382-0SE-0100TE-6DYR-0E',
        'TCI141382-0SE-0100TE-7DYR-0E',
        'TCI141385-0SE-0100TE-1DYR-0E',
        'TCI141394-0SE-0100TE-1DYR-0E',
        'TCI141394-0SE-0100TE-2DYR-0E',
        'TCI141394-0SE-0100TE-3DYR-0E',
        'TCI141394-0SE-0100TE-5DYR-0E',
        'TCI142027-0SE-0100TE-4DYR-0E',
        'TCI142027-0SE-0100TE-9DYR-0E',
        'TCI142042-0SE-0100TE-1DYR-0E',
        'TCI142042-0SE-0100TE-2DYR-0E',
        'TCI142043-0SE-0100TE-1DYR-0E',
        'TCI142043-0SE-0100TE-2DYR-0E',
        'TCI142058-0SE-0100TE-2DYR-0E',
        'TCI142064-0SE-0100TE-1DYR-0E',
        'TCI142064-0SE-0100TE-2DYR-0E',
        'TCI142064-0SE-0100TE-4DYR-0E',
        'TCI142064-0SE-0100TE-5DYR-0E',
        'TCI142104-0SE-0100TE-6DYR-0E',
        'TCI142104-0SE-0100TE-7DYR-0E',
        'TCI142104-0SE-0100TE-8DYR-0E',
        'TCI142104-0SE-0100TE-10DYR-0E',
        'TCI142140-0SE-0100TE-1DYR-0E',
        'TCI142140-0SE-0100TE-2DYR-0E',
        'TCI142140-0SE-0100TE-3DYR-0E',
        'TCI142140-0SE-0100TE-5DYR-0E',
        'TCI142140-0SE-0100TE-6DYR-0E',
        'TCI142140-0SE-0100TE-9DYR-0E',
        'TCI142140-0SE-0100TE-10DYR-0E',
        'TCI142141-0SE-0100TE-1DYR-0E',
        'TCI142141-0SE-0100TE-2DYR-0E',
        'TCI142141-0SE-0100TE-4DYR-0E',
        'TCI142141-0SE-0100TE-6DYR-0E',
        'TCI142141-0SE-0100TE-8DYR-0E',
        'TCI142141-0SE-0100TE-9DYR-0E',
        'TCI142141-0SE-0100TE-10DYR-0E',
        'TCI142141-0SE-0100TE-12DYR-0E',
        'TCI142141-0SE-0100TE-14DYR-0E',
        'TCI142141-0SE-0100TE-15DYR-0E',
        'TCI142141-0SE-0100TE-16DYR-0E',
        'TCI142150-0SE-0100TE-1DYR-0E',
        'TCI142165-0SE-0100TE-4DYR-0E',
        'TCI142167-0SE-0100TE-1DYR-0E',
        'TCI142167-0SE-0100TE-2DYR-0E',
        'TCI142167-0SE-0100TE-3DYR-0E',
        'TCI142178-0SE-0100TE-4DYR-0E',
        'TCI142178-0SE-0100TE-5DYR-0E',
        'TCI142180-0SE-0100TE-1DYR-0E',
        'TCI142191-0SE-0100TE-2DYR-0E',
        'TCI142191-0SE-0100TE-3DYR-0E',
        'TCI142200-0SE-0100TE-6DYR-0E',
        'TCI142212-0SE-0100TE-4DYR-0E',
        'TCI142225-0SE-0100TE-2DYR-0E',
        'TCI142225-0SE-0100TE-4DYR-0E',
        'TCI141487-0E-0E-4YC-0E',
        'TCI141488-0E-0E-1YC-0E',
        'TCI141488-0E-0E-2YC-0E',
        'TCI141490-0E-0E-1YC-0E',
        'TCI141490-0E-0E-3YC-0E',
        'TCI141491-0E-0E-3YC-0E',
        'TCI141491-0E-0E-4YC-0E',
        'TCI141493-0E-0E-3YC-0E',
        'TCI141499-0E-0E-1YC-0E',
        'TCI141505-0E-0E-2YC-0E',
        'TCI141505-0E-0E-3YC-0E',
        'TCI141506-0E-0E-7YC-0E',
        'X1420871-1B-1B-0UNL-1DYR-0E',
        'X1420871-1B-1B-0UNL-2DYR-0E',
        'X1420871-1B-1B-0UNL-3DYR-0E',
        'X1420871-1B-1B-0UNL-4DYR-0E',
        'X1420871-1B-1B-0UNL-5DYR-0E',
        'X1421100-1B-1B-0UNL-1DYR-0E',
        'X1421100-1B-1B-0UNL-3DYR-0E',
        'X1421023-1B-1B-0UNL-2DYR-0E',
        'X1421023-1B-1B-0UNL-4DYR-0E',
        'X1421023-1B-1B-0UNL-6DYR-0E',
        'X1421023-1B-1B-0UNL-7DYR-0E',
        'X1421024-1B-1B-0UNL-5DYR-0E',
        'X1421024-1B-1B-0UNL-6DYR-0E',
        'X1420956-1B-1B-0UNL-3DYR-0E',
        'X1420994-1B-1B-0UNL-1DYR-0E',
        'X1420716-1B-2B-0UNL-4DYR-0E',
        'X1420923-1B-1B-0UNL-1DYR-0E',
        'X1420955-1B-1B-0UNL-3DYR-0E',
        'X1420955-1B-1B-0UNL-4DYR-0E',
        'X1421156-1B-1B-0UNL-1DYR-0E',
        'X1421141-1B-1B-0UNL-1DYR-0E',
        'X1421141-1B-2B-0UNL-1DYR-0E',
        'X1421152-1B-2B-0UNL-1DYR-0E',
        'X1420812-1B-1B-0UNL-2DYR-0E',
        'X1420811-1B-1B-0UNL-2DYR-0E',
        'X1420478-1B-1B-0UNL-1DYR-0E',
        'X1420258-1B-1B-0UNL-3DYR-0E',
        'X1420257-1B-1B-0UNL-1DYR-0E',
        'X1420257-1B-1B-0UNL-2DYR-0E',
        'X1420344-1B-1B-0UNL-1DYR-0E',
        'X1420344-1B-1B-0UNL-2DYR-0E',
        'X1420343-1B-1B-0UNL-1DYR-0E',
        'X1420343-1B-1B-0UNL-2DYR-0E',
        'X1420187-1B-2B-0UNL-2DYR-0E',
        'X1420479-1B-1B-0UNL-2DYR-0E',
        'X1421018-1B-1B-0UNL-1DYR-0E',
        'X1420873-1B-1B-0UNL-1DYR-0E',
        'X1420873-1B-1B-0UNL-3DYR-0E',
        'X1420869-1B-1B-0UNL-1DYR-0E',
        'X1420869-1B-1B-0UNL-2DYR-0E',
        'X1420869-1B-1B-0UNL-3DYR-0E',
        'X1420869-1B-1B-0UNL-4DYR-0E',
        'X1420869-1B-1B-0UNL-5DYR-0E',
        'X1420869-1B-1B-0UNL-6DYR-0E',
        'X1421020-1B-1B-0UNL-4DYR-0E',
        'X1420185-1B-1B-0UNL-2DYR-0E',
        'X1420185-1B-1B-0UNL-5DYR-0E',
        'X1421145-1B-1B-0UNL-1DYR-0E',
        'X1421157-1B-1B-0UNL-2DYR-0E',
        'X1421157-1B-1B-0UNL-3DYR-0E',
        'TCI141006-0UNL-1DYR-0E',
        'TCI141011-0UNL-1DYR-0E',
        'TCI141011-0UNL-2DYR-0E',
        'TCI141029-0UNL-1DYR-0E',
        'TCI141040-0UNL-1DYR-0E',
        'TCI141040-0UNL-3DYR-0E',
        'TCI141041-0UNL-3DYR-0E',
        'TCI141063-0UNL-2DYR-0E',
        'TCI141070-0UNL-1DYR-0E',
        'TCI141092-0UNL-5DYR-0E',
        'TCI141093-0UNL-1DYR-0E',
        'TCI141108-0UNL-2DYR-0E',
        'TCI141131-0UNL-3DYR-0E',
        'TCI141135-0UNL-3DYR-0E',
        'TCI141135-0UNL-5DYR-0E',
        'TCI141135-0UNL-6DYR-0E',
        'TCI141196-0UNL-4DYR-0E',
        'TCI141201-0UNL-1DYR-0E',
        'TCI141201-0UNL-2DYR-0E',
        'TCI141201-0UNL-4DYR-0E',
        'TCI141201-0UNL-5DYR-0E',
        'TCI141201-0UNL-6DYR-0E',
        'TCI141201-0UNL-7DYR-0E',
        'TCI141207-0UNL-1DYR-0E',
        'TCI141207-0UNL-2DYR-0E',
        'TCI141208-0UNL-3DYR-0E',
        'TCI141220-0UNL-1DYR-0E',
        'TCI141220-0UNL-2DYR-0E',
        'TCI141220-0UNL-4DYR-0E',
        'TCI141220-0UNL-5DYR-0E',
        'TCI141276-0UNL-2DYR-0E',
        'TCI141330-0UNL-1DYR-0E',
        'TCI141330-0UNL-2DYR-0E',
        'TCI141330-0UNL-3DYR-0E',
        'TCI141333-0UNL-3DYR-0E',
        'TCI141345-0UNL-1DYR-0E',
        'TCI141345-0UNL-2DYR-0E',
        'TCI131515-0E-0DE-0YA-1YA-0E',
        'TCI131547-0E-0DE-0YA-1YA-0E',
        'TCI131549-0E-0DE-0YA-2YA-0E',
        'TCI131549-0E-0DE-0YA-3YA-0E',
        'TCI131571-0E-0DE-0YA-1YA-0E',
        'TCI131571-0E-0DE-0YA-2YA-0E',
        'TCI131579-0E-0DE-0YA-1YA-0E',
        'TCI131579-0E-0DE-0YA-2YA-0E',
        'TCI131581-0E-0DE-0YA-5YA-0E',
        'TCI131627-0E-0DE-0YA-2YA-0E',
        'TCI131627-0E-0DE-0YA-4YA-0E',
        'TCI131627-0E-0DE-0YA-5YA-0E',
        'TCI131627-0E-0DE-0YA-6YA-0E',
        'TCI131636-0E-0DE-0YA-1YA-0E',
        'TCI131636-0E-0DE-0YA-2YA-0E',
        'TCI131645-0E-0DE-0YA-1YA-0E',
        'TCI131645-0E-0DE-0YA-2YA-0E',
        'TCI131645-0E-0DE-0YA-3YA-0E',
        'TCI131645-0E-0DE-0YA-4YA-0E',
        'TCI131645-0E-0DE-0YA-5YA-0E',
        'TCI131649-0E-0DE-0YA-2YA-0E',
        'TCI131649-0E-0DE-0YA-3YA-0E',
        'TCI131670-0E-0DE-0YA-2YA-0E',
        'X1320156-1B-0UNL-0UNL-0KM-1YA-0E',
        'X1320156-1B-0UNL-0UNL-0KM-2YA-0E',
        'X1320156-1B-0UNL-0UNL-0KM-4YA-0E',
        'X1320156-1B-0UNL-0UNL-0KM-5YA-0E',
        'X1320156-1B-0UNL-0UNL-0KM-6YA-0E',
        'X1320776-1B-0UNL-0UNL-0KM-1YA-0E',
        'X1320776-1B-0UNL-0UNL-0KM-5YA-0E',
        'X1320776-1B-0UNL-0UNL-0KM-6YA-0E',
        'X1320835-1B-0UNL-0UNL-0KM-1YA-0E',
        'X1320849-1B-0UNL-0UNL-0KM-2YA-0E',
        'X1320896-1B-0UNL-0UNL-0KM-1YA-0E',
        'X1320896-1B-0UNL-0UNL-0KM-2YA-0E',
        'X1320898-1B-0UNL-0UNL-0KM-2YA-0E',
        'X1320964-1B-0UNL-0UNL-0KM-4YA-0E',
        'X1320964-1B-0UNL-0UNL-0KM-5YA-0E',
        '-0UNL-0UNL-0KM-1YA-0E',
        'TCI121030-0SE-0100TE-5DYR-0E-1E-0E',
        'TCI121031-0SE-0100TE-8DYR-0E-1E-0E',
        'TCI121031-0SE-0100TE-9DYR-0E-1E-0E',
        'TCI121033-0SE-0100TE-10DYR-0E-2E-0E',
        'TCI121033-0SE-0100TE-10DYR-0E-3E-0E',
        'TCI121057-0SE-0100TE-7DYR-0E-2E-0E',
        'TCI121079-0SE-0100TE-11DYR-0E-1E-0E',
        'TCI121084-0SE-0100TE-4DYR-0E-1E-0E',
        'TCI121084-0SE-0100TE-4DYR-0E-2E-0E',
        'TCI121102-0SE-0100TE-8DYR-0E-1E-0E',
        'TCI121102-0SE-0100TE-8DYR-0E-2E-0E',
        'TCI121128-0SE-0100TE-8DYR-0E-1E-0E',
        'TCI121147-0SE-0100TE-1DYR-0E-1E-0E',
        'TCI121155-0SE-0100TE-11DYR-0E-1E-0E',
        'TCI121155-0SE-0100TE-11DYR-0E-2E-0E',
        'TCI121164-0SE-0100TE-1DYR-0E-2E-0E',
        'TCI121205-0SE-0100TE-3DYR-0E-1E-0E',
        'TCI111665-0SE-0TE-050YC-1YC-0E-1E-0E',
        'TCI111665-0SE-0TE-050YC-6YC-0E-1E-0E',
        'TCI111665-0SE-0TE-050YC-8YC-0E-1E-0E',
        'TCI111666-0SE-0TE-050YC-4YC-0E-1E-0E',
        'TCI111666-0SE-0TE-050YC-4YC-0E-3E-0E',
        'OCW12S269T-0OK-0KM-3YA-0E-1E-0E',
        'OCW12S269T-0OK-0KM-3YA-0E-2E-0E',
        'OCW11S308S-0OK-0KM-8YA-0E-1E-0E',
        'OCW11S308S-0OK-0KM-8YA-0E-2E-0E',
        'OCW11S781T-0OK-0KM-4YA-0E-1E-0E',
        'OCW11S781T-0OK-0KM-4YA-0E-2E-0E',
        'TCI111106-0OK-0KM-2YA-0E-2E-0E',
        'OCW12S265T-0OK-0KM-2YA-0E-1E-0E',
        'OCW11S302S-0OK-0OK-0KM-5YA-0E-2E-0E',
        'TCI12SP008-0E-0E-0E-0E-29E-0E',
        'TCI12SP008-0E-0E-0E-0E-66E-0E',
        'TCI12SP008-0E-0E-0E-0E-84E-0E',
        'TCI12SP011-0E-0E-0E-0E-2E-0E',
        'TCI12SP011-0E-0E-0E-0E-14E-0E',
        'TCI12SP022-0E-0E-0E-0E-3E-0E',
        'TCI12SP022-0E-0E-0E-0E-16E-0E',
        'TCI12SP022-0E-0E-0E-0E-18E-0E',
        'TCI12SP022-0E-0E-0E-0E-20E-0E',
        'TCI12SP022-0E-0E-0E-0E-22E-0E',
        'TCI12SP022-0E-0E-0E-0E-42E-0E',
        'TCI12SP022-0E-0E-0E-0E-50E-0E',
        'TCI12SP022-0E-0E-0E-0E-53E-0E',
        'TCI12SP022-0E-0E-0E-0E-54E-0E',
        'TCI12SP022-0E-0E-0E-0E-75E-0E',
        'TCI12SP022-0E-0E-0E-0E-107E-0E',
        'TCI12SP022-0E-0E-0E-0E-114E-0E',
        'TCI12SP022-0E-0E-0E-0E-146E-0E',
        'TCI12SP024-0E-0E-0E-0E-2E-0E',
        'TCI12SP024-0E-0E-0E-0E-9E-0E',
        'TCI12SP024-0E-0E-0E-0E-14E-0E',
        'TCI12SP024-0E-0E-0E-0E-17E-0E',
        'TCI12SP024-0E-0E-0E-0E-20E-0E',
        'TCI12SP024-0E-0E-0E-0E-38E-0E',
        'TCI12SP024-0E-0E-0E-0E-40E-0E',
        'TCI12SP024-0E-0E-0E-0E-43E-0E',
        'TCI12SP024-0E-0E-0E-0E-58E-0E',
        'TCI12SP024-0E-0E-0E-0E-66E-0E',
        'TCI12SP024-0E-0E-0E-0E-72E-0E',
        'TCI12SP024-0E-0E-0E-0E-75E-0E',
        'TCI12SP024-0E-0E-0E-0E-79E-0E',
        'TCI12SP024-0E-0E-0E-0E-82E-0E',
        'TCI12SP024-0E-0E-0E-0E-86E-0E',
        'TCI12SP024-0E-0E-0E-0E-87E-0E',
        'TCI12SP024-0E-0E-0E-0E-92E-0E',
        'TCI12SP024-0E-0E-0E-0E-94E-0E',
        'TCI12SP024-0E-0E-0E-0E-106E-0E',
        'TCI12SP024-0E-0E-0E-0E-107E-0E',
        'TCI12SP024-0E-0E-0E-0E-119E-0E',
        'TCI12SP024-0E-0E-0E-0E-151E-0E',
        'TCI12SP026-0E-0E-0E-0E-10E-0E',
        'TCI12SP027-0E-0E-0E-0E-2E-0E',
        'TCI12SP027-0E-0E-0E-0E-4E-0E',
        'TCI12SP027-0E-0E-0E-0E-7E-0E',
        'TCI12SP027-0E-0E-0E-0E-19E-0E',
        'TCI12SP027-0E-0E-0E-0E-23E-0E',
        'TCI12SP027-0E-0E-0E-0E-33E-0E',
        'TCI12SP027-0E-0E-0E-0E-51E-0E',
        'TCI12SP027-0E-0E-0E-0E-53E-0E',
        'TCI12SP027-0E-0E-0E-0E-59E-0E',
        'TCI12SP027-0E-0E-0E-0E-63E-0E',
        'TCI12SP027-0E-0E-0E-0E-64E-0E',
        'TCI12SP027-0E-0E-0E-0E-84E-0E',
        'TCI12SP027-0E-0E-0E-0E-86E-0E',
        'TCI12SP027-0E-0E-0E-0E-96E-0E',
        'TCI12SP027-0E-0E-0E-0E-97E-0E',
        'TCI12SP027-0E-0E-0E-0E-108E-0E',
        'TCI12SP027-0E-0E-0E-0E-126E-0E',
        'TCI12SP027-0E-0E-0E-0E-129E-0E',
        'TCI12SP027-0E-0E-0E-0E-130E-0E',
        'TCI12SP027-0E-0E-0E-0E-131E-0E',
        'TCI12SP027-0E-0E-0E-0E-134E-0E',
        'TCI12SP027-0E-0E-0E-0E-135E-0E',
        'TCI12SP027-0E-0E-0E-0E-138E-0E',
        'TCI12SP030-0E-0E-0E-0E-6E-0E',
        'TCI12SP030-0E-0E-0E-0E-29E-0E',
        'TCI12SP030-0E-0E-0E-0E-31E-0E',
        'TCI12SP030-0E-0E-0E-0E-32E-0E',
        'TCI12SP030-0E-0E-0E-0E-42E-0E',
        'TCI12SP030-0E-0E-0E-0E-43E-0E',
        'TCI12SP030-0E-0E-0E-0E-44E-0E',
        'TCI12SP032-0E-0E-0E-0E-12E-0E',
        'TCI12SP032-0E-0E-0E-0E-16E-0E',
        'TCI12SP032-0E-0E-0E-0E-29E-0E',
        'TCI12SP032-0E-0E-0E-0E-73E-0E',
        'TCI12SP032-0E-0E-0E-0E-127E-0E',
        'TCI12SP032-0E-0E-0E-0E-134E-0E',
        'TCI12SP036-0E-0E-0E-0E-7E-0E',
        'TCI12SP036-0E-0E-0E-0E-22E-0E',
        'TCI12SP036-0E-0E-0E-0E-37E-0E',
        'TCI12SP037-0E-0E-0E-0E-15E-0E',
        'TCI12SP038-0E-0E-0E-0E-12E-0E',
        'TCI12SP038-0E-0E-0E-0E-15E-0E',
        'TCI12SP038-0E-0E-0E-0E-17E-0E',
        'TCI12SP038-0E-0E-0E-0E-18E-0E',
        'TCI12SP038-0E-0E-0E-0E-30E-0E',
        'TCI12SP039-0E-0E-0E-0E-6E-0E',
        'TCI12SP039-0E-0E-0E-0E-13E-0E',
        'TCI12SP039-0E-0E-0E-0E-23E-0E',
        'TCI12SP040-0E-0E-0E-0E-5E-0E',
        'TCI12SP040-0E-0E-0E-0E-60E-0E',
        'TCI12SP040-0E-0E-0E-0E-66E-0E',
        'TCI12SP040-0E-0E-0E-0E-67E-0E',
        'TCI12SP040-0E-0E-0E-0E-125E-0E',
        'TCI12SP043-0E-0E-0E-0E-2E-0E',
        'TCI12SP043-0E-0E-0E-0E-16E-0E',
        'TCI12SP044-0E-0E-0E-0E-8E-0E',
        'TCI12SP044-0E-0E-0E-0E-13E-0E',
        'TCI12SP044-0E-0E-0E-0E-22E-0E',
        'TCI12SP044-0E-0E-0E-0E-30E-0E',
        'TCI12SP044-0E-0E-0E-0E-36E-0E',
        'TCI12SP044-0E-0E-0E-0E-37E-0E',
        'TCI12SP044-0E-0E-0E-0E-39E-0E',
        'TCI12SP044-0E-0E-0E-0E-42E-0E',
        'TCI12SP045-0E-0E-0E-0E-9E-0E',
        'TCI12SP045-0E-0E-0E-0E-17E-0E',
        'TCI12SP045-0E-0E-0E-0E-45E-0E',
        'TCI12SP045-0E-0E-0E-0E-50E-0E',
        'TCI12SP045-0E-0E-0E-0E-51E-0E',
        'TCI12SP045-0E-0E-0E-0E-59E-0E',
        'TCI12SP045-0E-0E-0E-0E-65E-0E',
        'TCI12SP045-0E-0E-0E-0E-78E-0E',
        'TCI12SP045-0E-0E-0E-0E-84E-0E',
        'TCI12SP045-0E-0E-0E-0E-96E-0E',
        'TCI12SP045-0E-0E-0E-0E-113E-0E',
        'TCI12SP046-0E-0E-0E-0E-2E-0E',
        'TCI12SP046-0E-0E-0E-0E-20E-0E',
        'TCI12SP046-0E-0E-0E-0E-30E-0E',
        'TCI12SP046-0E-0E-0E-0E-34E-0E',
        'TCI12SP046-0E-0E-0E-0E-35E-0E',
        'TCI12SP046-0E-0E-0E-0E-37E-0E',
        'TCI12SP046-0E-0E-0E-0E-39E-0E',
        'TCI12SP046-0E-0E-0E-0E-41E-0E',
        'TCI12SP046-0E-0E-0E-0E-49E-0E',
        'TCI12SP046-0E-0E-0E-0E-68E-0E',
        'TCI12SP046-0E-0E-0E-0E-74E-0E',
        'TCI12SP046-0E-0E-0E-0E-78E-0E',
        'TCI12SP046-0E-0E-0E-0E-83E-0E',
        'TCI12SP046-0E-0E-0E-0E-84E-0E',
        'TCI12SP046-0E-0E-0E-0E-87E-0E',
        'TCI12SP046-0E-0E-0E-0E-91E-0E',
        'TCI12SP046-0E-0E-0E-0E-100E-0E',
        'TCI12SP046-0E-0E-0E-0E-103E-0E',
        'TCI12SP046-0E-0E-0E-0E-119E-0E',
        'TCI12SP046-0E-0E-0E-0E-122E-0E',
        'TCI12SP046-0E-0E-0E-0E-133E-0E',
        'TCI12SP046-0E-0E-0E-0E-149E-0E',
        'TCI12SP046-0E-0E-0E-0E-155E-0E',
        'TCI12SP047-0E-0E-0E-0E-2E-0E',
        'TCI12SP048-0E-0E-0E-0E-12E-0E',
        'TCI12SP048-0E-0E-0E-0E-13E-0E',
        'TCI12SP048-0E-0E-0E-0E-23E-0E',
        'TCI151485-0SE-1YC-0YM',
        'TCI151491-0SE-6YC-0YM',
        'TCI151532-0SE-7YC-0YM',
        'TCI151534-0SE-1YC-0YM',
        'TCI141110-0SE-0100TE-16YC-0YM',
        'TCI141118-0SE-0100TE-19YC-0YM',
        'TCI141132-0SE-0100TE-5YC-0YM',
        'TCI141132-0SE-0100TE-10YC-0YM',
        'TCI141144-0SE-0100TE-22YC-0YM',
        'TCI141144-0SE-0100TE-23YC-0YM',
        'TCI141147-0SE-0100TE-5YC-0YM',
        'TCI141151-0SE-0100TE-2YC-0YM',
        'TCI141151-0SE-0100TE-7YC-0YM',
        'TCI141151-0SE-0100TE-9YC-0YM',
        'TCI141151-0SE-0100TE-11YC-0YM',
        'TCI141151-0SE-0100TE-15YC-0YM',
        'TCI141151-0SE-0100TE-17YC-0YM',
        'TCI141151-0SE-0100TE-23YC-0YM',
        'TCI141152-0SE-0100TE-16YC-0YM',
        'TCI141152-0SE-0100TE-17YC-0YM',
        'TCI141152-0SE-0100TE-19YC-0YM',
        'TCI141152-0SE-0100TE-20YC-0YM',
        'TCI141152-0SE-0100TE-22YC-0YM',
        'TCI141172-0SE-0100TE-9YC-0YM',
        'TCI141172-0SE-0100TE-16YC-0YM',
        'TCI141172-0SE-0100TE-22YC-0YM',
        'TCI141299-0SE-0100TE-5YC-0YM',
        'TCI141299-0SE-0100TE-18YC-0YM',
        'TCI141325-0SE-0100TE-14YC-0YM',
        'TCI141325-0SE-0100TE-17YC-0YM',
        'TCI141327-0SE-0100TE-2YC-0YM',
        'TCI142180-0SE-0100TE-17YC-0YM',
        'TCI131177-0SE-0100TE-020DYR-1SEGH-4DE-0YM'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback 	experiment.entry AS e
--rollback USING
--rollback 	experiment.entry_list AS el
--rollback INNER JOIN
--rollback 	experiment.experiment AS ee
--rollback ON
--rollback 	el.experiment_id = ee.id
--rollback WHERE
--rollback  	ee.experiment_name='WW-AYT-2021-DS-001'
--rollback AND
--rollback 	e.entry_list_id = el.id;



--changeset postgres:populate_occurrence_for_winter_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate occurrence for Winter Wheat exp



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT 
    experiment.generate_code('occurrence') AS occurrence_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS occurrence_name,
    'planted' AS occurrence_status,
    exp.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    1 AS rep_count,
    1 AS occurrence_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='WW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name='WW-AYT-2021-DS-001')
--rollback ;



--changeset postgres:populate_geospatial_object_for_winter_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate geospatial_object for Winter Wheat exp



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS geospatial_object_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name,
    'planting area' AS geospatial_object_type,
    'breeding location' AS geospatial_object_subtype,
    exp.creator_id AS creator_id,
    field.id AS parent_geospatial_object_id,
    site.id AS root_geospatial_object_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='WW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='WW-AYT-2021-DS-001'
--rollback );



--changeset postgres:populate_location_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate location for Winter Wheat exp



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS location_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name,
    'planted' AS location_status,
    'planting area' AS location_type,
    exp.experiment_year AS location_year,
    tss.id AS season_id ,
    1 AS location_number,
    site.id AS site_id,
    field.id AS field_id,
    exp.creator_id AS steward_id,
    pgo.id AS geospatial_object_id,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    place.geospatial_object pgo
ON
    pgo.geospatial_object_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001'),
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field

WHERE
    exp.experiment_name='WW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',exp.experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='WW-AYT-2021-DS-001'
--rollback );



--changeset postgres:populate_location_occurrence_group_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate location_occurrence_group for Winter Wheat exp



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment exp
INNER JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    experiment.occurrence occ
ON
    occ.experiment_id = exp.id
INNER JOIN
    experiment.location loc
ON
    loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
WHERE
    exp.experiment_name = 'WW-AYT-2021-DS-001'
ORDER BY
    occ.id
;
    


--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     occurrence_id 
--rollback IN 
--rollback     (
--rollback      SELECT
--rollback          occ.id AS occurrence_id
--rollback      FROM
--rollback          experiment.experiment exp
--rollback      INNER JOIN
--rollback          tenant.season tss
--rollback      ON
--rollback          tss.id = exp.season_id
--rollback      INNER JOIN
--rollback          tenant.stage ts
--rollback      ON
--rollback          ts.id = exp.stage_id
--rollback      INNER JOIN
--rollback          tenant.program tp
--rollback      ON
--rollback          exp.program_id = tp.id
--rollback      INNER JOIN
--rollback          experiment.occurrence occ
--rollback      ON
--rollback          occ.experiment_id = exp.id
--rollback      INNER JOIN
--rollback          experiment.location loc
--rollback      ON
--rollback          loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
--rollback      WHERE
--rollback          exp.experiment_name = 'WW-AYT-2021-DS-001'
--rollback      ORDER BY
--rollback          occ.id
--rollback     )
--rollback ;



--changeset postgres:populate_plot_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate plot for Winter Wheat exp



WITH t1 AS (
    SELECT
        eo.id AS occurrence_id,
        loc.id AS location_id,
        ee.id AS entry_id,
        'plot' AS plot_type,
        generate_series(1,2) AS rep,
        ee.creator_id
    FROM
        experiment.experiment exp
    INNER JOIN
        experiment.entry_list el
    ON
        el.experiment_id = exp.id
    INNER JOIN
        experiment.entry ee
    ON
        ee.entry_list_id = el.id
    INNER JOIN
        experiment.occurrence eo
    ON
        eo.experiment_id = exp.id
    INNER JOIN
        experiment.location_occurrence_group elo
    ON
        elo.occurrence_id = eo.id
    INNER JOIN
        experiment.location loc
    ON
        elo.location_id = loc.id
    WHERE
        exp.experiment_name = 'WW-AYT-2021-DS-001'
    ORDER BY
        rep,
        ee.id
), t2 AS (
    SELECT
        *,
        ROW_NUMBER() OVER() AS plot_code
    FROM
        t1 t
), t3 AS (
    SELECT 
        occurrence_id,
        location_id, 
        entry_id,
        plot_code, 
        plot_code AS plot_number, 
        plot_type, 
        rep,
        1 AS design_x,
        plot_code AS design_y,
        1 AS pa_x,
        plot_code AS pa_y,
        1 AS field_x,
        plot_code AS field_y,
        rep AS block_number,
        'active' AS plot_status,
        'Q' AS plot_qc_code,
        creator_id,
        'NO_HARVEST' AS harvest_status
    FROM
        t2 t
)
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT t.* FROM t3 t;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plot.occurrence_id = occ.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='WW-AYT-2021-DS-001'
--rollback 	)
--rollback ;



--changeset postgres:populate_plot_data_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate plot data for Winter Wheat exp



INSERT INTO
    experiment.plot_data
        (plot_id, variable_id, data_value, data_qc_code, creator_id)

SELECT
    plt.id plot_id,
    mv.id variable_id,
    'Bulk' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HV_METH_DISC'
WHERE
    exp.experiment_name = 'WW-AYT-2021-DS-001'

UNION ALL

SELECT
    plt.id plot_id,
    mv.id variable_id,
    '2021-06-09' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HVDATE_CONT'
WHERE
    exp.experiment_name = 'WW-AYT-2021-DS-001'
;



--rollback DELETE FROM
--rollback     experiment.plot_data AS pltd
--rollback USING
--rollback 	experiment.plot AS plt,
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plt.occurrence_id = occ.id
--rollback AND
--rollback 	pltd.plot_id = plt.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='WW-AYT-2021-DS-001'
--rollback 	)
--rollback ;



--changeset postgres:populate_planting_instruction_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate planting instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ee.entry_code,
    ee.entry_number,
    ee.entry_name,
    ee.entry_type,
    ee.entry_role,
    ee.entry_status,
    ee.id AS entry_id,
    plt.id AS plot_id,
    ee.germplasm_id,
    ee.seed_id,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.entry ee
ON
    ee.entry_list_id = el.id
INNER JOIN
    experiment.plot plt
ON
    plt.entry_id = ee.id
WHERE
    exp.experiment_name = 'WW-AYT-2021-DS-001'
ORDER BY
    ee.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback      experiment.experiment exp
--rollback INNER JOIN
--rollback     experiment.entry_list el
--rollback ON
--rollback     el.experiment_id = exp.id
--rollback INNER JOIN
--rollback     experiment.entry ee
--rollback ON
--rollback     ee.entry_list_id = el.id
--rollback INNER JOIN
--rollback     experiment.plot plt
--rollback ON
--rollback     plt.entry_id = ee.id
--rollback WHERE
--rollback     exp.experiment_name = 'WW-AYT-2021-DS-001'
--rollback AND
--rollback     plantinst.entry_id = ee.id
--rollback AND
--rollback     plantinst.plot_id = plt.id
--rollback ;



--changeset postgres:populate_experiment_design_for_winter_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment design for Winter Wheat exp



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    eo.id AS occurrence_id,
    ep.rep AS design_id,
    ep.id AS plot_id,
    'replication block',
    ep.rep AS block_value,
    ep.rep AS block_level_number,
    exp.creator_id AS creator_id,
    'replicate' AS block_name
FROM 
    experiment.experiment exp
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.plot ep
ON
    ep.occurrence_id = eo.id
WHERE 
    exp.experiment_name='WW-AYT-2021-DS-001'
;



--rollback DELETE FROM 
--rollback     experiment.experiment_design ee
--rollback USING 
--rollback     experiment.occurrence eo,
--rollback     experiment.experiment exp
--rollback WHERE
--rollback     eo.experiment_id = exp.id
--rollback AND
--rollback     ee.occurrence_id = eo.id
--rollback AND
--rollback     experiment_name = 'WW-AYT-2021-DS-001'
--rollback ;