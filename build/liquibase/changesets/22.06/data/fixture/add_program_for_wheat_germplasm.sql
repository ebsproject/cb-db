--liquibase formatted sql

--changeset postgres:add_crop_program_for_wheat_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1268 Add crop program for wheat germplasm



INSERT INTO
    tenant.crop_program (
        crop_program_code,
        crop_program_name,
        description,
        organization_id,
        crop_id,
        creator_id
    )
SELECT 
    t.crop_program_code,
    t.crop_program_name,
    t.description,
    tor.id organization_id,
    tc.id crop_id,
    1 creator_id
FROM
    (
        VALUES
            ('DWP', 'Durum Wheat Program', 'Durum wheat program'),
            ('WWP', 'Winter Wheat Program', 'Winter wheat program'),
            ('TCLP', 'Triticale Wheat Program', 'Triticale wheat program')
    ) AS t (crop_program_code,crop_program_name,description)
INNER JOIN
    tenant.organization tor
ON
    tor.organization_code = 'CIMMYT'
INNER JOIN
    tenant.crop tc
ON
    tc.crop_code = 'WHEAT'
;



--rollback DELETE FROM tenant.crop_program WHERE crop_program_code IN ('DWP','WWP','TCLP');



--changeset postgres:add_program_for_wheat_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1268 Add program for wheat germplasm



INSERT INTO
    tenant.program (
       program_code,
       program_name,
       program_type,
       program_status,
       description,
       crop_program_id,
       creator_id
    )
SELECT
    program_code,
    program_name,
    program_type,
    program_status,
    description,
    crop_program_id,
    1 creator_id
FROM
    (
        VALUES
            ('DW', 'Durum Wheat Program', 'breeding', 'active','Durum Wheat Program',(SELECT id FROM tenant.crop_program WHERE crop_program_code='DWP')),
            ('WW', 'Winter Wheat Program', 'breeding', 'active','Winter Wheat Program',(SELECT id FROM tenant.crop_program WHERE crop_program_code='WWP')),
            ('TCL', 'Triticale Wheat Program', 'breeding', 'active','Triticale Wheat Program',(SELECT id FROM tenant.crop_program WHERE crop_program_code='TCLP'))
    ) AS t (program_code,program_name,program_type,program_status,description,crop_program_id)
;



--rollback DELETE FROM tenant.program WHERE program_code IN ('DW','WW','TCL');