--liquibase formatted sql

--changeset postgres:populate_durum_wheat_experiments context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate Durum Wheat experiments



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'DW') AS program_id,
    NULL pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'AYT') AS stage_id,
    NULL project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'DS') AS season_id,
    'DS' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'DW-AYT-2021-DS-001' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    NULL AS experiment_sub_type,
    NULL AS experiment_sub_sub_type,
    'RCBD' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'DW-AYT-2021-DS-001'
--rollback ;



--changeset postgres:populate_protocol_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate protocol for Durum Wheat experiment



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
SELECT
    CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code,
    CONCAT(INITCAP(ptc.protocol_type),' Protocol ',UPPER(exp.experiment_name)) AS protocol_name,
    ptc.protocol_type AS protocol_type,
    exp.program_id AS program_id,
    exp.creator_id AS creator_id
FROM 
(
    SELECT 
        experiment_name,
        program_id,
        creator_id
    FROM
        experiment.experiment
    WHERE
        experiment_name = 'DW-AYT-2021-DS-001'
) AS exp,
(
    VALUES
        ('trait'),
        ('management'),
        ('planting'),
        ('harvest')
) AS ptc (protocol_type)



--rollback DELETE FROM 
--rollback     tenant.protocol 
--rollback WHERE 
--rollback     protocol_code 
--rollback IN 
--rollback     (
--rollback         SELECT
--rollback             CONCAT(UPPER(ptc.protocol_type),'_PROTOCOL_',UPPER(exp.experiment_name)) AS protocol_code
--rollback         FROM 
--rollback         (
--rollback             SELECT 
--rollback                 experiment_name,
--rollback                 program_id,
--rollback                 creator_id
--rollback             FROM
--rollback                 experiment.experiment
--rollback             WHERE
--rollback                 experiment_name = 'DW-AYT-2021-DS-001'
--rollback         ) AS exp,
--rollback         (
--rollback             VALUES
--rollback                 ('trait'),
--rollback                 ('management'),
--rollback                 ('planting'),
--rollback                 ('harvest')
--rollback         ) AS ptc (protocol_type)
--rollback 
--rollback     )
--rollback ;



--changeset postgres:populate_experiment_data_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment_data for Durum Wheat experiment



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    mv.id AS variable_id,
    exp_data.data_value AS data_value,
    'N' AS data_qc_code,
    tp.id AS protocol_id,
    exp.creator_id AS creator_id
FROM	
    experiment.experiment AS exp,
    (
        VALUES
            ('FIRST_PLOT_POSITION_VIEW','Top Left',NULL),
            ('TRAIT_PROTOCOL_LIST_ID','1393','trait'),
            ('MANAGEMENT_PROTOCOL_LIST_ID','1394','management'),
            ('ESTABLISHMENT','transplanted','planting'),
            ('PLANTING_TYPE','Flat','planting'),
            ('PLOT_TYPE','6R','planting'),
            ('ROWS_PER_PLOT_CONT','6','planting'),
            ('DIST_BET_ROWS','20','planting'),
            ('PLOT_WIDTH','1.2','planting'),
            ('PLOT_LN','20','planting'),
            ('PLOT_AREA_2','24','planting'),
            ('ALLEY_LENGTH','1','planting'),
            ('SEEDING_RATE','Normal','planting'),
            ('PROTOCOL_TARGET_LEVEL','occurrence',NULL),
            ('HV_METH_DISC','Bulk','harvest')
    ) AS exp_data (variable_abbrev, data_value,protocol_type)
    INNER JOIN
        master.variable mv
    ON
        mv.abbrev = exp_data.variable_abbrev
    LEFT JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(exp_data.protocol_type),'_PROTOCOL_',UPPER('DW-AYT-2021-DS-001')))
WHERE
    exp.experiment_name='DW-AYT-2021-DS-001'
;



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='DW-AYT-2021-DS-001');



--changeset postgres:populate_experiment_protocol_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment_protocol for Durum Wheat exp



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
SELECT
    exp.id AS experiment_id,
    tp.id AS protocol_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM 	
    experiment.experiment AS exp,
    (
        VALUES
            ('trait'),
            ('management'),
            ('planting'),
            ('harvest')
    ) AS t (protocol_type)
    INNER JOIN
        tenant.protocol tp
    ON
        tp.protocol_code = (CONCAT(UPPER(t.protocol_type),'_PROTOCOL_',UPPER('DW-AYT-2021-DS-001')))
WHERE	
    exp.experiment_name='DW-AYT-2021-DS-001'
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='DW-AYT-2021-DS-001');



--changeset postgres:populate_entry_list_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate entry_list for Durum Wheat exp



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT 
	experiment.generate_code('entry_list') AS entry_list_code,
  	CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS entry_list_name,
	'completed' AS entry_list_status,
	exp.id AS experiment_id,
	exp.creator_id AS creator_id,
	FALSE AS is_void,
	'entry list' AS entry_list_type
FROM
	experiment.experiment AS exp
JOIN
	tenant.stage ts
ON
	ts.id = exp.stage_id
JOIN
	tenant.season tss
ON
	tss.id = exp.season_id
JOIN
	tenant.program tp
ON
	exp.program_id = tp.id
WHERE
	experiment_name='DW-AYT-2021-DS-001'
;    
    


-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='DW-AYT-2021-DS-001');



--changeset postgres:populate_entry_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate entry for Durum Wheat exp



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, package_id, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT 
    ROW_NUMBER() OVER() AS entry_code,
    ROW_NUMBER() OVER() AS entry_number,
    gg.designation AS entry_name,
    'test' AS entry_type,
    NULL AS entry_class,
    gp.id AS package_id,
    NULL AS entry_role,
    'active' AS entry_status,
    el.id AS entry_list_id,
    gg.id AS germplasm_id,
    gs.id AS seed_id,
    ee.creator_id AS creator_id,
    FALSE AS is_void
FROM 
    germplasm.germplasm gg
INNER JOIN
    germplasm.seed gs
ON
    gs.germplasm_id = gg.id
INNER JOIN
    germplasm.package gp
ON
    gp.seed_id = gs.id
INNER JOIN
    experiment.experiment ee
ON
    ee.experiment_name = 'DW-AYT-2021-DS-001'
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = ee.id
WHERE
    gg.designation
IN
    (
        'CM470-1M-3Y-0M',
        'CM9799-126M-1M-5Y-0M',
        'CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y',
        'CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX',
        'CDSS02B00849T-0TOPB-0Y-0M-7Y-2M-04Y-0B',
        'CDSS06B00461T-099Y-099M-15Y-1M-04Y-0B',
        'CDSS09Y00246S-099Y-038M-5Y-0M-04Y-0B',
        'CDSS09Y00285S-099Y-027M-23Y-0M-04Y-0B',
        'CDSS09Y00298S-099Y-043M-27Y-0M-04Y-0B',
        'CDSS09Y00386S-099Y-058M-23Y-0M-04Y-0B',
        'CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B',
        'CDSS09Y00762T-099Y-024M-27Y-0M-04Y-0B',
        'CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B',
        'CDSS09Y00771T-099Y-040M-10Y-0M-04Y-0B',
        'CDSS09Y00795T-099Y-024M-29Y-0M-04Y-0B',
        'CDSS09Y00449S-099Y-011M-10Y-0M-04Y-0B',
        'CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B',
        'CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B',
        'CDSS09Y00970T-099Y-063M-11Y-0M-04Y-0B',
        'CDSS09B00077S-099Y-014M-4Y-3M-06Y-0B',
        'CDSS10Y00288S-099Y-038M-17Y-1M-06Y-0B',
        'CDSS10Y00291S-099Y-044M-5Y-2M-06Y-0B',
        'CDSS10Y00491T-099Y-040M-7Y-1M-06Y-0B',
        'CDSS10Y00493T-099Y-035M-9Y-4M-06Y-0B',
        'CDSS10Y00498T-099Y-018M-14Y-4M-06Y-0B',
        'CDSS10Y00500T-099Y-028M-1Y-4M-06Y-0B',
        'CDSS10Y00504T-099Y-037M-5Y-2M-06Y-0B',
        'CDSS10Y00517T-099Y-055M-13Y-4M-06Y-0B',
        'CDSS10Y00524T-099Y-044M-4Y-1M-06Y-0B',
        'CDSS09B00165S-099Y-010M-10Y-3M-06Y-0B',
        'CDSS09B00167S-099Y-019M-4Y-1M-06Y-0B',
        'CDSS09B00170S-099Y-011M-2Y-4M-06Y-0B',
        'CDSS09B00171S-099Y-041M-11Y-3M-06Y-0B',
        'CDSS09B00261T-099Y-046M-7Y-2M-06Y-0B',
        'CDSS09B00268T-099Y-050M-3Y-4M-06Y-0B',
        'CDSS09B00347T-099Y-033M-13Y-3M-06Y-0B',
        'CDSS10Y00539T-099Y-025M-29Y-1M-06Y-0B',
        'CDSS10Y00550T-099Y-014M-7Y-3M-06Y-0B',
        'CDSS10Y00550T-099Y-014M-8Y-3M-06Y-0B',
        'CDSS10Y00553T-099Y-068M-8Y-3M-06Y-0B',
        'CDSS10Y00556T-099Y-031M-16Y-2M-06Y-0B',
        'CDSS09B00483D-099Y-032M-2Y-1M-06Y-0B',
        'CDSS09B00490D-099Y-032M-11Y-3M-06Y-0B',
        'CDSS10Y00572T-099Y-030M-13Y-1M-06Y-0B',
        'CDSS10Y00573T-099Y-056M-4Y-4M-06Y-0B',
        'CDSS09B00128S-099Y-057M-1Y-1M-06Y-0B',
        'CDSS09B00150S-099Y-023M-4Y-1M-06Y-0B',
        'CDSS10Y00017S-099Y-034M-7Y-1M-06Y-0B',
        'CMSS08B01003S-099B-099Y-22B-0Y',
        'CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B',
        'CDSS09Y00310S-099Y-034M-17Y-0M-04Y-0B',
        'CMSS08B01019S-099B-099Y-21B-0Y',
        'CDSS08Y00333S-099Y-022M-11Y-3M-0Y',
        'CDSS09Y00933T-099Y-023M-6Y-0M-04Y-0B',
        'CDSS09Y00911T-099Y-024M-26Y-0M-04Y-0B',
        'CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B',
        'CDSS07B00661T-0TOPY-099Y-029M-5Y-3M-0Y',
        'CDSS09Y00209S-099Y-052M-36Y-0M-04Y-0B',
        'CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y',
        'CDSS08Y00237S-099Y-020M-24Y-3M-0Y',
        'CMSS08B01001S-099B-099Y-41B-0Y',
        'CDSS09Y00286S-099Y-026M-24Y-0M-04Y-0B',
        'CDSS09Y00994T-099Y-020M-5Y-0M-04Y-0B',
        'CDSS08Y00433S-099Y-016M-26Y-4M-0Y',
        'CDSS08Y00583S-099Y-023M-7Y-0M-04Y-0B',
        'CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B',
        'CDSS09Y00930T-099Y-031M-13Y-0M-04Y-0B',
        'CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX',
        'CM9799-126M-1M-5Y-0M',
        'CDSS09Y00347S-099Y-020M-33Y-0M-04Y-0B',
        'CDSS09Y00350S-099Y-023M-3Y-0M-04Y-0B',
        'CDSS09Y00041S-099Y-048M-41Y-0M-04Y-0B',
        'CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B',
        'CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B',
        'CDSS08Y00337S-099Y-014M-19Y-2M-0Y',
        'CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B',
        'CDSS09Y00771T-099Y-040M-4Y-0M-04Y-0B',
        'CDSS08Y00433S-099Y-016M-26Y-4M-0Y',
        'CDSS09Y00327S-099Y-041M-19Y-0M-04Y-0B',
        'CDSS09Y00762T-099Y-024M-19Y-0M-04Y-0B',
        'CDSS09Y00285S-099Y-027M-24Y-0M-04Y-0B',
        'CDSS09Y00270S-099Y-050M-11Y-0M-04Y-0B',
        'CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y',
        'CDSS09Y00400S-099Y-030M-19Y-0M-04Y-0B',
        'CDSS08Y00237S-099Y-020M-24Y-3M-0Y',
        'CDSS09Y00765T-099Y-018M-1Y-0M-04Y-0B',
        'CDSS09Y00286S-099Y-026M-8Y-0M-04Y-0B',
        'CDSS09Y00939T-099Y-012M-19Y-0M-04Y-0B',
        'CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B',
        'CDSS09Y00907T-099Y-024M-14Y-0M-04Y-0B',
        'CDSS08Y00337S-099Y-014M-19Y-2M-0Y',
        'CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B',
        'CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B',
        'CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y',
        'CDSS09Y00759T-099Y-033M-28Y-0M-04Y-0B',
        'CDSS09Y00771T-099Y-040M-38Y-0M-04Y-0B',
        'CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B',
        'CDSS09Y00216S-099Y-032M-2Y-0M-04Y-0B',
        'CDSS09Y00241S-099Y-022M-10Y-0M-04Y-0B',
        'CMSS08B00996S-099B-099Y-23B-0Y',
        'CDSS09Y00386S-099Y-058M-30Y-0M-04Y-0B'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback 	experiment.entry AS e
--rollback USING
--rollback 	experiment.entry_list AS el
--rollback INNER JOIN
--rollback 	experiment.experiment AS ee
--rollback ON
--rollback 	el.experiment_id = ee.id
--rollback WHERE
--rollback  	ee.experiment_name='DW-AYT-2021-DS-001'
--rollback AND
--rollback 	e.entry_list_id = el.id;



--changeset postgres:populate_occurrence_for_durum_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate occurrence for Durum Wheat exp



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT 
    experiment.generate_code('occurrence') AS occurrence_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS occurrence_name,
    'planted' AS occurrence_status,
    exp.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    1 AS rep_count,
    1 AS occurrence_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='DW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name='DW-AYT-2021-DS-001')
--rollback ;



--changeset postgres:populate_geospatial_object_for_durum_wheat context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate geospatial_object for Durum Wheat exp



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS geospatial_object_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name,
    'planting area' AS geospatial_object_type,
    'breeding location' AS geospatial_object_subtype,
    exp.creator_id AS creator_id,
    field.id AS parent_geospatial_object_id,
    site.id AS root_geospatial_object_id
FROM
    experiment.experiment AS exp
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id,
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field
WHERE
    experiment_name='DW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS geospatial_object_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='DW-AYT-2021-DS-001'
--rollback );



--changeset postgres:populate_location_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate location for Durum Wheat exp



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
SELECT 
    CONCAT(site.geospatial_object_code,'-',exp.experiment_name) AS location_code,
    CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name,
    'planted' AS location_status,
    'planting area' AS location_type,
    exp.experiment_year AS location_year,
    tss.id AS season_id ,
    1 AS location_number,
    site.id AS site_id,
    field.id AS field_id,
    exp.creator_id AS steward_id,
    pgo.id AS geospatial_object_id,
    exp.creator_id AS creator_id
FROM
    experiment.experiment AS exp
JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    place.geospatial_object pgo
ON
    pgo.geospatial_object_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001'),
(
        VALUES
            ('IRRI, Los Baños, Laguna, Philippines','400')
) AS t (site, field)
INNER JOIN
    place.geospatial_object site
ON
    site.geospatial_object_name = t.site
INNER JOIN
    place.geospatial_object field
ON
    field.geospatial_object_name = t.field

WHERE
    exp.experiment_name='DW-AYT-2021-DS-001'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN (
--rollback          SELECT 
--rollback              CONCAT(tp.program_code,'-',ts.stage_code,'-',exp.experiment_year,'-',tss.season_code,'-00',ROW_NUMBER() OVER()) AS location_name
--rollback          FROM
--rollback              experiment.experiment AS exp
--rollback          INNER JOIN
--rollback              tenant.stage ts
--rollback          ON
--rollback              ts.id = exp.stage_id
--rollback          JOIN
--rollback              tenant.season tss
--rollback          ON
--rollback              tss.id = exp.season_id
--rollback          INNER JOIN
--rollback              tenant.program tp
--rollback          ON
--rollback              exp.program_id = tp.id,
--rollback          (
--rollback                  VALUES
--rollback                      ('IRRI, Los Baños, Laguna, Philippines','400')
--rollback          ) AS t (site, field)
--rollback          INNER JOIN
--rollback              place.geospatial_object site
--rollback          ON
--rollback              site.geospatial_object_name = t.site
--rollback          INNER JOIN
--rollback              place.geospatial_object field
--rollback          ON
--rollback              field.geospatial_object_name = t.field
--rollback          WHERE
--rollback              experiment_name='DW-AYT-2021-DS-001'
--rollback );



--changeset postgres:populate_location_occurrence_group_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate location_occurrence_group for Durum Wheat exp



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
SELECT
    loc.id AS location_id,
    occ.id AS occurrence_id,
    ROW_NUMBER() OVER() AS order_number,
    exp.creator_id AS creator_id
FROM
    experiment.experiment exp
INNER JOIN
    tenant.season tss
ON
    tss.id = exp.season_id
INNER JOIN
    tenant.stage ts
ON
    ts.id = exp.stage_id
INNER JOIN
    tenant.program tp
ON
    exp.program_id = tp.id
INNER JOIN
    experiment.occurrence occ
ON
    occ.experiment_id = exp.id
INNER JOIN
    experiment.location loc
ON
    loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
WHERE
    exp.experiment_name = 'DW-AYT-2021-DS-001'
ORDER BY
    occ.id
;


--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     occurrence_id 
--rollback IN 
--rollback     (
--rollback      SELECT
--rollback          occ.id AS occurrence_id
--rollback      FROM
--rollback          experiment.experiment exp
--rollback      INNER JOIN
--rollback          tenant.season tss
--rollback      ON
--rollback          tss.id = exp.season_id
--rollback      INNER JOIN
--rollback          tenant.stage ts
--rollback      ON
--rollback          ts.id = exp.stage_id
--rollback      INNER JOIN
--rollback          tenant.program tp
--rollback      ON
--rollback          exp.program_id = tp.id
--rollback      INNER JOIN
--rollback          experiment.occurrence occ
--rollback      ON
--rollback          occ.experiment_id = exp.id
--rollback      INNER JOIN
--rollback          experiment.location loc
--rollback      ON
--rollback          loc.location_name = CONCAT(tp.program_code,'-',ts.stage_code,'-',experiment_year,'-',tss.season_code,'-001')
--rollback      WHERE
--rollback          exp.experiment_name = 'DW-AYT-2021-DS-001'
--rollback      ORDER BY
--rollback          occ.id
--rollback     )
--rollback ;



--changeset postgres:populate_plot_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate plot for Durum Wheat exp



WITH t1 AS (
    SELECT
        eo.id AS occurrence_id,
        loc.id AS location_id,
        ee.id AS entry_id,
        'plot' AS plot_type,
        generate_series(1,2) AS rep,
        ee.creator_id
    FROM
        experiment.experiment exp
    INNER JOIN
        experiment.entry_list el
    ON
        el.experiment_id = exp.id
    INNER JOIN
        experiment.entry ee
    ON
        ee.entry_list_id = el.id
    INNER JOIN
        experiment.occurrence eo
    ON
        eo.experiment_id = exp.id
    INNER JOIN
        experiment.location_occurrence_group elo
    ON
        elo.occurrence_id = eo.id
    INNER JOIN
        experiment.location loc
    ON
        elo.location_id = loc.id
    WHERE
        exp.experiment_name = 'DW-AYT-2021-DS-001'
    ORDER BY
        rep,
        ee.id
), t2 AS (
    SELECT
        *,
        ROW_NUMBER() OVER() AS plot_code
    FROM
        t1 t
), t3 AS (
    SELECT 
        occurrence_id,
        location_id, 
        entry_id,
        plot_code, 
        plot_code AS plot_number, 
        plot_type, 
        rep,
        1 AS design_x,
        plot_code AS design_y,
        1 AS pa_x,
        plot_code AS pa_y,
        1 AS field_x,
        plot_code AS field_y,
        rep AS block_number,
        'active' AS plot_status,
        'Q' AS plot_qc_code,
        creator_id,
        'NO_HARVEST' AS harvest_status
    FROM
        t2 t
)
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT t.* FROM t3 t;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plot.occurrence_id = occ.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='DW-AYT-2021-DS-001'
--rollback 	)
--rollback ;



--changeset postgres:populate_plot_data_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate plot data for Durum Wheat exp



INSERT INTO
    experiment.plot_data
        (plot_id, variable_id, data_value, data_qc_code, creator_id)

SELECT
    plt.id plot_id,
    mv.id variable_id,
    'Bulk' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HV_METH_DISC'
WHERE
    exp.experiment_name = 'DW-AYT-2021-DS-001'

UNION ALL

SELECT
    plt.id plot_id,
    mv.id variable_id,
    '2021-06-09' data_value,
    'Q' data_qc_code,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.location_occurrence_group elo
ON
    elo.occurrence_id = eo.id
INNER JOIN
    experiment.location loc
ON
    elo.location_id = loc.id
INNER JOIN
    experiment.plot plt
ON
    plt.occurrence_id = eo.id
INNER JOIN
    master.variable mv
ON
    mv.abbrev='HVDATE_CONT'
WHERE
    exp.experiment_name = 'DW-AYT-2021-DS-001'
;



--rollback DELETE FROM
--rollback     experiment.plot_data AS pltd
--rollback USING
--rollback 	experiment.plot AS plt,
--rollback 	experiment.occurrence AS occ
--rollback WHERE
--rollback 	plt.occurrence_id = occ.id
--rollback AND
--rollback 	pltd.plot_id = plt.id
--rollback AND 
--rollback 	occ.experiment_id IN (
--rollback 	SELECT 
--rollback 		id
--rollback 	FROM
--rollback 		experiment.experiment
--rollback 	WHERE
--rollback 		experiment_name='DW-AYT-2021-DS-001'
--rollback 	)
--rollback ;



--changeset postgres:populate_planting_instruction_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate planting instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ee.entry_code,
    ee.entry_number,
    ee.entry_name,
    ee.entry_type,
    ee.entry_role,
    ee.entry_status,
    ee.id AS entry_id,
    plt.id AS plot_id,
    ee.germplasm_id,
    ee.seed_id,
    exp.creator_id
FROM
    experiment.experiment exp
INNER JOIN
    experiment.entry_list el
ON
    el.experiment_id = exp.id
INNER JOIN
    experiment.entry ee
ON
    ee.entry_list_id = el.id
INNER JOIN
    experiment.plot plt
ON
    plt.entry_id = ee.id
WHERE
    exp.experiment_name = 'DW-AYT-2021-DS-001'
ORDER BY
    ee.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback      experiment.experiment exp
--rollback INNER JOIN
--rollback     experiment.entry_list el
--rollback ON
--rollback     el.experiment_id = exp.id
--rollback INNER JOIN
--rollback     experiment.entry ee
--rollback ON
--rollback     ee.entry_list_id = el.id
--rollback INNER JOIN
--rollback     experiment.plot plt
--rollback ON
--rollback     plt.entry_id = ee.id
--rollback WHERE
--rollback     exp.experiment_name = 'DW-AYT-2021-DS-001'
--rollback AND
--rollback     plantinst.entry_id = ee.id
--rollback AND
--rollback     plantinst.plot_id = plt.id
--rollback ;



--changeset postgres:populate_experiment_design_for_durum_wheat_exp context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1157 Populate experiment design for Durum Wheat exp



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    eo.id AS occurrence_id,
    ep.rep AS design_id,
    ep.id AS plot_id,
    'replication block',
    ep.rep AS block_value,
    ep.rep AS block_level_number,
    exp.creator_id AS creator_id,
    'replicate' AS block_name
FROM 
    experiment.experiment exp
INNER JOIN
    experiment.occurrence eo
ON
    eo.experiment_id = exp.id
INNER JOIN
    experiment.plot ep
ON
    ep.occurrence_id = eo.id
WHERE 
    exp.experiment_name='DW-AYT-2021-DS-001'
;



--rollback DELETE FROM 
--rollback     experiment.experiment_design ee
--rollback USING 
--rollback     experiment.occurrence eo,
--rollback     experiment.experiment exp
--rollback WHERE
--rollback     eo.experiment_id = exp.id
--rollback AND
--rollback     ee.occurrence_id = eo.id
--rollback AND
--rollback     experiment_name = 'DW-AYT-2021-DS-001'
--rollback ;