--liquibase formatted sql

--changeset postgres:populate_occurrence_id_column_in_cross_table context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1271 Populate occurrence_id column in germplasm.cross table



-- drop index temporarily
DROP INDEX germplasm.cross_occurrence_id_idx;

-- populate occurrence_id column
UPDATE
    germplasm.cross AS crs
SET
    occurrence_id = t.female_occurrence_id
FROM
    (
        SELECT
            crs.id AS cross_id,
            crspar.female_parent_id,
            crspar.female_occurrence_id
        FROM
            germplasm.cross AS crs
            INNER JOIN LATERAL (
                    SELECT
                        crspar.id AS female_parent_id,
                        crspar.occurrence_id AS female_occurrence_id
                    FROM
                        germplasm.cross_parent AS crspar
                    WHERE
                        crspar.cross_id = crs.id -- get the female parent of the cross
                        AND crspar.parent_role IN ('female', 'female-and-male') -- either female or both parent roles
                        AND crspar.occurrence_id IS NOT NULL -- occurrence_id is defined
                        AND NOT crspar.is_void
                    ORDER BY
                        crspar.id
                    LIMIT
                        1 -- get only one female parent
                ) AS crspar
                ON TRUE
        WHERE
            crs.cross_method <> 'selfing' -- not selfs
            AND crs.occurrence_id IS NULL -- crosses with empty occurrence_id only
            AND NOT crs.is_void
    ) AS t
WHERE
    crs.id = t.cross_id
    AND crs.occurrence_id IS NULL
;

-- re-create index
CREATE INDEX cross_occurrence_id_idx
    ON germplasm.cross USING btree (occurrence_id)
;



-- revert changes
--rollback DROP INDEX germplasm.cross_occurrence_id_idx;
--rollback 
--rollback UPDATE
--rollback     germplasm.cross
--rollback SET
--rollback     occurrence_id = NULL
--rollback WHERE
--rollback     occurrence_id IS NOT NULL
--rollback ;
--rollback 
--rollback CREATE INDEX cross_occurrence_id_idx
--rollback     ON germplasm.cross USING btree (occurrence_id)
--rollback ;
