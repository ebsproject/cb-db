--liquibase formatted sql

--changeset postgres:populate_growth_habit_and_cross_number_attributes_for_durum context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1290 Populate GROWTH_HABIT and CROSS_NUMBER attributes for Durum



WITH t1 AS (
    SELECT
        germ.id germplasm_id,
        t.cross_number,
        t.growth_habit
    FROM
        (
            VALUES
                ('CM470-1M-3Y-0M','0','S'),
                ('CM9799-126M-1M-5Y-0M','0','S'),
                ('CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y','0','S'),
                ('CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX','0','S'),
                ('CDSS02B00849T-0TOPB-0Y-0M-7Y-2M-04Y-0B','4','S'),
                ('CDSS06B00461T-099Y-099M-15Y-1M-04Y-0B','11','S'),
                ('CDSS09Y00246S-099Y-038M-5Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00285S-099Y-027M-23Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00298S-099Y-043M-27Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00386S-099Y-058M-23Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00762T-099Y-024M-27Y-0M-04Y-0B','11','S'),
                ('CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00771T-099Y-040M-10Y-0M-04Y-0B','12','S'),
                ('CDSS09Y00795T-099Y-024M-29Y-0M-04Y-0B','11','S'),
                ('CDSS09Y00449S-099Y-011M-10Y-0M-04Y-0B','3','S'),
                ('CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B','10','S'),
                ('CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00970T-099Y-063M-11Y-0M-04Y-0B','7','S'),
                ('CDSS09B00077S-099Y-014M-4Y-3M-06Y-0B','1','S'),
                ('CDSS10Y00288S-099Y-038M-17Y-1M-06Y-0B','7','S'),
                ('CDSS10Y00291S-099Y-044M-5Y-2M-06Y-0B','2','S'),
                ('CDSS10Y00491T-099Y-040M-7Y-1M-06Y-0B','9','S'),
                ('CDSS10Y00493T-099Y-035M-9Y-4M-06Y-0B','9','S'),
                ('CDSS10Y00498T-099Y-018M-14Y-4M-06Y-0B','10','S'),
                ('CDSS10Y00500T-099Y-028M-1Y-4M-06Y-0B','9','S'),
                ('CDSS10Y00504T-099Y-037M-5Y-2M-06Y-0B','8','S'),
                ('CDSS10Y00517T-099Y-055M-13Y-4M-06Y-0B','8','S'),
                ('CDSS10Y00524T-099Y-044M-4Y-1M-06Y-0B','10','S'),
                ('CDSS09B00165S-099Y-010M-10Y-3M-06Y-0B','10','S'),
                ('CDSS09B00167S-099Y-019M-4Y-1M-06Y-0B','10','S'),
                ('CDSS09B00170S-099Y-011M-2Y-4M-06Y-0B','7','S'),
                ('CDSS09B00171S-099Y-041M-11Y-3M-06Y-0B','9','S'),
                ('CDSS09B00261T-099Y-046M-7Y-2M-06Y-0B','12','S'),
                ('CDSS09B00268T-099Y-050M-3Y-4M-06Y-0B','9','S'),
                ('CDSS09B00347T-099Y-033M-13Y-3M-06Y-0B','12','S'),
                ('CDSS10Y00539T-099Y-025M-29Y-1M-06Y-0B','11','S'),
                ('CDSS10Y00550T-099Y-014M-7Y-3M-06Y-0B','7','S'),
                ('CDSS10Y00550T-099Y-014M-8Y-3M-06Y-0B','7','S'),
                ('CDSS10Y00553T-099Y-068M-8Y-3M-06Y-0B','7','S'),
                ('CDSS10Y00556T-099Y-031M-16Y-2M-06Y-0B','8','S'),
                ('CDSS09B00483D-099Y-032M-2Y-1M-06Y-0B','5','S'),
                ('CDSS09B00490D-099Y-032M-11Y-3M-06Y-0B','5','S'),
                ('CDSS10Y00572T-099Y-030M-13Y-1M-06Y-0B','9','S'),
                ('CDSS10Y00573T-099Y-056M-4Y-4M-06Y-0B','7','S'),
                ('CDSS09B00128S-099Y-057M-1Y-1M-06Y-0B','10','S'),
                ('CDSS09B00150S-099Y-023M-4Y-1M-06Y-0B','12','S'),
                ('CDSS10Y00017S-099Y-034M-7Y-1M-06Y-0B','10','S'),
                ('CMSS08B01003S-099B-099Y-22B-0Y','3','S'),
                ('CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B','10','S'),
                ('CDSS09Y00310S-099Y-034M-17Y-0M-04Y-0B','6','S'),
                ('CMSS08B01019S-099B-099Y-21B-0Y','3','S'),
                ('CDSS08Y00333S-099Y-022M-11Y-3M-0Y','11','S'),
                ('CDSS09Y00933T-099Y-023M-6Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00911T-099Y-024M-26Y-0M-04Y-0B','8','S'),
                ('CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B','5','S'),
                ('CDSS07B00661T-0TOPY-099Y-029M-5Y-3M-0Y','6','S'),
                ('CDSS09Y00209S-099Y-052M-36Y-0M-04Y-0B','9','S'),
                ('CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y','11','S'),
                ('CDSS08Y00237S-099Y-020M-24Y-3M-0Y','11','S'),
                ('CMSS08B01001S-099B-099Y-41B-0Y','1','S'),
                ('CDSS09Y00286S-099Y-026M-24Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00994T-099Y-020M-5Y-0M-04Y-0B','11','S'),
                ('CDSS08Y00433S-099Y-016M-26Y-4M-0Y','12','S'),
                ('CDSS08Y00583S-099Y-023M-7Y-0M-04Y-0B','8','S'),
                ('CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00930T-099Y-031M-13Y-0M-04Y-0B','12','S'),
                ('CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX','0','S'),
                ('CM9799-126M-1M-5Y-0M','0','S'),
                ('CDSS09Y00347S-099Y-020M-33Y-0M-04Y-0B','5','S'),
                ('CDSS09Y00350S-099Y-023M-3Y-0M-04Y-0B','11','S'),
                ('CDSS09Y00041S-099Y-048M-41Y-0M-04Y-0B','5','S'),
                ('CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B','10','S'),
                ('CDSS08Y00337S-099Y-014M-19Y-2M-0Y','11','S'),
                ('CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B','5','S'),
                ('CDSS09Y00771T-099Y-040M-4Y-0M-04Y-0B','12','S'),
                ('CDSS08Y00433S-099Y-016M-26Y-4M-0Y','12','S'),
                ('CDSS09Y00327S-099Y-041M-19Y-0M-04Y-0B','5','S'),
                ('CDSS09Y00762T-099Y-024M-19Y-0M-04Y-0B','11','S'),
                ('CDSS09Y00285S-099Y-027M-24Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00270S-099Y-050M-11Y-0M-04Y-0B','9','S'),
                ('CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y','0','S'),
                ('CDSS09Y00400S-099Y-030M-19Y-0M-04Y-0B','1','S'),
                ('CDSS08Y00237S-099Y-020M-24Y-3M-0Y','11','S'),
                ('CDSS09Y00765T-099Y-018M-1Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00286S-099Y-026M-8Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00939T-099Y-012M-19Y-0M-04Y-0B','12','S'),
                ('CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00907T-099Y-024M-14Y-0M-04Y-0B','9','S'),
                ('CDSS08Y00337S-099Y-014M-19Y-2M-0Y','11','S'),
                ('CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B','6','S'),
                ('CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B','10','S'),
                ('CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y','11','S'),
                ('CDSS09Y00759T-099Y-033M-28Y-0M-04Y-0B','11','S'),
                ('CDSS09Y00771T-099Y-040M-38Y-0M-04Y-0B','12','S'),
                ('CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B','7','S'),
                ('CDSS09Y00216S-099Y-032M-2Y-0M-04Y-0B','9','S'),
                ('CDSS09Y00241S-099Y-022M-10Y-0M-04Y-0B','3','S'),
                ('CMSS08B00996S-099B-099Y-23B-0Y','1','S'),
                ('CDSS09Y00386S-099Y-058M-30Y-0M-04Y-0B','7','S')
        ) AS t (designation, cross_number, growth_habit)
        INNER JOIN
            germplasm.germplasm germ
        ON
            germ.designation = t.designation
), t2 AS  (
    SELECT 	
        t1.germplasm_id germplasm_id,
        var.id variable_id,
        t1.cross_number data_value,
        'G' data_qc_code,
        1 creator_id
    FROM
        t1
    JOIN
        master.variable var
    ON
        var.abbrev='CROSS_NUMBER'
), t3 AS (
    SELECT 	
        t1.germplasm_id germplasm_id,
        var.id variable_id,
        t1.growth_habit data_value,
        'G' data_qc_code,
        1 creator_id
    FROM
        t1
    JOIN
        master.variable var
    ON
        var.abbrev='GROWTH_HABIT'
), t4 AS (
    SELECT
        *
    FROM 
        t2
    UNION ALL
    SELECT 
        *
    FROM
        t3
)
INSERT INTO 
    germplasm.germplasm_attribute
        (germplasm_id, variable_id, data_value, data_qc_code, creator_id)
SELECT t.* FROM t4 t
;



--rollback WITH t1 AS (
--rollback 	SELECT
--rollback 		germ.id germplasm_id,
--rollback 		t.cross_number,
--rollback 		t.growth_habit
--rollback 	FROM
--rollback 		(
--rollback 			VALUES
--rollback 				('CM470-1M-3Y-0M','0','S'),
--rollback 				('CM9799-126M-1M-5Y-0M','0','S'),
--rollback 				('CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y','0','S'),
--rollback 				('CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX','0','S'),
--rollback 				('CDSS02B00849T-0TOPB-0Y-0M-7Y-2M-04Y-0B','4','S'),
--rollback 				('CDSS06B00461T-099Y-099M-15Y-1M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00246S-099Y-038M-5Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00285S-099Y-027M-23Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00298S-099Y-043M-27Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00386S-099Y-058M-23Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00762T-099Y-024M-27Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00771T-099Y-040M-10Y-0M-04Y-0B','12','S'),
--rollback 				('CDSS09Y00795T-099Y-024M-29Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00449S-099Y-011M-10Y-0M-04Y-0B','3','S'),
--rollback 				('CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B','10','S'),
--rollback 				('CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00970T-099Y-063M-11Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09B00077S-099Y-014M-4Y-3M-06Y-0B','1','S'),
--rollback 				('CDSS10Y00288S-099Y-038M-17Y-1M-06Y-0B','7','S'),
--rollback 				('CDSS10Y00291S-099Y-044M-5Y-2M-06Y-0B','2','S'),
--rollback 				('CDSS10Y00491T-099Y-040M-7Y-1M-06Y-0B','9','S'),
--rollback 				('CDSS10Y00493T-099Y-035M-9Y-4M-06Y-0B','9','S'),
--rollback 				('CDSS10Y00498T-099Y-018M-14Y-4M-06Y-0B','10','S'),
--rollback 				('CDSS10Y00500T-099Y-028M-1Y-4M-06Y-0B','9','S'),
--rollback 				('CDSS10Y00504T-099Y-037M-5Y-2M-06Y-0B','8','S'),
--rollback 				('CDSS10Y00517T-099Y-055M-13Y-4M-06Y-0B','8','S'),
--rollback 				('CDSS10Y00524T-099Y-044M-4Y-1M-06Y-0B','10','S'),
--rollback 				('CDSS09B00165S-099Y-010M-10Y-3M-06Y-0B','10','S'),
--rollback 				('CDSS09B00167S-099Y-019M-4Y-1M-06Y-0B','10','S'),
--rollback 				('CDSS09B00170S-099Y-011M-2Y-4M-06Y-0B','7','S'),
--rollback 				('CDSS09B00171S-099Y-041M-11Y-3M-06Y-0B','9','S'),
--rollback 				('CDSS09B00261T-099Y-046M-7Y-2M-06Y-0B','12','S'),
--rollback 				('CDSS09B00268T-099Y-050M-3Y-4M-06Y-0B','9','S'),
--rollback 				('CDSS09B00347T-099Y-033M-13Y-3M-06Y-0B','12','S'),
--rollback 				('CDSS10Y00539T-099Y-025M-29Y-1M-06Y-0B','11','S'),
--rollback 				('CDSS10Y00550T-099Y-014M-7Y-3M-06Y-0B','7','S'),
--rollback 				('CDSS10Y00550T-099Y-014M-8Y-3M-06Y-0B','7','S'),
--rollback 				('CDSS10Y00553T-099Y-068M-8Y-3M-06Y-0B','7','S'),
--rollback 				('CDSS10Y00556T-099Y-031M-16Y-2M-06Y-0B','8','S'),
--rollback 				('CDSS09B00483D-099Y-032M-2Y-1M-06Y-0B','5','S'),
--rollback 				('CDSS09B00490D-099Y-032M-11Y-3M-06Y-0B','5','S'),
--rollback 				('CDSS10Y00572T-099Y-030M-13Y-1M-06Y-0B','9','S'),
--rollback 				('CDSS10Y00573T-099Y-056M-4Y-4M-06Y-0B','7','S'),
--rollback 				('CDSS09B00128S-099Y-057M-1Y-1M-06Y-0B','10','S'),
--rollback 				('CDSS09B00150S-099Y-023M-4Y-1M-06Y-0B','12','S'),
--rollback 				('CDSS10Y00017S-099Y-034M-7Y-1M-06Y-0B','10','S'),
--rollback 				('CMSS08B01003S-099B-099Y-22B-0Y','3','S'),
--rollback 				('CDSS08B00133T-099Y-046M-1Y-0M-04Y-0B','10','S'),
--rollback 				('CDSS09Y00310S-099Y-034M-17Y-0M-04Y-0B','6','S'),
--rollback 				('CMSS08B01019S-099B-099Y-21B-0Y','3','S'),
--rollback 				('CDSS08Y00333S-099Y-022M-11Y-3M-0Y','11','S'),
--rollback 				('CDSS09Y00933T-099Y-023M-6Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00911T-099Y-024M-26Y-0M-04Y-0B','8','S'),
--rollback 				('CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B','5','S'),
--rollback 				('CDSS07B00661T-0TOPY-099Y-029M-5Y-3M-0Y','6','S'),
--rollback 				('CDSS09Y00209S-099Y-052M-36Y-0M-04Y-0B','9','S'),
--rollback 				('CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y','11','S'),
--rollback 				('CDSS08Y00237S-099Y-020M-24Y-3M-0Y','11','S'),
--rollback 				('CMSS08B01001S-099B-099Y-41B-0Y','1','S'),
--rollback 				('CDSS09Y00286S-099Y-026M-24Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00994T-099Y-020M-5Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS08Y00433S-099Y-016M-26Y-4M-0Y','12','S'),
--rollback 				('CDSS08Y00583S-099Y-023M-7Y-0M-04Y-0B','8','S'),
--rollback 				('CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00930T-099Y-031M-13Y-0M-04Y-0B','12','S'),
--rollback 				('CD91Y636-1Y-040M-030Y-1M-0Y-0B-1Y-0B-0MEX','0','S'),
--rollback 				('CM9799-126M-1M-5Y-0M','0','S'),
--rollback 				('CDSS09Y00347S-099Y-020M-33Y-0M-04Y-0B','5','S'),
--rollback 				('CDSS09Y00350S-099Y-023M-3Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00041S-099Y-048M-41Y-0M-04Y-0B','5','S'),
--rollback 				('CDSS09Y00388S-099Y-047M-19Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B','10','S'),
--rollback 				('CDSS08Y00337S-099Y-014M-19Y-2M-0Y','11','S'),
--rollback 				('CDSS09Y00024S-099Y-020M-8Y-0M-04Y-0B','5','S'),
--rollback 				('CDSS09Y00771T-099Y-040M-4Y-0M-04Y-0B','12','S'),
--rollback 				('CDSS08Y00433S-099Y-016M-26Y-4M-0Y','12','S'),
--rollback 				('CDSS09Y00327S-099Y-041M-19Y-0M-04Y-0B','5','S'),
--rollback 				('CDSS09Y00762T-099Y-024M-19Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00285S-099Y-027M-24Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00270S-099Y-050M-11Y-0M-04Y-0B','9','S'),
--rollback 				('CD22344-A-8M-1Y-1M-1Y-2Y-1M-0Y','0','S'),
--rollback 				('CDSS09Y00400S-099Y-030M-19Y-0M-04Y-0B','1','S'),
--rollback 				('CDSS08Y00237S-099Y-020M-24Y-3M-0Y','11','S'),
--rollback 				('CDSS09Y00765T-099Y-018M-1Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00286S-099Y-026M-8Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00939T-099Y-012M-19Y-0M-04Y-0B','12','S'),
--rollback 				('CDSS09Y00763T-099Y-011M-26Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00907T-099Y-024M-14Y-0M-04Y-0B','9','S'),
--rollback 				('CDSS08Y00337S-099Y-014M-19Y-2M-0Y','11','S'),
--rollback 				('CDSS09Y00029S-099Y-020M-16Y-0M-04Y-0B','6','S'),
--rollback 				('CDSS09Y00913T-099Y-014M-16Y-0M-04Y-0B','10','S'),
--rollback 				('CDSS08Y00716T-0TOPB-099Y-010M-27Y-3M-0Y','11','S'),
--rollback 				('CDSS09Y00759T-099Y-033M-28Y-0M-04Y-0B','11','S'),
--rollback 				('CDSS09Y00771T-099Y-040M-38Y-0M-04Y-0B','12','S'),
--rollback 				('CDSS08B00149T-099Y-056M-16Y-0M-04Y-0B','7','S'),
--rollback 				('CDSS09Y00216S-099Y-032M-2Y-0M-04Y-0B','9','S'),
--rollback 				('CDSS09Y00241S-099Y-022M-10Y-0M-04Y-0B','3','S'),
--rollback 				('CMSS08B00996S-099B-099Y-23B-0Y','1','S'),
--rollback 				('CDSS09Y00386S-099Y-058M-30Y-0M-04Y-0B','7','S')
--rollback 		) AS t (designation, cross_number, growth_habit)
--rollback 		INNER JOIN
--rollback 			germplasm.germplasm germ
--rollback 		ON
--rollback 			germ.designation = t.designation
--rollback ), t2 AS  (
--rollback 	SELECT 	
--rollback 		t1.germplasm_id germplasm_id,
--rollback 		var.id variable_id,
--rollback 		t1.cross_number data_value,
--rollback 		'G' data_qc_code,
--rollback 		1 creator_id
--rollback 	FROM
--rollback 		t1
--rollback 	JOIN
--rollback 		master.variable var
--rollback 	ON
--rollback 		var.abbrev='CROSS_NUMBER'
--rollback ), t3 AS (
--rollback 	SELECT 	
--rollback 		t1.germplasm_id germplasm_id,
--rollback 		var.id variable_id,
--rollback 		t1.growth_habit data_value,
--rollback 		'G' data_qc_code,
--rollback 		1 creator_id
--rollback 	FROM
--rollback 		t1
--rollback 	JOIN
--rollback 		master.variable var
--rollback 	ON
--rollback 		var.abbrev='GROWTH_HABIT'
--rollback ), t4 AS (
--rollback 	SELECT
--rollback 		*
--rollback 	FROM 
--rollback 		t2
--rollback 	UNION ALL
--rollback 	SELECT 
--rollback 		*
--rollback 	FROM
--rollback 		t3
--rollback )
--rollback DELETE FROM
--rollback 	    germplasm.germplasm_attribute ga
--rollback USING
--rollback  	t4 t
--rollback WHERE
--rollback 	    ga.germplasm_id = t.germplasm_id
--rollback AND
--rollback 	    ga.variable_id = t.variable_id
--rollback AND
--rollback 	    ga.data_value = t.data_value
--rollback ;