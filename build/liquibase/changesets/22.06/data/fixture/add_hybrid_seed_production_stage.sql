--liquibase formatted sql

--changeset postgres:add_hybrid_seed_production_stage context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1257 Add hybrid seed production stage



INSERT INTO tenant.stage
    (stage_code, stage_name, description, creator_id)
VALUES
    ('HSP', 'Hybrid Seed Production', 'Hybrid Seed Production', 1)
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.stage
--rollback WHERE
--rollback     stage_code = 'HSP'
--rollback ;
