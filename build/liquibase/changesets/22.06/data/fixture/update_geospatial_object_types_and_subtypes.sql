--liquibase formatted sql

--changeset postgres:update_geospatial_object_subtype_to_farm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Update geospatial_object_subtype to farm



-- update subtype to farm
UPDATE
    place.geospatial_object
SET
    geospatial_object_subtype = 'farm'
WHERE
    geospatial_object_code IN (
        'IIRR_FARM',
        'INFANTA_FARM_1',
        'INFANTA_FARM_2',
        'INFANTA_FARM_3'
    )
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'plot'
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'IIRR_FARM',
--rollback         'INFANTA_FARM_1',
--rollback         'INFANTA_FARM_2',
--rollback         'INFANTA_FARM_3'
--rollback     )
--rollback ;



--changeset postgres:update_geospatial_object_subtype_to_breeding_location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Update geospatial_object_subtype to breeding location



-- update subtype to breeding location
UPDATE
    place.geospatial_object
SET
    geospatial_object_subtype = 'breeding location'
WHERE
    geospatial_object_code IN (
        'AHERO_SITE',
        'AKAGOMA_SITE',
        'BAGAMOYO_SITE',
        'CANKUZO_SITE',
        'DAKAWA_SITE',
        'DIHIMBA_SITE',
        'GIHANGA_SITE',
        'GISHA_SITE',
        'IGUNGA_SITE',
        'IRSEA_SITE',
        'IRSEA_SITE_1_SAGBAYAN',
        'KIREKURA_SITE',
        'LILIDO_SITE',
        'LUPEMBE_SITE',
        'MBARAGA_SITE',
        'MET_AKAGOMA_SITE',
        'MET_MUBONE_SITE',
        'MET_MURAMBA_SITE',
        'MET_SITE_1',
        'MET_SITE_2',
        'MET_SITE_3',
        'MET_SITE_4',
        'MET_SITE_5',
        'MET_SITE_6',
        'MET_SITE_7',
        'MURAMBA_SITE',
        'NGAMANGA_SITE',
        'RUGOMBO_SITE',
        'SALINITY_SITE_1',
        'SALINITY_SITE_1_SAGBAYAN',
        'SALINITY_SITE_1_STO_ROSARIO',
        'SALINITY_SITE_2_PILI',
        'UMBELUZI_SITE'
    )
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'plot'
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'AHERO_SITE',
--rollback         'AKAGOMA_SITE',
--rollback         'BAGAMOYO_SITE',
--rollback         'CANKUZO_SITE',
--rollback         'DAKAWA_SITE',
--rollback         'DIHIMBA_SITE',
--rollback         'GIHANGA_SITE',
--rollback         'GISHA_SITE',
--rollback         'IGUNGA_SITE',
--rollback         'IRSEA_SITE',
--rollback         'IRSEA_SITE_1_SAGBAYAN',
--rollback         'KIREKURA_SITE',
--rollback         'LILIDO_SITE',
--rollback         'LUPEMBE_SITE',
--rollback         'MBARAGA_SITE',
--rollback         'MET_AKAGOMA_SITE',
--rollback         'MET_MUBONE_SITE',
--rollback         'MET_MURAMBA_SITE',
--rollback         'MET_SITE_1',
--rollback         'MET_SITE_2',
--rollback         'MET_SITE_3',
--rollback         'MET_SITE_4',
--rollback         'MET_SITE_5',
--rollback         'MET_SITE_6',
--rollback         'MET_SITE_7',
--rollback         'MURAMBA_SITE',
--rollback         'NGAMANGA_SITE',
--rollback         'RUGOMBO_SITE',
--rollback         'SALINITY_SITE_1',
--rollback         'SALINITY_SITE_1_SAGBAYAN',
--rollback         'SALINITY_SITE_1_STO_ROSARIO',
--rollback         'SALINITY_SITE_2_PILI',
--rollback         'UMBELUZI_SITE'
--rollback     )
--rollback ;
