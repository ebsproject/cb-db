--liquibase formatted sql

--changeset postgres:add_hm_browser_config_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3568 HM DB: Add default browser configuration



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'HM_DATA_BROWSER_CONFIG_DEFAULT',
    'HM Default Data Browser Configuration (any crop without a specific config)', 
    $$
        {
            "default": {
                "default": {
                    "default": {
                        "input_columns": [],
                        "numeric_variables": [],
                        "additional_required_variables": []
                    }
                }
            }
        }
    $$,
    1,
    'harvest_manager',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'HM_DATA_BROWSER_CONFIG_DEFAULT';
