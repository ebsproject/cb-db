--liquibase formatted sql

--changeset postgres:update_gefu_variable_config_wheat context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3437 GM DB: Update attributes/fields of configuration used



UPDATE platform.config
SET
    usage = 'File upload, data validation, and creation of records',
	config_value = 
    $${
        "values":[
            {
                "abbrev": "GERMPLASM_TYPE",
                "name": "Germplasm Type",
                "usage": "optional",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "germplasmType",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GRAIN_COLOR",
                "name": "Grain Color",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GROWTH_HABIT",
                "name": "Growth Habit",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "CROSS_NUMBER",
                "name": "Cross Number",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PROGRAM_CODE",
                "name": "Program Code",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",                
                "required": "false",
                "data_type": "string",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_search_field": "programCode",
                "retrieve_api_value_field":  "programDbId"
            },
            {
                "abbrev": "HVDATE_CONT",
                "name": "Harvest Date",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "date",
                "api_field": "harvestDate",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "HV_METH_DISC",
                "name": "Harvest Method",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "harvestMethod",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "DESCRIPTION",
                "name": "Description",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "description",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "MTA_STATUS",
                "name": "MTA Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IP_STATUS",
                "name": "IP Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_ORGANIZATION",
                "name": "Source Organization",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "ORIGIN",
                "name": "Origin",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_STUDY",
                "name": "Source Study",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""                
            },
            {
                "abbrev": "MTA",
                "name": "MTA Number",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IMPORT",
                "name": "Import Attribute",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "VOLUME",
                "name": "Package Quantity",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "float",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PACKAGE_UNIT",
                "name": "Unit of Measure",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            }
        ]
    }$$
WHERE
	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT'
;



--rollback  UPDATE platform.config
--rollback  SET
--rollback      usage = 'File upload, data validation, and creation of records',
--rollback  	config_value = 
--rollback      $${
--rollback          "values":[
--rollback              {
--rollback                  "abbrev": "GERMPLASM_TYPE",
--rollback                  "name": "Germplasm Type",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "germplasm",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "GRAIN_COLOR",
--rollback                  "name": "Grain color",
--rollback                  "data_type": "string",
--rollback                  "usage": "required",
--rollback                  "required": "true",
--rollback                  "entity": "germplasm",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "GROWTH_HABIT",
--rollback                  "name": "Growth habit",
--rollback                  "data_type": "string",
--rollback                  "usage": "required",
--rollback                  "required": "true",
--rollback                  "entity": "germplasm",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "PROGRAM_CODE",
--rollback                  "name": "Program code",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "HVDATE_CONT",
--rollback                  "name": "Harvest Date",
--rollback                  "data_type": "date",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "HV_METH_DISC",
--rollback                  "name": "Harvest Method",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "DESCRIPTION",
--rollback                  "name": "Description",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "MTA_STATUS",
--rollback                  "name": "MTA Status",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "IP_STATUS",
--rollback                  "name": "IP Status",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "SOURCE_ORGANIZATION",
--rollback                  "name": "Source Organization",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "ORIGIN",
--rollback                  "name": "Origin",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "SOURCE_STUDY",
--rollback                  "name": "Source study",
--rollback                  "data_type": "string",
--rollback                  "usage": "optional",
--rollback                  "required": "false",
--rollback                  "entity": "seed",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "VOLUME",
--rollback                  "name": "Package Quantity",
--rollback                  "data_type": "float",
--rollback                  "usage": "required",
--rollback                  "required": "true",
--rollback                  "entity": "package",
--rollback                  "visible": "true"
--rollback              },
--rollback              {
--rollback                  "abbrev": "PACKAGE_UNIT",
--rollback                  "name": "Unit of measure",
--rollback                  "data_type": "string",
--rollback                  "usage": "required",
--rollback                  "required": "true",
--rollback                  "entity": "package",
--rollback                  "visible": "true"
--rollback              }
--rollback          ]
--rollback      }$$
--rollback  WHERE
--rollback  	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_WHEAT_DEFAULT'
--rollback  ;
