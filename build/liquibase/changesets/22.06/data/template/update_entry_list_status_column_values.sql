--liquibase formatted sql

--changeset postgres:update_entry_list_status_column_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Update entry_list_status column values



-- update entry_list_status values
WITH t_expt AS (
    SELECT
        entlist.id AS entry_list_id,
        entlist.entry_list_status,
        occ.occurrence_status
    FROM
        experiment.experiment AS expt
        INNER JOIN experiment.entry_list AS entlist
            ON entlist.experiment_id = expt.id
        INNER JOIN LATERAL (
                SELECT
                    occ.*
                FROM
                    experiment.occurrence AS occ
                WHERE
                    occ.experiment_id = expt.id
                    AND occ.is_void = FALSE
                LIMIT
                    1
            ) AS occ
                ON TRUE
    WHERE
        expt.experiment_type = 'Intentional Crossing Nursery'
)
UPDATE
    experiment.entry_list AS entlist
SET
    entry_list_status = (
        CASE t.occurrence_status
            WHEN 'created' THEN 'cross list specified'
            WHEN 'planted' THEN 'finalized'
            ELSE entlist.entry_list_status
        END
    )
FROM
    t_expt AS t
WHERE
    entlist.id = t.entry_list_id
;

-- update other values
UPDATE
    experiment.entry_list
SET
    entry_list_status = 'completed'
WHERE
    entry_list_status = 'created'
;



-- revert changes
--rollback SELECT NULL;
