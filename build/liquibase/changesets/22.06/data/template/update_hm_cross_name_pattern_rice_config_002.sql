--liquibase formatted sql

--changeset postgres:update_hm_cross_name_pattern_rice_config_002 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3198 HM API: Update RICE Backcross use cases to take into account transgenic/genome-edited parent(s)



UPDATE platform.config
SET
    config_value = '
        {
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "bulk": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ],
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "bulk":{
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                }
                            ],
                            "transgenic" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ],
                            "genome_edited" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ]
                        },
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ],
                        "single seed numbering": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                }
                            ],
                            "transgenic" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ],
                            "genome_edited" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ]
                        }
                    }
                }
            },
            "backcross": {
                "default": {
                    "default": {
                        "bulk":{
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                }
                            ],
                            "transgenic" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ],
                            "genome_edited" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ]
                        },
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ],
                        "single seed numbering": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                }
                            ],
                            "transgenic" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ],
                            "genome_edited" : [
                                {
                                    "type": "free-text",
                                    "value": "IR ",
                                    "order_number": 0
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "germplasm",
                                    "order_number": 1,
                                    "sequence_name": "cross_number_seq"
                                },
                                {
                                    "type": "free-text",
                                    "value": " TR",
                                    "order_number": 2
                                }
                            ]
                        }
                    }
                }
            },
            "transgenesis": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "backgroundGermplasm",
                                "field_name": "designation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "constructGermplasm",
                                "field_name": "designation",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "genome editing": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "backgroundGermplasm",
                                "field_name": "designation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "constructGermplasm",
                                "field_name": "designation",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "hybrid formation": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            },
                            {
                                "type": "free-text",
                                "value": " H",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "cms multiplication": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "order_number": 1,
                                "sequence_name": "cross_number_seq"
                            },
                            {
                                "type": "free-text",
                                "value": " A",
                                "order_number": 2
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }
    '
WHERE
    abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT'



--rollback  UPDATE platform.config
--rollback  SET
--rollback      config_value = '
--rollback          {
--rollback          "PLOT": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR XXXXX",
--rollback                                  "order_number": 0
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "bulk": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ],
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              },
--rollback              "single cross": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "bulk": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ],
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ],
--rollback                          "single seed numbering": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              },
--rollback              "transgenesis": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ],
--rollback                          "single seed numbering": [
--rollback                              {
--rollback                                  "type": "field",
--rollback                                  "entity": "backgroundGermplasm",
--rollback                                  "field_name": "designation",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "delimiter",
--rollback                                  "value": "-",
--rollback                                  "order_number": 1
--rollback                              },
--rollback                              {
--rollback                                  "type": "field",
--rollback                                  "entity": "constructGermplasm",
--rollback                                  "field_name": "designation",
--rollback                                  "order_number": 2
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              },
--rollback              "genome editing": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              }
--rollback                          ],
--rollback                          "single seed numbering": [
--rollback                              {
--rollback                                  "type": "field",
--rollback                                  "entity": "backgroundGermplasm",
--rollback                                  "field_name": "designation",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "delimiter",
--rollback                                  "value": "-",
--rollback                                  "order_number": 1
--rollback                              },
--rollback                              {
--rollback                                  "type": "field",
--rollback                                  "entity": "constructGermplasm",
--rollback                                  "field_name": "designation",
--rollback                                  "order_number": 2
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              },
--rollback              "hybrid formation": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              },
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": " H",
--rollback                                  "order_number": 2
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              },
--rollback              "cms multiplication": {
--rollback                  "default": {
--rollback                      "default": {
--rollback                          "default": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "IR ",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "germplasm",
--rollback                                  "order_number": 1,
--rollback                                  "sequence_name": "cross_number_seq"
--rollback                              },
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": " A",
--rollback                                  "order_number": 2
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              }
--rollback          },
--rollback          "harvest_mode": {
--rollback              "cross_method": {
--rollback                  "germplasm_state": {
--rollback                      "germplasm_type": {
--rollback                          "harvest_method": [
--rollback                              {
--rollback                                  "type": "free-text",
--rollback                                  "value": "ABC",
--rollback                                  "order_number": 0
--rollback                              },
--rollback                              {
--rollback                                  "type": "field",
--rollback                                  "entity": "<entity>",
--rollback                                  "field_name": "<field_name>",
--rollback                                  "order_number": 1
--rollback                              },
--rollback                              {
--rollback                                  "type": "delimiter",
--rollback                                  "value": "-",
--rollback                                  "order_number": 1
--rollback                              },
--rollback                              {
--rollback                                  "type": "counter",
--rollback                                  "order_number": 3
--rollback                              },
--rollback                              {
--rollback                                  "type": "db-sequence",
--rollback                                  "schema": "<schema>",
--rollback                                  "order_number": 4,
--rollback                                  "sequence_name": "<sequence_name>"
--rollback                              }
--rollback                          ]
--rollback                      }
--rollback                  }
--rollback              }
--rollback          }
--rollback      }
--rollback      '
--rollback  WHERE
--rollback      abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT'