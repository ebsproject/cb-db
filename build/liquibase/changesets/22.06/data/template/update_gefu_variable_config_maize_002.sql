--liquibase formatted sql

--changeset postgres:update_gefu_variable_config_maize_002 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3641 GM: Change UNIT OF MEASURE to PACKAGE UNIT



UPDATE platform.config
SET
    usage = 'File upload, data validation, and creation of records',
	config_value = 
    $${
        "values":[
            {
                "abbrev": "GERMPLASM_TYPE",
                "name": "Germplasm Type",
                "usage": "optional",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "germplasmType",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GRAIN_COLOR",
                "name": "Grain Color",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "HETEROTIC_GROUP",
                "name": "Heterotic Group",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "DH_FIRST_INCREASE_COMPLETED",
                "name": "DH First Increase Completed",
                "usage": "optional",
                "entity": "germplasm",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PROGRAM_CODE",
                "name": "Program Code",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_search_field": "programCode",
                "retrieve_api_value_field":  "programDbId"
            },
            {
                "abbrev": "HVDATE_CONT",
                "name": "Harvest Date",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "date",
                "api_field": "harvestDate",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "HV_METH_DISC",
                "name": "Harvest Method",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "harvestMethod",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "DESCRIPTION",
                "name": "Description",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "description",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "MTA_STATUS",
                "name": "MTA Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IP_STATUS",
                "name": "IP Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_ORGANIZATION",
                "name": "Source Organization",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "ORIGIN",
                "name": "Origin",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_STUDY",
                "name": "Source Study",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_HARV_YEAR",
                "name": "Source Harvest Year",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "MTA",
                "name": "MTA Number",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IMPORT",
                "name": "Import Attribute",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "VOLUME",
                "name": "Package Quantity",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "float",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PACKAGE_UNIT",
                "name": "Package Unit",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            }
        ]
    }$$
WHERE
	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_MAIZE_DEFAULT'
;



--rollback  UPDATE platform.config
--rollback  SET
--rollback  	config_value = 
--rollback      $${
--rollback          "values":[
--rollback              {
--rollback                  "name": "Germplasm Type",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "GERMPLASM_TYPE",
--rollback                  "entity": "germplasm",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Program code",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "PROGRAM_CODE",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Harvest Date",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "HVDATE_CONT",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "date"
--rollback              },
--rollback              {
--rollback                  "name": "Harvest Method",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "HV_METH_DISC",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Description",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "DESCRIPTION",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "MTA Status",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "MTA_STATUS",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "IP Status",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "IP_STATUS",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Source Organization",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "SOURCE_ORGANIZATION",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Origin",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "ORIGIN",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Source study",
--rollback                  "usage": "optional",
--rollback                  "abbrev": "SOURCE_STUDY",
--rollback                  "entity": "seed",
--rollback                  "visible": "true",
--rollback                  "required": "false",
--rollback                  "data_type": "string"
--rollback              },
--rollback              {
--rollback                  "name": "Package Quantity",
--rollback                  "usage": "required",
--rollback                  "abbrev": "VOLUME",
--rollback                  "entity": "package",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "float"
--rollback              },
--rollback              {
--rollback                  "name": "Unit of measure",
--rollback                  "usage": "required",
--rollback                  "abbrev": "PACKAGE_UNIT",
--rollback                  "entity": "package",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string"
--rollback              }
--rollback          ]
--rollback      }$$
--rollback  WHERE
--rollback  	abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_MAIZE_DEFAULT'
--rollback  ;
