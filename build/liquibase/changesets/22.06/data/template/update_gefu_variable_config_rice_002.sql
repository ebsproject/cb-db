--liquibase formatted sql

--changeset postgres:update_gefu_variable_config_rice_002 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3527 GM: Implement updated and new configuration - Data Validation



UPDATE platform.config
SET
    config_value = 
    $${
        "values":[
            {
                "abbrev": "GERMPLASM_TYPE",
                "name": "Germplasm Type",
                "usage": "optional",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "germplasmType",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PROGRAM_CODE",
                "name": "Program Code",
                "usage": "required",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_search_field": "programCode",
                "retrieve_api_value_field":  "programDbId"
            },
            {
                "abbrev": "HVDATE_CONT",
                "name": "Harvest Date",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "date",
                "api_field": "harvestDate",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "HV_METH_DISC",
                "name": "Harvest Method",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "harvestMethod",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "DESCRIPTION",
                "name": "Description",
                "usage": "optional",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "description",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "MTA_STATUS",
                "name": "MTA Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IP_STATUS",
                "name": "IP Status",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_ORGANIZATION",
                "name": "Source Organization",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "ORIGIN",
                "name": "Origin",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",  
                "data_type": "string",     
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""         
            },
            {
                "abbrev": "SOURCE_STUDY",
                "name": "Source Study",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "SOURCE_HARV_YEAR",
                "name": "Source Harvest Year",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "MTA",
                "name": "MTA Number",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "IMPORT",
                "name": "Import Attribute",
                "usage": "optional",
                "entity": "seed",
                "type": "attribute",
                "visible": "true",
                "required": "false",
                "data_type": "string",
                "api_field": "dataValue",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "VOLUME",
                "name": "Package Quantity",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "float",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PACKAGE_UNIT",
                "name": "Unit of Measure",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            }
        ]
    }$$
WHERE
    abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_RICE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback     usage = 'File upload, data validation, and creation of records',
--rollback     config_value = 
--rollback     $${
--rollback         "values":[
--rollback             {
--rollback                 "abbrev": "GERMPLASM_TYPE",
--rollback                 "name": "Germplasm Type",
--rollback                 "usage": "optional",
--rollback                 "entity": "germplasm",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "germplasmType",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "PROGRAM_CODE",
--rollback                 "name": "Program Code",
--rollback                 "usage": "required",
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "data_type": "string",
--rollback                 "api_field": "programDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "retrieve_endpoint": "programs",
--rollback                 "retrieve_api_search_field": "programCode",
--rollback                 "retrieve_api_value_field":  "programDbId"
--rollback             },
--rollback             {
--rollback                 "abbrev": "HVDATE_CONT",
--rollback                 "name": "Harvest Date",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "date",
--rollback                 "api_field": "harvestDate",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "HV_METH_DISC",
--rollback                 "name": "Harvest Method",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "harvestMethod",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "DESCRIPTION",
--rollback                 "name": "Description",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "description",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "MTA_STATUS",
--rollback                 "name": "MTA Status",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "IP_STATUS",
--rollback                 "name": "IP Status",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "SOURCE_ORGANIZATION",
--rollback                 "name": "Source Organization",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "ORIGIN",
--rollback                 "name": "Origin",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",  
--rollback                 "data_type": "string",     
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""         
--rollback             },
--rollback             {
--rollback                 "abbrev": "SOURCE_STUDY",
--rollback                 "name": "Source Study",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "MTA",
--rollback                 "name": "MTA Number",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "IMPORT",
--rollback                 "name": "Import Attribute",
--rollback                 "usage": "optional",
--rollback                 "entity": "seed",
--rollback                 "type": "attribute",
--rollback                 "visible": "true",
--rollback                 "required": "false",
--rollback                 "data_type": "string",
--rollback                 "api_field": "dataValue",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "VOLUME",
--rollback                 "name": "Package Quantity",
--rollback                 "usage": "required",
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "data_type": "float",
--rollback                 "api_field": "packageQuantity",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             },
--rollback             {
--rollback                 "abbrev": "PACKAGE_UNIT",
--rollback                 "name": "Unit of Measure",
--rollback                 "usage": "required",
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "visible": "true",
--rollback                 "required": "true",
--rollback                 "data_type": "string",
--rollback                 "api_field": "packageUnit",
--rollback                 "retrieve_db_id": "false",
--rollback                 "retrieve_endpoint": "",
--rollback                 "retrieve_api_search_field": "",
--rollback                 "retrieve_api_value_field":  ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_RICE_DEFAULT'
--rollback ;
