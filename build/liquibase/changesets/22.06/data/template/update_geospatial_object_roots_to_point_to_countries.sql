--liquibase formatted sql

--changeset postgres:update_geospatial_object_roots_to_point_to_countries context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Update geospatial object roots to point to countries



-- update root to point to country of their current's root
WITH t_geo AS (
    SELECT
        geo.id,
        ctry.id AS country_id
    FROM
        place.geospatial_object AS geo
        LEFT JOIN place.geospatial_object AS root
            ON root.id = geo.root_geospatial_object_id
        LEFT JOIN place.geospatial_object AS site
            ON site.id = geo.root_geospatial_object_id
        LEFT JOIN place.geospatial_object AS ctry
            ON ctry.id = site.root_geospatial_object_id
    WHERE
        (
            geo.root_geospatial_object_id IS NULL
            OR root.geospatial_object_type <> 'country'
        )
        AND geo.geospatial_object_code <> 'UNKNOWN'
        AND site.geospatial_object_type = 'site'
        AND ctry.geospatial_object_type = 'country'
)
UPDATE
    place.geospatial_object AS geo
SET
    root_geospatial_object_id = t.country_id,
    notes = platform.append_text(geo.notes, 'DB-1023 root-country from ' || geo.root_geospatial_object_id)
FROM
    t_geo AS t
WHERE
    geo.id = t.id
    AND geo.root_geospatial_object_id <> t.id
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object AS geo
--rollback SET
--rollback     root_geospatial_object_id = (SELECT (regexp_matches(geo.notes, '(DB-1023 root-country from )(\d+)'))[2]::integer),
--rollback     notes = platform.detach_text(geo.notes, 'DB-1023 root-country from \d+')
--rollback WHERE
--rollback     geo.notes LIKE '%DB-1023 root-country from%'
--rollback     AND geo.root_geospatial_object_id IS NOT NULL
--rollback ;
