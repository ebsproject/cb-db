--liquibase formatted sql

--changeset postgres:update_error_log_variable_data_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1255 Update ERROR_LOG variable data type



-- update data type
UPDATE
    master.variable
SET
    data_type = 'jsonb'
WHERE
    abbrev = 'ERROR_LOG'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_type = 'text'
--rollback WHERE
--rollback     abbrev = 'ERROR_LOG'
--rollback ;
