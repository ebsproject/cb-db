--liquibase formatted sql

--changeset postgres:trim_geospatial_codes_and_names context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Trim geospatial codes and names



-- trim codes and names
UPDATE
    place.geospatial_object
SET
    geospatial_object_code = TRIM(geospatial_object_code),
    geospatial_object_name = TRIM(geospatial_object_name)
;



-- revert changes
--rollback SELECT NULL;



--changeset postgres:fix_geospatial_object_codes context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Fix geospatial object codes


-- remove extra underscores
UPDATE
    place.geospatial_object AS geo
SET
    geospatial_object_code = regexp_replace(trim(geo.geospatial_object_code), '^\_+', '')
WHERE
    geo.geospatial_object_code ~ '^\_+'
;

UPDATE
    place.geospatial_object AS geo
SET
    geospatial_object_code = regexp_replace(trim(geo.geospatial_object_code), '\_{2,}', '_')
WHERE
    geo.geospatial_object_code ~ '\_{2,}'
;

UPDATE
    place.geospatial_object AS geo
SET
    geospatial_object_code = regexp_replace(trim(geo.geospatial_object_code), '\_+$', '')
WHERE
    geo.geospatial_object_code ~ '\_+$'
;



-- revert changes
--rollback SELECT NULL;
