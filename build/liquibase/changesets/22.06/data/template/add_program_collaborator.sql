--liquibase formatted sql

--changeset postgres:add_collaborator_to_team context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM tenant.team WHERE team_code = 'COLLABORATOR') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1279 Add COLLABORATOR to team



INSERT INTO
    tenant.team
    (team_code, team_name, description, creator_id)
SELECT
    tbl.team_code,
    tbl.team_name,
    tbl.description,
    tbl.creator_id
FROM
    (
        VALUES
            ('COLLABORATOR', 'Collaborator', 'Collaborator team', 1)
    ) as tbl (team_code, team_name, description, creator_id)
;



--rollback DELETE FROM tenant.team WHERE team_code='COLLABORATOR';



--changeset postgres:add_collaborator_to_program context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM tenant.program WHERE program_code = 'COLLABORATOR') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1279 Add COLLABORATOR to program



INSERT INTO
    tenant.program
    (program_code, program_name, program_type, program_status, description, creator_id)
SELECT
    tbl.program_code,
    tbl.program_name,
    tbl.program_type,
    tbl.program_status,
    tbl.description,
    tbl.creator_id
FROM
    (
        VALUES
            (
                'COLLABORATOR',
                'Collaborator',
                'breeding',
                'active',
                'Collaborator program',
                1
            )
    ) as tbl (program_code, program_name, program_type, program_status, description, creator_id)
;



--rollback DELETE FROM tenant.program WHERE program_code='COLLABORATOR';



--changeset postgres:add_collaborator_to_program_team context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM tenant.program_team WHERE program_id = (SELECT id FROM tenant.program WHERE program_code = 'COLLABORATOR') AND team_id = (SELECT id FROM tenant.team WHERE team_code = 'COLLABORATOR') ) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1279 Add COLLABORATOR to program_team



INSERT INTO
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
SELECT
    tbl.program_id,
    tbl.team_id,
    tbl.order_number,
    tbl.creator_id
FROM
    (
        VALUES
            (
                (SELECT id FROM tenant.program WHERE program_code='COLLABORATOR'),
                (SELECT id FROM tenant.team WHERE team_code='COLLABORATOR'), 
                1,
                1
            )
    ) as tbl (program_id, team_id, order_number, creator_id)
;



--rollback DELETE FROM tenant.program_team 
--rollback WHERE program_id=(SELECT id FROM tenant.program WHERE program_code='COLLABORATOR')
--rollback AND team_id=(SELECT id FROM tenant.team WHERE team_code='COLLABORATOR');



--changeset postgres:link_collaborator_program_to_crop_program context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM tenant.crop_program WHERE is_void = FALSE) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1279 Link COLLABORATOR program to crop_program



UPDATE
    tenant.program
SET
    crop_program_id = cropprog.id
FROM
    (
        SELECT
            cp.id
        FROM
            tenant.crop_program cp
        WHERE
            cp.is_void = FALSE
        ORDER BY
            cp.id
        LIMIT
            1
    ) AS cropprog
WHERE
    program_code = 'COLLABORATOR'
;



-- revert changes
--rollback UPDATE
--rollback     tenant.program
--rollback SET
--rollback     crop_program_id = NULL
--rollback WHERE
--rollback     program_code = 'COLLABORATOR'
--rollback ;
