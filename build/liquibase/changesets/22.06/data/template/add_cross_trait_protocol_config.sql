--liquibase formatted sql

--changeset postgres:add_mapping_file_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3534 Add global and program-specific configs for required traits to be included in the downloaded file



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'CROSS_TRAIT_PROTOCOL_DEFAULT',
        'Cross trait protocol default configuration',
        $${
            "required_traits": []
        }$$,
        'cross manager and data collection export tool'
    ),
    (
        'CROSS_TRAIT_PROTOCOL_BW',
        'Cross trait protocol configuration for BW program',
        $${
            "required_traits": [
                "DATE_CROSSED",
                "REMARKS",
                "NO_OF_SEED"
            ]
        }$$,
        'cross manager and data collection export tool'
    ),
    (
        'CROSS_TRAIT_PROTOCOL_IRSEA',
        'Cross trait protocol configuration for IRSEA program',
        $${
            "required_traits": [
                "DATE_CROSSED",
                "REMARKS",
                "NO_OF_SEED",
                "HV_METH_DISC"
            ]
        }$$,
        'cross manager and data collection export tool'
    )
    ;



-- rollback DELETE FROM platform.config WHERE abbrev IN ('CROSS_TRAIT_PROTOCOL_DEFAULT', 'CROSS_TRAIT_PROTOCOL_BW', 'CROSS_TRAIT_PROTOCOL_IRSEA' );