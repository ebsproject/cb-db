--liquibase formatted sql

--changeset postgres:add_min_value_for_all_year_related_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1261 Add min_value for all year related variables



WITH t1 AS (
    SELECT 
        scale_id
    FROM 
        master.variable
    WHERE
        abbrev
    ILIKE
        '%YEAR%'
    ORDER BY
        id
)
UPDATE
    master.scale AS ms
SET
     min_value = '1950'
FROM
    t1
WHERE
    ms.id = t1.scale_id;
;



--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         scale_id
--rollback     FROM 
--rollback         master.variable
--rollback     WHERE
--rollback         abbrev
--rollback     ILIKE
--rollback         '%YEAR%'
--rollback     ORDER BY
--rollback         id
--rollback )
--rollback UPDATE
--rollback     master.scale AS ms
--rollback SET
--rollback      min_value = NULL
--rollback FROM
--rollback     t1
--rollback WHERE
--rollback     ms.id = t1.scale_id;
--rollback ;