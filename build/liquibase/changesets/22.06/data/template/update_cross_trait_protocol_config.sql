--liquibase formatted sql

--changeset postgres:add_mapping_file_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3645 Remove 'REMARKS' trait in the trait protocol configuration



UPDATE
    platform.config
SET config_value =
        $${
            "required_traits": [
                "DATE_CROSSED",
                "NO_OF_SEED"
            ]
        }$$
WHERE abbrev = 'CROSS_TRAIT_PROTOCOL_BW';

UPDATE
    platform.config
SET config_value =
        $${
            "required_traits": [
                "DATE_CROSSED",
                "NO_OF_SEED",
                "HV_METH_DISC"
            ]
        }$$
WHERE abbrev = 'CROSS_TRAIT_PROTOCOL_IRSEA';



--rollback UPDATE
--rollback     platform.config
--rollback SET config_value =
--rollback         $${
--rollback             "required_traits": [
--rollback                 "DATE_CROSSED",
--rollback                 "REMARKS",
--rollback                 "NO_OF_SEED"
--rollback             ]
--rollback         }$$
--rollback WHERE abbrev = 'CROSS_TRAIT_PROTOCOL_BW';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET config_value =
--rollback         $${
--rollback             "required_traits": [
--rollback                 "DATE_CROSSED",
--rollback                 "REMARKS",
--rollback                 "NO_OF_SEED",
--rollback                 "HV_METH_DISC"
--rollback             ]
--rollback         }$$
--rollback WHERE abbrev = 'CROSS_TRAIT_PROTOCOL_IRSEA';
