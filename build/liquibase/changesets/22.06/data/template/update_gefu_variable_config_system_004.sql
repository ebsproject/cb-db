--liquibase formatted sql

--changeset postgres:update_gefu_variable_config_system_004 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3527 GM: Implement updated and new configuration - Data Validation



UPDATE platform.config
SET
    config_value = 
    $${
        "values":[
            {
                "abbrev": "DESIGNATION",
                "name": "Germplasm Name",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "designation",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GERMPLASM_NAME_TYPE",
                "name": "Germplasm Name Type",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "nameType",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PARENTAGE",
                "name": "Parentage",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "parentage",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GENERATION",
                "name": "Generation",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "generation",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GERMPLASM_STATE",
                "name": "Germplasm State",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "state",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "TAXON_ID",
                "name": "Taxon ID",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "taxonomyDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "taxonomies",
                "retrieve_api_search_field": "taxonId",
                "retrieve_api_value_field":  "taxonomyDbId"
            },
            {
                "abbrev": "SEED_NAME",
                "name": "Seed Name",
                "usage": "required",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "seedName",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PACKAGE_LABEL",
                "name": "Package Label",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageLabel",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PROGRAM",
                "name": "Program",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_search_field": "programCode",
                "retrieve_api_value_field":  "programDbId"
            },
            {
                "abbrev": "PACKAGE_STATUS",
                "name": "Package Status",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageStatus",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            }
        ]
    }$$
WHERE
    abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT'
;



--rollback  UPDATE platform.config
--rollback  SET
--rollback      config_value = 
--rollback      $${
--rollback          "values":[
--rollback              {
--rollback                  "abbrev": "DESIGNATION",
--rollback                  "name": "Germplasm Name",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "designation",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "GERMPLASM_NAME_TYPE",
--rollback                  "name": "Germplasm Name Type",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "nameType",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "PARENTAGE",
--rollback                  "name": "Parentage",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "parentage",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "GENERATION",
--rollback                  "name": "Generation",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "generation",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "GERMPLASM_STATE",
--rollback                  "name": "Germplasm State",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "state",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "TAXON_ID",
--rollback                  "name": "Taxon ID",
--rollback                  "usage": "required",
--rollback                  "entity": "germplasm",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "taxonomyDbId",
--rollback                  "retrieve_db_id": "true",
--rollback                  "retrieve_endpoint": "taxonomies",
--rollback                  "retrieve_api_search_field": "taxonId",
--rollback                  "retrieve_api_value_field":  "taxonomyDbId"
--rollback              },
--rollback              {
--rollback                  "abbrev": "SEED_NAME",
--rollback                  "name": "Seed Name",
--rollback                  "usage": "required",
--rollback                  "entity": "seed",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "seedName",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "LABEL",
--rollback                  "name": "Package Label",
--rollback                  "usage": "required",
--rollback                  "entity": "package",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "packageLabel",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              },
--rollback              {
--rollback                  "abbrev": "PROGRAM",
--rollback                  "name": "Program",
--rollback                  "usage": "required",
--rollback                  "entity": "package",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "programDbId",
--rollback                  "retrieve_db_id": "true",
--rollback                  "retrieve_endpoint": "programs",
--rollback                  "retrieve_api_search_field": "programCode",
--rollback                  "retrieve_api_value_field":  "programDbId"
--rollback              },
--rollback              {
--rollback                  "abbrev": "PACKAGE_STATUS",
--rollback                  "name": "Package Status",
--rollback                  "usage": "required",
--rollback                  "entity": "package",
--rollback                  "type": "column",
--rollback                  "visible": "true",
--rollback                  "required": "true",
--rollback                  "data_type": "string",
--rollback                  "api_field": "packageStatus",
--rollback                  "retrieve_db_id": "false",
--rollback                  "retrieve_endpoint": "",
--rollback                  "retrieve_api_search_field": "",
--rollback                  "retrieve_api_value_field":  ""
--rollback              }
--rollback          ]
--rollback      }$$
--rollback  WHERE
--rollback      abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT'
--rollback  ;
