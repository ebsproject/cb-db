--liquibase formatted sql

--changeset postgres:add_new_gefu_variable_config_system context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3438 GM DB: Create new configuration for system-defined variables



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT',
    'Germplasm File Upload system-defined variables configuration', 
    $${
        "values":[
            {
                "abbrev": "DESIGNATION",
                "name": "Germplasm Name",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "designation",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GERMPLASM_NAME_TYPE",
                "name": "Germplasm Name Type",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "germplasmNameType",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PARENTAGE",
                "name": "Parentage",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "parentage",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GENERATION",
                "name": "Generation",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "generation",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "GERMPLASM_STATE",
                "name": "Generation",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "germplasmState",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "TAXON_ID",
                "name": "Taxonomy ID",
                "usage": "required",
                "entity": "germplasm",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "taxonomyDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "taxonomies",
                "retrieve_api_search_field": "taxonomyIdValue",
                "retrieve_api_value_field":  "taxonomyDbId"
            },
            {
                "abbrev": "SEED_NAME",
                "name": "Seed Name",
                "usage": "required",
                "entity": "seed",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "seedName",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "LABEL",
                "name": "Package Label",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageLabel",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            },
            {
                "abbrev": "PROGRAM",
                "name": "Program",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "retrieve_endpoint": "programs",
                "retrieve_api_search_field": "programCode",
                "retrieve_api_value_field":  "programDbId"
            },
            {
                "abbrev": "PACKAGE_STATUS",
                "name": "Package Status",
                "usage": "required",
                "entity": "package",
                "type": "column",
                "visible": "true",
                "required": "true",
                "data_type": "string",
                "api_field": "packageStatus",
                "retrieve_db_id": "false",
                "retrieve_endpoint": "",
                "retrieve_api_search_field": "",
                "retrieve_api_value_field":  ""
            }
        ]
    }$$,
    1,
    'File upload, data validation, and creation of records',
    1,
    'added by k.delarosa'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT';
