--liquibase formatted sql

--changeset postgres:add_germplasm_family_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1059 Add germplasm family entity



-- add table to dictionary
INSERT INTO "dictionary"."table"
    (database_id, schema_id, abbrev, name, comment, creator_id)
SELECT
    db.id AS database_id,
    sch.id AS schema_id,
    'FAMILY' AS abbrev,
    'family' AS name,
    'Relationships of the germplasm in the genealogy' AS comment,
    1 AS creator_id
FROM
    "dictionary"."database" AS db,
    "dictionary"."schema" AS sch
WHERE
    db.abbrev = 'CB'
    AND sch.name = 'germplasm'
    AND db.id = sch.database_id
;

-- add entity to dictionary
INSERT INTO "dictionary".entity
    (table_id, abbrev, name, description, creator_id)
SELECT
    tbl.id AS table_id,
    'FAMILY' AS abbrev,
    'family' AS name,
    'Relationships of the germplasm in the genealogy' AS description,
    1 AS creator_id
FROM
    "dictionary"."schema" AS sch,
    "dictionary"."table" AS tbl
WHERE
    sch.name = 'germplasm'
    AND tbl.name = 'family'
    AND sch.id = tbl.schema_id
;



-- revert changes
--rollback DELETE FROM
--rollback     "dictionary"."entity"
--rollback WHERE
--rollback     abbrev = 'FAMILY'
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     "dictionary"."table"
--rollback WHERE
--rollback     abbrev = 'FAMILY'
--rollback ;



--changeset postgres:add_germplasm_family_member_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1059 Add germplasm family_member entity



-- add table to dictionary
INSERT INTO "dictionary"."table"
    (database_id, schema_id, abbrev, name, comment, creator_id)
SELECT
    db.id AS database_id,
    sch.id AS schema_id,
    'FAMILY_MEMBER' AS abbrev,
    'family member' AS name,
    'Germplasm members of a family' AS comment,
    1 AS creator_id
FROM
    "dictionary"."database" AS db,
    "dictionary"."schema" AS sch
WHERE
    db.abbrev = 'CB'
    AND sch.name = 'germplasm'
    AND db.id = sch.database_id
;

-- add entity to dictionary
INSERT INTO "dictionary".entity
    (table_id, abbrev, name, description, creator_id)
SELECT
    tbl.id AS table_id,
    'FAMILY_MEMBER' AS abbrev,
    'family member' AS name,
    'Germplasm members of a family' AS description,
    1 AS creator_id
FROM
    "dictionary"."schema" AS sch,
    "dictionary"."table" AS tbl
WHERE
    sch.name = 'germplasm'
    AND tbl.name = 'family_member'
    AND sch.id = tbl.schema_id
;



-- revert changes
--rollback DELETE FROM
--rollback     "dictionary"."entity"
--rollback WHERE
--rollback     abbrev = 'FAMILY_MEMBER'
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     "dictionary"."table"
--rollback WHERE
--rollback     abbrev = 'FAMILY_MEMBER'
--rollback ;
