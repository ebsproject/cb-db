--liquibase formatted sql

--changeset postgres:update_country_geospatial_objects context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1023 Update country geospatial objects



-- update records to type = country where type = site and sub-type = country
UPDATE
    place.geospatial_object AS geo
SET
    geospatial_object_type = 'country',
    geospatial_object_subtype = NULL,
    notes = platform.append_text(geo.notes, 'DB-1023 country')
WHERE
    geo.geospatial_object_type = 'site'
    AND geo.geospatial_object_subtype = 'country'
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object AS geo
--rollback SET
--rollback     geospatial_object_type = 'site',
--rollback     geospatial_object_subtype = 'country',
--rollback     notes = platform.detach_text(geo.notes, 'DB-1023 country')
--rollback WHERE
--rollback     geo.geospatial_object_type = 'country'
--rollback     AND geo.geospatial_object_subtype IS NULL
--rollback     AND geo.notes LIKE '%DB-1023 country%'
--rollback ;
