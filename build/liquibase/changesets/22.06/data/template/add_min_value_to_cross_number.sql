--liquibase formatted sql


--changeset postgres:add_min_value_to_cross_number context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1270 Add min_value to CROSS_NUMBER



INSERT INTO
    master.scale (abbrev,name,unit,type,level,min_value) 
VALUES 
    ('CROSS_NUMBER','Cross Number',NULL,'discrete','nominal','0')
;
    
UPDATE 
    master.variable AS mv
SET
    scale_id = ms.id
FROM
    master.scale AS ms
WHERE
    ms.abbrev = 'CROSS_NUMBER'
AND
    mv.abbrev = 'CROSS_NUMBER'
;



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     scale_id = NULL
--rollback WHERE
--rollback     abbrev = 'CROSS_NUMBER'
--rollback ;
--rollback 
--rollback DELETE FROM 
--rollback     master.scale
--rollback WHERE
--rollback     abbrev = 'CROSS_NUMBER'
--rollback ;