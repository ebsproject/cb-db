--liquibase formatted sql

--changeset postgres:add_scale_values_to_entry_list_status_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Add scale values to ENTRY_LIST_STATUS variable



-- add scale value/s to a variable
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 1) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'ENTRY_LIST_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('parents added', 'parents added', 'parents added', 'PARENTS_ADDED', 'show'),
        ('parent list specified', 'parent list specified', 'parent list specified', 'PARENT_LIST_SPECIFIED', 'show'),
        ('crosses added', 'crosses added', 'crosses added', 'CROSSES_ADDED', 'show'),
        ('cross list specified', 'cross list specified', 'cross list specified', 'CROSS_LIST_SPECIFIED', 'show'),
        ('deletion in progress', 'deletion in progress', 'deletion in progress', 'DELETION_IN_PROGRESS', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'ENTRY_LIST_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'ENTRY_LIST_STATUS_PARENTS_ADDED',
--rollback         'ENTRY_LIST_STATUS_PARENT_LIST_SPECIFIED',
--rollback         'ENTRY_LIST_STATUS_CROSSES_ADDED',
--rollback         'ENTRY_LIST_STATUS_CROSS_LIST_SPECIFIED',
--rollback         'ENTRY_LIST_STATUS_DELETION_IN_PROGRESS'
--rollback     )
--rollback ;
