--liquibase formatted sql

--changeset postgres:remove_created_scale_value_from_entry_list_status_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1252 Remove created scale value from ENTRY_LIST_STATUS variable



-- remove scale value
DELETE FROM
    master.scale_value
WHERE
    abbrev IN (
        'ENTRY_LIST_STATUS_CREATED'
    )
;

-- update ordering of scale values
WITH t_scalval AS (
    SELECT
        scalval.id,
        ROW_NUMBER() OVER (ORDER BY scalval.order_number) AS order_number
    FROM
        master.scale_value AS scalval
        JOIN master.variable AS var
            ON var.scale_id = scalval.scale_id
    WHERE
        var.abbrev = 'ENTRY_LIST_STATUS'
    ORDER BY
        scalval.order_number
)
UPDATE
    master.scale_value AS scalval
SET
    order_number = t.order_number
FROM
    t_scalval AS t
WHERE
    scalval.id = t.id
;



-- revert changes
--rollback -- add scale value/s to a variable
--rollback WITH t_last_order_number AS (
--rollback     SELECT
--rollback         COALESCE(MAX(scalval.order_number), 1) AS last_order_number
--rollback     FROM
--rollback         master.variable AS var
--rollback         LEFT JOIN master.scale_value AS scalval
--rollback             ON scalval.scale_id = var.scale_id
--rollback     WHERE
--rollback         var.abbrev = 'ENTRY_LIST_STATUS'
--rollback         AND var.is_void = FALSE
--rollback         AND scalval.is_void = FALSE
--rollback )
--rollback INSERT INTO master.scale_value
--rollback     (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.value,
--rollback     t.last_order_number + ROW_NUMBER() OVER () AS order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.scale_value_status,
--rollback     var.abbrev || '_' || scalval.abbrev AS abbrev,
--rollback     var.creator_id
--rollback FROM
--rollback     master.variable AS var,
--rollback     t_last_order_number AS t,
--rollback     (
--rollback         VALUES
--rollback         ('created', 'created', 'created', 'CREATED', 'show')
--rollback     ) AS scalval (
--rollback         value, description, display_name, abbrev, scale_value_status
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'ENTRY_LIST_STATUS'
--rollback ;
--rollback 
--rollback -- update ordering of scale values
--rollback WITH t_scalval AS (
--rollback     SELECT
--rollback         scalval.id,
--rollback         ROW_NUMBER() OVER (ORDER BY scalval.order_number) AS order_number
--rollback     FROM
--rollback         master.scale_value AS scalval
--rollback         JOIN master.variable AS var
--rollback             ON var.scale_id = scalval.scale_id
--rollback     WHERE
--rollback         var.abbrev = 'ENTRY_LIST_STATUS'
--rollback     ORDER BY
--rollback         scalval.order_number
--rollback )
--rollback UPDATE
--rollback     master.scale_value AS scalval
--rollback SET
--rollback     order_number = t.order_number
--rollback FROM
--rollback     t_scalval AS t
--rollback WHERE
--rollback     scalval.id = t.id
--rollback ;
