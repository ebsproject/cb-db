--liquibase formatted sql

--changeset postgres:clear_error_log_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1255 Clear error_log values



-- clear values
UPDATE
    germplasm.file_upload
SET
    error_log = NULL
WHERE
    error_log IS NOT NULL
;



-- revert changes
--rollback SELECT NULL;
