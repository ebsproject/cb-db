--liquibase formatted sql

--changeset postgres:update_unknown_parentage_in_germplasm context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1259 Update UNKNOWN parentage in germplasm



-- change casing
UPDATE
    germplasm.germplasm AS ge
SET
    parentage = 'UNKNOWN'
WHERE
    parentage ILIKE 'unknown%'
;



-- revert changes
--rollback SELECT NULL;
