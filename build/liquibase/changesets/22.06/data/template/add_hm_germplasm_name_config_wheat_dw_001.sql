--liquibase formatted sql

--changeset postgres:add_hm_germplasm_name_config_wheat_dw_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3267 HM DB: Add BCID rules to the wheat naming configs


-- add germplasm name config
INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'HM_NAME_PATTERN_GERMPLASM_WHEAT_DW',
    'Harvest Manager Wheat Germplasm Name Pattern-Durum Wheat (DW)', 
    $$
        {
            "PLOT": {
                "default": {
                    "not_fixed": {
                        "default": {
                            "bulk": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "free-text",
                                    "value": "0",
                                    "order_number": 2
                                },
                                {
                                    "type": "free-text",
                                    "value": "TOP",
                                    "special": "top",
                                    "order_number": 3
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "grainColor",
                                    "order_number": 4
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "fieldOriginSiteCode",
                                    "order_number": 5
                                }
                            ],
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "",
                                    "order_number": 0
                                }
                            ],
                            "selected bulk": {
                                "with_no_of_plant": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "germplasmDesignation",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "0",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "field",
                                        "entity": "harvestData",
                                        "field_name": "no_of_plants",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "TOP",
                                        "special": "top",
                                        "order_number": 4
                                    },
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "grainColor",
                                        "order_number": 5
                                    },
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "fieldOriginSiteCode",
                                        "order_number": 6
                                    }
                                ],
                                "without_no_of_plant": [
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "germplasmDesignation",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "delimiter",
                                        "value": "-",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "099",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "TOP",
                                        "special": "top",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "grainColor",
                                        "order_number": 4
                                    },
                                    {
                                        "type": "field",
                                        "entity": "plot",
                                        "field_name": "fieldOriginSiteCode",
                                        "order_number": 5
                                    }
                                ]
                            },
                            "individual spike": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "counter",
                                    "order_number": 2
                                },
                                {
                                    "type": "free-text",
                                    "value": "TOP",
                                    "special": "top",
                                    "order_number": 3
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "fieldOriginSiteCode",
                                    "order_number": 4
                                }
                            ],
                            "single plant selection": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "counter",
                                    "order_number": 2
                                },
                                {
                                    "type": "free-text",
                                    "value": "TOP",
                                    "special": "top",
                                    "order_number": 4
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "fieldOriginSiteCode",
                                    "order_number": 3
                                }
                            ]
                        }
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "",
                                    "order_number": 0
                                }
                            ]
                        }
                    }
                },
                "single cross": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "CD",
                                    "order_number": 0
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "growthHabit",
                                    "order_number": 1
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "harvestYearYY",
                                    "order_number": 2
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "originSiteCode",
                                    "order_number": 3
                                },
                                {
                                    "type": "counter",
                                    "max_digits": 5,
                                    "leading_zero": "yes",
                                    "order_number": 4
                                },
                                {
                                    "type": "free-text",
                                    "value": "S",
                                    "order_number": 5
                                }
                            ]
                        }
                    }
                },
                "double cross": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "CD",
                                    "order_number": 0
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "growthHabit",
                                    "order_number": 1
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "harvestYearYY",
                                    "order_number": 2
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "originSiteCode",
                                    "order_number": 3
                                },
                                {
                                    "type": "counter",
                                    "max_digits": 5,
                                    "leading_zero": "yes",
                                    "order_number": 4
                                },
                                {
                                    "type": "free-text",
                                    "value": "D",
                                    "order_number": 5
                                }
                            ]
                        }
                    }
                },
                "top cross": {
                    "default": {
                        "default": {
                            "default": [
                                {
                                    "type": "free-text",
                                    "value": "CD",
                                    "order_number": 0
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "growthHabit",
                                    "order_number": 1
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "harvestYearYY",
                                    "order_number": 2
                                },
                                {
                                    "type": "field",
                                    "entity": "cross",
                                    "field_name": "originSiteCode",
                                    "order_number": 3
                                },
                                {
                                    "type": "counter",
                                    "max_digits": 5,
                                    "leading_zero": "yes",
                                    "order_number": 4
                                },
                                {
                                    "type": "free-text",
                                    "value": "T",
                                    "order_number": 5
                                }
                            ]
                        }
                    }
                },
                "backcross": {
                    "default": {
                        "default": {
                            "default": {
                                "femaleRecurrent": [
                                    {
                                        "type": "free-text",
                                        "value": "CD",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "growthHabit",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "harvestYearYY",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "originSiteCode",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 5,
                                        "leading_zero": "yes",
                                        "order_number": 4
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "F",
                                        "order_number": 5
                                    }
                                ],
                                "maleRecurrent": [
                                    {
                                        "type": "free-text",
                                        "value": "CD",
                                        "order_number": 0
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "growthHabit",
                                        "order_number": 1
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "harvestYearYY",
                                        "order_number": 2
                                    },
                                    {
                                        "type": "field",
                                        "entity": "cross",
                                        "field_name": "originSiteCode",
                                        "order_number": 3
                                    },
                                    {
                                        "type": "counter",
                                        "max_digits": 5,
                                        "leading_zero": "yes",
                                        "order_number": 4
                                    },
                                    {
                                        "type": "free-text",
                                        "value": "M",
                                        "order_number": 5
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state": {
                        "germplasm_type": {
                            "harvest_method": [
                                {
                                    "type": "free-text",
                                    "value": "ABC",
                                    "order_number": 0
                                },
                                {
                                    "type": "field",
                                    "entity": "<entity>",
                                    "field_name": "<field_name>",
                                    "order_number": 1
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "counter",
                                    "order_number": 3
                                },
                                {
                                    "type": "db-sequence",
                                    "schema": "<schema>",
                                    "order_number": 4,
                                    "sequence_name": "<sequence_name>"
                                },
                                {
                                    "type": "free-text-repeater",
                                    "value": "ABC",
                                    "minimum": "2",
                                    "delimiter": "*",
                                    "order_number": 5
                                }
                            ]
                        }
                    }
                }
            }
        }
    $$,
    1,
    'harvest_manager',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NAME_PATTERN_GERMPLASM_WHEAT_DW';