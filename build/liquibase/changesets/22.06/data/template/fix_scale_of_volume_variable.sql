--liquibase formatted sql

--changeset postgres:remove_scale_of_volume_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1292 Remove scale of VOLUME variable



-- unlink scale
UPDATE
    master.variable
SET
    scale_id = NULL
WHERE
    abbrev = 'VOLUME'
;



-- revert changes
--rollback SELECT NULL;



--changeset postgres:create_scale_for_volume_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1292 Create scale for VOLUME variable



-- link new scale
WITH t AS (
    INSERT INTO master.scale
        ("type", "level", min_value, creator_id)
    VALUES
        ('continuous', 'ratio', '0', 1)
    RETURNING
        id
)
UPDATE
    master.variable AS var
SET
    scale_id = t.id
FROM
    t
WHERE
    var.abbrev = 'VOLUME'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     scale_id = NULL
--rollback WHERE
--rollback     abbrev = 'VOLUME'
--rollback ;
