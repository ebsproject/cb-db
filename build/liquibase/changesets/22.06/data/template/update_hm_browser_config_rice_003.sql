--liquibase formatted sql

--changeset postgres:update_hm_browser_config_rice_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3579 HM DB: Update configuration records for RICE Selfing Single Plant Seed Increase Harvest Use Case



-- update browser config
UPDATE platform.config
SET
    config_value = '{
        "default": {
            "default": {
                "default": {
                    "input_columns": [],
                    "numeric_variables": [],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_SELFING": {
            "fixed": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": false,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                },
                "fixed_line": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "NO_OF_PLANTS",
                            "sub_type": "single_int",
                            "field_name": "noOfPlant",
                            "placeholder": "No. of plants",
                            "harvest_methods": [
                                "HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "unknown": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "not_fixed": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK",
                                "HV_METH_DISC_SINGLE_SEED_DESCENT"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "NO_OF_PLANTS",
                            "sub_type": "single_int",
                            "field_name": "noOfPlant",
                            "placeholder": "No. of plants",
                            "harvest_methods": [
                                "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "PANNO_SEL",
                            "sub_type": "single_int",
                            "field_name": "noOfPanicle",
                            "placeholder": "No. of panicles",
                            "harvest_methods": [
                                "HV_METH_DISC_PANICLE_SELECTION"
                            ]
                        },
                        {
                            "min": null,
                            "type": "text",
                            "abbrev": "SPECIFIC_PLANT",
                            "sub_type": "comma_sep_int",
                            "field_name": "specificPlantNo",
                            "placeholder": "Specific plant no.",
                            "harvest_methods": [
                                "HV_METH_DISC_PLANT_SPECIFIC",
                                "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                },
                "transgenic": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "NO_OF_PLANTS",
                            "sub_type": "single_int",
                            "field_name": "noOfPlant",
                            "placeholder": "No. of plants",
                            "harvest_methods": [
                                "HV_METH_DISC_SINGLE_PLANT_SELECTION"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                },
                "genome_edited": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "NO_OF_PLANTS",
                            "sub_type": "single_int",
                            "field_name": "noOfPlant",
                            "placeholder": "No. of plants",
                            "harvest_methods": [
                                "HV_METH_DISC_SINGLE_PLANT_SELECTION"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "Test-cross verification": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_BACKCROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_BULK",
                                "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_TEST_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_BAGS",
                            "sub_type": "single_int",
                            "field_name": "noOfBag",
                            "placeholder": "No. of bags",
                            "harvest_methods": [
                                "HV_METH_DISC_TEST_CROSS_HARVEST"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_DOUBLE_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_SINGLE_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_BULK",
                                "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_TRANSGENESIS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_COMPLEX_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_GENOME_EDITING": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_THREE_WAY_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_SEED",
                            "sub_type": "single_int",
                            "field_name": "noOfSeed",
                            "placeholder": "No. of seeds",
                            "harvest_methods": [
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_HYBRID_FORMATION": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_BAGS",
                            "sub_type": "single_int",
                            "field_name": "noOfBag",
                            "placeholder": "No. of bags",
                            "harvest_methods": [
                                "HV_METH_DISC_H_HARVEST"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        },
        "CROSS_METHOD_CMS_MULTIPLICATION": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "required": true,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "required": true,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "required": null,
                            "column_name": "numericVar"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        },
                        {
                            "min": 0,
                            "type": "number",
                            "abbrev": "NO_OF_BAGS",
                            "sub_type": "single_int",
                            "field_name": "noOfBag",
                            "placeholder": "No. of bags",
                            "harvest_methods": [
                                "HV_METH_DISC_A_LINE_HARVEST"
                            ]
                        }
                    ],
                    "additional_required_variables": [
                        {
                            "abbrev": "DATE_CROSSED",
                            "field_name": "crossingDate"
                        }
                    ]
                }
            }
        }
    }
    '
WHERE
    abbrev = 'HM_DATA_BROWSER_CONFIG_RICE'



--rollback  UPDATE platform.config
--rollback  SET
--rollback  config_value = '{
--rollback          "default": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [],
--rollback                      "numeric_variables": [],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_SELFING": {
--rollback              "fixed": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": false,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  },
--rollback                  "fixed_line": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SEED_INCREASE"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  },
--rollback                  "transgenic": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  },
--rollback                  "genome_edited": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              },
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              },
--rollback              "unknown": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              },
--rollback              "not_fixed": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK",
--rollback                                  "HV_METH_DISC_SINGLE_SEED_DESCENT"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION",
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "PANNO_SEL",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPanicle",
--rollback                              "placeholder": "No. of panicles",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_PANICLE_SELECTION"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "text",
--rollback                              "abbrev": "SPECIFIC_PLANT",
--rollback                              "sub_type": "comma_sep_int",
--rollback                              "field_name": "specificPlantNo",
--rollback                              "placeholder": "Specific plant no.",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_PLANT_SPECIFIC",
--rollback                                  "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  },
--rollback                  "transgenic": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  },
--rollback                  "genome_edited": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  "",
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 1,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_PLANTS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfPlant",
--rollback                              "placeholder": "No. of plants",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_SINGLE_PLANT_SELECTION"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              },
--rollback              "Test-cross verification": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_BACKCROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_BULK",
--rollback                                  "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_TEST_CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_BAGS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfBag",
--rollback                              "placeholder": "No. of bags",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_TEST_CROSS_HARVEST"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_DOUBLE_CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_SINGLE_CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_BULK",
--rollback                                  "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {

--rollback                             "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_TRANSGENESIS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_COMPLEX_CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_GENOME_EDITING": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": []
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_THREE_WAY_CROSS": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_SEED",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfSeed",
--rollback                              "placeholder": "No. of seeds",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_BULK"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_HYBRID_FORMATION": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>", 
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_BAGS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfBag",
--rollback                              "placeholder": "No. of bags",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_H_HARVEST"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                             "abbrev": "DATE_CROSSED",
--rollback                             "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          },
--rollback          "CROSS_METHOD_CMS_MULTIPLICATION": {
--rollback              "default": {
--rollback                  "default": {
--rollback                      "input_columns": [
--rollback                          {
--rollback                              "abbrev": "HVDATE_CONT",
--rollback                              "required": true,
--rollback                              "column_name": "harvestDate"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "HV_METH_DISC",
--rollback                              "required": true,
--rollback                              "column_name": "harvestMethod"
--rollback                          },
--rollback                          {
--rollback                              "abbrev": "<none>",
--rollback                              "required": null,
--rollback                              "column_name": "numericVar"
--rollback                          }
--rollback                      ],
--rollback                      "numeric_variables": [
--rollback                          {
--rollback                              "min": null,
--rollback                              "type": "number",
--rollback                              "abbrev": "<none>",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "<none>",
--rollback                              "placeholder": "Not applicable",
--rollback                              "harvest_methods": [
--rollback                                  ""
--rollback                              ]
--rollback                          },
--rollback                          {
--rollback                              "min": 0,
--rollback                              "type": "number",
--rollback                              "abbrev": "NO_OF_BAGS",
--rollback                              "sub_type": "single_int",
--rollback                              "field_name": "noOfBag",
--rollback                              "placeholder": "No. of bags",
--rollback                              "harvest_methods": [
--rollback                                  "HV_METH_DISC_A_LINE_HARVEST"
--rollback                              ]
--rollback                          }
--rollback                      ],
--rollback                      "additional_required_variables": [
--rollback                          {
--rollback                              "abbrev": "DATE_CROSSED",
--rollback                              "field_name": "crossingDate"
--rollback                          }
--rollback                      ]
--rollback                  }
--rollback              }
--rollback          }
--rollback      }
--rollback      '
--rollback  WHERE
--rollback  	abbrev = 'HM_DATA_BROWSER_CONFIG_RICE'
--rollback  ;