--liquibase formatted sql

--changeset postgres:add_place_facility_attribute context:schema splitStatements:false rollbackSplitStatements:false
--comment: CB: update the facility table and add a sub-facility and container tables



-- Create Table
CREATE TABLE place.facility_attribute (
	id serial4 NOT NULL,
	facility_id int4 NOT NULL,
	variable_id int4 NOT NULL,
	data_value varchar NOT NULL,
	creator_id int4 NOT NULL,
	creation_timestamp timestamptz NOT NULL DEFAULT now(),
	modifier_id int4 NULL,
	modification_timestamp timestamptz NULL,
	is_void bool NOT NULL DEFAULT false,
	notes text NULL,
	event_log jsonb NULL,
	CONSTRAINT pk_facility_attribute_id PRIMARY KEY (id)
);
CREATE INDEX facility_attribute_creator_id_idx ON place.facility_attribute USING btree (creator_id);
CREATE INDEX facility_attribute_facility_id_variable_id_da ON place.facility_attribute USING btree (facility_id, variable_id, data_value);
COMMENT ON INDEX place.facility_attribute_facility_id_variable_id_da IS 'A facility attribute can be retrieved by its variable and value within a facility.';
CREATE INDEX facility_attribute_facility_id_variable_id_id ON place.facility_attribute USING btree (facility_id, variable_id);
COMMENT ON INDEX place.facility_attribute_facility_id_variable_id_id IS 'A facility attribute can be retrieved by its variable within a facility.';
CREATE INDEX facility_attribute_is_void_idx ON place.facility_attribute USING btree (is_void);
CREATE INDEX facility_attribute_modifier_id_idx ON place.facility_attribute USING btree (modifier_id);

-- Table Triggers
create trigger facility_attribute_event_log_tgr before
insert
    or
update
    on
    place.facility_attribute for each row execute function platform.log_record_event();

-- place.facility_attribute foreign keys
ALTER TABLE place.facility_attribute ADD CONSTRAINT facility_attribute_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE place.facility_attribute ADD CONSTRAINT facility_attribute_facility_id_fk FOREIGN KEY (facility_id) REFERENCES place.facility(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE place.facility_attribute ADD CONSTRAINT facility_attribute_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;



--rollback 