--liquibase formatted sql

--changeset postgres:add_hm_taxonomy_preference_config_rice_default context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6056: Add new config for HM Taxonomy Preference for Rice (Default)



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_TAXONOMY_PREFERENCE_RICE_DEFAULT',
        'HM: Taxonomy preference configuration for Rice (Default) Non-selfing cross harvest',
        $$			      
            {
                "taxon_id": "4530",
	            "add_germplasm_attribute": "true"
            }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-6056: Add new config for HM Taxonomy Preference for Rice (Default)'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_TAXONOMY_PREFERENCE_RICE_DEFAULT';