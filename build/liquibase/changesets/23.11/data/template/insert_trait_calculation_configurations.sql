--liquibase formatted sql

--changeset postgres:insert_trait_calculation_configurations.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6600: DC-DB: Create configurations for trait calculation default list



DO $$ 
DECLARE
    json_array jsonb;
BEGIN

    SELECT 
        jsonb_agg(abbrev) 
    INTO 
        json_array
    FROM (
        SELECT DISTINCT
            master.variable.abbrev
        FROM
            master.formula_parameter
        JOIN
            master.variable ON master.formula_parameter.result_variable_id = master.variable.id
        WHERE
            master.formula_parameter.is_void = false
        AND
            master.variable.is_void = false
    ) subquery;
    
    -- Insert for DC_GLOBAL_PLOT_SETTINGS

    INSERT INTO platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
    VALUES
    (
        'DC_GLOBAL_PLOT_SETTINGS',
        'DC Global Plot Settings',
        jsonb_build_object(
            'CALCULATED_TRAITS', json_array
        ),
        'data_collection'
    );

    -- Insert for DC_PROGRAM_IRSEA_PLOT_SETTINGS

    INSERT INTO platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
    VALUES
    (
        'DC_PROGRAM_IRSEA_PLOT_SETTINGS',
        'DC Program IRSEA Plot Settings',
        '{
            "CALCULATED_TRAITS": [
                "ADJAYLD_G_CONT",
                "ADJAYLD_KG_CONT",
                "ADJAYLD_KG_CONT4",
                "ADJHVAREA_CONT1",
                "ADJHVAREA_CONT2",
                "ADJHVAREA_CONT3",
                "ADJYLD3_CONT",
                "AREA_FACT",
                "AYLD_CONT",
                "DISEASE_INDEX",
                "GYLD_DSR_KG",
                "HI_CONT",
                "HV_AREA_DSR_SQM",
                "HV_AREA_SQM",
                "HVHILL_PCT",
                "MISS_HILL_CONT",
                "PLOT_AREA_DSR_SQM",
                "STRWWT_CONT",
                "STRWWTBM_CONT",
                "SUB_SURVIVAL_PCT",
                "TOT_PLTGAP_AREA_SQM",
                "TOTAL_HILL_CONT",
                "TOTBM_CONT_KG",
                "TOTBM_CONT1",
                "YLD_0_CONT1",
                "YLD_0_CONT2",
                "YLD_0_CONT3",
                "YLD_0_CONT4",
                "YLD_0_CONT5",
                "YLD_0_CONT6",
                "YLD_0_CONT7",
                "YLD_CONT_TON",
                "YLD_CONT_TON2",
                "YLD_CONT_TON5",
                "YLD_CONT_TON6",
                "YLD_CONT_TON7",
                "YLD_CONT1",
                "YLD_CONT2",
                "YLD_CONT4",
                "YLD_DSR_TON"
            ]
        }',
        'data_collection'
    );

    -- Insert for DC_PROGRAM_KE_PLOT_SETTINGS

    INSERT INTO platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
    VALUES
    (
        'DC_PROGRAM_KE_PLOT_SETTINGS',
        'DC Program KE Plot Settings',
        '{
            "CALCULATED_TRAITS": [
                "GY_CMP_THA",
                "GY_MOI15_CMP_THA",
                "GY_MOI13_5_CMP_THA",
                "GW_M_KG",
                "GW_CMP_KG",
                "GW_SUB_M_KG",
                "EW_M_KG",
                "EW_SUB_M_KG",
                "SHELL_CMP_PCT",
                "GMOI_M_PCT",
                "PLOT_AREA_HARVESTED"
            ]
        }',
        'data_collection'
    );

    -- Insert for DC_PROGRAM_BW_PLOT_SETTINGS

    INSERT INTO platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
    VALUES
    (
        'DC_PROGRAM_BW_PLOT_SETTINGS',
        'DC Program BW Plot Settings',
        '{
            "CALCULATED_TRAITS" : [
                "GY_CALC_KGHA",
                "GY_CALC_GM2",
                "GY_CALC_DAHA",
                "GY_CALC_THA",
                "PH_M_CM",
                "PH_M_M",
                "PLANTING_DATE",
                "EMER_DATE_YMD",
                "EMER_DTO_DAY",
                "BOOT_DATEINIT_YMD",
                "BOOT_DTOINIT_DAY",
                "BOOT_DATE_YMD",
                "BOOT_DTO_DAY",
                "HD_DATE_YMD",
                "HD_DTO_DAY",
                "MAT_DATE_YMD",
                "MAT_DTO_DAY",
                "TSPL_DATE_YMD",
                "TSPL_DTO_DAY"
            ]
        }',
        'data_collection'
    );

    -- Insert for DC_ROLE_COLLABORATOR_PLOT_SETTINGS

    INSERT INTO platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
    VALUES
    (
        'DC_ROLE_COLLABORATOR_PLOT_SETTINGS',
        'DC Role Collaborator Plot Settings',
        jsonb_build_object(
            'CALCULATED_TRAITS', json_array
        ),
        'data_collection'
    );
END $$;



--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_PLOT_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_PLOT_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_PLOT_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW_PLOT_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_PLOT_SETTINGS';