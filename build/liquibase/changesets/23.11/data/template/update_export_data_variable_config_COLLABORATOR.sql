--liquibase formatted sql

--changeset postgres:update_export_data_variable_config_COLLABORATOR context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6565 M8 CB-EM: Some variables associated with the planting protocol did not have data in the Occurrence metadata and plot data export file



UPDATE
    platform.config
SET
    config_value = $${
        "experiment":[
            {
                "abbrev": "EXPERIMENT_CODE",
                "label": "Experiment Code",
                "attribute": "experimentCode",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_NAME",
                "label": "Experiment Name",
                "attribute": "experimentName",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_YEAR",
                "label": "Experiment Year",
                "attribute": "experimentYear",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "SEASON",
                "label": "Season",
                "attribute": "experimentSeason",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "STAGE_CODE",
                "label": "Stage Code",
                "attribute": "experimentStageCode",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PROJECT",
                "label": "Project",
                "attribute": "experimentProject",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PIPELINE",
                "label": "Pipeline",
                "attribute": "experimentPipeline",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_OBJECTIVE",
                "label": "Experiment Objective",
                "attribute": "experimentObjective",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_TYPE",
                "label": "Experiment Type",
                "attribute": "experimentType",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_SUB_TYPE",
                "label": "Experiment Subtype",
                "attribute": "experimentSubtype",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_DESIGN_TYPE",
                "label": "Experiment Design Type",
                "attribute": "experimentDesignType",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_STATUS",
                "label": "Experiment Status",
                "attribute": "experimentStatus",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "EXPERIMENT_STEWARD",
                "label": "Experiment Steward",
                "attribute": "experimentSteward",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLANTING_SEASON",
                "label": "Planting Season",
                "attribute": "plantingSeason",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "MARKET_SEGMENT",
                "label": "Market Segment",
                "attribute": "MARKET_SEGMENT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ECOSYSTEM",
                "label": "Ecosystem",
                "attribute": "ECOSYSTEM",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "REMARKS",
                "label": "Experiment Remarks",
                "attribute": "experimentRemarks",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            }
        ],
        "occurrence":[
            {
                "abbrev": "OCCURRENCE_CODE",
                "label": "Occurrence Code",
                "attribute": "occurrenceCode",
                "required": "true",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "OCCURRENCE_NAME",
                "label": "Occurrence Name",
                "attribute": "occurrenceName",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "OCCURRENCE_NUMBER",
                "label": "Occurrence Number",
                "attribute": "occurrenceNumber",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "SITE_CODE",
                "label": "Site Code",
                "attribute": "siteCode",
                "required": "true",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "SITE",
                "label": "Site Name",
                "attribute": "siteName",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "LOCATION_CODE",
                "label": "Location Code",
                "attribute": "locationCode",
                "required": "true",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "LOCATION_NAME",
                "label": "Location Name",
                "attribute": "locationName",
                "required": "true",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLANTING_DATE",
                "label": "Planting Date",
                "attribute": "plantingDate",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "true"
            },
            {
                "abbrev": "GEOSPATIAL_COORDS",
                "label": "Geospatial Coordinates",
                "attribute": "GEOSPATIAL_COORDS",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "LONGITUDE",
                "label": "Longitude",
                "attribute": "LONGITUDE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "LATITUDE",
                "label": "Latitude",
                "attribute": "LATITUDE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ECOSYSTEM",
                "label": "Ecosystem",
                "attribute": "ECOSYSTEM",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "DESCRIPTION",
                "label": "Description",
                "attribute": "DESCRIPTION",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "CONTCT_PERSON_CONT",
                "label": "Contact Person",
                "attribute": "CONTCT_PERSON_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            }
        ],
        "planting_protocol":[
            {
                "abbrev": "ESTABLISHMENT",
                "label": "Crop Establishment",
                "attribute": "ESTABLISHMENT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLANTING_TYPE",
                "label": "Planting Type",
                "attribute": "PLANTING_TYPE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_TYPE",
                "label": "Plot Type",
                "attribute": "PLOT_TYPE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_LN",
                "label": "PLOT LN",
                "attribute": "PLOT_LN",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_WIDTH",
                "label": "Plot Width",
                "attribute": "PLOT_WIDTH",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ALLEY_LENGTH",
                "label": "Alley Length",
                "attribute": "ALLEY_LENGTH",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "TRANS_DATE_CONT",
                "label": "Transplanting Date",
                "attribute": "TRANS_DATE_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "SEEDING_RATE",
                "label": "Seeding Density",
                "attribute": "SEEDING_RATE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLANTING_INSTRUCTIONS",
                "label": "Planting Instructions",
                "attribute": "PLANTING_INSTRUCTIONS",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_AREA_SQM_CONT",
                "label": "PLOT AREA SQM CONT",
                "attribute": "PLOT_AREA_SQM_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "BED_WIDTH",
                "label": "BED WIDTH",
                "attribute": "BED_WIDTH",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "DISTANCE_BET_PLOTS_IN_M",
                "label": "DISTANCE BET PLOTS IN M",
                "attribute": "DISTANCE_BET_PLOTS_IN_M",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "NO_OF_BEDS_PER_PLOT",
                "label": "NO OF BEDS PER PLOT",
                "attribute": "NO_OF_BEDS_PER_PLOT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_AREA_1",
                "label": "PLOT AREA 1",
                "attribute": "PLOT_AREA_1",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_AREA_2",
                "label": "PLOT AREA 2",
                "attribute": "PLOT_AREA_2",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_AREA_3",
                "label": "PLOT AREA 3",
                "attribute": "PLOT_AREA_3",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_AREA_4",
                "label": "PLOT AREA 4",
                "attribute": "PLOT_AREA_4",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_LN_1",
                "label": "PLOT LN 1",
                "attribute": "PLOT_LN_1",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_WIDTH_MAIZE_BED",
                "label": "PLOT WIDTH MAIZE BED",
                "attribute": "PLOT_WIDTH_MAIZE_BED",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_WIDTH_RICE",
                "label": "PLOT WIDTH RICE",
                "attribute": "PLOT_WIDTH_RICE",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_WIDTH_WHEAT_FLAT",
                "label": "PLOT WIDTH WHEAT FLAT",
                "attribute": "PLOT_WIDTH_WHEAT_FLAT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            }        
        ],
        "management_protocol":[
            {
                "abbrev": "DIST_BET_ROWS",
                "label": "Distance Between Rows",
                "attribute": "DIST_BET_ROWS",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "ROWS_PER_PLOT_CONT",
                "label": "ROWS PER PLOT CONT",
                "attribute": "ROWS_PER_PLOT_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "SEEDING_DATE_CONT",
                "label": "Seeding Date",
                "attribute": "SEEDING_DATE_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "false"
            },
            {
                "abbrev": "FERT1_TYPE_DISC",
                "label": "Fertilizer Type - 1st Application",
                "attribute": "FERT1_TYPE_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT1_DATE_CONT",
                "label": "Fertilizer Date - 1st Application",
                "attribute": "FERT1_DATE_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT1_BRAND",
                "label": "Fertilizer Brand - 1st Application",
                "attribute": "FERT1_BRAND",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT1_METH_DISC",
                "label": "Fertilizer Method - 1st Application",
                "attribute": "FERT1_METH_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT2_TYPE_DISC",
                "label": "Fertilizer Type - 2nd Application",
                "attribute": "FERT2_TYPE_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT2_DATE_CONT",
                "label": "Fertilizer Date - 2nd Application",
                "attribute": "FERT2_DATE_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT2_BRAND",
                "label": "Fertilizer Brand - 2nd Application",
                "attribute": "FERT2_BRAND",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT2_METH_DISC",
                "label": "Fertilizer Method - 2nd Application",
                "attribute": "FERT2_METH_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT3_TYPE_DISC",
                "label": "Fertilizer Type - 3rd Application",
                "attribute": "FERT3_TYPE_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT3_DATE_CONT",
                "label": "Fertilizer Date - 3rd Application",
                "attribute": "FERT3_DATE_CONT",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT3_BRAND",
                "label": "Fertilizer Brand - 3rd Application",
                "attribute": "FERT3_BRAND",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            },
            {
                "abbrev": "FERT3_METH_DISC",
                "label": "Fertilizer Method - 3rd Application",
                "attribute": "FERT3_METH_DISC",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "false",
                "is_uploadable": "true"
            }
        ],
        "entry":[
            {
                "abbrev": "ENTRY_NUMBER",
                "label": "Entry Number",
                "attribute": "entryNumber",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ENTRY_CODE",
                "label": "Entry Code",
                "attribute": "entryCode",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ENTRY_TYPE",
                "label": "Entry Type",
                "attribute": "entryType",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ENTRY_CLASS",
                "label": "Entry Class",
                "attribute": "entryClass",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "ENTRY_ROLE",
                "label": "Entry Role",
                "attribute": "entryRole",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            }
        ],
        "plot":[
            {
                "abbrev": "PLOT_ID",
                "label": "Plot ID",
                "attribute": "plotDbId",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOTNO",
                "label": "Plot Number",
                "attribute": "plotNumber",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_CODE",
                "label": "Plot Code",
                "attribute": "plotCode",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PLOT_TYPE",
                "label": "Plot Type",
                "attribute": "plotType",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "REP",
                "label": "Replication",
                "attribute": "rep",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "BLOCK_NO_CONT",
                "label": "Block Number",
                "attribute": "blockNumber",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PA_X",
                "label": "PA X",
                "attribute": "paX",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            },
            {
                "abbrev": "PA_Y",
                "label": "PA Y",
                "attribute": "paY",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true",
                "is_uploadable": "false"
            }
        ],
        "germplasm":[
            {
                "abbrev": "GERMPLASM_CODE",
                "label": "Germplasm Code",
                "attribute": "germplasmCode",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_NAME",
                "label": "Germplasm Name",
                "attribute": "germplasmName",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_STATE",
                "label": "Germplasm State",
                "attribute": "germplasmState",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "TAXONOMY_NAME",
                "label": "Taxonomy Name",
                "attribute": "taxonomyName",
                "required": "false",
                "is_selected": "true",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_CODE",
                "label": "Female Parent Germplasm Code",
                "attribute": "femaleParentGermplasmCode",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_CODE",
                "label": "Male Parent Germplasm Code",
                "attribute": "maleParentGermplasmCode",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_TYPE",
                "label": "Germplasm Type",
                "attribute": "germplasmType",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GERMPLASM_NAME_TYPE",
                "label": "Germplasm Name Type",
                "attribute": "germplasmNameType",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            }
        ],
        "trait":[
            {
                "abbrev": "BL_NURS_0_9",
                "label": "Bl1 0-9",
                "attribute": "BL_NURS_0_9",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "YLD_CONT2",
                "label": "YLD_CONT2",
                "attribute": "YLD_CONT2",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "YLD_CONT_TON",
                "label": "YLD_TON",
                "attribute": "YLD_CONT_TON",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "TIL_AVE_CONT",
                "label": "TILLER_AVG",
                "attribute": "TIL_AVE_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "NO_OF_SEED",
                "label": "NO OF SEED",
                "attribute": "NO_OF_SEED",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PACP_SCOR_1_9",
                "label": "Pacp 1-9",
                "attribute": "PACP_SCOR_1_9",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "FLW50",
                "label": "FLW50",
                "attribute": "FLW50",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "DATE_CROSSED",
                "label": "CROSSING DATE",
                "attribute": "DATE_CROSSED",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "TRANSFORMATION_DATE",
                "label": "TRANSFORMATION DATE",
                "attribute": "TRANSFORMATION_DATE",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HVDATE_CONT",
                "label": "HARVEST DATE",
                "attribute": "HVDATE_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "RLODGINC_CT_PLNTPLOT",
                "label": "RLODGINC CT PLNTPLOT",
                "attribute": "RLODGINC_CT_PLNTPLOT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PSTANDHV_CT_PLNTPLOT",
                "label": "PSTANDHV CT PLNTPLOT",
                "attribute": "PSTANDHV_CT_PLNTPLOT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HVHILL_CONT",
                "label": "HV HILL",
                "attribute": "HVHILL_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "TOTAL_HILL_CONT",
                "label": "TOTAL HILLS",
                "attribute": "TOTAL_HILL_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PSTANDTH_CT_PLNTPLOT",
                "label": "PSTANDTH CT PLNTPLOT",
                "attribute": "PSTANDTH_CT_PLNTPLOT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "RLODGINC_CMP_PCT",
                "label": "RLODGINC CMP PCT",
                "attribute": "RLODGINC_CMP_PCT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "ASI_TRAIT",
                "label": "ASI Trait",
                "attribute": "ASI_TRAIT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT4_CONT",
                "label": "Ht4",
                "attribute": "HT4_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "CML5_CONT",
                "label": "CL5",
                "attribute": "CML5_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "AYLD_CONT",
                "label": "AYLD_G",
                "attribute": "AYLD_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT11_CONT",
                "label": "Ht11",
                "attribute": "HT11_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT_CONT",
                "label": "HT_AVG",
                "attribute": "HT_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PNL5_CONT",
                "label": "PnL 5",
                "attribute": "PNL5_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT_CONT_ENT",
                "label": "HT_AVG_ENT",
                "attribute": "HT_CONT_ENT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT_CAT",
                "label": "HT",
                "attribute": "HT_CAT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "MAT_DATE_CONT",
                "label": "Maturity date",
                "attribute": "MAT_DATE_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT12_CONT",
                "label": "Ht12",
                "attribute": "HT12_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "BB_SES5_GH_SCOR_1_9",
                "label": "BB GH 1-9 - SES5",
                "attribute": "BB_SES5_GH_SCOR_1_9",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "FLW_DATE_CONT",
                "label": "FLW DATE",
                "attribute": "FLW_DATE_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "LG_SCOR_1_9",
                "label": "Lg 1-9",
                "attribute": "LG_SCOR_1_9",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT2_CONT",
                "label": "Ht2",
                "attribute": "HT2_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "GRAIN_COLOR",
                "label": "Grain Color",
                "attribute": "GRAIN_COLOR",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "FLW_CONT",
                "label": "FLW",
                "attribute": "FLW_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT9_CONT",
                "label": "HT9",
                "attribute": "HT9_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT8_CONT",
                "label": "HT8",
                "attribute": "HT8_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PNL6_CONT",
                "label": "PnL 6",
                "attribute": "PNL6_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT6_CONT",
                "label": "Ht6",
                "attribute": "HT6_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "NO_OF_EARS",
                "label": "Number of Ears Selected",
                "attribute": "NO_OF_EARS",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "MC_CONT",
                "label": "MC",
                "attribute": "MC_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "SPECIFIC_PLANT",
                "label": "Specific plant number selected",
                "attribute": "SPECIFIC_PLANT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "NO_OF_PLANTS",
                "label": "NO OF PLANTS SELECTED",
                "attribute": "NO_OF_PLANTS",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "CML6_CONT",
                "label": "CL6",
                "attribute": "CML6_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT10_CONT",
                "label": "HT10",
                "attribute": "HT10_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT7_CONT",
                "label": "HT7",
                "attribute": "HT7_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT5_CONT",
                "label": "Ht5",
                "attribute": "HT5_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "PANNO_SEL",
                "label": "PANSEL",
                "attribute": "PANNO_SEL",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT3_CONT",
                "label": "Ht3",
                "attribute": "HT3_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HETEROTIC_GROUP",
                "label": "Heterotic Group",
                "attribute": "HETEROTIC_GROUP",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HT1_CONT",
                "label": "Ht1",
                "attribute": "HT1_CONT",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            },
            {
                "abbrev": "HV_METH_DISC",
                "label": "HARV METH",
                "attribute": "HV_METH_DISC",
                "required": "false",
                "is_selected": "false",
                "should_retrieve_data_value": "true"
            }                
        ]
    }$$,
    usage = 'application'
WHERE
    abbrev = 'CB_ROLE_COLLABORATOR_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback     "experiment":[
--rollback         {
--rollback             "abbrev": "EXPERIMENT_CODE",
--rollback             "label": "Experiment Code",
--rollback             "attribute": "experimentCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_NAME",
--rollback             "label": "Experiment Name",
--rollback             "attribute": "experimentName",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_YEAR",
--rollback             "label": "Experiment Year",
--rollback             "attribute": "experimentYear",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SEASON",
--rollback             "label": "Season",
--rollback             "attribute": "experimentSeason",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "STAGE_CODE",
--rollback             "label": "Stage Code",
--rollback             "attribute": "experimentStageCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PROJECT",
--rollback             "label": "Project",
--rollback             "attribute": "experimentProject",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PIPELINE",
--rollback             "label": "Pipeline",
--rollback             "attribute": "experimentPipeline",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_OBJECTIVE",
--rollback             "label": "Experiment Objective",
--rollback             "attribute": "experimentObjective",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_TYPE",
--rollback             "label": "Experiment Type",
--rollback             "attribute": "experimentType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_SUB_TYPE",
--rollback             "label": "Experiment Subtype",
--rollback             "attribute": "experimentSubtype",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_DESIGN_TYPE",
--rollback             "label": "Experiment Design Type",
--rollback             "attribute": "experimentDesignType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_STATUS",
--rollback             "label": "Experiment Status",
--rollback             "attribute": "experimentStatus",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "EXPERIMENT_STEWARD",
--rollback             "label": "Experiment Steward",
--rollback             "attribute": "experimentSteward",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLANTING_SEASON",
--rollback             "label": "Planting Season",
--rollback             "attribute": "plantingSeason",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "MARKET_SEGMENT",
--rollback             "label": "Market Segment",
--rollback             "attribute": "marketSegment",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ECOSYSTEM",
--rollback             "label": "Ecosystem",
--rollback             "attribute": "ecosystem",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "REMARKS",
--rollback             "label": "Remarks",
--rollback             "attribute": "experimentRemarks",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "occurrence":[
--rollback         {
--rollback             "abbrev": "OCCURRENCE_CODE",
--rollback             "label": "Occurrence Code",
--rollback             "attribute": "occurrenceCode",
--rollback             "required": "true",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "OCCURRENCE_NAME",
--rollback             "label": "Occurrence Name",
--rollback             "attribute": "occurrenceName",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "OCCURRENCE_NUMBER",
--rollback             "label": "Occurrence Number",
--rollback             "attribute": "occurrenceCount",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SITE_CODE",
--rollback             "label": "Site Code",
--rollback             "attribute": "siteCode",
--rollback             "required": "true",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SITE",
--rollback             "label": "Site Name",
--rollback             "attribute": "siteName",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "LOCATION_CODE",
--rollback             "label": "Location Code",
--rollback             "attribute": "locationCode",
--rollback             "required": "true",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "LOCATION_NAME",
--rollback             "label": "Location Name",
--rollback             "attribute": "locationName",
--rollback             "required": "true",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLANTING_DATE",
--rollback             "label": "Planting Date",
--rollback             "attribute": "plantingDate",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GEOSPATIAL_COORDS",
--rollback             "label": "Geospatial Coordinates",
--rollback             "attribute": "geospatialCoordinates",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "LONGITUDE",
--rollback             "label": "Longitude",
--rollback             "attribute": "longitude",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "LATITUDE",
--rollback             "label": "Latitude",
--rollback             "attribute": "latitude",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ECOSYSTEM",
--rollback             "label": "Ecosystem",
--rollback             "attribute": "ecosystem",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "DESCRIPTION",
--rollback             "label": "Description",
--rollback             "attribute": "description",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "CONTCT_PERSON_CONT",
--rollback             "label": "Contact Person",
--rollback             "attribute": "contactPerson",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "planting_protocol":[
--rollback         {
--rollback             "abbrev": "ESTABLISHMENT",
--rollback             "label": "Crop Establishment",
--rollback             "attribute": "cropEstablishment",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLANTING_TYPE",
--rollback             "label": "Planting Type",
--rollback             "attribute": "plantingType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_TYPE",
--rollback             "label": "Plot Type",
--rollback             "attribute": "plotType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_LN",
--rollback             "label": "Plot Length",
--rollback             "attribute": "plotLength",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_WIDTH",
--rollback             "label": "Plot Width",
--rollback             "attribute": "plotWidth",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_AREA_SQM_CONT",
--rollback             "label": "Plot Area sqm.",
--rollback             "attribute": "plotAreaSQM",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ALLEY_LENGTH",
--rollback             "label": "Alley Length",
--rollback             "attribute": "alleyLength",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "TRANS_DATE_CONT",
--rollback             "label": "Transplanting Date",
--rollback             "attribute": "transplantingDate",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SEEDING_RATE",
--rollback             "label": "Seeding Density",
--rollback             "attribute": "seedingRate",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLANTING_INSTRUCTIONS",
--rollback             "label": "Planting Instructions",
--rollback             "attribute": "plantingInstructions",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "management_protocol":[
--rollback         {
--rollback             "abbrev": "DIST_BET_ROWS",
--rollback             "label": "Distance Between Rows",
--rollback             "attribute": "distBetRows",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ROWS_PER_PLOT_CONT",
--rollback             "label": "Rows per Plot",
--rollback             "attribute": "rowsPerPlot",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SEEDING_DATE_CONT",
--rollback             "label": "Seeding Date",
--rollback             "attribute": "seedingDate",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT1_TYPE_DISC",
--rollback             "label": "Fertilizer Type - 1st Application",
--rollback             "attribute": "fert1Type",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT1_DATE_CONT",
--rollback             "label": "Fertilizer Date - 1st Application",
--rollback             "attribute": "fert1Date",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT1_BRAND",
--rollback             "label": "Fertilizer Brand - 1st Application",
--rollback             "attribute": "fert1Brand",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT1_METH_DISC",
--rollback             "label": "Fertilizer Method - 1st Application",
--rollback             "attribute": "fert1Method",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT2_TYPE_DISC",
--rollback             "label": "Fertilizer Type - 2nd Application",
--rollback             "attribute": "fert2Type",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT2_DATE_CONT",
--rollback             "label": "Fertilizer Date - 2nd Application",
--rollback             "attribute": "fert2Date",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT2_BRAND",
--rollback             "label": "Fertilizer Brand - 2nd Application",
--rollback             "attribute": "fert2Brand",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT2_METH_DISC",
--rollback             "label": "Fertilizer Method - 2nd Application",
--rollback             "attribute": "fert2Method",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT3_TYPE_DISC",
--rollback             "label": "Fertilizer Type - 3rd Application",
--rollback             "attribute": "fert3Type",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT3_DATE_CONT",
--rollback             "label": "Fertilizer Date - 3rd Application",
--rollback             "attribute": "fert3Date",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT3_BRAND",
--rollback             "label": "Fertilizer Brand - 3rd Application",
--rollback             "attribute": "fert3Brand",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FERT3_METH_DISC",
--rollback             "label": "Fertilizer Method - 3rd Application",
--rollback             "attribute": "fert3Method",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "entry":[
--rollback         {
--rollback             "abbrev": "ENTRY_NUMBER",
--rollback             "label": "Entry Number",
--rollback             "attribute": "entryNumber",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ENTRY_CODE",
--rollback             "label": "Entry Code",
--rollback             "attribute": "entryCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ENTRY_TYPE",
--rollback             "label": "Entry Type",
--rollback             "attribute": "entryType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ENTRY_CLASS",
--rollback             "label": "Entry Class",
--rollback             "attribute": "entryClass",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ENTRY_ROLE",
--rollback             "label": "Entry Role",
--rollback             "attribute": "entryRole",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "plot":[
--rollback         {
--rollback             "abbrev": "PLOTNO",
--rollback             "label": "Plot Number",
--rollback             "attribute": "plotNumber",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_CODE",
--rollback             "label": "Plot Code",
--rollback             "attribute": "plotCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PLOT_TYPE",
--rollback             "label": "Plot Type",
--rollback             "attribute": "plotType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "REP",
--rollback             "label": "Replication",
--rollback             "attribute": "rep",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "NO_OF_BLOCKS",
--rollback             "label": "Blocks",
--rollback             "attribute": "blockCount",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PA_X",
--rollback             "label": "PA X",
--rollback             "attribute": "paX",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PA_Y",
--rollback             "label": "PA Y",
--rollback             "attribute": "paY",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "germplasm":[
--rollback         {
--rollback             "abbrev": "GERMPLASM_CODE",
--rollback             "label": "Germplasm Code",
--rollback             "attribute": "germplasmCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_NAME",
--rollback             "label": "Germplasm Name",
--rollback             "attribute": "germplasmName",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_STATE",
--rollback             "label": "Germplasm State",
--rollback             "attribute": "germplasmState",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "TAXONOMY_NAME",
--rollback             "label": "Taxonomy Name",
--rollback             "attribute": "taxonomyName",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_CODE",
--rollback             "label": "Parent 1 Germplasm Code",
--rollback             "attribute": "p1GermplasmCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_CODE",
--rollback             "label": "Parent 2 Germplasm Code",
--rollback             "attribute": "p2GermplasmCode",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_TYPE",
--rollback             "label": "Germplasm Type",
--rollback             "attribute": "germplasmType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GERMPLASM_NAME_TYPE",
--rollback             "label": "Germplasm Name Type",
--rollback             "attribute": "germplasmNameType",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         }
--rollback     ],
--rollback     "trait":[
--rollback         {
--rollback             "abbrev": "BL_NURS_0_9",
--rollback             "label": "Bl1 0-9",
--rollback             "attribute": "BL_NURS_0_9",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "YLD_CONT2",
--rollback             "label": "YLD_CONT2",
--rollback             "attribute": "YLD_CONT2",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "YLD_CONT_TON",
--rollback             "label": "YLD_TON",
--rollback             "attribute": "YLD_CONT_TON",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "TIL_AVE_CONT",
--rollback             "label": "TILLER_AVG",
--rollback             "attribute": "TIL_AVE_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "NO_OF_SEED",
--rollback             "label": "NO OF SEED",
--rollback             "attribute": "NO_OF_SEED",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PACP_SCOR_1_9",
--rollback             "label": "Pacp 1-9",
--rollback             "attribute": "PACP_SCOR_1_9",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FLW50",
--rollback             "label": "FLW50",
--rollback             "attribute": "FLW50",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "DATE_CROSSED",
--rollback             "label": "CROSSING DATE",
--rollback             "attribute": "DATE_CROSSED",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "TRANSFORMATION_DATE",
--rollback             "label": "TRANSFORMATION DATE",
--rollback             "attribute": "TRANSFORMATION_DATE",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HVDATE_CONT",
--rollback             "label": "HARVEST DATE",
--rollback             "attribute": "HVDATE_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "RLODGINC_CT_PLNTPLOT",
--rollback             "label": "RLODGINC CT PLNTPLOT",
--rollback             "attribute": "RLODGINC_CT_PLNTPLOT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PSTANDHV_CT_PLNTPLOT",
--rollback             "label": "PSTANDHV CT PLNTPLOT",
--rollback             "attribute": "PSTANDHV_CT_PLNTPLOT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HVHILL_CONT",
--rollback             "label": "HV HILL",
--rollback             "attribute": "HVHILL_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "TOTAL_HILL_CONT",
--rollback             "label": "TOTAL HILLS",
--rollback             "attribute": "TOTAL_HILL_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PSTANDTH_CT_PLNTPLOT",
--rollback             "label": "PSTANDTH CT PLNTPLOT",
--rollback             "attribute": "PSTANDTH_CT_PLNTPLOT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "RLODGINC_CMP_PCT",
--rollback             "label": "RLODGINC CMP PCT",
--rollback             "attribute": "RLODGINC_CMP_PCT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "ASI_TRAIT",
--rollback             "label": "ASI Trait",
--rollback             "attribute": "ASI_TRAIT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT4_CONT",
--rollback             "label": "Ht4",
--rollback             "attribute": "HT4_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "CML5_CONT",
--rollback             "label": "CL5",
--rollback             "attribute": "CML5_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "AYLD_CONT",
--rollback             "label": "AYLD_G",
--rollback             "attribute": "AYLD_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT11_CONT",
--rollback             "label": "Ht11",
--rollback             "attribute": "HT11_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT_CONT",
--rollback             "label": "HT_AVG",
--rollback             "attribute": "HT_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PNL5_CONT",
--rollback             "label": "PnL 5",
--rollback             "attribute": "PNL5_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT_CONT_ENT",
--rollback             "label": "HT_AVG_ENT",
--rollback             "attribute": "HT_CONT_ENT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT_CAT",
--rollback             "label": "HT",
--rollback             "attribute": "HT_CAT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "MAT_DATE_CONT",
--rollback             "label": "Maturity date",
--rollback             "attribute": "MAT_DATE_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT12_CONT",
--rollback             "label": "Ht12",
--rollback             "attribute": "HT12_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "BB_SES5_GH_SCOR_1_9",
--rollback             "label": "BB GH 1-9 - SES5",
--rollback             "attribute": "BB_SES5_GH_SCOR_1_9",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FLW_DATE_CONT",
--rollback             "label": "FLW DATE",
--rollback             "attribute": "FLW_DATE_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "LG_SCOR_1_9",
--rollback             "label": "Lg 1-9",
--rollback             "attribute": "LG_SCOR_1_9",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT2_CONT",
--rollback             "label": "Ht2",
--rollback             "attribute": "HT2_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "GRAIN_COLOR",
--rollback             "label": "Grain Color",
--rollback             "attribute": "GRAIN_COLOR",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "FLW_CONT",
--rollback             "label": "FLW",
--rollback             "attribute": "FLW_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT9_CONT",
--rollback             "label": "HT9",
--rollback             "attribute": "HT9_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT8_CONT",
--rollback             "label": "HT8",
--rollback             "attribute": "HT8_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PNL6_CONT",
--rollback             "label": "PnL 6",
--rollback             "attribute": "PNL6_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT6_CONT",
--rollback             "label": "Ht6",
--rollback             "attribute": "HT6_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "NO_OF_EARS",
--rollback             "label": "Number of Ears Selected",
--rollback             "attribute": "NO_OF_EARS",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "MC_CONT",
--rollback             "label": "MC",
--rollback             "attribute": "MC_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "SPECIFIC_PLANT",
--rollback             "label": "Specific plant number selected",
--rollback             "attribute": "SPECIFIC_PLANT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "NO_OF_PLANTS",
--rollback             "label": "NO OF PLANTS SELECTED",
--rollback             "attribute": "NO_OF_PLANTS",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "CML6_CONT",
--rollback             "label": "CL6",
--rollback             "attribute": "CML6_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT10_CONT",
--rollback             "label": "HT10",
--rollback             "attribute": "HT10_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT7_CONT",
--rollback             "label": "HT7",
--rollback             "attribute": "HT7_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT5_CONT",
--rollback             "label": "Ht5",
--rollback             "attribute": "HT5_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "PANNO_SEL",
--rollback             "label": "PANSEL",
--rollback             "attribute": "PANNO_SEL",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT3_CONT",
--rollback             "label": "Ht3",
--rollback             "attribute": "HT3_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HETEROTIC_GROUP",
--rollback             "label": "Heterotic Group",
--rollback             "attribute": "HETEROTIC_GROUP",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HT1_CONT",
--rollback             "label": "Ht1",
--rollback             "attribute": "HT1_CONT",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         },
--rollback         {
--rollback             "abbrev": "HV_METH_DISC",
--rollback             "label": "HARV METH",
--rollback             "attribute": "HV_METH_DISC",
--rollback             "required": "false",
--rollback             "is_selected": "false"
--rollback         } 
--rollback     ]
--rollback }$$
--rollback WHERE
--rollback     abbrev = 'CB_ROLE_COLLABORATOR_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';