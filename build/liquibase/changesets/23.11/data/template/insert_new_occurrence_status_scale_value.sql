--liquibase formatted sql

--changeset postgres:insert_new_occurrence_status_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6607 CB DB: Add new Occurrence status "Upload Occurrence Data in Progress"



WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'OCCURRENCE_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('upload occurrence data in progress', 'Upload Occurrence Data in Progress', 'Upload Occurrence Data in Progress', 'UPLOAD_OCCURRENCE_DATA_IN_PROGRESS', 'show'),
        ('upload occurrence data failed', 'Upload Occurrence Data Failed', 'Upload Occurrence Data Failed', 'UPLOAD_OCCURRENCE_DATA_FAILED', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



--rollback DELETE FROM master.scale_value
--rollback WHERE abbrev in (
--rollback 	'OCCURRENCE_STATUS_UPLOAD_OCCURRENCE_DATA_IN_PROGRESS', 'OCCURRENCE_STATUS_UPLOAD_OCCURRENCE_DATA_FAILED'
--rollback );