--liquibase formatted sql

--changeset postgres:curate_variable_definition context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3075 CB-DB: Curate CB variable definitions

UPDATE 
  master.scale ms
SET 
  type = null,
  notes = CONCAT(ms.notes, ' (DEVOPS-3075 rollback)')
FROM 
	master.scale s
	left join master.scale_value sv 
	on s.id = sv.scale_id 
	and s.is_void = false 
	and sv.is_void = false 
WHERE 
	s.is_void  = false
	and s.type = 'categorical' 
	and sv.value is null;



--rolllback UPDATE 
--rolllback    master.scale
--rolllback SET 
--rolllback    type = 'categorical'
--rolllback WHERE
--rolllback notes 
--rolllback LIKE
--rolllback '%DEVOPS-3075 rollback%';