--liquibase formatted sql

--changeset postgres:add_config_hm_browser_config_wheat_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-100 Add config HM_BROWSER_CONFIG_WHEAT for cross browser to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'HM_BROWSER_CONFIG_WHEAT',
        'Harvest Manager Browser Config for Wheat',
    $$
        {
            "name": "HM_BROWSER_CONFIG_WHEAT",
            "values": [
                {
                    "plot": {
                        "germplasm_state": {
                            "fixed": {
                                "harvest_method": [
                                    "Bulk"
                                ],
                                "display_column": [
                                    "harvestDate",
                                    "harvestMethod"
                                ]
                            },
                            "not_fixed": {
                                "harvest_method": [
                                    "Bulk",
                                    "Selected bulk",
                                    "Single Plant Selection",
                                    "Individual spike"
                                ],
                                "display_column": [
                                    "harvestDate",
                                    "harvestMethod",
                                    "numericVar"
                                ]
                            }
                        }
                    },
                    "cross": {
                        "cross_method": {
                            "CROSS_METHOD_SINGLE_CROSS": {
                                "harvest_method": [
                                    "Bulk"
                                ]
                            }
                        },
                        "display_column": [
                            "harvestDate",
                            "harvestMethod"
                        ]
                    }
                }
            ]
        }
    $$,
        1,
        'harvest_manager',
        1,
        'created wheat browser configuration'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_WHEAT';