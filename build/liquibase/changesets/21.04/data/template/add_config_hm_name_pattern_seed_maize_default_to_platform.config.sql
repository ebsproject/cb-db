--liquibase formatted sql

--changeset postgres:add_config_hm_name_pattern_seed_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-274 Add config HM_NAME_PATTERN_SEED_MAIZE_DEFAULT to platform.config



INSERT INTO
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'HM_NAME_PATTERN_SEED_MAIZE_DEFAULT',
        'Harvest Manager Maize Seed Name Pattern-Default V2',
        $$
        {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": {
                        "additional_condition": []
                    }
                }
            },
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "germplasm",
                            "field_name": "designation",
                            "order_number": 0
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            }
        }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-996 - j.bantay 2021-04-18'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_SEED_MAIZE_DEFAULT';