--liquibase formatted sql

--changeset postgres:update_config_hm_name_pattern_seed_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Update config HM_NAME_PATTERN_SEED_MAIZE_DEFAULT to platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
    {
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            }
        },
        "PLOT": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "germplasm",
                            "field_name": "designation",
                            "order_number": 0
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "same_nursery": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 8
                            }
                        ],
                        "different_nurseries": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentSeasonCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 5
                            },
                            {
                                "type": "field",
                                "entity": "femaleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 6
                            },
                            {
                                "type": "delimiter",
                                "value": "/",
                                "order_number": 7
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "sourceExperimentName",
                                "order_number": 8
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 9
                            },
                            {
                                "type": "field",
                                "entity": "maleCrossParent",
                                "field_name": "entryNumber",
                                "order_number": 10
                            }
                        ]
                    }
                }
            }
        }
    }
    '
WHERE 
    abbrev = 'HM_NAME_PATTERN_SEED_MAIZE_DEFAULT';



-- rollback
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback     {
--rollback         "default": {
--rollback             "default": {
--rollback                 "default": [
--rollback                     {
--rollback                         "type": "field",
--rollback                         "entity": "germplasm",
--rollback                         "field_name": "designation",
--rollback                         "order_number": 0
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         },
--rollback         "cross_method": {
--rollback             "germplasm_state/germplasm_type": {
--rollback                 "harvest_method": {
--rollback                     "additional_condition": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "single cross": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "same_nursery": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "originSiteCode",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "experimentYearYY",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "experimentSeasonCode",
--rollback                             "order_number": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "femaleCrossParent",
--rollback                             "field_name": "sourceExperimentName",
--rollback                             "order_number": 4
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 5
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "femaleCrossParent",
--rollback                             "field_name": "entryNumber",
--rollback                             "order_number": 6
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "/",
--rollback                             "order_number": 7
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "maleCrossParent",
--rollback                             "field_name": "entryNumber",
--rollback                             "order_number": 8
--rollback                         }
--rollback                     ],
--rollback                     "different_nurseries": [
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "originSiteCode",
--rollback                             "order_number": 0
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "experimentYearYY",
--rollback                             "order_number": 1
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "cross",
--rollback                             "field_name": "experimentSeasonCode",
--rollback                             "order_number": 2
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 3
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "femaleCrossParent",
--rollback                             "field_name": "sourceExperimentName",
--rollback                             "order_number": 4
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 5
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "femaleCrossParent",
--rollback                             "field_name": "entryNumber",
--rollback                             "order_number": 6
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "/",
--rollback                             "order_number": 7
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "maleCrossParent",
--rollback                             "field_name": "sourceExperimentName",
--rollback                             "order_number": 8
--rollback                         },
--rollback                         {
--rollback                             "type": "delimiter",
--rollback                             "value": "-",
--rollback                             "order_number": 9
--rollback                         },
--rollback                         {
--rollback                             "type": "field",
--rollback                             "entity": "maleCrossParent",
--rollback                             "field_name": "entryNumber",
--rollback                             "order_number": 10
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HM_NAME_PATTERN_SEED_MAIZE_DEFAULT';