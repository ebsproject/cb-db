--liquibase formatted sql

--changeset postgres:add_config_hm_name_pattern_germplasm_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Add config HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT to platform.config



DELETE FROM platform.config WHERE abbrev='HM_GERMPLASM_NAME_PATTERN_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT',
        'Harvest Manager Rice Germplasm Name Pattern-Default',
        $$					
        {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            },
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                },
                "progeny": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B",
                            "order_number": 2
                        }
                    ],
                    "plant-specific": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "panicle selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    "single seed descent": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "free-text",
                            "value": "B RGA",
                            "order_number": 2
                        }
                    ],
                    "single plant selection": [
                        {
                            "type": "field",
                            "entity": "plot",
                            "field_name": "germplasmDesignation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ],
                    "bulk": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ]
                }
            }
            
        }
        $$,
                1,
                'harvest_manager',
                1,
                'CORB-983 - j.bantay 2021-04-15'
    );



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_GERMPLASM_RICE_DEFAULT';