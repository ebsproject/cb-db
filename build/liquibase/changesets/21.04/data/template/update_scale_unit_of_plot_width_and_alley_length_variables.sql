--liquibase formatted sql

--changeset postgres:update_scale_unit_of_plot_width_and_alley_length_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-68 Update scale unit of PLOT_WIDTH and ALLEY_LENGTH variables



-- update scale unit to m for ALLEY_LENGTH and PLOT WIDTH
UPDATE master.SCALE SET unit = 'm' WHERE abbrev IN ('ALLEY_LENGTH','PLOT_WIDTH');



-- rollback
--rollback UPDATE master.SCALE SET unit = NULL WHERE abbrev IN ('ALLEY_LENGTH','PLOT_WIDTH');