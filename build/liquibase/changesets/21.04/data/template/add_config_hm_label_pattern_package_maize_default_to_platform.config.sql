--liquibase formatted sql

--changeset postgres:add_config_hm_label_pattern_package_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-273 Add config HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT to platform.config



INSERT INTO
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id,notes)
VALUES
    (
        'HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT',
        'Harvest Manager Maize Package Label Pattern-Default V2',
    $$
        {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": {
                        "additional_condition": []
                    }
                }
            },
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "germplasm",
                            "field_name": "designation",
                            "order_number": 0
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "seed",
                            "field_name": "seedName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "leading_zero": "yes",
                            "max_digits": 3,
                            "order_number": 2
                        }
                    ]
                }
            }
        }
    $$,
        1,
        'harvest_manager',
        1,
        'CORB-996 - j.bantay 2021-04-18'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT';