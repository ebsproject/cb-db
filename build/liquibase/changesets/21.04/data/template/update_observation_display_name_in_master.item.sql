--liquibase formatted sql

--changeset postgres:update_observation_display_name_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Update observation display name in master.item



UPDATE 
    master.item 
SET 
    display_name = 'Observation Experiment' 
WHERE  
    abbrev = 'OBSERVATION_DATA_PROCESS';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     display_name = 'Breeding Trial Experiment' 
--rollback WHERE  
--rollback     abbrev = 'OBSERVATION_DATA_PROCESS';