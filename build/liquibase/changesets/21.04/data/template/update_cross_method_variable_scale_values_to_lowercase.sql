--liquibase formatted sql

--changeset postgres:update_cross_method_variable_scale_values_to_lowercase context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-148 Update CROSS_METHOD variable scale values to lowercase



UPDATE master.scale_value SET value = LOWER(value) WHERE scale_id = 616;
UPDATE master.scale_value SET display_name = LOWER(value) WHERE scale_id = 616;



-- rollback
--rollback UPDATE master.scale_value SET value = UPPER(value) WHERE scale_id = 616;
--rollback UPDATE master.scale_value SET display_name = NULL WHERE scale_id = 616;