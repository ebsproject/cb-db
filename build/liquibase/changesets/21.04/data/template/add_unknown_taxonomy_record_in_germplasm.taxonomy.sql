--liquibase formatted sql

--changeset postgres:add_unknown_taxonomy_record_in_germplasm.taxonomy context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-272 Add Unknown taxonomy record in germplasm.taxonomy



INSERT INTO
    germplasm.taxonomy
        (taxon_id,taxonomy_name,creator_id)
VALUES
    ('unknown','Unknown',1);



-- rollback
--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = 'unknown';