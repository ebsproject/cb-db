--liquibase formatted sql

--changeset postgres:update_config_hm_label_pattern_package_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Update config HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT to platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
    {
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            }
        },
        "PLOT": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "germplasm",
                            "field_name": "designation",
                            "order_number": 0
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "cross",
                            "field_name": "crossName",
                            "order_number": 0
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": [
                        {
                            "type": "field",
                            "entity": "seed",
                            "field_name": "seedName",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "leading_zero": "yes",
                            "max_digits": 3,
                            "order_number": 2
                        }
                    ]
                }
            }
        }
    }
    '
WHERE 
    abbrev = 'HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT';



-- rollback
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback     {
--rollback         "default": {
--rollback             "default": {
--rollback                 "default": [
--rollback                     {
--rollback                         "type": "field",
--rollback                         "entity": "germplasm",
--rollback                         "field_name": "designation",
--rollback                         "order_number": 0
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         },
--rollback         "cross_method": {
--rollback             "germplasm_state/germplasm_type": {
--rollback                 "harvest_method": {
--rollback                     "additional_condition": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "single cross": {
--rollback             "default": {
--rollback                 "default": [
--rollback                     {
--rollback                         "type": "field",
--rollback                         "entity": "seed",
--rollback                         "field_name": "seedName",
--rollback                         "order_number": 0
--rollback                     },
--rollback                     {
--rollback                         "type": "delimiter",
--rollback                         "value": "-",
--rollback                         "order_number": 1
--rollback                     },
--rollback                     {
--rollback                         "type": "counter",
--rollback                         "max_digits": 3,
--rollback                         "leading_zero": "yes",
--rollback                         "order_number": 2
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HM_LABEL_PATTERN_PACKAGE_MAIZE_DEFAULT';