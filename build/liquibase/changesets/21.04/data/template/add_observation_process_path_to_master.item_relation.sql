--liquibase formatted sql

--changeset postgres:add_observation_process_path_to_master.item_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add observation process paths to master.item_relation



INSERT INTO 
    master.item_relation 
        (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'OBSERVATION_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'OBSERVATION_DATA_PROCESS') AS parent_id,
    id AS child_id,
    CASE
        WHEN abbrev = 'OBSERVATION_BASIC_INFO_ACT' THEN 1
        WHEN abbrev = 'OBSERVATION_ENTRY_LIST_ACT'THEN 2
        WHEN abbrev = 'OBSERVATION_DESIGN_ACT'THEN 3
        WHEN abbrev = 'OBSERVATION_PROTOCOL_ACT'THEN 4
        WHEN abbrev = 'OBSERVATION_PLACE_ACT'THEN 5
        WHEN abbrev = 'OBSERVATION_REVIEW_ACT'THEN 6
        ELSE 7 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev IN ('OBSERVATION_BASIC_INFO_ACT','OBSERVATION_ENTRY_LIST_ACT','OBSERVATION_DESIGN_ACT','OBSERVATION_PROTOCOL_ACT', 'OBSERVATION_PLACE_ACT', 'OBSERVATION_REVIEW_ACT');

INSERT INTO 
    master.item_relation 
        (root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT
    (SELECT id FROM master.item WHERE abbrev = 'OBSERVATION_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev = 'OBSERVATION_PROTOCOL_ACT') AS parent_id,
    id AS child_id,
  CASE
        WHEN abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT' THEN 1
        WHEN abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT'THEN 2
        WHEN abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT'THEN 3
        WHEN abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT'THEN 4
        WHEN abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT'THEN 5
        ELSE 6 END AS order_number,
    0,
    1,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev IN ('OBSERVATION_PLANTING_PROTOCOL_ACT','OBSERVATION_POLLINATION_PROTOCOL_ACT','OBSERVATION_TRAITS_PROTOCOL_ACT','OBSERVATION_MANAGEMENT_PROTOCOL_ACT','OBSERVATION_HARVEST_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback     master.item_relation 
--rollback WHERE
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='OBSERVATION_DATA_PROCESS'
--rollback     )
--rollback AND 
--rollback     parent_id 
--rollback IN
--rollback     (
--rollback          SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='OBSERVATION_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE
--rollback 	        abbrev 
--rollback         IN 
--rollback         (
--rollback             'OBSERVATION_BASIC_INFO_ACT',
--rollback             'OBSERVATION_ENTRY_LIST_ACT',
--rollback             'OBSERVATION_DESIGN_ACT',
--rollback             'OBSERVATION_PROTOCOL_ACT',
--rollback             'OBSERVATION_PLACE_ACT',
--rollback             'OBSERVATION_REVIEW_ACT'
--rollback         )
--rollback );

--rollback DELETE FROM
--rollback     master.item_relation 
--rollback WHERE
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='OBSERVATION_DATA_PROCESS'
--rollback     )
--rollback AND 
--rollback     parent_id 
--rollback IN
--rollback     (
--rollback          SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE 
--rollback             abbrev='OBSERVATION_PROTOCOL_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id 
--rollback         FROM 
--rollback             master.item 
--rollback         WHERE
--rollback 	        abbrev 
--rollback         IN 
--rollback         (
--rollback             'OBSERVATION_PLANTING_PROTOCOL_ACT',
--rollback             'OBSERVATION_POLLINATION_PROTOCOL_ACT',
--rollback             'OBSERVATION_TRAITS_PROTOCOL_ACT',
--rollback             'OBSERVATION_MANAGEMENT_PROTOCOL_ACT',
--rollback             'OBSERVATION_HARVEST_PROTOCOL_ACT'
--rollback         )
--rollback );