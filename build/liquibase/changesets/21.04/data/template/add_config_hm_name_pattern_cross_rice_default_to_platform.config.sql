--liquibase formatted sql

--changeset postgres:add_config_hm_name_pattern_cross_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Add config HM_NAME_PATTERN_CROSS_RICE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_CROSS_RICE_DEFAULT',
        'Harvest Manager Rice Cross Name Pattern-Default',
        $$			   
        {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            },
            "default": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR XXXXX",
                            "order_number": 0
                        }
                    ]
                }
            },
            "single cross": {
                "default": {
                    "default": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "sequence_name": "cross_name_seq",
                            "order_number": 1
                        }
                    ],
                    "bulk": [
                        {
                            "type": "free-text",
                            "value": "IR ",
                            "order_number": 0
                        },
                        {
                            "type": "db-sequence",
                            "schema": "germplasm",
                            "sequence_name": "cross_number_seq",
                            "order_number": 1
                        }
                    ]
                }
            }
            
        }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-983 - j.bantay 2021-04-15'
    );



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_CROSS_RICE_DEFAULT';