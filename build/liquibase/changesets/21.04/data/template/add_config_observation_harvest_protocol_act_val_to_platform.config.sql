--liquibase formatted sql

--changeset postgres:add_config_observation_harvest_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add config OBSERVATION_HARVEST_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'OBSERVATION_HARVEST_PROTOCOL_ACT_VAL', 
        'Harvest Protocol', 
        '{
            "Name": "Required experiment level harvest protocol variables for Observation data process",
            "Values": [{
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "HV_METH_DISC"
                },{
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "HARVEST_INSTRUCTIONS"
                }
            ]
        }'::json, 1, 'experiment_creation', 1,'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev='OBSERVATION_HARVEST_PROTOCOL_ACT_VAL';