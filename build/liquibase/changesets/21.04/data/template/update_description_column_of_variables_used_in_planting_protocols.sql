--liquibase formatted sql

--changeset postgres:update_description_column_of_variables_used_in_planting_protocols context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-46 Update description columns of variables used in planting protocol



UPDATE master.variable SET description = 'The length of plot derived from computation and was used in a study.' WHERE abbrev = 'PLOT_LN';
UPDATE master.variable SET description = 'Measure of the volume of seeds, in grams, per linear meter.' WHERE abbrev = 'SEEDING_RATE';



-- rollback
--rollback UPDATE master.variable SET description = 'The length of plot derived from computation and was used in a study.' WHERE abbrev = 'PLOT_LN';
--rollback UPDATE master.variable SET description = 'The seed showing rate like sparse, normal, dense' WHERE abbrev = 'SEEDING_RATE';