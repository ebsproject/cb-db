--liquibase formatted sql

--changeset postgres:add_observation_process_to_platform.item_module context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add observation process to platform.item_module



INSERT INTO 
    platform.item_module
        (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
        WHEN abbrev = 'OBSERVATION_BASIC_INFO_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_BASIC_INFO_ACT_MOD')
        WHEN abbrev = 'OBSERVATION_ENTRY_LIST_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_ENTRY_LIST_ACT_MOD')
        WHEN abbrev = 'OBSERVATION_DESIGN_ACT' THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_DESIGN_ACT_MOD')
        WHEN abbrev = 'OBSERVATION_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_PROTOCOL_ACT_MOD')
        WHEN abbrev = 'OBSERVATION_PLACE_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_PLACE_ACT_MOD')
        WHEN abbrev = 'OBSERVATION_REVIEW_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_REVIEW_ACT_MOD')
        ELSE (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_REVIEW_ACT_MOD') END AS module_id,
    1,
    'added by l.gallardo ' || now()
FROM
    master.item
WHERE
    abbrev IN ('OBSERVATION_BASIC_INFO_ACT','OBSERVATION_ENTRY_LIST_ACT','OBSERVATION_DESIGN_ACT', 'OBSERVATION_PROTOCOL_ACT', 'OBSERVATION_PLACE_ACT', 'OBSERVATION_REVIEW_ACT');

INSERT INTO 
    platform.item_module
        (item_id,module_id,creator_id,notes)
SELECT
    id AS item_id,
    CASE
    WHEN abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_PLANTING_PROTOCOL_ACT_MOD')
    WHEN abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD')
    WHEN abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_TRAITS_PROTOCOL_ACT_MOD')
    WHEN abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD')
    WHEN abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT'THEN (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD')
    ELSE (SELECT id FROM platform.module WHERE abbrev = 'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD') END AS module_id,
    1,
    'added by j.antonio ' || now()
FROM
    master.item
WHERE
    abbrev 
IN 
    ('OBSERVATION_PLANTING_PROTOCOL_ACT','OBSERVATION_POLLINATION_PROTOCOL_ACT','OBSERVATION_TRAITS_PROTOCOL_ACT','OBSERVATION_MANAGEMENT_PROTOCOL_ACT','OBSERVATION_HARVEST_PROTOCOL_ACT');



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'OBSERVATION_BASIC_INFO_ACT',
--rollback         'OBSERVATION_ENTRY_LIST_ACT',
--rollback         'OBSERVATION_DESIGN_ACT',
--rollback         'OBSERVATION_PROTOCOL_ACT',
--rollback         'OBSERVATION_PLACE_ACT',
--rollback         'OBSERVATION_REVIEW_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'OBSERVATION_BASIC_INFO_ACT_MOD',
--rollback         'OBSERVATION_ENTRY_LIST_ACT_MOD',
--rollback         'OBSERVATION_DESIGN_ACT_MOD',
--rollback         'OBSERVATION_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_PLACE_ACT_MOD',
--rollback         'OBSERVATION_REVIEW_ACT_MOD'
--rollback 	    )
--rollback   );

--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'OBSERVATION_PLANTING_PROTOCOL_ACT',
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT',
--rollback         'OBSERVATION_TRAITS_PROTOCOL_ACT',
--rollback         'OBSERVATION_MANAGEMENT_PROTOCOL_ACT',
--rollback         'OBSERVATION_HARVEST_PROTOCOL_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'OBSERVATION_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD'
--rollback 	    )
--rollback   );