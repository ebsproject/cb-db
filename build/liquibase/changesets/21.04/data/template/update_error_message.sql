--liquibase formatted sql

--changeset postgres:update_error_message context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-245 Update error message



UPDATE
    api.messages set message = 'There is at least one(1) plot that does not exist in the location.'
WHERE
    id 
IN
    (SELECT id FROM api.messages m WHERE message ='There is at least one(1) plot that do not exist in the location.');



--rollback UPDATE
--rollback     api.messages set message = 'There is at least one(1) plot that do not exist in the location.'
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (SELECT id FROM api.messages m WHERE message ='There is at least one(1) plot that does not exist in the location.');