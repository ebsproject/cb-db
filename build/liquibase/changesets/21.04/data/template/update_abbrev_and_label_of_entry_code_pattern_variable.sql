--liquibase formatted sql

--changeset postgres:update_abbrev_and_label_of_entry_code_pattern_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-209 Update abbrev and label of ENTRY_CODE_PATTERN variable



-- update abbrev and label of ENTRY_CODE_PATTERN
UPDATE
    master.variable AS var
SET
    abbrev = 'ENTRY_CODE_PATTERN',
    label = 'ENTRY CODE PATTERN'
WHERE
    var.abbrev = 'ENTCODE_PATTERN'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     abbrev = 'ENTCODE_PATTERN',
--rollback     label = 'ENTCODE PATTERN'
--rollback WHERE
--rollback     var.abbrev = 'ENTRY_CODE_PATTERN'
--rollback ;
