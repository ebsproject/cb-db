--liquibase formatted sql

--changeset postgres:add_undefined_record_to_tenant.stage context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-229 Add undefined record to tenant.stage



INSERT INTO
    tenant.stage
        (stage_code,stage_name,description,creator_id)
VALUES
    (
        'UND',
        'Undefined',
        'Undefined',
        1
    );



--rollback DELETE FROM tenant.stage WHERE stage_code='UND';