--liquibase formatted sql

--changeset postgres:delete_unlinked_variable_id_from_master.variable_set_member context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 DELETE unlinked variable_id from master.variable_set_member



DELETE FROM 
    master.variable_set_member
WHERE 
    variable_id=2361;



--rollback INSERT INTO 
--rollback     master.variable_set_member
--rollback         (id,variable_set_id,variable_id,order_number,creator_id)
--rollback VALUES
--rollback     (
--rollback         18771,
--rollback         396,
--rollback         2361,
--rollback         6,
--rollback         1
--rollback     );