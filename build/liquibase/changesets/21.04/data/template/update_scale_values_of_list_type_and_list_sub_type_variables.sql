--liquibase formatted sql

--changeset postgres:update_scale_values_of_list_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-189 Update LIST_TYPE scale values



-- update LIST_TYPE scale values
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('LIST_TYPE_TRAIT', 'trait', 1, 'Trait', 'Trait', 1),
        ('LIST_TYPE_GERMPLASM', 'germplasm', 2, 'Germplasm', 'Germplasm', 1),
        ('LIST_TYPE_SEED', 'seed', 3, 'Seed', 'Seed', 1),
        ('LIST_TYPE_PACKAGE', 'package', 4, 'Package', 'Package', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id
    )
WHERE
    var.abbrev = 'LIST_TYPE'
;


-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('LIST_TYPE_TRAIT', 'LIST_TYPE_GERMPLASM', 'LIST_TYPE_SEED', 'LIST_TYPE_PACKAGE')
--rollback ;



--changeset postgres:update_scale_values_of_list_sub_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-189 Update LIST_SUB_TYPE scale values



-- update LIST_SUB_TYPE scale values
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('LIST_SUB_TYPE_TRAIT_PROTOCOL', 'trait protocol', 5, 'Trait protocol', 'Trait protocol', 1),
        ('LIST_SUB_TYPE_MANAGEMENT_PROTOCOL', 'management protocol', 6, 'Management protocol', 'Management protocol', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id
    )
WHERE
    var.abbrev = 'LIST_SUB_TYPE'
;

UPDATE
    master.scale_value
SET
    value = lower(value)
WHERE
    abbrev IN ('LIST_SUB_TYPE_ENTRY_LIST', 'LIST_SUB_TYPE_POPULATION_LIST', 'LIST_SUB_TYPE_SAMPLE_LIST', 'LIST_SUB_TYPE_SHIPMENT_LIST')
;


-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('LIST_SUB_TYPE_TRAIT_PROTOCOL', 'LIST_SUB_TYPE_MANAGEMENT_PROTOCOL')
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value AS sval
--rollback SET
--rollback     value = sval2.old_value
--rollback FROM
--rollback     (
--rollback         VALUES
--rollback         ('LIST_SUB_TYPE_ENTRY_LIST', 'Entry list'),
--rollback         ('LIST_SUB_TYPE_POPULATION_LIST', 'Population list'),
--rollback         ('LIST_SUB_TYPE_SAMPLE_LIST', 'Sample list'),
--rollback         ('LIST_SUB_TYPE_SHIPMENT_LIST', 'Shipment list')
--rollback     ) AS sval2 (
--rollback         abbrev, old_value
--rollback     )
--rollback WHERE
--rollback     sval.abbrev = sval2.abbrev
--rollback ;
