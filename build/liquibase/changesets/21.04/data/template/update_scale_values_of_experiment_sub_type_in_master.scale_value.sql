--liquibase formatted sql

--changeset postgres:update_scale_values_of_experiment_sub_type_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-219 Update scale values of experiment sub type in master.scale_value



INSERT INTO 
    master.scale_value 
        (scale_id, value, order_number, description, display_name,abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
        'Screening Nursery',
        10,
        'Screening Nursery',
        'Screening Nursery',
        'EXPERIMENT_SUB_TYPE_SN'
    ),
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
        'International Screening Nursery',
        11,
        'International Screening Nursery',
        'International Screening Nursery',
        'EXPERIMENT_SUB_TYPE_INS'
    ),
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
        'Parcela Chica',
        12,
        'Parcela Chica',
        'Parcela Chica',
        'EXPERIMENT_SUB_TYPE_PC'
    ),
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
        'Elite Parcela Chica',
        13,
        'Elite Parcela Chica',
        'Elite Parcela Chica',
        'EXPERIMENT_SUB_TYPE_EPC'
    ),
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_SUB_TYPE'),
        'Disease Screening Nursery',
        14,
        'Disease Screening Nursery',
        'Disease Screening Nursery',
        'EXPERIMENT_SUB_TYPE_DSN'
    );
   


--rollback DELETE FROM master.scale_value WHERE abbrev IN ('EXPERIMENT_SUB_TYPE_SN','EXPERIMENT_SUB_TYPE_INS','EXPERIMENT_SUB_TYPE_PC','EXPERIMENT_SUB_TYPE_EPC','EXPERIMENT_SUB_TYPE_DSN');