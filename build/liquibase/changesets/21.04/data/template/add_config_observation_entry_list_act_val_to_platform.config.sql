--liquibase formatted sql

--changeset postgres:add_config_observation_entry_list_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add config OBSERVATION_ENTRY_LIST_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'OBSERVATION_ENTRY_LIST_ACT_VAL', 
        'Observation Experiment Entry List','
        {
            "Name": "Required and default entry level metadata variables for Observation data process",
            "Values": [{
                    "default": "entry",
                    "allowed_values": [
                        "ENTRY_TYPE_CHECK",
                        "ENTRY_TYPE_ENTRY"
                    ],
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_TYPE",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "entries",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : "identification"
                }
            ]
        }
        '::json, 1, 'experiment_creation', 1,'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev='OBSERVATION_ENTRY_LIST_ACT_VAL';