--liquibase formatted sql

--changeset postgres:add_data_level_of_cross_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-256 Data Collection : Add data_level of cross in master.variable for DATE_CROSSED, HV_METH_DISC, NO_OF_SEED, and HVDATE_CONT



UPDATE
    master.variable 
SET
    data_level = CONCAT(data_level, ',cross')
WHERE
    abbrev
IN
    ('DATE_CROSSED', 'HV_METH_DISC', 'NO_OF_SEED', 'HVDATE_CONT')
;



-- rollback
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = TRIM(data_level, ',cross')
--rollback WHERE
--rollback     abbrev
--rollback IN 
--rollback     ('DATE_CROSSED', 'HV_METH_DISC', 'NO_OF_SEED', 'HVDATE_CONT')
--rollback ;