--liquibase formatted sql

--changeset postgres:add_scale_values_to_entry_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-188 Add scale values to ENTRY_TYPE variable



-- add scale values to ENTRY_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('ENTRY_TYPE_TEST', 'test', 7, 'Test', 'Test', 1, 'hide')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'ENTRY_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('ENTRY_TYPE_TEST')
--rollback ;
