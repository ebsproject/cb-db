--liquibase formatted sql

--changeset postgres:add_config_observation_pollination_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add config OBSERVATION_POLLINATION_PROTOCOL_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
   (
        'OBSERVATION_POLLINATION_PROTOCOL_ACT_VAL', 
        'Observation Pollination Protocol variables', 
        '{
        "Name": "Required experiment level pollination protocol variables for Observation data process",
        "Values": [
            {
                "default": false,
                "disabled": false,
                "variable_abbrev": "POLLINATION_INSTRUCTIONS"
            }
        ]
    }'::json, 1, 'experiment_creation', 1,'added by j.antonio');



--rollback DELETE FROM platform.config WHERE abbrev='OBSERVATION_POLLINATION_PROTOCOL_ACT_VAL';