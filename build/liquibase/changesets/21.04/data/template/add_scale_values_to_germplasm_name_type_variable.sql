--liquibase formatted sql

--changeset postgres:add_scale_values_to_germplasm_name_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-233 Add scale values to GERMPLASM_NAME_TYPE variable



-- add scale values to GERMPLASM_NAME_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('GERMPLASM_NAME_TYPE_CROSS_ABBREVIATION', 'cross_abbreviation', 34, 'cross_abbreviation', 'cross_abbreviation', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id
    )
WHERE
    var.abbrev = 'GERMPLASM_NAME_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('GERMPLASM_NAME_TYPE_CROSS_ABBREVIATION')
--rollback ;
