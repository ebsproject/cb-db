--liquibase formatted sql

--changeset postgres:update_config_hm_name_pattern_seed_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Update config HM_NAME_PATTERN_SEED_RICE_DEFAULT to platform.config
--validCheckSum: 8:4058121ca998df075fe87c1f57253495


UPDATE
    platform.config
SET
    config_value =
    '
    {
        "harvest_mode": {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            }
        },
        "PLOT": {
            "default": {
                "default" : {
                    "default" : [
                        {
                            "type" : "field",
                            "entity" : "germplasm",
                            "field_name" : "designation",
                            "order_number":0
                        }
                    ]
                }
            }
        },
        "CROSS": {
            "default": {
                "default" : {
                    "default" : [
                        {
                            "type" : "field",
                            "entity" : "cross",
                            "field_name" : "crossName",
                            "order_number":0
                        }
                    ]
                }
            },
            "single cross": {
                "default" : {
                    "default" : [
                        {
                            "type" : "db-sequence",
                            "schema": "germplasm",
                            "sequence_name": "gid_seq",
                            "order_number": 0
                        }
                    ]
                }
            }
        }
    }
    '
WHERE abbrev = 'HM_NAME_PATTERN_SEED_RICE_DEFAULT';



-- rollback
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value =
--rollback     '
--rollback     {
--rollback         "default": {
--rollback             "default": {
--rollback                 "default": [
--rollback                     {
--rollback                         "type": "field",
--rollback                         "entity": "germplasm",
--rollback                         "field_name": "designation",
--rollback                         "order_number": 0
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         },
--rollback         "cross_method": {
--rollback             "germplasm_state/germplasm_type": {
--rollback                 "harvest_method": [
--rollback                     {
--rollback                         "type": "free-text",
--rollback                         "value": "ABC",
--rollback                         "order_number": 0
--rollback                     },
--rollback                     {
--rollback                         "type": "field",
--rollback                         "entity": "<entity>",
--rollback                         "field_name": "<field_name>",
--rollback                         "order_number": 1
--rollback                     },
--rollback                     {
--rollback                         "type": "delimiter",
--rollback                         "value": "-",
--rollback                         "order_number": 1
--rollback                     },
--rollback                     {
--rollback                         "type": "counter",
--rollback                         "order_number": 3
--rollback                     },
--rollback                     {
--rollback                         "type": "db-sequence",
--rollback                         "schema": "<schema>",
--rollback                         "order_number": 4,
--rollback                         "sequence_name": "<sequence_name>"
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         },
--rollback         "single cross": {
--rollback             "default": {
--rollback                 "default": [
--rollback                     {
--rollback                         "type": "db-sequence",
--rollback                         "schema": "germplasm",
--rollback                         "order_number": 0,
--rollback                         "sequence_name": "gid_seq"
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE abbrev = 'HM_NAME_PATTERN_SEED_RICE_DEFAULT';