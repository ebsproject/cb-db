--liquibase formatted sql

--changeset postgres:add_observation_process_path_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add observation process paths to master.item



INSERT INTO
    master.item
        (abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage)
VALUES
    ('OBSERVATION_DATA_PROCESS','Observation',40,'Are experiment objectives such as yield, disease, physiological, etc.','Breeding Trial Experiment',1,'experiment_creation_data_process','active','fa fa-th-list','experiment_creation');
    
INSERT INTO
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    ('OBSERVATION_BASIC_INFO_ACT','Specify Basic Information',30,'Specify Basic Information','Basic',1,'active','fa fa-file-text'),
    ('OBSERVATION_ENTRY_LIST_ACT','Specify Entry List',30,'Specify Entry List','Entry List',1,'active','fa fa-list-ol'),
    ('OBSERVATION_DESIGN_ACT','Planting Arrangement',30,'Planting Arrangement','Planting Arrangement',1,'active','fa fa-files-o'),
    ('OBSERVATION_ADD_BLOCKS_ACT','Add blocks',20,'Add blocks','Add blocks',1,'active','fa fa-random'),
    ('OBSERVATION_ASSIGN_ENTRIES_ACT','Assign entries',20,'Assign entries','Assign entries',1,'active','fa fa-random'),
    ('OBSERVATION_MANAGE_BLOCKS_ACT','Manage blocks',20,'Manage blocks','Manage blocks',1,'active','fa fa-random'),
    ('OBSERVATION_OVERVIEW_ACT','Overview',20,'Overview','Overview',1,'active','fa fa-random'),
    ('OBSERVATION_PROTOCOL_ACT','Specify Protocol',30,'Specify Protocol','Protocol',1,'active','fa fa-map-marker'),
    ('OBSERVATION_PLANTING_PROTOCOL_ACT','Planting Protocol',20,'Planting Protocol','Planting',1,'active','fa fa-pagelines'),
    ('OBSERVATION_POLLINATION_PROTOCOL_ACT','Pollination Protocol',20,'Pollination Protocol','Pollination',1,'active','fa fa-crosshairs'),
    ('OBSERVATION_TRAITS_PROTOCOL_ACT','Traits Protocol',20,'Traits Protocol','Traits',1,'active','fa fa-braille'),
    ('OBSERVATION_MANAGEMENT_PROTOCOL_ACT','Management Protocol',20,'Management Protocol','Management',1,'active','fa fa-tags'),
    ('OBSERVATION_HARVEST_PROTOCOL_ACT','Harvest Protocol',20,'Harvest Protocol','Harvest',1,'active','fa fa-pied-piper'),
    ('OBSERVATION_PLACE_ACT','Specify Site',30,'Specify Site','Site',1,'active','fa fa-map-marker'),
    ('OBSERVATION_REVIEW_ACT','Review',30,'Review','Review',1,'active','fa fa-files-o');



--rollback DELETE FROM
--rollback     master.item
--rollback WHERE
--rollback     abbrev 
--rollback IN   
--rollback     (
--rollback         'OBSERVATION_DATA_PROCESS',
--rollback         'OBSERVATION_BASIC_INFO_ACT',
--rollback         'OBSERVATION_ENTRY_LIST_ACT',
--rollback         'OBSERVATION_DESIGN_ACT',
--rollback         'OBSERVATION_ADD_BLOCKS_ACT',
--rollback         'OBSERVATION_ASSIGN_ENTRIES_ACT',
--rollback         'OBSERVATION_MANAGE_BLOCKS_ACT',
--rollback         'OBSERVATION_OVERVIEW_ACT',
--rollback         'OBSERVATION_PROTOCOL_ACT',
--rollback         'OBSERVATION_PLANTING_PROTOCOL_ACT',
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT',
--rollback         'OBSERVATION_TRAITS_PROTOCOL_ACT',
--rollback         'OBSERVATION_MANAGEMENT_PROTOCOL_ACT',
--rollback         'OBSERVATION_HARVEST_PROTOCOL_ACT',
--rollback         'OBSERVATION_PLACE_ACT',
--rollback         'OBSERVATION_REVIEW_ACT'
--rollback     );