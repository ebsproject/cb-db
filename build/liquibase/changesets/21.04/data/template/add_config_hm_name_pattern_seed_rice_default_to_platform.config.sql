--liquibase formatted sql

--changeset postgres:add_config_hm_name_pattern_seed_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-124 Add config HM_NAME_PATTERN_SEED_RICE_DEFAULT to platform.config



DELETE FROM platform.config WHERE abbrev='HM_SEED_NAME_PATTERN_RICE_DEFAULT';

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NAME_PATTERN_SEED_RICE_DEFAULT',
        'Harvest Manager Rice Seed Name Pattern-Default',
        $$			       
        {
            "cross_method": {
                "germplasm_state/germplasm_type": {
                    "harvest_method": [
                        {
                            "type": "free-text",
                            "value": "ABC",
                            "order_number": 0
                        },
                        {
                            "type": "field",
                            "entity": "<entity>",
                            "field_name": "<field_name>",
                            "order_number": 1
                        },
                        {
                            "type": "delimiter",
                            "value": "-",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 3
                        },
                        {
                            "type": "db-sequence",
                            "schema": "<schema>",
                            "sequence_name": "<sequence_name>",
                            "order_number": 4
                        }
                    ]
                }
            },
            "default": {
                "default" : {
                    "default" : [
                        {
                            "type" : "field",
                            "entity" : "germplasm",
                            "field_name" : "designation",
                            "order_number":0
                        }
                    ]
                }
            },
            "single cross": {
                "default" : {
                    "default" : [
                        {
                            "type" : "db-sequence",
                            "schema": "germplasm",
                            "sequence_name": "gid_seq",
                            "order_number": 0
                        }
                    ]
                }
            }
        }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-983 - j.bantay 2021-04-15'
        );



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_SEED_RICE_DEFAULT';