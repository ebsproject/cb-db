--liquibase formatted sql

--changeset postgres:add_observation_process_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add observation process to platform.module



INSERT INTO 
    platform.module 
        (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes, required_status)
VALUES
    ('OBSERVATION_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
    ('OBSERVATION_ENTRY_LIST_ACT_MOD','Specify Entry List','Specify Entry List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
    ('OBSERVATION_DESIGN_ACT_MOD','Planting Arrangement','Planting Arrangement','experimentCreation','create','specify-cross-design',1,'added by j.antonio ' || now(), 'design generated'),
    ('OBSERVATION_PROTOCOL_ACT_MOD','Protocol','Protocol','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_PLANTING_PROTOCOL_ACT_MOD','Planting Protocol','Planting Protocol','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD','Pollination Protocol','Pollination Protocol','experimentCreation','protocol','pollination-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_TRAITS_PROTOCOL_ACT_MOD','Traits Protocol','Traits Protocol','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD','Management Protocol','Management Protocol','experimentCreation','protocol','management-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_HARVEST_PROTOCOL_ACT_MOD','Harvest Protocol','Harvest Protocol','experimentCreation','protocol','harvest-protocol',1,'added by j.antonio ' || now(), 'protocol specified'),
    ('OBSERVATION_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'occurrences created'),
    ('OBSERVATION_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'created');



--rollback DELETE FROM 
--rollback     platform.module 
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'OBSERVATION_BASIC_INFO_ACT_MOD',
--rollback         'OBSERVATION_ENTRY_LIST_ACT_MOD',
--rollback         'OBSERVATION_DESIGN_ACT_MOD',
--rollback         'OBSERVATION_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_PLANTING_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_TRAITS_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_MANAGEMENT_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_HARVEST_PROTOCOL_ACT_MOD',
--rollback         'OBSERVATION_PLACE_ACT_MOD',
--rollback         'OBSERVATION_REVIEW_ACT_MOD'
--rollback     );