--liquibase formatted sql

--changeset postgres:update_config_cross_parent_nursery_phase_i_planting_protocol_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Update config CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL to platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
       {
            "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase I data process",
            "Values": [
                {
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "ESTABLISHMENT"
                },
                {
                    "default": false,
                    "disabled": false,
                    "variable_abbrev": "PLANTING_TYPE"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "PLOT_TYPE"
                }
            ]
        }
    ' 
WHERE 
    abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level protocol variables for Cross Parent Nursery Phase I data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "ESTABLISHMENT"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "PLANTING_TYPE"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "variable_abbrev": "PLOT_TYPE"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'CROSS_PARENT_NURSERY_PHASE_I_PLANTING_PROTOCOL_ACT_VAL';