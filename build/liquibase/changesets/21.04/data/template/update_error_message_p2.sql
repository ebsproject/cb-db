--liquibase formatted sql

--changeset postgres:update_error_message_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-250 Update error message p2



UPDATE
  api.messages 
SET 
    message = 'There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot, cross'
WHERE
  id 
IN 
    (SELECT id FROM api.messages m WHERE message ='There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot');



--rollback UPDATE
--rollback     api.messages set message = 'There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot'
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (SELECT id FROM api.messages m WHERE message ='There is at least one(1) invalid record in occurrences. The dataUnit should be one of the following: plot, cross');