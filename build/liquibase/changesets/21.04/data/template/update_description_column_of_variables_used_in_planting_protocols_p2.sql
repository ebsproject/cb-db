--liquibase formatted sql

--changeset postgres:update_description_column_of_variables_used_in_planting_protocols_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-46 Update description columns of variables used in planting protocol p2



UPDATE master.variable SET description = 'Plot size in square meters derived from computation based on plot length and plot width' WHERE abbrev IN ('PLOT_AREA_1', 'PLOT_AREA_2', 'PLOT_AREA_3', 'PLOT_AREA_4');



-- rollback
--rollback UPDATE master.variable SET description = 'Plot size in square meters' WHERE abbrev IN ('PLOT_AREA_1', 'PLOT_AREA_2', 'PLOT_AREA_3', 'PLOT_AREA_4');