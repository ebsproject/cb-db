--liquibase formatted sql

--changeset postgres:add_config_hm_browser_config_rice_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-100 Add config HM_BROWSER_CONFIG_RICE for cross browser to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'HM_BROWSER_CONFIG_RICE',
        'Harvest Manager Browser Config for Rice',
    $$
        {
            "name": "HM_BROWSER_CONFIG_RICE",
            "values": [
                {
                    "plot": {
                        "germplasm_state": {
                            "fixed": {
                                "harvest_method": [
                                    "Bulk"
                                ],
                                "display_column": [
                                    "harvestDate",
                                    "harvestMethod"
                                ]
                            },
                            "not_fixed": {
                                "harvest_method": [
                                    "Bulk",
                                    "Panicle Selection",
                                    "Single Plant Selection",
                                    "Single Plant Selection and Bulk",
                                    "plant-specific",
                                    "Plant-Specific and Bulk",
                                    "Single Seed Descent"
                                ],
                                "display_column": [
                                    "harvestDate",
                                    "harvestMethod",
                                    "numericVar"
                                ]
                            }
                        }
                    },
                    "cross": {
                        "cross_method": {
                            "CROSS_METHOD_SINGLE_CROSS": {
                                "harvest_method": [
                                    "Bulk"
                                ]
                            },
                            "CROSS_METHOD_THREE_WAY_CROSS": {
                                "harvest_method": [
                                    "Bulk"
                                ]
                            },
                            "CROSS_METHOD_DOUBLE_CROSS": {
                                "harvest_method": [
                                    "Bulk"
                                ]
                            },
                            "CROSS_METHOD_COMPLEX_CROSS": {
                                "harvest_method": [
                                    "Bulk"
                                ]
                            }
                        },
                        "display_column": [
                            "harvestDate",
                            "harvestMethod",
                            "numericVar"
                        ]
                    }
                }
            ]
        }
    $$,
        1,
        'harvest_manager',
        1,
        'created rice browser config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_BROWSER_CONFIG_RICE';