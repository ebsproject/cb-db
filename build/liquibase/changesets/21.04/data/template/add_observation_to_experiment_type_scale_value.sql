--liquibase formatted sql

--changeset postgres:add_observation_to_experiment_type_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-207 Add observation to experiment type scale value



INSERT INTO 
    master.scale_value 
        (scale_id, value, order_number, description, display_name,abbrev) 
VALUES 
    (
        (SELECT scale_id FROM master.variable WHERE abbrev='EXPERIMENT_TYPE'),
        'Observation',
        5,
        'Observation',
        'Observation',
        'EXPERIMENT_TYPE_OBSERVATION'
    );



--rollback DELETE FROM master.scale_value WHERE abbrev = 'EXPERIMENT_TYPE_OBSERVATION';