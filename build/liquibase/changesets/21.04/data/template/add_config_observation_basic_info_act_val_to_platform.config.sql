--liquibase formatted sql

--changeset postgres:add_config_observation_basic_info_act_val_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-237 Add config OBSERVATION_BASIC_INFO_ACT_VAL to platform.config



INSERT INTO 
    platform.config
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES 
    (
        'OBSERVATION_BASIC_INFO_ACT_VAL', 'Observation Experiment Basic Information', 
        '{
        "Name": "Required experiment level metadata variables for Observation Trial data process",
        "Values": [
            {
                "default": false,
                "disabled": true,
                "required": "required",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_TYPE"
            },
            {
                "default": "RICE",
                "disabled": true,
                "required": "required",
                "target_column": "cropDbId",
                "target_value" : "cropCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "crops",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=cropCode",
                "variable_type" : "identification",
                "variable_abbrev": "CROP"
            },
            {
                "disabled": true,
                "required": "required",
                "target_column": "programDbId",
                "target_value":"programCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "programs",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=programCode",
                "variable_type" : "identification",
                "variable_abbrev": "PROGRAM"
            },
            {
                "default" : "EXPXXXXX",
                "disabled": true,
                "required": "required",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_CODE"
            },
            {
                "disabled": false,
                "required": "required",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_NAME"
            },
            {
                "disabled": false,
                "required": "required",
                "allowed_values": [
                    "UND"
                ],
                "target_column": "stageDbId",
                "target_value":"stageCode",
                "api_resource_method": "POST",
                "api_resource_endpoint": "stages-search",
                "api_resource_sort": "sort=stageCode",
                "api_resource_filter" : "",
                "variable_type" : "identification",
                "variable_abbrev": "STAGE"
            },
            {
                "disabled": false,
                "required": "required",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_YEAR"
            },
            {
                "disabled": false,
                "required": "required",
                "target_column": "seasonDbId",
                "target_value":"seasonCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "seasons",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=seasonCode",
                "variable_type" : "identification",
                "variable_abbrev": "SEASON"
            },
            {
                "disabled": false,
                "required": "required",
                "target_column": "stewardDbId",
                "secondary_target_column": "personDbId",
                "target_value":"personName",
                "api_resource_method": "GET",
                "api_resource_endpoint": "persons",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=personName",
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_STEWARD"
            },
            {
                "disabled": false,
                "target_column": "pipelineDbId",
                "target_value":"pipelineCode",
                "api_resource_method" : "GET",
                "api_resource_endpoint" : "pipelines",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=pipelineCode",
                "variable_type" : "identification",
                "variable_abbrev": "PIPELINE"
            },
            {
                "disabled": false,
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_OBJECTIVE"
            },
            {
                "disabled": false,
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "PLANTING_SEASON"
            },
            {
                "disabled": false,
                "target_column": "projectDbId",
                "target_value": "projectCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "projects",
                "api_resource_filter" : "",
                "api_resource_sort": "sort=projectCode",
                "variable_type" : "identification",
                "variable_abbrev": "PROJECT"
            },
            {
                "disabled": false,
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"value",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "allowed_values": [
                "Disease Screening Nursery",
                "Elite Parcela Chica",
                "International Screening Nursery",
                "Parcela Chica",
                "Screening Nursery"
                ],
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_SUB_TYPE"
            },
            {
                "disabled": false,
                "allowed_values": [
                    "First Year",
                    "Second Year",
                    "Re Test",
                    "Germplasm Viability"
                ],
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"value",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification",
                "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
            },
            {
                "disabled": false,
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint" : "",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type": "identification",
                "variable_abbrev": "DESCRIPTION"
            }
        ]
    }
    '::json, 1, 'experiment_creation', 1,'added by j.antonio'
);



--rollback DELETE FROM platform.config WHERE abbrev='OBSERVATION_BASIC_INFO_ACT_VAL';