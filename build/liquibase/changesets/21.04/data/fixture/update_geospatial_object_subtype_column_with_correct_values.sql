--liquibase formatted sql

--changeset postgres:update_geospatial_object_subtype_column_with_correct_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-265 Update geospatial_object_subtype column with correct values



UPDATE
	place.geospatial_object
SET
	geospatial_object_subtype = 'breeding location'
WHERE
	geospatial_object_subtype
IN
	('beeding_location', 'breeding_location');



-- rollback
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'breeding_location'
--rollback WHERE
--rollback     geospatial_object_subtype = 'breeding location'
--rollback AND
--rollback     geospatial_object_code
--rollback IN
--rollback     ('GEO000000000017','GEO000000000018','TL','AF','HA','HY','NJ','MXI');
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'beeding_location'
--rollback WHERE
--rollback     geospatial_object_subtype = 'breeding location'
--rollback AND
--rollback     geospatial_object_code
--rollback IN
--rollback     (
--rollback         'CO',
--rollback         'EB',
--rollback         'TO',
--rollback         'KI',
--rollback         'GEO000000000001',
--rollback         'GEO000000000002',
--rollback         'GEO000000000003',
--rollback         'GEO000000000004',
--rollback         'GEO000000000005',
--rollback         'GEO000000000006',
--rollback         'GEO000000000007',
--rollback         'GEO000000000008',
--rollback         'GEO000000000009',
--rollback         'GEO000000000010',
--rollback         'GEO000000000011',
--rollback         'GEO000000000012',
--rollback         'GEO000000000013',
--rollback         'GEO000000000014',
--rollback         'GEO000000000015',
--rollback         'GEO000000000016'
--rollback     )
--rollback ;