--liquibase formatted sql

--changeset postgres:update_cross_method_in_cross_table_with_correct_scale_values context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-48 Update cross_method in cross table with correct scale values



-- update single cross 
UPDATE germplasm.cross SET cross_method = 'single cross' WHERE cross_method = 'SIMPLE CROSS';
UPDATE germplasm.cross SET cross_method = 'selfing' WHERE cross_method = 'SELFING';



-- rollback
--rollback UPDATE germplasm.cross SET cross_method = 'SELFING' WHERE cross_method = 'selfing';
--rollback UPDATE germplasm.cross SET cross_method = 'SIMPLE CROSS' WHERE cross_method = 'single cross' AND experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id IN (102,103));