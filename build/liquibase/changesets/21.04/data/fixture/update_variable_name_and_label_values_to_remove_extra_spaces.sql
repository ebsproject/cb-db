--liquibase formatted sql

--changeset postgres:update_variable_name_and_label_values_to_remove_extra_spaces context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-138 Update variable name and label values to remove extra spaces



-- replace multi-spaces to single spaces
UPDATE master.variable SET name = trim(regexp_replace(name, '\s+', ' ', 'g')) WHERE name ILIKE '%  %';
UPDATE master.variable SET label = trim(regexp_replace(name, '\s+', ' ', 'g')) WHERE label ILIKE '%  %';



-- rollback
--rollback UPDATE master.variable SET name = 'panicle length  of sample 1' WHERE abbrev = 'PNL1_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 2' WHERE abbrev = 'PNL2_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 3' WHERE abbrev = 'PNL3_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 4' WHERE abbrev = 'PNL4_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 5' WHERE abbrev = 'PNL5_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 7' WHERE abbrev = 'PNL7_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 8' WHERE abbrev = 'PNL8_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 9' WHERE abbrev = 'PNL9_CONT';
--rollback UPDATE master.variable SET name = 'Starting  Column Coordinate' WHERE abbrev = 'START_COORD_COLUMN';
--rollback UPDATE master.variable SET name = 'Specific Plant number  selected' WHERE abbrev = 'SPECIFIC_PLANT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 10' WHERE abbrev = 'PNL10_CONT';
--rollback UPDATE master.variable SET name = 'Actual  plot yield in kilograms' WHERE abbrev = 'AYLD_KG_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 6' WHERE abbrev = 'PNL6_CONT';
--rollback UPDATE master.variable SET label = 'Tensiometer  Data' WHERE abbrev = 'TENSIOMETER_OBSERVATION';
--rollback UPDATE master.variable SET label = 'Specific plant number  selected' WHERE abbrev = 'SPECIFIC_PLANT';



--changeset postgres:reapply_update_variable_name_and_label_values_to_remove_extra_spaces context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-285 Re-apply update variable name and label values to remove extra spaces



-- replace multi-spaces to single spaces
UPDATE master.variable SET name = trim(regexp_replace(name, '\s+', ' ', 'g')) WHERE name ILIKE '%  %';
UPDATE master.variable SET label = trim(regexp_replace(name, '\s+', ' ', 'g')) WHERE label ILIKE '%  %';



-- rollback
--rollback UPDATE master.variable SET name = 'panicle length  of sample 1' WHERE abbrev = 'PNL1_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 2' WHERE abbrev = 'PNL2_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 3' WHERE abbrev = 'PNL3_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 4' WHERE abbrev = 'PNL4_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 5' WHERE abbrev = 'PNL5_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 7' WHERE abbrev = 'PNL7_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 8' WHERE abbrev = 'PNL8_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 9' WHERE abbrev = 'PNL9_CONT';
--rollback UPDATE master.variable SET name = 'Starting  Column Coordinate' WHERE abbrev = 'START_COORD_COLUMN';
--rollback UPDATE master.variable SET name = 'Specific Plant number  selected' WHERE abbrev = 'SPECIFIC_PLANT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 10' WHERE abbrev = 'PNL10_CONT';
--rollback UPDATE master.variable SET name = 'Actual  plot yield in kilograms' WHERE abbrev = 'AYLD_KG_CONT';
--rollback UPDATE master.variable SET name = 'panicle length  of sample 6' WHERE abbrev = 'PNL6_CONT';
--rollback UPDATE master.variable SET label = 'Tensiometer  Data' WHERE abbrev = 'TENSIOMETER_OBSERVATION';
--rollback UPDATE master.variable SET label = 'Specific plant number  selected' WHERE abbrev = 'SPECIFIC_PLANT';