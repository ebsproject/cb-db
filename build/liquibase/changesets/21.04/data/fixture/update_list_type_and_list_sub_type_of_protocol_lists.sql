--liquibase formatted sql

--changeset postgres:update_list_type_and_list_sub_type_of_protocol_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-189 Update list_type and list_sub_type of protocol lists



-- update list_type and list_sub_type column in platform.list table
UPDATE
    platform.list
SET
    type = 'trait',
    list_sub_type = 'trait protocol'
WHERE
    type = 'trait protocol'
;



-- revert changes
--rollback UPDATE
--rollback     platform.list
--rollback SET
--rollback     type = 'trait protocol',
--rollback     list_sub_type = null
--rollback WHERE
--rollback     type = 'trait'
--rollback     AND list_sub_type = 'trait protocol'
--rollback ;
