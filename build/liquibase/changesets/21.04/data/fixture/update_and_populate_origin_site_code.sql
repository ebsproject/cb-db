--liquibase formatted sql

--changeset postgres:update_and_populate_origin_site_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-34 Update and populate origin site code



UPDATE 
    place.geospatial_object_attribute AS t
SET
    data_value = c.dv_new
FROM
    (
        VALUES
            ('CO','Y'),
            ('EB','B'),
            ('TO','M')
    ) AS c(dv_old, dv_new)
WHERE
    data_value = c.dv_old
AND 
    variable_id = (SELECT id FROM master.variable WHERE abbrev='ORIGIN_SITE_CODE');

UPDATE place.geospatial_object SET geospatial_object_code='AFG' WHERE geospatial_object_name='Afghanistan';

INSERT INTO
    place.geospatial_object
        (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,creator_id)
VALUES
    ('TL','Tlaltizapan','site','breeding_location',1),
    ('AF','Agua Fria','site','breeding_location',1),
    ('HA','Harare','site','breeding_location',1),
    ('HY','Hyderabad','site','breeding_location',1),
    ('NJ','Njoro','site','breeding_location',1),
    ('MXI','Mexicali','site','breeding_location',1);

INSERT INTO
    place.geospatial_object_attribute
        (geospatial_object_id,variable_id,data_value,creator_id)
SELECT
    pg.id AS geospatial_object_id,
    mv.id AS variable_id,
    t.geospatial_object_code AS data_value,
     1 creator_id
FROM
    (
        VALUES
            ('TL'),
            ('AF'),
            ('HA'),
            ('HY'),
            ('NJ'),
            ('MXI')
    ) AS t (geospatial_object_code)
INNER JOIN
	place.geospatial_object pg
ON
	pg.geospatial_object_code=t.geospatial_object_code
INNER JOIN
	master.variable mv
ON
	mv.abbrev='ORIGIN_SITE_CODE'



--rollback DELETE FROM place.geospatial_object_attribute WHERE data_value IN ('TL','AF','HA','HY','NJ','MXI') AND  variable_id = (SELECT id FROM master.variable WHERE abbrev='ORIGIN_SITE_CODE');

--rollback DELETE FROM place.geospatial_object WHERE geospatial_object_code IN ('TL','AF','HA','HY','NJ','MXI');

--rollback UPDATE place.geospatial_object SET geospatial_object_code='AF' WHERE geospatial_object_name='Afghanistan';

--rollback UPDATE 
--rollback     place.geospatial_object_attribute AS t
--rollback SET
--rollback     data_value = c.dv_new
--rollback FROM
--rollback     (
--rollback         VALUES
--rollback             ('Y','CO'),
--rollback             ('B','EB'),
--rollback             ('M','TO')
--rollback     ) AS c(dv_old, dv_new)
--rollback WHERE
--rollback     data_value = c.dv_old
--rollback AND 
--rollback     variable_id = (SELECT id FROM master.variable WHERE abbrev='ORIGIN_SITE_CODE');