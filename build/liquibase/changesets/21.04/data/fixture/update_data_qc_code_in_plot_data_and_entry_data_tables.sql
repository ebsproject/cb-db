--liquibase formatted sql

--changeset postgres:update_data_qc_code_in_plot_data_and_entry_data_tables context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-42 Update data_qc_code values from N to Q in plot_data and entry_data tables



-- update plot_data data_qc_code
UPDATE experiment.plot_data SET data_qc_code = 'Q' WHERE data_qc_code = 'N';

-- update plot_data data_qc_code
UPDATE experiment.entry_data SET data_qc_code = 'Q' WHERE data_qc_code = 'N';



-- rollback
--rollback UPDATE experiment.plot_data SET data_qc_code = 'N' WHERE data_qc_code = 'Q';
--rollback UPDATE experiment.entry_data SET data_qc_code = 'N' WHERE data_qc_code = 'Q';