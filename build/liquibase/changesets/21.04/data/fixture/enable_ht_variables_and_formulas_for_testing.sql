--liquibase formatted sql

--changeset postgres:enable_ht_variables_and_formulas_for_testing context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-280 Enable HT variables and formulas for testing



-- enable HT variables
UPDATE
    master.variable
SET
    is_void = FALSE
WHERE
    abbrev IN (
        'HT1_CONT',
        'HT2_CONT',
        'HT3_CONT',
        'HT4_CONT',
        'HT5_CONT',
        'HT6_CONT',
        'HT7_CONT',
        'HT8_CONT',
        'HT9_CONT',
        'HT10_CONT',
        'HT11_CONT',
        'HT12_CONT',
        'HT_CAT',
        'HT_CONT',
        'HT_CONT_ENT',
        'PNL1_CONT',
        'PNL2_CONT',
        'PNL3_CONT',
        'PNL4_CONT',
        'PNL5_CONT',
        'PNL6_CONT',
        'CML1_CONT',
        'CML2_CONT',
        'CML3_CONT',
        'CML4_CONT',
        'CML5_CONT',
        'CML6_CONT'
    )
    AND is_void = TRUE
;

-- enable HT formulas
UPDATE
    master.formula
SET
    is_void = FALSE
WHERE
    result_variable_id IN (
        SELECT
            id
        FROM
            master.variable
        WHERE
            abbrev IN (
                'HT1_CONT',
                'HT2_CONT',
                'HT3_CONT',
                'HT4_CONT',
                'HT5_CONT',
                'HT6_CONT',
                'HT7_CONT',
                'HT8_CONT',
                'HT9_CONT',
                'HT10_CONT',
                'HT11_CONT',
                'HT12_CONT',
                'HT_CAT',
                'HT_CONT',
                'HT_CONT_ENT'
            )
    )
    AND is_void = TRUE
;


-- enable HT formula parameters
UPDATE
    master.formula_parameter
SET
    is_void = FALSE
WHERE
    formula_id IN (
        SELECT
            id
        FROM
            master.formula
        WHERE
            result_variable_id IN (
                SELECT
                    id
                FROM
                    master.variable
                WHERE
                    abbrev IN (
                        'HT1_CONT',
                        'HT2_CONT',
                        'HT3_CONT',
                        'HT4_CONT',
                        'HT5_CONT',
                        'HT6_CONT',
                        'HT7_CONT',
                        'HT8_CONT',
                        'HT9_CONT',
                        'HT10_CONT',
                        'HT11_CONT',
                        'HT12_CONT',
                        'HT_CAT',
                        'HT_CONT',
                        'HT_CONT_ENT'
                    )
            )
    )
    AND is_void = TRUE
;



-- revert changes

--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     is_void = TRUE
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HT1_CONT',
--rollback         'HT2_CONT',
--rollback         'HT3_CONT',
--rollback         'HT4_CONT',
--rollback         'HT5_CONT',
--rollback         'HT6_CONT',
--rollback         'HT7_CONT',
--rollback         'HT8_CONT',
--rollback         'HT9_CONT',
--rollback         'HT10_CONT',
--rollback         'HT11_CONT',
--rollback         'HT12_CONT',
--rollback         'HT_CAT',
--rollback         'HT_CONT',
--rollback         'HT_CONT_ENT',
--rollback         'PNL1_CONT',
--rollback         'PNL2_CONT',
--rollback         'PNL3_CONT',
--rollback         'PNL4_CONT',
--rollback         'PNL5_CONT',
--rollback         'PNL6_CONT',
--rollback         'CML1_CONT',
--rollback         'CML2_CONT',
--rollback         'CML3_CONT',
--rollback         'CML4_CONT',
--rollback         'CML5_CONT',
--rollback         'CML6_CONT'
--rollback     )
--rollback     AND is_void = FALSE
--rollback ;

--rollback UPDATE
--rollback     master.formula
--rollback SET
--rollback     is_void = TRUE
--rollback WHERE
--rollback     result_variable_id IN (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             master.variable
--rollback         WHERE
--rollback             abbrev IN (
--rollback                 'HT1_CONT',
--rollback                 'HT2_CONT',
--rollback                 'HT3_CONT',
--rollback                 'HT4_CONT',
--rollback                 'HT5_CONT',
--rollback                 'HT6_CONT',
--rollback                 'HT7_CONT',
--rollback                 'HT8_CONT',
--rollback                 'HT9_CONT',
--rollback                 'HT10_CONT',
--rollback                 'HT11_CONT',
--rollback                 'HT12_CONT',
--rollback                 'HT_CAT',
--rollback                 'HT_CONT',
--rollback                 'HT_CONT_ENT'
--rollback             )
--rollback     )
--rollback     AND is_void = FALSE
--rollback ;

--rollback UPDATE
--rollback     master.formula_parameter
--rollback SET
--rollback     is_void = TRUE
--rollback WHERE
--rollback     formula_id IN (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             master.formula
--rollback         WHERE
--rollback             result_variable_id IN (
--rollback                 SELECT
--rollback                     id
--rollback                 FROM
--rollback                     master.variable
--rollback                 WHERE
--rollback                     abbrev IN (
--rollback                         'HT1_CONT',
--rollback                         'HT2_CONT',
--rollback                         'HT3_CONT',
--rollback                         'HT4_CONT',
--rollback                         'HT5_CONT',
--rollback                         'HT6_CONT',
--rollback                         'HT7_CONT',
--rollback                         'HT8_CONT',
--rollback                         'HT9_CONT',
--rollback                         'HT10_CONT',
--rollback                         'HT11_CONT',
--rollback                         'HT12_CONT',
--rollback                         'HT_CAT',
--rollback                         'HT_CONT',
--rollback                         'HT_CONT_ENT'
--rollback                     )
--rollback             )
--rollback     )
--rollback     AND is_void = FALSE
--rollback ;
