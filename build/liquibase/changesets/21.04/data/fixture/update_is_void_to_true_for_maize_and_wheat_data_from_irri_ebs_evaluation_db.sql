--liquibase formatted sql

--changeset postgres:update_is_void_to_true_for_maize_and_wheat_data_from_irri_ebs_evaluation_db context:fixture labels:irri splitStatements:false rollbackSplitStatements:false
--comment: DB-141 Update is_void to TRUE for maize and wheat data from the current IRRI EBS evaluation database



-- update entry_data
UPDATE experiment.entry_data SET is_void = FALSE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update planting_instruction
UPDATE experiment.planting_instruction SET is_void = FALSE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update experiment_design
UPDATE experiment.experiment_design SET is_void = FALSE WHERE plot_id IN (SELECT id FROM experiment.plot WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'))));

-- update plot
UPDATE experiment.plot SET is_void = FALSE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update cross_parent
UPDATE germplasm.cross_parent SET is_void = FALSE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update entry
UPDATE experiment.entry SET is_void = FALSE WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'));

-- update entry_list
UPDATE experiment.entry_list SET is_void = FALSE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');

-- update location_occurrence_group
UPDATE experiment.location_occurrence_group SET is_void = FALSE WHERE id IN (SELECT id FROM experiment.location_occurrence_group log WHERE occurrence_id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update location
UPDATE experiment.location SET is_void = FALSE WHERE id IN (SELECT location_id FROM experiment.location_occurrence_group log WHERE occurrence_id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));

-- update occurrence
UPDATE experiment.occurrence SET is_void = FALSE WHERE id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'));

-- update cross
UPDATE germplasm.cross SET is_void = FALSE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');

-- update experiment_data
UPDATE experiment.experiment_data SET is_void = FALSE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');

-- update experiment_protocol
UPDATE experiment.experiment_protocol SET is_void = FALSE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');

-- update experiment
UPDATE experiment.experiment SET is_void = FALSE WHERE program_id <> '101';

-- update germplasm_name
UPDATE germplasm.germplasm_name SET is_void = FALSE WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1);

-- update package_id
UPDATE germplasm.package_log SET is_void = FALSE WHERE package_id IN (SELECT id FROM germplasm.package WHERE seed_id IN (SELECT id FROM germplasm.seed WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1)));

-- update seed_id
UPDATE germplasm.package SET is_void = FALSE WHERE seed_id IN (SELECT id FROM germplasm.seed WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1));

-- update seed
UPDATE germplasm.seed SET is_void = FALSE WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1);

-- update germplasm
UPDATE germplasm.germplasm SET is_void = FALSE WHERE crop_id <> 1;



-- rollback
--rollback UPDATE experiment.entry_data SET is_void = TRUE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE experiment.planting_instruction SET is_void = TRUE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE experiment.experiment_design SET is_void = TRUE WHERE plot_id IN (SELECT id FROM experiment.plot WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'))));
--rollback UPDATE experiment.plot SET is_void = TRUE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE germplasm.cross_parent SET is_void = TRUE WHERE entry_id IN (SELECT id FROM experiment.entry WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE experiment.entry SET is_void = TRUE WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'));
--rollback UPDATE experiment.entry_list SET is_void = TRUE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');
--rollback UPDATE experiment.location_occurrence_group SET is_void = TRUE WHERE id IN (SELECT id FROM experiment.location_occurrence_group log WHERE occurrence_id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE experiment.location SET is_void = TRUE WHERE id IN (SELECT location_id FROM experiment.location_occurrence_group log WHERE occurrence_id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101')));
--rollback UPDATE experiment.occurrence SET is_void = TRUE WHERE id IN (SELECT id FROM experiment.occurrence o WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101'));
--rollback UPDATE germplasm.cross SET is_void = TRUE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');
--rollback UPDATE experiment.experiment_data SET is_void = TRUE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');
--rollback UPDATE experiment.experiment_protocol SET is_void = TRUE WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE program_id <> '101');
--rollback UPDATE experiment.experiment SET is_void = TRUE WHERE program_id <> '101';
--rollback UPDATE germplasm.germplasm_name SET is_void = TRUE WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1);
--rollback UPDATE germplasm.package_log SET is_void = TRUE WHERE package_id IN (SELECT id FROM germplasm.package WHERE seed_id IN (SELECT id FROM germplasm.seed WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1)));
--rollback UPDATE germplasm.package SET is_void = TRUE WHERE seed_id IN (SELECT id FROM germplasm.seed WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1));
--rollback UPDATE germplasm.seed SET is_void = TRUE WHERE germplasm_id IN (SELECT id FROM germplasm.germplasm WHERE crop_id <> 1);
--rollback UPDATE germplasm.germplasm SET is_void = TRUE WHERE crop_id <> 1;