--liquibase formatted sql

--changeset postgres:create_cross_number_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-212 Create cross number sequence



-- create cross number sequence

CREATE SEQUENCE germplasm.cross_number_seq
    INCREMENT BY 1
    MINVALUE 1
    START 1;



-- revert changes
--rollback DROP SEQUENCE germplasm.cross_number_seq;



--changeset postgres:update_default_gid_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-212 Update default GID sequence



-- update GID sequence

ALTER SEQUENCE germplasm.gid_seq
    MINVALUE 300000000
    RESTART WITH 300000000;



-- revert changes
--rollback ALTER SEQUENCE germplasm.gid_seq
--rollback     MINVALUE 301499723
--rollback     RESTART WITH 301499723;



--changeset postgres:update_cross_number_sequence context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-212 Update cross number sequence



-- update cross number sequence

ALTER SEQUENCE germplasm.cross_number_seq
    RESTART WITH 145455;



-- revert changes
--rollback ALTER SEQUENCE germplasm.cross_number_seq
--rollback     RESTART WITH 1;



--changeset postgres:update_gid_sequence context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-212 Update GID sequence



-- update GID sequence

ALTER SEQUENCE germplasm.gid_seq
    RESTART WITH 301678462;



-- revert changes
--rollback ALTER SEQUENCE germplasm.gid_seq
--rollback     RESTART WITH 300000000;
