--liquibase formatted sql

--changeset postgres:add_harvest_status_column_to_germplasm.cross context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-220 Add harvest_status column to germplasm.cross



ALTER TABLE 
    germplasm.cross
ADD COLUMN 
    harvest_status character varying NOT NULL DEFAULT 'NO_HARVEST'::character varying;



--rollback ALTER TABLE germplasm.cross DROP COLUMN harvest_status;