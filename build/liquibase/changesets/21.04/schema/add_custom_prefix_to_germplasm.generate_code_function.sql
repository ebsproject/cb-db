--liquibase formatted sql

--changeset postgres:add_custom_prefix_to_germplasm.generate_code_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-277 Add custom prefix to germplasm.generate_code function



-- drop dependencies
ALTER TABLE germplasm.germplasm
    ALTER COLUMN germplasm_code
    DROP DEFAULT
;

-- drop old function
DROP FUNCTION germplasm.generate_code(varchar,varchar);

-- add custom prefix
CREATE OR REPLACE FUNCTION germplasm.generate_code(
    entity character varying,
    prefix character varying DEFAULT NULL::character varying,
    custom_value character varying DEFAULT NULL::character varying,
    OUT code character varying
)
    RETURNS character varying
    LANGUAGE plpgsql
AS $function$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
    
    IF (prefix IS NULL) THEN
        IF (entity = 'germplasm') THEN
            entity_code = 'GE';
        ELSIF (entity = 'seed') THEN
            entity_code = 'SEED';
        ELSIF (entity = 'package') THEN
            entity_code = 'PKG';
        END IF;
    ELSE
        entity_code = upper(regexp_replace(prefix, '\s', ''));
    END IF;
    
    IF (custom_value IS NULL) THEN
        code := entity_code || lpad(seq_id::varchar, 12, '0');
    ELSE
        code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
    END IF;
END; $function$
;

-- drop dependencies
ALTER TABLE germplasm.germplasm
    ALTER COLUMN germplasm_code
    SET DEFAULT germplasm.generate_code('germplasm')
;


-- revert changes

--rollback ALTER TABLE germplasm.germplasm
--rollback     ALTER COLUMN germplasm_code
--rollback     DROP DEFAULT
--rollback ;

--rollback DROP FUNCTION germplasm.generate_code(varchar,varchar,varchar);

--rollback CREATE OR REPLACE FUNCTION germplasm.generate_code(
--rollback     entity character varying,
--rollback     custom_value character varying DEFAULT NULL::character varying,
--rollback     OUT code character varying
--rollback )
--rollback     RETURNS character varying
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     seq_id bigint;
--rollback     entity_code varchar;
--rollback BEGIN
--rollback     SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
--rollback     
--rollback     IF (entity = 'germplasm') THEN
--rollback         entity_code = 'GE';
--rollback     ELSIF (entity = 'seed') THEN
--rollback         entity_code = 'SEED';
--rollback     ELSIF (entity = 'package') THEN
--rollback         entity_code = 'PKG';
--rollback     END IF;
--rollback     
--rollback     IF (custom_value IS NULL) THEN
--rollback         code := entity_code || lpad(seq_id::varchar, 12, '0');
--rollback     ELSE
--rollback         code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
--rollback     END IF;
--rollback END; $function$
--rollback ;

--rollback ALTER TABLE germplasm.germplasm
--rollback     ALTER COLUMN germplasm_code
--rollback     SET DEFAULT germplasm.generate_code('germplasm')
--rollback ;
