--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_of_data_value_column_in_experiment.plot_data context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-217 Remove not null constraint of data_value column in experiment.plot_data



ALTER TABLE
    experiment.plot_data 
ALTER COLUMN 
    data_value DROP NOT NULL;



--rollback ALTER TABLE experiment.plot_data ALTER COLUMN data_value SET NOT NULL;