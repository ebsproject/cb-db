--liquibase formatted sql

--changeset postgres:add_transaction_id_and_collection_timestamp_columns_to_germplasm.cross_data context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-257 Add transaction_id and collection_timestamp columns to germplasm.cross_data



ALTER TABLE germplasm.cross_data
    ADD COLUMN transaction_id integer;

COMMENT ON COLUMN germplasm.cross_data.transaction_id
    IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [CRSDATA_TXN_ID]';

ALTER TABLE germplasm.cross_data
    ADD COLUMN collection_timestamp timestamp without time zone;

COMMENT ON COLUMN germplasm.cross_data.collection_timestamp
    IS 'Collection Timestamp: Timestamp when the cross data was collected [CRSDATA_COLLTSTAMP]';



--rollback ALTER TABLE germplasm.cross_data DROP COLUMN transaction_id;
--rollback ALTER TABLE germplasm.cross_data DROP COLUMN collection_timestamp;