--liquibase formatted sql

--changeset postgres:remove_unique_constraints_in_germplasm_relation_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-275 Remove unique constraints in germplasm_relation table



-- drop unique index constraints for parent and child
ALTER TABLE germplasm.germplasm_relation
    DROP CONSTRAINT germplasm_relation_parent_germplasm_id_child_germplasm_id_idx
;

-- drop unique index constraints for parent and order number
ALTER TABLE germplasm.germplasm_relation
    DROP CONSTRAINT germplasm_relation_parent_germplasm_id_order_number_idx
;



-- revert changes
--rollback CREATE UNIQUE INDEX germplasm_relation_parent_germplasm_id_child_germplasm_id_idx
--rollback     ON germplasm.germplasm_relation
--rollback     USING btree (parent_germplasm_id, child_germplasm_id)
--rollback ;

--rollback ALTER TABLE germplasm.germplasm_relation
--rollback     ADD CONSTRAINT germplasm_relation_parent_germplasm_id_child_germplasm_id_idx
--rollback     UNIQUE USING INDEX germplasm_relation_parent_germplasm_id_child_germplasm_id_idx
--rollback ;

--rollback CREATE UNIQUE INDEX germplasm_relation_parent_germplasm_id_order_number_idx
--rollback     ON germplasm.germplasm_relation
--rollback     USING btree (parent_germplasm_id, order_number)
--rollback ;

--rollback ALTER TABLE germplasm.germplasm_relation
--rollback     ADD CONSTRAINT germplasm_relation_parent_germplasm_id_order_number_idx
--rollback     UNIQUE USING INDEX germplasm_relation_parent_germplasm_id_order_number_idx
--rollback ;

--rollback COMMENT ON CONSTRAINT germplasm_relation_parent_germplasm_id_order_number_idx ON germplasm.germplasm_relation IS 'A germplasm relation can be retrieved using its unique order number within a germplasm (parent).';

--rollback COMMENT ON CONSTRAINT germplasm_relation_parent_germplasm_id_child_germplasm_id_idx ON germplasm.germplasm_relation IS 'A germplasm relation can be retrieved using its unique germplasm (child) within another germplasm (parent).';
