--liquibase formatted sql

--changeset postgres:remove_unique_constraints_in_seed_relation_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-275 Remove unique constraints in seed_relation table



-- drop unique index constraints for parent and child
ALTER TABLE germplasm.seed_relation
    DROP CONSTRAINT seed_relation_parent_seed_id_child_seed_id_idx
;

-- drop unique index constraints for parent and order number
ALTER TABLE germplasm.seed_relation
    DROP CONSTRAINT seed_relation_parent_seed_id_order_number_idx
;



-- revert changes
--rollback CREATE UNIQUE INDEX seed_relation_parent_seed_id_child_seed_id_idx
--rollback     ON germplasm.seed_relation
--rollback     USING btree (parent_seed_id, child_seed_id)
--rollback ;

--rollback ALTER TABLE germplasm.seed_relation
--rollback     ADD CONSTRAINT seed_relation_parent_seed_id_child_seed_id_idx
--rollback     UNIQUE USING INDEX seed_relation_parent_seed_id_child_seed_id_idx
--rollback ;

--rollback CREATE UNIQUE INDEX seed_relation_parent_seed_id_order_number_idx
--rollback     ON germplasm.seed_relation
--rollback     USING btree (parent_seed_id, order_number)
--rollback ;

--rollback ALTER TABLE germplasm.seed_relation
--rollback     ADD CONSTRAINT seed_relation_parent_seed_id_order_number_idx
--rollback     UNIQUE USING INDEX seed_relation_parent_seed_id_order_number_idx
--rollback ;

--rollback COMMENT ON CONSTRAINT seed_relation_parent_seed_id_order_number_idx ON germplasm.seed_relation IS 'A seed relation can be retrieved using its unique order number within a seed (parent).';

--rollback COMMENT ON CONSTRAINT seed_relation_parent_seed_id_child_seed_id_idx ON germplasm.seed_relation IS 'A seed relation can be retrieved using its unique seed (child) within another seed (parent).';
