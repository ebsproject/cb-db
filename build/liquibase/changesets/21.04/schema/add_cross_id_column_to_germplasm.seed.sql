--liquibase formatted sql

--changeset postgres:add_cross_id_column_to_germplasm.seed context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-210 Add cross_id column to germplasm.seed



--add column
ALTER TABLE 
    germplasm.seed
ADD COLUMN 
    cross_id integer;

COMMENT ON COLUMN germplasm.seed.cross_id
    IS 'Cross ID: Reference to the cross where the seeds are harvested [SEED_CROSS_ID]';

--add fk
ALTER TABLE 
    germplasm.seed
ADD CONSTRAINT 
    seed_cross_id_fk 
FOREIGN KEY 
    (cross_id)
REFERENCES germplasm.cross (id) 
    MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE RESTRICT;

--add idx
CREATE INDEX seed_cross_id_idx
    ON germplasm.seed USING btree
    (cross_id ASC NULLS LAST)
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.seed_cross_id_idx
    IS 'A seed can be retrieved by its cross.';



--rollback ALTER TABLE germplasm.seed DROP COLUMN cross_id;