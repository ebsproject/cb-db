--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_of_crop_id_column_in_germplasm.taxonomy context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-276 Remove not null constraint of crop_id column in germplasm.taxonomy



ALTER TABLE
    germplasm.taxonomy
ALTER COLUMN 
    crop_id DROP NOT NULL;



--rollback ALTER TABLE germplasm.taxonomy ALTER COLUMN crop_id SET NOT NULL;