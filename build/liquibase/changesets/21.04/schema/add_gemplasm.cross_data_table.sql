--liquibase formatted sql

--changeset postgres:add_gemplasm.cross_data_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-211 Add germplasm.cross_data table



CREATE TABLE germplasm.cross_data
(
    id SERIAL NOT NULL,
    cross_id integer NOT NULL,
    variable_id integer NOT NULL,
    data_value character varying NOT NULL,
    data_qc_code character varying(8) COLLATE pg_catalog."default" NOT NULL,
    creator_id integer NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
    modifier_id integer,
    modification_timestamp timestamp without time zone,
    is_void boolean NOT NULL DEFAULT false,
    notes text COLLATE pg_catalog."default",
    event_log jsonb,
    CONSTRAINT pk_cross_data_id PRIMARY KEY (id),
    CONSTRAINT cross_data_creator_id_fkey FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT cross_data_modifier_id_fkey FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT cross_data_cross_id_fk FOREIGN KEY (cross_id)
        REFERENCES germplasm."cross" (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT cross_data_variable_id_fk FOREIGN KEY (variable_id)
        REFERENCES master.variable (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
        NOT VALID
);

COMMENT ON TABLE germplasm.cross_data
    IS 'Cross Data: Data related to the cross [CRSDATA]';

COMMENT ON COLUMN germplasm.cross_data.id
    IS 'Cross Data ID: Database identifier of the cross data [CRSDATA_ID]';

COMMENT ON COLUMN germplasm.cross_data.cross_id
    IS 'Cross ID: Reference to the cross where the data is associated [CRSDATA_CRS_ID]';

COMMENT ON COLUMN germplasm.cross_data.variable_id
    IS 'Variable ID: Reference to the variable of the data [CRSDATA_VAR_ID]';

COMMENT ON COLUMN germplasm.cross_data.data_value
    IS 'Cross Data Data Value: Value of the cross data [CRSDATA_DATAVAL]';

COMMENT ON COLUMN germplasm.cross_data.data_qc_code
    IS 'Cross Data Data QC Code: Quality of the cross data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [CRSDATA_QCCODE]';

COMMENT ON COLUMN germplasm.cross_data.creator_id
    IS 'Creator ID: Reference to the person who created the record [CRSDATA_CPERSON]';

COMMENT ON COLUMN germplasm.cross_data.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [CRSDATA_CTSTAMP]';

COMMENT ON COLUMN germplasm.cross_data.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [CRSDATA_MPERSON]';

COMMENT ON COLUMN germplasm.cross_data.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [CRSDATA_MTSTAMP]';

COMMENT ON COLUMN germplasm.cross_data.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [CRSDATA_ISVOID]';

COMMENT ON COLUMN germplasm.cross_data.notes
    IS 'Notes: Technical details about the record [CRSDATA_NOTES]';
COMMENT ON CONSTRAINT pk_package_data_id ON germplasm.package_data
    IS 'A cross data is identified by its unique database ID.';

COMMENT ON CONSTRAINT cross_data_cross_id_fk ON germplasm.cross_data
    IS 'A cross data refers to one cross. A cross can have one or many cross datas.';
COMMENT ON CONSTRAINT cross_data_variable_id_fk ON germplasm.cross_data
    IS 'A cross data refers to one variable. A variable can be referred by one or many cross datas.';

CREATE INDEX cross_data_creator_id_idx
    ON germplasm.cross_data USING btree
    (creator_id)
    TABLESPACE pg_default;

CREATE INDEX cross_data_is_void_idx
    ON germplasm.cross_data USING btree
    (is_void)
    TABLESPACE pg_default;

CREATE INDEX cross_data_modifier_id_idx
    ON germplasm.cross_data USING btree
    (modifier_id)
    TABLESPACE pg_default;

CREATE INDEX cross_data_cross_id_data_qc_code_idx
    ON germplasm.cross_data USING btree
    (cross_id, data_qc_code COLLATE pg_catalog."default")
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.cross_data_cross_id_data_qc_code_idx
    IS 'A cross data can be retrieved by its data QC code within a cross.';

CREATE INDEX cross_data_cross_id_idx
    ON germplasm.cross_data USING btree
    (cross_id)
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.cross_data_cross_id_idx
    IS 'A cross data can be retrieved by its cross.';

CREATE INDEX cross_data_cross_id_variable_id_data_value_idx
    ON germplasm.cross_data USING btree
    (cross_id, variable_id, data_value COLLATE pg_catalog."default")
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.cross_data_cross_id_variable_id_data_value_idx
    IS 'A cross data can be retrieved by its variable and data value within a cross.';

CREATE INDEX cross_data_cross_id_variable_id_idx
    ON germplasm.cross_data USING btree
    (cross_id, variable_id)
    TABLESPACE pg_default;

COMMENT ON INDEX germplasm.cross_data_cross_id_variable_id_idx
    IS 'A cross data can be retrieved by its variable within a cross.';

CREATE INDEX cross_data_variable_id_idx
    ON germplasm.cross_data USING btree
    (variable_id)
    TABLESPACE pg_default;

CREATE TRIGGER cross_data_event_log_tgr
    BEFORE INSERT OR UPDATE 
    ON germplasm.cross_data
    FOR EACH ROW
    EXECUTE PROCEDURE platform.log_record_event();



--rollback DROP TABLE germplasm.cross_data;