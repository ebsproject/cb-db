--liquibase formatted sql

--changeset postgres:insert_missing_protocols_to_experiments_with_no_management_protocols context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-510 CB-DB: insert missing MANAGEMENT_PROTOCOL with experiments



INSERT INTO  tenant.protocol 
	(protocol_code,protocol_name,protocol_type,description,program_id,creator_id,notes)
SELECT
	    'MANAGEMENT_PROTOCOL_'||ex.experiment_code AS protocol_code,
	    'Management Protocol Exp'||substring(ex.experiment_code, 4) AS protocol_name,
	    'management' AS protocol_type,
	    null AS description,
	    ex.program_id,
	    (SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin') AS creator_id,
		'Inserted via liquibase changeset (BDS-510)' notes
	FROM
	   	experiment.experiment ex
	where 
		ex.id in (SELECT
	distinct(occurrence.experiment_id)
	FROM
		experiment.occurrence occurrence
	LEFT JOIN
		(
			SELECT
				occurrence_id
			FROM
				experiment.occurrence_data
			WHERE
				variable_id IN (SELECT id FROM master.variable WHERE abbrev IN ('MANAGEMENT_PROTOCOL_LIST_ID', 'TRAIT_PROTOCOL_LIST_ID'))
		) occurrence_data
	ON
		occurrence.id = occurrence_data.occurrence_id
	WHERE
		occurrence_data.occurrence_id IS null)
	AND
		ex.experiment_code NOT IN (SELECT
	substring(proc.protocol_code,21)
	FROM
		tenant.protocol proc
	WHERE protocol_code ilike 'MANAGEMENT_PROTOCOL_EXP%');



--rollback DELETE FROM
--rollback     tenant.protocol
--rollback WHERE
--rollback     
--rollback 		protocol_code ILIKE 'MANAGEMENT_PROTOCOL_%'
--rollback AND
--rollback 		notes = 'Inserted via liquibase changeset (BDS-510)';