--liquibase formatted sql

--changeset postgres:insert_missing_lists_to_occurrence_with_no_management_protocol_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-510 CB-DB: insert missing MANAGEMENT_LIST with occurrence



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'MANAGEMENT_PROTOCOL_' || occur.occurrence_code AS abbrev,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'variable' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'VARIABLE') AS entity_id,
	(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-510)' notes,
	'management protocol' AS list_sub_type
FROM 
	experiment.occurrence occur
WHERE
	occur.id IN (
		SELECT
	DISTINCT(occurrence.id)
	FROM
		experiment.occurrence occurrence
	LEFT JOIN
		(
			SELECT
				occurrence_id
			FROM
				experiment.occurrence_data
			WHERE
				variable_id IN (SELECT id FROM master.variable WHERE abbrev IN ('MANAGEMENT_PROTOCOL_LIST_ID', 'TRAIT_PROTOCOL_LIST_ID'))
		) occurrence_data
	ON
		occurrence.id = occurrence_data.occurrence_id
	WHERE
		occurrence_data.occurrence_id IS NULL
		AND
		occurrence.occurrence_code NOT IN (SELECT
	substring(list.abbrev,21)
	FROM
		platform.list list
	WHERE abbrev ilike 'MANAGEMENT_PROTOCOL_%'
	)
	);



--rollback DELETE FROM
--rollback     platform.list
--rollback WHERE
--rollback abbrev ILIKE 'MANAGEMENT_PROTOCOL_%'
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-510)'



