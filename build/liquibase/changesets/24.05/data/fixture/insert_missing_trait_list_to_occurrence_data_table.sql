--liquibase formatted sql

--changeset postgres:insert_missing_trait_list_to_occurrence_data_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-510 CB-DB: insert proper occurrence records to occurrence data with TRAIT LIST variable.


INSERT INTO 
    experiment.occurrence_data
        (occurrence_id,variable_id,data_value,data_qc_code,creator_id,notes,protocol_id)
SELECT 
	occur.id AS occurrence_id,
	(SELECT id FROM master.variable WHERE abbrev = 'TRAIT_PROTOCOL_LIST_ID') AS variable_id,
	(SELECT id FROM platform.list WHERE abbrev IN (SELECT 'TRAIT_PROTOCOL_' ||occur.occurrence_code from experiment.occurrence where id = occur.id )) AS data_value,
	'G',
	(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-510)' notes,
	(SELECT id FROM tenant.protocol WHERE protocol_code  IN (SELECT 'TRAIT_PROTOCOL_' || exp.experiment_code FROM experiment.experiment exp WHERE id = occur.experiment_id)) AS protocol_id
FROM
	experiment.occurrence occur
WHERE 
	occur.id IN (
		SELECT
	DISTINCT(occurrence.id)
	FROM
		experiment.occurrence occurrence
	LEFT JOIN
		(
			SELECT
				occurrence_id
			FROM
				experiment.occurrence_data
			WHERE
				variable_id IN (SELECT id FROM master.variable WHERE abbrev IN ('MANAGEMENT_PROTOCOL_LIST_ID', 'TRAIT_PROTOCOL_LIST_ID'))
		) occurrence_data
	ON
		occurrence.id = occurrence_data.occurrence_id
	WHERE
		occurrence_data.occurrence_id IS NULL
	)



--rollback DELETE FROM experiment.occurrence_data 
--rollback WHERE
--rollback variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'TRAIT_PROTOCOL_LIST_ID') 
--rollback AND
--rollback notes = 'Inserted via liquibase changeset (BDS-510)'