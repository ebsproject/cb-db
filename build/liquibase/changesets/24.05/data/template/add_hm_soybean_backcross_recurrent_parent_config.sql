--liquibase formatted sql

--changeset postgres:add_hm_soybean_backcross_recurrent_parent_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1182 CB-HM: Soybean Re-use existing COWPEA use cases for SOYBEAN



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_SOYBEAN_DEFAULT',
        'Harvest Manager Recurrent Parent Pattern for Soybean Backcrosses',
        $$
            {
                "pattern": [
                    [
                        {
                            "type": "field",
                            "entity": "recurrentParentGermplasm",
                            "field_name": "designation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "<",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    [
                        {
                            "type": "field",
                            "entity": "recurrentParentGermplasm",
                            "field_name": "designation",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "<",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ]
                ],
                "delimiter_parentage": "/",
                "delimiter_backcross_number": "<"
            }
        $$,
        1,
        'harvest_manager',
        1,
        'BDS-1182 CB-HM: Soybean Re-use existing COWPEA use cases for SOYBEAN - k.delarosa'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_RECURRENT_PARENT_PATTERN_BACKCROSS_SOYBEAN_DEFAULT';