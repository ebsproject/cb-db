--liquibase formatted sql

--changeset postgres:add_hm_configs_harvest_data_requirements_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1092 CB-HM: Add new config for harvest data requirements for HM



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_HARVEST_DATA_REQUIREMENTS_CONFIG',
        'Harvest Manager - Harvest Data Requirements Configuration',
        $$			
            [
                {
                    "cropCode": "*",
                    "state": "unknown",
                    "config": {
                        "harvestData": [ ],
                        "harvestMethods": { }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "selfing",
                    "state": "fixed",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": []
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "selfing",
                    "state": "fixed",
                    "type": "fixed_line",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": [],
                            "SINGLE_PLANT_SEED_INCREASE": [
                                {
                                    "variableAbbrev": "NO_OF_PLANTS",
                                    "apiFieldName": "noOfPlant",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "selfing",
                    "state": "*",
                    "type": [ 
                        "genome_edited", "transgenic" 
                    ],
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": [],
                            "SINGLE_PLANT_SELECTION": [
                                {
                                    "variableAbbrev": "NO_OF_PLANTS",
                                    "apiFieldName": "noOfPlant",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "selfing",
                    "state": "not_fixed",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": [],
                            "SINGLE_SEED_DESCENT": [],
                            "SINGLE_PLANT_SELECTION": [
                                {
                                    "variableAbbrev": "NO_OF_PLANTS",
                                    "apiFieldName": "noOfPlant",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ],
                            "SINGLE_PLANT_SELECTION_AND_BULK": [
                                {
                                    "variableAbbrev": "NO_OF_PLANTS",
                                    "apiFieldName": "noOfPlant",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ],
                            "PANICLE_SELECTION": [
                                {
                                    "variableAbbrev": "PANNO_SEL",
                                    "apiFieldName": "noOfPanicle",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ],
                            "PLANT_SPECIFIC": [
                                {
                                    "variableAbbrev": "SPECIFIC_PLANT",
                                    "apiFieldName": "specificPlantNo",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "comma_sep_int"
                                    }
                                }
                            ],
                            "PLANT_SPECIFIC_AND_BULK": [
                                {
                                    "variableAbbrev": "SPECIFIC_PLANT",
                                    "apiFieldName": "specificPlantNo",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "comma_sep_int"
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "stageCode": "TCV",
                    "crossMethod": "selfing",
                    "state": "fixed",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "R_LINE_HARVEST": [
                                {
                                    "variableAbbrev": "NO_OF_BAGS",
                                    "apiFieldName": "noOfBag",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 1
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "stageCode": "TCV",
                    "crossMethod": "selfing",
                    "state": "not_fixed",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": { }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": [
                        "single cross", "backcross"
                    ],
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": [
                                {
                                    "variableAbbrev": "NO_OF_SEED",
                                    "apiFieldName": "noOfSeed",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ],
                            "HEAD_SINGLE_SEED_NUMBERING": [
                                {
                                    "variableAbbrev": "NO_OF_SEED",
                                    "apiFieldName": "noOfSeed",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": [
                        "double cross", "three-way cross",
                        "complex cross"
                    ],
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": [
                                {
                                    "variableAbbrev": "NO_OF_SEED",
                                    "apiFieldName": "noOfSeed",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": [
                        "transgenesis", "genome editing"
                    ],
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "HEAD_SINGLE_SEED_NUMBERING": [
                                {
                                    "variableAbbrev": "NO_OF_SEED",
                                    "apiFieldName": "noOfSeed",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "hybrid formation",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "H_HARVEST": [
                                {
                                    "variableAbbrev": "NO_OF_BAGS",
                                    "apiFieldName": "noOfBag",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "CMS multiplication",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "A_LINE_HARVEST": [
                                {
                                    "variableAbbrev": "NO_OF_BAGS",
                                    "apiFieldName": "noOfBag",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "RICE",
                    "crossMethod": "test cross",
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            },
                            {
                                "variableAbbrev": "DATE_CROSSED",
                                "apiFieldName": "crossingDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "TEST_CROSS_HARVEST": [
                                {
                                    "variableAbbrev": "NO_OF_BAGS",
                                    "apiFieldName": "noOfBag",
                                    "required": true,
                                    "inputType": "number",
                                    "inputOptions": {
                                        "inputSubType": "single_int",
                                        "minimumValue": 0
                                    }
                                }
                            ]
                        }
                    } 
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethod": [
                        "single cross", "double cross", "top cross", "backcross", "hybrid formation"
                    ],
                    "config": {
                        "harvestData": [
                            {
                                "variableAbbrev": "HVDATE_CONT",
                                "apiFieldName": "harvestDate",
                                "required": true,
                                "inputType": "datepicker",
                                "inputOptions": {}
                            }
                        ],
                        "harvestMethods": {
                            "BULK": []
                        }
                    } 
                }
            ]
        $$,
        1,
        'harvest_manager',
        1,
        'BDS-1092 CB-HM: Add new config for harvest data requirements for HM - j.bantay'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_HARVEST_DATA_REQUIREMENTS_CONFIG';