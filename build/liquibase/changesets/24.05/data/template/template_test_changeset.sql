--liquibase formatted sql

--changeset postgres:template_test_changeset context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:
--precondition-sql-check expectedResult:'admin-EBS' select first_name || '-' || last_name from tenant.person where username ='admin'
--comment:For presentation

--preconditions onFail:HALT onError:HALT
--precondition-sql-check expectedResult:expected value SELECT CASE EXISTS(query) WHEN TRUE THEN 1 ELSE 0 END;

UPDATE tenant.person
SET
    username = 'admin_user'
WHERE
    username = 'admin'
;



--rollback UPDATE tenant.person
--rollback SET
--rollback     username = 'admin'
--rollback WHERE
--rollback     username = 'admin_user'
--rollback ;