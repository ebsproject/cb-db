--liquibase formatted sql

--changeset postgres:update_platform_application_person context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-772 CB: Remove person tool - the persons, programs, role, and teams are managed in CS



--update application record to hide person tool
UPDATE platform.application
SET
    is_void = true
WHERE
    abbrev = 'PERSONS'
;



--rollback UPDATE platform.application
--rollback SET
--rollback     is_void = false
--rollback WHERE
--rollback     abbrev = 'PERSONS'
--rollback ;