--liquibase formatted sql

--changeset postgres:update_working_list_config_default_RICE context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1011 CB-SI: Inventory Search > Working list: Having a hard time reordering items in this page 



--update data browser config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
            "sort": "true",
            "label": "Package Program",
            "abbrev": "PROGRAM",
            "filter": "true",
            "data_level": "package",
            "description": "Package program",
            "display_name": "Program",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Seed Name",
            "abbrev": "SEED_NAME",
            "filter": "true",
            "data_level": "seed",
            "description": "Seed Name",
            "display_name": "Seed Name",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Seed Code",
            "abbrev": "SEED_CODE",
            "filter": "true",
            "data_level": "seed",
            "description": "Seed Code",
            "display_name": "Seed Code",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Package Code",
            "abbrev": "PACKAGE_CODE",
            "filter": "true",
            "data_level": "package",
            "description": "Package Code",
            "display_name": "Package Code",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Package Label",
            "abbrev": "PACKAGE_LABEL",
            "filter": "true",
            "data_level": "package",
            "description": "Package Label",
            "display_name": "Package Label",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Germplasm Code",
            "abbrev": "GERMPLASM_CODE",
            "filter": "true",
            "data_level": "germplasm",
            "description": "Germplasm Code",
            "display_name": "Germplasm Code",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Germplasm Name",
            "abbrev": "GERMPLASM_NAME",
            "filter": "true",
            "data_level": "germplasm",
            "description": "Germplasm Name",
            "display_name": "Germplasm Name",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "true",
            "label": "Parentage",
            "abbrev": "PARENTAGE",
            "filter": "true",
            "data_level": "germplasm",
            "description": "Parentage",
            "display_name": "Parentage",
            "browser_column": "true",
            "visible_export": "true",
            "visible_browser": "true",
            "visible_template": "true"
            },
            {
            "sort": "false",
            "label": "Seed ID",
            "abbrev": "SEED_ID",
            "filter": "false",
            "data_level": "seed",
            "description": "Seed ID",
            "display_name": "Seed ID",
            "browser_column": "false",
            "visible_export": "false",
            "visible_browser": "false",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Package ID",
            "abbrev": "PACKAGE_ID",
            "filter": "false",
            "data_level": "package",
            "description": "Package ID",
            "display_name": "Package ID",
            "browser_column": "false",
            "visible_export": "false",
            "visible_browser": "false",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Germplasm ID",
            "abbrev": "GERMPLASM_ID",
            "filter": "false",
            "data_level": "germplasm",
            "description": "Germplasm ID",
            "display_name": "Germplasm ID",
            "browser_column": "false",
            "visible_export": "false",
            "visible_browser": "false",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Experiment Name",
            "abbrev": "EXPERIMENT_NAME",
            "filter": "false",
            "data_level": "seed",
            "description": "Experiment Name",
            "display_name": "Experiment Name",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Facility",
            "abbrev": "FACILITY",
            "filter": "false",
            "data_level": "package",
            "description": "Facility",
            "display_name": "Facility",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Container",
            "abbrev": "CONTAINER",
            "filter": "false",
            "data_level": "package",
            "description": "Container",
            "display_name": "Container",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Package Quantity",
            "abbrev": "PACKAGE_QUANTITY",
            "filter": "false",
            "data_level": "package",
            "description": "Package Quantity",
            "display_name": "Package Quantity",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Order Number",
            "abbrev": "ORDER_NUMBER",
            "filter": "false",
            "data_level": "package",
            "description": "Order Number",
            "display_name": "Order Number",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            },
            {
            "sort": "false",
            "label": "Order Number",
            "abbrev": "ORDER_NUMBER",
            "filter": "false",
            "data_level": "package",
            "description": "Order Number",
            "display_name": "Order Number",
            "browser_column": "true",
            "visible_export": "false",
            "visible_browser": "true",
            "visible_template": "false"
            }
        ]
        }$$
WHERE
    abbrev = 'WORKING_LIST_VARIABLES_CONFIG_RICE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback    config_value = $${
--rollback   "values": [
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Package Program",
--rollback       "abbrev": "PROGRAM",
--rollback       "filter": "true",
--rollback       "data_level": "package",
--rollback       "description": "Package program",
--rollback       "display_name": "Program",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Seed Name",
--rollback       "abbrev": "SEED_NAME",
--rollback       "filter": "true",
--rollback       "data_level": "seed",
--rollback       "description": "Seed Name",
--rollback       "display_name": "Seed Name",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Seed Code",
--rollback       "abbrev": "SEED_CODE",
--rollback       "filter": "true",
--rollback       "data_level": "seed",
--rollback       "description": "Seed Code",
--rollback       "display_name": "Seed Code",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Package Code",
--rollback       "abbrev": "PACKAGE_CODE",
--rollback       "filter": "true",
--rollback       "data_level": "package",
--rollback       "description": "Package Code",
--rollback       "display_name": "Package Code",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Package Label",
--rollback       "abbrev": "PACKAGE_LABEL",
--rollback       "filter": "true",
--rollback       "data_level": "package",
--rollback       "description": "Package Label",
--rollback       "display_name": "Package Label",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Germplasm Code",
--rollback       "abbrev": "GERMPLASM_CODE",
--rollback       "filter": "true",
--rollback       "data_level": "germplasm",
--rollback       "description": "Germplasm Code",
--rollback       "display_name": "Germplasm Code",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Germplasm Name",
--rollback       "abbrev": "GERMPLASM_NAME",
--rollback       "filter": "true",
--rollback       "data_level": "germplasm",
--rollback       "description": "Germplasm Name",
--rollback       "display_name": "Germplasm Name",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "true",
--rollback       "label": "Parentage",
--rollback       "abbrev": "PARENTAGE",
--rollback       "filter": "true",
--rollback       "data_level": "germplasm",
--rollback       "description": "Parentage",
--rollback       "display_name": "Parentage",
--rollback       "browser_column": "true",
--rollback       "visible_export": "true",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "true"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Seed ID",
--rollback       "abbrev": "SEED_ID",
--rollback       "filter": "false",
--rollback       "data_level": "seed",
--rollback       "description": "Seed ID",
--rollback       "display_name": "Seed ID",
--rollback       "browser_column": "false",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "false",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Package ID",
--rollback       "abbrev": "PACKAGE_ID",
--rollback       "filter": "false",
--rollback       "data_level": "package",
--rollback       "description": "Package ID",
--rollback       "display_name": "Package ID",
--rollback       "browser_column": "false",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "false",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Germplasm ID",
--rollback       "abbrev": "GERMPLASM_ID",
--rollback       "filter": "false",
--rollback       "data_level": "germplasm",
--rollback       "description": "Germplasm ID",
--rollback       "display_name": "Germplasm ID",
--rollback       "browser_column": "false",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "false",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Experiment Name",
--rollback       "abbrev": "EXPERIMENT_NAME",
--rollback       "filter": "false",
--rollback       "data_level": "seed",
--rollback       "description": "Experiment Name",
--rollback       "display_name": "Experiment Name",
--rollback       "browser_column": "true",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Facility",
--rollback       "abbrev": "FACILITY",
--rollback       "filter": "false",
--rollback       "data_level": "package",
--rollback       "description": "Facility",
--rollback       "display_name": "Facility",
--rollback       "browser_column": "true",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Container",
--rollback       "abbrev": "CONTAINER",
--rollback       "filter": "false",
--rollback       "data_level": "package",
--rollback       "description": "Container",
--rollback       "display_name": "Container",
--rollback       "browser_column": "true",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Package Quantity",
--rollback       "abbrev": "PACKAGE_QUANTITY",
--rollback       "filter": "false",
--rollback       "data_level": "package",
--rollback       "description": "Package Quantity",
--rollback       "display_name": "Package Quantity",
--rollback       "browser_column": "true",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "false"
--rollback     },
--rollback     {
--rollback       "sort": "false",
--rollback       "label": "Order Number",
--rollback       "abbrev": "ORDER_NUMBER",
--rollback       "filter": "false",
--rollback       "data_level": "package",
--rollback       "description": "Order Number",
--rollback       "display_name": "Order Number",
--rollback       "browser_column": "true",
--rollback       "visible_export": "false",
--rollback       "visible_browser": "true",
--rollback       "visible_template": "false"
--rollback     }
--rollback   ]
--rollback }$$
--rollback WHERE
--rollback     abbrev = 'WORKING_LIST_VARIABLES_CONFIG_RICE_DEFAULT'
--rollback ;