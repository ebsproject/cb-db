--liquibase formatted sql

--changeset postgres:add_hm_soybean_germplasm_name_pattern_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1182 CB-HM: Soybean Re-use existing COWPEA use cases for SOYBEAN



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
     'HM_NAME_PATTERN_GERMPLASM_SOYBEAN_DEFAULT',
     'Harvest Manager Germplasm Name Pattern for Soybean',
     $${
        "PLOT": {
            "default": {
                "not_fixed": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            }
                        ],
                        "single plant selection": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "bi-parental cross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "bulk": [
                            {
                                "type": "free-text",
                                "value": "IT",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "harvestYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "counter",
                                "order_number": 4
                            }
                        ]
                    },
                    "not_fixed": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "bulk": [
                            {
                                "type": "free-text",
                                "value": "IT",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "harvestYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "originSiteCode",
                                "order_number": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 3
                            },
                            {
                                "type": "counter",
                                "order_number": 4
                            }
                        ]
                    }
                }
            },
            "backcross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "bulk": [
                            {
                                "type": "field",
                                "entity": "nonRecurrentParentGermplasm",
                                "field_name": "designation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
            "germplasm_state": {
                "germplasm_type": {
                "harvest_method": [
                    {
                    "type": "free-text",
                    "value": "ABC",
                    "order_number": 0
                    },
                    {
                    "type": "field",
                    "entity": "<entity>",
                    "field_name": "<field_name>",
                    "order_number": 1
                    },
                    {
                    "type": "delimiter",
                    "value": "-",
                    "order_number": 1
                    },
                    {
                    "type": "counter",
                    "order_number": 3
                    },
                    {
                    "type": "db-sequence",
                    "schema": "<schema>",
                    "order_number": 4,
                    "sequence_name": "<sequence_name>"
                    },
                    {
                    "type": "free-text-repeater",
                    "value": "ABC",
                    "minimum": "2",
                    "delimiter": "*",
                    "order_number": 5
                    }
                ]
                }
            }
            }
        }
    }$$,
    1,
    'Harvest Manager SOYBEAN germplasm name config',
    1,
    'BDS-1182 CB-HM: Soybean Re-use existing COWPEA use cases for SOYBEAN - k.delarosa'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_NAME_PATTERN_GERMPLASM_SOYBEAN_DEFAULT';