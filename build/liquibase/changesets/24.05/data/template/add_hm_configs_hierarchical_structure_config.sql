--liquibase formatted sql

--changeset postgres:add_hm_configs_hierarchical_structure_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-1092 CB-HM: Add new config for hierarchical structure for HM



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_CONFIGS_HIERARCHICAL_STRUCTURE',
        'Harvest Manager - Basis for the hierarchical structure of configurations (PLEASE DO NOT MODIFY. See notes for more info.)',
        $$			
            {
                "entities": [
                    {
                        "entity": "germplasm",
                        "level": 0,
                        "fields": [
                            "state",
                            "type",
                            "generation"
                        ]
                    },
                    {
                        "entity": "cross",
                        "level": 1,
                        "fields": [
                            "crossMethod"
                        ]
                    },
                    {
                        "entity": "experiment",
                        "level": 2,
                        "fields": [
                            "stageCode"
                        ]
                    },
                    {
                        "entity": "program",
                        "level": 2,
                        "fields": [
                            "programCode"
                        ]
                    },
                    {
                        "entity": "crop",
                        "level": 3,
                        "fields": [
                            "cropCode"
                        ]
                    }
                ]
            }
        $$,
        1,
        'harvest_manager',
        1,
        'PLEASE DO NOT MODIFY. Kindly reach out to the CB development team if changes are needed. (BDS-1092 CB-HM: Add new config for hierarchical structure for HM)'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_CONFIGS_HIERARCHICAL_STRUCTURE';