--liquibase formatted sql

--changeset postgres:add_experiments_view_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2875 CB-BPM-DB: Create changeset for experiments_view config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'EXPERIMENTS_VIEW_GLOBAL',
        'Experiments View Global',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_ID",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_VIEW_ROLE_COLLABORATOR',
        'Experiments View Role Collaborator',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_VIEW_PROGRAM_IRSEA',
        'Experiments View Program IRSEA',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_ID",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_VIEW_PROGRAM_KE',
        'Experiments View Program KE',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_VIEW_PROGRAM_BW-CIMMYT',
        'Experiments View Program BW-CIMMYT',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_VIEW_GLOBAL';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_VIEW_ROLE_COLLABORATOR';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_VIEW_PROGRAM_IRSEA';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_VIEW_PROGRAM_KE';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_VIEW_PROGRAM_BW-CIMMYT';