--liquibase formatted sql

--changeset postgres:update_all_post_harvest_rows_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2544	CB-DB: Update config values of POST HARVEST ROWS



UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Default configuration for the first row in the post-harvest data collection page",
    "Values": [
        {
            "include_form": true,
            "disabled": true,
            "target_value": "programCode",
            "target_column": "programDbId",
            "variable_abbrev": "PROGRAM",
            "variable_type": "metadata"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "experimentYear",
            "target_column": "experimentYear",
            "variable_abbrev": "EXPERIMENT_YEAR"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Default configuration for the second row in the post-harvest data collection page",
    "Values": [
        {
            "disabled": true,
            "include_form": true,
            "target_value": "occurrenceName",
            "target_column": "occurrenceDbId",
            "variable_type": "metadata",
            "variable_abbrev": "OCCURRENCE_NAME"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_2_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Default configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "REMARKS",
            "target_column": "REMARKS",
            "variable_type": "metadata",
            "variable_abbrev": "REMARKS",
            "entity_data_type": "plot-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program BW-CIMMYT configuration for the first row in the post-harvest data collection page",
    "Values": [
        {
            "include_form": true,
            "disabled": true,
            "target_value": "programCode",
            "target_column": "programDbId",
            "variable_abbrev": "PROGRAM",
            "variable_type": "metadata"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "experimentYear",
            "target_column": "experimentYear",
            "variable_abbrev": "EXPERIMENT_YEAR"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program BW-CIMMYT configuration for the second row in the post-harvest data collection page",
    "Values": [
        {
            "disabled": true,
            "include_form": true,
            "target_value": "occurrenceName",
            "target_column": "occurrenceDbId",
            "variable_type": "metadata",
            "variable_abbrev": "OCCURRENCE_NAME"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "designation",
            "target_column": "designation",
            "variable_type": "metadata",
            "variable_abbrev": "DESIGNATION"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_2_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program BW-CIMMYT configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "MC_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "MC_CONT",
            "entity_data_type": "trait-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_3_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
    "Values": [
        {
            "include_form": true,
            "disabled": true,
            "target_value": "programCode",
            "target_column": "programDbId",
            "variable_abbrev": "PROGRAM",
            "variable_type": "metadata"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "experimentYear",
            "target_column": "experimentYear",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "seasonCode",
            "target_column": "seasonDbId",
            "variable_type": "metadata",
            "variable_abbrev": "SEASON"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "data_value",
            "target_column": "occurrenceDataDbId",
            "variable_type": "metadata",
            "variable_abbrev": "POST_HARVEST_UNIT",
            "entity_data_type": "occurrence_data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "data_value",
            "target_column": "occurrenceDataDbId",
            "variable_type": "metadata",
            "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
            "entity_data_type": "occurrence_data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program IRSEA configuration for the second row in the post-harvest data collection page",
    "Values": [
        {
            "disabled": true,
            "include_form": true,
            "target_value": "occurrenceName",
            "target_column": "occurrenceDbId",
            "variable_type": "metadata",
            "variable_abbrev": "OCCURRENCE_NAME"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "designation",
            "target_column": "designation",
            "variable_type": "metadata",
            "variable_abbrev": "DESIGNATION"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "entryNumber",
            "target_column": "entryDbId",
            "variable_type": "metadata",
            "variable_abbrev": "ENTRY_NUMBER"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "plotCode",
            "target_column": "plotDbId",
            "variable_type": "metadata",
            "variable_abbrev": "PLOT_CODE"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_2_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "AYLD_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "AYLD_CONT",
            "entity_data_type": "trait-data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "MC_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "MC_CONT",
            "entity_data_type": "trait-data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "REMARKS",
            "target_column": "REMARKS",
            "variable_type": "metadata",
            "variable_abbrev": "REMARKS",
            "entity_data_type": "plot-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program KE configuration for the first row in the post-harvest data collection page",
    "Values": [
        {
            "include_form": true,
            "disabled": true,
            "target_value": "programCode",
            "target_column": "programDbId",
            "variable_abbrev": "PROGRAM",
            "variable_type": "metadata"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "seasonCode",
            "target_column": "seasonDbId",
            "variable_type": "metadata",
            "variable_abbrev": "SEASON"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program KE configuration for the second row in the post-harvest data collection page",
    "Values": [
        {
            "disabled": true,
            "include_form": true,
            "target_value": "occurrenceName",
            "target_column": "occurrenceDbId",
            "variable_type": "metadata",
            "variable_abbrev": "OCCURRENCE_NAME"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "designation",
            "target_column": "designation",
            "variable_type": "metadata",
            "variable_abbrev": "DESIGNATION"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "entryNumber",
            "target_column": "entryDbId",
            "variable_type": "metadata",
            "variable_abbrev": "ENTRY_NUMBER"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_2_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program KE configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "MC_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "MC_CONT",
            "entity_data_type": "trait-data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "REMARKS",
            "target_column": "REMARKS",
            "variable_type": "metadata",
            "variable_abbrev": "REMARKS",
            "entity_data_type": "plot-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_3_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Role collaborator configuration for the first row in the post-harvest data collection page",
    "Values": [
        {
            "include_form": true,
            "disabled": true,
            "target_value": "programCode",
            "target_column": "programDbId",
            "variable_abbrev": "PROGRAM",
            "variable_type": "metadata"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "experimentYear",
            "target_column": "experimentYear",
            "variable_abbrev": "EXPERIMENT_YEAR"
        },
        {
            "include_form": true,
            "disabled": true,
            "target_value": "seasonCode",
            "target_column": "seasonDbId",
            "variable_type": "metadata",
            "variable_abbrev": "SEASON"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Role collaborator configuration for the second row in the post-harvest data collection page",
    "Values": [
        {
            "disabled": true,
            "include_form": true,
            "target_value": "entryNumber",
            "target_column": "entryDbId",
            "variable_type": "metadata",
            "variable_abbrev": "ENTRY_NUMBER"
        },
        {
            "disabled": true,
            "include_form": true,
            "target_value": "plotCode",
            "target_column": "plotDbId",
            "variable_type": "metadata",
            "variable_abbrev": "PLOT_CODE"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_2_SETTINGS';

UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Role collaborator configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "MC_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "MC_CONT",
            "entity_data_type": "trait-data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "REMARKS",
            "target_column": "REMARKS",
            "variable_type": "metadata",
            "variable_abbrev": "REMARKS",
            "entity_data_type": "plot-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_3_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "seasonCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Season",
--rollback                 "api_resource_sort": "sort=seasonCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "seasons"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "Post_Harvest_Unit",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post_Harvest_Unit",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_UNIT",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": ""
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "Post Harvest completion date",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post Harvest completion date",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for the second row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Occurrence name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "occurrence_name",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME",
--rollback                 "api_resource_sort": "sort=occurrenceName",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "occurrences-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Germplasm Name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "designation",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "DESIGNATION",
--rollback                 "api_resource_sort": "sort=designation",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "germplasm-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Entry no",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "entry_number",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "ENTRY_NUMBER",
--rollback                 "api_resource_sort": "sort=entryNumber",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "entries-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Plot_code",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "plot_code",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PLOT_CODE",
--rollback                 "api_resource_sort": "sort=plotCode",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "plots-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_2_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "AYLD_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "AYLD_CONT",
--rollback                 "api_resource_sort": "sort=AYLD_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "MC_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "MC_CONT",
--rollback                 "api_resource_sort": "sort=MC_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "Notes",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "notes",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "NOTES",
--rollback                 "api_resource_sort": "sort=notes",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "plots"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program BW-CIMMYT configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program BW-CIMMYT configuration for the second row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Germplasm Name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "designation",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "DESIGNATION",
--rollback                 "api_resource_sort": "sort=designation",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "germplasm-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_2_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program BW-CIMMYT configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "AYLD_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "AYLD_CONT",
--rollback                 "api_resource_sort": "sort=AYLD_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "Notes",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "notes",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "NOTES",
--rollback                 "api_resource_sort": "sort=notes",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "plots"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_3_SETTINGS';
--rollback 
--rollback
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "seasonCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Season",
--rollback                 "api_resource_sort": "sort=seasonCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "seasons"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the second row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Occurrence name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "occurrence_name",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME",
--rollback                 "api_resource_sort": "sort=occurrenceName",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "occurrences-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Germplasm Name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "designation",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "DESIGNATION",
--rollback                 "api_resource_sort": "sort=designation",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "germplasm-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_2_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "AYLD_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "AYLD_CONT",
--rollback                 "api_resource_sort": "sort=AYLD_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program KE configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program KE configuration for the second row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Occurrence name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "occurrence_name",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME",
--rollback                 "api_resource_sort": "sort=occurrenceName",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "occurrences-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_2_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program KE configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "MC_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "MC_CONT",
--rollback                 "api_resource_sort": "sort=MC_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_3_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Role configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "seasonCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Season",
--rollback                 "api_resource_sort": "sort=seasonCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "seasons"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "Post_Harvest_Unit",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post_Harvest_Unit",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_UNIT",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Role configuration for the second row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Occurrence name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "occurrence_name",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "OCCURRENCE_NAME",
--rollback                 "api_resource_sort": "sort=occurrenceName",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "occurrences-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Germplasm Name",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "designation",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "DESIGNATION",
--rollback                 "api_resource_sort": "sort=designation",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "germplasm-search"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "target_value": "Entry no",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "entry_number",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "ENTRY_NUMBER",
--rollback                 "api_resource_sort": "sort=entryNumber",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "entries-search"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_2_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Role configuration for the third row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "AYLD_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "AYLD_CONT",
--rollback                 "api_resource_sort": "sort=AYLD_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "target_value": "MC_CONT",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "value",
--rollback                 "variable_type": "observation",
--rollback                 "variable_abbrev": "MC_CONT",
--rollback                 "api_resource_sort": "sort=MC_CONT",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "terminal-transactions/{id}/dataset-table"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_3_SETTINGS';