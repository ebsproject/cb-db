--liquibase formatted sql

--changeset postgres:update_config_value_for_manage_occurrence_data context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-1639: CB-EM: request to allow user to update location name



UPDATE
    platform.config
SET
    config_value = $${
        "Name": "Default configuration for manage occurrence-level data",
        "Values": [
            {
                "required": "required",
                "target_column": "occurrenceName",
                "variable_type": "identification",
                "variable_abbrev": "OCCURRENCE_NAME"
            },
            {
                "required": "required",
                "target_value": "geospatialObjectName",
                "target_column": "siteDbId",
                "variable_type": "identification",
                "variable_abbrev": "SITE",
                "api_resource_filter": {
                    "geospatialObjectType": "site"
                },
                "api_resource_method": "POST",
                "api_resource_endpoint": "geospatial-objects-search",
                "secondary_target_column": "geospatialObjectDbId"
            },
            {
                "target_value": "scaleName",
                "target_column": "fieldDbId",
                "variable_type": "identification",
                "variable_abbrev": "FIELD",
                "api_resource_filter": {
                    "geospatialObjectType": "field"
                },
                "api_resource_method": "POST",
                "api_resource_endpoint": "geospatial-objects-search",
                "secondary_target_column": "geospatialObjectDbId"
            },
            {
                "disabled": false,
                "target_column": "description",
                "variable_type": "identification",
                "variable_abbrev": "DESCRIPTION"
            },
            {
                "target_value": "personName",
                "allow_new_val": true,
                "target_column": "contactPerson",
                "variable_type": "metadata",
                "variable_abbrev": "CONTCT_PERSON_CONT",
                "api_resource_sort": "sort=personName",
                "api_resource_method": "GET",
                "api_resource_endpoint": "persons",
                "secondary_target_column": "personDbId"
            },
            {
                "target_column": "locationName",
                "variable_type": "identification",
                "variable_abbrev": "LOCATION"
            }
        ]
    }$$
WHERE abbrev = 'MANAGE_OCCURRENCE_DATA_DEFAULT'
;

UPDATE 
    platform.config
SET 
    config_value = $${
        "Name": "Default configuration for manage occurrence-level data",
        "Values": [
            {
                "BREEDING_TRIAL_DATA_PROCESS": [
                    {
                        "required": "required",
                        "target_column": "occurrenceName",
                        "variable_type": "identification",
                        "variable_abbrev": "OCCURRENCE_NAME"
                    },
                    {
                        "required": "required",
                        "target_value": "geospatialObjectName",
                        "target_column": "siteDbId",
                        "variable_type": "identification",
                        "variable_abbrev": "SITE",
                        "api_resource_filter": {
                            "geospatialObjectType": "site"
                        },
                        "api_resource_method": "POST",
                        "api_resource_endpoint": "geospatial-objects-search",
                        "secondary_target_column": "geospatialObjectDbId"
                    },
                    {
                        "target_value": "scaleName",
                        "target_column": "fieldDbId",
                        "variable_type": "identification",
                        "variable_abbrev": "FIELD",
                        "api_resource_filter": {
                            "geospatialObjectType": "field"
                        },
                        "api_resource_method": "POST",
                        "api_resource_endpoint": "geospatial-objects-search",
                        "secondary_target_column": "geospatialObjectDbId"
                    },
                    {
                        "target_column": "description",
                        "variable_type": "identification",
                        "variable_abbrev": "DESCRIPTION"
                    },
                    {
                        "target_value": "personName",
                        "allow_new_val": true,
                        "target_column": "contactPerson",
                        "variable_type": "metadata",
                        "variable_abbrev": "CONTCT_PERSON_CONT",
                        "api_resource_sort": "sort=personName",
                        "api_resource_method": "GET",
                        "api_resource_endpoint": "persons",
                        "secondary_target_column": "personDbId"
                    },
                    {
                        "target_column": "plantingDate",
                        "variable_type": "metadata",
                        "variable_abbrev": "PLANTING_DATE"
                    },
                    {
                        "target_column": "locationName",
                        "variable_type": "identification",
                        "variable_abbrev": "LOCATION"
                    }
                ]
            }
        ]
    }$$,
    notes = 'updated by o.panzo'
WHERE 
    abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA'
;



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for manage occurrence-level data",
--rollback         "Values": [
--rollback             {
--rollback             "required": "required",
--rollback             "target_column": "occurrenceName",
--rollback             "variable_type": "identification",
--rollback             "variable_abbrev": "OCCURRENCE_NAME"
--rollback             },
--rollback             {
--rollback             "required": "required",
--rollback             "target_value": "geospatialObjectName",
--rollback             "target_column": "siteDbId",
--rollback             "variable_type": "identification",
--rollback             "variable_abbrev": "SITE",
--rollback             "api_resource_filter": {
--rollback                 "geospatialObjectType": "site"
--rollback             },
--rollback             "api_resource_method": "POST",
--rollback             "api_resource_endpoint": "geospatial-objects-search",
--rollback             "secondary_target_column": "geospatialObjectDbId"
--rollback             },
--rollback             {
--rollback             "target_value": "scaleName",
--rollback             "target_column": "fieldDbId",
--rollback             "variable_type": "identification",
--rollback             "variable_abbrev": "FIELD",
--rollback             "api_resource_filter": {
--rollback                 "geospatialObjectType": "field"
--rollback             },
--rollback             "api_resource_method": "POST",
--rollback             "api_resource_endpoint": "geospatial-objects-search",
--rollback             "secondary_target_column": "geospatialObjectDbId"
--rollback             },
--rollback             {
--rollback             "disabled": false,
--rollback             "target_column": "description",
--rollback             "variable_type": "identification",
--rollback             "variable_abbrev": "DESCRIPTION"
--rollback             },
--rollback             {
--rollback             "target_value": "personName",
--rollback             "allow_new_val": true,
--rollback             "target_column": "contactPerson",
--rollback             "variable_type": "metadata",
--rollback             "variable_abbrev": "CONTCT_PERSON_CONT",
--rollback             "api_resource_sort": "sort=personName",
--rollback             "api_resource_method": "GET",
--rollback             "api_resource_endpoint": "persons",
--rollback             "secondary_target_column": "personDbId"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE abbrev = 'MANAGE_OCCURRENCE_DATA_DEFAULT'
--rollback ;
--rollback
--rollback UPDATE 
--rollback     platform.config
--rollback SET 
--rollback     config_value = $${
--rollback             "Name": "Default configuration for manage occurrence-level data",
--rollback             "Values": [
--rollback                 {
--rollback                     "BREEDING_TRIAL_DATA_PROCESS": [
--rollback                         {
--rollback                             "required": "required",
--rollback                             "target_column": "occurrenceName",
--rollback                             "variable_type": "identification",
--rollback                             "variable_abbrev": "OCCURRENCE_NAME"
--rollback                         },
--rollback                         {
--rollback                             "required": "required",
--rollback                             "target_value": "geospatialObjectName",
--rollback                             "target_column": "siteDbId",
--rollback                             "variable_type": "identification",
--rollback                             "variable_abbrev": "SITE",
--rollback                             "api_resource_filter": {
--rollback                                 "geospatialObjectType": "site"
--rollback                             },
--rollback                             "api_resource_method": "POST",
--rollback                             "api_resource_endpoint": "geospatial-objects-search",
--rollback                             "secondary_target_column": "geospatialObjectDbId"
--rollback                         },
--rollback                         {
--rollback                             "target_value": "scaleName",
--rollback                             "target_column": "fieldDbId",
--rollback                             "variable_type": "identification",
--rollback                             "variable_abbrev": "FIELD",
--rollback                             "api_resource_filter": {
--rollback                                 "geospatialObjectType": "field"
--rollback                             },
--rollback                             "api_resource_method": "POST",
--rollback                             "api_resource_endpoint": "geospatial-objects-search",
--rollback                             "secondary_target_column": "geospatialObjectDbId"
--rollback                         },
--rollback                         {
--rollback                             "target_column": "description",
--rollback                             "variable_type": "identification",
--rollback                             "variable_abbrev": "DESCRIPTION"
--rollback                         },
--rollback                         {
--rollback                             "target_value": "personName",
--rollback                             "allow_new_val": true,
--rollback                             "target_column": "contactPerson",
--rollback                             "variable_type": "metadata",
--rollback                             "variable_abbrev": "CONTCT_PERSON_CONT",
--rollback                             "api_resource_sort": "sort=personName",
--rollback                             "api_resource_method": "GET",
--rollback                             "api_resource_endpoint": "persons",
--rollback                             "secondary_target_column": "personDbId"
--rollback                         },
--rollback                         {
--rollback                             "target_column": "plantingDate",
--rollback                             "variable_type": "metadata",
--rollback                             "variable_abbrev": "PLANTING_DATE"
--rollback                         }
--rollback                     ]
--rollback                 }
--rollback             ]
--rollback     }$$
--rollback WHERE 
--rollback     abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA'
--rollback ;

