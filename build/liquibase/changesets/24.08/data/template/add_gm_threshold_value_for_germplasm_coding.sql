--liquibase formatted sql

--changeset postgres:add_post_harvest_printouts_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2936 CB-GM DB: Add threshold value for germplasm coding (5000)



UPDATE
    platform.config
SET
    config_value = $${
        "searchCharMin": {
            "size": "3",
            "description": "Minimum number of characters when searching."
        },
        "ancestryExportDepth": {
            "size": "10",
            "description": "Maximum depth of exported germplasm ancestry information."
        },
        "germplasmCodingMax": {
            "size" : "5000",
            "description" : "Maximum threshold value when processing germplasm coding"
        }
    }$$
WHERE 
    abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD'
;



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "searchCharMin": {
--rollback             "size": "3",
--rollback             "description": "Minimum number of characters when searching."
--rollback         },
--rollback         "ancestryExportDepth": {
--rollback             "size": "10",
--rollback             "description": "Maximum depth of exported germplasm ancestry information."
--rollback         }
--rollback     }$$
--rollback WHERE 
--rollback     abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD'
--rollback ;

