--liquibase formatted sql

--changeset postgres:add_experiments_update_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2876 CB-BPM-DB: Create changeset for experiments_update config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'EXPERIMENTS_UPDATE_GLOBAL',
        'Experiments Update Global',
        '{
            "Name": "Default configuration for update experiment-level data",
            "Values": [
                {  
                    "target_value": "personName",
                    "allow_new_val": true,
                    "target_column": "stewardDbId",
                    "variable_type": "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "api_resource_sort": "sort=personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "secondary_target_column": "personDbId"
                },
                {  
                    "target_value": "Product_Development_Stage",
                    "allow_new_val": true,
                    "target_column": "Product_Development_Stage",
                    "variable_type": "metadata",
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                },
                {  
                    "target_value": "Description",
                    "allow_new_val": true,
                    "target_column": "Description",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                }
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_UPDATE_ROLE_COLLABORATOR',
        'Experiments Update Role Collaborator',
        '{
            "Name": "Default configuration for update experiment-level data",
            "Values": [
                {  
                    "target_value": "personName",
                    "allow_new_val": true,
                    "target_column": "stewardDbId",
                    "variable_type": "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "api_resource_sort": "sort=personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "secondary_target_column": "personDbId"
                },
                {  
                    "target_value": "Product_Development_Stage",
                    "allow_new_val": true,
                    "target_column": "Product_Development_Stage",
                    "variable_type": "metadata",
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                },
                {  
                    "target_value": "Description",
                    "allow_new_val": true,
                    "target_column": "Description",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                }
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_UPDATE_PROGRAM_IRSEA',
        'Experiments Update Program IRSEA',
        '{
            "Name": "Default configuration for update experiment-level data",
            "Values": [
                {  
                    "target_value": "personName",
                    "allow_new_val": true,
                    "target_column": "stewardDbId",
                    "variable_type": "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "api_resource_sort": "sort=personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "secondary_target_column": "personDbId"
                },
                {  
                    "target_value": "Product_Development_Stage",
                    "allow_new_val": true,
                    "target_column": "Product_Development_Stage",
                    "variable_type": "metadata",
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                },
                {  
                    "target_value": "Description",
                    "allow_new_val": true,
                    "target_column": "Description",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                }
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_UPDATE_PROGRAM_KE',
        'Experiments Update Program KE',
        '{
            "Name": "Default configuration for update experiment-level data",
            "Values": [
                {  
                    "target_value": "personName",
                    "allow_new_val": true,
                    "target_column": "stewardDbId",
                    "variable_type": "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "api_resource_sort": "sort=personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "secondary_target_column": "personDbId"
                },
                {  
                    "target_value": "Product_Development_Stage",
                    "allow_new_val": true,
                    "target_column": "Product_Development_Stage",
                    "variable_type": "metadata",
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                },
                {  
                    "target_value": "Description",
                    "allow_new_val": true,
                    "target_column": "Description",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                }
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_UPDATE_PROGRAM_BW-CIMMYT',
        'Experiments Update Program BW-CIMMYT',
        '{
            "Name": "Default configuration for update experiment-level data",
            "Values": [
                {  
                    "target_value": "personName",
                    "allow_new_val": true,
                    "target_column": "stewardDbId",
                    "variable_type": "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "api_resource_sort": "sort=personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "secondary_target_column": "personDbId"
                },
                {  
                    "target_value": "Product_Development_Stage",
                    "allow_new_val": true,
                    "target_column": "Product_Development_Stage",
                    "variable_type": "metadata",
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                },
                {  
                    "target_value": "Description",
                    "allow_new_val": true,
                    "target_column": "Description",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESCRIPTION",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "secondary_target_column": ""
                }
            ]
        }',
        'breeding_program_manager'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_UPDATE_GLOBAL';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_UPDATE_ROLE_COLLABORATOR';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_UPDATE_PROGRAM_IRSEA';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_UPDATE_PROGRAM_KE';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_UPDATE_PROGRAM_BW-CIMMYT';