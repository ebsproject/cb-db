--liquibase formatted sql

--changeset postgres:update_dc_other_file_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3042 CB-DC-DB: Add Field Book app file upload option in other file config



UPDATE platform.config
SET
    config_value = $$
        {
            "Field Book": {
                "Upload": [
                    {
                        "abbrev": "PLOT_ID",
                        "header": "ObservationUnitDbId",
                        "attribute": "plotDbId",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "abbrev": "HVDATE_CONT",
                        "header": "harvest date",
                        "attribute": "HVDATE_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "HV_METH_DISC",
                        "header": "Harvest Method",
                        "attribute": "HV_METH_DISC",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_PLANTS",
                        "header": "No Of Plants Selected",
                        "attribute": "NO_OF_PLANTS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_BAGS",
                        "header": "No. of Bags",
                        "attribute": "NO_OF_BAGS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BB_SES5_GH_SCOR_1_9",
                        "header": "Bacterial blight - GH, SES5",
                        "attribute": "BB_SES5_GH_SCOR_1_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BL_NURS_0_9",
                        "header": "Leaf blast",
                        "attribute": "BL_NURS_0_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "GRAIN_COLOR",
                        "header": "Grain Color",
                        "attribute": "GRAIN_COLOR",
                        "identifier": false,
                        "isObservation": true
                    }

                ]
            },
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "GID",
                        "attribute": "germplasmCode",
                        "abbrev": "GERMPLASM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                      {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                     {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            }
        }
    $$
WHERE
    abbrev = 'DC_GLOBAL_OTHER_FILE_SETTINGS';

UPDATE platform.config
SET
    config_value = $$
        {
            "Field Book": {
                "Upload": [
                    {
                        "abbrev": "PLOT_ID",
                        "header": "ObservationUnitDbId",
                        "attribute": "plotDbId",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "abbrev": "HVDATE_CONT",
                        "header": "harvest date",
                        "attribute": "HVDATE_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "HV_METH_DISC",
                        "header": "Harvest Method",
                        "attribute": "HV_METH_DISC",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_PLANTS",
                        "header": "No Of Plants Selected",
                        "attribute": "NO_OF_PLANTS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_BAGS",
                        "header": "No. of Bags",
                        "attribute": "NO_OF_BAGS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BB_SES5_GH_SCOR_1_9",
                        "header": "Bacterial blight - GH, SES5",
                        "attribute": "BB_SES5_GH_SCOR_1_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BL_NURS_0_9",
                        "header": "Leaf blast",
                        "attribute": "BL_NURS_0_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "GRAIN_COLOR",
                        "header": "Grain Color",
                        "attribute": "GRAIN_COLOR",
                        "identifier": false,
                        "isObservation": true
                    }

                ]
            },
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "GID",
                        "attribute": "germplasmCode",
                        "abbrev": "GERMPLASM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            }
        }
    $$
WHERE
    abbrev = 'DC_IRSEA_OTHER_FILE_SETTINGS';

UPDATE platform.config
SET
    config_value = $$
        {
            "Field Book": {
                "Upload": [
                    {
                        "abbrev": "PLOT_ID",
                        "header": "ObservationUnitDbId",
                        "attribute": "plotDbId",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "abbrev": "HVDATE_CONT",
                        "header": "harvest date",
                        "attribute": "HVDATE_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "HV_METH_DISC",
                        "header": "Harvest Method",
                        "attribute": "HV_METH_DISC",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_PLANTS",
                        "header": "No Of Plants Selected",
                        "attribute": "NO_OF_PLANTS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "NO_OF_BAGS",
                        "header": "No. of Bags",
                        "attribute": "NO_OF_BAGS",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BB_SES5_GH_SCOR_1_9",
                        "header": "Bacterial blight - GH, SES5",
                        "attribute": "BB_SES5_GH_SCOR_1_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "BL_NURS_0_9",
                        "header": "Leaf blast",
                        "attribute": "BL_NURS_0_9",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "abbrev": "GRAIN_COLOR",
                        "header": "Grain Color",
                        "attribute": "GRAIN_COLOR",
                        "identifier": false,
                        "isObservation": true
                    }

                ]
            },
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "PLOT_ID",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            }
        }
    $$
WHERE
    abbrev = 'DC_COLLABORATOR_OTHER_FILE_SETTINGS';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "Easy Harvest": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PROGRAM",
--rollback                         "attribute": "programCode",
--rollback                         "abbrev": "PROGRAM_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AREA",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "RESEARCHER",
--rollback                         "attribute": "contactPerson",
--rollback                         "abbrev": "RESEARCHER",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "STUDY",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "HARVYR",
--rollback                         "attribute": "experimentYear",
--rollback                         "abbrev": "EXPERIMENT_YEAR",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SEASON",
--rollback                         "attribute": "experimentSeason",
--rollback                         "abbrev": "SEASON",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DEEP/ROW",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "WIDE/COLUMN",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "ENTNO",
--rollback                         "attribute": "entryNumber",
--rollback                         "abbrev": "ENTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "GID",
--rollback                         "attribute": "germplasmCode",
--rollback                         "abbrev": "GERMPLASM_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_KEY",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "Field",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Trial",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plots deep(Y)",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plot wide(X)",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Weight",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Density",
--rollback                         "attribute": "DENSITY_CONT",
--rollback                         "abbrev": "DENSITY_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "User",
--rollback                         "attribute": "USER",
--rollback                         "abbrev": "USER",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Date",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Time",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SequenceNo",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Qrcode",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "Moisture Meter 1": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "KSU Inventory": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                       {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                      {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'DC_GLOBAL_OTHER_FILE_SETTINGS';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "Easy Harvest": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PROGRAM",
--rollback                         "attribute": "programCode",
--rollback                         "abbrev": "PROGRAM_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AREA",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "RESEARCHER",
--rollback                         "attribute": "contactPerson",
--rollback                         "abbrev": "RESEARCHER",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "STUDY",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "HARVYR",
--rollback                         "attribute": "experimentYear",
--rollback                         "abbrev": "EXPERIMENT_YEAR",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SEASON",
--rollback                         "attribute": "experimentSeason",
--rollback                         "abbrev": "SEASON",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DEEP/ROW",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "WIDE/COLUMN",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "ENTNO",
--rollback                         "attribute": "entryNumber",
--rollback                         "abbrev": "ENTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "GID",
--rollback                         "attribute": "germplasmCode",
--rollback                         "abbrev": "GERMPLASM_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_KEY",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "Field",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Trial",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plots deep(Y)",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plot wide(X)",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Weight",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Density",
--rollback                         "attribute": "DENSITY_CONT",
--rollback                         "abbrev": "DENSITY_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "User",
--rollback                         "attribute": "USER",
--rollback                         "abbrev": "USER",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Date",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Time",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SequenceNo",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Qrcode",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "Moisture Meter 1": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "KSU Inventory": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DESIGNATION",
--rollback                         "attribute": "entryName",
--rollback                         "abbrev": "DESIGNATION",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'DC_IRSEA_OTHER_FILE_SETTINGS';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "Easy Harvest": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PROGRAM",
--rollback                         "attribute": "programCode",
--rollback                         "abbrev": "PROGRAM_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AREA",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "RESEARCHER",
--rollback                         "attribute": "contactPerson",
--rollback                         "abbrev": "RESEARCHER",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "STUDY",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "HARVYR",
--rollback                         "attribute": "experimentYear",
--rollback                         "abbrev": "EXPERIMENT_YEAR",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SEASON",
--rollback                         "attribute": "experimentSeason",
--rollback                         "abbrev": "SEASON",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "DEEP/ROW",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "WIDE/COLUMN",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "ENTNO",
--rollback                         "attribute": "entryNumber",
--rollback                         "abbrev": "ENTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_KEY",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "Field",
--rollback                         "attribute": "field",
--rollback                         "abbrev": "FIELD",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Trial",
--rollback                         "attribute": "occurrenceName",
--rollback                         "abbrev": "OCCURRENCE_NAME",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plots deep(Y)",
--rollback                         "attribute": "paY",
--rollback                         "abbrev": "PA_Y",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Plot wide(X)",
--rollback                         "attribute": "paX",
--rollback                         "abbrev": "PA_X",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Weight",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "Density",
--rollback                         "attribute": "DENSITY_CONT",
--rollback                         "abbrev": "DENSITY_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "User",
--rollback                         "attribute": "USER",
--rollback                         "abbrev": "USER",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Date",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Time",
--rollback                         "attribute": "collectionTimestamp",
--rollback                         "abbrev": "COLLECTION_TIMESTAMP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "SequenceNo",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "Qrcode",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "Moisture Meter 1": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "plotcode",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "plotno",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "replication",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "moisture",
--rollback                         "attribute": "MC_CONT",
--rollback                         "abbrev": "MC_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "KSU Inventory": {
--rollback                 "Export": [
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "visible": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "visible": true,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "visible": true,
--rollback                         "isObservation": true
--rollback                     }
--rollback                 ],
--rollback                 "Upload": [
--rollback                     {
--rollback                         "header": "PLOT_CODE",
--rollback                         "attribute": "plotCode",
--rollback                         "abbrev": "PLOT_CODE",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOTNO",
--rollback                         "attribute": "plotNumber",
--rollback                         "abbrev": "PLOTNO",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "REP",
--rollback                         "attribute": "rep",
--rollback                         "abbrev": "REP",
--rollback                         "identifier": false,
--rollback                         "isObservation": false
--rollback                     },
--rollback                     {
--rollback                         "header": "AYLD_CONT",
--rollback                         "attribute": "AYLD_CONT",
--rollback                         "abbrev": "AYLD_CONT",
--rollback                         "identifier": false,
--rollback                         "isObservation": true
--rollback                     },
--rollback                     {
--rollback                         "header": "PLOT_ID",
--rollback                         "attribute": "plotDbId",
--rollback                         "abbrev": "PLOT_ID",
--rollback                         "identifier": true,
--rollback                         "isObservation": false
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'DC_COLLABORATOR_OTHER_FILE_SETTINGS';