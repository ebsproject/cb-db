--liquibase formatted sql

--changeset postgres:add_variable_post_harvest_unit context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.variable WHERE abbrev = 'POST_HARVEST_UNIT') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2900 CB-DC-DB: Create changeset for adding new variable POST_HARVEST_UNIT and POST_HARVEST_COMPLETION_DATE



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name) 
    VALUES 
        ('POST_HARVEST_UNIT', 'POST HARVEST UNIT', 'Post Harvest Unit', 'character varying', false, 'metadata', 'plot', 'occurrence', 'Post Harvest Unit', 'active', '1', 'Post Harvest Unit')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'POST_HARVEST_UNIT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('POST_HARVEST_UNIT', 'Post Harvest Unit', 'Post Harvest Unit') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('POST_HARVEST_UNIT_METHOD', 'Post Harvest Unit method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('POST_HARVEST_UNIT_SCALE', 'Post Harvest Unit scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'POST_HARVEST_UNIT'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'POST_HARVEST_UNIT'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback DELETE FROM
--rollback     master.property AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'POST_HARVEST_UNIT'
--rollback     AND t.id = var.property_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'POST_HARVEST_UNIT'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'POST_HARVEST_UNIT'
--rollback ;