--liquibase formatted sql

--changeset postgres:update_controller_action_bpm context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.application_action WHERE module = 'breedingProgramManager') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2681 CB-BPM: Implement RBAC and entry point for MVI


-- update value of controller column
UPDATE
    platform.application_action 
SET
    controller = 'default'
WHERE 
    module = 'breedingProgramManager';


-- update value of action column
UPDATE
    platform.application_action 
SET
    action = 'index'
WHERE 
    module = 'breedingProgramManager';

--rollback UPDATE
--rollback     platform.application_action 
--rollback SET
--rollback     controller = 'index'
--rollback WHERE 
--rollback     module = 'breedingProgramManager';
--rollback UPDATE
--rollback     platform.application_action 
--rollback SET
--rollback     action = 'default'
--rollback WHERE 
--rollback     module = 'breedingProgramManager';