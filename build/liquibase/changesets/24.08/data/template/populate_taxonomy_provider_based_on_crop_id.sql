--liquibase formatted sql

--changeset postgres:populate_taxonomy_provider_based_on_crop_id context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2498 CB-DB: Populate new column germplasm.taxonomy.taxonomy_provider for BrAPI taxonId sourceName



UPDATE
	germplasm.taxonomy
SET
    taxonomy_provider = CASE
        WHEN (crop_id = (SELECT id FROM tenant.crop WHERE crop_code = 'RICE'))
            THEN 'IRRI Genebank'
        WHEN (crop_id = (SELECT id FROM tenant.crop WHERE crop_code = 'WHEAT'))
            THEN 'GRIN-Global'
        WHEN (crop_id = (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE'))
            THEN 'GRIN-Global'
        ELSE 'Unconfirmed'
    END;



--rollback UPDATE
--rollback     germplasm.taxonomy
--rollback SET
--rollback     taxonomy_provider = null;