--liquibase formatted sql

--changeset postgres:add_germplasm_coding_name_type_config_MAIZE context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'GM_CROP_MAIZE_GERMPLASM_CODING_NAME_TYPE_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2938: CB-GM DB: Insert configs for germplasm name types available for Maize, Rice and GLOBAL (germplasm coding)



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_CROP_MAIZE_GERMPLASM_CODING_NAME_TYPE_CONFIG',
        'Germplasm Manager Germplasm Coding GERMPLASM_NAME_TYPE config for MAIZE',
        $${
            "germplasm_name_type":[
                "GERMPLASM_NAME_TYPE_HYBRID_CODE",
                "GERMPLASM_NAME_TYPE_LINE_CODE"
            ]
        }$$,
        1,
        'Germplasm Manager Germplasm Coding GERMPLASM_NAME_TYPE options',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-2938: CB-GM DB: Insert configs for germplasm name types available for Maize, Rice and GLOBAL (germplasm coding) - k.delarosa'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='GM_CROP_MAIZE_GERMPLASM_CODING_NAME_TYPE_CONFIG';