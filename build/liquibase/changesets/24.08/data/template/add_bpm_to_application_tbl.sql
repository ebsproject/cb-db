--liquibase formatted sql

--changeset postgres:add_bpm_to_application_tbl context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.application WHERE abbrev = 'BREEDING_PROGRAM_MANAGER') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2681 CB-BPM: Implement RBAC and entry point for MVI


INSERT INTO platform.application 
(
    abbrev,
    "label",
    action_label,
    icon,
    description,
    creator_id
) values (
    'BREEDING_PROGRAM_MANAGER',
    'Breeding Manager Program',
    'Breeding Manager Program',
    'filter_none',
    'Browse and manage pipelines, market segments, and experiments.',
     (
        SELECT
            id
        FROM
            tenant.person
        WHERE
            email = 'admin@ebsproject.org'
    )
);

--rollback DELETE FROM platform.application WHERE abbrev='BREEDING_PROGRAM_MANAGER';