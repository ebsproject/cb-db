--liquibase formatted sql

--changeset postgres:update_irsea_post_harvest_row_1_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2544	CB-DB: Update IRSEA post harvest row 1 setting - Add ZES default value for POST_HARVEST_UNIT



UPDATE 
    platform.config
SET
    config_value = $${
        "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
        "Values": [
            {
                "include_form": true,
                "disabled": true,
                "target_value": "programCode",
                "target_column": "programDbId",
                "variable_abbrev": "PROGRAM",
                "variable_type": "metadata"
            },
            {
                "include_form": true,
                "disabled": true,
                "target_value": "experimentYear",
                "target_column": "experimentYear",
                "variable_abbrev": "EXPERIMENT_YEAR"
            },
            {
                "include_form": true,
                "disabled": true,
                "target_value": "seasonCode",
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "SEASON"
            },
            {
                "default": "ZES Team",
                "disabled": false,
                "target_value": "data_value",
                "target_column": "occurrenceDataDbId",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_UNIT",
                "entity_data_type": "occurrence-data"
            },
            {
                "default": "",
                "disabled": false,
                "target_value": "data_value",
                "target_column": "occurrenceDataDbId",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
                "entity_data_type": "occurrence-data"
            }
        ]
    }$$
WHERE
	abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {
--rollback                 "include_form": true,
--rollback                 "disabled": true,
--rollback                 "target_value": "programCode",
--rollback                 "target_column": "programDbId",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "variable_type": "metadata"
--rollback             },
--rollback             {
--rollback                 "include_form": true,
--rollback                 "disabled": true,
--rollback                 "target_value": "experimentYear",
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_abbrev": "EXPERIMENT_YEAR"
--rollback             },
--rollback             {
--rollback                 "include_form": true,
--rollback                 "disabled": true,
--rollback                 "target_value": "seasonCode",
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "SEASON"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "target_value": "data_value",
--rollback                 "target_column": "occurrenceDataDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_UNIT",
--rollback                 "entity_data_type": "occurrence_data"
--rollback             },
--rollback             {
--rollback                 "default": "",
--rollback                 "disabled": false,
--rollback                 "target_value": "data_value",
--rollback                 "target_column": "occurrenceDataDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
--rollback                 "entity_data_type": "occurrence_data"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS'
--rollback ;