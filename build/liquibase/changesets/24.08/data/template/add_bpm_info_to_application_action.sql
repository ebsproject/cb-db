--liquibase formatted sql

--changeset postgres:add_bpm_info_to_application_action context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.application_action WHERE module = 'breedingProgramManager') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2681 CB-BPM: Implement RBAC and entry point for MVI


INSERT INTO platform.application_action 
(
    application_id,
    "module",
    "action",
    controller,
    creator_id
) values (
    (
        SELECT
            id
        FROM
            platform.application 
        WHERE
            abbrev = 'BREEDING_PROGRAM_MANAGER'
    ),
    'breedingProgramManager',
    'default',
    'index',
    (
        SELECT
            id
        FROM
            tenant.person
        WHERE
            email = 'admin@ebsproject.org'
    )
);

--rollback DELETE FROM platform.application_action WHERE module='breedingProgramManager';