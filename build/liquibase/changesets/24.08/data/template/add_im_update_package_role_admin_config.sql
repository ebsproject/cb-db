--liquibase formatted sql

--changeset postgres:add_im_update_package_role_admin_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'IM_UPDATE_PACKAGE_ROLE_ADMIN') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2972 CB-IM DB: Insert new config for package update - admin role



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'IM_UPDATE_PACKAGE_ROLE_ADMIN',
        'Inventory Manager configuration for admin role variables - package update',
        $$
            {
                "values": [
                    {
                        "name": "Package Code",
                        "type": "column",
                        "view": {
                            "visible": "false",
                            "entities": []
                        },
                        "usage": "required",
                        "abbrev": "PACKAGE_CODE",
                        "entity": "package",
                        "required": "true",
                        "api_field": "packageDbId",
                        "data_type": "string",
                        "http_method": "POST",
                        "skip_update": "false",
                        "value_filter": "packageCode",
                        "retrieve_db_id": "true",
                        "url_parameters": "dataLevel=all&limit=1",
                        "db_id_api_field": "packageDbId",
                        "search_endpoint": "seed-packages-search",
                        "additional_filters": {}
                    },
                    {
                        "name": "Package Label",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "PACKAGE_LABEL",
                        "entity": "package",
                        "required": "false",
                        "api_field": "packageDbId",
                        "data_type": "string",
                        "http_method": "POST",
                        "skip_update": "true",
                        "value_filter": "packageLabel",
                        "retrieve_db_id": "true",
                        "url_parameters": "dataLevel=all&limit=1",
                        "db_id_api_field": "packageDbId",
                        "search_endpoint": "seed-packages-search",
                        "additional_filters": {}
                    },
                    {
                        "name": "Program",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "PROGRAM",
                        "entity": "package",
                        "required": "false",
                        "api_field": "programDbId",
                        "data_type": "string",
                        "http_method": "POST",
                        "skip_update": "false",
                        "value_filter": "programCode",
                        "retrieve_db_id": "true",
                        "url_parameters": "limit=1",
                        "db_id_api_field": "programDbId",
                        "search_endpoint": "programs-search",
                        "additional_filters": {}
                    },
                    {
                        "name": "Package Status",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "PACKAGE_STATUS",
                        "entity": "package",
                        "required": "false",
                        "api_field": "packageStatus",
                        "data_type": "string",
                        "http_method": "",
                        "skip_update": "false",
                        "value_filter": "",
                        "retrieve_db_id": "false",
                        "url_parameters": "",
                        "db_id_api_field": "",
                        "search_endpoint": "",
                        "additional_filters": {}
                    },
                    {
                        "name": "Package Quantity",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "VOLUME",
                        "entity": "package",
                        "required": "false",
                        "api_field": "packageQuantity",
                        "data_type": "float",
                        "http_method": "",
                        "skip_update": "false",
                        "value_filter": "",
                        "retrieve_db_id": "false",
                        "url_parameters": "",
                        "db_id_api_field": "",
                        "search_endpoint": "",
                        "additional_filters": {}
                    },
                    {
                        "name": "Package Unit",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "PACKAGE_UNIT",
                        "entity": "package",
                        "required": "false",
                        "api_field": "packageUnit",
                        "data_type": "string",
                        "http_method": "",
                        "skip_update": "false",
                        "value_filter": "",
                        "retrieve_db_id": "false",
                        "url_parameters": "",
                        "db_id_api_field": "",
                        "search_endpoint": "",
                        "additional_filters": {}
                    },
                    {
                        "name": "Facility Code",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "FACILITY_CODE",
                        "entity": "package",
                        "required": "false",
                        "api_field": "facilityDbId",
                        "data_type": "string",
                        "http_method": "POST",
                        "skip_update": "false",
                        "value_filter": "facilityCode",
                        "retrieve_db_id": "true",
                        "url_parameters": "limit=1",
                        "db_id_api_field": "facilityDbId",
                        "search_endpoint": "facilities-search",
                        "additional_filters": {}
                    }
                ]
            }
        $$,
        1,
        'Inventory Manager file upload and data validation',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-2972 CB-IM DB: Insert new config for package update - admin role - j.bantay'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='IM_UPDATE_PACKAGE_ROLE_ADMIN';