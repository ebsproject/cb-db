--liquibase formatted sql

--changeset postgres:update_post_harvest_row_1_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2901	CB-DB: Update config value from 'POST_HARVEST_PROGRAM' to 'POST_HARVEST_UNIT'



UPDATE 
    platform.config
SET
    config_value = $${
        "Name": "Default configuration for the first row in the post-harvest data collection page",
        "Values": [
            {  
                "target_value": "programCode",
                "allow_new_val": false,
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM",
                "api_resource_sort": "sort=programCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "programs",
                "disabled": false,
                "required": "required",
                "default": ""
            },
            {  
                "target_value": "experimentYear",
                "allow_new_val": false,
                "target_column": "experimentYear",
                "variable_type": "metadata",
                "variable_abbrev": "Year",
                "api_resource_sort": "sort=experimentYear",
                "api_resource_method": "POST",
                "api_resource_endpoint": "experiments-search",
                "disabled": false,
                "required": "required",
                "default": ""
            },
            {  
                "target_value": "seasonCode",
                "allow_new_val": false,
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "Season",
                "api_resource_sort": "sort=seasonCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "seasons",
                "disabled": false,
                "required": "required",
                "default": ""   
            },
            {  
                "target_value": "Post_Harvest_Unit",
                "allow_new_val": true,
                "target_column": "Post_Harvest_Unit",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_UNIT",
                "api_resource_sort": "",
                "api_resource_method": "",
                "api_resource_endpoint": "",
                "disabled": false,
                "required": "",
                "default": ""  
            },
            {  
                "target_value": "Post Harvest completion date",
                "allow_new_val": true,
                "target_column": "Post Harvest completion date",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
                "api_resource_sort": "",
                "api_resource_method": "",
                "api_resource_endpoint": "",
                "disabled": false,
                "required": "",
                "default": ""
            }
        ]
    }$$
WHERE
	abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS'
;

UPDATE 
    platform.config
SET
    config_value = $${
        "Name": "Role configuration for the first row in the post-harvest data collection page",
        "Values": [
            {  
                "target_value": "programCode",
                "allow_new_val": false,
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM",
                "api_resource_sort": "sort=programCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "programs",
                "disabled": false,
                "required": "required",
                "default": ""
            },
            {  
                "target_value": "experimentYear",
                "allow_new_val": false,
                "target_column": "experimentYear",
                "variable_type": "metadata",
                "variable_abbrev": "Year",
                "api_resource_sort": "sort=experimentYear",
                "api_resource_method": "POST",
                "api_resource_endpoint": "experiments-search",
                "disabled": false,
                "required": "required",
                "default": ""
            },
            {  
                "target_value": "seasonCode",
                "allow_new_val": false,
                "target_column": "seasonDbId",
                "variable_type": "metadata",
                "variable_abbrev": "Season",
                "api_resource_sort": "sort=seasonCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "seasons",
                "disabled": false,
                "required": "required",
                "default": ""   
            },
            {  
                "target_value": "Post_Harvest_Unit",
                "allow_new_val": true,
                "target_column": "Post_Harvest_Unit",
                "variable_type": "metadata",
                "variable_abbrev": "POST_HARVEST_UNIT",
                "api_resource_sort": "",
                "api_resource_method": "",
                "api_resource_endpoint": "",
                "disabled": false,
                "required": "",
                "default": ""  
            }
        ]
    }$$
WHERE
	abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Default configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {  
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""
--rollback             },
--rollback             {  
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""
--rollback             },
--rollback             {  
--rollback                 "target_value": "seasonCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Season",
--rollback                 "api_resource_sort": "sort=seasonCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "seasons",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""   
--rollback             },
--rollback             {  
--rollback                 "target_value": "Post_Harvest_Program",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post_Harvest_Program",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_PROGRAM",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "default": ""  
--rollback             },
--rollback             {  
--rollback                 "target_value": "Post Harvest completion date",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post Harvest completion date",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "default": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS'
--rollback ;
--rollback 
--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "Name": "Role configuration for the first row in the post-harvest data collection page",
--rollback         "Values": [
--rollback             {  
--rollback                 "target_value": "programCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "programDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "PROGRAM",
--rollback                 "api_resource_sort": "sort=programCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "programs",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""
--rollback             },
--rollback             {  
--rollback                 "target_value": "experimentYear",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "experimentYear",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Year",
--rollback                 "api_resource_sort": "sort=experimentYear",
--rollback                 "api_resource_method": "POST",
--rollback                 "api_resource_endpoint": "experiments-search",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""
--rollback             },
--rollback             {  
--rollback                 "target_value": "seasonCode",
--rollback                 "allow_new_val": false,
--rollback                 "target_column": "seasonDbId",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "Season",
--rollback                 "api_resource_sort": "sort=seasonCode",
--rollback                 "api_resource_method": "GET",
--rollback                 "api_resource_endpoint": "seasons",
--rollback                 "disabled": false,
--rollback                 "required": "required",
--rollback                 "default": ""   
--rollback             },
--rollback             {  
--rollback                 "target_value": "Post_Harvest_Program",
--rollback                 "allow_new_val": true,
--rollback                 "target_column": "Post_Harvest_Program",
--rollback                 "variable_type": "metadata",
--rollback                 "variable_abbrev": "POST_HARVEST_PROGRAM",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "",
--rollback                 "disabled": false,
--rollback                 "required": "",
--rollback                 "default": ""  
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS'
--rollback ;