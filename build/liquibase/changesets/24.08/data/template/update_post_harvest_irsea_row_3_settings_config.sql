--liquibase formatted sql

--changeset postgres:update_post_harvest_irsea_row_3_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2259	CB-DC-DB: Add IRSEA device parameter for moisture meter



UPDATE 
    platform.config
SET
    config_value = $${
    "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
    "Values": [
        {
            "default": "",
            "disabled": false,
            "target_value": "AYLD_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "AYLD_CONT",
            "entity_data_type": "trait-data"
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "MC_CONT",
            "target_column": "value",
            "variable_type": "observation",
            "variable_abbrev": "MC_CONT",
            "entity_data_type": "trait-data",
            "devices" : [
                {
                    "os": "Linux",
                    "connector": "/dev/ttyUSB0",
                    "baud_rate": 9600,
                    "parity": "None",
                    "data_bits": "Eight",
                    "stop_bits": 1,
                    "flow_control": "None",
                    "input_buffer_size": 1024,
                    "output_buffer_size": 512

                },
                {
                    "os": "Windows",
                    "connector": "COM3",
                    "baud_rate": 9600,
                    "parity": "None",
                    "data_bits": "Eight",
                    "stop_bits": 1,
                    "flow_control": "None",
                    "input_buffer_size": 1024,
                    "output_buffer_size": 512
                }
            ]
        },
        {
            "default": "",
            "disabled": false,
            "target_value": "REMARKS",
            "target_column": "REMARKS",
            "variable_type": "metadata",
            "variable_abbrev": "REMARKS",
            "entity_data_type": "plot-data"
        }
    ]
}$$
WHERE 
    abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback     "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
--rollback     "Values": [
--rollback         {
--rollback             "default": "",
--rollback             "disabled": false,
--rollback             "target_value": "AYLD_CONT",
--rollback             "target_column": "value",
--rollback             "variable_type": "observation",
--rollback             "variable_abbrev": "AYLD_CONT",
--rollback             "entity_data_type": "trait-data"
--rollback         },
--rollback         {
--rollback             "default": "",
--rollback             "disabled": false,
--rollback             "target_value": "MC_CONT",
--rollback             "target_column": "value",
--rollback             "variable_type": "observation",
--rollback             "variable_abbrev": "MC_CONT",
--rollback             "entity_data_type": "trait-data"
--rollback         },
--rollback         {
--rollback             "default": "",
--rollback             "disabled": false,
--rollback             "target_value": "REMARKS",
--rollback             "target_column": "REMARKS",
--rollback             "variable_type": "metadata",
--rollback             "variable_abbrev": "REMARKS",
--rollback             "entity_data_type": "plot-data"
--rollback         }
--rollback     ]
--rollback }$$
--rollback WHERE 
--rollback     abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';