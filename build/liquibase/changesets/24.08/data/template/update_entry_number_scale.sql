--liquibase formatted sql

--changeset postgres:update_entry_number_scale context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3076	CB-DB: Rectify variable ENTRY_NUMBER scale values'

DO $$
DECLARE
    var_scale_id int;
BEGIN
    -- check if ENTRY_NUMBER scale exists
    SELECT id FROM master.scale WHERE abbrev = 'ENTRY_NUMBER' INTO var_scale_id;
    IF var_scale_id IS NULL THEN
        INSERT INTO
            master.scale
            (abbrev,
                name,
                level,
                creator_id,
                notes
            )
        VALUES
            (
                'ENTRY_NUMBER',
                'Entry Number',
                'nominal',
                (
                    SELECT 
                        id
                    FROM
                        tenant.person
                    WHERE 
                        person_name = 'EBS, Admin'
                ),
                'BDS-3076 CB-DB: Rectify variable ENTRY_NUMBER scale values'
            )
        RETURNING id INTO var_scale_id;
    END IF;

    UPDATE
        master.variable
    SET
        scale_id = var_scale_id
    WHERE
        abbrev = 'ENTRY_NUMBER';
END      
$$

--rollback DELETE FROM
--rollback     master.scale s
--rollback USING
--rollback     master.variable v
--rollback WHERE
--rollback     v.abbrev = 'ENTRY_NUMBER'
--rollback     AND s.id = v.scale_id;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET 
--rollback     scale_id = null
--rollback WHERE
--rollback     abbrev = 'ENTRY_NUMBER';