--liquibase formatted sql

--changeset postgres:add_experiments_browser_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2874 CB-BPM-DB: Create changeset for experiments_browser config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'EXPERIMENTS_BROWSER_GLOBAL',
        'Experiments Browser Global',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_ID",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_BROWSER_ROLE_COLLABORATOR',
        'Experiments Browser Role Collaborator',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_BROWSER_PROGRAM_IRSEA',
        'Experiments Browser Program IRSEA',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_ID",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_BROWSER_PROGRAM_KE',
        'Experiments Browser Program KE',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "SEASON_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    ),
    (
        'EXPERIMENTS_BROWSER_PROGRAM_BW-CIMMYT',
        'Experiments Browser Program BW-CIMMYT',
        '{
            "Values": [
                {
                    "variable_abbrev": "BP_SHORT_NAME", 
                    "default": "show"
                },
                {
                    "variable_abbrev": "PRODUCT_DEVELOPMENT_STAGE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STATUS",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_CODE",
                    "default": "hide"
                },
                {
                    "variable_abbrev": "EXPERIMENT_NAME",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "STAGE_CODE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_YEAR",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "default": "show"
                },
                {
                    "variable_abbrev": "EXPERIMENT_STEWARD",
                    "default": "show"
                }    
            ]
        }',
        'breeding_program_manager'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_BROWSER_GLOBAL';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_BROWSER_ROLE_COLLABORATOR';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_BROWSER_PROGRAM_IRSEA';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_BROWSER_PROGRAM_KE';
--rollback DELETE FROM platform.config WHERE abbrev = 'EXPERIMENTS_BROWSER_PROGRAM_BW-CIMMYT';