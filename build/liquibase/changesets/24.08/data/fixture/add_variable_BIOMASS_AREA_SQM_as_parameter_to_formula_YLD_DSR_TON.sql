--liquibase formatted sql

--changeset postgres:add_variable_BIOMASS_AREA_SQM_as_parameter_to_formula_YLD_DSR_TON context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3068 CB-DB: Insert BIOMASS_AREA_SQM as a formula parameter to YLD_DSR_TON



-- BIOMASS_AREA_SQM
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id, display_name,notes) 
    VALUES
        ('BIOMASS_AREA_SQM', 'BIOMASS_AREA_SQM', 'BIOMASS_AREA_SQM', 'float', false, 'observation', 'plot', 'occurrence', 'Take plants from 50cm of the same row without getting the border; 70 deg for 5 days of drying; NO FRESH WEIGHT needed', 'active', (SELECT id FROM tenant.person where email = 'admin@ebsproject.org'), 'Total area sampled for biomass per plot', NULL)
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'BIOMASS_AREA_SQM' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('BIOMASS_AREA_SQM', 'BIOMASS_AREA_SQM', 'Total area sampled for biomass per plot') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('BIOMASS_AREA_SQM_METHOD', 'BIOMASS_AREA_SQM')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('BIOMASS_AREA_SQM_SCALE', 'BIOMASS_AREA_SQM', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;

INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('BIOMASS_AREA_SQM') -- must be the same order as defined in the database function
    AND var.abbrev = 'YLD_DSR_TON'
;

select master.populate_order_number_for_master_formula_parameter();



--rollback DELETE FROM master.property WHERE abbrev IN
--rollback ('BIOMASS_AREA_SQM');
--rollback DELETE FROM master.formula_parameter where formula_id IN (SELECT id FROM master.formula WHERE formula = 'YLD_DSR_TON = (ADJAYLD_G_CONT/(((DIST_BET_ROWS/100) * ROW_LENGTH_CONT * ROWS_PER_PLOT_CONT) -(BIOMASS_AREA_SQM + TOT_PLTGAP_AREA_SQM))) / 100') AND param_variable_id IN (SELECT id FROM master.variable WHERE abbrev='BIOMASS_AREA_SQM');
--rollback select master.populate_order_number_for_master_formula_parameter();
--rollback DELETE FROM master.method WHERE abbrev IN
--rollback ('BIOMASS_AREA_SQM_METHOD');
--rollback DELETE FROM master.scale WHERE abbrev IN
--rollback ('BIOMASS_AREA_SQM_SCALE');
--rollback DELETE FROM master.variable WHERE abbrev IN
--rollback ('BIOMASS_AREA_SQM');
