--liquibase formatted sql

--changeset postgres:add_c_collation_index_germplasm_name_table_name_value_column context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT * FROM pg_indexes WHERE indexname = 'germplasm_name_name_value_c_idx') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2935 - Add germplasm_name.name_value btree C-collation index


-- Index: germplasm_name_name_value_c_idx
CREATE INDEX IF NOT EXISTS germplasm_name_name_value_c_idx
    ON germplasm.germplasm_name USING btree
    (name_value COLLATE pg_catalog."C" ASC NULLS LAST)
    TABLESPACE pg_default;
    
COMMENT ON INDEX germplasm.germplasm_name_name_value_c_idx
    IS 'BDS-2935 - Add germplasm_name.name_value btree C-collation index';


-- revert changes
--rollback DROP INDEX IF EXISTS germplasm.germplasm_name_name_value_c_idx;
