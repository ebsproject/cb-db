--liquibase formatted sql

--changeset postgres:add_taxonomy_provider_to_germplasm_taxonomy_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2498 CB-DB: Populate new column germplasm.taxonomy.taxonomy_provider for BrAPI taxonId sourceName



ALTER TABLE
	germplasm.taxonomy
ADD
	taxonomy_provider varchar;



--rollback ALTER TABLE
--rollback 	germplasm.taxonomy
--rollback DROP COLUMN
--rollback 	taxonomy_provider;