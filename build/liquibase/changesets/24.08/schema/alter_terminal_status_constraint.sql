--liquibase formatted sql

--changeset postgres:alter_terminal_status_constraint.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2830 CB-DC: Add new terminal transaction status 'trait calculation failed'



ALTER TABLE IF EXISTS data_terminal.transaction DROP CONSTRAINT IF EXISTS terminal_status;

ALTER TABLE IF EXISTS data_terminal.transaction
    ADD CONSTRAINT terminal_status CHECK (status::text = ANY (
        ARRAY[
            'in queue'::character varying::text, 
            
            'uploading in progress'::character varying::text, 
            'error in background process'::character varying::text, 

            'to be suppressed'::character varying::text,
            'suppression in progress'::character varying::text,

            'to be unsuppressed'::character varying::text,
            'undo suppression in progress'::character varying::text, 

            'removing data in progress'::character varying::text, 
            'undo removing data in progress'::character varying::text, 

            'uploaded'::character varying::text,
            'uploaded: to be calculated'::character varying::text,
            'calculation in progress'::character varying::text,
            'trait calculation failed'::character varying::text,

            'to be committed'::character varying::text,
            'committing in progress'::character varying::text,
            'committed'::character varying::text
        ])
    );



-- revert changes
--rollback ALTER TABLE IF EXISTS data_terminal.transaction DROP CONSTRAINT IF EXISTS terminal_status;
--rollback 
--rollback ALTER TABLE IF EXISTS data_terminal.transaction
--rollback     ADD CONSTRAINT terminal_status CHECK (status::text = ANY (
--rollback         ARRAY[
--rollback             'in queue'::character varying::text, 
--rollback             
--rollback             'uploading in progress'::character varying::text, 
--rollback             'error in background process'::character varying::text, 
--rollback 
--rollback             'to be suppressed'::character varying::text,
--rollback             'suppression in progress'::character varying::text,
--rollback 
--rollback             'to be unsuppressed'::character varying::text,
--rollback             'undo suppression in progress'::character varying::text, 
--rollback 
--rollback             'removing data in progress'::character varying::text, 
--rollback             'undo removing data in progress'::character varying::text, 
--rollback 
--rollback             'uploaded'::character varying::text,
--rollback             'uploaded: to be calculated'::character varying::text,
--rollback             'calculation in progress'::character varying::text,
--rollback 
--rollback             'to be committed'::character varying::text,
--rollback             'committing in progress'::character varying::text,
--rollback             'committed'::character varying::text
--rollback         ])
--rollback     );