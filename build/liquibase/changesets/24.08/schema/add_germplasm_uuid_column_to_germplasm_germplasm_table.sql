--liquibase formatted sql

--changeset postgres:add_germplasm_uuid_column_to_germplasm_germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-2497 CB-DB: Populate new column germplasm.germplasm.germplasm_uuid to store BrAPI germplasmPUI



ALTER TABLE
	germplasm.germplasm
ADD
	germplasm_uuid uuid
NOT NULL DEFAULT
	uuid_generate_v4();

COMMENT ON COLUMN
	germplasm.germplasm.germplasm_uuid
IS
	'Universally unique identifier (UUID) of the record';



--rollback ALTER TABLE
--rollback 	germplasm.germplasm
--rollback DROP COLUMN
--rollback 	germplasm_uuid;