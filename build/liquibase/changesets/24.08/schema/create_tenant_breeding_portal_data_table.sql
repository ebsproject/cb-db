--liquibase formatted sql

--changeset postgres:create_tenant_breeding_portal_data_table context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS ( SELECT FROM information_schema.tables WHERE  table_schema = 'tenant' AND    table_name   = 'breeding_portal_data') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2669 - Create tenant.breeding_portal_data_table and initial data


-- tenant.breeding_portal_data definition
CREATE TABLE tenant.breeding_portal_data (
    id serial NOT NULL,
    entity_name varchar(64) NOT NULL,
    entity_value jsonb NULL,
    transformed_data jsonb NULL,
    description text NULL,
    creator_id int4 NOT NULL,
    creation_timestamp timestamp DEFAULT now() NOT NULL,
    modifier_id int4 NULL,
    modification_timestamp timestamp NULL,
    is_void bool DEFAULT false NOT NULL,
    notes text NULL,
    event_log jsonb NULL,
    CONSTRAINT pk_breeding_portal_data_id PRIMARY KEY (id),
    CONSTRAINT breeding_portal_data_entity_name_unq UNIQUE (entity_name), 
    CONSTRAINT breeding_portal_data_id_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT breeding_portal_data_id_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

-- creation of indexes
CREATE INDEX breeding_portal_data_creator_id_idx ON tenant.breeding_portal_data USING btree (creator_id);
CREATE INDEX breeding_portal_data_is_void_idx ON tenant.breeding_portal_data USING btree (is_void);
CREATE INDEX breeding_portal_data_modifier_id_idx ON tenant.breeding_portal_data USING btree (modifier_id);
CREATE INDEX breeding_portal_data_entity_name_idx ON tenant.breeding_portal_data USING btree (entity_name);
COMMENT ON INDEX tenant.breeding_portal_data_entity_name_idx IS 'A breeding_portal_data can be retrieved by its entity name.';
COMMENT ON CONSTRAINT breeding_portal_data_entity_name_unq ON tenant.breeding_portal_data  IS 'An experiment plan can be retrieved by its unique experiment plan code.';

-- revert changes
--rollback DROP TABLE IF EXISTS tenant.breeding_portal_data;
