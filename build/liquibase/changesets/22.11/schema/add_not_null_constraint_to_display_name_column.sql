--liquibase formatted sql

--changeset postgres:add_not_null_constraint_to_display_name_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1565 Add not null constraint to display_name column



-- set not null constraint
ALTER TABLE
    master.variable
ALTER COLUMN
    display_name
    SET NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     master.variable
--rollback ALTER COLUMN
--rollback     display_name
--rollback     DROP NOT NULL
--rollback ;
