--liquibase formatted sql

--changeset postgres:create_table_germplasm.seed_transfer context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1571 Create table germplasm.seed_transfer



-- create table
CREATE TABLE germplasm.seed_transfer (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    sender_program_id integer,
    sender_id integer,
    send_timestamp timestamptz,
    receiver_program_id integer,
    receiver_id integer,
    receive_timestamp timestamptz,
    source_list_id integer,
    staging_list_id integer,
    destination_list_id integer,
    seed_transfer_status varchar(32) DEFAULT 'draft',
    package_quantity float,
    package_unit varchar(16),
    is_send_whole_package boolean,
    error_log jsonb,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT seed_transfer_id_pk PRIMARY KEY (id),
    
    -- column foreign key constraints
    CONSTRAINT seed_transfer_sender_program_id_fk FOREIGN KEY (sender_program_id)
        REFERENCES tenant.program (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_sender_id_fk FOREIGN KEY (sender_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_receiver_program_id_fk FOREIGN KEY (receiver_program_id)
        REFERENCES tenant.program (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_receiver_id_fk FOREIGN KEY (receiver_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_source_list_id_fk FOREIGN KEY (source_list_id)
        REFERENCES platform.list (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_staging_list_id_fk FOREIGN KEY (staging_list_id)
        REFERENCES platform.list (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    CONSTRAINT seed_transfer_destination_list_id_fk FOREIGN KEY (destination_list_id)
        REFERENCES platform.list (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    -- audit foreign key constraints
    CONSTRAINT seed_transfer_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT seed_transfer_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    -- check constraint
    CONSTRAINT seed_transfer_status_chk 
        CHECK (seed_transfer_status::text = ANY (ARRAY[
            'draft'::text, 'creation in progress'::text, 'creation failed'::text,
            'created'::text, 'sending in progress'::text, 'sending failed'::text, 
            'sent'::text, 'receipt in progress'::text, 'receipt failed'::text, 
            'received'::text, 'declined'::text]
        ))

);

-- table comment
COMMENT ON TABLE germplasm.seed_transfer
    IS 'Seed Transfer: Transactions recorded in seed transfer [SEEDTRNS]';

-- column comments
COMMENT ON COLUMN germplasm.seed_transfer.sender_program_id
    IS 'Sender Program ID: Program initiating the seed transfer [SEEDTRNS_SENDERPRGID]';
COMMENT ON COLUMN germplasm.seed_transfer.sender_id
    IS 'Sender ID: Person initiating the seed transfer [SEEDTRNS_SENDERID]';
COMMENT ON COLUMN germplasm.seed_transfer.send_timestamp
    IS 'Send Timestamp: Date and time of seed transfer [SEEDTRNS_SENDTIMESTMP]';
COMMENT ON COLUMN germplasm.seed_transfer.receiver_program_id
    IS 'Receiver Program ID: Program receiving the seed transfer [SEEDTRNS_RECEIVERPRGID]';
COMMENT ON COLUMN germplasm.seed_transfer.receiver_id
    IS 'Receiver ID: Person receiving the seed transfer [SEEDTRNS_RECEIVERID]';
COMMENT ON COLUMN germplasm.seed_transfer.receive_timestamp
    IS 'Receive Timestamp: Date and time of seed transfer received [SEEDTRNS_RCVTIMESTMP]';
COMMENT ON COLUMN germplasm.seed_transfer.source_list_id
    IS 'Source List ID: List containing the packages to be transferred [SEEDTRNS_SRCLISTID]';
COMMENT ON COLUMN germplasm.seed_transfer.staging_list_id
    IS 'Staging List ID: List containing the packages to be transferred [SEEDTRNS_STGLISTID]';
COMMENT ON COLUMN germplasm.seed_transfer.destination_list_id
    IS 'Destination List ID: List containing the packages to be transferred [SEEDTRNS_DESTLISTID]';
COMMENT ON COLUMN germplasm.seed_transfer.seed_transfer_status
    IS 'Seed Transfer Status: Status of the seed transfer transaction [SEEDTRNS_SEEDTRNSSTATS]';
COMMENT ON COLUMN germplasm.seed_transfer.package_quantity
    IS 'Package Quantity: Package quantity to be transferred [SEEDTRNS_PACKAGEQTY]';
COMMENT ON COLUMN germplasm.seed_transfer.package_unit
    IS 'Package Unit: Unit of measure for the package transfer [SEEDTRNS_PACKAGEUNT]';
COMMENT ON COLUMN germplasm.seed_transfer.is_send_whole_package
    IS 'Is Send Whole Package: Determines whether or not the whole package quantity is transferred [SEEDTRNS_ISSENDWHOLEPKG]';
COMMENT ON COLUMN germplasm.seed_transfer.error_log
    IS 'Error Log: Error details for the seed transfer [SEEDTRNS_ERRLOG]';

-- audit column comments
COMMENT ON COLUMN germplasm.seed_transfer.id
    IS 'Entity Name ID: Database identifier of the record [SEEDTRNS_ID]';
COMMENT ON COLUMN germplasm.seed_transfer.remarks
    IS 'Remarks: Additional notes to record [SEEDTRNS_REMARKS]';
COMMENT ON COLUMN germplasm.seed_transfer.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created SEEDTRNS_CTSTAMP]';
COMMENT ON COLUMN germplasm.seed_transfer.creator_id
    IS 'Creator ID: Reference to the person who created the record [SEEDTRNS_CPERSON]';
COMMENT ON COLUMN germplasm.seed_transfer.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [SEEDTRNS_MTSTAMP]';
COMMENT ON COLUMN germplasm.seed_transfer.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [SEEDTRNS_MPERSON]';
COMMENT ON COLUMN germplasm.seed_transfer.notes
    IS 'Notes: Technical details about the record [SEEDTRNS_NOTES]';
COMMENT ON COLUMN germplasm.seed_transfer.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [SEEDTRNS_ISVOID]';
COMMENT ON COLUMN germplasm.seed_transfer.event_log
    IS 'Event Log: Historical transactions of the record [SEEDTRNS_EVENTLOG]';

-- indices
CREATE INDEX seed_transfer_sender_program_id_idx ON germplasm.seed_transfer USING btree (sender_program_id);
CREATE INDEX seed_transfer_sender_id_idx ON germplasm.seed_transfer USING btree (sender_id);
CREATE INDEX seed_transfer_receiver_program_id_idx ON germplasm.seed_transfer USING btree (receiver_program_id);
CREATE INDEX seed_transfer_receiver_id_idx ON germplasm.seed_transfer USING btree (receiver_id);
CREATE INDEX seed_transfer_source_list_id_idx ON germplasm.seed_transfer USING btree (source_list_id);
CREATE INDEX seed_transfer_staging_list_id_idx ON germplasm.seed_transfer USING btree (staging_list_id);
CREATE INDEX seed_transfer_destination_list_id_idx ON germplasm.seed_transfer USING btree (destination_list_id);
CREATE INDEX seed_transfer_seed_transfer_status_idx ON germplasm.seed_transfer USING btree (seed_transfer_status);

-- audit indices
CREATE INDEX seed_transfer_creator_id_idx ON germplasm.seed_transfer USING btree (creator_id);
CREATE INDEX seed_transfer_modifier_id_idx ON germplasm.seed_transfer USING btree (modifier_id);
CREATE INDEX seed_transfer_is_void_idx ON germplasm.seed_transfer USING btree (is_void);

-- triggers
CREATE TRIGGER table_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.seed_transfer FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.seed_transfer;