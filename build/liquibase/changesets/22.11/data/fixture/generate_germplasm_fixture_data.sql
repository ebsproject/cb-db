--liquibase formatted sql

--changeset postgres:insert_crop context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert crop



--# tenant.crop

--/* insert
INSERT INTO
    tenant.crop (
        crop_code,
        crop_name,
        description,
        creator_id
    )
SELECT
    crop.crop_code,
    crop.crop_name,
    crop.description,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPY', 'Crop Y', 'Test Crop', 1)
    ) AS crop (
        crop_code,
        crop_name,
        description,
        creator
    )
    JOIN tenant.person AS crtr
        ON crtr.id = crop.creator
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     tenant.crop
--rollback WHERE
--rollback     crop_code = 'CROPY'
--rollback ;



--changeset postgres:insert_crop_program context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert crop program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table
INSERT INTO
    tenant.crop_program (
        crop_program_code,
        crop_program_name,
        organization_id,
        crop_id,
        creator_id
    )
SELECT
    t.crop_program_code,
    t.crop_program_name,
    org.id AS organization_id,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROPPROGY', 'Crop Program Y', 1, 'IRRI', 'CROPY')
    ) AS t (
        crop_program_code, crop_program_name, creator_id, organization_id, crop_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.organization AS org
        ON org.organization_code = t.organization_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
;
--*/



-- revert changes
--rollback -- delete 1 records
--rollback DELETE FROM
--rollback     tenant.crop_program AS cropprog
--rollback WHERE
--rollback     cropprog.crop_program_code = 'CROPPROGY'
--rollback ;



--changeset postgres:insert_program context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table
INSERT INTO
    tenant.program (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
SELECT
    t.program_code,
    t.program_name,
    t.program_type,
    t.program_status,
    cropprog.id AS crop_program_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('PROGY', 'Program Y', 'breeding', 'active', 'CROPPROGY', 1)
    ) AS t (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop_program AS cropprog
        ON cropprog.crop_program_code = t.crop_program_id
;
--*/



-- revert changes
-- delete 1 record/s 
--rollback DELETE FROM
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     prog.program_code = 'PROGY'
--rollback ;



--changeset postgres:insert_taxonomy context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert taxonomy



--# germplasm.taxonomy

--/* insert
INSERT INTO
    germplasm.taxonomy (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
SELECT
    t.taxon_id,
    t.taxonomy_name,
    t.description,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('112', 'Taxonomy Y', 'Test Taxonomy', 'CROPY', 1)
    ) AS t (
        taxon_id,
        taxonomy_name,
        description,
        crop_id,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
;
--*/



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.taxonomy
--rollback WHERE
--rollback     taxon_id = '112'
--rollback ;



--changeset postgres:insert_germplasm context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert germplasm



--# germplasm.germplasm

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = TRUE;

-- insert 1,000,000 records to table
INSERT INTO
    germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        creator_id,
        germplasm_type
    )
SELECT
    (t.designation || '-' || gs.num) AS designation,
    t.parentage,
    (t.generation || floor(random() * (12-6 + 1) + 6)::int) AS generation,
    t.germplasm_state,
    t.germplasm_name_type,
    crop.id AS crop_id,
    (t.germplasm_normalized_name || '-' || gs.num) AS germplasm_normalized_name,
    taxon.id AS taxonomy_id,
    crtr.id AS creator_id,
    t.germplasm_type
FROM (
        VALUES
        ('CYGE', '?/?', 'F', 'fixed', 'line_name', 'CROPY', 'CYGE', '112', 'fixed_line', 1)
    ) AS t (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        germplasm_type,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN germplasm.taxonomy AS taxon
        ON taxon.taxon_id = t.taxonomy_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id,
    generate_series(1, 1000000) AS gs (num) -- 1M fixed lines
UNION ALL
    SELECT
        (t.designation || '-' || gs.num) AS designation,
        t.parentage,
        (t.generation || ceiling(random() * (6 + 1))::int) AS generation,
        t.germplasm_state,
        t.germplasm_name_type,
        crop.id AS crop_id,
        (t.germplasm_normalized_name || '-' || gs.num) AS germplasm_normalized_name,
        taxon.id AS taxonomy_id,
        crtr.id AS creator_id,
        t.germplasm_type
    FROM (
            VALUES
            ('CYGE', '?/?', 'F', 'not_fixed', 'derivative_name', 'CROPY', 'CYGE', '112', 'progeny', 1)
        ) AS t (
            designation,
            parentage,
            generation,
            germplasm_state,
            germplasm_name_type,
            crop_id,
            germplasm_normalized_name,
            taxonomy_id,
            germplasm_type,
            creator_id
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = t.crop_id
        JOIN germplasm.taxonomy AS taxon
            ON taxon.taxon_id = t.taxonomy_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id,
        generate_series(1000001, 12000000) AS gs (num) -- 11M progenies
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = FALSE;

-- reindex table
-- REINDEX TABLE germplasm.germplasm;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;


--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
--rollback SET session_replication_role = replica;
--rollback --UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 12,000,000 records 
--rollback DELETE FROM
--rollback     germplasm.germplasm AS ge
--rollback USING
--rollback     tenant.crop AS crop
--rollback WHERE
--rollback     crop.id = ge.crop_id
--rollback     AND crop.crop_code = 'CROPY'
--rollback ;
--rollback 
--rollback -- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table
--rollback -- REINDEX TABLE germplasm.germplasm;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
--rollback SET session_replication_role = origin;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback --



--changeset postgres:insert_germplasm_names context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert germplasm names



--# germplasm.germplasm_name

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = TRUE;

-- insert 3,000,000 records to table 
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    ge.designation || t.name_value AS name_value,
    t.germplasm_name_type,
    t.germplasm_name_status,
    ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
    ge.creator_id
FROM (
        VALUES
        ('', 'line_name', 'standard', ''),
        ('A', 'alternative_cultivar_name', 'active', ' A'),
        ('B', 'alternative_cultivar_name', 'active', ' B')
    ) AS t (
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPY'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        ge.designation || t.name_value AS name_value,
        t.germplasm_name_type,
        t.germplasm_name_status,
        ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
        ge.creator_id
    FROM (
            VALUES
            ('', 'derivative_name', 'standard', ''),
            ('A', 'alternative_derivative_name', 'active', ' A'),
            ('B', 'alternative_derivative_name', 'active', ' B')
        ) AS t (
            name_value,
            germplasm_name_type,
            germplasm_name_status,
            germplasm_normalized_name
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPY'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = FALSE;

-- reindex table 
-- REINDEX TABLE germplasm.germplasm_name;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;


--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
--rollback SET session_replication_role = replica;
--rollback --UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 3,000,000 records 
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = gename.germplasm_id
--rollback     AND crop.crop_code = 'CROPY'
--rollback ;
--rollback 
--rollback --UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table 
--rollback --REINDEX TABLE germplasm.germplasm_name;
--rollback SET session_replication_role = origin;
--rollback --ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback --



--changeset postgres:insert_germplasm_attributes context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1672 Insert germplasm attributes



--# germplasm.germplasm_attribute

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = TRUE;

-- insert 1,500,000 records to table 
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    var.id AS variable_id,
    (floor(random() * 8 + 1))::varchar AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROSS_NUMBER', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'CROPY'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
        AND ge.germplasm_state = 'not_fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        ((array['S', 'W'])[floor(random() * 2 + 1)])::varchar AS data_value,
        t.data_qc_code,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('GROWTH_HABIT', 'G', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPY'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        ((array['RG', 'WG', 'BG'])[floor(random() * 3 + 1)])::varchar AS data_value,
        t.data_qc_code,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('GRAIN_COLOR', 'G', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPY'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        ((array['true', 'false'])[floor(random() * 2 + 1)])::varchar AS data_value,
        t.data_qc_code,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('DH_FIRST_INCREASE_COMPLETED', 'G', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPY'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        var.id AS variable_id,
        ((array['A', 'B', 'C'])[floor(random() * 3 + 1)])::varchar AS data_value,
        t.data_qc_code,
        crtr.id AS creator_id
    FROM (
            VALUES
            ('HETEROTIC_GROUP', 'G', 1)
        ) AS t (
            variable_id,
            data_qc_code,
            creator_id
        )
        JOIN master.variable AS var
            ON var.abbrev = t.variable_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'CROPY'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
            AND ge.germplasm_state = 'not_fixed'
ORDER BY
    germplasm_id,
    variable_id
;



-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = FALSE;

-- reindex table 
-- REINDEX TABLE germplasm.germplasm_attribute;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
SET session_replication_role = origin;


--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
--rollback SET session_replication_role = replica;
--rollback --UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 1,500,000 records 
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS geattr
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = geattr.germplasm_id
--rollback     AND crop.crop_code = 'CROPY'
--rollback ;
--rollback 
--rollback --UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table
--rollback --REINDEX TABLE germplasm.germplasm_attribute;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
--rollback SET session_replication_role = origin;
--rollback --