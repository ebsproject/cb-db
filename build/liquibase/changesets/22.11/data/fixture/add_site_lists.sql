--liquibase formatted sql

--changeset postgres:add_site_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1692 Add site lists



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, list_usage, status, is_active, creator_id)
SELECT 
    concat('DEVOPS-1692_SITE_LIST_',site_no) AS abbrev,
    concat('Site List ',site_no) AS name,
    concat('Site List ',site_no) AS display_name,
    'sites' AS type,
    t.entity_no AS entity_id,
    'site' AS list_usage,
    'created' AS status,
    TRUE AS is_active,
    1 AS creator_id
FROM 
    generate_series(1,10) AS site_no,
    (
        VALUES
            ((SELECT id FROM "dictionary".entity e WHERE abbrev='SITE'))
    ) AS t(entity_no)
;
 


-- revert changes
--rollback DELETE FROM 
--rollback     platform.list
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'DEVOPS-1692_SITE_LIST_1','DEVOPS-1692_SITE_LIST_2','DEVOPS-1692_SITE_LIST_3','DEVOPS-1692_SITE_LIST_4','DEVOPS-1692_SITE_LIST_5',
--rollback         'DEVOPS-1692_SITE_LIST_6','DEVOPS-1692_SITE_LIST_7','DEVOPS-1692_SITE_LIST_8','DEVOPS-1692_SITE_LIST_9','DEVOPS-1692_SITE_LIST_10'
--rollback     )
--rollback ;



--changeset postgres:add_random_sites context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1692 Add random sites



-- set 1
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL1-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL1 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 2
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL2-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL2 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 3
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL3-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL3 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 4
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL4-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL4 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 5
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL5-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL5 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 6
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL6-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL6 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 7
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL7-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL7 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 8
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL8-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL8 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 9
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL9-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL9 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;

-- set 10
INSERT INTO 
    place.geospatial_object 
        (
            geospatial_object_code, geospatial_object_name, geospatial_object_type,
            parent_geospatial_object_id, root_geospatial_object_id, creator_id
        )
SELECT 
    concat('DEVOPS-1692-SITE-SL10-RS-',ctr) AS geospatial_object_code,
    concat('DEVOPS-1692 Site SL10 RS',ctr) AS geospatial_object_name,
    'site' AS geospatial_object_type,
    t.parent_geospatial_object_id AS parent_geospatial_object_id,
    t.parent_geospatial_object_id AS root_geospatial_object_id,
    1 creator_id
FROM 
    generate_series(1,10) AS ctr,
    (
        VALUES
            ((SELECT id FROM place.geospatial_object go2 WHERE go2.geospatial_object_code='PH'))
    ) AS t(parent_geospatial_object_id)
;



--rollback DELETE FROM 
--rollback     place.geospatial_object 
--rollback WHERE 
--rollback     geospatial_object_code 
--rollback IN 
--rollback     (
--rollback          --site 1
--rollback         'DEVOPS-1692-SITE-SL1-RS-1',
--rollback         'DEVOPS-1692-SITE-SL1-RS-2',
--rollback         'DEVOPS-1692-SITE-SL1-RS-3',
--rollback         'DEVOPS-1692-SITE-SL1-RS-4',
--rollback         'DEVOPS-1692-SITE-SL1-RS-5',
--rollback         'DEVOPS-1692-SITE-SL1-RS-6',
--rollback         'DEVOPS-1692-SITE-SL1-RS-7',
--rollback         'DEVOPS-1692-SITE-SL1-RS-8',
--rollback         'DEVOPS-1692-SITE-SL1-RS-9',
--rollback         'DEVOPS-1692-SITE-SL1-RS-10',
--rollback          --site 2
--rollback         'DEVOPS-1692-SITE-SL2-RS-1',
--rollback         'DEVOPS-1692-SITE-SL2-RS-2',
--rollback         'DEVOPS-1692-SITE-SL2-RS-3',
--rollback         'DEVOPS-1692-SITE-SL2-RS-4',
--rollback         'DEVOPS-1692-SITE-SL2-RS-5',
--rollback         'DEVOPS-1692-SITE-SL2-RS-6',
--rollback         'DEVOPS-1692-SITE-SL2-RS-7',
--rollback         'DEVOPS-1692-SITE-SL2-RS-8',
--rollback         'DEVOPS-1692-SITE-SL2-RS-9',
--rollback         'DEVOPS-1692-SITE-SL2-RS-10',
--rollback          --site 3
--rollback         'DEVOPS-1692-SITE-SL3-RS-1',
--rollback         'DEVOPS-1692-SITE-SL3-RS-2',
--rollback         'DEVOPS-1692-SITE-SL3-RS-3',
--rollback         'DEVOPS-1692-SITE-SL3-RS-4',
--rollback         'DEVOPS-1692-SITE-SL3-RS-5',
--rollback         'DEVOPS-1692-SITE-SL3-RS-6',
--rollback         'DEVOPS-1692-SITE-SL3-RS-7',
--rollback         'DEVOPS-1692-SITE-SL3-RS-8',
--rollback         'DEVOPS-1692-SITE-SL3-RS-9',
--rollback         'DEVOPS-1692-SITE-SL3-RS-10',
--rollback          --site 4
--rollback         'DEVOPS-1692-SITE-SL4-RS-1',
--rollback         'DEVOPS-1692-SITE-SL4-RS-2',
--rollback         'DEVOPS-1692-SITE-SL4-RS-3',
--rollback         'DEVOPS-1692-SITE-SL4-RS-4',
--rollback         'DEVOPS-1692-SITE-SL4-RS-5',
--rollback         'DEVOPS-1692-SITE-SL4-RS-6',
--rollback         'DEVOPS-1692-SITE-SL4-RS-7',
--rollback         'DEVOPS-1692-SITE-SL4-RS-8',
--rollback         'DEVOPS-1692-SITE-SL4-RS-9',
--rollback         'DEVOPS-1692-SITE-SL4-RS-10',
--rollback          --site 5
--rollback         'DEVOPS-1692-SITE-SL5-RS-1',
--rollback         'DEVOPS-1692-SITE-SL5-RS-2',
--rollback         'DEVOPS-1692-SITE-SL5-RS-3',
--rollback         'DEVOPS-1692-SITE-SL5-RS-4',
--rollback         'DEVOPS-1692-SITE-SL5-RS-5',
--rollback         'DEVOPS-1692-SITE-SL5-RS-6',
--rollback         'DEVOPS-1692-SITE-SL5-RS-7',
--rollback         'DEVOPS-1692-SITE-SL5-RS-8',
--rollback         'DEVOPS-1692-SITE-SL5-RS-9',
--rollback         'DEVOPS-1692-SITE-SL5-RS-10',
--rollback          --site 6
--rollback         'DEVOPS-1692-SITE-SL6-RS-1',
--rollback         'DEVOPS-1692-SITE-SL6-RS-2',
--rollback         'DEVOPS-1692-SITE-SL6-RS-3',
--rollback         'DEVOPS-1692-SITE-SL6-RS-4',
--rollback         'DEVOPS-1692-SITE-SL6-RS-5',
--rollback         'DEVOPS-1692-SITE-SL6-RS-6',
--rollback         'DEVOPS-1692-SITE-SL6-RS-7',
--rollback         'DEVOPS-1692-SITE-SL6-RS-8',
--rollback         'DEVOPS-1692-SITE-SL6-RS-9',
--rollback         'DEVOPS-1692-SITE-SL6-RS-10',
--rollback          --site 7
--rollback         'DEVOPS-1692-SITE-SL7-RS-1',
--rollback         'DEVOPS-1692-SITE-SL7-RS-2',
--rollback         'DEVOPS-1692-SITE-SL7-RS-3',
--rollback         'DEVOPS-1692-SITE-SL7-RS-4',
--rollback         'DEVOPS-1692-SITE-SL7-RS-5',
--rollback         'DEVOPS-1692-SITE-SL7-RS-6',
--rollback         'DEVOPS-1692-SITE-SL7-RS-7',
--rollback         'DEVOPS-1692-SITE-SL7-RS-8',
--rollback         'DEVOPS-1692-SITE-SL7-RS-9',
--rollback         'DEVOPS-1692-SITE-SL7-RS-10',
--rollback          --site 8
--rollback         'DEVOPS-1692-SITE-SL8-RS-1',
--rollback         'DEVOPS-1692-SITE-SL8-RS-2',
--rollback         'DEVOPS-1692-SITE-SL8-RS-3',
--rollback         'DEVOPS-1692-SITE-SL8-RS-4',
--rollback         'DEVOPS-1692-SITE-SL8-RS-5',
--rollback         'DEVOPS-1692-SITE-SL8-RS-6',
--rollback         'DEVOPS-1692-SITE-SL8-RS-7',
--rollback         'DEVOPS-1692-SITE-SL8-RS-8',
--rollback         'DEVOPS-1692-SITE-SL8-RS-9',
--rollback         'DEVOPS-1692-SITE-SL8-RS-10',
--rollback          --site 9
--rollback         'DEVOPS-1692-SITE-SL9-RS-1',
--rollback         'DEVOPS-1692-SITE-SL9-RS-2',
--rollback         'DEVOPS-1692-SITE-SL9-RS-3',
--rollback         'DEVOPS-1692-SITE-SL9-RS-4',
--rollback         'DEVOPS-1692-SITE-SL9-RS-5',
--rollback         'DEVOPS-1692-SITE-SL9-RS-6',
--rollback         'DEVOPS-1692-SITE-SL9-RS-7',
--rollback         'DEVOPS-1692-SITE-SL9-RS-8',
--rollback         'DEVOPS-1692-SITE-SL9-RS-9',
--rollback         'DEVOPS-1692-SITE-SL9-RS-10',
--rollback          --site 10
--rollback         'DEVOPS-1692-SITE-SL10-RS-1',
--rollback         'DEVOPS-1692-SITE-SL10-RS-2',
--rollback         'DEVOPS-1692-SITE-SL10-RS-3',
--rollback         'DEVOPS-1692-SITE-SL10-RS-4',
--rollback         'DEVOPS-1692-SITE-SL10-RS-5',
--rollback         'DEVOPS-1692-SITE-SL10-RS-6',
--rollback         'DEVOPS-1692-SITE-SL10-RS-7',
--rollback         'DEVOPS-1692-SITE-SL10-RS-8',
--rollback         'DEVOPS-1692-SITE-SL10-RS-9',
--rollback         'DEVOPS-1692-SITE-SL10-RS-10'
--rollback     )
--rollback ;



--changeset postgres:add_random_sites_to_list_member context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1692 Add random sites to list member



--set 1
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL1-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL1 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_1'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 2
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL2-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL2 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_2'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 3
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL3-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL3 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_3'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 4
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL4-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL4 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_4'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 5
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL5-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL5 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_5'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 6
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL6-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL6 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_6'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 7
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL7-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL7 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_7'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 8
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL8-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL8 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_8'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 9
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL9-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL9 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_9'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;

--set 10
WITH t1 AS (
    SELECT 
        t.list_id AS list_id,
        (SELECT id FROM place.geospatial_object go2 WHERE geospatial_object_code = (concat('DEVOPS-1692-SITE-SL10-RS-',ctr))) AS data_id,
        concat('DEVOPS-1692 Site SL10 RS',ctr) AS display_value,
        ctr AS order_number
    FROM 
        generate_series(1,10) AS ctr,
        (
            VALUES
                ((SELECT id FROM platform.list WHERE abbrev='DEVOPS-1692_SITE_LIST_10'))
        ) AS t(list_id)
)
INSERT INTO 
    platform.list_member 
        (list_id, data_id, display_value, order_number, creator_id)
SELECT 
    t.list_id, 
    t.data_id,
    t.display_value,
    t.order_number,
    1 creator_id 
FROM t1 t;



--rollback DELETE FROM 
--rollback     platform.list_member 
--rollback WHERE list_id IN 
--rollback     (
--rollback         SELECT id FROM platform.list WHERE abbrev IN 
--rollback             (
--rollback                 'DEVOPS-1692_SITE_LIST_1',
--rollback                 'DEVOPS-1692_SITE_LIST_2',
--rollback                 'DEVOPS-1692_SITE_LIST_3',
--rollback                 'DEVOPS-1692_SITE_LIST_4',
--rollback                 'DEVOPS-1692_SITE_LIST_5',
--rollback                 'DEVOPS-1692_SITE_LIST_6',
--rollback                 'DEVOPS-1692_SITE_LIST_7',
--rollback                 'DEVOPS-1692_SITE_LIST_8',
--rollback                 'DEVOPS-1692_SITE_LIST_9',
--rollback                 'DEVOPS-1692_SITE_LIST_10'
--rollback             )
--rollback     )
--rollback ;