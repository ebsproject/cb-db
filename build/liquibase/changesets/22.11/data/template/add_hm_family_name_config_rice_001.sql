--liquibase formatted sql

--changeset postgres:add_hm_family_name_config_rice_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4319 HM DB: Add configurations for Family Names for RICE



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'HM_NAME_PATTERN_FAMILY_RICE_DEFAULT',
    'Harvest Manager Rice Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NAME_PATTERN_FAMILY_RICE_DEFAULT';