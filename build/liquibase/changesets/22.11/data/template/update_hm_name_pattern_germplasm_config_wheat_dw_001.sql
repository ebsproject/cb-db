--liquibase formatted sql

--changeset postgres:update_hm_name_pattern_germplasm_config_wheat_dw_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4592 HM: Update Wheat browser and name pattern configuration to support hybrid formation cross method



-- update browser config
UPDATE platform.config
SET
    config_value = '{
      "PLOT": {
        "default": {
          "not_fixed": {
            "default": {
              "bulk": [
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "germplasmDesignation",
                  "order_number": 0
                },
                {
                  "type": "delimiter",
                  "value": "-",
                  "order_number": 1
                },
                {
                  "type": "free-text",
                  "value": "0",
                  "order_number": 2
                },
                {
                  "type": "free-text",
                  "value": "TOP",
                  "special": "top",
                  "order_number": 3
                },
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "grainColor",
                  "order_number": 4
                },
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "fieldOriginSiteCode",
                  "order_number": 5
                }
              ],
              "default": [
                {
                  "type": "free-text",
                  "value": "",
                  "order_number": 0
                }
              ],
              "selected bulk": {
                "with_no_of_plant": [
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "germplasmDesignation",
                    "order_number": 0
                  },
                  {
                    "type": "delimiter",
                    "value": "-",
                    "order_number": 1
                  },
                  {
                    "type": "free-text",
                    "value": "0",
                    "order_number": 2
                  },
                  {
                    "type": "field",
                    "entity": "harvestData",
                    "field_name": "no_of_plants",
                    "order_number": 3
                  },
                  {
                    "type": "free-text",
                    "value": "TOP",
                    "special": "top",
                    "order_number": 4
                  },
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "grainColor",
                    "order_number": 5
                  },
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "fieldOriginSiteCode",
                    "order_number": 6
                  }
                ],
                "without_no_of_plant": [
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "germplasmDesignation",
                    "order_number": 0
                  },
                  {
                    "type": "delimiter",
                    "value": "-",
                    "order_number": 1
                  },
                  {
                    "type": "free-text",
                    "value": "099",
                    "order_number": 2
                  },
                  {
                    "type": "free-text",
                    "value": "TOP",
                    "special": "top",
                    "order_number": 3
                  },
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "grainColor",
                    "order_number": 4
                  },
                  {
                    "type": "field",
                    "entity": "plot",
                    "field_name": "fieldOriginSiteCode",
                    "order_number": 5
                  }
                ]
              },
              "individual spike": [
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "germplasmDesignation",
                  "order_number": 0
                },
                {
                  "type": "delimiter",
                  "value": "-",
                  "order_number": 1
                },
                {
                  "type": "counter",
                  "order_number": 2
                },
                {
                  "type": "free-text",
                  "value": "TOP",
                  "special": "top",
                  "order_number": 3
                },
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "fieldOriginSiteCode",
                  "order_number": 4
                }
              ],
              "single plant selection": [
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "germplasmDesignation",
                  "order_number": 0
                },
                {
                  "type": "delimiter",
                  "value": "-",
                  "order_number": 1
                },
                {
                  "type": "counter",
                  "order_number": 2
                },
                {
                  "type": "free-text",
                  "value": "TOP",
                  "special": "top",
                  "order_number": 4
                },
                {
                  "type": "field",
                  "entity": "plot",
                  "field_name": "fieldOriginSiteCode",
                  "order_number": 3
                }
              ]
            }
          }
        }
      },
      "CROSS": {
        "default": {
          "default": {
            "default": {
              "default": [
                {
                  "type": "free-text",
                  "value": "",
                  "order_number": 0
                }
              ]
            }
          }
        },
        "backcross": {
          "default": {
            "default": {
              "default": {
                "maleRecurrent": [
                  {
                    "type": "free-text",
                    "value": "CD",
                    "order_number": 0
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "growthHabit",
                    "order_number": 1
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "harvestYearYY",
                    "order_number": 2
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "originSiteCode",
                    "order_number": 3
                  },
                  {
                    "type": "counter",
                    "max_digits": 5,
                    "leading_zero": "yes",
                    "order_number": 4
                  },
                  {
                    "type": "free-text",
                    "value": "M",
                    "order_number": 5
                  }
                ],
                "femaleRecurrent": [
                  {
                    "type": "free-text",
                    "value": "CD",
                    "order_number": 0
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "growthHabit",
                    "order_number": 1
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "harvestYearYY",
                    "order_number": 2
                  },
                  {
                    "type": "field",
                    "entity": "cross",
                    "field_name": "originSiteCode",
                    "order_number": 3
                  },
                  {
                    "type": "counter",
                    "max_digits": 5,
                    "leading_zero": "yes",
                    "order_number": 4
                  },
                  {
                    "type": "free-text",
                    "value": "F",
                    "order_number": 5
                  }
                ]
              }
            }
          }
        },
        "top cross": {
          "default": {
            "default": {
              "default": [
                {
                  "type": "free-text",
                  "value": "CD",
                  "order_number": 0
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "growthHabit",
                  "order_number": 1
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "harvestYearYY",
                  "order_number": 2
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "originSiteCode",
                  "order_number": 3
                },
                {
                  "type": "counter",
                  "max_digits": 5,
                  "leading_zero": "yes",
                  "order_number": 4
                },
                {
                  "type": "free-text",
                  "value": "T",
                  "order_number": 5
                }
              ]
            }
          }
        },
        "double cross": {
          "default": {
            "default": {
              "default": [
                {
                  "type": "free-text",
                  "value": "CD",
                  "order_number": 0
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "growthHabit",
                  "order_number": 1
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "harvestYearYY",
                  "order_number": 2
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "originSiteCode",
                  "order_number": 3
                },
                {
                  "type": "counter",
                  "max_digits": 5,
                  "leading_zero": "yes",
                  "order_number": 4
                },
                {
                  "type": "free-text",
                  "value": "D",
                  "order_number": 5
                }
              ]
            }
          }
        },
        "single cross": {
          "default": {
            "default": {
              "default": [
                {
                  "type": "free-text",
                  "value": "CD",
                  "order_number": 0
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "growthHabit",
                  "order_number": 1
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "harvestYearYY",
                  "order_number": 2
                },
                {
                  "type": "field",
                  "entity": "cross",
                  "field_name": "originSiteCode",
                  "order_number": 3
                },
                {
                  "type": "counter",
                  "max_digits": 5,
                  "leading_zero": "yes",
                  "order_number": 4
                },
                {
                  "type": "free-text",
                  "value": "S",
                  "order_number": 5
                }
              ]
            }
          }
        },
        "hybrid formation": {
            "default": {
                "default": {
                    "default": [
                        {
                        "type": "free-text",
                        "value": "CD",
                        "order_number": 0
                        },
                        {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "growthHabit",
                        "order_number": 1
                        },
                        {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "harvestYearYY",
                        "order_number": 2
                        },
                        {
                        "type": "field",
                        "entity": "cross",
                        "field_name": "originSiteCode",
                        "order_number": 3
                        },
                        {
                        "type": "counter",
                        "max_digits": 5,
                        "leading_zero": "yes",
                        "order_number": 4
                        },
                        {
                        "type": "free-text",
                        "value": "H",
                        "order_number": 5
                        }
                    ]
                }
          }
        }
      },
      "harvest_mode": {
        "cross_method": {
          "germplasm_state": {
            "germplasm_type": {
              "harvest_method": [
                {
                  "type": "free-text",
                  "value": "ABC",
                  "order_number": 0
                },
                {
                  "type": "field",
                  "entity": "<entity>",
                  "field_name": "<field_name>",
                  "order_number": 1
                },
                {
                  "type": "delimiter",
                  "value": "-",
                  "order_number": 1
                },
                {
                  "type": "counter",
                  "order_number": 3
                },
                {
                  "type": "db-sequence",
                  "schema": "<schema>",
                  "order_number": 4,
                  "sequence_name": "<sequence_name>"
                },
                {
                  "type": "free-text-repeater",
                  "value": "ABC",
                  "minimum": "2",
                  "delimiter": "*",
                  "order_number": 5
                }
              ]
            }
          }
        }
      }
    }'
WHERE
    abbrev = 'HM_NAME_PATTERN_GERMPLASM_WHEAT_DW'
;



--rollback UPDATE platform.config
--rollback SET
--rollback config_value = 
--rollback '{
--rollback   "PLOT": {
--rollback     "default": {
--rollback       "not_fixed": {
--rollback         "default": {
--rollback           "bulk": [
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "germplasmDesignation",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "delimiter",
--rollback               "value": "-",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "0",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "TOP",
--rollback               "special": "top",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "grainColor",
--rollback               "order_number": 4
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "fieldOriginSiteCode",
--rollback               "order_number": 5
--rollback             }
--rollback           ],
--rollback           "default": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "",
--rollback               "order_number": 0
--rollback             }
--rollback           ],
--rollback           "selected bulk": {
--rollback             "with_no_of_plant": [
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "germplasmDesignation",
--rollback                 "order_number": 0
--rollback               },
--rollback               {
--rollback                 "type": "delimiter",
--rollback                 "value": "-",
--rollback                 "order_number": 1
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "0",
--rollback                 "order_number": 2
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "harvestData",
--rollback                 "field_name": "no_of_plants",
--rollback                 "order_number": 3
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "TOP",
--rollback                 "special": "top",
--rollback                 "order_number": 4
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "grainColor",
--rollback                 "order_number": 5
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "fieldOriginSiteCode",
--rollback                 "order_number": 6
--rollback               }
--rollback             ],
--rollback             "without_no_of_plant": [
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "germplasmDesignation",
--rollback                 "order_number": 0
--rollback               },
--rollback               {
--rollback                 "type": "delimiter",
--rollback                 "value": "-",
--rollback                 "order_number": 1
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "099",
--rollback                 "order_number": 2
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "TOP",
--rollback                 "special": "top",
--rollback                 "order_number": 3
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "grainColor",
--rollback                 "order_number": 4
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "plot",
--rollback                 "field_name": "fieldOriginSiteCode",
--rollback                 "order_number": 5
--rollback               }
--rollback             ]
--rollback           },
--rollback           "individual spike": [
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "germplasmDesignation",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "delimiter",
--rollback               "value": "-",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "TOP",
--rollback               "special": "top",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "fieldOriginSiteCode",
--rollback               "order_number": 4
--rollback             }
--rollback           ],
--rollback           "single plant selection": [
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "germplasmDesignation",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "delimiter",
--rollback               "value": "-",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "TOP",
--rollback               "special": "top",
--rollback               "order_number": 4
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "plot",
--rollback               "field_name": "fieldOriginSiteCode",
--rollback               "order_number": 3
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     }
--rollback   },
--rollback   "CROSS": {
--rollback     "default": {
--rollback       "default": {
--rollback         "default": {
--rollback           "default": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "",
--rollback               "order_number": 0
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     },
--rollback     "backcross": {
--rollback       "default": {
--rollback         "default": {
--rollback           "default": {
--rollback             "maleRecurrent": [
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "CD",
--rollback                 "order_number": 0
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "growthHabit",
--rollback                 "order_number": 1
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "harvestYearYY",
--rollback                 "order_number": 2
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "originSiteCode",
--rollback                 "order_number": 3
--rollback               },
--rollback               {
--rollback                 "type": "counter",
--rollback                 "max_digits": 5,
--rollback                 "leading_zero": "yes",
--rollback                 "order_number": 4
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "M",
--rollback                 "order_number": 5
--rollback               }
--rollback             ],
--rollback             "femaleRecurrent": [
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "CD",
--rollback                 "order_number": 0
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "growthHabit",
--rollback                 "order_number": 1
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "harvestYearYY",
--rollback                 "order_number": 2
--rollback               },
--rollback               {
--rollback                 "type": "field",
--rollback                 "entity": "cross",
--rollback                 "field_name": "originSiteCode",
--rollback                 "order_number": 3
--rollback               },
--rollback               {
--rollback                 "type": "counter",
--rollback                 "max_digits": 5,
--rollback                 "leading_zero": "yes",
--rollback                 "order_number": 4
--rollback               },
--rollback               {
--rollback                 "type": "free-text",
--rollback                 "value": "F",
--rollback                 "order_number": 5
--rollback               }
--rollback             ]
--rollback           }
--rollback         }
--rollback       }
--rollback     },
--rollback     "top cross": {
--rollback       "default": {
--rollback         "default": {
--rollback           "default": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "CD",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "growthHabit",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "harvestYearYY",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "originSiteCode",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "max_digits": 5,
--rollback               "leading_zero": "yes",
--rollback               "order_number": 4
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "T",
--rollback               "order_number": 5
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     },
--rollback     "double cross": {
--rollback       "default": {
--rollback         "default": {
--rollback           "default": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "CD",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "growthHabit",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "harvestYearYY",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "originSiteCode",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "max_digits": 5,
--rollback               "leading_zero": "yes",
--rollback               "order_number": 4
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "D",
--rollback               "order_number": 5
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     },
--rollback     "single cross": {
--rollback       "default": {
--rollback         "default": {
--rollback           "default": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "CD",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "growthHabit",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "harvestYearYY",
--rollback               "order_number": 2
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "cross",
--rollback               "field_name": "originSiteCode",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "max_digits": 5,
--rollback               "leading_zero": "yes",
--rollback               "order_number": 4
--rollback             },
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "S",
--rollback               "order_number": 5
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     }
--rollback   },
--rollback   "harvest_mode": {
--rollback     "cross_method": {
--rollback       "germplasm_state": {
--rollback         "germplasm_type": {
--rollback           "harvest_method": [
--rollback             {
--rollback               "type": "free-text",
--rollback               "value": "ABC",
--rollback               "order_number": 0
--rollback             },
--rollback             {
--rollback               "type": "field",
--rollback               "entity": "<entity>",
--rollback               "field_name": "<field_name>",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "delimiter",
--rollback               "value": "-",
--rollback               "order_number": 1
--rollback             },
--rollback             {
--rollback               "type": "counter",
--rollback               "order_number": 3
--rollback             },
--rollback             {
--rollback               "type": "db-sequence",
--rollback               "schema": "<schema>",
--rollback               "order_number": 4,
--rollback               "sequence_name": "<sequence_name>"
--rollback             },
--rollback             {
--rollback               "type": "free-text-repeater",
--rollback               "value": "ABC",
--rollback               "minimum": "2",
--rollback               "delimiter": "*",
--rollback               "order_number": 5
--rollback             }
--rollback           ]
--rollback         }
--rollback       }
--rollback     }
--rollback   }
--rollback }'
--rollback WHERE
--rollback     abbrev = 'HM_NAME_PATTERN_GERMPLASM_WHEAT_DW'
--rollback ;