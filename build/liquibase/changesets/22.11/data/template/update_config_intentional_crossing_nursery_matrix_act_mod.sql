--liquibase formatted sql

--changeset postgres:update_config_intentional_crossing_nursery_matrix_act_mod context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-1732 Update config INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT_MOD



UPDATE 
    platform.module 
SET 
    controller_id = 'cross-pattern', 
    action_id = 'manage-cross-pattern' 
WHERE 
    abbrev  = 'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT_MOD'
;



-- revert changes
--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     controller_id = 'create', 
--rollback     action_id = 'manage-crossing-matrix' 
--rollback WHERE 
--rollback     abbrev  = 'INTENTIONAL_CROSSING_NURSERY_MATRIX_ACT_MOD'
--rollback ;