--liquibase formatted sql

--changeset postgres:add_scale_values_to_seed_transfer_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1572 Add scale values to SEED_TRANSFER_STATUS



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'SEED_TRANSFER_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('draft', 'draft', 'draft', 'DRAFT', 'show'),
        ('creation in progress', 'creation in progress', 'creation in progress', 'CREATION_IN_PROGRESS', 'show'),
        ('creation failed', 'creation failed', 'creation failed', 'CREATION_FAILED', 'show'),
        ('created', 'created', 'created', 'CREATED', 'show'),
        ('sending in progress', 'sending in progress', 'sending in progress', 'SENDING_IN_PROGRESS', 'show'),
        ('sending failed', 'sending failed', 'sending failed', 'SENDING_FAILED', 'show'),
        ('sent', 'sent', 'sent', 'SENT', 'show'),
        ('receipt in progress', 'receipt in progress', 'receipt in progress', 'RECEIPT_IN_PROGRESS', 'show'),
        ('receipt failed', 'receipt failed', 'receipt failed', 'RECEIPT_FAILED', 'show'),
        ('received', 'received', 'received', 'RECEIVED', 'show'),
        ('declined', 'declined', 'declined', 'DECLINED', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'SEED_TRANSFER_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'SEED_TRANSFER_STATUS_DRAFT',
--rollback         'SEED_TRANSFER_STATUS_CREATION_IN_PROGRESS',
--rollback         'SEED_TRANSFER_STATUS_CREATION_FAILED',
--rollback         'SEED_TRANSFER_STATUS_CREATED',
--rollback         'SEED_TRANSFER_STATUS_SENDING_IN_PROGRESS',
--rollback         'SEED_TRANSFER_STATUS_SENDING_FAILED',
--rollback         'SEED_TRANSFER_STATUS_SENT',
--rollback         'SEED_TRANSFER_STATUS_RECEIPT_IN_PROGRESS',
--rollback         'SEED_TRANSFER_STATUS_RECEIPT_FAILED',
--rollback         'SEED_TRANSFER_STATUS_RECEIVED',
--rollback         'SEED_TRANSFER_STATUS_DECLINED'
--rollback     )
--rollback ;