--liquibase formatted sql

--changeset postgres:add_hm_family_name_config_maize_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4318 HM DB: Add configurations for Family Names for MAIZE



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'HM_NAME_PATTERN_FAMILY_MAIZE_DEFAULT',
    'Harvest Manager Maize Family Name Pattern-Default', 
    $${
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "_",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "programCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "S",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "max_digits": 8,
                                "leading_zero": "yes",
                                "order_number": 3
                            }
                        ]
                    }
                }
            },
            "double cross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "programCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "D",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "max_digits": 8,
                                "leading_zero": "yes",
                                "order_number": 3
                            }
                        ]
                    }
                }
            },
            "three-way cross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "programCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "T",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "max_digits": 8,
                                "leading_zero": "yes",
                                "order_number": 3
                            }
                        ]
                    }
                }
            },
            "backcross": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "programCode",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "experimentYearYY",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "B",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "max_digits": 8,
                                "leading_zero": "yes",
                                "order_number": 3
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }$$,
    1,
    'harvest_manager',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NAME_PATTERN_FAMILY_MAIZE_DEFAULT';