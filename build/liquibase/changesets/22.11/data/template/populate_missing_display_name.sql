--liquibase formatted sql

--changeset postgres:populate_missing_display_name context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1566 Populate missing display_name



UPDATE 
    master.variable
SET
    display_name = name
WHERE
    display_name IS NULL
OR 
    display_name = ''
;



-- revert changes
--rollback SELECT NULL;