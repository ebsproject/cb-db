--liquibase formatted sql

--changeset postgres:add_seed_transfer_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1589 Add SEED_TRANSFER entity



-- add table to dictionary
INSERT INTO "dictionary"."table"
    (database_id, schema_id, abbrev, name, comment, creator_id)
SELECT
    db.id AS database_id,
    sch.id AS schema_id,
    'SEED_TRANSFER' AS abbrev,
    'seed_transfer' AS name,
    'Transactions recorded in seed transfer' AS comment,
    1 AS creator_id
FROM
    "dictionary"."database" AS db,
    "dictionary"."schema" AS sch
WHERE
    db.abbrev = 'CB'
    AND sch.name = 'germplasm'
    AND db.id = sch.database_id
;

-- add entity to dictionary
INSERT INTO "dictionary".entity
    (table_id, abbrev, name, description, creator_id)
SELECT
    tbl.id AS table_id,
    'SEED_TRANSFER' AS abbrev,
    'seed_transfer' AS name,
    'Transactions recorded in seed transfer' AS description,
    1 AS creator_id
FROM
    "dictionary"."schema" AS sch,
    "dictionary"."table" AS tbl
WHERE
    sch.name = 'germplasm'
    AND tbl.name = 'seed_transfer'
    AND sch.id = tbl.schema_id
;



-- revert changes
--rollback DELETE FROM dictionary.entity WHERE abbrev = 'SEED_TRANSFER';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'SEED_TRANSFER';