--liquibase formatted sql



--changeset postgres:insert_crop_program context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert crop program



--# tenant.crop_program

--/* insert
-- insert 1 record/s to table (eta: ~4ms)
INSERT INTO
    tenant.crop_program (
        crop_program_code,
        crop_program_name,
        organization_id,
        crop_id,
        creator_id
    )
SELECT
    t.crop_program_code,
    t.crop_program_name,
    org.id AS organization_id,
    crop.id AS crop_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('GWTCP', 'Global Wheat Test Crop Program', 1, 'CIMMYT', 'WHEAT')
    ) AS t (
        crop_program_code, crop_program_name, creator_id, organization_id, crop_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.organization AS org
        ON org.organization_code = t.organization_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
;
--*/



-- revert changes
--rollback -- delete 1 records (eta: ~27ms)
--rollback DELETE FROM
--rollback     tenant.crop_program AS cropprog
--rollback WHERE
--rollback     cropprog.crop_program_code = 'GWTCP'
--rollback ;



--changeset postgres:insert_program context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert program



--# tenant.program

--/* insert
-- insert 1 record/s to table (eta: ~8ms)
INSERT INTO
    tenant.program (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
SELECT
    t.program_code,
    t.program_name,
    t.program_type,
    t.program_status,
    cropprog.id AS crop_program_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('WTP', 'Wheat Test Program', 'breeding', 'active', 'GWTCP', 1)
    ) AS t (
        program_code,
        program_name,
        program_type,
        program_status,
        crop_program_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop_program AS cropprog
        ON cropprog.crop_program_code = t.crop_program_id
;
--*/



-- revert changes
-- delete 1 record/s (eta: ~113ms)
--rollback DELETE FROM
--rollback     tenant.program AS prog
--rollback WHERE
--rollback     prog.program_code = 'WTP'
--rollback ;



--changeset postgres:insert_team context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert team



--# tenant.team

--/* insert
-- insert 1 record/s to table (eta: ~8ms)
INSERT INTO
    tenant.team (
        team_code,
        team_name,
        creator_id
    )
SELECT
    t.team_code,
    t.team_name,
    crtr.id AS creator_id
FROM (
        VALUES
        ('WTP_TEAM', 'Wheat Test Program Team', 1)
    ) AS t (
        team_code,
        team_name,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
;
--*/



-- revert changes
-- delete 1 record/s (eta: ~113ms)
--rollback DELETE FROM
--rollback     tenant.team
--rollback WHERE
--rollback     team_code = 'WTP_TEAM'
--rollback ;



--changeset postgres:insert_program_team context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert program team



--# tenant.program_team

--/* insert
-- insert 1 record/s to table (eta: ~8ms)
INSERT INTO
    tenant.program_team (
        program_id,
        team_id,
        order_number,
        creator_id
    )
SELECT
    prog.id AS program_id,
    team.id AS team_id,
    1 AS order_number,
    crtr.id AS creator_id
FROM (
        VALUES
        ('WTP', 'WTP_TEAM', 1)
    ) AS t (
        program_code,
        team_code,
        creator_id
    )
    JOIN tenant.program AS prog
        ON prog.program_code = t.program_code
    JOIN tenant.team AS team
        ON team.team_code = t.team_code
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
;
--*/



-- revert changes
--rollback -- delete 1 record/s (eta: ~113ms)
--rollback DELETE FROM
--rollback     tenant.program_team AS progteam
--rollback USING
--rollback     tenant.program AS prog,
--rollback     tenant.team AS team
--rollback WHERE
--rollback     progteam.program_id = prog.id
--rollback     AND progteam.team_id = team.id
--rollback     AND prog.program_code = 'WTP'
--rollback     AND team_code = 'WTP_TEAM'
--rollback ;



--changeset postgres:insert_germplasm context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert germplasm



--# germplasm.germplasm

--/* insert 3,000,000
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = TRUE;

-- insert 3,000,000 records to table (eta: ~45.804s)
INSERT INTO
    germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        creator_id,
        germplasm_type
    )
SELECT
    (t.designation || '-' || gs.num) AS designation,
    t.parentage,
    --(t.generation || floor(random() * (12-6 + 1) + 6)::int) AS generation,
    'UNKNOWN' AS generation,
    t.germplasm_state,
    t.germplasm_name_type,
    crop.id AS crop_id,
    (t.germplasm_normalized_name || ' ' || gs.num) AS germplasm_normalized_name,
    taxon.id AS taxonomy_id,
    crtr.id AS creator_id,
    t.germplasm_type
FROM (
        VALUES
        ('WTGE', '?/?', 'F', 'fixed', 'common_name', 'WHEAT', 'WTGE', 'Triticum aestivum', NULL, 1)
    ) AS t (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        crop_id,
        germplasm_normalized_name,
        taxonomy_id,
        germplasm_type,
        creator_id
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop_id
    JOIN germplasm.taxonomy AS taxon
        ON taxon.taxonomy_name = t.taxonomy_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id,
    generate_series(1, 1000000) AS gs (num) -- 3M fixed
UNION ALL
    SELECT
        (t.designation || '-' || gs.num) AS designation,
        t.parentage,
        (t.generation || ceiling(random() * (6 + 1))::int) AS generation,
        --'UNKNOWN' AS generation,
        t.germplasm_state,
        t.germplasm_name_type,
        crop.id AS crop_id,
        (t.germplasm_normalized_name || ' ' || gs.num) AS germplasm_normalized_name,
        taxon.id AS taxonomy_id,
        crtr.id AS creator_id,
        t.germplasm_type
    FROM (
            VALUES
            ('WTGE', '?/?', 'F', 'not_fixed', 'selection_history', 'WHEAT', 'WTGE', 'Triticum aestivum', NULL, 1)
        ) AS t (
            designation,
            parentage,
            generation,
            germplasm_state,
            germplasm_name_type,
            crop_id,
            germplasm_normalized_name,
            taxonomy_id,
            germplasm_type,
            creator_id
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = t.crop_id
        JOIN germplasm.taxonomy AS taxon
            ON taxon.taxonomy_name = t.taxonomy_id
        JOIN tenant.person AS crtr
            ON crtr.id = t.creator_id,
        generate_series(1000001, 3000000) AS gs (num) -- 6M not_fixed
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~20.529)
-- REINDEX TABLE germplasm.germplasm;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

-- total eta: ~1m 6s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = TRUE;
--rollback 
--rollback -- delete 3,000,000 records (eta: ~6.172s)
--rollback DELETE FROM
--rollback     germplasm.germplasm AS ge
--rollback USING
--rollback     tenant.crop AS crop
--rollback WHERE
--rollback     crop.id = ge.crop_id
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback     AND crop.crop_code = 'WHEAT'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm'::regclass::oid AND indisprimary = FALSE AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~5.125s)
--rollback REINDEX TABLE germplasm.germplasm;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback ALTER TABLE germplasm.germplasm DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback -- total eta: ~11.297s



--changeset postgres:insert_germplasm_names context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert germplasm names



--# germplasm.germplasm_name

--/* insert 9,000,000
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = TRUE;

-- insert 9,000,000 records to table (eta: ~39.207s)
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    ge.designation || t.name_value AS name_value,
    t.germplasm_name_type,
    t.germplasm_name_status,
    ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
    ge.creator_id
FROM (
        VALUES
        ('', 'common_name', 'standard', ''),
        ('A', 'common_name', 'active', ' A'),
        ('B', 'common_name', 'active', ' B')
    ) AS t (
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
WHERE
    ge.germplasm_state = 'fixed'
    AND ge.designation LIKE 'WTGE%'
UNION ALL
    SELECT
        ge.id AS germplasm_id,
        ge.designation || t.name_value AS name_value,
        t.germplasm_name_type,
        t.germplasm_name_status,
        ge.germplasm_normalized_name || t.germplasm_normalized_name AS germplasm_normalized_name,
        ge.creator_id
    FROM (
            VALUES
            ('', 'selection_history', 'standard', ''),
            ('A', 'selection_history', 'active', ' A'),
            ('B', 'selection_history', 'active', ' B')
        ) AS t (
            name_value,
            germplasm_name_type,
            germplasm_name_status,
            germplasm_normalized_name
        )
        JOIN tenant.crop AS crop
            ON crop.crop_code = 'WHEAT'
        JOIN germplasm.germplasm AS ge
            ON ge.crop_id = crop.id
    WHERE
        ge.germplasm_state = 'not_fixed'
        AND ge.designation LIKE 'WTGE%'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~43.359s)
-- REINDEX TABLE germplasm.germplasm_name;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;

-- total eta: ~1m 22s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 9,000,000 records (eta: ~31.50s)
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = gename.germplasm_id
--rollback     AND crop.crop_code = 'WHEAT'
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_name'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~7.614s)
--rollback REINDEX TABLE germplasm.germplasm_name;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_name ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.germplasm_name DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback -- total eta: ~39.114s



--changeset postgres:insert_germplasm_attributes context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert germplasm attributes



--# germplasm.germplasm_attribute

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = TRUE;

-- insert 3,000,000 records to table (eta: ~9.865s)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    var.id AS variable_id,
    (floor(random() * 8 + 1)) AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('CROSS_NUMBER', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
WHERE
    ge.designation LIKE 'WTGE%'
;

-- insert 3,000,000 records to table (eta: ~8.720s)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    var.id AS variable_id,
    ((array['S', 'W'])[floor(random() * 2 + 1)]) AS data_value,
    t.data_qc_code,
    crtr.id AS creator_id
FROM (
        VALUES
        ('GROWTH_HABIT', 'G', 1)
    ) AS t (
        variable_id,
        data_qc_code,
        creator_id
    )
    JOIN master.variable AS var
        ON var.abbrev = t.variable_id
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
WHERE
    ge.designation LIKE 'WTGE%'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~25.656s)
-- REINDEX TABLE germplasm.germplasm_attribute;

-- restore triggers and constraints
--ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~44.241s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.germplasm_attribute DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 6,000,000 records (eta: ~34.150s)
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS geattr
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = geattr.germplasm_id
--rollback     AND ge.designation LIKE 'WTGE%' 
--rollback     AND crop.crop_code = 'WHEAT'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.germplasm_attribute'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~6.894s)
--rollback REINDEX TABLE germplasm.germplasm_attribute;
--rollback 
--rollback --ALTER TABLE germplasm.germplasm_attribute ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~41.044s



--changeset postgres:insert_seeds context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert seeds



--# germplasm.seed

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = TRUE;

-- insert 250,000 records to table (eta: ~4.486s)
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    (ge.designation || '-SD' || gs.num) AS seed_name,
    (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01'))::date AS harvest_date,
    'Bulk' AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    'plot' AS harvest_source,
    ge.creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'WTP',
    generate_series(1, 1) AS gs (num)
WHERE
    crop.crop_code = 'WHEAT'
    AND ge.designation LIKE 'WTGE%'
    AND ge.germplasm_state = 'fixed'
LIMIT
    250000
;

-- insert 500,000 records to table (eta: ~8.122s)
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    (ge.designation || '-SD' || gs.num) AS seed_name,
    (timestamp '2016-01-01' + random() * (timestamp '2020-12-31' - timestamp '2016-01-01'))::date AS harvest_date,
    ((array['Bulk', 'Single Plant Selection', 'Plant-specific'])[floor(random() * 3 + 1)]) AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    ((array['plot', 'cross'])[floor(random() * 2 + 1)]) AS harvest_source,
    ge.creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'WTP',
    generate_series(1, 1) AS gs (num)
WHERE
    crop.crop_code = 'WHEAT'
    AND ge.designation LIKE 'WTGE%'
    AND ge.germplasm_state = 'not_fixed'
LIMIT
    500000
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~11.64s)
-- REINDEX TABLE germplasm.seed;

-- restore triggers and constraints
--ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~24.248s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.seed DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 750,000 records (eta: ~3.227s)
--rollback DELETE FROM
--rollback     germplasm.seed AS t
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     ge.id = t.germplasm_id
--rollback     AND crop.crop_code = 'WHEAT'
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.seed'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~2.911s)
--rollback REINDEX TABLE germplasm.seed;
--rollback 
--rollback --ALTER TABLE germplasm.seed ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~6.138s



--changeset postgres:insert_packages context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert packages



--# germplasm.package

--/* insert
-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.package DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = TRUE;

-- insert 100,000 records to table (eta: ~4.872s)
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    (sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('-PKG', 'g', 'active', 1)
    ) AS t (
        package_label,
        package_unit,
        package_status,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN tenant.program AS prog
        ON prog.program_code = 'WTP'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'fixed'
    AND ge.designation LIKE 'WTGE%'
LIMIT
    100000
;

-- insert 400,000 records to table (eta: ~6.272s)
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    (sd.seed_name || t.package_label || gs.num) AS package_label,
    floor(random() * 300) AS package_quantity,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('-PKG', 'g', 'active', 1)
    ) AS t (
        package_label,
        package_unit,
        package_status,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN tenant.program AS prog
        ON prog.program_code = 'WTP'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id,
    generate_series(1, 1) AS gs (num)
WHERE
    ge.germplasm_state = 'not_fixed'
    AND ge.designation LIKE 'WTGE%'
LIMIT
    400000
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~4.681s)
-- REINDEX TABLE germplasm.package;

-- restore triggers and constraints
--ALTER TABLE germplasm.package ENABLE TRIGGER ALL;
SET session_replication_role = origin;
ALTER TABLE germplasm.package DISABLE TRIGGER package_update_package_document_tgr;

-- total eta: ~15.825s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.package DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 500,000 records (eta: ~5.528s)
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     tenant.crop AS crop
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.crop_id = crop.id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback WHERE
--rollback     sd.id = pkg.seed_id
--rollback     AND crop.crop_code = 'WHEAT'
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~1.623s)
--rollback REINDEX TABLE germplasm.package;
--rollback 
--rollback --ALTER TABLE germplasm.package ENABLE TRIGGER ALL;
--rollback ALTER TABLE germplasm.package DISABLE TRIGGER package_update_package_document_tgr;
--rollback 
--rollback -- total eta: ~7.151s



--changeset postgres:insert_package_logs context:fixture labels:stress,medium,evaluation splitStatements:false rollbackSplitStatements:false
--comment: DB-687 Insert package logs



--# germplasm.package_log

--/* insert
-- fix id sequence
SELECT SETVAL('germplasm.package_log_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM germplasm.package_log;

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE germplasm.package_log DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
-- UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = TRUE;

-- insert 500,000 records to table (eta: ~6.462s)
INSERT INTO
    germplasm.package_log (
        package_id,
        package_quantity,
        package_unit,
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
SELECT
    pkg.id AS package_id,
    pkg.package_quantity,
    pkg.package_unit,
    t.package_transaction_type,
    enty.id AS entity_id,
    t.data_id::integer AS data_id,
    crtr.id AS creator_id
FROM (
        VALUES
        ('deposit', 'PLOT', NULL, 1)
    ) AS t (
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
    JOIN tenant.person AS crtr
        ON crtr.id = t.creator_id
    JOIN dictionary.entity AS enty
        ON enty.abbrev = t.entity_id
    JOIN tenant.crop AS crop
        ON crop.crop_code = 'WHEAT'
    JOIN germplasm.germplasm AS ge
        ON ge.crop_id = crop.id
    JOIN germplasm.seed AS sd
        ON sd.germplasm_id = ge.id
    JOIN germplasm.package AS pkg
        ON pkg.seed_id = sd.id
WHERE
    ge.designation LIKE 'WTGE%'
;

-- restore indices
-- UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~2.898s)
-- REINDEX TABLE germplasm.package_log;

-- restore triggers and constraints
--ALTER TABLE germplasm.package_log ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- total eta: ~9.36s
--*/



-- revert changes
--rollback --ALTER TABLE germplasm.package_log DISABLE TRIGGER ALL;
--rollback 
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = TRUE;
--rollback 
--rollback -- delete 500,000 records (eta: ~5.947s)
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     tenant.crop AS crop
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.crop_id = crop.id
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback     JOIN germplasm.package AS pkg
--rollback         ON pkg.seed_id = sd.id
--rollback WHERE
--rollback     pkg.id = pkglog.package_id
--rollback     AND crop.crop_code = 'WHEAT'
--rollback     AND ge.designation LIKE 'WTGE%'
--rollback ;
--rollback 
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'germplasm.package_log'::regclass::oid AND indisready = FALSE;
--rollback 
--rollback -- reindex table (eta: ~960ms)
--rollback REINDEX TABLE germplasm.package_log;
--rollback 
--rollback --ALTER TABLE germplasm.package_log ENABLE TRIGGER ALL;
--rollback 
--rollback -- total eta: ~6.907s
