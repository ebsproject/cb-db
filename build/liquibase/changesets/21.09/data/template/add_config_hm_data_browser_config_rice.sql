--liquibase formatted sql

--changeset postgres:add_config_hm_data_browser_config_rice.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-681 Add HM_DATA_BROWSER_CONFIG_<CROP> for RICE, WHEAT, and MAIZE in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_DATA_BROWSER_CONFIG_RICE',
        'HM Data Browser Configuration for Rice',
        $$			
            {      
                "CROSS_METHOD_SELFING": {
                    "fixed": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": false
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "Not applicable"
                            }
                        ],
                        "additional_required_variables": []
                    },
                    "not_fixed": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_SINGLE_SEED_DESCENT"
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_PLANTS",
                                "field_name": "noOfPlant",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 1,
                                "harvest_methods": [
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION_AND_BULK"
                                ],
                                "placeholder": "No. of plants"
                            },
                            {
                                "abbrev": "PANNO_SEL",
                                "field_name": "noOfPanicle",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 1,
                                "harvest_methods": [
                                    "HV_METH_DISC_PANICLE_SELECTION"
                                ],
                                "placeholder": "No. of panicles"
                            },
                            {
                                "abbrev": "SPECIFIC_PLANT",
                                "field_name": "specificPlantNo",
                                "type": "text",
                                "sub_type": "comma_sep_int",
                                "min": null,
                                "harvest_methods": [
                                    "HV_METH_DISC_PLANT_SPECIFIC",
                                    "HV_METH_DISC_PLANT_SPECIFIC_AND_BULK"
                                ],
                                "placeholder": "Specific plant no."
                            }
                        ],
                        "additional_required_variables": []
                    }
                },
                "CROSS_METHOD_SINGLE_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    ""
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_SEED",
                                "field_name": "noOfSeed",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 0,
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_HEAD_SINGLE_SEED_NUMBERING"
                                ],
                                "placeholder": "No. of seeds"
                            }
                        ],
                        "additional_required_variables": [
                            {
                                "abbrev": "DATE_CROSSED",
                                "field_name": "crossingDate"
                            }
                        ]
                    }
                },
                "CROSS_METHOD_DOUBLE_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    ""
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_SEED",
                                "field_name": "noOfSeed",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 0,
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "No. of seeds"
                            }
                        ],
                        "additional_required_variables": [
                            {
                                "abbrev": "DATE_CROSSED",
                                "field_name": "crossingDate"
                            }
                        ]
                    }
                },
                "CROSS_METHOD_THREE_WAY_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    ""
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_SEED",
                                "field_name": "noOfSeed",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 0,
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "No. of seeds"
                            }
                        ],
                        "additional_required_variables": [
                            {
                                "abbrev": "DATE_CROSSED",
                                "field_name": "crossingDate"
                            }
                        ]
                    }
                },
                "CROSS_METHOD_COMPLEX_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    ""
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_SEED",
                                "field_name": "noOfSeed",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 0,
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "No. of seeds"
                            }
                        ],
                        "additional_required_variables": [
                            {
                                "abbrev": "DATE_CROSSED",
                                "field_name": "crossingDate"
                            }
                        ]
                    }
                },
                "CROSS_METHOD_BACKCROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    ""
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_SEED",
                                "field_name": "noOfSeed",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 0,
                                "harvest_methods": [
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "No. of seeds"
                            }
                        ],
                        "additional_required_variables": [
                            {
                                "abbrev": "DATE_CROSSED",
                                "field_name": "crossingDate"
                            }
                        ]
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-681 Add HM_DATA_BROWSER_CONFIG_<CROP> for RICE, WHEAT, and MAIZE in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_DATA_BROWSER_CONFIG_RICE';