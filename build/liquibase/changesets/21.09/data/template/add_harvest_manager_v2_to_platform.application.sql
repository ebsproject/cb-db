--liquibase formatted sql

--changeset postgres:add_harvest_manager_v2_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-661 Add HARVEST_MANAGER_V2 in platform.application



WITH application AS (
	INSERT INTO
		platform.application (
			abbrev,
			label,
			action_label,
			icon,
			description,
			is_public,
			creator_id,
			is_void
		)
	VALUES
		(
			'HARVEST_MANAGER_V2',
			'Harvest manager 2.0',
			'Harvest manager 2.0',
			'agriculture',
			'The tool to facilitate the harvest activity of experiments. This automates creation of new germplasm, seedlots and packages according to a harvest method used.',
			true,
			1,
			false
		)
	RETURNING id
)
INSERT INTO
	platform.application_action (
		application_id,
		module,
		controller,
		action,
		creator_id,
		is_void
	)
VALUES
	(
		(
			SELECT
				id
			FROM
				application
		),
		'harvestManager',
		'occurrence-selection',
		'index',
		1,
		false
	)


--rollback DELETE FROM platform.application WHERE abbrev='HARVEST_MANAGER_V2';