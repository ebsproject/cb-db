--liquibase formatted sql

--changeset postgres:add_config_hm_search_parameters_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-660 Add config HM_SEARCH_PARAMETERS in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_SEARCH_PARAMETERS',
        'HM Search Paramaters',
        $$			      
            {
                "name": "Harvest Manager Filters",
                "values": [
                    {
                        "disabled": "true",
                        "required": "false",
                        "input_type": "single",
                        "field_label": "Program",
                        "input_field": "selection",
                        "order_number": "1",
                        "default_value": "",
                        "allowed_values": "",
                        "basic_parameter": "true",
                        "variable_abbrev": "PROGRAM",
                        "field_description": "Program that manages the experiment"
                    },
                    {
                        "disabled": "false",
                        "required": "false",
                        "input_type": "single",
                        "field_label": "Experiment",
                        "input_field": "selection",
                        "order_number": "3",
                        "default_value": "",
                        "allowed_values": "",
                        "basic_parameter": "true",
                        "variable_abbrev": "EXPERIMENT_NAME",
                        "field_description": "Name of the experiment"
                    },
                    {
                        "disabled": "false",
                        "required": "true",
                        "input_type": "single",
                        "field_label": "Occurrence",
                        "input_field": "selection",
                        "order_number": "5",
                        "default_value": "",
                        "allowed_values": "",
                        "basic_parameter": "true",
                        "variable_abbrev": "OCCURRENCE_NAME",
                        "field_description": "Name of the occurrence"
                    }
                ]
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-660 Add config HM_SEARCH_PARAMETERS in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_SEARCH_PARAMETERS';