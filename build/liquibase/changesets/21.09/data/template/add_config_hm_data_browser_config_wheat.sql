--liquibase formatted sql

--changeset postgres:add_config_hm_data_browser_config_wheat.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-681 Add HM_DATA_BROWSER_CONFIG_<CROP> for RICE, WHEAT, and MAIZE in platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_DATA_BROWSER_CONFIG_WHEAT',
        'HM Data Browser Configuration for Wheat',
        $$			
            {
                "CROSS_METHOD_SELFING": {
                    "fixed": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "Not applicable"
                            }
                        ],
                        "additional_required_variables": []
                    },
                    "not_fixed": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            },
                            {
                                "column_name": "numericVar",
                                "abbrev": "<none>",
                                "required": null
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK",
                                    "HV_METH_DISC_SELECTED_BULK"
                                ],
                                "placeholder": "Not applicable"
                            },
                            {
                                "abbrev": "NO_OF_PLANTS",
                                "field_name": "noOfPlant",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": 1,
                                "harvest_methods": [
                                    "HV_METH_DISC_SELECTED_BULK",
                                    "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                    "HV_METH_DISC_INDIVIDUAL_SPIKE"
                                ],
                                "placeholder": "No. of plants"
                            }
                        ],
                        "additional_required_variables": []
                    }
                },
                "CROSS_METHOD_SINGLE_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "Not applicable"
                            }
                        ],
                        "additional_required_variables": []
                    }
                },
                "CROSS_METHOD_TOP_CROSS": {
                    "default": {
                        "input_columns": [
                            {
                                "column_name": "harvestDate",
                                "abbrev": "HVDATE_CONT",
                                "required": true
                            },
                            {
                                "column_name": "harvestMethod",
                                "abbrev": "HV_METH_DISC",
                                "required": true
                            }
                        ],
                        "numeric_variables": [
                            {
                                "abbrev": "<none>",
                                "field_name": "<none>",
                                "type": "number",
                                "sub_type": "single_int",
                                "min": null,
                                "harvest_methods": [
                                    "",
                                    "HV_METH_DISC_BULK"
                                ],
                                "placeholder": "Not applicable"
                            }
                        ],
                        "additional_required_variables": []
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-681 Add HM_DATA_BROWSER_CONFIG_<CROP> for RICE, WHEAT, and MAIZE in platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_DATA_BROWSER_CONFIG_WHEAT';