--liquibase formatted sql

--changeset postgres:create_gin_index_for_germplasm_normalized_name_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-698 Create gin index for germplasm_normalized_name column



-- create extension
CREATE EXTENSION IF NOT EXISTS pg_trgm;

-- create additional index for germplasm_normalized_name column to speed up partial searches
CREATE INDEX IF NOT EXISTS
    germplasm_name_normalized_name_trgm_idx
ON
    germplasm.germplasm_name
USING
    gin (
        germplasm_normalized_name gin_trgm_ops
    )
;

CREATE INDEX IF NOT EXISTS
    germplasm_normalized_name_trgm_idx
ON
    germplasm.germplasm
USING
    gin (
        germplasm_normalized_name gin_trgm_ops
    )
;



-- revert changes
--rollback DROP INDEX IF EXISTS germplasm.germplasm_normalized_name_trgm_idx;
--rollback DROP INDEX IF EXISTS germplasm.germplasm_name_normalized_name_trgm_idx;
