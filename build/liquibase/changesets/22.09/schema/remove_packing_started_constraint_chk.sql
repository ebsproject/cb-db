--liquibase formatted sql

--changeset author_name:remove_packing_started_constraint_chk context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1431 PIM: Remove packing started check constraint



-- Remove "packing started as valid status value"
ALTER TABLE experiment.planting_job
 DROP CONSTRAINT planting_job_status_chk,
    ADD CONSTRAINT planting_job_status_chk CHECK (planting_job_status::text = ANY (ARRAY[
        'draft'::text,
        'ready for packing'::text,
        'packing'::text, 'packed'::text,
        'packing on hold'::text,
        'packing cancelled'::text,
        'updating entries in progress'::text
    ]));



--rollback ALTER TABLE experiment.planting_job
--rollback     DROP CONSTRAINT planting_job_status_chk,
--rollback     ADD CONSTRAINT planting_job_status_chk CHECK (planting_job_status::text = ANY (ARRAY[
--rollback         'draft'::text,
--rollback         'ready for packing'::text,
--rollback         'packing'::text, 'packed'::text,
--rollback         'packing on hold'::text,
--rollback         'packing cancelled'::text,
--rollback         'updating entries in progress'::text,
--rollback         'packing started'::text
--rollback     ]));
