--liquibase formatted sql

--changeset postgres:add_column_file_upload_origin context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1402 Add column file_upload_origin to germplasm.file_upload



ALTER TABLE
    germplasm.file_upload
ADD COLUMN
    file_upload_origin character varying
    NOT NULL
    DEFAULT 'GERMPLASM_CATALOG'
;

COMMENT ON COLUMN germplasm.file_upload.file_upload_origin
    IS 'File Upload Origin: It contains application abbrevs from platform.application table [GEFILEUPLOAD_FILEUPLOADORIGIN]';

-- create index for regular column
CREATE INDEX file_upload_file_upload_origin_idx
    ON germplasm.file_upload USING btree (file_upload_origin)
;



--rollback ALTER TABLE germplasm.file_upload DROP COLUMN file_upload_origin;