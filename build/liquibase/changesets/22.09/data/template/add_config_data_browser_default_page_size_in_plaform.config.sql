--liquibase formatted sql

--changeset postgres:add_config_data_browser_default_page_size_in_plaform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4023 Add config DATA_BROWSER_DEFAULT_PAGE_SIZE in platform.config



INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'DATA_BROWSER_DEFAULT_PAGE_SIZE',
    'The default number of results a data browser can display per page',
    '50',
    1,
    'data_browser',
    1,
    'added by j.castillo'
);



--rollback DELETE FROM platform.config WHERE abbrev='DATA_BROWSER_DEFAULT_PAGE_SIZE';