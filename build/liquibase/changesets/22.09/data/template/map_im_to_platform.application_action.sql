--liquibase formatted sql

--changeset postgres:map_im_to_platform.application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4067 Map inventory manager in platform.application_action



INSERT INTO
    platform.application_action (application_id, module, creator_id)
SELECT
    id,
    'inventoryManager',
    1
FROM
    platform.application
WHERE
    abbrev = 'INVENTORY_MANAGER';



--rollback DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev = 'INVENTORY_MANAGER');