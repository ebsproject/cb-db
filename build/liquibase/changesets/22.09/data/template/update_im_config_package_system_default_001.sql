--liquibase formatted sql

--changeset postgres:update_im_config_package_system_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 HM DB: Fix typos in IM configs



-- update browser config
UPDATE platform.config
SET
    config_value = $${
        "values":[
            {
                "entity": "package",
                "type": "column",
                "name": "Seed Code",
                "abbrev": "SEED_CODE",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "seedDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "seeds-search",
                "value_filter": "seedCode",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "seedDbId"
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Seed Name",
                "abbrev": "SEED_NAME",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "true",
                "api_field": "",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "seeds-search",
                "value_filter": "seedName",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "seedDbId"
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Label",
                "abbrev": "PACKAGE_LABEL",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageLabel",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Program",
                "abbrev": "PROGRAM",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "programs-search",
                "value_filter": "programCode",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "programDbId"
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Status",
                "abbrev": "PACKAGE_STATUS",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageStatus",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            }
        ]
    }$$
WHERE
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values":[
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Seed Code",
--rollback                 "abbrev": "SEED_CODE",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "seedDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "seeds-search",
--rollback                 "value_filter": "seedCode",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "seedDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Seed Name",
--rollback                 "abbrev": "SEED_NAME",
--rollback                 "data_type": "string",
--rollback                 "usage": "optional",
--rollback                 "required": "false",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "true",
--rollback                 "api_field": "",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "seeds-search",
--rollback                 "value_filter": "seedName",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "seedDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Label",
--rollback                 "abbrev": "PACKAGE_LABEL",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageLabel",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Program",
--rollback                 "abbrev": "PROGRAM",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "programDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "programs-search",
--rollback                 "value_filter": "programCode",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "programDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Status",
--rollback                 "abbrev": "PACKAGE_STATUS",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageStatus",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_SYSTEM_DEFAULT';