--liquibase formatted sql

--changeset postgres:append_trait_data_collected_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1391 Append trait data collected occurrence status



-- update occurrence status
UPDATE
    experiment.occurrence AS occ
SET
    occurrence_status = platform.append_text(occ.occurrence_status, 'trait data collected', ';')
FROM (
        SELECT
            occ.id AS occurrence_id,
            occ.occurrence_name,
            occ.occurrence_status,
            t.*
        FROM
            experiment.occurrence AS occ
            INNER JOIN LATERAL (
                SELECT
                    var.abbrev AS at_least_one_trait_observation
                FROM
                    experiment.plot AS plot
                    INNER JOIN experiment.plot_data AS plotdata
                        ON plotdata.plot_id = plot.id
                    INNER JOIN master.variable AS var
                        ON var.id = plotdata.variable_id
                WHERE
                    plot.occurrence_id = occ.id
                    AND var.type = 'observation'
                LIMIT
                    1
            ) AS t
                ON TRUE
    ) AS t
WHERE
    occ.id = t.occurrence_id
    AND occ.occurrence_status NOT LIKE '%trait data collected%'
;



-- revert changes
--rollback SELECT NULL;
