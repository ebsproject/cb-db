--liquibase formatted sql

--changeset postgres:add_im_config_package_maize_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 Implement new configurations for Inventory Manager


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_FILE_UPLOAD_PACKAGE_MAIZE_DEFAULT',
    'Inventory Manager File Upload configuration for MAIZE variables - package only', 
    $${
        "values":[
            {
                "entity": "package",
                "type": "column",
                "name": "Package Quantity",
                "abbrev": "VOLUME",
                "data_type": "float",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Unit",
                "abbrev": "PACKAGE_UNIT",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Facility Code",
                "abbrev": "FACILITY_CODE",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "facilityDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "facilities-search",
                "value_filter": "facilityCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "facilityDbId"
            }
        ]
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_FILE_UPLOAD_PACKAGE_MAIZE_DEFAULT';
