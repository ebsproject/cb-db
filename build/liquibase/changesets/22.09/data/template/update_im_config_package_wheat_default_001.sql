--liquibase formatted sql

--changeset postgres:update_im_config_package_wheat_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 HM DB: Fix typos in IM configs



-- update browser config
UPDATE platform.config
SET
    config_value = $${
        "values":[
            {
                "entity": "package",
                "type": "column",
                "name": "Package Quantity",
                "abbrev": "VOLUME",
                "data_type": "float",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Unit",
                "abbrev": "PACKAGE_UNIT",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Facility Code",
                "abbrev": "FACILITY_CODE",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "facilityDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "facilities-search",
                "value_filter": "facilityCode",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "facilityDbId"
            }
        ]
    }$$
WHERE
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values":[
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Quantity",
--rollback                 "abbrev": "VOLUME",
--rollback                 "data_type": "float",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageQuantity",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Unit",
--rollback                 "abbrev": "PACKAGE_UNIT",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageUnit",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Facility Code",
--rollback                 "abbrev": "FACILITY_CODE",
--rollback                 "data_type": "string",
--rollback                 "usage": "optional",
--rollback                 "required": "false",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "facilityDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "facilities-search",
--rollback                 "value_filter": "facilityCode",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "facilityDbId"
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT';