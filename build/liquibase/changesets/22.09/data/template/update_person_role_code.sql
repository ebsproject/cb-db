--liquibase formatted sql

--changeset author_name:update_person_role_code context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1395 Update person_role_code to CB_USER



UPDATE
    tenant.person_role
SET
    person_role_code = 'CB_USER'
WHERE
    person_role_code = 'B4R_USER'
;



-- revert changes
--rollback UPDATE
--rollback     tenant.person_role
--rollback SET
--rollback     person_role_code = 'B4R_USER'
--rollback WHERE
--rollback     person_role_code = 'CB_USER'
--rollback ;