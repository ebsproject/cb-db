--liquibase formatted sql

--changeset postgres:add_im_config_seed_package_wheat_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 Implement new configurations for Inventory Manager


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_FILE_UPLOAD_SEED_PACKAGE_WHEAT_DEFAULT',
    'Inventory Manager File Upload configuration for WHEAT variables - seed and package', 
    $${
        "values":[
            {
                "entity": "seed",
                "type": "column",
                "name": "Program Code",
                "abbrev": "PROGRAM_CODE",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "programs-search",
                "value_filter": "programCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "programDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Harvest Date",
                "abbrev": "HVDATE_CONT",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "harvestDate",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Harvest Method",
                "abbrev": "HV_METH_DISC",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "harvestMethod",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Source Experiment Code",
                "abbrev": "EXPERIMENT_CODE",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "sourceExperimentDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "experiments-search",
                "value_filter": "experimentCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "experimentDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Desctription",
                "abbrev": "DESCRIPTION",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "desctription",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Quantity",
                "abbrev": "VOLUME",
                "data_type": "float",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageQuantity",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Unit",
                "abbrev": "PACKAGE_UNIT",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageUnit",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Facility Code",
                "abbrev": "FACILITY_CODE",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "facilityDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "facilities-search",
                "value_filter": "facilityCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "facilityDbId"
            }
        ]
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_WHEAT_DEFAULT';
