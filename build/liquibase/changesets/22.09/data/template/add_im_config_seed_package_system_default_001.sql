--liquibase formatted sql

--changeset postgres:add_im_config_seed_package_system_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 Implement new configurations for Inventory Manager


INSERT INTO
    platform.config(
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES (
    'IM_FILE_UPLOAD_SEED_PACKAGE_CONFIG_SYSTEM_DEFAULT',
    'Inventory Manager File Upload configuration for system-default variables - seed and package', 
    $${
        "values":[
            {
                "entity": "seed",
                "type": "column",
                "name": "Germplasm Code",
                "abbrev": "GERMPLASM_CODE",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "germplasmDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "germplasm-search",
                "value_filter": "germplasmCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "germplasmDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Germplasm Name",
                "abbrev": "DESIGNATION",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "true",
                "api_field": "",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "germplasm-names-search",
                "value_filter": "nameValue",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "germplasmDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Seed Name",
                "abbrev": "SEED_NAME",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "seedName",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Label",
                "abbrev": "PACKAGE_LABEL",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageLabel",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Program",
                "abbrev": "PROGRAM",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "programs-search",
                "value_filter": "programCode",
                "url_paramters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "programDbId"
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Status",
                "abbrev": "PACKAGE_STATUS",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageStatus",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_paramters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            }
        ]
    }$$,
    1,
    'Inventory Manager file upload and data validation',
    1,
    'added by j.bantay'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_CONFIG_SYSTEM_DEFAULT';
