--liquibase formatted sql

--changeset author_name:show_test_scale_value_in_entry_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1409 Show test scale value in ENTRY_TYPE



UPDATE
    master.scale_value
SET
    scale_value_status = 'show'
WHERE
    abbrev = 'ENTRY_TYPE_TEST'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     scale_value_status = 'hide'
--rollback WHERE
--rollback     abbrev = 'ENTRY_TYPE_TEST'
--rollback ;