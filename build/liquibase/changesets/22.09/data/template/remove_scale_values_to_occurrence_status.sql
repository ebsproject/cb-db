--liquibase formatted sql

--changeset author_name:remove_scale_values_to_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1432 Remove scale values to OCCURRENCE_STATUS



DELETE FROM
    master.scale_value
WHERE
    abbrev IN (
        'OCCURRENCE_STATUS_PLANTED_PACKING_STARTED'
    )
;



-- revert changes
--rollback WITH t_last_order_number AS (
--rollback     SELECT
--rollback         COALESCE(MAX(scalval.order_number), 0) AS last_order_number
--rollback     FROM
--rollback         master.variable AS var
--rollback         LEFT JOIN master.scale_value AS scalval
--rollback             ON scalval.scale_id = var.scale_id
--rollback     WHERE
--rollback         var.abbrev = 'OCCURRENCE_STATUS'
--rollback         AND var.is_void = FALSE
--rollback         AND scalval.is_void = FALSE
--rollback )
--rollback INSERT INTO master.scale_value
--rollback     (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.value,
--rollback     t.last_order_number + ROW_NUMBER() OVER () AS order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.scale_value_status,
--rollback     var.abbrev || '_' || scalval.abbrev AS abbrev,
--rollback     var.creator_id
--rollback FROM
--rollback     master.variable AS var,
--rollback     t_last_order_number AS t,
--rollback     (
--rollback         VALUES
--rollback         ('planted;packing started', 'Planted; Packing Started', 'Planted; Packing Started', 'PLANTED_PACKING_STARTED', 'show')
--rollback     ) AS scalval (
--rollback         value, description, display_name, abbrev, scale_value_status
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'OCCURRENCE_STATUS'
--rollback ;