--liquibase formatted sql

--changeset author_name:update_abbrev_of_mta_and_import_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1407 Update abbrev of MTA and IMPORT variables



UPDATE
    master.variable AS var
SET
    abbrev = t.new_abbrev
FROM (
        VALUES
        ('MTA', 'MTA_NUMBER'),
        ('IMPORT', 'IMPORT_ATTRIBUTE')
) AS t (old_abbrev, new_abbrev)
WHERE
    abbrev = t.old_abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     abbrev = t.new_abbrev
--rollback FROM (
--rollback         VALUES
--rollback         ('MTA_NUMBER', 'MTA'),
--rollback         ('IMPORT_ATTRIBUTE', 'IMPORT')
--rollback ) AS t (old_abbrev, new_abbrev)
--rollback WHERE
--rollback     abbrev = t.old_abbrev
--rollback ;