--liquibase formatted sql

--changeset author_name:add_scale_values_to_occurrence_status_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1433 Add scale values to OCCURRENCE_STATUS p2



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'OCCURRENCE_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('planted;packed;trait data collected', 'Planted; Packed; Trait Data Collected', 'Planted; Packed; Trait Data Collected', 'PLANTED_PACKED_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;packing;trait data collected', 'Planted; Packing; Trait Data Collected', 'Planted; Packing; Trait Data Collected', 'PLANTED_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;packing cancelled;trait data collected', 'Planted; Packing Cancelled; Trait Data Collected', 'Planted; Packing Cancelled; Trait Data Collected', 'PLANTED_PACKING_CANCELLED_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;packing on hold;trait data collected', 'Planted; Packing on Hold; Trait Data Collected', 'Planted; Packing on Hold; Trait Data Collected', 'PLANTED_PACKING_ON_HOLD_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;ready for packing;trait data collected', 'Planted; Ready for Packing; Trait Data Collected', 'Planted; Ready for Packing; Trait Data Collected', 'PLANTED_READY_FOR_PACKING_TRAIT_DATA_COLLECTED', 'show'),
        ('planted;trait data collected;packed', 'Planted; Trait Data Collected; Packed', 'Planted; Trait Data Collected; Packed', 'PLANTED_TRAIT_DATA_COLLECTED_PACKED', 'show'),
        ('planted;trait data collected;packing', 'Planted; Trait Data Collected; Packing', 'Planted; Trait Data Collected; Packing', 'PLANTED_TRAIT_DATA_COLLECTED_PACKING', 'show'),
        ('planted;trait data collected;packing cancelled', 'Planted; Trait Data Collected; Packing Cancelled', 'Planted; Trait Data Collected; Packing Cancelled', 'PLANTED_TRAIT_DATA_COLLECTED_PACKING_CANCELLED', 'show'),
        ('planted;trait data collected;packing on hold', 'Planted; Trait Data Collected; Packing on Hold', 'Planted; Trait Data Collected; Packing on Hold', 'PLANTED_TRAIT_DATA_COLLECTED_PACKING_ON_HOLD', 'show'),
        ('planted;trait data collected;ready for packing', 'Planted; Trait Data Collected; Ready for Packing', 'Planted; Trait Data Collected; Ready for Packing', 'PLANTED_TRAIT_DATA_COLLECTED_READY_FOR_PACKING', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKED_TRAIT_DATA_COLLECTED',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING_TRAIT_DATA_COLLECTED',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING_CANCELLED_TRAIT_DATA_COLLECTED',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING_ON_HOLD_TRAIT_DATA_COLLECTED',
--rollback         'OCCURRENCE_STATUS_PLANTED_READY_FOR_PACKING_TRAIT_DATA_COLLECTED',
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_PACKED',
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_PACKING',
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_PACKING_CANCELLED',
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_PACKING_ON_HOLD',
--rollback         'OCCURRENCE_STATUS_PLANTED_TRAIT_DATA_COLLECTED_READY_FOR_PACKING'
--rollback     )
--rollback ;