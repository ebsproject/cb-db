--liquibase formatted sql

--changeset author_name:update_im_config_abbrevs_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 Implement new configurations for Inventory Manager



-- update seed package system default
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_SYSTEM_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_CONFIG_SYSTEM_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_CONFIG_SYSTEM_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_SYSTEM_DEFAULT'
--rollback ;


-- update seed package maize
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_MAIZE_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_MAIZE_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_MAIZE_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_MAIZE_DEFAULT'
--rollback ;


-- update seed package rice
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_RICE_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_RICE_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_RICE_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_RICE_DEFAULT'
--rollback ;


-- update seed package wheat
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_WHEAT_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_WHEAT_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_SEED_PACKAGE_WHEAT_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_WHEAT_DEFAULT'
--rollback ;


-- update package system default
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_SYSTEM_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_PACKAGE_CONFIG_SYSTEM_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_PACKAGE_CONFIG_SYSTEM_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_SYSTEM_DEFAULT'
--rollback ;


-- update package maize
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_MAIZE_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_PACKAGE_MAIZE_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_PACKAGE_MAIZE_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_MAIZE_DEFAULT'
--rollback ;


-- update package rice
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_RICE_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_PACKAGE_RICE_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_PACKAGE_RICE_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_RICE_DEFAULT'
--rollback ;


-- update package wheat
UPDATE
    platform.config
SET
    abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT'
WHERE
    abbrev = 'IM_FILE_UPLOAD_PACKAGE_WHEAT_DEFAULT'
;


-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'IM_FILE_UPLOAD_PACKAGE_WHEAT_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_PACKAGE_WHEAT_DEFAULT'
--rollback ;