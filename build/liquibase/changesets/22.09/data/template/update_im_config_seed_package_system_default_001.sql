--liquibase formatted sql

--changeset postgres:update_im_config_seed_package_system_default_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4005 HM DB: Fix typos in IM configs



-- update browser config
UPDATE platform.config
SET
    config_value = $${
        "values":[
            {
                "entity": "seed",
                "type": "column",
                "name": "Germplasm Code",
                "abbrev": "GERMPLASM_CODE",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "germplasmDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "germplasm-search",
                "value_filter": "germplasmCode",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "germplasmDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Germplasm Name",
                "abbrev": "DESIGNATION",
                "data_type": "string",
                "usage": "optional",
                "required": "false",
                "visible": "true",
                "skip_creation": "true",
                "api_field": "",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "germplasm-names-search",
                "value_filter": "nameValue",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "germplasmDbId"
            },
            {
                "entity": "seed",
                "type": "column",
                "name": "Seed Name",
                "abbrev": "SEED_NAME",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "seedName",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Label",
                "abbrev": "PACKAGE_LABEL",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageLabel",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Program",
                "abbrev": "PROGRAM",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "programDbId",
                "retrieve_db_id": "true",
                "http_method": "POST",
                "search_endpoint": "programs-search",
                "value_filter": "programCode",
                "url_parameters": "limit=1",
                "additional_filters": {},
                "db_id_api_field": "programDbId"
            },
            {
                "entity": "package",
                "type": "column",
                "name": "Package Status",
                "abbrev": "PACKAGE_STATUS",
                "data_type": "string",
                "usage": "required",
                "required": "true",
                "visible": "true",
                "skip_creation": "false",
                "api_field": "packageStatus",
                "retrieve_db_id": "false",
                "http_method": "",
                "search_endpoint": "",
                "value_filter": "",
                "url_parameters": "",
                "additional_filters": {},
                "db_id_api_field": ""
            }
        ]
    }$$
WHERE
    abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values":[
--rollback             {
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "name": "Germplasm Code",
--rollback                 "abbrev": "GERMPLASM_CODE",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "germplasmDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "germplasm-search",
--rollback                 "value_filter": "germplasmCode",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "germplasmDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "name": "Germplasm Name",
--rollback                 "abbrev": "DESIGNATION",
--rollback                 "data_type": "string",
--rollback                 "usage": "optional",
--rollback                 "required": "false",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "true",
--rollback                 "api_field": "",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "germplasm-names-search",
--rollback                 "value_filter": "nameValue",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "germplasmDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "seed",
--rollback                 "type": "column",
--rollback                 "name": "Seed Name",
--rollback                 "abbrev": "SEED_NAME",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "seedName",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Label",
--rollback                 "abbrev": "PACKAGE_LABEL",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageLabel",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Program",
--rollback                 "abbrev": "PROGRAM",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "programDbId",
--rollback                 "retrieve_db_id": "true",
--rollback                 "http_method": "POST",
--rollback                 "search_endpoint": "programs-search",
--rollback                 "value_filter": "programCode",
--rollback                 "url_paramters": "limit=1",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": "programDbId"
--rollback             },
--rollback             {
--rollback                 "entity": "package",
--rollback                 "type": "column",
--rollback                 "name": "Package Status",
--rollback                 "abbrev": "PACKAGE_STATUS",
--rollback                 "data_type": "string",
--rollback                 "usage": "required",
--rollback                 "required": "true",
--rollback                 "visible": "true",
--rollback                 "skip_creation": "false",
--rollback                 "api_field": "packageStatus",
--rollback                 "retrieve_db_id": "false",
--rollback                 "http_method": "",
--rollback                 "search_endpoint": "",
--rollback                 "value_filter": "",
--rollback                 "url_paramters": "",
--rollback                 "additional_filters": {},
--rollback                 "db_id_api_field": ""
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'IM_FILE_UPLOAD_CONFIG_SEED_PACKAGE_SYSTEM_DEFAULT';