--liquibase formatted sql

--changeset postgres:add_im_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4067 Add inventory manager in platform.application



INSERT INTO
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES
    ('INVENTORY_MANAGER', 'Inventory manager', 'Manage inventory', 'inventory', 1);



--rollback DELETE FROM platform.application WHERE abbrev = 'INVENTORY_MANAGER';