--liquibase formatted sql

--changeset author_name:add_replacement_type_replace_with_germplasm context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-4171 PIM DB: Add REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM record to master.scale_value



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'REPLACEMENT_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('Replace with germplasm', 'Replace with germplasm', 'Replace with germplasm', 'REPLACE_WITH_GERMPLASM', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'REPLACEMENT_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'REPLACEMENT_TYPE_REPLACE_WITH_GERMPLASM'
--rollback     )
--rollback ;