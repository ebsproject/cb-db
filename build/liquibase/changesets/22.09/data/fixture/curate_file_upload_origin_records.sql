--liquibase formatted sql

--changeset author_name:curate_file_upload_origin_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1403 Curate file_upload_origin records



UPDATE
    germplasm.file_upload
SET
    file_upload_origin = 'GERMPLASM_CATALOG'
WHERE
    file_upload_origin IS NULL
AND 
    DATE(creation_timestamp) <= DATE('2022-09-28')
;



-- revert changes
--rollback SELECT NULL;