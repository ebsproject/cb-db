--liquibase formatted sql

--changeset postgres:update_constraint_germplasm_file_upload context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-5571: GM - Add one new scale value for FILE_UPLOAD_ACTION variable



ALTER TABLE
    germplasm.file_upload
DROP CONSTRAINT IF EXISTS
    file_upload_file_upload_action_chk;
	
ALTER TABLE 
    germplasm.file_upload
ADD CONSTRAINT 
    file_upload_file_upload_action_chk CHECK (file_upload_action::text = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text, 'merge'::text]));

ALTER TABLE
    germplasm.file_upload
DROP CONSTRAINT IF EXISTS
    file_upload_file_status_chk;
	
ALTER TABLE 
    germplasm.file_upload
ADD CONSTRAINT 
    file_upload_file_status_chk CHECK (
        file_status::text = ANY (
            ARRAY['in queue'::text, 'created'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text, 'merge ready'::text, 'merging in progress'::text, 'merge failed'::text]
    ));



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback DROP CONSTRAINT IF EXISTS
--rollback     file_upload_file_upload_action_chk;
	
--rollback ALTER TABLE 
--rollback     germplasm.file_upload
--rollback ADD CONSTRAINT 
--rollback     file_upload_file_upload_action_chk CHECK (file_upload_action::text = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text]));

-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.file_upload
--rollback DROP CONSTRAINT IF EXISTS
--rollback     file_upload_file_status_chk;
	
--rollback ALTER TABLE 
--rollback     germplasm.file_upload
--rollback ADD CONSTRAINT 
--rollback     file_upload_file_status_chk CHECK (file_status::text = ANY (ARRAY['in queue'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text]));