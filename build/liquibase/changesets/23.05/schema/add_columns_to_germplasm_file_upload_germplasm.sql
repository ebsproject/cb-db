--liquibase formatted sql

--changeset postgres:add_columns_to_germplasm_file_upload_germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-5570 GM: Add new columns to germplasm.file_upload_germplasm



-- add entity_id column
ALTER TABLE
    germplasm.file_upload_germplasm
ADD COLUMN
    entity_id integer,
ADD COLUMN
    entity VARCHAR(32);

COMMENT ON COLUMN germplasm.file_upload_germplasm.entity_id
    IS 'Entity ID: Unique identifier of referenced entity record.';

COMMENT ON COLUMN germplasm.file_upload_germplasm.entity
    IS 'Entity: Indicator of the type of entity being referenced.';

-- add index to new column
CREATE INDEX file_upload_germplasm_entity_id_idx
    ON germplasm.file_upload_germplasm USING btree (entity_id);

-- add index to new column
CREATE INDEX file_upload_germplasm_entity_idx
    ON germplasm.file_upload_germplasm USING btree (entity);



-- revert changes
--rollback DROP INDEX germplasm.file_upload_germplasm_entity_id_idx;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.file_upload_germplasm
--rollback DROP COLUMN
--rollback     entity_id
--rollback ;
--rollback
--rollback DROP INDEX germplasm.file_upload_germplasm_entity_idx;
--rollback 
--rollback ALTER TABLE
--rollback     germplasm.file_upload_germplasm
--rollback DROP COLUMN
--rollback     entity
--rollback ;