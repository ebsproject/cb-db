--liquibase formatted sql

--changeset postgres:add_function_get_parent_germplasm context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2452 Add function get_parent_germplasm



CREATE OR REPLACE FUNCTION germplasm.get_parent_germplasm(germplasm_id INTEGER) RETURNS TABLE (
   child INTEGER,
   root INTEGER,
   order_number INTEGER,
   designation VARCHAR,
   DEPTH INTEGER
) AS $$
BEGIN
    RETURN QUERY
    WITH initialLevel AS (
        SELECT
            c.id,
            c.child_germplasm_id AS child,
            c.parent_germplasm_id AS root,
            c.order_number,
            1 AS depth
        FROM
            germplasm.germplasm_relation c
        WHERE 
            c.is_void = FALSE
            AND c.child_germplasm_id = germplasm_id
    ), intermediateLevel AS (
        SELECT
            t.id,
            g.child_germplasm_id AS child,
            g.parent_germplasm_id AS root,
            g.order_number,
            t.depth + 1 AS depth
        FROM
            initialLevel t
             JOIN germplasm.germplasm_relation g ON g.child_germplasm_id = t.root
    ), collatedRes AS (
        SELECT
            t.child,
            t.root,
            t.order_number,
            g.designation,
            t.depth
        FROM
            initialLevel t
            JOIN germplasm.germplasm g ON g.id = t.root
        UNION ALL
        SELECT
            t.child,
            t.root,
            t.order_number,
            g.designation,
            t.DEPTH 
        FROM
            intermediateLevel t
            JOIN germplasm.germplasm g ON g.id = t.root
    )
    SELECT
        t.child,
        t.root,
        t.order_number,
        t.designation,
        t.depth
    FROM
        collatedRes t;
END;
$$ LANGUAGE plpgsql;



-- revert changes
--rollback DROP FUNCTION germplasm.get_parent_germplasm;