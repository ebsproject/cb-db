--liquibase formatted sql

--changeset postgres:add_enable_rbac_flag_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5609 Add ENABLE_RBAC_FLAT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id
    )
VALUES
    (
        'ENABLE_RBAC_FLAG',
        'Flag for enabling RBAC for Core Breeding',
        $$			
        false
        $$,
        1,
        'RBAC',
        1
    )
;



-- revert changes
-- rollback DELETE FROM platform.config WHERE abbrev = 'ENABLE_RBAC_FLAG';