--liquibase formatted sql

--changeset postgres:insert_gm_create_germplasm_relation_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5458 CORB-5503 GM-DB: Create system-default configuration for the upload germplasm relations template file.



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'GM_CREATE_GERMPLASM_RELATION_SYSTEM_DEFAULT',
        'Germplasm Manager - CREATE Germplasm Relation System-default Config',
        $$
        {
            "values": [
                {
                    "name": "Parent Germplasm Code",
                    "type": "column",
                    "view": {
                        "visible": "false",
                        "entities": []
                    },
                    "usage": "required",
                    "abbrev": "PARENT_GERMPLASM_CODE",
                    "entity": "germplasm_relation",
                    "required": "true",
                    "api_field": "germplasmDbId",
                    "data_type": "string",
                    "http_method": "POST",
                    "value_filter": "germplasmCode",
                    "skip_creation": "false",
                    "retrieve_db_id": "true",
                    "url_parameters": "limit=1",
                    "db_id_api_field": "germplasmDbId",
                    "search_endpoint": "germplasm-search",
                    "additional_filters": {}
                },
                {
                    "name": "Parent Designation",
                    "type": "column",
                    "view": {
                        "visible": "true",
                        "entities": [
                            "germplasm_relation"
                        ]
                    },
                    "usage": "optional",
                    "abbrev": "PARENT_DESIGNATION",
                    "entity": "germplasm_relation",
                    "required": "false",
                    "api_field": "",
                    "data_type": "string",
                    "http_method": "POST",
                    "value_filter": "nameValue",
                    "skip_creation": "true",
                    "retrieve_db_id": "true",
                    "url_parameters": "limit=1",
                    "db_id_api_field": "germplasmDbId",
                    "search_endpoint": "germplasm-names-search",
                    "additional_filters": {}
                },
                {
                    "name": "Child Germplasm Code",
                    "type": "column",
                    "view": {
                        "visible": "false",
                        "entities": []
                    },
                    "usage": "required",
                    "abbrev": "CHILD_GERMPLASM_CODE",
                    "entity": "germplasm_relation",
                    "required": "true",
                    "api_field": "germplasmDbId",
                    "data_type": "string",
                    "http_method": "POST",
                    "value_filter": "germplasmCode",
                    "skip_creation": "false",
                    "retrieve_db_id": "true",
                    "url_parameters": "limit=1",
                    "db_id_api_field": "germplasmDbId",
                    "search_endpoint": "germplasm-search",
                    "additional_filters": {}
                },
                {
                    "name": "Child Designation",
                    "type": "column",
                    "view": {
                        "visible": "true",
                        "entities": [
                            "germplasm_relation"
                        ]
                    },
                    "usage": "optional",
                    "abbrev": "CHILD_DESIGNATION",
                    "entity": "germplasm_relation",
                    "required": "false",
                    "api_field": "",
                    "data_type": "string",
                    "http_method": "POST",
                    "value_filter": "nameValue",
                    "skip_creation": "true",
                    "retrieve_db_id": "true",
                    "url_parameters": "limit=1",
                    "db_id_api_field": "germplasmDbId",
                    "search_endpoint": "germplasm-names-search",
                    "additional_filters": {}
                },
                {
                    "name": "Order Number",
                    "type": "column",
                    "view": {
                        "visible": "true",
                        "entities": [
                            "germplasm_relation"
                        ]
                    },
                    "usage": "required",
                    "abbrev": "ORDER_NUMBER",
                    "entity": "germplasm_relation",
                    "required": "true",
                    "api_field": "packageLabel",
                    "data_type": "integer",
                    "http_method": "",
                    "value_filter": "",
                    "skip_creation": "false",
                    "retrieve_db_id": "false",
                    "url_parameters": "",
                    "db_id_api_field": "",
                    "search_endpoint": "",
                    "additional_filters": {}
                }
            ]
        }
        $$,
        'germplasm_manager'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_CREATE_GERMPLASM_RELATION_SYSTEM_DEFAULT';
