--liquibase formatted sql

--changeset postgres:insert_brapi_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5463 DC-DB: Add BrAPI global, program-specific and collaborator role settings



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'BRAPI_GLOBAL_OCCURRENCE_SETTINGS',
        'BrAPI Global Occurrence-level Settings',
        '{
            "variables": [
                "OCCURRENCE_NAME",
                "ENTNO",
                "PLOT_CODE",
                "PLOTNO",
                "REP",
                "DESIGNATION",
                "FIELD_X",
                "FIELD_Y"
            ],
            "traits": [
                "MC_CONT",
                "AYLD_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS",
                    "WS"
                ]
            }
        }',
        'data_collection'
    ),
    (
        'BRAPI_IRSEA_OCCURRENCE_SETTINGS',
        'BrAPI IRSEA Occurrence-level Settings',
        '{
            "variables": [
                "OCCURRENCE_NAME",
                "ENTNO",
                "PLOT_CODE",
                "PLOTNO",
                "REP",
                "DESIGNATION",
                "FIELD_X",
                "FIELD_Y"
            ],
            "traits": [
                "MC_CONT",
                "AYLD_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS"
                ]
            }
        }',
        'data_collection'
    ),
    (
        'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS',
        'BrAPI Collaborator Occurrence-level Settings',
        '{
            "variables": [
                "OCCURRENCE_NAME",
                "ENTNO",
                "PLOT_CODE",
                "PLOTNO",
                "REP",
                "FIELD_X",
                "FIELD_Y"
            ],
            "traits": [
                "HT1_CONT",
                "HT2_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS",
                    "WS"
                ]
            }
        }',
        'data_collection'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';
