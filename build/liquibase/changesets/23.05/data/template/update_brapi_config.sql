--liquibase formatted sql

--changeset postgres:update_brapi_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5464 DC-DB: Remove variable configuration in BrAPI global, program-specific and collaborator role settings



UPDATE platform.config
SET
    config_value = $$
        {
            "traits": [
                "MC_CONT",
                "AYLD_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS",
                    "WS"
                ]
            }
        }
    $$
WHERE
    abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';

UPDATE platform.config
SET
    config_value = $$
        {
            "traits": [
                "MC_CONT",
                "AYLD_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS"
                ]
            }
        }
    $$
WHERE
    abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';

UPDATE platform.config
SET
    config_value = $$
        {
            "traits": [
                "HT1_CONT",
                "HT2_CONT"
            ],
            "metadata": {
                "EXPERIMENT_YEAR": [
                    "2023"
                ],
                "SEASON": [
                    "DS",
                    "WS"
                ]
            }
        }
    $$
WHERE
    abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "variables": [
--rollback                 "OCCURRENCE_NAME",
--rollback                 "ENTNO",
--rollback                 "PLOT_CODE",
--rollback                 "PLOTNO",
--rollback                 "REP",
--rollback                 "DESIGNATION",
--rollback                 "FIELD_X",
--rollback                 "FIELD_Y"
--rollback             ],
--rollback             "traits": [
--rollback                 "MC_CONT",
--rollback                 "AYLD_CONT"
--rollback             ],
--rollback             "metadata": {
--rollback                 "EXPERIMENT_YEAR": [
--rollback                     "2023"
--rollback                 ],
--rollback                 "SEASON": [
--rollback                     "DS",
--rollback                     "WS"
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "variables": [
--rollback                 "OCCURRENCE_NAME",
--rollback                 "ENTNO",
--rollback                 "PLOT_CODE",
--rollback                 "PLOTNO",
--rollback                 "REP",
--rollback                 "DESIGNATION",
--rollback                 "FIELD_X",
--rollback                 "FIELD_Y"
--rollback             ],
--rollback             "traits": [
--rollback                 "MC_CONT",
--rollback                 "AYLD_CONT"
--rollback             ],
--rollback             "metadata": {
--rollback                 "EXPERIMENT_YEAR": [
--rollback                     "2023"
--rollback                 ],
--rollback                 "SEASON": [
--rollback                     "DS"
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "variables": [
--rollback                 "OCCURRENCE_NAME",
--rollback                 "ENTNO",
--rollback                 "PLOT_CODE",
--rollback                 "PLOTNO",
--rollback                 "REP",
--rollback                 "FIELD_X",
--rollback                 "FIELD_Y"
--rollback             ],
--rollback             "traits": [
--rollback                 "HT1_CONT",
--rollback                 "HT2_CONT"
--rollback             ],
--rollback             "metadata": {
--rollback                 "EXPERIMENT_YEAR": [
--rollback                     "2023"
--rollback                 ],
--rollback                 "SEASON": [
--rollback                     "DS",
--rollback                     "WS"
--rollback                 ]
--rollback             }
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';
