--liquibase formatted sql

--changeset postgres:add_gm_merge_germplasm_template_column_crop_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5518 GM: Implement Download merge germplasm transaction template



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_MERGE_GERMPLASM_CONFIG_RICE_DEFAULT',
        'Germplasm Manager Merge Germplasm variables for RICE',
        $$			
        {
            "values":[
                {
                    "name":"Germplasm Code Keep",
                    "label":"Germplasm Code of retained germplasm",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Germplasm Code Merge",
                    "label":"Germplasm Code of germplasm to be merged",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_MERGE",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Keep",
                    "label":"Designation of retained germplasm",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Merge",
                    "label":"Designation of germplasm to be merged",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_MERGE",
                    "entity":"germplasm"
                }
            ]
        }
        $$,
        1,
        'germplasm_manager',
        1,
        'CORB-5518 - k.delarosa 2023-05-19'
    )
;

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_MERGE_GERMPLASM_CONFIG_WHEAT_DEFAULT',
        'Germplasm Manager Merge Germplasm variables for WHEAT',
        $$			
        {
            "values":[
                {
                    "name":"Germplasm Code Keep",
                    "label":"Germplasm Code of retained germplasm",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Germplasm Code Merge",
                    "label":"Germplasm Code of germplasm to be merged",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_MERGE",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Keep",
                    "label":"Designation of retained germplasm",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Merge",
                    "label":"Designation of germplasm to be merged",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_MERGE",
                    "entity":"germplasm"
                }
            ]
        }
        $$,
        1,
        'germplasm_manager',
        1,
        'CORB-5518 - k.delarosa 2023-05-19'
    )
;

INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_MERGE_GERMPLASM_CONFIG_MAIZE_DEFAULT',
        'Germplasm Manager Merge Germplasm variables for MAIZE',
        $$			
        {
            "values":[
                {
                    "name":"Germplasm Code Keep",
                    "label":"Germplasm Code of retained germplasm",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Germplasm Code Merge",
                    "label":"Germplasm Code of germplasm to be merged",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_MERGE",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Keep",
                    "label":"Designation of retained germplasm",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Merge",
                    "label":"Designation of germplasm to be merged",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_MERGE",
                    "entity":"germplasm"
                }
            ]
        }
        $$,
        1,
        'germplasm_manager',
        1,
        'CORB-5518 - k.delarosa 2023-05-19'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_MERGE_GERMPLASM_CONFIG_RICE_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'GM_MERGE_GERMPLASM_CONFIG_WHEAT_DEFAULT';
--rollback DELETE FROM platform.config WHERE abbrev = 'GM_MERGE_GERMPLASM_CONFIG_MAIZE_DEFAULT';