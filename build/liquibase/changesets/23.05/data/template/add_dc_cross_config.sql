--liquibase formatted sql

--changeset postgres:add_dc_cross_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5497 DC-DB: Add the cross list export/view column configuration settings for KSU Field book files



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_CROSS_SETTINGS',
        'DC Global Cross List Settings',
        '{
            "Export": [
                {
                    "header": "CROSS_ID",
                    "attribute": "crossDbId",
                    "abbrev": "CROSS_ID",
                    "visible": false,
                    "identifier": true
                },
                {
                    "header": "Cross name",
                    "attribute": "crossName",
                    "abbrev": "CROSS_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Parent",
                    "attribute": "crossFemaleParent",
                    "abbrev": "DESIGNATION",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Parentage",
                    "attribute": "femaleParentage",
                    "abbrev": "PARENTAGE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Parent",
                    "attribute": "crossMaleParent",
                    "abbrev": "DESIGNATION",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Parentage",
                    "attribute": "maleParentage",
                    "abbrev": "PARENTAGE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Method",
                    "attribute": "crossMethod",
                    "abbrev": "CROSS_METHOD",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Entry Number",
                    "attribute": "femaleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Entry Number",
                    "attribute": "maleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence Code",
                    "attribute": "femaleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Occurrence Code",
                    "attribute": "maleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                }
            ]
        }',
        'data_collection'
    ),
    (
        'DC_IRSEA_CROSS_SETTINGS',
        'DC IRSEA Program Cross List Settings',
        '{
            "Export": [
                {
                    "header": "CROSS_ID",
                    "attribute": "crossDbId",
                    "abbrev": "CROSS_ID",
                    "visible": false,
                    "identifier": true
                },
                {
                    "header": "Cross name",
                    "attribute": "crossName",
                    "abbrev": "CROSS_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Parent",
                    "attribute": "crossFemaleParent",
                    "abbrev": "DESIGNATION",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Parentage",
                    "attribute": "femaleParentage",
                    "abbrev": "PARENTAGE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Parent",
                    "attribute": "crossMaleParent",
                    "abbrev": "DESIGNATION",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Parentage",
                    "attribute": "maleParentage",
                    "abbrev": "PARENTAGE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Method",
                    "attribute": "crossMethod",
                    "abbrev": "CROSS_METHOD",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Entry Number",
                    "attribute": "femaleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Entry Number",
                    "attribute": "maleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence Code",
                    "attribute": "femaleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Occurrence Code",
                    "attribute": "maleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Cross Remarks",
                    "attribute": "crossRemarks",
                    "abbrev": "REMARKS",
                    "visible": true,
                    "identifier": false
                }
            ]
        }',
        'data_collection'
    ),
    (
        'DC_COLLABORATOR_CROSS_SETTINGS',
        'DC Collaborator Role Cross List Settings',
        '{
            "Export": [
                {
                    "header": "CROSS_ID",
                    "attribute": "crossDbId",
                    "abbrev": "CROSS_ID",
                    "visible": false,
                    "identifier": true
                },
                {
                    "header": "Method",
                    "attribute": "crossMethod",
                    "abbrev": "CROSS_METHOD",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Entry Number",
                    "attribute": "femaleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Entry Number",
                    "attribute": "maleParentEntryNumber",
                    "abbrev": "ENTRY_NUMBER",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence",
                    "attribute": "femaleOccurrenceName",
                    "abbrev": "OCCURRENCE_NAME",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Female Occurrence Code",
                    "attribute": "femaleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Male Occurrence Code",
                    "attribute": "maleOccurrenceCode",
                    "abbrev": "OCCURRENCE_CODE",
                    "visible": true,
                    "identifier": false
                },
                {
                    "header": "Cross Remarks",
                    "attribute": "crossRemarks",
                    "abbrev": "REMARKS",
                    "visible": true,
                    "identifier": false
                }
            ]
        }',
        'data_collection'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_CROSS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_IRSEA_CROSS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_COLLABORATOR_CROSS_SETTINGS';
