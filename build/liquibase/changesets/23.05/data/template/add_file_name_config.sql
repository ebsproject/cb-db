--liquibase formatted sql

--changeset postgres:add_file_name_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5534 Add config DOWNLOAD_FILE_NAME in platform.config

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage)
	VALUES (
		'DOWNLOAD_FILE_NAME',
		'Attibute to be used in downloaded file names', 
		'
		{
			"DEFAULT": {
				"suffix": "File",
				"attribute": "occurrenceName"
			},
			"FIELD_LAYOUT": {
				"suffix": "Field_Layout",
				"attribute": "locationName"
			},
			"DESIGN_LAYOUT": {
				"suffix": "Design_Layout",
				"attribute": "occurrenceName"
			},
			"MAPPING_FILES": {
				"suffix": "Mapping_Files",
				"attribute": "occurrenceName"
			},
			"PLOT_LIST_LOCATION": {
				"suffix": "Plot_List",
				"attribute": "locationName"
			},
			"PLOT_LIST_OCCURRENCE": {
				"suffix": "Plot_List",
				"attribute": "occurrenceName"
			},
			"EASY_HARVEST_LOCATION": {
				"suffix": "Easy_Harvest",
				"attribute": "locationName"
			},
			"ENTRY_LIST_OCCURRENCE": {
				"suffix": "Entry_List",
				"attribute": "occurrenceName"
			},
			"KSU_INVENTORY_LOCATION": {
				"suffix": "KSU_Inventory",
				"attribute": "locationName"
			},
			"EASY_HARVEST_OCCURRENCE": {
				"suffix": "Easy_Harvest",
				"attribute": "occurrenceName"
			},
			"KSU_INVENTORY_OCCURRENCE": {
				"suffix": "KSU_Inventory",
				"attribute": "occurrenceName"
			},
			"DATA_COLLECTION_SUMMARIES": {
				"suffix": "Data_Collection_Summaries",
				"attribute": "occurrenceName"
			},
			"MOISTURE_METER_1_LOCATION": {
				"suffix": "Moisuture_Meter_1",
				"attribute": "locationName"
			},
			"MOISTURE_METER_1_OCCURRENCE": {
				"suffix": "Moisuture_Meter_1",
				"attribute": "occurrenceName"
			},
			"PLANTING_INSTRUCTIONS_LOCATION": {
				"suffix": "Planting_Instructions",
				"attribute": "locationName"
			},
			"TRAIT_DATA_COLLECTION_LOCATION": {
				"suffix": "Trait_Data_Collection",
				"attribute": "locationName"
			},
			"PLANTING_INSTRUCTIONS_OCCURRENCE": {
				"suffix": "Planting_Instructions",
				"attribute": "occurrenceName"
			},
			"TRAIT_DATA_COLLECTION_OCCURRENCE": {
				"suffix": "Trait_Data_Collection",
				"attribute": "occurrenceName"
			}
		}
		'::json, 
		1, 
		'download_file_name'
	);

	
--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev = 'DOWNLOAD_FILE_NAME'
--rollback ;