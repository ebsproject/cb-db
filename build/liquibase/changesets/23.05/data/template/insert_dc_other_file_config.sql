--liquibase formatted sql

--changeset postgres:insert_dc_other_file_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3497 DC-DB: Enable other file configurations for the data collection



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_OTHER_FILE_SETTINGS',
        'DC Global Other File Settings',
        '{
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "GID",
                        "attribute": "germplasmCode",
                        "abbrev": "GERMPLASM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                     {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                      {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                     {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            }
        }',
        'data_collection'
    ),
    (
        'DC_IRSEA_OTHER_FILE_SETTINGS',
        'DC IRSEA Program Other File Settings',
        '{
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "GID",
                        "attribute": "germplasmCode",
                        "abbrev": "GERMPLASM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "DESIGNATION",
                        "attribute": "entryName",
                        "abbrev": "DESIGNATION",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            }
        }',
        'data_collection'
    ),
    (
        'DC_COLLABORATOR_OTHER_FILE_SETTINGS',
        'DC Collaborator Role Other File Settings',
        '{
            "Easy Harvest": {
                "Export": [
                    {
                        "header": "PROGRAM",
                        "attribute": "programCode",
                        "abbrev": "PROGRAM_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AREA",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "RESEARCHER",
                        "attribute": "contactPerson",
                        "abbrev": "RESEARCHER",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "STUDY",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "HARVYR",
                        "attribute": "experimentYear",
                        "abbrev": "EXPERIMENT_YEAR",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "SEASON",
                        "attribute": "experimentSeason",
                        "abbrev": "SEASON",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "DEEP/ROW",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "WIDE/COLUMN",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "ENTNO",
                        "attribute": "entryNumber",
                        "abbrev": "ENTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOT_KEY",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "visible": true,
                        "isObservation": false
                    }
                ],
                "Upload": [
                    {
                        "header": "Field",
                        "attribute": "field",
                        "abbrev": "FIELD",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Trial",
                        "attribute": "occurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plots deep(Y)",
                        "attribute": "paY",
                        "abbrev": "PA_Y",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Plot wide(X)",
                        "attribute": "paX",
                        "abbrev": "PA_X",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Weight",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    },
                    {
                        "header": "Density",
                        "attribute": "DENSITY_CONT",
                        "abbrev": "DENSITY_CONT",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "User",
                        "attribute": "USER",
                        "abbrev": "USER",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Date",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Time",
                        "attribute": "collectionTimestamp",
                        "abbrev": "COLLECTION_TIMESTAMP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "SequenceNo",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "Qrcode",
                        "attribute": "plotDbId",
                        "abbrev": "PLOT_ID",
                        "identifier": true,
                        "isObservation": false
                    }
                ]
            },
            "Moisture Meter 1": {
                "Export": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "plotcode",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "plotno",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "replication",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "moisture",
                        "attribute": "MC_CONT",
                        "abbrev": "MC_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            },
            "KSU Inventory": {
                "Export": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "visible": true,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "visible": true,
                        "isObservation": true
                    }
                ],
                "Upload": [
                    {
                        "header": "PLOT_CODE",
                        "attribute": "plotCode",
                        "abbrev": "PLOT_CODE",
                        "identifier": true,
                        "isObservation": false
                    },
                    {
                        "header": "PLOTNO",
                        "attribute": "plotNumber",
                        "abbrev": "PLOTNO",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "REP",
                        "attribute": "rep",
                        "abbrev": "REP",
                        "identifier": false,
                        "isObservation": false
                    },
                    {
                        "header": "AYLD_CONT",
                        "attribute": "AYLD_CONT",
                        "abbrev": "AYLD_CONT",
                        "identifier": false,
                        "isObservation": true
                    }
                ]
            }
        }',
        'data_collection'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_OTHER_FILE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_IRSEA_OTHER_FILE_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_COLLABORATOR_OTHER_FILE_SETTINGS';
