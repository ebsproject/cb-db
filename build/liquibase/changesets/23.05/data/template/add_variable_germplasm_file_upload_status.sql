--liquibase formatted sql

--changeset postgres:add_variable_germplasm_file_upload_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2377 Add variable GERMPLASM_FILE_UPLOAD_STATUS



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, display_name, creator_id) 
    VALUES 
        ('GERMPLASM_FILE_UPLOAD_STATUS', 'Germplasm File Upload Status', 'Germplasm File Upload Status', 'character varying', false, 'system', NULL, 'application', NULL, 'active', 'Germplasm File Upload Status', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'Germplasm File Upload Status', 'Germplasm File Upload Status') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('GERMPLASM_FILE_UPLOAD_STATUS', 'Germplasm File Upload Status')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('GERMPLASM_FILE_UPLOAD_STATUS', 'Germplasm File Upload Status', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- scale_value (uncomment if variable has scale values; else, delete this code)
    INSERT INTO
        master.scale_value (
            scale_id, order_number, value, description, abbrev, scale_value_status
        )
    SELECT
        var_scale_id AS scale_id,
        ROW_NUMBER() OVER (PARTITION BY t.variable) AS order_number,
        t.value,
        t.description,
        t.variable || '_' || t.abbrev AS abbrev,
        t.scale_value_status
    FROM (
            VALUES
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'in queue', 'In Queue', 'IN_QUEUE', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'created', 'In Queue', 'CREATED', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'validation in progress', 'Validation in Progress', 'VALIDATION_IN_PROGRESS', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'validation error', 'Validation Error', 'VALIDATION_ERROR', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'validated', 'Validated', 'VALIDATED', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'creation in progress', 'Creation in Progress', 'CREATION_IN_PROGRESS', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'creation failed', 'Creation Failed', 'CREATION_FAILED', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'completed', 'Completed', 'COMPLETED', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'update in progress', 'Update in Progress', 'UPDATE_IN_PROGRESS', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'update failed', 'Update Failed', 'UPDATE FAILED', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'merge ready', 'Merge Ready', 'MERGE_READY', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'merging in progress', 'Merging in Progress', 'MERGING_IN_PROGRESS', 'show'),
            ('GERMPLASM_FILE_UPLOAD_STATUS', 'merge failed', 'Merge Failed', 'MERGE_FAILED', 'show')
            -- TODO: Add more scale values here if needed
        ) AS t (
            variable, value, description, abbrev, scale_value_status
        )
    ;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
--rollback ;