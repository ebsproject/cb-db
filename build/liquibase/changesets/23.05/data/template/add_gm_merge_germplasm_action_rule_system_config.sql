--liquibase formatted sql

--changeset postgres:add_gm_merge_germplasm_action_rule_system_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5569 GM: Create system default germplasm merge action rules configuration



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_MERGE_GERMPLASM_ACTION_RULE_SYSTEM_DEFAULT',
        'Germplasm Manager Merge Germplasm action rules',
        $${
            "values":[{
                "CONFLICT_MARKERS":{
                    "MATCH_VALUE":{
                        "DESIGNATION":{
                            "schema": "germplasm",
                            "table": "germplasm",
                            "column": "designation",
                            "target":{
                                "column":"name_value",
                                "table":"germplasm_name",
                                "process":"filter",
                                "filter":[
                                    {
                                        "column":"germplasm_name_status",
                                        "value":"standard"
                                    }
                                ]
                            },
                            "condition":[]
                        },
                        "OTHER_NAMES":{
                            "schema": "germplasm",
                            "table": "germplasm",
                            "column": "other_names",
                            "target": {
                                "column":"name_value",
                                "table":"germplasm_name",
                                "process":"aggregate",
                                "filter":[]
                            },
                            "condition": []
                        }
                    },
                    "SINGLE_RECORD":{
                        "GERMPLASM_NAME":{
                            "schema": "germplasm",
                            "table": "germplasm_name",
                            "column": "id",
                            "target":{
                                "column":"id",
                                "table":"germplasm_name",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"germplasmNameStatus",
                                    "value":"standard"
                                },
                                {
                                    "column":"germplasmNameType",
                                    "value":"pedigree, selection_history"
                                }
                            ]
                        },
                        "GRAIN_COLOR":{
                            "schema": "germplasm",
                            "table": "germplasm_attribute",
                            "column": "id",
                            "target": {
                                "column":"id",
                                "table":"germplasm_attribute",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"variableAbbrev",
                                    "value":"GRAIN_COLOR"
                                }
                            ]
                        },
                        "GROWTH_HABIT":{
                            "schema": "germplasm",
                            "table": "germplasm_attribute",
                            "column": "id",
                            "target": {
                                "column":"id",
                                "table":"germplasm_attribute",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"variableAbbrev",
                                    "value":"GROWTH_HABIT",
                                    "data_value":"attributeDataValue"
                                }
                            ]
                        },
                        "CROSS_NUMBER":{
                            "schema": "germplasm",
                            "table": "germplasm_attribute",
                            "column": "id",
                            "target": {
                                "column":"id",
                                "table":"germplasm_attribute",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"variableAbbrev",
                                    "value":"CROSS_NUMBER",
                                    "data_value":"attributeDataValue"
                                }
                            ]
                        },
                        "HETEROTIC_GROUP":{
                            "schema": "germplasm",
                            "table": "germplasm_attribute",
                            "column": "id",
                            "target": {
                                "column":"id",
                                "table":"germplasm_attribute",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"variableAbbrev",
                                    "value":"HETEROTIC_GROUP",
                                    "data_value":"attributeDataValue"
                                }
                            ]
                        },
                        "DH_FIRST_INCREASE_COMPLETED":{
                            "schema": "germplasm",
                            "table": "germplasm_attribute",
                            "column": "id",
                            "target": {
                                "column":"id",
                                "table":"germplasm_attribute",
                                "process":"",
                                "filter":[]
                            },
                            "condition": [
                                {
                                    "column":"variableAbbrev",
                                    "value":"DH_FIRST_INCREASE_COMPLETED",
                                    "data_value":"attributeDataValue"
                                }
                            ]
                        }
                    }
                }
            }]
            }$$,
        1,
        'germplasm_manager',
        1,
        'CORB-5569 - k.delarosa 2023-05-12'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_MERGE_GERMPLASM_ACTION_RULE_SYSTEM_DEFAULT';