--liquibase formatted sql

--changeset postgres:insert_printouts_max_occurrences_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5533 EM DB: Add config for maximum number of occurrences to be selected for printouts


INSERT INTO
    platform.config 
    (
        abbrev, 
        name, 
        config_value, 
        usage
    )
VALUES 
    (
        'PRINTOUTS_MAX_NUMBER_OF_OCCURRENCES',
        'Maximum number of occurrence to be processed in the Printouts functionality',
        '100',
        'application'
    );


--rollback DELETE FROM platform.config WHERE abbrev = 'PRINTOUTS_MAX_NUMBER_OF_OCCURRENCES';
