--liquibase formatted sql

--changeset postgres:update_gm_merge_germplasm_template_column_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5518 GM: Implement Download merge germplasm transaction template



UPDATE platform.config
SET
    config_value = $$
        {
            "values":[
                {
                    "name":"Germplasm Code Keep",
                    "label":"Germplasm Code of retained germplasm",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Germplasm Code Merge",
                    "label":"Germplasm Code of germplasm to be merged",
                    "abbrev":"GERMPLASM_CODE",
                    "required":"true",
                    "column_header":"GERMPLASM_CODE_MERGE",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Keep",
                    "label":"Designation of retained germplasm",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_KEEP",
                    "entity":"germplasm"
                },
                {
                    "name":"Designation Merge",
                    "label":"Designation of germplasm to be merged",
                    "abbrev":"DESIGNATION",
                    "required":"false",
                    "column_header":"DESIGATION_MERGE",
                    "entity":"germplasm"
                }
            ]
        }
        $$
WHERE
    abbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback             "values":[
--rollback                                {
--rollback                                    "name":"Germplasm Code Keep",
--rollback                                    "label":"Germplasm Code of retained germplasm",
--rollback                                    "abbrev":"GERMPLASM_CODE_KEEP",
--rollback                                    "required":"true",
--rollback                                    "variable_abbrev":"GERMPLASM_CODE"
--rollback                                },
--rollback                                {
--rollback                                    "name":"Germplasm Code Merge",
--rollback                                    "label":"Germplasm Code of germplasm to be merged",
--rollback                                    "abbrev":"GERMPLASM_CODE_MERGE",
--rollback                                    "required":"true",
--rollback                                    "variable_abbrev":"GERMPLASM_CODE"
--rollback                                },
--rollback                                {
--rollback                                    "name":"Designation Keep",
--rollback                                    "label":"Designation of retained germplasm",
--rollback                                    "abbrev":"DESIGNATION_KEEP",
--rollback                                    "required":"false",
--rollback                                    "variable_abbrev":"DESIGATION"
--rollback                                },
--rollback                                {
--rollback                                    "name":"Designation Merge",
--rollback                                    "label":"Designation of germplasm to be merged",
--rollback                                    "abbrev":"DESIGNATION_MERGE",
--rollback                                    "required":"false",
--rollback                                    "variable_abbrev":"DESIGATION"
--rollback                                }
--rollback             ]
--rollback         }
--rollback         $$
--rollback WHERE
--rollback     abbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';