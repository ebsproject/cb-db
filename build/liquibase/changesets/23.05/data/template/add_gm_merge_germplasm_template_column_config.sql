--liquibase formatted sql

--changeset postgres:add_gm_merge_germplasm_template_column_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5502 GM: Create system default germplasm merge column header configuration



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT',
        'Germplasm Manager Merge Germplasm variables',
        $$			
        {
            "values":[
                {
                    "name":"Germplasm Code Keep",
                    "label":"Germplasm Code of retained germplasm",
                    "abbrev":"GERMPLASM_CODE_KEEP",
                    "required":"true",
                    "variable_abbrev":"GERMPLASM_CODE"
                },
                {
                    "name":"Germplasm Code Merge",
                    "label":"Germplasm Code of germplasm to be merged",
                    "abbrev":"GERMPLASM_CODE_MERGE",
                    "required":"true",
                    "variable_abbrev":"GERMPLASM_CODE"
                },
                {
                    "name":"Designation Keep",
                    "label":"Designation of retained germplasm",
                    "abbrev":"DESIGNATION_KEEP",
                    "required":"false",
                    "variable_abbrev":"DESIGATION"
                },
                {
                    "name":"Designation Merge",
                    "label":"Designation of germplasm to be merged",
                    "abbrev":"DESIGNATION_MERGE",
                    "required":"false",
                    "variable_abbrev":"DESIGATION"
                }
            ]
        }
        $$,
        1,
        'germplasm_manager',
        1,
        'CORB-5502 - k.delarosa 2023-05-5'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev = 'GM_MERGE_GERMPLASM_CONFIG_SYSTEM_DEFAULT';