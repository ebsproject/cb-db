--liquibase formatted sql

--changeset postgres:set_order_number_scale_id_to_null context:template splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2466 Set order_number scale_id to null



UPDATE master.variable SET scale_id = NULL WHERE abbrev='ORDER_NUMBER';



-- revert changes
--rollback SELECT NULL;