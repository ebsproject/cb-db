--liquibase formatted sql

--changeset postgres:update_gm_create_germplasm_relation_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5458 CORB-5547 GM-DB: Update germplasm relation config



-- update config
UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
                "name": "Parent Germplasm Code",
                "type": "column",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "required",
                "abbrev": "PARENT_GERMPLASM_CODE",
                "entity": "germplasm_relation",
                "required": "true",
                "api_field": "parentGermplasmDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "germplasmCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "germplasmDbId",
                "search_endpoint": "germplasm-search",
                "additional_filters": {}
            },
            {
                "name": "Parent Designation",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm_relation"
                    ]
                },
                "usage": "optional",
                "abbrev": "PARENT_DESIGNATION",
                "entity": "germplasm_relation",
                "required": "false",
                "api_field": "",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "nameValue",
                "skip_creation": "true",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "germplasmDbId",
                "search_endpoint": "germplasm-names-search",
                "additional_filters": {}
            },
            {
                "name": "Child Germplasm Code",
                "type": "column",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "required",
                "abbrev": "CHILD_GERMPLASM_CODE",
                "entity": "germplasm_relation",
                "required": "true",
                "api_field": "childGermplasmDbId",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "germplasmCode",
                "skip_creation": "false",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "germplasmDbId",
                "search_endpoint": "germplasm-search",
                "additional_filters": {}
            },
            {
                "name": "Child Designation",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm_relation"
                    ]
                },
                "usage": "optional",
                "abbrev": "CHILD_DESIGNATION",
                "entity": "germplasm_relation",
                "required": "false",
                "api_field": "",
                "data_type": "string",
                "http_method": "POST",
                "value_filter": "nameValue",
                "skip_creation": "true",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "germplasmDbId",
                "search_endpoint": "germplasm-names-search",
                "additional_filters": {}
            },
            {
                "name": "Order Number",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "germplasm_relation"
                    ]
                },
                "usage": "required",
                "abbrev": "ORDER_NUMBER",
                "entity": "germplasm_relation",
                "required": "true",
                "api_field": "orderNumber",
                "data_type": "integer",
                "http_method": "",
                "value_filter": "",
                "skip_creation": "false",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            }
        ]
    }
    $$
WHERE
    abbrev = 'GM_CREATE_GERMPLASM_RELATION_SYSTEM_DEFAULT';



--revert changes
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback     {
--rollback         "values": [
--rollback             {
--rollback                 "name": "Parent Germplasm Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "false",
--rollback                     "entities": []
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "PARENT_GERMPLASM_CODE",
--rollback                 "entity": "germplasm_relation",
--rollback                 "required": "true",
--rollback                 "api_field": "germplasmDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "germplasmCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "germplasmDbId",
--rollback                 "search_endpoint": "germplasm-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Parent Designation",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "germplasm_relation"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "PARENT_DESIGNATION",
--rollback                 "entity": "germplasm_relation",
--rollback                 "required": "false",
--rollback                 "api_field": "",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "nameValue",
--rollback                 "skip_creation": "true",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "germplasmDbId",
--rollback                 "search_endpoint": "germplasm-names-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Child Germplasm Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "false",
--rollback                     "entities": []
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "CHILD_GERMPLASM_CODE",
--rollback                 "entity": "germplasm_relation",
--rollback                 "required": "true",
--rollback                 "api_field": "germplasmDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "germplasmCode",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "germplasmDbId",
--rollback                 "search_endpoint": "germplasm-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Child Designation",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "germplasm_relation"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "CHILD_DESIGNATION",
--rollback                 "entity": "germplasm_relation",
--rollback                 "required": "false",
--rollback                 "api_field": "",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "value_filter": "nameValue",
--rollback                 "skip_creation": "true",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "germplasmDbId",
--rollback                 "search_endpoint": "germplasm-names-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Order Number",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "germplasm_relation"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "ORDER_NUMBER",
--rollback                 "entity": "germplasm_relation",
--rollback                 "required": "true",
--rollback                 "api_field": "packageLabel",
--rollback                 "data_type": "integer",
--rollback                 "http_method": "",
--rollback                 "value_filter": "",
--rollback                 "skip_creation": "false",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             }
--rollback         ]
--rollback     }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'GM_CREATE_GERMPLASM_RELATION_SYSTEM_DEFAULT';