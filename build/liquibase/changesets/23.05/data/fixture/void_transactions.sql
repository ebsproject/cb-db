--liquibase formatted sql

--changeset postgres:void_transactions context:fixture labels:stress,large splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2420 Void transactions



UPDATE data_terminal."transaction" t SET is_void=true WHERE id > 1000002;
UPDATE data_terminal."transaction_dataset" t SET is_void=true WHERE transaction_id > 1000002;



--rollback UPDATE data_terminal."transaction" t SET is_void=false WHERE id > 1000002;
--rollback UPDATE data_terminal."transaction_dataset" t SET is_void=false WHERE transaction_id > 1000002;
