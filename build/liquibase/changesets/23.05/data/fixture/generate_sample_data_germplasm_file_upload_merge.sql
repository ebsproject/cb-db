--liquibase formatted sql

--changeset postgres:generate_sample_data_germplasm_file_upload_merge context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CORB-5605 GM: Create sample merge germplasm file upload testing data



INSERT INTO germplasm.file_upload
    (program_id, file_name, file_data, file_status, germplasm_count, creator_id, is_void, file_upload_origin, file_upload_action,notes)
SELECT 
	pr.id program_id,
	file_name, 
	file_data::json, 
	file_status, 
	germplasm_count, 
	p.id creator_id, 
	t.is_void, 
	file_upload_origin, 
	file_upload_action,
	t.notes
FROM
	(
		VALUES		
		(
        	'Test Merge Germplasm Transaction #011', 
        	'[{"keep": {"germplasmCode": "WGE000016057998","designation": "CMSS23Y00008S"},"merge": {"germplasmCode": "WGE000016057997","designation": "CMSS23Y00007S"}}]',
        	'created', 
        	1, 
        	false, 
        	'GERMPLASM_CATALOG', 'merge','CORB-5605 GM: Create sample merge germplasm file upload testing data'
        ),
	    (
	    	'Test Merge Germplasm Transaction #012', 
	    	'[{"keep": {"germplasmCode": "WGE000016057998","designation": "CMSS23Y00008S"},"merge": {"germplasmCode": "WGE000016057997","designation": "CMSS23Y00007S"}}]',
	    	'validation in progress', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 'merge','CORB-5605 GM: Create sample merge germplasm file upload testing data'),
	    (
	    	'Test Merge Germplasm Transaction #013', 
	    	'[{"keep": {"germplasmCode": "WGE000016057998","designation": "CMSS23Y00008S"},"merge": {"germplasmCode": "WGE000016057997","designation": "CMSS23Y00007S"}}]',
	    	'merge ready', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #014', 
	    	'[{"keep": {"germplasmCode": "WGE000016057998","designation": "CMSS23Y00008S"},"merge": {"germplasmCode": "WGE000016057997","designation": "CMSS23Y00007S"}}]',
	    	'merging in progress', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #015', 
	    	'[{"keep": {"germplasmCode": "WGE000016057998","designation": "CMSS23Y00008S"},"merge": {"germplasmCode": "WGE000016057997","designation": "CMSS23Y00007S"}}]',
	    	'completed', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #016', 
	    	'[{"keep": {"designation": "CMSS23Y00008S","germplasm_code": "WGE000016057998"},"merge": {"designation": "CMSS23Y00007S","germplasm_code": "WGE000016057997"}},{"keep": {"designation": "CMSS23Y00008S2","germplasm_code": "WGE0000160579982"},"merge": {"designation": "CMSS23Y00007S2","germplasm_code": "WGE0000160579972"}},{"keep": {"designation": "CMSS23Y00008S3","germplasm_code": "WGE0000160579983"},"merge": {"designation": "CMSS23Y00007S3","germplasm_code": "WGE0000160579973"}}]',
	    	'created', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #017', 
	    	'[{"keep": {"designation": "CMSS23Y00008S","germplasm_code": "WGE000016057998"},"merge": {"designation": "CMSS23Y00007S","germplasm_code": "WGE000016057997"}},{"keep": {"designation": "CMSS23Y00008S2","germplasm_code": "WGE0000160579982"},"merge": {"designation": "CMSS23Y00007S2","germplasm_code": "WGE0000160579972"}},{"keep": {"designation": "CMSS23Y00008S3","germplasm_code": "WGE0000160579983"},"merge": {"designation": "CMSS23Y00007S3","germplasm_code": "WGE0000160579973"}}]',
	    	'validation in progress', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #018', 
	    	'[{"keep": {"designation": "CMSS23Y00008S","germplasm_code": "WGE000016057998"},"merge": {"designation": "CMSS23Y00007S","germplasm_code": "WGE000016057997"}},{"keep": {"designation": "CMSS23Y00008S2","germplasm_code": "WGE0000160579982"},"merge": {"designation": "CMSS23Y00007S2","germplasm_code": "WGE0000160579972"}},{"keep": {"designation": "CMSS23Y00008S3","germplasm_code": "WGE0000160579983"},"merge": {"designation": "CMSS23Y00007S3","germplasm_code": "WGE0000160579973"}}]',
	    	'merge ready', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 'merge','CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
	    (
	    	'Test Merge Germplasm Transaction #019', 
	    	'[{"keep": {"designation": "CMSS23Y00008S","germplasm_code": "WGE000016057998"},"merge": {"designation": "CMSS23Y00007S","germplasm_code": "WGE000016057997"}},{"keep": {"designation": "CMSS23Y00008S2","germplasm_code": "WGE0000160579982"},"merge": {"designation": "CMSS23Y00007S2","germplasm_code": "WGE0000160579972"}},{"keep": {"designation": "CMSS23Y00008S3","germplasm_code": "WGE0000160579983"},"merge": {"designation": "CMSS23Y00007S3","germplasm_code": "WGE0000160579973"}}]',
	    	'merging in progress', 
	    	1, 
	    	false, 
	    	'GERMPLASM_CATALOG', 
	    	'merge',
	    	'CORB-5605 GM: Create sample merge germplasm file upload testing data'
	    ),
    	( 
    		'Test Merge Germplasm Transaction #020', 
    		'[{"keep": {"designation": "CMSS23Y00008S","germplasm_code": "WGE000016057998"},"merge": {"designation": "CMSS23Y00007S","germplasm_code": "WGE000016057997"}},{"keep": {"designation": "CMSS23Y00008S2","germplasm_code": "WGE0000160579982"},"merge": {"designation": "CMSS23Y00007S2","germplasm_code": "WGE0000160579972"}},{"keep": {"designation": "CMSS23Y00008S3","germplasm_code": "WGE0000160579983"},"merge": {"designation": "CMSS23Y00007S3","germplasm_code": "WGE0000160579973"}}]',
    		'completed', 
    		1, 
    		false, 
    		'GERMPLASM_CATALOG', 
    		'merge',
    		'CORB-5605 GM: Create sample merge germplasm file upload testing data'
    	)
	) AS t (file_name, file_data, file_status, germplasm_count, is_void, file_upload_origin, file_upload_action,notes)
JOIN
	tenant.PROGRAM pr
ON
	pr.program_code = 'IRSEA'
JOIN
	tenant.person p 
ON
	p.username = 'k.delarosa'
;



--rollback DELETE FROM
--rollback    germplasm.file_upload
--rollback WHERE
--rollback    notes = 'CORB-5605 GM: Create sample merge germplasm file upload testing data'
--rollback ;