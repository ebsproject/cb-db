--liquibase formatted sql

--changeset postgres:delete_postgres_person context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-935 Delete postgres person



-- delete postgres
DELETE FROM tenant.person WHERE username = 'postgres';



-- revert changes
--rollback INSERT INTO tenant.person
--rollback     (id, username, first_name, last_name, person_name, person_type, email, person_role_id, person_status, creator_id, is_void, is_active, person_document)
--rollback VALUES
--rollback     (0, 'postgres', 'Postgres', 'Postgres', 'Postgres, Postgres', 'admin', 'postgres', 1, 'active', 1, false, true, '''activ'':1C ''admin'':1C ''postgr'':1A,2A'::tsvector)
--rollback ;
