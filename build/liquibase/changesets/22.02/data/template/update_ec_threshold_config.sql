--liquibase formatted sql

--changeset postgres:update_ec_bg_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-925 Update EC background processing threshold config



UPDATE platform.config
    SET config_value=$${
        "createEntries": {
            "size": "500",
            "description": "Create entries of an Experiment"
        },
        "deleteEntries": {
            "size": "500",
            "description": "Delete entries of an Experiment"
        },
        "updateEntries": {
            "size": "500",
            "description": "Update entries of an Experiment"
        },
        "createOccurrences": {
            "size": "500",
            "description": "Create occurrence plot and/or planting instruction records of an Experiment"
        },
        "deleteOccurrences": {
            "size": "500",
            "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
        },
        "reorderAllEntries": {
            "size": "200",
            "description": "Reorder all entries of an Experiment"
        }
    }$$
WHERE abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';



-- revert changes
--rollback UPDATE platform.config
--rollback     SET config_value=$${
--rollback         "createEntries": {
--rollback             "size": "500",
--rollback             "description": "Create entries of an Experiment"
--rollback         },
--rollback         "deleteEntries": {
--rollback             "size": "500",
--rollback             "description": "Delete entries of an Experiment"
--rollback         },
--rollback         "updateEntries": {
--rollback             "size": "500",
--rollback             "description": "Update entries of an Experiment"
--rollback         },
--rollback         "createOccurrences": {
--rollback             "size": "500",
--rollback             "description": "Create occurrence plot and/or planting instruction records of an Experiment"
--rollback         },
--rollback         "deleteOccurrences": {
--rollback             "size": "500",
--rollback             "description": "Delete occurrence plot and/or planting instruction records of an Experiment"
--rollback         }
--rollback     }$$
--rollback WHERE abbrev = 'EXPERIMENT_CREATION_BG_PROCESSING_THRESHOLD';
