--liquibase formatted sql

--changeset postgres:remove_obsolete_hm_configs context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-848 Remove obsolete HM configs



DELETE FROM 
    platform.config
WHERE
    abbrev
IN
    (
        'HARVEST_MANAGER_FILTERS',
        'HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT',
        'HM_BROWSER_CONFIG_MAIZE',
        'HM_BROWSER_CONFIG_RICE',
        'HM_BROWSER_CONFIG_WHEAT',
        'HM_CROP_BULK_HARVEST_CODE',
        'HM_CROSSMETH_HARVMETH_COMPAT',
        'HM_GERMPLASM_NAME_SUFFIX',
        'HM_HARVEST_DATA_VARIABLE_ABBREVS',
        'HM_NUMVAR_COMPAT',
        'HM_PACKAGE_NAME_PATTERN_MAIZE_DEFAULT',
        'HM_PACKAGE_NAME_PATTERN_RICE_DEFAULT',
        'HM_PACKAGE_NAME_PATTERN_WHEAT_DEFAULT',
        'HM_SEED_NAME_PATTERN_MAIZE_DEFAULT',
        'HM_SEED_NAME_PATTERN_WHEAT_DEFAULT'
    )
;



--rollback SELECT NULL;