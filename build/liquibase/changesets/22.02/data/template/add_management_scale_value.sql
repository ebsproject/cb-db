--liquibase formatted sql

--changeset postgres:add_management_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-936 Add management scale value



-- add scale values to LIST_SUB_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('LIST_SUB_TYPE_MANAGEMENT', 'management', 7, 'Management', 'Management', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id
    )
WHERE
    var.abbrev = 'LIST_SUB_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('LIST_SUB_TYPE_MANAGEMENT')
--rollback ;
