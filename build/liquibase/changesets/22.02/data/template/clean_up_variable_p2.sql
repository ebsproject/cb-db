--liquibase formatted sql

--changeset postgres:clean_up_variable_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up variable p2



DO $$
DECLARE
    var_id int;
    frm_id int;
    record_count int;
    deleted_var_id int;
    deleted_frm_id int;
BEGIN

    -- select ids of variables
    FOR var_id IN (
        SELECT 
            id
        FROM 
            master.variable v 
        WHERE
            abbrev 
        IN 
            (
                'ABBREVIATED_CULTIVAR_NAME',
                'ACCESSION_ID_IN_OTHER_GENEBANK',
                'ACCESS_TOKEN',
                'ACCNO_CONT',
                'ADV_PROGENIES_SRC_STUDY_ID',
                'ALTERNATIVE_ABBREVIATION',
                'ALTERNATIVE_CROSS_NAME',
                'ALTERNATIVE_CULTIVAR_NAME',
                'ALTERNATIVE_DERIVATIVE_NAME',
                'ARCHIVER_ID',
                'ARCHIVE_TIMESTAMP',
                'ARGUMENT',
                'AVERAGE',
                'BASE_TYPE',
                'BLK',
                'BREEDING_LINE_NAME',
                'CACHE',
                'CIAT_GERMPLASM_BANK_ACCESSION_NUMBER',
                'CLASS_VARIABLE_ID',
                'COLLATION',
                'COLLECTORS_NUMBER',
                'COLUMN_TABLE',
                'COMMAND',
                'COMMITTED_BY',
                'COMMITTED_RECORDS_COUNT',
                'CONCURRENT',
                'CONDITION',
                'CROSS_NAME',
                'CULTIVAR_NAME',
                'DEPRECATED_NAME',
                'DERIVATIVE_NAME',
                'DONORS_ACCESSION_ID',
                'ELITE_LINES',
                'FIXED_LINE_NAME',
                'GQNPC_UNIQUE_ID',
                'INTERNAL_ID_CODE_FOR_CLOTHO',
                'IRIS_LOCATION_ID',
                'IRIS_USER_ID',
                'IRRI-DEVGEN_SKEP_PROJECT',
                'IRRI-HRDC_CODE',
                'IRRI_MICRONUTRIENT/SALINITY_LINE_CODE',
                'LINE_NAME',
                'LOCAL_COMMON_NAME',
                'MANAGEMENT_NAME',
                'MIDDLE_NAME',
                'NATIONAL_RICE_COOPERATIVE_TESTING_PROJECT_ID',
                'OLD_MUTANT_NAME_1',
                'OLD_MUTANT_NAME_2',
                'QR_CODE',
                'RELEASE_NAME',
                'SAMPLE_ID',
                'SEED_HOLDER',
                'SEED_STOCK_ID_FOR_BIMS_-_TEMPORARY_ALTENATE_NAME',
                'SPECIES_NAME',
                'TEMPORARY_/_QUARANTINE_ID',
                'UNNAMED_CROSS',
                'UNRESOLVED_NAME'
            )
            ORDER BY abbrev 
        )
    LOOP
        -- count experiment.entry_data record
        SELECT count(*) FROM experiment.entry_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.entry_data 
            DELETE FROM experiment.entry_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.entry_data %', record_count;
        END IF;
    
        -- count experiment.experiment_data record
        SELECT count(*) FROM experiment.experiment_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.experiment_data 
            DELETE FROM experiment.experiment_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.experiment_data %', record_count;
        END IF;
    
        -- count experiment.location_data record
        SELECT count(*) FROM experiment.location_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.location_data 
            DELETE FROM experiment.location_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.location_data %', record_count;
        END IF;
    
        -- count experiment.occurrence_data record
        SELECT count(*) FROM experiment.occurrence_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.occurrence_data 
            DELETE FROM experiment.location_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.location_data %', record_count;
        END IF;
    
        -- count experiment.plant_data record
        SELECT count(*) FROM experiment.plant_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.occurrence_data 
            DELETE FROM experiment.plant_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.plant_data %', record_count;
        END IF;
    
        -- count experiment.plot_data record
        SELECT count(*) FROM experiment.plot_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            DELETE FROM experiment.plot_data WHERE variable_id  = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.plot_data %', record_count;
        END IF;
    
        -- count experiment.sample_measurement record
        SELECT count(*) FROM experiment.sample_measurement  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.sample_measurement 
            DELETE FROM experiment.sample_measurement WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.sample_measurement %', record_count;
        END IF;
    
        -- count experiment.subplot_data record
        SELECT count(*) FROM experiment.subplot_data  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.subplot_data 
            DELETE FROM experiment.subplot_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.subplot_data %', record_count;
        END IF;
    
        -- count germplasm.cross_attribute record
        SELECT count(*) FROM germplasm.cross_attribute  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.cross_attribute 
            DELETE FROM germplasm.cross_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.cross_attribute %', record_count;
        END IF;
    
        -- count germplasm.cross_data record
        SELECT count(*) FROM germplasm.cross_data  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.cross_data 
            DELETE FROM germplasm.cross_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.cross_data %', record_count;
        END IF;
    
        -- count germplasm.germplasm_attribute record
        SELECT count(*) FROM germplasm.germplasm_attribute  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.germplasm_attribute 
            DELETE FROM germplasm.germplasm_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.germplasm_attribute %', record_count;
        END IF;
    
        -- count germplasm.germplasm_trait record
        SELECT count(*) FROM germplasm.germplasm_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.germplasm_trait 
            DELETE FROM germplasm.germplasm_trait WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.germplasm_trait %', record_count;
        END IF;
    
        -- count germplasm.package_data record
        SELECT count(*) FROM germplasm.package_data  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.package_data 
            DELETE FROM germplasm.package_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.package_data %', record_count;
        END IF;
    
        -- count germplasm.package_trait record
        SELECT count(*) FROM germplasm.package_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.package_trait 
            DELETE FROM germplasm.package_trait WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.package_trait %', record_count;
        END IF;
    
        -- count germplasm.seed_attribute record
        SELECT count(*) FROM germplasm.package_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.seed_attribute 
            DELETE FROM germplasm.seed_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.seed_attribute %', record_count;
        END IF;
    
        -- count germplasm.seed_data record
        SELECT count(*) FROM germplasm.seed_data  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.seed_data 
            DELETE FROM germplasm.seed_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.seed_data %', record_count;
        END IF;
    
        -- count master.formula record
        SELECT count(*) FROM master.formula  WHERE result_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
                
            SELECT id FROM master.formula WHERE result_variable_id = var_id INTO frm_id;
            
            -- delete master.formula_parameter 
            DELETE FROM master.formula_parameter WHERE formula_id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula_parameter %', record_count;
        
            -- delete master.formula
            DELETE FROM master.formula WHERE id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula %', record_count;
            
            -- delete master.method 
            DELETE FROM master.method WHERE formula_id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.method %', record_count;
    
        END IF;
    
        SELECT count(*) FROM master.formula_parameter WHERE param_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.formula_parameter WHERE param_variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula_parameter %', record_count;
        END IF;
    
        SELECT count(*) FROM master.item_metadata WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.item_metadata WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.item_metadata %', record_count;
        END IF;
    
        SELECT count(*) FROM master.item_metadata WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.record_variable
            DELETE FROM master.record_variable WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.record_variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE member_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- update master.record_variable
            UPDATE master.variable SET member_variable_id = NULL;
            RAISE NOTICE '!TOTAL UPDATED RECORDS FROM master.variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE target_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- update master.record_variable
            UPDATE master.variable SET target_variable_id = NULL;
            RAISE NOTICE '!TOTAL UPDATED RECORDS FROM master.variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable_mapping vm WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable_mapping WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.variable_mapping %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable_set_member WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable_set_member WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.variable_set_member %', record_count;
        END IF;
    
        SELECT count(*) FROM master.record_variable WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.record_variable WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.record_variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable WHERE id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.record_variable %', record_count;
        END IF;
    
    END LOOP;
    
END;
$$



--rollback SELECT NULL;
