--liquibase formatted sql

--changeset postgres:update_role_display_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-944 Update role display values



-- update scale values
UPDATE
    tenant.person_role AS prole
SET
    person_role_name = t.person_role_name,
    description = t.description
FROM
    (
        VALUES
        ('ADMIN', 'Administrator', 'Authorized user who has full access to the entire data and the system'),
        ('B4R_USER', 'User', 'Default role assigned to all users'),
        ('DATA_CONSUMER', 'Data Consumer', 'Has basic read access to a data set'),
        ('DATA_OWNER', 'Data Owner', 'Owns a particular data set and has the ability to manage its ownership'),
        ('DATA_PRODUCER', 'Data Producer', 'Granted read and write privileges to a data set'),
        ('DM', 'Data Manager', 'Has management privileges to data within a respective program'),
        ('ETM', 'Experienced Team Member', 'Responsible for executing daily process, activities, and tasks within a program'),
        ('TL', 'Team Leader', 'Responsible for managing the entire program, its process, staff, etc.'),
        ('TM', 'Team Member', 'Responsible for executing specific tasks and steps within a program')
    ) AS t (
        person_role_code,
        person_role_name,
        description
    )
WHERE
    prole.person_role_code = t.person_role_code
;



-- revert changes
--rollback UPDATE
--rollback     tenant.person_role AS prole
--rollback SET
--rollback     person_role_name = t.person_role_name,
--rollback     description = t.description
--rollback FROM
--rollback     (
--rollback         VALUES
--rollback         ('ADMIN', 'administrator', 'Authorized user who has full access to the entire system'),
--rollback         ('B4R_USER', 'B4R User', 'Default role assigned to all Breeding4Rice users'),
--rollback         ('DATA_CONSUMER', 'data consumer', 'Data consumers have the basic access to a record and that is...'),
--rollback         ('DATA_OWNER', 'data owner', 'Data owners are users who possess the ownership of a record....'),
--rollback         ('DATA_PRODUCER', 'data producer', '### Data producer\r\n\r\nData producers are users who are grante...'),
--rollback         ('DM', 'data manager', 'The role for data admin.'),
--rollback         ('ETM', 'experienced team member', '- Is responsible for executing Process" daily Activities, Ta...'),
--rollback         ('TL', 'Team Leader', '- Is responsible for management of a Process (financial and ...'),
--rollback         ('TM', 'member', 'Executes tasks and steps\n')
--rollback     ) AS t (
--rollback         person_role_code,
--rollback         person_role_name,
--rollback         description
--rollback     )
--rollback WHERE
--rollback     prole.person_role_code = t.person_role_code
--rollback ;
