--liquibase formatted sql

--changeset postgres:activate_partially_replicated_and_augmented_design_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-950 Activate partially replicated and augmented design scale values



UPDATE 
    master.scale_value 
SET 
    is_void = false 
WHERE 
    value 
IN 
    ('Partially Replicated', 'Augmented Design') 
AND 
    scale_id 
IN 
    (
        SELECT 
            scale_id 
        FROM 
            master.variable 
        WHERE 
            abbrev = 'DESIGN'
    )
;



--rollback UPDATE 
--rollback     master.scale_value 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE 
--rollback     value 
--rollback IN 
--rollback     ('Partially Replicated', 'Augmented Design') 
--rollback AND 
--rollback     scale_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             scale_id 
--rollback         FROM 
--rollback             master.variable 
--rollback         WHERE 
--rollback             abbrev = 'DESIGN'
--rollback     )
--rollback ;