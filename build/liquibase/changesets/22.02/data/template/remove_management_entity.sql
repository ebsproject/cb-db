--liquibase formatted sql

--changeset postgres:remove_management_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-535 Remove MANAGEMENT entity



DELETE FROM 
    dictionary.entity
WHERE
    abbrev = 'MANAGEMENT'
;



--rollback INSERT INTO
--rollback     dictionary.entity
--rollback         (abbrev,name,description,table_id,creator_id)
--rollback VALUES
--rollback     (
--rollback         'MANAGEMENT',
--rollback         'Management',
--rollback         'Management',
--rollback         (SELECT id FROM dictionary.table WHERE abbrev='VARIABLE'),
--rollback         1
--rollback     )
--rollback ;