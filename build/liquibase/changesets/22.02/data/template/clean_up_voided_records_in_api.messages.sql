--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_api.messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in api.messages



DELETE FROM
    api.messages
WHERE
    code = '404040'
AND
    is_void = true;



--rollback SELECT NULL;
