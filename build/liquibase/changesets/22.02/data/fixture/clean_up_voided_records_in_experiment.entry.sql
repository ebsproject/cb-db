--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_experiment.entry_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in experiment.entry_data



DELETE FROM 
    experiment.entry_data 
WHERE 
    entry_id 
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in experiment.planting_instruction



UPDATE 
    experiment.planting_instruction 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_experiment.planting_job_entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in experiment.planting_job_entry



UPDATE 
    experiment.planting_job_entry 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in experiment.plot



UPDATE 
    experiment.plot 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_experiment.sample context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in experiment.sample



UPDATE 
    experiment.sample 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_germplasm.cross context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in germplasm.cross



UPDATE 
    germplasm.cross 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_entry_id_to_null_in_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set entry_id to null in germplasm.cross_parent



UPDATE 
    germplasm.cross_parent 
SET 
    entry_id = NULL
WHERE
    entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:set_source_entry_id_to_null_in_germplasm.seed context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Set source_entry_id in germplasm.seed



UPDATE 
    germplasm.seed 
SET 
    source_entry_id = NULL
WHERE
    source_entry_id
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
;



--rollback SELECT NULL;



--changeset postgres:clean_up_voided_records_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in experiment.entry



DELETE FROM 
    experiment.entry 
WHERE 
    id 
IN 
    (
        '1183741',
        '1183742',
        '1183743',
        '1183744',
        '1183745',
        '1183747',
        '1183748',
        '1183749',
        '1184567',
        '1184568',
        '1184570',
        '1184571',
        '1184572',
        '1184573',
        '1184576',
        '1184658',
        '1184659',
        '1184660',
        '1184661',
        '1184662',
        '1184664',
        '1184665',
        '1184666',
        '1184675',
        '1184682',
        '1184769',
        '1184770',
        '1184772',
        '1184773',
        '1184774',
        '1184775',
        '1184778',
        '1184816',
        '1184817',
        '1184818',
        '1184819',
        '1184820',
        '1184822',
        '1184823',
        '1184824'
    )
AND
    is_void = true
;



--rollback SELECT NULL;


