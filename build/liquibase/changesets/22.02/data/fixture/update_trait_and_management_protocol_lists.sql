--liquibase formatted sql

--changeset postgres:update_management_protocol_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-535 Update management protocol lists



-- general use cases
UPDATE
    platform.list AS lst
SET
    type = 'variable',
    list_sub_type = 'management',
    entity_id = enty.id,
    notes = platform.append_text(lst.notes, 'DB-535 management 1')
FROM
    dictionary.entity AS enty
WHERE
    lst.list_usage = 'final list'
    AND lst.type = 'trait'
    AND lst.list_sub_type = 'management protocol'
    AND enty.abbrev = 'VARIABLE'
;

UPDATE
    platform.list AS lst
SET
    type = 'variable',
    list_sub_type = 'management protocol',
    entity_id = enty.id,
    notes = platform.append_text(lst.notes, 'DB-535 management 2')
FROM
    dictionary.entity AS enty
WHERE
    lst.list_usage = 'working list'
    AND lst.type = 'trait'
    AND lst.list_sub_type IS NULL
    AND enty.abbrev = 'VARIABLE'
;

-- special use case
UPDATE
    platform.list AS lst
SET
    type = 'variable',
    list_sub_type = 'management protocol',
    entity_id = enty.id,
    notes = platform.append_text(lst.notes, 'DB-535 management 3')
FROM
    dictionary.entity AS enty
WHERE
    lst.list_usage = 'working list'
    AND lst.type = 'management protocol'
    AND enty.abbrev = 'VARIABLE'
;



-- revert changes
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     type = 'trait',
--rollback     list_sub_type = 'management protocol',
--rollback     entity_id = enty.id,
--rollback     notes = REPLACE(lst.notes, 'DB-535 management 1', '')
--rollback FROM
--rollback     dictionary.entity AS enty
--rollback WHERE
--rollback     lst.list_usage = 'final list'
--rollback     AND lst.type = 'variable'
--rollback     AND lst.list_sub_type = 'management'
--rollback     AND enty.abbrev = 'MANAGEMENT'
--rollback     AND lst.notes LIKE '%DB-535 management 1%'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     type = 'management protocol',
--rollback     list_sub_type = NULL,
--rollback     entity_id = enty.id,
--rollback     notes = REPLACE(lst.notes, 'DB-535 management 2', '')
--rollback FROM
--rollback     dictionary.entity AS enty
--rollback WHERE
--rollback     enty.abbrev = 'MANAGEMENT'
--rollback     AND lst.type = 'variable'
--rollback     AND lst.list_sub_type = 'management protocol'
--rollback     AND lst.notes LIKE '%DB-535 management 2%'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     type = 'management protocol',
--rollback     list_sub_type = NULL,
--rollback     entity_id = enty.id,
--rollback     notes = REPLACE(lst.notes, 'DB-535 management 3', '')
--rollback FROM
--rollback     dictionary.entity AS enty
--rollback WHERE
--rollback     enty.abbrev = 'MANAGEMENT'
--rollback     AND lst.type = 'variable'
--rollback     AND lst.list_sub_type = 'management protocol'
--rollback     AND lst.notes LIKE '%DB-535 management 3%'
--rollback ;



--changeset postgres:update_trait_protocol_lists context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-535 Update trait protocol lists



-- general use cases
UPDATE
    platform.list AS lst
SET
    list_sub_type = NULL,
    notes = platform.append_text(lst.notes, 'DB-535 trait 1')
WHERE
    lst.list_usage = 'final list'
    AND lst.type = 'trait'
    AND lst.list_sub_type = 'trait protocol'
;

UPDATE
    platform.list AS lst
SET
    list_sub_type = 'trait protocol',
    notes = platform.append_text(lst.notes, 'DB-535 trait 2')
WHERE
    lst.list_usage = 'working list'
    AND lst.type = 'trait'
    AND lst.list_sub_type IS NULL
;

-- special use cases
UPDATE
    platform.list AS lst
SET
    entity_id = enty.id,
    notes = platform.append_text(lst.notes, 'DB-535 trait 3')
FROM
    dictionary.entity AS enty
WHERE
    enty.abbrev = 'TRAIT'
    AND lst.type = 'trait'
    AND lst.list_sub_type = 'trait protocol'
;

UPDATE
    platform.list AS lst
SET
    type = 'trait',
    list_sub_type = 'trait protocol',
    notes = platform.append_text(lst.notes, 'DB-535 trait 4')
WHERE
    lst.type = 'trait protocol'
    AND lst.list_sub_type IS NULL
;



-- revert changes
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     list_sub_type = 'trait protocol',
--rollback     notes = REPLACE(lst.notes, 'DB-535 trait 1', '')
--rollback WHERE
--rollback     lst.list_usage = 'final list'
--rollback     AND lst.type = 'trait'
--rollback     AND lst.list_sub_type IS NULL
--rollback     AND lst.notes LIKE '%DB-535 trait 1%'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     list_sub_type = NULL,
--rollback     notes = REPLACE(lst.notes, 'DB-535 trait 2', '')
--rollback WHERE
--rollback     lst.list_usage = 'working list'
--rollback     AND lst.type = 'trait'
--rollback     AND lst.list_sub_type = 'trait protocol'
--rollback     AND lst.notes LIKE '%DB-535 trait 2%'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     entity_id = enty.id,
--rollback     notes = REPLACE(lst.notes, 'DB-535 trait 3', '')
--rollback FROM
--rollback     dictionary.entity AS enty
--rollback WHERE
--rollback     enty.abbrev = 'VARIABLE'
--rollback     AND lst.type = 'trait'
--rollback     AND lst.list_sub_type = 'trait protocol'
--rollback     AND lst.notes LIKE '%DB-535 trait 3%'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     platform.list AS lst
--rollback SET
--rollback     type = 'trait protocol',
--rollback     list_sub_type = NULL,
--rollback     notes = REPLACE(lst.notes, 'DB-535 trait 4', '')
--rollback WHERE
--rollback     lst.type = 'trait'
--rollback     AND lst.list_sub_type = 'trait protocol'
--rollback     AND lst.notes LIKE '%DB-535 trait 4%'
--rollback ;
