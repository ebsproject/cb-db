--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_experiment.plot_data_p3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in experiment.plot_data p3



DELETE FROM 
    experiment.plot_data 
WHERE 
    id 
IN 
    (
        '7286819',
        '7286827',
        '7286829',
        '7286845',
        '7286846',
        '7286847',
        '7286849',
        '7286850',
        '7286852',
        '7286855',
        '7286860',
        '7286863',
        '7286868',
        '7286871',
        '7286879',
        '7286882',
        '7286884',
        '7286885',
        '7286890',
        '7286893',
        '7286895',
        '7286903',
        '7286904',
        '7286905',
        '7286907',
        '7286909',
        '7286933',
        '7286934',
        '7286935',
        '7286937',
        '7286941',
        '7286944',
        '7286992',
        '7286993',
        '7286996',
        '7286998',
        '7287002',
        '7287005',
        '7287015',
        '7287018',
        '7287023',
        '7287027',
        '7287029',
        '7287032',
        '7287034',
        '7287039',
        '7287040',
        '7287042',
        '7287043',
        '7287050',
        '7287056',
        '7287066',
        '7287067',
        '7287068',
        '7287070',
        '7287075',
        '7287088',
        '7287098',
        '7287100',
        '7287104',
        '7287105',
        '7287108',
        '7287113',
        '7287120',
        '7287121',
        '7287122',
        '7287125',
        '7287126',
        '7287144',
        '7287150',
        '7287151',
        '7287158',
        '7287160',
        '7287161',
        '7287163',
        '7287164',
        '7287174',
        '7287176',
        '7287182',
        '7287183',
        '7287188',
        '7287193',
        '7287194',
        '7287202',
        '7287204',
        '7287206',
        '7287219',
        '7287221',
        '7287222',
        '7287228',
        '7287235',
        '7287237',
        '7287241',
        '7287244',
        '7287247',
        '7287248',
        '7287249',
        '7287258',
        '7287265',
        '7287271',
        '7287273',
        '7287275',
        '7287276',
        '7287282',
        '7287288',
        '7287289',
        '7287291',
        '7287294',
        '7287301',
        '7287304',
        '7287305',
        '7287309',
        '7287311',
        '7287317',
        '7287318',
        '7287319',
        '7287341',
        '7287342',
        '7287349',
        '7287351',
        '7287352',
        '7287353',
        '7287393',
        '7287394',
        '7287395',
        '7287404',
        '7287406',
        '7287407',
        '7287414',
        '7287415',
        '7287417',
        '7287418',
        '7287428',
        '7287431',
        '7287453',
        '7287458',
        '7287467',
        '7287469',
        '7287471',
        '7287474',
        '7287479',
        '7287484',
        '7287490',
        '7287491',
        '7287493',
        '7287494',
        '7287498',
        '7287503',
        '7287505',
        '7287510',
        '7287511',
        '7287513',
        '7287522',
        '7287523',
        '7287524',
        '7287526',
        '7287528',
        '7287538',
        '7287543',
        '7287546',
        '7287552',
        '7287559',
        '7287560',
        '7287562',
        '7287572',
        '7287573',
        '7287574',
        '7287575',
        '7287580',
        '7287582',
        '7287587',
        '7287593',
        '7287599',
        '7287602',
        '7287603',
        '7287606',
        '7287607',
        '7287610',
        '7287613',
        '7287617',
        '7287625',
        '7287627',
        '7287639',
        '7287643',
        '7287649',
        '7287651',
        '7287653',
        '7287657',
        '7287677',
        '7287678',
        '7287680',
        '7287685',
        '7287687',
        '7287692',
        '7287699',
        '7287702',
        '7287703',
        '7287705',
        '7287709',
        '7287714',
        '7287748',
        '7287750',
        '7287756',
        '7287759',
        '7287762',
        '7287765',
        '7287770',
        '7287771',
        '7287773',
        '7287779',
        '7287780',
        '7287786',
        '7287789',
        '7287791',
        '7287796',
        '7287798',
        '7287805',
        '7287809',
        '7287865',
        '7287867',
        '7287868',
        '7287870',
        '7287879',
        '7287881',
        '7287913',
        '7287916',
        '7287917',
        '7287924',
        '7287926',
        '7287932',
        '7287962',
        '7287965',
        '7287969',
        '7287972',
        '7287975',
        '7287976',
        '7287980',
        '7287983',
        '7287988',
        '7287989',
        '7287990',
        '7287995',
        '7288006',
        '7288010',
        '7288011',
        '7288012',
        '7288015',
        '7288016',
        '7288023',
        '7288025',
        '7288033',
        '7288035',
        '7288041',
        '7288042',
        '7288043',
        '7288047',
        '7288049',
        '7288050',
        '7288053',
        '7288063',
        '7288077',
        '7288078',
        '7288082',
        '7288084',
        '7288088',
        '7288093',
        '7288108',
        '7288109',
        '7288111',
        '7288117',
        '7288119',
        '7288125',
        '7288134',
        '7288137',
        '7288143',
        '7288144',
        '7288147',
        '7288148',
        '7288163',
        '7288165',
        '7288172',
        '7288174',
        '7288176',
        '7288179',
        '7288194',
        '7288195',
        '7288198',
        '7288203',
        '7288208',
        '7288209',
        '7288216',
        '7288223',
        '7288229',
        '7288231',
        '7288233',
        '7288234',
        '7288237',
        '7288241',
        '7288243',
        '7288245',
        '7288255',
        '7288256',
        '7288271',
        '7288274',
        '7288276',
        '7288283',
        '7288287',
        '7288288',
        '7288304',
        '7288308',
        '7288312',
        '7288314',
        '7288318',
        '7288319',
        '7288325',
        '7288326',
        '7288331',
        '7288335',
        '7288336',
        '7288342',
        '7288345',
        '7288346',
        '7288348',
        '7288349',
        '7288358',
        '7288361',
        '7288368',
        '7288369',
        '7288374',
        '7288376',
        '7288384',
        '7288386',
        '7288393',
        '7288394',
        '7288402',
        '7288403',
        '7288405',
        '7288406',
        '7288421',
        '7288424',
        '7288432',
        '7288436',
        '7288438',
        '7288440',
        '7288449',
        '7288451',
        '7288456',
        '7288458',
        '7288460',
        '7288462',
        '7288468',
        '7288471',
        '7288472',
        '7288476',
        '7288478',
        '7288480',
        '9517755',
        '9517756',
        '9517757',
        '9517758',
        '9517759',
        '9517760',
        '9517761',
        '9517762',
        '9517763',
        '9517764',
        '9517765',
        '9517766',
        '9517767',
        '9517768',
        '9517769',
        '9517770',
        '9517771',
        '9517772',
        '9517773',
        '9517774',
        '9517775',
        '9517776',
        '9517777',
        '9517778',
        '9517779',
        '9517780',
        '9517781',
        '9517782',
        '9517783',
        '9517784',
        '9517785',
        '9517854',
        '9517855',
        '9517856',
        '9517857',
        '9517858',
        '9517859',
        '9517860',
        '9517861',
        '9517862',
        '9517863',
        '9517864',
        '9517865',
        '9517866',
        '9517867',
        '9517868',
        '9517869',
        '9517870',
        '9517871',
        '9517872',
        '9517873',
        '9517874',
        '9517875',
        '9517876',
        '9517877',
        '9517878',
        '9517879',
        '9517880',
        '9517881',
        '9517882',
        '9517883',
        '9517884',
        '9517885',
        '9517886',
        '9517887',
        '9517888',
        '9517889',
        '9517890',
        '9517891',
        '9517892',
        '9517893',
        '9517894',
        '9517895',
        '9517896',
        '9517897',
        '9517898',
        '9517899',
        '9517900',
        '9517901',
        '9517902',
        '9517903',
        '9517904',
        '9517905',
        '9517906',
        '9517907',
        '9517908',
        '9517909',
        '9517910',
        '9517911',
        '9517912',
        '9517913',
        '9517914',
        '9517915',
        '9517916',
        '9517917',
        '9517918',
        '9517919',
        '9517920',
        '9517921',
        '9517922',
        '9517923',
        '9517924',
        '9517925',
        '9517926',
        '9517927',
        '9517928',
        '9517929',
        '9517930',
        '9517931',
        '9517932',
        '9517933',
        '9517934',
        '9517935',
        '9517936',
        '9517937',
        '9517938',
        '9517939',
        '9517940',
        '9517941',
        '9517942',
        '9517943',
        '9517944',
        '9517945',
        '9517946',
        '9517947',
        '9517948',
        '9517949',
        '9517950',
        '9517951',
        '9517952',
        '9517953',
        '9517954',
        '9517955',
        '9517956',
        '9517957',
        '9517958',
        '9517959',
        '9517960',
        '9517961',
        '9517962',
        '9517963',
        '9517964',
        '9517965',
        '9517966',
        '9517967',
        '9517968',
        '9517969',
        '9517970',
        '9517971',
        '9517972',
        '9517973',
        '9517974',
        '9517975',
        '9517976',
        '9517977',
        '9517978',
        '9517979',
        '9517980',
        '9517981',
        '9517982',
        '9517983',
        '9517984',
        '9517985',
        '9517986',
        '9517987',
        '9517988',
        '9517989',
        '9517990',
        '9517991',
        '9517992',
        '9517993',
        '9517994',
        '9517995',
        '9517996',
        '9517997',
        '9517998',
        '9517999',
        '9518000',
        '9518001',
        '9518002',
        '9518003',
        '9518004',
        '9518005',
        '9518006',
        '9518007',
        '9518008',
        '9518009',
        '9518010',
        '9518011',
        '9518012',
        '9518013',
        '9518014',
        '9518015',
        '9518016',
        '9518017',
        '9518018',
        '9518019',
        '9518020',
        '9518021',
        '9518022',
        '9518023',
        '9518024',
        '9518025',
        '9518026',
        '9518027',
        '9518028',
        '9518029',
        '9518030',
        '9518031',
        '9518032',
        '9518033',
        '9518034',
        '9518035',
        '9518036',
        '9518037',
        '9518038',
        '9518039',
        '9518040',
        '9518041',
        '9518042',
        '9518043',
        '9518044',
        '9518045',
        '9518046',
        '9518047',
        '9518048',
        '9518049',
        '9518050',
        '9518051',
        '9518052',
        '9518053',
        '9518054',
        '9518055',
        '9518056',
        '9518057',
        '9518058',
        '9518059',
        '9518060',
        '9518061',
        '9518062',
        '9518063',
        '9518064',
        '9518065',
        '9518066',
        '9518067',
        '9518068',
        '9518069',
        '9518070',
        '9518071',
        '9518072',
        '9518073',
        '9518074',
        '9518075',
        '9518076',
        '9518077',
        '9518078',
        '9518079',
        '9518080',
        '9518081',
        '9518082',
        '9518083',
        '9518084',
        '9518085',
        '9518086',
        '9518087',
        '9518088',
        '9518089',
        '9518090',
        '9518091',
        '9518092',
        '9518093',
        '9518094',
        '9518095',
        '9518096',
        '9518097',
        '9518098',
        '9518099',
        '9518100',
        '9518101',
        '9518102',
        '9518103',
        '9518104',
        '9518105',
        '9518106',
        '9518107',
        '9518108',
        '9518109',
        '9518110',
        '9518111',
        '9518112',
        '9518113',
        '9518114',
        '9518115',
        '9518116',
        '9518117',
        '9518118',
        '9518119',
        '9518120',
        '9518121',
        '9518122',
        '9518123',
        '9518124',
        '9518125',
        '9518126',
        '9518127',
        '9518128',
        '9518129',
        '9518130',
        '9518131',
        '9518132',
        '9518133',
        '9518134',
        '9518135',
        '9518136',
        '9518137',
        '9518138',
        '9518139',
        '9518140',
        '9518141',
        '9518142',
        '9518143',
        '9518144',
        '9518145',
        '9518146',
        '9518147',
        '9518148',
        '9518149',
        '9518150',
        '9518151',
        '9518152',
        '9518153',
        '9518154',
        '9518155',
        '9518156',
        '9518157',
        '9518158',
        '9518159',
        '9518160',
        '9518161',
        '9518162',
        '9518163',
        '9518164',
        '9518165',
        '9518166',
        '9518167',
        '9518168',
        '9518169',
        '9518170',
        '9518171',
        '9518172',
        '9518173',
        '9518174',
        '9518175',
        '9518176',
        '9518177',
        '9518178',
        '9518179',
        '9518180',
        '9518181',
        '9518182',
        '9518183',
        '9518184',
        '9518185',
        '9518186',
        '9518187',
        '9518188',
        '9518189',
        '9518190',
        '9518191',
        '9518192',
        '9518193',
        '9518194',
        '9518195',
        '9518196',
        '9518197',
        '9518198',
        '9518199',
        '9518200',
        '9518201',
        '9518202',
        '9518203',
        '9518204',
        '9518205',
        '9518206',
        '9518207',
        '9518208',
        '9518209',
        '9518210',
        '9518211',
        '9518212',
        '9518213',
        '9518214',
        '9518215',
        '9518216',
        '9518217',
        '9518218',
        '9518219',
        '9518220',
        '9518221',
        '9518222',
        '9518223',
        '9518224',
        '9518225',
        '9518226',
        '9518227',
        '9518228',
        '9518229',
        '9518230',
        '9518231',
        '9518232',
        '9518233',
        '9518234',
        '9518235',
        '9518236',
        '9518237',
        '9518238',
        '9518239',
        '9518240',
        '9518241',
        '9518242',
        '9518243',
        '9518244',
        '9518245',
        '9518246',
        '9518247',
        '9518248',
        '9518249',
        '9518250',
        '9518251',
        '9518252',
        '9518253',
        '9518254',
        '9518255',
        '9518256',
        '9518257',
        '9518258',
        '9518259',
        '9518260',
        '9518261',
        '9518262',
        '9518263',
        '9518264',
        '9518265',
        '9518266',
        '9518267',
        '9518268',
        '9518269',
        '9518270',
        '9518271',
        '9518272',
        '9518273',
        '9518274',
        '9518275',
        '9518276',
        '9518277',
        '9518278',
        '9518279',
        '9518280',
        '9518281',
        '9518282',
        '9518283',
        '9518284',
        '9518285',
        '9518286',
        '9518287',
        '9518288',
        '9518289',
        '9518290',
        '9518291',
        '9518292',
        '9518293',
        '9518294',
        '9518295',
        '9518296',
        '9518297',
        '9518298',
        '9518299',
        '9518300',
        '9518301',
        '9518302',
        '9518303',
        '9518304',
        '9518305',
        '9518306',
        '9518307',
        '9518308',
        '9518309',
        '9518310',
        '9518311',
        '9518312',
        '9518313',
        '9518314',
        '9518315',
        '9518316',
        '9518317',
        '9518318',
        '9518319',
        '9518320',
        '9518321',
        '9518322',
        '9518323',
        '9518324',
        '9518325',
        '9518326',
        '9518327',
        '9518328',
        '9518329',
        '9518330',
        '9518331',
        '9518332',
        '9518333'
    )
;



--rollback SELECT NULL;