--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_experiment.planting_instruction_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in experiment.planting_instruction p2



UPDATE
    experiment.planting_instruction 
SET
    seed_id = NULL
WHERE
    seed_id
IN 
    (
        '1981882',
        '1981883',
        '1981884',
        '1981885',
        '1981886',
        '1981887',
        '1981888',
        '1981889',
        '1981890',
        '1981893',
        '1981894',
        '1981895',
        '1981896',
        '1981897',
        '1981898'
    )
;



--rollback SELECT NULL;