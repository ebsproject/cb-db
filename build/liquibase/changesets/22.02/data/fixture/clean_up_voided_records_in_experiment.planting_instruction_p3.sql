--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_experiment.planting_instruction_p3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in experiment.planting_instruction p3



DELETE FROM
    experiment.planting_instruction 
WHERE
    id
IN 
    (
        '1363949',
        '1451319',
        '1451329',
        '1451549',
        '1451663',
        '1451699',
        '1451837',
        '1451654',
        '1451686',
        '1451727',
        '1451803',
        '1451367',
        '1451474',
        '1451482',
        '1451747',
        '1451808',
        '1451315',
        '1451339',
        '1451609',
        '1451615',
        '1451644',
        '1451664',
        '1451726',
        '1451773',
        '1451370',
        '1451380',
        '1451466',
        '1451484',
        '1451572',
        '1451691',
        '1451761',
        '1451412',
        '1451431',
        '1451438',
        '1451526',
        '1451558',
        '1451775',
        '1451795',
        '1451318',
        '1451537',
        '1451622',
        '1451641',
        '1451796',
        '1451826',
        '1451971',
        '1451978',
        '1451928',
        '1452000',
        '1451950',
        '1451963',
        '1451969',
        '1452031',
        '1451911',
        '1451980',
        '1451988',
        '1451993',
        '1452003',
        '1451990'
    )
;



--rollback SELECT NULL;