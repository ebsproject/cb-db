--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.seed_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.seed p2



DELETE FROM
    germplasm.package_log 
WHERE 
    package_id
IN 
    (
        SELECT 
            id 
        FROM 
            germplasm.package 
        WHERE 
            package_label = 'WE-KIB-17A-41-36'
    )
;

DELETE FROM 
    germplasm.package 
WHERE 
    package_label = 'WE-KIB-17A-41-36'
;

DELETE FROM 
    germplasm.seed 
WHERE 
    seed_name = 'WE-KIB-17A-41-36'
;



--rollback SELECT NULL;