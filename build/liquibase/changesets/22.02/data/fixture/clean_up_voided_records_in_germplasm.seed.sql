--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.seed context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.seed



UPDATE
    experiment.entry
SET
    seed_id = NULL
WHERE
    seed_id = 1981877
;

UPDATE
    experiment.planting_instruction
SET
    seed_id = NULL
WHERE
    seed_id = 1981877
;

UPDATE
    germplasm.cross_parent
SET
    seed_id = NULL
WHERE
    seed_id = 1981877
;

DELETE FROM 
    germplasm.seed 
WHERE 
    id 
IN 
    (
        '1981877',
        '1981883',
        '1981884',
        '1981885',
        '1981886',
        '1981887',
        '1981888',
        '1981889',
        '1981890',
        '1981893',
        '1981894',
        '1981895',
        '1981896',
        '1981897',
        '1981898'
    )
;



--rollback SELECT NULL;