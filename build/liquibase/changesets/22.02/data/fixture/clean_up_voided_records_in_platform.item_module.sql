--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_platform.item_module context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in platform.item_module



DELETE FROM 
    platform.item_module 
WHERE 
    id 
IN 
    (
        '57',
        '123',
        '124',
        '125',
        '126',
        '127',
        '128',
        '129',
        '130',
        '131',
        '132',
        '149',
        '150',
        '151',
        '152',
        '153',
        '154',
        '155',
        '156',
        '157',
        '158'
    )
;



--rollback SELECT NULL;