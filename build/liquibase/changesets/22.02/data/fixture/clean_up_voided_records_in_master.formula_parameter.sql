--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_master.formula_parameter context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in master.formula_parameter



DELETE FROM 
    master.formula_parameter 
WHERE 
    id 
IN 
    (
        '20',
        '27',
        '45',
        '69',
        '70',
        '71',
        '283',
        '284',
        '285',
        '286',
        '459',
        '462',
        '467',
        '469',
        '470',
        '471',
        '472',
        '473',
        '475',
        '480',
        '484',
        '485',
        '487',
        '495',
        '498',
        '504',
        '505',
        '506',
        '507',
        '510',
        '511',
        '547',
        '548',
        '549',
        '550',
        '551',
        '552',
        '553',
        '554',
        '555',
        '556',
        '557',
        '558',
        '691',
        '692'
    )
;



--rollback SELECT NULL;