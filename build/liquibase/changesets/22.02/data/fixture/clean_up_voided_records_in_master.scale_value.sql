--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_master.scale_value context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in master.scale_value



DELETE FROM 
    master.scale_value 
WHERE 
    id 
IN 
    (
        '2072',
        '2073',
        '2277',
        '2278',
        '2279',
        '2280',
        '2605',
        '2606',
        '2661'
    )
;



--rollback SELECT NULL;