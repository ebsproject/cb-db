--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_platform.item_module_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in platform.item_module p2



DELETE FROM 
    platform.item_module 
WHERE 
    module_id 
IN 
    (
        '348',
        '357',
        '372'
    )
;



--rollback SELECT NULL;