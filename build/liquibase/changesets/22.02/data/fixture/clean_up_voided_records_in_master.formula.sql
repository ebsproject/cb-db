--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_master.formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in master.formula



DELETE FROM 
    master.formula 
WHERE 
    id 
IN 
    (
        '29',
        '30',
        '31',
        '32',
        '33',
        '34'
    )
;



--rollback SELECT NULL;