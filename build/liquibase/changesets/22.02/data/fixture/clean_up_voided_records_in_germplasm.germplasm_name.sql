--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.germplasm_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.germplasm_name



DELETE FROM 
    germplasm.germplasm_name
WHERE 
    germplasm_id
IN 
    (
        '1363949',
        '1363950',
        '1363951',
        '1363952',
        '1363953',
        '1363954',
        '1363955',
        '1363956',
        '1363957',
        '1363960',
        '1363961',
        '1363962',
        '1363963',
        '1363964',
        '1363965'
    )
;



--rollback SELECT NULL;