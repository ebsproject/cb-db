--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_platform.dashboard_widget context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in platform.dashboard_widget



DELETE FROM 
    platform.dashboard_widget 
WHERE 
    id 
IN 
    (
        '2',
        '8',
        '10',
        '11'
    )
;



--rollback SELECT NULL;