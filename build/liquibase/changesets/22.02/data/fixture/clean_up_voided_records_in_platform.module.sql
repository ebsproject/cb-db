--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_platform.module context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in platform.module



DELETE FROM 
    platform.module 
WHERE 
    id 
IN 
    (
        '152',
        '222',
        '223',
        '224',
        '225',
        '226',
        '227',
        '228',
        '229',
        '230',
        '231',
        '248',
        '249',
        '250',
        '251',
        '252',
        '253',
        '254',
        '255',
        '256',
        '257',
        '348',
        '357',
        '372'
    )
;



--rollback SELECT NULL;