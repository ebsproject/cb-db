--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.cross_parent_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.cross_parent p2



UPDATE
    germplasm.cross_parent 
SET
    seed_id = NULL
WHERE
    seed_id
IN 
    (
        '1981882',
        '1981883',
        '1981884',
        '1981885',
        '1981886',
        '1981887',
        '1981888',
        '1981889',
        '1981890',
        '1981893',
        '1981894',
        '1981895',
        '1981896',
        '1981897',
        '1981898'
    )
;



--rollback SELECT NULL;