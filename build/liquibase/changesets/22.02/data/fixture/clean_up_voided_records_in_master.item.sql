--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_master.item context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in master.item



DELETE FROM 
    master.item 
WHERE 
    id 
IN 
    (
        '1147',
        '1207',
        '1215',
        '1216',
        '1300',
        '1319',
        '1326',
        '1332',
        '1337',
        '1341',
        '1353',
        '1354',
        '1355',
        '1356',
        '1359',
        '1375',
        '1391',
        '1407',
        '1420',
        '1421',
        '1422',
        '1423',
        '1424',
        '1425',
        '1426',
        '1427',
        '1428',
        '1429',
        '1430',
        '1447',
        '1448',
        '1449',
        '1450',
        '1451',
        '1452',
        '1453',
        '1454',
        '1455',
        '1456',
        '1457',
        '1458',
        '1469',
        '1472',
        '1483',
        '1486',
        '1556',
        '1565',
        '1585'
    )
;



--rollback SELECT NULL;