--liquibase formatted sql

--changeset postgres:delete_postgres_person_relations context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-935 Delete postgres person relations



-- delete from team_member
DELETE FROM
    tenant.team_member AS tmem
USING
    tenant.person AS prsn
WHERE
    tmem.person_id = prsn.id
    AND prsn.username = 'postgres'
;



-- revert changes
--rollback SELECT NULL;
