--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.germplasm



DELETE FROM
    experiment.planting_instruction
WHERE
    germplasm_id
IN
    (
        SELECT
            id 
        FROM 
            germplasm.germplasm 
        WHERE 
            designation 
        IN 
            (
                '((CML442/KS23-6)-B)@103-B',
                '((CML537/KS523-5)-B)@10-B',
                'CLRCY034',
                'CML444',
                'CML494',
                'CML536',
                'CLYN261',
                'CML463',
                'CML495',
                'CML204',
                'CML442',
                'CML539',
                'CML543',
                'CML547',
                'CML464'
            )
    )
;

DELETE  FROM
    germplasm.package_log
WHERE
    package_id
IN
    (
        SELECT 
            id 
        FROM
            germplasm.package
        WHERE
            seed_id
        IN
            (

                SELECT 
                    id 
                FROM 
                    germplasm.seed
                WHERE
                    germplasm_id 
                IN
                    (
                        SELECT
                            id 
                        FROM 
                            germplasm.germplasm 
                        WHERE 
                            designation 
                        IN 
                            (
                                '((CML442/KS23-6)-B)@103-B',
                                '((CML537/KS523-5)-B)@10-B',
                                'CLRCY034',
                                'CML444',
                                'CML494',
                                'CML536',
                                'CLYN261',
                                'CML463',
                                'CML495',
                                'CML204',
                                'CML442',
                                'CML539',
                                'CML543',
                                'CML547',
                                'CML464'
                            )
                    )
            )   
    )
;

DELETE FROM
    germplasm.package
WHERE
    seed_id
IN
    (

        SELECT 
            id 
        FROM 
            germplasm.seed
        WHERE
            germplasm_id 
        IN
            (
                SELECT
                    id 
                FROM 
                    germplasm.germplasm 
                WHERE 
                    designation 
                IN 
                    (
                        '((CML442/KS23-6)-B)@103-B',
                        '((CML537/KS523-5)-B)@10-B',
                        'CLRCY034',
                        'CML444',
                        'CML494',
                        'CML536',
                        'CLYN261',
                        'CML463',
                        'CML495',
                        'CML204',
                        'CML442',
                        'CML539',
                        'CML543',
                        'CML547',
                        'CML464'
                    )
            )
        )   
;

DELETE FROM
    germplasm.seed
WHERE
    germplasm_id
IN
    (
        SELECT
            id 
        FROM 
            germplasm.germplasm 
        WHERE 
            designation 
        IN 
            (
                '((CML442/KS23-6)-B)@103-B',
                '((CML537/KS523-5)-B)@10-B',
                'CLRCY034',
                'CML444',
                'CML494',
                'CML536',
                'CLYN261',
                'CML463',
                'CML495',
                'CML204',
                'CML442',
                'CML539',
                'CML543',
                'CML547',
                'CML464'
            )
    )
;

DELETE FROM 
    germplasm.germplasm 
WHERE 
    designation 
IN 
    (
        '((CML442/KS23-6)-B)@103-B',
        '((CML537/KS523-5)-B)@10-B',
        'CLRCY034',
        'CML444',
        'CML494',
        'CML536',
        'CLYN261',
        'CML463',
        'CML495',
        'CML204',
        'CML442',
        'CML539',
        'CML543',
        'CML547',
        'CML464'
    )
;



--rollback SELECT NULL;