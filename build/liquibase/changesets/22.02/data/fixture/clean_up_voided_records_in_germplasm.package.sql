--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_germplasm.package context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in germplasm.package



UPDATE 
    experiment.planting_instruction
SET
    package_id = NULL
WHERE
    package_id 
IN
    (
        SELECT 
            id
        FROM 
            germplasm.package 
        WHERE 
            id 
        IN 
            (
                '2271493',
                '2271499',
                '2271500',
                '2271501',
                '2271502',
                '2271503',
                '2271504',
                '2271505',
                '2271506',
                '2271509',
                '2271510',
                '2271511',
                '2271512',
                '2271513',
                '2271514'
            )
    )
;

DELETE FROM 
    germplasm.package 
WHERE 
    id 
IN 
    (
        '2271493',
        '2271499',
        '2271500',
        '2271501',
        '2271502',
        '2271503',
        '2271504',
        '2271505',
        '2271506',
        '2271509',
        '2271510',
        '2271511',
        '2271512',
        '2271513',
        '2271514'
    )
;



--rollback SELECT NULL;