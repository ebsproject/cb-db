--liquibase formatted sql

--changeset postgres:clean_up_voided_records_in_platform.application context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-242 Clean up voided records in platform.application



DELETE FROM 
    platform.application 
WHERE 
    id 
IN 
    (
        '4',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
        '24',
        '25',
        '27',
        '28',
        '29',
        '30',
        '31',
        '32',
        '33',
        '34',
        '35',
        '36',
        '37',
        '38',
        '39',
        '40',
        '41',
        '42',
        '43',
        '44',
        '45',
        '46',
        '47',
        '48',
        '52',
        '57'
    )
;



--rollback SELECT NULL;