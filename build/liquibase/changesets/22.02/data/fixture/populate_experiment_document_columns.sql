--liquibase formatted sql

--changeset postgres:populate_experiment_document_columns context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-938 Populate experiment document columns



UPDATE
    experiment.experiment
SET
    modification_timestamp = now()
WHERE
    is_void = FALSE
;

UPDATE
    experiment.occurrence
SET
    modification_timestamp = now()
WHERE
    is_void = FALSE
;

UPDATE
    experiment.location
SET
    modification_timestamp = now()
WHERE
    is_void = FALSE
;



-- revert changes
--rollback SELECT NULL;
