--liquibase formatted sql

--changeset postgres:add_indices_to_experiment_document_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-938 Add indices to experiment document columns



CREATE INDEX experiment_experiment_document_idx ON experiment.experiment USING gin (experiment_document);



-- revert changes
--rollback DROP INDEX experiment.experiment_experiment_document_idx;
