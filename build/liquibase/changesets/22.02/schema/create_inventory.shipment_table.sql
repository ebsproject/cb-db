--liquibase formatted sql

--changeset postgres:create_inventory.shipment_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create inventory.shipment table



-- create table
CREATE TABLE inventory.shipment (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    program_id integer NOT NULL,
    project_id integer,
    shipment_code varchar(128) NOT NULL DEFAULT inventory.generate_code('shipment'),
    shipment_name varchar(128) NOT NULL,
    shu_reference_number varchar(128),
    shipment_status varchar(128) NOT NULL DEFAULT 'draft',
    shipment_tracking_status varchar(64),
    shipment_transaction_type varchar(32) NOT NULL,
    shipment_type varchar(32) NOT NULL,
    shipment_purpose text,
    entity_id integer NOT NULL,
    material_type varchar(32) NOT NULL,
    material_subtype varchar(32),
    total_item_count integer NOT NULL DEFAULT 0,
    total_package_count integer NOT NULL DEFAULT 0,
    total_package_weight float NOT NULL DEFAULT 0,
    package_unit varchar(32) NOT NULL DEFAULT 'g',
    document_generated_date date,
    document_signed_date date,
    authorized_signatory varchar(64),
    shipped_date date,
    airway_bill_number varchar,
    received_date date,
    processor_name varchar(128) NOT NULL,
    processor_id integer,
    sender_name varchar(128) NOT NULL,
    sender_email varchar(128),
    sender_address varchar(256) NOT NULL,
    sender_institution varchar(256),
    sender_facility varchar(256),
    sender_country varchar(64) NOT NULL,
    sender_id integer,
    sender_address_id integer,
    sender_institution_id integer,
    sender_facility_id integer,
    sender_country_id integer,
    recipient_type varchar(128) NOT NULL,
    recipient_name varchar(128) NOT NULL,
    recipient_email varchar(128),
    recipient_address varchar(256) NOT NULL,
    recipient_institution varchar(256),
    recipient_facility varchar(256),
    recipient_country varchar(64) NOT NULL,
    recipient_id integer,
    recipient_address_id integer,
    recipient_institution_id integer,
    recipient_facility_id integer,
    recipient_country_id integer,
    requestor_name varchar(128) NOT NULL,
    requestor_email varchar(128),
    requestor_address varchar(256) NOT NULL,
    requestor_institution varchar(256),
    requestor_country varchar(64) NOT NULL,
    requestor_id integer,
    requestor_address_id integer,
    requestor_institution_id integer,
    requestor_country_id integer,
    shipment_remarks text,
    tracking_remarks text,
    dispatch_remarks text,
    acknowledgment_remarks text,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT shipment_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT shipment_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT shipment_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE inventory.shipment ADD CONSTRAINT shipment_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_program_id_fk ON inventory.shipment IS 'A shipment refers to exactly one program.';

ALTER TABLE inventory.shipment ADD CONSTRAINT shipment_project_id_fk FOREIGN KEY (project_id) REFERENCES tenant.project (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_project_id_fk ON inventory.shipment IS 'A shipment may refer to zero or one project.';

ALTER TABLE inventory.shipment ADD CONSTRAINT shipment_entity_id_fk FOREIGN KEY (entity_id) REFERENCES dictionary.entity (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_entity_id_fk ON inventory.shipment IS 'A shipment has exactly one entity.';

ALTER TABLE inventory.shipment ADD CONSTRAINT shipment_processor_id_fk FOREIGN KEY (processor_id) REFERENCES tenant.person (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_processor_id_fk ON inventory.shipment IS 'A shipment is processed by exactly one person.';

-- table comment
COMMENT ON TABLE inventory.shipment
    IS 'Shipment: Incoming or outgoing shipment transactions';

-- column comments
COMMENT ON COLUMN inventory.shipment.program_id IS 'Program ID: Reference to the program where the shipment was made [SHIPMENT_PROG_ID]';
COMMENT ON COLUMN inventory.shipment.project_id IS 'Project ID: Reference to the project where the shipment was made [SHIPMENT_PROJ_ID]';
COMMENT ON COLUMN inventory.shipment.shipment_code IS 'Shipment Code: Textual identifier of the shipment [SHIPMENT_CODE]';
COMMENT ON COLUMN inventory.shipment.shipment_name IS 'Shipment Name: Full name of the shipment provided by the user [SHIPMENT_NAME]';
COMMENT ON COLUMN inventory.shipment.shu_reference_number IS 'SHU Reference Number: Internal reference number provided by the Seed Health Unit [SHIPMENT_SHUREFNO]';
COMMENT ON COLUMN inventory.shipment.shipment_status IS 'Shipment Status: High-level status of the shipment in the workflow {draft, submitted, processing, on hold, ready, sent, received, cancelled} [SHIPMENT_STATUS]';
COMMENT ON COLUMN inventory.shipment.shipment_tracking_status IS 'Shipment Tracking Status: Low-level status related to service requirements associated with a shipment {submitted, processing, on hold, ready, sent, received, cancelled} [SHIPMENT_TRACKSTATUS]';
COMMENT ON COLUMN inventory.shipment.shipment_transaction_type IS 'Shipment Transaction Type: Transaction type of the shipment {incoming, outgoing} [SHIPMENT_TXNTYPE]';
COMMENT ON COLUMN inventory.shipment.shipment_type IS 'Shipment Type: Transaction type of the shipment {internal, external} [SHIPMENT_TYPE]';
COMMENT ON COLUMN inventory.shipment.shipment_purpose IS 'Shipment Purpose: Reason/purpose of making the shipment transaction [SHIPMENT_PURPOSE]';
COMMENT ON COLUMN inventory.shipment.entity_id IS 'Entity ID: Reference to the entity of the items in the shipment [SHIPMENT_ENT_ID]';
COMMENT ON COLUMN inventory.shipment.material_type IS 'Material Type: Type of the materials included in the shipment {propagative, non-propagative} [SHIPMENT_MATTYPE]';
COMMENT ON COLUMN inventory.shipment.material_subtype IS 'Material Sub-type: Sub-type of the materials included in the shipment, usually depending on the purpose of the shipment {seed, DNA, soil, NSBM, etc.} [SHIPMENT_MATSUBTYPE]';
COMMENT ON COLUMN inventory.shipment.total_item_count IS 'Total Item Count: Total number of unique items in the shipment [SHIPMENT_TOTALITEMCOUNT]';
COMMENT ON COLUMN inventory.shipment.total_package_count IS 'Total Package Count: Total number of packages across all shipment items [SHIPMENT_TOTALPKGCOUNT]';
COMMENT ON COLUMN inventory.shipment.total_package_weight IS 'Total Package Weight: Total weight of packages across all shipment items [SHIPMENT_TOTALPKGWEIGHT]';
COMMENT ON COLUMN inventory.shipment.package_unit IS 'Package Unit: Unit of weight/quantity used for the packages of shipment items [SHIPMENT_PKGUNIT]';
COMMENT ON COLUMN inventory.shipment.document_generated_date IS 'Document Generated Date: Date when the OMTA or other legal documents were generated [SHIPMENT_DOCGENDATE]';
COMMENT ON COLUMN inventory.shipment.document_signed_date IS 'Document Signed Date: Date when the OMTA or other legal documents were signed [SHIPMENT_DOCSIGNDATE]';
COMMENT ON COLUMN inventory.shipment.authorized_signatory IS 'Authorized Signatory: Name/s of the authorized signatory/ies in the legal documents [SHIPMENT_AUTHSIGN]';
COMMENT ON COLUMN inventory.shipment.shipped_date IS 'Shipment Date: Actual date when the shipment was shipped [SHIPMENT_SHIPDATE]';
COMMENT ON COLUMN inventory.shipment.airway_bill_number IS 'Airway Bill Number: Shipping label atrributed to the shipment [SHIPMENT_AIRWAYBILLNO]';
COMMENT ON COLUMN inventory.shipment.received_date IS 'Received Date: Actual date when the shipment was received [SHIPMENT_RECVDATE]';
COMMENT ON COLUMN inventory.shipment.processor_name IS 'Processor Name: Full name person from SHU who processed the shipment [SHIPMENT_PROC_NAME]';
COMMENT ON COLUMN inventory.shipment.processor_id IS 'Processor ID: Reference to the person from SHU who processed the shipment [SHIPMENT_PROC_ID]';
COMMENT ON COLUMN inventory.shipment.sender_name IS 'Sender Name: Full name of the sender [SHIPMENT_SENDER_NAME]';
COMMENT ON COLUMN inventory.shipment.sender_email IS 'Sender Email: Email address of the sender [SHIPMENT_SENDER_EMAIL]';
COMMENT ON COLUMN inventory.shipment.sender_address IS 'Sender Address: Physical address of the sender [SHIPMENT_SENDER_ADDR]';
COMMENT ON COLUMN inventory.shipment.sender_institution IS 'Sender Institution: Institution where the sender belongs to [SHIPMENT_SENDER_INST]';
COMMENT ON COLUMN inventory.shipment.sender_facility IS 'Sender Facility: Facility where the shipment was sent from [SHIPMENT_SENDER_FAC]';
COMMENT ON COLUMN inventory.shipment.sender_country IS 'Sender Country: Country where the shipment was sent from [SHIPMENT_SENDER_CNTRY]';
COMMENT ON COLUMN inventory.shipment.sender_id IS 'Sender ID: Reference to the sender of the shipment (linked to CRM) [SHIPMENT_SENDER_ID]';
COMMENT ON COLUMN inventory.shipment.sender_address_id IS 'Sender Address ID: Reference to the address of the sender (linked to CRM) [SHIPMENT_SENDADDR_ID]';
COMMENT ON COLUMN inventory.shipment.sender_institution_id IS 'Sender Insitution ID: Reference to the institution where the shipment was sent from (linked to CRM) [SHIPMENT_SENDINST_ID]';
COMMENT ON COLUMN inventory.shipment.sender_facility_id IS 'Sender Facility ID: Reference to the facility where the shipment was sent from (linked to CRM) [SHIPMENT_SENDFAC_ID]';
COMMENT ON COLUMN inventory.shipment.sender_country_id IS 'Sender Country ID: Reference to the country where the shipment was sent from (linked to CRM) [SHIPMENT_SENDCNTRY_ID]';
COMMENT ON COLUMN inventory.shipment.recipient_type IS 'Recipient Type: Type of recipient who/that will receive the shipment {individual, private company, international agricultural center, university, etc.} [SHIPMENT_RECIPTYPE]';
COMMENT ON COLUMN inventory.shipment.recipient_name IS 'Recipient Name: Full name of the recipient [SHIPMENT_RECIPIENT_NAME]';
COMMENT ON COLUMN inventory.shipment.recipient_email IS 'Recipient Email: Email address of the recipient [SHIPMENT_RECIPIENT_EMAIL]';
COMMENT ON COLUMN inventory.shipment.recipient_address IS 'Recipient Address: Physical address of the recipient [SHIPMENT_RECIPIENT_ADDR]';
COMMENT ON COLUMN inventory.shipment.recipient_institution IS 'Recipient Institution: Institution where the recipient belongs to [SHIPMENT_RECIPIENT_INST]';
COMMENT ON COLUMN inventory.shipment.recipient_facility IS 'Recipient Facility: Facility where the shipment will be sent to [SHIPMENT_RECIPIENT_FAC]';
COMMENT ON COLUMN inventory.shipment.recipient_country IS 'Recipient Country: Country where the shipment will be sent to [SHIPMENT_RECIPIENT_CNTRY]';
COMMENT ON COLUMN inventory.shipment.recipient_id IS 'Recipient ID: Reference to the recipient of the shipment (linked to CRM) [SHIPMENT_RECIPIENT_ID]';
COMMENT ON COLUMN inventory.shipment.recipient_address_id IS 'Recipient Address ID: Reference to the address of the recipient (linked to CRM) [SHIPMENT_RECIPADDR_ID]';
COMMENT ON COLUMN inventory.shipment.recipient_institution_id IS 'Recipient Insitution ID: Reference to the institution where the shipment will be sent to (linked to CRM) [SHIPMENT_RECIPINST_ID]';
COMMENT ON COLUMN inventory.shipment.recipient_facility_id IS 'Recipient Facility ID: Reference to the facility where the shipment will be sent to (linked to CRM) [SHIPMENT_RECIPFAC_ID]';
COMMENT ON COLUMN inventory.shipment.recipient_country_id IS 'Recipient Country ID: Reference to the country where the shipment will be sent to (linked to CRM) [SHIPMENT_RECIPCNTRY_ID]';
COMMENT ON COLUMN inventory.shipment.requestor_name IS 'Requestor Name: Full name of the requestor [SHIPMENT_REQUESTOR_NAME]';
COMMENT ON COLUMN inventory.shipment.requestor_email IS 'Requestor Email: Email address of the requestor [SHIPMENT_REQUESTOR_EMAIL]';
COMMENT ON COLUMN inventory.shipment.requestor_address IS 'Requestor Address: Physical address of the requestor [SHIPMENT_REQUESTOR_ADDR]';
COMMENT ON COLUMN inventory.shipment.requestor_institution IS 'Requestor Institution: Institution where the requestor belongs to [SHIPMENT_REQUESTOR_INST]';
COMMENT ON COLUMN inventory.shipment.requestor_country IS 'Requestor Country: Country where the shipment was requested from [SHIPMENT_REQUESTOR_CNTRY]';
COMMENT ON COLUMN inventory.shipment.requestor_id IS 'Requestor ID: Reference to the requestor of the shipment (linked to CRM) [SHIPMENT_REQUESTOR_ID]';
COMMENT ON COLUMN inventory.shipment.requestor_address_id IS 'Requestor Address ID: Reference to the address of the requestor (linked to CRM) [SHIPMENT_RECIPADDR_ID]';
COMMENT ON COLUMN inventory.shipment.requestor_institution_id IS 'Requestor Insitution ID: Reference to the institution where the shipment was requested from (linked to CRM) [SHIPMENT_RECIPINST_ID]';
COMMENT ON COLUMN inventory.shipment.requestor_country_id IS 'Requestor Country ID: Reference to the country where the shipment was requested from (linked to CRM) [SHIPMENT_RECIPCNTRY_ID]';
COMMENT ON COLUMN inventory.shipment.shipment_remarks IS 'Shipment Remarks: Remarks about the shipment in general [SHIPMENT_SHIPREMARKS]';
COMMENT ON COLUMN inventory.shipment.tracking_remarks IS 'Tracking Remarks: Remarks about the tracking status and related activities of the shipment [SHIPMENT_TRACKREMARKS]';
COMMENT ON COLUMN inventory.shipment.dispatch_remarks IS 'Dispatch Remarks: Remarks about the dispatch status and related activities of the shipment [SHIPMENT_DISPREMARKS]';
COMMENT ON COLUMN inventory.shipment.acknowledgment_remarks IS 'Acknowledgment Remarks: Remarks about the acknowledgment status and related activities of the shipment [SHIPMENT_ACKREMARKS]';

-- audit column comments
COMMENT ON COLUMN inventory.shipment.id
    IS 'Shipment ID: Database identifier of the record [SHIPMENT_ID]';
COMMENT ON COLUMN inventory.shipment.remarks
    IS 'Remarks: Additional notes to record [SHIPMENT_REMARKS]';
COMMENT ON COLUMN inventory.shipment.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [SHIPMENT_CTSTAMP]';
COMMENT ON COLUMN inventory.shipment.creator_id
    IS 'Creator ID: Reference to the person who created the record [SHIPMENT_CPERSON]';
COMMENT ON COLUMN inventory.shipment.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [SHIPMENT_MTSTAMP]';
COMMENT ON COLUMN inventory.shipment.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [SHIPMENT_MPERSON]';
COMMENT ON COLUMN inventory.shipment.notes
    IS 'Notes: Technical details about the record [SHIPMENT_NOTES]';
COMMENT ON COLUMN inventory.shipment.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [SHIPMENT_ISVOID]';
COMMENT ON COLUMN inventory.shipment.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [SHIPMENT_EVENTLOG]';

-- indices
CREATE INDEX shipment_program_id_idx ON inventory.shipment USING btree (program_id);
CREATE INDEX shipment_project_id_idx ON inventory.shipment USING btree (project_id);
CREATE INDEX shipment_shipment_code_idx ON inventory.shipment USING btree (shipment_code);
CREATE INDEX shipment_shipment_name_idx ON inventory.shipment USING btree (shipment_name);
CREATE INDEX shipment_shu_reference_number_idx ON inventory.shipment USING btree (shu_reference_number);
CREATE INDEX shipment_shipment_status_idx ON inventory.shipment USING btree (shipment_status);
CREATE INDEX shipment_shipment_tracking_status_idx ON inventory.shipment USING btree (shipment_tracking_status);
CREATE INDEX shipment_shipment_transaction_type_idx ON inventory.shipment USING btree (shipment_transaction_type);
CREATE INDEX shipment_shipment_type_idx ON inventory.shipment USING btree (shipment_type);
CREATE INDEX shipment_shipment_purpose_idx ON inventory.shipment USING btree (shipment_purpose);
CREATE INDEX shipment_entity_id_idx ON inventory.shipment USING btree (entity_id);
CREATE INDEX shipment_material_type_idx ON inventory.shipment USING btree (material_type);
CREATE INDEX shipment_material_subtype_idx ON inventory.shipment USING btree (material_subtype);
CREATE INDEX shipment_processor_id_idx ON inventory.shipment USING btree (processor_id);

-- audit indices
CREATE INDEX shipment_creator_id_idx ON inventory.shipment USING btree (creator_id);
CREATE INDEX shipment_modifier_id_idx ON inventory.shipment USING btree (modifier_id);
CREATE INDEX shipment_is_void_idx ON inventory.shipment USING btree (is_void);

-- triggers
CREATE TRIGGER shipment_event_log_tgr BEFORE INSERT OR UPDATE
    ON inventory.shipment FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE inventory.shipment;
