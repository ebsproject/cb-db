--liquibase formatted sql

--changeset postgres:create_inventory.shipment_file_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create inventory.shipment_file table



-- create table
CREATE TABLE inventory.shipment_file (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    shipment_id integer NOT NULL,
    file_name varchar(128) NOT NULL,
    file_type varchar(32) NOT NULL,
    file_size varchar(32) NOT NULL,
    file_path varchar(256) NOT NULL,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT shipment_file_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT shipment_file_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT shipment_file_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE inventory.shipment_file ADD CONSTRAINT shipment_file_shipment_id_fk FOREIGN KEY (shipment_id) REFERENCES inventory.shipment (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_file_shipment_id_fk ON inventory.shipment_file IS 'A file is attached to exactly one shipment.';

-- table comment
COMMENT ON TABLE inventory.shipment_file
    IS 'Shipment File: Support documents and files attached to shipments [SHIPFILE]';

-- column comments
COMMENT ON COLUMN inventory.shipment_file.id IS 'Shipment File ID: Database identifier of the shipment file [SHIPFILE_ID]';
COMMENT ON COLUMN inventory.shipment_file.shipment_id IS 'Shipment ID: Reference to the shipment where the file is attached [SHIPFILE_SHIPMENT_ID]';
COMMENT ON COLUMN inventory.shipment_file.file_name IS 'File Name: Full name of the file [SHIPFILE_NAME]';
COMMENT ON COLUMN inventory.shipment_file.file_type IS 'File Type: Type of the file [SHIPFILE_TYPE]';
COMMENT ON COLUMN inventory.shipment_file.file_size IS 'File Size: Size of the file [SHIPFILE_SIZE]';
COMMENT ON COLUMN inventory.shipment_file.file_path IS 'File Path: Path where the file is stored [SHIPFILE_PATH]';

-- audit column comments
COMMENT ON COLUMN inventory.shipment_file.id
    IS 'Shipment File ID: Database identifier of the record [SHIPFILE_ID]';
COMMENT ON COLUMN inventory.shipment_file.remarks
    IS 'Remarks: Additional notes to record [SHIPFILE_REMARKS]';
COMMENT ON COLUMN inventory.shipment_file.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [SHIPFILE_CTSTAMP]';
COMMENT ON COLUMN inventory.shipment_file.creator_id
    IS 'Creator ID: Reference to the person who created the record [SHIPFILE_CPERSON]';
COMMENT ON COLUMN inventory.shipment_file.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [SHIPFILE_MTSTAMP]';
COMMENT ON COLUMN inventory.shipment_file.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [SHIPFILE_MPERSON]';
COMMENT ON COLUMN inventory.shipment_file.notes
    IS 'Notes: Technical details about the record [SHIPFILE_NOTES]';
COMMENT ON COLUMN inventory.shipment_file.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [SHIPFILE_ISVOID]';
COMMENT ON COLUMN inventory.shipment_file.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [SHIPFILE_EVENTLOG]';

-- indices
CREATE INDEX shipment_file_shipment_id_idx ON inventory.shipment_file USING btree (shipment_id);

-- audit indices
CREATE INDEX shipment_file_creator_id_idx ON inventory.shipment_file USING btree (creator_id);
CREATE INDEX shipment_file_modifier_id_idx ON inventory.shipment_file USING btree (modifier_id);
CREATE INDEX shipment_file_is_void_idx ON inventory.shipment_file USING btree (is_void);

-- triggers
CREATE TRIGGER shipment_file_event_log_tgr BEFORE INSERT OR UPDATE
    ON inventory.shipment_file FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE inventory.shipment_file;
