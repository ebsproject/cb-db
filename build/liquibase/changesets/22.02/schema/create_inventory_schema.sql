--liquibase formatted sql

--changeset postgres:create_inventory_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create inventory schema



-- create schema
CREATE SCHEMA inventory;

COMMENT ON SCHEMA inventory IS 'Inventory for shipments and other transactions related to germplasm, seeds, and packages';



-- revert changes
--rollback DROP SCHEMA inventory;
