--liquibase formatted sql

--changeset postgres:enable_germplasm_document_triggers context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-912 Enable document triggers on germplasm tables



-- enable document triggers
ALTER TABLE germplasm.germplasm
    ENABLE TRIGGER germplasm_update_germplasm_document_tgr;

ALTER TABLE germplasm.germplasm
    ENABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;

ALTER TABLE germplasm.germplasm_name
    ENABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;

ALTER TABLE germplasm.package
    ENABLE TRIGGER package_update_package_document_tgr;

ALTER TABLE germplasm.seed
    ENABLE TRIGGER seed_update_package_document_from_seed_tgr;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm
--rollback     DISABLE TRIGGER germplasm_update_germplasm_document_tgr;
--rollback 
--rollback ALTER TABLE germplasm.germplasm
--rollback     DISABLE TRIGGER germplasm_update_package_document_from_germplasm_tgr;
--rollback 
--rollback ALTER TABLE germplasm.germplasm_name
--rollback     DISABLE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr;
--rollback 
--rollback ALTER TABLE germplasm.package
--rollback     DISABLE TRIGGER package_update_package_document_tgr;
--rollback 
--rollback ALTER TABLE germplasm.seed
--rollback     DISABLE TRIGGER seed_update_package_document_from_seed_tgr;
