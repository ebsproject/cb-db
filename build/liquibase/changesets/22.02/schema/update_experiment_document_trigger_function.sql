--liquibase formatted sql

--changeset postgres:update_experiment_document_trigger_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-938 Update experiment document trigger function



-- update function
CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_experiment()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

           SELECT
                concat(
                    setweight(to_tsvector(new.experiment_code),'A'),' ',
                    setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
                    setweight(to_tsvector(new.experiment_year::text),'B'),' ',
                    setweight(to_tsvector(new.experiment_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_design_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_status),'B'),' ',
                    setweight(to_tsvector(new.planting_season),'C'),' ',
                    setweight(to_tsvector(ts.season_code),'C'),' ',
                    setweight(to_tsvector(ts.season_name),'C'),' ',
                    setweight(to_tsvector(tst.stage_code),'C'),' ',
                    setweight(to_tsvector(tst.stage_name),'C'),' ',
                    setweight(to_tsvector(tp.project_code),'CD'),' ',
                    setweight(to_tsvector(tp.project_name),'CD'),' ',
                    setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
                    setweight(to_tsvector(unaccent(tps.person_name)),'CD')
                ) INTO var_document
            FROM
                experiment.experiment ex
            INNER JOIN
                tenant.season ts
            ON
                new.season_id = ts.id
            INNER JOIN
                tenant.stage tst
            ON
                new.stage_id = tst.id
            LEFT JOIN
                tenant.project tp
            ON
                new.project_id = tp.id
            INNER JOIN
                tenant.person tpr
            ON
                new.steward_id = tpr.id
            INNER JOIN
                tenant.person tps
            ON 
                new.creator_id = tps.id
            WHERE
                ex.id = new.id;
                
        ELSE
            
            SELECT
                concat(
                    setweight(to_tsvector(new.experiment_code),'A'),' ',
                    setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
                    setweight(to_tsvector(new.experiment_year::text),'B'),' ',
                    setweight(to_tsvector(new.experiment_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_design_type),'B'),' ',
                    setweight(to_tsvector(new.experiment_status),'B'),' ',
                    setweight(to_tsvector(new.planting_season),'C'),' ',
                    setweight(to_tsvector(ts.season_code),'C'),' ',
                    setweight(to_tsvector(ts.season_name),'C'),' ',
                    setweight(to_tsvector(tst.stage_code),'C'),' ',
                    setweight(to_tsvector(tst.stage_name),'C'),' ',
                    setweight(to_tsvector(tp.project_code),'CD'),' ',
                    setweight(to_tsvector(tp.project_name),'CD'),' ',
                    setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
                    setweight(to_tsvector(unaccent(tps.person_name)),'CD')
                ) INTO var_document
            FROM
                experiment.experiment ex
            INNER JOIN
                tenant.season ts
            ON
                new.season_id = ts.id
            INNER JOIN
                tenant.stage tst
            ON
                new.stage_id = tst.id
            LEFT JOIN
                tenant.project tp
            ON
                new.project_id = tp.id
            INNER JOIN
                tenant.person tpr
            ON
                new.steward_id = tpr.id
            INNER JOIN
                tenant.person tps
            ON 
                new.creator_id = tps.id;
            
        END IF;

        new.experiment_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION experiment.update_document_column_for_experiment_experiment()
--rollback  RETURNS trigger
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback 
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN
--rollback 
--rollback            SELECT
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.experiment_code),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
--rollback                     setweight(to_tsvector(new.experiment_year::text),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_design_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_status),'B'),' ',
--rollback                     setweight(to_tsvector(new.planting_season),'C'),' ',
--rollback                     setweight(to_tsvector(ts.season_code),'C'),' ',
--rollback                     setweight(to_tsvector(ts.season_name),'C'),' ',
--rollback                     setweight(to_tsvector(tst.stage_code),'C'),' ',
--rollback                     setweight(to_tsvector(tst.stage_name),'C'),' ',
--rollback                     setweight(to_tsvector(tp.project_code),'CD'),' ',
--rollback                     setweight(to_tsvector(tp.project_name),'CD'),' ',
--rollback                     setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
--rollback                     setweight(to_tsvector(unaccent(tps.person_name)),'CD')
--rollback                 ) INTO var_document
--rollback             FROM
--rollback                 experiment.experiment ex
--rollback             INNER JOIN
--rollback                 tenant.season ts
--rollback             ON
--rollback                 new.season_id = ts.id
--rollback             INNER JOIN
--rollback                 tenant.stage tst
--rollback             ON
--rollback                 new.stage_id = tst.id
--rollback             INNER JOIN
--rollback                 tenant.project tp
--rollback             ON
--rollback                 new.project_id = tp.id
--rollback             INNER JOIN
--rollback                 tenant.person tpr
--rollback             ON
--rollback                 new.steward_id = tpr.id
--rollback             INNER JOIN
--rollback                 tenant.person tps
--rollback             ON 
--rollback                 new.creator_id = tps.id
--rollback             WHERE
--rollback                 ex.id = new.id;
--rollback                 
--rollback         ELSE
--rollback             
--rollback             SELECT
--rollback                 concat(
--rollback                     setweight(to_tsvector(new.experiment_code),'A'),' ',
--rollback                     setweight(to_tsvector(unaccent(new.experiment_name)),'A'),' ',
--rollback                     setweight(to_tsvector(new.experiment_year::text),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_sub_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_sub_sub_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_design_type),'B'),' ',
--rollback                     setweight(to_tsvector(new.experiment_status),'B'),' ',
--rollback                     setweight(to_tsvector(new.planting_season),'C'),' ',
--rollback                     setweight(to_tsvector(ts.season_code),'C'),' ',
--rollback                     setweight(to_tsvector(ts.season_name),'C'),' ',
--rollback                     setweight(to_tsvector(tst.stage_code),'C'),' ',
--rollback                     setweight(to_tsvector(tst.stage_name),'C'),' ',
--rollback                     setweight(to_tsvector(tp.project_code),'CD'),' ',
--rollback                     setweight(to_tsvector(tp.project_name),'CD'),' ',
--rollback                     setweight(to_tsvector(unaccent(tpr.person_name)),'CD'),' ',
--rollback                     setweight(to_tsvector(unaccent(tps.person_name)),'CD')
--rollback                 ) INTO var_document
--rollback             FROM
--rollback                 experiment.experiment ex
--rollback             INNER JOIN
--rollback                 tenant.season ts
--rollback             ON
--rollback                 new.season_id = ts.id
--rollback             INNER JOIN
--rollback                 tenant.stage tst
--rollback             ON
--rollback                 new.stage_id = tst.id
--rollback             INNER JOIN
--rollback                 tenant.project tp
--rollback             ON
--rollback                 new.project_id = tp.id
--rollback             INNER JOIN
--rollback                 tenant.person tpr
--rollback             ON
--rollback                 new.steward_id = tpr.id
--rollback             INNER JOIN
--rollback                 tenant.person tps
--rollback             ON 
--rollback                 new.creator_id = tps.id;
--rollback             
--rollback         END IF;
--rollback 
--rollback         new.experiment_document = var_document;
--rollback         
--rollback     END IF;
--rollback     
--rollback     RETURN NEW;
--rollback END;
--rollback $function$
--rollback ;
