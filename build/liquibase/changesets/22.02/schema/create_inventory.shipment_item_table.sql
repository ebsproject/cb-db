--liquibase formatted sql

--changeset postgres:create_inventory.shipment_item_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create inventory.shipment_item table



-- create table
CREATE TABLE inventory.shipment_item (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    shipment_id integer NOT NULL,
    germplasm_id integer NOT NULL,
    seed_id integer NOT NULL,
    package_id integer,
    shipment_item_number integer NOT NULL,
    shipment_item_code varchar(128) NOT NULL DEFAULT inventory.generate_code('shipment_item'),
    shipment_item_status varchar(64) NOT NULL DEFAULT 'passed',
    shipment_item_weight integer NOT NULL,
    package_unit varchar(32) NOT NULL,
    package_count integer NOT NULL,
    test_code varchar(128),
    mta_status varchar(64),
    availability varchar(64),
    use varchar(64),
    smta_id varchar(256),
    mls_ancestors varchar,
    genetic_stock varchar,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT shipment_item_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT shipment_item_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT shipment_item_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE inventory.shipment_item ADD CONSTRAINT shipment_item_shipment_id_fk FOREIGN KEY (shipment_id) REFERENCES inventory.shipment (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_item_shipment_id_fk ON inventory.shipment_item IS 'A shipment item belongs to exactly one shipment.';

ALTER TABLE inventory.shipment_item ADD CONSTRAINT shipment_item_germplasm_id_fk FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_item_germplasm_id_fk ON inventory.shipment_item IS 'A shipment item refers to exactly one germplasm.';

ALTER TABLE inventory.shipment_item ADD CONSTRAINT shipment_item_seed_id_fk FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_item_seed_id_fk ON inventory.shipment_item IS 'A shipment item may refer to zero of exactly one seed.';

ALTER TABLE inventory.shipment_item ADD CONSTRAINT shipment_item_package_id_fk FOREIGN KEY (package_id) REFERENCES germplasm.package (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT shipment_item_package_id_fk ON inventory.shipment_item IS 'A shipment item may refer to zero of exactly one package.';

-- table comment
COMMENT ON TABLE inventory.shipment_item
    IS 'Shipment Item: List of items included in a shipment transaction [SHIPITEM]';

-- column comments
COMMENT ON COLUMN inventory.shipment_item.id IS 'Shipment Item ID: Database identifier of the shipment item [SHIPITEM_ID]';
COMMENT ON COLUMN inventory.shipment_item.shipment_id IS 'Shipment ID: Reference to the shipment where the item is included [SHIPITEM_SHIPMENT_ID]';
COMMENT ON COLUMN inventory.shipment_item.germplasm_id IS 'Germplasm ID: Reference to the germplasm of the shipment item [SHIPITEM_GE_ID]';
COMMENT ON COLUMN inventory.shipment_item.seed_id IS 'Seed ID: Reference to the seed of the shipment item [SHIPITEM_SEED_ID]';
COMMENT ON COLUMN inventory.shipment_item.package_id IS 'Package ID: Reference to the package of the shipment item [SHIPITEM_PKG_ID]';
COMMENT ON COLUMN inventory.shipment_item.shipment_item_number IS 'Item Number: Sequential number of the item within the shipment [SHIPITEM_ITEMNO]';
COMMENT ON COLUMN inventory.shipment_item.shipment_item_code IS 'Item Code: Textual identifier of the item within the shipment [SHIPITEM_ITEMCODE]';
COMMENT ON COLUMN inventory.shipment_item.shipment_item_status IS 'Item Status: Status of the shipment item after testing/checking [SHIPITEM_ITEMSTATUS]';
COMMENT ON COLUMN inventory.shipment_item.shipment_item_weight IS 'Item Weight: Weight of the shipment item with all its packages combined [SHIPITEM_ITEMWEIGHT]';
COMMENT ON COLUMN inventory.shipment_item.package_unit IS 'Package Unit: Unit of weight of the packages of the shipment item [SHIPITEM_PKGUNIT]';
COMMENT ON COLUMN inventory.shipment_item.package_count IS 'Package Count: Number of packages that the shipment item has [SHIPITEM_PKGCOUNT]';
COMMENT ON COLUMN inventory.shipment_item.test_code IS 'Test Code: Testing code/identifier given to the shipment item, which can be linked to external system [SHIPITEM_TESTCODE]';
COMMENT ON COLUMN inventory.shipment_item.mta_status IS 'MTA Status: MTA status of the shipment item at the time of the shipment [SHIPITEM_MTASTATUS]';
COMMENT ON COLUMN inventory.shipment_item.availability IS 'Availability: Availability of the shipment item at the time of the shipment [SHIPITEM_MTASTATUS]';
COMMENT ON COLUMN inventory.shipment_item.use IS 'Use: Use of the shipment item at the time of the shipment [SHIPITEM_MTASTATUS]';
COMMENT ON COLUMN inventory.shipment_item.mls_ancestors IS 'MLS Ancestors: MLS Ancestors of the shipment item at the time of the shipment [SHIPITEM_MTASTATUS]';
COMMENT ON COLUMN inventory.shipment_item.genetic_stock IS 'Genetic Stock: Genetic stock of the shipment item at the time of the shipment [SHIPITEM_MTASTATUS]';

-- audit column comments
COMMENT ON COLUMN inventory.shipment_item.id
    IS 'Shipment Item ID: Database identifier of the record [SHIPITEM_ID]';
COMMENT ON COLUMN inventory.shipment_item.remarks
    IS 'Remarks: Additional notes to record [SHIPITEM_REMARKS]';
COMMENT ON COLUMN inventory.shipment_item.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [SHIPITEM_CTSTAMP]';
COMMENT ON COLUMN inventory.shipment_item.creator_id
    IS 'Creator ID: Reference to the person who created the record [SHIPITEM_CPERSON]';
COMMENT ON COLUMN inventory.shipment_item.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [SHIPITEM_MTSTAMP]';
COMMENT ON COLUMN inventory.shipment_item.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [SHIPITEM_MPERSON]';
COMMENT ON COLUMN inventory.shipment_item.notes
    IS 'Notes: Technical details about the record [SHIPITEM_NOTES]';
COMMENT ON COLUMN inventory.shipment_item.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [SHIPITEM_ISVOID]';
COMMENT ON COLUMN inventory.shipment_item.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [SHIPITEM_EVENTLOG]';

-- indices
CREATE INDEX shipment_item_shipment_id_idx ON inventory.shipment_item USING btree (shipment_id);
CREATE INDEX shipment_item_germplasm_id_idx ON inventory.shipment_item USING btree (germplasm_id);
CREATE INDEX shipment_item_seed_id_idx ON inventory.shipment_item USING btree (seed_id);
CREATE INDEX shipment_item_package_id_idx ON inventory.shipment_item USING btree (package_id);
CREATE INDEX shipment_item_shipment_item_number_idx ON inventory.shipment_item USING btree (shipment_item_number);
CREATE INDEX shipment_item_shipment_item_code_idx ON inventory.shipment_item USING btree (shipment_item_code);
CREATE INDEX shipment_item_shipment_item_status_idx ON inventory.shipment_item USING btree (shipment_item_status);
CREATE INDEX shipment_item_test_code_idx ON inventory.shipment_item USING btree (test_code);
CREATE INDEX shipment_item_mta_status_idx ON inventory.shipment_item USING btree (mta_status);

-- audit indices
CREATE INDEX shipment_item_creator_id_idx ON inventory.shipment_item USING btree (creator_id);
CREATE INDEX shipment_item_modifier_id_idx ON inventory.shipment_item USING btree (modifier_id);
CREATE INDEX shipment_item_is_void_idx ON inventory.shipment_item USING btree (is_void);

-- triggers
CREATE TRIGGER shipment_item_event_log_tgr BEFORE INSERT OR UPDATE
    ON inventory.shipment_item FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE inventory.shipment_item;
