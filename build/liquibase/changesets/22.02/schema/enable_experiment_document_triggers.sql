--liquibase formatted sql

--changeset postgres:enable_experiment_document_triggers context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-938 Enable document triggers on experiment tables



-- enable document triggers
ALTER TABLE experiment.experiment
    ENABLE TRIGGER experiment_update_experiment_document_tgr;

ALTER TABLE experiment.location
    ENABLE TRIGGER location_update_location_document_tgr;

ALTER TABLE experiment.occurrence
    ENABLE TRIGGER occurrence_update_occurrence_document_tgr;



-- revert changes
--rollback ALTER TABLE experiment.experiment
--rollback     DISABLE TRIGGER experiment_update_experiment_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.occurrence
--rollback     DISABLE TRIGGER occurrence_update_occurrence_document_tgr;
--rollback 
--rollback ALTER TABLE experiment.location
--rollback     DISABLE TRIGGER location_update_location_document_tgr;
