--liquibase formatted sql

--changeset postgres:add_indices_to_germplasm_document_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-912 Add indices to germplasm document columns



CREATE INDEX germplasm_germplasm_document_idx ON germplasm.germplasm USING gin (germplasm_document);
CREATE INDEX germplasm_package_document_idx ON germplasm.package USING gin (package_document);



-- revert changes
--rollback DROP INDEX germplasm.germplasm_germplasm_document_idx;
--rollback DROP INDEX germplasm.germplasm_package_document_idx;
