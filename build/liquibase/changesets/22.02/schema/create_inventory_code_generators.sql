--liquibase formatted sql

--changeset postgres:create_shipment_code_sequences context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create sequences for shipments



-- create sequence for shipments
CREATE SEQUENCE inventory.shipment_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;

-- create sequence for shipment items
CREATE SEQUENCE inventory.shipment_item_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE inventory.shipment_code_seq;
--rollback DROP SEQUENCE inventory.shipment_item_code_seq;



--changeset postgres:create_inventory.generate_code_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create inventory.generate_code function



-- create function to generate code
CREATE OR REPLACE FUNCTION inventory.generate_code(IN entity varchar, IN custom_value varchar DEFAULT NULL, OUT code varchar)
    RETURNS varchar
    LANGUAGE plpgsql
AS $$
DECLARE
    seq_id bigint;
    entity_code varchar;
BEGIN
    SELECT nextval('inventory.' || entity || '_code_seq') INTO seq_id;
    
    IF (entity = 'shipment') THEN
        entity_code = 'SHIP';
    ELSIF (entity = 'shipment_item') THEN
        entity_code = 'SHIPITEM';
    END IF;
    
    IF (custom_value IS NULL) THEN
        code := entity_code || lpad(seq_id::varchar, 12, '0');
    ELSE
        code := entity_code || custom_value || lpad(seq_id::varchar, 12, '0');
    END IF;
END; $$
;



-- revert changes
--rollback DROP FUNCTION inventory.generate_code(IN varchar, IN varchar, OUT varchar);
