--liquibase formatted sql

--changeset postgres:add_variable_in_list_type_chk_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-889 Add variable in the list_type_chk constraint



ALTER TABLE platform.list DROP CONSTRAINT list_type_chk;

ALTER TABLE platform.list
    ADD CONSTRAINT list_type_chk CHECK (type::text = ANY (ARRAY['seed', 'plot', 'location', 'germplasm', 'study', 'trait', 'trait protocol', 'package', 'management protocol', 'sites', 'experiment', 'variable']));



--rollback ALTER TABLE platform.list DROP CONSTRAINT list_type_chk;

--rollback ALTER TABLE platform.list
--rollback  ADD CONSTRAINT list_type_chk CHECK (type::text = ANY (ARRAY['seed', 'plot', 'location', 'germplasm', 'study', 'trait', 'trait protocol', 'package', 'management protocol', 'sites', 'experiment']));