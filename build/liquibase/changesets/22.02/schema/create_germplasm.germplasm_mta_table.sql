--liquibase formatted sql

--changeset postgres:create_germplasm.germplasm_mta_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-820 Create germplasm.germplasm_mta table



-- create table
CREATE TABLE germplasm.germplasm_mta (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    germplasm_id integer NOT NULL,
    seed_id integer,
    mta_status varchar(64) NOT NULL,
    use varchar(64),
    mls_ancestors varchar,
    genetic_stock varchar,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT germplasm_mta_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT germplasm_mta_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT germplasm_mta_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE germplasm.germplasm_mta ADD CONSTRAINT germplasm_mta_germplasm_id_fk FOREIGN KEY (germplasm_id) REFERENCES germplasm.germplasm (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT germplasm_mta_germplasm_id_fk ON germplasm.germplasm_mta IS 'An MTA status refers to exactly one germplasm.';

ALTER TABLE germplasm.germplasm_mta ADD CONSTRAINT germplasm_mta_seed_id_fk FOREIGN KEY (seed_id) REFERENCES germplasm.seed (id) ON UPDATE CASCADE ON DELETE RESTRICT;
COMMENT ON CONSTRAINT germplasm_mta_seed_id_fk ON germplasm.germplasm_mta IS 'An MTA status may refer to zero or exactly one seed.';

-- table comment
COMMENT ON TABLE germplasm.germplasm_mta
    IS 'Germplasm MTA: Attributes pertaining to the MTA status of a germplasm [GEMTA]';

-- column comments
COMMENT ON COLUMN germplasm.germplasm_mta.id IS 'Germplasm MTA ID: Database identifier of the germplasm MTA data [GEMTA_ID]';
COMMENT ON COLUMN germplasm.germplasm_mta.germplasm_id IS 'Germplasm ID: Reference to the germplasm where the MTA status is attributed to [GEMTA_GE_ID]';
COMMENT ON COLUMN germplasm.germplasm_mta.seed_id IS 'Seed ID: Reference to the seed where the MTA status is attributed to [GEMTA_SEED_ID]';
COMMENT ON COLUMN germplasm.germplasm_mta.mta_status IS 'MTA Status: Contract that governs the transfer of tangible research materials like germplasm and seeds between two organizations when the recipient intends to use it for his or her own research purposes [GEMTA_MTASTATUS]';
COMMENT ON COLUMN germplasm.germplasm_mta.use IS 'Use: Sensitivity germplasm while and after development [GEMTA_USE]';
COMMENT ON COLUMN germplasm.germplasm_mta.mls_ancestors IS 'MLS Ancestors: Concatenated ancestors with MLS status required for certain types of MTA statuses/uses [GEMTA_MLSANCESTORS]';
COMMENT ON COLUMN germplasm.germplasm_mta.genetic_stock IS 'Genetic Stock: Genetic stock of the germplasm [GEMTA_GENSTOCK]';

-- audit column comments
COMMENT ON COLUMN germplasm.germplasm_mta.id
    IS 'Germplasm MTA ID: Database identifier of the record [GEMTA_ID]';
COMMENT ON COLUMN germplasm.germplasm_mta.remarks
    IS 'Remarks: Additional notes to record [GEMTA_REMARKS]';
COMMENT ON COLUMN germplasm.germplasm_mta.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [GEMTA_CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_mta.creator_id
    IS 'Creator ID: Reference to the person who created the record [GEMTA_CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_mta.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [GEMTA_MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_mta.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [GEMTA_MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_mta.notes
    IS 'Notes: Technical details about the record [GEMTA_NOTES]';
COMMENT ON COLUMN germplasm.germplasm_mta.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [GEMTA_ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_mta.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [GEMTA_EVENTLOG]';

-- indices
CREATE INDEX germplasm_mta_germplasm_id_idx ON germplasm.germplasm_mta USING btree (germplasm_id);
CREATE INDEX germplasm_mta_seed_id_idx ON germplasm.germplasm_mta USING btree (seed_id);
CREATE INDEX germplasm_mta_mta_status_idx ON germplasm.germplasm_mta USING btree (mta_status);
CREATE INDEX germplasm_mta_use_idx ON germplasm.germplasm_mta USING btree (use);

-- audit indices
CREATE INDEX germplasm_mta_creator_id_idx ON germplasm.germplasm_mta USING btree (creator_id);
CREATE INDEX germplasm_mta_modifier_id_idx ON germplasm.germplasm_mta USING btree (modifier_id);
CREATE INDEX germplasm_mta_is_void_idx ON germplasm.germplasm_mta USING btree (is_void);

-- triggers
CREATE TRIGGER germplasm_mta_event_log_tgr BEFORE INSERT OR UPDATE
    ON germplasm.germplasm_mta FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE germplasm.germplasm_mta;
