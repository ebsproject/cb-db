--liquibase formatted sql

--changeset postgres:SF9.1_experiment_entry_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.entry with initial Soybean fixture data



INSERT INTO experiment.entry
(entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,is_void,notes,package_id)
 VALUES 
(1,1,'SYB_fixed_0001','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007360' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007360' LIMIT 1)),
(2,2,'SYB_fixed_0002','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007361' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007361' LIMIT 1)),
(3,3,'SYB_fixed_0003','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007362' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007362' LIMIT 1)),
(8,8,'SYB_fixed_0008','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007367' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007367' LIMIT 1)),
(9,9,'SYB_fixed_0009','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007368' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007368' LIMIT 1)),
(10,10,'SYB_fixed_0010','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007369' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007369' LIMIT 1)),
(4,4,'SYB_fixed_0004','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007363' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007363' LIMIT 1)),
(5,5,'SYB_fixed_0005','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007364' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007364' LIMIT 1)),
(6,6,'SYB_fixed_0006','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007365' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007365' LIMIT 1)),
(7,7,'SYB_fixed_0007','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007366' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM germplasm.package WHERE notes ='SYB-PKG000000007366' LIMIT 1));



--rollback DELETE FROM experiment.entry
--rollback WHERE entry_name IN (
--rollback 'SYB_fixed_0001',
--rollback 'SYB_fixed_0002',
--rollback 'SYB_fixed_0003',
--rollback 'SYB_fixed_0008',
--rollback 'SYB_fixed_0009',
--rollback 'SYB_fixed_0010',
--rollback 'SYB_fixed_0004',
--rollback 'SYB_fixed_0005',
--rollback 'SYB_fixed_0006',
--rollback 'SYB_fixed_0007'
--rollback );