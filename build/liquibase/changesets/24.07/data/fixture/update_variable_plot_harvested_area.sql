--liquibase formatted sql

--changeset postgres:update_variable_plot_harvested_area context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2602	CB-DB: Update PLOT_AREA_HARVESTED usage from 'occurrence' to 'management'

-- update usage of PLOT_AREA_HARVESTED
UPDATE
    master.variable 
SET
    usage = 'management' 
WHERE
    id = (SELECT id FROM master.variable WHERE abbrev = 'PLOT_AREA_HARVESTED')
;



-- revert changes
--rollback UPDATE
--rollback     master.variable 
--rollback SET
--rollback     usage = 'occurrence' 
--rollback WHERE
--rollback     id = (SELECT id FROM master.variable WHERE abbrev = 'PLOT_AREA_HARVESTED')
--rollback ;