--liquibase formatted sql

--changeset postgres:update_IRSEA_2016_packages_mta_value context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2484 update packages with varied MTA_value


UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'FAO'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 0 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'IRRI'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 373 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'MLS'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 746 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'Prohib'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 1119 LIMIT 373
);


UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'PROHIBB'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 1492 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'PUD'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 1865 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'PUD1'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 2238 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'REL'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 2611 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'REST'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 2984 LIMIT 373
);

UPDATE germplasm.germplasm_mta
SET 
    mta_status = 'SMTA'
WHERE seed_id IN (
SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
) order by id desc OFFSET 3357 LIMIT 373
);


--rollback
--rollback UPDATE germplasm.germplasm_mta
--rollback SET 
--rollback     mta_status = 'PUD2'
--rollback WHERE seed_id IN (
--rollback SELECT id FROM germplasm.seed WHERE source_experiment_id IN (
--rollback SELECT id FROM experiment.experiment e WHERE experiment_year=2016 AND program_id IN (SELECT id FROM tenant.program WHERE program_code='IRSEA')
--rollback )
--rollback );
--rollback
--rollback UPDATE germplasm.germplasm_mta
--rollback SET 
--rollback     mta_status = 'SMTA'
--rollback WHERE seed_id IN (SELECT id from germplasm.seed WHERE seed_name = '300266703') AND notes = 'DEVOPS-1958 Populate mta_status and rsht';
