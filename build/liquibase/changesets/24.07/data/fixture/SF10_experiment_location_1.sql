--liquibase formatted sql

--changeset postgres:SF10_experiment_location_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.location with Soybean fixture data



INSERT INTO experiment.location
(location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,is_void,notes,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
 VALUES 
('SYB-IRRIHQ-2018-WS-001','SYB-IRRIHQ-2018-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='SYB-IRRIHQ-2018-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,2018,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,0,25),
('SYB-IRRIHQ-2024-DS-001','SYB-IRRIHQ-2024-DS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='SYB-IRRIHQ-2024-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,0,10),
('SYB-IRRIHQ-2019-DS-001','SYB-IRRIHQ-2019-DS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='SYB-IRRIHQ-2019-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,2019,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,0,25),
('SYB-IRRIHQ-2019-WS-001','SYB-IRRIHQ-2019-WS-001','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='SYB-IRRIHQ-2019-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,2019,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,0,125);



--rollback DELETE FROM experiment.location
--rollback WHERE location_code IN (
--rollback 'SYB-IRRIHQ-2018-WS-001',
--rollback 'SYB-IRRIHQ-2024-DS-001',
--rollback 'SYB-IRRIHQ-2019-DS-001',
--rollback 'SYB-IRRIHQ-2019-WS-001'
--rollback );