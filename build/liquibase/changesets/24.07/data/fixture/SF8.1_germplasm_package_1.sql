--liquibase formatted sql

--changeset postgres:SF8.1_germplasm_package_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate germplasm.package with initial Soybean fixture data



INSERT INTO germplasm.package
(package_code,package_label,package_quantity,package_unit,package_status,seed_id,program_id,geospatial_object_id,facility_id,creator_id,is_void,notes,package_reserved)
 VALUES 
((germplasm.generate_code('package')),'SYB_fixed_0002_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007361' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007361',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0003_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007362' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007362',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0008_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007367' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007367',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0001_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007360' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007360',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0010_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007369' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007369',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0005_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007364' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007364',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0009_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007368' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007368',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0007_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007366' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007366',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0004_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007363' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007363',100.0),
((germplasm.generate_code('package')),'SYB_fixed_0006_PACKAGE-001',485.0,'g','active',(SELECT id FROM germplasm.seed WHERE notes ='SYB-SEED000000007365' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-PKG000000007365',100.0);



--rollback DELETE FROM germplasm.package
--rollback WHERE package_label IN (
--rollback 'SYB_fixed_0002_PACKAGE-001',
--rollback 'SYB_fixed_0003_PACKAGE-001',
--rollback 'SYB_fixed_0008_PACKAGE-001',
--rollback 'SYB_fixed_0001_PACKAGE-001',
--rollback 'SYB_fixed_0010_PACKAGE-001',
--rollback 'SYB_fixed_0005_PACKAGE-001',
--rollback 'SYB_fixed_0009_PACKAGE-001',
--rollback 'SYB_fixed_0007_PACKAGE-001',
--rollback 'SYB_fixed_0004_PACKAGE-001',
--rollback 'SYB_fixed_0006_PACKAGE-001'
--rollback );