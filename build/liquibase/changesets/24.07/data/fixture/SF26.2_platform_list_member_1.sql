--liquibase formatted sql

--changeset postgres:SF26.2_platform_list_member_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate platform.list_member with Soybean fixture data (Trait_Protocol)



INSERT INTO platform.list_member
(list_id,data_id,order_number,display_value,remarks,creator_id,is_void,record_uuid,notes,is_active)
 VALUES 
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-F1-2018-WS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'HVDATE_CONT' LIMIT 1),1,'HARVEST DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'76fff432-ab63-400f-ae7b-bff74df5ab7b','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-F4-2020-DS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'HVDATE_CONT' LIMIT 1),1,'HARVEST DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'2a09de08-3120-4cef-91ea-ad41b0477aca','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-HB-2018-DS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'FLW_DATE_CONT' LIMIT 1),1,'FLW DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'c0be21bb-e92a-4986-897e-f0e64bcf7214','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-HB-2018-DS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'DATE_CROSSED' LIMIT 1),2,'CROSSING DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'2304e4a6-0e89-49df-a42e-0a3d893828ec','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-HB-2018-DS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'HV_METH_DISC' LIMIT 1),3,'HARV METH',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'76313d7b-1bc4-4ca7-b688-658eb7dacf30','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-F2-2019-DS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'HVDATE_CONT' LIMIT 1),1,'HARVEST DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'7bdbc1b3-984e-4766-880e-d2b1813a768e','Inserted via changeset BDS-2008',True),
((SELECT id FROM platform.list WHERE abbrev = (SELECT 'TRAIT_PROTOCOL_' || occurrence_code FROM experiment.occurrence WHERE occurrence_name = 'SYB-F3-2019-WS-001-001') LIMIT 1),(SELECT id FROM master.variable WHERE abbrev = 'HVDATE_CONT' LIMIT 1),1,'HARVEST DATE',NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'a2a58714-8512-4757-ba17-82410ba1caa9','Inserted via changeset BDS-2008',True);



--rollback DELETE FROM platform.list_member
--rollback WHERE notes = 'Inserted via changeset BDS-2008';