--liquibase formatted sql

--changeset postgres:SF6_experiment_occurrence_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.occurrence with Soybean fixture data



INSERT INTO experiment.occurrence
(occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,is_void,notes,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
 VALUES 
((experiment.generate_code('occurrence')),'SYB-F1-2018-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'SYB-F2-2019-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,25,25),
((experiment.generate_code('occurrence')),'SYB-F3-2019-WS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,125,125),
((experiment.generate_code('occurrence')),'SYB-F4-2020-DS-001-001','created;packed',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,221,221),
((experiment.generate_code('occurrence')),'SYB-HB-2018-DS-001-001','planted;trait data collected',NULL,(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),NULL,1,NULL,10,10);



--rollback DELETE FROM experiment.occurrence
--rollback WHERE occurrence_name IN (
--rollback 'SYB-F1-2018-WS-001-001',
--rollback 'SYB-F2-2019-DS-001-001',
--rollback 'SYB-F3-2019-WS-001-001',
--rollback 'SYB-F4-2020-DS-001-001',
--rollback 'SYB-HB-2018-DS-001-001'
--rollback );