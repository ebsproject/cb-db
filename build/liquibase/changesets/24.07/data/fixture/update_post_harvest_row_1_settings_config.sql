--liquibase formatted sql

--changeset postgres:update_post_harvest_row_1_settings_config context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2261: CB-DC: Update post_harvest_row_1_settings_config from BW to BW-CIMMYT



UPDATE 
    platform.config
SET
	abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS',
    name = 'DC Program BW-CIMMYT Post Harvest Row 1 Settings',
    config_value = $${
        "Name": "Program BW-CIMMYT configuration for the first row in the post-harvest data collection page",
        "Values": [
            {  
                "target_value": "programCode",
                "allow_new_val": false,
                "target_column": "programDbId",
                "variable_type": "metadata",
                "variable_abbrev": "PROGRAM",
                "api_resource_sort": "sort=programCode",
                "api_resource_method": "GET",
                "api_resource_endpoint": "programs",
                "disabled": false,
                "required": "required",
                "default": ""
            }
        ]
    }$$
WHERE
	abbrev = 'DC_PROGRAM_BW_POST_HARVEST_ROW_1_SETTINGS'
;

--rollback UPDATE 
--rollback    platform.config
--rollback SET
--rollback  abbrev='DC_PROGRAM_BW_POST_HARVEST_ROW_1_SETTINGS',
--rollback  name='DC Program BW Post Harvest Row 1 Settings',
--rollback  "config_value" = $${
--rollback        "Name": "Program BW configuration for the first row in the post-harvest data collection page",
--rollback        "Values": [
--rollback            {  
--rollback                "target_value": "programCode",
--rollback                "allow_new_val": false,
--rollback                "target_column": "programDbId",
--rollback                "variable_type": "metadata",
--rollback                "variable_abbrev": "PROGRAM",
--rollback                "api_resource_sort": "sort=programCode",
--rollback                "api_resource_method": "GET",
--rollback                "api_resource_endpoint": "programs",
--rollback                "disabled": false,
--rollback                "required": "required",
--rollback                "default": ""
--rollback            }
--rollback        ]
--rollback    }$$
--rollback  WHERE
--rollback  abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_1_SETTINGS';



