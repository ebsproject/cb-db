--liquibase formatted sql

--changeset postgres:SF24_experiment_experiment_block_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.experiment_block with Soybean fixture data



INSERT INTO experiment.experiment_block
(experiment_block_code,experiment_block_name,experiment_id,parent_experiment_block_id,order_number,block_type,no_of_blocks,no_of_ranges,no_of_cols,no_of_reps,plot_numbering_order,starting_corner,creator_id,is_void,notes)
 VALUES 
('VOIDED-324-324-35','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('SYB-F1-2018-WS-001|EO-1','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('SYB-F2-2019-DS-001|EO-1','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-357-357-68','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('SYB-F3-2019-WS-001|EO-1','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-390-390-101','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('VOIDED-423-423-134','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL),
('SYB-F4-2020-DS-001|EO-1','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('SYB-HB-2018-DS-001|EO-1','Entry order block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,0.0,0.0,NULL,'Column serpentine','Top Left',(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'[]'),
('VOIDED-291-291-2','Nursery block 1',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),NULL,1,'parent',0,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),True,NULL);



--rollback DELETE FROM experiment.experiment_block
--rollback WHERE experiment_id IN (SELECT id FROM experiment.experiment WHERE experiment_name IN (
--rollback 'SYB-F1-2018-WS-001',
--rollback 'SYB-F2-2019-DS-001',
--rollback 'SYB-F3-2019-WS-001',
--rollback 'SYB-F4-2020-DS-001',
--rollback 'SYB-HB-2024-DS-001',
--rollback 'SYB-HB-2018-DS-001'
--rollback ));