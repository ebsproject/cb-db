--liquibase formatted sql

--changeset postgres:SF5_place_geospatial_object_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate place.geospatial_object with Soybean fixture data



INSERT INTO place.geospatial_object
(geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,parent_geospatial_object_id,root_geospatial_object_id,creator_id,is_void,notes,coordinates)
 VALUES 
('SYB-IRRIHQ-2024-DS-001','SYB-IRRIHQ-2024-DS-001','planting area','breeding location',NULL,NULL,'For Soybean Fixture Data',(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2018-WS-001','SYB-IRRIHQ-2018-WS-001','planting area','breeding location',NULL,NULL,'For Soybean Fixture Data',(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2019-DS-001','SYB-IRRIHQ-2019-DS-001','planting area','breeding location',NULL,NULL,'For Soybean Fixture Data',(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL),
('SYB-IRRIHQ-2019-WS-001','SYB-IRRIHQ-2019-WS-001','planting area','breeding location',NULL,NULL,'For Soybean Fixture Data',(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='PH' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,NULL);



--rollback DELETE FROM place.geospatial_object
--rollback WHERE geospatial_object_code IN (
--rollback 'SYB-IRRIHQ-2024-DS-001',
--rollback 'SYB-IRRIHQ-2018-WS-001',
--rollback 'SYB-IRRIHQ-2019-DS-001',
--rollback 'SYB-IRRIHQ-2019-WS-001'
--rollback );