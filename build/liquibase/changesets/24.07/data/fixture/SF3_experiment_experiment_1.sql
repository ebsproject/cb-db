--liquibase formatted sql

--changeset postgres:SF3_experiment_experiment_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.experiment with Soybean fixture data



INSERT INTO experiment.experiment
(program_id,pipeline_id,stage_id,project_id,experiment_year,season_id,planting_season,experiment_code,experiment_name,experiment_objective,experiment_type,experiment_sub_type,experiment_sub_sub_type,experiment_design_type,experiment_status,description,steward_id,creator_id,is_void,notes,data_process_id,crop_id,remarks)
 VALUES 
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='F1' LIMIT 1),NULL,2018,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-F1-2018-WS-001',NULL,'Generation Nursery',NULL,NULL,'Entry Order','planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='GENERATION_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL),
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='F2' LIMIT 1),NULL,2019,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-F2-2019-DS-001',NULL,'Generation Nursery',NULL,NULL,'Entry Order','planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='GENERATION_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL),
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='F3' LIMIT 1),NULL,2019,(SELECT id FROM tenant.season WHERE season_code ='WS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-F3-2019-WS-001',NULL,'Generation Nursery',NULL,NULL,'Entry Order','planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='GENERATION_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL),
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='F4' LIMIT 1),NULL,2020,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-F4-2020-DS-001',NULL,'Generation Nursery',NULL,NULL,'Entry Order','created',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='GENERATION_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL),
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='HB' LIMIT 1),NULL,2024,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-HB-2024-DS-001',NULL,'Intentional Crossing Nursery',NULL,'Crossing Block','Systematic Arrangement','entry list created',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL),
((SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,(SELECT id FROM tenant.stage WHERE stage_code ='HB' LIMIT 1),NULL,2018,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),NULL,(experiment.generate_code('experiment')),'SYB-HB-2018-DS-001',NULL,'Intentional Crossing Nursery',NULL,'Crossing Block','Entry Order','planted',NULL,(SELECT id FROM tenant.person WHERE username ='admin' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,(SELECT id FROM master.item WHERE abbrev ='INTENTIONAL_CROSSING_NURSERY_DATA_PROCESS' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),NULL);



--rollback DELETE FROM experiment.experiment
--rollback WHERE experiment_name IN (
--rollback 'SYB-F1-2018-WS-001',
--rollback 'SYB-F2-2019-DS-001',
--rollback 'SYB-F3-2019-WS-001',
--rollback 'SYB-F4-2020-DS-001',
--rollback 'SYB-HB-2024-DS-001',
--rollback 'SYB-HB-2018-DS-001'
--rollback );