--liquibase formatted sql

--changeset postgres:SF7.1_germplasm_seed_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate germplasm.seed with initial Soybean fixture data



INSERT INTO germplasm.seed
(seed_code,seed_name,harvest_date,harvest_method,germplasm_id,program_id,source_experiment_id,source_entry_id,source_occurrence_id,source_location_id,source_plot_id,creator_id,is_void,notes,cross_id,harvest_source)
 VALUES 
((germplasm.generate_code('seed')),'SYB_fixed_0003_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0003' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007362',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0009_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0009' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007368',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0008_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0008' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007367',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0002_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0002' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007361',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0004_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0004' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007363',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0007_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0007' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007366',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0005_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0005' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007364',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0006_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0006' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007365',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0001_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007360',NULL,NULL),
((germplasm.generate_code('seed')),'301678462',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),NULL,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007410',NULL,NULL),
((germplasm.generate_code('seed')),'301678495',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),NULL,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007411',NULL,NULL),
((germplasm.generate_code('seed')),'301678496',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),NULL,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007412',NULL,NULL),
((germplasm.generate_code('seed')),'301678528',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0001' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),NULL,NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007413',NULL,NULL),
((germplasm.generate_code('seed')),'SYB_fixed_0010_SEED-001',NULL,NULL,(SELECT id FROM germplasm.germplasm WHERE designation='SYB_fixed_0010' AND crop_id IN (SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN') LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1),NULL,NULL,NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,'SYB-SEED000000007369',NULL,NULL);



--rollback DELETE FROM germplasm.seed
--rollback WHERE seed_name IN (
--rollback 'SYB_fixed_0003_SEED-001',
--rollback 'SYB_fixed_0009_SEED-001',
--rollback 'SYB_fixed_0008_SEED-001',
--rollback 'SYB_fixed_0002_SEED-001',
--rollback 'SYB_fixed_0004_SEED-001',
--rollback 'SYB_fixed_0007_SEED-001',
--rollback 'SYB_fixed_0005_SEED-001',
--rollback 'SYB_fixed_0006_SEED-001',
--rollback 'SYB_fixed_0001_SEED-001',
--rollback '301678462',
--rollback '301678495',
--rollback '301678496',
--rollback '301678528',
--rollback 'SYB_fixed_0010_SEED-001'
--rollback );