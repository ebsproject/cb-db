--liquibase formatted sql

--changeset postgres:SF22_experiment_entry_list_data_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.entry_list_data with Soybean fixture data


INSERT INTO platform.list 
(abbrev,name,display_name,type,entity_id,creator_id,notes,is_void,record_uuid,list_usage,status,is_active)
VALUES
((SELECT 'TRAIT_PROTOCOL_'||entry_list_code FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT entry_list_name||' Trait Protocol ('||entry_list_code||')' FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT entry_list_name||'Trait Protocol ('||entry_list_code||')' FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),'trait',(SELECT id FROM dictionary.entity WHERE abbrev = 'TRAIT'), (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1), 'Created via changesets BDS-2008',False,'fe5d9422-97f5-4a44-9ba7-4bc3df426c2d','working list','created',True);


INSERT INTO experiment.entry_list_data
(entry_list_id,variable_id,data_value,data_qc_code,protocol_id,remarks,creator_id,notes,is_void)
 VALUES 
((SELECT id FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID' LIMIT 1),(SELECT id FROM platform.list WHERE abbrev IN (SELECT 'TRAIT_PROTOCOL_'||entry_list_code FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1)),'G',NULL,NULL,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),NULL,False);


--rollback DELETE FROM platform.list
--rollback WHERE abbrev IN (SELECT 'TRAIT_PROTOCOL_'||entry_list_code FROM experiment.entry_list WHERE entry_list_name ='SYB-HB-2024-DS-001' AND entry_list_status='cross list specified' LIMIT 1);
--rollback DELETE FROM experiment.entry_list_data
--rollback WHERE entry_list_id IN (SELECT id FROM experiment.entry_list WHERE entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status='cross list specified');