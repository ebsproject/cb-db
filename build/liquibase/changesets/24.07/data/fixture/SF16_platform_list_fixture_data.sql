--liquibase formatted sql

--changeset postgres:SF16_platform_list_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate platform.list with Soybean fixture data



INSERT INTO platform.list 
    (abbrev, name, display_name, type, entity_id, creator_id, notes, list_sub_type)
SELECT
	'TRAIT_PROTOCOL_' || occur.occurrence_code AS abbrev,
	'TRAIT LIST FOR ' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) AS name,
	'TRAIT LIST FOR ' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) AS display_name,
	'trait' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'TRAIT') AS entity_id,
	(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-2009 insert)' notes,
	'trait protocol' AS list_sub_type
FROM 
	experiment.occurrence occur
WHERE
    occurrence_name IN (
        'SYB-F1-2018-WS-001-001',
        'SYB-F2-2019-DS-001-001',
        'SYB-F3-2019-WS-001-001',
        'SYB-F4-2020-DS-001-001',
        'SYB-HB-2018-DS-001-001'
    )
UNION
SELECT
	'MANAGEMENT_PROTOCOL_' || occur.occurrence_code AS abbrev,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS name,
	occur.occurrence_name|| ' Management Protocol (' || (SELECT experiment_code from experiment.experiment where id = occur.experiment_id) || ')' AS display_name,
	'variable' AS type,
	(SELECT id FROM "dictionary".entity WHERE abbrev = 'VARIABLE') AS entity_id,
	(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin') AS creator_id,
	'Inserted via liquibase changeset (BDS-2009 insert)' notes,
	'management protocol' AS list_sub_type
FROM 
	experiment.occurrence occur
WHERE
    occurrence_name IN (
        'SYB-F1-2018-WS-001-001',
        'SYB-F2-2019-DS-001-001',
        'SYB-F3-2019-WS-001-001',
        'SYB-F4-2020-DS-001-001',
        'SYB-HB-2018-DS-001-001'
    )



--rollback DELETE FROM platform.list
--rollback WHERE notes = 'Inserted via liquibase changeset (BDS-2009 insert)';