--liquibase formatted sql

--changeset postgres:SF4_experiment_entry_list_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.entry_list with Soybean fixture data



INSERT INTO experiment.entry_list
(entry_list_code,entry_list_name,description,entry_list_status,experiment_id,creator_id,is_void,notes,entry_list_type,cross_count,experiment_year,season_id,site_id,crop_id,program_id)
 VALUES 
((experiment.generate_code('entry_list')),'SYB-F1-2018-WS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F1-2018-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'SYB-F2-2019-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F2-2019-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'SYB-F3-2019-WS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F3-2019-WS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'SYB-F4-2020-DS-001',NULL,'completed',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-F4-2020-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'SYB-HB-2024-DS-001',NULL,'draft',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2024-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',0,NULL,NULL,NULL,(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1)),
((experiment.generate_code('entry_list')),'SYB-HB-2024-DS-001',NULL,'cross list specified',(SELECT id FROM experiment.experiment WHERE experiment_name ='SYB-HB-2018-DS-001' LIMIT 1),(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL,'entry list',25,2024.0,(SELECT id FROM tenant.season WHERE season_code ='DS' LIMIT 1),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code ='IRRIHQ' LIMIT 1),(SELECT id FROM tenant.crop WHERE crop_code='SOYBEAN' LIMIT 1),(SELECT id FROM tenant.program WHERE program_name ='Soybean Breeding Program' LIMIT 1));



--rollback DELETE FROM experiment.entry_list
--rollback WHERE entry_list_name IN (
--rollback 'SYB-F1-2018-WS-001',
--rollback 'SYB-F2-2019-DS-001',
--rollback 'SYB-F3-2019-WS-001',
--rollback 'SYB-F4-2020-DS-001',
--rollback 'SYB-HB-2024-DS-001',
--rollback 'SYB-HB-2024-DS-001'
--rollback );