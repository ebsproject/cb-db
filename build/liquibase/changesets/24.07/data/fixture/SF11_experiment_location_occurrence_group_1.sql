--liquibase formatted sql

--changeset postgres:SF11_experiment_location_occurrence_group_1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-2009 Populate experiment.location_occurrence_group with Soybean fixture data



INSERT INTO experiment.location_occurrence_group
(location_id,occurrence_id,order_number,creator_id,is_void,notes)
 VALUES 
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2024-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-HB-2018-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2019-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F3-2019-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2019-DS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F2-2019-DS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL),
((SELECT id FROM experiment.location WHERE location_name ='SYB-IRRIHQ-2018-WS-001' LIMIT 1),(SELECT id FROM experiment.occurrence WHERE occurrence_name ='SYB-F1-2018-WS-001-001' LIMIT 1),1,(SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),False,NULL);



--rollback DELETE FROM experiment.location_occurrence_group
--rollback WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN(
--rollback      'SYB-HB-2018-DS-001-001',
--rollback      'SYB-F3-2019-WS-001-001',
--rollback      'SYB-F2-2019-DS-001-001',
--rollback      'SYB-F1-2018-WS-001-001'
--rollback      )
--rollback );