--liquibase formatted sql

--changeset postgres:add_post_harvest_row_3_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2262 CB-DC: As an admin, I should be able to specify POST_HARVEST_ROW_3 config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS',
        'DC Global Post Harvest Row 3 Settings',
        '{
            "Name": "Default configuration for the third row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "AYLD_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "AYLD_CONT",
                    "api_resource_sort": "sort=AYLD_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""
                },
                {  
                    "target_value": "MC_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "MC_CONT",
                    "api_resource_sort": "sort=MC_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""   
                },
                {  
                    "target_value": "Notes",
                    "allow_new_val": true,
                    "target_column": "notes",
                    "variable_type": "metadata",
                    "variable_abbrev": "NOTES",
                    "api_resource_sort": "sort=notes",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "plots",
                    "disabled": false,
                    "required": "",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_3_SETTINGS',
        'DC Role Collaborator Post Harvest Row 3 Settings',
        '{
            "Name": "Role configuration for the third row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "AYLD_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "AYLD_CONT",
                    "api_resource_sort": "sort=AYLD_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""
                },
                {  
                    "target_value": "MC_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "MC_CONT",
                    "api_resource_sort": "sort=MC_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS',
        'DC Program IRSEA Post Harvest Row 3 Settings',
        '{
            "Name": "Program IRSEA configuration for the third row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "AYLD_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "AYLD_CONT",
                    "api_resource_sort": "sort=AYLD_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_KE_POST_HARVEST_ROW_3_SETTINGS',
        'DC Program KE Post Harvest Row 3 Settings',
        '{
            "Name": "Program KE configuration for the third row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "MC_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "MC_CONT",
                    "api_resource_sort": "sort=MC_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_3_SETTINGS',
        'DC Program BW-CIMMYT Post Harvest Row 3 Settings',
        '{
            "Name": "Program BW-CIMMYT configuration for the third row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "AYLD_CONT",
                    "allow_new_val": true,
                    "target_column": "value",
                    "variable_type": "observation",
                    "variable_abbrev": "AYLD_CONT",
                    "api_resource_sort": "sort=AYLD_CONT",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "terminal-transactions/{id}/dataset-table",
                    "disabled": false,
                    "required": "",
                    "default": ""
                },
                {  
                    "target_value": "Notes",
                    "allow_new_val": true,
                    "target_column": "notes",
                    "variable_type": "metadata",
                    "variable_abbrev": "NOTES",
                    "api_resource_sort": "sort=notes",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "plots",
                    "disabled": false,
                    "required": "",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_3_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_3_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_3_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_3_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_3_SETTINGS';