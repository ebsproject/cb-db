--liquibase formatted sql

--changeset postgres:add_post_harvest_view_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2263 CB-DC: As an admin, I should be able to specify POST_HARVEST_VIEW config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_POST_HARVEST_VIEW_SETTINGS',
        'DC Global Post Harvest View Settings',
        '{
            "headers" : [
                { 
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "display_name": "Occurrence Name"    
                },
                { 
                    "variable_abbrev": "GERMPLASM_NAME",
                    "display_name": "Germplasm Name"      
                },
                { 
                    "variable_abbrev": "PLOTNO",
                    "display_name": "Plot Number"     
                },
                { 
                    "variable_abbrev": "AYLD_CONT",
                    "display_name": "Actual Grain Yield In Grams"     
                },
                { 
                    "variable_abbrev": "MC_CONT",
                    "display_name": "Moisture Content %"     
                },
                { 
                    "variable_abbrev": "REMARKS",
                    "display_name": "Plot Remarks"
                }
            ],
            "numberOfPreviewItems" : 5
        }',
        'post-harvest'
    ),
    (
        'DC_ROLE_COLLABORATOR_POST_HARVEST_VIEW_SETTINGS',
        'DC Role Collaborator Post Harvest View Settings',
        '{
            "headers" : [
                { 
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "display_name": "Occurrence Name"    
                },
                { 
                    "variable_abbrev": "GERMPLASM_NAME",
                    "display_name": "Germplasm Name"     
                },
                { 
                    "variable_abbrev": "PLOTNO",
                    "display_name": "Plot Number"      
                },
                { 
                    "variable_abbrev": "AYLD_CONT",
                    "display_name": "Actual Grain Yield In Grams"  
                },
                { 
                    "variable_abbrev": "MC_CONT",
                    "display_name": "Moisture Content %"    
                }
            ],
            "numberOfPreviewItems" : 5
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_IRSEA_POST_HARVEST_VIEW_SETTINGS',
        'DC Program IRSEA Post Harvest View Settings',
        '{
            "headers" : [
                { 
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "display_name": "Occurrence Name"    
                },
                { 
                    "variable_abbrev": "GERMPLASM_NAME",
                    "display_name": "Germplasm Name"      
                },
                { 
                    "variable_abbrev": "PLOTNO",
                    "display_name": "Plot Number"    
                },
                { 
                    "variable_abbrev": "AYLD_CONT",
                    "display_name": "Actual Grain Yield In Grams"     
                }
            ],
            "numberOfPreviewItems" : 5
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_KE_POST_HARVEST_VIEW_SETTINGS',
        'DC Program KE Post Harvest View Settings',
        '{
            "headers" : [
                { 
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "display_name": "Occurrence Name"    
                },
                { 
                    "variable_abbrev": "GERMPLASM_NAME",
                    "display_name": "Germplasm Name"    
                },
                { 
                    "variable_abbrev": "PLOTNO",
                    "display_name": "Plot Number"    
                }
            ],
            "numberOfPreviewItems" : 5
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_VIEW_SETTINGS',
        'DC Program BW-CIMMYT Post Harvest View Settings',
        '{
            "headers" : [
                { 
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "display_name": "Occurrence Name"   
                },
                { 
                    "variable_abbrev": "GERMPLASM_NAME",
                    "display_name": "Germplasm Name"    
                }
            ],
            "numberOfPreviewItems" : 5
        }',
        'post-harvest'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_POST_HARVEST_VIEW_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_VIEW_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_VIEW_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_POST_HARVEST_VIEW_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_VIEW_SETTINGS';