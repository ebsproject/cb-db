--liquibase formatted sql

--changeset postgres:add_post_harvest_printouts_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2264 DC-DB: Specify POST_HARVEST_PRINTOUTS config for post-harvest printouts in Core System



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_POST_HARVEST_PRINTOUTS_SETTINGS',
        'DC Global Post Harvest Printouts Global Settings',
        '{
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-template"
                },
                {
                    "template_name": "SAMPLE-OCC-printout-role-collaborator-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-role-collaborator-template"
                },
                {
                    "template_name": "SAMPLE-OCC-printout-program-irsea-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-irsea-template"
                },
                {
                    "template_name": "SAMPLE-OCC-printout-program-ke-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-ke-template"
                },
                {
                    "template_name": "SAMPLE-OCC-printout-program-bw-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-bw-template"
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_ROLE_COLLABORATOR_POST_HARVEST_PRINTOUTS_SETTINGS',
        'DC Role Collaborator Post Harvest Printouts Settings',
        '{
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-role-collaborator-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-role-collaborator-template"
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_IRSEA_POST_HARVEST_PRINTOUTS_SETTINGS',
        'DC Program IRSEA Post Harvest Printouts Settings',
        '{
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-program-irsea-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-irsea-template"
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_KE_POST_HARVEST_PRINTOUTS_SETTINGS',
        'DC Program KE Post Harvest Printouts Settings',
        '{
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-program-ke-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-ke-template"
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_BW_POST_HARVEST_PRINTOUTS_SETTINGS',
        'DC Program BW Post Harvest Printouts Settings',
        '{
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-program-bw-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-bw-template"
                }
            ]
        }',
        'post-harvest'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_POST_HARVEST_PRINTOUTS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_PRINTOUTS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_PRINTOUTS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_POST_HARVEST_PRINTOUTS_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW_POST_HARVEST_PRINTOUTS_SETTINGS';