--liquibase formatted sql

--changeset postgres:add_scale_value_file_upload_action_coding context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev = 'FILE_UPLOAD_ACTION_CODING') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2425: CB-GM: Add FILE_UPLOAD_ACTION_CODING to FILE_UPLOAD_ACTION scale values



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'FILE_UPLOAD_ACTION'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES ('coding', 'Coding', 'Coding', 'CODING', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'FILE_UPLOAD_ACTION'
;



--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback      'FILE_UPLOAD_ACTION_CODING'
--rollback     )
--rollback ;