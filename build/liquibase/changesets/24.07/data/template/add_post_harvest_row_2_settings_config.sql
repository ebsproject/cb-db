--liquibase formatted sql

--changeset postgres:add_post_harvest_row_2_settings_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2261 CB-DC: As an admin, I should be able to specify POST_HARVEST_ROW_2 config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_POST_HARVEST_ROW_2_SETTINGS',
        'DC Global Post Harvest Row 2 Settings',
        '{
            "Name": "Default configuration for the second row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "Occurrence name",
                    "allow_new_val": true,
                    "target_column": "occurrence_name",
                    "variable_type": "metadata",
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "api_resource_sort": "sort=occurrenceName",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "occurrences-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "Germplasm Name",
                    "allow_new_val": true,
                    "target_column": "designation",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESIGNATION",
                    "api_resource_sort": "sort=designation",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "germplasm-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""    
                },
                {  
                    "target_value": "Entry no",
                    "allow_new_val": true,
                    "target_column": "entry_number",
                    "variable_type": "metadata",
                    "variable_abbrev": "ENTRY_NUMBER",
                    "api_resource_sort": "sort=entryNumber",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "entries-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                },
                {  
                    "target_value": "Plot_code",
                    "allow_new_val": true,
                    "target_column": "plot_code",
                    "variable_type": "metadata",
                    "variable_abbrev": "PLOT_CODE",
                    "api_resource_sort": "sort=plotCode",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "plots-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_2_SETTINGS',
        'DC Role Collaborator Post Harvest Row 2 Settings',
        '{
            "Name": "Role configuration for the second row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "Occurrence name",
                    "allow_new_val": true,
                    "target_column": "occurrence_name",
                    "variable_type": "metadata",
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "api_resource_sort": "sort=occurrenceName",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "occurrences-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "Germplasm Name",
                    "allow_new_val": true,
                    "target_column": "designation",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESIGNATION",
                    "api_resource_sort": "sort=designation",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "germplasm-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""    
                },
                {  
                    "target_value": "Entry no",
                    "allow_new_val": true,
                    "target_column": "entry_number",
                    "variable_type": "metadata",
                    "variable_abbrev": "ENTRY_NUMBER",
                    "api_resource_sort": "sort=entryNumber",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "entries-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_2_SETTINGS',
        'DC Program IRSEA Post Harvest Row 2 Settings',
        '{
            "Name": "Program IRSEA configuration for the second row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "Occurrence name",
                    "allow_new_val": true,
                    "target_column": "occurrence_name",
                    "variable_type": "metadata",
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "api_resource_sort": "sort=occurrenceName",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "occurrences-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "Germplasm Name",
                    "allow_new_val": true,
                    "target_column": "designation",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESIGNATION",
                    "api_resource_sort": "sort=designation",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "germplasm-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""    
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_KE_POST_HARVEST_ROW_2_SETTINGS',
        'DC Program KE Post Harvest Row 2 Settings',
        '{
            "Name": "Program KE configuration for the second row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "Occurrence name",
                    "allow_new_val": true,
                    "target_column": "occurrence_name",
                    "variable_type": "metadata",
                    "variable_abbrev": "OCCURRENCE_NAME",
                    "api_resource_sort": "sort=occurrenceName",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "occurrences-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_2_SETTINGS',
        'DC Program BW-CIMMYT Post Harvest Row 2 Settings',
        '{
            "Name": "Program BW-CIMMYT configuration for the second row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "Germplasm Name",
                    "allow_new_val": true,
                    "target_column": "designation",
                    "variable_type": "metadata",
                    "variable_abbrev": "DESIGNATION",
                    "api_resource_sort": "sort=designation",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "germplasm-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""    
                }
            ]
        }',
        'post-harvest'
    );
    
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_2_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_2_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_2_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_2_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_ROW_2_SETTINGS';