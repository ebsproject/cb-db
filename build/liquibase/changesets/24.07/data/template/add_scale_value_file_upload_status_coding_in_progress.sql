--liquibase formatted sql

--changeset postgres:add_scale_value_file_upload_status_coding_in_progress context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS_CODING_IN_PROGRESS') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2426 CB-GM: Add GERMPLASM_FILE_UPLOAD_STATUS_CODING_IN_PROGRESS to GERMPLASM_FILE_UPLOAD_STATUS scale values



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES 
            ('coding in progress', 'Coding In Progress', 'Coding In Progress', 'CODING_IN_PROGRESS', 'show'),
            ('coding failed', 'Coding Failed', 'Coding Failed', 'CODING_FAILED', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_FILE_UPLOAD_STATUS'
;



--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback      'GERMPLASM_FILE_UPLOAD_STATUS_CODING_IN_PROGRESS',
--rollback      'GERMPLASM_FILE_UPLOAD_STATUS_CODING_FAILED'
--rollback     )
--rollback ;