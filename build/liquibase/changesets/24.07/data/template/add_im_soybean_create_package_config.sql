--liquibase formatted sql

--changeset postgres:add_im_soybean_create_package_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'IM_CREATE_PACKAGE_SOYBEAN_DEFAULT') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-1800 CB-IM DB: Insert new config for SOYBEAN package-only creation



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'IM_CREATE_PACKAGE_SOYBEAN_DEFAULT',
        'Inventory Manager File Upload configuration for SOYBEAN variables - package only',
        $$
            {
                "values": [
                    {
                        "name": "Package Quantity",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "VOLUME",
                        "entity": "package",
                        "required": "true",
                        "api_field": "packageQuantity",
                        "data_type": "float",
                        "http_method": "",
                        "value_filter": "",
                        "skip_creation": "false",
                        "retrieve_db_id": "false",
                        "url_parameters": "",
                        "db_id_api_field": "",
                        "search_endpoint": "",
                        "additional_filters": {}
                    },
                    {
                        "name": "Package Unit",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "required",
                        "abbrev": "PACKAGE_UNIT",
                        "entity": "package",
                        "required": "true",
                        "api_field": "packageUnit",
                        "data_type": "string",
                        "http_method": "",
                        "value_filter": "",
                        "skip_creation": "false",
                        "retrieve_db_id": "false",
                        "url_parameters": "",
                        "db_id_api_field": "",
                        "search_endpoint": "",
                        "additional_filters": {}
                    },
                    {
                        "name": "Facility Code",
                        "type": "column",
                        "view": {
                            "visible": "true",
                            "entities": [
                                "package"
                            ]
                        },
                        "usage": "optional",
                        "abbrev": "FACILITY_CODE",
                        "entity": "package",
                        "required": "false",
                        "api_field": "facilityDbId",
                        "data_type": "string",
                        "http_method": "POST",
                        "value_filter": "facilityCode",
                        "skip_creation": "false",
                        "retrieve_db_id": "true",
                        "url_parameters": "limit=1",
                        "db_id_api_field": "facilityDbId",
                        "search_endpoint": "facilities-search",
                        "additional_filters": {}
                    }
                ]
            }
        $$,
        1,
        'Inventory Manager file upload and data validation',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-1800 CB-IM DB: Insert new config for SOYBEAN package-only creation - j.bantay'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='IM_CREATE_PACKAGE_SOYBEAN_DEFAULT';