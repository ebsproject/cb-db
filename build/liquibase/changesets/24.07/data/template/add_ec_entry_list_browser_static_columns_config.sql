--liquibase formatted sql

--changeset postgres:add_ec_entry_list_browser_static_columns_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'EC_ENTRY_LIST_BROWSER_STATIC_COLUMNS_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-1016 CB-EC: Add hard coded UI columns in Entry List to DB config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id
    )
VALUES
    (
        'EC_ENTRY_LIST_BROWSER_STATIC_COLUMNS_CONFIG',
        'Experiment Creation Entry List Tab browser static columns',
        $$
            {
                "values": [
                    {
                        "attribute": "entryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "title": "Entry No.",
                        "description": "Sequential numbering of the entries"
                    },
                    {
                        "attribute": "seedName",
                        "abbrev": "SEED_NAME",
                        "title": "Seed name",
                        "description": "Seed Name"
                    },
                    {
                        "attribute": "packageLabel",
                        "abbrev": "PACKAGE_LABEL",
                        "title": "Package Label",
                        "description": "Package Label"
                    },
                    {
                        "attribute": "designation",
                        "abbrev": "DESIGNATION",
                        "title": "Germplasm Name",
                        "description": "Gemplasm Designation"
                    },
                    {
                        "attribute": "germplasmCode",
                        "abbrev": "GERMPLASM_CODE",
                        "title": "Germplasm Code",
                        "description": "Gemplasm Designation"
                    },
                    {
                        "attribute": "parentage",
                        "abbrev": "PARENTAGE",
                        "title": "Parentage",
                        "description": "Pedigree Information"
                    },
                    {
                        "attribute": "generation",
                        "abbrev": "GENERATION",
                        "title": "Generation",
                        "description": "Generation"
                    },
                    {
                        "attribute": "germplasmState",
                        "abbrev": "GERMPLASM_STATE",
                        "title": "Germplasm State",
                        "description": "Germplasm State"
                    },
                    {
                        "attribute": "packageQuantity",
                        "abbrev": "PACKAGE_QUANTITY",
                        "title": "Package Quantity",
                        "description": "Package Quantity"
                    },
                    {
                        "attribute": "packageUnit",
                        "abbrev": "PACKAGE_UNIT",
                        "title": "Package Unit",
                        "description": "Package Unit"
                    }
                ]
            }
        $$,
        1,
        'experiment_creation',
        (
            SELECT
                id
            FROM
                tenant.person
            WHERE
                email = 'admin@ebsproject.org'
        )
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='EC_ENTRY_LIST_BROWSER_STATIC_COLUMNS_CONFIG';