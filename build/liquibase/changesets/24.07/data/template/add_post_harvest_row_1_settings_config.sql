--liquibase formatted sql

--changeset postgres:add_post_harvest_printouts_config context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2260 CB-DC: As an admin, I should be able to specify POST_HARVEST_ROW_1 config



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS',
        'DC Global Post Harvest Row 1 Settings',
        '{
            "Name": "Default configuration for the first row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "programCode",
                    "allow_new_val": false,
                    "target_column": "programDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "PROGRAM",
                    "api_resource_sort": "sort=programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "experimentYear",
                    "allow_new_val": false,
                    "target_column": "experimentYear",
                    "variable_type": "metadata",
                    "variable_abbrev": "Year",
                    "api_resource_sort": "sort=experimentYear",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "experiments-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "seasonCode",
                    "allow_new_val": false,
                    "target_column": "seasonDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "Season",
                    "api_resource_sort": "sort=seasonCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "seasons",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                },
                {  
                    "target_value": "Post_Harvest_Program",
                    "allow_new_val": true,
                    "target_column": "Post_Harvest_Program",
                    "variable_type": "metadata",
                    "variable_abbrev": "POST_HARVEST_PROGRAM",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "disabled": false,
                    "required": "",
                    "default": ""  
                },
                {  
                    "target_value": "Post Harvest completion date",
                    "allow_new_val": true,
                    "target_column": "Post Harvest completion date",
                    "variable_type": "metadata",
                    "variable_abbrev": "POST_HARVEST_COMPLETION_DATE",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "disabled": false,
                    "required": "",
                    "default": ""
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS',
        'DC Role Collaborator Post Harvest Row 1 Settings',
        '{
            "Name": "Role configuration for the first row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "programCode",
                    "allow_new_val": false,
                    "target_column": "programDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "PROGRAM",
                    "api_resource_sort": "sort=programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "experimentYear",
                    "allow_new_val": false,
                    "target_column": "experimentYear",
                    "variable_type": "metadata",
                    "variable_abbrev": "Year",
                    "api_resource_sort": "sort=experimentYear",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "experiments-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "seasonCode",
                    "allow_new_val": false,
                    "target_column": "seasonDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "Season",
                    "api_resource_sort": "sort=seasonCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "seasons",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                },
                {  
                    "target_value": "Post_Harvest_Program",
                    "allow_new_val": true,
                    "target_column": "Post_Harvest_Program",
                    "variable_type": "metadata",
                    "variable_abbrev": "POST_HARVEST_PROGRAM",
                    "api_resource_sort": "",
                    "api_resource_method": "",
                    "api_resource_endpoint": "",
                    "disabled": false,
                    "required": "",
                    "default": ""  
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS',
        'DC Program IRSEA Post Harvest Row 1 Settings',
        '{
            "Name": "Program IRSEA configuration for the first row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "programCode",
                    "allow_new_val": false,
                    "target_column": "programDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "PROGRAM",
                    "api_resource_sort": "sort=programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "experimentYear",
                    "allow_new_val": false,
                    "target_column": "experimentYear",
                    "variable_type": "metadata",
                    "variable_abbrev": "Year",
                    "api_resource_sort": "sort=experimentYear",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "experiments-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "seasonCode",
                    "allow_new_val": false,
                    "target_column": "seasonDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "Season",
                    "api_resource_sort": "sort=seasonCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "seasons",
                    "disabled": false,
                    "required": "required",
                    "default": ""   
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS',
        'DC Program KE Post Harvest Row 1 Settings',
        '{
            "Name": "Program KE configuration for the first row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "programCode",
                    "allow_new_val": false,
                    "target_column": "programDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "PROGRAM",
                    "api_resource_sort": "sort=programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                },
                {  
                    "target_value": "experimentYear",
                    "allow_new_val": false,
                    "target_column": "experimentYear",
                    "variable_type": "metadata",
                    "variable_abbrev": "Year",
                    "api_resource_sort": "sort=experimentYear",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "experiments-search",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                }
            ]
        }',
        'post-harvest'
    ),
    (
        'DC_PROGRAM_BW_POST_HARVEST_ROW_1_SETTINGS',
        'DC Program BW Post Harvest Row 1 Settings',
        '{
            "Name": "Program BW configuration for the first row in the post-harvest data collection page",
            "Values": [
                {  
                    "target_value": "programCode",
                    "allow_new_val": false,
                    "target_column": "programDbId",
                    "variable_type": "metadata",
                    "variable_abbrev": "PROGRAM",
                    "api_resource_sort": "sort=programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "disabled": false,
                    "required": "required",
                    "default": ""
                }
            ]
        }',
        'post-harvest'
    );

--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_POST_HARVEST_ROW_1_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_POST_HARVEST_ROW_1_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_POST_HARVEST_ROW_1_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_KE_POST_HARVEST_ROW_1_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_BW_POST_HARVEST_ROW_1_SETTINGS';