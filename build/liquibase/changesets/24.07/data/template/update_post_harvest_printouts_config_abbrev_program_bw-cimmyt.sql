--liquibase formatted sql

--changeset postgres:update_post_harvest_printouts_config_abbrev_program_bw-cimmyt context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-2264 DC-DB: Specify POST_HARVEST_PRINTOUTS config for post-harvest printouts in Core System



UPDATE
    platform.config
SET
    abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_PRINTOUTS_SETTINGS',
    name = 'DC Program BW-CIMMYT Post Harvest Printouts Settings',
    config_value = $${
            "templates": [
                {
                    "template_name": "SAMPLE-OCC-printout-program-bw-cimmyt-template",
                    "button_name": "PRINT SAMPLE-OCC-printout-program-bw-cimmyt-template"
                }
            ]
        }$$
WHERE
    abbrev = 'DC_PROGRAM_BW_POST_HARVEST_PRINTOUTS_SETTINGS'



--rollback UPDATE platform.config SET
--rollback  abbrev = 'DC_PROGRAM_BW_POST_HARVEST_PRINTOUTS_SETTINGS',
--rollback  name = 'DC Program BW Post Harvest Printouts Settings',
--rollback  config_value = $${
--rollback          "templates": [
--rollback              {
--rollback                  "template_name": "SAMPLE-OCC-printout-program-bw-template",
--rollback                  "button_name": "PRINT SAMPLE-OCC-printout-program-bw-template"
--rollback              }
--rollback          ]
--rollback  }$$
--rollback WHERE abbrev = 'DC_PROGRAM_BW-CIMMYT_POST_HARVEST_PRINTOUTS_SETTINGS';