--liquibase formatted sql

--changeset postgres:update_germplasm_file_upload_constraints_02 context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT * FROM pg_catalog.pg_constraint WHERE conname = 'file_upload_file_upload_action_chk') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-2443: CB-GM:Update file upload status and action constraints

-- Add CODING IN PROGRESS scale value for FILE_UPLOAD_STATUS constraint
ALTER TABLE
    germplasm.file_upload
DROP CONSTRAINT IF EXISTS
    file_upload_file_status_chk;
	
ALTER TABLE 
    germplasm.file_upload
ADD CONSTRAINT 
    file_upload_file_status_chk CHECK (
        file_status::text = ANY (
            ARRAY['in queue'::text, 'created'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text, 'merge ready'::text, 'merging in progress'::text, 'merge failed'::text, 'coding ready'::text, 'coding in progress'::text, 'coding failed'::text]
    ));



-- revert changes
--rollback  ALTER TABLE
--rollback      germplasm.file_upload
--rollback  DROP CONSTRAINT IF EXISTS
--rollback      file_upload_file_status_chk;
	
--rollback  ALTER TABLE 
--rollback      germplasm.file_upload
--rollback  ADD CONSTRAINT 
--rollback      file_upload_file_status_chk CHECK (
--rollback          file_status::text = ANY (
--rollback              ARRAY['in queue'::text, 'created'::text, 'validation in progress'::text, 'validation error'::text, 'validated'::text, 'creation in progress'::text, 'creation failed'::text, 'completed'::text, 'update in progress'::text, 'update failed'::text, 'merge ready'::text, 'merging in progress'::text, 'merge failed'::text, 'coding ready'::text]
--rollback      ));