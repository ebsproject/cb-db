--liquibase formatted sql

--changeset postgres:create_schema_operational_data_terminal_temp context:schema splitStatements:false   
CREATE SCHEMA IF NOT EXISTS operational_data_terminal_temp;

--rollback DROP SCHEMA IF EXISTS operational_data_terminal_temp CASCADE;