--liquibase formatted sql

--changeset postgres:update_sequence_tenant.person_id_seq context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Update sequence tenant.person_id_seq



-- update sequence
SELECT SETVAL('tenant.person_id_seq', COALESCE(MAX(id), 1) ) FROM tenant.person;



--rollback SELECT NULL;
