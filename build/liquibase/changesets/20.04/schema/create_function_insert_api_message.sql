--liquibase formatted sql

--changeset postgres:create_function_insert_api_message context:schema splitStatements:false rollbackSplitStatements:false
--comment: Create function insert api message



CREATE OR REPLACE FUNCTION api.insert_messages(
    var_http_status_code integer,
    var_msg_type character varying,
    variadic var_msg text[]
)
RETURNS character varying
LANGUAGE plpgsql
AS $function$
DECLARE
    currentMsg text;
BEGIN

    FOREACH currentMsg IN ARRAY var_msg
    LOOP
        IF NOT EXISTS(SELECT 1 FROM api.messages WHERE http_status_code=var_http_status_code AND message=currentMsg)
        THEN
            INSERT INTO api.messages(http_status_code, code, type, message, url)
            SELECT
                var_http_status_code,
                (SELECT code+1 FROM  api.messages WHERE http_status_code=var_http_status_code ORDER BY code DESC LIMIT 1),
                var_msg_type,
                currentMsg,
                'responses.html#'|| (SELECT code+1 FROM api.messages WHERE http_status_code=var_http_status_code ORDER BY code DESC LIMIT 1);

        END IF;
    END LOOP;
    
RETURN 'success';
    
END;	
$function$;



--rollback DROP FUNCTION IF EXISTS api.insert_messages(var_http_status_code integer, var_msg_type character varying, variadic var_msg text[]);
