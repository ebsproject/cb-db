--liquibase formatted sql

--changeset postgres:alter_column_rename_column_facility_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5690 Alter column rename column facility_type to facility_class



ALTER TABLE IF EXISTS place.facility RENAME COLUMN facility_type TO facility_class;



--rollback ALTER TABLE IF EXISTS place.facility RENAME COLUMN facility_class TO facility_type;



--changeset postgres:alter_column_rename_column_facility_subtype context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5690 Alter column rename column facility_subtype to facility_type



ALTER TABLE IF EXISTS place.facility RENAME COLUMN facility_subtype TO facility_type;



--rollback ALTER TABLE IF EXISTS place.facility RENAME COLUMN facility_type TO facility_subtype;




