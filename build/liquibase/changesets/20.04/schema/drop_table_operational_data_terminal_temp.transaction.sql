--liquibase formatted sql

--changeset postgres:drop_schema_operational_data_terminal_temp context:schema splitStatements:false   

DROP SCHEMA IF EXISTS operational_data_terminal_temp CASCADE;

--rollback CREATE SCHEMA IF NOT EXISTS operational_data_terminal_temp;

--changeset postgres:drop_table_operational_data_terminal_temp.transaction context:schema splitStatements:false   

DROP TABLE IF EXISTS operational_data_terminal_temp.transaction CASCADE;

--rollback CREATE SCHEMA IF NOT EXISTS operational_data_terminal_temp;
--rollback CREATE TABLE IF NOT EXISTS operational_data_terminal_temp.transaction (
--rollback   id serial NOT NULL,
--rollback   status character varying NOT NULL,
--rollback   location_id integer,
--rollback   occurrence jsonb,
--rollback   checksum text,
--rollback   action character varying NOT NULL DEFAULT 'data_collection'::character varying, 
--rollback   creator_id integer NOT NULL,
--rollback   creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
--rollback   committed_timestamp timestamp without time zone, 
--rollback   committer_id integer,
--rollback   modifier_id integer, 
--rollback   modification_timestamp timestamp without time zone, 
--rollback   is_void boolean, 
--rollback   remarks character varying,
--rollback   notes text,
--rollback 	event_log jsonb,
--rollback   CONSTRAINT transaction_location_id_fkey FOREIGN KEY (location_id)
--rollback       REFERENCES experiment.location (id) MATCH SIMPLE
--rollback       ON UPDATE CASCADE ON DELETE RESTRICT, 
--rollback  CONSTRAINT transaction_creator_id_fkey FOREIGN KEY (creator_id)
--rollback      REFERENCES tenant.person (id) MATCH SIMPLE
--rollback      ON UPDATE CASCADE ON DELETE RESTRICT, 
--rollback  CONSTRAINT transaction_committer_id_fkey FOREIGN KEY (committer_id)
--rollback      REFERENCES tenant.person (id) MATCH SIMPLE
--rollback      ON UPDATE CASCADE ON DELETE RESTRICT, 
--rollback  CONSTRAINT transaction_modifier_id_fkey FOREIGN KEY (modifier_id)
--rollback      REFERENCES tenant.person (id) MATCH SIMPLE
--rollback      ON UPDATE CASCADE ON DELETE RESTRICT,
--rollback  CONSTRAINT transaction_id_pkey PRIMARY KEY (id),
--rollback  CONSTRAINT terminal_status CHECK (status::text = ANY (ARRAY[ 'uploading in progress'::character varying::text, 'error in background process'::character varying::text, 'in queue'::character varying::text, 'committing in progress'::character varying::text, 'suppression in progress'::character varying::text, 'undo suppression in progress'::character varying::text, 'removing data in progress'::character varying::text, 'undo removing data in progress'::character varying::text, 'uploaded'::character varying::text, 'validation in progress'::character varying::text, 'committed'::character varying::text, 'validated'::character varying::text])))
--rollback WITH (OIDS=FALSE);

--rollback CREATE INDEX IF NOT EXISTS transaction_location_id_idx
--rollback ON operational_data_terminal_temp.transaction
--rollback USING btree(location_id);

--rollback CREATE INDEX IF NOT EXISTS transaction_is_void_idx
--rollback ON operational_data_terminal_temp.transaction
--rollback USING btree (is_void);

--rollback CREATE INDEX IF NOT EXISTS transaction_creator_id_idx
--rollback ON operational_data_terminal_temp.transaction
--rollback USING btree (creator_id);

--rollback CREATE INDEX IF NOT EXISTS transaction_status_idx
--rollback ON operational_data_terminal_temp.transaction
--rollback USING btree (status COLLATE pg_catalog."default");

--rollback SELECT t.* FROM platform.audit_table('operational_data_terminal_temp.transaction') t;