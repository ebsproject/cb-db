--liquibase formatted sql

--changeset postgres:create_table_operational_data_terminal_temp.transaction context:schema splitStatements:false tagDatabase:create_schema_operational_data_terminal_temp

CREATE TABLE IF NOT EXISTS operational_data_terminal_temp.transaction (
  id serial NOT NULL,
  status character varying NOT NULL,
  location_id integer,
  occurrence jsonb,
  checksum text,
  action character varying NOT NULL DEFAULT 'data_collection'::character varying, 
  creator_id integer NOT NULL,
  creation_timestamp timestamp without time zone NOT NULL DEFAULT now(),
  committed_timestamp timestamp without time zone, 
  committer_id integer,
  modifier_id integer, 
  modification_timestamp timestamp without time zone, 
  is_void boolean, 
  remarks character varying,
  notes text,
	event_log jsonb,
  CONSTRAINT transaction_location_id_fkey FOREIGN KEY (location_id)
      REFERENCES experiment.location (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT, 
  CONSTRAINT transaction_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT, 
  CONSTRAINT transaction_committer_id_fkey FOREIGN KEY (committer_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT, 
  CONSTRAINT transaction_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT transaction_id_pkey PRIMARY KEY (id),
  CONSTRAINT terminal_status CHECK (status::text = ANY (ARRAY[ 'uploading in progress'::character varying::text, 'error in background process'::character varying::text, 'in queue'::character varying::text, 'committing in progress'::character varying::text, 'suppression in progress'::character varying::text, 'undo suppression in progress'::character varying::text, 'removing data in progress'::character varying::text, 'undo removing data in progress'::character varying::text, 'uploaded'::character varying::text, 'validation in progress'::character varying::text, 'committed'::character varying::text, 'validated'::character varying::text]))
)
WITH (
  OIDS=FALSE
);

CREATE INDEX IF NOT EXISTS transaction_location_id_idx
  ON operational_data_terminal_temp.transaction
  USING btree
  (location_id);

CREATE INDEX IF NOT EXISTS transaction_is_void_idx
  ON operational_data_terminal_temp.transaction
  USING btree
  (is_void);

CREATE INDEX IF NOT EXISTS transaction_creator_id_idx
  ON operational_data_terminal_temp.transaction
  USING btree
  (creator_id);

CREATE INDEX IF NOT EXISTS transaction_status_idx
  ON operational_data_terminal_temp.transaction
  USING btree
  (status COLLATE pg_catalog."default");

SELECT t.* FROM platform.audit_table('operational_data_terminal_temp.transaction') t;

--rollback DROP TABLE IF EXISTS operational_data_terminal_temp.transaction CASCADE;
--rollback DROP INDEX IF EXISTS operational_data_terminal_temp.transaction_location_id_idx;
--rollback DROP INDEX IF EXISTS operational_data_terminal_temp.transaction_is_void_idx;
--rollback DROP INDEX IF EXISTS operational_data_terminal_temp.transaction_creator_id_idx;
--rollback DROP INDEX IF EXISTS operational_data_terminal_temp.transaction_status_idx;
--rollback DROP TRIGGER IF EXISTS transaction_event_log_tgr ON operational_data_terminal_temp.transaction;

