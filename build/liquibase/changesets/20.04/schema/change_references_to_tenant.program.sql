--liquibase formatted sql

--changeset postgres:change_references_to_tenant.program context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Change references to tenant.program (e.g. program_id)



ALTER TABLE master.item_service_team ADD CONSTRAINT item_service_team_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE master.program_relation ADD CONSTRAINT program_relation_child_id_fk FOREIGN KEY (child_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE master.program_relation ADD CONSTRAINT program_relation_parent_id_fk FOREIGN KEY (parent_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE master.program_scale_value ADD CONSTRAINT program_scale_value_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE master.service ADD CONSTRAINT service_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE operational.service_request ADD CONSTRAINT service_request_requestor_program_id_fk FOREIGN KEY (requestor_program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE operational.service_request ADD CONSTRAINT service_request_provider_program_id_fk FOREIGN KEY (provider_program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE platform.module ADD CONSTRAINT module_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE seed_warehouse.plate_layout ADD CONSTRAINT plate_layout_program_id_fk FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT;



--rollback ALTER TABLE master.item_service_team DROP CONSTRAINT item_service_team_program_id_fk;
--rollback ALTER TABLE master.program_relation DROP CONSTRAINT program_relation_child_id_fk;
--rollback ALTER TABLE master.program_relation DROP CONSTRAINT program_relation_parent_id_fk;
--rollback ALTER TABLE master.program_scale_value DROP CONSTRAINT program_scale_value_program_id_fk;
--rollback ALTER TABLE master.service DROP CONSTRAINT service_program_id_fk;
--rollback ALTER TABLE operational.service_request DROP CONSTRAINT service_request_requestor_program_id_fk;
--rollback ALTER TABLE operational.service_request DROP CONSTRAINT service_request_provider_program_id_fk;
--rollback ALTER TABLE platform.module DROP CONSTRAINT module_program_id_fk;
--rollback ALTER TABLE seed_warehouse.plate_layout DROP CONSTRAINT plate_layout_program_id_fk;
