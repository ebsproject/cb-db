--liquibase formatted sql

--changeset postgres:alter_column_creation_timestamp_in_data_terminal.transaction context:schema splitStatements:FALSE
ALTER TABLE IF EXISTS
    data_terminal.transaction
ALTER COLUMN
    creation_timestamp
TYPE
    timestamp,
ALTER COLUMN
    creation_timestamp
SET 
    NOT NULL,
ALTER COLUMN
    creation_timestamp
SET 
    DEFAULT NOW(); 

--rollback ALTER TABLE IF EXISTS data_terminal.transaction
--rollback ALTER COLUMN creation_timestamp TYPE timestamp without time zone,
--rollback ALTER COLUMN creation_timestamp DROP DEFAULT,
--rollback ALTER COLUMN creation_timestamp DROP NOT NULL;

--changeset postgres:alter_column_modification_timestamp_in_data_terminal.transaction context:schema splitStatements:FALSE
ALTER TABLE IF EXISTS
    data_terminal.transaction
ALTER COLUMN
    modification_timestamp
TYPE
    timestamp;

--rollback ALTER TABLE IF EXISTS data_terminal.transaction
--rollback ALTER COLUMN modification_timestamp TYPE timestamp without time zone;

--changeset postgres:alter_column_is_void_in_data_terminal.transaction context:schema splitStatements:FALSE
ALTER TABLE IF EXISTS
    data_terminal.transaction
ALTER COLUMN
    is_void
SET
    NOT NULL,
ALTER COLUMN
    is_void
SET 
    DEFAULT FALSE;

--rollback ALTER TABLE IF EXISTS data_terminal.transaction
--rollback ALTER COLUMN is_void DROP DEFAULT,
--rollback ALTER COLUMN is_void DROP NOT NULL;