--liquibase formatted sql

--changeset postgres:add_column_data_process_id_in_experiment_experiment context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment.experiment 
ADD COLUMN IF NOT EXISTS
	data_process_id integer,
ADD FOREIGN KEY 
    (data_process_id)
REFERENCES 
    master.item (id);
	
COMMENT ON COLUMN 
    experiment.experiment.data_process_id 
IS 'Data process of the experiment';

--rollback ALTER TABLE IF EXISTS experiment.experiment
--rollback DROP COLUMN IF EXISTS data_process_id;

--changeset postgres:add_idx_process_id_in_experiment_experiment context:schema splitStatements:FALSE
CREATE INDEX IF NOT EXISTS 
	experiment_data_process_id_idx 
ON 
	experiment.experiment ( data_process_id );

--rollback ALTER TABLE IF EXISTS experiment.experiment 
--rollback DROP CONSTRAINT IF EXISTS experiment_data_process_id_idx;
