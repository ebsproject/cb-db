--liquibase formatted sql

--changeset postgres:add_column_is_active_in_tenant_person context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	tenant.person 
ADD COLUMN IF NOT EXISTS
	is_active boolean DEFAULT TRUE;
	
COMMENT ON COLUMN 
    tenant.person.is_active 
IS 
	'Status of the person';

--rollback ALTER TABLE IF EXISTS tenant.person
--rollback DROP COLUMN IF EXISTS is_active;