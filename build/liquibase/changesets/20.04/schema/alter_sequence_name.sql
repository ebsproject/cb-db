--liquibase formatted sql

--changeset postgres:alter_sequence_name context:schema splitStatements:FALSE

ALTER SEQUENCE api.transaction_id_seq1 RENAME TO transaction_id_seq;

--rollback ALTER SEQUENCE api.transaction_id_seq RENAME TO transaction_id_seq1;