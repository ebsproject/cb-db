--liquibase formatted sql

--changeset postgres:alter_column_entry_list_type_add_not_null_constraint_in_experiment.entry_list.sql context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5686 Alter column entry_list_type add not null constraint in experiment.entry_list



ALTER TABLE IF EXISTS
    experiment.entry_list
ALTER COLUMN
    entry_list_type
SET
    NOT NULL;



--rollback ALTER TABLE IF EXISTS experiment.entry_list
--rollback ALTER COLUMN entry_list_type DROP NOT NULL;
