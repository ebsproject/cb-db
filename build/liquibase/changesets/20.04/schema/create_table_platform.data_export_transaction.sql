--liquibase formatted sql

--changeset postgres:create_table_platform.data_export_transaction context:schema splitStatements:false

CREATE TABLE IF NOT EXISTS platform.data_export_transaction (
	id serial NOT NULL,
	person_id int4 NOT NULL,
	data_export_status text NULL,
	total_count int4 NULL,
	current_count int4 NULL,
	filename text NULL,
	creator_id integer NOT NULL,
  	creation_timestamp timestamp NOT NULL DEFAULT now(),
 	modifier_id integer, 
  	modification_timestamp timestamp, 
  	is_void boolean DEFAULT FALSE, 
  	remarks character varying,
  	notes text,
	event_log jsonb,
	CONSTRAINT data_export_transaction_person_id_fkey FOREIGN KEY (person_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT, 
	CONSTRAINT data_export_transaction_creator_id_fkey FOREIGN KEY (creator_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT, 
    CONSTRAINT data_export_transaction_modifier_id_fkey FOREIGN KEY (modifier_id)
      REFERENCES tenant.person (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT data_export_transaction_id_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN platform.data_export_transaction.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN platform.data_export_transaction.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN platform.data_export_transaction.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN platform.data_export_transaction.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN platform.data_export_transaction.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN platform.data_export_transaction.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	data_export_transaction_person_id_idx
ON
	platform.data_export_transaction
USING btree ( person_id );

CREATE INDEX
	data_export_transaction_is_void_idx
ON
	platform.data_export_transaction
USING
	btree ( is_void );

CREATE INDEX
	data_export_transaction_creator_id_idx
ON
	platform.data_export_transaction
USING btree ( creator_id );

CREATE INDEX
	data_export_transaction_modifier_id_idx
ON
	platform.data_export_transaction
USING btree ( modifier_id );

SELECT t.* FROM platform.audit_table('platform.data_export_transaction') t;

--rollback DROP TABLE IF EXISTS platform.data_export_transaction CASCADE;
--rollback DROP INDEX IF EXISTS platform.data_export_transaction_person_id_idx;
--rollback DROP INDEX IF EXISTS platform.data_export_transaction_is_void_idx;
--rollback DROP INDEX IF EXISTS platform.data_export_transaction_creator_id_idx;
--rollback DROP INDEX IF EXISTS platform.data_export_transaction_modifier_id_idx;

