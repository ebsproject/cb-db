--liquibase formatted sql

--changeset postgres:create_germplasm.cross_parent_id_seq context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5464 Create germplasm.cross_parent_id_seq



-- create sequence for germplasm.cross_parent.id
CREATE SEQUENCE germplasm.cross_parent_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;

-- set default value for germplasm.cross_parent.id
ALTER TABLE germplasm.cross_parent
    ALTER COLUMN id SET DEFAULT nextval('germplasm.cross_parent_id_seq'::regclass);



--rollback ALTER TABLE germplasm.cross_parent
--rollback     ALTER COLUMN id DROP DEFAULT;

--rollback DROP SEQUENCE germplasm.cross_parent_id_seq;
