--liquibase formatted sql

--changeset postgres:add_column_location_document_in_experiment_location context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment.location 
ADD COLUMN IF NOT EXISTS
	location_document tsvector;
	
COMMENT ON COLUMN 
    experiment.location.location_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';

--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS location_document;

--changeset postgres:add_idx_location_document_in_experiment_location context:schema splitStatements:FALSE
CREATE INDEX IF NOT EXISTS 
	location_location_document_idx 
ON 
	experiment.location 
USING 
	gin ( location_document );

--rollback ALTER TABLE IF EXISTS experiment.location 
--rollback DROP CONSTRAINT IF EXISTS location_location_document_idx;
