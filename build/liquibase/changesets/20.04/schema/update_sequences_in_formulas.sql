--liquibase formatted sql

--changeset postgres:update_sequences_in_formulas context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5760 Update sequences in formulas



-- update sequences
SELECT SETVAL('master.formula_id_seq', COALESCE(MAX(id), 1)) FROM master.formula;

SELECT SETVAL('master.formula_parameter_id_seq', COALESCE(MAX(id), 1)) FROM master.formula_parameter;


--rollback SELECT NULL;
