--liquibase formatted sql

--changeset postgres:create_table_master.program_variable_set context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Create table master.program_variable_set



-- create table
CREATE TABLE master.program_variable_set (
	id serial NOT NULL,

	program_id int4 NOT NULL,
	variable_set_id int4 NOT NULL,
	remarks text NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	creator_id int4 NOT NULL DEFAULT 1,
	modification_timestamp timestamp NULL,
	modifier_id int4 NULL,
	notes text NULL,
	is_void bool NOT NULL DEFAULT false,

	CONSTRAINT program_variable_set_id_pkey PRIMARY KEY (id),
	CONSTRAINT program_variable_set_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person (id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT program_variable_set_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person (id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT program_variable_set_program_id_fkey FOREIGN KEY (program_id) REFERENCES tenant.program (id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT program_variable_set_variable_set_id_fkey FOREIGN KEY (variable_set_id) REFERENCES master.variable_set (id) ON UPDATE CASCADE ON DELETE RESTRICT
);


COMMENT ON TABLE master.program_variable_set IS 'Program and variable set relationship';
COMMENT ON COLUMN master.program_variable_set.id IS 'Primary key of the record in the table';
COMMENT ON COLUMN master.program_variable_set.program_id IS 'Program of the variable set';
COMMENT ON COLUMN master.program_variable_set.variable_set_id IS 'Variable set of the program';
COMMENT ON COLUMN master.program_variable_set.remarks IS 'Additional details about the record';
COMMENT ON COLUMN master.program_variable_set.creation_timestamp IS 'Timestamp when the record was added to the table';
COMMENT ON COLUMN master.program_variable_set.creator_id IS 'ID of the user who added the record to the table';
COMMENT ON COLUMN master.program_variable_set.modification_timestamp IS 'Timestamp when the record was last modified';
COMMENT ON COLUMN master.program_variable_set.modifier_id IS 'ID of the user who last modified the record';
COMMENT ON COLUMN master.program_variable_set.notes IS 'Additional details added by an admin; can be technical or advanced details';
COMMENT ON COLUMN master.program_variable_set.is_void IS 'Indicator whether the record is deleted or not';



--rollback DROP TABLE master.program_variable_set;
