--liquibase formatted sql

--changeset postgres:create_schema_data_terminal context:schema splitStatements:false   
CREATE SCHEMA IF NOT EXISTS data_terminal;

--rollback DROP SCHEMA IF EXISTS data_terminal CASCADE;