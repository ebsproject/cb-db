--liquibase formatted sql

--changeset postgres:alter_column_stage_id_add_constraint_not_null_in_experiment.location context:schema splitStatements:FALSE

ALTER TABLE IF EXISTS
    experiment.LOCATION
ALTER COLUMN
    stage_id
SET
    NOT NULL;

--rollback ALTER TABLE IF EXISTS experiment.location
--rollback ALTER COLUMN stage_id DROP NOT NULL;

--changeset postgres:alter_column_location_year_add_constraint_not_null_in_experiment.location context:schema splitStatements:FALSE

ALTER TABLE IF EXISTS
    experiment.LOCATION
ALTER COLUMN
    location_year
SET
    NOT NULL;

--rollback ALTER TABLE IF EXISTS experiment.location
--rollback ALTER COLUMN location_year DROP NOT NULL;

--changeset postgres:alter_column_season_id_add_constraint_not_null_in_experiment.location context:schema splitStatements:FALSE

ALTER TABLE IF EXISTS
    experiment.LOCATION
ALTER COLUMN
    season_id
SET
    NOT NULL;

--rollback ALTER TABLE IF EXISTS experiment.location
--rollback ALTER COLUMN season_id DROP NOT NULL;

--changeset postgres:alter_column_program_id_add_constraint_not_null_in_experiment.location context:schema splitStatements:FALSE

ALTER TABLE IF EXISTS
    experiment.LOCATION
ALTER COLUMN
    program_id
SET
    NOT NULL;

--rollback ALTER TABLE IF EXISTS experiment.location
--rollback ALTER COLUMN program_id DROP NOT NULL;