--liquibase formatted sql

--changeset postgres:add_constraint_unique_api.messages.code context:schema splitStatements:false

DELETE FROM api.messages CASCADE;

ALTER TABLE IF EXISTS 
	api.messages
ADD CONSTRAINT
	messages_code_unq
UNIQUE (
	code
);

COMMENT ON COLUMN 
    api.messages.code 
IS 
	'Reference code for API messages';

--rollback ALTER TABLE IF EXISTS api.messages
--rollback DROP CONSTRAINT messages_code_unq;