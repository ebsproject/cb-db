--liquibase formatted sql

--changeset postgres:drop_not_null_germplasm_family_cross_id_source_seed_id context:schema splitStatements:FALSE

ALTER TABLE
    germplasm.family
ALTER COLUMN
    cross_id
    DROP NOT NULL;

ALTER TABLE
    germplasm.family
ALTER COLUMN
    source_seed_id
    DROP NOT NULL
;

--rollback ALTER TABLE germplasm.family ALTER COLUMN cross_id SET NOT NULL;
--rollback ALTER TABLE germplasm.family ALTER COLUMN source_seed_id SET NOT NULL;