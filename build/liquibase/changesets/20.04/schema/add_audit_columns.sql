--liquibase formatted sql

--changeset postgres:add_audit_columns context:schema splitStatements:false

WITH t_get_new_tables AS (
    SELECT
        concat(ist.table_schema, '.', ist.table_name) full_table_name,
        ist.table_schema schema_name,
        ist.table_name
    FROM
        information_schema.tables ist
    WHERE
        ist.table_catalog = CURRENT_CATALOG
        AND ist.table_schema IN (
            'experiment',
            'germplasm',
            'tenant',
            'place'
        )
    ORDER BY
        ist.table_schema,
        ist.table_name
), t_construct_queries AS (
    SELECT
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                is_void boolean NOT NULL DEFAULT FALSE
            ;
            
            COMMENT ON COLUMN
                %1$s.is_void
            IS
                'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]'
            ;
            
            CREATE INDEX
                %2$s_is_void_idx
            ON
                %1$s
            USING
                btree (
                    is_void
                )
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) is_void,
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                creation_timestamp timestamp NOT NULL DEFAULT current_timestamp
            ;
            
            COMMENT ON COLUMN
                %1$s.creation_timestamp
            IS
                'Creation Timestamp: Timestamp when the record was created [CTSTAMP]'
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) creation_timestamp,
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                creator_id integer
            ;
            
            COMMENT ON COLUMN
                %1$s.creator_id
            IS
                'Creator ID: Reference to the person who created the record [CPERSON]'
            ;
        
            UPDATE
                %1$s
            SET
                creator_id = 1
            ;
            
            ALTER TABLE
                %1$s
            ALTER COLUMN
                creator_id
            SET
                NOT NULL
            ;
            
            CREATE INDEX
                %2$s_creator_id_idx
            ON
                %1$s
            USING
                btree (
                    creator_id
                )
            ;
            
            ALTER TABLE
                %1$s
            ADD CONSTRAINT
                %2$s_creator_id_fk
            FOREIGN KEY (
                creator_id
            )
            REFERENCES
                tenant.person (id)
            ON UPDATE
                CASCADE
            ON DELETE
                RESTRICT
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) creator_id,
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                modification_timestamp timestamp
            ;
            
            COMMENT ON COLUMN
                %1$s.modification_timestamp
            IS
                'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]'
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) modification_timestamp,
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                modifier_id integer
            ;
            
            COMMENT ON COLUMN
                %1$s.modifier_id
            IS
                'Modifier ID: Reference to the person who last modified the record [MPERSON]'
            ;
            
            CREATE INDEX
                %2$s_modifier_id_idx
            ON
                %1$s
            USING
                btree (
                    modifier_id
                )
            ;
            
            ALTER TABLE
                %1$s
            ADD CONSTRAINT
                %2$s_modifier_id_fk
            FOREIGN KEY (
                modifier_id
            )
            REFERENCES
                tenant.person (id)
            ON UPDATE
                CASCADE
            ON DELETE
                RESTRICT
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) modifier_id,
        format($$
            ALTER TABLE
                %1$s
            ADD COLUMN
                notes text
            ;
            
            COMMENT ON COLUMN
                %1$s.notes
            IS
                'NOTES: Technical details about the record [ISVOID]'
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) notes,
        format($$
            SELECT
                t.*
            FROM
                platform.audit_table(
                    in_table_name := '%1$s'
                ) t
            ;
        $$,
            t.full_table_name,
            t.table_name
        ) event_log
    FROM
        t_get_new_tables t
), t_execute AS (
    SELECT
        platform.execute(t.is_void) is_void,
        platform.execute(t.creation_timestamp) creation_timestamp,
        platform.execute(t.creator_id) creator_id,
        platform.execute(t.modification_timestamp) modification_timestamp,
        platform.execute(t.modifier_id) modifier_id,
        platform.execute(t.notes) notes,
        platform.execute(t.event_log) event_log
    FROM
        t_construct_queries t
)
SELECT
    t.*
FROM
    t_execute t
;

--rollback select 1;