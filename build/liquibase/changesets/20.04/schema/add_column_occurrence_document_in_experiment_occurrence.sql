--liquibase formatted sql

--changeset postgres:add_column_occurrence_document_in_experiment_occurrence context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment.occurrence 
ADD COLUMN IF NOT EXISTS
	occurrence_document tsvector;

COMMENT ON COLUMN 
    experiment.occurrence.occurrence_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';
	
--rollback ALTER TABLE IF EXISTS experiment.occurrence
--rollback DROP COLUMN IF EXISTS occurrence_document;

--changeset postgres:add_idx_occurrence_document_in_experiment_occurrence context:schema splitStatements:FALSE
CREATE INDEX IF NOT EXISTS 
	occurrence_occurrence_document_idx 
ON 
	experiment.occurrence 
USING 
	gin ( occurrence_document );

--rollback ALTER TABLE IF EXISTS experiment.occurrence 
--rollback DROP CONSTRAINT IF EXISTS occurrence_document_idx;
