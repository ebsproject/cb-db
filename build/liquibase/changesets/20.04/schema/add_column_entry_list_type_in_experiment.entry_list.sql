--liquibase formatted sql

--changeset postgres:add_column_entry_list_type_in_experiment.entry_list context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5686 Add column entry_list_type in experiment.entry_list



ALTER TABLE IF EXISTS 
	experiment.entry_list 
ADD COLUMN IF NOT EXISTS
	entry_list_type character varying;
	
COMMENT ON COLUMN 
    experiment.entry_list.entry_list_type 
IS 'Type of the entry list';

CREATE INDEX IF NOT EXISTS 
	entry_list_entry_list_type_idx 
ON 
	experiment.entry_list ( entry_list_type );


--rollback ALTER TABLE IF EXISTS experiment.entry_list
--rollback DROP COLUMN IF EXISTS entry_list_type;
--rollback DROP INDEX IF EXISTS experiment.entry_list_entry_list_type_idx;
