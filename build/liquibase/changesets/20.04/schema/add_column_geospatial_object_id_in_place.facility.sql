--liquibase formatted sql

--changeset postgres:add_column_geospatial_object_id_in_place.facility context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5547 Add column geospatial_object_id in place.facility



-- add column
ALTER TABLE place.facility
    ADD COLUMN geospatial_object_id INTEGER;

COMMENT ON COLUMN place.facility.geospatial_object_id
    IS 'Geospatial Object ID: Reference to the geospatial object where the facility is located [FAC_GEO_ID]';

ALTER TABLE place.facility
    ADD CONSTRAINT facility_geospatial_object_id_fk
    FOREIGN KEY (geospatial_object_id)
    REFERENCES place.geospatial_object (id)
    ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON CONSTRAINT facility_geospatial_object_id_fk ON place.facility
    IS 'A facility can be found in one geospatial object. A geospatial object can have zero or many facilities within it.';



--rollback ALTER TABLE place.facility
--rollback     DROP CONSTRAINT facility_geospatial_object_id_fk;

--rollback ALTER TABLE place.facility
--rollback     DROP COLUMN geospatial_object_id;
