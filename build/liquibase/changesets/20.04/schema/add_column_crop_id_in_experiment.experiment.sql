--liquibase formatted sql

--changeset postgres:add_column_crop_id_in_experiment.experiment context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment.experiment
ADD COLUMN IF NOT EXISTS 
	crop_id integer NOT NULL DEFAULT 1,
ADD FOREIGN KEY 
    (crop_id)
REFERENCES 
    tenant.crop (id);
    
COMMENT ON COLUMN 
    experiment.experiment.crop_id 
IS 
	'Crop ID: Reference to the experiment"s crop [EXP_CROP_ID]';

CREATE INDEX IF NOT EXISTS 
	experiment_crop_id_idx
ON 
	experiment.experiment
USING 
	btree (crop_id);
 
--rollback ALTER TABLE IF EXISTS experiment.experiment
--rollback DROP COLUMN IF EXISTS crop_id;

--rollback ALTER TABLE IF EXISTS experiment.experiment 
--rollback DROP CONSTRAINT IF EXISTS experiment_crop_id_idx;

--changeset postgres:drop_default_value_of_crop_id_in_experiment.experiment context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment 
ALTER COLUMN
	crop_id DROP DEFAULT;

--rollback ALTER TABLE IF EXISTS experiment.experiment 
--rollback ALTER COLUMN crop_id SET DEFAULT 1;
