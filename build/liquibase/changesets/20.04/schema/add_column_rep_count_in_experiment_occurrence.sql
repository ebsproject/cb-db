--liquibase formatted sql

--changeset postgres:add_column_rep_count_in_experiment_occurrence context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment.occurrence 
ADD COLUMN IF NOT EXISTS
	rep_count integer;

COMMENT ON COLUMN 
    experiment.occurrence.rep_count 
IS 
	'Number of replication';

--rollback ALTER TABLE IF EXISTS experiment.occurrence
--rollback DROP COLUMN IF EXISTS rep_count;