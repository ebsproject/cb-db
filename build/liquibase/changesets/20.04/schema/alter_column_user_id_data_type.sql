--liquibase formatted sql

--changeset postgres:alter_column_user_id_data_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Alter column user_id data type to integer


-- change data type to integer
ALTER TABLE api.oauth_access_tokens ALTER COLUMN user_id SET DATA TYPE integer USING user_id::integer;



--rollback ALTER TABLE api.oauth_access_tokens ALTER COLUMN user_id SET DATA TYPE varchar(255);
