--liquibase formatted sql

--changeset postgres:add_column_stage_id_in_experiment.location context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment."location"
ADD COLUMN IF NOT EXISTS 
	stage_id integer,
ADD FOREIGN KEY 
    (stage_id)
REFERENCES 
    tenant.stage (id);
    
COMMENT ON COLUMN 
    experiment."location".stage_id 
IS 
	'Stage ID: Reference to the experiment"s stage [LOC_STAGE_ID]';

CREATE INDEX IF NOT EXISTS 
	location_stage_id_idx
ON 
	experiment."location"
USING 
	btree (stage_id);
 
--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS stage_id;
--rollback DROP INDEX IF EXISTS experiment.location_stage_id_idx;

--changeset postgres:add_column_location_year_in_experiment.location context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment."location"
ADD COLUMN IF NOT EXISTS 
	location_year integer;
    
COMMENT ON COLUMN 
    experiment."location".location_year 
IS 
	'Location Year: Experiment location year';

CREATE INDEX IF NOT EXISTS 
	location_location_year_idx
ON 
	experiment."location"
USING 
	btree (location_year);
 
--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS location_year;
--rollback DROP INDEX IF EXISTS experiment.location_location_year_idx;

--changeset postgres:add_column_season_id_in_experiment.location context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment."location"
ADD COLUMN IF NOT EXISTS 
	season_id integer,
ADD FOREIGN KEY 
    (season_id)
REFERENCES 
    tenant.season (id);
    
COMMENT ON COLUMN 
    experiment."location".season_id 
IS 
	'Season ID: Reference to the experiment"s season [LOC_SEASON_ID]';

CREATE INDEX IF NOT EXISTS 
	location_season_id_idx
ON 
	experiment."location"
USING 
	btree (season_id);
 
--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS season_id;
--rollback DROP INDEX IF EXISTS experiment.location_season_id_idx;

--changeset postgres:add_column_program_id_in_experiment.location context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	experiment."location"
ADD COLUMN IF NOT EXISTS 
	program_id integer,
ADD FOREIGN KEY 
    (program_id)
REFERENCES 
    tenant.program (id);
    
COMMENT ON COLUMN 
    experiment."location".program_id 
IS 
	'Program ID: Reference to the experiment"s program [LOC_PROGRAM_ID]';

CREATE INDEX IF NOT EXISTS 
	location_program_id_idx
ON 
	experiment."location"
USING 
	btree (program_id);
 
--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS program_id;
--rollback DROP INDEX IF EXISTS experiment.location_program_id_idx;


