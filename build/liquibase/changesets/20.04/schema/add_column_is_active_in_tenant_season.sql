--liquibase formatted sql

--changeset postgres:add_column_is_active_in_tenant_season context:schema splitStatements:false
ALTER TABLE IF EXISTS 
	tenant.season 
ADD COLUMN IF NOT EXISTS
	is_active boolean DEFAULT TRUE;
	
COMMENT ON COLUMN 
    tenant.season.is_active 
IS 
	'Status of the season';

--rollback ALTER TABLE IF EXISTS tenant.season
--rollback DROP COLUMN IF EXISTS is_active;