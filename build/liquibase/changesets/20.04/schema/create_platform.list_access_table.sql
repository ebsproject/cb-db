--liquibase formatted sql

--changeset postgres:create_platform.list_acess_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Create platform.list_access_table



CREATE TABLE platform.list_access (
	id serial NOT NULL,
	list_id int4 NOT NULL,
	user_id int4 NOT NULL,
	"permission" varchar NOT NULL,
	remarks text NULL,
	creation_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	creator_id int4 NOT NULL,
	modification_timestamp timestamp NULL,
	modifier_id int4 NULL,
	notes text NULL,
	is_void bool NOT NULL DEFAULT false,
	event_log jsonb NULL,
	record_uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
	CONSTRAINT list_access_id_pkey PRIMARY KEY (id),
	CONSTRAINT list_access_permission_chk CHECK (((permission)::text = ANY (ARRAY['read'::text, 'read_write'::text]))),
	CONSTRAINT list_access_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT list_access_list_id_fkey FOREIGN KEY (list_id) REFERENCES platform.list(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT list_access_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT,
	CONSTRAINT list_access_user_id_fkey FOREIGN KEY (user_id) REFERENCES tenant.person(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE INDEX list_access_list_id_idx ON platform.list_access USING btree (list_id);
CREATE INDEX list_access_user_id_idx ON platform.list_access USING btree (user_id);
CREATE INDEX list_access_creator_id_idx ON platform.list_access USING btree (creator_id);
CREATE INDEX list_access_is_void_idx ON platform.list_access USING btree (is_void);
CREATE INDEX list_access_modifier_id_idx ON platform.list_access USING btree (modifier_id);
CREATE INDEX list_access_record_uuid_idx ON platform.list_access USING btree (record_uuid);

COMMENT ON TABLE platform.list_access IS 'Stores the items that are added to a specific list.';

COMMENT ON COLUMN platform.list_access.id IS 'Identifier of the record within the table';

COMMENT ON COLUMN platform.list_access.list_id IS 'ID of the list. References to platform.list';

COMMENT ON COLUMN platform.list_access.user_id IS 'ID of the user that the list has been shared to';

COMMENT ON COLUMN platform.list_access.permission IS 'Permission given to the user. Can be either read or read_write';

COMMENT ON COLUMN platform.list_access.remarks IS 'Additional details about the record';

COMMENT ON COLUMN platform.list_access.creation_timestamp IS 'Timestamp when the record was added to the table';

COMMENT ON COLUMN platform.list_access.creator_id IS 'ID of the user who added the record to the table';

COMMENT ON COLUMN platform.list_access.modification_timestamp IS 'Timestamp when the record was last modified';

COMMENT ON COLUMN platform.list_access.modifier_id IS 'ID of the user who last modified the record';

COMMENT ON COLUMN platform.list_access.notes IS 'Additional details added by an admin; can be technical or advanced details';

COMMENT ON COLUMN platform.list_access.is_void IS 'Indicator whether the record is deleted (true) or not (false)';

COMMENT ON COLUMN platform.list_access.event_log IS 'Historical transactions of the record';

COMMENT ON COLUMN platform.list_access.record_uuid IS 'Universally unique identifier (UUID) of the record';

--rollback DROP TABLE platform.list_access;
