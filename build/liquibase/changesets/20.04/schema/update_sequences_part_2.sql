--liquibase formatted sql

--changeset postgres:update_sequences context:schema splitStatements:FALSE rollbackSplitStatements:FALSE
--comment: B4R-5658 Ensure id sequences are up-to-date



SELECT SETVAL('data_terminal.background_process_id_seq', COALESCE(MAX(id), 1) ) FROM data_terminal.background_process;
SELECT SETVAL('data_terminal.transaction_dataset_id_seq', COALESCE(MAX(id), 1) ) FROM data_terminal.transaction_dataset;
SELECT SETVAL('data_terminal.transaction_file_id_seq', COALESCE(MAX(id), 1) ) FROM data_terminal.transaction_file;
SELECT SETVAL('data_terminal.transaction_id_seq', COALESCE(MAX(id), 1) ) FROM data_terminal.transaction;
SELECT SETVAL('platform.data_export_transaction_id_seq', COALESCE(MAX(id), 1) ) FROM platform.data_export_transaction;



--rollback SELECT NULL;
