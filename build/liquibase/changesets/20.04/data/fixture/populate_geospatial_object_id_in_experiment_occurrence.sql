--liquibase formatted sql

--changeset postgres:populate_geospatial_object_id_in_experiment_occurrence context:fixture splitStatements:false

do $$
declare
	record_occurrence record;
	var_occurrence_id int;
	var_geospatial_object_id int;
	var_location_id int;
	var_location_geospatial_object_id int;
begin
	for record_occurrence in (select * from experiment.occurrence)
	loop
		var_occurrence_id = record_occurrence.id;
		var_geospatial_object_id = record_occurrence.geospatial_object_id;
	
		select 
			el.id,
			el.geospatial_object_id
		into 
			var_location_id, 
			var_location_geospatial_object_id
		from 
			experiment.location el
		where 
			el.id = var_occurrence_id;
		
		raise notice '% %', var_location_id, var_location_geospatial_object_id;
		
		update 
			experiment.occurrence
		set 
			geospatial_object_id = var_location_geospatial_object_id
		where
			id = var_location_id;
			
	end loop;
end 
$$

--rollback update experiment.occurrence set geospatial_object_id = null;
