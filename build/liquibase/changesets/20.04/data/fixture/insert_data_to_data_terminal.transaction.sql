--liquibase formatted sql

--changeset postgres:insert_data_to_data_terminal.transaction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5620 Insert data to data_terminal.transaction



-- insert data to data_terminal.transaction
INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6', 'committed', '390', '{"390": "OCC004201"}', NULL, 'insert', '18', '2014-09-29 07:41:47.897807', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('10', 'committed', '323', '{"323": "OCC001201"}', NULL, 'insert', '18', '2014-09-30 04:31:04.585993', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('12', 'committed', '340', '{"340": "OCC000801"}', NULL, 'insert', '18', '2014-09-30 06:53:49.934859', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('15', 'committed', '206', '{"206": "OCC001501"}', NULL, 'insert', '18', '2014-09-30 08:12:59.160514', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('16', 'committed', '231', '{"231": "OCC001701"}', NULL, 'insert', '18', '2014-09-30 09:20:05.729222', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('17', 'committed', '205', '{"205": "OCC001401"}', NULL, 'insert', '18', '2014-09-30 10:30:55.754969', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('18', 'committed', '228', '{"228": "OCC001301"}', NULL, 'insert', '18', '2014-09-30 10:43:13.883301', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('22', 'committed', '230', '{"230": "OCC001901"}', NULL, 'insert', '18', '2014-10-01 02:24:59.390267', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('23', 'committed', '232', '{"232": "OCC002101"}', NULL, 'insert', '18', '2014-10-01 02:43:15.895459', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('24', 'committed', '234', '{"234": "OCC002301"}', NULL, 'insert', '18', '2014-10-01 03:04:27.833349', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('25', 'committed', '236', '{"236": "OCC002201"}', NULL, 'insert', '18', '2014-10-01 03:33:11.110919', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('35', 'committed', '235', '{"235": "OCC001801"}', NULL, 'insert', '18', '2014-10-01 09:22:22.963098', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('130', 'committed', '170', '{"170": "OCC000901"}', NULL, 'insert', '18', '2014-10-03 01:13:41.420745', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('169', 'committed', '236', '{"236": "OCC002201"}', NULL, 'insert', '18', '2014-10-06 06:47:47.789684', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('170', 'committed', '235', '{"235": "OCC001801"}', NULL, 'insert', '18', '2014-10-06 06:58:51.29118', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('171', 'committed', '234', '{"234": "OCC002301"}', NULL, 'insert', '18', '2014-10-06 07:09:25.072722', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('172', 'committed', '233', '{"233": "OCC002001"}', NULL, 'insert', '18', '2014-10-06 07:13:56.584936', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('174', 'committed', '705', '{"705": "OCC002502"}', NULL, 'insert', '18', '2014-10-09 00:49:11.455668', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('175', 'committed', '700', '{"700": "OCC002501"}', NULL, 'insert', '18', '2014-10-09 02:51:55.845477', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('176', 'committed', '706', '{"706": "OCC000601"}', NULL, 'insert', '18', '2014-10-09 02:53:30.489075', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('177', 'committed', '706', '{"706": "OCC000601"}', NULL, 'insert', '18', '2014-10-09 03:59:58.718422', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('178', 'committed', '706', '{"706": "OCC000601"}', NULL, 'insert', '18', '2014-10-09 04:14:52.346758', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('179', 'committed', '706', '{"706": "OCC000601"}', NULL, 'insert', '18', '2014-10-09 06:38:58.697877', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('199', 'committed', '714', '{"714": "OCC004401"}', NULL, 'insert', '18', '2014-10-13 07:33:34.719865', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('200', 'committed', '715', '{"715": "OCC003301"}', NULL, 'insert', '18', '2014-10-13 07:36:41.458615', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('201', 'committed', '716', '{"716": "OCC003201"}', NULL, 'insert', '18', '2014-10-13 07:39:32.975359', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('208', 'committed', '720', '{"720": "OCC000701"}', NULL, 'insert', '18', '2014-10-13 10:44:12.464966', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('211', 'committed', '720', '{"720": "OCC000701"}', NULL, 'insert', '18', '2014-10-14 01:53:04.433163', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('212', 'committed', '720', '{"720": "OCC000701"}', NULL, 'insert', '18', '2014-10-14 02:10:04.707702', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('215', 'committed', '323', '{"323": "OCC001201"}', NULL, 'insert', '18', '2014-10-14 02:19:45.586031', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('216', 'committed', '235', '{"235": "OCC001801"}', NULL, 'insert', '18', '2014-10-14 02:34:32.318129', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('217', 'committed', '235', '{"235": "OCC001801"}', NULL, 'insert', '18', '2014-10-14 02:40:04.015041', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('228', 'committed', '737', '{"737": "OCC001601"}', NULL, 'insert', '18', '2014-10-15 06:19:19.244009', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('275', 'committed', '721', '{"721": "OCC002601"}', NULL, 'insert', '18', '2014-10-17 02:10:41.964296', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('276', 'committed', '705', '{"705": "OCC002502"}', NULL, 'insert', '18', '2014-10-17 02:14:01.785616', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('277', 'committed', '698', '{"698": "OCC004202"}', NULL, 'insert', '18', '2014-10-17 02:15:30.252706', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('279', 'committed', '700', '{"700": "OCC002501"}', NULL, 'insert', '18', '2014-10-17 02:19:24.839185', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('280', 'committed', '557', '{"557": "OCC004801"}', NULL, 'insert', '18', '2014-10-17 02:20:48.462868', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('281', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '18', '2014-10-17 02:25:01.861729', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('282', 'committed', '511', '{"511": "OCC004601"}', NULL, 'insert', '18', '2014-10-17 02:27:05.003451', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('283', 'committed', '473', '{"473": "OCC003101"}', NULL, 'insert', '18', '2014-10-17 02:30:50.896516', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('284', 'committed', '421', '{"421": "OCC004301"}', NULL, 'insert', '18', '2014-10-17 02:38:45.122647', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('285', 'committed', '385', '{"385": "OCC002701"}', NULL, 'insert', '18', '2014-10-17 02:40:14.603527', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('289', 'committed', '524', '{"524": "OCC004901"}', NULL, 'insert', '18', '2014-10-17 02:58:17.824889', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('290', 'committed', '463', '{"463": "OCC004701"}', NULL, 'insert', '18', '2014-10-17 03:00:12.818796', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('292', 'committed', '705', '{"705": "OCC002502"}', NULL, 'insert', '18', '2014-10-17 03:08:04.138607', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('297', 'committed', '715', '{"715": "OCC003301"}', NULL, 'insert', '18', '2014-10-18 02:54:59.55086', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('298', 'committed', '390', '{"390": "OCC004201"}', NULL, 'insert', '18', '2014-10-18 03:01:46.984985', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('299', 'committed', '716', '{"716": "OCC003201"}', NULL, 'insert', '18', '2014-10-18 03:10:57.134448', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('484', 'committed', '511', '{"511": "OCC004601"}', NULL, 'insert', '18', '2014-11-28 10:12:48.00516', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('485', 'committed', '511', '{"511": "OCC004601"}', NULL, 'insert', '18', '2014-11-28 10:19:32.542514', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('487', 'committed', '390', '{"390": "OCC004201"}', NULL, 'insert', '18', '2014-11-28 10:35:59.891309', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('489', 'committed', '385', '{"385": "OCC002701"}', NULL, 'insert', '18', '2014-11-28 10:44:44.290122', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('490', 'committed', '385', '{"385": "OCC002701"}', NULL, 'insert', '18', '2014-11-28 10:49:03.169451', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('500', 'committed', '714', '{"714": "OCC004401"}', NULL, 'insert', '18', '2014-12-01 05:44:42.754157', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('501', 'committed', '421', '{"421": "OCC004301"}', NULL, 'insert', '18', '2014-12-01 05:54:31.140601', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('503', 'committed', '716', '{"716": "OCC003201"}', NULL, 'insert', '18', '2014-12-01 10:26:33.483344', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('602', 'committed', '721', '{"721": "OCC002601"}', NULL, 'insert', '18', '2014-12-04 10:01:04.040417', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('603', 'committed', '473', '{"473": "OCC003101"}', NULL, 'insert', '18', '2014-12-04 10:09:48.136098', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('604', 'committed', '715', '{"715": "OCC003301"}', NULL, 'insert', '18', '2014-12-04 10:14:08.926882', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('605', 'committed', '524', '{"524": "OCC004901"}', NULL, 'insert', '18', '2014-12-04 10:22:46.034179', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('608', 'committed', '385', '{"385": "OCC002701"}', NULL, 'insert', '18', '2014-12-05 01:09:34.447661', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('609', 'committed', '511', '{"511": "OCC004601"}', NULL, 'insert', '18', '2014-12-05 01:14:31.383475', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('610', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '18', '2014-12-05 01:22:57.528714', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('611', 'committed', '463', '{"463": "OCC004701"}', NULL, 'insert', '18', '2014-12-05 01:33:18.169261', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('688', 'committed', '378', '{"378": "OCC002901"}', NULL, 'insert', '18', '2015-01-07 03:19:56.342217', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('706', 'committed', '843', '{"843": "OCC004501"}', NULL, 'insert', '18', '2015-01-12 09:08:13.424554', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('707', 'committed', '705', '{"705": "OCC002502"}', NULL, 'insert', '18', '2015-01-12 11:57:50.257317', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('713', 'committed', '715', '{"715": "OCC003301"}', NULL, 'insert', '18', '2015-01-15 08:29:50.430241', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('714', 'committed', '716', '{"716": "OCC003201"}', NULL, 'insert', '18', '2015-01-15 08:30:13.229404', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('715', 'committed', '821', '{"821": "OCC002401"}', NULL, 'insert', '18', '2015-01-15 11:33:34.939241', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('716', 'committed', '826', '{"826": "OCC004101"}', NULL, 'insert', '18', '2015-01-15 11:42:10.463937', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('752', 'committed', '843', '{"843": "OCC004501"}', NULL, 'insert', '18', '2015-02-03 07:03:05.578624', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('899', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-04 07:54:29.346823', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('909', 'committed', '1131', '{"1131": "OCC005101"}', NULL, 'insert', '18', '2015-03-05 01:26:02.774128', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('920', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '18', '2015-03-05 02:59:42.789082', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('974', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '18', '2015-03-05 10:53:45.708878', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('975', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '18', '2015-03-05 11:01:55.992705', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('979', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-10 00:52:42.273317', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('982', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '18', '2015-03-10 08:07:48.963723', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('983', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '18', '2015-03-10 09:37:41.868953', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('999', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-17 00:20:24.698434', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1002', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-18 05:27:38.020266', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1007', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-03-18 08:50:36.42108', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1008', 'committed', '1015', '{"1015": "OCC007401"}', NULL, 'insert', '30', '2015-03-18 08:51:39.41506', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1009', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-18 08:52:47.482267', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1010', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-03-18 08:53:50.360059', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1094', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-24 02:12:13.778966', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1095', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-03-24 14:02:02.581962', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1097', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-24 14:13:13.060953', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1098', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-03-24 14:21:04.178188', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1099', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 14:26:53.217497', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1101', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:35:58.25561', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1102', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:39:14.434059', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1103', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-24 15:42:45.751804', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1104', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:44:18.723872', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1105', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:46:07.55934', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1106', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:50:12.875029', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1107', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-24 15:55:35.658529', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1109', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-25 08:51:35.771353', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1110', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 08:57:51.389329', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1111', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:00:12.225641', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1112', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:23:46.295144', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1113', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:25:40.62184', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1114', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:29:20.872108', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1115', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:37:01.362942', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1116', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 09:41:31.610191', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1117', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 10:17:24.245981', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1118', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 10:21:15.374749', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1119', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 10:35:32.055914', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1125', 'committed', '1037', '{"1037": "OCC007601"}', NULL, 'insert', '30', '2015-03-25 14:20:02.29797', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1128', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-03-25 14:24:38.300308', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1134', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-03-25 14:32:00.453008', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1135', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-25 14:55:46.819518', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1136', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-25 15:01:09.245297', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1143', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-25 15:41:04.020719', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1160', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-03-27 13:12:58.096028', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1184', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-30 15:44:35.128119', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1185', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-03-30 15:55:46.164793', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1190', 'committed', '1015', '{"1015": "OCC007401"}', NULL, 'insert', '30', '2015-03-31 07:49:57.260326', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1191', 'committed', '1015', '{"1015": "OCC007401"}', NULL, 'insert', '30', '2015-03-31 07:55:03.884719', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1192', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-03-31 07:59:05.278167', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1193', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-03-31 08:10:40.977886', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1227', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-31 08:26:54.177814', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1228', 'committed', '1037', '{"1037": "OCC007601"}', NULL, 'insert', '30', '2015-03-31 08:26:54.185353', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1249', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-31 10:50:06.676167', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1258', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-31 12:12:39.82589', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1259', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-03-31 12:40:09.104962', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1267', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-06 06:27:52.491736', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1271', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-07 06:47:22.280098', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1272', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-07 07:00:50.485396', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1273', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-07 07:38:41.504458', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1277', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-07 13:05:29.011127', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1279', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-04-07 14:08:27.816127', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1285', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-04-08 07:14:54.005171', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1286', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-04-08 07:36:56.029628', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1288', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-04-08 11:27:05.593858', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1301', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-10 10:12:56.83911', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1318', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-13 15:29:33.920796', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1320', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-04-14 15:34:39.838742', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1326', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-15 11:13:03.570463', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1337', 'committed', '1105', '{"1105": "OCC008501"}', NULL, 'insert', '18', '2015-04-16 17:11:49.160538', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1338', 'committed', '1110', '{"1110": "OCC008601"}', NULL, 'insert', '18', '2015-04-16 17:12:12.198489', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1340', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-04-17 08:53:34.502252', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1349', 'committed', '1405', '{"1405": "OCC010101"}', NULL, 'insert', '18', '2015-04-17 10:48:36.261848', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1360', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-04-20 09:03:10.926481', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1405', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-04-23 10:58:29.24983', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1406', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-04-23 14:04:32.061588', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1409', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-04-23 15:22:34.671912', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1419', 'committed', '916', '{"916": "OCC006501"}', NULL, 'insert', '67', '2015-04-28 15:58:12.029572', NULL, '67');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1477', 'committed', '1073', '{"1073": "OCC008901"}', NULL, 'insert', '18', '2015-05-02 12:54:04.488571', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1489', 'committed', '1100', '{"1100": "OCC006001"}', NULL, 'insert', '18', '2015-05-04 17:38:39.262514', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1490', 'committed', '1099', '{"1099": "OCC005901"}', NULL, 'insert', '18', '2015-05-04 17:45:15.069972', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1513', 'committed', '1100', '{"1100": "OCC006001"}', NULL, 'insert', '184', '2015-05-05 16:27:07.321396', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1515', 'committed', '1099', '{"1099": "OCC005901"}', NULL, 'insert', '18', '2015-05-05 18:20:06.531328', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1518', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '30', '2015-05-06 13:39:05.04546', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1519', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-05-06 15:00:45.809341', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1520', 'committed', '1016', '{"1016": "OCC008401"}', NULL, 'insert', '30', '2015-05-06 15:08:42.179176', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1521', 'committed', '1017', '{"1017": "OCC005301"}', NULL, 'insert', '30', '2015-05-06 15:15:37.797392', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1522', 'committed', '1131', '{"1131": "OCC005101"}', NULL, 'insert', '27', '2015-05-06 15:55:41.498516', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1523', 'committed', '1129', '{"1129": "OCC005201"}', NULL, 'insert', '27', '2015-05-06 16:07:56.104456', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1529', 'committed', '1100', '{"1100": "OCC006001"}', NULL, 'insert', '81', '2015-05-07 10:23:40.853454', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1530', 'committed', '1099', '{"1099": "OCC005901"}', NULL, 'insert', '81', '2015-05-07 10:27:05.910175', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1535', 'committed', '1110', '{"1110": "OCC008601"}', NULL, 'insert', '18', '2015-05-07 14:32:45.45281', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1537', 'committed', '1105', '{"1105": "OCC008501"}', NULL, 'insert', '81', '2015-05-07 14:54:25.24234', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1569', 'committed', '1037', '{"1037": "OCC007601"}', NULL, 'insert', '30', '2015-05-08 13:41:07.940553', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1571', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-05-08 14:03:24.658008', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1572', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '30', '2015-05-08 14:06:09.452322', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1579', 'committed', '919', '{"919": "OCC007501"}', NULL, 'insert', '27', '2015-05-08 15:37:56.013886', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1589', 'committed', '1358', '{"1358": "OCC010001"}', NULL, 'insert', '184', '2015-05-11 11:57:56.158757', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1596', 'committed', '1004', '{"1004": "OCC007201"}', NULL, 'insert', '27', '2015-05-13 09:18:34.492366', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1598', 'committed', '1073', '{"1073": "OCC008901"}', NULL, 'insert', '27', '2015-05-13 09:22:16.482702', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1604', 'committed', '1005', '{"1005": "OCC009201"}', NULL, 'insert', '18', '2015-05-15 17:23:59.801743', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1606', 'committed', '1036', '{"1036": "OCC009101"}', NULL, 'insert', '18', '2015-05-15 17:35:59.163037', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1607', 'committed', '1037', '{"1037": "OCC007601"}', NULL, 'insert', '18', '2015-05-15 18:01:28.665997', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1618', 'committed', '903', '{"903": "OCC009901"}', NULL, 'insert', '82', '2015-05-18 18:25:50.513275', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1621', 'committed', '1028', '{"1028": "OCC005701"}', NULL, 'insert', '82', '2015-05-18 18:37:43.616542', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1622', 'committed', '895', '{"895": "OCC006601"}', NULL, 'insert', '82', '2015-05-18 18:54:41.343904', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1623', 'committed', '894', '{"894": "OCC006801"}', NULL, 'insert', '82', '2015-05-18 18:55:41.418896', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1626', 'committed', '1003', '{"1003": "OCC007301"}', NULL, 'insert', '27', '2015-05-19 09:21:32.840589', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1659', 'committed', '1361', '{"1361": "OCC010002"}', NULL, 'insert', '184', '2015-05-21 15:57:56.729873', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1661', 'committed', '1362', '{"1362": "OCC009502"}', NULL, 'insert', '184', '2015-05-21 16:00:00.541059', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1783', 'committed', '893', '{"893": "OCC007101"}', NULL, 'insert', '82', '2015-06-03 12:13:47.504845', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1797', 'committed', '916', '{"916": "OCC006501"}', NULL, 'insert', '82', '2015-06-04 16:47:05.130364', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1847', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '53', '2015-06-05 14:56:44.105598', NULL, '53');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1899', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '1', '2015-06-10 17:03:59.686624', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1900', 'committed', '1125', '{"1125": "OCC009701"}', NULL, 'insert', '1', '2015-06-10 17:04:39.037493', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1901', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '1', '2015-06-10 17:19:55.390652', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1902', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '1', '2015-06-10 17:28:39.339684', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1903', 'committed', '1125', '{"1125": "OCC009701"}', NULL, 'insert', '1', '2015-06-10 17:29:11.310992', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1931', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '30', '2015-06-18 13:25:17.071767', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1946', 'committed', '1110', '{"1110": "OCC008601"}', NULL, 'insert', '18', '2015-06-26 16:28:34.774243', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1947', 'committed', '1358', '{"1358": "OCC010001"}', NULL, 'insert', '18', '2015-06-26 19:08:00.732435', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1948', 'committed', '1360', '{"1360": "OCC009501"}', NULL, 'insert', '18', '2015-06-26 19:21:09.262355', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1949', 'committed', '1360', '{"1360": "OCC009501"}', NULL, 'insert', '18', '2015-06-26 19:24:32.201954', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1950', 'committed', '1361', '{"1361": "OCC010002"}', NULL, 'insert', '18', '2015-06-26 19:27:53.882729', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1951', 'committed', '1362', '{"1362": "OCC009502"}', NULL, 'insert', '18', '2015-06-26 19:35:02.141275', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1979', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '18', '2015-07-04 16:00:44.092409', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1989', 'committed', '1015', '{"1015": "OCC007401"}', NULL, 'insert', '30', '2015-07-10 14:15:37.132336', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('1992', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '18', '2015-07-12 22:54:19.755774', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2002', 'committed', '385', '{"385": "OCC002701"}', NULL, 'insert', '18', '2015-07-19 22:44:53.65694', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2008', 'committed', '1047', '{"1047": "OCC007701"}', NULL, 'insert', '30', '2015-07-22 10:35:36.036021', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2010', 'committed', '1085', '{"1085": "OCC009001"}', NULL, 'insert', '18', '2015-07-26 22:14:06.385085', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2011', 'committed', '1572', '{"1572": "OCC002801"}', NULL, 'insert', '18', '2015-07-27 16:41:25.416792', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2037', 'committed', '1408', '{"1408": "OCC006101"}', NULL, 'insert', '18', '2015-08-18 11:57:56.510505', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2039', 'committed', '1408', '{"1408": "OCC006101"}', NULL, 'insert', '18', '2015-08-18 12:23:12.671131', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2040', 'committed', '1384', '{"1384": "OCC005601"}', NULL, 'insert', '18', '2015-08-18 12:34:38.492533', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2043', 'committed', '1396', '{"1396": "OCC005801"}', NULL, 'insert', '18', '2015-08-18 20:10:46.748021', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2044', 'committed', '1383', '{"1383": "OCC008201"}', NULL, 'insert', '18', '2015-08-18 21:25:49.889074', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2045', 'committed', '1382', '{"1382": "OCC007901"}', NULL, 'insert', '18', '2015-08-19 08:46:50.536691', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2104', 'committed', '1396', '{"1396": "OCC005801"}', NULL, 'insert', '18', '2015-08-28 15:58:01.006376', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2105', 'committed', '1382', '{"1382": "OCC007901"}', NULL, 'insert', '18', '2015-08-28 16:09:12.57296', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2106', 'committed', '1383', '{"1383": "OCC008201"}', NULL, 'insert', '18', '2015-08-28 16:25:28.573385', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2107', 'committed', '1408', '{"1408": "OCC006101"}', NULL, 'insert', '18', '2015-08-28 16:33:36.647664', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2109', 'committed', '903', '{"903": "OCC009901"}', NULL, 'insert', '18', '2015-09-03 08:29:41.506831', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2116', 'committed', '1028', '{"1028": "OCC005701"}', NULL, 'insert', '18', '2015-09-03 17:27:09.797682', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2219', 'committed', '1099', '{"1099": "OCC005901"}', NULL, 'insert', '1', '2015-09-18 11:31:33.222288', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2220', 'committed', '1114', '{"1114": "OCC009801"}', NULL, 'insert', '1', '2015-09-18 11:54:56.552582', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2221', 'committed', '1105', '{"1105": "OCC008501"}', NULL, 'insert', '1', '2015-09-18 12:01:30.866061', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2222', 'committed', '1110', '{"1110": "OCC008601"}', NULL, 'insert', '1', '2015-09-18 12:02:26.597631', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2226', 'committed', '1360', '{"1360": "OCC009501"}', NULL, 'insert', '1', '2015-09-18 13:30:38.364122', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2233', 'committed', '1413', '{"1413": "OCC006301"}', NULL, 'insert', '18', '2015-09-18 15:19:12.625198', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2234', 'committed', '1414', '{"1414": "OCC006201"}', NULL, 'insert', '18', '2015-09-18 15:20:03.612412', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2239', 'committed', '1411', '{"1411": "OCC008701"}', NULL, 'insert', '18', '2015-09-18 15:39:22.660854', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2240', 'committed', '1412', '{"1412": "OCC008801"}', NULL, 'insert', '18', '2015-09-18 15:44:39.825173', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2303', 'committed', '1798', '{"1798": "OCC014401"}', NULL, 'insert', '30', '2015-10-01 11:01:31.319355', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2380', 'committed', '1362', '{"1362": "OCC009502"}', NULL, 'insert', '18', '2015-10-09 10:04:34.31024', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2381', 'committed', '1361', '{"1361": "OCC010002"}', NULL, 'insert', '18', '2015-10-09 10:12:22.229376', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2382', 'committed', '1360', '{"1360": "OCC009501"}', NULL, 'insert', '18', '2015-10-09 10:12:24.234841', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2383', 'committed', '1358', '{"1358": "OCC010001"}', NULL, 'insert', '18', '2015-10-09 10:12:26.148492', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2384', 'committed', '1115', '{"1115": "OCC009401"}', NULL, 'insert', '18', '2015-10-09 10:33:12.606741', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2402', 'committed', '1114', '{"1114": "OCC009801"}', NULL, 'insert', '18', '2015-10-09 17:13:07.784131', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2404', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '18', '2015-10-09 17:20:57.594967', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2405', 'committed', '1125', '{"1125": "OCC009701"}', NULL, 'insert', '18', '2015-10-09 17:25:41.795877', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2406', 'committed', '1115', '{"1115": "OCC009401"}', NULL, 'insert', '18', '2015-10-09 17:32:21.157747', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2407', 'committed', '1114', '{"1114": "OCC009801"}', NULL, 'insert', '18', '2015-10-09 17:32:24.943164', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2408', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '18', '2015-10-09 17:32:28.740904', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2409', 'committed', '1125', '{"1125": "OCC009701"}', NULL, 'insert', '18', '2015-10-09 17:32:33.610361', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2411', 'committed', '1115', '{"1115": "OCC009401"}', NULL, 'insert', '18', '2015-10-09 17:36:42.208053', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2412', 'committed', '1114', '{"1114": "OCC009801"}', NULL, 'insert', '18', '2015-10-09 17:36:42.707529', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2413', 'committed', '1124', '{"1124": "OCC009601"}', NULL, 'insert', '18', '2015-10-09 17:36:43.198109', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2414', 'committed', '1125', '{"1125": "OCC009701"}', NULL, 'insert', '18', '2015-10-09 17:36:44.102384', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2415', 'committed', '1362', '{"1362": "OCC009502"}', NULL, 'insert', '18', '2015-10-09 17:36:45.001919', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2416', 'committed', '1361', '{"1361": "OCC010002"}', NULL, 'insert', '18', '2015-10-09 17:36:45.425031', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2417', 'committed', '1360', '{"1360": "OCC009501"}', NULL, 'insert', '18', '2015-10-09 17:36:45.85407', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2418', 'committed', '1358', '{"1358": "OCC010001"}', NULL, 'insert', '18', '2015-10-09 17:36:46.284512', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2422', 'committed', '1114', '{"1114": "OCC009801"}', NULL, 'insert', '18', '2015-10-09 17:52:11.069681', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2423', 'committed', '1115', '{"1115": "OCC009401"}', NULL, 'insert', '18', '2015-10-09 17:52:11.319149', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2511', 'committed', '1764', '{"1764": "OCC013401"}', NULL, 'insert', '184', '2015-10-20 16:41:59.474206', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2512', 'committed', '1763', '{"1763": "OCC013501"}', NULL, 'insert', '184', '2015-10-20 16:43:15.320459', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2513', 'committed', '1765', '{"1765": "OCC011601"}', NULL, 'insert', '184', '2015-10-20 16:45:07.815962', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2514', 'committed', '1766', '{"1766": "OCC011301"}', NULL, 'insert', '184', '2015-10-20 16:48:31.149764', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2594', 'committed', '1769', '{"1769": "OCC012001"}', NULL, 'insert', '184', '2015-10-28 10:34:03.402778', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2595', 'committed', '1771', '{"1771": "OCC011101"}', NULL, 'insert', '184', '2015-10-28 10:34:48.527786', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2596', 'committed', '1785', '{"1785": "OCC012002"}', NULL, 'insert', '184', '2015-10-28 10:35:13.082404', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2597', 'committed', '1786', '{"1786": "OCC011102"}', NULL, 'insert', '184', '2015-10-28 10:35:35.748467', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2621', 'committed', '1567', '{"1567": "OCC012201"}', NULL, 'insert', '67', '2015-11-02 16:39:31.978487', NULL, '67');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2630', 'committed', '1763', '{"1763": "OCC013501"}', NULL, 'insert', '19', '2015-11-03 14:17:11.475398', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2634', 'committed', '1764', '{"1764": "OCC013401"}', NULL, 'insert', '19', '2015-11-03 16:32:42.090785', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2665', 'committed', '1765', '{"1765": "OCC011601"}', NULL, 'insert', '19', '2015-11-06 08:47:38.03062', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2672', 'committed', '1746', '{"1746": "OCC012701"}', NULL, 'insert', '82', '2015-11-06 10:25:20.917579', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2673', 'committed', '1766', '{"1766": "OCC011301"}', NULL, 'insert', '19', '2015-11-06 10:26:15.351648', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2705', 'committed', '1769', '{"1769": "OCC012001"}', NULL, 'insert', '18', '2015-11-09 14:08:33.387807', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2752', 'committed', '1797', '{"1797": "OCC013001"}', NULL, 'insert', '30', '2015-11-11 15:09:23.920394', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2753', 'committed', '1797', '{"1797": "OCC013001"}', NULL, 'insert', '30', '2015-11-11 15:20:54.47543', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2755', 'committed', '1697', '{"1697": "OCC010801"}', NULL, 'insert', '27', '2015-11-11 15:33:46.445905', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2756', 'committed', '1792', '{"1792": "OCC010701"}', NULL, 'insert', '27', '2015-11-11 15:35:03.820755', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2831', 'committed', '1765', '{"1765": "OCC011601"}', NULL, 'insert', '18', '2015-11-19 13:32:18.917576', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2838', 'committed', '1766', '{"1766": "OCC011301"}', NULL, 'insert', '18', '2015-11-19 14:53:06.28441', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2839', 'committed', '1763', '{"1763": "OCC013501"}', NULL, 'insert', '18', '2015-11-19 15:01:28.546648', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2840', 'committed', '1764', '{"1764": "OCC013401"}', NULL, 'insert', '18', '2015-11-19 15:21:47.085797', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2843', 'committed', '1769', '{"1769": "OCC012001"}', NULL, 'insert', '18', '2015-11-19 16:42:25.507974', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2845', 'committed', '1771', '{"1771": "OCC011101"}', NULL, 'insert', '18', '2015-11-19 16:47:18.693599', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2846', 'committed', '1785', '{"1785": "OCC012002"}', NULL, 'insert', '18', '2015-11-19 16:54:11.767535', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2848', 'committed', '1786', '{"1786": "OCC011102"}', NULL, 'insert', '18', '2015-11-19 17:01:31.89217', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2868', 'committed', '1520', '{"1520": "OCC011001"}', NULL, 'insert', '82', '2015-11-24 16:41:09.455393', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2869', 'committed', '907', '{"907": "OCC009301"}', NULL, 'insert', '18', '2015-11-24 17:04:29.194488', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2875', 'committed', '1798', '{"1798": "OCC014401"}', NULL, 'insert', '30', '2015-11-25 15:02:11.748849', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2877', 'committed', '1798', '{"1798": "OCC014401"}', NULL, 'insert', '30', '2015-11-26 11:18:47.910295', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2884', 'committed', '1489', '{"1489": "OCC010501"}', NULL, 'insert', '27', '2015-11-26 13:25:44.229218', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2885', 'committed', '1855', '{"1855": "OCC014501"}', NULL, 'insert', '30', '2015-11-26 13:45:20.797724', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2886', 'committed', '1861', '{"1861": "OCC014601"}', NULL, 'insert', '30', '2015-11-26 14:02:39.09734', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2908', 'committed', '1521', '{"1521": "OCC012301"}', NULL, 'insert', '82', '2015-12-01 16:31:26.076352', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2928', 'committed', '1567', '{"1567": "OCC012201"}', NULL, 'insert', '18', '2015-12-07 16:05:32.732771', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2929', 'committed', '1567', '{"1567": "OCC012201"}', NULL, 'insert', '18', '2015-12-07 16:07:59.400643', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2952', 'committed', '1621', '{"1621": "OCC012501"}', NULL, 'insert', '82', '2015-12-10 16:21:37.923523', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2953', 'committed', '1840', '{"1840": "OCC014701"}', NULL, 'insert', '18', '2015-12-10 21:01:48.618433', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2959', 'committed', '1798', '{"1798": "OCC014401"}', NULL, 'insert', '30', '2015-12-14 11:11:33.22556', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2965', 'committed', '1603', '{"1603": "OCC012601"}', NULL, 'insert', '82', '2015-12-15 10:32:18.44537', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2974', 'committed', '1889', '{"1889": "OCC012101"}', NULL, 'insert', '18', '2015-12-16 11:32:34.906816', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2975', 'committed', '1799', '{"1799": "OCC010901"}', NULL, 'insert', '18', '2015-12-16 11:40:10.346742', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2977', 'committed', '1801', '{"1801": "OCC013201"}', NULL, 'insert', '18', '2015-12-16 12:33:56.644593', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('2978', 'committed', '1800', '{"1800": "OCC013801"}', NULL, 'insert', '18', '2015-12-16 12:45:56.998489', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3017', 'committed', '1603', '{"1603": "OCC012601"}', NULL, 'insert', '18', '2016-01-08 19:15:48.077294', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3018', 'committed', '1621', '{"1621": "OCC012501"}', NULL, 'insert', '18', '2016-01-08 19:26:34.914735', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3019', 'committed', '1521', '{"1521": "OCC012301"}', NULL, 'insert', '18', '2016-01-08 19:26:39.728014', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3020', 'committed', '1520', '{"1520": "OCC011001"}', NULL, 'insert', '18', '2016-01-08 19:26:56.075174', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3059', 'committed', '1520', '{"1520": "OCC011001"}', NULL, 'insert', '18', '2016-01-13 17:46:30.504093', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3067', 'committed', '2026', '{"2026": "OCC014101"}', NULL, 'insert', '76', '2016-01-18 16:26:47.089619', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3102', 'committed', '1825', '{"1825": "OCC014801"}', NULL, 'insert', '18', '2016-01-26 08:57:10.245885', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3118', 'committed', '1825', '{"1825": "OCC014801"}', NULL, 'insert', '30', '2016-01-28 10:18:09.686985', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3119', 'committed', '1841', '{"1841": "OCC013101"}', NULL, 'insert', '30', '2016-01-28 10:33:46.414835', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3127', 'committed', '1798', '{"1798": "OCC014401"}', NULL, 'insert', '18', '2016-02-01 13:42:33.536882', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3157', 'committed', '2177', '{"2177": "OCC014001"}', NULL, 'insert', '19', '2016-02-09 15:45:45.638367', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3171', 'committed', '2178', '{"2178": "OCC011501"}', NULL, 'insert', '76', '2016-02-16 16:43:30.801298', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3193', 'committed', '1793', '{"1793": "OCC010601"}', NULL, 'insert', '18', '2016-02-19 08:23:46.00432', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3198', 'committed', '1796', '{"1796": "OCC013701"}', NULL, 'insert', '18', '2016-02-19 14:11:55.550817', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3199', 'committed', '1888', '{"1888": "OCC013601"}', NULL, 'insert', '18', '2016-02-19 14:22:54.843431', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3220', 'committed', '2179', '{"2179": "OCC011201"}', NULL, 'insert', '76', '2016-02-29 17:46:01.275121', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3258', 'committed', '1888', '{"1888": "OCC013601"}', NULL, 'insert', '18', '2016-03-15 10:35:53.416923', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3259', 'committed', '1796', '{"1796": "OCC013701"}', NULL, 'insert', '18', '2016-03-15 10:35:56.917447', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3260', 'committed', '1793', '{"1793": "OCC010601"}', NULL, 'insert', '18', '2016-03-15 10:36:02.985006', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3262', 'committed', '1796', '{"1796": "OCC013701"}', NULL, 'insert', '18', '2016-03-15 10:46:23.418777', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3263', 'committed', '1888', '{"1888": "OCC013601"}', NULL, 'insert', '18', '2016-03-15 10:48:09.962324', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3264', 'committed', '1796', '{"1796": "OCC013701"}', NULL, 'insert', '18', '2016-03-15 10:48:13.580372', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3267', 'committed', '1621', '{"1621": "OCC012501"}', NULL, 'insert', '18', '2016-03-15 11:24:26.871147', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3270', 'committed', '1603', '{"1603": "OCC012601"}', NULL, 'insert', '18', '2016-03-15 12:39:50.392292', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3272', 'committed', '1521', '{"1521": "OCC012301"}', NULL, 'insert', '18', '2016-03-15 16:06:41.856685', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3520', 'committed', '2166', '{"2166": "OCC015901"}', NULL, 'insert', '67', '2016-04-15 11:43:17.585982', NULL, '67');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3533', 'committed', '2134', '{"2134": "OCC017401"}', NULL, 'insert', '184', '2016-04-18 13:55:09.692358', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3554', 'committed', '2232', '{"2232": "OCC015201"}', NULL, 'insert', '184', '2016-04-21 16:09:14.753295', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3570', 'committed', '2184', '{"2184": "OCC017801"}', NULL, 'insert', '184', '2016-04-26 16:01:28.810214', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3571', 'committed', '2181', '{"2181": "OCC015101"}', NULL, 'insert', '184', '2016-04-26 16:06:18.175365', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3578', 'committed', '2442', '{"2442": "OCC017201"}', NULL, 'insert', '30', '2016-04-27 15:21:24.364513', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3583', 'committed', '2442', '{"2442": "OCC017201"}', NULL, 'insert', '30', '2016-04-28 14:00:42.413746', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3586', 'committed', '2442', '{"2442": "OCC017201"}', NULL, 'insert', '18', '2016-04-28 14:39:32.394531', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3627', 'committed', '2442', '{"2442": "OCC017201"}', NULL, 'insert', '18', '2016-04-29 13:08:40.880593', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3636', 'committed', '2224', '{"2224": "OCC017901"}', NULL, 'insert', '184', '2016-05-02 16:04:20.011113', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3637', 'committed', '2183', '{"2183": "OCC018001"}', NULL, 'insert', '184', '2016-05-02 16:04:45.088274', NULL, '184');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3642', 'committed', '2323', '{"2323": "OCC017701"}', NULL, 'insert', '30', '2016-05-03 14:13:57.901233', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3695', 'committed', '2232', '{"2232": "OCC015201"}', NULL, 'insert', '82', '2016-05-13 09:46:40.454951', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3701', 'committed', '2332', '{"2332": "OCC015601"}', NULL, 'insert', '81', '2016-05-16 08:30:57.19634', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3702', 'committed', '2338', '{"2338": "OCC015301"}', NULL, 'insert', '81', '2016-05-16 08:32:16.076751', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3703', 'committed', '2134', '{"2134": "OCC017401"}', NULL, 'insert', '81', '2016-05-16 08:34:46.773978', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3861', 'committed', '2235', '{"2235": "OCC017601"}', NULL, 'insert', '82', '2016-05-30 17:47:14.592867', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3865', 'committed', '2233', '{"2233": "OCC016101"}', NULL, 'insert', '82', '2016-05-31 09:16:56.619584', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3866', 'committed', '2232', '{"2232": "OCC015201"}', NULL, 'insert', '82', '2016-05-31 09:32:24.928659', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3914', 'committed', '1956', '{"1956": "OCC014201"}', NULL, 'insert', '18', '2016-06-04 20:25:39.125696', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3915', 'committed', '1955', '{"1955": "OCC014301"}', NULL, 'insert', '18', '2016-06-04 21:22:02.580555', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3949', 'committed', '2324', '{"2324": "OCC017101"}', NULL, 'insert', '30', '2016-06-10 14:33:13.685433', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3952', 'committed', '2442', '{"2442": "OCC017201"}', NULL, 'insert', '30', '2016-06-10 15:39:37.191063', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3957', 'committed', '2166', '{"2166": "OCC015901"}', NULL, 'insert', '82', '2016-06-14 09:55:25.727772', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3962', 'committed', '2233', '{"2233": "OCC016101"}', NULL, 'insert', '82', '2016-06-14 15:29:52.089349', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3963', 'committed', '2255', '{"2255": "OCC017001"}', NULL, 'insert', '27', '2016-06-14 16:05:45.848767', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3970', 'committed', '2181', '{"2181": "OCC015101"}', NULL, 'insert', '27', '2016-06-15 13:09:04.85342', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3971', 'committed', '2224', '{"2224": "OCC017901"}', NULL, 'insert', '27', '2016-06-15 13:10:47.714741', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3972', 'committed', '2184', '{"2184": "OCC017801"}', NULL, 'insert', '27', '2016-06-15 13:11:44.510559', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3973', 'committed', '2183', '{"2183": "OCC018001"}', NULL, 'insert', '27', '2016-06-15 13:12:47.595879', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('3974', 'committed', '2254', '{"2254": "OCC016901"}', NULL, 'insert', '27', '2016-06-15 14:48:59.96704', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4003', 'committed', '2181', '{"2181": "OCC015101"}', NULL, 'insert', '18', '2016-06-25 11:42:44.809299', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4022', 'committed', '2181', '{"2181": "OCC015101"}', NULL, 'insert', '18', '2016-06-29 11:37:50.745387', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4049', 'committed', '2247', '{"2247": "OCC015801"}', NULL, 'insert', '81', '2016-07-14 14:43:27.364876', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4050', 'committed', '2248', '{"2248": "OCC015401"}', NULL, 'insert', '81', '2016-07-14 14:45:01.413788', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4051', 'committed', '2335', '{"2335": "OCC015802"}', NULL, 'insert', '81', '2016-07-14 14:46:45.253603', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4052', 'committed', '2336', '{"2336": "OCC015402"}', NULL, 'insert', '81', '2016-07-14 14:48:00.433177', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4053', 'committed', '2249', '{"2249": "OCC015501"}', NULL, 'insert', '81', '2016-07-14 14:49:37.048809', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4255', 'committed', '2527', '{"2527": "OCC022001"}', NULL, 'insert', '30', '2016-08-16 09:31:08.296959', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4363', 'committed', '2527', '{"2527": "OCC022001"}', NULL, 'insert', '30', '2016-08-31 14:29:32.328493', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4402', 'committed', '2707', '{"2707": "OCC021601"}', 'z1DA8WEOMaSbBecU3iVY5YAXEtM=', 'insert', '30', '2016-09-05 10:14:20.137991', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4459', 'committed', '2707', '{"2707": "OCC021601"}', 'i/RrzVqnG8f+1NPATwcnfY5p/Qw=', 'insert', '30', '2016-09-07 10:08:33.68147', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4460', 'committed', '2707', '{"2707": "OCC021601"}', 'lB09YcyKAzCLAzum0itthdBCFRI=', 'insert', '30', '2016-09-07 10:31:40.858425', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4493', 'committed', '2707', '{"2707": "OCC021601"}', 'EVwJTVw9NWPht2Rwf2GQy9epMyo=', 'insert', '30', '2016-09-08 07:36:42.583075', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4494', 'committed', '2707', '{"2707": "OCC021601"}', 'iUg/HCKe+7lCnWb8h3nbOh5p9iY=', 'insert', '30', '2016-09-08 09:07:47.693438', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4496', 'committed', '2707', '{"2707": "OCC021601"}', 'A3QDTmuO8D0q59ipPa26nJCKc0Q=', 'insert', '30', '2016-09-08 12:29:45.459774', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4497', 'committed', '2707', '{"2707": "OCC021601"}', 'ThDRfFP96IjLmFx5YLipzmBm4WM=', 'insert', '30', '2016-09-08 12:30:19.718046', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4520', 'committed', '2939', '{"2939": "OCC022401"}', 'u+9WmVYu+mvHpFit79TChQY9Lio=', 'insert', '30', '2016-09-13 13:56:24.418968', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4521', 'committed', '2707', '{"2707": "OCC021601"}', 'FAvuG1Kv0MKbrSjUaX7fvzz51QE=', 'insert', '30', '2016-09-13 13:56:31.185309', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4546', 'committed', '2707', '{"2707": "OCC021601"}', 'fTYHU0XsGItOJCeZju1aAlEJgnk=', 'insert', '30', '2016-09-14 08:27:08.543492', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4618', 'committed', '2707', '{"2707": "OCC021601"}', 'J9E7SeTiXhfXn2ToVFvQmSUN/fo=', 'insert', '30', '2016-09-19 13:30:26.660607', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4633', 'committed', '2707', '{"2707": "OCC021601"}', 'LHu561jnKBJIC4X5NtOe8x6HE58=', 'insert', '30', '2016-09-20 10:05:07.077514', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4645', 'committed', '2707', '{"2707": "OCC021601"}', 'AF/sPAI3+x7FObRxS+Spj8rJOQA=', 'insert', '30', '2016-09-21 10:10:26.875322', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4662', 'committed', '2939', '{"2939": "OCC022401"}', 'O8TKHCO0VZxnTUGarNXe1Dyr0m8=', 'insert', '30', '2016-09-22 15:09:29.831119', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4686', 'committed', '2707', '{"2707": "OCC021601"}', 'OPxde9W9yxjq3G0QWOOg0WDvzV8=', 'insert', '30', '2016-09-26 13:11:55.720501', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4696', 'committed', '2707', '{"2707": "OCC021601"}', 'SgYxBK3W3B4D/YMCwox1yFW0+CM=', 'insert', '30', '2016-09-28 10:26:35.753106', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4697', 'committed', '2707', '{"2707": "OCC021601"}', 'WK3RFUETUCPF1Mwd6PW/UFBJUh4=', 'insert', '30', '2016-09-28 13:13:46.31278', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4740', 'committed', '2707', '{"2707": "OCC021601"}', '2TNiAF5eKmI6XFLcaiHBut3nirA=', 'insert', '30', '2016-09-30 09:03:40.22198', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4747', 'committed', '2707', '{"2707": "OCC021601"}', 'tffxMTvVK8Glhdp+LMmszGEZKEA=', 'insert', '30', '2016-09-30 14:58:28.815327', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4771', 'committed', '2707', '{"2707": "OCC021601"}', 'TEfHPtYp9RZpxd5NGbvbGkzR/iY=', 'insert', '30', '2016-10-03 14:59:26.86308', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4772', 'committed', '2707', '{"2707": "OCC021601"}', 'cwKBspEWY05XFm2Nf8sTPJ6F3YA=', 'insert', '30', '2016-10-03 15:02:10.020739', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4773', 'committed', '2939', '{"2939": "OCC022401"}', 'nT9JvtERBIzeE+J6A50e1Aynz6Q=', 'insert', '30', '2016-10-03 15:02:17.733202', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4785', 'committed', '2942', '{"2942": "OCC018701"}', NULL, 'insert', '18', '2016-10-06 09:33:40.428705', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4786', 'committed', '2707', '{"2707": "OCC021601"}', 'FPN9OHhOPYv1HL6ReW0bUr8s4Lw=', 'insert', '30', '2016-10-06 09:44:14.713877', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4792', 'committed', '2942', '{"2942": "OCC018701"}', NULL, 'delete', '18', '2016-10-07 11:16:05.57826', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4801', 'committed', '2232', '{"2232": "OCC015201"}', NULL, 'insert', '18', '2016-10-07 15:42:56.77852', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4930', 'committed', '2707', '{"2707": "OCC021601"}', NULL, 'insert', '30', '2016-10-20 15:53:49.092359', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4944', 'committed', '2918', '{"2918": "OCC021201"}', NULL, 'insert', '18', '2016-10-24 09:54:12.379964', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4948', 'committed', '2932', '{"2932": "OCC019901"}', NULL, 'insert', '18', '2016-10-24 15:47:26.693481', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4949', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2016-10-24 15:53:55.719394', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4950', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '18', '2016-10-24 15:56:41.190952', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4955', 'committed', '2942', '{"2942": "OCC018701"}', 'I0moIgJctdCUnSVWwzYxIBPu+Qw=', 'insert', '30', '2016-10-26 10:40:31.643533', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('4956', 'committed', '2939', '{"2939": "OCC022401"}', '7W3A9r+/1O8kUNde1JRI5TYN3+s=', 'insert', '30', '2016-10-26 13:20:21.163953', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5018', 'committed', '2942', '{"2942": "OCC018701"}', '9GPt5KBtxtu4MrSWX0Ur3mxivik=', 'insert', '30', '2016-11-02 11:19:17.598336', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5042', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2016-11-04 11:16:16.676388', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5043', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '82', '2016-11-04 11:18:46.91109', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5044', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2016-11-04 13:26:06.207031', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5045', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'delete', '18', '2016-11-04 13:34:52.905595', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5048', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2016-11-04 15:30:11.052098', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5049', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2016-11-04 17:56:05.00299', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5055', 'committed', '2918', '{"2918": "OCC021201"}', NULL, 'insert', '82', '2016-11-07 11:06:59.436029', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5077', 'committed', '2332', '{"2332": "OCC015601"}', NULL, 'insert', '18', '2016-11-07 23:00:03.463472', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5078', 'committed', '2338', '{"2338": "OCC015301"}', NULL, 'insert', '18', '2016-11-07 23:11:02.742821', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5080', 'committed', '2134', '{"2134": "OCC017401"}', NULL, 'insert', '18', '2016-11-07 23:59:48.151057', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5081', 'committed', '2134', '{"2134": "OCC017401"}', NULL, 'insert', '18', '2016-11-08 08:34:21.241488', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5083', 'committed', '2918', '{"2918": "OCC021201"}', NULL, 'insert', '18', '2016-11-08 09:48:33.434977', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5097', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '82', '2016-11-08 11:13:15.314344', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5098', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '18', '2016-11-08 12:21:50.660254', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5100', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '18', '2016-11-08 13:36:29.816244', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5106', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '18', '2016-11-08 15:27:02.897287', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5115', 'committed', '2929', '{"2929": "OCC021301"}', NULL, 'insert', '82', '2016-11-09 15:50:58.794115', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5118', 'committed', '2918', '{"2918": "OCC021201"}', NULL, 'delete', '18', '2016-11-10 08:36:58.615676', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5132', 'committed', '3026', '{"3026": "OCC018401"}', NULL, 'insert', '19', '2016-11-11 09:19:18.415549', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5175', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '19', '2016-11-18 10:37:10.575144', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5274', 'committed', '3044', '{"3044": "OCC022301"}', NULL, 'insert', '76', '2016-11-24 15:43:01.53672', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5284', 'committed', '3044', '{"3044": "OCC022301"}', NULL, 'insert', '19', '2016-11-25 11:52:51.749521', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5294', 'committed', '3028', '{"3028": "OCC019701"}', NULL, 'insert', '76', '2016-11-25 15:49:52.702715', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5303', 'committed', '3043', '{"3043": "OCC022101"}', NULL, 'insert', '76', '2016-11-28 12:20:07.490653', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5309', 'committed', '3041', '{"3041": "OCC019501"}', NULL, 'insert', '76', '2016-11-28 15:46:53.450937', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5313', 'committed', '2929', '{"2929": "OCC021301"}', NULL, 'insert', '18', '2016-11-29 12:32:08.011259', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5314', 'committed', '2183', '{"2183": "OCC018001"}', NULL, 'delete', '18', '2016-11-29 13:13:33.070762', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5316', 'committed', '2676', '{"2676": "OCC019201"}', NULL, 'insert', '81', '2016-11-29 14:23:34.718532', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5317', 'committed', '2675', '{"2675": "OCC021701"}', NULL, 'insert', '81', '2016-11-29 14:26:10.056382', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5318', 'committed', '2837', '{"2837": "OCC018801"}', NULL, 'insert', '81', '2016-11-29 14:29:49.455105', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5319', 'committed', '2797', '{"2797": "OCC019001"}', NULL, 'insert', '81', '2016-11-29 14:30:53.476006', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5320', 'committed', '2795', '{"2795": "OCC018901"}', NULL, 'insert', '81', '2016-11-29 14:31:58.794988', NULL, '81');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5322', 'committed', '2312', '{"2312": "OCC014901"}', NULL, 'insert', '18', '2016-11-29 20:19:27.878187', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5323', 'committed', '2314', '{"2314": "OCC017301"}', NULL, 'insert', '18', '2016-11-29 21:55:44.633013', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5324', 'committed', '2412', '{"2412": "OCC017501"}', NULL, 'insert', '18', '2016-11-30 09:36:38.473112', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5325', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'delete', '18', '2016-11-30 13:11:15.696967', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5335', 'committed', '3042', '{"3042": "OCC022201"}', NULL, 'insert', '76', '2016-12-01 16:12:00.560181', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5346', 'committed', '2633', '{"2633": "OCC021401"}', NULL, 'insert', '18', '2016-12-01 21:29:01.364702', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5347', 'committed', '2634', '{"2634": "OCC021501"}', NULL, 'insert', '18', '2016-12-01 21:33:51.490339', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5353', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '6', '2016-12-02 11:42:25.499607', NULL, '6');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5354', 'committed', '2465', '{"2465": "OCC015701"}', NULL, 'insert', '18', '2016-12-02 12:05:45.918984', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5357', 'committed', '2466', '{"2466": "OCC015302"}', NULL, 'insert', '18', '2016-12-02 13:12:27.427407', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5358', 'committed', '2466', '{"2466": "OCC015302"}', NULL, 'insert', '18', '2016-12-02 13:20:40.286542', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5359', 'committed', '2465', '{"2465": "OCC015701"}', NULL, 'insert', '18', '2016-12-02 13:20:52.1406', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5361', 'committed', '2942', '{"2942": "OCC018701"}', '9pAOoLMXF0bcNBk1rotToQrrvmg=', 'insert', '30', '2016-12-02 14:43:27.619903', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5364', 'committed', '2666', '{"2666": "OCC021901"}', NULL, 'insert', '82', '2016-12-02 18:18:46.425275', NULL, '82');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5365', 'committed', '2312', '{"2312": "OCC014901"}', NULL, 'insert', '18', '2016-12-03 14:59:53.47452', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5366', 'committed', '2314', '{"2314": "OCC017301"}', NULL, 'insert', '18', '2016-12-03 15:03:32.848557', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5367', 'committed', '2412', '{"2412": "OCC017501"}', NULL, 'insert', '18', '2016-12-03 15:04:06.303693', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5369', 'committed', '2313', '{"2313": "OCC015001"}', NULL, 'insert', '18', '2016-12-03 15:38:54.029371', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5387', 'committed', '2608', '{"2608": "OCC019801"}', NULL, 'insert', '67', '2016-12-05 17:22:46.093987', NULL, '67');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5403', 'committed', '1989', '{"1989": "OCC011402"}', NULL, 'insert', '18', '2016-12-06 14:46:53.460512', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5404', 'committed', '1988', '{"1988": "OCC011702"}', NULL, 'insert', '18', '2016-12-06 14:54:25.438866', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5408', 'committed', '2468', '{"2468": "OCC015303"}', NULL, 'insert', '18', '2016-12-06 15:53:40.931138', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5409', 'committed', '2467', '{"2467": "OCC015702"}', NULL, 'insert', '18', '2016-12-06 15:55:44.412724', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5417', 'committed', '2467', '{"2467": "OCC015702"}', NULL, 'insert', '18', '2016-12-06 17:36:17.284962', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5418', 'committed', '2468', '{"2468": "OCC015303"}', NULL, 'insert', '18', '2016-12-06 17:36:45.163123', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5419', 'committed', '2335', '{"2335": "OCC015802"}', NULL, 'insert', '18', '2016-12-06 17:36:59.13117', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5420', 'committed', '2336', '{"2336": "OCC015402"}', NULL, 'insert', '18', '2016-12-06 17:37:06.51532', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5421', 'committed', '2249', '{"2249": "OCC015501"}', NULL, 'insert', '18', '2016-12-06 17:37:14.273359', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5422', 'committed', '2248', '{"2248": "OCC015401"}', NULL, 'insert', '18', '2016-12-06 17:37:41.665144', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5423', 'committed', '2235', '{"2235": "OCC017601"}', NULL, 'insert', '18', '2016-12-06 17:37:55.940805', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5424', 'committed', '2224', '{"2224": "OCC017901"}', NULL, 'insert', '18', '2016-12-06 17:38:32.951419', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5425', 'committed', '2183', '{"2183": "OCC018001"}', NULL, 'insert', '18', '2016-12-06 17:38:49.212414', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5426', 'committed', '2247', '{"2247": "OCC015801"}', NULL, 'insert', '18', '2016-12-06 17:39:02.06087', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5427', 'committed', '2233', '{"2233": "OCC016101"}', NULL, 'insert', '18', '2016-12-06 17:39:10.086157', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5432', 'committed', '3040', '{"3040": "OCC019601"}', NULL, 'insert', '76', '2016-12-07 11:03:00.520767', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5436', 'committed', '2739', '{"2739": "OCC021801"}', NULL, 'insert', '18', '2016-12-08 08:07:33.085557', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5440', 'committed', '3024', '{"3024": "OCC018501"}', NULL, 'insert', '76', '2016-12-08 10:05:34.832367', NULL, '76');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5445', 'committed', '2741', '{"2741": "OCC019401"}', NULL, 'insert', '18', '2016-12-08 11:10:50.42544', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5446', 'committed', '2740', '{"2740": "OCC021802"}', NULL, 'insert', '18', '2016-12-08 12:47:08.980223', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5447', 'committed', '2741', '{"2741": "OCC019401"}', NULL, 'delete', '18', '2016-12-08 12:54:32.581187', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5448', 'committed', '2740', '{"2740": "OCC021802"}', NULL, 'delete', '18', '2016-12-08 13:19:23.997284', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5449', 'committed', '1367', '{"1367": "OCC005501"}', NULL, 'insert', '18', '2016-12-08 13:40:19.366542', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5453', 'committed', '1367', '{"1367": "OCC005501"}', NULL, 'delete', '18', '2016-12-08 14:05:14.104589', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5467', 'committed', '2707', '{"2707": "OCC021601"}', NULL, 'insert', '30', '2016-12-09 09:01:21.269739', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5475', 'committed', '1200', '{"1200": "OCC006002"}', NULL, 'insert', '18', '2016-12-09 12:41:01.363136', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5478', 'committed', '1193', '{"1193": "OCC005903"}', NULL, 'insert', '18', '2016-12-09 13:00:11.990565', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5481', 'committed', '1191', '{"1191": "OCC005902"}', NULL, 'insert', '18', '2016-12-09 13:20:28.51985', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5482', 'committed', '1201', '{"1201": "OCC006003"}', NULL, 'insert', '18', '2016-12-09 13:31:27.925345', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5486', 'committed', '1377', '{"1377": "OCC007801"}', NULL, 'insert', '18', '2016-12-10 09:57:59.285627', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5487', 'committed', '1380', '{"1380": "OCC008101"}', NULL, 'insert', '18', '2016-12-10 10:16:19.342919', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5489', 'committed', '1794', '{"1794": "OCC013901"}', NULL, 'insert', '18', '2016-12-10 10:42:22.301208', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5490', 'committed', '1868', '{"1868": "OCC013301"}', NULL, 'insert', '18', '2016-12-10 10:54:55.965803', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5493', 'committed', '1380', '{"1380": "OCC008101"}', NULL, 'delete', '18', '2016-12-10 13:17:27.32774', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5494', 'committed', '1377', '{"1377": "OCC007801"}', NULL, 'delete', '18', '2016-12-10 13:22:03.243874', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5514', 'committed', '2632', '{"2632": "OCC018301"}', NULL, 'insert', '27', '2016-12-12 14:01:16.182025', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5518', 'committed', '2738', '{"2738": "OCC018101"}', NULL, 'insert', '27', '2016-12-12 16:32:46.092544', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5521', 'committed', '2632', '{"2632": "OCC018301"}', NULL, 'insert', '18', '2016-12-13 13:58:26.011198', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5533', 'committed', '2632', '{"2632": "OCC018301"}', NULL, 'delete', '18', '2016-12-14 13:15:36.1916', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5581', 'committed', '2702', '{"2702": "OCC019302"}', NULL, 'insert', '18', '2016-12-15 15:50:11.806607', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5714', 'committed', '2701', '{"2701": "OCC019301"}', NULL, 'insert', '18', '2017-01-03 10:56:18.395324', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5717', 'committed', '2939', '{"2939": "OCC022401"}', 'VQW1IpqHVjy7Dt8mQ6NhMJcgLCc=', 'insert', '30', '2017-01-03 15:02:49.931369', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5718', 'committed', '2701', '{"2701": "OCC019301"}', NULL, 'delete', '18', '2017-01-04 08:15:58.225282', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5719', 'committed', '2701', '{"2701": "OCC019301"}', NULL, 'delete', '18', '2017-01-04 08:22:32.942977', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5734', 'committed', '2016', '{"2016": "OCC016301"}', NULL, 'insert', '1', '2017-01-05 18:50:10.452482', NULL, '1');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5740', 'committed', '1378', '{"1378": "OCC008001"}', NULL, 'insert', '18', '2017-01-06 18:55:21.868497', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5741', 'committed', '1381', '{"1381": "OCC008301"}', NULL, 'insert', '18', '2017-01-06 19:19:00.554463', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5750', 'committed', '2674', '{"2674": "OCC018201"}', NULL, 'insert', '18', '2017-01-10 11:54:08.128096', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5756', 'committed', '2608', '{"2608": "OCC019801"}', NULL, 'insert', '18', '2017-01-10 15:58:49.084648', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5764', 'committed', '2932', '{"2932": "OCC019901"}', NULL, 'insert', '18', '2017-01-11 13:06:14.600273', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5765', 'committed', '2918', '{"2918": "OCC021201"}', NULL, 'insert', '18', '2017-01-11 14:13:17.747958', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5766', 'committed', '2846', '{"2846": "OCC020001"}', NULL, 'insert', '18', '2017-01-11 16:04:26.850971', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5770', 'committed', '1788', '{"1788": "OCC011801"}', NULL, 'insert', '18', '2017-01-12 08:35:21.463778', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5771', 'committed', '1789', '{"1789": "OCC011901"}', NULL, 'insert', '18', '2017-01-12 08:44:42.324754', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5772', 'committed', '1788', '{"1788": "OCC011801"}', NULL, 'insert', '18', '2017-01-12 08:52:43.537236', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5773', 'committed', '1788', '{"1788": "OCC011801"}', NULL, 'delete', '18', '2017-01-12 08:53:30.605158', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5780', 'committed', '2942', '{"2942": "OCC018701"}', 'Y6n0+FWUeZZCTndgdjoka3gkRQI=', 'insert', '30', '2017-01-13 13:59:53.637483', NULL, '30');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5781', 'committed', '2707', '{"2707": "OCC021601"}', NULL, 'insert', '18', '2017-01-13 15:08:31.232364', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5793', 'committed', '2707', '{"2707": "OCC021601"}', NULL, 'insert', '18', '2017-01-18 12:22:38.322137', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5799', 'committed', '1218', '{"1218": "OCC006004"}', NULL, 'insert', '18', '2017-01-20 09:10:37.988712', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5807', 'committed', '1217', '{"1217": "OCC005904"}', NULL, 'insert', '18', '2017-01-23 12:55:56.748355', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5808', 'committed', '2633', '{"2633": "OCC021401"}', NULL, 'insert', '18', '2017-01-23 13:27:16.295461', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('5809', 'committed', '2634', '{"2634": "OCC021501"}', NULL, 'insert', '18', '2017-01-23 13:46:23.504208', NULL, '18');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6602', 'committed', '3373', '{"3373": "OCC020601"}', NULL, 'insert', '27', '2017-04-25 15:17:38.19128', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6603', 'committed', '3376', '{"3376": "OCC020201"}', NULL, 'insert', '27', '2017-04-25 15:19:23.028152', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6605', 'committed', '3374', '{"3374": "OCC020701"}', NULL, 'insert', '27', '2017-04-25 15:44:17.879038', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6606', 'committed', '3375', '{"3375": "OCC020801"}', NULL, 'insert', '27', '2017-04-25 15:48:47.903088', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6607', 'committed', '3371', '{"3371": "OCC020501"}', NULL, 'insert', '27', '2017-04-25 15:56:42.911014', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6611', 'committed', '3370', '{"3370": "OCC020401"}', NULL, 'insert', '27', '2017-04-26 08:43:55.965929', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6811', 'committed', '2225', '{"2225": "OCC016601"}', NULL, 'insert', '19', '2017-05-09 15:43:55.80997', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6814', 'committed', '2225', '{"2225": "OCC016601"}', NULL, 'insert', '19', '2017-05-09 16:05:59.21997', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6815', 'committed', '2227', '{"2227": "OCC016501"}', NULL, 'insert', '19', '2017-05-09 16:16:40.676376', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('6998', 'committed', '2225', '{"2225": "OCC016601"}', NULL, 'insert', '19', '2017-05-22 15:54:15.152366', NULL, '19');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('7233', 'committed', '1822', '{"1822": "OCC012801"}', NULL, 'insert', '27', '2017-06-07 10:01:01.667451', NULL, '27');

INSERT INTO data_terminal."transaction" (id, status, location_id, occurrence, checksum, "action", creator_id, creation_timestamp, committed_timestamp, committer_id)
    VALUES ('7254', 'committed', '2016', '{"2016": "OCC016301"}', NULL, 'insert', '27', '2017-06-07 16:02:46.637893', NULL, '27');



-- update sequence used by data_terminal.transaction
SELECT SETVAL('data_terminal.transaction_id_seq', COALESCE(MAX(id), 1) ) FROM data_terminal."transaction";



--rollback DELETE FROM data_terminal."transaction";
