--liquibase formatted sql

--changeset postgres:update_value_experiment.occurrence.occurrence_code context:fixture splitStatements:FALSE

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000101',
    occurrence_name = 'EXPT0001-OCC01'
WHERE occ.id = 71
    AND (occ.occurrence_code <> 'OCC000101'
    OR occ.occurrence_name <> 'EXPT0001-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000201',
    occurrence_name = 'EXPT0002-OCC01'
WHERE occ.id = 72
    AND (occ.occurrence_code <> 'OCC000201'
    OR occ.occurrence_name <> 'EXPT0002-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000301',
    occurrence_name = 'EXPT0003-OCC01'
WHERE occ.id = 73
    AND (occ.occurrence_code <> 'OCC000301'
    OR occ.occurrence_name <> 'EXPT0003-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000401',
    occurrence_name = 'EXPT0004-OCC01'
WHERE occ.id = 74
    AND (occ.occurrence_code <> 'OCC000401'
    OR occ.occurrence_name <> 'EXPT0004-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000501',
    occurrence_name = 'EXPT0005-OCC01'
WHERE occ.id = 815
    AND (occ.occurrence_code <> 'OCC000501'
    OR occ.occurrence_name <> 'EXPT0005-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000601',
    occurrence_name = 'EXPT0006-OCC01'
WHERE occ.id = 706
    AND (occ.occurrence_code <> 'OCC000601'
    OR occ.occurrence_name <> 'EXPT0006-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000701',
    occurrence_name = 'EXPT0007-OCC01'
WHERE occ.id = 720
    AND (occ.occurrence_code <> 'OCC000701'
    OR occ.occurrence_name <> 'EXPT0007-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000801',
    occurrence_name = 'EXPT0008-OCC01'
WHERE occ.id = 340
    AND (occ.occurrence_code <> 'OCC000801'
    OR occ.occurrence_name <> 'EXPT0008-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC000901',
    occurrence_name = 'EXPT0009-OCC01'
WHERE occ.id = 170
    AND (occ.occurrence_code <> 'OCC000901'
    OR occ.occurrence_name <> 'EXPT0009-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001001',
    occurrence_name = 'EXPT0010-OCC01'
WHERE occ.id = 164
    AND (occ.occurrence_code <> 'OCC001001'
    OR occ.occurrence_name <> 'EXPT0010-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001101',
    occurrence_name = 'EXPT0011-OCC01'
WHERE occ.id = 816
    AND (occ.occurrence_code <> 'OCC001101'
    OR occ.occurrence_name <> 'EXPT0011-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001201',
    occurrence_name = 'EXPT0012-OCC01'
WHERE occ.id = 323
    AND (occ.occurrence_code <> 'OCC001201'
    OR occ.occurrence_name <> 'EXPT0012-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001301',
    occurrence_name = 'EXPT0013-OCC01'
WHERE occ.id = 228
    AND (occ.occurrence_code <> 'OCC001301'
    OR occ.occurrence_name <> 'EXPT0013-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001401',
    occurrence_name = 'EXPT0014-OCC01'
WHERE occ.id = 205
    AND (occ.occurrence_code <> 'OCC001401'
    OR occ.occurrence_name <> 'EXPT0014-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001501',
    occurrence_name = 'EXPT0015-OCC01'
WHERE occ.id = 206
    AND (occ.occurrence_code <> 'OCC001501'
    OR occ.occurrence_name <> 'EXPT0015-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001601',
    occurrence_name = 'EXPT0016-OCC01'
WHERE occ.id = 737
    AND (occ.occurrence_code <> 'OCC001601'
    OR occ.occurrence_name <> 'EXPT0016-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001701',
    occurrence_name = 'EXPT0017-OCC01'
WHERE occ.id = 231
    AND (occ.occurrence_code <> 'OCC001701'
    OR occ.occurrence_name <> 'EXPT0017-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001801',
    occurrence_name = 'EXPT0018-OCC01'
WHERE occ.id = 235
    AND (occ.occurrence_code <> 'OCC001801'
    OR occ.occurrence_name <> 'EXPT0018-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC001901',
    occurrence_name = 'EXPT0019-OCC01'
WHERE occ.id = 230
    AND (occ.occurrence_code <> 'OCC001901'
    OR occ.occurrence_name <> 'EXPT0019-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002001',
    occurrence_name = 'EXPT0020-OCC01'
WHERE occ.id = 233
    AND (occ.occurrence_code <> 'OCC002001'
    OR occ.occurrence_name <> 'EXPT0020-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002101',
    occurrence_name = 'EXPT0021-OCC01'
WHERE occ.id = 232
    AND (occ.occurrence_code <> 'OCC002101'
    OR occ.occurrence_name <> 'EXPT0021-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002201',
    occurrence_name = 'EXPT0022-OCC01'
WHERE occ.id = 236
    AND (occ.occurrence_code <> 'OCC002201'
    OR occ.occurrence_name <> 'EXPT0022-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002301',
    occurrence_name = 'EXPT0023-OCC01'
WHERE occ.id = 234
    AND (occ.occurrence_code <> 'OCC002301'
    OR occ.occurrence_name <> 'EXPT0023-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002401',
    occurrence_name = 'EXPT0024-OCC01'
WHERE occ.id = 821
    AND (occ.occurrence_code <> 'OCC002401'
    OR occ.occurrence_name <> 'EXPT0024-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002501',
    occurrence_name = 'EXPT0025-OCC01'
WHERE occ.id = 700
    AND (occ.occurrence_code <> 'OCC002501'
    OR occ.occurrence_name <> 'EXPT0025-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002502',
    occurrence_name = 'EXPT0025-OCC02'
WHERE occ.id = 705
    AND (occ.occurrence_code <> 'OCC002502'
    OR occ.occurrence_name <> 'EXPT0025-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002601',
    occurrence_name = 'EXPT0026-OCC01'
WHERE occ.id = 721
    AND (occ.occurrence_code <> 'OCC002601'
    OR occ.occurrence_name <> 'EXPT0026-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002701',
    occurrence_name = 'EXPT0027-OCC01'
WHERE occ.id = 385
    AND (occ.occurrence_code <> 'OCC002701'
    OR occ.occurrence_name <> 'EXPT0027-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002801',
    occurrence_name = 'EXPT0028-OCC01'
WHERE occ.id = 1572
    AND (occ.occurrence_code <> 'OCC002801'
    OR occ.occurrence_name <> 'EXPT0028-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC002901',
    occurrence_name = 'EXPT0029-OCC01'
WHERE occ.id = 378
    AND (occ.occurrence_code <> 'OCC002901'
    OR occ.occurrence_name <> 'EXPT0029-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003001',
    occurrence_name = 'EXPT0030-OCC01'
WHERE occ.id = 359
    AND (occ.occurrence_code <> 'OCC003001'
    OR occ.occurrence_name <> 'EXPT0030-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003101',
    occurrence_name = 'EXPT0031-OCC01'
WHERE occ.id = 473
    AND (occ.occurrence_code <> 'OCC003101'
    OR occ.occurrence_name <> 'EXPT0031-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003201',
    occurrence_name = 'EXPT0032-OCC01'
WHERE occ.id = 716
    AND (occ.occurrence_code <> 'OCC003201'
    OR occ.occurrence_name <> 'EXPT0032-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003301',
    occurrence_name = 'EXPT0033-OCC01'
WHERE occ.id = 715
    AND (occ.occurrence_code <> 'OCC003301'
    OR occ.occurrence_name <> 'EXPT0033-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003401',
    occurrence_name = 'EXPT0034-OCC01'
WHERE occ.id = 836
    AND (occ.occurrence_code <> 'OCC003401'
    OR occ.occurrence_name <> 'EXPT0034-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003501',
    occurrence_name = 'EXPT0035-OCC01'
WHERE occ.id = 804
    AND (occ.occurrence_code <> 'OCC003501'
    OR occ.occurrence_name <> 'EXPT0035-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003601',
    occurrence_name = 'EXPT0036-OCC01'
WHERE occ.id = 375
    AND (occ.occurrence_code <> 'OCC003601'
    OR occ.occurrence_name <> 'EXPT0036-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003701',
    occurrence_name = 'EXPT0037-OCC01'
WHERE occ.id = 1491
    AND (occ.occurrence_code <> 'OCC003701'
    OR occ.occurrence_name <> 'EXPT0037-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003801',
    occurrence_name = 'EXPT0038-OCC01'
WHERE occ.id = 1504
    AND (occ.occurrence_code <> 'OCC003801'
    OR occ.occurrence_name <> 'EXPT0038-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC003901',
    occurrence_name = 'EXPT0039-OCC01'
WHERE occ.id = 1508
    AND (occ.occurrence_code <> 'OCC003901'
    OR occ.occurrence_name <> 'EXPT0039-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004001',
    occurrence_name = 'EXPT0040-OCC01'
WHERE occ.id = 1507
    AND (occ.occurrence_code <> 'OCC004001'
    OR occ.occurrence_name <> 'EXPT0040-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004101',
    occurrence_name = 'EXPT0041-OCC01'
WHERE occ.id = 826
    AND (occ.occurrence_code <> 'OCC004101'
    OR occ.occurrence_name <> 'EXPT0041-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004201',
    occurrence_name = 'EXPT0042-OCC01'
WHERE occ.id = 390
    AND (occ.occurrence_code <> 'OCC004201'
    OR occ.occurrence_name <> 'EXPT0042-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004202',
    occurrence_name = 'EXPT0042-OCC02'
WHERE occ.id = 698
    AND (occ.occurrence_code <> 'OCC004202'
    OR occ.occurrence_name <> 'EXPT0042-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004301',
    occurrence_name = 'EXPT0043-OCC01'
WHERE occ.id = 421
    AND (occ.occurrence_code <> 'OCC004301'
    OR occ.occurrence_name <> 'EXPT0043-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004401',
    occurrence_name = 'EXPT0044-OCC01'
WHERE occ.id = 714
    AND (occ.occurrence_code <> 'OCC004401'
    OR occ.occurrence_name <> 'EXPT0044-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004501',
    occurrence_name = 'EXPT0045-OCC01'
WHERE occ.id = 843
    AND (occ.occurrence_code <> 'OCC004501'
    OR occ.occurrence_name <> 'EXPT0045-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004601',
    occurrence_name = 'EXPT0046-OCC01'
WHERE occ.id = 511
    AND (occ.occurrence_code <> 'OCC004601'
    OR occ.occurrence_name <> 'EXPT0046-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004701',
    occurrence_name = 'EXPT0047-OCC01'
WHERE occ.id = 463
    AND (occ.occurrence_code <> 'OCC004701'
    OR occ.occurrence_name <> 'EXPT0047-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004801',
    occurrence_name = 'EXPT0048-OCC01'
WHERE occ.id = 557
    AND (occ.occurrence_code <> 'OCC004801'
    OR occ.occurrence_name <> 'EXPT0048-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC004901',
    occurrence_name = 'EXPT0049-OCC01'
WHERE occ.id = 524
    AND (occ.occurrence_code <> 'OCC004901'
    OR occ.occurrence_name <> 'EXPT0049-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005001',
    occurrence_name = 'EXPT0050-OCC01'
WHERE occ.id = 1389
    AND (occ.occurrence_code <> 'OCC005001'
    OR occ.occurrence_name <> 'EXPT0050-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005101',
    occurrence_name = 'EXPT0051-OCC01'
WHERE occ.id = 1131
    AND (occ.occurrence_code <> 'OCC005101'
    OR occ.occurrence_name <> 'EXPT0051-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005201',
    occurrence_name = 'EXPT0052-OCC01'
WHERE occ.id = 1129
    AND (occ.occurrence_code <> 'OCC005201'
    OR occ.occurrence_name <> 'EXPT0052-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005301',
    occurrence_name = 'EXPT0053-OCC01'
WHERE occ.id = 1017
    AND (occ.occurrence_code <> 'OCC005301'
    OR occ.occurrence_name <> 'EXPT0053-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005401',
    occurrence_name = 'EXPT0054-OCC01'
WHERE occ.id = 1397
    AND (occ.occurrence_code <> 'OCC005401'
    OR occ.occurrence_name <> 'EXPT0054-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005501',
    occurrence_name = 'EXPT0055-OCC01'
WHERE occ.id = 1367
    AND (occ.occurrence_code <> 'OCC005501'
    OR occ.occurrence_name <> 'EXPT0055-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005601',
    occurrence_name = 'EXPT0056-OCC01'
WHERE occ.id = 1384
    AND (occ.occurrence_code <> 'OCC005601'
    OR occ.occurrence_name <> 'EXPT0056-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005701',
    occurrence_name = 'EXPT0057-OCC01'
WHERE occ.id = 1028
    AND (occ.occurrence_code <> 'OCC005701'
    OR occ.occurrence_name <> 'EXPT0057-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005801',
    occurrence_name = 'EXPT0058-OCC01'
WHERE occ.id = 1396
    AND (occ.occurrence_code <> 'OCC005801'
    OR occ.occurrence_name <> 'EXPT0058-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005901',
    occurrence_name = 'EXPT0059-OCC01'
WHERE occ.id = 1099
    AND (occ.occurrence_code <> 'OCC005901'
    OR occ.occurrence_name <> 'EXPT0059-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005902',
    occurrence_name = 'EXPT0059-OCC02'
WHERE occ.id = 1191
    AND (occ.occurrence_code <> 'OCC005902'
    OR occ.occurrence_name <> 'EXPT0059-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005903',
    occurrence_name = 'EXPT0059-OCC03'
WHERE occ.id = 1193
    AND (occ.occurrence_code <> 'OCC005903'
    OR occ.occurrence_name <> 'EXPT0059-OCC03');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC005904',
    occurrence_name = 'EXPT0059-OCC04'
WHERE occ.id = 1217
    AND (occ.occurrence_code <> 'OCC005904'
    OR occ.occurrence_name <> 'EXPT0059-OCC04');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006001',
    occurrence_name = 'EXPT0060-OCC01'
WHERE occ.id = 1100
    AND (occ.occurrence_code <> 'OCC006001'
    OR occ.occurrence_name <> 'EXPT0060-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006002',
    occurrence_name = 'EXPT0060-OCC02'
WHERE occ.id = 1200
    AND (occ.occurrence_code <> 'OCC006002'
    OR occ.occurrence_name <> 'EXPT0060-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006003',
    occurrence_name = 'EXPT0060-OCC03'
WHERE occ.id = 1201
    AND (occ.occurrence_code <> 'OCC006003'
    OR occ.occurrence_name <> 'EXPT0060-OCC03');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006004',
    occurrence_name = 'EXPT0060-OCC04'
WHERE occ.id = 1218
    AND (occ.occurrence_code <> 'OCC006004'
    OR occ.occurrence_name <> 'EXPT0060-OCC04');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006101',
    occurrence_name = 'EXPT0061-OCC01'
WHERE occ.id = 1408
    AND (occ.occurrence_code <> 'OCC006101'
    OR occ.occurrence_name <> 'EXPT0061-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006201',
    occurrence_name = 'EXPT0062-OCC01'
WHERE occ.id = 1414
    AND (occ.occurrence_code <> 'OCC006201'
    OR occ.occurrence_name <> 'EXPT0062-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006301',
    occurrence_name = 'EXPT0063-OCC01'
WHERE occ.id = 1413
    AND (occ.occurrence_code <> 'OCC006301'
    OR occ.occurrence_name <> 'EXPT0063-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006401',
    occurrence_name = 'EXPT0064-OCC01'
WHERE occ.id = 1527
    AND (occ.occurrence_code <> 'OCC006401'
    OR occ.occurrence_name <> 'EXPT0064-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006501',
    occurrence_name = 'EXPT0065-OCC01'
WHERE occ.id = 916
    AND (occ.occurrence_code <> 'OCC006501'
    OR occ.occurrence_name <> 'EXPT0065-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006601',
    occurrence_name = 'EXPT0066-OCC01'
WHERE occ.id = 895
    AND (occ.occurrence_code <> 'OCC006601'
    OR occ.occurrence_name <> 'EXPT0066-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006701',
    occurrence_name = 'EXPT0067-OCC01'
WHERE occ.id = 885
    AND (occ.occurrence_code <> 'OCC006701'
    OR occ.occurrence_name <> 'EXPT0067-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006801',
    occurrence_name = 'EXPT0068-OCC01'
WHERE occ.id = 894
    AND (occ.occurrence_code <> 'OCC006801'
    OR occ.occurrence_name <> 'EXPT0068-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC006901',
    occurrence_name = 'EXPT0069-OCC01'
WHERE occ.id = 1540
    AND (occ.occurrence_code <> 'OCC006901'
    OR occ.occurrence_name <> 'EXPT0069-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007001',
    occurrence_name = 'EXPT0070-OCC01'
WHERE occ.id = 2163
    AND (occ.occurrence_code <> 'OCC007001'
    OR occ.occurrence_name <> 'EXPT0070-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007101',
    occurrence_name = 'EXPT0071-OCC01'
WHERE occ.id = 893
    AND (occ.occurrence_code <> 'OCC007101'
    OR occ.occurrence_name <> 'EXPT0071-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007201',
    occurrence_name = 'EXPT0072-OCC01'
WHERE occ.id = 1004
    AND (occ.occurrence_code <> 'OCC007201'
    OR occ.occurrence_name <> 'EXPT0072-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007301',
    occurrence_name = 'EXPT0073-OCC01'
WHERE occ.id = 1003
    AND (occ.occurrence_code <> 'OCC007301'
    OR occ.occurrence_name <> 'EXPT0073-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007401',
    occurrence_name = 'EXPT0074-OCC01'
WHERE occ.id = 1015
    AND (occ.occurrence_code <> 'OCC007401'
    OR occ.occurrence_name <> 'EXPT0074-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007501',
    occurrence_name = 'EXPT0075-OCC01'
WHERE occ.id = 919
    AND (occ.occurrence_code <> 'OCC007501'
    OR occ.occurrence_name <> 'EXPT0075-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007601',
    occurrence_name = 'EXPT0076-OCC01'
WHERE occ.id = 1037
    AND (occ.occurrence_code <> 'OCC007601'
    OR occ.occurrence_name <> 'EXPT0076-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007701',
    occurrence_name = 'EXPT0077-OCC01'
WHERE occ.id = 1047
    AND (occ.occurrence_code <> 'OCC007701'
    OR occ.occurrence_name <> 'EXPT0077-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007801',
    occurrence_name = 'EXPT0078-OCC01'
WHERE occ.id = 1377
    AND (occ.occurrence_code <> 'OCC007801'
    OR occ.occurrence_name <> 'EXPT0078-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC007901',
    occurrence_name = 'EXPT0079-OCC01'
WHERE occ.id = 1382
    AND (occ.occurrence_code <> 'OCC007901'
    OR occ.occurrence_name <> 'EXPT0079-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008001',
    occurrence_name = 'EXPT0080-OCC01'
WHERE occ.id = 1378
    AND (occ.occurrence_code <> 'OCC008001'
    OR occ.occurrence_name <> 'EXPT0080-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008101',
    occurrence_name = 'EXPT0081-OCC01'
WHERE occ.id = 1380
    AND (occ.occurrence_code <> 'OCC008101'
    OR occ.occurrence_name <> 'EXPT0081-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008201',
    occurrence_name = 'EXPT0082-OCC01'
WHERE occ.id = 1383
    AND (occ.occurrence_code <> 'OCC008201'
    OR occ.occurrence_name <> 'EXPT0082-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008301',
    occurrence_name = 'EXPT0083-OCC01'
WHERE occ.id = 1381
    AND (occ.occurrence_code <> 'OCC008301'
    OR occ.occurrence_name <> 'EXPT0083-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008401',
    occurrence_name = 'EXPT0084-OCC01'
WHERE occ.id = 1016
    AND (occ.occurrence_code <> 'OCC008401'
    OR occ.occurrence_name <> 'EXPT0084-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008501',
    occurrence_name = 'EXPT0085-OCC01'
WHERE occ.id = 1105
    AND (occ.occurrence_code <> 'OCC008501'
    OR occ.occurrence_name <> 'EXPT0085-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008601',
    occurrence_name = 'EXPT0086-OCC01'
WHERE occ.id = 1110
    AND (occ.occurrence_code <> 'OCC008601'
    OR occ.occurrence_name <> 'EXPT0086-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008701',
    occurrence_name = 'EXPT0087-OCC01'
WHERE occ.id = 1411
    AND (occ.occurrence_code <> 'OCC008701'
    OR occ.occurrence_name <> 'EXPT0087-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008801',
    occurrence_name = 'EXPT0088-OCC01'
WHERE occ.id = 1412
    AND (occ.occurrence_code <> 'OCC008801'
    OR occ.occurrence_name <> 'EXPT0088-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC008901',
    occurrence_name = 'EXPT0089-OCC01'
WHERE occ.id = 1073
    AND (occ.occurrence_code <> 'OCC008901'
    OR occ.occurrence_name <> 'EXPT0089-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009001',
    occurrence_name = 'EXPT0090-OCC01'
WHERE occ.id = 1085
    AND (occ.occurrence_code <> 'OCC009001'
    OR occ.occurrence_name <> 'EXPT0090-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009101',
    occurrence_name = 'EXPT0091-OCC01'
WHERE occ.id = 1036
    AND (occ.occurrence_code <> 'OCC009101'
    OR occ.occurrence_name <> 'EXPT0091-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009201',
    occurrence_name = 'EXPT0092-OCC01'
WHERE occ.id = 1005
    AND (occ.occurrence_code <> 'OCC009201'
    OR occ.occurrence_name <> 'EXPT0092-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009301',
    occurrence_name = 'EXPT0093-OCC01'
WHERE occ.id = 907
    AND (occ.occurrence_code <> 'OCC009301'
    OR occ.occurrence_name <> 'EXPT0093-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009401',
    occurrence_name = 'EXPT0094-OCC01'
WHERE occ.id = 1115
    AND (occ.occurrence_code <> 'OCC009401'
    OR occ.occurrence_name <> 'EXPT0094-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009501',
    occurrence_name = 'EXPT0095-OCC01'
WHERE occ.id = 1360
    AND (occ.occurrence_code <> 'OCC009501'
    OR occ.occurrence_name <> 'EXPT0095-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009502',
    occurrence_name = 'EXPT0095-OCC02'
WHERE occ.id = 1362
    AND (occ.occurrence_code <> 'OCC009502'
    OR occ.occurrence_name <> 'EXPT0095-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009601',
    occurrence_name = 'EXPT0096-OCC01'
WHERE occ.id = 1124
    AND (occ.occurrence_code <> 'OCC009601'
    OR occ.occurrence_name <> 'EXPT0096-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009701',
    occurrence_name = 'EXPT0097-OCC01'
WHERE occ.id = 1125
    AND (occ.occurrence_code <> 'OCC009701'
    OR occ.occurrence_name <> 'EXPT0097-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009801',
    occurrence_name = 'EXPT0098-OCC01'
WHERE occ.id = 1114
    AND (occ.occurrence_code <> 'OCC009801'
    OR occ.occurrence_name <> 'EXPT0098-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC009901',
    occurrence_name = 'EXPT0099-OCC01'
WHERE occ.id = 903
    AND (occ.occurrence_code <> 'OCC009901'
    OR occ.occurrence_name <> 'EXPT0099-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010001',
    occurrence_name = 'EXPT0100-OCC01'
WHERE occ.id = 1358
    AND (occ.occurrence_code <> 'OCC010001'
    OR occ.occurrence_name <> 'EXPT0100-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010002',
    occurrence_name = 'EXPT0100-OCC02'
WHERE occ.id = 1361
    AND (occ.occurrence_code <> 'OCC010002'
    OR occ.occurrence_name <> 'EXPT0100-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010101',
    occurrence_name = 'EXPT0101-OCC01'
WHERE occ.id = 1405
    AND (occ.occurrence_code <> 'OCC010101'
    OR occ.occurrence_name <> 'EXPT0101-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010201',
    occurrence_name = 'EXPT0102-OCC01'
WHERE occ.id = 1061
    AND (occ.occurrence_code <> 'OCC010201'
    OR occ.occurrence_name <> 'EXPT0102-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010301',
    occurrence_name = 'EXPT0103-OCC01'
WHERE occ.id = 1314
    AND (occ.occurrence_code <> 'OCC010301'
    OR occ.occurrence_name <> 'EXPT0103-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010401',
    occurrence_name = 'EXPT0104-OCC01'
WHERE occ.id = 1313
    AND (occ.occurrence_code <> 'OCC010401'
    OR occ.occurrence_name <> 'EXPT0104-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010501',
    occurrence_name = 'EXPT0105-OCC01'
WHERE occ.id = 1489
    AND (occ.occurrence_code <> 'OCC010501'
    OR occ.occurrence_name <> 'EXPT0105-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010601',
    occurrence_name = 'EXPT0106-OCC01'
WHERE occ.id = 1793
    AND (occ.occurrence_code <> 'OCC010601'
    OR occ.occurrence_name <> 'EXPT0106-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010701',
    occurrence_name = 'EXPT0107-OCC01'
WHERE occ.id = 1792
    AND (occ.occurrence_code <> 'OCC010701'
    OR occ.occurrence_name <> 'EXPT0107-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010801',
    occurrence_name = 'EXPT0108-OCC01'
WHERE occ.id = 1697
    AND (occ.occurrence_code <> 'OCC010801'
    OR occ.occurrence_name <> 'EXPT0108-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC010901',
    occurrence_name = 'EXPT0109-OCC01'
WHERE occ.id = 1799
    AND (occ.occurrence_code <> 'OCC010901'
    OR occ.occurrence_name <> 'EXPT0109-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011001',
    occurrence_name = 'EXPT0110-OCC01'
WHERE occ.id = 1520
    AND (occ.occurrence_code <> 'OCC011001'
    OR occ.occurrence_name <> 'EXPT0110-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011101',
    occurrence_name = 'EXPT0111-OCC01'
WHERE occ.id = 1771
    AND (occ.occurrence_code <> 'OCC011101'
    OR occ.occurrence_name <> 'EXPT0111-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011102',
    occurrence_name = 'EXPT0111-OCC02'
WHERE occ.id = 1786
    AND (occ.occurrence_code <> 'OCC011102'
    OR occ.occurrence_name <> 'EXPT0111-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011201',
    occurrence_name = 'EXPT0112-OCC01'
WHERE occ.id = 2179
    AND (occ.occurrence_code <> 'OCC011201'
    OR occ.occurrence_name <> 'EXPT0112-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011301',
    occurrence_name = 'EXPT0113-OCC01'
WHERE occ.id = 1766
    AND (occ.occurrence_code <> 'OCC011301'
    OR occ.occurrence_name <> 'EXPT0113-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011401',
    occurrence_name = 'EXPT0114-OCC01'
WHERE occ.id = 1982
    AND (occ.occurrence_code <> 'OCC011401'
    OR occ.occurrence_name <> 'EXPT0114-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011402',
    occurrence_name = 'EXPT0114-OCC02'
WHERE occ.id = 1989
    AND (occ.occurrence_code <> 'OCC011402'
    OR occ.occurrence_name <> 'EXPT0114-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011403',
    occurrence_name = 'EXPT0114-OCC03'
WHERE occ.id = 1991
    AND (occ.occurrence_code <> 'OCC011403'
    OR occ.occurrence_name <> 'EXPT0114-OCC03');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011501',
    occurrence_name = 'EXPT0115-OCC01'
WHERE occ.id = 2178
    AND (occ.occurrence_code <> 'OCC011501'
    OR occ.occurrence_name <> 'EXPT0115-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011601',
    occurrence_name = 'EXPT0116-OCC01'
WHERE occ.id = 1765
    AND (occ.occurrence_code <> 'OCC011601'
    OR occ.occurrence_name <> 'EXPT0116-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011701',
    occurrence_name = 'EXPT0117-OCC01'
WHERE occ.id = 1981
    AND (occ.occurrence_code <> 'OCC011701'
    OR occ.occurrence_name <> 'EXPT0117-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011702',
    occurrence_name = 'EXPT0117-OCC02'
WHERE occ.id = 1988
    AND (occ.occurrence_code <> 'OCC011702'
    OR occ.occurrence_name <> 'EXPT0117-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011703',
    occurrence_name = 'EXPT0117-OCC03'
WHERE occ.id = 1990
    AND (occ.occurrence_code <> 'OCC011703'
    OR occ.occurrence_name <> 'EXPT0117-OCC03');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011801',
    occurrence_name = 'EXPT0118-OCC01'
WHERE occ.id = 1788
    AND (occ.occurrence_code <> 'OCC011801'
    OR occ.occurrence_name <> 'EXPT0118-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC011901',
    occurrence_name = 'EXPT0119-OCC01'
WHERE occ.id = 1789
    AND (occ.occurrence_code <> 'OCC011901'
    OR occ.occurrence_name <> 'EXPT0119-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012001',
    occurrence_name = 'EXPT0120-OCC01'
WHERE occ.id = 1769
    AND (occ.occurrence_code <> 'OCC012001'
    OR occ.occurrence_name <> 'EXPT0120-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012002',
    occurrence_name = 'EXPT0120-OCC02'
WHERE occ.id = 1785
    AND (occ.occurrence_code <> 'OCC012002'
    OR occ.occurrence_name <> 'EXPT0120-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012101',
    occurrence_name = 'EXPT0121-OCC01'
WHERE occ.id = 1889
    AND (occ.occurrence_code <> 'OCC012101'
    OR occ.occurrence_name <> 'EXPT0121-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012201',
    occurrence_name = 'EXPT0122-OCC01'
WHERE occ.id = 1567
    AND (occ.occurrence_code <> 'OCC012201'
    OR occ.occurrence_name <> 'EXPT0122-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012301',
    occurrence_name = 'EXPT0123-OCC01'
WHERE occ.id = 1521
    AND (occ.occurrence_code <> 'OCC012301'
    OR occ.occurrence_name <> 'EXPT0123-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012401',
    occurrence_name = 'EXPT0124-OCC01'
WHERE occ.id = 2059
    AND (occ.occurrence_code <> 'OCC012401'
    OR occ.occurrence_name <> 'EXPT0124-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012501',
    occurrence_name = 'EXPT0125-OCC01'
WHERE occ.id = 1621
    AND (occ.occurrence_code <> 'OCC012501'
    OR occ.occurrence_name <> 'EXPT0125-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012601',
    occurrence_name = 'EXPT0126-OCC01'
WHERE occ.id = 1603
    AND (occ.occurrence_code <> 'OCC012601'
    OR occ.occurrence_name <> 'EXPT0126-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012701',
    occurrence_name = 'EXPT0127-OCC01'
WHERE occ.id = 1746
    AND (occ.occurrence_code <> 'OCC012701'
    OR occ.occurrence_name <> 'EXPT0127-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012801',
    occurrence_name = 'EXPT0128-OCC01'
WHERE occ.id = 1822
    AND (occ.occurrence_code <> 'OCC012801'
    OR occ.occurrence_name <> 'EXPT0128-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC012901',
    occurrence_name = 'EXPT0129-OCC01'
WHERE occ.id = 2125
    AND (occ.occurrence_code <> 'OCC012901'
    OR occ.occurrence_name <> 'EXPT0129-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013001',
    occurrence_name = 'EXPT0130-OCC01'
WHERE occ.id = 1797
    AND (occ.occurrence_code <> 'OCC013001'
    OR occ.occurrence_name <> 'EXPT0130-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013101',
    occurrence_name = 'EXPT0131-OCC01'
WHERE occ.id = 1841
    AND (occ.occurrence_code <> 'OCC013101'
    OR occ.occurrence_name <> 'EXPT0131-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013201',
    occurrence_name = 'EXPT0132-OCC01'
WHERE occ.id = 1801
    AND (occ.occurrence_code <> 'OCC013201'
    OR occ.occurrence_name <> 'EXPT0132-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013301',
    occurrence_name = 'EXPT0133-OCC01'
WHERE occ.id = 1868
    AND (occ.occurrence_code <> 'OCC013301'
    OR occ.occurrence_name <> 'EXPT0133-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013401',
    occurrence_name = 'EXPT0134-OCC01'
WHERE occ.id = 1764
    AND (occ.occurrence_code <> 'OCC013401'
    OR occ.occurrence_name <> 'EXPT0134-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013501',
    occurrence_name = 'EXPT0135-OCC01'
WHERE occ.id = 1763
    AND (occ.occurrence_code <> 'OCC013501'
    OR occ.occurrence_name <> 'EXPT0135-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013601',
    occurrence_name = 'EXPT0136-OCC01'
WHERE occ.id = 1888
    AND (occ.occurrence_code <> 'OCC013601'
    OR occ.occurrence_name <> 'EXPT0136-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013701',
    occurrence_name = 'EXPT0137-OCC01'
WHERE occ.id = 1796
    AND (occ.occurrence_code <> 'OCC013701'
    OR occ.occurrence_name <> 'EXPT0137-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013801',
    occurrence_name = 'EXPT0138-OCC01'
WHERE occ.id = 1800
    AND (occ.occurrence_code <> 'OCC013801'
    OR occ.occurrence_name <> 'EXPT0138-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC013901',
    occurrence_name = 'EXPT0139-OCC01'
WHERE occ.id = 1794
    AND (occ.occurrence_code <> 'OCC013901'
    OR occ.occurrence_name <> 'EXPT0139-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014001',
    occurrence_name = 'EXPT0140-OCC01'
WHERE occ.id = 2177
    AND (occ.occurrence_code <> 'OCC014001'
    OR occ.occurrence_name <> 'EXPT0140-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014101',
    occurrence_name = 'EXPT0141-OCC01'
WHERE occ.id = 2026
    AND (occ.occurrence_code <> 'OCC014101'
    OR occ.occurrence_name <> 'EXPT0141-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014201',
    occurrence_name = 'EXPT0142-OCC01'
WHERE occ.id = 1956
    AND (occ.occurrence_code <> 'OCC014201'
    OR occ.occurrence_name <> 'EXPT0142-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014301',
    occurrence_name = 'EXPT0143-OCC01'
WHERE occ.id = 1955
    AND (occ.occurrence_code <> 'OCC014301'
    OR occ.occurrence_name <> 'EXPT0143-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014401',
    occurrence_name = 'EXPT0144-OCC01'
WHERE occ.id = 1798
    AND (occ.occurrence_code <> 'OCC014401'
    OR occ.occurrence_name <> 'EXPT0144-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014501',
    occurrence_name = 'EXPT0145-OCC01'
WHERE occ.id = 1855
    AND (occ.occurrence_code <> 'OCC014501'
    OR occ.occurrence_name <> 'EXPT0145-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014601',
    occurrence_name = 'EXPT0146-OCC01'
WHERE occ.id = 1861
    AND (occ.occurrence_code <> 'OCC014601'
    OR occ.occurrence_name <> 'EXPT0146-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014701',
    occurrence_name = 'EXPT0147-OCC01'
WHERE occ.id = 1840
    AND (occ.occurrence_code <> 'OCC014701'
    OR occ.occurrence_name <> 'EXPT0147-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014801',
    occurrence_name = 'EXPT0148-OCC01'
WHERE occ.id = 1825
    AND (occ.occurrence_code <> 'OCC014801'
    OR occ.occurrence_name <> 'EXPT0148-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC014901',
    occurrence_name = 'EXPT0149-OCC01'
WHERE occ.id = 2312
    AND (occ.occurrence_code <> 'OCC014901'
    OR occ.occurrence_name <> 'EXPT0149-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015001',
    occurrence_name = 'EXPT0150-OCC01'
WHERE occ.id = 2313
    AND (occ.occurrence_code <> 'OCC015001'
    OR occ.occurrence_name <> 'EXPT0150-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015101',
    occurrence_name = 'EXPT0151-OCC01'
WHERE occ.id = 2181
    AND (occ.occurrence_code <> 'OCC015101'
    OR occ.occurrence_name <> 'EXPT0151-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015201',
    occurrence_name = 'EXPT0152-OCC01'
WHERE occ.id = 2232
    AND (occ.occurrence_code <> 'OCC015201'
    OR occ.occurrence_name <> 'EXPT0152-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015301',
    occurrence_name = 'EXPT0153-OCC01'
WHERE occ.id = 2338
    AND (occ.occurrence_code <> 'OCC015301'
    OR occ.occurrence_name <> 'EXPT0153-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015302',
    occurrence_name = 'EXPT0153-OCC02'
WHERE occ.id = 2466
    AND (occ.occurrence_code <> 'OCC015302'
    OR occ.occurrence_name <> 'EXPT0153-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015303',
    occurrence_name = 'EXPT0153-OCC03'
WHERE occ.id = 2468
    AND (occ.occurrence_code <> 'OCC015303'
    OR occ.occurrence_name <> 'EXPT0153-OCC03');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015401',
    occurrence_name = 'EXPT0154-OCC01'
WHERE occ.id = 2248
    AND (occ.occurrence_code <> 'OCC015401'
    OR occ.occurrence_name <> 'EXPT0154-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015402',
    occurrence_name = 'EXPT0154-OCC02'
WHERE occ.id = 2336
    AND (occ.occurrence_code <> 'OCC015402'
    OR occ.occurrence_name <> 'EXPT0154-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015501',
    occurrence_name = 'EXPT0155-OCC01'
WHERE occ.id = 2249
    AND (occ.occurrence_code <> 'OCC015501'
    OR occ.occurrence_name <> 'EXPT0155-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015601',
    occurrence_name = 'EXPT0156-OCC01'
WHERE occ.id = 2332
    AND (occ.occurrence_code <> 'OCC015601'
    OR occ.occurrence_name <> 'EXPT0156-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015701',
    occurrence_name = 'EXPT0157-OCC01'
WHERE occ.id = 2465
    AND (occ.occurrence_code <> 'OCC015701'
    OR occ.occurrence_name <> 'EXPT0157-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015702',
    occurrence_name = 'EXPT0157-OCC02'
WHERE occ.id = 2467
    AND (occ.occurrence_code <> 'OCC015702'
    OR occ.occurrence_name <> 'EXPT0157-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015801',
    occurrence_name = 'EXPT0158-OCC01'
WHERE occ.id = 2247
    AND (occ.occurrence_code <> 'OCC015801'
    OR occ.occurrence_name <> 'EXPT0158-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015802',
    occurrence_name = 'EXPT0158-OCC02'
WHERE occ.id = 2335
    AND (occ.occurrence_code <> 'OCC015802'
    OR occ.occurrence_name <> 'EXPT0158-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC015901',
    occurrence_name = 'EXPT0159-OCC01'
WHERE occ.id = 2166
    AND (occ.occurrence_code <> 'OCC015901'
    OR occ.occurrence_name <> 'EXPT0159-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016001',
    occurrence_name = 'EXPT0160-OCC01'
WHERE occ.id = 2531
    AND (occ.occurrence_code <> 'OCC016001'
    OR occ.occurrence_name <> 'EXPT0160-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016101',
    occurrence_name = 'EXPT0161-OCC01'
WHERE occ.id = 2359
    AND (occ.occurrence_code <> 'OCC016101'
    OR occ.occurrence_name <> 'EXPT0161-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016201',
    occurrence_name = 'EXPT0162-OCC01'
WHERE occ.id = 2233
    AND (occ.occurrence_code <> 'OCC016201'
    OR occ.occurrence_name <> 'EXPT0162-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016301',
    occurrence_name = 'EXPT0163-OCC01'
WHERE occ.id = 2016
    AND (occ.occurrence_code <> 'OCC016301'
    OR occ.occurrence_name <> 'EXPT0163-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016401',
    occurrence_name = 'EXPT0164-OCC01'
WHERE occ.id = 2223
    AND (occ.occurrence_code <> 'OCC016401'
    OR occ.occurrence_name <> 'EXPT0164-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016501',
    occurrence_name = 'EXPT0165-OCC01'
WHERE occ.id = 2227
    AND (occ.occurrence_code <> 'OCC016501'
    OR occ.occurrence_name <> 'EXPT0165-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016601',
    occurrence_name = 'EXPT0166-OCC01'
WHERE occ.id = 2225
    AND (occ.occurrence_code <> 'OCC016601'
    OR occ.occurrence_name <> 'EXPT0166-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016701',
    occurrence_name = 'EXPT0167-OCC01'
WHERE occ.id = 2221
    AND (occ.occurrence_code <> 'OCC016701'
    OR occ.occurrence_name <> 'EXPT0167-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016801',
    occurrence_name = 'EXPT0168-OCC01'
WHERE occ.id = 2226
    AND (occ.occurrence_code <> 'OCC016801'
    OR occ.occurrence_name <> 'EXPT0168-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016901',
    occurrence_name = 'EXPT0169-OCC01'
WHERE occ.id = 2254
    AND (occ.occurrence_code <> 'OCC016901'
    OR occ.occurrence_name <> 'EXPT0169-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017001',
    occurrence_name = 'EXPT0170-OCC01'
WHERE occ.id = 2255
    AND (occ.occurrence_code <> 'OCC017001'
    OR occ.occurrence_name <> 'EXPT0170-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017101',
    occurrence_name = 'EXPT0171-OCC01'
WHERE occ.id = 2324
    AND (occ.occurrence_code <> 'OCC017101'
    OR occ.occurrence_name <> 'EXPT0171-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017201',
    occurrence_name = 'EXPT0172-OCC01'
WHERE occ.id = 2442
    AND (occ.occurrence_code <> 'OCC017201'
    OR occ.occurrence_name <> 'EXPT0172-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017301',
    occurrence_name = 'EXPT0173-OCC01'
WHERE occ.id = 2314
    AND (occ.occurrence_code <> 'OCC017301'
    OR occ.occurrence_name <> 'EXPT0173-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017401',
    occurrence_name = 'EXPT0174-OCC01'
WHERE occ.id = 2134
    AND (occ.occurrence_code <> 'OCC017401'
    OR occ.occurrence_name <> 'EXPT0174-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017501',
    occurrence_name = 'EXPT0175-OCC01'
WHERE occ.id = 2412
    AND (occ.occurrence_code <> 'OCC017501'
    OR occ.occurrence_name <> 'EXPT0175-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017601',
    occurrence_name = 'EXPT0176-OCC01'
WHERE occ.id = 2235
    AND (occ.occurrence_code <> 'OCC017601'
    OR occ.occurrence_name <> 'EXPT0176-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017701',
    occurrence_name = 'EXPT0177-OCC01'
WHERE occ.id = 2323
    AND (occ.occurrence_code <> 'OCC017701'
    OR occ.occurrence_name <> 'EXPT0177-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017801',
    occurrence_name = 'EXPT0178-OCC01'
WHERE occ.id = 2184
    AND (occ.occurrence_code <> 'OCC017801'
    OR occ.occurrence_name <> 'EXPT0178-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC017901',
    occurrence_name = 'EXPT0179-OCC01'
WHERE occ.id = 2224
    AND (occ.occurrence_code <> 'OCC017901'
    OR occ.occurrence_name <> 'EXPT0179-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018001',
    occurrence_name = 'EXPT0180-OCC01'
WHERE occ.id = 2183
    AND (occ.occurrence_code <> 'OCC018001'
    OR occ.occurrence_name <> 'EXPT0180-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018101',
    occurrence_name = 'EXPT0181-OCC01'
WHERE occ.id = 2738
    AND (occ.occurrence_code <> 'OCC018101'
    OR occ.occurrence_name <> 'EXPT0181-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018201',
    occurrence_name = 'EXPT0182-OCC01'
WHERE occ.id = 2674
    AND (occ.occurrence_code <> 'OCC018201'
    OR occ.occurrence_name <> 'EXPT0182-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018301',
    occurrence_name = 'EXPT0183-OCC01'
WHERE occ.id = 2632
    AND (occ.occurrence_code <> 'OCC018301'
    OR occ.occurrence_name <> 'EXPT0183-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018401',
    occurrence_name = 'EXPT0184-OCC01'
WHERE occ.id = 3026
    AND (occ.occurrence_code <> 'OCC018401'
    OR occ.occurrence_name <> 'EXPT0184-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018501',
    occurrence_name = 'EXPT0185-OCC01'
WHERE occ.id = 3024
    AND (occ.occurrence_code <> 'OCC018501'
    OR occ.occurrence_name <> 'EXPT0185-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018601',
    occurrence_name = 'EXPT0186-OCC01'
WHERE occ.id = 3030
    AND (occ.occurrence_code <> 'OCC018601'
    OR occ.occurrence_name <> 'EXPT0186-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018701',
    occurrence_name = 'EXPT0187-OCC01'
WHERE occ.id = 2942
    AND (occ.occurrence_code <> 'OCC018701'
    OR occ.occurrence_name <> 'EXPT0187-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018801',
    occurrence_name = 'EXPT0188-OCC01'
WHERE occ.id = 2837
    AND (occ.occurrence_code <> 'OCC018801'
    OR occ.occurrence_name <> 'EXPT0188-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC018901',
    occurrence_name = 'EXPT0189-OCC01'
WHERE occ.id = 2795
    AND (occ.occurrence_code <> 'OCC018901'
    OR occ.occurrence_name <> 'EXPT0189-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019001',
    occurrence_name = 'EXPT0190-OCC01'
WHERE occ.id = 2797
    AND (occ.occurrence_code <> 'OCC019001'
    OR occ.occurrence_name <> 'EXPT0190-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019101',
    occurrence_name = 'EXPT0191-OCC01'
WHERE occ.id = 2842
    AND (occ.occurrence_code <> 'OCC019101'
    OR occ.occurrence_name <> 'EXPT0191-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019201',
    occurrence_name = 'EXPT0192-OCC01'
WHERE occ.id = 2676
    AND (occ.occurrence_code <> 'OCC019201'
    OR occ.occurrence_name <> 'EXPT0192-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019301',
    occurrence_name = 'EXPT0193-OCC01'
WHERE occ.id = 2701
    AND (occ.occurrence_code <> 'OCC019301'
    OR occ.occurrence_name <> 'EXPT0193-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019302',
    occurrence_name = 'EXPT0193-OCC02'
WHERE occ.id = 2702
    AND (occ.occurrence_code <> 'OCC019302'
    OR occ.occurrence_name <> 'EXPT0193-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019401',
    occurrence_name = 'EXPT0194-OCC01'
WHERE occ.id = 2741
    AND (occ.occurrence_code <> 'OCC019401'
    OR occ.occurrence_name <> 'EXPT0194-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019501',
    occurrence_name = 'EXPT0195-OCC01'
WHERE occ.id = 3041
    AND (occ.occurrence_code <> 'OCC019501'
    OR occ.occurrence_name <> 'EXPT0195-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019601',
    occurrence_name = 'EXPT0196-OCC01'
WHERE occ.id = 3040
    AND (occ.occurrence_code <> 'OCC019601'
    OR occ.occurrence_name <> 'EXPT0196-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019701',
    occurrence_name = 'EXPT0197-OCC01'
WHERE occ.id = 3028
    AND (occ.occurrence_code <> 'OCC019701'
    OR occ.occurrence_name <> 'EXPT0197-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019801',
    occurrence_name = 'EXPT0198-OCC01'
WHERE occ.id = 2608
    AND (occ.occurrence_code <> 'OCC019801'
    OR occ.occurrence_name <> 'EXPT0198-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC019901',
    occurrence_name = 'EXPT0199-OCC01'
WHERE occ.id = 2932
    AND (occ.occurrence_code <> 'OCC019901'
    OR occ.occurrence_name <> 'EXPT0199-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020001',
    occurrence_name = 'EXPT0200-OCC01'
WHERE occ.id = 2846
    AND (occ.occurrence_code <> 'OCC020001'
    OR occ.occurrence_name <> 'EXPT0200-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020101',
    occurrence_name = 'EXPT0201-OCC01'
WHERE occ.id = 2443
    AND (occ.occurrence_code <> 'OCC020101'
    OR occ.occurrence_name <> 'EXPT0201-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020201',
    occurrence_name = 'EXPT0202-OCC01'
WHERE occ.id = 3376
    AND (occ.occurrence_code <> 'OCC020201'
    OR occ.occurrence_name <> 'EXPT0202-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020301',
    occurrence_name = 'EXPT0203-OCC01'
WHERE occ.id = 2445
    AND (occ.occurrence_code <> 'OCC020301'
    OR occ.occurrence_name <> 'EXPT0203-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020401',
    occurrence_name = 'EXPT0204-OCC01'
WHERE occ.id = 3370
    AND (occ.occurrence_code <> 'OCC020401'
    OR occ.occurrence_name <> 'EXPT0204-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020501',
    occurrence_name = 'EXPT0205-OCC01'
WHERE occ.id = 3371
    AND (occ.occurrence_code <> 'OCC020501'
    OR occ.occurrence_name <> 'EXPT0205-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020601',
    occurrence_name = 'EXPT0206-OCC01'
WHERE occ.id = 3373
    AND (occ.occurrence_code <> 'OCC020601'
    OR occ.occurrence_name <> 'EXPT0206-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020701',
    occurrence_name = 'EXPT0207-OCC01'
WHERE occ.id = 3374
    AND (occ.occurrence_code <> 'OCC020701'
    OR occ.occurrence_name <> 'EXPT0207-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020801',
    occurrence_name = 'EXPT0208-OCC01'
WHERE occ.id = 3375
    AND (occ.occurrence_code <> 'OCC020801'
    OR occ.occurrence_name <> 'EXPT0208-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC020901',
    occurrence_name = 'EXPT0209-OCC01'
WHERE occ.id = 2441
    AND (occ.occurrence_code <> 'OCC020901'
    OR occ.occurrence_name <> 'EXPT0209-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021001',
    occurrence_name = 'EXPT0210-OCC01'
WHERE occ.id = 2446
    AND (occ.occurrence_code <> 'OCC021001'
    OR occ.occurrence_name <> 'EXPT0210-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021101',
    occurrence_name = 'EXPT0211-OCC01'
WHERE occ.id = 2449
    AND (occ.occurrence_code <> 'OCC021101'
    OR occ.occurrence_name <> 'EXPT0211-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021201',
    occurrence_name = 'EXPT0212-OCC01'
WHERE occ.id = 2918
    AND (occ.occurrence_code <> 'OCC021201'
    OR occ.occurrence_name <> 'EXPT0212-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021301',
    occurrence_name = 'EXPT0213-OCC01'
WHERE occ.id = 2929
    AND (occ.occurrence_code <> 'OCC021301'
    OR occ.occurrence_name <> 'EXPT0213-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021401',
    occurrence_name = 'EXPT0214-OCC01'
WHERE occ.id = 2633
    AND (occ.occurrence_code <> 'OCC021401'
    OR occ.occurrence_name <> 'EXPT0214-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021501',
    occurrence_name = 'EXPT0215-OCC01'
WHERE occ.id = 2634
    AND (occ.occurrence_code <> 'OCC021501'
    OR occ.occurrence_name <> 'EXPT0215-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021601',
    occurrence_name = 'EXPT0216-OCC01'
WHERE occ.id = 2707
    AND (occ.occurrence_code <> 'OCC021601'
    OR occ.occurrence_name <> 'EXPT0216-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021701',
    occurrence_name = 'EXPT0217-OCC01'
WHERE occ.id = 2675
    AND (occ.occurrence_code <> 'OCC021701'
    OR occ.occurrence_name <> 'EXPT0217-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021801',
    occurrence_name = 'EXPT0218-OCC01'
WHERE occ.id = 2739
    AND (occ.occurrence_code <> 'OCC021801'
    OR occ.occurrence_name <> 'EXPT0218-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021802',
    occurrence_name = 'EXPT0218-OCC02'
WHERE occ.id = 2740
    AND (occ.occurrence_code <> 'OCC021802'
    OR occ.occurrence_name <> 'EXPT0218-OCC02');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC021901',
    occurrence_name = 'EXPT0219-OCC01'
WHERE occ.id = 2666
    AND (occ.occurrence_code <> 'OCC021901'
    OR occ.occurrence_name <> 'EXPT0219-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC022001',
    occurrence_name = 'EXPT0220-OCC01'
WHERE occ.id = 2527
    AND (occ.occurrence_code <> 'OCC022001'
    OR occ.occurrence_name <> 'EXPT0220-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC022101',
    occurrence_name = 'EXPT0221-OCC01'
WHERE occ.id = 3043
    AND (occ.occurrence_code <> 'OCC022101'
    OR occ.occurrence_name <> 'EXPT0221-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC022201',
    occurrence_name = 'EXPT0222-OCC01'
WHERE occ.id = 3042
    AND (occ.occurrence_code <> 'OCC022201'
    OR occ.occurrence_name <> 'EXPT0222-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC022301',
    occurrence_name = 'EXPT0223-OCC01'
WHERE occ.id = 3044
    AND (occ.occurrence_code <> 'OCC022301'
    OR occ.occurrence_name <> 'EXPT0223-OCC01');

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC022401',
    occurrence_name = 'EXPT0224-OCC01'
WHERE occ.id = 2939
    AND (occ.occurrence_code <> 'OCC022401'
    OR occ.occurrence_name <> 'EXPT0224-OCC01');

--rollback SELECT NULL;