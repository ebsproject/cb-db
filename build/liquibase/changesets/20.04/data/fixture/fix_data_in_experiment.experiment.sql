--liquibase formatted sql

--changeset postgres:fix_data_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5716 Fix data in experiment.experiment



UPDATE experiment.experiment
SET experiment_design_type = NULL,
    steward_id = 1
WHERE id = 3;

UPDATE experiment.experiment
SET experiment_design_type = NULL,
    steward_id = 19
WHERE id = 5;

UPDATE experiment.experiment
SET experiment_design_type = 'Pedigree order',
    steward_id = 18
WHERE id = 13;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 15;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented RCBD',
    steward_id = 18
WHERE id = 16;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 18
WHERE id = 18;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 19;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 20;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 18
WHERE id = 21;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 30;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 18
WHERE id = 31;

UPDATE experiment.experiment
SET experiment_design_type = 'Pedigree order',
    steward_id = 18
WHERE id = 32;

UPDATE experiment.experiment
SET experiment_design_type = 'Pedigree order',
    steward_id = 18
WHERE id = 43;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented RCBD',
    steward_id = 18
WHERE id = 45;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented Design',
    steward_id = 18
WHERE id = 50;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented Design',
    steward_id = 18
WHERE id = 51;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented Design',
    steward_id = 18
WHERE id = 52;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 53;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic Arrangement',
    steward_id = 18
WHERE id = 55;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 18
WHERE id = 57;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 59;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 18
WHERE id = 61;

UPDATE experiment.experiment
SET experiment_design_type = 'RANDOM',
    steward_id = 18
WHERE id = 64;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 65;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 18
WHERE id = 84;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 86;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 89;

UPDATE experiment.experiment
SET experiment_design_type = 'Pedigree order',
    steward_id = 18
WHERE id = 90;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 93;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 18
WHERE id = 98;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 99;

UPDATE experiment.experiment
SET experiment_design_type = 'Random',
    steward_id = 18
WHERE id = 102;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 104;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 108;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented RCBD',
    steward_id = 27
WHERE id = 109;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 81
WHERE id = 111;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 114;

UPDATE experiment.experiment
SET experiment_design_type = 'Pedigree order',
    steward_id = 19
WHERE id = 115;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 116;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 117;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 18
WHERE id = 121;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 124;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 82
WHERE id = 125;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 128;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 82
WHERE id = 129;

UPDATE experiment.experiment
SET experiment_design_type = 'RCBD',
    steward_id = 30
WHERE id = 131;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 132;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 133;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 134;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 135;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 136;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 137;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 139;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented RCBD',
    steward_id = 27
WHERE id = 151;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 82
WHERE id = 152;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 153;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 81
WHERE id = 154;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 18
WHERE id = 156;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 157;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 81
WHERE id = 158;

UPDATE experiment.experiment
SET experiment_design_type = 'Complete Randomized Design',
    steward_id = 82
WHERE id = 161;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 18
WHERE id = 162;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 174;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 175;

UPDATE experiment.experiment
SET experiment_design_type = 'Systematic arrangement',
    steward_id = 82
WHERE id = 182;

UPDATE experiment.experiment
SET experiment_design_type = 'Augmented RCBD',
    steward_id = 27
WHERE id = 183;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 81
WHERE id = 189;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 81
WHERE id = 191;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 192;

UPDATE experiment.experiment
SET experiment_design_type = 'Row-Column',
    steward_id = 27
WHERE id = 194;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 81
WHERE id = 217;

UPDATE experiment.experiment
SET experiment_design_type = 'P-REP',
    steward_id = 27
WHERE id = 218;




--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = NULL,
--rollback     steward_id = 19
--rollback WHERE id = 3;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = NULL,
--rollback     steward_id = 1
--rollback WHERE id = 5;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 13;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Pedigree order',
--rollback     steward_id = 18
--rollback WHERE id = 15;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 16;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 18;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 19;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 18
--rollback WHERE id = 20;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 21;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Pedigree order',
--rollback     steward_id = 18
--rollback WHERE id = 30;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 31;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 32;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 43;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Pedigree order',
--rollback     steward_id = 18
--rollback WHERE id = 45;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic Arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 50;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 51;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 52;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented Design',
--rollback     steward_id = 18
--rollback WHERE id = 53;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented Design',
--rollback     steward_id = 18
--rollback WHERE id = 55;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented Design',
--rollback     steward_id = 18
--rollback WHERE id = 57;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 18
--rollback WHERE id = 59;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 61;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 64;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RANDOM',
--rollback     steward_id = 18
--rollback WHERE id = 65;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 84;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 18
--rollback WHERE id = 86;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Pedigree order',
--rollback     steward_id = 18
--rollback WHERE id = 89;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 90;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 93;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 98;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 18
--rollback WHERE id = 99;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 102;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Random',
--rollback     steward_id = 18
--rollback WHERE id = 104;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented RCBD',
--rollback     steward_id = 27
--rollback WHERE id = 108;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 109;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 111;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Pedigree order',
--rollback     steward_id = 19
--rollback WHERE id = 114;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 115;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 116;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 18
--rollback WHERE id = 117;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 81
--rollback WHERE id = 121;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 82
--rollback WHERE id = 124;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 125;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 82
--rollback WHERE id = 128;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 129;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 131;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 132;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 133;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 134;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 135;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 136;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 137;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'RCBD',
--rollback     steward_id = 30
--rollback WHERE id = 139;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 82
--rollback WHERE id = 151;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented RCBD',
--rollback     steward_id = 27
--rollback WHERE id = 152;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 18
--rollback WHERE id = 153;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 154;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 81
--rollback WHERE id = 156;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 81
--rollback WHERE id = 157;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 158;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 18
--rollback WHERE id = 161;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Complete Randomized Design',
--rollback     steward_id = 82
--rollback WHERE id = 162;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 174;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 175;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Augmented RCBD',
--rollback     steward_id = 27
--rollback WHERE id = 182;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Systematic arrangement',
--rollback     steward_id = 82
--rollback WHERE id = 183;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 189;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 27
--rollback WHERE id = 191;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 81
--rollback WHERE id = 192;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'Row-Column',
--rollback     steward_id = 81
--rollback WHERE id = 194;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 27
--rollback WHERE id = 217;

--rollback UPDATE experiment.experiment
--rollback SET experiment_design_type = 'P-REP',
--rollback     steward_id = 81
--rollback WHERE id = 218;
