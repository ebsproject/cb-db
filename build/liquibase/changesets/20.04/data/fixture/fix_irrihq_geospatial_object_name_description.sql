--liquibase formatted sql

--changeset postgres:fix_irrihq_geospatial_object_name_description context:fixture splitStatements:false
UPDATE 
	place.geospatial_object 
SET geospatial_object_name = 'IRRI, Los Baños, Laguna, Philippines',
	description = 'IRRI, Los Baños, Laguna, Philippines'
WHERE id = 10001;

--rollback UPDATE place.geospatial_object 
--rollback SET geospatial_object_name = 'IRRI, Los Ba\xc3\xb1os, Laguna, Philippines', description = 'IRRI, Los Ba\xc3\xb1os, Laguna, Philippines'
--rollback WHERE id = 10001;
