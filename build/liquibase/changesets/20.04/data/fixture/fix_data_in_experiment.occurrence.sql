--liquibase formatted sql

--changeset postgres:fix_data_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5716 Fix data in experiment.occurrence



-- correct mapping of occurrence to experiment
UPDATE experiment.occurrence
SET experiment_id = 1
WHERE id = 71;

UPDATE experiment.occurrence
SET experiment_id = 2
WHERE id = 72;

UPDATE experiment.occurrence
SET experiment_id = 3
WHERE id = 73;

UPDATE experiment.occurrence
SET experiment_id = 15
WHERE id = 206;

UPDATE experiment.occurrence
SET experiment_id = 13
WHERE id = 228;

UPDATE experiment.occurrence
SET experiment_id = 19
WHERE id = 230;

UPDATE experiment.occurrence
SET experiment_id = 21
WHERE id = 232;

UPDATE experiment.occurrence
SET experiment_id = 20
WHERE id = 233;

UPDATE experiment.occurrence
SET experiment_id = 18
WHERE id = 235;

UPDATE experiment.occurrence
SET experiment_id = 22
WHERE id = 236;

UPDATE experiment.occurrence
SET experiment_id = 30
WHERE id = 359;

UPDATE experiment.occurrence
SET experiment_id = 43
WHERE id = 421;

UPDATE experiment.occurrence
SET experiment_id = 47
WHERE id = 463;

UPDATE experiment.occurrence
SET experiment_id = 31
WHERE id = 473;

UPDATE experiment.occurrence
SET experiment_id = 46
WHERE id = 511;

UPDATE experiment.occurrence
SET experiment_id = 49
WHERE id = 524;

UPDATE experiment.occurrence
SET experiment_id = 48
WHERE id = 557;

UPDATE experiment.occurrence
SET experiment_id = 25
WHERE id = 700;

UPDATE experiment.occurrence
SET experiment_id = 25
WHERE id = 705;

UPDATE experiment.occurrence
SET experiment_id = 32
WHERE id = 716;

UPDATE experiment.occurrence
SET experiment_id = 16
WHERE id = 737;

UPDATE experiment.occurrence
SET experiment_id = 5
WHERE id = 815;

UPDATE experiment.occurrence
SET experiment_id = 24
WHERE id = 821;

UPDATE experiment.occurrence
SET experiment_id = 45
WHERE id = 843;

UPDATE experiment.occurrence
SET experiment_id = 99
WHERE id = 903;

UPDATE experiment.occurrence
SET experiment_id = 93
WHERE id = 907;

UPDATE experiment.occurrence
SET experiment_id = 65
WHERE id = 916;

UPDATE experiment.occurrence
SET experiment_id = 73
WHERE id = 1003;

UPDATE experiment.occurrence
SET experiment_id = 72
WHERE id = 1004;

UPDATE experiment.occurrence
SET experiment_id = 92
WHERE id = 1005;

UPDATE experiment.occurrence
SET experiment_id = 84
WHERE id = 1016;

UPDATE experiment.occurrence
SET experiment_id = 53
WHERE id = 1017;

UPDATE experiment.occurrence
SET experiment_id = 57
WHERE id = 1028;

UPDATE experiment.occurrence
SET experiment_id = 102
WHERE id = 1061;

UPDATE experiment.occurrence
SET experiment_id = 89
WHERE id = 1073;

UPDATE experiment.occurrence
SET experiment_id = 90
WHERE id = 1085;

UPDATE experiment.occurrence
SET experiment_id = 59
WHERE id = 1099;

UPDATE experiment.occurrence
SET experiment_id = 85
WHERE id = 1105;

UPDATE experiment.occurrence
SET experiment_id = 86
WHERE id = 1110;

UPDATE experiment.occurrence
SET experiment_id = 98
WHERE id = 1114;

UPDATE experiment.occurrence
SET experiment_id = 94
WHERE id = 1115;

UPDATE experiment.occurrence
SET experiment_id = 97
WHERE id = 1125;

UPDATE experiment.occurrence
SET experiment_id = 52
WHERE id = 1129;

UPDATE experiment.occurrence
SET experiment_id = 51
WHERE id = 1131;

UPDATE experiment.occurrence
SET experiment_id = 59
WHERE id = 1191;

UPDATE experiment.occurrence
SET experiment_id = 59
WHERE id = 1193;

UPDATE experiment.occurrence
SET experiment_id = 59
WHERE id = 1217;

UPDATE experiment.occurrence
SET experiment_id = 104
WHERE id = 1313;

UPDATE experiment.occurrence
SET experiment_id = 100
WHERE id = 1358;

UPDATE experiment.occurrence
SET experiment_id = 95
WHERE id = 1360;

UPDATE experiment.occurrence
SET experiment_id = 100
WHERE id = 1361;

UPDATE experiment.occurrence
SET experiment_id = 95
WHERE id = 1362;

UPDATE experiment.occurrence
SET experiment_id = 55
WHERE id = 1367;

UPDATE experiment.occurrence
SET experiment_id = 78
WHERE id = 1377;

UPDATE experiment.occurrence
SET experiment_id = 80
WHERE id = 1378;

UPDATE experiment.occurrence
SET experiment_id = 81
WHERE id = 1380;

UPDATE experiment.occurrence
SET experiment_id = 83
WHERE id = 1381;

UPDATE experiment.occurrence
SET experiment_id = 79
WHERE id = 1382;

UPDATE experiment.occurrence
SET experiment_id = 82
WHERE id = 1383;

UPDATE experiment.occurrence
SET experiment_id = 56
WHERE id = 1384;

UPDATE experiment.occurrence
SET experiment_id = 50
WHERE id = 1389;

UPDATE experiment.occurrence
SET experiment_id = 61
WHERE id = 1408;

UPDATE experiment.occurrence
SET experiment_id = 63
WHERE id = 1413;

UPDATE experiment.occurrence
SET experiment_id = 62
WHERE id = 1414;

UPDATE experiment.occurrence
SET experiment_id = 105
WHERE id = 1489;

UPDATE experiment.occurrence
SET experiment_id = 38
WHERE id = 1504;

UPDATE experiment.occurrence
SET experiment_id = 39
WHERE id = 1508;

UPDATE experiment.occurrence
SET experiment_id = 64
WHERE id = 1527;

UPDATE experiment.occurrence
SET experiment_id = 126
WHERE id = 1603;

UPDATE experiment.occurrence
SET experiment_id = 125
WHERE id = 1621;

UPDATE experiment.occurrence
SET experiment_id = 108
WHERE id = 1697;

UPDATE experiment.occurrence
SET experiment_id = 135
WHERE id = 1763;

UPDATE experiment.occurrence
SET experiment_id = 134
WHERE id = 1764;

UPDATE experiment.occurrence
SET experiment_id = 116
WHERE id = 1765;

UPDATE experiment.occurrence
SET experiment_id = 113
WHERE id = 1766;

UPDATE experiment.occurrence
SET experiment_id = 120
WHERE id = 1769;

UPDATE experiment.occurrence
SET experiment_id = 111
WHERE id = 1771;

UPDATE experiment.occurrence
SET experiment_id = 120
WHERE id = 1785;

UPDATE experiment.occurrence
SET experiment_id = 111
WHERE id = 1786;

UPDATE experiment.occurrence
SET experiment_id = 118
WHERE id = 1788;

UPDATE experiment.occurrence
SET experiment_id = 119
WHERE id = 1789;

UPDATE experiment.occurrence
SET experiment_id = 107
WHERE id = 1792;

UPDATE experiment.occurrence
SET experiment_id = 106
WHERE id = 1793;

UPDATE experiment.occurrence
SET experiment_id = 139
WHERE id = 1794;

UPDATE experiment.occurrence
SET experiment_id = 137
WHERE id = 1796;

UPDATE experiment.occurrence
SET experiment_id = 109
WHERE id = 1799;

UPDATE experiment.occurrence
SET experiment_id = 138
WHERE id = 1800;

UPDATE experiment.occurrence
SET experiment_id = 132
WHERE id = 1801;

UPDATE experiment.occurrence
SET experiment_id = 128
WHERE id = 1822;

UPDATE experiment.occurrence
SET experiment_id = 131
WHERE id = 1841;

UPDATE experiment.occurrence
SET experiment_id = 145
WHERE id = 1855;

UPDATE experiment.occurrence
SET experiment_id = 146
WHERE id = 1861;

UPDATE experiment.occurrence
SET experiment_id = 133
WHERE id = 1868;

UPDATE experiment.occurrence
SET experiment_id = 136
WHERE id = 1888;

UPDATE experiment.occurrence
SET experiment_id = 121
WHERE id = 1889;

UPDATE experiment.occurrence
SET experiment_id = 143
WHERE id = 1955;

UPDATE experiment.occurrence
SET experiment_id = 142
WHERE id = 1956;

UPDATE experiment.occurrence
SET experiment_id = 117
WHERE id = 1981;

UPDATE experiment.occurrence
SET experiment_id = 114
WHERE id = 1982;

UPDATE experiment.occurrence
SET experiment_id = 117
WHERE id = 1988;

UPDATE experiment.occurrence
SET experiment_id = 114
WHERE id = 1989;

UPDATE experiment.occurrence
SET experiment_id = 117
WHERE id = 1990;

UPDATE experiment.occurrence
SET experiment_id = 114
WHERE id = 1991;

UPDATE experiment.occurrence
SET experiment_id = 141
WHERE id = 2026;

UPDATE experiment.occurrence
SET experiment_id = 124
WHERE id = 2059;

UPDATE experiment.occurrence
SET experiment_id = 129
WHERE id = 2125;

UPDATE experiment.occurrence
SET experiment_id = 174
WHERE id = 2134;

UPDATE experiment.occurrence
SET experiment_id = 140
WHERE id = 2177;

UPDATE experiment.occurrence
SET experiment_id = 115
WHERE id = 2178;

UPDATE experiment.occurrence
SET experiment_id = 112
WHERE id = 2179;

UPDATE experiment.occurrence
SET experiment_id = 151
WHERE id = 2181;

UPDATE experiment.occurrence
SET experiment_id = 180
WHERE id = 2183;

UPDATE experiment.occurrence
SET experiment_id = 178
WHERE id = 2184;

UPDATE experiment.occurrence
SET experiment_id = 167
WHERE id = 2221;

UPDATE experiment.occurrence
SET experiment_id = 164
WHERE id = 2223;

UPDATE experiment.occurrence
SET experiment_id = 166
WHERE id = 2225;

UPDATE experiment.occurrence
SET experiment_id = 168
WHERE id = 2226;

UPDATE experiment.occurrence
SET experiment_id = 152
WHERE id = 2232;

UPDATE experiment.occurrence
SET experiment_id = 158
WHERE id = 2247;

UPDATE experiment.occurrence
SET experiment_id = 154
WHERE id = 2248;

UPDATE experiment.occurrence
SET experiment_id = 155
WHERE id = 2249;

UPDATE experiment.occurrence
SET experiment_id = 169
WHERE id = 2254;

UPDATE experiment.occurrence
SET experiment_id = 170
WHERE id = 2255;

UPDATE experiment.occurrence
SET experiment_id = 150
WHERE id = 2313;

UPDATE experiment.occurrence
SET experiment_id = 171
WHERE id = 2324;

UPDATE experiment.occurrence
SET experiment_id = 156
WHERE id = 2332;

UPDATE experiment.occurrence
SET experiment_id = 158
WHERE id = 2335;

UPDATE experiment.occurrence
SET experiment_id = 154
WHERE id = 2336;

UPDATE experiment.occurrence
SET experiment_id = 153
WHERE id = 2338;

UPDATE experiment.occurrence
SET experiment_id = 175
WHERE id = 2412;

UPDATE experiment.occurrence
SET experiment_id = 172
WHERE id = 2442;

UPDATE experiment.occurrence
SET experiment_id = 203
WHERE id = 2445;

UPDATE experiment.occurrence
SET experiment_id = 211
WHERE id = 2449;

UPDATE experiment.occurrence
SET experiment_id = 157
WHERE id = 2465;

UPDATE experiment.occurrence
SET experiment_id = 153
WHERE id = 2466;

UPDATE experiment.occurrence
SET experiment_id = 157
WHERE id = 2467;

UPDATE experiment.occurrence
SET experiment_id = 153
WHERE id = 2468;

UPDATE experiment.occurrence
SET experiment_id = 183
WHERE id = 2632;

UPDATE experiment.occurrence
SET experiment_id = 214
WHERE id = 2633;

UPDATE experiment.occurrence
SET experiment_id = 215
WHERE id = 2634;

UPDATE experiment.occurrence
SET experiment_id = 182
WHERE id = 2674;

UPDATE experiment.occurrence
SET experiment_id = 217
WHERE id = 2675;

UPDATE experiment.occurrence
SET experiment_id = 192
WHERE id = 2676;

UPDATE experiment.occurrence
SET experiment_id = 193
WHERE id = 2701;

UPDATE experiment.occurrence
SET experiment_id = 193
WHERE id = 2702;

UPDATE experiment.occurrence
SET experiment_id = 181
WHERE id = 2738;

UPDATE experiment.occurrence
SET experiment_id = 218
WHERE id = 2739;

UPDATE experiment.occurrence
SET experiment_id = 218
WHERE id = 2740;

UPDATE experiment.occurrence
SET experiment_id = 194
WHERE id = 2741;

UPDATE experiment.occurrence
SET experiment_id = 189
WHERE id = 2795;

UPDATE experiment.occurrence
SET experiment_id = 190
WHERE id = 2797;

UPDATE experiment.occurrence
SET experiment_id = 188
WHERE id = 2837;

UPDATE experiment.occurrence
SET experiment_id = 191
WHERE id = 2842;

UPDATE experiment.occurrence
SET experiment_id = 204
WHERE id = 3370;

UPDATE experiment.occurrence
SET experiment_id = 205
WHERE id = 3371;

UPDATE experiment.occurrence
SET experiment_id = 206
WHERE id = 3373;

UPDATE experiment.occurrence
SET experiment_id = 207
WHERE id = 3374;

UPDATE experiment.occurrence
SET experiment_id = 202
WHERE id = 3376;


-- correct experiment_code and experiment_name
-- set temporary values
UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016101_TEMP',
    occurrence_name = 'EXPT0161-OCC01_TEMP'
WHERE occ.id = 2233;

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016201',
    occurrence_name = 'EXPT0162-OCC01'
WHERE occ.id = 2359;

UPDATE experiment.occurrence AS occ
SET occurrence_code = 'OCC016101',
    occurrence_name = 'EXPT0161-OCC01'
WHERE occ.id = 2233;



--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 2
--rollback WHERE id = 71;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 1
--rollback WHERE id = 72;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 5
--rollback WHERE id = 73;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 16
--rollback WHERE id = 206;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 15
--rollback WHERE id = 228;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 21
--rollback WHERE id = 230;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 19
--rollback WHERE id = 232;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 22
--rollback WHERE id = 233;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 20
--rollback WHERE id = 235;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 18
--rollback WHERE id = 236;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 31
--rollback WHERE id = 359;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 45
--rollback WHERE id = 421;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 46
--rollback WHERE id = 463;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 32
--rollback WHERE id = 473;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 49
--rollback WHERE id = 511;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 48
--rollback WHERE id = 524;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 47
--rollback WHERE id = 557;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 24
--rollback WHERE id = 700;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 24
--rollback WHERE id = 705;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 30
--rollback WHERE id = 716;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 13
--rollback WHERE id = 737;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 3
--rollback WHERE id = 815;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 25
--rollback WHERE id = 821;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 43
--rollback WHERE id = 843;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 98
--rollback WHERE id = 903;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 92
--rollback WHERE id = 907;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 64
--rollback WHERE id = 916;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 72
--rollback WHERE id = 1003;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 73
--rollback WHERE id = 1004;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 90
--rollback WHERE id = 1005;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 86
--rollback WHERE id = 1016;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 56
--rollback WHERE id = 1017;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 52
--rollback WHERE id = 1028;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 104
--rollback WHERE id = 1061;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 93
--rollback WHERE id = 1073;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 89
--rollback WHERE id = 1085;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 61
--rollback WHERE id = 1099;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 84
--rollback WHERE id = 1105;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 85
--rollback WHERE id = 1110;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 94
--rollback WHERE id = 1114;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 95
--rollback WHERE id = 1115;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 100
--rollback WHERE id = 1125;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 57
--rollback WHERE id = 1129;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 55
--rollback WHERE id = 1131;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 61
--rollback WHERE id = 1191;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 61
--rollback WHERE id = 1193;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 61
--rollback WHERE id = 1217;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 102
--rollback WHERE id = 1313;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 99
--rollback WHERE id = 1358;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 97
--rollback WHERE id = 1360;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 99
--rollback WHERE id = 1361;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 97
--rollback WHERE id = 1362;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 50
--rollback WHERE id = 1367;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 79
--rollback WHERE id = 1377;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 81
--rollback WHERE id = 1378;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 78
--rollback WHERE id = 1380;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 82
--rollback WHERE id = 1381;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 80
--rollback WHERE id = 1382;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 83
--rollback WHERE id = 1383;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 51
--rollback WHERE id = 1384;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 53
--rollback WHERE id = 1389;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 59
--rollback WHERE id = 1408;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 62
--rollback WHERE id = 1413;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 63
--rollback WHERE id = 1414;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 106
--rollback WHERE id = 1489;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 39
--rollback WHERE id = 1504;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 38
--rollback WHERE id = 1508;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 65
--rollback WHERE id = 1527;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 124
--rollback WHERE id = 1603;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 126
--rollback WHERE id = 1621;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 109
--rollback WHERE id = 1697;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 131
--rollback WHERE id = 1763;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 132
--rollback WHERE id = 1764;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 113
--rollback WHERE id = 1765;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 115
--rollback WHERE id = 1766;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 118
--rollback WHERE id = 1769;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 120
--rollback WHERE id = 1771;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 118
--rollback WHERE id = 1785;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 120
--rollback WHERE id = 1786;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 119
--rollback WHERE id = 1788;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 121
--rollback WHERE id = 1789;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 108
--rollback WHERE id = 1792;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 105
--rollback WHERE id = 1793;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 136
--rollback WHERE id = 1794;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 133
--rollback WHERE id = 1796;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 107
--rollback WHERE id = 1799;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 135
--rollback WHERE id = 1800;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 138
--rollback WHERE id = 1801;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 129
--rollback WHERE id = 1822;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 139
--rollback WHERE id = 1841;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 146
--rollback WHERE id = 1855;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 145
--rollback WHERE id = 1861;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 134
--rollback WHERE id = 1868;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 137
--rollback WHERE id = 1888;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 117
--rollback WHERE id = 1889;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 142
--rollback WHERE id = 1955;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 143
--rollback WHERE id = 1956;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 111
--rollback WHERE id = 1981;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 116
--rollback WHERE id = 1982;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 111
--rollback WHERE id = 1988;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 116
--rollback WHERE id = 1989;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 111
--rollback WHERE id = 1990;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 116
--rollback WHERE id = 1991;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 140
--rollback WHERE id = 2026;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 125
--rollback WHERE id = 2059;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 128
--rollback WHERE id = 2125;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 175
--rollback WHERE id = 2134;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 141
--rollback WHERE id = 2177;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 112
--rollback WHERE id = 2178;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 114
--rollback WHERE id = 2179;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 150
--rollback WHERE id = 2181;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 178
--rollback WHERE id = 2183;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 180
--rollback WHERE id = 2184;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 166
--rollback WHERE id = 2221;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 168
--rollback WHERE id = 2223;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 164
--rollback WHERE id = 2225;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 167
--rollback WHERE id = 2226;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 151
--rollback WHERE id = 2232;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 156
--rollback WHERE id = 2247;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 157
--rollback WHERE id = 2248;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 158
--rollback WHERE id = 2249;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 170
--rollback WHERE id = 2254;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 169
--rollback WHERE id = 2255;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 152
--rollback WHERE id = 2313;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 172
--rollback WHERE id = 2324;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 153
--rollback WHERE id = 2332;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 156
--rollback WHERE id = 2335;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 157
--rollback WHERE id = 2336;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 155
--rollback WHERE id = 2338;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 174
--rollback WHERE id = 2412;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 171
--rollback WHERE id = 2442;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 211
--rollback WHERE id = 2445;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 207
--rollback WHERE id = 2449;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 154
--rollback WHERE id = 2465;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 155
--rollback WHERE id = 2466;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 154
--rollback WHERE id = 2467;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 155
--rollback WHERE id = 2468;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 181
--rollback WHERE id = 2632;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 215
--rollback WHERE id = 2633;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 214
--rollback WHERE id = 2634;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 183
--rollback WHERE id = 2674;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 218
--rollback WHERE id = 2675;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 188
--rollback WHERE id = 2676;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 189
--rollback WHERE id = 2701;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 189
--rollback WHERE id = 2702;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 182
--rollback WHERE id = 2738;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 217
--rollback WHERE id = 2739;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 217
--rollback WHERE id = 2740;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 191
--rollback WHERE id = 2741;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 194
--rollback WHERE id = 2795;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 192
--rollback WHERE id = 2797;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 193
--rollback WHERE id = 2837;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 190
--rollback WHERE id = 2842;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 205
--rollback WHERE id = 3370;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 203
--rollback WHERE id = 3371;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 202
--rollback WHERE id = 3373;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 206
--rollback WHERE id = 3374;

--rollback UPDATE experiment.occurrence
--rollback SET experiment_id = 204
--rollback WHERE id = 3376;



--rollback UPDATE experiment.occurrence AS occ
--rollback SET occurrence_code = 'OCC016101_TEMP',
--rollback     occurrence_name = 'EXPT0161-OCC01_TEMP'
--rollback WHERE occ.id = 2359;

--rollback UPDATE experiment.occurrence AS occ
--rollback SET occurrence_code = 'OCC016201',
--rollback     occurrence_name = 'EXPT0162-OCC01'
--rollback WHERE occ.id = 2233;

--rollback UPDATE experiment.occurrence AS occ
--rollback SET occurrence_code = 'OCC016101',
--rollback     occurrence_name = 'EXPT0161-OCC01'
--rollback WHERE occ.id = 2359;