--liquibase formatted sql

--changeset postgres:insert_data_to_germplasm.cross context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5464 Insert data to germplasm.cross



-- insert fixture data
INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20097', 'IR 112205', 'single cross', '162500', '156531', '31', '181155', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20098', 'IR 112206', 'single cross', '162501', '156532', '31', '181156', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20102', 'IR 112212', 'single cross', '162505', '156536', '31', '181161', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20103', 'IR 112213', 'single cross', '162506', '156537', '31', '181162', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20104', 'IR 112216', 'single cross', '116545', '110430', '30', '119792', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20105', 'IR 112217', 'single cross', '116546', '110431', '30', '119793', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20107', 'IR 112219', 'single cross', '162509', '156540', '31', '181168', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20108', 'IR 112220', 'single cross', '162510', '156541', '31', '181169', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20109', 'IR 112221', 'single cross', '116548', '110433', '30', '119795', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20111', 'IR 112225', 'single cross', '162514', '156545', '31', '181174', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20112', 'IR 112226', 'single cross', '162515', '156546', '31', '181175', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20118', 'IR 112234', 'single cross', '162522', '156553', '31', '181185', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20122', 'IR 112239', 'single cross', '162527', '156558', '31', '181191', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20129', 'IR 112207', 'single cross', '162502', '156533', '31', '181157', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20136', 'IR 112224', 'single cross', '162513', '156544', '31', '181173', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20138', 'IR 112229', 'single cross', '162518', '156549', '31', '181179', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20139', 'IR 112230', 'single cross', '162519', '156550', '31', '181180', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20140', 'IR 112231', 'single cross', '162520', '156551', '31', '181181', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20141', 'IR 112235', 'single cross', '162523', '156554', '31', '181186', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20142', 'IR 112236', 'single cross', '162524', '156555', '31', '181187', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20143', 'IR 112240', 'single cross', '162528', '156559', '31', '181192', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20144', 'IR 112241', 'single cross', '162529', '156560', '31', '181193', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20145', 'IR 112243', 'single cross', '162531', '156562', '31', '181196', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20146', 'IR 112244', 'single cross', '162532', '156563', '31', '181197', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20147', 'IR 112245', 'single cross', '162533', '156564', '31', '181198', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20149', 'IR 112248', 'single cross', '116550', '110435', '30', '119797', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20151', 'IR 112250', 'single cross', '116552', '110437', '30', '119799', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20152', 'IR 112251', 'single cross', '116553', '110438', '30', '119800', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20153', 'IR 112252', 'single cross', '116554', '110439', '30', '119801', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20154', 'IR 112253', 'single cross', '116555', '110440', '30', '119802', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20155', 'IR 112254', 'single cross', '116556', '110441', '30', '119803', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20157', 'IR 112256', 'single cross', '116558', '110443', '30', '119805', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20179', 'IR 112607', 'backcross', '162701', '156732', '33', '255339', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('20180', 'IR 112608', 'backcross', '162702', '156733', '33', '255340', '18', '2014-10-09 08:16:10.036205');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21154', 'IR 117532', 'single cross', '257562', '252624', '67', '317074', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21165', 'IR 117511', 'single cross', '257544', '252606', '67', '317092', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21171', 'IR 117381', 'single cross', '257442', '252504', '67', '317104', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21175', 'IR 117415', 'single cross', '257469', '252531', '67', '317130', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21177', 'IR 117421', 'single cross', '257474', '252536', '67', '317131', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21179', 'IR 117430', 'single cross', '257482', '252544', '67', '317134', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21183', 'IR 117510', 'single cross', '264052', '259149', '67', '317091', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21185', 'IR 117450', 'single cross', '257500', '252562', '67', '317121', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21187', 'IR 117465', 'single cross', '257510', '252572', '67', '317127', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21194', 'IR 117502', 'single cross', '257538', '252600', '67', '317083', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21196', 'IR 117516', 'single cross', '264053', '259150', '67', '317097', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21197', 'IR 117517', 'single cross', '264054', '259151', '67', '317098', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21218', 'IR 117621', 'single cross', '257635', '252697', '67', '317049', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21219', 'IR 117622', 'single cross', '257636', '252698', '67', '317050', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21220', 'IR 117627', 'single cross', '257641', '252703', '67', '317055', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21221', 'IR 117630', 'single cross', '257644', '252706', '67', '317058', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21230', 'IR 117536', 'single cross', '257564', '252626', '67', '317078', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21251', 'IR 117512', 'single cross', '257545', '252607', '67', '317093', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21252', 'IR 117513', 'single cross', '257546', '252608', '67', '317094', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21257', 'IR 117409', 'single cross', '264028', '259121', '67', '317114', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21258', 'IR 117526', 'single cross', '257557', '252619', '67', '317068', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21262', 'IR 117375', 'single cross', '257436', '252498', '67', '317103', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21266', 'IR 117384', 'single cross', '257445', '252507', '67', '317106', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21270', 'IR 117391', 'single cross', '264025', '259118', '67', '317108', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21271', 'IR 117394', 'single cross', '257453', '252515', '67', '317109', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21272', 'IR 117395', 'single cross', '257454', '252516', '67', '317110', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21283', 'IR 117414', 'single cross', '257468', '252530', '67', '317129', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21285', 'IR 117424', 'single cross', '257476', '252538', '67', '317133', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21289', 'IR 117434', 'single cross', '257486', '252548', '67', '317116', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21292', 'IR 117443', 'single cross', '257495', '252557', '67', '317119', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21293', 'IR 117444', 'single cross', '257496', '252558', '67', '317120', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21295', 'IR 117456', 'single cross', '257504', '252566', '67', '317124', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21297', 'IR 117455', 'single cross', '257503', '252565', '67', '317123', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21302', 'IR 117471', 'single cross', '257515', '252577', '67', '317128', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21314', 'IR 117498', 'single cross', '257536', '252598', '67', '317079', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21315', 'IR 117499', 'single cross', '257537', '252599', '67', '317080', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21316', 'IR 117508', 'single cross', '257542', '252604', '67', '317089', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21317', 'IR 117504', 'single cross', '257540', '252602', '67', '317085', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21318', 'IR 117518', 'single cross', '257549', '252611', '67', '317099', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21319', 'IR 117505', 'single cross', '264050', '259147', '67', '317086', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21321', 'IR 117519', 'single cross', '257550', '252612', '67', '317100', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21322', 'IR 117522', 'single cross', '257553', '252615', '67', '317064', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21325', 'IR 117531', 'single cross', '257561', '252623', '67', '317073', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21350', 'IR 117623', 'single cross', '257637', '252699', '67', '317051', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21351', 'IR 117625', 'single cross', '257639', '252701', '67', '317053', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21352', 'IR 117631', 'single cross', '257645', '252707', '67', '317059', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21353', 'IR 117626', 'single cross', '257640', '252702', '67', '317054', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21355', 'IR 117632', 'single cross', '264069', '259166', '67', '317060', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21358', 'IR 117620', 'single cross', '264068', '259165', '67', '317048', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21360', 'IR 117371', 'single cross', '257432', '252494', '67', '317101', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21362', 'IR 117464', 'single cross', '257509', '252571', '67', '317126', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21367', 'IR 117374', 'single cross', '257435', '252497', '67', '317102', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21375', 'IR 117400', 'single cross', '265171', '260276', '67', '317111', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21384', 'IR 117423', 'single cross', '264031', '259124', '67', '317132', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21388', 'IR 117432', 'single cross', '257484', '252546', '67', '317115', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21389', 'IR 117435', 'single cross', '257487', '252549', '67', '317117', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21391', 'IR 117452', 'single cross', '264037', '259134', '67', '317122', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21392', 'IR 117441', 'single cross', '257493', '252555', '67', '317118', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21398', 'IR 117462', 'single cross', '264041', '259138', '67', '317125', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21409', 'IR 117500', 'single cross', '264049', '259146', '67', '317081', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21410', 'IR 117501', 'single cross', '264909', '260014', '67', '317082', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21411', 'IR 117506', 'single cross', '264051', '259148', '67', '317087', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21412', 'IR 117507', 'single cross', '257541', '252603', '67', '317088', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21413', 'IR 117514', 'single cross', '257547', '252609', '67', '317095', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21414', 'IR 117515', 'single cross', '257548', '252610', '67', '317096', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21416', 'IR 117521', 'single cross', '257552', '252614', '67', '317063', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21417', 'IR 117527', 'single cross', '264055', '259152', '67', '317069', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21421', 'IR 117624', 'single cross', '257638', '252700', '67', '317052', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21422', 'IR 117628', 'single cross', '257642', '252704', '67', '317056', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21423', 'IR 117629', 'single cross', '257643', '252705', '67', '317057', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('21425', 'IR 117634', 'single cross', '257646', '252708', '67', '317062', '18', '2015-01-06 01:22:48.888328');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84900', 'IR 121393', 'backcross', '397553', '394944', '127', '558096', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84901', 'IR 121394', 'backcross', '397554', '394945', '127', '558097', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84902', 'IR 121395', 'backcross', '397555', '394946', '127', '558098', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84903', 'IR 121396', 'backcross', '397556', '394947', '127', '558099', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84904', 'IR 121397', 'backcross', '397557', '394948', '127', '558100', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84905', 'IR 121398', 'backcross', '397558', '394949', '127', '558101', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84906', 'IR 121399', 'backcross', '397559', '394950', '127', '558102', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84907', 'IR 121400', 'backcross', '397560', '394951', '127', '558103', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84908', 'IR 121402', 'backcross', '397562', '394953', '127', '558105', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84909', 'IR 121403', 'backcross', '397563', '394954', '127', '558106', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84910', 'IR 121404', 'backcross', '397564', '394955', '127', '558107', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84911', 'IR 121405', 'backcross', '397565', '394956', '127', '558108', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84927', 'IR 121251', 'single cross', '397417', '394808', '125', '520182', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84928', 'IR 121252', 'single cross', '397418', '394809', '125', '520183', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84929', 'IR 121253', 'single cross', '397419', '394810', '125', '520184', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84930', 'IR 121254', 'single cross', '397420', '394811', '125', '520185', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84935', 'IR 121259', 'single cross', '397425', '394816', '125', '520186', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84936', 'IR 121260', 'single cross', '397426', '394817', '125', '520187', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84937', 'IR 121261', 'single cross', '397427', '394818', '125', '520188', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84938', 'IR 121262', 'single cross', '397428', '394819', '125', '520189', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84939', 'IR 121263', 'single cross', '397429', '394820', '125', '520190', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84940', 'IR 121264', 'single cross', '397430', '394821', '125', '520191', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84941', 'IR 121265', 'single cross', '397431', '394822', '125', '520192', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84942', 'IR 121266', 'single cross', '397432', '394823', '125', '520193', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84943', 'IR 121267', 'single cross', '397433', '394824', '125', '520194', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84944', 'IR 121268', 'single cross', '397434', '394825', '125', '520195', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84945', 'IR 121269', 'single cross', '397435', '394826', '125', '520196', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84946', 'IR 121270', 'single cross', '397436', '394827', '125', '520197', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84947', 'IR 121271', 'single cross', '397437', '394828', '125', '520198', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84948', 'IR 121272', 'single cross', '397438', '394829', '125', '520199', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84949', 'IR 121273', 'single cross', '397439', '394830', '125', '520200', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84950', 'IR 121274', 'single cross', '397440', '394831', '125', '520201', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84951', 'IR 121275', 'single cross', '397441', '394832', '125', '520202', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84952', 'IR 121276', 'single cross', '397442', '394833', '125', '520203', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84953', 'IR 121277', 'single cross', '397443', '394834', '125', '520204', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84954', 'IR 121278', 'single cross', '397444', '394835', '125', '520205', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84955', 'IR 121279', 'single cross', '397445', '394836', '125', '520206', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84956', 'IR 121280', 'single cross', '397446', '394837', '125', '520207', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84957', 'IR 121281', 'single cross', '397447', '394838', '125', '520208', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84958', 'IR 121282', 'single cross', '397448', '394839', '125', '520209', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84959', 'IR 121283', 'single cross', '397449', '394840', '126', '546268', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84960', 'IR 121284', 'single cross', '397450', '394841', '126', '546269', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84961', 'IR 121285', 'single cross', '397451', '394842', '126', '546270', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84962', 'IR 121286', 'single cross', '397452', '394843', '126', '546271', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84963', 'IR 121287', 'single cross', '397453', '394844', '126', '546272', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84964', 'IR 121288', 'single cross', '397454', '394845', '126', '546273', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84965', 'IR 121289', 'single cross', '397455', '394846', '126', '546274', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84966', 'IR 121290', 'single cross', '397456', '394847', '126', '546275', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84967', 'IR 121291', 'single cross', '397457', '394848', '126', '546276', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84968', 'IR 121292', 'single cross', '397458', '394849', '126', '546277', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84969', 'IR 121293', 'single cross', '397459', '394850', '125', '520210', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84970', 'IR 121294', 'single cross', '397460', '394851', '125', '520211', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84971', 'IR 121295', 'single cross', '397461', '394852', '125', '520212', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84972', 'IR 121296', 'single cross', '397462', '394853', '125', '520213', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84973', 'IR 121297', 'single cross', '397463', '394854', '125', '520214', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84974', 'IR 121298', 'single cross', '397464', '394855', '125', '520215', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84977', 'IR 121303', 'single cross', '397467', '394858', '127', '558092', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84978', 'IR 121304', 'single cross', '397468', '394859', '127', '558093', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84979', 'IR 121305', 'single cross', '397469', '394860', '127', '558094', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84980', 'IR 121306', 'single cross', '397470', '394861', '127', '558095', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84986', 'IR 121312', 'single cross', '397476', '394867', '127', '558069', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84987', 'IR 121313', 'single cross', '397477', '394868', '127', '558068', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('84988', 'IR 121314', 'single cross', '397478', '394869', '127', '558070', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85005', 'IR 121356', 'three-way cross', '397516', '394907', '162', '745433', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85006', 'IR 121357', 'three-way cross', '397517', '394908', '162', '745432', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85007', 'IR 121358', 'three-way cross', '397518', '394909', '162', '745436', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85008', 'IR 121359', 'three-way cross', '397519', '394910', '162', '745435', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85009', 'IR 121360', 'three-way cross', '397520', '394911', '162', '745434', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85010', 'IR 121361', 'three-way cross', '397521', '394912', '162', '745439', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85011', 'IR 121362', 'three-way cross', '397522', '394913', '162', '745438', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85012', 'IR 121363', 'three-way cross', '397523', '394914', '162', '745437', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85013', 'IR 121364', 'three-way cross', '397524', '394915', '162', '745442', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85014', 'IR 121365', 'three-way cross', '397525', '394916', '162', '745441', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85015', 'IR 121366', 'three-way cross', '397526', '394917', '162', '745440', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85017', 'IR 121367', 'three-way cross', '397527', '394918', '162', '745445', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85018', 'IR 121368', 'three-way cross', '397528', '394919', '162', '745444', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85019', 'IR 121369', 'three-way cross', '397529', '394920', '162', '745443', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85020', 'IR 121370', 'three-way cross', '397530', '394921', '162', '745451', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85021', 'IR 121371', 'three-way cross', '397531', '394922', '162', '745450', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85022', 'IR 121372', 'three-way cross', '397532', '394923', '162', '745453', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85023', 'IR 121373', 'three-way cross', '397533', '394924', '162', '745452', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85024', 'IR 121374', 'three-way cross', '397534', '394925', '162', '745455', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85025', 'IR 121375', 'three-way cross', '397535', '394926', '162', '745454', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85026', 'IR 121376', 'three-way cross', '397536', '394927', '162', '745456', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85027', 'IR 121377', 'three-way cross', '397537', '394928', '162', '745457', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85028', 'IR 121378', 'three-way cross', '397538', '394929', '162', '745458', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85029', 'IR 121379', 'three-way cross', '397539', '394930', '162', '745459', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85030', 'IR 121380', 'three-way cross', '397540', '394931', '162', '745460', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85036', 'IR 121386', 'three-way cross', '397546', '394937', '162', '745446', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85037', 'IR 121387', 'three-way cross', '397547', '394938', '162', '745448', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('85042', 'IR 121401', 'backcross', '397561', '394952', '127', '558104', '18', '2015-05-29 10:18:35.242856');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99809', 'IR 125565', 'single cross', '521206', '521877', '161', '726079', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99810', 'IR 125588', 'single cross', '521229', '521900', '162', '745387', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99814', 'IR 125566', 'single cross', '521207', '521878', '161', '726080', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99815', 'IR 125567', 'single cross', '521208', '521879', '161', '726081', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99816', 'IR 125568', 'single cross', '521209', '521880', '161', '726082', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99817', 'IR 125569', 'single cross', '521210', '521881', '161', '726083', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99818', 'IR 125571', 'single cross', '521212', '521883', '161', '726085', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99819', 'IR 125572', 'single cross', '521213', '521884', '161', '726086', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99820', 'IR 125573', 'single cross', '521214', '521885', '161', '726087', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99821', 'IR 125574', 'single cross', '521215', '521886', '161', '726088', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99822', 'IR 125570', 'single cross', '521211', '521882', '161', '726084', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99823', 'IR 125575', 'single cross', '521216', '521887', '161', '726089', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99824', 'IR 125576', 'single cross', '521217', '521888', '161', '726090', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99825', 'IR 125577', 'single cross', '521218', '521889', '161', '726091', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99826', 'IR 125578', 'single cross', '521219', '521890', '161', '726092', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99827', 'IR 125579', 'single cross', '521220', '521891', '161', '726093', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99828', 'IR 125581', 'single cross', '521222', '521893', '161', '726095', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99829', 'IR 125582', 'single cross', '521223', '521894', '162', '745381', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99830', 'IR 125583', 'single cross', '521224', '521895', '162', '745382', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99831', 'IR 125584', 'single cross', '521225', '521896', '162', '745383', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99832', 'IR 125580', 'single cross', '521221', '521892', '161', '726094', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99833', 'IR 125585', 'single cross', '521226', '521897', '162', '745384', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99834', 'IR 125586', 'single cross', '521227', '521898', '162', '745385', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99835', 'IR 125587', 'single cross', '521228', '521899', '162', '745386', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99836', 'IR 125589', 'single cross', '521230', '521901', '162', '745388', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99837', 'IR 125590', 'single cross', '521231', '521902', '162', '745389', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99838', 'IR 125591', 'single cross', '521232', '521903', '162', '745390', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99839', 'IR 125592', 'single cross', '521233', '521904', '162', '745391', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99840', 'IR 125593', 'single cross', '521234', '521905', '162', '745392', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99841', 'IR 125594', 'single cross', '521235', '521906', '162', '745393', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99842', 'IR 125595', 'single cross', '521236', '521907', '162', '745394', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99843', 'IR 125598', 'single cross', '521239', '521910', '162', '745397', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99844', 'IR 125599', 'single cross', '521240', '521911', '162', '745398', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99845', 'IR 125596', 'single cross', '521237', '521908', '162', '745395', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99846', 'IR 125597', 'single cross', '521238', '521909', '162', '745396', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99847', 'IR 125600', 'single cross', '521241', '521912', '162', '745399', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99848', 'IR 125601', 'single cross', '521242', '521913', '162', '745400', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99849', 'IR 125602', 'single cross', '521243', '521914', '162', '745401', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99850', 'IR 125603', 'single cross', '521244', '521915', '162', '745402', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99851', 'IR 125604', 'single cross', '521245', '521916', '162', '745403', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99852', 'IR 125605', 'single cross', '521246', '521917', '162', '745404', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99853', 'IR 125606', 'single cross', '521247', '521918', '162', '745405', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99854', 'IR 125607', 'single cross', '521248', '521919', '162', '745406', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99855', 'IR 125608', 'single cross', '521249', '521920', '162', '745407', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99856', 'IR 125609', 'single cross', '521250', '521921', '162', '745408', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99857', 'IR 125610', 'single cross', '521251', '521922', '162', '745409', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99858', 'IR 125611', 'single cross', '521252', '521923', '162', '745410', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99859', 'IR 125615', 'backcross', '521256', '521927', '162', '745414', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99860', 'IR 125616', 'three-way cross', '521257', '521928', '162', '745415', '18', '2015-11-12 17:35:34.463136');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99876', 'IR 125631', 'backcross', '521334', '522005', '162', '745430', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99877', 'IR 125632', 'backcross', '521335', '522006', '162', '745431', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99878', 'IR 125617', 'backcross', '521320', '521991', '162', '745416', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99879', 'IR 125618', 'backcross', '521321', '521992', '162', '745417', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99880', 'IR 125619', 'backcross', '521322', '521993', '162', '745418', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99881', 'IR 125620', 'backcross', '521323', '521994', '162', '745419', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99882', 'IR 125621', 'backcross', '521324', '521995', '162', '745420', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99883', 'IR 125622', 'backcross', '521325', '521996', '162', '745421', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99884', 'IR 125623', 'backcross', '521326', '521997', '162', '745422', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99885', 'IR 125624', 'backcross', '521327', '521998', '162', '745423', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99886', 'IR 125625', 'backcross', '521328', '521999', '162', '745424', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99887', 'IR 125626', 'backcross', '521329', '522000', '162', '745425', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99888', 'IR 125627', 'backcross', '521330', '522001', '162', '745426', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99889', 'IR 125628', 'backcross', '521331', '522002', '162', '745427', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99890', 'IR 125629', 'backcross', '521332', '522003', '162', '745428', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('99891', 'IR 125630', 'backcross', '521333', '522004', '162', '745429', '18', '2015-11-13 13:59:43.921987');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121350', 'IR 127672', 'single cross', '618810', '621922', '200', '994078', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121354', 'IR 127734', 'single cross', '618872', '621984', '200', '994142', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121355', 'IR 127735', 'single cross', '618873', '621985', '200', '994143', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121366', 'IR 127671', 'single cross', '618809', '621921', '200', '994077', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121427', 'IR 127633', 'three-way cross', '618771', '621883', '200', '994153', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121428', 'IR 127634', 'three-way cross', '618772', '621884', '200', '994154', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121429', 'IR 127635', 'three-way cross', '618773', '621885', '200', '994155', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121430', 'IR 127636', 'three-way cross', '618774', '621886', '200', '994156', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121431', 'IR 127637', 'three-way cross', '618775', '621887', '200', '994157', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121432', 'IR 127640', 'three-way cross', '618778', '621890', '200', '994160', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121433', 'IR 127638', 'three-way cross', '618776', '621888', '200', '994158', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121434', 'IR 127639', 'three-way cross', '618777', '621889', '200', '994159', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121435', 'IR 127641', 'three-way cross', '618779', '621891', '200', '994161', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121436', 'IR 127642', 'three-way cross', '618780', '621892', '200', '994162', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121437', 'IR 127643', 'three-way cross', '618781', '621893', '200', '994163', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121438', 'IR 127644', 'three-way cross', '618782', '621894', '200', '994164', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121439', 'IR 127645', 'three-way cross', '618783', '621895', '200', '994165', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121440', 'IR 127646', 'three-way cross', '618784', '621896', '200', '994166', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121441', 'IR 127647', 'three-way cross', '618785', '621897', '200', '994167', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121442', 'IR 127648', 'three-way cross', '618786', '621898', '200', '994168', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121443', 'IR 127649', 'three-way cross', '618787', '621899', '200', '994169', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121444', 'IR 127650', 'three-way cross', '618788', '621900', '200', '994170', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121445', 'IR 127654', 'three-way cross', '618792', '621904', '200', '994174', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121446', 'IR 127651', 'three-way cross', '618789', '621901', '200', '994171', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121447', 'IR 127652', 'three-way cross', '618790', '621902', '200', '994172', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121448', 'IR 127653', 'three-way cross', '618791', '621903', '200', '994173', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121449', 'IR 127655', 'three-way cross', '618793', '621905', '200', '994175', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121450', 'IR 127656', 'three-way cross', '618794', '621906', '200', '994176', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121451', 'IR 127657', 'three-way cross', '618795', '621907', '200', '994177', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121452', 'IR 127658', 'three-way cross', '618796', '621908', '200', '994178', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121453', 'IR 127659', 'three-way cross', '618797', '621909', '200', '994179', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121454', 'IR 127660', 'three-way cross', '618798', '621910', '200', '994180', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121455', 'IR 127661', 'three-way cross', '618799', '621911', '200', '994181', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121456', 'IR 127662', 'three-way cross', '618800', '621912', '200', '994182', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121457', 'IR 127663', 'single cross', '618801', '621913', '200', '994069', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121458', 'IR 127664', 'single cross', '618802', '621914', '200', '994070', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121459', 'IR 127665', 'single cross', '618803', '621915', '200', '994071', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121460', 'IR 127666', 'single cross', '618804', '621916', '200', '994072', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121461', 'IR 127667', 'single cross', '618805', '621917', '200', '994073', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121462', 'IR 127668', 'single cross', '618806', '621918', '200', '994074', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121463', 'IR 127669', 'single cross', '618807', '621919', '200', '994075', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121464', 'IR 127670', 'single cross', '618808', '621920', '200', '994076', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121465', 'IR 127673', 'single cross', '618811', '621923', '200', '994079', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121466', 'IR 127674', 'single cross', '618812', '621924', '200', '994080', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121467', 'IR 127675', 'single cross', '618813', '621925', '200', '994081', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121468', 'IR 127676', 'single cross', '618814', '621926', '200', '994082', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121469', 'IR 127677', 'single cross', '618815', '621927', '200', '994083', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121470', 'IR 127678', 'single cross', '618816', '621928', '200', '994084', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121471', 'IR 127679', 'single cross', '618817', '621929', '200', '994085', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121472', 'IR 127680', 'single cross', '618818', '621930', '200', '994086', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121473', 'IR 127681', 'single cross', '618819', '621931', '200', '994087', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121474', 'IR 127682', 'single cross', '618820', '621932', '200', '994088', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121475', 'IR 127683', 'single cross', '618821', '621933', '200', '994089', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121476', 'IR 127684', 'single cross', '618822', '621934', '200', '994090', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121477', 'IR 127685', 'single cross', '618823', '621935', '200', '994091', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121478', 'IR 127686', 'single cross', '618824', '621936', '200', '994092', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121479', 'IR 127687', 'single cross', '618825', '621937', '200', '994093', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121480', 'IR 127688', 'single cross', '618826', '621938', '200', '994094', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121481', 'IR 127689', 'single cross', '618827', '621939', '200', '994095', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121482', 'IR 127690', 'single cross', '618828', '621940', '200', '994096', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121483', 'IR 127691', 'single cross', '618829', '621941', '200', '994097', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121484', 'IR 127692', 'single cross', '618830', '621942', '200', '994098', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121485', 'IR 127693', 'single cross', '618831', '621943', '200', '994099', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121486', 'IR 127694', 'single cross', '618832', '621944', '200', '994100', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121487', 'IR 127695', 'single cross', '618833', '621945', '200', '994101', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121488', 'IR 127696', 'single cross', '618834', '621946', '200', '994102', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121489', 'IR 127697', 'single cross', '618835', '621947', '200', '994103', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121490', 'IR 127698', 'single cross', '618836', '621948', '200', '994104', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121491', 'IR 127699', 'single cross', '618837', '621949', '200', '994105', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121492', 'IR 127700', 'single cross', '618838', '621950', '200', '994106', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121493', 'IR 127701', 'single cross', '618839', '621951', '200', '994107', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121494', 'IR 127702', 'single cross', '618840', '621952', '200', '994108', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121495', 'IR 127703', 'single cross', '618841', '621953', '200', '994109', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121496', 'IR 127704', 'single cross', '618842', '621954', '200', '994110', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121497', 'IR 127705', 'single cross', '618843', '621955', '200', '994111', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121498', 'IR 127706', 'single cross', '618844', '621956', '200', '994112', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121499', 'IR 127707', 'single cross', '618845', '621957', '200', '994113', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121500', 'IR 127708', 'single cross', '618846', '621958', '200', '994114', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121501', 'IR 127709', 'single cross', '618847', '621959', '200', '994115', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121502', 'IR 127710', 'single cross', '618848', '621960', '200', '994116', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121503', 'IR 127711', 'single cross', '618849', '621961', '200', '994117', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121504', 'IR 127712', 'single cross', '618850', '621962', '200', '994118', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121505', 'IR 127713', 'single cross', '618851', '621963', '200', '994119', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121506', 'IR 127714', 'single cross', '618852', '621964', '200', '994120', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121510', 'IR 127718', 'single cross', '618856', '621968', '200', '994124', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121511', 'IR 127719', 'single cross', '618857', '621969', '200', '994125', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121512', 'IR 127720', 'single cross', '618858', '621970', '200', '994126', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121513', 'IR 127721', 'single cross', '618859', '621971', '200', '994127', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121514', 'IR 127722', 'single cross', '618860', '621972', '200', '994128', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121515', 'IR 127723', 'single cross', '618861', '621973', '200', '994129', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121516', 'IR 127724', 'single cross', '618862', '621974', '200', '994130', '18', '2016-05-04 08:34:20.667793');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121612', 'IR 127888', 'backcross', '715085', '718199', '200', '994216', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121613', 'IR 127896', 'backcross', '715093', '718207', '200', '994224', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121617', 'IR 127874', 'three-way cross', '715071', '718185', '200', '994188', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121618', 'IR 127880', 'backcross', '715077', '718191', '200', '994208', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121619', 'IR 127886', 'backcross', '715083', '718197', '200', '994214', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121620', 'IR 127889', 'backcross', '715086', '718200', '200', '994217', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121621', 'IR 127897', 'backcross', '715094', '718208', '200', '994225', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121626', 'IR 127869', 'three-way cross', '715066', '718180', '200', '994183', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121627', 'IR 127870', 'three-way cross', '715067', '718181', '200', '994184', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121628', 'IR 127871', 'three-way cross', '715068', '718182', '200', '994185', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121629', 'IR 127872', 'three-way cross', '715069', '718183', '200', '994186', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121630', 'IR 127875', 'three-way cross', '715072', '718186', '200', '994189', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121631', 'IR 127876', 'three-way cross', '715073', '718187', '200', '994190', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121632', 'IR 127877', 'backcross', '715074', '718188', '200', '994205', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121633', 'IR 127878', 'backcross', '715075', '718189', '200', '994206', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121634', 'IR 127881', 'backcross', '715078', '718192', '200', '994209', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121635', 'IR 127882', 'backcross', '715079', '718193', '200', '994210', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121636', 'IR 127883', 'backcross', '715080', '718194', '200', '994211', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121637', 'IR 127884', 'backcross', '715081', '718195', '200', '994212', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121638', 'IR 127887', 'backcross', '715084', '718198', '200', '994215', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121639', 'IR 127890', 'backcross', '715087', '718201', '200', '994218', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121640', 'IR 127891', 'backcross', '715088', '718202', '200', '994219', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121641', 'IR 127892', 'backcross', '715089', '718203', '200', '994220', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121642', 'IR 127893', 'backcross', '715090', '718204', '200', '994221', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121643', 'IR 127894', 'backcross', '715091', '718205', '200', '994222', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121644', 'IR 127895', 'backcross', '715092', '718206', '200', '994223', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121645', 'IR 127898', 'backcross', '715095', '718209', '200', '994226', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121646', 'IR 127899', 'backcross', '715096', '718210', '200', '994227', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121647', 'IR 127900', 'backcross', '715097', '718211', '200', '994228', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121648', 'IR 127901', 'backcross', '715098', '718212', '200', '994229', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121649', 'IR 127902', 'backcross', '715099', '718213', '200', '994230', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121650', 'IR 127903', 'backcross', '715100', '718214', '200', '994231', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121651', 'IR 127904', 'backcross', '715101', '718215', '200', '994232', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121652', 'IR 127905', 'backcross', '715102', '718216', '200', '994233', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121653', 'IR 127906', 'backcross', '715103', '718217', '200', '994234', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121654', 'IR 127907', 'backcross', '715104', '718218', '200', '994235', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121655', 'IR 127908', 'backcross', '715105', '718219', '200', '994236', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121656', 'IR 127909', 'backcross', '715106', '718220', '200', '994237', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121657', 'IR 127910', 'backcross', '715107', '718221', '200', '994238', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121658', 'IR 127911', 'backcross', '715108', '718222', '200', '994239', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121659', 'IR 127912', 'backcross', '715109', '718223', '200', '994240', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121660', 'IR 127913', 'backcross', '715110', '718224', '200', '994241', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121661', 'IR 127914', 'backcross', '715111', '718225', '200', '994242', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121662', 'IR 127915', 'backcross', '715112', '718226', '200', '994243', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121663', 'IR 127916', 'backcross', '715113', '718227', '200', '994244', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121664', 'IR 127917', 'backcross', '715114', '718228', '200', '994245', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121665', 'IR 127918', 'backcross', '715115', '718229', '200', '994246', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121666', 'IR 127919', 'backcross', '715116', '718230', '200', '994247', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121697', 'IR 127873', 'three-way cross', '715070', '718184', '200', '994187', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121698', 'IR 127879', 'backcross', '715076', '718190', '200', '994207', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('121699', 'IR 127885', 'backcross', '715082', '718196', '200', '994213', '18', '2016-05-14 11:41:54.83059');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122438', 'IR 129058', 'three-way cross', '729024', '732749', '200', '994200', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122441', 'IR 129053', 'three-way cross', '729019', '732744', '200', '994195', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122443', 'IR 129050', 'three-way cross', '729016', '732741', '200', '994192', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122444', 'IR 129065', 'backcross', '729031', '732756', '200', '994248', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122446', 'IR 129059', 'three-way cross', '729025', '732750', '200', '994201', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122449', 'IR 129051', 'three-way cross', '729017', '732742', '200', '994193', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122450', 'IR 129054', 'three-way cross', '729020', '732745', '200', '994196', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122451', 'IR 129055', 'three-way cross', '729021', '732746', '200', '994197', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122452', 'IR 129056', 'three-way cross', '729022', '732747', '200', '994198', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122453', 'IR 129057', 'three-way cross', '729023', '732748', '200', '994199', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122454', 'IR 129060', 'three-way cross', '729026', '732751', '200', '994202', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122455', 'IR 129061', 'three-way cross', '729027', '732752', '200', '994203', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122456', 'IR 129062', 'three-way cross', '729028', '732753', '200', '994204', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122468', 'IR 129049', 'three-way cross', '729015', '732740', '200', '994191', '18', '2016-06-06 15:00:01.651177');

INSERT INTO germplasm.cross (id, cross_name, cross_method, germplasm_id, seed_id, experiment_id, entry_id, creator_id, creation_timestamp)
    VALUES ('122469', 'IR 129052', 'three-way cross', '729018', '732743', '200', '994194', '18', '2016-06-06 15:00:01.651177');


-- update sequence
SELECT SETVAL('germplasm.cross_id_seq', COALESCE(MAX(id), 1) ) FROM germplasm.cross;



--rollback DELETE FROM germplasm.cross;

--rollback SELECT SETVAL('germplasm.cross_id_seq', COALESCE(MAX(id), 1) ) FROM germplasm.cross;
