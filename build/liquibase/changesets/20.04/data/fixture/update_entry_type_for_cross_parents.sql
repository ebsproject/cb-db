--liquibase formatted sql

--changeset postgres:update_entry_type_for_cross_parents context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5463 Update entry type for cross parents



-- update entry_type to parent
UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 558068;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734462;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734454;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763861;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745453;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116646;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734413;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547734;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547742;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192417;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745432;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763857;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151073;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151104;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344419;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621392;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734456;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317074;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763849;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734450;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745402;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734482;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763864;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745392;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734414;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745381;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317060;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344434;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547690;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317068;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621391;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621397;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734464;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116647;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344366;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317065;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344427;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116655;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116665;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734447;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745401;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745388;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344414;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734477;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344428;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745383;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745451;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734481;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621398;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547748;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344350;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 322017;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192418;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116644;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763832;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745408;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621401;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151098;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734411;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621389;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344364;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 322014;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547726;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621402;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547731;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763831;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734426;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547750;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151085;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763837;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734437;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151074;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763836;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745443;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763855;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734484;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547752;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745441;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317076;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745397;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745433;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734472;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317063;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745391;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547755;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734412;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151060;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763859;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317070;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745390;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317069;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577776;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734424;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763841;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763827;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547754;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577773;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745415;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734470;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116631;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317054;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577774;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734441;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763860;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317075;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 27862;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734449;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763834;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734423;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547753;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734431;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151068;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734453;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547743;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763865;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734430;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745404;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344431;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317056;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745444;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151088;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763848;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317061;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344433;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734422;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151082;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734458;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547749;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547733;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745398;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621399;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344355;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763835;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763863;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151094;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116632;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734410;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547685;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734445;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745389;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547746;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745450;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745394;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763868;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317059;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192415;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763866;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745407;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621394;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151069;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734433;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 558069;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 181208;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734421;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745387;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745440;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734468;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745382;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745385;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344357;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734425;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734473;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116664;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745454;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621390;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547756;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547684;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734485;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621400;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745445;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116620;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734448;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734460;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763845;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745410;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745409;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547738;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745459;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763854;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734429;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763850;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734474;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 322018;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745405;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734436;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745438;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763828;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745434;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734480;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734451;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734416;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734457;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745442;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547751;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151078;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317078;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745393;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745399;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317071;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734439;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621393;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547740;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734427;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745395;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547744;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116663;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745439;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763867;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734420;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763840;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745452;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734459;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344402;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317072;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344365;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734471;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547747;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763853;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621395;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763847;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734478;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577775;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734452;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317077;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763844;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763862;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763846;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577779;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344358;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344367;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547745;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151066;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547691;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763839;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547741;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734463;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734419;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763851;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317062;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734469;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151097;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745435;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317048;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577780;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734461;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763858;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745406;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745384;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734438;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577778;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 27861;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734444;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151076;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621396;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734432;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547730;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763833;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317050;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151083;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745403;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192416;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734443;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734446;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621403;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344430;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547722;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116624;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763869;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745458;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192419;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116650;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317073;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745400;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547732;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151057;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151059;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763842;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151089;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763838;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547739;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317053;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317052;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745456;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734479;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734440;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151081;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734442;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734415;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745396;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116683;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763852;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344363;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151079;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763829;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745386;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763830;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547683;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 322011;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745436;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547682;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317051;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317049;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344369;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151077;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344408;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734428;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344417;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734435;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344362;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344368;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151080;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734434;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344422;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151072;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 192420;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 322010;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 621404;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763856;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 745457;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 763843;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116660;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734418;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 547737;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 317055;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344381;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 151095;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116666;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 734417;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 116628;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 577777;

UPDATE experiment.entry
    SET entry_type = 'parent'
    WHERE entry.id = 344349;



--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 558068;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734462;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734454;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763861;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745453;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116646;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734413;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547734;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547742;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192417;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745432;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763857;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151073;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151104;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344419;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621392;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734456;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317074;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763849;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734450;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745402;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734482;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763864;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745392;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734414;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745381;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317060;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344434;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547690;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317068;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621391;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621397;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734464;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116647;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344366;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317065;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344427;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116655;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116665;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734447;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745401;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745388;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344414;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734477;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344428;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745383;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745451;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734481;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621398;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547748;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344350;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 322017;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192418;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116644;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763832;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745408;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621401;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151098;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734411;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621389;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344364;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 322014;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547726;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621402;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547731;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763831;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734426;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547750;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151085;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763837;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734437;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151074;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763836;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745443;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763855;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734484;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547752;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745441;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317076;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745397;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745433;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734472;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317063;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745391;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547755;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734412;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151060;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763859;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317070;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745390;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317069;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577776;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734424;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763841;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763827;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547754;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577773;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745415;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734470;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116631;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317054;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577774;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734441;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763860;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317075;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 27862;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734449;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763834;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734423;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547753;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734431;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151068;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734453;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547743;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763865;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734430;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745404;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344431;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317056;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745444;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151088;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763848;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317061;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344433;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734422;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151082;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734458;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547749;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547733;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745398;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621399;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344355;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763835;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763863;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151094;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116632;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734410;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547685;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734445;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745389;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547746;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745450;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745394;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763868;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317059;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192415;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763866;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745407;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621394;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151069;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734433;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 558069;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 181208;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734421;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745387;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745440;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734468;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745382;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745385;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344357;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734425;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734473;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116664;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745454;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621390;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547756;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547684;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734485;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621400;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745445;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116620;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734448;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734460;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763845;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745410;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745409;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547738;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745459;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763854;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734429;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763850;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734474;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 322018;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745405;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734436;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745438;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763828;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745434;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734480;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734451;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734416;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734457;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745442;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547751;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151078;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317078;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745393;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745399;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317071;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734439;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621393;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547740;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734427;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745395;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547744;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116663;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745439;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763867;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734420;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763840;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745452;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734459;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344402;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317072;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344365;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734471;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547747;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763853;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621395;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763847;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734478;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577775;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734452;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317077;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763844;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763862;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763846;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577779;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344358;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344367;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547745;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151066;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547691;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763839;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547741;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734463;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734419;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763851;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317062;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734469;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151097;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745435;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317048;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577780;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734461;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763858;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745406;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745384;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734438;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577778;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 27861;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734444;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151076;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621396;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734432;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547730;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763833;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317050;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151083;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745403;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192416;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734443;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734446;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621403;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344430;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547722;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116624;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763869;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745458;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192419;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116650;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317073;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745400;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547732;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151057;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151059;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763842;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151089;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763838;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547739;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317053;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317052;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745456;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734479;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734440;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151081;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734442;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734415;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745396;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116683;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763852;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344363;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151079;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763829;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745386;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763830;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547683;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 322011;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745436;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547682;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317051;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317049;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344369;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151077;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344408;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734428;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344417;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734435;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344362;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344368;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151080;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734434;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344422;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151072;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 192420;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 322010;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 621404;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763856;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 745457;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 763843;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116660;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734418;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 547737;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 317055;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344381;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 151095;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116666;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 734417;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 116628;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 577777;

--rollback UPDATE experiment.entry
--rollback SET entry_type = 'entry'
--rollback WHERE entry.id = 344349;
