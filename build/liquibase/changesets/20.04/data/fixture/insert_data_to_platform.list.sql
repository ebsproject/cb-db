--liquibase formatted sql

--changeset postgres:insert_data_to_platform.list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5727 Insert data to platform.list



-- insert data to platform.list
INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('526', 'LIST0001', 'Designation List #0001', 'Designation List #0001', 'designation', '13', NULL, '2015-05-07 13:51:58.46043', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('528', 'LIST0002', 'Designation List #0002', 'Designation List #0002', 'designation', '13', NULL, '2015-06-15 14:57:58.065474', '53');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('529', 'LIST0003', 'Designation List #0003', 'Designation List #0003', 'designation', '13', NULL, '2015-06-15 15:12:19.974169', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('532', 'LIST0004', 'Designation List #0004', 'Designation List #0004', 'designation', '13', NULL, '2015-06-18 14:42:47.152909', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('536', 'LIST0005', 'Designation List #0005', 'Designation List #0005', 'designation', '13', NULL, '2015-06-26 16:33:11.24512', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('537', 'LIST0006', 'Designation List #0006', 'Designation List #0006', 'designation', '13', NULL, '2015-06-26 16:51:21.216127', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('541', 'LIST0007', 'Designation List #0007', 'Designation List #0007', 'designation', '13', NULL, '2015-06-29 13:51:54.780833', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('542', 'LIST0008', 'Designation List #0008', 'Designation List #0008', 'designation', '13', NULL, '2015-07-01 08:42:04.845125', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('544', 'LIST0009', 'Designation List #0009', 'Designation List #0009', 'designation', '13', NULL, '2015-07-02 10:37:42.102408', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('545', 'LIST0010', 'Designation List #0010', 'Designation List #0010', 'designation', '13', NULL, '2015-07-03 15:06:23.464265', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('546', 'LIST0011', 'Designation List #0011', 'Designation List #0011', 'designation', '13', NULL, '2015-07-03 16:23:29.068785', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('547', 'LIST0012', 'Designation List #0012', 'Designation List #0012', 'designation', '13', NULL, '2015-08-17 10:52:31.791453', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('548', 'LIST0013', 'Designation List #0013', 'Designation List #0013', 'designation', '13', NULL, '2015-07-03 17:25:39.156838', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('549', 'LIST0014', 'Designation List #0014', 'Designation List #0014', 'designation', '13', NULL, '2015-07-04 13:55:54.162744', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('550', 'LIST0015', 'Designation List #0015', 'Designation List #0015', 'designation', '13', NULL, '2015-07-04 16:54:53.719981', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('551', 'LIST0016', 'Designation List #0016', 'Designation List #0016', 'designation', '13', NULL, '2015-07-06 16:35:30.218995', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('552', 'LIST0017', 'Designation List #0017', 'Designation List #0017', 'designation', '13', NULL, '2015-07-08 16:29:49.227199', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('553', 'LIST0018', 'Designation List #0018', 'Designation List #0018', 'designation', '13', NULL, '2015-07-13 17:46:38.302326', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('559', 'LIST0019', 'Designation List #0019', 'Designation List #0019', 'designation', '13', NULL, '2015-07-21 11:08:08.67208', '30');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('561', 'LIST0020', 'Designation List #0020', 'Designation List #0020', 'designation', '13', NULL, '2015-07-28 18:13:04.416224', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('562', 'LIST0021', 'Designation List #0021', 'Designation List #0021', 'designation', '13', NULL, '2015-07-30 14:44:22.141978', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('563', 'LIST0022', 'Designation List #0022', 'Designation List #0022', 'designation', '13', NULL, '2015-07-30 14:45:35.439797', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('564', 'LIST0023', 'Designation List #0023', 'Designation List #0023', 'designation', '13', NULL, '2015-07-31 13:17:50.595436', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('565', 'LIST0024', 'Designation List #0024', 'Designation List #0024', 'designation', '13', NULL, '2015-08-03 08:21:08.969681', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('567', 'LIST0025', 'Designation List #0025', 'Designation List #0025', 'designation', '13', NULL, '2015-08-13 10:32:06.487841', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('568', 'LIST0026', 'Designation List #0026', 'Designation List #0026', 'designation', '13', NULL, '2015-09-04 07:22:28.588875', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('569', 'LIST0027', 'Designation List #0027', 'Designation List #0027', 'designation', '13', NULL, '2015-09-04 07:23:51.311999', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('570', 'LIST0028', 'Designation List #0028', 'Designation List #0028', 'designation', '13', NULL, '2015-09-21 09:06:41.249698', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('571', 'LIST0029', 'Designation List #0029', 'Designation List #0029', 'designation', '13', NULL, '2015-09-22 16:54:11.363986', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('572', 'LIST0030', 'Designation List #0030', 'Designation List #0030', 'designation', '13', NULL, '2015-09-25 11:24:40.460386', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('573', 'LIST0031', 'Designation List #0031', 'Designation List #0031', 'designation', '13', NULL, '2015-10-05 10:25:15.753925', '7');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('574', 'LIST0032', 'Designation List #0032', 'Designation List #0032', 'designation', '13', NULL, '2015-10-19 11:35:37.482502', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('575', 'LIST0033', 'Designation List #0033', 'Designation List #0033', 'designation', '13', NULL, '2015-10-19 11:49:11.708094', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('578', 'LIST0034', 'Designation List #0034', 'Designation List #0034', 'designation', '13', NULL, '2015-12-15 09:39:04.753368', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('579', 'LIST0035', 'Designation List #0035', 'Designation List #0035', 'designation', '13', NULL, '2016-01-15 17:19:39.951632', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('580', 'LIST0036', 'Designation List #0036', 'Designation List #0036', 'designation', '13', NULL, '2016-01-18 16:07:48.70529', '19');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('591', 'LIST0037', 'Designation List #0037', 'Designation List #0037', 'designation', '13', NULL, '2016-06-21 08:19:36.24527', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('592', 'LIST0038', 'Designation List #0038', 'Designation List #0038', 'designation', '13', NULL, '2016-06-21 08:19:55.348613', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('595', 'LIST0039', 'Designation List #0039', 'Designation List #0039', 'designation', '13', NULL, '2016-06-27 13:47:57.08265', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('596', 'LIST0040', 'Designation List #0040', 'Designation List #0040', 'designation', '13', NULL, '2016-06-28 14:50:43.342768', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('597', 'LIST0041', 'Designation List #0041', 'Designation List #0041', 'designation', '13', NULL, '2016-06-30 09:09:49.891306', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('598', 'LIST0042', 'Designation List #0042', 'Designation List #0042', 'designation', '13', NULL, '2016-07-08 13:52:28.964783', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('599', 'LIST0043', 'Designation List #0043', 'Designation List #0043', 'designation', '13', NULL, '2016-07-08 13:59:57.884475', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('601', 'LIST0044', 'Designation List #0044', 'Designation List #0044', 'designation', '13', NULL, '2016-07-26 13:07:22.367676', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('602', 'LIST0045', 'Designation List #0045', 'Designation List #0045', 'designation', '13', NULL, '2016-08-03 13:04:39.899292', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('603', 'LIST0046', 'Designation List #0046', 'Designation List #0046', 'designation', '13', NULL, '2016-08-04 11:40:04.930794', '82');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('607', 'LIST0047', 'Designation List #0047', 'Designation List #0047', 'designation', '13', NULL, '2016-10-19 16:25:11.085296', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('608', 'LIST0048', 'Designation List #0048', 'Designation List #0048', 'designation', '13', NULL, '2016-10-26 11:48:13.405694', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('609', 'LIST0049', 'Designation List #0049', 'Designation List #0049', 'designation', '13', NULL, '2017-03-22 14:03:52.092606', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('611', 'LIST0050', 'Designation List #0050', 'Designation List #0050', 'designation', '13', NULL, '2017-06-23 09:23:39.182244', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('612', 'LIST0051', 'Designation List #0051', 'Designation List #0051', 'designation', '13', NULL, '2017-06-23 09:24:15.897778', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('614', 'LIST0052', 'Designation List #0052', 'Designation List #0052', 'designation', '13', NULL, '2017-07-27 20:43:18.037377', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('617', 'LIST0053', 'Designation List #0053', 'Designation List #0053', 'designation', '13', NULL, '2017-07-28 20:52:33.050419', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('619', 'LIST0054', 'Designation List #0054', 'Designation List #0054', 'designation', '13', NULL, '2017-07-28 20:49:47.208407', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('620', 'LIST0055', 'Designation List #0055', 'Designation List #0055', 'designation', '13', NULL, '2017-07-31 10:27:05.812418', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('630', 'LIST0056', 'Designation List #0056', 'Designation List #0056', 'designation', '13', NULL, '2017-09-22 08:46:16.747222', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('635', 'LIST0057', 'Designation List #0057', 'Designation List #0057', 'designation', '13', NULL, '2017-10-11 13:32:01.193914', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('640', 'LIST0058', 'Designation List #0058', 'Designation List #0058', 'designation', '13', NULL, '2017-11-15 20:46:35.454433', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('641', 'LIST0059', 'Designation List #0059', 'Designation List #0059', 'designation', '13', NULL, '2017-11-16 16:39:55.041578', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('643', 'LIST0060', 'Designation List #0060', 'Designation List #0060', 'designation', '13', NULL, '2017-11-23 14:57:19.859015', '81');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('644', 'LIST0061', 'Designation List #0061', 'Designation List #0061', 'designation', '13', NULL, '2017-11-24 14:19:55.430934', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('647', 'LIST0062', 'Designation List #0062', 'Designation List #0062', 'designation', '13', NULL, '2018-02-07 14:35:43.178797', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('648', 'LIST0063', 'Designation List #0063', 'Designation List #0063', 'designation', '13', NULL, '2018-02-07 14:40:09.25922', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('655', 'LIST0064', 'Designation List #0064', 'Designation List #0064', 'designation', '13', NULL, '2018-03-26 00:52:17.724047', '7');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('659', 'LIST0065', 'Designation List #0065', 'Designation List #0065', 'designation', '13', NULL, '2018-07-11 03:03:00.437803', '264');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('660', 'LIST0066', 'Designation List #0066', 'Designation List #0066', 'designation', '13', NULL, '2018-07-13 08:36:37.8906', '264');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('662', 'LIST0067', 'Designation List #0067', 'Designation List #0067', 'designation', '13', NULL, '2018-09-07 14:30:52.095621', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('665', 'LIST0068', 'Designation List #0068', 'Designation List #0068', 'designation', '13', NULL, '2018-10-29 15:34:15.970993', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('670', 'LIST0069', 'Designation List #0069', 'Designation List #0069', 'designation', '13', NULL, '2018-12-19 10:06:20.908253', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('671', 'LIST0070', 'Designation List #0070', 'Designation List #0070', 'designation', '13', NULL, '2018-12-19 10:17:43.84031', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('684', 'LIST0071', 'Designation List #0071', 'Designation List #0071', 'designation', '13', NULL, '2019-01-28 09:16:58.836159', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('691', 'LIST0072', 'Designation List #0072', 'Designation List #0072', 'designation', '13', NULL, '2019-02-14 13:55:50.626824', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('706', 'LIST0073', 'Designation List #0073', 'Designation List #0073', 'designation', '13', NULL, '2019-03-13 13:35:22.623511', '53');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('747', 'LIST0074', 'Designation List #0074', 'Designation List #0074', 'designation', '13', NULL, '2019-06-06 15:38:14.746664', '19');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('749', 'LIST0075', 'Designation List #0075', 'Designation List #0075', 'designation', '13', NULL, '2019-06-06 15:40:07.870659', '19');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('753', 'LIST0076', 'Designation List #0076', 'Designation List #0076', 'designation', '13', NULL, '2019-07-08 12:58:22.920738', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('755', 'LIST0077', 'Designation List #0077', 'Designation List #0077', 'designation', '13', NULL, '2019-07-09 09:21:14.485263', '18');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('765', 'LIST0078', 'Designation List #0078', 'Designation List #0078', 'designation', '13', NULL, '2019-07-09 12:51:46.927182', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('822', 'LIST0079', 'Designation List #0079', 'Designation List #0079', 'designation', '13', NULL, '2019-10-06 19:46:39.89964', '389');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('919', 'LIST0080', 'Designation List #0080', 'Designation List #0080', 'designation', '13', NULL, '2019-12-16 18:17:27.562733', '125');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('929', 'LIST0081', 'Designation List #0081', 'Designation List #0081', 'designation', '13', NULL, '2020-01-09 10:54:14.270244', '417');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('938', 'LIST0082', 'Designation List #0082', 'Designation List #0082', 'designation', '13', NULL, '2020-01-16 10:34:46.420327', '125');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('960', 'LIST0083', 'Designation List #0083', 'Designation List #0083', 'designation', '13', NULL, '2020-02-19 08:06:54.491081', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('962', 'LIST0084', 'Designation List #0084', 'Designation List #0084', 'designation', '13', NULL, '2020-02-19 08:30:22.129548', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('964', 'LIST0085', 'Designation List #0085', 'Designation List #0085', 'designation', '13', NULL, '2020-02-19 10:58:25.664938', '417');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('965', 'LIST0086', 'Designation List #0086', 'Designation List #0086', 'designation', '13', NULL, '2020-02-19 11:12:03.548458', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('971', 'LIST0087', 'Designation List #0087', 'Designation List #0087', 'designation', '13', NULL, '2020-02-20 13:18:43.465076', '1');

INSERT INTO platform.list (id, abbrev, "name", display_name, "type", entity_id, description, creation_timestamp, creator_id)
    VALUES ('1011', 'LIST0088', 'Designation List #0088', 'Designation List #0088', 'designation', '13', NULL, '2020-03-12 21:04:53.821851', '413');


-- update sequence after the inserts
SELECT SETVAL('platform.list_id_seq', COALESCE(MAX(id), 1)) FROM platform.list;



--rollback DELETE FROM platform.list;

--rollback SELECT SETVAL('platform.list_id_seq', COALESCE(MAX(id), 1)) FROM platform.list;
