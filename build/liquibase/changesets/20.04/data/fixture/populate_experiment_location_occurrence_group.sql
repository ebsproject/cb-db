--liquibase formatted sql

--changeset postgres:populate_experiment_location_occurrence_group context:fixture splitStatements:false
INSERT INTO experiment.location_occurrence_group
WITH t1 AS (
	SELECT 
		ROW_NUMBER () OVER () id,
		el.id location_id,
		eo.id occurrence_id,
		1 order_number,
		1 creator_id
	FROM 
		experiment.occurrence eo
	LEFT JOIN
		experiment."location" el 
	ON 
		eo.id = el.id 
	ORDER BY 
		el.id
)
SELECT t.* FROM t1 t;

--rollback TRUNCATE experiment.location_occurrence_group CASCADE;