--liquibase formatted sql

--changeset postgres:update_unique_experiment_name_in_experiment.experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5775 Update unique experiment_name



-- update experiment_name
UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2013-DS-1'
WHERE id = '1';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2013-DS-2'
WHERE id = '2';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2013-WS-3'
WHERE id = '5';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2013-WS-1'
WHERE id = '3';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2013-WS-2'
WHERE id = '4';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2014-DS-2'
WHERE id = '7';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2014-DS-1'
WHERE id = '6';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2014-DS-1'
WHERE id = '8';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-DS-1'
WHERE id = '9';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-DS-2'
WHERE id = '10';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-DS-1'
WHERE id = '11';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2014-DS-1'
WHERE id = '12';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-DS-3'
WHERE id = '13';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-DS-4'
WHERE id = '15';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-DS-1'
WHERE id = '14';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-DS-2'
WHERE id = '16';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-5'
WHERE id = '19';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-7'
WHERE id = '23';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-4'
WHERE id = '20';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-1'
WHERE id = '17';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-3'
WHERE id = '21';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-2'
WHERE id = '22';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-DS-6'
WHERE id = '18';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2014-WS-2'
WHERE id = '26';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2014-WS-1'
WHERE id = '24';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2014-WS-3'
WHERE id = '25';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2014-WS-1'
WHERE id = '28';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2014-WS-2'
WHERE id = '27';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2014-WS-1'
WHERE id = '29';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-WS-1'
WHERE id = '31';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-WS-3'
WHERE id = '34';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-WS-2'
WHERE id = '33';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-WS-4'
WHERE id = '32';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2014-WS-5'
WHERE id = '30';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-3'
WHERE id = '35';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-6'
WHERE id = '38';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-5'
WHERE id = '37';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-4'
WHERE id = '36';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-2'
WHERE id = '39';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2014-WS-1'
WHERE id = '40';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2014-WS-2'
WHERE id = '41';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2014-WS-1'
WHERE id = '42';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-WS-3'
WHERE id = '45';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-WS-1'
WHERE id = '43';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2014-WS-2'
WHERE id = '44';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-WS-2'
WHERE id = '46';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-WS-1'
WHERE id = '47';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-WS-3'
WHERE id = '49';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2014-WS-4'
WHERE id = '48';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-5'
WHERE id = '51';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-6'
WHERE id = '52';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-7'
WHERE id = '53';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-8'
WHERE id = '55';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-9'
WHERE id = '57';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-1'
WHERE id = '50';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-2'
WHERE id = '54';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-3'
WHERE id = '56';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-DS-4'
WHERE id = '58';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-DS-4'
WHERE id = '61';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-DS-5'
WHERE id = '60';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-DS-2'
WHERE id = '63';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-DS-3'
WHERE id = '62';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-DS-1'
WHERE id = '59';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2015-DS-2'
WHERE id = '64';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2015-DS-1'
WHERE id = '65';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-DS-1'
WHERE id = '67';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-DS-2'
WHERE id = '66';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-DS-3'
WHERE id = '68';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2015-DS-2'
WHERE id = '70';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2015-DS-1'
WHERE id = '69';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2015-DS-1'
WHERE id = '71';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F3-2015-DS-1'
WHERE id = '73';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F3-2015-DS-2'
WHERE id = '72';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F4-2015-DS-1'
WHERE id = '74';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F5-2015-DS-2'
WHERE id = '76';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F5-2015-DS-1'
WHERE id = '75';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F6-2015-DS-1'
WHERE id = '77';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-7'
WHERE id = '82';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-11'
WHERE id = '78';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-10'
WHERE id = '87';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-9'
WHERE id = '81';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-8'
WHERE id = '88';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-6'
WHERE id = '83';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-5'
WHERE id = '85';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-4'
WHERE id = '80';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-3'
WHERE id = '79';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-2'
WHERE id = '86';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-DS-1'
WHERE id = '84';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-DS-3'
WHERE id = '93';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-DS-2'
WHERE id = '89';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-DS-5'
WHERE id = '92';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-DS-4'
WHERE id = '90';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-DS-1'
WHERE id = '91';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-3'
WHERE id = '101';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-5'
WHERE id = '97';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-4'
WHERE id = '100';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-6'
WHERE id = '99';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-7'
WHERE id = '95';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-2'
WHERE id = '98';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-1'
WHERE id = '96';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AGR-2015-DS-8'
WHERE id = '94';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-SEM-2015-DS-3'
WHERE id = '103';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-SEM-2015-DS-1'
WHERE id = '104';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-SEM-2015-DS-2'
WHERE id = '102';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-1'
WHERE id = '106';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-6'
WHERE id = '105';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-4'
WHERE id = '109';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-3'
WHERE id = '110';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-5'
WHERE id = '108';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2015-WS-2'
WHERE id = '107';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-6'
WHERE id = '111';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-11'
WHERE id = '113';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-10'
WHERE id = '118';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-9'
WHERE id = '119';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-8'
WHERE id = '114';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-7'
WHERE id = '115';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-5'
WHERE id = '116';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-4'
WHERE id = '117';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-3'
WHERE id = '121';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-2'
WHERE id = '120';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2015-WS-1'
WHERE id = '112';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2015-WS-1'
WHERE id = '122';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-WS-5'
WHERE id = '125';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-WS-2'
WHERE id = '126';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-WS-3'
WHERE id = '123';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-WS-1'
WHERE id = '127';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2015-WS-4'
WHERE id = '124';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2015-WS-1'
WHERE id = '129';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2015-WS-2'
WHERE id = '128';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F5-2015-WS-1'
WHERE id = '130';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-9'
WHERE id = '134';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-13'
WHERE id = '139';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-12'
WHERE id = '137';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-7'
WHERE id = '132';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-5'
WHERE id = '138';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-4'
WHERE id = '141';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-3'
WHERE id = '142';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-8'
WHERE id = '133';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-2'
WHERE id = '143';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-1'
WHERE id = '140';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-11'
WHERE id = '136';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-10'
WHERE id = '135';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2015-WS-6'
WHERE id = '131';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-WS-2'
WHERE id = '145';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-WS-3'
WHERE id = '144';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PN-2015-WS-1'
WHERE id = '146';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2015-WS-2'
WHERE id = '147';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2015-WS-1'
WHERE id = '148';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-DS-3'
WHERE id = '151';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-DS-1'
WHERE id = '149';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-DS-2'
WHERE id = '150';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-DS-4'
WHERE id = '152';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-3'
WHERE id = '157';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-1'
WHERE id = '154';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-2'
WHERE id = '153';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-6'
WHERE id = '158';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-5'
WHERE id = '155';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-DS-4'
WHERE id = '156';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2016-DS-1'
WHERE id = '160';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2016-DS-2'
WHERE id = '159';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2016-DS-1'
WHERE id = '162';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2016-DS-2'
WHERE id = '161';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RGA-2016-DS-1'
WHERE id = '163';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-DS-4'
WHERE id = '166';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-DS-1'
WHERE id = '168';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-DS-2'
WHERE id = '165';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-DS-5'
WHERE id = '167';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-DS-3'
WHERE id = '164';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F4-2016-DS-1'
WHERE id = '169';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F4-2016-DS-2'
WHERE id = '170';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F6-2016-DS-1'
WHERE id = '172';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F6-2016-DS-2'
WHERE id = '171';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-DS-1'
WHERE id = '175';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-DS-2'
WHERE id = '173';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-DS-3'
WHERE id = '174';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-SEM-2016-DS-1'
WHERE id = '176';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2016-DS-2'
WHERE id = '179';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2016-DS-3'
WHERE id = '178';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2016-DS-4'
WHERE id = '177';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-IYT-2016-DS-1'
WHERE id = '180';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-2'
WHERE id = '181';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-6'
WHERE id = '182';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-3'
WHERE id = '184';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-4'
WHERE id = '185';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-5'
WHERE id = '183';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-OYT-2016-WS-1'
WHERE id = '186';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-RYT-2016-WS-1'
WHERE id = '187';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-9'
WHERE id = '188';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-10'
WHERE id = '194';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-8'
WHERE id = '190';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-7'
WHERE id = '193';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-6'
WHERE id = '196';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-5'
WHERE id = '195';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-4'
WHERE id = '197';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-3'
WHERE id = '191';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-2'
WHERE id = '192';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-AYT-2016-WS-1'
WHERE id = '189';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-HB-2016-WS-1'
WHERE id = '198';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2016-WS-1'
WHERE id = '199';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F1-2016-WS-2'
WHERE id = '200';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-6'
WHERE id = '211';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-7'
WHERE id = '210';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-10'
WHERE id = '207';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-8'
WHERE id = '209';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-11'
WHERE id = '206';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-1'
WHERE id = '202';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-3'
WHERE id = '205';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-4'
WHERE id = '203';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-5'
WHERE id = '204';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-9'
WHERE id = '208';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F2-2016-WS-2'
WHERE id = '201';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F3-2016-WS-1'
WHERE id = '212';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F4-2016-WS-1'
WHERE id = '213';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F5-2016-WS-1'
WHERE id = '215';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F5-2016-WS-2'
WHERE id = '214';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-F6-2016-WS-1'
WHERE id = '216';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-4'
WHERE id = '217';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-6'
WHERE id = '218';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-7'
WHERE id = '222';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-5'
WHERE id = '219';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-1'
WHERE id = '223';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-2'
WHERE id = '221';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-PYT-2016-WS-3'
WHERE id = '220';

UPDATE experiment.experiment
SET experiment_name = 'IRSEA-SEM-2016-WS-1'
WHERE id = '224';



-- revert updates to experiment_name
--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2013-DS'
--rollback WHERE id = '1';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2013-DS'
--rollback WHERE id = '2';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2013-WS'
--rollback WHERE id = '5';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2013-WS'
--rollback WHERE id = '3';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2013-WS'
--rollback WHERE id = '4';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2014-DS'
--rollback WHERE id = '7';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2014-DS'
--rollback WHERE id = '6';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2014-DS'
--rollback WHERE id = '8';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-DS'
--rollback WHERE id = '9';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-DS'
--rollback WHERE id = '10';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-DS'
--rollback WHERE id = '11';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2014-DS'
--rollback WHERE id = '12';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-DS'
--rollback WHERE id = '13';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-DS'
--rollback WHERE id = '15';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-DS'
--rollback WHERE id = '14';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-DS'
--rollback WHERE id = '16';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '19';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '23';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '20';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '17';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '21';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '22';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-DS'
--rollback WHERE id = '18';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2014-WS'
--rollback WHERE id = '26';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2014-WS'
--rollback WHERE id = '24';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2014-WS'
--rollback WHERE id = '25';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2014-WS'
--rollback WHERE id = '28';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2014-WS'
--rollback WHERE id = '27';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2014-WS'
--rollback WHERE id = '29';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-WS'
--rollback WHERE id = '31';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-WS'
--rollback WHERE id = '34';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-WS'
--rollback WHERE id = '33';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-WS'
--rollback WHERE id = '32';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2014-WS'
--rollback WHERE id = '30';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '35';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '38';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '37';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '36';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '39';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2014-WS'
--rollback WHERE id = '40';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2014-WS'
--rollback WHERE id = '41';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2014-WS'
--rollback WHERE id = '42';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-WS'
--rollback WHERE id = '45';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-WS'
--rollback WHERE id = '43';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2014-WS'
--rollback WHERE id = '44';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-WS'
--rollback WHERE id = '46';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-WS'
--rollback WHERE id = '47';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-WS'
--rollback WHERE id = '49';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2014-WS'
--rollback WHERE id = '48';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '51';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '52';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '53';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '55';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '57';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '50';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '54';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '56';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-DS'
--rollback WHERE id = '58';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-DS'
--rollback WHERE id = '61';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-DS'
--rollback WHERE id = '60';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-DS'
--rollback WHERE id = '63';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-DS'
--rollback WHERE id = '62';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-DS'
--rollback WHERE id = '59';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2015-DS'
--rollback WHERE id = '64';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2015-DS'
--rollback WHERE id = '65';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-DS'
--rollback WHERE id = '67';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-DS'
--rollback WHERE id = '66';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-DS'
--rollback WHERE id = '68';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2015-DS'
--rollback WHERE id = '70';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2015-DS'
--rollback WHERE id = '69';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2015-DS'
--rollback WHERE id = '71';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F3-2015-DS'
--rollback WHERE id = '73';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F3-2015-DS'
--rollback WHERE id = '72';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F4-2015-DS'
--rollback WHERE id = '74';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F5-2015-DS'
--rollback WHERE id = '76';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F5-2015-DS'
--rollback WHERE id = '75';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F6-2015-DS'
--rollback WHERE id = '77';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '82';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '78';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '87';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '81';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '88';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '83';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '85';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '80';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '79';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '86';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-DS'
--rollback WHERE id = '84';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-DS'
--rollback WHERE id = '93';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-DS'
--rollback WHERE id = '89';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-DS'
--rollback WHERE id = '92';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-DS'
--rollback WHERE id = '90';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-DS'
--rollback WHERE id = '91';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '101';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '97';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '100';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '99';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '95';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '98';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '96';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AGR-2015-DS'
--rollback WHERE id = '94';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-SEM-2015-DS'
--rollback WHERE id = '103';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-SEM-2015-DS'
--rollback WHERE id = '104';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-SEM-2015-DS'
--rollback WHERE id = '102';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '106';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '105';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '109';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '110';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '108';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2015-WS'
--rollback WHERE id = '107';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '111';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '113';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '118';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '119';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '114';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '115';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '116';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '117';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '121';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '120';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2015-WS'
--rollback WHERE id = '112';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2015-WS'
--rollback WHERE id = '122';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-WS'
--rollback WHERE id = '125';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-WS'
--rollback WHERE id = '126';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-WS'
--rollback WHERE id = '123';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-WS'
--rollback WHERE id = '127';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2015-WS'
--rollback WHERE id = '124';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2015-WS'
--rollback WHERE id = '129';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2015-WS'
--rollback WHERE id = '128';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F5-2015-WS'
--rollback WHERE id = '130';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '134';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '139';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '137';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '132';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '138';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '141';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '142';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '133';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '143';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '140';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '136';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '135';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2015-WS'
--rollback WHERE id = '131';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-WS'
--rollback WHERE id = '145';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-WS'
--rollback WHERE id = '144';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PN-2015-WS'
--rollback WHERE id = '146';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2015-WS'
--rollback WHERE id = '147';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2015-WS'
--rollback WHERE id = '148';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-DS'
--rollback WHERE id = '151';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-DS'
--rollback WHERE id = '149';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-DS'
--rollback WHERE id = '150';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-DS'
--rollback WHERE id = '152';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '157';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '154';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '153';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '158';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '155';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-DS'
--rollback WHERE id = '156';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2016-DS'
--rollback WHERE id = '160';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2016-DS'
--rollback WHERE id = '159';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2016-DS'
--rollback WHERE id = '162';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2016-DS'
--rollback WHERE id = '161';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RGA-2016-DS'
--rollback WHERE id = '163';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-DS'
--rollback WHERE id = '166';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-DS'
--rollback WHERE id = '168';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-DS'
--rollback WHERE id = '165';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-DS'
--rollback WHERE id = '167';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-DS'
--rollback WHERE id = '164';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F4-2016-DS'
--rollback WHERE id = '169';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F4-2016-DS'
--rollback WHERE id = '170';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F6-2016-DS'
--rollback WHERE id = '172';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F6-2016-DS'
--rollback WHERE id = '171';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-DS'
--rollback WHERE id = '175';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-DS'
--rollback WHERE id = '173';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-DS'
--rollback WHERE id = '174';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-SEM-2016-DS'
--rollback WHERE id = '176';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2016-DS'
--rollback WHERE id = '179';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2016-DS'
--rollback WHERE id = '178';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2016-DS'
--rollback WHERE id = '177';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-IYT-2016-DS'
--rollback WHERE id = '180';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '181';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '182';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '184';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '185';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '183';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-OYT-2016-WS'
--rollback WHERE id = '186';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-RYT-2016-WS'
--rollback WHERE id = '187';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '188';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '194';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '190';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '193';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '196';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '195';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '197';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '191';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '192';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-AYT-2016-WS'
--rollback WHERE id = '189';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-HB-2016-WS'
--rollback WHERE id = '198';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2016-WS'
--rollback WHERE id = '199';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F1-2016-WS'
--rollback WHERE id = '200';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '211';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '210';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '207';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '209';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '206';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '202';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '205';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '203';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '204';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '208';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F2-2016-WS'
--rollback WHERE id = '201';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F3-2016-WS'
--rollback WHERE id = '212';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F4-2016-WS'
--rollback WHERE id = '213';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F5-2016-WS'
--rollback WHERE id = '215';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F5-2016-WS'
--rollback WHERE id = '214';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-F6-2016-WS'
--rollback WHERE id = '216';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '217';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '218';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '222';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '219';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '223';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '221';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-PYT-2016-WS'
--rollback WHERE id = '220';

--rollback UPDATE experiment.experiment
--rollback SET experiment_name = 'IRSEA-SEM-2016-WS'
--rollback WHERE id = '224';
