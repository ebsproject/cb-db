--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_nursery_cross_list_irrihq_entry_list_act_val context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry list template EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
            "Values": 
            [
                {
                    "fixed": true,
                    "default": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_ROLE"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "fixed": true,
--rollback                     "default": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Parent Role",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "PARENT_TYPE",
--rollback                     "field_description": "Parent Role"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Entry Remarks"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_ENTRY_LIST_ACT_VAL';

