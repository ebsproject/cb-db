--liquibase formatted sql

--changeset postgres:fix_phnesm2_geospatilal_object_name_description context:fixture splitStatements:false
UPDATE 
	place.geospatial_object 
SET geospatial_object_name = 'Nueva Ecija Muñoz',
	description = 'Nueva Ecija Muñoz'
WHERE id = 10002;

--rollback UPDATE place.geospatial_object 
--rollback SET geospatial_object_name = 'Nueva Ecija Mu\xc3\xb1oz', description = 'Nueva Ecija Mu\xc3\xb1oz'
--rollback WHERE id = 10002;
