--liquibase formatted sql

--changeset postgres:populate_location_document_column_with_added_basic_info_in_experiment.location context:fixture splitStatements:false

UPDATE experiment.location SET location_document = NULL;

CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		el.id,
		concat(
			setweight(to_tsvector(unaccent(el.location_name)),'A'), ' ',
			setweight(to_tsvector(el.location_code),'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = el.geospatial_object_id)))), 'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = el.geospatial_object_id))),'B'), ' ',
			setweight(to_tsvector(ee.experiment_year::varchar),'C'), ' ',
			setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  ee.season_id)),'C'), ' ',
			setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = ee.stage_id)), 'C'), ' ',
			setweight(to_tsvector(el.location_type), 'CD'), ' ',
			setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = el.creator_id))),'CD'), ' ',
			setweight(to_tsvector(el.location_status),'CD'), ' ',
			setweight(to_tsvector(ee.stage_id::varchar),'CD'), ' ',
			setweight(to_tsvector(ee.season_id::varchar),'CD'), ' ',
			setweight(to_tsvector(ee.program_id::varchar),'CD')
		) AS doc
	FROM 
		experiment.LOCATION el
	LEFT JOIN 
		experiment.location_occurrence_group elg 
	ON 
		elg.location_id = el.id
	LEFT JOIN
		experiment.occurrence eo 
	ON 
		eo.id = elg.occurrence_id
	LEFT JOIN 
		experiment.experiment ee 
	ON 
		ee.id = eo.experiment_id
)

UPDATE experiment.location el SET location_document = cast(t1.doc AS tsvector) FROM t1 WHERE el.id = t1.id;

--ROLLBACK UPDATE experiment.location SET location_document = NULL;
