--liquibase formatted sql

--changeset postgres:load_plot_data_formulas context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5760 Load plot data formulas



-- insert formulas
INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('1', 'FLW_CONT = FLW_DATE_CONT - SEEDING_DATE_CONT', '78', '124', 'plot', 'master.formula_flw_cont(flw_date_cont, seeding_date_cont)', NULL, 'create or replace function master.formula_flw_cont(
                flw_date_cont date,
                seeding_date_cont date
            ) returns float as
            $body$
            declare
                flw_cont float;
            begin
                flw_cont = flw_date_cont - seeding_date_cont;
                
                return flw_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('2', 'HT_CONT = AVE(HT<OBSN>_CONT)', '212', '345', 'plot', 'master.formula_ht_cont(ht1_cont, ht2_cont, ht3_cont, ht4_cont, ht5_cont, ht6_cont, ht7_cont, ht8_cont, ht9_cont, ht10_cont, ht11_cont, ht12_cont)', NULL, 'create or replace function master.formula_ht_cont(
                ht1_cont float,
                ht2_cont float,
                ht3_cont float,
                ht4_cont float,
                ht5_cont float,
                ht6_cont float,
                ht7_cont float,
                ht8_cont float,
                ht9_cont float,
                ht10_cont float,
                ht11_cont float,
                ht12_cont float
            ) returns float as
            $body$
            declare
                ht_cont float;
              local_ht1_cont float;
              local_ht2_cont float;
              local_ht3_cont float;
              local_ht4_cont float;
              local_ht5_cont float;
              local_ht6_cont float;
              local_ht7_cont float;
              local_ht8_cont float;
              local_ht9_cont float;
              local_ht10_cont float;
              local_ht11_cont float;
              local_ht12_cont float;
            begin
                
ht_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(ht1_cont),
( ht2_cont),
( ht3_cont),
( ht4_cont),
( ht5_cont),
( ht6_cont),
( ht7_cont),
( ht8_cont),
( ht9_cont),
( ht10_cont),
( ht11_cont),
( ht12_cont)
                            ) t(x)
                    );
                
                return round(ht_cont::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '6', '2014-09-15 15:05:10');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('3', 'TIL_AVE_CONT = AVE(TILL<OBSN>_CONT)', '285', '463', 'plot', 'master.formula_til_ave_cont(till1_cont, till2_cont, till3_cont, till4_cont, till5_cont, till6_cont, till7_cont, till8_cont, till9_cont, till10_cont)', NULL, 'create or replace function master.formula_til_ave_cont(
                till1_cont int,
                till2_cont int,
                till3_cont int,
                till4_cont int,
                till5_cont int,
                till6_cont int,
                till7_cont int,
                till8_cont int,
                till9_cont int,
                till10_cont int
            ) returns float as
            $body$
            declare
                til_ave_cont float;
              local_till1_cont int;
              local_till2_cont int;
              local_till3_cont int;
              local_till4_cont int;
              local_till5_cont int;
              local_till6_cont int;
              local_till7_cont int;
              local_till8_cont int;
              local_till9_cont int;
              local_till10_cont int;
            begin
                
til_ave_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(till1_cont),
( till2_cont),
( till3_cont),
( till4_cont),
( till5_cont),
( till6_cont),
( till7_cont),
( till8_cont),
( till9_cont),
( till10_cont)
                            ) t(x)
                    );
                
                return round(til_ave_cont::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '6', '2014-09-15 15:05:10');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('4', 'MISS_HILL_CONT = TOTAL_HILL_CONT - HVHILL_CONT', '181', '261', 'plot', 'master.formula_miss_hill_cont(total_hill_cont, hvhill_cont)', NULL, 'create or replace function master.formula_miss_hill_cont(
                total_hill_cont integer,
                hvhill_cont integer
            ) returns float as
            $body$
            declare
                miss_hill_cont float;
            begin
                miss_hill_cont = total_hill_cont - hvhill_cont;
                
                return miss_hill_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('5', 'YLD_CONT1 = AYLD_CONT * MF_CONT * 10 / ADJHVAREA_CONT1', '413', '194', 'plot', 'master.formula_yld_cont1(ayld_cont, mf_cont, adjhvarea_cont1)', NULL, 'create or replace function master.formula_yld_cont1(
                ayld_cont float,
                mf_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_cont1 float;
            begin
                yld_cont1 = ayld_cont * mf_cont * 10 / adjhvarea_cont1;
                
                return round(yld_cont1::numeric, 4);
            end
            $body$
            language plpgsql;', '4', '6', '2014-09-18 13:05:25');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('7', 'MF_CONT = (100 - MC_CONT) / 86', '183', '263', 'plot', 'master.formula_mf_cont(mc_cont)', NULL, 'create or replace function master.formula_mf_cont(
				mc_cont float
			) returns float as
			$body$
			declare
				mf_cont float;
              local_mc_cont float;
			begin
				
mf_cont = (100 - mc_cont) / 86;
				
				return round(mf_cont::numeric, 7);
			end
			$body$
			language plpgsql;', '7', '6', '2014-09-18 13:38:20');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('11', 'YLD_0_CONT1 = AYLD_CONT * 10 / ADJHVAREA_CONT1', '414', '195', 'plot', 'master.formula_yld_0_cont1(ayld_cont, adjhvarea_cont1)', NULL, 'create or replace function master.formula_yld_0_cont1(
                ayld_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont1 float;
            begin
                yld_0_cont1 = ayld_cont * 10 / adjhvarea_cont1;
                
                return yld_0_cont1;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-10-05 03:37:42.013231');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('14', 'YLD_CONT_TON = YLD_CONT2 / 1000', '754', '824', 'plot', 'master.formula_yld_cont_ton(yld_cont2)', NULL, 'create or replace function master.formula_yld_cont_ton(
                yld_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton float;
            begin
                yld_cont_ton = yld_cont2 / 1000;
                
                return round(yld_cont_ton::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '6', '2014-10-05 03:39:25.387986');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('16', 'DTH_CONT = HVDATE_CONT - SEEDING_DATE_CONT', '403', '125', 'plot', 'master.formula_dth_cont(hvdate_cont, seeding_date_cont)', NULL, 'create or replace function master.formula_dth_cont(
                hvdate_cont date,
                seeding_date_cont date
            ) returns integer as
            $body$
            declare
                dth_cont integer;
            begin
                dth_cont = hvdate_cont - seeding_date_cont;
                
                return dth_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-10-05 03:40:51.347854');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('17', 'TPLYLD_CONT_PS = AYLD_CONT + SUM(GRNWT_PS<OBSN>_CONT) + 	GRNWT_PLANT_CONT', '411', '44', 'plot', 'master.formula_tplyld_cont_ps(ayld_cont, grnwt_ps5_cont, grnwt_ps10_cont, grnwt_ps15_cont, grnwt_ps20_cont, grnwt_ps25_cont, grnwt_ps50_cont, grnwt_plant_cont)', NULL, 'create or replace function master.formula_tplyld_cont_ps(
				ayld_cont float,
                grnwt_ps5_cont float,
                grnwt_ps10_cont float,
                grnwt_ps15_cont float,
                grnwt_ps20_cont float,
                grnwt_ps25_cont float,
                grnwt_ps50_cont float,
                grnwt_plant_cont float
			) returns float as
			$body$
			declare
				tplyld_cont_ps float;
              local_ayld_cont float;
              local_grnwt_ps5_cont float;
              local_grnwt_ps10_cont float;
              local_grnwt_ps15_cont float;
              local_grnwt_ps20_cont float;
              local_grnwt_ps25_cont float;
              local_grnwt_ps50_cont float;
              local_grnwt_plant_cont float;
			begin
				
tplyld_cont_ps = ayld_cont + 
					(
						select
							sum(t.x)::float
						from (
								values
(grnwt_ps5_cont),
( grnwt_ps10_cont),
( grnwt_ps15_cont),
( grnwt_ps20_cont),
( grnwt_ps25_cont),
( grnwt_ps50_cont),
(0)
							) t(x)
					)
								 + 	grnwt_plant_cont;
				
				return tplyld_cont_ps;
			end
			$body$
			language plpgsql;', NULL, '6', '2014-10-05 03:41:41.604642');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('21', 'YLD_0_CONT2 = AYLD_CONT * 10 / HV_AREA_SQM', '1144', '846', 'plot', 'master.formula_yld_0_cont2(ayld_cont, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_0_cont2(
                ayld_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont2 float;
            begin
                yld_0_cont2 = ayld_cont * 10 / hv_area_sqm;
                
                return yld_0_cont2;
            end
            $body$
            language plpgsql;', NULL, '19', '2014-10-16 01:52:05.393105');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('22', 'YLD_CONT2 = AYLD_CONT * MF_CONT * 10/HV_AREA_SQM', '1143', '845', 'plot', 'master.formula_yld_cont2(ayld_cont, mf_cont, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont2(
                ayld_cont float,
                mf_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont2 float;
              local_ayld_cont float;
              local_mf_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont2 = ayld_cont * mf_cont * 10/hv_area_sqm;
                
                return yld_cont2;
            end
            $body$
            language plpgsql;', NULL, '19', '2014-10-16 01:53:32.961788');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('27', 'YLD_CONT_TON2 = ADJYLD3_CONT / 1000', '1213', '908', 'plot', 'master.formula_yld_cont_ton2(adjyld3_cont)', NULL, 'create or replace function master.formula_yld_cont_ton2(
                adjyld3_cont float
            ) returns float as
            $body$
            declare
                yld_cont_ton2 float;
              local_adjyld3_cont float;
            begin
                
yld_cont_ton2 = adjyld3_cont / 1000;
                
                return round(yld_cont_ton2::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2014-12-02 01:11:01.254032');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('29', 'HT1_CONT = PNL1_CONT + CML1_CONT', '213', '340', 'plot', 'master.formula_ht1_cont(pnl1_cont, cml1_cont)', NULL, 'create or replace function master.formula_ht1_cont(
                pnl1_cont float,
                cml1_cont integer
            ) returns integer as
            $body$
            declare
                ht1_cont integer;
            begin
                ht1_cont = pnl1_cont + cml1_cont;
                
                return ht1_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 05:34:22.139097');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('30', 'HT2_CONT = PNL2_CONT + CML2_CONT', '214', '344', 'plot', 'master.formula_ht2_cont(pnl2_cont, cml2_cont)', NULL, 'create or replace function master.formula_ht2_cont(
                pnl2_cont float,
                cml2_cont integer
            ) returns integer as
            $body$
            declare
                ht2_cont integer;
            begin
                ht2_cont = pnl2_cont + cml2_cont;
                
                return ht2_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 07:41:01.58705');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('31', 'HT3_CONT = PNL3_CONT + CML3_CONT', '215', '342', 'plot', 'master.formula_ht3_cont(pnl3_cont, cml3_cont)', NULL, 'create or replace function master.formula_ht3_cont(
                pnl3_cont float,
                cml3_cont integer
            ) returns integer as
            $body$
            declare
                ht3_cont integer;
            begin
                ht3_cont = pnl3_cont + cml3_cont;
                
                return ht3_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 07:41:32.041682');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('32', 'HT4_CONT = PNL4_CONT + CML4_CONT', '216', '343', 'plot', 'master.formula_ht4_cont(pnl4_cont, cml4_cont)', NULL, 'create or replace function master.formula_ht4_cont(
                pnl4_cont float,
                cml4_cont integer
            ) returns integer as
            $body$
            declare
                ht4_cont integer;
            begin
                ht4_cont = pnl4_cont + cml4_cont;
                
                return ht4_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 07:42:05.977848');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('33', 'HT5_CONT = PNL5_CONT + CML5_CONT', '585', '567', 'plot', 'master.formula_ht5_cont(pnl5_cont, cml5_cont)', NULL, 'create or replace function master.formula_ht5_cont(
                pnl5_cont float,
                cml5_cont int
            ) returns float as
            $body$
            declare
                ht5_cont float;
            begin
                ht5_cont = pnl5_cont + cml5_cont;
                
                return ht5_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 07:43:44.08208');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('34', 'HT6_CONT = PNL6_CONT + CML6_CONT', '586', '568', 'plot', 'master.formula_ht6_cont(pnl6_cont, cml6_cont)', NULL, 'create or replace function master.formula_ht6_cont(
                pnl6_cont float,
                cml6_cont int
            ) returns float as
            $body$
            declare
                ht6_cont float;
            begin
                ht6_cont = pnl6_cont + cml6_cont;
                
                return ht6_cont;
            end
            $body$
            language plpgsql;', NULL, '6', '2014-12-02 07:44:40.042828');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('35', 'CML_CONT = AVE(CML<OBSN>_CONT)', '75', '121', 'plot', 'master.formula_cml_cont(cml1_cont, cml2_cont, cml3_cont, cml4_cont, cml5_cont, cml6_cont, cml7_cont, cml8_cont, cml9_cont, cml10_cont)', NULL, 'create or replace function master.formula_cml_cont(
				cml1_cont int,
                cml2_cont int,
                cml3_cont int,
                cml4_cont int,
                cml5_cont int,
                cml6_cont int,
                cml7_cont int,
                cml8_cont int,
                cml9_cont int,
                cml10_cont int
			) returns float as
			$body$
			declare
				cml_cont float;
              local_cml1_cont int;
              local_cml2_cont int;
              local_cml3_cont int;
              local_cml4_cont int;
              local_cml5_cont int;
              local_cml6_cont int;
              local_cml7_cont int;
              local_cml8_cont int;
              local_cml9_cont int;
              local_cml10_cont int;
			begin
				
cml_cont = 
					(
						select
							avg(t.x)::float
						from (
								values
(cml1_cont),
( cml2_cont),
( cml3_cont),
( cml4_cont),
( cml5_cont),
( cml6_cont),
( cml7_cont),
( cml8_cont),
( cml9_cont),
( cml10_cont)
							) t(x)
					);
				
				return cml_cont;
			end
			$body$
			language plpgsql;', NULL, '6', '2014-12-02 07:57:57.74263');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('36', 'PNL_CONT = AVE(PNL<OBSN>_CONT)', '198', '279', 'plot', 'master.formula_pnl_cont(pnl1_cont, pnl2_cont, pnl3_cont, pnl4_cont, pnl5_cont, pnl6_cont, pnl7_cont, pnl8_cont, pnl9_cont, pnl10_cont, pnl11_cont, pnl12_cont, pnl13_cont, pnl14_cont, pnl15_cont, pnl16_cont, pnl17_cont, pnl18_cont, pnl19_cont, pnl20_cont, pnl21_cont, pnl22_cont, pnl23_cont, pnl24_cont, pnl25_cont)', NULL, 'create or replace function master.formula_pnl_cont(
                pnl1_cont float,
                pnl2_cont float,
                pnl3_cont float,
                pnl4_cont float,
                pnl5_cont float,
                pnl6_cont float,
                pnl7_cont float,
                pnl8_cont float,
                pnl9_cont float,
                pnl10_cont float,
                pnl11_cont float,
                pnl12_cont float,
                pnl13_cont float,
                pnl14_cont float,
                pnl15_cont float,
                pnl16_cont float,
                pnl17_cont float,
                pnl18_cont float,
                pnl19_cont float,
                pnl20_cont float,
                pnl21_cont float,
                pnl22_cont float,
                pnl23_cont float,
                pnl24_cont float,
                pnl25_cont float
            ) returns float as
            $body$
            declare
                pnl_cont float;
              local_pnl1_cont float;
              local_pnl2_cont float;
              local_pnl3_cont float;
              local_pnl4_cont float;
              local_pnl5_cont float;
              local_pnl6_cont float;
              local_pnl7_cont float;
              local_pnl8_cont float;
              local_pnl9_cont float;
              local_pnl10_cont float;
              local_pnl11_cont float;
              local_pnl12_cont float;
              local_pnl13_cont float;
              local_pnl14_cont float;
              local_pnl15_cont float;
              local_pnl16_cont float;
              local_pnl17_cont float;
              local_pnl18_cont float;
              local_pnl19_cont float;
              local_pnl20_cont float;
              local_pnl21_cont float;
              local_pnl22_cont float;
              local_pnl23_cont float;
              local_pnl24_cont float;
              local_pnl25_cont float;
            begin
                
pnl_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(pnl1_cont),
( pnl2_cont),
( pnl3_cont),
( pnl4_cont),
( pnl5_cont),
( pnl6_cont),
( pnl7_cont),
( pnl8_cont),
( pnl9_cont),
( pnl10_cont),
( pnl11_cont),
( pnl12_cont),
( pnl13_cont),
( pnl14_cont),
( pnl15_cont),
( pnl16_cont),
( pnl17_cont),
( pnl18_cont),
( pnl19_cont),
( pnl20_cont),
( pnl21_cont),
( pnl22_cont),
( pnl23_cont),
( pnl24_cont),
( pnl25_cont)
                            ) t(x)
                    );
                
                return round(pnl_cont::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '6', '2014-12-02 07:58:50.898205');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('37', 'PANNO_CONT = AVE(PANNO<OBSN>_CONT)', '429', '280', 'plot', 'master.formula_panno_cont(panno1_cont, panno2_cont, panno3_cont, panno4_cont, panno5_cont, panno6_cont, panno7_cont, panno8_cont, panno9_cont, panno10_cont, panno11_cont, panno12_cont, panno13_cont, panno14_cont, panno15_cont)', NULL, 'create or replace function master.formula_panno_cont(
                panno1_cont int,
                panno2_cont int,
                panno3_cont int,
                panno4_cont int,
                panno5_cont int,
                panno6_cont int,
                panno7_cont int,
                panno8_cont int,
                panno9_cont int,
                panno10_cont int,
                panno11_cont int,
                panno12_cont int,
                panno13_cont int,
                panno14_cont int,
                panno15_cont int
            ) returns float as
            $body$
            declare
                panno_cont float;
              local_panno1_cont int;
              local_panno2_cont int;
              local_panno3_cont int;
              local_panno4_cont int;
              local_panno5_cont int;
              local_panno6_cont int;
              local_panno7_cont int;
              local_panno8_cont int;
              local_panno9_cont int;
              local_panno10_cont int;
              local_panno11_cont int;
              local_panno12_cont int;
              local_panno13_cont int;
              local_panno14_cont int;
              local_panno15_cont int;
            begin
                
panno_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(panno1_cont),
( panno2_cont),
( panno3_cont),
( panno4_cont),
( panno5_cont),
( panno6_cont),
( panno7_cont),
( panno8_cont),
( panno9_cont),
( panno10_cont),
( panno11_cont),
( panno12_cont),
( panno13_cont),
( panno14_cont),
( panno15_cont)
                            ) t(x)
                    );
                
                return panno_cont;
            end
            $body$
            language plpgsql;', '0', '6', '2014-12-02 08:00:25.525065');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('44', 'ADJAYLD_G_CONT=AYLD_CONT*MF_CONT', '1238', '935', 'plot', 'master.formula_adjayld_g_cont(ayld_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_g_cont(
				ayld_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_g_cont float;
              local_ayld_cont float;
              local_mf_cont float;
			begin
				
adjayld_g_cont=ayld_cont*mf_cont;
				
				return round(adjayld_g_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2015-01-12 03:05:06.308187');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('47', 'ADJYLD3_CONT = (AYLD_CONT/HVHILL_CONT) * TOTAL_HILL_CONT * MF_CONT', '1169', '869', 'plot', 'master.formula_adjyld3_cont(ayld_cont, hvhill_cont, total_hill_cont, mf_cont)', NULL, 'create or replace function master.formula_adjyld3_cont(
				ayld_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
			) returns float as
			$body$
			declare
				adjyld3_cont float;
              local_ayld_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
			begin
				
adjyld3_cont = (ayld_cont/hvhill_cont) * total_hill_cont * mf_cont;
				
				return round(adjyld3_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '19', '2015-03-24 02:06:14.048405');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('52', 'ADJAYLD_KG_CONT = AYLD_KG_CONT * MF_CONT', '1440', '1142', 'plot', 'master.formula_adjayld_kg_cont(ayld_kg_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont(
				ayld_kg_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont float;
              local_ayld_kg_cont float;
              local_mf_cont float;
			begin
				
adjayld_kg_cont = ayld_kg_cont * mf_cont;
				
				return round(adjayld_kg_cont::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '1', '2015-05-04 16:44:17.834913');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('54', 'MAT_CONT = FLW_CONT + 30', '180', '259', 'plot', 'master.formula_mat_cont(flw_cont)', NULL, 'create or replace function master.formula_mat_cont(
                flw_cont int
            ) returns float as
            $body$
            declare
                mat_cont float;
              local_flw_cont int;
            begin
                
mat_cont = flw_cont + 30;
                
                return mat_cont;
            end
            $body$
            language plpgsql;', NULL, '19', '2015-05-21 11:20:54.327126');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('55', 'ADJAYLD_KG_CONT3 = GRNYLD_HARV_CON * MF_HARV_CONT', '1469', '1171', 'plot', 'master.formula_adjayld_kg_cont3(grnyld_harv_con, mf_harv_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont3(
				grnyld_harv_con float,
                mf_harv_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont3 float;
              local_grnyld_harv_con float;
              local_mf_harv_cont float;
			begin
				
adjayld_kg_cont3 = grnyld_harv_con * mf_harv_cont;
				
				return round(adjayld_kg_cont3::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '19', '2015-05-22 11:05:27.283726');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('56', 'MF_HARV_CONT = (100 - MC_HARV_CONT)/86', '1471', '1173', 'plot', 'master.formula_mf_harv_cont(mc_harv_cont)', NULL, 'create or replace function master.formula_mf_harv_cont(
                mc_harv_cont float
            ) returns float as
            $body$
            declare
                mf_harv_cont float;
              local_mc_harv_cont float;
            begin
                
mf_harv_cont = (100 - mc_harv_cont)/86;
                
                return round(mf_harv_cont::numeric, 6);
            end
            $body$
            language plpgsql;', '6', '19', '2015-05-22 14:22:51.622644');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('57', 'YLD_CONT_TON3 = ADJAYLD_KG_CONT3 * 10/HV_AREA_SQM', '1470', '1172', 'plot', 'master.formula_yld_cont_ton3(adjayld_kg_cont3, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont_ton3(
                adjayld_kg_cont3 float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton3 float;
              local_adjayld_kg_cont3 float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton3 = adjayld_kg_cont3 * 10/hv_area_sqm;
                
                return round(yld_cont_ton3::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2015-05-22 14:23:52.637335');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('59', 'YLD_CONT_TON5 = ADJAYLD_KG_CONT * 10/HV_AREA_SQM', '1475', '1177', 'plot', 'master.formula_yld_cont_ton5(adjayld_kg_cont, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont_ton5(
                adjayld_kg_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton5 float;
              local_adjayld_kg_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton5 = adjayld_kg_cont * 10/hv_area_sqm;
                
                return round(yld_cont_ton5::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '19', '2015-05-25 11:14:33.379237');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('88', 'TOTAL_HILL_CONT = ROWS_PER_PLOT_CONT * HILLS_PER_ROW_CONT', '293', '472', 'plot', 'master.formula_total_hill_cont(rows_per_plot_cont, hills_per_row_cont)', NULL, 'create or replace function master.formula_total_hill_cont(
                rows_per_plot_cont int,
                hills_per_row_cont int
            ) returns integer as
            $body$
            declare
                total_hill_cont integer;
              local_rows_per_plot_cont int;
              local_hills_per_row_cont int;
            begin
                
total_hill_cont = rows_per_plot_cont * hills_per_row_cont;
                
                return total_hill_cont;
            end
            $body$
            language plpgsql;', NULL, '18', '2015-10-20 13:05:16.062421');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('91', 'ADJAYLD_KG_CONT4 = ( AYLD_KG_CONT/HVHILL_CONT) * TOTAL_HILL_CONT *MF_CONT', '1755', '1424', 'plot', 'master.formula_adjayld_kg_cont4(ayld_kg_cont, hvhill_cont, total_hill_cont, mf_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont4(
				ayld_kg_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont4 float;
              local_ayld_kg_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
			begin
				
adjayld_kg_cont4 = ( ayld_kg_cont/hvhill_cont) * total_hill_cont *mf_cont;
				
				return round(adjayld_kg_cont4::numeric, 3);
			end
			$body$
			language plpgsql;', '3', '18', '2016-06-14 16:47:19.287753');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('92', 'YLD_CONT_TON6  =  ADJAYLD_KG_CONT4 * 10/HV_AREA_SQM', '1756', '1425', 'plot', 'master.formula_yld_cont_ton6(adjayld_kg_cont4, hv_area_sqm)', NULL, 'create or replace function master.formula_yld_cont_ton6(
				adjayld_kg_cont4 float,
                hv_area_sqm float
			) returns float as
			$body$
			declare
				yld_cont_ton6 float;
              local_adjayld_kg_cont4 float;
              local_hv_area_sqm float;
			begin
				
yld_cont_ton6  =  adjayld_kg_cont4 * 10/hv_area_sqm;
				
				return round(yld_cont_ton6::numeric, 2);
			end
			$body$
			language plpgsql;', '2', '18', '2016-06-14 17:03:56.263145');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('93', 'TOTAL_PLANT_TESTED = PLANT_TESTED_SCORE1 + PLANT_TESTED_SCORE3 + PLANT_TESTED_SCORE5 + PLANT_TESTED_SCORE7 + PLANT_TESTED_SCORE9', '1759', '1428', 'plot', 'master.formula_total_plant_tested(plant_tested_score1, plant_tested_score3, plant_tested_score5, plant_tested_score7, plant_tested_score9)', NULL, 'create or replace function master.formula_total_plant_tested(
				plant_tested_score1 int,
                plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int
			) returns integer as
			$body$
			declare
				total_plant_tested integer;
              local_plant_tested_score1 int;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
			begin
				
total_plant_tested = plant_tested_score1 + plant_tested_score3 + plant_tested_score5 + plant_tested_score7 + plant_tested_score9;
				
				return total_plant_tested;
			end
			$body$
			language plpgsql;', NULL, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('94', 'DISEASE_INDEX = ((3*PLANT_TESTED_SCORE3)+(5*PLANT_TESTED_SCORE5)+(7*PLANT_TESTED_SCORE7)+(9*PLANT_TESTED_SCORE9))/TOTAL_PLANT_TESTED::float', '1765', '1434', 'plot', 'master.formula_disease_index(plant_tested_score3, plant_tested_score5, plant_tested_score7, plant_tested_score9, total_plant_tested)', NULL, 'create or replace function master.formula_disease_index(
				plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int,
                total_plant_tested int
			) returns float as
			$body$
			declare
				disease_index float;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
              local_total_plant_tested int;
			begin
				
disease_index = ((3*plant_tested_score3)+(5*plant_tested_score5)+(7*plant_tested_score7)+(9*plant_tested_score9))/total_plant_tested;
				
				return round(disease_index::numeric, 2);
			end
			$body$
			language plpgsql;', '2', '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('95', 'ADJAYLD_KG_CONT5 = ( GRNYLD_HARV_CON/HVHILL_CONT) * TOTAL_HILL_CONT *MF_HARV_CONT', '1898', '1567', 'plot', 'master.formula_adjayld_kg_cont5(grnyld_harv_con, hvhill_cont, total_hill_cont, mf_harv_cont)', NULL, 'create or replace function master.formula_adjayld_kg_cont5(
				grnyld_harv_con float,
                hvhill_cont int,
                total_hill_cont int,
                mf_harv_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont5 float;
              local_grnyld_harv_con float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_harv_cont float;
			begin
				
adjayld_kg_cont5 = ( grnyld_harv_con/hvhill_cont) * total_hill_cont *mf_harv_cont;
				
				return round(adjayld_kg_cont5::numeric, 2);
			end
			$body$
			language plpgsql;', '2', '18', '2016-11-04 10:53:55.299176');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('96', 'MAT_CONT2=MAT_DATE_CONT - FLW_DATE_CONT', '1899', '1568', 'plot', 'master.formula_mat_cont2(mat_date_cont, flw_date_cont)', NULL, 'create or replace function master.formula_mat_cont2(
				mat_date_cont date,
                flw_date_cont date
			) returns integer as
			$body$
			declare
				mat_cont2 integer;
              local_mat_date_cont date;
              local_flw_date_cont date;
			begin
				
mat_cont2=mat_date_cont - flw_date_cont;
				
				return mat_cont2;
			end
			$body$
			language plpgsql;', NULL, '18', '2016-11-04 13:19:41.205423');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('98', 'YLD_CONT_TON7=ADJAYLD_KG_CONT*10/ADJHVAREA_CONT2', '1905', '1574', 'plot', 'master.formula_yld_cont_ton7(adjayld_kg_cont, adjhvarea_cont2)', NULL, 'create or replace function master.formula_yld_cont_ton7(
                adjayld_kg_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton7 float;
              local_adjayld_kg_cont float;
              local_adjhvarea_cont2 float;
            begin
                
yld_cont_ton7=adjayld_kg_cont*10/adjhvarea_cont2;
                
                return round(yld_cont_ton7::numeric, 2);
            end
            $body$
            language plpgsql;', '2', '18', '2016-11-16 17:06:57.431355');

INSERT INTO master.formula (id, formula, result_variable_id, method_id, data_level, function_name, formatted_formula, database_formula, decimal_place, creator_id, creation_timestamp)
    VALUES ('105', 'AREA_FACT = 10/HV_AREA_SQM', '2117', '1784', 'plot', 'master.formula_area_fact(hv_area_sqm)', NULL, 'create or replace function master.formula_area_fact(
                hv_area_sqm float
            ) returns integer as
            $body$
            declare
                area_fact integer;
              local_hv_area_sqm float;
            begin
                
area_fact = 10/hv_area_sqm;
                
                return area_fact;
            end
            $body$
            language plpgsql;', NULL, '19', '2017-10-18 11:40:48.868719');


-- insert formula parameters
INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('1', '1', '400', 'plot', '78', false, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('2', '1', '250', 'study;entry;plot', '78', false, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('15', '4', '293', 'plot', '181', false, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('16', '4', '142', 'plot', '181', false, '6', '2014-09-15 15:05:10');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('24', '5', '397', 'plot', '413', false, '6', '2014-10-05 03:13:58.138416');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('25', '5', '183', 'plot', '413', false, '6', '2014-10-05 03:13:58.138416');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('26', '5', '412', 'plot', '413', false, '6', '2014-10-05 03:13:58.138416');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('34', '11', '397', 'plot', '414', false, '6', '2014-10-05 03:37:42.013231');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('35', '11', '412', 'plot', '414', false, '6', '2014-10-05 03:37:42.013231');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('40', '16', '141', 'plot', '403', false, '6', '2014-10-05 03:40:51.347854');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('41', '16', '250', 'plot;entry;study', '403', false, '6', '2014-10-05 03:40:51.347854');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('52', '14', '1143', 'plot', '754', false, '19', '2014-11-10 02:14:54.135762');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('72', '29', '650', 'plot', '213', false, '6', '2014-12-02 05:34:22.139097');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('73', '29', '675', 'plot', '213', false, '6', '2014-12-02 05:34:22.139097');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('74', '30', '651', 'plot', '214', false, '6', '2014-12-02 07:41:01.58705');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('75', '30', '676', 'plot', '214', false, '6', '2014-12-02 07:41:01.58705');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('76', '31', '652', 'plot', '215', false, '6', '2014-12-02 07:41:32.041682');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('77', '31', '677', 'plot', '215', false, '6', '2014-12-02 07:41:32.041682');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('78', '32', '653', 'plot', '216', false, '6', '2014-12-02 07:42:05.977848');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('79', '32', '678', 'plot', '216', false, '6', '2014-12-02 07:42:05.977848');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('272', '21', '397', 'plot', '1144', false, '6', '2014-12-11 06:53:07.580002');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('273', '21', '562', 'study;plot', '1144', false, '6', '2014-12-11 06:53:07.580002');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('283', '34', '668', 'plot', '586', false, '6', '2014-12-11 07:18:14.329376');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('284', '34', '680', 'plot', '586', false, '6', '2014-12-11 07:18:14.329376');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('285', '33', '654', 'plot', '585', false, '6', '2014-12-11 07:19:27.73158');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('286', '33', '679', 'plot', '585', false, '6', '2014-12-11 07:19:27.73158');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('399', '54', '78', 'plot', '180', false, '19', '2015-05-21 11:20:54.327126');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('437', '56', '591', 'plot', '1471', false, '19', '2015-05-22 14:22:51.622644');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('451', '57', '1469', 'plot', '1470', false, '19', '2015-05-22 15:05:22.791672');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('452', '57', '562', 'plot', '1470', false, '19', '2015-05-22 15:05:22.791672');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('465', '59', '1440', 'plot', '1475', false, '19', '2015-05-25 11:14:33.379237');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('466', '59', '562', 'plot', '1475', false, '19', '2015-05-25 11:14:33.379237');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('545', '27', '1169', 'plot', '1213', false, '19', '2015-10-13 14:38:38.06757');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('563', '88', '188', 'study', '293', false, '18', '2015-10-20 13:05:16.062421');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('564', '88', '380', 'study', '293', false, '18', '2015-10-20 13:05:16.062421');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('605', '37', '655', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('606', '37', '656', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('607', '37', '657', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('608', '37', '658', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('609', '37', '659', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('610', '37', '663', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('611', '37', '664', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('612', '37', '665', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('613', '37', '666', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('614', '37', '667', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('615', '37', '1455', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('616', '37', '1456', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('617', '37', '1457', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('618', '37', '1458', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('619', '37', '1459', 'plot', '429', false, '18', '2016-03-15 15:32:51.092046');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('645', '3', '287', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('646', '3', '288', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('647', '3', '289', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('648', '3', '290', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('649', '3', '291', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('650', '3', '292', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('651', '3', '1138', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('652', '3', '1139', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('653', '3', '1140', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('654', '3', '1141', 'plot', '285', false, '18', '2016-04-08 21:08:00.986831');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('679', '92', '1755', 'plot', '1756', false, '18', '2016-06-14 17:03:56.263145');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('680', '92', '562', 'plot', '1756', false, '18', '2016-06-14 17:03:56.263145');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('681', '93', '1760', 'plot', '1759', false, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('682', '93', '1761', 'plot', '1759', false, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('683', '93', '1762', 'plot', '1759', false, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('684', '93', '1763', 'plot', '1759', false, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('685', '93', '1764', 'plot', '1759', false, '19', '2016-06-24 13:24:05.651351');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('686', '94', '1761', 'plot', '1765', false, '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('687', '94', '1762', 'plot', '1765', false, '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('688', '94', '1763', 'plot', '1765', false, '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('689', '94', '1764', 'plot', '1765', false, '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('690', '94', '1759', 'plot', '1765', false, '19', '2016-06-24 13:54:47.015532');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('695', '95', '590', 'plot', '1898', false, '18', '2016-11-04 10:53:55.299176');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('696', '95', '142', 'plot', '1898', false, '18', '2016-11-04 10:53:55.299176');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('697', '95', '293', 'plot', '1898', false, '18', '2016-11-04 10:53:55.299176');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('698', '95', '1471', 'plot', '1898', false, '18', '2016-11-04 10:53:55.299176');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('701', '96', '1560', 'plot', '1899', false, '18', '2016-11-04 17:55:25.253983');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('702', '96', '400', 'plot', '1899', false, '18', '2016-11-04 17:55:25.253983');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('703', '17', '397', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('704', '17', '1224', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('705', '17', '423', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('706', '17', '424', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('707', '17', '425', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('708', '17', '426', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('709', '17', '427', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('710', '17', '642', 'plot', '411', false, '1', '2016-11-07 11:29:26.058851');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('717', '35', '675', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('718', '35', '676', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('719', '35', '677', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('720', '35', '678', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('721', '35', '679', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('722', '35', '680', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('723', '35', '681', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('724', '35', '682', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('725', '35', '683', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('726', '35', '684', 'plot', '75', false, '6', '2016-11-22 13:33:57.451308');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('727', '7', '182', 'plot', '183', false, '19', '2016-12-01 14:17:21.238648');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('728', '52', '628', 'plot', '1440', false, '19', '2016-12-01 14:38:15.231525');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('729', '52', '183', 'plot', '1440', false, '19', '2016-12-01 14:38:15.231525');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('732', '55', '590', 'plot', '1469', false, '19', '2016-12-01 14:42:42.961858');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('733', '55', '1471', 'plot', '1469', false, '19', '2016-12-01 14:42:42.961858');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('734', '91', '628', 'plot', '1755', false, '19', '2016-12-01 15:46:43.334669');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('735', '91', '142', 'plot', '1755', false, '19', '2016-12-01 15:46:43.334669');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('736', '91', '293', 'plot', '1755', false, '19', '2016-12-01 15:46:43.334669');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('737', '91', '183', 'plot', '1755', false, '19', '2016-12-01 15:46:43.334669');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('738', '47', '397', 'plot', '1169', false, '19', '2016-12-01 15:47:48.143033');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('739', '47', '142', 'plot', '1169', false, '19', '2016-12-01 15:47:48.143033');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('740', '47', '293', 'plot', '1169', false, '19', '2016-12-01 15:47:48.143033');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('741', '47', '183', 'plot', '1169', false, '19', '2016-12-01 15:47:48.143033');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('742', '44', '397', 'plot', '1238', false, '19', '2016-12-01 15:57:50.734383');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('743', '44', '183', 'plot', '1238', false, '19', '2016-12-01 15:57:50.734383');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('744', '36', '650', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('745', '36', '651', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('746', '36', '652', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('747', '36', '653', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('748', '36', '654', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('749', '36', '668', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('750', '36', '669', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('751', '36', '670', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('752', '36', '671', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('753', '36', '672', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('754', '36', '1623', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('755', '36', '1624', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('756', '36', '1625', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('757', '36', '1626', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('758', '36', '1627', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('759', '36', '1628', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('760', '36', '1629', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('761', '36', '1630', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('762', '36', '1631', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('763', '36', '1632', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('764', '36', '1633', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('765', '36', '1634', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('766', '36', '1635', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('767', '36', '1637', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('768', '36', '1636', 'plot', '198', false, '18', '2017-04-25 09:27:34.648658');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('769', '2', '213', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('770', '2', '214', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('771', '2', '215', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('772', '2', '216', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('773', '2', '585', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('774', '2', '586', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('775', '2', '1428', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('776', '2', '1429', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('777', '2', '1430', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('778', '2', '1431', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('779', '2', '1974', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('780', '2', '1975', 'plot', '212', false, '18', '2017-04-25 09:30:31.431852');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('794', '105', '562', 'plot', '2117', false, '19', '2017-10-18 11:40:48.868719');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('798', '22', '397', 'plot', '1143', false, '19', '2018-01-05 12:09:36.878239');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('799', '22', '183', 'plot', '1143', false, '19', '2018-01-05 12:09:36.878239');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('800', '22', '562', 'plot;study', '1143', false, '19', '2018-01-05 12:09:36.878239');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('863', '98', '1440', 'plot', '1905', false, '18', '2019-12-13 18:21:24.440904');

INSERT INTO master.formula_parameter (id, formula_id, param_variable_id, data_level, result_variable_id, is_array, creator_id, creation_timestamp)
    VALUES ('864', '98', '1142', 'plot', '1905', false, '18', '2019-12-13 18:21:24.440904');


-- create functions
create or replace function master.formula_yld_cont1(
                ayld_cont float,
                mf_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_cont1 float;
            begin
                yld_cont1 = ayld_cont * mf_cont * 10 / adjhvarea_cont1;
                
                return round(yld_cont1::numeric, 4);
            end
            $body$
            language plpgsql;

create or replace function master.formula_flw_cont(
                flw_date_cont date,
                seeding_date_cont date
            ) returns float as
            $body$
            declare
                flw_cont float;
            begin
                flw_cont = flw_date_cont - seeding_date_cont;
                
                return flw_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_ht5_cont(
                pnl5_cont float,
                cml5_cont int
            ) returns float as
            $body$
            declare
                ht5_cont float;
            begin
                ht5_cont = pnl5_cont + cml5_cont;
                
                return ht5_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_panno_cont(
                panno1_cont int,
                panno2_cont int,
                panno3_cont int,
                panno4_cont int,
                panno5_cont int,
                panno6_cont int,
                panno7_cont int,
                panno8_cont int,
                panno9_cont int,
                panno10_cont int,
                panno11_cont int,
                panno12_cont int,
                panno13_cont int,
                panno14_cont int,
                panno15_cont int
            ) returns float as
            $body$
            declare
                panno_cont float;
              local_panno1_cont int;
              local_panno2_cont int;
              local_panno3_cont int;
              local_panno4_cont int;
              local_panno5_cont int;
              local_panno6_cont int;
              local_panno7_cont int;
              local_panno8_cont int;
              local_panno9_cont int;
              local_panno10_cont int;
              local_panno11_cont int;
              local_panno12_cont int;
              local_panno13_cont int;
              local_panno14_cont int;
              local_panno15_cont int;
            begin
                
panno_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(panno1_cont),
( panno2_cont),
( panno3_cont),
( panno4_cont),
( panno5_cont),
( panno6_cont),
( panno7_cont),
( panno8_cont),
( panno9_cont),
( panno10_cont),
( panno11_cont),
( panno12_cont),
( panno13_cont),
( panno14_cont),
( panno15_cont)
                            ) t(x)
                    );
                
                return panno_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_tplyld_cont_ps(
				ayld_cont float,
                grnwt_ps5_cont float,
                grnwt_ps10_cont float,
                grnwt_ps15_cont float,
                grnwt_ps20_cont float,
                grnwt_ps25_cont float,
                grnwt_ps50_cont float,
                grnwt_plant_cont float
			) returns float as
			$body$
			declare
				tplyld_cont_ps float;
              local_ayld_cont float;
              local_grnwt_ps5_cont float;
              local_grnwt_ps10_cont float;
              local_grnwt_ps15_cont float;
              local_grnwt_ps20_cont float;
              local_grnwt_ps25_cont float;
              local_grnwt_ps50_cont float;
              local_grnwt_plant_cont float;
			begin
				
tplyld_cont_ps = ayld_cont + 
					(
						select
							sum(t.x)::float
						from (
								values
(grnwt_ps5_cont),
( grnwt_ps10_cont),
( grnwt_ps15_cont),
( grnwt_ps20_cont),
( grnwt_ps25_cont),
( grnwt_ps50_cont),
(0)
							) t(x)
					)
								 + 	grnwt_plant_cont;
				
				return tplyld_cont_ps;
			end
			$body$
			language plpgsql;

create or replace function master.formula_til_ave_cont(
                till1_cont int,
                till2_cont int,
                till3_cont int,
                till4_cont int,
                till5_cont int,
                till6_cont int,
                till7_cont int,
                till8_cont int,
                till9_cont int,
                till10_cont int
            ) returns float as
            $body$
            declare
                til_ave_cont float;
              local_till1_cont int;
              local_till2_cont int;
              local_till3_cont int;
              local_till4_cont int;
              local_till5_cont int;
              local_till6_cont int;
              local_till7_cont int;
              local_till8_cont int;
              local_till9_cont int;
              local_till10_cont int;
            begin
                
til_ave_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(till1_cont),
( till2_cont),
( till3_cont),
( till4_cont),
( till5_cont),
( till6_cont),
( till7_cont),
( till8_cont),
( till9_cont),
( till10_cont)
                            ) t(x)
                    );
                
                return round(til_ave_cont::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_ht2_cont(
                pnl2_cont float,
                cml2_cont integer
            ) returns integer as
            $body$
            declare
                ht2_cont integer;
            begin
                ht2_cont = pnl2_cont + cml2_cont;
                
                return ht2_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_mf_cont(
				mc_cont float
			) returns float as
			$body$
			declare
				mf_cont float;
              local_mc_cont float;
			begin
				
mf_cont = (100 - mc_cont) / 86;
				
				return round(mf_cont::numeric, 7);
			end
			$body$
			language plpgsql;

create or replace function master.formula_ht4_cont(
                pnl4_cont float,
                cml4_cont integer
            ) returns integer as
            $body$
            declare
                ht4_cont integer;
            begin
                ht4_cont = pnl4_cont + cml4_cont;
                
                return ht4_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_0_cont1(
                ayld_cont float,
                adjhvarea_cont1 float
            ) returns float as
            $body$
            declare
                yld_0_cont1 float;
            begin
                yld_0_cont1 = ayld_cont * 10 / adjhvarea_cont1;
                
                return yld_0_cont1;
            end
            $body$
            language plpgsql;

create or replace function master.formula_dth_cont(
                hvdate_cont date,
                seeding_date_cont date
            ) returns integer as
            $body$
            declare
                dth_cont integer;
            begin
                dth_cont = hvdate_cont - seeding_date_cont;
                
                return dth_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_ht_cont(
                ht1_cont float,
                ht2_cont float,
                ht3_cont float,
                ht4_cont float,
                ht5_cont float,
                ht6_cont float,
                ht7_cont float,
                ht8_cont float,
                ht9_cont float,
                ht10_cont float,
                ht11_cont float,
                ht12_cont float
            ) returns float as
            $body$
            declare
                ht_cont float;
              local_ht1_cont float;
              local_ht2_cont float;
              local_ht3_cont float;
              local_ht4_cont float;
              local_ht5_cont float;
              local_ht6_cont float;
              local_ht7_cont float;
              local_ht8_cont float;
              local_ht9_cont float;
              local_ht10_cont float;
              local_ht11_cont float;
              local_ht12_cont float;
            begin
                
ht_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(ht1_cont),
( ht2_cont),
( ht3_cont),
( ht4_cont),
( ht5_cont),
( ht6_cont),
( ht7_cont),
( ht8_cont),
( ht9_cont),
( ht10_cont),
( ht11_cont),
( ht12_cont)
                            ) t(x)
                    );
                
                return round(ht_cont::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_cont_ton(
                yld_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton float;
            begin
                yld_cont_ton = yld_cont2 / 1000;
                
                return round(yld_cont_ton::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_ht3_cont(
                pnl3_cont float,
                cml3_cont integer
            ) returns integer as
            $body$
            declare
                ht3_cont integer;
            begin
                ht3_cont = pnl3_cont + cml3_cont;
                
                return ht3_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_pnl_cont(
                pnl1_cont float,
                pnl2_cont float,
                pnl3_cont float,
                pnl4_cont float,
                pnl5_cont float,
                pnl6_cont float,
                pnl7_cont float,
                pnl8_cont float,
                pnl9_cont float,
                pnl10_cont float,
                pnl11_cont float,
                pnl12_cont float,
                pnl13_cont float,
                pnl14_cont float,
                pnl15_cont float,
                pnl16_cont float,
                pnl17_cont float,
                pnl18_cont float,
                pnl19_cont float,
                pnl20_cont float,
                pnl21_cont float,
                pnl22_cont float,
                pnl23_cont float,
                pnl24_cont float,
                pnl25_cont float
            ) returns float as
            $body$
            declare
                pnl_cont float;
              local_pnl1_cont float;
              local_pnl2_cont float;
              local_pnl3_cont float;
              local_pnl4_cont float;
              local_pnl5_cont float;
              local_pnl6_cont float;
              local_pnl7_cont float;
              local_pnl8_cont float;
              local_pnl9_cont float;
              local_pnl10_cont float;
              local_pnl11_cont float;
              local_pnl12_cont float;
              local_pnl13_cont float;
              local_pnl14_cont float;
              local_pnl15_cont float;
              local_pnl16_cont float;
              local_pnl17_cont float;
              local_pnl18_cont float;
              local_pnl19_cont float;
              local_pnl20_cont float;
              local_pnl21_cont float;
              local_pnl22_cont float;
              local_pnl23_cont float;
              local_pnl24_cont float;
              local_pnl25_cont float;
            begin
                
pnl_cont = 
                    (
                        select
                            avg(t.x)::float
                        from (
                                values
(pnl1_cont),
( pnl2_cont),
( pnl3_cont),
( pnl4_cont),
( pnl5_cont),
( pnl6_cont),
( pnl7_cont),
( pnl8_cont),
( pnl9_cont),
( pnl10_cont),
( pnl11_cont),
( pnl12_cont),
( pnl13_cont),
( pnl14_cont),
( pnl15_cont),
( pnl16_cont),
( pnl17_cont),
( pnl18_cont),
( pnl19_cont),
( pnl20_cont),
( pnl21_cont),
( pnl22_cont),
( pnl23_cont),
( pnl24_cont),
( pnl25_cont)
                            ) t(x)
                    );
                
                return round(pnl_cont::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_cml_cont(
				cml1_cont int,
                cml2_cont int,
                cml3_cont int,
                cml4_cont int,
                cml5_cont int,
                cml6_cont int,
                cml7_cont int,
                cml8_cont int,
                cml9_cont int,
                cml10_cont int
			) returns float as
			$body$
			declare
				cml_cont float;
              local_cml1_cont int;
              local_cml2_cont int;
              local_cml3_cont int;
              local_cml4_cont int;
              local_cml5_cont int;
              local_cml6_cont int;
              local_cml7_cont int;
              local_cml8_cont int;
              local_cml9_cont int;
              local_cml10_cont int;
			begin
				
cml_cont = 
					(
						select
							avg(t.x)::float
						from (
								values
(cml1_cont),
( cml2_cont),
( cml3_cont),
( cml4_cont),
( cml5_cont),
( cml6_cont),
( cml7_cont),
( cml8_cont),
( cml9_cont),
( cml10_cont)
							) t(x)
					);
				
				return cml_cont;
			end
			$body$
			language plpgsql;

create or replace function master.formula_ht6_cont(
                pnl6_cont float,
                cml6_cont int
            ) returns float as
            $body$
            declare
                ht6_cont float;
            begin
                ht6_cont = pnl6_cont + cml6_cont;
                
                return ht6_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_miss_hill_cont(
                total_hill_cont integer,
                hvhill_cont integer
            ) returns float as
            $body$
            declare
                miss_hill_cont float;
            begin
                miss_hill_cont = total_hill_cont - hvhill_cont;
                
                return miss_hill_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_ht1_cont(
                pnl1_cont float,
                cml1_cont integer
            ) returns integer as
            $body$
            declare
                ht1_cont integer;
            begin
                ht1_cont = pnl1_cont + cml1_cont;
                
                return ht1_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_adjayld_kg_cont4(
				ayld_kg_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont4 float;
              local_ayld_kg_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
			begin
				
adjayld_kg_cont4 = ( ayld_kg_cont/hvhill_cont) * total_hill_cont *mf_cont;
				
				return round(adjayld_kg_cont4::numeric, 3);
			end
			$body$
			language plpgsql;

create or replace function master.formula_adjayld_kg_cont5(
				grnyld_harv_con float,
                hvhill_cont int,
                total_hill_cont int,
                mf_harv_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont5 float;
              local_grnyld_harv_con float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_harv_cont float;
			begin
				
adjayld_kg_cont5 = ( grnyld_harv_con/hvhill_cont) * total_hill_cont *mf_harv_cont;
				
				return round(adjayld_kg_cont5::numeric, 2);
			end
			$body$
			language plpgsql;

create or replace function master.formula_total_hill_cont(
                rows_per_plot_cont int,
                hills_per_row_cont int
            ) returns integer as
            $body$
            declare
                total_hill_cont integer;
              local_rows_per_plot_cont int;
              local_hills_per_row_cont int;
            begin
                
total_hill_cont = rows_per_plot_cont * hills_per_row_cont;
                
                return total_hill_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_adjayld_g_cont(
				ayld_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_g_cont float;
              local_ayld_cont float;
              local_mf_cont float;
			begin
				
adjayld_g_cont=ayld_cont*mf_cont;
				
				return round(adjayld_g_cont::numeric, 3);
			end
			$body$
			language plpgsql;

create or replace function master.formula_yld_cont_ton7(
                adjayld_kg_cont float,
                adjhvarea_cont2 float
            ) returns float as
            $body$
            declare
                yld_cont_ton7 float;
              local_adjayld_kg_cont float;
              local_adjhvarea_cont2 float;
            begin
                
yld_cont_ton7=adjayld_kg_cont*10/adjhvarea_cont2;
                
                return round(yld_cont_ton7::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_cont_ton6(
				adjayld_kg_cont4 float,
                hv_area_sqm float
			) returns float as
			$body$
			declare
				yld_cont_ton6 float;
              local_adjayld_kg_cont4 float;
              local_hv_area_sqm float;
			begin
				
yld_cont_ton6  =  adjayld_kg_cont4 * 10/hv_area_sqm;
				
				return round(yld_cont_ton6::numeric, 2);
			end
			$body$
			language plpgsql;

create or replace function master.formula_mat_cont2(
				mat_date_cont date,
                flw_date_cont date
			) returns integer as
			$body$
			declare
				mat_cont2 integer;
              local_mat_date_cont date;
              local_flw_date_cont date;
			begin
				
mat_cont2=mat_date_cont - flw_date_cont;
				
				return mat_cont2;
			end
			$body$
			language plpgsql;

create or replace function master.formula_mf_harv_cont(
                mc_harv_cont float
            ) returns float as
            $body$
            declare
                mf_harv_cont float;
              local_mc_harv_cont float;
            begin
                
mf_harv_cont = (100 - mc_harv_cont)/86;
                
                return round(mf_harv_cont::numeric, 6);
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_cont_ton2(
                adjyld3_cont float
            ) returns float as
            $body$
            declare
                yld_cont_ton2 float;
              local_adjyld3_cont float;
            begin
                
yld_cont_ton2 = adjyld3_cont / 1000;
                
                return round(yld_cont_ton2::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_adjayld_kg_cont3(
				grnyld_harv_con float,
                mf_harv_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont3 float;
              local_grnyld_harv_con float;
              local_mf_harv_cont float;
			begin
				
adjayld_kg_cont3 = grnyld_harv_con * mf_harv_cont;
				
				return round(adjayld_kg_cont3::numeric, 3);
			end
			$body$
			language plpgsql;

create or replace function master.formula_adjyld3_cont(
				ayld_cont float,
                hvhill_cont int,
                total_hill_cont int,
                mf_cont float
			) returns float as
			$body$
			declare
				adjyld3_cont float;
              local_ayld_cont float;
              local_hvhill_cont int;
              local_total_hill_cont int;
              local_mf_cont float;
			begin
				
adjyld3_cont = (ayld_cont/hvhill_cont) * total_hill_cont * mf_cont;
				
				return round(adjyld3_cont::numeric, 3);
			end
			$body$
			language plpgsql;

create or replace function master.formula_yld_0_cont2(
                ayld_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_0_cont2 float;
            begin
                yld_0_cont2 = ayld_cont * 10 / hv_area_sqm;
                
                return yld_0_cont2;
            end
            $body$
            language plpgsql;

create or replace function master.formula_disease_index(
				plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int,
                total_plant_tested int
			) returns float as
			$body$
			declare
				disease_index float;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
              local_total_plant_tested int;
			begin
				
disease_index = ((3*plant_tested_score3)+(5*plant_tested_score5)+(7*plant_tested_score7)+(9*plant_tested_score9))/total_plant_tested;
				
				return round(disease_index::numeric, 2);
			end
			$body$
			language plpgsql;

create or replace function master.formula_yld_cont_ton3(
                adjayld_kg_cont3 float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton3 float;
              local_adjayld_kg_cont3 float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton3 = adjayld_kg_cont3 * 10/hv_area_sqm;
                
                return round(yld_cont_ton3::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_cont_ton5(
                adjayld_kg_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont_ton5 float;
              local_adjayld_kg_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont_ton5 = adjayld_kg_cont * 10/hv_area_sqm;
                
                return round(yld_cont_ton5::numeric, 2);
            end
            $body$
            language plpgsql;

create or replace function master.formula_yld_cont2(
                ayld_cont float,
                mf_cont float,
                hv_area_sqm float
            ) returns float as
            $body$
            declare
                yld_cont2 float;
              local_ayld_cont float;
              local_mf_cont float;
              local_hv_area_sqm float;
            begin
                
yld_cont2 = ayld_cont * mf_cont * 10/hv_area_sqm;
                
                return yld_cont2;
            end
            $body$
            language plpgsql;

create or replace function master.formula_total_plant_tested(
				plant_tested_score1 int,
                plant_tested_score3 int,
                plant_tested_score5 int,
                plant_tested_score7 int,
                plant_tested_score9 int
			) returns integer as
			$body$
			declare
				total_plant_tested integer;
              local_plant_tested_score1 int;
              local_plant_tested_score3 int;
              local_plant_tested_score5 int;
              local_plant_tested_score7 int;
              local_plant_tested_score9 int;
			begin
				
total_plant_tested = plant_tested_score1 + plant_tested_score3 + plant_tested_score5 + plant_tested_score7 + plant_tested_score9;
				
				return total_plant_tested;
			end
			$body$
			language plpgsql;

create or replace function master.formula_area_fact(
                hv_area_sqm float
            ) returns integer as
            $body$
            declare
                area_fact integer;
              local_hv_area_sqm float;
            begin
                
area_fact = 10/hv_area_sqm;
                
                return area_fact;
            end
            $body$
            language plpgsql;

create or replace function master.formula_mat_cont(
                flw_cont int
            ) returns float as
            $body$
            declare
                mat_cont float;
              local_flw_cont int;
            begin
                
mat_cont = flw_cont + 30;
                
                return mat_cont;
            end
            $body$
            language plpgsql;

create or replace function master.formula_adjayld_kg_cont(
				ayld_kg_cont float,
                mf_cont float
			) returns float as
			$body$
			declare
				adjayld_kg_cont float;
              local_ayld_kg_cont float;
              local_mf_cont float;
			begin
				
adjayld_kg_cont = ayld_kg_cont * mf_cont;
				
				return round(adjayld_kg_cont::numeric, 3);
			end
			$body$
			language plpgsql;

-- update formula_id in methods
UPDATE master.method
SET formula_id = '1'
WHERE id = '124';

UPDATE master.method
SET formula_id = '2'
WHERE id = '345';

UPDATE master.method
SET formula_id = '3'
WHERE id = '463';

UPDATE master.method
SET formula_id = '4'
WHERE id = '261';

UPDATE master.method
SET formula_id = '5'
WHERE id = '194';

UPDATE master.method
SET formula_id = '7'
WHERE id = '263';

UPDATE master.method
SET formula_id = '11'
WHERE id = '195';

UPDATE master.method
SET formula_id = '14'
WHERE id = '824';

UPDATE master.method
SET formula_id = '16'
WHERE id = '125';

UPDATE master.method
SET formula_id = '17'
WHERE id = '44';

UPDATE master.method
SET formula_id = '21'
WHERE id = '846';

UPDATE master.method
SET formula_id = '22'
WHERE id = '845';

UPDATE master.method
SET formula_id = '27'
WHERE id = '908';

UPDATE master.method
SET formula_id = '29'
WHERE id = '340';

UPDATE master.method
SET formula_id = '30'
WHERE id = '344';

UPDATE master.method
SET formula_id = '31'
WHERE id = '342';

UPDATE master.method
SET formula_id = '32'
WHERE id = '343';

UPDATE master.method
SET formula_id = '33'
WHERE id = '567';

UPDATE master.method
SET formula_id = '34'
WHERE id = '568';

UPDATE master.method
SET formula_id = '35'
WHERE id = '121';

UPDATE master.method
SET formula_id = '36'
WHERE id = '279';

UPDATE master.method
SET formula_id = '37'
WHERE id = '280';

UPDATE master.method
SET formula_id = '44'
WHERE id = '935';

UPDATE master.method
SET formula_id = '47'
WHERE id = '869';

UPDATE master.method
SET formula_id = '52'
WHERE id = '1142';

UPDATE master.method
SET formula_id = '54'
WHERE id = '259';

UPDATE master.method
SET formula_id = '55'
WHERE id = '1171';

UPDATE master.method
SET formula_id = '56'
WHERE id = '1173';

UPDATE master.method
SET formula_id = '57'
WHERE id = '1172';

UPDATE master.method
SET formula_id = '59'
WHERE id = '1177';

UPDATE master.method
SET formula_id = '88'
WHERE id = '472';

UPDATE master.method
SET formula_id = '91'
WHERE id = '1424';

UPDATE master.method
SET formula_id = '92'
WHERE id = '1425';

UPDATE master.method
SET formula_id = '93'
WHERE id = '1428';

UPDATE master.method
SET formula_id = '94'
WHERE id = '1434';

UPDATE master.method
SET formula_id = '95'
WHERE id = '1567';

UPDATE master.method
SET formula_id = '96'
WHERE id = '1568';

UPDATE master.method
SET formula_id = '98'
WHERE id = '1574';

UPDATE master.method
SET formula_id = '105'
WHERE id = '1784';



-- reset formula_id in methods
--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '124';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '345';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '463';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '261';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '194';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '263';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '195';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '824';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '125';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '44';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '846';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '845';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '908';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '340';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '344';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '342';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '343';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '567';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '568';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '121';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '279';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '280';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '935';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '869';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1142';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '259';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1171';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1173';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1172';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1177';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '472';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1424';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1425';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1428';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1434';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1567';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1568';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1574';

--rollback UPDATE master.method
--rollback SET formula_id = NULL
--rollback WHERE id = '1784';


-- drop functions

--rollback DROP FUNCTION master.formula_yld_cont1;

--rollback DROP FUNCTION master.formula_flw_cont;

--rollback DROP FUNCTION master.formula_ht5_cont;

--rollback DROP FUNCTION master.formula_panno_cont;

--rollback DROP FUNCTION master.formula_tplyld_cont_ps;

--rollback DROP FUNCTION master.formula_til_ave_cont;

--rollback DROP FUNCTION master.formula_ht2_cont;

--rollback DROP FUNCTION master.formula_mf_cont;

--rollback DROP FUNCTION master.formula_ht4_cont;

--rollback DROP FUNCTION master.formula_yld_0_cont1;

--rollback DROP FUNCTION master.formula_dth_cont;

--rollback DROP FUNCTION master.formula_ht_cont;

--rollback DROP FUNCTION master.formula_yld_cont_ton;

--rollback DROP FUNCTION master.formula_ht3_cont;

--rollback DROP FUNCTION master.formula_pnl_cont;

--rollback DROP FUNCTION master.formula_cml_cont;

--rollback DROP FUNCTION master.formula_ht6_cont;

--rollback DROP FUNCTION master.formula_miss_hill_cont;

--rollback DROP FUNCTION master.formula_ht1_cont;

--rollback DROP FUNCTION master.formula_adjayld_kg_cont4;

--rollback DROP FUNCTION master.formula_adjayld_kg_cont5;

--rollback DROP FUNCTION master.formula_total_hill_cont;

--rollback DROP FUNCTION master.formula_adjayld_g_cont;

--rollback DROP FUNCTION master.formula_yld_cont_ton7;

--rollback DROP FUNCTION master.formula_yld_cont_ton6;

--rollback DROP FUNCTION master.formula_mat_cont2;

--rollback DROP FUNCTION master.formula_mf_harv_cont;

--rollback DROP FUNCTION master.formula_yld_cont_ton2;

--rollback DROP FUNCTION master.formula_adjayld_kg_cont3;

--rollback DROP FUNCTION master.formula_adjyld3_cont;

--rollback DROP FUNCTION master.formula_yld_0_cont2;

--rollback DROP FUNCTION master.formula_disease_index;

--rollback DROP FUNCTION master.formula_yld_cont_ton3;

--rollback DROP FUNCTION master.formula_yld_cont_ton5;

--rollback DROP FUNCTION master.formula_yld_cont2;

--rollback DROP FUNCTION master.formula_total_plant_tested;

--rollback DROP FUNCTION master.formula_area_fact;

--rollback DROP FUNCTION master.formula_mat_cont;

--rollback DROP FUNCTION master.formula_adjayld_kg_cont;


-- delete formula parameters
--rollback DELETE FROM master.formula_parameter WHERE id = '1';

--rollback DELETE FROM master.formula_parameter WHERE id = '2';

--rollback DELETE FROM master.formula_parameter WHERE id = '15';

--rollback DELETE FROM master.formula_parameter WHERE id = '16';

--rollback DELETE FROM master.formula_parameter WHERE id = '24';

--rollback DELETE FROM master.formula_parameter WHERE id = '25';

--rollback DELETE FROM master.formula_parameter WHERE id = '26';

--rollback DELETE FROM master.formula_parameter WHERE id = '34';

--rollback DELETE FROM master.formula_parameter WHERE id = '35';

--rollback DELETE FROM master.formula_parameter WHERE id = '40';

--rollback DELETE FROM master.formula_parameter WHERE id = '41';

--rollback DELETE FROM master.formula_parameter WHERE id = '52';

--rollback DELETE FROM master.formula_parameter WHERE id = '72';

--rollback DELETE FROM master.formula_parameter WHERE id = '73';

--rollback DELETE FROM master.formula_parameter WHERE id = '74';

--rollback DELETE FROM master.formula_parameter WHERE id = '75';

--rollback DELETE FROM master.formula_parameter WHERE id = '76';

--rollback DELETE FROM master.formula_parameter WHERE id = '77';

--rollback DELETE FROM master.formula_parameter WHERE id = '78';

--rollback DELETE FROM master.formula_parameter WHERE id = '79';

--rollback DELETE FROM master.formula_parameter WHERE id = '272';

--rollback DELETE FROM master.formula_parameter WHERE id = '273';

--rollback DELETE FROM master.formula_parameter WHERE id = '283';

--rollback DELETE FROM master.formula_parameter WHERE id = '284';

--rollback DELETE FROM master.formula_parameter WHERE id = '285';

--rollback DELETE FROM master.formula_parameter WHERE id = '286';

--rollback DELETE FROM master.formula_parameter WHERE id = '399';

--rollback DELETE FROM master.formula_parameter WHERE id = '437';

--rollback DELETE FROM master.formula_parameter WHERE id = '451';

--rollback DELETE FROM master.formula_parameter WHERE id = '452';

--rollback DELETE FROM master.formula_parameter WHERE id = '465';

--rollback DELETE FROM master.formula_parameter WHERE id = '466';

--rollback DELETE FROM master.formula_parameter WHERE id = '545';

--rollback DELETE FROM master.formula_parameter WHERE id = '563';

--rollback DELETE FROM master.formula_parameter WHERE id = '564';

--rollback DELETE FROM master.formula_parameter WHERE id = '605';

--rollback DELETE FROM master.formula_parameter WHERE id = '606';

--rollback DELETE FROM master.formula_parameter WHERE id = '607';

--rollback DELETE FROM master.formula_parameter WHERE id = '608';

--rollback DELETE FROM master.formula_parameter WHERE id = '609';

--rollback DELETE FROM master.formula_parameter WHERE id = '610';

--rollback DELETE FROM master.formula_parameter WHERE id = '611';

--rollback DELETE FROM master.formula_parameter WHERE id = '612';

--rollback DELETE FROM master.formula_parameter WHERE id = '613';

--rollback DELETE FROM master.formula_parameter WHERE id = '614';

--rollback DELETE FROM master.formula_parameter WHERE id = '615';

--rollback DELETE FROM master.formula_parameter WHERE id = '616';

--rollback DELETE FROM master.formula_parameter WHERE id = '617';

--rollback DELETE FROM master.formula_parameter WHERE id = '618';

--rollback DELETE FROM master.formula_parameter WHERE id = '619';

--rollback DELETE FROM master.formula_parameter WHERE id = '645';

--rollback DELETE FROM master.formula_parameter WHERE id = '646';

--rollback DELETE FROM master.formula_parameter WHERE id = '647';

--rollback DELETE FROM master.formula_parameter WHERE id = '648';

--rollback DELETE FROM master.formula_parameter WHERE id = '649';

--rollback DELETE FROM master.formula_parameter WHERE id = '650';

--rollback DELETE FROM master.formula_parameter WHERE id = '651';

--rollback DELETE FROM master.formula_parameter WHERE id = '652';

--rollback DELETE FROM master.formula_parameter WHERE id = '653';

--rollback DELETE FROM master.formula_parameter WHERE id = '654';

--rollback DELETE FROM master.formula_parameter WHERE id = '679';

--rollback DELETE FROM master.formula_parameter WHERE id = '680';

--rollback DELETE FROM master.formula_parameter WHERE id = '681';

--rollback DELETE FROM master.formula_parameter WHERE id = '682';

--rollback DELETE FROM master.formula_parameter WHERE id = '683';

--rollback DELETE FROM master.formula_parameter WHERE id = '684';

--rollback DELETE FROM master.formula_parameter WHERE id = '685';

--rollback DELETE FROM master.formula_parameter WHERE id = '686';

--rollback DELETE FROM master.formula_parameter WHERE id = '687';

--rollback DELETE FROM master.formula_parameter WHERE id = '688';

--rollback DELETE FROM master.formula_parameter WHERE id = '689';

--rollback DELETE FROM master.formula_parameter WHERE id = '690';

--rollback DELETE FROM master.formula_parameter WHERE id = '695';

--rollback DELETE FROM master.formula_parameter WHERE id = '696';

--rollback DELETE FROM master.formula_parameter WHERE id = '697';

--rollback DELETE FROM master.formula_parameter WHERE id = '698';

--rollback DELETE FROM master.formula_parameter WHERE id = '701';

--rollback DELETE FROM master.formula_parameter WHERE id = '702';

--rollback DELETE FROM master.formula_parameter WHERE id = '703';

--rollback DELETE FROM master.formula_parameter WHERE id = '704';

--rollback DELETE FROM master.formula_parameter WHERE id = '705';

--rollback DELETE FROM master.formula_parameter WHERE id = '706';

--rollback DELETE FROM master.formula_parameter WHERE id = '707';

--rollback DELETE FROM master.formula_parameter WHERE id = '708';

--rollback DELETE FROM master.formula_parameter WHERE id = '709';

--rollback DELETE FROM master.formula_parameter WHERE id = '710';

--rollback DELETE FROM master.formula_parameter WHERE id = '717';

--rollback DELETE FROM master.formula_parameter WHERE id = '718';

--rollback DELETE FROM master.formula_parameter WHERE id = '719';

--rollback DELETE FROM master.formula_parameter WHERE id = '720';

--rollback DELETE FROM master.formula_parameter WHERE id = '721';

--rollback DELETE FROM master.formula_parameter WHERE id = '722';

--rollback DELETE FROM master.formula_parameter WHERE id = '723';

--rollback DELETE FROM master.formula_parameter WHERE id = '724';

--rollback DELETE FROM master.formula_parameter WHERE id = '725';

--rollback DELETE FROM master.formula_parameter WHERE id = '726';

--rollback DELETE FROM master.formula_parameter WHERE id = '727';

--rollback DELETE FROM master.formula_parameter WHERE id = '728';

--rollback DELETE FROM master.formula_parameter WHERE id = '729';

--rollback DELETE FROM master.formula_parameter WHERE id = '732';

--rollback DELETE FROM master.formula_parameter WHERE id = '733';

--rollback DELETE FROM master.formula_parameter WHERE id = '734';

--rollback DELETE FROM master.formula_parameter WHERE id = '735';

--rollback DELETE FROM master.formula_parameter WHERE id = '736';

--rollback DELETE FROM master.formula_parameter WHERE id = '737';

--rollback DELETE FROM master.formula_parameter WHERE id = '738';

--rollback DELETE FROM master.formula_parameter WHERE id = '739';

--rollback DELETE FROM master.formula_parameter WHERE id = '740';

--rollback DELETE FROM master.formula_parameter WHERE id = '741';

--rollback DELETE FROM master.formula_parameter WHERE id = '742';

--rollback DELETE FROM master.formula_parameter WHERE id = '743';

--rollback DELETE FROM master.formula_parameter WHERE id = '744';

--rollback DELETE FROM master.formula_parameter WHERE id = '745';

--rollback DELETE FROM master.formula_parameter WHERE id = '746';

--rollback DELETE FROM master.formula_parameter WHERE id = '747';

--rollback DELETE FROM master.formula_parameter WHERE id = '748';

--rollback DELETE FROM master.formula_parameter WHERE id = '749';

--rollback DELETE FROM master.formula_parameter WHERE id = '750';

--rollback DELETE FROM master.formula_parameter WHERE id = '751';

--rollback DELETE FROM master.formula_parameter WHERE id = '752';

--rollback DELETE FROM master.formula_parameter WHERE id = '753';

--rollback DELETE FROM master.formula_parameter WHERE id = '754';

--rollback DELETE FROM master.formula_parameter WHERE id = '755';

--rollback DELETE FROM master.formula_parameter WHERE id = '756';

--rollback DELETE FROM master.formula_parameter WHERE id = '757';

--rollback DELETE FROM master.formula_parameter WHERE id = '758';

--rollback DELETE FROM master.formula_parameter WHERE id = '759';

--rollback DELETE FROM master.formula_parameter WHERE id = '760';

--rollback DELETE FROM master.formula_parameter WHERE id = '761';

--rollback DELETE FROM master.formula_parameter WHERE id = '762';

--rollback DELETE FROM master.formula_parameter WHERE id = '763';

--rollback DELETE FROM master.formula_parameter WHERE id = '764';

--rollback DELETE FROM master.formula_parameter WHERE id = '765';

--rollback DELETE FROM master.formula_parameter WHERE id = '766';

--rollback DELETE FROM master.formula_parameter WHERE id = '767';

--rollback DELETE FROM master.formula_parameter WHERE id = '768';

--rollback DELETE FROM master.formula_parameter WHERE id = '769';

--rollback DELETE FROM master.formula_parameter WHERE id = '770';

--rollback DELETE FROM master.formula_parameter WHERE id = '771';

--rollback DELETE FROM master.formula_parameter WHERE id = '772';

--rollback DELETE FROM master.formula_parameter WHERE id = '773';

--rollback DELETE FROM master.formula_parameter WHERE id = '774';

--rollback DELETE FROM master.formula_parameter WHERE id = '775';

--rollback DELETE FROM master.formula_parameter WHERE id = '776';

--rollback DELETE FROM master.formula_parameter WHERE id = '777';

--rollback DELETE FROM master.formula_parameter WHERE id = '778';

--rollback DELETE FROM master.formula_parameter WHERE id = '779';

--rollback DELETE FROM master.formula_parameter WHERE id = '780';

--rollback DELETE FROM master.formula_parameter WHERE id = '794';

--rollback DELETE FROM master.formula_parameter WHERE id = '798';

--rollback DELETE FROM master.formula_parameter WHERE id = '799';

--rollback DELETE FROM master.formula_parameter WHERE id = '800';

--rollback DELETE FROM master.formula_parameter WHERE id = '863';

--rollback DELETE FROM master.formula_parameter WHERE id = '864';


-- delete formulas
--rollback DELETE FROM master.formula WHERE id = '1';

--rollback DELETE FROM master.formula WHERE id = '2';

--rollback DELETE FROM master.formula WHERE id = '3';

--rollback DELETE FROM master.formula WHERE id = '4';

--rollback DELETE FROM master.formula WHERE id = '5';

--rollback DELETE FROM master.formula WHERE id = '7';

--rollback DELETE FROM master.formula WHERE id = '11';

--rollback DELETE FROM master.formula WHERE id = '14';

--rollback DELETE FROM master.formula WHERE id = '16';

--rollback DELETE FROM master.formula WHERE id = '17';

--rollback DELETE FROM master.formula WHERE id = '21';

--rollback DELETE FROM master.formula WHERE id = '22';

--rollback DELETE FROM master.formula WHERE id = '27';

--rollback DELETE FROM master.formula WHERE id = '29';

--rollback DELETE FROM master.formula WHERE id = '30';

--rollback DELETE FROM master.formula WHERE id = '31';

--rollback DELETE FROM master.formula WHERE id = '32';

--rollback DELETE FROM master.formula WHERE id = '33';

--rollback DELETE FROM master.formula WHERE id = '34';

--rollback DELETE FROM master.formula WHERE id = '35';

--rollback DELETE FROM master.formula WHERE id = '36';

--rollback DELETE FROM master.formula WHERE id = '37';

--rollback DELETE FROM master.formula WHERE id = '44';

--rollback DELETE FROM master.formula WHERE id = '47';

--rollback DELETE FROM master.formula WHERE id = '52';

--rollback DELETE FROM master.formula WHERE id = '54';

--rollback DELETE FROM master.formula WHERE id = '55';

--rollback DELETE FROM master.formula WHERE id = '56';

--rollback DELETE FROM master.formula WHERE id = '57';

--rollback DELETE FROM master.formula WHERE id = '59';

--rollback DELETE FROM master.formula WHERE id = '88';

--rollback DELETE FROM master.formula WHERE id = '91';

--rollback DELETE FROM master.formula WHERE id = '92';

--rollback DELETE FROM master.formula WHERE id = '93';

--rollback DELETE FROM master.formula WHERE id = '94';

--rollback DELETE FROM master.formula WHERE id = '95';

--rollback DELETE FROM master.formula WHERE id = '96';

--rollback DELETE FROM master.formula WHERE id = '98';

--rollback DELETE FROM master.formula WHERE id = '105';
