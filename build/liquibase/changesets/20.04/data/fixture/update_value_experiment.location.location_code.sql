--liquibase formatted sql

--changeset postgres:update_value_experiment.location.location_code context:fixture splitStatements:FALSE

UPDATE experiment.location AS loc
SET location_code = 'LOC0001',
    location_name = 'Location #1'
WHERE loc.id = 71
    AND (loc.location_code <> 'LOC0001'
    OR loc.location_name <> 'Location #1');

UPDATE experiment.location AS loc
SET location_code = 'LOC0002',
    location_name = 'Location #2'
WHERE loc.id = 72
    AND (loc.location_code <> 'LOC0002'
    OR loc.location_name <> 'Location #2');

UPDATE experiment.location AS loc
SET location_code = 'LOC0003',
    location_name = 'Location #3'
WHERE loc.id = 73
    AND (loc.location_code <> 'LOC0003'
    OR loc.location_name <> 'Location #3');

UPDATE experiment.location AS loc
SET location_code = 'LOC0004',
    location_name = 'Location #4'
WHERE loc.id = 74
    AND (loc.location_code <> 'LOC0004'
    OR loc.location_name <> 'Location #4');

UPDATE experiment.location AS loc
SET location_code = 'LOC0005',
    location_name = 'Location #5'
WHERE loc.id = 815
    AND (loc.location_code <> 'LOC0005'
    OR loc.location_name <> 'Location #5');

UPDATE experiment.location AS loc
SET location_code = 'LOC0006',
    location_name = 'Location #6'
WHERE loc.id = 706
    AND (loc.location_code <> 'LOC0006'
    OR loc.location_name <> 'Location #6');

UPDATE experiment.location AS loc
SET location_code = 'LOC0007',
    location_name = 'Location #7'
WHERE loc.id = 720
    AND (loc.location_code <> 'LOC0007'
    OR loc.location_name <> 'Location #7');

UPDATE experiment.location AS loc
SET location_code = 'LOC0008',
    location_name = 'Location #8'
WHERE loc.id = 340
    AND (loc.location_code <> 'LOC0008'
    OR loc.location_name <> 'Location #8');

UPDATE experiment.location AS loc
SET location_code = 'LOC0009',
    location_name = 'Location #9'
WHERE loc.id = 170
    AND (loc.location_code <> 'LOC0009'
    OR loc.location_name <> 'Location #9');

UPDATE experiment.location AS loc
SET location_code = 'LOC0010',
    location_name = 'Location #10'
WHERE loc.id = 164
    AND (loc.location_code <> 'LOC0010'
    OR loc.location_name <> 'Location #10');

UPDATE experiment.location AS loc
SET location_code = 'LOC0011',
    location_name = 'Location #11'
WHERE loc.id = 816
    AND (loc.location_code <> 'LOC0011'
    OR loc.location_name <> 'Location #11');

UPDATE experiment.location AS loc
SET location_code = 'LOC0012',
    location_name = 'Location #12'
WHERE loc.id = 323
    AND (loc.location_code <> 'LOC0012'
    OR loc.location_name <> 'Location #12');

UPDATE experiment.location AS loc
SET location_code = 'LOC0013',
    location_name = 'Location #13'
WHERE loc.id = 228
    AND (loc.location_code <> 'LOC0013'
    OR loc.location_name <> 'Location #13');

UPDATE experiment.location AS loc
SET location_code = 'LOC0014',
    location_name = 'Location #14'
WHERE loc.id = 205
    AND (loc.location_code <> 'LOC0014'
    OR loc.location_name <> 'Location #14');

UPDATE experiment.location AS loc
SET location_code = 'LOC0015',
    location_name = 'Location #15'
WHERE loc.id = 206
    AND (loc.location_code <> 'LOC0015'
    OR loc.location_name <> 'Location #15');

UPDATE experiment.location AS loc
SET location_code = 'LOC0016',
    location_name = 'Location #16'
WHERE loc.id = 737
    AND (loc.location_code <> 'LOC0016'
    OR loc.location_name <> 'Location #16');

UPDATE experiment.location AS loc
SET location_code = 'LOC0017',
    location_name = 'Location #17'
WHERE loc.id = 231
    AND (loc.location_code <> 'LOC0017'
    OR loc.location_name <> 'Location #17');

UPDATE experiment.location AS loc
SET location_code = 'LOC0018',
    location_name = 'Location #18'
WHERE loc.id = 235
    AND (loc.location_code <> 'LOC0018'
    OR loc.location_name <> 'Location #18');

UPDATE experiment.location AS loc
SET location_code = 'LOC0019',
    location_name = 'Location #19'
WHERE loc.id = 230
    AND (loc.location_code <> 'LOC0019'
    OR loc.location_name <> 'Location #19');

UPDATE experiment.location AS loc
SET location_code = 'LOC0020',
    location_name = 'Location #20'
WHERE loc.id = 233
    AND (loc.location_code <> 'LOC0020'
    OR loc.location_name <> 'Location #20');

UPDATE experiment.location AS loc
SET location_code = 'LOC0021',
    location_name = 'Location #21'
WHERE loc.id = 232
    AND (loc.location_code <> 'LOC0021'
    OR loc.location_name <> 'Location #21');

UPDATE experiment.location AS loc
SET location_code = 'LOC0022',
    location_name = 'Location #22'
WHERE loc.id = 236
    AND (loc.location_code <> 'LOC0022'
    OR loc.location_name <> 'Location #22');

UPDATE experiment.location AS loc
SET location_code = 'LOC0023',
    location_name = 'Location #23'
WHERE loc.id = 234
    AND (loc.location_code <> 'LOC0023'
    OR loc.location_name <> 'Location #23');

UPDATE experiment.location AS loc
SET location_code = 'LOC0024',
    location_name = 'Location #24'
WHERE loc.id = 821
    AND (loc.location_code <> 'LOC0024'
    OR loc.location_name <> 'Location #24');

UPDATE experiment.location AS loc
SET location_code = 'LOC0025',
    location_name = 'Location #25'
WHERE loc.id = 700
    AND (loc.location_code <> 'LOC0025'
    OR loc.location_name <> 'Location #25');

UPDATE experiment.location AS loc
SET location_code = 'LOC0026',
    location_name = 'Location #26'
WHERE loc.id = 705
    AND (loc.location_code <> 'LOC0026'
    OR loc.location_name <> 'Location #26');

UPDATE experiment.location AS loc
SET location_code = 'LOC0027',
    location_name = 'Location #27'
WHERE loc.id = 721
    AND (loc.location_code <> 'LOC0027'
    OR loc.location_name <> 'Location #27');

UPDATE experiment.location AS loc
SET location_code = 'LOC0028',
    location_name = 'Location #28'
WHERE loc.id = 385
    AND (loc.location_code <> 'LOC0028'
    OR loc.location_name <> 'Location #28');

UPDATE experiment.location AS loc
SET location_code = 'LOC0029',
    location_name = 'Location #29'
WHERE loc.id = 1572
    AND (loc.location_code <> 'LOC0029'
    OR loc.location_name <> 'Location #29');

UPDATE experiment.location AS loc
SET location_code = 'LOC0030',
    location_name = 'Location #30'
WHERE loc.id = 378
    AND (loc.location_code <> 'LOC0030'
    OR loc.location_name <> 'Location #30');

UPDATE experiment.location AS loc
SET location_code = 'LOC0031',
    location_name = 'Location #31'
WHERE loc.id = 359
    AND (loc.location_code <> 'LOC0031'
    OR loc.location_name <> 'Location #31');

UPDATE experiment.location AS loc
SET location_code = 'LOC0032',
    location_name = 'Location #32'
WHERE loc.id = 473
    AND (loc.location_code <> 'LOC0032'
    OR loc.location_name <> 'Location #32');

UPDATE experiment.location AS loc
SET location_code = 'LOC0033',
    location_name = 'Location #33'
WHERE loc.id = 716
    AND (loc.location_code <> 'LOC0033'
    OR loc.location_name <> 'Location #33');

UPDATE experiment.location AS loc
SET location_code = 'LOC0034',
    location_name = 'Location #34'
WHERE loc.id = 715
    AND (loc.location_code <> 'LOC0034'
    OR loc.location_name <> 'Location #34');

UPDATE experiment.location AS loc
SET location_code = 'LOC0035',
    location_name = 'Location #35'
WHERE loc.id = 836
    AND (loc.location_code <> 'LOC0035'
    OR loc.location_name <> 'Location #35');

UPDATE experiment.location AS loc
SET location_code = 'LOC0036',
    location_name = 'Location #36'
WHERE loc.id = 804
    AND (loc.location_code <> 'LOC0036'
    OR loc.location_name <> 'Location #36');

UPDATE experiment.location AS loc
SET location_code = 'LOC0037',
    location_name = 'Location #37'
WHERE loc.id = 375
    AND (loc.location_code <> 'LOC0037'
    OR loc.location_name <> 'Location #37');

UPDATE experiment.location AS loc
SET location_code = 'LOC0038',
    location_name = 'Location #38'
WHERE loc.id = 1491
    AND (loc.location_code <> 'LOC0038'
    OR loc.location_name <> 'Location #38');

UPDATE experiment.location AS loc
SET location_code = 'LOC0039',
    location_name = 'Location #39'
WHERE loc.id = 1504
    AND (loc.location_code <> 'LOC0039'
    OR loc.location_name <> 'Location #39');

UPDATE experiment.location AS loc
SET location_code = 'LOC0040',
    location_name = 'Location #40'
WHERE loc.id = 1508
    AND (loc.location_code <> 'LOC0040'
    OR loc.location_name <> 'Location #40');

UPDATE experiment.location AS loc
SET location_code = 'LOC0041',
    location_name = 'Location #41'
WHERE loc.id = 1507
    AND (loc.location_code <> 'LOC0041'
    OR loc.location_name <> 'Location #41');

UPDATE experiment.location AS loc
SET location_code = 'LOC0042',
    location_name = 'Location #42'
WHERE loc.id = 826
    AND (loc.location_code <> 'LOC0042'
    OR loc.location_name <> 'Location #42');

UPDATE experiment.location AS loc
SET location_code = 'LOC0043',
    location_name = 'Location #43'
WHERE loc.id = 390
    AND (loc.location_code <> 'LOC0043'
    OR loc.location_name <> 'Location #43');

UPDATE experiment.location AS loc
SET location_code = 'LOC0044',
    location_name = 'Location #44'
WHERE loc.id = 698
    AND (loc.location_code <> 'LOC0044'
    OR loc.location_name <> 'Location #44');

UPDATE experiment.location AS loc
SET location_code = 'LOC0045',
    location_name = 'Location #45'
WHERE loc.id = 421
    AND (loc.location_code <> 'LOC0045'
    OR loc.location_name <> 'Location #45');

UPDATE experiment.location AS loc
SET location_code = 'LOC0046',
    location_name = 'Location #46'
WHERE loc.id = 714
    AND (loc.location_code <> 'LOC0046'
    OR loc.location_name <> 'Location #46');

UPDATE experiment.location AS loc
SET location_code = 'LOC0047',
    location_name = 'Location #47'
WHERE loc.id = 843
    AND (loc.location_code <> 'LOC0047'
    OR loc.location_name <> 'Location #47');

UPDATE experiment.location AS loc
SET location_code = 'LOC0048',
    location_name = 'Location #48'
WHERE loc.id = 511
    AND (loc.location_code <> 'LOC0048'
    OR loc.location_name <> 'Location #48');

UPDATE experiment.location AS loc
SET location_code = 'LOC0049',
    location_name = 'Location #49'
WHERE loc.id = 463
    AND (loc.location_code <> 'LOC0049'
    OR loc.location_name <> 'Location #49');

UPDATE experiment.location AS loc
SET location_code = 'LOC0050',
    location_name = 'Location #50'
WHERE loc.id = 557
    AND (loc.location_code <> 'LOC0050'
    OR loc.location_name <> 'Location #50');

UPDATE experiment.location AS loc
SET location_code = 'LOC0051',
    location_name = 'Location #51'
WHERE loc.id = 524
    AND (loc.location_code <> 'LOC0051'
    OR loc.location_name <> 'Location #51');

UPDATE experiment.location AS loc
SET location_code = 'LOC0052',
    location_name = 'Location #52'
WHERE loc.id = 1389
    AND (loc.location_code <> 'LOC0052'
    OR loc.location_name <> 'Location #52');

UPDATE experiment.location AS loc
SET location_code = 'LOC0053',
    location_name = 'Location #53'
WHERE loc.id = 1131
    AND (loc.location_code <> 'LOC0053'
    OR loc.location_name <> 'Location #53');

UPDATE experiment.location AS loc
SET location_code = 'LOC0054',
    location_name = 'Location #54'
WHERE loc.id = 1129
    AND (loc.location_code <> 'LOC0054'
    OR loc.location_name <> 'Location #54');

UPDATE experiment.location AS loc
SET location_code = 'LOC0055',
    location_name = 'Location #55'
WHERE loc.id = 1017
    AND (loc.location_code <> 'LOC0055'
    OR loc.location_name <> 'Location #55');

UPDATE experiment.location AS loc
SET location_code = 'LOC0056',
    location_name = 'Location #56'
WHERE loc.id = 1397
    AND (loc.location_code <> 'LOC0056'
    OR loc.location_name <> 'Location #56');

UPDATE experiment.location AS loc
SET location_code = 'LOC0057',
    location_name = 'Location #57'
WHERE loc.id = 1367
    AND (loc.location_code <> 'LOC0057'
    OR loc.location_name <> 'Location #57');

UPDATE experiment.location AS loc
SET location_code = 'LOC0058',
    location_name = 'Location #58'
WHERE loc.id = 1384
    AND (loc.location_code <> 'LOC0058'
    OR loc.location_name <> 'Location #58');

UPDATE experiment.location AS loc
SET location_code = 'LOC0059',
    location_name = 'Location #59'
WHERE loc.id = 1028
    AND (loc.location_code <> 'LOC0059'
    OR loc.location_name <> 'Location #59');

UPDATE experiment.location AS loc
SET location_code = 'LOC0060',
    location_name = 'Location #60'
WHERE loc.id = 1396
    AND (loc.location_code <> 'LOC0060'
    OR loc.location_name <> 'Location #60');

UPDATE experiment.location AS loc
SET location_code = 'LOC0061',
    location_name = 'Location #61'
WHERE loc.id = 1099
    AND (loc.location_code <> 'LOC0061'
    OR loc.location_name <> 'Location #61');

UPDATE experiment.location AS loc
SET location_code = 'LOC0062',
    location_name = 'Location #62'
WHERE loc.id = 1191
    AND (loc.location_code <> 'LOC0062'
    OR loc.location_name <> 'Location #62');

UPDATE experiment.location AS loc
SET location_code = 'LOC0063',
    location_name = 'Location #63'
WHERE loc.id = 1193
    AND (loc.location_code <> 'LOC0063'
    OR loc.location_name <> 'Location #63');

UPDATE experiment.location AS loc
SET location_code = 'LOC0064',
    location_name = 'Location #64'
WHERE loc.id = 1217
    AND (loc.location_code <> 'LOC0064'
    OR loc.location_name <> 'Location #64');

UPDATE experiment.location AS loc
SET location_code = 'LOC0065',
    location_name = 'Location #65'
WHERE loc.id = 1100
    AND (loc.location_code <> 'LOC0065'
    OR loc.location_name <> 'Location #65');

UPDATE experiment.location AS loc
SET location_code = 'LOC0066',
    location_name = 'Location #66'
WHERE loc.id = 1200
    AND (loc.location_code <> 'LOC0066'
    OR loc.location_name <> 'Location #66');

UPDATE experiment.location AS loc
SET location_code = 'LOC0067',
    location_name = 'Location #67'
WHERE loc.id = 1201
    AND (loc.location_code <> 'LOC0067'
    OR loc.location_name <> 'Location #67');

UPDATE experiment.location AS loc
SET location_code = 'LOC0068',
    location_name = 'Location #68'
WHERE loc.id = 1218
    AND (loc.location_code <> 'LOC0068'
    OR loc.location_name <> 'Location #68');

UPDATE experiment.location AS loc
SET location_code = 'LOC0069',
    location_name = 'Location #69'
WHERE loc.id = 1408
    AND (loc.location_code <> 'LOC0069'
    OR loc.location_name <> 'Location #69');

UPDATE experiment.location AS loc
SET location_code = 'LOC0070',
    location_name = 'Location #70'
WHERE loc.id = 1414
    AND (loc.location_code <> 'LOC0070'
    OR loc.location_name <> 'Location #70');

UPDATE experiment.location AS loc
SET location_code = 'LOC0071',
    location_name = 'Location #71'
WHERE loc.id = 1413
    AND (loc.location_code <> 'LOC0071'
    OR loc.location_name <> 'Location #71');

UPDATE experiment.location AS loc
SET location_code = 'LOC0072',
    location_name = 'Location #72'
WHERE loc.id = 1527
    AND (loc.location_code <> 'LOC0072'
    OR loc.location_name <> 'Location #72');

UPDATE experiment.location AS loc
SET location_code = 'LOC0073',
    location_name = 'Location #73'
WHERE loc.id = 916
    AND (loc.location_code <> 'LOC0073'
    OR loc.location_name <> 'Location #73');

UPDATE experiment.location AS loc
SET location_code = 'LOC0074',
    location_name = 'Location #74'
WHERE loc.id = 895
    AND (loc.location_code <> 'LOC0074'
    OR loc.location_name <> 'Location #74');

UPDATE experiment.location AS loc
SET location_code = 'LOC0075',
    location_name = 'Location #75'
WHERE loc.id = 885
    AND (loc.location_code <> 'LOC0075'
    OR loc.location_name <> 'Location #75');

UPDATE experiment.location AS loc
SET location_code = 'LOC0076',
    location_name = 'Location #76'
WHERE loc.id = 894
    AND (loc.location_code <> 'LOC0076'
    OR loc.location_name <> 'Location #76');

UPDATE experiment.location AS loc
SET location_code = 'LOC0077',
    location_name = 'Location #77'
WHERE loc.id = 1540
    AND (loc.location_code <> 'LOC0077'
    OR loc.location_name <> 'Location #77');

UPDATE experiment.location AS loc
SET location_code = 'LOC0078',
    location_name = 'Location #78'
WHERE loc.id = 2163
    AND (loc.location_code <> 'LOC0078'
    OR loc.location_name <> 'Location #78');

UPDATE experiment.location AS loc
SET location_code = 'LOC0079',
    location_name = 'Location #79'
WHERE loc.id = 893
    AND (loc.location_code <> 'LOC0079'
    OR loc.location_name <> 'Location #79');

UPDATE experiment.location AS loc
SET location_code = 'LOC0080',
    location_name = 'Location #80'
WHERE loc.id = 1004
    AND (loc.location_code <> 'LOC0080'
    OR loc.location_name <> 'Location #80');

UPDATE experiment.location AS loc
SET location_code = 'LOC0081',
    location_name = 'Location #81'
WHERE loc.id = 1003
    AND (loc.location_code <> 'LOC0081'
    OR loc.location_name <> 'Location #81');

UPDATE experiment.location AS loc
SET location_code = 'LOC0082',
    location_name = 'Location #82'
WHERE loc.id = 1015
    AND (loc.location_code <> 'LOC0082'
    OR loc.location_name <> 'Location #82');

UPDATE experiment.location AS loc
SET location_code = 'LOC0083',
    location_name = 'Location #83'
WHERE loc.id = 919
    AND (loc.location_code <> 'LOC0083'
    OR loc.location_name <> 'Location #83');

UPDATE experiment.location AS loc
SET location_code = 'LOC0084',
    location_name = 'Location #84'
WHERE loc.id = 1037
    AND (loc.location_code <> 'LOC0084'
    OR loc.location_name <> 'Location #84');

UPDATE experiment.location AS loc
SET location_code = 'LOC0085',
    location_name = 'Location #85'
WHERE loc.id = 1047
    AND (loc.location_code <> 'LOC0085'
    OR loc.location_name <> 'Location #85');

UPDATE experiment.location AS loc
SET location_code = 'LOC0086',
    location_name = 'Location #86'
WHERE loc.id = 1377
    AND (loc.location_code <> 'LOC0086'
    OR loc.location_name <> 'Location #86');

UPDATE experiment.location AS loc
SET location_code = 'LOC0087',
    location_name = 'Location #87'
WHERE loc.id = 1382
    AND (loc.location_code <> 'LOC0087'
    OR loc.location_name <> 'Location #87');

UPDATE experiment.location AS loc
SET location_code = 'LOC0088',
    location_name = 'Location #88'
WHERE loc.id = 1378
    AND (loc.location_code <> 'LOC0088'
    OR loc.location_name <> 'Location #88');

UPDATE experiment.location AS loc
SET location_code = 'LOC0089',
    location_name = 'Location #89'
WHERE loc.id = 1380
    AND (loc.location_code <> 'LOC0089'
    OR loc.location_name <> 'Location #89');

UPDATE experiment.location AS loc
SET location_code = 'LOC0090',
    location_name = 'Location #90'
WHERE loc.id = 1383
    AND (loc.location_code <> 'LOC0090'
    OR loc.location_name <> 'Location #90');

UPDATE experiment.location AS loc
SET location_code = 'LOC0091',
    location_name = 'Location #91'
WHERE loc.id = 1381
    AND (loc.location_code <> 'LOC0091'
    OR loc.location_name <> 'Location #91');

UPDATE experiment.location AS loc
SET location_code = 'LOC0092',
    location_name = 'Location #92'
WHERE loc.id = 1016
    AND (loc.location_code <> 'LOC0092'
    OR loc.location_name <> 'Location #92');

UPDATE experiment.location AS loc
SET location_code = 'LOC0093',
    location_name = 'Location #93'
WHERE loc.id = 1105
    AND (loc.location_code <> 'LOC0093'
    OR loc.location_name <> 'Location #93');

UPDATE experiment.location AS loc
SET location_code = 'LOC0094',
    location_name = 'Location #94'
WHERE loc.id = 1110
    AND (loc.location_code <> 'LOC0094'
    OR loc.location_name <> 'Location #94');

UPDATE experiment.location AS loc
SET location_code = 'LOC0095',
    location_name = 'Location #95'
WHERE loc.id = 1411
    AND (loc.location_code <> 'LOC0095'
    OR loc.location_name <> 'Location #95');

UPDATE experiment.location AS loc
SET location_code = 'LOC0096',
    location_name = 'Location #96'
WHERE loc.id = 1412
    AND (loc.location_code <> 'LOC0096'
    OR loc.location_name <> 'Location #96');

UPDATE experiment.location AS loc
SET location_code = 'LOC0097',
    location_name = 'Location #97'
WHERE loc.id = 1073
    AND (loc.location_code <> 'LOC0097'
    OR loc.location_name <> 'Location #97');

UPDATE experiment.location AS loc
SET location_code = 'LOC0098',
    location_name = 'Location #98'
WHERE loc.id = 1085
    AND (loc.location_code <> 'LOC0098'
    OR loc.location_name <> 'Location #98');

UPDATE experiment.location AS loc
SET location_code = 'LOC0099',
    location_name = 'Location #99'
WHERE loc.id = 1036
    AND (loc.location_code <> 'LOC0099'
    OR loc.location_name <> 'Location #99');

UPDATE experiment.location AS loc
SET location_code = 'LOC0100',
    location_name = 'Location #100'
WHERE loc.id = 1005
    AND (loc.location_code <> 'LOC0100'
    OR loc.location_name <> 'Location #100');

UPDATE experiment.location AS loc
SET location_code = 'LOC0101',
    location_name = 'Location #101'
WHERE loc.id = 907
    AND (loc.location_code <> 'LOC0101'
    OR loc.location_name <> 'Location #101');

UPDATE experiment.location AS loc
SET location_code = 'LOC0102',
    location_name = 'Location #102'
WHERE loc.id = 1115
    AND (loc.location_code <> 'LOC0102'
    OR loc.location_name <> 'Location #102');

UPDATE experiment.location AS loc
SET location_code = 'LOC0103',
    location_name = 'Location #103'
WHERE loc.id = 1360
    AND (loc.location_code <> 'LOC0103'
    OR loc.location_name <> 'Location #103');

UPDATE experiment.location AS loc
SET location_code = 'LOC0104',
    location_name = 'Location #104'
WHERE loc.id = 1362
    AND (loc.location_code <> 'LOC0104'
    OR loc.location_name <> 'Location #104');

UPDATE experiment.location AS loc
SET location_code = 'LOC0105',
    location_name = 'Location #105'
WHERE loc.id = 1124
    AND (loc.location_code <> 'LOC0105'
    OR loc.location_name <> 'Location #105');

UPDATE experiment.location AS loc
SET location_code = 'LOC0106',
    location_name = 'Location #106'
WHERE loc.id = 1125
    AND (loc.location_code <> 'LOC0106'
    OR loc.location_name <> 'Location #106');

UPDATE experiment.location AS loc
SET location_code = 'LOC0107',
    location_name = 'Location #107'
WHERE loc.id = 1114
    AND (loc.location_code <> 'LOC0107'
    OR loc.location_name <> 'Location #107');

UPDATE experiment.location AS loc
SET location_code = 'LOC0108',
    location_name = 'Location #108'
WHERE loc.id = 903
    AND (loc.location_code <> 'LOC0108'
    OR loc.location_name <> 'Location #108');

UPDATE experiment.location AS loc
SET location_code = 'LOC0109',
    location_name = 'Location #109'
WHERE loc.id = 1358
    AND (loc.location_code <> 'LOC0109'
    OR loc.location_name <> 'Location #109');

UPDATE experiment.location AS loc
SET location_code = 'LOC0110',
    location_name = 'Location #110'
WHERE loc.id = 1361
    AND (loc.location_code <> 'LOC0110'
    OR loc.location_name <> 'Location #110');

UPDATE experiment.location AS loc
SET location_code = 'LOC0111',
    location_name = 'Location #111'
WHERE loc.id = 1405
    AND (loc.location_code <> 'LOC0111'
    OR loc.location_name <> 'Location #111');

UPDATE experiment.location AS loc
SET location_code = 'LOC0112',
    location_name = 'Location #112'
WHERE loc.id = 1061
    AND (loc.location_code <> 'LOC0112'
    OR loc.location_name <> 'Location #112');

UPDATE experiment.location AS loc
SET location_code = 'LOC0113',
    location_name = 'Location #113'
WHERE loc.id = 1314
    AND (loc.location_code <> 'LOC0113'
    OR loc.location_name <> 'Location #113');

UPDATE experiment.location AS loc
SET location_code = 'LOC0114',
    location_name = 'Location #114'
WHERE loc.id = 1313
    AND (loc.location_code <> 'LOC0114'
    OR loc.location_name <> 'Location #114');

UPDATE experiment.location AS loc
SET location_code = 'LOC0115',
    location_name = 'Location #115'
WHERE loc.id = 1489
    AND (loc.location_code <> 'LOC0115'
    OR loc.location_name <> 'Location #115');

UPDATE experiment.location AS loc
SET location_code = 'LOC0116',
    location_name = 'Location #116'
WHERE loc.id = 1793
    AND (loc.location_code <> 'LOC0116'
    OR loc.location_name <> 'Location #116');

UPDATE experiment.location AS loc
SET location_code = 'LOC0117',
    location_name = 'Location #117'
WHERE loc.id = 1792
    AND (loc.location_code <> 'LOC0117'
    OR loc.location_name <> 'Location #117');

UPDATE experiment.location AS loc
SET location_code = 'LOC0118',
    location_name = 'Location #118'
WHERE loc.id = 1697
    AND (loc.location_code <> 'LOC0118'
    OR loc.location_name <> 'Location #118');

UPDATE experiment.location AS loc
SET location_code = 'LOC0119',
    location_name = 'Location #119'
WHERE loc.id = 1799
    AND (loc.location_code <> 'LOC0119'
    OR loc.location_name <> 'Location #119');

UPDATE experiment.location AS loc
SET location_code = 'LOC0120',
    location_name = 'Location #120'
WHERE loc.id = 1520
    AND (loc.location_code <> 'LOC0120'
    OR loc.location_name <> 'Location #120');

UPDATE experiment.location AS loc
SET location_code = 'LOC0121',
    location_name = 'Location #121'
WHERE loc.id = 1771
    AND (loc.location_code <> 'LOC0121'
    OR loc.location_name <> 'Location #121');

UPDATE experiment.location AS loc
SET location_code = 'LOC0122',
    location_name = 'Location #122'
WHERE loc.id = 1786
    AND (loc.location_code <> 'LOC0122'
    OR loc.location_name <> 'Location #122');

UPDATE experiment.location AS loc
SET location_code = 'LOC0123',
    location_name = 'Location #123'
WHERE loc.id = 2179
    AND (loc.location_code <> 'LOC0123'
    OR loc.location_name <> 'Location #123');

UPDATE experiment.location AS loc
SET location_code = 'LOC0124',
    location_name = 'Location #124'
WHERE loc.id = 1766
    AND (loc.location_code <> 'LOC0124'
    OR loc.location_name <> 'Location #124');

UPDATE experiment.location AS loc
SET location_code = 'LOC0125',
    location_name = 'Location #125'
WHERE loc.id = 1982
    AND (loc.location_code <> 'LOC0125'
    OR loc.location_name <> 'Location #125');

UPDATE experiment.location AS loc
SET location_code = 'LOC0126',
    location_name = 'Location #126'
WHERE loc.id = 1989
    AND (loc.location_code <> 'LOC0126'
    OR loc.location_name <> 'Location #126');

UPDATE experiment.location AS loc
SET location_code = 'LOC0127',
    location_name = 'Location #127'
WHERE loc.id = 1991
    AND (loc.location_code <> 'LOC0127'
    OR loc.location_name <> 'Location #127');

UPDATE experiment.location AS loc
SET location_code = 'LOC0128',
    location_name = 'Location #128'
WHERE loc.id = 2178
    AND (loc.location_code <> 'LOC0128'
    OR loc.location_name <> 'Location #128');

UPDATE experiment.location AS loc
SET location_code = 'LOC0129',
    location_name = 'Location #129'
WHERE loc.id = 1765
    AND (loc.location_code <> 'LOC0129'
    OR loc.location_name <> 'Location #129');

UPDATE experiment.location AS loc
SET location_code = 'LOC0130',
    location_name = 'Location #130'
WHERE loc.id = 1981
    AND (loc.location_code <> 'LOC0130'
    OR loc.location_name <> 'Location #130');

UPDATE experiment.location AS loc
SET location_code = 'LOC0131',
    location_name = 'Location #131'
WHERE loc.id = 1988
    AND (loc.location_code <> 'LOC0131'
    OR loc.location_name <> 'Location #131');

UPDATE experiment.location AS loc
SET location_code = 'LOC0132',
    location_name = 'Location #132'
WHERE loc.id = 1990
    AND (loc.location_code <> 'LOC0132'
    OR loc.location_name <> 'Location #132');

UPDATE experiment.location AS loc
SET location_code = 'LOC0133',
    location_name = 'Location #133'
WHERE loc.id = 1788
    AND (loc.location_code <> 'LOC0133'
    OR loc.location_name <> 'Location #133');

UPDATE experiment.location AS loc
SET location_code = 'LOC0134',
    location_name = 'Location #134'
WHERE loc.id = 1789
    AND (loc.location_code <> 'LOC0134'
    OR loc.location_name <> 'Location #134');

UPDATE experiment.location AS loc
SET location_code = 'LOC0135',
    location_name = 'Location #135'
WHERE loc.id = 1769
    AND (loc.location_code <> 'LOC0135'
    OR loc.location_name <> 'Location #135');

UPDATE experiment.location AS loc
SET location_code = 'LOC0136',
    location_name = 'Location #136'
WHERE loc.id = 1785
    AND (loc.location_code <> 'LOC0136'
    OR loc.location_name <> 'Location #136');

UPDATE experiment.location AS loc
SET location_code = 'LOC0137',
    location_name = 'Location #137'
WHERE loc.id = 1889
    AND (loc.location_code <> 'LOC0137'
    OR loc.location_name <> 'Location #137');

UPDATE experiment.location AS loc
SET location_code = 'LOC0138',
    location_name = 'Location #138'
WHERE loc.id = 1567
    AND (loc.location_code <> 'LOC0138'
    OR loc.location_name <> 'Location #138');

UPDATE experiment.location AS loc
SET location_code = 'LOC0139',
    location_name = 'Location #139'
WHERE loc.id = 1521
    AND (loc.location_code <> 'LOC0139'
    OR loc.location_name <> 'Location #139');

UPDATE experiment.location AS loc
SET location_code = 'LOC0140',
    location_name = 'Location #140'
WHERE loc.id = 2059
    AND (loc.location_code <> 'LOC0140'
    OR loc.location_name <> 'Location #140');

UPDATE experiment.location AS loc
SET location_code = 'LOC0141',
    location_name = 'Location #141'
WHERE loc.id = 1621
    AND (loc.location_code <> 'LOC0141'
    OR loc.location_name <> 'Location #141');

UPDATE experiment.location AS loc
SET location_code = 'LOC0142',
    location_name = 'Location #142'
WHERE loc.id = 1603
    AND (loc.location_code <> 'LOC0142'
    OR loc.location_name <> 'Location #142');

UPDATE experiment.location AS loc
SET location_code = 'LOC0143',
    location_name = 'Location #143'
WHERE loc.id = 1746
    AND (loc.location_code <> 'LOC0143'
    OR loc.location_name <> 'Location #143');

UPDATE experiment.location AS loc
SET location_code = 'LOC0144',
    location_name = 'Location #144'
WHERE loc.id = 1822
    AND (loc.location_code <> 'LOC0144'
    OR loc.location_name <> 'Location #144');

UPDATE experiment.location AS loc
SET location_code = 'LOC0145',
    location_name = 'Location #145'
WHERE loc.id = 2125
    AND (loc.location_code <> 'LOC0145'
    OR loc.location_name <> 'Location #145');

UPDATE experiment.location AS loc
SET location_code = 'LOC0146',
    location_name = 'Location #146'
WHERE loc.id = 1797
    AND (loc.location_code <> 'LOC0146'
    OR loc.location_name <> 'Location #146');

UPDATE experiment.location AS loc
SET location_code = 'LOC0147',
    location_name = 'Location #147'
WHERE loc.id = 1841
    AND (loc.location_code <> 'LOC0147'
    OR loc.location_name <> 'Location #147');

UPDATE experiment.location AS loc
SET location_code = 'LOC0148',
    location_name = 'Location #148'
WHERE loc.id = 1801
    AND (loc.location_code <> 'LOC0148'
    OR loc.location_name <> 'Location #148');

UPDATE experiment.location AS loc
SET location_code = 'LOC0149',
    location_name = 'Location #149'
WHERE loc.id = 1868
    AND (loc.location_code <> 'LOC0149'
    OR loc.location_name <> 'Location #149');

UPDATE experiment.location AS loc
SET location_code = 'LOC0150',
    location_name = 'Location #150'
WHERE loc.id = 1764
    AND (loc.location_code <> 'LOC0150'
    OR loc.location_name <> 'Location #150');

UPDATE experiment.location AS loc
SET location_code = 'LOC0151',
    location_name = 'Location #151'
WHERE loc.id = 1763
    AND (loc.location_code <> 'LOC0151'
    OR loc.location_name <> 'Location #151');

UPDATE experiment.location AS loc
SET location_code = 'LOC0152',
    location_name = 'Location #152'
WHERE loc.id = 1888
    AND (loc.location_code <> 'LOC0152'
    OR loc.location_name <> 'Location #152');

UPDATE experiment.location AS loc
SET location_code = 'LOC0153',
    location_name = 'Location #153'
WHERE loc.id = 1796
    AND (loc.location_code <> 'LOC0153'
    OR loc.location_name <> 'Location #153');

UPDATE experiment.location AS loc
SET location_code = 'LOC0154',
    location_name = 'Location #154'
WHERE loc.id = 1800
    AND (loc.location_code <> 'LOC0154'
    OR loc.location_name <> 'Location #154');

UPDATE experiment.location AS loc
SET location_code = 'LOC0155',
    location_name = 'Location #155'
WHERE loc.id = 1794
    AND (loc.location_code <> 'LOC0155'
    OR loc.location_name <> 'Location #155');

UPDATE experiment.location AS loc
SET location_code = 'LOC0156',
    location_name = 'Location #156'
WHERE loc.id = 2177
    AND (loc.location_code <> 'LOC0156'
    OR loc.location_name <> 'Location #156');

UPDATE experiment.location AS loc
SET location_code = 'LOC0157',
    location_name = 'Location #157'
WHERE loc.id = 2026
    AND (loc.location_code <> 'LOC0157'
    OR loc.location_name <> 'Location #157');

UPDATE experiment.location AS loc
SET location_code = 'LOC0158',
    location_name = 'Location #158'
WHERE loc.id = 1956
    AND (loc.location_code <> 'LOC0158'
    OR loc.location_name <> 'Location #158');

UPDATE experiment.location AS loc
SET location_code = 'LOC0159',
    location_name = 'Location #159'
WHERE loc.id = 1955
    AND (loc.location_code <> 'LOC0159'
    OR loc.location_name <> 'Location #159');

UPDATE experiment.location AS loc
SET location_code = 'LOC0160',
    location_name = 'Location #160'
WHERE loc.id = 1798
    AND (loc.location_code <> 'LOC0160'
    OR loc.location_name <> 'Location #160');

UPDATE experiment.location AS loc
SET location_code = 'LOC0161',
    location_name = 'Location #161'
WHERE loc.id = 1855
    AND (loc.location_code <> 'LOC0161'
    OR loc.location_name <> 'Location #161');

UPDATE experiment.location AS loc
SET location_code = 'LOC0162',
    location_name = 'Location #162'
WHERE loc.id = 1861
    AND (loc.location_code <> 'LOC0162'
    OR loc.location_name <> 'Location #162');

UPDATE experiment.location AS loc
SET location_code = 'LOC0163',
    location_name = 'Location #163'
WHERE loc.id = 1840
    AND (loc.location_code <> 'LOC0163'
    OR loc.location_name <> 'Location #163');

UPDATE experiment.location AS loc
SET location_code = 'LOC0164',
    location_name = 'Location #164'
WHERE loc.id = 1825
    AND (loc.location_code <> 'LOC0164'
    OR loc.location_name <> 'Location #164');

UPDATE experiment.location AS loc
SET location_code = 'LOC0165',
    location_name = 'Location #165'
WHERE loc.id = 2312
    AND (loc.location_code <> 'LOC0165'
    OR loc.location_name <> 'Location #165');

UPDATE experiment.location AS loc
SET location_code = 'LOC0166',
    location_name = 'Location #166'
WHERE loc.id = 2313
    AND (loc.location_code <> 'LOC0166'
    OR loc.location_name <> 'Location #166');

UPDATE experiment.location AS loc
SET location_code = 'LOC0167',
    location_name = 'Location #167'
WHERE loc.id = 2181
    AND (loc.location_code <> 'LOC0167'
    OR loc.location_name <> 'Location #167');

UPDATE experiment.location AS loc
SET location_code = 'LOC0168',
    location_name = 'Location #168'
WHERE loc.id = 2232
    AND (loc.location_code <> 'LOC0168'
    OR loc.location_name <> 'Location #168');

UPDATE experiment.location AS loc
SET location_code = 'LOC0169',
    location_name = 'Location #169'
WHERE loc.id = 2338
    AND (loc.location_code <> 'LOC0169'
    OR loc.location_name <> 'Location #169');

UPDATE experiment.location AS loc
SET location_code = 'LOC0170',
    location_name = 'Location #170'
WHERE loc.id = 2466
    AND (loc.location_code <> 'LOC0170'
    OR loc.location_name <> 'Location #170');

UPDATE experiment.location AS loc
SET location_code = 'LOC0171',
    location_name = 'Location #171'
WHERE loc.id = 2468
    AND (loc.location_code <> 'LOC0171'
    OR loc.location_name <> 'Location #171');

UPDATE experiment.location AS loc
SET location_code = 'LOC0172',
    location_name = 'Location #172'
WHERE loc.id = 2248
    AND (loc.location_code <> 'LOC0172'
    OR loc.location_name <> 'Location #172');

UPDATE experiment.location AS loc
SET location_code = 'LOC0173',
    location_name = 'Location #173'
WHERE loc.id = 2336
    AND (loc.location_code <> 'LOC0173'
    OR loc.location_name <> 'Location #173');

UPDATE experiment.location AS loc
SET location_code = 'LOC0174',
    location_name = 'Location #174'
WHERE loc.id = 2249
    AND (loc.location_code <> 'LOC0174'
    OR loc.location_name <> 'Location #174');

UPDATE experiment.location AS loc
SET location_code = 'LOC0175',
    location_name = 'Location #175'
WHERE loc.id = 2332
    AND (loc.location_code <> 'LOC0175'
    OR loc.location_name <> 'Location #175');

UPDATE experiment.location AS loc
SET location_code = 'LOC0176',
    location_name = 'Location #176'
WHERE loc.id = 2465
    AND (loc.location_code <> 'LOC0176'
    OR loc.location_name <> 'Location #176');

UPDATE experiment.location AS loc
SET location_code = 'LOC0177',
    location_name = 'Location #177'
WHERE loc.id = 2467
    AND (loc.location_code <> 'LOC0177'
    OR loc.location_name <> 'Location #177');

UPDATE experiment.location AS loc
SET location_code = 'LOC0178',
    location_name = 'Location #178'
WHERE loc.id = 2247
    AND (loc.location_code <> 'LOC0178'
    OR loc.location_name <> 'Location #178');

UPDATE experiment.location AS loc
SET location_code = 'LOC0179',
    location_name = 'Location #179'
WHERE loc.id = 2335
    AND (loc.location_code <> 'LOC0179'
    OR loc.location_name <> 'Location #179');

UPDATE experiment.location AS loc
SET location_code = 'LOC0180',
    location_name = 'Location #180'
WHERE loc.id = 2166
    AND (loc.location_code <> 'LOC0180'
    OR loc.location_name <> 'Location #180');

UPDATE experiment.location AS loc
SET location_code = 'LOC0181',
    location_name = 'Location #181'
WHERE loc.id = 2531
    AND (loc.location_code <> 'LOC0181'
    OR loc.location_name <> 'Location #181');

UPDATE experiment.location AS loc
SET location_code = 'LOC0182',
    location_name = 'Location #182'
WHERE loc.id = 2359
    AND (loc.location_code <> 'LOC0182'
    OR loc.location_name <> 'Location #182');

UPDATE experiment.location AS loc
SET location_code = 'LOC0183',
    location_name = 'Location #183'
WHERE loc.id = 2233
    AND (loc.location_code <> 'LOC0183'
    OR loc.location_name <> 'Location #183');

UPDATE experiment.location AS loc
SET location_code = 'LOC0184',
    location_name = 'Location #184'
WHERE loc.id = 2016
    AND (loc.location_code <> 'LOC0184'
    OR loc.location_name <> 'Location #184');

UPDATE experiment.location AS loc
SET location_code = 'LOC0185',
    location_name = 'Location #185'
WHERE loc.id = 2223
    AND (loc.location_code <> 'LOC0185'
    OR loc.location_name <> 'Location #185');

UPDATE experiment.location AS loc
SET location_code = 'LOC0186',
    location_name = 'Location #186'
WHERE loc.id = 2227
    AND (loc.location_code <> 'LOC0186'
    OR loc.location_name <> 'Location #186');

UPDATE experiment.location AS loc
SET location_code = 'LOC0187',
    location_name = 'Location #187'
WHERE loc.id = 2225
    AND (loc.location_code <> 'LOC0187'
    OR loc.location_name <> 'Location #187');

UPDATE experiment.location AS loc
SET location_code = 'LOC0188',
    location_name = 'Location #188'
WHERE loc.id = 2221
    AND (loc.location_code <> 'LOC0188'
    OR loc.location_name <> 'Location #188');

UPDATE experiment.location AS loc
SET location_code = 'LOC0189',
    location_name = 'Location #189'
WHERE loc.id = 2226
    AND (loc.location_code <> 'LOC0189'
    OR loc.location_name <> 'Location #189');

UPDATE experiment.location AS loc
SET location_code = 'LOC0190',
    location_name = 'Location #190'
WHERE loc.id = 2254
    AND (loc.location_code <> 'LOC0190'
    OR loc.location_name <> 'Location #190');

UPDATE experiment.location AS loc
SET location_code = 'LOC0191',
    location_name = 'Location #191'
WHERE loc.id = 2255
    AND (loc.location_code <> 'LOC0191'
    OR loc.location_name <> 'Location #191');

UPDATE experiment.location AS loc
SET location_code = 'LOC0192',
    location_name = 'Location #192'
WHERE loc.id = 2324
    AND (loc.location_code <> 'LOC0192'
    OR loc.location_name <> 'Location #192');

UPDATE experiment.location AS loc
SET location_code = 'LOC0193',
    location_name = 'Location #193'
WHERE loc.id = 2442
    AND (loc.location_code <> 'LOC0193'
    OR loc.location_name <> 'Location #193');

UPDATE experiment.location AS loc
SET location_code = 'LOC0194',
    location_name = 'Location #194'
WHERE loc.id = 2314
    AND (loc.location_code <> 'LOC0194'
    OR loc.location_name <> 'Location #194');

UPDATE experiment.location AS loc
SET location_code = 'LOC0195',
    location_name = 'Location #195'
WHERE loc.id = 2134
    AND (loc.location_code <> 'LOC0195'
    OR loc.location_name <> 'Location #195');

UPDATE experiment.location AS loc
SET location_code = 'LOC0196',
    location_name = 'Location #196'
WHERE loc.id = 2412
    AND (loc.location_code <> 'LOC0196'
    OR loc.location_name <> 'Location #196');

UPDATE experiment.location AS loc
SET location_code = 'LOC0197',
    location_name = 'Location #197'
WHERE loc.id = 2235
    AND (loc.location_code <> 'LOC0197'
    OR loc.location_name <> 'Location #197');

UPDATE experiment.location AS loc
SET location_code = 'LOC0198',
    location_name = 'Location #198'
WHERE loc.id = 2323
    AND (loc.location_code <> 'LOC0198'
    OR loc.location_name <> 'Location #198');

UPDATE experiment.location AS loc
SET location_code = 'LOC0199',
    location_name = 'Location #199'
WHERE loc.id = 2184
    AND (loc.location_code <> 'LOC0199'
    OR loc.location_name <> 'Location #199');

UPDATE experiment.location AS loc
SET location_code = 'LOC0200',
    location_name = 'Location #200'
WHERE loc.id = 2224
    AND (loc.location_code <> 'LOC0200'
    OR loc.location_name <> 'Location #200');

UPDATE experiment.location AS loc
SET location_code = 'LOC0201',
    location_name = 'Location #201'
WHERE loc.id = 2183
    AND (loc.location_code <> 'LOC0201'
    OR loc.location_name <> 'Location #201');

UPDATE experiment.location AS loc
SET location_code = 'LOC0202',
    location_name = 'Location #202'
WHERE loc.id = 2738
    AND (loc.location_code <> 'LOC0202'
    OR loc.location_name <> 'Location #202');

UPDATE experiment.location AS loc
SET location_code = 'LOC0203',
    location_name = 'Location #203'
WHERE loc.id = 2674
    AND (loc.location_code <> 'LOC0203'
    OR loc.location_name <> 'Location #203');

UPDATE experiment.location AS loc
SET location_code = 'LOC0204',
    location_name = 'Location #204'
WHERE loc.id = 2632
    AND (loc.location_code <> 'LOC0204'
    OR loc.location_name <> 'Location #204');

UPDATE experiment.location AS loc
SET location_code = 'LOC0205',
    location_name = 'Location #205'
WHERE loc.id = 3026
    AND (loc.location_code <> 'LOC0205'
    OR loc.location_name <> 'Location #205');

UPDATE experiment.location AS loc
SET location_code = 'LOC0206',
    location_name = 'Location #206'
WHERE loc.id = 3024
    AND (loc.location_code <> 'LOC0206'
    OR loc.location_name <> 'Location #206');

UPDATE experiment.location AS loc
SET location_code = 'LOC0207',
    location_name = 'Location #207'
WHERE loc.id = 3030
    AND (loc.location_code <> 'LOC0207'
    OR loc.location_name <> 'Location #207');

UPDATE experiment.location AS loc
SET location_code = 'LOC0208',
    location_name = 'Location #208'
WHERE loc.id = 2942
    AND (loc.location_code <> 'LOC0208'
    OR loc.location_name <> 'Location #208');

UPDATE experiment.location AS loc
SET location_code = 'LOC0209',
    location_name = 'Location #209'
WHERE loc.id = 2837
    AND (loc.location_code <> 'LOC0209'
    OR loc.location_name <> 'Location #209');

UPDATE experiment.location AS loc
SET location_code = 'LOC0210',
    location_name = 'Location #210'
WHERE loc.id = 2795
    AND (loc.location_code <> 'LOC0210'
    OR loc.location_name <> 'Location #210');

UPDATE experiment.location AS loc
SET location_code = 'LOC0211',
    location_name = 'Location #211'
WHERE loc.id = 2797
    AND (loc.location_code <> 'LOC0211'
    OR loc.location_name <> 'Location #211');

UPDATE experiment.location AS loc
SET location_code = 'LOC0212',
    location_name = 'Location #212'
WHERE loc.id = 2842
    AND (loc.location_code <> 'LOC0212'
    OR loc.location_name <> 'Location #212');

UPDATE experiment.location AS loc
SET location_code = 'LOC0213',
    location_name = 'Location #213'
WHERE loc.id = 2676
    AND (loc.location_code <> 'LOC0213'
    OR loc.location_name <> 'Location #213');

UPDATE experiment.location AS loc
SET location_code = 'LOC0214',
    location_name = 'Location #214'
WHERE loc.id = 2701
    AND (loc.location_code <> 'LOC0214'
    OR loc.location_name <> 'Location #214');

UPDATE experiment.location AS loc
SET location_code = 'LOC0215',
    location_name = 'Location #215'
WHERE loc.id = 2702
    AND (loc.location_code <> 'LOC0215'
    OR loc.location_name <> 'Location #215');

UPDATE experiment.location AS loc
SET location_code = 'LOC0216',
    location_name = 'Location #216'
WHERE loc.id = 2741
    AND (loc.location_code <> 'LOC0216'
    OR loc.location_name <> 'Location #216');

UPDATE experiment.location AS loc
SET location_code = 'LOC0217',
    location_name = 'Location #217'
WHERE loc.id = 3041
    AND (loc.location_code <> 'LOC0217'
    OR loc.location_name <> 'Location #217');

UPDATE experiment.location AS loc
SET location_code = 'LOC0218',
    location_name = 'Location #218'
WHERE loc.id = 3040
    AND (loc.location_code <> 'LOC0218'
    OR loc.location_name <> 'Location #218');

UPDATE experiment.location AS loc
SET location_code = 'LOC0219',
    location_name = 'Location #219'
WHERE loc.id = 3028
    AND (loc.location_code <> 'LOC0219'
    OR loc.location_name <> 'Location #219');

UPDATE experiment.location AS loc
SET location_code = 'LOC0220',
    location_name = 'Location #220'
WHERE loc.id = 2608
    AND (loc.location_code <> 'LOC0220'
    OR loc.location_name <> 'Location #220');

UPDATE experiment.location AS loc
SET location_code = 'LOC0221',
    location_name = 'Location #221'
WHERE loc.id = 2932
    AND (loc.location_code <> 'LOC0221'
    OR loc.location_name <> 'Location #221');

UPDATE experiment.location AS loc
SET location_code = 'LOC0222',
    location_name = 'Location #222'
WHERE loc.id = 2846
    AND (loc.location_code <> 'LOC0222'
    OR loc.location_name <> 'Location #222');

UPDATE experiment.location AS loc
SET location_code = 'LOC0223',
    location_name = 'Location #223'
WHERE loc.id = 2443
    AND (loc.location_code <> 'LOC0223'
    OR loc.location_name <> 'Location #223');

UPDATE experiment.location AS loc
SET location_code = 'LOC0224',
    location_name = 'Location #224'
WHERE loc.id = 3376
    AND (loc.location_code <> 'LOC0224'
    OR loc.location_name <> 'Location #224');

UPDATE experiment.location AS loc
SET location_code = 'LOC0225',
    location_name = 'Location #225'
WHERE loc.id = 2445
    AND (loc.location_code <> 'LOC0225'
    OR loc.location_name <> 'Location #225');

UPDATE experiment.location AS loc
SET location_code = 'LOC0226',
    location_name = 'Location #226'
WHERE loc.id = 3370
    AND (loc.location_code <> 'LOC0226'
    OR loc.location_name <> 'Location #226');

UPDATE experiment.location AS loc
SET location_code = 'LOC0227',
    location_name = 'Location #227'
WHERE loc.id = 3371
    AND (loc.location_code <> 'LOC0227'
    OR loc.location_name <> 'Location #227');

UPDATE experiment.location AS loc
SET location_code = 'LOC0228',
    location_name = 'Location #228'
WHERE loc.id = 3373
    AND (loc.location_code <> 'LOC0228'
    OR loc.location_name <> 'Location #228');

UPDATE experiment.location AS loc
SET location_code = 'LOC0229',
    location_name = 'Location #229'
WHERE loc.id = 3374
    AND (loc.location_code <> 'LOC0229'
    OR loc.location_name <> 'Location #229');

UPDATE experiment.location AS loc
SET location_code = 'LOC0230',
    location_name = 'Location #230'
WHERE loc.id = 3375
    AND (loc.location_code <> 'LOC0230'
    OR loc.location_name <> 'Location #230');

UPDATE experiment.location AS loc
SET location_code = 'LOC0231',
    location_name = 'Location #231'
WHERE loc.id = 2441
    AND (loc.location_code <> 'LOC0231'
    OR loc.location_name <> 'Location #231');

UPDATE experiment.location AS loc
SET location_code = 'LOC0232',
    location_name = 'Location #232'
WHERE loc.id = 2446
    AND (loc.location_code <> 'LOC0232'
    OR loc.location_name <> 'Location #232');

UPDATE experiment.location AS loc
SET location_code = 'LOC0233',
    location_name = 'Location #233'
WHERE loc.id = 2449
    AND (loc.location_code <> 'LOC0233'
    OR loc.location_name <> 'Location #233');

UPDATE experiment.location AS loc
SET location_code = 'LOC0234',
    location_name = 'Location #234'
WHERE loc.id = 2918
    AND (loc.location_code <> 'LOC0234'
    OR loc.location_name <> 'Location #234');

UPDATE experiment.location AS loc
SET location_code = 'LOC0235',
    location_name = 'Location #235'
WHERE loc.id = 2929
    AND (loc.location_code <> 'LOC0235'
    OR loc.location_name <> 'Location #235');

UPDATE experiment.location AS loc
SET location_code = 'LOC0236',
    location_name = 'Location #236'
WHERE loc.id = 2633
    AND (loc.location_code <> 'LOC0236'
    OR loc.location_name <> 'Location #236');

UPDATE experiment.location AS loc
SET location_code = 'LOC0237',
    location_name = 'Location #237'
WHERE loc.id = 2634
    AND (loc.location_code <> 'LOC0237'
    OR loc.location_name <> 'Location #237');

UPDATE experiment.location AS loc
SET location_code = 'LOC0238',
    location_name = 'Location #238'
WHERE loc.id = 2707
    AND (loc.location_code <> 'LOC0238'
    OR loc.location_name <> 'Location #238');

UPDATE experiment.location AS loc
SET location_code = 'LOC0239',
    location_name = 'Location #239'
WHERE loc.id = 2675
    AND (loc.location_code <> 'LOC0239'
    OR loc.location_name <> 'Location #239');

UPDATE experiment.location AS loc
SET location_code = 'LOC0240',
    location_name = 'Location #240'
WHERE loc.id = 2739
    AND (loc.location_code <> 'LOC0240'
    OR loc.location_name <> 'Location #240');

UPDATE experiment.location AS loc
SET location_code = 'LOC0241',
    location_name = 'Location #241'
WHERE loc.id = 2740
    AND (loc.location_code <> 'LOC0241'
    OR loc.location_name <> 'Location #241');

UPDATE experiment.location AS loc
SET location_code = 'LOC0242',
    location_name = 'Location #242'
WHERE loc.id = 2666
    AND (loc.location_code <> 'LOC0242'
    OR loc.location_name <> 'Location #242');

UPDATE experiment.location AS loc
SET location_code = 'LOC0243',
    location_name = 'Location #243'
WHERE loc.id = 2527
    AND (loc.location_code <> 'LOC0243'
    OR loc.location_name <> 'Location #243');

UPDATE experiment.location AS loc
SET location_code = 'LOC0244',
    location_name = 'Location #244'
WHERE loc.id = 3043
    AND (loc.location_code <> 'LOC0244'
    OR loc.location_name <> 'Location #244');

UPDATE experiment.location AS loc
SET location_code = 'LOC0245',
    location_name = 'Location #245'
WHERE loc.id = 3042
    AND (loc.location_code <> 'LOC0245'
    OR loc.location_name <> 'Location #245');

UPDATE experiment.location AS loc
SET location_code = 'LOC0246',
    location_name = 'Location #246'
WHERE loc.id = 3044
    AND (loc.location_code <> 'LOC0246'
    OR loc.location_name <> 'Location #246');

UPDATE experiment.location AS loc
SET location_code = 'LOC0247',
    location_name = 'Location #247'
WHERE loc.id = 2939
    AND (loc.location_code <> 'LOC0247'
    OR loc.location_name <> 'Location #247');

--rollback SELECT NULL;