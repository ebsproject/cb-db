--liquibase formatted sql

--changeset postgres:update_entry_type_for_crosses context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5463 Update entry type for crosses



-- update entry_type to cross or cross; parent
UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181155;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181156;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181161;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181162;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119792;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119793;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181168;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181169;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119795;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181174;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181175;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181185;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181191;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181157;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181173;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181179;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181180;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181181;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181186;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181187;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181192;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181193;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181196;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181197;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 181198;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119797;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119799;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119800;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119801;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119802;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119803;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 119805;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 255339;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 255340;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317074;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317092;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317104;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317130;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317131;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317134;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317091;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317121;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317127;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317083;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317097;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317098;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317049;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317050;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317055;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317058;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317078;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317093;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317094;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317114;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317068;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317103;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317106;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317108;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317109;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317110;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317129;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317133;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317116;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317119;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317120;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317124;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317123;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317128;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317079;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317080;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317089;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317085;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317099;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317086;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317100;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317064;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317073;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317051;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317053;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317059;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317054;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317060;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317048;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317101;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317126;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317102;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317111;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317132;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317115;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317117;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317122;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317118;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317125;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317081;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317082;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317087;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317088;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317095;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317096;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317063;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317069;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317052;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317056;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 317057;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 317062;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558096;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558097;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558098;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558099;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558100;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558101;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558102;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558103;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558105;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558106;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558107;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558108;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520182;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520183;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520184;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520185;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520186;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520187;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520188;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520189;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520190;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520191;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520192;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520193;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520194;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520195;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520196;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520197;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520198;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520199;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520200;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520201;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520202;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520203;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520204;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520205;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520206;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520207;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520208;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520209;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546268;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546269;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546270;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546271;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546272;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546273;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546274;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546275;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546276;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 546277;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520210;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520211;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520212;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520213;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520214;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 520215;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558092;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558093;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558094;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558095;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 558069;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 558068;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558070;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745433;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745432;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745436;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745435;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745434;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745439;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745438;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745437;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745442;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745441;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745440;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745445;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745444;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745443;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745451;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745450;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745453;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745452;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745455;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745454;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745456;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745457;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745458;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745459;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745460;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745446;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745448;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 558104;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726079;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745387;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726080;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726081;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726082;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726083;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726085;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726086;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726087;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726088;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726084;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726089;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726090;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726091;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726092;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726093;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726095;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745381;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745382;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745383;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 726094;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745384;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745385;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745386;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745388;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745389;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745390;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745391;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745392;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745393;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745394;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745397;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745398;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745395;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745396;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745399;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745400;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745401;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745402;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745403;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745404;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745405;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745406;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745407;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745408;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745409;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745410;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745414;

UPDATE experiment.entry
    SET entry_type = 'cross; parent'
    WHERE id = 745415;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745430;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745431;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745416;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745417;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745418;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745419;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745420;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745421;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745422;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745423;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745424;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745425;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745426;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745427;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745428;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 745429;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994078;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994142;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994143;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994077;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994153;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994154;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994155;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994156;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994157;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994160;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994158;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994159;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994161;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994162;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994163;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994164;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994165;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994166;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994167;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994168;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994169;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994170;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994174;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994171;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994172;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994173;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994175;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994176;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994177;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994178;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994179;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994180;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994181;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994182;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994069;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994070;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994071;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994072;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994073;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994074;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994075;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994076;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994079;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994080;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994081;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994082;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994083;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994084;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994085;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994086;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994087;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994088;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994089;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994090;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994091;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994092;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994093;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994094;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994095;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994096;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994097;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994098;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994099;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994100;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994101;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994102;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994103;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994104;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994105;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994106;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994107;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994108;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994109;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994110;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994111;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994112;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994113;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994114;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994115;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994116;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994117;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994118;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994119;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994120;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994124;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994125;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994126;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994127;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994128;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994129;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994130;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994216;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994224;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994188;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994208;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994214;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994217;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994225;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994183;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994184;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994185;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994186;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994189;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994190;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994205;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994206;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994209;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994210;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994211;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994212;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994215;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994218;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994219;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994220;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994221;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994222;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994223;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994226;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994227;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994228;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994229;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994230;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994231;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994232;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994233;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994234;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994235;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994236;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994237;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994238;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994239;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994240;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994241;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994242;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994243;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994244;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994245;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994246;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994247;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994187;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994207;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994213;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994200;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994195;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994192;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994248;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994201;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994193;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994196;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994197;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994198;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994199;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994202;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994203;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994204;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994191;

UPDATE experiment.entry
    SET entry_type = 'cross'
    WHERE id = 994194;



--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181155;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181156;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181161;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181162;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119792;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119793;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181168;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181169;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119795;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181174;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181175;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181185;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181191;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181157;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181173;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181179;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181180;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181181;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181186;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181187;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181192;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181193;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181196;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181197;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 181198;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119797;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119799;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119800;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119801;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119802;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119803;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 119805;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 255339;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 255340;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317074;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317092;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317104;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317130;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317131;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317134;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317091;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317121;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317127;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317083;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317097;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317098;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317049;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317050;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317055;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317058;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317078;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317093;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317094;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317114;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317068;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317103;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317106;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317108;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317109;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317110;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317129;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317133;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317116;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317119;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317120;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317124;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317123;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317128;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317079;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317080;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317089;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317085;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317099;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317086;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317100;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317064;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317073;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317051;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317053;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317059;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317054;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317060;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317048;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317101;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317126;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317102;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317111;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317132;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317115;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317117;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317122;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317118;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317125;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317081;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317082;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317087;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317088;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317095;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317096;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317063;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317069;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317052;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317056;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 317057;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 317062;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558096;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558097;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558098;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558099;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558100;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558101;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558102;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558103;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558105;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558106;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558107;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558108;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520182;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520183;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520184;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520185;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520186;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520187;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520188;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520189;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520190;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520191;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520192;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520193;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520194;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520195;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520196;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520197;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520198;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520199;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520200;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520201;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520202;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520203;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520204;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520205;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520206;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520207;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520208;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520209;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546268;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546269;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546270;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546271;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546272;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546273;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546274;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546275;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546276;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 546277;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520210;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520211;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520212;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520213;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520214;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 520215;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558092;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558093;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558094;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558095;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 558069;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 558068;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558070;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745433;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745432;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745436;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745435;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745434;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745439;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745438;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745437;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745442;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745441;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745440;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745445;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745444;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745443;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745451;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745450;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745453;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745452;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745455;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745454;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745456;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745457;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745458;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745459;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745460;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745446;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745448;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 558104;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726079;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745387;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726080;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726081;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726082;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726083;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726085;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726086;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726087;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726088;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726084;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726089;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726090;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726091;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726092;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726093;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726095;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745381;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745382;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745383;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 726094;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745384;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745385;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745386;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745388;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745389;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745390;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745391;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745392;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745393;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745394;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745397;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745398;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745395;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745396;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745399;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745400;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745401;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745402;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745403;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745404;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745405;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745406;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745407;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745408;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745409;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745410;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745414;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'parent'
--rollback     WHERE id = 745415;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745430;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745431;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745416;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745417;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745418;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745419;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745420;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745421;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745422;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745423;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745424;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745425;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745426;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745427;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745428;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 745429;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994078;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994142;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994143;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994077;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994153;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994154;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994155;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994156;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994157;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994160;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994158;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994159;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994161;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994162;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994163;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994164;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994165;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994166;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994167;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994168;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994169;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994170;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994174;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994171;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994172;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994173;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994175;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994176;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994177;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994178;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994179;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994180;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994181;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994182;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994069;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994070;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994071;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994072;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994073;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994074;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994075;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994076;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994079;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994080;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994081;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994082;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994083;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994084;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994085;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994086;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994087;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994088;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994089;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994090;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994091;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994092;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994093;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994094;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994095;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994096;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994097;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994098;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994099;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994100;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994101;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994102;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994103;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994104;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994105;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994106;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994107;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994108;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994109;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994110;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994111;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994112;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994113;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994114;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994115;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994116;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994117;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994118;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994119;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994120;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994124;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994125;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994126;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994127;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994128;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994129;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994130;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994216;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994224;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994188;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994208;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994214;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994217;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994225;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994183;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994184;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994185;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994186;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994189;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994190;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994205;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994206;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994209;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994210;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994211;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994212;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994215;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994218;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994219;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994220;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994221;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994222;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994223;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994226;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994227;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994228;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994229;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994230;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994231;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994232;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994233;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994234;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994235;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994236;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994237;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994238;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994239;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994240;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994241;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994242;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994243;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994244;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994245;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994246;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994247;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994187;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994207;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994213;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994200;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994195;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994192;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994248;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994201;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994193;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994196;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994197;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994198;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994199;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994202;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994203;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994204;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994191;

--rollback UPDATE experiment.entry
--rollback     SET entry_type = 'entry'
--rollback     WHERE id = 994194;
