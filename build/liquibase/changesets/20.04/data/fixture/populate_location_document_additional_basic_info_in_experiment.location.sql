--liquibase formatted sql

--changeset postgres:populate_location_document_additional_basic_info_in_experiment.location context:fixture splitStatements:false

UPDATE experiment.location SET location_document = NULL;

CREATE EXTENSION IF NOT EXISTS unaccent;

WITH t1 AS (
	SELECT 
		el.id,
		concat(
			setweight(to_tsvector(unaccent(el.location_name)),'A'), ' ',
			setweight(to_tsvector(el.location_code),'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT g0.geospatial_object_name FROM place.geospatial_object g0 WHERE g0.id IN (SELECT g1.root_geospatial_object_id FROM place.geospatial_object g1 WHERE g1.id = el.geospatial_object_id)))), 'B'), ' ',
			setweight(to_tsvector(unaccent((SELECT pg.geospatial_object_name FROM place.geospatial_object pg WHERE pg.id = el.geospatial_object_id))),'B'), ' ',
			setweight(to_tsvector((select program_name from tenant.program where id = el.program_id)),'C'), ' ',
			setweight(to_tsvector(el.location_year::varchar),'C'), ' ',
			setweight(to_tsvector((SELECT s.season_name FROM tenant.season s WHERE s.id =  el.season_id)),'C'), ' ',
			setweight(to_tsvector((SELECT ts.stage_name FROM tenant.stage ts WHERE ts.id = el.stage_id)), 'C'), ' ',
			setweight(to_tsvector(el.location_type), 'CD'), ' ',
			setweight(to_tsvector(unaccent((SELECT tp.person_name FROM tenant.person tp WHERE  tp.id = el.creator_id))),'CD'), ' ',
			setweight(to_tsvector(el.location_status),'CD')
		) AS doc
	FROM 
		experiment.LOCATION el
)

UPDATE experiment.location el SET location_document = cast(t1.doc AS tsvector) FROM t1 WHERE el.id = t1.id;

--ROLLBACK UPDATE experiment.location SET location_document = NULL;
