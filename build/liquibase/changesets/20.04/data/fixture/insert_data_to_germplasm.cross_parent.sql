--liquibase formatted sql

--changeset postgres:insert_data_to_germplasm.cross_parent context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5464 Insert data to germplasm.cross_parent



-- insert fixture data
INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('1', '20097', '47685', '41151', 'female', '1', '8', '116620', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('2', '20097', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('3', '20098', '47685', '41151', 'female', '1', '8', '116620', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('4', '20098', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('5', '20102', '47689', '41155', 'female', '1', '8', '116624', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('6', '20102', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('7', '20103', '47689', '41155', 'female', '1', '8', '116624', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('8', '20103', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('9', '20104', '1043726', '41159', 'female', '1', '8', '116628', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('10', '20104', '540408', '41191', 'male', '2', '8', '116663', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('11', '20105', '1043726', '41159', 'female', '1', '8', '116628', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('12', '20105', '47728', '41194', 'male', '2', '8', '116666', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('13', '20107', '47695', '41161', 'female', '1', '8', '116631', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('14', '20107', '1043726', '41159', 'male', '2', '8', '116628', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('15', '20108', '47695', '41161', 'female', '1', '8', '116631', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('16', '20108', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('17', '20109', '47695', '41161', 'female', '1', '8', '116631', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('18', '20109', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('19', '20111', '47696', '41162', 'female', '1', '8', '116632', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('20', '20111', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('21', '20112', '47696', '41162', 'female', '1', '8', '116632', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('22', '20112', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('23', '20118', '47709', '41175', 'female', '1', '8', '116646', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('24', '20118', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('25', '20122', '47710', '41176', 'female', '1', '8', '116647', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('26', '20122', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('27', '20129', '47685', '41151', 'female', '1', '8', '116620', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('28', '20129', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('29', '20136', '47696', '41162', 'female', '1', '8', '116632', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('30', '20136', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('31', '20138', '47708', '41174', 'female', '1', '8', '116644', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('32', '20138', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('33', '20139', '47708', '41174', 'female', '1', '8', '116644', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('34', '20139', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('35', '20140', '47708', '41174', 'female', '1', '8', '116644', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('36', '20140', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('37', '20141', '47709', '41175', 'female', '1', '8', '116646', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('38', '20141', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('39', '20142', '47709', '41175', 'female', '1', '8', '116646', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('40', '20142', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('41', '20143', '47710', '41176', 'female', '1', '8', '116647', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('42', '20143', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('43', '20144', '47710', '41176', 'female', '1', '8', '116647', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('44', '20144', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('45', '20145', '47713', '41179', 'female', '1', '8', '116650', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('46', '20145', '1043726', '41159', 'male', '2', '31', '181208', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('47', '20146', '47713', '41179', 'female', '1', '8', '116650', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('48', '20146', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('49', '20147', '47713', '41179', 'female', '1', '8', '116650', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('50', '20147', '1042650', '41192', 'male', '2', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('51', '20149', '913149', '41189', 'female', '1', '8', '116660', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('52', '20149', '47689', '41155', 'male', '2', '8', '116624', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('53', '20151', '1042650', '41192', 'female', '1', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('54', '20151', '540408', '41191', 'male', '2', '8', '116663', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('55', '20152', '1042650', '41192', 'female', '1', '8', '116664', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('56', '20152', '47728', '41194', 'male', '2', '8', '116666', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('57', '20153', '1042597', '41193', 'female', '1', '8', '116665', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('58', '20153', '47718', '41184', 'male', '2', '8', '116655', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('59', '20154', '1042597', '41193', 'female', '1', '8', '116665', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('60', '20154', '540408', '41191', 'male', '2', '8', '116663', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('61', '20155', '1042597', '41193', 'female', '1', '8', '116665', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('62', '20155', '47728', '41194', 'male', '2', '8', '116666', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('63', '20157', '47745', '41211', 'female', '1', '8', '116683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('64', '20157', '47689', '41155', 'male', '2', '8', '116624', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('65', '20179', '48122', '41541', 'female', '1', '9', '27861', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('66', '20179', '1042597', '41193', 'male', '2', '8', '116665', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('67', '20180', '48123', '41542', 'female', '1', '9', '27862', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('68', '20180', '1042597', '41193', 'male', '2', '8', '116665', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('69', '21154', '91731', '85194', 'female', '1', '49', '192419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('70', '21154', '1042993', '98749', 'male', '2', '49', '192420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('71', '21165', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('72', '21165', '810551', '85166', 'male', '2', '29', '151060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('73', '21171', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('74', '21171', '1043338', '85046', 'male', '2', '29', '151095', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('75', '21175', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('76', '21175', '91591', '85040', 'male', '2', '29', '151089', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('77', '21177', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('78', '21177', '1043338', '85046', 'male', '2', '29', '151095', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('79', '21179', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('80', '21179', '91607', '85056', 'male', '2', '29', '151104', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('81', '21183', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('82', '21183', '47684', '41150', 'male', '2', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('83', '21185', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('84', '21185', '91607', '85056', 'male', '2', '29', '151104', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('85', '21187', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('86', '21187', '91600', '85049', 'male', '2', '29', '151098', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('87', '21194', '1042650', '41192', 'female', '1', '29', '151074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('88', '21194', '1042646', '95007', 'male', '2', '29', '151072', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('89', '21196', '1042225', '98739', 'female', '1', '49', '192416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('90', '21196', '810551', '85166', 'male', '2', '29', '151060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('91', '21197', '1042225', '98739', 'female', '1', '49', '192416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('92', '21197', '104925', '98601', 'male', '2', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('93', '21218', '47718', '41184', 'female', '1', '29', '151057', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('94', '21218', '48177', '41596', 'male', '2', '29', '151079', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('95', '21219', '47684', '41150', 'female', '1', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('96', '21219', '48174', '41593', 'male', '2', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('97', '21220', '91709', '85162', 'female', '1', '29', '151068', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('98', '21220', '48176', '41595', 'male', '2', '29', '151078', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('99', '21221', '809570', '85090', 'female', '1', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('100', '21221', '48175', '41594', 'male', '2', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('101', '21230', '91731', '85194', 'female', '1', '49', '192419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('102', '21230', '47682', '41212', 'male', '2', '29', '151073', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('103', '21251', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('104', '21251', '104925', '98601', 'male', '2', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('105', '21252', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('106', '21252', '809570', '85090', 'male', '2', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('107', '21257', '48174', '41593', 'female', '1', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('108', '21257', '91607', '85056', 'male', '2', '29', '151104', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('109', '21258', '91729', '85192', 'female', '1', '49', '192418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('110', '21258', '167867', '162418', 'male', '2', '49', '192415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('111', '21262', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('112', '21262', '91591', '85040', 'male', '2', '29', '151089', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('113', '21266', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('114', '21266', '91600', '85049', 'male', '2', '29', '151098', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('115', '21270', '48174', '41593', 'female', '1', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('116', '21270', '91587', '85036', 'male', '2', '29', '151085', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('117', '21271', '48174', '41593', 'female', '1', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('118', '21271', '91590', '85039', 'male', '2', '29', '151088', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('119', '21272', '48174', '41593', 'female', '1', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('120', '21272', '91591', '85040', 'male', '2', '29', '151089', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('121', '21283', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('122', '21283', '91590', '85039', 'male', '2', '29', '151088', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('123', '21285', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('124', '21285', '91600', '85049', 'male', '2', '29', '151098', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('125', '21289', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('126', '21289', '91590', '85039', 'male', '2', '29', '151088', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('127', '21292', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('128', '21292', '1043605', '85048', 'male', '2', '29', '151097', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('129', '21293', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('130', '21293', '91600', '85049', 'male', '2', '29', '151098', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('131', '21295', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('132', '21295', '91591', '85040', 'male', '2', '29', '151089', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('133', '21297', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('134', '21297', '91590', '85039', 'male', '2', '29', '151088', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('135', '21302', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('136', '21302', '91607', '85056', 'male', '2', '29', '151104', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('137', '21314', '1042650', '41192', 'female', '1', '29', '151074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('138', '21314', '47684', '41150', 'male', '2', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('139', '21315', '1042650', '41192', 'female', '1', '29', '151074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('140', '21315', '810551', '85166', 'male', '2', '29', '151060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('141', '21316', '48361', '41782', 'female', '1', '29', '151076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('142', '21316', '1042646', '95007', 'male', '2', '29', '151072', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('143', '21317', '48361', '41782', 'female', '1', '29', '151076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('144', '21317', '47684', '41150', 'male', '2', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('145', '21318', '1042225', '98739', 'female', '1', '49', '192416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('146', '21318', '809570', '85090', 'male', '2', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('147', '21319', '48361', '41782', 'female', '1', '29', '151076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('148', '21319', '810551', '85166', 'male', '2', '29', '151060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('149', '21321', '1042225', '98739', 'female', '1', '49', '192416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('150', '21321', '1042646', '95007', 'male', '2', '29', '151072', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('151', '21322', '91733', '85196', 'female', '1', '49', '192417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('152', '21322', '1042993', '98749', 'male', '2', '49', '192420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('153', '21325', '91731', '85194', 'female', '1', '49', '192419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('154', '21325', '167867', '162418', 'male', '2', '49', '192415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('155', '21350', '47684', '41150', 'female', '1', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('156', '21350', '48175', '41594', 'male', '2', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('157', '21351', '104925', '98601', 'female', '1', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('158', '21351', '48176', '41595', 'male', '2', '29', '151078', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('159', '21352', '1043511', '85045', 'female', '1', '29', '151094', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('160', '21352', '48176', '41595', 'male', '2', '29', '151078', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('161', '21353', '91709', '85162', 'female', '1', '29', '151068', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('162', '21353', '48174', '41593', 'male', '2', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('163', '21355', '1043511', '85045', 'female', '1', '29', '151094', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('164', '21355', '48177', '41596', 'male', '2', '29', '151079', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('165', '21358', '47718', '41184', 'female', '1', '29', '151057', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('166', '21358', '48176', '41595', 'male', '2', '29', '151078', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('167', '21360', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('168', '21360', '91587', '85036', 'male', '2', '29', '151085', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('169', '21362', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('170', '21362', '1043605', '85048', 'male', '2', '29', '151097', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('171', '21367', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('172', '21367', '91590', '85039', 'male', '2', '29', '151088', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('173', '21375', '48174', '41593', 'female', '1', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('174', '21375', '1043338', '85046', 'male', '2', '29', '151095', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('175', '21384', '48175', '41594', 'female', '1', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('176', '21384', '1043605', '85048', 'male', '2', '29', '151097', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('177', '21388', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('178', '21388', '91587', '85036', 'male', '2', '29', '151085', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('179', '21389', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('180', '21389', '91591', '85040', 'male', '2', '29', '151089', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('181', '21391', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('182', '21391', '91587', '85036', 'male', '2', '29', '151085', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('183', '21392', '48369', '41790', 'female', '1', '29', '151082', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('184', '21392', '1043338', '85046', 'male', '2', '29', '151095', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('185', '21398', '91796', '85268', 'female', '1', '29', '151083', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('186', '21398', '1043338', '85046', 'male', '2', '29', '151095', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('187', '21409', '1042650', '41192', 'female', '1', '29', '151074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('188', '21409', '104925', '98601', 'male', '2', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('189', '21410', '1042650', '41192', 'female', '1', '29', '151074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('190', '21410', '809570', '85090', 'male', '2', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('191', '21411', '48361', '41782', 'female', '1', '29', '151076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('192', '21411', '104925', '98601', 'male', '2', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('193', '21412', '48361', '41782', 'female', '1', '29', '151076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('194', '21412', '809570', '85090', 'male', '2', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('195', '21413', '1043726', '41159', 'female', '1', '29', '151077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('196', '21413', '1042646', '95007', 'male', '2', '29', '151072', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('197', '21414', '1042225', '98739', 'female', '1', '49', '192416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('198', '21414', '47684', '41150', 'male', '2', '29', '151059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('199', '21416', '91733', '85196', 'female', '1', '49', '192417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('200', '21416', '167867', '162418', 'male', '2', '49', '192415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('201', '21417', '91729', '85192', 'female', '1', '49', '192418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('202', '21417', '1042993', '98749', 'male', '2', '49', '192420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('203', '21421', '104925', '98601', 'female', '1', '29', '151066', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('204', '21421', '48175', '41594', 'male', '2', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('205', '21422', '91709', '85162', 'female', '1', '29', '151068', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('206', '21422', '48177', '41596', 'male', '2', '29', '151079', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('207', '21423', '809570', '85090', 'female', '1', '29', '151069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('208', '21423', '48174', '41593', 'male', '2', '29', '151080', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('209', '21425', '1043511', '85045', 'female', '1', '29', '151094', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('210', '21425', '48175', '41594', 'male', '2', '29', '151081', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('211', '84900', '257552', '252614', 'female', '1', '67', '317063', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('212', '84900', '91626', '85075', 'male', '2', '65', '344367', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('213', '84901', '257554', '252616', 'female', '1', '67', '317065', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('214', '84901', '91626', '85075', 'male', '2', '65', '344367', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('215', '84902', '257557', '252619', 'female', '1', '67', '317068', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('216', '84902', '91729', '85192', 'male', '2', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('217', '84903', '264055', '259152', 'female', '1', '67', '317069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('218', '84903', '91729', '85192', 'male', '2', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('219', '84904', '257558', '252620', 'female', '1', '67', '317070', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('220', '84904', '91729', '85192', 'male', '2', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('221', '84905', '257559', '252621', 'female', '1', '67', '317071', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('222', '84905', '91729', '85192', 'male', '2', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('223', '84906', '257560', '252622', 'female', '1', '67', '317072', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('224', '84906', '91729', '85192', 'male', '2', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('225', '84907', '257561', '252623', 'female', '1', '67', '317073', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('226', '84907', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('227', '84908', '257563', '252625', 'female', '1', '67', '317075', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('228', '84908', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('229', '84909', '264056', '259153', 'female', '1', '67', '317076', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('230', '84909', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('231', '84910', '264057', '259154', 'female', '1', '67', '317077', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('232', '84910', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('233', '84911', '257564', '252626', 'female', '1', '67', '317078', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('234', '84911', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('235', '84927', '86675', '80120', 'female', '1', '65', '344357', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('236', '84927', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('237', '84928', '86675', '80120', 'female', '1', '65', '344357', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('238', '84928', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('239', '84929', '86675', '80120', 'female', '1', '65', '344357', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('240', '84929', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('241', '84930', '86675', '80120', 'female', '1', '65', '344357', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('242', '84930', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('243', '84935', '86451', '79896', 'female', '1', '65', '344362', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('244', '84935', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('245', '84936', '86451', '79896', 'female', '1', '65', '344362', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('246', '84936', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('247', '84937', '86451', '79896', 'female', '1', '65', '344362', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('248', '84937', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('249', '84938', '86451', '79896', 'female', '1', '65', '344362', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('250', '84938', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('251', '84939', '87554', '80999', 'female', '1', '65', '344363', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('252', '84939', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('253', '84940', '87554', '80999', 'female', '1', '65', '344363', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('254', '84940', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('255', '84941', '87554', '80999', 'female', '1', '65', '344363', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('256', '84941', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('257', '84942', '87554', '80999', 'female', '1', '65', '344363', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('258', '84942', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('259', '84943', '86454', '79899', 'female', '1', '65', '344364', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('260', '84943', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('261', '84944', '86454', '79899', 'female', '1', '65', '344364', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('262', '84944', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('263', '84945', '86454', '79899', 'female', '1', '65', '344364', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('264', '84945', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('265', '84946', '86454', '79899', 'female', '1', '65', '344364', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('266', '84946', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('267', '84947', '86888', '80333', 'female', '1', '65', '344365', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('268', '84947', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('269', '84948', '86888', '80333', 'female', '1', '65', '344365', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('270', '84948', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('271', '84949', '86888', '80333', 'female', '1', '65', '344365', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('272', '84949', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('273', '84950', '86888', '80333', 'female', '1', '65', '344365', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('274', '84950', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('275', '84951', '105769', '99445', 'female', '1', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('276', '84951', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('277', '84952', '105769', '99445', 'female', '1', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('278', '84952', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('279', '84953', '105769', '99445', 'female', '1', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('280', '84953', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('281', '84954', '105769', '99445', 'female', '1', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('282', '84954', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('283', '84955', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('284', '84955', '810551', '85166', 'male', '2', '65', '344417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('285', '84956', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('286', '84956', '106567', '100266', 'male', '2', '65', '344419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('287', '84957', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('288', '84957', '91694', '85147', 'male', '2', '65', '344422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('289', '84958', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('290', '84958', '81065', '74504', 'male', '2', '65', '344428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('291', '84959', '91609', '85058', 'female', '1', '99', '322010', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('292', '84959', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('293', '84960', '91609', '85058', 'female', '1', '99', '322010', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('294', '84960', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('295', '84961', '91610', '85059', 'female', '1', '99', '322011', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('296', '84961', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('297', '84962', '91610', '85059', 'female', '1', '99', '322011', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('298', '84962', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('299', '84963', '1043169', '85062', 'female', '1', '99', '322014', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('300', '84963', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('301', '84964', '1043169', '85062', 'female', '1', '99', '322014', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('302', '84964', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('303', '84965', '1043157', '85065', 'female', '1', '99', '322017', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('304', '84965', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('305', '84966', '1043157', '85065', 'female', '1', '99', '322017', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('306', '84966', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('307', '84967', '1043500', '85066', 'female', '1', '99', '322018', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('308', '84967', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('309', '84968', '1043500', '85066', 'female', '1', '99', '322018', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('310', '84968', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('311', '84969', '48205', '188617', 'female', '1', '65', '344408', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('312', '84969', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('313', '84970', '48205', '188617', 'female', '1', '65', '344408', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('314', '84970', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('315', '84971', '91632', '85081', 'female', '1', '65', '344414', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('316', '84971', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('317', '84972', '91632', '85081', 'female', '1', '65', '344414', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('318', '84972', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('319', '84973', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('320', '84973', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('321', '84974', '271870', '266990', 'female', '1', '65', '344434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('322', '84974', '1042597', '41193', 'male', '2', '65', '344381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('323', '84977', '1042640', '98754', 'female', '1', '65', '344355', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('324', '84977', '86675', '80120', 'male', '2', '65', '344357', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('325', '84978', '1042640', '98754', 'female', '1', '65', '344355', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('326', '84978', '87111', '80556', 'male', '2', '65', '344358', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('327', '84979', '1042640', '98754', 'female', '1', '65', '344355', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('328', '84979', '86451', '79896', 'male', '2', '65', '344362', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('329', '84980', '1042640', '98754', 'female', '1', '65', '344355', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('330', '84980', '87554', '80999', 'male', '2', '65', '344363', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('331', '84986', '91729', '85192', 'female', '1', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('332', '84986', '295710', '265774', 'male', '2', '65', '344366', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('333', '84987', '91729', '85192', 'female', '1', '65', '344368', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('334', '84987', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('335', '84988', '91731', '85194', 'female', '1', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('336', '84988', '295710', '265774', 'male', '2', '65', '344366', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('337', '85005', '264068', '259165', 'female', '1', '67', '317048', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('338', '85005', '105829', '99505', 'male', '2', '65', '344430', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('339', '85006', '264068', '259165', 'female', '1', '67', '317048', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('340', '85006', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('341', '85007', '257645', '252707', 'female', '1', '67', '317059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('342', '85007', '105769', '99445', 'male', '2', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('343', '85008', '257645', '252707', 'female', '1', '67', '317059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('344', '85008', '91602', '85051', 'male', '2', '65', '344402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('345', '85009', '257645', '252707', 'female', '1', '67', '317059', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('346', '85009', '1042650', '85100', 'male', '2', '65', '344350', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('347', '85010', '264069', '259166', 'female', '1', '67', '317060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('348', '85010', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('349', '85011', '264069', '259166', 'female', '1', '67', '317060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('350', '85011', '91602', '85051', 'male', '2', '65', '344402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('351', '85012', '264069', '259166', 'female', '1', '67', '317060', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('352', '85012', '1042650', '85100', 'male', '2', '65', '344350', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('353', '85013', '264086', '259182', 'female', '1', '67', '317061', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('354', '85013', '105769', '99445', 'male', '2', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('355', '85014', '264086', '259182', 'female', '1', '67', '317061', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('356', '85014', '91602', '85051', 'male', '2', '65', '344402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('357', '85015', '264086', '259182', 'female', '1', '67', '317061', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('358', '85015', '1042650', '85100', 'male', '2', '65', '344350', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('359', '85017', '257646', '252708', 'female', '1', '67', '317062', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('360', '85017', '105769', '99445', 'male', '2', '65', '344433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('361', '85018', '257646', '252708', 'female', '1', '67', '317062', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('362', '85018', '91602', '85051', 'male', '2', '65', '344402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('363', '85019', '257646', '252708', 'female', '1', '67', '317062', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('364', '85019', '1042650', '85100', 'male', '2', '65', '344350', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('365', '85020', '257635', '252697', 'female', '1', '67', '317049', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('366', '85020', '105829', '99505', 'male', '2', '65', '344430', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('367', '85021', '257635', '252697', 'female', '1', '67', '317049', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('368', '85021', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('369', '85022', '257636', '252698', 'female', '1', '67', '317050', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('370', '85022', '105829', '99505', 'male', '2', '65', '344430', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('371', '85023', '257636', '252698', 'female', '1', '67', '317050', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('372', '85023', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('373', '85024', '257637', '252699', 'female', '1', '67', '317051', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('374', '85024', '105829', '99505', 'male', '2', '65', '344430', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('375', '85025', '257637', '252699', 'female', '1', '67', '317051', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('376', '85025', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('377', '85026', '257638', '252700', 'female', '1', '67', '317052', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('378', '85026', '105821', '99497', 'male', '2', '65', '344431', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('379', '85027', '257639', '252701', 'female', '1', '67', '317053', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('380', '85027', '105821', '99497', 'male', '2', '65', '344431', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('381', '85028', '257640', '252702', 'female', '1', '67', '317054', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('382', '85028', '105821', '99497', 'male', '2', '65', '344431', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('383', '85029', '257641', '252703', 'female', '1', '67', '317055', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('384', '85029', '106397', '100096', 'male', '2', '65', '344427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('385', '85030', '257642', '252704', 'female', '1', '67', '317056', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('386', '85030', '105821', '99497', 'male', '2', '65', '344431', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('387', '85036', '257552', '252614', 'female', '1', '67', '317063', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('388', '85036', '1042650', '85100', 'male', '2', '65', '344350', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('389', '85037', '257554', '252616', 'female', '1', '67', '317065', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('390', '85037', '47682', '73798', 'male', '2', '65', '344349', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('391', '85042', '257562', '252624', 'female', '1', '67', '317074', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('392', '85042', '91731', '85194', 'male', '2', '65', '344369', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('393', '99809', '105084', '98760', 'female', '1', '122', '547684', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('394', '99809', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('395', '99810', '270342', '265447', 'female', '1', '122', '577774', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('396', '99810', '105124', '98800', 'male', '2', '122', '547744', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('397', '99814', '105084', '98760', 'female', '1', '122', '547684', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('398', '99814', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('399', '99815', '105084', '98760', 'female', '1', '122', '547684', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('400', '99815', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('401', '99816', '1042339', '85233', 'female', '1', '122', '547685', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('402', '99816', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('403', '99817', '1042339', '85233', 'female', '1', '122', '547685', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('404', '99817', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('405', '99818', '271870', '266990', 'female', '1', '122', '547731', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('406', '99818', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('407', '99819', '271870', '266990', 'female', '1', '122', '547731', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('408', '99819', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('409', '99820', '271870', '266990', 'female', '1', '122', '547731', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('410', '99820', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('411', '99821', '91932', '85404', 'female', '1', '122', '547740', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('412', '99821', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('413', '99822', '1042339', '85233', 'female', '1', '122', '547685', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('414', '99822', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('415', '99823', '91932', '85404', 'female', '1', '122', '547740', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('416', '99823', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('417', '99824', '106457', '100156', 'female', '1', '122', '547753', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('418', '99824', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('419', '99825', '106457', '100156', 'female', '1', '122', '547753', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('420', '99825', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('421', '99826', '106457', '100156', 'female', '1', '122', '547753', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('422', '99826', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('423', '99827', '106570', '100269', 'female', '1', '122', '547755', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('424', '99827', '1042650', '85100', 'male', '2', '122', '547683', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('425', '99828', '106570', '100269', 'female', '1', '122', '547755', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('426', '99828', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('427', '99829', '270341', '265446', 'female', '1', '122', '577773', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('428', '99829', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('429', '99830', '270341', '265446', 'female', '1', '122', '577773', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('430', '99830', '105514', '99190', 'male', '2', '122', '547733', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('431', '99831', '270341', '265446', 'female', '1', '122', '577773', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('432', '99831', '106360', '100059', 'male', '2', '122', '547743', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('433', '99832', '106570', '100269', 'female', '1', '122', '547755', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('434', '99832', '810553', '85167', 'male', '2', '122', '547726', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('435', '99833', '270341', '265446', 'female', '1', '122', '577773', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('436', '99833', '105124', '98800', 'male', '2', '122', '547744', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('437', '99834', '270341', '265446', 'female', '1', '122', '577773', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('438', '99834', '106482', '100181', 'male', '2', '122', '547750', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('439', '99835', '270342', '265447', 'female', '1', '122', '577774', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('440', '99835', '106323', '100022', 'male', '2', '122', '547737', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('441', '99836', '270342', '265447', 'female', '1', '122', '577774', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('442', '99836', '91638', '85087', 'male', '2', '122', '547751', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('443', '99837', '270343', '265448', 'female', '1', '122', '577775', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('444', '99837', '105152', '98828', 'male', '2', '122', '547741', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('445', '99838', '270343', '265448', 'female', '1', '122', '577775', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('446', '99838', '106362', '100061', 'male', '2', '122', '547745', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('447', '99839', '270343', '265448', 'female', '1', '122', '577775', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('448', '99839', '91715', '85168', 'male', '2', '122', '547752', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('449', '99840', '270344', '265449', 'female', '1', '122', '577776', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('450', '99840', '105664', '99340', 'male', '2', '122', '547738', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('451', '99841', '270344', '265449', 'female', '1', '122', '577776', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('452', '99841', '106502', '100201', 'male', '2', '122', '547746', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('453', '99842', '270344', '265449', 'female', '1', '122', '577776', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('454', '99842', '106457', '100156', 'male', '2', '122', '547753', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('455', '99843', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('456', '99843', '105493', '99169', 'male', '2', '122', '547734', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('457', '99844', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('458', '99844', '91719', '85172', 'male', '2', '122', '547739', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('459', '99845', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('460', '99845', '106567', '100266', 'male', '2', '122', '547722', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('461', '99846', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('462', '99846', '105528', '99204', 'male', '2', '122', '547732', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('463', '99847', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('464', '99847', '105152', '98828', 'male', '2', '122', '547741', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('465', '99848', '270345', '265450', 'female', '1', '122', '577777', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('466', '99848', '106361', '100060', 'male', '2', '122', '547747', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('467', '99849', '270346', '265451', 'female', '1', '122', '577778', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('468', '99849', '91932', '85404', 'male', '2', '122', '547740', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('469', '99850', '270346', '265451', 'female', '1', '122', '577778', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('470', '99850', '106515', '100214', 'male', '2', '122', '547748', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('471', '99851', '270346', '265451', 'female', '1', '122', '577778', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('472', '99851', '87821', '81266', 'male', '2', '122', '547754', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('473', '99852', '270347', '265452', 'female', '1', '122', '577779', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('474', '99852', '81065', '74504', 'male', '2', '122', '547730', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('475', '99853', '270347', '265452', 'female', '1', '122', '577779', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('476', '99853', '105528', '99204', 'male', '2', '122', '547732', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('477', '99854', '270347', '265452', 'female', '1', '122', '577779', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('478', '99854', '91699', '85152', 'male', '2', '122', '547749', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('479', '99855', '270347', '265452', 'female', '1', '122', '577779', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('480', '99855', '106570', '100269', 'male', '2', '122', '547755', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('481', '99856', '270348', '265453', 'female', '1', '122', '577780', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('482', '99856', '47682', '73798', 'male', '2', '122', '547682', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('483', '99857', '270348', '265453', 'female', '1', '122', '577780', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('484', '99857', '91716', '85169', 'male', '2', '122', '547742', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('485', '99858', '270348', '265453', 'female', '1', '122', '577780', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('486', '99858', '106594', '100293', 'male', '2', '122', '547756', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('487', '99859', '397477', '394868', 'female', '1', '127', '558068', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('488', '99859', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('489', '99860', '397476', '394867', 'female', '1', '127', '558069', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('490', '99860', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('491', '99876', '521192', '521863', 'female', '1', '124', '621403', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('492', '99876', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('493', '99877', '521193', '521864', 'female', '1', '124', '621404', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('494', '99877', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('495', '99878', '521194', '521865', 'female', '1', '124', '621389', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('496', '99878', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('497', '99879', '521195', '521866', 'female', '1', '124', '621390', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('498', '99879', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('499', '99880', '521196', '521867', 'female', '1', '124', '621391', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('500', '99880', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('501', '99881', '521197', '521868', 'female', '1', '124', '621392', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('502', '99881', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('503', '99882', '521198', '521869', 'female', '1', '124', '621393', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('504', '99882', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('505', '99883', '521199', '521870', 'female', '1', '124', '621394', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('506', '99883', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('507', '99884', '521200', '521871', 'female', '1', '124', '621395', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('508', '99884', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('509', '99885', '521201', '521872', 'female', '1', '124', '621396', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('510', '99885', '91729', '85192', 'male', '2', '122', '547690', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('511', '99886', '521202', '521873', 'female', '1', '124', '621397', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('512', '99886', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('513', '99887', '521203', '521874', 'female', '1', '124', '621398', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('514', '99887', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('515', '99888', '521204', '521875', 'female', '1', '124', '621399', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('516', '99888', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('517', '99889', '521205', '521876', 'female', '1', '124', '621400', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('518', '99889', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('519', '99890', '521190', '521861', 'female', '1', '124', '621401', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('520', '99890', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('521', '99891', '521191', '521862', 'female', '1', '124', '621402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('522', '99891', '91731', '85194', 'male', '2', '122', '547691', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('523', '121350', '48361', '555328', 'female', '1', '159', '734419', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('524', '121350', '100975', '555335', 'male', '2', '159', '734421', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('525', '121354', '91729', '85192', 'female', '1', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('526', '121354', '1042993', '98749', 'male', '2', '159', '734478', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('527', '121355', '91731', '85194', 'female', '1', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('528', '121355', '1042993', '98749', 'male', '2', '159', '734478', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('529', '121366', '47682', '73798', 'female', '1', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('530', '121366', '100975', '555335', 'male', '2', '159', '734421', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('531', '121427', '521223', '521894', 'female', '1', '162', '745381', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('532', '121427', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('533', '121428', '521224', '521895', 'female', '1', '162', '745382', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('534', '121428', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('535', '121429', '521225', '521896', 'female', '1', '162', '745383', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('536', '121429', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('537', '121430', '521226', '521897', 'female', '1', '162', '745384', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('538', '121430', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('539', '121431', '521227', '521898', 'female', '1', '162', '745385', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('540', '121431', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('541', '121432', '521230', '521901', 'female', '1', '162', '745388', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('542', '121432', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('543', '121433', '521228', '521899', 'female', '1', '162', '745386', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('544', '121433', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('545', '121434', '521229', '521900', 'female', '1', '162', '745387', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('546', '121434', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('547', '121435', '521231', '521902', 'female', '1', '162', '745389', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('548', '121435', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('549', '121436', '521232', '521903', 'female', '1', '162', '745390', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('550', '121436', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('551', '121437', '521233', '521904', 'female', '1', '162', '745391', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('552', '121437', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('553', '121438', '521234', '521905', 'female', '1', '162', '745392', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('554', '121438', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('555', '121439', '521235', '521906', 'female', '1', '162', '745393', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('556', '121439', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('557', '121440', '521236', '521907', 'female', '1', '162', '745394', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('558', '121440', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('559', '121441', '521237', '521908', 'female', '1', '162', '745395', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('560', '121441', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('561', '121442', '521238', '521909', 'female', '1', '162', '745396', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('562', '121442', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('563', '121443', '521239', '521910', 'female', '1', '162', '745397', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('564', '121443', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('565', '121444', '521240', '521911', 'female', '1', '162', '745398', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('566', '121444', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('567', '121445', '521244', '521915', 'female', '1', '162', '745402', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('568', '121445', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('569', '121446', '521241', '521912', 'female', '1', '162', '745399', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('570', '121446', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('571', '121447', '521242', '521913', 'female', '1', '162', '745400', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('572', '121447', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('573', '121448', '521243', '521914', 'female', '1', '162', '745401', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('574', '121448', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('575', '121449', '521245', '521916', 'female', '1', '162', '745403', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('576', '121449', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('577', '121450', '521246', '521917', 'female', '1', '162', '745404', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('578', '121450', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('579', '121451', '521247', '521918', 'female', '1', '162', '745405', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('580', '121451', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('581', '121452', '521248', '521919', 'female', '1', '162', '745406', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('582', '121452', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('583', '121453', '521249', '521920', 'female', '1', '162', '745407', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('584', '121453', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('585', '121454', '521250', '521921', 'female', '1', '162', '745408', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('586', '121454', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('587', '121455', '521251', '521922', 'female', '1', '162', '745409', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('588', '121455', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('589', '121456', '521252', '521923', 'female', '1', '162', '745410', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('590', '121456', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('591', '121457', '1042596', '555337', 'female', '1', '159', '734410', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('592', '121457', '100974', '555334', 'male', '2', '159', '734420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('593', '121458', '1042650', '85100', 'female', '1', '159', '734411', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('594', '121458', '100974', '555334', 'male', '2', '159', '734420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('595', '121459', '295710', '265774', 'female', '1', '159', '734412', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('596', '121459', '48174', '188609', 'male', '2', '159', '734414', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('597', '121460', '91632', '85081', 'female', '1', '159', '734413', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('598', '121460', '48175', '188610', 'male', '2', '159', '734415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('599', '121461', '48174', '188609', 'female', '1', '159', '734414', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('600', '121461', '100975', '555335', 'male', '2', '159', '734421', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('601', '121462', '48175', '188610', 'female', '1', '159', '734415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('602', '121462', '100975', '555335', 'male', '2', '159', '734421', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('603', '121463', '48176', '188618', 'female', '1', '159', '734416', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('604', '121463', '100974', '555334', 'male', '2', '159', '734420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('605', '121464', '48177', '188602', 'female', '1', '159', '734417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('606', '121464', '100974', '555334', 'male', '2', '159', '734420', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('607', '121465', '270341', '265446', 'female', '1', '159', '734423', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('608', '121465', '100977', '555336', 'male', '2', '159', '734422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('609', '121466', '270342', '265447', 'female', '1', '159', '734424', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('610', '121466', '100977', '555336', 'male', '2', '159', '734422', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('611', '121467', '270343', '265448', 'female', '1', '159', '734425', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('612', '121467', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('613', '121468', '270344', '265449', 'female', '1', '159', '734426', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('614', '121468', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('615', '121469', '270345', '265450', 'female', '1', '159', '734427', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('616', '121469', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('617', '121470', '270346', '265451', 'female', '1', '159', '734428', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('618', '121470', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('619', '121471', '270347', '265452', 'female', '1', '159', '734429', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('620', '121471', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('621', '121472', '270348', '265453', 'female', '1', '159', '734430', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('622', '121472', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('623', '121473', '106415', '566369', 'female', '1', '159', '734431', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('624', '121473', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('625', '121474', '106425', '566377', 'female', '1', '159', '734432', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('626', '121474', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('627', '121475', '106343', '566374', 'female', '1', '159', '734433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('628', '121475', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('629', '121476', '106466', '566375', 'female', '1', '159', '734434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('630', '121476', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('631', '121477', '106570', '566373', 'female', '1', '159', '734435', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('632', '121477', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('633', '121478', '106549', '566351', 'female', '1', '159', '734436', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('634', '121478', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('635', '121479', '106546', '566359', 'female', '1', '159', '734437', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('636', '121479', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('637', '121480', '106594', '566347', 'female', '1', '159', '734438', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('638', '121480', '1043158', '555344', 'male', '2', '159', '734456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('639', '121481', '91922', '585746', 'female', '1', '159', '734440', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('640', '121481', '163207', '585740', 'male', '2', '159', '734439', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('641', '121482', '91708', '585744', 'female', '1', '159', '734441', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('642', '121482', '163207', '585740', 'male', '2', '159', '734439', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('643', '121483', '152748', '585745', 'female', '1', '159', '734442', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('644', '121483', '163207', '585740', 'male', '2', '159', '734439', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('645', '121484', '810551', '585741', 'female', '1', '159', '734443', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('646', '121484', '110378', '555341', 'male', '2', '159', '734453', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('647', '121485', '810450', '585743', 'female', '1', '159', '734444', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('648', '121485', '110378', '555341', 'male', '2', '159', '734453', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('649', '121486', '89746', '585742', 'female', '1', '159', '734445', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('650', '121486', '110378', '555341', 'male', '2', '159', '734453', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('651', '121487', '152816', '585748', 'female', '1', '159', '734446', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('652', '121487', '1043338', '586692', 'male', '2', '159', '734451', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('653', '121488', '152812', '585749', 'female', '1', '159', '734447', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('654', '121488', '1043338', '586692', 'male', '2', '159', '734451', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('655', '121489', '91800', '585750', 'female', '1', '159', '734448', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('656', '121489', '1043338', '586692', 'male', '2', '159', '734451', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('657', '121490', '47716', '585747', 'female', '1', '159', '734449', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('658', '121490', '1043338', '586692', 'male', '2', '159', '734451', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('659', '121491', '1043741', '555340', 'female', '1', '159', '734452', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('660', '121491', '110384', '555342', 'male', '2', '159', '734454', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('661', '121492', '47708', '41174', 'female', '1', '159', '734457', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('662', '121492', '110350', '555339', 'male', '2', '159', '734450', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('663', '121493', '1042479', '41605', 'female', '1', '159', '734458', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('664', '121493', '110350', '555339', 'male', '2', '159', '734450', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('665', '121494', '271870', '266990', 'female', '1', '159', '734459', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('666', '121494', '110350', '555339', 'male', '2', '159', '734450', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('667', '121495', '167861', '162411', 'female', '1', '159', '734460', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('668', '121495', '110350', '555339', 'male', '2', '159', '734450', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('669', '121496', '978969', '85094', 'female', '1', '159', '734463', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('670', '121496', '110384', '555342', 'male', '2', '159', '734454', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('671', '121497', '91646', '85095', 'female', '1', '159', '734464', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('672', '121497', '110384', '555342', 'male', '2', '159', '734454', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('673', '121498', '553839', '555466', 'female', '1', '159', '734468', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('674', '121498', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('675', '121499', '553840', '555467', 'female', '1', '159', '734469', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('676', '121499', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('677', '121500', '553841', '555468', 'female', '1', '159', '734470', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('678', '121500', '1043500', '426332', 'male', '2', '159', '734461', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('679', '121501', '553842', '555469', 'female', '1', '159', '734471', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('680', '121501', '553838', '1007063', 'male', '2', '159', '734462', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('681', '121502', '553843', '555470', 'female', '1', '159', '734472', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('682', '121502', '553838', '1007063', 'male', '2', '159', '734462', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('683', '121503', '553844', '555471', 'female', '1', '159', '734473', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('684', '121503', '553838', '1007063', 'male', '2', '159', '734462', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('685', '121504', '553848', '555475', 'female', '1', '159', '734477', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('686', '121504', '295710', '265774', 'male', '2', '159', '734412', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('687', '121505', '1361470', '1007064', 'female', '1', '159', '734482', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('688', '121505', '1043726', '85182', 'male', '2', '159', '734485', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('689', '121506', '1042331', '73801', 'female', '1', '159', '734484', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('690', '121506', '48177', '188602', 'male', '2', '159', '734417', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('691', '121510', '553839', '555466', 'female', '1', '159', '734468', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('692', '121510', '1042993', '98749', 'male', '2', '159', '734478', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('693', '121511', '553840', '555467', 'female', '1', '159', '734469', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('694', '121511', '1042993', '98749', 'male', '2', '159', '734478', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('695', '121512', '553841', '555468', 'female', '1', '159', '734470', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('696', '121512', '1042993', '98749', 'male', '2', '159', '734478', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('697', '121513', '553842', '555469', 'female', '1', '159', '734471', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('698', '121513', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('699', '121514', '553843', '555470', 'female', '1', '159', '734472', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('700', '121514', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('701', '121515', '553844', '555471', 'female', '1', '159', '734473', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('702', '121515', '147289', '162416', 'male', '2', '159', '734481', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('703', '121516', '553845', '555472', 'female', '1', '159', '734474', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('704', '121516', '147289', '162416', 'male', '2', '159', '734481', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('705', '121612', '715038', '718152', 'female', '1', '160', '763838', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('706', '121612', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('707', '121613', '715008', '718122', 'female', '1', '160', '763846', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('708', '121613', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('709', '121617', '397533', '394924', 'female', '1', '162', '745452', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('710', '121617', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('711', '121618', '715030', '718144', 'female', '1', '160', '763830', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('712', '121618', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('713', '121619', '715036', '718150', 'female', '1', '160', '763836', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('714', '121619', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('715', '121620', '715039', '718153', 'female', '1', '160', '763839', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('716', '121620', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('717', '121621', '715009', '718123', 'female', '1', '160', '763847', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('718', '121621', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('719', '121626', '397517', '394908', 'female', '1', '162', '745432', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('720', '121626', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('721', '121627', '397522', '394913', 'female', '1', '162', '745438', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('722', '121627', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('723', '121628', '397521', '394912', 'female', '1', '162', '745439', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('724', '121628', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('725', '121629', '397531', '394922', 'female', '1', '162', '745450', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('726', '121629', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('727', '121630', '397535', '394926', 'female', '1', '162', '745454', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('728', '121630', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('729', '121631', '397539', '394930', 'female', '1', '162', '745459', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('730', '121631', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('731', '121632', '715027', '718141', 'female', '1', '160', '763827', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('732', '121632', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('733', '121633', '715028', '718142', 'female', '1', '160', '763828', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('734', '121633', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('735', '121634', '715031', '718145', 'female', '1', '160', '763831', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('736', '121634', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('737', '121635', '715032', '718146', 'female', '1', '160', '763832', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('738', '121635', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('739', '121636', '715033', '718147', 'female', '1', '160', '763833', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('740', '121636', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('741', '121637', '715034', '718148', 'female', '1', '160', '763834', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('742', '121637', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('743', '121638', '715037', '718151', 'female', '1', '160', '763837', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('744', '121638', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('745', '121639', '715040', '718154', 'female', '1', '160', '763840', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('746', '121639', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('747', '121640', '715041', '718155', 'female', '1', '160', '763841', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('748', '121640', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('749', '121641', '715042', '718156', 'female', '1', '160', '763842', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('750', '121641', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('751', '121642', '715043', '718157', 'female', '1', '160', '763843', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('752', '121642', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('753', '121643', '715006', '718120', 'female', '1', '160', '763844', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('754', '121643', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('755', '121644', '715007', '718121', 'female', '1', '160', '763845', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('756', '121644', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('757', '121645', '715010', '718124', 'female', '1', '160', '763848', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('758', '121645', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('759', '121646', '715011', '718125', 'female', '1', '160', '763849', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('760', '121646', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('761', '121647', '715012', '718126', 'female', '1', '160', '763850', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('762', '121647', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('763', '121648', '715013', '718127', 'female', '1', '160', '763851', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('764', '121648', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('765', '121649', '715048', '718162', 'female', '1', '160', '763852', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('766', '121649', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('767', '121650', '715014', '718128', 'female', '1', '160', '763853', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('768', '121650', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('769', '121651', '715015', '718129', 'female', '1', '160', '763854', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('770', '121651', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('771', '121652', '715016', '718130', 'female', '1', '160', '763855', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('772', '121652', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('773', '121653', '715017', '718131', 'female', '1', '160', '763856', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('774', '121653', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('775', '121654', '715018', '718132', 'female', '1', '160', '763857', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('776', '121654', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('777', '121655', '715019', '718133', 'female', '1', '160', '763858', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('778', '121655', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('779', '121656', '715020', '718134', 'female', '1', '160', '763859', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('780', '121656', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('781', '121657', '715021', '718135', 'female', '1', '160', '763860', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('782', '121657', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('783', '121658', '715044', '718158', 'female', '1', '160', '763861', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('784', '121658', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('785', '121659', '715045', '718159', 'female', '1', '160', '763862', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('786', '121659', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('787', '121660', '715046', '718160', 'female', '1', '160', '763863', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('788', '121660', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('789', '121661', '715047', '718161', 'female', '1', '160', '763864', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('790', '121661', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('791', '121662', '715022', '718136', 'female', '1', '160', '763865', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('792', '121662', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('793', '121663', '715023', '718137', 'female', '1', '160', '763866', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('794', '121663', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('795', '121664', '715024', '718138', 'female', '1', '160', '763867', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('796', '121664', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('797', '121665', '715025', '718139', 'female', '1', '160', '763868', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('798', '121665', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('799', '121666', '715026', '718140', 'female', '1', '160', '763869', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('800', '121666', '91731', '85194', 'male', '2', '159', '734480', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('801', '121697', '397530', '394921', 'female', '1', '162', '745451', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('802', '121697', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('803', '121698', '715029', '718143', 'female', '1', '160', '763829', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('804', '121698', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('805', '121699', '715035', '718149', 'female', '1', '160', '763835', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('806', '121699', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('807', '122438', '397527', '394918', 'female', '1', '162', '745445', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('808', '122438', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('809', '122441', '397526', '394917', 'female', '1', '162', '745440', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('810', '122441', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('811', '122443', '397520', '394911', 'female', '1', '162', '745434', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('812', '122443', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('813', '122444', '521257', '521928', 'female', '1', '162', '745415', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('814', '122444', '91729', '85192', 'male', '2', '159', '734479', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('815', '122446', '397532', '394923', 'female', '1', '162', '745453', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('816', '122446', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('817', '122449', '397519', '394910', 'female', '1', '162', '745435', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('818', '122449', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('819', '122450', '397525', '394916', 'female', '1', '162', '745441', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('820', '122450', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('821', '122451', '397524', '394915', 'female', '1', '162', '745442', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('822', '122451', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('823', '122452', '397529', '394920', 'female', '1', '162', '745443', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('824', '122452', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('825', '122453', '397528', '394919', 'female', '1', '162', '745444', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('826', '122453', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('827', '122454', '397536', '394927', 'female', '1', '162', '745456', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('828', '122454', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('829', '122455', '397537', '394928', 'female', '1', '162', '745457', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('830', '122455', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('831', '122456', '397538', '394929', 'female', '1', '162', '745458', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('832', '122456', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('833', '122468', '397516', '394907', 'female', '1', '162', '745433', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('834', '122468', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('835', '122469', '397518', '394909', 'female', '1', '162', '745436', NULL, '1', '2020-04-23 20:25:33.96353');

INSERT INTO germplasm.cross_parent (id, cross_id, germplasm_id, seed_id, parent_role, order_number, experiment_id, entry_id, plot_id, creator_id, creation_timestamp)
    VALUES ('836', '122469', '47682', '73798', 'male', '2', '159', '734418', NULL, '1', '2020-04-23 20:25:33.96353');


-- update sequence
SELECT SETVAL('germplasm.cross_parent_id_seq', COALESCE(MAX(id), 1) ) FROM germplasm.cross_parent;



--rollback DELETE FROM germplasm.cross_parent;

--rollback SELECT SETVAL('germplasm.cross_parent_id_seq', COALESCE(MAX(id), 1) ) FROM germplasm.cross_parent;
