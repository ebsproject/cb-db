--liquibase formatted sql

--changeset postgres:update_experiment_status_scale_values_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update experiment_status scale values in master.scale_value



DELETE FROM 
    master.scale_value 
WHERE 
    scale_id 
IN
    (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS');

INSERT INTO
    master.scale_value (scale_id, value, order_number, description, display_name)
VALUES
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Draft','1','Draft','Draft'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Entry List Created','2','Entry List Created','Entry List Created'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Design Generated','3','Design Generated','Design Generated'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Experiment Group Specified','4','Experiment Group Specified','Experiment Group Specified'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Location Rep Studies Created','5','Location Rep Studies Created','Location Rep Studies Created'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Completed','6','Completed','Completed'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Active','7','Active','Active'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Created','8','Created','Created');



--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE 
--rollback     scale_id 
--rollback IN
--rollback     (SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS');
--rollback INSERT INTO
--rollback     master.scale_value (scale_id, value, order_number, description, display_name)
--rollback VALUES
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Draft','1','Draft','Draft'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Created','2','Created','Created'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Active','3','Active','Active'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'EXPERIMENT_STATUS'),'Completed','4','Completed','Completed');
    