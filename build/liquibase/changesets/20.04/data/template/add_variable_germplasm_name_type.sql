--liquibase formatted sql

--changeset postgres:add_variable_germplasm_name_type context:template splitStatements:false

DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('GERMPLASM_NAME_TYPE','Germplasm Name Type','Germplasm Name Type') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Germplasm Name Type','GERMPLASM_NAME_TYPE',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'GERMPLASM_NAME_TYPE' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('GERMPLASM_NAME_TYPE','categorical','Germplasm Name Type',NULL,NULL) 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'abbreviated_cultivar_name',1,'abbreviated_cultivar_name','abbreviated_cultivar_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'accession_id_in_other_genebank',2,'accession_id_in_other_genebank','accession_id_in_other_genebank');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'alternative_abbreviation',3,'alternative_abbreviation','alternative_abbreviation');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'alternative_cultivar_name',4,'alternative_cultivar_name','alternative_cultivar_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'alternative_derivative_name',5,'alternative_derivative_name','alternative_derivative_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'breeding_line_name',6,'breeding_line_name','breeding_line_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'collectors_number',7,'collectors_number','collectors_number');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'cross_name',8,'cross_name','cross_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'cultivar_name',9,'cultivar_name','cultivar_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'derivative_name',10,'derivative_name','derivative_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'donors_accession_id',11,'donors_accession_id','donors_accession_id');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'elite_lines',12,'elite_lines','elite_lines');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'germplasm_bank_accession_id',13,'germplasm_bank_accession_id','germplasm_bank_accession_id');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'gqnpc_unique_id',14,'gqnpc_unique_id','gqnpc_unique_id');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'international_testing_number',15,'international_testing_number','international_testing_number');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'line_name',16,'line_name','line_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'local_common_name',17,'local_common_name','local_common_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'management_name',18,'management_name','management_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'metcode',19,'metcode','metcode');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'met_stage_and_ecosystem',20,'met_stage_and_ecosystem','met_stage_and_ecosystem');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'met_stage_and_ecosystem_2',21,'met_stage_and_ecosystem_2','met_stage_and_ecosystem_2');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'met_stage_and_ecosystem_3',22,'met_stage_and_ecosystem_3','met_stage_and_ecosystem_3');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'national_testing_number',23,'national_testing_number','national_testing_number');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'release_name',24,'release_name','release_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'seed_stock_id_for_bims_-_temporary_altenate_name',25,'seed_stock_id_for_bims_-_temporary_altenate_name','seed_stock_id_for_bims_-_temporary_altenate_name');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'temporary_quarantine_id',26,'temporary_quarantine_id','temporary_quarantine_id');

	UPDATE master.scale SET min_value='abbreviated_cultivar_name', max_value='temporary_quarantine_id' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Germplasm Name Type','Germplasm Name Type','character varying','Type of name designated to the germplasm ','Germplasm Name Type','True','GERMPLASM_NAME_TYPE','study','identification',NULL) 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'GERMPLASM_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'GERMPLASM_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('GERMPLASM_INFO','Germplasm Info') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$

--rollback --ALTER TABLE master.scale_value DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE');
--rollback --ALTER TABLE master.scale_value ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.scale DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE');
--rollback --ALTER TABLE master.scale ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master."property" DISABLE TRIGGER ALL;
--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE');
--rollback --ALTER TABLE master."property" ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.method DISABLE TRIGGER ALL;
--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE');
--rollback --ALTER TABLE master.method ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set_member DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE');
--rollback --ALTER TABLE master.variable_set_member ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set WHERE abbrev = 'GERMPLASM_INFO';
--rollback --ALTER TABLE master.variable_set ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'GERMPLASM_NAME_TYPE';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
