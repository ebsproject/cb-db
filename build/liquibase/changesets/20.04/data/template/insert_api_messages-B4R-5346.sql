--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5346 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5346



select api.insert_messages(
400,
'ERR',
'Required parameters are missing. Ensure that measurementVariables fields are not empty.',
'Invalid format, measurementVariables must be an array.',
'The terminal transaction ID does not exist.',
'There is at least one(1) invalid variable in measurementVariables field.',
'The dataset cannot be created. Ensure that the status of the terminal transaction ID is uploaded, uploading in progress, or validated.',
'There is at least one(1) plot that do not exist in the location.'
);



--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that measurementVariables fields are not empty.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format, measurementVariables must be an array.';
--rollback DELETE FROM api.messages WHERE message = 'The terminal transaction ID does not exist.';
--rollback DELETE FROM api.messages WHERE message = 'There is at least one(1) invalid variable in measurementVariables field.';
--rollback DELETE FROM api.messages WHERE message = 'The dataset cannot be created. Ensure that the status of the terminal transaction ID is uploaded, uploading in progress, or validated.';
--rollback DELETE FROM api.messages WHERE message = 'There is at least one(1) plot that do not exist in the location.';