--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5348 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5348



SELECT api.insert_messages(
    400,
    'ERR',
    'The provided processName is invalid. ProcessName should be one of the following: DataCollectionUpload',
    'Invalid format, transactionDbId must be an integer.',
    'Required parameters are missing. Ensure that transactionDbId and processName fields are not empty.'
);



--rollback DELETE FROM api.messages WHERE message = 'The provided processName is invalid. ProcessName should be one of the following: DataCollectionUpload';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format, transactionDbId must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that transactionDbId and processName fields are not empty.';



