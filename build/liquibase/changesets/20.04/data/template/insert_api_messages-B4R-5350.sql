--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5350 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5350



SELECT api.insert_messages(
    400,
    'ERR',
    'The transaction status is invalid. Status should be one of the following: uploaded, invalid, committed, suppressed',
    'Required parameters are missing. Ensure that transactionDbId, status, and dataset fields are not empty.',
    'The transaction file status is invalid. Status is empty.'
);



--rollback DELETE FROM api.messages WHERE message = 'The transaction status is invalid. Status should be one of the following: uploaded, invalid, committed, suppressed';
--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that transactionDbId, status, and dataset fields are not empty.';
--rollback DELETE FROM api.messages WHERE message = 'The transaction file status is invalid. Status is empty.';