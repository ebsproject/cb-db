--liquibase formatted sql

--changeset postgres:update_harvest_manager_filters_config_value_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update harvest_manager_filters config_value in platform.config



UPDATE 
    platform.config 
SET 
    config_value=$$
    {
        "name": "Harvest Manager Filters",
        "values": [
            {
                "disabled": "true",
                "required": "false",
                "input_type": "single",
                "field_label": "Breeding Program",
                "input_field": "selection",
                "order_number": "1",
                "default_value": "",
                "allowed_values": "",
                "basic_parameter": "true",
                "variable_abbrev": "PROGRAM",
                "field_description": "Program that manages the experiment"
            },
            {
                "disabled": "false",
                "required": "false",
                "input_type": "single",
                "field_label": "Experiment Name",
                "input_field": "selection",
                "order_number": "3",
                "default_value": "",
                "allowed_values": "",
                "basic_parameter": "true",
                "variable_abbrev": "EXPERIMENT_NAME",
                "field_description": "Name of the experiment"
            },
            {
                "disabled": "false",
                "required": "true",
                "input_type": "single",
                "field_label": "Experiment Location",
                "input_field": "selection",
                "order_number": "5",
                "default_value": "",
                "allowed_values": "",
                "basic_parameter": "true",
                "variable_abbrev": "LOCATION_NAME",
                "field_description": "Name of the Location"
            }
        ]
    }
$$
WHERE abbrev = 'HARVEST_MANAGER_FILTERS';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value =
--rollback     '
--rollback         {
--rollback             "name": "Harvest Manager Filters",
--rollback             "values": [
--rollback                     {
--rollback                         "disabled": "true",
--rollback                         "required": "false",
--rollback                         "input_type": "single",
--rollback                         "field_label": "Breeding Program",
--rollback                         "input_field": "selection",
--rollback                         "order_number": "1",
--rollback                         "target_table": "master.program",
--rollback                         "default_value": "",
--rollback                         "filter_column": "id",
--rollback                         "target_column": "name",
--rollback                         "allowed_values": "",
--rollback                         "basic_parameter": "true",
--rollback                         "variable_abbrev": "PROGRAM",
--rollback                         "reference_column": "program_id",
--rollback                         "field_description": "Program that manages the experiment",
--rollback                         "primary_attribute": "name",
--rollback                         "target_table_alias": "prog",
--rollback                         "secondary_attribute": "abbrev",
--rollback                         "secondary_target_table": "",
--rollback                         "secondary_target_column": ""
--rollback                     },
--rollback                     {
--rollback                         "disabled": "false",
--rollback                         "required": "false",
--rollback                         "input_type": "single",
--rollback                         "field_label": "Experiment Name",
--rollback                         "input_field": "selection",
--rollback                         "order_number": "3",
--rollback                         "target_table": "operational.experiment",
--rollback                     "default_value": "",
--rollback                     "filter_column": "id",
--rollback                     "target_column": "experiment_name",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "reference_column": "source_experiment_id",
--rollback                     "field_description": "Name of the experiment",
--rollback                     "primary_attribute": "",
--rollback                     "target_table_alias": "ex",
--rollback                     "secondary_attribute": "",
--rollback                     "secondary_target_table": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": "false",
--rollback                     "required": "true",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Experiment Loc Rep",
--rollback                     "input_field": "selection",
--rollback                     "order_number": "5",
--rollback                     "target_table": "operational.study",
--rollback                     "default_value": "",
--rollback                     "filter_column": "id",
--rollback                     "target_column": "study",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "STUDY_NAME",
--rollback                     "reference_column": "source_study_id",
--rollback                     "field_description": "Name of the study",
--rollback                     "primary_attribute": "",
--rollback                     "target_table_alias": "s",
--rollback                     "secondary_attribute": "",
--rollback                     "secondary_target_table": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": "false",
--rollback                     "required": "false",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Plot No(s)",
--rollback                     "input_field": "text",
--rollback                     "order_number": "7",
--rollback                     "target_table": "operational.plot",
--rollback                     "default_value": "",
--rollback                     "filter_column": "id",
--rollback                     "target_column": "plotno",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "PLOTNO",
--rollback                     "reference_column": "",
--rollback                     "field_description": "Plot Numbers",
--rollback                     "primary_attribute": "",
--rollback                     "target_table_alias": "op",
--rollback                     "secondary_attribute": "",
--rollback                     "secondary_target_table": "",
--rollback                     "secondary_target_column": ""
--rollback                 },
--rollback                 {
--rollback                     "disabled": "false",
--rollback                     "required": "false",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Rep",
--rollback                     "input_field": "text",
--rollback                     "order_number": "9",
--rollback                     "target_table": "operational.plot",
--rollback                     "default_value": "",
--rollback                     "filter_column": "id",
--rollback                     "target_column": "rep",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "REPLICATION_NUMBER",
--rollback                     "reference_column": "",
--rollback                     "field_description": "Replication Number",
--rollback                     "primary_attribute": "",
--rollback                     "target_table_alias": "op",
--rollback                     "secondary_attribute": "",
--rollback                     "secondary_target_table": "",
--rollback                     "secondary_target_column": ""
--rollback                 }
--rollback             ],
--rollback             "main_table": "",
--rollback             "main_table_alias": "ss",
--rollback             "main_table_primary_key": "id"
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'HARVEST_MANAGER_FILTERS';