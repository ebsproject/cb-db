--liquibase formatted sql

--changeset postgres:update_pipeline_target_model_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update pipeline target_model in master.variable



UPDATE
    master.variable
SET
    target_model = 'Pipeline'
WHERE
    abbrev = 'PIPELINE';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     target_model = NULL
--rollback WHERE
--rollback     abbrev = 'PIPELINE';
