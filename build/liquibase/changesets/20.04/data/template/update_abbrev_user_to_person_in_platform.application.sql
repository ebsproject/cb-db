--liquibase formatted sql

--changeset postgres:update_abbrev_user_to_person_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update abbrev user to person in platform.application



UPDATE 
    platform.application 
SET 
    abbrev = 'PERSONS' where abbrev = 'USERS';



--rollback UPDATE 
--rollback     platform.application 
--rollback SET 
--rollback     abbrev = 'USERS' where abbrev = 'PERSONS';

