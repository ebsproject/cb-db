--liquibase formatted sql

--changeset postgres:insert_schema_in_dictionary.schema context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5711 Insert schema in dictionary.schema



INSERT INTO 
    dictionary.schema (database_id, abbrev, name, creator_id)
VALUES 
    (1, 'DATA_TERMINAL', 'data_terminal', 1),
    (1, 'DICTIONARY', 'dictionary', 1),
    (1, 'EXPERIMENT', 'experiment', 1),
    (1, 'GERMPLASM', 'germplasm', 1),
    (1, 'PLACE', 'place', 1),
    (1, 'PLATFORM', 'platform', 1),
    (1, 'SEED_WAREHOUSE', 'seed_warehouse', 1),
    (1, 'TENANT', 'tenant', 1);



--rollback DELETE FROM dictionary.schema WHERE abbrev IN ('DATA_TERMINAL','DICTIONARY','EXPERIMENT','GERMPLASM','PLACE','PLATFORM','SEED_WAREHOUSE','TENANT');
