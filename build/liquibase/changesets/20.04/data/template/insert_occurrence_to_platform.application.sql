--liquibase formatted sql

--changeset postgres:insert_occurrence_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Insert occurrence to platform.application



INSERT INTO 
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES
    ('OCCURRENCES', 'Occurrences', 'Occurrences', 'settings_applications', 1);



--rollback DELETE FROM platform.application WHERE abbrev = 'OCCURRENCES';
