--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5460 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5460



SELECT api.insert_messages(
    404,
    'ERR',
    'The dataset records you have requested do not exist.'
);



--rollback DELETE FROM api.messages WHERE message = 'The dataset records you have requested do not exist.';
