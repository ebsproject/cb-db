--liquibase formatted sql

--changeset postgres:update_variable_program_id_display_name_target_model_target_table context:template splitStatements:false
SET session_replication_role = replica;
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Program', target_model='Program', target_table='tenant.program' WHERE abbrev = 'PROGRAM_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Program Id', target_model=NULL, target_table=NULL WHERE abbrev = 'PROGRAM_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;

--changeset postgres:update_variable_pipeline_id_display_name context:template splitStatements:false
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Pipeline' WHERE abbrev = 'PIPELINE_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Pipeline Id' WHERE abbrev = 'PIPELINE_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;

--changeset postgres:update_variable_stage_id_display_name_target_model_target_table context:template splitStatements:false
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Stage', target_model='Stage', target_table='tenant.stage' WHERE abbrev = 'STAGE_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Stage ID', target_model=NULL, target_table=NULL WHERE abbrev = 'STAGE_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;

--changeset postgres:update_variable_project_id_display_name_target_model_target_table context:template splitStatements:false
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Project', target_model='Project', target_table='tenant.project' WHERE abbrev = 'PROJECT_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Project ID', target_model=NULL, target_table=NULL WHERE abbrev = 'PROJECT_ID';
--rollbackA LTER TABLE master.variable ENABLE TRIGGER ALL;

--changeset postgres:update_variable_season_id_display_name_target_model_target_table context:template splitStatements:false
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Season', target_model='Season', target_table='tenant.season' WHERE abbrev = 'SEASON_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Season Id', target_model=NULL, target_table=NULL WHERE abbrev = 'SEASON_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;

--changeset postgres:update_variable_steward_id_display_name_target_model_target_table context:template splitStatements:false
--ALTER TABLE master.variable DISABLE TRIGGER ALL;
UPDATE master.variable SET display_name = 'Steward', target_model='Person', target_table='tenant.person' WHERE abbrev = 'STEWARD_ID';
--ALTER TABLE master.variable ENABLE TRIGGER ALL;
SET session_replication_role = origin;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback UPDATE master.variable SET display_name = 'Steward ID', target_model=NULL, target_table=NULL WHERE abbrev = 'STEWARD_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;







