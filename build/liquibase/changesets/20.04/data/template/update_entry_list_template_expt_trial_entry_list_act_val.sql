--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_trial_entry_list_act_val context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry list template EXPT_TRIAL_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for trial data process",
            "Values": 
            [
                {
                    "default": "entry",
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_TYPE"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_TRIAL_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for trial data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": "entry",
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Entry Role",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "ENTRY_TYPE",
--rollback                     "field_description": "Entry Type"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Entry Class",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "ENTRY_CLASS",
--rollback                     "field_description": "Entry Class"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 3,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Entry Remarks"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'EXPT_TRIAL_ENTRY_LIST_ACT_VAL';