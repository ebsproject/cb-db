--liquibase formatted sql

--changeset postgres:add_variable_entry_role context:template splitStatements:false

DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'ENTRY_ROLE' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'ENTRY_ROLE' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('ENTRY_ROLE','Entry Role','Entry Role') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'ENTRY_ROLE' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'ENTRY_ROLE' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('Entry Role','ENTRY_ROLE',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'ENTRY_ROLE' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'ENTRY_ROLE' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('ENTRY_ROLE','categorical','Entry Role',NULL,NULL) 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'female',1,'female','female');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'male',2,'male','male');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'female-and-male',3,'female-and-male','female-and-male');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'spatial check',4,'spatial check','spatial check');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name) 
	VALUES 
		(var_scale_id,'performance check',5,'performance check','performance check');

	UPDATE master.scale SET min_value='female', max_value='performance check' WHERE id=var_scale_id;

	--VARIABLE

		INSERT INTO
			master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
		VALUES 
			('active','Entry Role','Entry Role','character varying','Function of the entry in the experiment, which may depend on the type of the experiment','Entry Role','False','ENTRY_ROLE','study','identification',NULL) 
		RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'EXPERIMENT_CREATION' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('EXPERIMENT_CREATION','Experiment Creation') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$

--rollback --ALTER TABLE master.scale_value DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE');
--rollback --ALTER TABLE master.scale_value ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.scale DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE');
--rollback --ALTER TABLE master.scale ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master."property" DISABLE TRIGGER ALL;
--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE');
--rollback --ALTER TABLE master."property" ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.method DISABLE TRIGGER ALL;
--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'ENTRY_ROLE');
--rollback --ALTER TABLE master.method ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set_member DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'ENTRY_ROLE');
--rollback --ALTER TABLE master.variable_set_member ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set WHERE abbrev = 'EXPERIMENT_CREATION';
--rollback --ALTER TABLE master.variable_set ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'ENTRY_ROLE';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
