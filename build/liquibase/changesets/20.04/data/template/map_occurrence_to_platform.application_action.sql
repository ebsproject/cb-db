--liquibase formatted sql

--changeset postgres:map_occurrence_to_platform.application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Map occurrence to platform.application_action



INSERT INTO 
    platform.application_action (application_id, module, creator_id)
SELECT 
	id,
	'occurrence',
	1
FROM 
	platform.application
WHERE 
	abbrev = 'OCCURRENCES';



--rollback DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev = 'OCCURRENCES');