--liquibase formatted sql

--changeset postgres:update_navigation_for_default_users_in_platform.space context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update navigation for default users in platform.space



UPDATE 
    platform.space 
SET 
    menu_data = 
    '
        {
            "left_menu_items": 
                [
                    {
                    "name": "experiment-creation",
                    "label": "Experiments",
                    "items": 
                        [
                            
                            {
                                "name": "experiment-creation-experiments",
                                "label": "Experiments",
                                "appAbbrev": "EXPERIMENT_CREATION"
                            }, 
                            {
                                "name": "experiment-creation-occurrences",
                                "label": "Occurrences",
                                "appAbbrev": "OCCURRENCES"
                            }
                        ]
                    }, 
                    {
                        "name": "data-collection-qc",
                        "items": 
                            [
                                {
                                    "name": "data-collection-qc-quality-control",
                                    "label": "Quality control",
                                    "appAbbrev": "QUALITY_CONTROL"
                                }
                            ],
                        "label": "Data collection & QC"
                    }, 
                    {
                        "name": "seeds",
                        "items": 
                            [
                                {
                                "name": "seeds-find-seeds",
                                "label": "Find seeds",
                                "appAbbrev": "FIND_SEEDS"
                                }, 
                                {
                                    "name": "seeds-harvest-manager",
                                    "label": "Harvest manager",
                                    "appAbbrev": "HARVEST_MANAGER"
                                }
                            ],
                        "label": "Seeds"
                    }
                        
                ]
        }
    '
WHERE abbrev = 'DEFAULT';



--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiments",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "planning",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "planning-draft-studies",
--rollback                             "label": "Draft studies",
--rollback                             "appAbbrev": "DRAFT_STUDIES"
--rollback                         },
--rollback                         {
--rollback                             "name": "planning-layouts",
--rollback                             "label": "Layouts",
--rollback                             "appAbbrev": "LAYOUTS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Planning"
--rollback                 },
--rollback                 {
--rollback                     "name": "operational-studies",
--rollback                     "label": "Operational studies",
--rollback                     "appAbbrev": "OPERATIONAL_STUDIES"
--rollback                 },
--rollback                 {
--rollback                     "name": "tasks",
--rollback                     "label": "Tasks",
--rollback                     "appAbbrev": "TASKS"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'DEFAULT';
