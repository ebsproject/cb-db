--liquibase formatted sql

--changeset postgres:insert_germplasm_catalog_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Insert germplasm_catalog to platform.application



INSERT INTO 
    platform.application (abbrev, label, action_label, icon, creator_id)
VALUES 
    ('GERMPLASM_CATALOG', 'Germplasm', 'Browse Germplasm', 'assignment', 1);



--rollback DELETE FROM platform.application WHERE abbrev = 'GERMPLASM_CATALOG';