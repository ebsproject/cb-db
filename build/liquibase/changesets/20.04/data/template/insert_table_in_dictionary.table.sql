--liquibase formatted sql

--changeset postgres:insert_table_in_dictionary.table context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5711 Insert table in dictionary.table



INSERT INTO 
    dictionary.table (database_id, schema_id, abbrev, name, creator_id)
VALUES 
    (1, (SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'), 'GERMPLASM', 'germplasm', 1),
    (1, (SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'), 'SEED', 'seed', 1), 
    (1, (SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'), 'OCCURRENCE', 'occurrence', 1), 
    (1, (SELECT id FROM dictionary.schema WHERE abbrev='PLACE'), 'GEOSPATIAL_OBJECT', 'geospatial_object', 1);



--rollback DELETE FROM dictionary.table WHERE abbrev in ('GERMPLASM','SEED','OCCURRENCE','GEOSPATIAL_OBJECT');
