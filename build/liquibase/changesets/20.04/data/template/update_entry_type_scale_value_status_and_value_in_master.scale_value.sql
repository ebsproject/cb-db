--liquibase formatted sql

--changeset postgres:update_entry_type_scale_value_status_and_value_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry_type scale_value_status and value in master.scale_value



UPDATE 
    master.scale_value
SET
    value = 'entry',
    scale_value_status = 'show'
WHERE 
    value = 'Entry';

UPDATE 
    master.scale_value
SET
    value = 'check',
    scale_value_status = 'show'
WHERE 
    value = 'Check';

UPDATE 
    master.scale_value
SET
    value = 'control',
    scale_value_status = 'hide'
WHERE 
    value = 'Control';

UPDATE 
    master.scale_value
SET
    value = 'filler',
    scale_value_status = 'hide'
WHERE 
    value = 'Filler';

UPDATE 
    master.scale_value
SET
    value = 'parent',
    scale_value_status = 'hide'
WHERE 
    value = 'Parent';

UPDATE 
    master.scale_value
SET
    value = 'cross',
    scale_value_status = 'hide'
WHERE 
    value = 'Cross';



--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Entry',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'entry';
--rollback 
--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Check',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'check';
--rollback 
--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Control',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'control';
--rollback 
--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Filler',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'filler';
--rollback 
--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Parent',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'parent';
--rollback 
--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Cross',
--rollback     scale_value_status = NULL
--rollback WHERE 
--rollback     value = 'cross';
