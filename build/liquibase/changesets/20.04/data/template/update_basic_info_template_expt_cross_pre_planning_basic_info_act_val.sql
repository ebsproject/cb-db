--liquibase formatted sql

--changeset postgres:update_basic_info_template_expt_cross_pre_planning_basic_info_act_val context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update basic info template EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required experiment level metadata variables for cross pre-planning data process",
            "Values": 
            [
                {
                    "default": false,
                    "disabled": true,
                    "required": "required",
                    "target_column": "dataProcessDbId",
                    "secondary_target_column":"itemDbId",
                    "target_value":"name",
                    "api_resource_method" : "POST",
                    "api_resource_endpoint" : "items-search",
                    "api_resource_filter" : {
                        "processType": "experiment_creation_data_process","abbrev":"not equals EXPT_DEFAULT_DATA_PROCESS"},
                        "api_resource_sort": "?sort=name", 
                        "variable_type" : "identification",
                        "variable_abbrev": "EXPERIMENT_TEMPLATE"
                },
                {
                    "default": false,
                    "disabled": true,
                    "required": "required",
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_TYPE"
                },
                {
                    "default": "RICE",
                    "disabled": true,
                    "required": "required",
                    "target_column": "cropDbId",
                    "target_value" : "cropCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "crops",
                    "api_resource_sort": "?sort=cropCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "CROP"
                },
                {
                    "disabled": true,
                    "required": "required",
                    "target_column": "programDbId",
                    "target_value":"programCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "programs",
                    "api_resource_sort": "?sort=programCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "PROGRAM"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "target_column": "pipelineDbId",
                    "target_value":"pipelineCode",
                    "api_resource_method" : "GET",
                    "api_resource_endpoint" : "pipelines",
                    "api_resource_sort": "?sort=pipelineCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "PIPELINE"
                },
                {
                    "disabled": false,
                    "allow_new_val": true,
                    "target_column": "projectDbId",
                    "target_value": "projectCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "projects",
                    "api_resource_sort": "?sort=projectCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "PROJECT"
                },
                {
                    "default" : "EXPT-XXXX",
                    "disabled": true,
                    "required": "required",
                            "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_CODE"
                },
                {
                    "disabled": false,
                    "required": "required",
                        "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_NAME"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_OBJECTIVE"
                },
                {
                    "default": "HB",
                    "disabled": false,
                    "required": "required",
                    "allowed_values": [
                        "HB",
                        "F1"
                    ],
                    "target_column": "stageDbId",
                    "target_value":"stageCode",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "stages-search",
                    "api_resource_sort": "?sort=stageCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "STAGE"
                },
                {
                    "disabled": false,
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_SUB_TYPE"
                },
                {
                    "default": "Crossing Block",
                    "disabled": false,
                    "allowed_values": [
                    "Crossing Block"
                    ],
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_SUB_SUB_TYPE"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_YEAR"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "target_column": "seasonDbId",
                    "target_value":"seasonCode",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "seasons",
                    "api_resource_sort": "?sort=seasonCode",
                    "variable_type" : "identification",
                    "variable_abbrev": "SEASON"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "variable_type" : "identification",
                    "variable_abbrev": "PLANTING_SEASON"
                },
                {
                    "disabled": false,
                    "required": "required",
                    "target_column": "stewardDbId",
                    "secondary_target_column": "personDbId",
                    "target_value":"personName",
                    "api_resource_method": "GET",
                    "api_resource_endpoint": "persons",
                    "api_resource_sort": "?sort=personName",
                    "variable_type" : "identification",
                    "variable_abbrev": "EXPERIMENT_STEWARD"
                },
                {
                    "disabled": false,
                    "variable_type": "identification",
                    "variable_abbrev": "DESCRIPTION"
                }
            ]
        }
    ' 
WHERE abbrev= 'EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level metadata variables for cross pre-planning data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment Template",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "EXPERIMENT_TEMPLATE",
--rollback                     "field_description": "Experiment Template"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment Type",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "EXPERIMENT_TYPE",
--rollback                     "field_description": "Experiment Type"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Crop",
--rollback                     "order_number": 3,
--rollback                     "variable_abbrev": "CROP",
--rollback                     "field_description": "Program Crop"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Program",
--rollback                     "order_number": 4,
--rollback                     "variable_abbrev": "PROGRAM",
--rollback                     "field_description": "Program"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Project",
--rollback                     "order_number": 5,
--rollback                     "allow_new_val": true,
--rollback                     "variable_abbrev": "PROJECT",
--rollback                     "field_description": "Project where the experiment is under"
--rollback                 },
--rollback                 {
--rollback                     "disabled": true,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment Code",
--rollback                     "order_number": 6,
--rollback                     "variable_abbrev": "EXPERIMENT_CODE",
--rollback                     "field_description": "Auto-generated code for the experiment"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment Name",
--rollback                     "order_number": 7,
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "field_description": "User inputted name for the experiment"
--rollback                 },
--rollback                 {
--rollback                     "default": "HB",
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment sub-type",
--rollback                     "order_number": 8,
--rollback                     "allowed_values": [
--rollback                         "HB",
--rollback                         "F1"
--rollback                     ],
--rollback                     "variable_abbrev": "PHASE",
--rollback                     "field_description": "Experiment Sub-type/Phase of the experiment"
--rollback                 },
--rollback                 {
--rollback                     "default": "Crossing Block",
--rollback                     "disabled": false,
--rollback                     "field_label": "Experiment Sub Sub-type",
--rollback                     "order_number": 9,
--rollback                     "allowed_values": [
--rollback                         "Crossing Block"
--rollback                     ],
--rollback                     "variable_abbrev": "EXPERIMENT_SUB_SUBTYPE",
--rollback                     "field_description": "Experiment Sub Sub-type/Objective of the experiment"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Evaluation Year",
--rollback                     "order_number": 10,
--rollback                     "variable_abbrev": "YEAR",
--rollback                     "field_description": "Evaluation Year"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Evaluation Season",
--rollback                     "order_number": 11,
--rollback                     "variable_abbrev": "SEASON",
--rollback                     "field_description": "Evaluation Season"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Experiment Steward",
--rollback                     "order_number": 12,
--rollback                     "variable_abbrev": "EXPERIMENT_STEWARD",
--rollback                     "field_description": "Steward of the experiment"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 13,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Experiment Comment"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev= 'EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL';
