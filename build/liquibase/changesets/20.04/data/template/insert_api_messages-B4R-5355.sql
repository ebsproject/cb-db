--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5355 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5355



SELECT api.insert_messages(
    400,
    'ERR',
    'The file records you have requested do not exist.'
);



--rollback DELETE FROM api.messages WHERE message = 'The file records you have requested do not exist.';
