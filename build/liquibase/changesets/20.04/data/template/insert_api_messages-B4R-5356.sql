--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5356 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5356



SELECT api.insert_messages(
    400,
    'ERR',
    'The transaction file ID does not exist.'
);



--rollback DELETE FROM api.messages WHERE message = 'The transaction file ID does not exist.';

