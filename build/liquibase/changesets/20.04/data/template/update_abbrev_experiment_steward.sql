--liquibase formatted sql

--changeset postgres:update_abbrev_experiment_steward context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update abbrev experiment_steward



UPDATE
    master.variable
SET
    abbrev = 'EXPERIMENT_STEWARD_OLD',
    label = 'Experiment Status Old',
    name = 'Experiment Status Old',
    display_name = 'Experiment Steward Old'
WHERE
    abbrev = 'EXPERIMENT_STEWARD';

UPDATE
    master.variable
SET
    abbrev = 'EXPERIMENT_STEWARD',
    label = 'Experiment Steward',
    name = 'Experiment Steward',
    display_name = 'Experiment Steward'
WHERE
    abbrev = 'STEWARD_ID';

UPDATE
    master.scale
SET
    abbrev = 'EXPERIMENT_STEWARD',
    name = 'Experiment Steward',
    display_name = 'Experiment Steward'
WHERE
    abbrev = 'STEWARD_ID';

UPDATE
    master.property
SET
    abbrev = 'EXPERIMENT_STEWARD',
    name = 'Experiment Steward',
    display_name = 'Experiment Steward'
WHERE
    abbrev = 'STEWARD_ID';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'STEWARD_ID',
--rollback     label = 'Steward ID',
--rollback     name = 'Steward ID',
--rollback     display_name = 'Steward ID'
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_STEWARD';
--rollback 
--rollback UPDATE
--rollback     master.scale
--rollback SET
--rollback     abbrev = 'STEWARD_ID',
--rollback     name = 'Steward ID',
--rollback     display_name = 'Steward ID'
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_STEWARD';
--rollback 
--rollback UPDATE
--rollback     master."property"
--rollback SET
--rollback     abbrev = 'STEWARD_ID',
--rollback     name = 'Steward ID',
--rollback     display_name = 'Steward ID'
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_STEWARD';
