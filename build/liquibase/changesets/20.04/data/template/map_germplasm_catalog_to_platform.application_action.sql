--liquibase formatted sql

--changeset postgres:map_germplasm_catalog_to_platform.application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Map germplasm_catalog to platform.application_action



INSERT INTO 
    platform.application_action (application_id, module, controller,creator_id)
SELECT
	app.id,
	'germplasm',
	'default',
	1
FROM
	platform.application app
WHERE
	app.abbrev = 'GERMPLASM_CATALOG';



--rollback DELETE FROM platform.application_action WHERE application_id IN (SELECT id FROM platform.application WHERE abbrev = 'GERMPLASM_CATALOG');