--liquibase formatted sql

--changeset postgres:update_abbrev_project context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update abbrev project



UPDATE
    master.variable
SET
    abbrev = 'PROJECT_OLD',
    label = 'Project Old',
    name = 'Project Old'
WHERE
    abbrev = 'PROJECT';

UPDATE
    master.variable
SET
    abbrev = 'PROJECT',
    label = 'Project',
    name = 'Project',
    display_name = 'Project'
WHERE
    abbrev = 'PROJECT_ID';

UPDATE
    master.scale
SET
    abbrev = 'PROJECT',
    name = 'Project',
    display_name = 'Project'
WHERE
    abbrev = 'PROJECT_ID';

UPDATE
    master.property
SET
    abbrev = 'PROJECT',
    name = 'Project',
    display_name = 'Project'
WHERE
    abbrev = 'PROJECT_ID';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'PROJECT_ID',
--rollback     label = 'Project ID',
--rollback     name = 'Project ID',
--rollback     display_name = 'Project ID'
--rollback WHERE
--rollback     abbrev = 'PROJECT';
--rollback 
--rollback UPDATE
--rollback     master.scale
--rollback SET
--rollback     abbrev = 'PROJECT_ID',
--rollback     name = 'Project ID',
--rollback     display_name = 'Project ID'
--rollback WHERE
--rollback     abbrev = 'PROJECT';
--rollback 
--rollback UPDATE
--rollback     master."property"
--rollback SET
--rollback     abbrev = 'PROJECT_ID',
--rollback     name = 'Project ID',
--rollback     display_name = 'Project ID'
--rollback WHERE
--rollback     abbrev = 'PROJECT';
