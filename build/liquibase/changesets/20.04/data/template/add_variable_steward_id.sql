--liquibase formatted sql

--changeset postgres:add_variable_steward_id context:template splitStatements:false

DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
BEGIN

	--ALTER TABLE master.variable DISABLE TRIGGER ALL;
	SET session_replication_role = replica;

	--ADD PROPERTY

	INSERT INTO
		master.property (abbrev,display_name,name) 
	VALUES 
		('STEWARD_ID','Steward ID','Steward ID') 
	RETURNING id INTO var_property_id;

	--ADD METHOD

	INSERT INTO
		master.method (description) 
	VALUES 
		(NULL) 
	RETURNING id INTO var_method_id;

	--ADD SCALE

	INSERT INTO
		master.scale (abbrev,type,name,unit,level) 
	VALUES 
		('STEWARD_ID','categorical','Steward ID',NULL,'nominal') 
	RETURNING id INTO var_scale_id;

	--ADD VARIABLE

	INSERT INTO
		master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
	VALUES 
		('active','Steward ID','Steward ID','integer','Steward where the experiment belongs','Steward ID','True','STEWARD_ID','study','identification','experiment, study') 
	RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--GET VARIABLE_SET_ID

	SELECT id FROM master.variable_set WHERE abbrev = 'EXPERIMENT_CREATION' INTO var_variable_set_id;

	--GET THE LAST ORDER NUMBER

	SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );

	--ALTER TABLE master.variable ENABLE TRIGGER ALL;
	SET session_replication_role = origin;

END;
$$

--rollback --ALTER TABLE master."property" DISABLE TRIGGER ALL;
--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'STEWARD_ID');
--rollback --ALTER TABLE master."property" ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.method DISABLE TRIGGER ALL;
--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'STEWARD_ID');
--rollback --ALTER TABLE master.method ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.scale DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'STEWARD_ID');
--rollback --ALTER TABLE master.scale ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.scale_value DISABLE TRIGGER ALL;
--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'STEWARD_ID');
--rollback --ALTER TABLE master.scale_value ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable_set_member DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'STEWARD_ID');
--rollback --ALTER TABLE master.variable_set_member ENABLE TRIGGER ALL;

--rollback --ALTER TABLE master.variable DISABLE TRIGGER ALL;
--rollback DELETE FROM master.variable WHERE abbrev = 'STEWARD_ID';
--rollback --ALTER TABLE master.variable ENABLE TRIGGER ALL;
