--liquibase formatted sql

--changeset postgres:insert_entity_in_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5711 Insert entity in dictionary.entity



INSERT INTO 
    dictionary.entity (abbrev, name, creator_id, table_id)
VALUES
    ('GERMPLASM', 'germplasm', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'GERMPLASM')),
    ('SEED', 'seed', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'SEED')),
    ('OCCURRENCE', 'occurrence', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'OCCURRENCE')),
    ('GEOSPATIAL_OBJECT', 'geospatial_object', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'GEOSPATIAL_OBJECT'));



--rollback DELETE FROM dictionary.entity WHERE abbrev IN ('GERMPLASM','SEED','OCCURRENCE','GEOSPATIAL_OBJECT');



