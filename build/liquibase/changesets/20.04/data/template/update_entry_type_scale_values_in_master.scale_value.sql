--liquibase formatted sql

--changeset postgres:update_entry_type_scale_values_in_master.scale_value.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update entry_type scale values in master.scale_value



DELETE FROM 
    master.scale_value 
WHERE 
    scale_id 
IN
    (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE');

INSERT INTO
    master.scale_value (scale_id, value, order_number, description, display_name)
VALUES
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Entry','1','Entry','Entry'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Check','2','Check','Check'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Control','3','Control','Control'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Filler','4','Filler','Filler'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Parent','5','Parent','Parent'),
    ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Cross','6','Cross','Cross');



--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE 
--rollback     scale_id 
--rollback IN
--rollback     (SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE');
--rollback 
--rollback INSERT INTO
--rollback     master.scale_value (scale_id, value, order_number, description, display_name)
--rollback VALUES
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Entry','1','Entry','Entry'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Check','2','Check','Check'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Grid','3','Grid','Grid'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Border','4','Border','Border'),
--rollback     ((SELECT scale_id FROM master.variable WHERE abbrev = 'ENTRY_TYPE'),'Filler','5','Filler','Filler');