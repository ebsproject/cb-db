--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5352 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5352



SELECT api.insert_messages(
    400,
    'ERR',
    'The background process ID does not exist.',
    'The background process status is invalid. Status should not be empty.',
    'The background process status is invalid. Status should be one of the following: in progress, success, error' ,
    'Invalid format, isSeen must be boolean.',
    'Required parameters are missing. Ensure that the field, message, is not empty.',
    'The background process message is invalid. Message should not be empty.',
    'Invalid format, processId must be an integer.'
);



--rollback DELETE FROM api.messages WHERE message = 'The background process ID does not exist.';
--rollback DELETE FROM api.messages WHERE message = 'The background process status is invalid. Status should not be empty.';
--rollback DELETE FROM api.messages WHERE message = 'The background process status is invalid. Status should be one of the following: in progress, success, error';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format, isSeen must be boolean.';
--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that the field, message, is not empty.';
--rollback DELETE FROM api.messages WHERE message = 'The background process message is invalid. Message should not be empty.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format, processId must be an integer.';