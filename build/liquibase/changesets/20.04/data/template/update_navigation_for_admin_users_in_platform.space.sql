--liquibase formatted sql

--changeset postgres:update_navigation_for_admin_users_in_platform.space context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update navigation for admin users in platform.space



UPDATE 
    platform.space 
SET 
    menu_data = 
    '
        {
            "left_menu_items": 
                [
                    {
                    "name": "experiment-creation",
                    "label": "Experiments",
                    "items": 
                        [
                            {
                                "name": "experiment-creation-experiments",
                                "label": "Experiments",
                                "appAbbrev": "EXPERIMENT_CREATION"
                            }, 
                            {
                                "name": "experiment-creation-occurrences",
                                "label": "Occurrences",
                                "appAbbrev": "OCCURRENCES"
                            }
                        ]
                    }, 
                    {
                        "name": "data-collection-qc",
                        "items": 
                            [
                                {
                                    "name": "data-collection-qc-quality-control",
                                    "label": "Quality control",
                                    "appAbbrev": "QUALITY_CONTROL"
                                }
                            ],
                        "label": "Data collection & QC"
                    }, 
                    {
                        "name": "seeds",
                        "items": 
                            [
                                {
                                    "name": "seeds-find-seeds",
                                    "label": "Find seeds",
                                    "appAbbrev": "FIND_SEEDS"
                                }, 
                                {
                                    "name": "seeds-harvest-manager",
                                    "label": "Harvest manager",
                                    "appAbbrev": "HARVEST_MANAGER"
                                }
                            ],
                        "label": "Seeds"
                    }
                ],
                "main_menu_items": 
                    [
                        {
                            "icon": "folder_special",
                            "name": "data-management",
                            "items": 
                                [
                                    {
                                        "name": "data-management_seasons",
                                        "label": "Seasons",
                                        "appAbbrev": "MANAGE_SEASONS"
                                    }, 
                                    {
                                        "name": "data-management_germplasm",
                                        "label": "Germplasm",
                                        "appAbbrev": "GERMPLASM_CATALOG"
                                    }
                                ],
                            "label": "Data management"
                        }, 
                        {
                            "icon": "settings",
                            "name": "administration",
                            "items": 
                                [
                                    {
                                    "name": "administration_users",
                                    "label": "Persons",
                                    "appAbbrev": "PERSONS"
                                    }
                                ],
                            "label": "Administration"
                        }
                    ]
        }
    '
WHERE abbrev = 'ADMIN';



--rollback UPDATE 
--rollback     platform.space 
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiments",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "operational-studies",
--rollback                     "label": "Operational studies",
--rollback                     "appAbbrev": "OPERATIONAL_STUDIES"
--rollback                 },
--rollback                 {
--rollback                     "name": "tasks",
--rollback                     "label": "Tasks",
--rollback                     "appAbbrev": "TASKS"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-collection-qc-quality-control",
--rollback                             "label": "Quality control",
--rollback                             "appAbbrev": "QUALITY_CONTROL"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data collection & QC"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "seeds-find-seeds",
--rollback                             "label": "Find seeds",
--rollback                             "appAbbrev": "FIND_SEEDS"
--rollback                         },
--rollback                         {
--rollback                             "name": "seeds-harvest-manager",
--rollback                             "label": "Harvest manager",
--rollback                             "appAbbrev": "HARVEST_MANAGER"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Seeds"
--rollback                 }
--rollback             ],
--rollback             "main_menu_items": [
--rollback                 {
--rollback                     "icon": "folder_special",
--rollback                     "name": "data-management",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "data-management_seasons",
--rollback                             "label": "Seasons",
--rollback                             "appAbbrev": "MANAGE_SEASONS"
--rollback                         },
--rollback                         {
--rollback                             "name": "data-management_germplasm",
--rollback                             "label": "Germplasm",
--rollback                             "appAbbrev": "GERMPLASM_CATALOG"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Data management"
--rollback                 },
--rollback                 {
--rollback                     "icon": "settings",
--rollback                     "name": "administration",
--rollback                     "items": [
--rollback                         {
--rollback                             "name": "administration_users",
--rollback                             "label": "Users",
--rollback                             "appAbbrev": "USERS"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Administration"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'ADMIN';