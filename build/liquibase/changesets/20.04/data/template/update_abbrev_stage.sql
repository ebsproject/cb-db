--liquibase formatted sql

--changeset postgres:update_abbrev_stage context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5743 Update abbrev stage
--validCheckSum: 8:180ba0e2d7c590de5f68b1e25d39d472



UPDATE
    master.variable
SET
    abbrev = 'STAGE',
    label = 'Stage',
    name = 'Stage',
    display_name = 'Stage'
WHERE
    abbrev = 'STAGE_ID';

UPDATE
    master.scale
SET
    abbrev = 'STAGE',
    name = 'Stage',
    display_name = 'Stage'
WHERE
    abbrev = 'STAGE_ID';

UPDATE
    master.property
SET
    abbrev = 'STAGE',
    name = 'Stage',
    display_name = 'Stage'
WHERE
    abbrev = 'STAGE_ID';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'STAGE_ID',
--rollback     label = 'Stage ID',
--rollback     name = 'Stage ID',
--rollback     display_name = 'Stage ID'
--rollback WHERE
--rollback     abbrev = 'STAGE';
--rollback 
--rollback UPDATE
--rollback     master.scale
--rollback SET
--rollback     abbrev = 'STAGE_ID',
--rollback     name = 'Stage ID',
--rollback     display_name = 'Stage ID'
--rollback WHERE
--rollback     abbrev = 'STAGE';
--rollback
--rollback UPDATE
--rollback     master."property"
--rollback SET
--rollback     abbrev = 'STAGE_ID',
--rollback     name = 'Stage ID',
--rollback     display_name = 'Stage ID'
--rollback WHERE
--rollback     abbrev = 'STAGE';
