--liquibase formatted sql

--changeset postgres:insert_germplasm_query_fields_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5712 Insert GERMPLASM_QUERY_FIELDS to platform.config



-- delete existing config
DELETE FROM platform.config
WHERE abbrev = 'GERMPLASM_QUERY_FIELDS';

-- add new config
INSERT INTO platform.config (abbrev, "name", config_value, rank, usage, creator_id, creation_timestamp)
VALUES (
    'GERMPLASM_QUERY_FIELDS',
    'Query fields for germplasm entity',
    '{
    "name": "Germplasm Query Fields",
    "fields": {
        "basic": [
            {
                "label": "Germplasm State",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "input_type": "multiple",
                "description": "The state of the product",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "1",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "germplasm_state",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "GERMPLASM_STATE",
                "filter_key_input": "germplasmState",
                "reference_column": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "germplasmState",
                "target_filter_column": ""
            }
        ],
        "additional": [
            {
                "label": "Designation",
                "disabled": "false",
                "required": "false",
                "list_type": "designation|seed",
                "input_type": "multiple",
                "description": "Designation of the product",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "2",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "designation",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "DESIGNATION",
                "filter_key_input": "designation",
                "reference_column": "",
                "allow_filter_list": "true",
                "list_filter_field": "designation",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "designation",
                "target_filter_column": ""
            },
            {
                "label": "Parentage",
                "disabled": "false",
                "required": "false",
                "list_type": "designation|seed",
                "input_type": "multiple",
                "description": "The parentage of the product",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "3",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "parentage",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "PARENTAGE",
                "filter_key_input": "parentage",
                "reference_column": "",
                "allow_filter_list": "true",
                "list_filter_field": "parentage",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "parentage",
                "target_filter_column": ""
            },
            {
                "label": "Generation",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "input_type": "multiple",
                "description": "The generation of the product",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "4",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "generation",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "GENERATION",
                "filter_key_input": "generation",
                "reference_column": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "generation",
                "target_filter_column": ""
            },
            {
                "label": "Germplasm Name Type",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "input_type": "multiple",
                "description": "Name type of the germplasm",
                "input_field": "selection",
                "primary_key": "",
                "order_number": "5",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "name_type",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "GERMPLASM_NAME_TYPE",
                "filter_key_input": "germplasmNameType",
                "reference_column": "",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "germplasmNameType",
                "target_filter_column": ""
            },
            {
                "label": "Creator",
                "disabled": "false",
                "required": "false",
                "list_type": "",
                "input_type": "multiple",
                "description": "Name of the creator of the product",
                "input_field": "selection",
                "primary_key": "id",
                "order_number": "6",
                "target_table": "master.user",
                "default_value": "",
                "target_column": "display_name",
                "advanced_search": "true",
                "reference_table": "pr",
                "variable_abbrev": "CREATOR",
                "filter_key_input": "creator",
                "reference_column": "creator_id",
                "allow_filter_list": "false",
                "list_filter_field": "",
                "primary_attribute": "",
                "target_table_alias": "u",
                "secondary_attribute": "",
                "filter_key_selection": "creatorDbId",
                "target_filter_column": "id"
            },
            {
                "label": "Other Germplasm Names",
                "disabled": "false",
                "required": "false",
                "list_type": "designation|seed",
                "input_type": "multiple",
                "description": "Other names of the germplasm",
                "input_field": "input field",
                "primary_key": "",
                "order_number": "7",
                "target_table": "main_table",
                "default_value": "",
                "target_column": "otherNames",
                "advanced_search": "true",
                "reference_table": "",
                "variable_abbrev": "OTHER_NAME",
                "filter_key_input": "otherNames",
                "reference_column": "",
                "allow_filter_list": "true",
                "list_filter_field": "designation",
                "primary_attribute": "",
                "target_table_alias": "",
                "secondary_attribute": "",
                "filter_key_selection": "otherNames",
                "target_filter_column": ""
            }
        ]
    },
    "main_table": "germplasm.germplasm",
    "main_table_alias": "germ",
    "main_table_primary_key": "id"
}',
    1,
    'query_tool',
    1,
    now()
);

-- set entity_id of config
UPDATE platform.config
SET entity_id = (
        SELECT id
        FROM dictionary.entity
        WHERE abbrev = 'GERMPLASM'
    )
WHERE
    abbrev = 'GERMPLASM_QUERY_FIELDS'
;


--rollback DELETE FROM platform.config
--rollback WHERE abbrev = 'GERMPLASM_QUERY_FIELDS';
