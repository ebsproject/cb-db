--liquibase formatted sql

--changeset postgres:add_variable_stage_id context:template splitStatements:false

DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
BEGIN

	--ADD PROPERTY

	INSERT INTO
		master.property (abbrev,display_name,name) 
	VALUES 
		('STAGE_ID','Stage ID','Stage ID') 
	RETURNING id INTO var_property_id;

	--ADD METHOD

	INSERT INTO
		master.method (description) 
	VALUES 
		(NULL) 
	RETURNING id INTO var_method_id;

	--ADD SCALE

	INSERT INTO
		master.scale (abbrev,type,name,unit,level) 
	VALUES 
		('STAGE_ID','categorical','Stage ID',NULL,'nominal') 
	RETURNING id INTO var_scale_id;

	--ADD VARIABLE

	INSERT INTO
		master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
	VALUES 
		('active','Stage ID','Stage ID','integer','Stage where the experiment belongs','Stage ID','True','STAGE_ID','study','identification','experiment, study') 
	RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--GET VARIABLE_SET_ID

	SELECT id FROM master.variable_set WHERE abbrev = 'EXPERIMENT_CREATION' INTO var_variable_set_id;

	--GET THE LAST ORDER NUMBER

	SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );
END;
$$

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'STAGE_ID');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'STAGE_ID');

--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'STAGE_ID');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'STAGE_ID');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'STAGE_ID');

--rollback DELETE FROM master.variable WHERE abbrev = 'STAGE_ID';
