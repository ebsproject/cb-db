--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5344 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5344



SELECT api.insert_messages(
    400,
    'ERR',
    'Required parameters are missing. Ensure that locationDbId and action fields are not empty.',
    'Invalid format, locationDbId must be an integer.',
    'The provided locationDbId does not exist.',
    'The provided action is invalid. Action should be data_collection or remove_operational_data.',
    'User still has an open transaction on locationDbId.'
);



--rollback DELETE FROM api.messages WHERE message = 'Required parameters are missing. Ensure that locationDbId and action fields are not empty.';
--rollback DELETE FROM api.messages WHERE message = 'Invalid format, locationDbId must be an integer.';
--rollback DELETE FROM api.messages WHERE message = 'The provided locationDbId does not exist.';
--rollback DELETE FROM api.messages WHERE message = 'The provided action is invalid. Action should be data_collection or remove_operational_data.';
--rollback DELETE FROM api.messages WHERE message = 'User still has an open transaction on locationDbId.';

