--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_cross_post_planning_entry_list_act_val context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry list template EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for cross post-planning data process",
            "Values": 
                [
                    {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION"
                    }
                ]
        }
    ' 
where abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for cross post-planning data process",
--rollback             "Values": 
--rollback                 [
--rollback                     {
--rollback                         "disabled": false,
--rollback                         "field_label": "Remarks",
--rollback                         "order_number": 2,
--rollback                         "variable_abbrev": "REMARKS",
--rollback                         "field_description": "Entry Remarks"
--rollback                     }
--rollback                 ]
--rollback         }
--rollback     '
--rollback where abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';
