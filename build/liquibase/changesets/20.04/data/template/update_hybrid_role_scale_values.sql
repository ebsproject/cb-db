--liquibase formatted sql

--changeset postgres:update_hybrid_role_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5762 Update HYBRID_ROLE scale values



-- update scale values
UPDATE master.scale_value
SET display_name = 'Tester'
WHERE abbrev = 'HYBRID_ROLE_TESTER';

UPDATE master.scale_value
SET display_name = 'Platform'
WHERE abbrev = 'HYBRID_ROLE_PLATFORM';



--rollback UPDATE master.scale_value
--rollback SET display_name = NULL
--rollback WHERE abbrev = 'HYBRID_ROLE_TESTER';

--rollback UPDATE master.scale_value
--rollback SET display_name = NULL
--rollback WHERE abbrev = 'HYBRID_ROLE_PLATFORM';
