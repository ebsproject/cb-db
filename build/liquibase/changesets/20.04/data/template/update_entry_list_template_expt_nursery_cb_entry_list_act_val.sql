--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_nursery_cb_entry_list_act_val context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry list template EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default place metadata variables for nursery crossing block data process",
            "Values": 
            [
                {
                    "fixed": true,
                    "disabled": false,
                    "required": "required",
                    "variable_abbrev": "ENTRY_ROLE"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "ENTRY_CLASS"
                },
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default place metadata variables for nursery crossing block data process",
--rollback             "Values": [
--rollback                 {
--rollback                     "fixed": true,
--rollback                     "disabled": false,
--rollback                     "required": "required",
--rollback                     "field_label": "Parent Role",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "PARENT_TYPE",
--rollback                     "field_description": "Parent Role"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Entry Class",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "ENTRY_CLASS",
--rollback                     "field_description": "Entry Class"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 3,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Entry Remarks"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'EXPT_NURSERY_CB_ENTRY_LIST_ACT_VAL';