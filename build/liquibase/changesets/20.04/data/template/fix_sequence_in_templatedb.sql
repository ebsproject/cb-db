--liquibase formatted sql

--changeset postgres:add_variable_steward_id context:temp_fix_seq splitStatements:false
ALTER SEQUENCE master.scale_id_seq RESTART WITH 1965;
ALTER SEQUENCE master.property_id_seq RESTART WITH 959;
ALTER SEQUENCE master.method_id_seq RESTART WITH 20012;
ALTER SEQUENCE master.variable_id_seq RESTART WITH 2340;
ALTER SEQUENCE master.scale_value_id_seq RESTART WITH 2160;
ALTER SEQUENCE master.variable_set_member_id_seq RESTART WITH 18750;

--rollback select 1;