--liquibase formatted sql

--changeset postgres:insert_api_messages-B4R-5349 context:template splitStatements:false rollbackSplitStatements:false
--comment: Insert api message B4R-5349



SELECT api.insert_messages(
    400,
    'ERR',
    'The transaction cannot be validated. Ensure that the status of the terminal transaction ID is uploaded.'
);



--rollback DELETE FROM api.messages WHERE message = 'The transaction cannot be validated. Ensure that the status of the terminal transaction ID is uploaded.';




