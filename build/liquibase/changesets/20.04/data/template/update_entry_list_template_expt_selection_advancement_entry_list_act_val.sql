--liquibase formatted sql

--changeset postgres:update_entry_list_template_expt_selection_advancement_entry_list_act_val context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update entry list template EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "Name": "Required and default entry level metadata variables for Selection and Advancement data process",
            "Values": 
            [
                {
                    "disabled": false,
                    "variable_abbrev": "DESCRIPTION"
                }
            ]
        }
    ' 
WHERE abbrev = 'EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required and default entry level metadata variables for Selection and Advancement data process",
--rollback             "Values": 
--rollback             [
--rollback                 {
--rollback                     "fixed": true,
--rollback                     "default": "female-and-male",
--rollback                     "disabled": true,
--rollback                     "field_label": "Parent Role",
--rollback                     "order_number": 1,
--rollback                     "variable_abbrev": "PARENT_TYPE",
--rollback                     "field_description": "Parent Role"
--rollback                 },
--rollback                 {
--rollback                     "disabled": false,
--rollback                     "field_label": "Remarks",
--rollback                     "order_number": 2,
--rollback                     "variable_abbrev": "REMARKS",
--rollback                     "field_description": "Entry Remarks"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE abbrev = 'EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT_VAL';
