--liquibase formatted sql

--changeset postgres:update_api_resource_sort_value_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5723 Update api_resource_sort_value in platform.config



UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_TRIAL_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_NURSERY_CB_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_SELECTION_ADVANCEMENT_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL';

UPDATE 
	platform.config
SET 
	config_value = replace(config_value::TEXT,'"?sort=','"sort=')::jsonb 
WHERE 
	abbrev = 'EXPT_NURSERY_PARENT_LIST_BASIC_INFO_ACT_VAL';



--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_TRIAL_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_TRIAL_IRRI_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_NURSERY_CB_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_NURSERY_CROSS_LIST_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_CROSS_PRE_PLANNING_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_SEED_INCREASE_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_SELECTION_ADVANCEMENT_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_NURSERY_CROSS_LIST_IRRIHQ_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_SELECTION_ADVANCEMENT_IRRI_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_SEED_INCREASE_IRRI_BASIC_INFO_ACT_VAL';
--rollback 
--rollback UPDATE 
--rollback 	platform.config
--rollback SET 
--rollback 	config_value = replace(config_value::TEXT,'"sort=','"?sort=')::jsonb 
--rollback WHERE 
--rollback 	abbrev = 'EXPT_NURSERY_PARENT_LIST_BASIC_INFO_ACT_VAL';
