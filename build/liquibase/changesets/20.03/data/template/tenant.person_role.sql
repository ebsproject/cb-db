--liquibase formatted sql
 
--changeset postgres:populate_tenant_person_role_data context:template splitStatements:false
 


SET session_replication_role = replica;  

--ALTER TABLE tenant.person_role DISABLE TRIGGER person_role_event_log_tgr;
 
INSERT INTO
	tenant.person_role
	(id, person_role_code, person_role_name, description, creator_id)
 VALUES 
	(1, 'ADMIN', 'administrator', 'Authorized user who has full access to the entire system', '1'),
	(14, 'TM', 'member', 'Executes tasks and steps\n', '1'),
	(15, 'TL', 'Team Leader', '- Is responsible for management of a Process (financial and ...', '1'),
	(16, 'ETM', 'experienced team member', '- Is responsible for executing Process" daily Activities, Ta...', '1'),
	(18, 'DM', 'data manager', 'The role for data admin.', '1'),
	(21, 'B4R_USER', 'B4R User', 'Default role assigned to all Breeding4Rice users', '1'),
	(25, 'DATA_OWNER', 'data owner', 'Data owners are users who possess the ownership of a record....', '1'),
	(26, 'DATA_PRODUCER', 'data producer', '### Data producer\r\n\r\nData producers are users who are grante...', '1'),
	(27, 'DATA_CONSUMER', 'data consumer', 'Data consumers have the basic access to a record and that is...', '1');

--ALTER TABLE tenant.person_role ENABLE TRIGGER person_role_event_log_tgr;

SET session_replication_role = origin;



--rollback TRUNCATE TABLE tenant.person_role CASCADE;