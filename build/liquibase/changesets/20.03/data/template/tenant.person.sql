--liquibase formatted sql

--changeset postgres:insert_tenant.person_template_data context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Add tenant.person template data



INSERT INTO
    tenant.person
    (id, username, first_name, last_name, person_name, person_type, email, person_role_id, person_status, creator_id)
 VALUES 
    (0, 'postgres', 'Postgres', 'Postgres', 'Postgres, Postgres', 'admin', 'postgres', 1, 'active', '1'),
    (1, 'bims.irri', 'Bims', 'Irri', 'Irri, Bims', 'admin', 'bims.irri@gmail.com', 1, 'active', '1');



--rollback DELETE FROM tenant.person WHERE id in (0, 1);
