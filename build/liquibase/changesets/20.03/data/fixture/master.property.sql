--liquibase formatted sql

--changeset postgres:populate_master.property context:fixture splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Populate master.property



INSERT INTO master.property (id,abbrev,"name",description,display_name,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,ontology_id,bibliographical_reference,property_notes,event_log) VALUES 
(297,'RTD','rice tungro disease',NULL,NULL,NULL,'2014-03-26 13:44:46',1,'2020-04-21 02:36:58.897634',1,NULL,false,NULL,NULL,NULL,'[{"actor_id": "1", "new_data": {"abbrev": "RTD", "is_void": false}, "row_data": {"abbrev": "RTD_VOIDED", "is_void": true}, "action_type": "RESTORE", "log_timestamp": "2020-04-21 02:36:58.897634+08", "transaction_id": "1830241"}]'),
(645,'THR_SES4','Panicle threshability_SES4',NULL,NULL,'deprecated','2014-11-06 06:22:24.559883',1,'2020-04-21 02:36:58.921601',1,NULL,false,NULL,NULL,NULL,'[{"actor_id": "1", "new_data": {"abbrev": "THR_SES4", "is_void": false}, "row_data": {"abbrev": "THR_SES4_VOIDED", "is_void": true}, "action_type": "RESTORE", "log_timestamp": "2020-04-21 02:36:58.921601+08", "transaction_id": "1830244"}]'),
(641,'Sen_SES5','Leaf senescence_SES5',NULL,NULL,'deprecated','2014-11-06 03:02:54.352261',1,'2020-04-21 02:36:58.920617',1,NULL,false,NULL,NULL,NULL,'[{"actor_id": "1", "new_data": {"abbrev": "Sen_SES5", "is_void": false}, "row_data": {"abbrev": "Sen_SES5_VOIDED", "is_void": true}, "action_type": "RESTORE", "log_timestamp": "2020-04-21 02:36:58.920617+08", "transaction_id": "1830243"}]'),
(203,'Exs_CO','Panicle exsertion_CO',NULL,NULL,'verified by CParducho','2014-03-26 13:44:46',1,'2020-04-21 02:36:58.919767',1,NULL,false,NULL,NULL,NULL,'[{"actor_id": "1", "new_data": {"abbrev": "Exs_CO", "is_void": false}, "row_data": {"abbrev": "Exs_CO_VOIDED", "is_void": true}, "action_type": "RESTORE", "log_timestamp": "2020-04-21 02:36:58.919767+08", "transaction_id": "1830242"}]');



--rollback DELETE FROM master."property" WHERE abbrev IN ('RTD_PROP1', 'Exs_CO_PROP1', 'Sen_SES5_PROP1', 'THR_SES4_PROP1')