--liquibase formatted sql
 
--changeset postgres:populate_tenant_team_member_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.team_member DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.team_member
	(id, team_id, person_id, person_role_id, order_number, creator_id)
 VALUES 
	(1, 3, 6, 18, 1, '1'),
	(2, 3, 7, 16, 2, '1'),
	(3, 3, 10, 14, 3, '1'),
	(4, 3, 18, 18, 4, '1'),
	(5, 3, 27, 16, 5, '1'),
	(6, 3, 76, 14, 6, '1'),
	(7, 3, 81, 16, 7, '1'),
	(8, 3, 82, 16, 8, '1'),
	(9, 3, 111, 16, 9, '1'),
	(10, 3, 117, 16, 10, '1'),
	(11, 3, 124, 14, 11, '1'),
	(12, 3, 125, 14, 12, '1'),
	(13, 3, 141, 16, 13, '1'),
	(14, 3, 184, 16, 14, '1'),
	(15, 3, 189, 14, 15, '1'),
	(16, 3, 190, 14, 16, '1'),
	(17, 3, 191, 14, 17, '1'),
	(18, 3, 192, 14, 18, '1'),
	(19, 3, 221, 14, 19, '1'),
	(20, 3, 246, 15, 20, '1'),
	(21, 3, 249, 14, 21, '1'),
	(22, 3, 264, 16, 22, '1'),
	(23, 3, 266, 16, 23, '1'),
	(24, 3, 291, 14, 24, '1'),
	(25, 3, 319, 14, 25, '1'),
	(26, 3, 363, 16, 26, '1'),
	(27, 3, 389, 16, 27, '1'),
	(28, 3, 390, 14, 28, '1'),
	(29, 3, 391, 16, 29, '1'),
	(30, 3, 413, 14, 30, '1'),
	(31, 3, 417, 14, 31, '1'),
	(32, 3, 418, 14, 32, '1'),
	(33, 3, 419, 16, 33, '1'),
	(34, 3, 448, 16, 34, '1'),
	(35, 3, 455, 16, 35, '1'),
	(36, 3, 458, 16, 36, '1');

--ALTER TABLE tenant.team_member ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.team_member CASCADE;