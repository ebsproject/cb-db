--liquibase formatted sql
 
--changeset postgres:populate_germplasm_taxonomy context:fixture splitStatements:false
 
--ALTER TABLE germplasm.taxonomy DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	germplasm.taxonomy
	(id, taxon_id, taxonomy_name, description, crop_id, creator_id)
 VALUES 
	(1, '4530', 'Oryza sativa', NULL, 1, '1');

--ALTER TABLE germplasm.taxonomy ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE germplasm.taxonomy CASCADE;