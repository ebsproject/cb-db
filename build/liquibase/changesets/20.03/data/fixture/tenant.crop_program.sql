--liquibase formatted sql
 
--changeset postgres:populate_tenant_crop_program_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.crop_program DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.crop_program
	(id, crop_program_code, crop_program_name, description, organization_id, crop_id, creator_id)
 VALUES 
	(1, 'RICE_PROG', 'Rice program', 'Rice crop', 22, 1, '1');

--ALTER TABLE tenant.crop_program ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.crop_program CASCADE;