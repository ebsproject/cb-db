--liquibase formatted sql
 
--changeset postgres:populate_tenant_team_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.team DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.team
	(id, team_code, team_name, description, creator_id)
 VALUES 
	(3, 'IRSEA', 'Irrigated South-East Asia', 'Irrigated South East Asia pipeline team', '1');

--ALTER TABLE tenant.team ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.team CASCADE;