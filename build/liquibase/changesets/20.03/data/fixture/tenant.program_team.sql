--liquibase formatted sql
 
--changeset postgres:populate_tenant_program_team_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.program_team DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.program_team
	(id, program_id, team_id, order_number, creator_id)
 VALUES 
	(1, 101, 3, 1, '1');

--ALTER TABLE tenant.program_team ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.program_team CASCADE;