--liquibase formatted sql
 
--changeset postgres:populate_tenant_crop_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.crop DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.crop
	(id, crop_code, crop_name, description, creator_id)
 VALUES 
	(1, 'RICE', 'Rice', 'Rice crop', '1');

--ALTER TABLE tenant.crop ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.crop CASCADE;