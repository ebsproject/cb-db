--liquibase formatted sql
 
--changeset postgres:populate_tenant_season_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.season DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.season
	(id, season_code, season_name, description, creator_id)
 VALUES 
	(11, 'DS', 'Dry', 'Dry season', '1'),
	(12, 'WS', 'Wet', 'Wet season', '1'),
	(13, 'CS', 'Custom', 'Custom season', '1');

--ALTER TABLE tenant.season ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.season CASCADE;