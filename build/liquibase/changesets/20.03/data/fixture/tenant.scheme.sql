--liquibase formatted sql
 
--changeset postgres:populate_tenant_scheme_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.scheme DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.scheme
	(id, scheme_code, scheme_name, pipeline_id, description, creator_id)
 VALUES 
	(1, 'IRSEA_SCHEME_1', 'IRSEA Scheme 1', 101, NULL, '1'),
	(2, 'IRSEA_SCHEME_2', 'IRSEA Scheme 2', 101, NULL, '1'),
	(3, 'IRSEA_SCHEME_3', 'IRSEA Scheme 3', 101, NULL, '1'),
	(4, 'IRSEA_SCHEME_4', 'IRSEA Scheme 4', 101, NULL, '1');

--ALTER TABLE tenant.scheme ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.scheme CASCADE;