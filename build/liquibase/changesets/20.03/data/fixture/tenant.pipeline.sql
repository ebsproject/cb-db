--liquibase formatted sql
 
--changeset postgres:populate_tenant_pipeline_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.pipeline DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.pipeline
	(id, pipeline_code, pipeline_name, pipeline_status, description, creator_id)
 VALUES 
	(101, 'IRSEA_PIPELINE', 'Irrigated South-East Asia', 'active', 'Breeding Irrigated Rice for South East Asia', '1');

--ALTER TABLE tenant.pipeline ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.pipeline CASCADE;