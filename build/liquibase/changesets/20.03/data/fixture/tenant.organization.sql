--liquibase formatted sql
 
--changeset postgres:populate_tenant_organization_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.organization DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.organization
	(id, organization_code, organization_name, description, creator_id)
 VALUES 
	(22, 'IRRI', 'International Rice Research Institute', 'International Rice Research Institute', '1');

--ALTER TABLE tenant.organization ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.organization CASCADE;