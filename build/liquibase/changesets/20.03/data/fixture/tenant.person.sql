--liquibase formatted sql
 
--changeset postgres:populate_tenant_person_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.person DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.person
	(id, username, first_name, last_name, person_name, person_type, email, person_role_id, person_status, creator_id)
 VALUES 
	(4, 'l.gallardo', 'Larise', 'Gallardo', 'Gallardo, Larise', 'user', 'l.gallardo@irri.org', 21, 'active', '1'),
	(6, 'a.flores', 'Argem Gerald', 'Flores', 'Flores, Argem Gerald', 'user', 'a.flores@irri.org', 18, 'active', '1'),
	(7, 'jp.ramos', 'Jahzeel', 'Ramos', 'Ramos, Jahzeel', 'user', 'jp.ramos@irri.org', 16, 'active', '1'),
	(10, 'e.tenorio', 'Eugenia', 'Tenorio', 'Tenorio, Eugenia', 'user', 'e.tenorio@irri.org', 14, 'active', '1'),
	(18, 'm.lotho', 'Connie', 'Lotho', 'Lotho, Connie', 'user', 'm.lotho@irri.org', 18, 'active', '1'),
	(19, 'c.parducho', 'Consie', 'Parducho', 'Parducho, Consie', 'user', 'c.parducho@irri.org', 21, 'active', '1'),
	(27, 'r.santelices', 'Ronald', 'Santelices', 'Santelices, Ronald', 'user', 'r.santelices@irri.org', 16, 'active', '1'),
	(30, 'r.mendoza', 'Rhulyx', 'Mendoza', 'Mendoza, Rhulyx', 'user', 'r.mendoza@irri.org', 21, 'active', '1'),
	(53, 'j.bonifacio', 'Justine', 'Bonifacio', 'Bonifacio, Justine', 'user', 'j.bonifacio@irri.org', 21, 'active', '1'),
	(67, 'r.gannaban', 'Ritchel', 'Gannaban', 'Gannaban, Ritchel', 'user', 'r.gannaban@irri.org', 21, 'active', '1'),
	(76, 'j.pacia', 'Jocelyn', 'Pacia', 'Pacia, Jocelyn', 'user', 'j.pacia@irri.org', 14, 'active', '1'),
	(81, 'v.lopena', 'Vitaliano', 'Lopena', 'Lopena, Vitaliano', 'user', 'v.lopena@irri.org', 16, 'active', '1'),
	(82, 'h.verdeprado', 'Holden', 'Verdeprado', 'Verdeprado, Holden', 'user', 'h.verdeprado@irri.org', 16, 'active', '1'),
	(84, 'j.h.shim', 'Jung-Hyun', 'Shim', 'Shim, Jung-Hyun', 'user', 'j.h.shim@irri.org', 21, 'active', '1'),
	(111, 'r.murori', 'Rosemary', 'Murori', 'Murori, Rosemary', 'user', 'r.murori@irri.org', 16, 'active', '1'),
	(117, 'a.ndayiragije', 'Alexis', 'Ndayiragije', 'Ndayiragije, Alexis', 'user', 'a.ndayiragije@irri.org', 16, 'active', '1'),
	(124, 'j.bizimana', 'Jean Berchmans', 'Bizimana', 'Bizimana, Jean Berchmans', 'user', 'j.bizimana@irri.org', 14, 'active', '1'),
	(125, 'j.nduwimana', 'Julien', 'Nduwimana', 'Nduwimana, Julien', 'user', 'j.nduwimana@irri.org', 14, 'active', '1'),
	(141, 'j.ignacio', 'John Carlos', 'Ignacio', 'Ignacio, John Carlos', 'user', 'j.ignacio@irri.org', 16, 'active', '1'),
	(184, 'p.delacruz', 'Princess', 'Dela Cruz', 'Dela Cruz, Princess', 'user', 'p.delacruz@irri.org', 16, 'active', '1'),
	(189, 'm.mkuya', 'Mohamed Seleman', 'Mkuya', 'Mkuya, Mohamed Seleman', 'user', 'm.mkuya@irri.org', 14, 'active', '1'),
	(190, 'o.nyongesa', 'Oliver', 'Nyongesa', 'Nyongesa, Oliver', 'user', 'o.nyongesa@irri.org', 14, 'active', '1'),
	(191, 'a.matsinhe', 'Arlindo', 'Matsinhe', 'Matsinhe, Arlindo', 'user', 'a.matsinhe@irri.org', 14, 'active', '1'),
	(192, 'd.bigirimana', 'Donatien', 'Bigirimana', 'Bigirimana, Donatien', 'user', 'd.bigirimana@irri.org', 14, 'active', '1'),
	(221, 'c.venkateshwarlu', 'Challa', 'Venkateshwarlu', 'Venkateshwarlu, Challa', 'user', 'c.venkateshwarlu@irri.org', 14, 'active', '1'),
	(246, 'j.cobb', 'Joshua Nathaniel', 'Cobb', 'Cobb, Joshua Nathaniel', 'user', 'j.cobb@irri.org', 15, 'active', '1'),
	(249, 'r.b.javier', 'Romnick', 'Javier', 'Javier, Romnick', 'user', 'r.b.javier@irri.org', 14, 'active', '1'),
	(264, 'p.s.biswas', 'Partha Sarathi', 'Biswas', 'Biswas, Partha Sarathi', 'user', 'p.s.biswas@irri.org', 16, 'active', '1'),
	(266, 'j.arbelaezvelez', 'Juan David Arbelaez', 'Velez', 'Velez, Juan David Arbelaez', 'user', 'j.arbelaezvelez@irri.org', 16, 'active', '1'),
	(291, 'k.pranesh', 'Pranesh', 'K J', 'K J, Pranesh', 'user', 'k.pranesh@irri.org', 14, 'active', '1'),
	(319, 'r.juma', 'Roselyne Uside', 'Juma', 'Juma, Roselyne Uside', 'user', 'r.juma@irri.org', 14, 'active', '1'),
	(363, 'j.p.evangelista', 'Joel', 'Evangelista', 'Evangelista, Joel', 'user', 'j.p.evangelista@irri.org', 16, 'active', '1'),
	(389, 'r.kwayu', 'Rehema', 'Kwayu', 'Kwayu, Rehema', 'user', 'r.kwayu@irri.org', 16, 'active', '1'),
	(390, 'c.manalang', 'Cezar', 'Manalang', 'Manalang, Cezar', 'user', 'c.manalang@irri.org', 14, 'active', '1'),
	(391, 'j.bartholome', 'Jerome', 'Bartholome', 'Bartholome, Jerome', 'user', 'j.bartholome@irri.org', 16, 'active', '1'),
	(413, 's.kariuki', 'Simon', 'Njau', 'Njau, Simon', 'user', 's.kariuki@irri.org', 14, 'active', '1'),
	(417, 'p.saha', 'Parth Sarothi', 'Saha', 'Saha, Parth Sarothi', 'user', 'p.saha@irri.org', 14, 'active', '1'),
	(418, 'k.hossain', 'Kamal', 'Hossain', 'Hossain, Kamal', 'user', 'k.hossain@irri.org', 14, 'active', '1'),
	(419, 'k.santos', 'Kristina Cassandra ', 'Santos', 'Santos, Kristina Cassandra ', 'user', 'k.santos@irri.org', 16, 'active', '1'),
	(448, 'p.prakash', 'Parthiban', 'Prakash', 'Prakash, Parthiban', 'user', 'p.prakash@irri.org', 16, 'active', '1'),
	(455, 'f.niyongabo', 'Fulgence', 'Niyongabo', 'Niyongabo, Fulgence', 'user', 'f.niyongabo@irri.org', 16, 'active', '1'),
	(458, 'waseem.hussain', 'Waseem', 'Hussain', 'Hussain, Waseem', 'user', 'waseem.hussain@irri.org', 16, 'active', '1');

--ALTER TABLE tenant.person ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.person CASCADE;