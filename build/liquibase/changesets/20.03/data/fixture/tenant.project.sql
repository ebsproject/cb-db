--liquibase formatted sql
 
--changeset postgres:populate_tenant_project_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.project DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.project
	(id, project_code, project_name, project_status, description, program_id, pipeline_id, leader_id, creator_id)
 VALUES 
	(101, 'IRSEA_PROJECT', 'Irrigated South-East Asia Project', 'active', 'Breeding Irrigated Rice for South East Asia', 101, 101, 246, '1');

--ALTER TABLE tenant.project ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.project CASCADE;