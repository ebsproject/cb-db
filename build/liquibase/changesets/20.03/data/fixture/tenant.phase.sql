--liquibase formatted sql
 
--changeset postgres:populate_tenant_phase_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.phase DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.phase
	(id, phase_code, phase_name, description, pipeline_id, creator_id)
 VALUES 
	(1, 'IRSEA_PHASE_1', 'Selection and Sorting', NULL, 101, '1'),
	(2, 'IRSEA_PHASE_2', 'Testing and Characterization', NULL, 101, '1'),
	(3, 'IRSEA_PHASE_3', 'Validation and Delivery', NULL, 101, '1');

--ALTER TABLE tenant.phase ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.phase CASCADE;