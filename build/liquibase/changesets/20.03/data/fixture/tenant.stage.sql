--liquibase formatted sql
 
--changeset postgres:populate_tenant_stage_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.stage DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.stage
	(id, stage_code, stage_name, description, creator_id)
 VALUES 
	(101, 'OYT', 'Observation Yield Trial', 'Observation Yield Trial', '1'),
	(102, 'RYT', 'Replicated Yield Trial', 'Replicated Yield Trial', '1'),
	(103, 'AYT', 'Advanced Yield Trial', 'Advanced Yield Trial', '1'),
	(104, 'MET0', 'MET0 - Multi-Environment Yield Trial', 'MET0 - Multi-Environment Yield Trial', '1'),
	(105, 'HB', 'Hybridization', 'Hybridization', '1'),
	(106, 'F1', 'F1', 'F1', '1'),
	(107, 'RGA', 'RGA - Rapid Generation Advance', 'Rapid Generation Advance', '1'),
	(108, 'F2', 'F2', 'F2', '1'),
	(109, 'F3', 'F3', 'F3', '1'),
	(110, 'F4', 'F4', 'F4', '1'),
	(111, 'F5', 'F5', 'F5', '1'),
	(112, 'F6', 'F6', 'F6', '1'),
	(116, 'PYT', 'Preliminary yield trial', 'Preliminary yield trial', '1'),
	(128, 'MET1', 'MET1 - Multi-Environment Yield Trial', 'MET0 - Multi-Environment Yield Trial', '1'),
	(129, 'MET2', 'MET2 - Multi-Environment Yield Trial', 'MET2 - Multi-Environment Yield Trial', '1'),
	(130, 'BRE', 'Breeders Seed Production', 'The step in the breeding process that produces Breeders Seeds.', '1'),
	(131, 'PN', 'Pedigree Nursery', 'Pedigree Nursery', '1'),
	(132, 'AGR', 'Agronomical Traits', NULL, '1'),
	(135, 'SEM', 'Seed Increase', 'Seed Increase', '1'),
	(136, 'IYT', 'Initial Yield Trial', 'Initial Yield Trial', '1'),
	(138, 'NAT', 'Not applicable', 'Not applicable', '1');

--ALTER TABLE tenant.stage ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.stage CASCADE;