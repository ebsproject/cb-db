--liquibase formatted sql
 
--changeset postgres:populate_tenant_phase_scheme_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.phase_scheme DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.phase_scheme
	(id, phase_id, scheme_id, order_number, creator_id)
 VALUES 
	(1, 1, 1, 1, '1'),
	(2, 2, 2, 1, '1'),
	(3, 1, 2, 2, '1'),
	(4, 2, 3, 2, '1'),
	(5, 3, 4, 1, '1');

--ALTER TABLE tenant.phase_scheme ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.phase_scheme CASCADE;