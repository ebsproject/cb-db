--liquibase formatted sql
 
--changeset postgres:populate_tenant_scheme_stage_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.scheme_stage DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.scheme_stage
	(id, scheme_id, stage_id, order_number, creator_id)
 VALUES 
	(1, 1, 105, 1, '1'),
	(2, 2, 106, 2, '1'),
	(3, 2, 105, 1, '1'),
	(4, 3, 112, 5, '1'),
	(5, 3, 111, 4, '1'),
	(6, 3, 110, 3, '1'),
	(7, 3, 109, 2, '1'),
	(8, 3, 108, 1, '1'),
	(9, 4, 129, 6, '1'),
	(10, 4, 128, 5, '1'),
	(11, 4, 104, 4, '1'),
	(12, 4, 103, 3, '1'),
	(13, 4, 116, 2, '1'),
	(14, 4, 101, 1, '1');

--ALTER TABLE tenant.scheme_stage ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.scheme_stage CASCADE;