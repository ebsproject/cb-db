--liquibase formatted sql
 
--changeset postgres:populate_tenant_program_data context:fixture splitStatements:false
 
--ALTER TABLE tenant.program DISABLE TRIGGER ALL;
SET session_replication_role = replica;
 
INSERT INTO
	tenant.program
	(id, program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
 VALUES 
	(101, 'IRSEA', 'Irrigated South-East Asia', 'breeding', 'active', 'Breeding Irrigated Rice for South East Asia', 1, '1');

--ALTER TABLE tenant.program ENABLE TRIGGER ALL;
SET session_replication_role = origin;
 
--rollback TRUNCATE TABLE tenant.program CASCADE;