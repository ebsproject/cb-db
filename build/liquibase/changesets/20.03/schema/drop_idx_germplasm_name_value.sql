--liquibase formatted sql

--changeset postgres:drop_idx_germplasm_name_value context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    germplasm.germplasm_name 
DROP CONSTRAINT IF EXISTS 
    germplasm_name_value_idx;

--rollback select (1)
