--liquibase formatted sql

--changeset postgres:drop_idx_package_package_code context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    germplasm.package 
DROP CONSTRAINT IF EXISTS 
    package_package_code_idx;

--rollback select (1);