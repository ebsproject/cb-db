--liquibase formatted sql

--changeset postgres:add_trigger_for_event_log_experiment_entry context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.entry') t;
--rollback DROP TRIGGER IF EXISTS entry_event_log_tgr ON experiment.entry;

--changeset postgres:add_trigger_for_event_log_experiment_entry_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.entry_data') t;
--rollback DROP TRIGGER IF EXISTS entry_data_event_log_tgr ON experiment.entry_data;

--changeset postgres:add_trigger_for_event_log_experiment_entry_list context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.entry_list') t;
--rollback DROP TRIGGER IF EXISTS entry_list_event_log_tgr ON experiment.entry_list;

--changeset postgres:add_trigger_for_event_log_experiment_experiment context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment') t;
--rollback DROP TRIGGER IF EXISTS experiment_event_log_tgr ON experiment.experiment;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_block context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_block') t;
--rollback DROP TRIGGER IF EXISTS experiment_block_event_log_tgr ON experiment.experiment_block;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_data') t;
--rollback DROP TRIGGER IF EXISTS experiment_data_event_log_tgr ON experiment.experiment_data;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_design context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_design') t;
--rollback DROP TRIGGER IF EXISTS experiment_design_event_log_tgr ON experiment.experiment_design;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_group context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_group') t;
--rollback DROP TRIGGER IF EXISTS experiment_group_event_log_tgr ON experiment.experiment_group;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_group_member context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_group_member') t;
--rollback DROP TRIGGER IF EXISTS experiment_group_member_event_log_tgr ON experiment.experiment_group_member;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_plan context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_plan') t;
--rollback DROP TRIGGER IF EXISTS experiment_plan_event_log_tgr ON experiment.experiment_plan;

--changeset postgres:add_trigger_for_event_log_experiment_experiment_protocol context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.experiment_protocol') t;
--rollback DROP TRIGGER IF EXISTS experiment_protocol_event_log_tgr ON experiment.experiment_protocol;

--changeset postgres:add_trigger_for_event_log_experiment_location context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.location') t;
--rollback DROP TRIGGER IF EXISTS location_event_log_tgr ON experiment.location;

--changeset postgres:add_trigger_for_event_log_experiment_location_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.location_data') t;
--rollback DROP TRIGGER IF EXISTS location_data_event_log_tgr ON experiment.location_data;

--changeset postgres:add_trigger_for_event_log_experiment_location_occurrence_group context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.location_occurrence_group') t;
--rollback DROP TRIGGER IF EXISTS location_occurrence_group_event_log_tgr ON experiment.location_occurrence_group;

--changeset postgres:add_trigger_for_event_log_experiment_location_status context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.location_status') t;
--rollback DROP TRIGGER IF EXISTS location_status_event_log_tgr ON experiment.location_status;

--changeset postgres:add_trigger_for_event_log_experiment_location_task context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.location_task') t;
--rollback DROP TRIGGER IF EXISTS location_task_event_log_tgr ON experiment.location_task;

--changeset postgres:add_trigger_for_event_log_experiment_occurrence context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.occurrence') t;
--rollback DROP TRIGGER IF EXISTS occurrence_event_log_tgr ON experiment.occurrence;

--changeset postgres:add_trigger_for_event_log_experiment_occurrence_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.occurrence_data') t;
--rollback DROP TRIGGER IF EXISTS occurrence_data_event_log_tgr ON experiment.occurrence_data;

--changeset postgres:add_trigger_for_event_log_experiment_occurrence_group context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.occurrence_group') t;
--rollback DROP TRIGGER IF EXISTS occurrence_group_event_log_tgr ON experiment.occurrence_group;

--changeset postgres:add_trigger_for_event_log_experiment_occurrence_group_member context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.occurrence_group_member') t;
--rollback DROP TRIGGER IF EXISTS occurrence_group_member_event_log_tgr ON experiment.occurrence_group_member;

--changeset postgres:add_trigger_for_event_log_experiment_occurrence_layout context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.occurrence_layout') t;
--rollback DROP TRIGGER IF EXISTS occurrence_layout_event_log_tgr ON experiment.occurrence_layout;

--changeset postgres:add_trigger_for_event_log_experiment_plant context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.plant') t;
--rollback DROP TRIGGER IF EXISTS plant_event_log_tgr ON experiment.plant;

--changeset postgres:add_trigger_for_event_log_experiment_plant_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.plant_data') t;
--rollback DROP TRIGGER IF EXISTS plant_data_event_log_tgr ON experiment.plant_data;

--changeset postgres:add_trigger_for_event_log_experiment_planting_envelope context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.planting_envelope') t;
--rollback DROP TRIGGER IF EXISTS planting_envelope_event_log_tgr ON experiment.planting_envelope;

--changeset postgres:add_trigger_for_event_log_experiment_planting_instruction context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.planting_instruction') t;
--rollback DROP TRIGGER IF EXISTS planting_instruction_event_log_tgr ON experiment.planting_instruction;

--changeset postgres:add_trigger_for_event_log_experiment_plot context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.plot') t;
--rollback DROP TRIGGER IF EXISTS plot_event_log_tgr ON experiment.plot;

--changeset postgres:add_trigger_for_event_log_experiment_plot_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.plot_data') t;
--rollback DROP TRIGGER IF EXISTS plot_data_event_log_tgr ON experiment.plot_data;

--changeset postgres:add_trigger_for_event_log_experiment_sample context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.sample') t;
--rollback DROP TRIGGER IF EXISTS sample_event_log_tgr ON experiment.sample;

--changeset postgres:add_trigger_for_event_log_experiment_sample_measurement context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.sample_measurement') t;
--rollback DROP TRIGGER IF EXISTS sample_measurement_event_log_tgr ON experiment.sample_measurement;

--changeset postgres:add_trigger_for_event_log_experiment_subplot context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.subplot') t;
--rollback DROP TRIGGER IF EXISTS subplot_event_log_tgr ON experiment.subplot;

--changeset postgres:add_trigger_for_event_log_experiment_subplot_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('experiment.subplot_data') t;
--rollback DROP TRIGGER IF EXISTS subplot_data_event_log_tgr ON experiment.subplot_data;

--changeset postgres:add_trigger_for_event_log_germplasm_cross context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.cross') t;
--rollback DROP TRIGGER IF EXISTS cross_event_log_tgr ON germplasm.cross;

--changeset postgres:add_trigger_for_event_log_germplasm_cross_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.cross_attribute') t;
--rollback DROP TRIGGER IF EXISTS cross_attribute_event_log_tgr ON germplasm.cross_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_cross_parent context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.cross_parent') t;
--rollback DROP TRIGGER IF EXISTS cross_parent_event_log_tgr ON germplasm.cross_parent;

--changeset postgres:add_trigger_for_event_log_germplasm_envelope context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.envelope') t;
--rollback DROP TRIGGER IF EXISTS envelope_event_log_tgr ON germplasm.envelope;

--changeset postgres:add_trigger_for_event_log_germplasm_family context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.family') t;
--rollback DROP TRIGGER IF EXISTS family_event_log_tgr ON germplasm.family;

--changeset postgres:add_trigger_for_event_log_germplasm_genetic_population context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.genetic_population') t;
--rollback DROP TRIGGER IF EXISTS genetic_population_event_log_tgr ON germplasm.genetic_population;

--changeset postgres:add_trigger_for_event_log_germplasm_genetic_population_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.genetic_population_attribute') t;
--rollback DROP TRIGGER IF EXISTS genetic_population_attribute_event_log_tgr ON germplasm.genetic_population_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm') t;
--rollback DROP TRIGGER IF EXISTS germplasm_event_log_tgr ON germplasm.germplasm;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm_attribute') t;
--rollback DROP TRIGGER IF EXISTS germplasm_attribute_event_log_tgr ON germplasm.germplasm_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm_name context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm_name') t;
--rollback DROP TRIGGER IF EXISTS germplasm_name_event_log_tgr ON germplasm.germplasm_name;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm_relation context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm_relation') t;
--rollback DROP TRIGGER IF EXISTS germplasm_relation_event_log_tgr ON germplasm.germplasm_relation;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm_state_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm_state_attribute') t;
--rollback DROP TRIGGER IF EXISTS germplasm_state_attribute_event_log_tgr ON germplasm.germplasm_state_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_germplasm_trait context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.germplasm_trait') t;
--rollback DROP TRIGGER IF EXISTS germplasm_trait_event_log_tgr ON germplasm.germplasm_trait;

--changeset postgres:add_trigger_for_event_log_germplasm_package context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.package') t;
--rollback DROP TRIGGER IF EXISTS package_event_log_tgr ON germplasm.package;

--changeset postgres:add_trigger_for_event_log_germplasm_package_relation context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.package_relation') t;
--rollback DROP TRIGGER IF EXISTS package_relation_event_log_tgr ON germplasm.package_relation;

--changeset postgres:add_trigger_for_event_log_germplasm_package_trait context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.package_trait') t;
--rollback DROP TRIGGER IF EXISTS package_trait_event_log_tgr ON germplasm.package_trait;

--changeset postgres:add_trigger_for_event_log_germplasm_product_profile context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.product_profile') t;
--rollback DROP TRIGGER IF EXISTS product_profile_event_log_tgr ON germplasm.product_profile;

--changeset postgres:add_trigger_for_event_log_germplasm_product_profile_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.product_profile_attribute') t;
--rollback DROP TRIGGER IF EXISTS product_profile_attribute_event_log_tgr ON germplasm.product_profile_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_seed context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.seed') t;
--rollback DROP TRIGGER IF EXISTS seed_event_log_tgr ON germplasm.seed;

--changeset postgres:add_trigger_for_event_log_germplasm_seed_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.seed_attribute') t;
--rollback DROP TRIGGER IF EXISTS seed_attribute_event_log_tgr ON germplasm.seed_attribute;

--changeset postgres:add_trigger_for_event_log_germplasm_seed_relation context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.seed_relation') t;
--rollback DROP TRIGGER IF EXISTS seed_relation_event_log_tgr ON germplasm.seed_relation;

--changeset postgres:add_trigger_for_event_log_germplasm_seed_trait context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.seed_trait') t;
--rollback DROP TRIGGER IF EXISTS seed_trait_event_log_tgr ON germplasm.seed_trait;

--changeset postgres:add_trigger_for_event_log_germplasm_taxonomy context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('germplasm.taxonomy') t;
--rollback DROP TRIGGER IF EXISTS taxonomy_event_log_tgr ON germplasm.taxonomy;

--changeset postgres:add_trigger_for_event_log_place_facility context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('place.facility') t;
--rollback DROP TRIGGER IF EXISTS facility_event_log_tgr ON place.facility;

--changeset postgres:add_trigger_for_event_log_place_geospatial_object context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('place.geospatial_object') t;
--rollback DROP TRIGGER IF EXISTS geospatial_object_event_log_tgr ON place.geospatial_object;

--changeset postgres:add_trigger_for_event_log_place_geospatial_object_attribute context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('place.geospatial_object_attribute') t;
--rollback DROP TRIGGER IF EXISTS geospatial_object_attribute_event_log_tgr ON place.geospatial_object_attribute;

--changeset postgres:add_trigger_for_event_log_tenant_breeding_zone context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.breeding_zone') t;
--rollback DROP TRIGGER IF EXISTS breeding_zone_event_log_tgr ON tenant.breeding_zone;

--changeset postgres:add_trigger_for_event_log_tenant_crop context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.crop') t;
--rollback DROP TRIGGER IF EXISTS crop_event_log_tgr ON tenant.crop;

--changeset postgres:add_trigger_for_event_log_tenant_crop_program context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.crop_program') t;
--rollback DROP TRIGGER IF EXISTS crop_program_event_log_tgr ON tenant.crop_program;

--changeset postgres:add_trigger_for_event_log_tenant_crop_program_team context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.crop_program_team') t;
--rollback DROP TRIGGER IF EXISTS crop_program_team_event_log_tgr ON tenant.crop_program_team;

--changeset postgres:add_trigger_for_event_log_tenant_experiment_plan context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.experiment_plan') t;
--rollback DROP TRIGGER IF EXISTS experiment_plan_event_log_tgr ON tenant.experiment_plan;

--changeset postgres:add_trigger_for_event_log_tenant_organization context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.organization') t;
--rollback DROP TRIGGER IF EXISTS organization_event_log_tgr ON tenant.organization;

--changeset postgres:add_trigger_for_event_log_tenant_person context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.person') t;
--rollback DROP TRIGGER IF EXISTS person_event_log_tgr ON tenant.person;

--changeset postgres:add_trigger_for_event_log_tenant_person_role context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.person_role') t;
--rollback DROP TRIGGER IF EXISTS person_role_event_log_tgr ON tenant.person_role;

--changeset postgres:add_trigger_for_event_log_tenant_phase context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.phase') t;
--rollback DROP TRIGGER IF EXISTS phase_event_log_tgr ON tenant.phase;

--changeset postgres:add_trigger_for_event_log_tenant_phase_scheme context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.phase_scheme') t;
--rollback DROP TRIGGER IF EXISTS phase_scheme_event_log_tgr ON tenant.phase_scheme;

--changeset postgres:add_trigger_for_event_log_tenant_pipeline context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.pipeline') t;
--rollback DROP TRIGGER IF EXISTS pipeline_event_log_tgr ON tenant.pipeline;

--changeset postgres:add_trigger_for_event_log_tenant_program context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.program') t;
--rollback DROP TRIGGER IF EXISTS program_event_log_tgr ON tenant.program;

--changeset postgres:add_trigger_for_event_log_tenant_program_config context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.program_config') t;
--rollback DROP TRIGGER IF EXISTS program_config_event_log_tgr ON tenant.program_config;

--changeset postgres:add_trigger_for_event_log_tenant_program_team context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.program_team') t;
--rollback DROP TRIGGER IF EXISTS program_team_event_log_tgr ON tenant.program_team;

--changeset postgres:add_trigger_for_event_log_tenant_project context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.project') t;
--rollback DROP TRIGGER IF EXISTS project_event_log_tgr ON tenant.project;

--changeset postgres:add_trigger_for_event_log_tenant_protocol context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.protocol') t;
--rollback DROP TRIGGER IF EXISTS protocol_event_log_tgr ON tenant.protocol;

--changeset postgres:add_trigger_for_event_log_tenant_protocol_data context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.protocol_data') t;
--rollback DROP TRIGGER IF EXISTS protocol_data_event_log_tgr ON tenant.protocol_data;

--changeset postgres:add_trigger_for_event_log_tenant_scheme context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.scheme') t;
--rollback DROP TRIGGER IF EXISTS scheme_event_log_tgr ON tenant.scheme;

--changeset postgres:add_trigger_for_event_log_tenant_scheme_stage context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.scheme_stage') t;
--rollback DROP TRIGGER IF EXISTS scheme_stage_event_log_tgr ON tenant.scheme_stage;

--changeset postgres:add_trigger_for_event_log_tenant_season context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.season') t;
--rollback DROP TRIGGER IF EXISTS season_event_log_tgr ON tenant.season;

--changeset postgres:add_trigger_for_event_log_tenant_service context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.service') t;
--rollback DROP TRIGGER IF EXISTS service_event_log_tgr ON tenant.service;

--changeset postgres:add_trigger_for_event_log_tenant_service_provider context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.service_provider') t;
--rollback DROP TRIGGER IF EXISTS service_provider_event_log_tgr ON tenant.service_provider;

--changeset postgres:add_trigger_for_event_log_tenant_service_provider_team context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.service_provider_team') t;
--rollback DROP TRIGGER IF EXISTS service_provider_team_event_log_tgr ON tenant.service_provider_team;

--changeset postgres:add_trigger_for_event_log_tenant_stage context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.stage') t;
--rollback DROP TRIGGER IF EXISTS stage_event_log_tgr ON tenant.stage;

--changeset postgres:add_trigger_for_event_log_tenant_team context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.team') t;
--rollback DROP TRIGGER IF EXISTS team_event_log_tgr ON tenant.team;

--changeset postgres:add_trigger_for_event_log_tenant_team_member context:schema splitStatements:false
SELECT t.* FROM platform.audit_table('tenant.team_member') t;
--rollback DROP TRIGGER IF EXISTS team_member_event_log_tgr ON tenant.team_member;
