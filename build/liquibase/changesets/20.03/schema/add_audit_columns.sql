--liquibase formatted sql

--changeset postgres:add_audit_columns_experiment_entry context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.entry
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.entry.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.entry.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.entry.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.entry.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.entry.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.entry.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	entry_is_void_idx
ON
	experiment.entry
USING
	btree ( is_void );

CREATE INDEX
	entry_creator_id_idx
ON
	experiment.entry
USING btree ( creator_id );

CREATE INDEX
	entry_modifier_id_idx
ON
	experiment.entry
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.entry
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_entry_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.entry_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.entry_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.entry_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.entry_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.entry_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.entry_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.entry_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	entry_data_is_void_idx
ON
	experiment.entry_data
USING
	btree ( is_void );

CREATE INDEX
	entry_data_creator_id_idx
ON
	experiment.entry_data
USING btree ( creator_id );

CREATE INDEX
	entry_data_modifier_id_idx
ON
	experiment.entry_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.entry_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_entry_list context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.entry_list
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.entry_list.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.entry_list.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.entry_list.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.entry_list.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.entry_list.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.entry_list.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	entry_list_is_void_idx
ON
	experiment.entry_list
USING
	btree ( is_void );

CREATE INDEX
	entry_list_creator_id_idx
ON
	experiment.entry_list
USING btree ( creator_id );

CREATE INDEX
	entry_list_modifier_id_idx
ON
	experiment.entry_list
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.entry_list
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_is_void_idx
ON
	experiment.experiment
USING
	btree ( is_void );

CREATE INDEX
	experiment_creator_id_idx
ON
	experiment.experiment
USING btree ( creator_id );

CREATE INDEX
	experiment_modifier_id_idx
ON
	experiment.experiment
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_block context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_block
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_block.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_block.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_block.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_block.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_block.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_block.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_block_is_void_idx
ON
	experiment.experiment_block
USING
	btree ( is_void );

CREATE INDEX
	experiment_block_creator_id_idx
ON
	experiment.experiment_block
USING btree ( creator_id );

CREATE INDEX
	experiment_block_modifier_id_idx
ON
	experiment.experiment_block
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_block
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_data_is_void_idx
ON
	experiment.experiment_data
USING
	btree ( is_void );

CREATE INDEX
	experiment_data_creator_id_idx
ON
	experiment.experiment_data
USING btree ( creator_id );

CREATE INDEX
	experiment_data_modifier_id_idx
ON
	experiment.experiment_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_design context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_design
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_design.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_design.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_design.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_design.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_design.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_design.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_design_is_void_idx
ON
	experiment.experiment_design
USING
	btree ( is_void );

CREATE INDEX
	experiment_design_creator_id_idx
ON
	experiment.experiment_design
USING btree ( creator_id );

CREATE INDEX
	experiment_design_modifier_id_idx
ON
	experiment.experiment_design
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_design
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_group context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_group
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_group.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_group.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_group.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_group.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_group.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_group.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_group_is_void_idx
ON
	experiment.experiment_group
USING
	btree ( is_void );

CREATE INDEX
	experiment_group_creator_id_idx
ON
	experiment.experiment_group
USING btree ( creator_id );

CREATE INDEX
	experiment_group_modifier_id_idx
ON
	experiment.experiment_group
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_group
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_group_member context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_group_member
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_group_member.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_group_member.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_group_member.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_group_member.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_group_member.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_group_member.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_group_member_is_void_idx
ON
	experiment.experiment_group_member
USING
	btree ( is_void );

CREATE INDEX
	experiment_group_member_creator_id_idx
ON
	experiment.experiment_group_member
USING btree ( creator_id );

CREATE INDEX
	experiment_group_member_modifier_id_idx
ON
	experiment.experiment_group_member
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_group_member
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_plan context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_plan
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_plan.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_plan.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_plan.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_plan.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_plan.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_plan.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_plan_is_void_idx
ON
	experiment.experiment_plan
USING
	btree ( is_void );

CREATE INDEX
	experiment_plan_creator_id_idx
ON
	experiment.experiment_plan
USING btree ( creator_id );

CREATE INDEX
	experiment_plan_modifier_id_idx
ON
	experiment.experiment_plan
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_plan
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_experiment_protocol context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.experiment_protocol
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.experiment_protocol.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.experiment_protocol.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.experiment_protocol.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.experiment_protocol.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.experiment_protocol.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.experiment_protocol.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_protocol_is_void_idx
ON
	experiment.experiment_protocol
USING
	btree ( is_void );

CREATE INDEX
	experiment_protocol_creator_id_idx
ON
	experiment.experiment_protocol
USING btree ( creator_id );

CREATE INDEX
	experiment_protocol_modifier_id_idx
ON
	experiment.experiment_protocol
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.experiment_protocol
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_location context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.location
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.location.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.location.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.location.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.location.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.location.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.location.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	location_is_void_idx
ON
	experiment.location
USING
	btree ( is_void );

CREATE INDEX
	location_creator_id_idx
ON
	experiment.location
USING btree ( creator_id );

CREATE INDEX
	location_modifier_id_idx
ON
	experiment.location
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.location
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_location_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.location_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.location_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.location_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.location_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.location_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.location_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.location_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	location_data_is_void_idx
ON
	experiment.location_data
USING
	btree ( is_void );

CREATE INDEX
	location_data_creator_id_idx
ON
	experiment.location_data
USING btree ( creator_id );

CREATE INDEX
	location_data_modifier_id_idx
ON
	experiment.location_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.location_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_location_occurrence_group context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.location_occurrence_group
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.location_occurrence_group.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.location_occurrence_group.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.location_occurrence_group.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.location_occurrence_group.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.location_occurrence_group.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.location_occurrence_group.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	location_occurrence_group_is_void_idx
ON
	experiment.location_occurrence_group
USING
	btree ( is_void );

CREATE INDEX
	location_occurrence_group_creator_id_idx
ON
	experiment.location_occurrence_group
USING btree ( creator_id );

CREATE INDEX
	location_occurrence_group_modifier_id_idx
ON
	experiment.location_occurrence_group
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.location_occurrence_group
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_location_status context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.location_status
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.location_status.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.location_status.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.location_status.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.location_status.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.location_status.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.location_status.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	location_status_is_void_idx
ON
	experiment.location_status
USING
	btree ( is_void );

CREATE INDEX
	location_status_creator_id_idx
ON
	experiment.location_status
USING btree ( creator_id );

CREATE INDEX
	location_status_modifier_id_idx
ON
	experiment.location_status
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.location_status
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_location_task context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.location_task
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.location_task.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.location_task.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.location_task.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.location_task.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.location_task.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.location_task.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	location_task_is_void_idx
ON
	experiment.location_task
USING
	btree ( is_void );

CREATE INDEX
	location_task_creator_id_idx
ON
	experiment.location_task
USING btree ( creator_id );

CREATE INDEX
	location_task_modifier_id_idx
ON
	experiment.location_task
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.location_task
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_occurrence context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.occurrence
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.occurrence.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	occurrence_is_void_idx
ON
	experiment.occurrence
USING
	btree ( is_void );

CREATE INDEX
	occurrence_creator_id_idx
ON
	experiment.occurrence
USING btree ( creator_id );

CREATE INDEX
	occurrence_modifier_id_idx
ON
	experiment.occurrence
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.occurrence
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_occurrence_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.occurrence_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.occurrence_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	occurrence_data_is_void_idx
ON
	experiment.occurrence_data
USING
	btree ( is_void );

CREATE INDEX
	occurrence_data_creator_id_idx
ON
	experiment.occurrence_data
USING btree ( creator_id );

CREATE INDEX
	occurrence_data_modifier_id_idx
ON
	experiment.occurrence_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.occurrence_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_occurrence_group context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.occurrence_group
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.occurrence_group.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence_group.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_group.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence_group.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_group.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence_group.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	occurrence_group_is_void_idx
ON
	experiment.occurrence_group
USING
	btree ( is_void );

CREATE INDEX
	occurrence_group_creator_id_idx
ON
	experiment.occurrence_group
USING btree ( creator_id );

CREATE INDEX
	occurrence_group_modifier_id_idx
ON
	experiment.occurrence_group
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.occurrence_group
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_occurrence_group_member context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.occurrence_group_member
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.occurrence_group_member.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence_group_member.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_group_member.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence_group_member.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_group_member.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence_group_member.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	occurrence_group_member_is_void_idx
ON
	experiment.occurrence_group_member
USING
	btree ( is_void );

CREATE INDEX
	occurrence_group_member_creator_id_idx
ON
	experiment.occurrence_group_member
USING btree ( creator_id );

CREATE INDEX
	occurrence_group_member_modifier_id_idx
ON
	experiment.occurrence_group_member
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.occurrence_group_member
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_occurrence_layout context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.occurrence_layout
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.occurrence_layout.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence_layout.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_layout.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence_layout.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_layout.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence_layout.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	occurrence_layout_is_void_idx
ON
	experiment.occurrence_layout
USING
	btree ( is_void );

CREATE INDEX
	occurrence_layout_creator_id_idx
ON
	experiment.occurrence_layout
USING btree ( creator_id );

CREATE INDEX
	occurrence_layout_modifier_id_idx
ON
	experiment.occurrence_layout
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.occurrence_layout
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_plant context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.plant
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.plant.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.plant.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.plant.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.plant.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.plant.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.plant.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	plant_is_void_idx
ON
	experiment.plant
USING
	btree ( is_void );

CREATE INDEX
	plant_creator_id_idx
ON
	experiment.plant
USING btree ( creator_id );

CREATE INDEX
	plant_modifier_id_idx
ON
	experiment.plant
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.plant
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_plant_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.plant_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.plant_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.plant_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.plant_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.plant_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.plant_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.plant_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	plant_data_is_void_idx
ON
	experiment.plant_data
USING
	btree ( is_void );

CREATE INDEX
	plant_data_creator_id_idx
ON
	experiment.plant_data
USING btree ( creator_id );

CREATE INDEX
	plant_data_modifier_id_idx
ON
	experiment.plant_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.plant_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_planting_envelope context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.planting_envelope
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.planting_envelope.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.planting_envelope.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.planting_envelope.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.planting_envelope.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.planting_envelope.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.planting_envelope.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	planting_envelope_is_void_idx
ON
	experiment.planting_envelope
USING
	btree ( is_void );

CREATE INDEX
	planting_envelope_creator_id_idx
ON
	experiment.planting_envelope
USING btree ( creator_id );

CREATE INDEX
	planting_envelope_modifier_id_idx
ON
	experiment.planting_envelope
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.planting_envelope
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_planting_instruction context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.planting_instruction
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.planting_instruction.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.planting_instruction.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.planting_instruction.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.planting_instruction.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.planting_instruction.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.planting_instruction.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	planting_instruction_is_void_idx
ON
	experiment.planting_instruction
USING
	btree ( is_void );

CREATE INDEX
	planting_instruction_creator_id_idx
ON
	experiment.planting_instruction
USING btree ( creator_id );

CREATE INDEX
	planting_instruction_modifier_id_idx
ON
	experiment.planting_instruction
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.planting_instruction
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_plot context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.plot
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.plot.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.plot.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.plot.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.plot.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.plot.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.plot.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	plot_is_void_idx
ON
	experiment.plot
USING
	btree ( is_void );

CREATE INDEX
	plot_creator_id_idx
ON
	experiment.plot
USING btree ( creator_id );

CREATE INDEX
	plot_modifier_id_idx
ON
	experiment.plot
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.plot
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_plot_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.plot_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.plot_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.plot_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.plot_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.plot_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.plot_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.plot_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	plot_data_is_void_idx
ON
	experiment.plot_data
USING
	btree ( is_void );

CREATE INDEX
	plot_data_creator_id_idx
ON
	experiment.plot_data
USING btree ( creator_id );

CREATE INDEX
	plot_data_modifier_id_idx
ON
	experiment.plot_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.plot_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_sample context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.sample
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.sample.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.sample.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.sample.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.sample.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.sample.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.sample.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	sample_is_void_idx
ON
	experiment.sample
USING
	btree ( is_void );

CREATE INDEX
	sample_creator_id_idx
ON
	experiment.sample
USING btree ( creator_id );

CREATE INDEX
	sample_modifier_id_idx
ON
	experiment.sample
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.sample
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_sample_measurement context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.sample_measurement
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.sample_measurement.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.sample_measurement.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.sample_measurement.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.sample_measurement.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.sample_measurement.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.sample_measurement.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	sample_measurement_is_void_idx
ON
	experiment.sample_measurement
USING
	btree ( is_void );

CREATE INDEX
	sample_measurement_creator_id_idx
ON
	experiment.sample_measurement
USING btree ( creator_id );

CREATE INDEX
	sample_measurement_modifier_id_idx
ON
	experiment.sample_measurement
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.sample_measurement
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_subplot context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.subplot
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.subplot.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.subplot.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.subplot.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.subplot.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.subplot.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.subplot.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	subplot_is_void_idx
ON
	experiment.subplot
USING
	btree ( is_void );

CREATE INDEX
	subplot_creator_id_idx
ON
	experiment.subplot
USING btree ( creator_id );

CREATE INDEX
	subplot_modifier_id_idx
ON
	experiment.subplot
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.subplot
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_experiment_subplot_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	experiment.subplot_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN experiment.subplot_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.subplot_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.subplot_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.subplot_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.subplot_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.subplot_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	subplot_data_is_void_idx
ON
	experiment.subplot_data
USING
	btree ( is_void );

CREATE INDEX
	subplot_data_creator_id_idx
ON
	experiment.subplot_data
USING btree ( creator_id );

CREATE INDEX
	subplot_data_modifier_id_idx
ON
	experiment.subplot_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS experiment.subplot_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_cross context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.cross
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.cross.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.cross.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.cross.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.cross.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.cross.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.cross.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	cross_is_void_idx
ON
	germplasm.cross
USING
	btree ( is_void );

CREATE INDEX
	cross_creator_id_idx
ON
	germplasm.cross
USING btree ( creator_id );

CREATE INDEX
	cross_modifier_id_idx
ON
	germplasm.cross
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.cross
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_cross_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.cross_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.cross_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.cross_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.cross_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.cross_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.cross_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.cross_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	cross_attribute_is_void_idx
ON
	germplasm.cross_attribute
USING
	btree ( is_void );

CREATE INDEX
	cross_attribute_creator_id_idx
ON
	germplasm.cross_attribute
USING btree ( creator_id );

CREATE INDEX
	cross_attribute_modifier_id_idx
ON
	germplasm.cross_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.cross_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_cross_parent context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.cross_parent
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.cross_parent.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.cross_parent.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.cross_parent.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.cross_parent.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.cross_parent.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.cross_parent.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	cross_parent_is_void_idx
ON
	germplasm.cross_parent
USING
	btree ( is_void );

CREATE INDEX
	cross_parent_creator_id_idx
ON
	germplasm.cross_parent
USING btree ( creator_id );

CREATE INDEX
	cross_parent_modifier_id_idx
ON
	germplasm.cross_parent
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.cross_parent
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_envelope context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.envelope
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.envelope.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.envelope.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.envelope.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.envelope.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.envelope.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.envelope.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	envelope_is_void_idx
ON
	germplasm.envelope
USING
	btree ( is_void );

CREATE INDEX
	envelope_creator_id_idx
ON
	germplasm.envelope
USING btree ( creator_id );

CREATE INDEX
	envelope_modifier_id_idx
ON
	germplasm.envelope
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.envelope
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_family context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.family
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.family.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.family.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.family.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.family.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.family.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.family.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	family_is_void_idx
ON
	germplasm.family
USING
	btree ( is_void );

CREATE INDEX
	family_creator_id_idx
ON
	germplasm.family
USING btree ( creator_id );

CREATE INDEX
	family_modifier_id_idx
ON
	germplasm.family
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.family
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_genetic_population context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.genetic_population
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.genetic_population.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.genetic_population.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.genetic_population.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.genetic_population.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.genetic_population.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.genetic_population.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	genetic_population_is_void_idx
ON
	germplasm.genetic_population
USING
	btree ( is_void );

CREATE INDEX
	genetic_population_creator_id_idx
ON
	germplasm.genetic_population
USING btree ( creator_id );

CREATE INDEX
	genetic_population_modifier_id_idx
ON
	germplasm.genetic_population
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.genetic_population
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_genetic_population_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.genetic_population_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.genetic_population_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.genetic_population_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	genetic_population_attribute_is_void_idx
ON
	germplasm.genetic_population_attribute
USING
	btree ( is_void );

CREATE INDEX
	genetic_population_attribute_creator_id_idx
ON
	germplasm.genetic_population_attribute
USING btree ( creator_id );

CREATE INDEX
	genetic_population_attribute_modifier_id_idx
ON
	germplasm.genetic_population_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.genetic_population_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_is_void_idx
ON
	germplasm.germplasm
USING
	btree ( is_void );

CREATE INDEX
	germplasm_creator_id_idx
ON
	germplasm.germplasm
USING btree ( creator_id );

CREATE INDEX
	germplasm_modifier_id_idx
ON
	germplasm.germplasm
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_attribute_is_void_idx
ON
	germplasm.germplasm_attribute
USING
	btree ( is_void );

CREATE INDEX
	germplasm_attribute_creator_id_idx
ON
	germplasm.germplasm_attribute
USING btree ( creator_id );

CREATE INDEX
	germplasm_attribute_modifier_id_idx
ON
	germplasm.germplasm_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm_name context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm_name
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm_name.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_name.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_name.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_name.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_name.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_name.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_name_is_void_idx
ON
	germplasm.germplasm_name
USING
	btree ( is_void );

CREATE INDEX
	germplasm_name_creator_id_idx
ON
	germplasm.germplasm_name
USING btree ( creator_id );

CREATE INDEX
	germplasm_name_modifier_id_idx
ON
	germplasm.germplasm_name
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm_name
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm_relation context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm_relation
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm_relation.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_relation.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_relation.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_relation.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_relation.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_relation_is_void_idx
ON
	germplasm.germplasm_relation
USING
	btree ( is_void );

CREATE INDEX
	germplasm_relation_creator_id_idx
ON
	germplasm.germplasm_relation
USING btree ( creator_id );

CREATE INDEX
	germplasm_relation_modifier_id_idx
ON
	germplasm.germplasm_relation
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm_relation
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm_state_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm_state_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm_state_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_state_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_state_attribute_is_void_idx
ON
	germplasm.germplasm_state_attribute
USING
	btree ( is_void );

CREATE INDEX
	germplasm_state_attribute_creator_id_idx
ON
	germplasm.germplasm_state_attribute
USING btree ( creator_id );

CREATE INDEX
	germplasm_state_attribute_modifier_id_idx
ON
	germplasm.germplasm_state_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm_state_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_germplasm_trait context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.germplasm_trait
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.germplasm_trait.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.germplasm_trait.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_trait.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.germplasm_trait.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.germplasm_trait.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.germplasm_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	germplasm_trait_is_void_idx
ON
	germplasm.germplasm_trait
USING
	btree ( is_void );

CREATE INDEX
	germplasm_trait_creator_id_idx
ON
	germplasm.germplasm_trait
USING btree ( creator_id );

CREATE INDEX
	germplasm_trait_modifier_id_idx
ON
	germplasm.germplasm_trait
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.germplasm_trait
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_package context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.package
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.package.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.package.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.package.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.package.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.package.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.package.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	package_is_void_idx
ON
	germplasm.package
USING
	btree ( is_void );

CREATE INDEX
	package_creator_id_idx
ON
	germplasm.package
USING btree ( creator_id );

CREATE INDEX
	package_modifier_id_idx
ON
	germplasm.package
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.package
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_package_relation context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.package_relation
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.package_relation.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.package_relation.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.package_relation.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.package_relation.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.package_relation.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.package_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	package_relation_is_void_idx
ON
	germplasm.package_relation
USING
	btree ( is_void );

CREATE INDEX
	package_relation_creator_id_idx
ON
	germplasm.package_relation
USING btree ( creator_id );

CREATE INDEX
	package_relation_modifier_id_idx
ON
	germplasm.package_relation
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.package_relation
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_package_trait context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.package_trait
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.package_trait.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.package_trait.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.package_trait.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.package_trait.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.package_trait.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.package_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	package_trait_is_void_idx
ON
	germplasm.package_trait
USING
	btree ( is_void );

CREATE INDEX
	package_trait_creator_id_idx
ON
	germplasm.package_trait
USING btree ( creator_id );

CREATE INDEX
	package_trait_modifier_id_idx
ON
	germplasm.package_trait
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.package_trait
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_product_profile context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.product_profile
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.product_profile.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.product_profile.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.product_profile.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.product_profile.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	product_profile_is_void_idx
ON
	germplasm.product_profile
USING
	btree ( is_void );

CREATE INDEX
	product_profile_creator_id_idx
ON
	germplasm.product_profile
USING btree ( creator_id );

CREATE INDEX
	product_profile_modifier_id_idx
ON
	germplasm.product_profile
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.product_profile
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_product_profile_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.product_profile_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.product_profile_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.product_profile_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.product_profile_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.product_profile_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.product_profile_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	product_profile_attribute_is_void_idx
ON
	germplasm.product_profile_attribute
USING
	btree ( is_void );

CREATE INDEX
	product_profile_attribute_creator_id_idx
ON
	germplasm.product_profile_attribute
USING btree ( creator_id );

CREATE INDEX
	product_profile_attribute_modifier_id_idx
ON
	germplasm.product_profile_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.product_profile_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_seed context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.seed
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.seed.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.seed.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.seed.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.seed.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.seed.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.seed.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	seed_is_void_idx
ON
	germplasm.seed
USING
	btree ( is_void );

CREATE INDEX
	seed_creator_id_idx
ON
	germplasm.seed
USING btree ( creator_id );

CREATE INDEX
	seed_modifier_id_idx
ON
	germplasm.seed
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.seed
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_seed_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.seed_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.seed_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.seed_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.seed_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.seed_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.seed_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.seed_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	seed_attribute_is_void_idx
ON
	germplasm.seed_attribute
USING
	btree ( is_void );

CREATE INDEX
	seed_attribute_creator_id_idx
ON
	germplasm.seed_attribute
USING btree ( creator_id );

CREATE INDEX
	seed_attribute_modifier_id_idx
ON
	germplasm.seed_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.seed_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_seed_relation context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.seed_relation
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.seed_relation.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.seed_relation.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.seed_relation.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.seed_relation.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.seed_relation.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.seed_relation.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	seed_relation_is_void_idx
ON
	germplasm.seed_relation
USING
	btree ( is_void );

CREATE INDEX
	seed_relation_creator_id_idx
ON
	germplasm.seed_relation
USING btree ( creator_id );

CREATE INDEX
	seed_relation_modifier_id_idx
ON
	germplasm.seed_relation
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.seed_relation
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_seed_trait context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.seed_trait
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.seed_trait.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.seed_trait.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.seed_trait.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.seed_trait.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.seed_trait.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.seed_trait.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	seed_trait_is_void_idx
ON
	germplasm.seed_trait
USING
	btree ( is_void );

CREATE INDEX
	seed_trait_creator_id_idx
ON
	germplasm.seed_trait
USING btree ( creator_id );

CREATE INDEX
	seed_trait_modifier_id_idx
ON
	germplasm.seed_trait
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.seed_trait
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_germplasm_taxonomy context:schema splitStatements:false
ALTER TABLE IF EXISTS
	germplasm.taxonomy
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN germplasm.taxonomy.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN germplasm.taxonomy.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN germplasm.taxonomy.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN germplasm.taxonomy.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN germplasm.taxonomy.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN germplasm.taxonomy.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	taxonomy_is_void_idx
ON
	germplasm.taxonomy
USING
	btree ( is_void );

CREATE INDEX
	taxonomy_creator_id_idx
ON
	germplasm.taxonomy
USING btree ( creator_id );

CREATE INDEX
	taxonomy_modifier_id_idx
ON
	germplasm.taxonomy
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS germplasm.taxonomy
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_place_facility context:schema splitStatements:false
ALTER TABLE IF EXISTS
	place.facility
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN place.facility.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN place.facility.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN place.facility.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN place.facility.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN place.facility.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN place.facility.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	facility_is_void_idx
ON
	place.facility
USING
	btree ( is_void );

CREATE INDEX
	facility_creator_id_idx
ON
	place.facility
USING btree ( creator_id );

CREATE INDEX
	facility_modifier_id_idx
ON
	place.facility
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS place.facility
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_place_geospatial_object context:schema splitStatements:false
ALTER TABLE IF EXISTS
	place.geospatial_object
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN place.geospatial_object.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN place.geospatial_object.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN place.geospatial_object.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN place.geospatial_object.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN place.geospatial_object.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN place.geospatial_object.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	geospatial_object_is_void_idx
ON
	place.geospatial_object
USING
	btree ( is_void );

CREATE INDEX
	geospatial_object_creator_id_idx
ON
	place.geospatial_object
USING btree ( creator_id );

CREATE INDEX
	geospatial_object_modifier_id_idx
ON
	place.geospatial_object
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS place.geospatial_object
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_place_geospatial_object_attribute context:schema splitStatements:false
ALTER TABLE IF EXISTS
	place.geospatial_object_attribute
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN place.geospatial_object_attribute.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN place.geospatial_object_attribute.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN place.geospatial_object_attribute.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN place.geospatial_object_attribute.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN place.geospatial_object_attribute.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN place.geospatial_object_attribute.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	geospatial_object_attribute_is_void_idx
ON
	place.geospatial_object_attribute
USING
	btree ( is_void );

CREATE INDEX
	geospatial_object_attribute_creator_id_idx
ON
	place.geospatial_object_attribute
USING btree ( creator_id );

CREATE INDEX
	geospatial_object_attribute_modifier_id_idx
ON
	place.geospatial_object_attribute
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS place.geospatial_object_attribute
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_breeding_zone context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.breeding_zone
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.breeding_zone.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.breeding_zone.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.breeding_zone.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.breeding_zone.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.breeding_zone.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.breeding_zone.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	breeding_zone_is_void_idx
ON
	tenant.breeding_zone
USING
	btree ( is_void );

CREATE INDEX
	breeding_zone_creator_id_idx
ON
	tenant.breeding_zone
USING btree ( creator_id );

CREATE INDEX
	breeding_zone_modifier_id_idx
ON
	tenant.breeding_zone
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.breeding_zone
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_crop context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.crop
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.crop.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.crop.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.crop.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.crop.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.crop.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.crop.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	crop_is_void_idx
ON
	tenant.crop
USING
	btree ( is_void );

CREATE INDEX
	crop_creator_id_idx
ON
	tenant.crop
USING btree ( creator_id );

CREATE INDEX
	crop_modifier_id_idx
ON
	tenant.crop
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.crop
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_crop_program context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.crop_program
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.crop_program.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.crop_program.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.crop_program.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.crop_program.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.crop_program.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.crop_program.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	crop_program_is_void_idx
ON
	tenant.crop_program
USING
	btree ( is_void );

CREATE INDEX
	crop_program_creator_id_idx
ON
	tenant.crop_program
USING btree ( creator_id );

CREATE INDEX
	crop_program_modifier_id_idx
ON
	tenant.crop_program
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.crop_program
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_crop_program_team context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.crop_program_team
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.crop_program_team.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.crop_program_team.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.crop_program_team.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.crop_program_team.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.crop_program_team.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.crop_program_team.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	crop_program_team_is_void_idx
ON
	tenant.crop_program_team
USING
	btree ( is_void );

CREATE INDEX
	crop_program_team_creator_id_idx
ON
	tenant.crop_program_team
USING btree ( creator_id );

CREATE INDEX
	crop_program_team_modifier_id_idx
ON
	tenant.crop_program_team
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.crop_program_team
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_experiment_plan context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.experiment_plan
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.experiment_plan.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.experiment_plan.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.experiment_plan.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.experiment_plan.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.experiment_plan.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.experiment_plan.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	experiment_plan_is_void_idx
ON
	tenant.experiment_plan
USING
	btree ( is_void );

CREATE INDEX
	experiment_plan_creator_id_idx
ON
	tenant.experiment_plan
USING btree ( creator_id );

CREATE INDEX
	experiment_plan_modifier_id_idx
ON
	tenant.experiment_plan
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.experiment_plan
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_organization context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.organization
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.organization.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.organization.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.organization.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.organization.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.organization.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.organization.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	organization_is_void_idx
ON
	tenant.organization
USING
	btree ( is_void );

CREATE INDEX
	organization_creator_id_idx
ON
	tenant.organization
USING btree ( creator_id );

CREATE INDEX
	organization_modifier_id_idx
ON
	tenant.organization
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.organization
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_person context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.person
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.person.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.person.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.person.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.person.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.person.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.person.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	person_is_void_idx
ON
	tenant.person
USING
	btree ( is_void );

CREATE INDEX
	person_creator_id_idx
ON
	tenant.person
USING btree ( creator_id );

CREATE INDEX
	person_modifier_id_idx
ON
	tenant.person
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.person
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_person_role context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.person_role
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.person_role.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.person_role.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.person_role.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.person_role.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.person_role.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.person_role.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	person_role_is_void_idx
ON
	tenant.person_role
USING
	btree ( is_void );

CREATE INDEX
	person_role_creator_id_idx
ON
	tenant.person_role
USING btree ( creator_id );

CREATE INDEX
	person_role_modifier_id_idx
ON
	tenant.person_role
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.person_role
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_phase context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.phase
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.phase.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.phase.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.phase.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.phase.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.phase.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.phase.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	phase_is_void_idx
ON
	tenant.phase
USING
	btree ( is_void );

CREATE INDEX
	phase_creator_id_idx
ON
	tenant.phase
USING btree ( creator_id );

CREATE INDEX
	phase_modifier_id_idx
ON
	tenant.phase
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.phase
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_phase_scheme context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.phase_scheme
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.phase_scheme.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.phase_scheme.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.phase_scheme.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.phase_scheme.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.phase_scheme.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.phase_scheme.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	phase_scheme_is_void_idx
ON
	tenant.phase_scheme
USING
	btree ( is_void );

CREATE INDEX
	phase_scheme_creator_id_idx
ON
	tenant.phase_scheme
USING btree ( creator_id );

CREATE INDEX
	phase_scheme_modifier_id_idx
ON
	tenant.phase_scheme
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.phase_scheme
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_pipeline context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.pipeline
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.pipeline.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.pipeline.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.pipeline.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.pipeline.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.pipeline.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.pipeline.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	pipeline_is_void_idx
ON
	tenant.pipeline
USING
	btree ( is_void );

CREATE INDEX
	pipeline_creator_id_idx
ON
	tenant.pipeline
USING btree ( creator_id );

CREATE INDEX
	pipeline_modifier_id_idx
ON
	tenant.pipeline
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.pipeline
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_program context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.program
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.program.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.program.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.program.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.program.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.program.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.program.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	program_is_void_idx
ON
	tenant.program
USING
	btree ( is_void );

CREATE INDEX
	program_creator_id_idx
ON
	tenant.program
USING btree ( creator_id );

CREATE INDEX
	program_modifier_id_idx
ON
	tenant.program
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.program
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_program_config context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.program_config
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.program_config.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.program_config.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.program_config.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.program_config.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.program_config.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.program_config.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	program_config_is_void_idx
ON
	tenant.program_config
USING
	btree ( is_void );

CREATE INDEX
	program_config_creator_id_idx
ON
	tenant.program_config
USING btree ( creator_id );

CREATE INDEX
	program_config_modifier_id_idx
ON
	tenant.program_config
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.program_config
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_program_team context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.program_team
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.program_team.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.program_team.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.program_team.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.program_team.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.program_team.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.program_team.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	program_team_is_void_idx
ON
	tenant.program_team
USING
	btree ( is_void );

CREATE INDEX
	program_team_creator_id_idx
ON
	tenant.program_team
USING btree ( creator_id );

CREATE INDEX
	program_team_modifier_id_idx
ON
	tenant.program_team
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.program_team
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_project context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.project
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.project.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.project.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.project.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.project.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.project.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.project.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	project_is_void_idx
ON
	tenant.project
USING
	btree ( is_void );

CREATE INDEX
	project_creator_id_idx
ON
	tenant.project
USING btree ( creator_id );

CREATE INDEX
	project_modifier_id_idx
ON
	tenant.project
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.project
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_protocol context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.protocol
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.protocol.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.protocol.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.protocol.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.protocol.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.protocol.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.protocol.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	protocol_is_void_idx
ON
	tenant.protocol
USING
	btree ( is_void );

CREATE INDEX
	protocol_creator_id_idx
ON
	tenant.protocol
USING btree ( creator_id );

CREATE INDEX
	protocol_modifier_id_idx
ON
	tenant.protocol
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.protocol
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_protocol_data context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.protocol_data
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.protocol_data.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.protocol_data.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.protocol_data.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.protocol_data.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.protocol_data.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.protocol_data.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	protocol_data_is_void_idx
ON
	tenant.protocol_data
USING
	btree ( is_void );

CREATE INDEX
	protocol_data_creator_id_idx
ON
	tenant.protocol_data
USING btree ( creator_id );

CREATE INDEX
	protocol_data_modifier_id_idx
ON
	tenant.protocol_data
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.protocol_data
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_scheme context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.scheme
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.scheme.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.scheme.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.scheme.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.scheme.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.scheme.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.scheme.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	scheme_is_void_idx
ON
	tenant.scheme
USING
	btree ( is_void );

CREATE INDEX
	scheme_creator_id_idx
ON
	tenant.scheme
USING btree ( creator_id );

CREATE INDEX
	scheme_modifier_id_idx
ON
	tenant.scheme
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.scheme
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_scheme_stage context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.scheme_stage
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.scheme_stage.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.scheme_stage.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.scheme_stage.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.scheme_stage.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.scheme_stage.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.scheme_stage.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	scheme_stage_is_void_idx
ON
	tenant.scheme_stage
USING
	btree ( is_void );

CREATE INDEX
	scheme_stage_creator_id_idx
ON
	tenant.scheme_stage
USING btree ( creator_id );

CREATE INDEX
	scheme_stage_modifier_id_idx
ON
	tenant.scheme_stage
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.scheme_stage
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_season context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.season
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.season.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.season.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.season.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.season.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.season.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.season.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	season_is_void_idx
ON
	tenant.season
USING
	btree ( is_void );

CREATE INDEX
	season_creator_id_idx
ON
	tenant.season
USING btree ( creator_id );

CREATE INDEX
	season_modifier_id_idx
ON
	tenant.season
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.season
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_service context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.service
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.service.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.service.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.service.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.service.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.service.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.service.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	service_is_void_idx
ON
	tenant.service
USING
	btree ( is_void );

CREATE INDEX
	service_creator_id_idx
ON
	tenant.service
USING btree ( creator_id );

CREATE INDEX
	service_modifier_id_idx
ON
	tenant.service
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.service
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_service_provider context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.service_provider
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.service_provider.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.service_provider.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.service_provider.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.service_provider.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.service_provider.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.service_provider.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	service_provider_is_void_idx
ON
	tenant.service_provider
USING
	btree ( is_void );

CREATE INDEX
	service_provider_creator_id_idx
ON
	tenant.service_provider
USING btree ( creator_id );

CREATE INDEX
	service_provider_modifier_id_idx
ON
	tenant.service_provider
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.service_provider
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_service_provider_team context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.service_provider_team
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.service_provider_team.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.service_provider_team.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.service_provider_team.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.service_provider_team.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.service_provider_team.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.service_provider_team.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	service_provider_team_is_void_idx
ON
	tenant.service_provider_team
USING
	btree ( is_void );

CREATE INDEX
	service_provider_team_creator_id_idx
ON
	tenant.service_provider_team
USING btree ( creator_id );

CREATE INDEX
	service_provider_team_modifier_id_idx
ON
	tenant.service_provider_team
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.service_provider_team
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_stage context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.stage
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.stage.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.stage.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.stage.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.stage.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.stage.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.stage.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	stage_is_void_idx
ON
	tenant.stage
USING
	btree ( is_void );

CREATE INDEX
	stage_creator_id_idx
ON
	tenant.stage
USING btree ( creator_id );

CREATE INDEX
	stage_modifier_id_idx
ON
	tenant.stage
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.stage
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_team context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.team
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.team.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.team.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.team.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.team.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.team.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.team.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	team_is_void_idx
ON
	tenant.team
USING
	btree ( is_void );

CREATE INDEX
	team_creator_id_idx
ON
	tenant.team
USING btree ( creator_id );

CREATE INDEX
	team_modifier_id_idx
ON
	tenant.team
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.team
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

--changeset postgres:add_audit_columns_tenant_team_member context:schema splitStatements:false
ALTER TABLE IF EXISTS
	tenant.team_member
ADD COLUMN IF NOT EXISTS
	creator_id integer NOT NULL,
ADD COLUMN IF NOT EXISTS
	creation_timestamp timestamp NOT NULL DEFAULT now(),
ADD COLUMN IF NOT EXISTS
	modifier_id integer,
ADD COLUMN IF NOT EXISTS
	modification_timestamp timestamp,
ADD COLUMN IF NOT EXISTS
	is_void boolean NOT NULL DEFAULT FALSE,
ADD COLUMN IF NOT EXISTS
	notes text COLLATE pg_catalog."default",
ADD COLUMN IF NOT EXISTS
	event_log jsonb,
ADD FOREIGN KEY
	( creator_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE
ON DELETE
	RESTRICT,
ADD FOREIGN KEY
	( modifier_id )
REFERENCES
	tenant.person (id)
ON UPDATE
	CASCADE 
ON DELETE
	RESTRICT;

COMMENT ON COLUMN tenant.team_member.is_void IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN tenant.team_member.creation_timestamp IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN tenant.team_member.creator_id IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN tenant.team_member.modification_timestamp IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN tenant.team_member.modifier_id IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN tenant.team_member.notes IS 'NOTES: Technical details about the record [ISVOID]';

CREATE INDEX
	team_member_is_void_idx
ON
	tenant.team_member
USING
	btree ( is_void );

CREATE INDEX
	team_member_creator_id_idx
ON
	tenant.team_member
USING btree ( creator_id );

CREATE INDEX
	team_member_modifier_id_idx
ON
	tenant.team_member
USING btree ( modifier_id );


--rollback ALTER TABLE IF EXISTS tenant.team_member
--rollback DROP COLUMN IF EXISTS creator_id,
--rollback DROP COLUMN IF EXISTS creation_timestamp,
--rollback DROP COLUMN IF EXISTS modifier_id,
--rollback DROP COLUMN IF EXISTS modification_timestamp,
--rollback DROP COLUMN IF EXISTS is_void,
--rollback DROP COLUMN IF EXISTS notes,
--rollback DROP COLUMN IF EXISTS access_data,
--rollback DROP COLUMN IF EXISTS event_log;

