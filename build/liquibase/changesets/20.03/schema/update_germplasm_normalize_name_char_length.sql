--liquibase formatted sql

--changeset postgres:update_germplasm_normalize_name_char_length context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    germplasm.germplasm_name 
DROP COLUMN IF EXISTS
    germplasm_normalized_name,
ADD COLUMN IF NOT EXISTS
	 germplasm_normalized_name varchar not null;

--rollback select (1)
