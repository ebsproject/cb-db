--liquibase formatted sql

--changeset postgres:drop_plot_occurrence_unq context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    experiment.plot 
DROP CONSTRAINT IF EXISTS 
    plot_occurrence_id_design_x_design_y_unq;

--rollback select (1)
