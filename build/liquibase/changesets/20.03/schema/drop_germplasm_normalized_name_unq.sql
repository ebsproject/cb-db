--liquibase formatted sql

--changeset postgres:drop_idx_germplasm_normalized_name_unq context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    germplasm.germplasm 
DROP CONSTRAINT IF EXISTS 
    germplasm_germplasm_normalized_name_idx;

--rollback select (1);
