--liquibase formatted sql

--changeset postgres:drop_idx_facility_code context:temp_fix splitStatements:false
ALTER TABLE IF EXISTS
    place.facility 
DROP CONSTRAINT IF EXISTS 
    facility_facility_code_idx;

--rollback select (1);
