--liquibase formatted sql

--changeset postgres:create_base_schema context:schema splitStatements:false

CREATE SCHEMA experiment;

CREATE SCHEMA germplasm;

CREATE SCHEMA place;

CREATE SCHEMA tenant;

CREATE  TABLE experiment.experiment_group ( 
	id                   serial  NOT NULL ,
	experiment_group_code varchar(64)  NOT NULL ,
	experiment_group_name varchar(128)  NOT NULL ,
	experiment_group_type varchar(64)  NOT NULL ,
	experiment_group_type_pattern varchar(64)   ,
	description          text   ,
	CONSTRAINT pk_experiment_group_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_experiment_group_id ON experiment.experiment_group IS 'An experiment group is identified by its unique database ID.';

CREATE INDEX experiment_group_experiment_group_code_idx ON experiment.experiment_group ( experiment_group_code );

COMMENT ON INDEX experiment.experiment_group_experiment_group_code_idx IS 'An experiment group can be retrieved by its experiment group code.';

CREATE INDEX experiment_group_experiment_group_name_idx ON experiment.experiment_group ( experiment_group_name );

COMMENT ON INDEX experiment.experiment_group_experiment_group_name_idx IS 'An experiment group can be retrieved by its experiment group name.';

CREATE INDEX experiment_group_experiment_group_type_idx ON experiment.experiment_group ( experiment_group_type );

COMMENT ON INDEX experiment.experiment_group_experiment_group_type_idx IS 'An experiment group can be retrieved by its experiment group type.';

COMMENT ON TABLE experiment.experiment_group IS 'Experiment Group: Grouping of experiments and occurrences for data analysis and decision-making processes [EXPTGRP]';

COMMENT ON COLUMN experiment.experiment_group.id IS 'Experiment Group ID: Database identifier of the experiment group [EXPTGRP_ID]';

COMMENT ON COLUMN experiment.experiment_group.experiment_group_code IS 'Experiment Group Code: Textual identifier of the experiment group [EXPTGRP_CODE]';

COMMENT ON COLUMN experiment.experiment_group.experiment_group_name IS 'Experiment Group Name: Full name of the experiment group [EXPTGRP_NAME]';

COMMENT ON COLUMN experiment.experiment_group.experiment_group_type IS 'Experiment Group Type: Type or objective of the experiment group {analysis block, decision block} [EXPTGRP_TYPE]';

COMMENT ON COLUMN experiment.experiment_group.experiment_group_type_pattern IS 'Experiment Group Type Pattern: Pattern or ordering of the plots if the experiment group type is a decision block: {diagonal pattern, row pattern} [EXPTGRP_TYPEPATTERN]';

COMMENT ON COLUMN experiment.experiment_group.description IS 'Experiment Group Description: Additional information about the experiment group [EXPTGRP_DESC]';

CREATE  TABLE experiment.experiment_plan ( 
	id                   serial  NOT NULL ,
	experiment_plan_code varchar(64)  NOT NULL ,
	experiment_plan_name varchar(128)  NOT NULL ,
	description          text   ,
	experiment_plan_status varchar(64)  NOT NULL ,
	project_id           integer  NOT NULL ,
	CONSTRAINT pk_experiment_plan_id PRIMARY KEY ( id ),
	CONSTRAINT experiment_plan_experiment_plan_code_unq UNIQUE ( experiment_plan_code ) 
 );

COMMENT ON CONSTRAINT pk_experiment_plan_id ON experiment.experiment_plan IS 'An experiment plan is identified by its unique database ID.';

COMMENT ON CONSTRAINT experiment_plan_experiment_plan_code_unq ON experiment.experiment_plan IS 'An experiment plan can be retrieved by its unique experiment plan code.';

CREATE INDEX experiment_plan_experiment_plan_name_idx ON experiment.experiment_plan ( experiment_plan_name );

COMMENT ON INDEX experiment.experiment_plan_experiment_plan_name_idx IS 'An experiment plan can be retrieved by its experiment plan name.';

CREATE INDEX experiment_plan_experiment_plan_status_idx ON experiment.experiment_plan ( experiment_plan_status );

COMMENT ON INDEX experiment.experiment_plan_experiment_plan_status_idx IS 'An experiment plan can be retrieved by its experiment plan status.';

CREATE INDEX experiment_plan_project_id_idx ON experiment.experiment_plan ( project_id );

COMMENT ON INDEX experiment.experiment_plan_project_id_idx IS 'An experiment plan can be retrieved by its project.';

COMMENT ON TABLE experiment.experiment_plan IS 'Experiment Plan: Planned template in conducting one or many experiments [EXPTPLAN]';

COMMENT ON COLUMN experiment.experiment_plan.id IS 'Experiment Plan ID: Database identifier of the experiment plan [EXPTPLAN_ID]';

COMMENT ON COLUMN experiment.experiment_plan.experiment_plan_code IS 'Experiment Plan Code: Textual identifier of the experiment plan [EXPTPLAN_CODE]';

COMMENT ON COLUMN experiment.experiment_plan.experiment_plan_name IS 'Experiment Plan Name: Full name of the experiment plan [EXPTPLAN_NAME]';

COMMENT ON COLUMN experiment.experiment_plan.description IS 'Experiment Plan Description: Additional information about the experiment plan [EXPTPLAN_DESC]';

COMMENT ON COLUMN experiment.experiment_plan.experiment_plan_status IS 'Experiment Plan Status: Status of the experiment plan [EXPTPLAN_STATUS]';

COMMENT ON COLUMN experiment.experiment_plan.project_id IS 'Project ID: Reference to the project that created the experiment plan [EXPTPLAN_PROJ_ID]';

CREATE  TABLE experiment."location" ( 
	id                   serial  NOT NULL ,
	location_code        varchar(64)  NOT NULL ,
	location_name        varchar(128)  NOT NULL ,
	location_status      varchar(128)  NOT NULL ,
	location_type        varchar(64)   ,
	description          text   ,
	steward_id           integer  NOT NULL ,
	location_planting_date date   ,
	location_harvest_date date   ,
	geospatial_object_id integer  NOT NULL ,
	CONSTRAINT location_location_code_idx UNIQUE ( location_code ) ,
	CONSTRAINT location_id_pk PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT location_location_code_idx ON experiment."location" IS 'A location can be retrieved by its location code.';

CREATE INDEX location_location_name_idx ON experiment."location" ( location_name );

COMMENT ON INDEX experiment.location_location_name_idx IS 'A location can be retrieved by its location name.';

CREATE INDEX location_location_status_idx ON experiment."location" ( location_status );

COMMENT ON INDEX experiment.location_location_status_idx IS 'A location can be retrieved by its location status.';

COMMENT ON CONSTRAINT location_id_pk ON experiment."location" IS 'A location is identified by its unique database ID.';

COMMENT ON TABLE experiment."location" IS 'Location: Planting area where occurrences of experiments are mapped and planted [LOC]';

COMMENT ON COLUMN experiment."location".id IS 'Location ID: Database identifier of the location [LOC_ID]';

COMMENT ON COLUMN experiment."location".location_code IS 'Location Code: Textual identifier of the location [LOC_CODE]';

COMMENT ON COLUMN experiment."location".location_name IS 'Location Name: Full name of the location [LOC_NAME]';

COMMENT ON COLUMN experiment."location".location_status IS 'Location Status: Status of the location {draft, operational, completed} [LOC_STATUS]';

COMMENT ON COLUMN experiment."location".location_type IS 'Location Type: Type of the location, which can be fixed or movable {greenhouse, base (zone in a greenhouse with pots/plants), etc.} [LOC_TYPE]';

COMMENT ON COLUMN experiment."location".description IS 'Location Description: Additional information about the location [LOC_DESC]';

COMMENT ON COLUMN experiment."location".steward_id IS 'Steward ID: Reference to the steward (person) who manages the location [LOC_STEWARD_ID]';

COMMENT ON COLUMN experiment."location".location_planting_date IS 'Location Planting Date: Date in general when the entries in the location were planted [LOC_PLANTDATE]';

COMMENT ON COLUMN experiment."location".location_harvest_date IS 'Location Harvest Date: Date in general when the plots in the location were harvested [LOC_HARVDATE]';

COMMENT ON COLUMN experiment."location".geospatial_object_id IS 'Geospatial Object ID: Reference to the geospatial object (planting area) where the location is situated [LOC_GEO_ID]';

CREATE  TABLE experiment.location_data ( 
	id                   serial  NOT NULL ,
	location_id          integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT idx_location_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT idx_location_data_id ON experiment.location_data IS 'A location data is identified by its unique database identifier.';

CREATE INDEX location_data_location_id_variable_id_idx ON experiment.location_data ( location_id, variable_id );

COMMENT ON INDEX experiment.location_data_location_id_variable_id_idx IS 'A location data can be retrieved by its variable within a location.';

CREATE INDEX location_data_location_id_variable_id_data_value_idx ON experiment.location_data ( location_id, variable_id, data_value );

COMMENT ON INDEX experiment.location_data_location_id_variable_id_data_value_idx IS 'A location data can be retrieved by its variable and data value within a location.';

CREATE INDEX location_data_location_id_data_qc_code_idx ON experiment.location_data ( location_id, data_qc_code );

COMMENT ON INDEX experiment.location_data_location_id_data_qc_code_idx IS 'A location data can be retrieved by its data QC code within a location.';

COMMENT ON TABLE experiment.location_data IS 'Location Data: Attribute of the location [LOCDATA]';

COMMENT ON COLUMN experiment.location_data.id IS 'Location Data ID: Database identifier of the location data [LOCDATA_ID]';

COMMENT ON COLUMN experiment.location_data.location_id IS 'Location ID: Reference to the location having the attribute [LOCDATA_LOC_ID]';

COMMENT ON COLUMN experiment.location_data.variable_id IS 'Variable ID: Reference to the variable of the attribute of the location [LOCDATA_VAR_ID]';

COMMENT ON COLUMN experiment.location_data.data_value IS 'Location Data Value: Value of the attribute of the location  [LOCDATA_DATAVAL]';

COMMENT ON COLUMN experiment.location_data.data_qc_code IS 'Location Data QC Code: Status of the location data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [LOCDATA_QCCODE]';

CREATE  TABLE experiment.location_status ( 
	id                   serial  NOT NULL ,
	location_id          integer  NOT NULL ,
	status_value         varchar(64)  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_location_status_id PRIMARY KEY ( id ),
	CONSTRAINT location_status_location_id_order_number_idx UNIQUE ( location_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_location_status_id ON experiment.location_status IS 'A location status is identified by its unique database ID.';

COMMENT ON CONSTRAINT location_status_location_id_order_number_idx ON experiment.location_status IS 'A location status can be retrieved by its order number within a location.';

COMMENT ON TABLE experiment.location_status IS 'Location Status: List of statuses to indicate the progress of the occurrences of experiments in a location [LOCSTATUS]';

COMMENT ON COLUMN experiment.location_status.id IS 'Location Status ID: Database identifier of the location status [LOCSTATUS_ID]';

COMMENT ON COLUMN experiment.location_status.location_id IS 'Location ID: Reference to the location referred by the location status [LOCSTATUS_LOC_ID]';

COMMENT ON COLUMN experiment.location_status.status_value IS 'Location Status Value: Value of the status of the location as a result of performing a specific activity or task [LOCSTATUS_STATUSVAL]';

COMMENT ON COLUMN experiment.location_status.order_number IS 'Location Status Order Number: Ordering of the status within the location [LOCSTATUS_ORDERNO]';

CREATE  TABLE experiment.occurrence_group ( 
	id                   serial  NOT NULL ,
	occurrence_group_code varchar(64)  NOT NULL ,
	occurrence_group_name varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_occurrence_group_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_occurrence_group_id ON experiment.occurrence_group IS 'An occurrence group is identified by its unique database ID.';

CREATE INDEX occurrence_group_occurrence_group_code_idx ON experiment.occurrence_group ( occurrence_group_code );

COMMENT ON INDEX experiment.occurrence_group_occurrence_group_code_idx IS 'An occurrence group can be retrieved by its occurrence group code.';

CREATE INDEX occurrence_group_occurrence_group_name_idx ON experiment.occurrence_group ( occurrence_group_name );

COMMENT ON INDEX experiment.occurrence_group_occurrence_group_name_idx IS 'An occurrence group can be retrieved by its occurrence group name.';

COMMENT ON TABLE experiment.occurrence_group IS 'Occurrence Group: Grouping of overlapping entries across occurrences, which is mainly intended for P-Rep experiments [OCCGRP]';

COMMENT ON COLUMN experiment.occurrence_group.id IS 'Occurrence Group ID: Database identifier of the occurrence group [OCCGRP_ID]';

COMMENT ON COLUMN experiment.occurrence_group.occurrence_group_code IS 'Occurrence Group Code: Textual identifier of the occurrence group [OCCGRP_CODE]';

COMMENT ON COLUMN experiment.occurrence_group.occurrence_group_name IS 'Occurrence Group Name: Full name of the occurrence group [OCCGRP_NAME]';

COMMENT ON COLUMN experiment.occurrence_group.description IS 'Occurrence Group Description: Additional information about the occurrence group [OCCGRP_DESC]';

CREATE  TABLE experiment.experiment ( 
	id                   serial  NOT NULL ,
	program_id           integer  NOT NULL ,
	pipeline_id          integer  NOT NULL ,
	stage_id             integer  NOT NULL ,
	project_id           integer   ,
	experiment_year      integer  NOT NULL ,
	season_id            integer  NOT NULL ,
	planting_season      varchar(32)  NOT NULL ,
	experiment_code      varchar(64)  NOT NULL ,
	experiment_name      varchar(128)  NOT NULL ,
	experiment_objective varchar(256)  NOT NULL ,
	experiment_type      varchar(64)  NOT NULL ,
	experiment_sub_type  varchar(64)   ,
	experiment_sub_sub_type varchar(64)   ,
	experiment_design_type varchar(64)   ,
	experiment_status    varchar(64)  NOT NULL ,
	description          text   ,
	steward_id           integer  NOT NULL ,
	experiment_plan_id   integer   ,
	CONSTRAINT pk_experiment_id PRIMARY KEY ( id ),
	CONSTRAINT experiment_experiment_code_unq UNIQUE ( experiment_code ) 
 );

COMMENT ON CONSTRAINT pk_experiment_id ON experiment.experiment IS 'An experiment is identified by its unique database ID.';

COMMENT ON CONSTRAINT experiment_experiment_code_unq ON experiment.experiment IS 'An experiment can be searched easily using its unique experiment code.';

CREATE INDEX experiment_experiment_name_idx ON experiment.experiment ( experiment_name );

COMMENT ON INDEX experiment.experiment_experiment_name_idx IS 'An experiment can be searched easily using its experiment name.';

CREATE INDEX experiment_experiment_year_idx ON experiment.experiment ( experiment_year );

COMMENT ON INDEX experiment.experiment_experiment_year_idx IS 'An experiment can be searched easily using its experiment year.';

CREATE INDEX experiment_experiment_year_planting_season_idx ON experiment.experiment ( experiment_year, planting_season );

COMMENT ON INDEX experiment.experiment_experiment_year_planting_season_idx IS 'An experiment can be searched easily using its experiment year with planting season.';

CREATE INDEX experiment_experiment_year_season_id_idx ON experiment.experiment ( experiment_year, season_id );

COMMENT ON INDEX experiment.experiment_experiment_year_season_id_idx IS 'An experiment can be searched easily using its experiment year with season.';

CREATE INDEX experiment_experiment_type_idx ON experiment.experiment ( experiment_type );

COMMENT ON INDEX experiment.experiment_experiment_type_idx IS 'An experiment can be searched easily using its experiment type.';

CREATE INDEX experiment_experiment_program_id_idx ON experiment.experiment ( program_id );

COMMENT ON INDEX experiment.experiment_experiment_program_id_idx IS 'An experiment can be searched easily using its program.';

CREATE INDEX experiment_experiment_status_idx ON experiment.experiment ( experiment_status );

COMMENT ON INDEX experiment.experiment_experiment_status_idx IS 'An experiment can be searched easily using its experiment status.';

COMMENT ON TABLE experiment.experiment IS 'Experiment: Activity to create or evaluate germplasm that are observed/tested in one to many locations in order to develop new crop varieties that satisfy the product profile of a program [EXPT]';

COMMENT ON COLUMN experiment.experiment.id IS 'Experiment ID: Database identifier of the experiment [EXPT_ID]';

COMMENT ON COLUMN experiment.experiment.program_id IS 'Program ID: Reference to the program that created the experiment [EXPT_PROG_ID]';

COMMENT ON COLUMN experiment.experiment.pipeline_id IS 'Pipeline ID: Reference to the pipeline where the experiment is subjected [EXPT_PIPE_ID]';

COMMENT ON COLUMN experiment.experiment.stage_id IS 'Stage ID: Reference to the stage in the pipeline of the experiment [EXPT_STAGE_ID]';

COMMENT ON COLUMN experiment.experiment.project_id IS 'Project ID: Reference to the project where the experiment is conceived [EXPT_PROJ_ID]';

COMMENT ON COLUMN experiment.experiment.experiment_year IS 'Experiment Year: Year when the experiment is evaluated, normally the year when it started [EXPT_YEAR]';

COMMENT ON COLUMN experiment.experiment.season_id IS 'Season ID: Reference to the season (cycle) when the experiment was conducted [EXPT_SEASON_ID]';

COMMENT ON COLUMN experiment.experiment.planting_season IS 'Planting Season: Name of the planting season where the experiment is conducted, usually based on an agreed naming convention (e.g. 2020DS, 2020WS) [EXPT_PLANTINGSEASON]';

COMMENT ON COLUMN experiment.experiment.experiment_code IS 'Experiment Code: Textual identifier of the experiment [EXPT_CODE]';

COMMENT ON COLUMN experiment.experiment.experiment_name IS 'Experiment Name: Full name of the experiment [EXPT_NAME]';

COMMENT ON COLUMN experiment.experiment.experiment_objective IS 'Experiment Objective: Purpose of the experiment in the context of its pipeline, stage, type, and sub-type provided by the user [EXPT_OBJECTIVE]';

COMMENT ON COLUMN experiment.experiment.experiment_type IS 'Experiment Type: Type of the experiment {trial, nursery} [EXPT_TYPE]';

COMMENT ON COLUMN experiment.experiment.experiment_sub_type IS 'Experiment Sub-Type: Sub-type of the experiment, which depends on the experiment objective or the experiment type [EXPT_SUBTYPE]';

COMMENT ON COLUMN experiment.experiment.experiment_sub_sub_type IS 'Experiment Sub-Sub-Type: Sub-sub-type of the experiment, which depends on the experiment objective or  thecontext of the experiment sub-sub-type [EXPT_SUBSUBTYPE]';

COMMENT ON COLUMN experiment.experiment.experiment_design_type IS 'Experiment Design: Design type of the experiment {RCBD, Augmented RCBD, P-REP, ...} [EXPT_DESIGNTYPE]';

COMMENT ON COLUMN experiment.experiment.experiment_status IS 'Experiment Status: Status of the experiment {draft, created, active, completed} [EXPT_STATUS]';

COMMENT ON COLUMN experiment.experiment.description IS 'Experiment Description: Additional information about the experiment [EXPT_DESC]';

COMMENT ON COLUMN experiment.experiment.steward_id IS 'Steward ID: Reference to the steward (person) who manages the experiment [EXPT_PERSON_ID]';

COMMENT ON COLUMN experiment.experiment.experiment_plan_id IS 'Experiment Plan ID: Reference to the experiment plan where the experiment may be based [EXPT_EXPTPLAN_ID]';

CREATE  TABLE experiment.experiment_block ( 
	id                   serial  NOT NULL ,
	experiment_block_code varchar(64)  NOT NULL ,
	experiment_block_name varchar(128)  NOT NULL ,
	experiment_id        integer  NOT NULL ,
	parent_experiment_block_id integer   ,
	order_number         integer  NOT NULL ,
	block_type           varchar(64)  NOT NULL ,
	no_of_blocks         integer  NOT NULL ,
	no_of_ranges         integer  NOT NULL ,
	no_of_cols           integer  NOT NULL ,
	no_of_reps           integer  NOT NULL ,
	plot_numbering_order varchar(64)  NOT NULL ,
	starting_corner      varchar(64)  NOT NULL ,
	entry_list_id_list   integer[]   ,
	CONSTRAINT pk_experiment_block_id PRIMARY KEY ( id ),
	CONSTRAINT experiment_block_experiment_id_experiment_block_code_idx UNIQUE ( experiment_id, experiment_block_code ) 
 );

COMMENT ON CONSTRAINT pk_experiment_block_id ON experiment.experiment_block IS 'An experiment block is identified by its unique database ID.';

COMMENT ON CONSTRAINT experiment_block_experiment_id_experiment_block_code_idx ON experiment.experiment_block IS 'An experiment block can be retrieved by its experiment block code within an experiment.';

CREATE INDEX experiment_block_experiment_id_experiment_block_name_idx ON experiment.experiment_block ( experiment_id, experiment_block_name );

COMMENT ON INDEX experiment.experiment_block_experiment_id_experiment_block_name_idx IS 'An experiment block can be retrieved by its experiment block name within an experiment.';

COMMENT ON TABLE experiment.experiment_block IS 'Experiment Block: Blocks and sub-blocks in the experiment [EXPTBLK_ID]';

COMMENT ON COLUMN experiment.experiment_block.id IS 'Experiment Block ID: Database identifier of the experiment block [EXPTBLK_ID]';

COMMENT ON COLUMN experiment.experiment_block.experiment_block_code IS 'Experiment Block Code: Textual identifier of the experiment block [EXPTBLK_CODE]';

COMMENT ON COLUMN experiment.experiment_block.experiment_block_name IS 'Experiment Block Name: Full name of the experiment block [EXPTBLK_NAME]';

COMMENT ON COLUMN experiment.experiment_block.experiment_id IS 'Experiment ID: Reference to the experiment where the experiment block is created [EXPTBLK_EXPT_ID]';

COMMENT ON COLUMN experiment.experiment_block.parent_experiment_block_id IS 'Parent Experiment Block ID: Reference to another experiment block where the experiment block belongs [EXPTBLK_PARENT_ID]';

COMMENT ON COLUMN experiment.experiment_block.order_number IS 'Experiment Block Order Number: Order of the experiment block within the parent experiment block [EXPTBLK_ORDERNO]';

COMMENT ON COLUMN experiment.experiment_block.block_type IS 'Experiment Block Type: Type of the experiment block {parent, cross, sub-block} [EXPTBLK_BLKTYPE]';

COMMENT ON COLUMN experiment.experiment_block.no_of_blocks IS 'No. of Blocks: Number of experiment blocks of the parent experiment block [EXPTBLK_NOOFBLKS]';

COMMENT ON COLUMN experiment.experiment_block.no_of_ranges IS 'Number of Ranges: Number of ranges (rows) in the experiment block [EXPTBLK_NUMRANGES]';

COMMENT ON COLUMN experiment.experiment_block.no_of_cols IS 'Number of Columns: Number of columns in the experiment block [EXPTBLK_NUMCOLS]';

COMMENT ON COLUMN experiment.experiment_block.no_of_reps IS 'No. of Reps: Number of replications in the experiment block [EXPTBLK_NOOFREPS]';

COMMENT ON COLUMN experiment.experiment_block.plot_numbering_order IS 'Plot Numbering Order: Ordering of plot number: row-serpentine, column-serpentine, row-order, column-order [EXPTBLK_PLOTNUMORDER]';

COMMENT ON COLUMN experiment.experiment_block.starting_corner IS 'Starting Corner: Corner in the experiment block where the ordering of the plot number will begin [EXPTBLK_STARTCORNER]';

COMMENT ON COLUMN experiment.experiment_block.entry_list_id_list IS 'Entry ID List: Array of entry list IDs belonging to the experiment block [EXPTBLK_ENTRYIDLIST]';

CREATE  TABLE experiment.experiment_data ( 
	id                   serial  NOT NULL ,
	experiment_id        integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	protocol_id          integer   ,
	CONSTRAINT pk_experiment_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_experiment_data_id ON experiment.experiment_data IS 'An experiment data is identified by its unique database ID.';

COMMENT ON TABLE experiment.experiment_data IS 'Experiment Data: Attribute of an experiment [EXPTDATA]';

COMMENT ON COLUMN experiment.experiment_data.id IS 'Experiment Data ID: Database identifier of the experiment data [EXPTDATA_ID]';

COMMENT ON COLUMN experiment.experiment_data.experiment_id IS 'Experiment ID: Reference to the experiment having the attribute [EXPTDATA_EXPT_ID]';

COMMENT ON COLUMN experiment.experiment_data.variable_id IS 'Experiment Variable ID: Reference to the variable of the attribute of the experiment [EXPTDATA_VAR_ID]';

COMMENT ON COLUMN experiment.experiment_data.data_value IS 'Experiment Data Value: Value of the attribute of the experiment [EXPTDATA_DATAVAL]';

COMMENT ON COLUMN experiment.experiment_data.data_qc_code IS 'Experiment Data QC Code: Status of the experiment data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [EXPTDATA_QCCODE]';

COMMENT ON COLUMN experiment.experiment_data.protocol_id IS 'Protocol ID: Reference to the protocol that the experiment follows [EXPTDATA_PROT_ID]';

CREATE  TABLE experiment.experiment_group_member ( 
	id                   serial  NOT NULL ,
	experiment_group_id  integer  NOT NULL ,
	experiment_id        integer  NOT NULL ,
	experiment_role      varchar(16)  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_experiment_group_member_id PRIMARY KEY ( id ),
	CONSTRAINT experiment_group_member_order_number_idx UNIQUE ( experiment_group_id, order_number ) ,
	CONSTRAINT experiment_group_member_experiment_group_id_experiment_id_idx UNIQUE ( experiment_group_id, experiment_id ) 
 );

COMMENT ON CONSTRAINT pk_experiment_group_member_id ON experiment.experiment_group_member IS 'An experiment group member is identified by its unique database ID.';

COMMENT ON CONSTRAINT experiment_group_member_order_number_idx ON experiment.experiment_group_member IS 'An experiment group member can be retrieved by its unique order number within an experiment group.';

COMMENT ON CONSTRAINT experiment_group_member_experiment_group_id_experiment_id_idx ON experiment.experiment_group_member IS 'An experiment group member can be retrieved by its unique experiment within an experiment group.';

COMMENT ON TABLE experiment.experiment_group_member IS 'Experiment Group Member: Membership of an experiment to an experiment group [EXPTGRPMEM]';

COMMENT ON COLUMN experiment.experiment_group_member.id IS 'Experiment Group Member ID: Database identifier of the experiment group member [EXPTGRPMEM_ID]';

COMMENT ON COLUMN experiment.experiment_group_member.experiment_group_id IS 'Experiment Group ID: Reference to the experiment group where the experiment is a member [EXPTGRPMEM_EXPTGRP_ID]';

COMMENT ON COLUMN experiment.experiment_group_member.experiment_id IS 'Experiment ID: Reference to the experiment that is a member of an experiment group [EXPTGRPMEM_EXPT_ID]';

COMMENT ON COLUMN experiment.experiment_group_member.experiment_role IS 'Experiment Role: Function of the experiment within the experiment group {test, check} [EXPTGRP_EXPT_ROLE]';

COMMENT ON COLUMN experiment.experiment_group_member.order_number IS 'Experiment Order Number: Ordering of the experiment within the experiment group [EXPTGRPMEM_ORDERNO]';

CREATE  TABLE experiment.experiment_protocol ( 
	id                   serial  NOT NULL ,
	experiment_id        integer  NOT NULL ,
	protocol_id          integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_experiment_protocol_id PRIMARY KEY ( id ),
	CONSTRAINT idx_experiment_protocol_experiment_id_protocol_id UNIQUE ( experiment_id, protocol_id ) ,
	CONSTRAINT idx_experiment_protocol_experiment_id_order_number UNIQUE ( experiment_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_experiment_protocol_id ON experiment.experiment_protocol IS 'An experiment protocol is identified by its unique database ID.';

COMMENT ON CONSTRAINT idx_experiment_protocol_experiment_id_protocol_id ON experiment.experiment_protocol IS 'An experiment protocol can be retrieved by its unique protocol within an experiment.';

COMMENT ON CONSTRAINT idx_experiment_protocol_experiment_id_order_number ON experiment.experiment_protocol IS 'An experiment protocol can be retrieved by its unique order number within an experiment.';

COMMENT ON TABLE experiment.experiment_protocol IS 'Experiment Protocol: Protocol that governs the conduct of the experiment [EXPTPROT]';

COMMENT ON COLUMN experiment.experiment_protocol.id IS 'Experiment Protocol ID: Database identifier of the experiment protocol [EXPTPROT_ID]';

COMMENT ON COLUMN experiment.experiment_protocol.experiment_id IS 'Experiment ID: Reference to the experiment that uses the protocol [EXPTPROT_EXPT_ID]';

COMMENT ON COLUMN experiment.experiment_protocol.protocol_id IS 'Protocol ID: Reference to the protocol used by the experiment [EXPTPROT_PROT_ID]';

COMMENT ON COLUMN experiment.experiment_protocol.order_number IS 'Experiment Protocol Order Number: Ordering of the protocol used by the experiment [EXPTPROT_ORDERNO]';

CREATE  TABLE experiment.occurrence ( 
	id                   serial  NOT NULL ,
	occurrence_code      varchar(64)  NOT NULL ,
	occurrence_name      varchar(128)  NOT NULL ,
	occurrence_status    varchar(64)  NOT NULL ,
	description          text   ,
	experiment_id        integer  NOT NULL ,
	geospatial_object_id integer   ,
	CONSTRAINT pk_occurrence_id PRIMARY KEY ( id ),
	CONSTRAINT occurrence_code_idx UNIQUE ( occurrence_code ) 
 );

COMMENT ON CONSTRAINT pk_occurrence_id ON experiment.occurrence IS 'An occurrence is identified by its unique database ID.';

COMMENT ON CONSTRAINT occurrence_code_idx ON experiment.occurrence IS 'An occurrence can be by using its occurrence code.';

CREATE INDEX occurrence_name_idx ON experiment.occurrence ( occurrence_name );

COMMENT ON INDEX experiment.occurrence_name_idx IS 'An occurrence can be retrieved by its occurrence name.';

COMMENT ON TABLE experiment.occurrence IS 'Occurrence: Instance of the experiment that is conducted in a location [OCC]';

COMMENT ON COLUMN experiment.occurrence.id IS 'Occurrrence ID: Database identifier of the occurrence [OCC_ID]';

COMMENT ON COLUMN experiment.occurrence.occurrence_code IS 'Occurrence Code: Textual identifier of the occurrence [OCC_CODE]';

COMMENT ON COLUMN experiment.occurrence.occurrence_name IS 'Occurrence Name: Full name of the occurrence [OCC_NAME]';

COMMENT ON COLUMN experiment.occurrence.occurrence_status IS 'Occurrence Status: Status of the occurrence with regard to its plots {draft, mapped} [OCC_STATUS]';

COMMENT ON COLUMN experiment.occurrence.description IS 'Occurrence Description: Additional information about the occurrence [OCC_DESC]';

COMMENT ON COLUMN experiment.occurrence.experiment_id IS 'Experiment ID: Reference to the experiment that is represented by the occurrence [OCC_EXPT_ID]';

COMMENT ON COLUMN experiment.occurrence.geospatial_object_id IS 'Geospatial Object ID: Reference to the site or field (geospatial object) where the occurrence is planned to be conducted [OCC_GEO_ID]';

CREATE  TABLE experiment.occurrence_data ( 
	id                   serial  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_occurrence_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_occurrence_data_id ON experiment.occurrence_data IS 'An occurrence data is identified by its unique database ID.';

CREATE INDEX occurrence_data_occurrence_id_variable_id_idx ON experiment.occurrence_data ( occurrence_id, variable_id );

COMMENT ON INDEX experiment.occurrence_data_occurrence_id_variable_id_idx IS 'An occurrence data can be retrieved by its variable within an occurrence.';

CREATE INDEX occurrence_data_occurrence_id_variable_id_data_value_idx ON experiment.occurrence_data ( occurrence_id, variable_id, data_value );

COMMENT ON INDEX experiment.occurrence_data_occurrence_id_variable_id_data_value_idx IS 'An occurrence data can be retrieved by its variable and data value within an occurrence.';

CREATE INDEX occurrence_data_occurrence_id_data_qc_code_idx ON experiment.occurrence_data ( occurrence_id, data_qc_code );

COMMENT ON INDEX experiment.occurrence_data_occurrence_id_data_qc_code_idx IS 'An occurrence data can be retrieved by its data QC code within an occurrence.';

COMMENT ON TABLE experiment.occurrence_data IS 'Occurrence Data: Additional information about the occurrence [OCCDATA]';

COMMENT ON COLUMN experiment.occurrence_data.id IS 'Occurrence Data ID: Database identifier of the occurrence data [OCCDATA_ID]';

COMMENT ON COLUMN experiment.occurrence_data.occurrence_id IS 'Occurrence ID: Reference to the occurrence that has the additional information [OCCDATA_OCC_ID]';

COMMENT ON COLUMN experiment.occurrence_data.variable_id IS 'Variable ID: Reference to the variable of the occurrence data [OCCDATA_VAR_ID]';

COMMENT ON COLUMN experiment.occurrence_data.data_value IS 'Occurrence Data Value: Value of the occurrence data [OCCDATA_DATAVAL]';

COMMENT ON COLUMN experiment.occurrence_data.data_qc_code IS 'Occurrence Data QC Code: Status of the occurrence data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [OCCDATA_QCCODE]';

CREATE  TABLE experiment.occurrence_group_member ( 
	id                   serial  NOT NULL ,
	occurrence_group_id  integer  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_occurrence_group_member_id PRIMARY KEY ( id ),
	CONSTRAINT occurrence_group_member_occurrence_group_id_occurrence_id_idx UNIQUE ( occurrence_group_id, occurrence_id ) ,
	CONSTRAINT occurrence_group_member_occurrence_group_id_order_number_idx UNIQUE ( occurrence_group_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_occurrence_group_member_id ON experiment.occurrence_group_member IS 'An occurrence group member is identified by its unique database ID.';

COMMENT ON CONSTRAINT occurrence_group_member_occurrence_group_id_occurrence_id_idx ON experiment.occurrence_group_member IS 'An occurrence group member can be retrieved by its occurrence within an occurrence group.';

COMMENT ON CONSTRAINT occurrence_group_member_occurrence_group_id_order_number_idx ON experiment.occurrence_group_member IS 'An occurrence group member can be retrieved by its order number within an occurrence group.';

COMMENT ON TABLE experiment.occurrence_group_member IS 'Occurrence Group Member: Membership of an occurrence to an occurrence group, which is intended for P-Rep experiments [OCCGRPMEM]';

COMMENT ON COLUMN experiment.occurrence_group_member.id IS 'Occurrence Group Member ID: Database identifier of the occurrence group member [OCCGRPMEM_ID]';

COMMENT ON COLUMN experiment.occurrence_group_member.occurrence_group_id IS 'Occurrence Group ID: Reference to the occurrence group where the occurrence belongs [OCCGRPMEM_OCCGRP_ID]';

COMMENT ON COLUMN experiment.occurrence_group_member.occurrence_id IS 'Occurrence ID: Reference to the occurrence that belongs to the occurrence group [OCCGRPMEM_OCC_ID]';

COMMENT ON COLUMN experiment.occurrence_group_member.order_number IS 'Occurrence Group Member Order Numbder: Ordering of the occurrence within the occurrence group [OCCGRPMEM_ORDERNO]';

CREATE  TABLE experiment.occurrence_layout ( 
	id                   serial  NOT NULL ,
	occurrence_layout_width integer  NOT NULL ,
	occurrence_layout_height integer  NOT NULL ,
	occurrence_layout_order varchar(64)  NOT NULL ,
	occurrence_layout_order_data jsonb  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	CONSTRAINT pk_occurrence_layout_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_occurrence_layout_id ON experiment.occurrence_layout IS 'An occurrence layout is identified by its unique database ID.';

COMMENT ON TABLE experiment.occurrence_layout IS 'Occurrence Layout: Details about the layout of the occurrence planting arrangement for rendering in UI [OCCLAYOUT]';

COMMENT ON COLUMN experiment.occurrence_layout.id IS 'Occurrence Layout ID: Database identifier of the occurrence layout [OCCLAYOUT_ID]';

COMMENT ON COLUMN experiment.occurrence_layout.occurrence_layout_width IS 'Occurrence Layout Width: Extent of the layout from side to side [OCCLAYOUT_WIDTH]';

COMMENT ON COLUMN experiment.occurrence_layout.occurrence_layout_height IS 'Occurrence Layout Height: Extent of the layout from base to top [OCCLAYOUT_HEIGHT]';

COMMENT ON COLUMN experiment.occurrence_layout.occurrence_layout_order IS 'Occurrence Layout Order: Ordering of the plots in the layout [OCCLAYOUT_ORDER]';

COMMENT ON COLUMN experiment.occurrence_layout.occurrence_layout_order_data IS 'Occurrence Layout Order Data: Plots in the layout and their color coding stored in JSON format [OCCLAYOUT_ORDERDATA]';

COMMENT ON COLUMN experiment.occurrence_layout.occurrence_id IS 'Occurrene ID: Reference to the occurrence that adopts the layout [OCCLAYOUT_OCC_ID]';

CREATE  TABLE experiment.entry_list ( 
	id                   serial  NOT NULL ,
	entry_list_code      varchar(64)  NOT NULL ,
	entry_list_name      varchar(128)  NOT NULL ,
	description          text   ,
	entry_list_status    varchar(64)  NOT NULL ,
	experiment_id        integer  NOT NULL ,
	CONSTRAINT pk_entry_list_id PRIMARY KEY ( id ),
	CONSTRAINT entry_list_entry_list_code_unq UNIQUE ( entry_list_code ) ,
	CONSTRAINT entry_list_experiment_id_unq UNIQUE ( experiment_id ) 
 );

COMMENT ON CONSTRAINT pk_entry_list_id ON experiment.entry_list IS 'An entry list is identified by its unique database ID.';

COMMENT ON CONSTRAINT entry_list_entry_list_code_unq ON experiment.entry_list IS 'An entry list can be searched easily using its entry list code.';

COMMENT ON CONSTRAINT entry_list_experiment_id_unq ON experiment.entry_list IS 'An entry list can be searched easily using its unique experiment.';

COMMENT ON TABLE experiment.entry_list IS 'Entry List: List of germplasm that are observed/tested as entries in an experiment [ENTLIST]';

COMMENT ON COLUMN experiment.entry_list.id IS 'Entry List ID: Database identifier of the entry list [ENTLIST_ID]';

COMMENT ON COLUMN experiment.entry_list.entry_list_code IS 'Entry List Code: Textual identifier of the entry list [ENTLIST_CODE]';

COMMENT ON COLUMN experiment.entry_list.entry_list_name IS 'Entry List Name: Full name of the entry list [ENTLIST_NAME]';

COMMENT ON COLUMN experiment.entry_list.description IS 'Entry List Description: Additional information about the entry list [ENTLIST_DESC]';

COMMENT ON COLUMN experiment.entry_list.entry_list_status IS 'Entry List Status: Status of the entry list {draft, created, completed} [ENTLIST_STATUS]';

COMMENT ON COLUMN experiment.entry_list.experiment_id IS 'Experiment ID: Reference to the experiment where the entry list is used [ENTLIST_EXPT_ID]';

CREATE  TABLE experiment.location_occurrence_group ( 
	id                   serial  NOT NULL ,
	location_id          integer  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_location_occurrence_group_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_location_occurrence_group_id ON experiment.location_occurrence_group IS 'A location occurrence group is identified by its unique database ID.';

COMMENT ON TABLE experiment.location_occurrence_group IS 'Location Occurrence Group: Ocurrences of one or many experiments conducted in a location [LOGRP]';

COMMENT ON COLUMN experiment.location_occurrence_group.id IS 'Location Occurrence Group ID: Database identifier of the location occurrence group [LOGRP_ID]';

COMMENT ON COLUMN experiment.location_occurrence_group.location_id IS 'Location ID: Reference to the location where the occurrence is conducted [LOGRP_LOC_ID]';

COMMENT ON COLUMN experiment.location_occurrence_group.occurrence_id IS 'Occurrence ID: Reference to the occurrence that is conducted in the location [LOGRP_OCC_ID]';

COMMENT ON COLUMN experiment.location_occurrence_group.order_number IS 'Order Number: Ordering of the occurrences within the location [LOGRP_ORDERNO]';

CREATE  TABLE experiment.location_task ( 
	id                   serial  NOT NULL ,
	location_id          integer   ,
	occurrence_id        integer   ,
	task_id              integer  NOT NULL ,
	task_value           jsonb  NOT NULL ,
	protocol_id          integer   ,
	CONSTRAINT pk_location_task_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_location_task_id ON experiment.location_task IS 'A location task is identified by its unique database ID.';

COMMENT ON TABLE experiment.location_task IS 'Location Task: Task carried out in the location [LOCTASK]';

COMMENT ON COLUMN experiment.location_task.id IS 'Location Task ID: Database identifier of the location task [LOCTASK_ID]';

COMMENT ON COLUMN experiment.location_task.location_id IS 'Location ID: Reference to the location where the task is carried out [LOCTASK_LOC_ID]';

COMMENT ON COLUMN experiment.location_task.occurrence_id IS 'Occurrence ID: Reference to the occurrence where the task is carried out [LOCTASK_OCC_ID]';

COMMENT ON COLUMN experiment.location_task.task_id IS 'Task ID: Reference to the task that is carried out in the location [LOCTASK_TASK_ID]';

COMMENT ON COLUMN experiment.location_task.task_value IS 'Task Value: Record of one or more instances of the task in the location (e.g. {"0": {"VAR1": "VAL1", "VAR2": "VAL2", …}, "1": {"VAR1": "VAL1", "VAR2": "VAL2", …} …}) [LOCTASK_TASKVAL]';

COMMENT ON COLUMN experiment.location_task.protocol_id IS 'Protocol ID: Reference to the protocol where the task belongs [LOCTASK_PROT_ID]';

CREATE  TABLE experiment.entry ( 
	id                   serial  NOT NULL ,
	entry_code           varchar(64)  NOT NULL ,
	entry_number         integer  NOT NULL ,
	entry_name           varchar(128)  NOT NULL ,
	entry_type           varchar(64)  NOT NULL ,
	entry_role           varchar(64)   ,
	entry_class          varchar   ,
	entry_status         varchar(64)  NOT NULL ,
	description          text   ,
	entry_list_id        integer  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	seed_id              integer   ,
	CONSTRAINT idx_entry_id PRIMARY KEY ( id ),
	CONSTRAINT entry_entry_list_id_entry_number_idx UNIQUE ( entry_list_id, entry_number ) ,
	CONSTRAINT entry_id_unq UNIQUE ( id, entry_code, entry_number ) 
 );

CREATE INDEX entry_entry_code_idx ON experiment.entry ( entry_code );

COMMENT ON INDEX experiment.entry_entry_code_idx IS 'An entry can be retrieved by its entry code.';

CREATE INDEX entry_entry_name_idx ON experiment.entry ( entry_name );

COMMENT ON INDEX experiment.entry_entry_name_idx IS 'An entry can be retrieved by its entry name (germplasm name).';

CREATE INDEX entry_entry_status_idx ON experiment.entry ( entry_status );

COMMENT ON INDEX experiment.entry_entry_status_idx IS 'An entry can be retrieved by its entry status.';

COMMENT ON CONSTRAINT idx_entry_id ON experiment.entry IS 'An entry is identified by its unique database ID.';

COMMENT ON CONSTRAINT entry_entry_list_id_entry_number_idx ON experiment.entry IS 'An entry can be retrieved by its unique entry list and entry number.';

CREATE INDEX entry_entry_list_id_entry_type_idx ON experiment.entry ( entry_list_id, entry_type );

COMMENT ON INDEX experiment.entry_entry_list_id_entry_type_idx IS 'An entry can be retrieved by its entry list and entry type.';

CREATE INDEX entry_entry_list_id_entry_status_idx ON experiment.entry ( entry_list_id, entry_status );

COMMENT ON INDEX experiment.entry_entry_list_id_entry_status_idx IS 'An entry can be retrieved by its entry list and entry status.';

COMMENT ON TABLE experiment.entry IS 'Entry: Germplasm used as an entry in an experiment [ENTRY]';

COMMENT ON COLUMN experiment.entry.id IS 'Entry ID: Database identifier of the entry [ENTRY_ID]';

COMMENT ON COLUMN experiment.entry.entry_code IS 'Entry Code: Textual identifier of the entry in the experiment [ENTRY_CODE]';

COMMENT ON COLUMN experiment.entry.entry_number IS 'Entry Number: Member number of the entry in the experiment [ENTRY_NO]';

COMMENT ON COLUMN experiment.entry.entry_name IS 'Entry Name: Name of the germplasm as an entry in the experiment [ENTRY_NAME]';

COMMENT ON COLUMN experiment.entry.entry_type IS 'Entry Type: Usage of the entry in the experiment {test, check, control, filler, parent, cross} [ENTRY_TYPE]';

COMMENT ON COLUMN experiment.entry.entry_role IS 'Entry Role: Function of the entry in the experiment, which may depend on the type of the experiment {female, male, female-and-male, spatial check, performance check} [ENTRY_ROLE]';

COMMENT ON COLUMN experiment.entry.entry_class IS 'Entry Class: Grouping class of the entry, which is mainly used in data analysis [ENTRY_CLASS]';

COMMENT ON COLUMN experiment.entry.entry_status IS 'Entry Status: Status condition of the entry in the experiment {draft, active} [ENTRY_STATUS]';

COMMENT ON COLUMN experiment.entry.description IS 'Entry Description: Additional information about the entry in the experiment [ENTRY_DESC]';

COMMENT ON COLUMN experiment.entry.entry_list_id IS 'Entry List ID: Reference to the entry list where the entry is added [ENTRY_ENTLIST_ID]';

COMMENT ON COLUMN experiment.entry.germplasm_id IS 'Germplasm ID: Reference to the germplasm under observation/test as an entry in the experiment [ENTRY_GERM_ID]';

COMMENT ON COLUMN experiment.entry.seed_id IS 'Seed ID: Reference to the seed that will represent the germplasm as an entry in the experiment [ENTRY_SEED_ID]';

CREATE  TABLE experiment.entry_data ( 
	id                   serial  NOT NULL ,
	entry_id             integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	transaction_id       integer   ,
	CONSTRAINT idx_entry_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT idx_entry_data_id ON experiment.entry_data IS 'An entry data is identified by its unique database ID.';

CREATE INDEX entry_data_entry_id_variable_id_idx ON experiment.entry_data ( entry_id, variable_id );

COMMENT ON INDEX experiment.entry_data_entry_id_variable_id_idx IS 'An entry data can be retrieved by its variable within an entry.';

CREATE INDEX entry_data_entry_id_variable_id_data_value_idx ON experiment.entry_data ( entry_id, variable_id, data_value );

COMMENT ON INDEX experiment.entry_data_entry_id_variable_id_data_value_idx IS 'An entry data can be retrieved by its variable and data value within an entry.';

CREATE INDEX entry_data_entry_id_data_qc_code_idx ON experiment.entry_data ( entry_id, data_qc_code );

COMMENT ON INDEX experiment.entry_data_entry_id_data_qc_code_idx IS 'An entry data can be retrieved by its data QC code within an entry.';

COMMENT ON TABLE experiment.entry_data IS 'Entry data: Attribute of an entry in an experiment [ENTRYDATA]';

COMMENT ON COLUMN experiment.entry_data.id IS 'Entry Data ID: Database identifier of the entry data [ENTDATA_ID]';

COMMENT ON COLUMN experiment.entry_data.entry_id IS 'Entry ID: Reference to the entry having the attribute [ENTRYDATA_ENTRY_ID]';

COMMENT ON COLUMN experiment.entry_data.variable_id IS 'Variable ID: Reference to the variable of the attribute of the entry [ENTRYDATA_VAR_ID]';

COMMENT ON COLUMN experiment.entry_data.data_value IS 'Entry Data Value: Value of the attribute of the entry [ENTRYDATA_DATAVAL]';

COMMENT ON COLUMN experiment.entry_data.data_qc_code IS 'Entry Data QC Code: Status of the entry data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [ENTRYDATA_QCCODE]';

COMMENT ON COLUMN experiment.entry_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [ENTRYDATA_TXN_ID]';

CREATE  TABLE experiment.plot ( 
	id                   serial  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	location_id          integer   ,
	entry_id             integer  NOT NULL ,
	plot_code            varchar(64)   ,
	plot_number          integer   ,
	plot_type            varchar(64)  NOT NULL ,
	rep                  integer  NOT NULL ,
	design_x             integer   ,
	design_y             integer   ,
	plot_order_number    integer   ,
	pa_x                 integer   ,
	pa_y                 integer   ,
	field_x              integer   ,
	field_y              integer   ,
	plot_status          varchar(64)  NOT NULL ,
	plot_qc_code         varchar(8)   ,
	CONSTRAINT pk_plot_id PRIMARY KEY ( id ),
	CONSTRAINT plot_location_id_plot_order_number_unq UNIQUE ( location_id, plot_order_number ) ,
	CONSTRAINT plot_occurrence_id_plot_number_unq UNIQUE ( occurrence_id, plot_number ) ,
	CONSTRAINT plot_occurrence_id_plot_code_idx UNIQUE ( occurrence_id, plot_code ) ,
	CONSTRAINT plot_occurrence_id_design_x_design_y_unq UNIQUE ( occurrence_id, design_x, design_y ) ,
	CONSTRAINT plot_location_id_pa_x_pa_y_unq UNIQUE ( location_id, pa_x, pa_y ) ,
	CONSTRAINT plot_location_id_field_x_field_y_unq UNIQUE ( location_id, field_x, field_y ) 
 );

COMMENT ON CONSTRAINT pk_plot_id ON experiment.plot IS 'A plot is identified by its unique database ID.';

COMMENT ON CONSTRAINT plot_location_id_plot_order_number_unq ON experiment.plot IS 'A plot can be retrieved by its unique location and plot order number.';

COMMENT ON CONSTRAINT plot_occurrence_id_plot_number_unq ON experiment.plot IS 'A plot can be retrieved by its unique occurrence and plot number.';

COMMENT ON CONSTRAINT plot_occurrence_id_plot_code_idx ON experiment.plot IS 'A plot can be retrieved by its unique occurrence and plot code.';

COMMENT ON CONSTRAINT plot_occurrence_id_design_x_design_y_unq ON experiment.plot IS 'A plot can be retrieved by its unique occurrence, and design x and design y coordinates.';

COMMENT ON CONSTRAINT plot_location_id_pa_x_pa_y_unq ON experiment.plot IS 'A plot can be retrieved by its unique location, and planting area x and planting area y coordinates.';

COMMENT ON CONSTRAINT plot_location_id_field_x_field_y_unq ON experiment.plot IS 'A plot can be retrieved by its unique location, and field x and field y coordinates.';

COMMENT ON TABLE experiment.plot IS 'Plot: Particular place where the entry is planted and if the entry is planted more than once, it is called the replication of the entry [PLOT]';

COMMENT ON COLUMN experiment.plot.id IS 'Plot ID: Database identifier of the plot [PLOT_ID]';

COMMENT ON COLUMN experiment.plot.occurrence_id IS 'Occurrence ID: Reference to the occurrence of the plot [PLOT_OCC_ID]';

COMMENT ON COLUMN experiment.plot.location_id IS 'Location ID: Reference to the location where the plot is mapped [PLOT_LOC_ID]';

COMMENT ON COLUMN experiment.plot.entry_id IS 'Entry ID: Reference to the entry of the plot [PLOT_ENTRY_ID]';

COMMENT ON COLUMN experiment.plot.plot_code IS 'Plot Code: Textual identifier of the plot within the location, which is used in data analysis [PLOT_CODE]';

COMMENT ON COLUMN experiment.plot.plot_number IS 'Plot Number: Ordering of the plot within the location, which is used in data analysis [PLOT_NO]';

COMMENT ON COLUMN experiment.plot.plot_type IS 'Plot Type: Type of the plot {options depend on the organization} [PLOT_TYPE]';

COMMENT ON COLUMN experiment.plot.rep IS 'Plot Replication: Replication number of the entry in the plot [PLOT_REP]';

COMMENT ON COLUMN experiment.plot.design_x IS 'Design X: Coordinate paired with Design Y that is generated by randomization and is used for data analysis [PLOT_DESX]';

COMMENT ON COLUMN experiment.plot.design_y IS 'Design Y: Coordinate paired with Design X that is generated by randomization and is used for data analysis [PLOT_DESY]';

COMMENT ON COLUMN experiment.plot.plot_order_number IS 'Plot Order Number: Order number of plot in the planting area [PLOT_ORDERNO]';

COMMENT ON COLUMN experiment.plot.pa_x IS 'Planting Area X: Coordinate paired with Planting Area Y that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAX]';

COMMENT ON COLUMN experiment.plot.pa_y IS 'Planting Area Y: Coordinate paired with Planting Area X that is generated by KDX or other tools and is used for mapping the plots in the planting area [PLOT_PAY]';

COMMENT ON COLUMN experiment.plot.field_x IS 'Field X: Coordinate paired with Field Y that determines the placement of the plot in the field [PLOT_FLDX]';

COMMENT ON COLUMN experiment.plot.field_y IS 'Field Y: Coordinate paired with Field X that determines the placement of the plot in the field [PLOT_FLDY]';

COMMENT ON COLUMN experiment.plot.plot_status IS 'Plot Status: Status of the plot {draft, active, replaced} [PLOT_STATUS]';

COMMENT ON COLUMN experiment.plot.plot_qc_code IS 'Plot QC Code: Quality control code assigned to the plot {G (good), Q (questionable), S (suppressed)} [PLOT_QCCODE]';

CREATE  TABLE experiment.plot_data ( 
	id                   serial  NOT NULL ,
	plot_id              integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	transaction_id       integer   ,
	collection_timestamp timestamp  NOT NULL ,
	CONSTRAINT pk_plot_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_plot_data_id ON experiment.plot_data IS 'A plot data is identified by its unique database identifier.';

CREATE INDEX plot_data_plot_id_variable_id_idx ON experiment.plot_data ( plot_id, variable_id );

COMMENT ON INDEX experiment.plot_data_plot_id_variable_id_idx IS 'A plot data can be retrieved by its variable within a plot.';

CREATE INDEX plot_data_plot_id_variable_id_data_value_idx ON experiment.plot_data ( plot_id, variable_id, data_value );

COMMENT ON INDEX experiment.plot_data_plot_id_variable_id_data_value_idx IS 'A plot data can be retrieved by its variable and data value within a plot.';

CREATE INDEX plot_data_plot_id_data_qc_code_idx ON experiment.plot_data ( plot_id, data_qc_code );

COMMENT ON INDEX experiment.plot_data_plot_id_data_qc_code_idx IS 'A plot data can be retrieved by its data QC code within a plot.';

COMMENT ON TABLE experiment.plot_data IS 'Plot Data: Data collected from a plot [PLOTDATA]';

COMMENT ON COLUMN experiment.plot_data.id IS 'Plot Data ID: Database identifier of the plot data [PLOTDATA_ID]';

COMMENT ON COLUMN experiment.plot_data.plot_id IS 'Plot ID: Reference to the plot where the data was collected [PLOTDATA_PLOT_ID]';

COMMENT ON COLUMN experiment.plot_data.variable_id IS 'Variable ID: Reference to the variable of the plot data [PLOTDATA_VAR_ID]';

COMMENT ON COLUMN experiment.plot_data.data_value IS 'Plot Data Value: Value of the data collected from the plot [PLOTDATA_DATAVAL]';

COMMENT ON COLUMN experiment.plot_data.data_qc_code IS 'Plot Data QC Code: Status of the plot data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PLOTDATA_QCCODE]';

COMMENT ON COLUMN experiment.plot_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [PLOTDATA_TXN_ID]';

COMMENT ON COLUMN experiment.plot_data.collection_timestamp IS 'Collection Timestamp: Timestamp when the plot data was collected [PLOTDATA_COLLTSTAMP]';

CREATE  TABLE experiment.subplot ( 
	id                   serial  NOT NULL ,
	subplot_code         varchar(64)  NOT NULL ,
	subplot_number       integer  NOT NULL ,
	subplot_type         varchar(64)  NOT NULL ,
	subplot_z_number     integer  NOT NULL ,
	plot_id              integer  NOT NULL ,
	parent_subplot_id    integer   ,
	CONSTRAINT pk_subplot_id PRIMARY KEY ( id ),
	CONSTRAINT subplot_subplot_code_idx UNIQUE ( subplot_code ) ,
	CONSTRAINT subplot_plot_id_subplot_code_idx UNIQUE ( plot_id, subplot_code ) ,
	CONSTRAINT subplot_plot_id_subplot_number_idx UNIQUE ( plot_id, subplot_number ) ,
	CONSTRAINT subplot_subplot_id_subplot_number_idx UNIQUE ( parent_subplot_id, subplot_number ) ,
	CONSTRAINT subplot_subplot_id_subplot_code_idx UNIQUE ( parent_subplot_id, subplot_code ) 
 );

COMMENT ON CONSTRAINT pk_subplot_id ON experiment.subplot IS 'A subplot is identified by its unique database ID.';

COMMENT ON CONSTRAINT subplot_subplot_code_idx ON experiment.subplot IS 'A subplot can be retrieved by its subplot code.';

COMMENT ON CONSTRAINT subplot_plot_id_subplot_code_idx ON experiment.subplot IS 'A subplot can be retrieved by its unique subplot code within a plot.';

COMMENT ON CONSTRAINT subplot_plot_id_subplot_number_idx ON experiment.subplot IS 'A subplot can be retrieved by its unique subplot number within a plot.';

COMMENT ON CONSTRAINT subplot_subplot_id_subplot_number_idx ON experiment.subplot IS 'A subplot can be retrieved by its unique subplot number within another subplot.';

COMMENT ON CONSTRAINT subplot_subplot_id_subplot_code_idx ON experiment.subplot IS 'A subplot can be retrieved by its unique subplot code within another subplot.';

COMMENT ON TABLE experiment.subplot IS 'Subplot: Subordinate of a plot, which may be a hill, a plant or another subplot [SUBPLOT]';

COMMENT ON COLUMN experiment.subplot.id IS 'Subplot ID: Database identifier of the subplot [SUBPLOT_ID]';

COMMENT ON COLUMN experiment.subplot.subplot_code IS 'Subplot Code: Textual identifier of the subplot [SUBPLOT_CODE]';

COMMENT ON COLUMN experiment.subplot.subplot_number IS 'Subplot Number: Sequential number of the subplot within the plot [SUBPLOT_NO]';

COMMENT ON COLUMN experiment.subplot.subplot_type IS 'Subplot Type: Type of the subplot {hill, row, any part of plant, ...} [SUBPLOT_TYPE]';

COMMENT ON COLUMN experiment.subplot.subplot_z_number IS 'Subplot Z Number: Ordering of subplot in the plot or another subplot [SUBPLOT_ZNO]';

COMMENT ON COLUMN experiment.subplot.plot_id IS 'Plot ID: Reference to the plot where the subplot is derived [SUBPLOT_PLOT_ID]';

COMMENT ON COLUMN experiment.subplot.parent_subplot_id IS 'Parent Subplot ID: Reference to the parent subplot of the subplot [SUBPLOT_SUBPLOT_ID]';

CREATE  TABLE experiment.subplot_data ( 
	id                   serial  NOT NULL ,
	subplot_id           integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	collection_timestamp timestamp  NOT NULL ,
	transaction_id       integer   ,
	CONSTRAINT pk_subplot_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_subplot_data_id ON experiment.subplot_data IS 'A subplot data is identified by its unique database identifier.';

CREATE INDEX subplot_data_subplot_id_variable_id_idx ON experiment.subplot_data ( subplot_id, variable_id );

COMMENT ON INDEX experiment.subplot_data_subplot_id_variable_id_idx IS 'A subplot data can be retrieved by its variable within a subplot.';

CREATE INDEX subplot_data_subplot_id_variable_id_data_value_idx ON experiment.subplot_data ( subplot_id, variable_id, data_value );

COMMENT ON INDEX experiment.subplot_data_subplot_id_variable_id_data_value_idx IS 'A subplot data can be retrieved by its variable and data value within a subplot.';

CREATE INDEX subplot_data_subplot_id_data_qc_code_idx ON experiment.subplot_data ( subplot_id, data_qc_code );

COMMENT ON INDEX experiment.subplot_data_subplot_id_data_qc_code_idx IS 'A subplot data can be retrieved by its data QC code within a subplot.';

COMMENT ON TABLE experiment.subplot_data IS 'Subplot Data: Data collected from a subplot [SUBPLOTDATA]';

COMMENT ON COLUMN experiment.subplot_data.id IS 'Subplot Data ID: Database identifier of the subplot data [SUBPLOTDATA_ID]';

COMMENT ON COLUMN experiment.subplot_data.subplot_id IS 'Subplot ID: Reference to the subplot where the data was collected [SUBPLOTDATA_SUBPLOT_ID]';

COMMENT ON COLUMN experiment.subplot_data.variable_id IS 'Variable ID: Reference to the variable of the plot data [SUBPLOTDATA_VAR_ID]';

COMMENT ON COLUMN experiment.subplot_data.data_value IS 'Subplot Data Value: Value of the data collected from the subplot [SUBPLOTDATA_DATAVAL]';

COMMENT ON COLUMN experiment.subplot_data.data_qc_code IS 'Subplot Data QC Code: Status of the subplot data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SUBPLOTDATA_QCCODE]';

COMMENT ON COLUMN experiment.subplot_data.collection_timestamp IS 'Collection Timestamp: Timestamp when the subplot data was collected [SUBPLOTDATA_COLLTSTAMP]';

COMMENT ON COLUMN experiment.subplot_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [SUBPLOTDATA_TXN_ID]';

CREATE  TABLE experiment.experiment_design ( 
	id                   serial  NOT NULL ,
	occurrence_id        integer  NOT NULL ,
	design_id            integer  NOT NULL ,
	plot_id              integer  NOT NULL ,
	block_type           varchar(64)  NOT NULL ,
	block_value          integer   ,
	block_level_number   integer  NOT NULL ,
	CONSTRAINT pk_experiment_design_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_experiment_design_id ON experiment.experiment_design IS 'An experiment design is identified by its unique database ID.';

CREATE INDEX experiment_design_occurrence_id_plot_id_idx ON experiment.experiment_design ( occurrence_id, plot_id );

COMMENT ON INDEX experiment.experiment_design_occurrence_id_plot_id_idx IS 'An experiment design can be retrieved by its plot within an occurrence.';

CREATE INDEX experiment_design_occurrence_id_design_id_idx ON experiment.experiment_design ( occurrence_id, design_id );

COMMENT ON INDEX experiment.experiment_design_occurrence_id_design_id_idx IS 'An experiment design can be retrieved by its design within an occurrence.';

CREATE INDEX experiment_design_occurrence_id_block_type_idx ON experiment.experiment_design ( occurrence_id, block_type );

COMMENT ON INDEX experiment.experiment_design_occurrence_id_block_type_idx IS 'An experiment design can be retrieved by its block type within an occurrence.';

CREATE INDEX experiment_design_occurrence_id_block_type_block_value_idx ON experiment.experiment_design ( occurrence_id, block_type, block_value );

COMMENT ON INDEX experiment.experiment_design_occurrence_id_block_type_block_value_idx IS 'An experiment design can be retrieved by its block type and block value within an occurrence.';

CREATE INDEX experiment_design_occurrence_id_block_level_number_idx ON experiment.experiment_design ( occurrence_id, block_level_number );

COMMENT ON INDEX experiment.experiment_design_occurrence_id_block_level_number_idx IS 'An experiment design can be retrieved by its block level number within an occurrence.';

COMMENT ON TABLE experiment.experiment_design IS 'Experiment Design: Design layout of experiment decision, treatment, and replication blocks [EXPTDES]';

COMMENT ON COLUMN experiment.experiment_design.id IS 'Experiment Design ID: Database identifier of the experiment design [EXPTDES_ID]';

COMMENT ON COLUMN experiment.experiment_design.occurrence_id IS 'Occurrence ID: Reference to the occurrence where the plot was randomized [EXPTDES_OCC_ID]';

COMMENT ON COLUMN experiment.experiment_design.design_id IS 'Design ID: Numeric identifier of the design array [EXPTDES_DESID]';

COMMENT ON COLUMN experiment.experiment_design.plot_id IS 'Plot ID: Reference to the plot that is being referred by the design value [EXPTDES_PLOT_ID]';

COMMENT ON COLUMN experiment.experiment_design.block_type IS 'Experiment Design Block Type: Type of the design block {treatment block, replication block} [EXPTDES_BLKTYPE]';

COMMENT ON COLUMN experiment.experiment_design.block_value IS 'Experiment Design Block Value: Value of the design block of the plot [EXPTDES_BLKVAL]';

COMMENT ON COLUMN experiment.experiment_design.block_level_number IS 'Experiment Design Block Level Number: Level number of the design block and value of the plot [EXPTDES_BLKLVLNO]';

CREATE  TABLE experiment.plant ( 
	id                   serial  NOT NULL ,
	plant_code           varchar(64)  NOT NULL ,
	plot_id              integer   ,
	subplot_id           integer   ,
	CONSTRAINT pk_plant_id PRIMARY KEY ( id ),
	CONSTRAINT plant_plot_id_plant_code_idx UNIQUE ( plot_id, plant_code ) ,
	CONSTRAINT plant_plot_id_plant_number_idx UNIQUE ( plot_id ) ,
	CONSTRAINT plant_subplot_id_plant_code_idx UNIQUE ( subplot_id, plant_code ) ,
	CONSTRAINT plant_subplot_id_plant_number_idx UNIQUE ( subplot_id ) 
 );

COMMENT ON CONSTRAINT pk_plant_id ON experiment.plant IS 'A plant is indentified by its unique database ID.';

CREATE INDEX plant_plant_code_idx ON experiment.plant ( plant_code );

COMMENT ON INDEX experiment.plant_plant_code_idx IS 'A plant can be retrieved by its plant code.';

COMMENT ON CONSTRAINT plant_plot_id_plant_code_idx ON experiment.plant IS 'A plant can be retrieved by its unique plot code within a plot.';

COMMENT ON CONSTRAINT plant_plot_id_plant_number_idx ON experiment.plant IS 'A plant can be retrieved by its unique plant number within a plot.';

COMMENT ON CONSTRAINT plant_subplot_id_plant_code_idx ON experiment.plant IS 'A plant can be retrieved by its unique plant code within a subplot.';

COMMENT ON CONSTRAINT plant_subplot_id_plant_number_idx ON experiment.plant IS 'A plant can be retrieved by its unique plant number within a subplot.';

COMMENT ON TABLE experiment.plant IS 'Plant: Plant that is planted in a plot or maybe a subplot [PLANT]';

COMMENT ON COLUMN experiment.plant.id IS 'Plant ID: Database identifier of the plant [PLANT_ID]';

COMMENT ON COLUMN experiment.plant.plant_code IS 'Plant Code: Textual identifier of the plant [PLANT_CODE]';

COMMENT ON COLUMN experiment.plant.plot_id IS 'Plot ID: Reference to the plot where the plant is planted [PLANT_PLOT_ID]';

COMMENT ON COLUMN experiment.plant.subplot_id IS 'Subplot ID: Reference to the subplot where the plant may be planted [PLANT_SUBPLOT_ID]';

CREATE  TABLE experiment.plant_data ( 
	id                   serial  NOT NULL ,
	plant_id             integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	collection_timestamp timestamp  NOT NULL ,
	transaction_id       integer   ,
	CONSTRAINT pk_plant_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_plant_data_id ON experiment.plant_data IS 'A plant data is identified by its unique database ID.';

CREATE INDEX plant_data_plant_id_variable_id_idx ON experiment.plant_data ( plant_id, variable_id );

COMMENT ON INDEX experiment.plant_data_plant_id_variable_id_idx IS 'A plant data can be retrieved by its variable within a plant.';

CREATE INDEX plant_data_plant_id_variable_id_data_value_idx ON experiment.plant_data ( plant_id, variable_id, data_value );

COMMENT ON INDEX experiment.plant_data_plant_id_variable_id_data_value_idx IS 'A plant data can be retrieved by its variable and data value within a plant.';

CREATE INDEX plant_data_plant_id_data_qc_code_idx ON experiment.plant_data ( plant_id, data_qc_code );

COMMENT ON INDEX experiment.plant_data_plant_id_data_qc_code_idx IS 'A plant data can be retrieved by its data QC code within a plant.';

COMMENT ON TABLE experiment.plant_data IS 'Plant Data: Data collected from a plant [PLANTDATA]';

COMMENT ON COLUMN experiment.plant_data.id IS 'Plant Data ID: Database identifier of the plant data [PLANTDATA_ID]';

COMMENT ON COLUMN experiment.plant_data.plant_id IS 'Plant ID: Reference to the plant where the observation was made [PLANTDATA_PLOT_ID]';

COMMENT ON COLUMN experiment.plant_data.variable_id IS 'Variable ID: Reference to the variable of the plant data [PLANTDATA_VAR_ID]';

COMMENT ON COLUMN experiment.plant_data.data_value IS 'Plant Data Value: Value of the data collected from the plot [PLANTDATA_DATAVAL]';

COMMENT ON COLUMN experiment.plant_data.data_qc_code IS 'Plant Data QC Code: Status of the plant data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PLANTDATA_QCCODE]';

COMMENT ON COLUMN experiment.plant_data.collection_timestamp IS 'Collection Timestamp: Timestamp when the plant data was collected [PLANTDATA_COLLTSTAMP]';

COMMENT ON COLUMN experiment.plant_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [PLANTDATA_TXN_ID]';

CREATE  TABLE experiment.planting_instruction ( 
	id                   integer  NOT NULL ,
	entry_code           varchar(64)  NOT NULL ,
	entry_number         integer  NOT NULL ,
	entry_name           varchar(100)  NOT NULL ,
	entry_type           varchar(64)  NOT NULL ,
	entry_role           varchar(64)   ,
	entry_class          varchar(64)   ,
	entry_status         varchar(64)  NOT NULL ,
	entry_id             integer  NOT NULL ,
	plot_id              integer  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	seed_id              integer  NOT NULL ,
	package_id           integer   ,
	CONSTRAINT pk_planting_instruction_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_planting_instruction_id ON experiment.planting_instruction IS 'A planting instruction is identified by its unique database ID.';

COMMENT ON TABLE experiment.planting_instruction IS 'Planting Instruction: Planting array that determines the assignment of envelopes to plots and location-specific planting instructions (e.g. replacement of germplasm) [PLANTINST]';

COMMENT ON COLUMN experiment.planting_instruction.id IS 'Planting Instruction ID: Database identifier of the planting instruction [PLANTINST_ID]';

COMMENT ON COLUMN experiment.planting_instruction.entry_code IS 'Entry Code: Textual identifier of the entry in the planting instruction [PLANTINST_ENTRY_CODE]';

COMMENT ON COLUMN experiment.planting_instruction.entry_number IS 'Entry Number: Member number of the entry in the experiment, which must not change throughout the experiment [PLANTINST_ENTRY_NO]';

COMMENT ON COLUMN experiment.planting_instruction.entry_name IS 'Entry Name: Name of the germplasm as an entry or possibly its replacement germplasm [PLANTINST_GERM_NAME]';

COMMENT ON COLUMN experiment.planting_instruction.entry_type IS 'Entry Type: Usage of the entry or possibly its replacement {test, check, control, filler, parent, cross} [PLANTINST_ENTRY_TYPE]';

COMMENT ON COLUMN experiment.planting_instruction.entry_role IS 'Entry Role: Function of the germplasm as an entry or possibly its replacement germplasm {female, male, female-and-male, spatial check, performance check} [PLANTINST_ENTRY_ROLE]';

COMMENT ON COLUMN experiment.planting_instruction.entry_class IS 'Entry Class: Grouping class of the entry in plot, which is mainly used in data analysis [PLANTINST_ENTRY_CLASS]';

COMMENT ON COLUMN experiment.planting_instruction.entry_status IS 'Entry Status: Status condition of the entry, which may also be the replacement germplasm {draft, active, replaced} [PLANTINST_ENTRY_STATUS]';

COMMENT ON COLUMN experiment.planting_instruction.entry_id IS 'Entry ID: Reference to the entry or possibly the original entry of the experiment before it was replaced [PLANTINST_ENTRY_ID]';

COMMENT ON COLUMN experiment.planting_instruction.plot_id IS 'Plot ID: Reference to the plot where the planting instruction is performed [PLANTINST_PLOT_ID]';

COMMENT ON COLUMN experiment.planting_instruction.germplasm_id IS 'Germplasm ID: Reference to the germplasm of the entry in the plot or possibly its replacement [PLANTINST_GERM_ID]';

COMMENT ON COLUMN experiment.planting_instruction.seed_id IS 'Seed ID: Reference to the seed of the germplasm to be planted and observed in the plot [PLANTINST_SEED_ID]';

COMMENT ON COLUMN experiment.planting_instruction.package_id IS 'Package ID: Reference to the package of seeds used for planting in the plot [PLANTINST_PKG_ID]';

CREATE  TABLE experiment.sample ( 
	id                   serial  NOT NULL ,
	sample_code          varchar(64)  NOT NULL ,
	sample_number        integer  NOT NULL ,
	sample_type          varchar(64)  NOT NULL ,
	location_id          integer  NOT NULL ,
	entry_id             integer   ,
	plot_id              integer   ,
	subplot_id           integer   ,
	plant_id             integer   ,
	seed_id              integer   ,
	geospatial_object_id integer   ,
	CONSTRAINT pk_sample_id PRIMARY KEY ( id ),
	CONSTRAINT sample_sample_code_idx UNIQUE ( sample_code ) 
 );

COMMENT ON CONSTRAINT pk_sample_id ON experiment.sample IS 'A sample is identified by its unique database ID.';

COMMENT ON CONSTRAINT sample_sample_code_idx ON experiment.sample IS 'A sample can be retrieved by its unique sample code.';

CREATE INDEX sample_sample_number_idx ON experiment.sample ( sample_number );

COMMENT ON INDEX experiment.sample_sample_number_idx IS 'A sample can be retrieved using its sample number.';

CREATE INDEX sample_sample_type_idx ON experiment.sample ( sample_type );

COMMENT ON INDEX experiment.sample_sample_type_idx IS 'A sample can be retrieved by its sample type.';

CREATE INDEX sample_location_id_idx ON experiment.sample ( location_id );

COMMENT ON INDEX experiment.sample_location_id_idx IS 'A sample can be retrieved by its location.';

CREATE INDEX sample_entry_id_idx ON experiment.sample ( entry_id );

COMMENT ON INDEX experiment.sample_entry_id_idx IS 'A sample can be retrieved by its entry.';

CREATE INDEX sample_plot_id_idx ON experiment.sample ( plot_id );

COMMENT ON INDEX experiment.sample_plot_id_idx IS 'A sample can be retrieved by its plot.';

CREATE INDEX sample_subplot_id_idx ON experiment.sample ( subplot_id );

COMMENT ON INDEX experiment.sample_subplot_id_idx IS 'A sample can be retrieved by its subplot.';

CREATE INDEX sample_plant_id_idx ON experiment.sample ( plant_id );

COMMENT ON INDEX experiment.sample_plant_id_idx IS 'A sample can be retrieved by its plant.';

CREATE INDEX sample_seed_id_idx ON experiment.sample ( seed_id );

COMMENT ON INDEX experiment.sample_seed_id_idx IS 'A sample can be retrieved by its seed.';

CREATE INDEX sample_geospatial_object_id_idx ON experiment.sample ( geospatial_object_id );

COMMENT ON INDEX experiment.sample_geospatial_object_id_idx IS 'A sample can be retrieved by its geospatial object.';

COMMENT ON TABLE experiment.sample IS 'Sample: Specimen of the crop taken for observation, testing, or analysis [SAMPLE]';

COMMENT ON COLUMN experiment.sample.id IS 'Sample ID: Database identifier of the sample [SAMPLE_ID]';

COMMENT ON COLUMN experiment.sample.sample_code IS 'Sample Code: Textual identifier of the sample [SAMPLE_CODE]';

COMMENT ON COLUMN experiment.sample.sample_number IS 'Sample Number: Sequential number of the sample within the observed entity [SAMPLE_NO]';

COMMENT ON COLUMN experiment.sample.sample_type IS 'Sample Type: Type of the sample {entry, plot, subplot, plant, organ, seed} [SAMPLE_TYPE]';

COMMENT ON COLUMN experiment.sample.location_id IS 'Location ID: Reference to the location where the entity is derived [SAMPLE_LOC_ID]';

COMMENT ON COLUMN experiment.sample.entry_id IS 'Entry ID: Reference to the entry where the sample is taken [SAMPLE_ENTRY_ID]';

COMMENT ON COLUMN experiment.sample.plot_id IS 'Plot ID: Reference to the plot where the sample is taken [SAMPLE_PLOT_ID]';

COMMENT ON COLUMN experiment.sample.subplot_id IS 'Subplot ID: Reference to the subplot where the sample is taken [SAMPLE_SUBPLOT_ID]';

COMMENT ON COLUMN experiment.sample.plant_id IS 'Plant ID: Reference to the plant where the sample is taken [SAMPLE_PLANT_ID]';

COMMENT ON COLUMN experiment.sample.seed_id IS 'Seed ID: Reference to the seed sample [SAMPLE_SEED_ID]';

COMMENT ON COLUMN experiment.sample.geospatial_object_id IS 'Geospatial Object ID: Reference to the geospatial object where the sample is found [SAMPLE_GEO_ID]';

CREATE  TABLE experiment.sample_measurement ( 
	id                   serial  NOT NULL ,
	sample_id            integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           integer  NOT NULL ,
	data_qc_code         varchar(64)  NOT NULL ,
	collection_timestamp timestamp  NOT NULL ,
	measurement_type     varchar(64)  NOT NULL ,
	transaction_id       integer   ,
	CONSTRAINT pk_sample_measurement_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_sample_measurement_id ON experiment.sample_measurement IS 'A sample measurement is identified by its unique database ID.';

CREATE INDEX sample_measurement_sample_id_idx ON experiment.sample_measurement ( sample_id );

COMMENT ON INDEX experiment.sample_measurement_sample_id_idx IS 'A sample measurement can be retrieved by its sample.';

CREATE INDEX sample_measurement_sample_id_variable_id_idx ON experiment.sample_measurement ( sample_id, variable_id );

COMMENT ON INDEX experiment.sample_measurement_sample_id_variable_id_idx IS 'A sample measurement can be retrieved by its variable within a sample.';

CREATE INDEX sample_measurement_sample_id_variable_id_data_value_idx ON experiment.sample_measurement ( sample_id, variable_id, data_value );

COMMENT ON INDEX experiment.sample_measurement_sample_id_variable_id_data_value_idx IS 'A sample measurement can be retrieved by its variable and data value within a sample.';

CREATE INDEX sample_measurement_sample_id_data_qc_code_idx ON experiment.sample_measurement ( sample_id, data_qc_code );

COMMENT ON INDEX experiment.sample_measurement_sample_id_data_qc_code_idx IS 'A sample measurement can be retrieved by its data QC code within a sample.';

COMMENT ON TABLE experiment.sample_measurement IS 'Sample Measurement: Collection of data from samples [SAMPMEASURE]';

COMMENT ON COLUMN experiment.sample_measurement.id IS 'Sample Measurement ID: Database identifier of the sample measurement [SAMPMEASURE_ID]';

COMMENT ON COLUMN experiment.sample_measurement.sample_id IS 'Sample ID: Reference to the sample where the measurement was made [SAMPMEASURE_SAMPLE_ID]';

COMMENT ON COLUMN experiment.sample_measurement.variable_id IS 'Variable ID: Reference to the variable of the sample measurement [SAMPMEASURE_VAR_ID]';

COMMENT ON COLUMN experiment.sample_measurement.data_value IS 'Sample Measurement Value: Value of the sample measurement [SAMPMEASURE_DATAVAL]';

COMMENT ON COLUMN experiment.sample_measurement.data_qc_code IS 'Sample Measurement Data QC Code: Assigned quality control code for the trait value {G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SAMPMEASURE_QCCODE]';

COMMENT ON COLUMN experiment.sample_measurement.collection_timestamp IS 'Collection Timestamp: Timestamp when the measurement was made [SAMPMEASURE_COLLTSTAMP]';

COMMENT ON COLUMN experiment.sample_measurement.measurement_type IS 'Sample Measurement Type: Type of measurement for the trait {regular, series, repeated} [SAMPMEASURE_TYPE]';

COMMENT ON COLUMN experiment.sample_measurement.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [SAMPMEASURE_TXN_ID]';

CREATE  TABLE experiment.planting_envelope ( 
	id                   serial  NOT NULL ,
	planting_instruction_id integer  NOT NULL ,
	envelope_id          integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_planting_envelope_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_planting_envelope_id ON experiment.planting_envelope IS 'A planting envelope is identified by its unique database identifier.';

COMMENT ON TABLE experiment.planting_envelope IS 'Planting Envelope: Allocation of one or many envelopes of seeds to a plot (e.g. 2 planting envelopes for a machine-planter per plot for an 8-row plot) [PLANTENVLP]';

COMMENT ON COLUMN experiment.planting_envelope.id IS 'Planting Envelope ID: Database identifier of the planting envelope [PLANTENVLP_ID]';

COMMENT ON COLUMN experiment.planting_envelope.planting_instruction_id IS 'Planting Instruction ID: Reference to the planting instruction of the plot where the envelope is allocated [PLANTENVLP_PLANTINST_ID]';

COMMENT ON COLUMN experiment.planting_envelope.envelope_id IS 'Envelope ID: Reference to the envelope that is allocated for planting in the plot [PLANTENVLP_ENVLP_ID]';

COMMENT ON COLUMN experiment.planting_envelope.order_number IS 'Planting Envelope Order Number: Ordering of the planting envelope within the planting instruction of the plot [PLANTENVLP_ORDERNO]';

CREATE  TABLE germplasm.product_profile ( 
	id                   serial  NOT NULL ,
	product_profile_code varchar(64)  NOT NULL ,
	product_profile_name varchar(128)  NOT NULL ,
	description          text   ,
	project_id           integer   ,
	pipeline_id          integer   ,
	CONSTRAINT pk_product_profile_id PRIMARY KEY ( id ),
	CONSTRAINT product_profile_code_unq UNIQUE ( product_profile_code ) 
 );

COMMENT ON CONSTRAINT pk_product_profile_id ON germplasm.product_profile IS 'A product profile is identified by its unique database ID.';

COMMENT ON CONSTRAINT product_profile_code_unq ON germplasm.product_profile IS 'A product profile can be retrieved by its unique product profile code.';

CREATE INDEX product_profile_product_profile_name_idx ON germplasm.product_profile ( product_profile_name );

COMMENT ON INDEX germplasm.product_profile_product_profile_name_idx IS 'A product profile can be retrieved by its product profile name.';

CREATE INDEX product_profile_project_id_idx ON germplasm.product_profile ( project_id );

COMMENT ON INDEX germplasm.product_profile_project_id_idx IS 'A product profile can be retrieved by its project.';

CREATE INDEX product_profile_pipeline_id_idx ON germplasm.product_profile ( pipeline_id );

COMMENT ON INDEX germplasm.product_profile_pipeline_id_idx IS 'A product profile can be retrieved by its pipeline.';

COMMENT ON TABLE germplasm.product_profile IS 'Product Profile: Outcome of the development and testing of germplasm in a defined scheme, which satisfies the requirements prepared by a program like possessing desirable traits to target a particular market segment [PRODPROF]';

COMMENT ON COLUMN germplasm.product_profile.id IS 'Product Profile ID: Database identifier of the product profile [PRODPROF_ID]';

COMMENT ON COLUMN germplasm.product_profile.product_profile_code IS 'Product Profile Code: Textual identifier of the product profile [PRODPROF_CODE]';

COMMENT ON COLUMN germplasm.product_profile.product_profile_name IS 'Product Profile Name: Full name of the product profile [PRODPROF_NAME]';

COMMENT ON COLUMN germplasm.product_profile.description IS 'Product Profile Description: Additional information about the product profile [PRODPROF_DESC]';

COMMENT ON COLUMN germplasm.product_profile.project_id IS 'Project ID: Reference to the project where the product profile is created [PRODPROF_PROJ_ID]';

COMMENT ON COLUMN germplasm.product_profile.pipeline_id IS 'Pipeline ID: Reference to the pipeline where the product profile may be conceptualized [PRODPROF_PIPE_ID]';

CREATE  TABLE germplasm.product_profile_attribute ( 
	id                   serial  NOT NULL ,
	product_profile_id   integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	CONSTRAINT pk_product_profile_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_product_profile_data_id ON germplasm.product_profile_attribute IS 'A product profile attribute is identified by its unique database ID.';

CREATE INDEX product_profile_attribute_product_profile_id_variable_id_idx ON germplasm.product_profile_attribute ( product_profile_id, variable_id );

COMMENT ON INDEX germplasm.product_profile_attribute_product_profile_id_variable_id_idx IS 'A product profile attribute can be retrieved by its variable within a product profile.';

CREATE INDEX product_profile_attribute_product_profile_id_variable_id_data_value_idx ON germplasm.product_profile_attribute ( product_profile_id, variable_id, data_value );

COMMENT ON INDEX germplasm.product_profile_attribute_product_profile_id_variable_id_data_value_idx IS 'A product profile attribute can be retrieved by its variable and data value within a product profile.';

COMMENT ON TABLE germplasm.product_profile_attribute IS 'Product Profile Data: Additional information of the product profile [PPATTR]';

COMMENT ON COLUMN germplasm.product_profile_attribute.id IS 'Product Profile Attribute ID: Database identifier of the product profile attribute [PPATTR_ID]';

COMMENT ON COLUMN germplasm.product_profile_attribute.product_profile_id IS 'Product Profile ID: Reference to the product profile where the attribute is linked [PPATTR_PRODPROF_ID]';

COMMENT ON COLUMN germplasm.product_profile_attribute.variable_id IS 'Variable ID: Referene to the variable of the product profile data [PPATTR_VAR_ID]';

COMMENT ON COLUMN germplasm.product_profile_attribute.data_value IS 'Product Profile Attribute Value: Value of the product profile attribute [PPATTR_DATAVAL]';

CREATE  TABLE germplasm.taxonomy ( 
	id                   serial  NOT NULL ,
	taxon_id             varchar(64)  NOT NULL ,
	taxonomy_name        varchar(128)  NOT NULL ,
	description          text   ,
	crop_id              integer  NOT NULL ,
	CONSTRAINT pk_taxonomy_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_taxonomy_id ON germplasm.taxonomy IS 'A taxonomy is identified by its unique database ID.';

CREATE INDEX taxonomy_taxon_id_idx ON germplasm.taxonomy ( taxon_id );

COMMENT ON INDEX germplasm.taxonomy_taxon_id_idx IS 'A taxonomy can be retrieved by its taxon ID.';

CREATE INDEX taxonomy_taxonomy_name_idx ON germplasm.taxonomy ( taxonomy_name );

COMMENT ON INDEX germplasm.taxonomy_taxonomy_name_idx IS 'A taxonomy can be retrieved by its taxonomy name.';

COMMENT ON TABLE germplasm.taxonomy IS 'Taxonomy: Schema of classifying a germplasm [TAXON]';

COMMENT ON COLUMN germplasm.taxonomy.id IS 'Taxonomy ID: Database identifier of the taxonomy [TAXON_ID]';

COMMENT ON COLUMN germplasm.taxonomy.taxon_id IS 'Taxon ID: Recognized identity or essential character of the germplasm [TAXON_ID]';

COMMENT ON COLUMN germplasm.taxonomy.taxonomy_name IS 'Taxonomy Name: Full name of the taxonomic identity [TAXON_NAME]';

COMMENT ON COLUMN germplasm.taxonomy.description IS 'Taxonomy Description: Additional information about the taxonomy [TAXON_DESC]';

COMMENT ON COLUMN germplasm.taxonomy.crop_id IS 'Crop ID: Reference to the crop of the taxonomy [TAXON_CROP_ID]';

CREATE  TABLE germplasm.germplasm ( 
	id                   serial  NOT NULL ,
	designation          varchar(256)  NOT NULL ,
	parentage            varchar  NOT NULL ,
	generation           varchar(64)  NOT NULL ,
	germplasm_state      varchar(64)  NOT NULL ,
	germplasm_name_type  varchar(64)  NOT NULL ,
	crop_id              integer  NOT NULL ,
	taxonomy_id          integer  NOT NULL ,
	product_profile_id   integer   ,
	germplasm_normalized_name varchar(256)  NOT NULL ,
	CONSTRAINT pk_germplasm_id PRIMARY KEY ( id ),
	CONSTRAINT germplasm_designation_idx UNIQUE ( designation ) ,
	CONSTRAINT germplasm_germplasm_normalized_name_idx UNIQUE ( germplasm_normalized_name ) 
 );

COMMENT ON CONSTRAINT pk_germplasm_id ON germplasm.germplasm IS 'A germplasm is identified by its unique database ID.';

COMMENT ON CONSTRAINT germplasm_designation_idx ON germplasm.germplasm IS 'A germplasm can be retrieved by its unique designation.';

CREATE INDEX germplasm_germplasm_state_idx ON germplasm.germplasm ( germplasm_state );

COMMENT ON INDEX germplasm.germplasm_germplasm_state_idx IS 'A germplasm can be retrieved by its germplasm state.';

COMMENT ON CONSTRAINT germplasm_germplasm_normalized_name_idx ON germplasm.germplasm IS 'A germplasm can be retrieved by its unique germplasm normalized name.';

COMMENT ON TABLE germplasm.germplasm IS 'Germplasm: Genetic material of a crop developed and produced in experiments in various breeding programs [GERM]';

COMMENT ON COLUMN germplasm.germplasm.id IS 'Germplasm ID: Database identifier of the germplasm [GERM_ID]';

COMMENT ON COLUMN germplasm.germplasm.designation IS 'Designation: Standard designation/name of the germplasm [GERM_DESIGNATION]';

COMMENT ON COLUMN germplasm.germplasm.parentage IS 'Parentage: Parentage string of the germplasm [GERM_PARENTAGE]';

COMMENT ON COLUMN germplasm.germplasm.generation IS 'Generation: Stage of development of a germplasm [GERM_GENERATION]';

COMMENT ON COLUMN germplasm.germplasm.germplasm_state IS 'Germplasm State: Current state of the germplasm: (progeny, fixed line, ...) [GERM_STATE]';

COMMENT ON COLUMN germplasm.germplasm.germplasm_name_type IS 'Germplasm Name Type: Type of name designated to the germplasm [GERM_NAMETYPE]';

COMMENT ON COLUMN germplasm.germplasm.crop_id IS 'Crop ID: Reference to the germplasm''s crop [GERM_CROP_ID]';

COMMENT ON COLUMN germplasm.germplasm.taxonomy_id IS 'Taxonomy ID: Reference to the taxonomy of the germplasm [GERM_TAXON_ID]';

COMMENT ON COLUMN germplasm.germplasm.product_profile_id IS 'Product Profile ID: Reference to the product profile where the germplasm is developed [GERM_PRODPROF_ID]';

COMMENT ON COLUMN germplasm.germplasm.germplasm_normalized_name IS 'Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERM_NORMNAME]';

CREATE  TABLE germplasm.germplasm_attribute ( 
	id                   serial  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_germplasm_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_germplasm_attribute_id ON germplasm.germplasm_attribute IS 'A germplasm attribute is identified by its unique database ID.';

CREATE INDEX germplasm_attribute_germplasm_id_data_qc_code_idx ON germplasm.germplasm_attribute ( germplasm_id, data_qc_code );

COMMENT ON INDEX germplasm.germplasm_attribute_germplasm_id_data_qc_code_idx IS 'A germplasm attribute can be retrieved by its data QC code within a germplasm.';

CREATE INDEX germplasm_attribute_germplasm_id_variable_id_idx ON germplasm.germplasm_attribute ( germplasm_id, variable_id );

COMMENT ON INDEX germplasm.germplasm_attribute_germplasm_id_variable_id_idx IS 'A germplasm attribute can be retrieved by its variable within a germplasm.';

CREATE INDEX germplasm_attribute_germplasm_id_variable_id_data_value_idx ON germplasm.germplasm_attribute ( germplasm_id, variable_id, data_value );

COMMENT ON INDEX germplasm.germplasm_attribute_germplasm_id_variable_id_data_value_idx IS 'A germplasm attribute can be retrieved by its variable and data value within a germplasm.';

COMMENT ON TABLE germplasm.germplasm_attribute IS 'Germplasm Attribute: Attribute linked to the germplasm [GERMATTR]';

COMMENT ON COLUMN germplasm.germplasm_attribute.id IS 'Germplasm Attribute ID: Database identifier of the germplasm attribute [GERMATTR_ID]';

COMMENT ON COLUMN germplasm.germplasm_attribute.germplasm_id IS 'Germplasm ID: Reference to the germplasm having the attribute [GERMATTR_GERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_attribute.variable_id IS 'Variable ID: Reference to the variable of the germplasm attribute [GERMATTR_VAR_ID]';

COMMENT ON COLUMN germplasm.germplasm_attribute.data_value IS 'Germplasm Attribute Data Value: Value of the germplasm attribute [GERMATTR_DATAVAL]';

COMMENT ON COLUMN germplasm.germplasm_attribute.data_qc_code IS 'Germlasm Attribute Data QC Code: Status of the germplasm attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMATTR_QCCODE]';

CREATE  TABLE germplasm.germplasm_name ( 
	id                   serial  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	name_value           varchar(256)  NOT NULL ,
	germplasm_name_type  varchar(64)  NOT NULL ,
	germplasm_name_status varchar(64)  NOT NULL ,
	germplasm_normalized_name varchar(256)  NOT NULL ,
	CONSTRAINT pk_germplasm_name_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_germplasm_name_id ON germplasm.germplasm_name IS 'A germplasm is identified by its unique database ID.';

CREATE INDEX germplasm_name_value_idx ON germplasm.germplasm_name ( name_value );

COMMENT ON INDEX germplasm.germplasm_name_value_idx IS 'A germplasm name can be retrieved by its name value.';

CREATE INDEX germplasm_name_germplasm_id_status_idx ON germplasm.germplasm_name ( germplasm_id, germplasm_name_status );

COMMENT ON INDEX germplasm.germplasm_name_germplasm_id_status_idx IS 'A germplasm name can be retrieved by its name status within a germplasm.';

CREATE INDEX germplasm_name_normalized_name_idx ON germplasm.germplasm_name ( germplasm_normalized_name );

COMMENT ON INDEX germplasm.germplasm_name_normalized_name_idx IS 'A germplasm name can be retrieved by its normalized.';

COMMENT ON TABLE germplasm.germplasm_name IS 'Germplasm Name: Name designated to the germplasm [GERMNAME]';

COMMENT ON COLUMN germplasm.germplasm_name.id IS 'Germplasm Name ID: Database identifier of the germplasm name [GERMNAME_ID]';

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_id IS 'Germplasm ID: Reference to the germplasm designated with the name [GERMNAME_GERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_name.name_value IS 'Germplasm Name Value: Value of the name designated to the germplasm [GERMNAME_NAMEVAL]';

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_name_type IS 'Germplasm Name Type: Type of name designated to the germplasm [GERMNAME_NAMETYPE]';

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_name_status IS 'Germplasm Name Status: Status of the germplasm name {standard, active, deprecated, inactive, voided} [GERMNAME_NAMESTATUS]';

COMMENT ON COLUMN germplasm.germplasm_name.germplasm_normalized_name IS 'Germplasm Normalized Name: Formatted germplasm name that is standardized for query purposes [GERMNAME_NORMNAME]';

CREATE  TABLE germplasm.germplasm_relation ( 
	id                   serial  NOT NULL ,
	parent_germplasm_id  integer  NOT NULL ,
	child_germplasm_id   integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_germplasm_relation_id PRIMARY KEY ( id ),
	CONSTRAINT germplasm_relation_parent_germplasm_id_order_number_idx UNIQUE ( parent_germplasm_id, order_number ) ,
	CONSTRAINT germplasm_relation_parent_germplasm_id_child_germplasm_id_idx UNIQUE ( parent_germplasm_id, child_germplasm_id ) 
 );

COMMENT ON CONSTRAINT pk_germplasm_relation_id ON germplasm.germplasm_relation IS 'A germplasm relation is identified by its unique database ID.';

COMMENT ON CONSTRAINT germplasm_relation_parent_germplasm_id_order_number_idx ON germplasm.germplasm_relation IS 'A germplasm relation can be retrieved using its unique order number within a germplasm (parent).';

COMMENT ON CONSTRAINT germplasm_relation_parent_germplasm_id_child_germplasm_id_idx ON germplasm.germplasm_relation IS 'A germplasm relation can be retrieved using its unique germplasm (child) within another germplasm (parent).';

COMMENT ON TABLE germplasm.germplasm_relation IS 'Germplasm Relation: Relationship of a germplasm with another germplasm [GERMREL]';

COMMENT ON COLUMN germplasm.germplasm_relation.id IS 'Germplasm Relation ID: Database identifier of the germplasm relation [GERMREL_ID]';

COMMENT ON COLUMN germplasm.germplasm_relation.parent_germplasm_id IS 'Parent Germplasm ID: Reference to the parent germplasm [GERMREL_PGERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_relation.child_germplasm_id IS 'Child Germplasm ID: Reference to the child germplasm [GERMREL_CGERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_relation.order_number IS 'Germplasm Relation Order Number: Ordering of the child germplasm within the parent germplasm [GERMREL_ORDERNO]';

CREATE  TABLE germplasm.germplasm_state_attribute ( 
	id                   serial  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	germplasm_state_type varchar(64)  NOT NULL ,
	germplasm_state_rationale varchar(256)  NOT NULL ,
	CONSTRAINT pk_germplasm_state_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_germplasm_state_attribute_id ON germplasm.germplasm_state_attribute IS 'A germplasm state attribute is identified by its unique database ID.';

CREATE INDEX germplasm_state_attribute_germplasm_id_germplasm_state_type_idx ON germplasm.germplasm_state_attribute ( germplasm_id, germplasm_state_type );

COMMENT ON INDEX germplasm.germplasm_state_attribute_germplasm_id_germplasm_state_type_idx IS 'A germplasm state attribute can be retrieved by its germplasm state type within a germplasm.';

COMMENT ON TABLE germplasm.germplasm_state_attribute IS 'Germplasm State Attribute: Rationale of the state of the germplasm [GSATTR]';

COMMENT ON COLUMN germplasm.germplasm_state_attribute.id IS 'Germplasm State Attribute ID: Database identifier of the germplasm state attribute [GSATTR_ID]';

COMMENT ON COLUMN germplasm.germplasm_state_attribute.germplasm_id IS 'Germplasm ID: Reference to the germplasm where the rationale is attributed [GSATTR_GERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_state_attribute.germplasm_state_type IS 'Germplasm State Type: Type of state of the germplasm [GSATTR_STATETYPE]';

COMMENT ON COLUMN germplasm.germplasm_state_attribute.germplasm_state_rationale IS 'Germplasm State Rationale: Reason of the state of the germplasm [GSATTR_RATIONALE]';

CREATE  TABLE germplasm.germplasm_trait ( 
	id                   serial  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_germplasm_trait_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_germplasm_trait_id ON germplasm.germplasm_trait IS 'A germplasm trait is identified by its unique database ID.';

CREATE INDEX germplasm_trait_germplasm_id_data_qc_code_idx ON germplasm.germplasm_trait ( germplasm_id, data_qc_code );

COMMENT ON INDEX germplasm.germplasm_trait_germplasm_id_data_qc_code_idx IS 'A germplasm trait can be retrieved by its data QC code within a germplasm.';

CREATE INDEX germplasm_trait_germplasm_id_idx ON germplasm.germplasm_trait ( germplasm_id );

COMMENT ON INDEX germplasm.germplasm_trait_germplasm_id_idx IS 'A germplasm trait can be retrieved by its germplasm.';

CREATE INDEX germplasm_trait_germplasm_id_variable_id_idx ON germplasm.germplasm_trait ( germplasm_id, variable_id );

COMMENT ON INDEX germplasm.germplasm_trait_germplasm_id_variable_id_idx IS 'A germplasm trait can be retrieved by its variable within a germplasm.';

CREATE INDEX germplasm_trait_germplasm_id_variable_id_data_value_idx ON germplasm.germplasm_trait ( germplasm_id, variable_id, data_value );

COMMENT ON INDEX germplasm.germplasm_trait_germplasm_id_variable_id_data_value_idx IS 'A germplasm trait can be retrieved by its variable and data value within a germplasm.';

COMMENT ON TABLE germplasm.germplasm_trait IS 'Germplasm Trait: Trait observed from the germplasm [GERMTRAIT]';

COMMENT ON COLUMN germplasm.germplasm_trait.id IS 'Germplasm Trait ID: Database identifier of the germplasm trait [GERMTRAIT_ID]';

COMMENT ON COLUMN germplasm.germplasm_trait.germplasm_id IS 'Germplasm ID: Reference to the germplasm having the trait [GERMTRAIT_GERM_ID]';

COMMENT ON COLUMN germplasm.germplasm_trait.variable_id IS 'Variable ID: Reference to the variable of the germplasm trait [GERMTRAIT_VAR_ID]';

COMMENT ON COLUMN germplasm.germplasm_trait.data_value IS 'Germplasm Trait Data Value: Value of the germplasm trait [GERMTRAIT_DATAVAL]';

COMMENT ON COLUMN germplasm.germplasm_trait.data_qc_code IS 'Germplasm Trait Data QC Code: Quality of the germplasm trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [GERMTRAIT_QCCODE]';

CREATE  TABLE germplasm.seed ( 
	id                   serial  NOT NULL ,
	seed_code            varchar(64)  NOT NULL ,
	seed_name            varchar(128)  NOT NULL ,
	harvest_date         date   ,
	harvest_method       varchar(64)   ,
	germplasm_id         integer  NOT NULL ,
	program_id           integer   ,
	source_experiment_id integer   ,
	source_entry_id      integer   ,
	source_occurrence_id integer   ,
	source_location_id   integer   ,
	source_plot_id       integer   ,
	description          text   ,
	CONSTRAINT pk_seed_id PRIMARY KEY ( id ),
	CONSTRAINT seed_seed_code_idx UNIQUE ( seed_code ) ,
	CONSTRAINT seed_germplasm_id_seed_code_idx UNIQUE ( germplasm_id, seed_code ) 
 );

COMMENT ON CONSTRAINT pk_seed_id ON germplasm.seed IS 'A seed is identified by its unique database ID.';

COMMENT ON CONSTRAINT seed_seed_code_idx ON germplasm.seed IS 'A seed can be retrieved by its unique seed code.';

CREATE INDEX seed_seed_name_idx ON germplasm.seed ( seed_name );

COMMENT ON INDEX germplasm.seed_seed_name_idx IS 'A seed can be retrieved by its seed name.';

CREATE INDEX seed_germplasm_id_idx ON germplasm.seed ( germplasm_id );

COMMENT ON INDEX germplasm.seed_germplasm_id_idx IS 'A seed can be retrieved by its germplasm.';

COMMENT ON CONSTRAINT seed_germplasm_id_seed_code_idx ON germplasm.seed IS 'A seed can be retrieved by its unique seed code within a germplasm.';

CREATE INDEX seed_program_id_idx ON germplasm.seed ( program_id );

COMMENT ON INDEX germplasm.seed_program_id_idx IS 'A seed can be retrieved by its program.';

CREATE INDEX seed_source_experiment_id_idx ON germplasm.seed ( source_experiment_id );

COMMENT ON INDEX germplasm.seed_source_experiment_id_idx IS 'A seed can be retrieved by its source experiment.';

CREATE INDEX seed_source_entry_id_idx ON germplasm.seed ( source_entry_id );

COMMENT ON INDEX germplasm.seed_source_entry_id_idx IS 'A seed can be retrieved by its source entry.';

CREATE INDEX seed_source_occurrence_id_idx ON germplasm.seed ( source_occurrence_id );

COMMENT ON INDEX germplasm.seed_source_occurrence_id_idx IS 'A seed can be retrieved by its source occurrence.';

CREATE INDEX seed_source_location_id_idx ON germplasm.seed ( source_location_id );

COMMENT ON INDEX germplasm.seed_source_location_id_idx IS 'A seed can be retrieved by its source location.';

CREATE INDEX seed_source_plot_id_idx ON germplasm.seed ( source_plot_id );

COMMENT ON INDEX germplasm.seed_source_plot_id_idx IS 'A seed can be retrieved by its source plot.';

COMMENT ON TABLE germplasm.seed IS 'Seed: Genetic entity of a germplasm [SEED]';

COMMENT ON COLUMN germplasm.seed.id IS 'Seed ID: Database identifier of the seed [SEED_ID]';

COMMENT ON COLUMN germplasm.seed.seed_code IS 'Seed Code: Textual identifier of the seed [SEED_CODE]';

COMMENT ON COLUMN germplasm.seed.seed_name IS 'Seed Name: Full name of the germplasm at the time the seed was harvested [SEED_NAME]';

COMMENT ON COLUMN germplasm.seed.harvest_date IS 'Seed Harvest Date: Date when the seeds were harvested from the location of an experiment [SEED_HARVESTDATE]';

COMMENT ON COLUMN germplasm.seed.harvest_method IS 'Harvest Method: Method of harvesting the plots in the location [SEED_HARVMETH]';

COMMENT ON COLUMN germplasm.seed.germplasm_id IS 'Germplasm ID: Reference to the germplasm where the seed is referred [SEED_GERM_ID]';

COMMENT ON COLUMN germplasm.seed.program_id IS 'Program ID: Reference to the program that owns the seeds [SEED_PROG_ID]';

COMMENT ON COLUMN germplasm.seed.source_experiment_id IS 'Source Experiment ID: Reference to the experiment where the seed was harvested [SEED_EXPT_ID]';

COMMENT ON COLUMN germplasm.seed.source_entry_id IS 'Source Entry ID: Reference to the entry where the seed was harvested [SEED_ENTRY_ID]';

COMMENT ON COLUMN germplasm.seed.source_occurrence_id IS 'Source Occurrence ID: Reference to the occurrence where the seed was harvested [SEED_OCC_ID]';

COMMENT ON COLUMN germplasm.seed.source_location_id IS 'Source Location ID: Location where the seed was harvested [SEED_LOC_ID]';

COMMENT ON COLUMN germplasm.seed.source_plot_id IS 'Source Plot ID: Reference to the plot where the seed was harvested [SEED_PLOT_ID]';

COMMENT ON COLUMN germplasm.seed.description IS 'Seed Description: Additional information about the seed [SEED_DESC]';

CREATE  TABLE germplasm.seed_attribute ( 
	id                   serial  NOT NULL ,
	seed_id              integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_seed_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_seed_attribute_id ON germplasm.seed_attribute IS 'A seed attribute is identified by its unique database ID.';

CREATE INDEX seed_attribute_seed_id_variable_id_idx ON germplasm.seed_attribute ( seed_id, variable_id );

COMMENT ON INDEX germplasm.seed_attribute_seed_id_variable_id_idx IS 'A seed attribute can be retrieved by its variable within a seed.';

CREATE INDEX seed_attribute_seed_id_variable_id_data_value_idx ON germplasm.seed_attribute ( seed_id, variable_id, data_value );

COMMENT ON INDEX germplasm.seed_attribute_seed_id_variable_id_data_value_idx IS 'A seed attribute can be retrieved by its variable and data value within a seed.';

CREATE INDEX seed_attribute_seed_id_data_qc_code_idx ON germplasm.seed_attribute ( seed_id, data_qc_code );

COMMENT ON INDEX germplasm.seed_attribute_seed_id_data_qc_code_idx IS 'A seed attribute can be retrieved by its data QC code within a seed.';

COMMENT ON TABLE germplasm.seed_attribute IS 'Seed Attribute: Attribute linked to the seed [SEEDATTR]';

COMMENT ON COLUMN germplasm.seed_attribute.id IS 'Seed Attribute ID: Database identifier of the seed attribute [SEEDATTR_ID]';

COMMENT ON COLUMN germplasm.seed_attribute.seed_id IS 'Seed ID: Reference to the seed having the attribute [SEEDATTR_SEED_ID]';

COMMENT ON COLUMN germplasm.seed_attribute.variable_id IS 'Seed Attribute Variable ID: Reference to the variable of the seed attribute [SEEDATTR_VAR_ID]';

COMMENT ON COLUMN germplasm.seed_attribute.data_value IS 'Seed Attribute Data Value: Value of the seed attribute [SEEDATTR_DATAVAL]';

COMMENT ON COLUMN germplasm.seed_attribute.data_qc_code IS 'Seed Attribute Data QC Code: Quality of the seed attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDATTR_QCCODE]';

CREATE  TABLE germplasm.seed_relation ( 
	id                   serial  NOT NULL ,
	parent_seed_id       integer  NOT NULL ,
	child_seed_id        integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_seed_relation_id PRIMARY KEY ( id ),
	CONSTRAINT seed_relation_parent_seed_id_child_seed_id_idx UNIQUE ( parent_seed_id, child_seed_id ) ,
	CONSTRAINT seed_relation_parent_seed_id_order_number_idx UNIQUE ( parent_seed_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_seed_relation_id ON germplasm.seed_relation IS 'A seed relation is identified by its unique database ID.';

COMMENT ON CONSTRAINT seed_relation_parent_seed_id_child_seed_id_idx ON germplasm.seed_relation IS 'A seed relation can be retrieved by its unique child seed within a parent seed.';

COMMENT ON CONSTRAINT seed_relation_parent_seed_id_order_number_idx ON germplasm.seed_relation IS 'A seed relation can be retrieved by its unique order number within a parent seed.';

COMMENT ON TABLE germplasm.seed_relation IS 'Seed Relation: Relation of a seed with another seed, possible due to the descent of a seed from another seed or a combination of two distinct seeds of the same germplasm [SEEDREL]';

COMMENT ON COLUMN germplasm.seed_relation.id IS 'Seed Relation ID: Database identifier of the seed relation [SEEDREL_ID]';

COMMENT ON COLUMN germplasm.seed_relation.parent_seed_id IS 'Parent Seed ID: Reference to the parent seed [SEEDREL_PSEED_ID]';

COMMENT ON COLUMN germplasm.seed_relation.child_seed_id IS 'Child Seed ID: Reference to the child seed [SEEDREL_CSEED_ID]';

COMMENT ON COLUMN germplasm.seed_relation.order_number IS 'Seed Relation Order Number: Order of the child seed within the parent seed [SEEDREL_ORDERNO]';

CREATE  TABLE germplasm.seed_trait ( 
	id                   serial  NOT NULL ,
	seed_id              integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           integer  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_seed_trait_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_seed_trait_id ON germplasm.seed_trait IS 'A seed is identified by its unique database ID.';

CREATE INDEX seed_trait_seed_id_data_qc_code_idx ON germplasm.seed_trait ( seed_id, data_qc_code );

COMMENT ON INDEX germplasm.seed_trait_seed_id_data_qc_code_idx IS 'A seed trait can be retrieved by its data QC code within a seed.';

CREATE INDEX seed_trait_seed_id_variable_id_idx ON germplasm.seed_trait ( seed_id, variable_id );

COMMENT ON INDEX germplasm.seed_trait_seed_id_variable_id_idx IS 'A seed trait can be retrieved by its variable within a seed.';

CREATE INDEX seed_trait_seed_id_variable_id_data_value_idx ON germplasm.seed_trait ( seed_id, variable_id, data_value );

COMMENT ON INDEX germplasm.seed_trait_seed_id_variable_id_data_value_idx IS 'A seed trait can be retrieved by its variable and data value within a seed.';

COMMENT ON TABLE germplasm.seed_trait IS 'Seed Trait: Trait linked to the seed [SEEDTRAIT]';

COMMENT ON COLUMN germplasm.seed_trait.id IS 'Seed Trait ID: Database identifier of the seed trait [SEEDTRAIT_ID]';

COMMENT ON COLUMN germplasm.seed_trait.seed_id IS 'Seed ID: Reference to the seed having the trait [SEEDTRAIT_SEED_ID]';

COMMENT ON COLUMN germplasm.seed_trait.variable_id IS 'Variable ID: Reference to the variable of the seed trait [SEEDTRAIT_VAR_ID]';

COMMENT ON COLUMN germplasm.seed_trait.data_value IS 'Seed Trait Data Value: Value of the seed trait [SEEDTRAIT_DATAVAL]';

COMMENT ON COLUMN germplasm.seed_trait.data_qc_code IS 'Seed Trait Data QC Code: Status of the seed trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDTRAIT_QCCODE]';

CREATE  TABLE germplasm."cross" ( 
	id                   serial  NOT NULL ,
	cross_name           varchar(256)  NOT NULL ,
	cross_method         varchar(64)  NOT NULL ,
	germplasm_id         integer   ,
	seed_id              integer   ,
	experiment_id        integer   ,
	entry_id             integer   ,
	CONSTRAINT pk_cross_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_cross_id ON germplasm."cross" IS 'A cross is identified by its unique database ID.';

CREATE INDEX cross_cross_name_idx ON germplasm."cross" ( cross_name );

COMMENT ON INDEX germplasm.cross_cross_name_idx IS 'A cross can be retrieved by its cross name.';

CREATE INDEX cross_cross_method_idx ON germplasm."cross" ( cross_method );

COMMENT ON INDEX germplasm.cross_cross_method_idx IS 'A cross can be retrieved by its cross method.';

CREATE INDEX cross_germplasm_id_idx ON germplasm."cross" ( germplasm_id );

COMMENT ON INDEX germplasm.cross_germplasm_id_idx IS 'A cross can be retrieved by its germplasm.';

CREATE INDEX cross_seed_id_idx ON germplasm."cross" ( seed_id );

COMMENT ON INDEX germplasm.cross_seed_id_idx IS 'A cross can be retrieved by its seed.';

CREATE INDEX cross_experiment_id_idx ON germplasm."cross" ( experiment_id );

COMMENT ON INDEX germplasm.cross_experiment_id_idx IS 'A cross can be retrieved by its experiment.';

CREATE INDEX cross_entry_id_idx ON germplasm."cross" ( entry_id );

COMMENT ON INDEX germplasm.cross_entry_id_idx IS 'A cross can be retrieved by its entry.';

COMMENT ON TABLE germplasm."cross" IS 'Cross: Combination of parental germplasm to create a cross [CROSS]';

COMMENT ON COLUMN germplasm."cross".id IS 'Cross ID: Database identifier of the cross [CROSS_ID]';

COMMENT ON COLUMN germplasm."cross".cross_name IS 'Cross Name: Name of the cross, temporarily set to the combination of the parents'' names (A/B) but when marked as successfull, it is assigned with a cross name that complies with the naming convention of the organization [CROSS_NAME]';

COMMENT ON COLUMN germplasm."cross".cross_method IS 'Cross Method: Method of crossing used in the cross {single cross, backcross, ...} [CROSS_METHOD]';

COMMENT ON COLUMN germplasm."cross".germplasm_id IS 'Germplasm ID: Reference to the germplasm that is the outcome of the cross [CROSS_GERM_ID]';

COMMENT ON COLUMN germplasm."cross".seed_id IS 'Seed ID: Reference to the seed that first created the germplasm of the cross [CROSS_SEED_ID]';

COMMENT ON COLUMN germplasm."cross".experiment_id IS 'Experiment ID: Reference to the experiment where the cross has been made [CROSS_EXPT_ID]';

COMMENT ON COLUMN germplasm."cross".entry_id IS 'Entry ID: Reference to the entry in the experiment where the cross is added as an entry [CROSS_ENTRY_ID]';

CREATE  TABLE germplasm.cross_attribute ( 
	id                   serial  NOT NULL ,
	cross_id             integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(64)  NOT NULL ,
	CONSTRAINT pk_cross_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_cross_attribute_id ON germplasm.cross_attribute IS 'A cross attribute is identified by its unique database ID.';

CREATE INDEX cross_attribute_cross_id_data_qc_code_idx ON germplasm.cross_attribute ( cross_id, data_qc_code );

COMMENT ON INDEX germplasm.cross_attribute_cross_id_data_qc_code_idx IS 'A cross attribute can be retrieved by its data QC code within a cross.';

CREATE INDEX cross_attribute_cross_id_variable_id_idx ON germplasm.cross_attribute ( cross_id, variable_id );

COMMENT ON INDEX germplasm.cross_attribute_cross_id_variable_id_idx IS 'A cross attribute can be retrieved by its variable within a cross.';

CREATE INDEX cross_attribute_cross_id_variable_id_data_value_idx ON germplasm.cross_attribute ( cross_id, variable_id, data_value );

COMMENT ON INDEX germplasm.cross_attribute_cross_id_variable_id_data_value_idx IS 'A cross attribute can be retrieved by its variable and data value within a cross.';

COMMENT ON TABLE germplasm.cross_attribute IS 'Cross Attribute: Attribute linked to the cross [CROSSATTR]';

COMMENT ON COLUMN germplasm.cross_attribute.id IS 'Cross Attribute ID: Database identifier of the cross attribute [CROSSATTR_]';

COMMENT ON COLUMN germplasm.cross_attribute.cross_id IS 'Cross ID: Reference to the cross having the attribute [CROSSATTR_CROSS_ID]';

COMMENT ON COLUMN germplasm.cross_attribute.variable_id IS 'Variable ID: Reference to the variable of the cross attribute [CROSSATTR_VAR_ID]';

COMMENT ON COLUMN germplasm.cross_attribute.data_value IS 'Cross Attribute Data Value: Value of the cross attribute [CROSSATTR_DATAVAL]';

COMMENT ON COLUMN germplasm.cross_attribute.data_qc_code IS 'Cross Attribute Data QC Code: Quality of the cross attribute {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [CROSSATTR_QCCODE]';

CREATE  TABLE germplasm.cross_parent ( 
	id                   integer  NOT NULL ,
	cross_id             integer  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	seed_id              integer   ,
	parent_role          varchar(32)  NOT NULL ,
	order_number         integer  NOT NULL ,
	experiment_id        integer   ,
	entry_id             integer   ,
	plot_id              integer   ,
	CONSTRAINT pk_cross_parent_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_cross_parent_id ON germplasm.cross_parent IS 'A parent in a cross is identified by its unique database ID.';

CREATE INDEX cross_parent_cross_id_idx ON germplasm.cross_parent ( cross_id );

COMMENT ON INDEX germplasm.cross_parent_cross_id_idx IS 'A parent in a cross can be retrieved by its cross.';

CREATE INDEX cross_parent_cross_id_parent_role_idx ON germplasm.cross_parent ( cross_id, parent_role );

COMMENT ON INDEX germplasm.cross_parent_cross_id_parent_role_idx IS 'A parent in a cross can be retrieved by its parent role in the cross.';

CREATE INDEX cross_parent_seed_id_idx ON germplasm.cross_parent ( seed_id );

COMMENT ON INDEX germplasm.cross_parent_seed_id_idx IS 'A parent in a cross can be retrieved by its seed.';

CREATE INDEX cross_parent_experiment_id_idx ON germplasm.cross_parent ( experiment_id );

COMMENT ON INDEX germplasm.cross_parent_experiment_id_idx IS 'A parent in a cross can be retrieved by its experiment.';

CREATE INDEX cross_parent_entry_id_idx ON germplasm.cross_parent ( entry_id );

COMMENT ON INDEX germplasm.cross_parent_entry_id_idx IS 'A parent in a cross can be retrieved by its entry.';

CREATE INDEX cross_parent_plot_id_idx ON germplasm.cross_parent ( plot_id );

CREATE INDEX cross_parent_germplasm_id_idx ON germplasm.cross_parent ( germplasm_id );

COMMENT ON INDEX germplasm.cross_parent_germplasm_id_idx IS 'A parent in a cross can be retrieved by its germplasm.';

COMMENT ON INDEX germplasm.cross_parent_plot_id_idx IS 'A parent in a cross can be retrieved by its plot.';

COMMENT ON TABLE germplasm.cross_parent IS 'Cross Parent: Parent germplasm that, when crossed with another parent, can produce a cross/new germplasm [CPARENT]';

COMMENT ON COLUMN germplasm.cross_parent.id IS 'Cross Parent ID: Database identifier of the cross parent [CPARENT_ID]';

COMMENT ON COLUMN germplasm.cross_parent.cross_id IS 'Cross ID: Reference to the cross record [CPARENT_CROSS_ID]';

COMMENT ON COLUMN germplasm.cross_parent.germplasm_id IS 'Germplasm ID: Reference to the germplasm used as a parent in a cross [CPARENT_GERM_ID]';

COMMENT ON COLUMN germplasm.cross_parent.seed_id IS 'Seed ID: Reference to the seed of the germplasm that was used as parent in a cross [CPARENT_SEED_ID]';

COMMENT ON COLUMN germplasm.cross_parent.parent_role IS 'Parent Role: Role of the parent in the cross {female, male} [CPARENT_PARENTROLE]';

COMMENT ON COLUMN germplasm.cross_parent.order_number IS 'Order Number: Ordering of parents in the cross [CPARENT_ORDERNO]';

COMMENT ON COLUMN germplasm.cross_parent.experiment_id IS 'Experiment ID: Reference to the experiment where the parent is added [CPARENT_EXPT_ID]';

COMMENT ON COLUMN germplasm.cross_parent.entry_id IS 'Entry ID: Reference to the entry in the experiment where the parent is represented [CPARENT_ENTRY_ID]';

COMMENT ON COLUMN germplasm.cross_parent.plot_id IS 'Plot ID: Reference to the specific plot in the experiment where the parent will be taken [CPARENT_PLOT_ID]';

CREATE  TABLE germplasm.family ( 
	id                   serial  NOT NULL ,
    cross_id             integer  NOT NULL ,
	parent_germplasm_id  integer  NOT NULL ,
	child_germplasm_id   integer  NOT NULL ,
	source_seed_id       integer  NOT NULL ,
	selection_number     integer  NOT NULL ,
	CONSTRAINT pk_family_id PRIMARY KEY ( id ),
	CONSTRAINT family_parent_germplasm_id_child_germplasm_id_idx UNIQUE ( parent_germplasm_id, child_germplasm_id ) ,
	CONSTRAINT family_parent_germplasm_id_selection_number_idx UNIQUE ( parent_germplasm_id, selection_number ) 
 );

COMMENT ON CONSTRAINT pk_family_id ON germplasm.family IS 'A family is identified by its unique database ID.';

COMMENT ON CONSTRAINT family_parent_germplasm_id_child_germplasm_id_idx ON germplasm.family IS 'A family can be retrieved by its germplasm (child) within another gemrplasm (parent).';

COMMENT ON CONSTRAINT family_parent_germplasm_id_selection_number_idx ON germplasm.family IS 'A family can be retrieved by its order number (child) within a gemrplasm (parent).';

COMMENT ON TABLE germplasm.family IS 'Family: Family consists of the genealogy of germplasm [FAM]';

COMMENT ON COLUMN germplasm.family.id IS 'Family ID: Database identifier of the family record [FAM_ID]';

COMMENT ON COLUMN germplasm.family.cross_id IS 'Cross ID: Reference to the cross where the family record originates [FAM_CRS_ID]';

COMMENT ON COLUMN germplasm.family.parent_germplasm_id IS 'Parent Germplasm ID: Reference to the parent germplasm where this is related/advanced from [FAM_PGERM_ID]';

COMMENT ON COLUMN germplasm.family.child_germplasm_id IS 'Child Germplasm ID: Reference to the germplasm related/advanced from the parent germplasm [FAM_CGERM_ID]';

COMMENT ON COLUMN germplasm.family.source_seed_id IS 'Source Seed ID: Reference to the seed of the germplasm that contains the source information [FAM_SEED_ID]';

COMMENT ON COLUMN germplasm.family.selection_number IS 'Selection Number: Sequence order of the germplasm from the family, if applicable [FAM_SELNO]';

CREATE INDEX family_cross_id_idx ON germplasm.family ( cross_id );

COMMENT ON INDEX germplasm.family_cross_id_idx IS 'A family can be retrieved by its cross.';

CREATE  TABLE germplasm.genetic_population ( 
	id                   serial  NOT NULL ,
	germplasm_id         integer  NOT NULL ,
	seed_id              integer  NOT NULL ,
	CONSTRAINT pk_genetic_population_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_genetic_population_id ON germplasm.genetic_population IS 'Genetic population ID is the primary key';

COMMENT ON TABLE germplasm.genetic_population IS 'Examination and modeling of genetic variation within populations of germplasm\n\nQUESTION/COMMENT: Requesting for sample data for this entity';

COMMENT ON COLUMN germplasm.genetic_population.id IS 'Database identifier of the genetic population';

COMMENT ON COLUMN germplasm.genetic_population.germplasm_id IS 'Reference to the germplasm';

COMMENT ON COLUMN germplasm.genetic_population.seed_id IS 'Reference to the seed';

CREATE  TABLE germplasm.genetic_population_attribute ( 
	id                   serial  NOT NULL ,
	genetic_population_id integer  NOT NULL ,
	variable_id          integer   ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT idx_genetic_population_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT idx_genetic_population_attribute_id ON germplasm.genetic_population_attribute IS 'Genetic population attribute ID is the primary key';

COMMENT ON TABLE germplasm.genetic_population_attribute IS 'Attribute linked to the genetic population';

COMMENT ON COLUMN germplasm.genetic_population_attribute.id IS 'Database identifier of the genetic population attribute';

COMMENT ON COLUMN germplasm.genetic_population_attribute.genetic_population_id IS 'Reference to the genetic population having the attribute';

COMMENT ON COLUMN germplasm.genetic_population_attribute.variable_id IS 'Reference to the variable of the genetic population attribute';

COMMENT ON COLUMN germplasm.genetic_population_attribute.data_value IS 'Value of the genetic population attribute';

COMMENT ON COLUMN germplasm.genetic_population_attribute.data_qc_code IS 'Status of the genetic population attribute  {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)}';

CREATE  TABLE germplasm.package ( 
	id                   serial  NOT NULL ,
	package_code         varchar(64)  NOT NULL ,
	package_label        varchar(256)  NOT NULL ,
	package_quantity     float8  NOT NULL ,
	package_unit         varchar(32)  NOT NULL ,
	package_status       varchar(64)  NOT NULL ,
	seed_id              integer  NOT NULL ,
	program_id           integer   ,
	geospatial_object_id integer   ,
	facility_id          integer   ,
	CONSTRAINT pk_package_id PRIMARY KEY ( id ),
	CONSTRAINT package_package_code_idx UNIQUE ( package_code ) 
 );

COMMENT ON CONSTRAINT pk_package_id ON germplasm.package IS 'A package is identified by its unique database ID.';

COMMENT ON CONSTRAINT package_package_code_idx ON germplasm.package IS 'A package can be retrieved by its unique package code.';

CREATE INDEX package_package_label_idx ON germplasm.package ( package_label );

COMMENT ON INDEX germplasm.package_package_label_idx IS 'A package can be retrieved by its package label.';

COMMENT ON TABLE germplasm.package IS 'Package: Physical bag of seeds [PKG]';

COMMENT ON COLUMN germplasm.package.id IS 'Package ID: Database identifier of the package [PKG_ID]';

COMMENT ON COLUMN germplasm.package.package_code IS 'Package Code: Textual identifier of the package [PKG]';

COMMENT ON COLUMN germplasm.package.package_label IS 'Package Label: Human-readable text printed on the seed package [PKG_LABEL]';

COMMENT ON COLUMN germplasm.package.package_quantity IS 'Package Quantity: Weight or number of seeds in the package [PKG_QTY]';

COMMENT ON COLUMN germplasm.package.package_unit IS 'Package Unit: Unit of quantity of the seed package [PKG_UNIT]';

COMMENT ON COLUMN germplasm.package.package_status IS 'Package Status: Status of the package {list values here} [PKG_STATUS]';

COMMENT ON COLUMN germplasm.package.seed_id IS 'Seed ID: Reference to the seed where the package is directly associated [PKG_SEED_ID]';

COMMENT ON COLUMN germplasm.package.program_id IS 'Program ID: Reference to the program that manages the seed package [PKG_PROG_ID]';

COMMENT ON COLUMN germplasm.package.geospatial_object_id IS 'Geospatial Object ID: Reference to the geospatial object where the package is stored [PKG_GEO_ID]';

COMMENT ON COLUMN germplasm.package.facility_id IS 'Facility ID: Reference to the facility where the package is kept [PKG_FAC_ID]';

CREATE  TABLE germplasm.package_relation ( 
	id                   serial  NOT NULL ,
	source_package_id    integer  NOT NULL ,
	destination_package_id integer  NOT NULL ,
	relation_type        varchar(32)  NOT NULL ,
	CONSTRAINT pk_package_relation_id PRIMARY KEY ( id ),
	CONSTRAINT package_relation_source_package_id_destination_package_id_idx UNIQUE ( source_package_id, destination_package_id ) 
 );

COMMENT ON CONSTRAINT pk_package_relation_id ON germplasm.package_relation IS 'A package relation is identified by its unique database ID.';

COMMENT ON CONSTRAINT package_relation_source_package_id_destination_package_id_idx ON germplasm.package_relation IS 'A package relation can be retrieved by its source package within a destination package (vice-versa).';

CREATE INDEX package_relation_source_package_id_relation_type_idx ON germplasm.package_relation ( source_package_id, relation_type );

COMMENT ON INDEX germplasm.package_relation_source_package_id_relation_type_idx IS 'A package relation can be retrieved by its source package and relation type.';

COMMENT ON TABLE germplasm.package_relation IS 'Package Relation: Association of the package with another package [PKGREL]';

COMMENT ON COLUMN germplasm.package_relation.id IS 'Package Relation ID: Database identifier of the package relation [PKGREL_ID]';

COMMENT ON COLUMN germplasm.package_relation.source_package_id IS 'Source Package ID: Reference to the source package that contributes to a destination package [PKGREL_SPKG_ID]';

COMMENT ON COLUMN germplasm.package_relation.destination_package_id IS 'Destination Package ID: Reference to the destination package of one or more source packages [PKGREL_DPKG_ID]';

COMMENT ON COLUMN germplasm.package_relation.relation_type IS 'Relation Type: Type of relation {combined (one or more source packages to produce one destination package), divided (one source package to produce one or many destination packages), subset (one source package to product one or many destination packages without the need to generate new seeds)} [PKGREL_RELTYPE]';

CREATE  TABLE germplasm.package_trait ( 
	id                   serial  NOT NULL ,
	package_id           integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	data_qc_code         varchar(8)  NOT NULL ,
	CONSTRAINT pk_package_trait_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_package_trait_id ON germplasm.package_trait IS 'A package trait is identified by its unique database ID.';

CREATE INDEX package_trait_package_id_idx ON germplasm.package_trait ( package_id );

COMMENT ON INDEX germplasm.package_trait_package_id_idx IS 'A package trait can be retrieved by its package.';

CREATE INDEX package_trait_package_id_variable_id_idx ON germplasm.package_trait ( package_id, variable_id );

COMMENT ON INDEX germplasm.package_trait_package_id_variable_id_idx IS 'A package trait can be retrieved by its variable within a package.';

CREATE INDEX package_trait_package_id_variable_id_data_value_idx ON germplasm.package_trait ( package_id, variable_id, data_value );

COMMENT ON INDEX germplasm.package_trait_package_id_variable_id_data_value_idx IS 'A package trait can be retrieved by its variable and data value within a package.';

CREATE INDEX package_trait_package_id_data_qc_code_idx ON germplasm.package_trait ( package_id, data_qc_code );

COMMENT ON INDEX germplasm.package_trait_package_id_data_qc_code_idx IS 'A package trait can be retrieved by its data QC code within a package.';

COMMENT ON TABLE germplasm.package_trait IS 'Package Trait: Trait related to the package [PKGTRAIT]';

COMMENT ON COLUMN germplasm.package_trait.id IS 'Package Trait ID: Database identifier of the package trait [PKGTRAIT_ID]';

COMMENT ON COLUMN germplasm.package_trait.package_id IS 'Package ID: Reference to the package where the trait is associated [PKGTRAIT_PKG_ID]';

COMMENT ON COLUMN germplasm.package_trait.variable_id IS 'Variable ID: Reference to the variable of the trait [PKGTRAIT_VAR_ID]';

COMMENT ON COLUMN germplasm.package_trait.data_value IS 'Package Trait Data Value: Value of the package trait [PKGTRAIT_DATAVAL]';

COMMENT ON COLUMN germplasm.package_trait.data_qc_code IS 'Package Trait Data QC Code: Quality of the package trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [PKGTRAIT_QCCODE]';

CREATE  TABLE germplasm.envelope ( 
	id                   serial  NOT NULL ,
	package_id           integer  NOT NULL ,
	envelope_code        integer  NOT NULL ,
	envelope_label       varchar(256)  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_envelope_id PRIMARY KEY ( id ),
	CONSTRAINT envelope_envelope_code_unq UNIQUE ( envelope_code ) ,
	CONSTRAINT envelope_package_id_order_number_idx UNIQUE ( package_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_envelope_id ON germplasm.envelope IS 'An envelope is identified by its unique database ID.';

COMMENT ON CONSTRAINT envelope_envelope_code_unq ON germplasm.envelope IS 'An envelope can be retrieved by its unique envelope code.';

COMMENT ON CONSTRAINT envelope_package_id_order_number_idx ON germplasm.envelope IS 'An envelope can be retrieved by its unique order number within a package.';

CREATE INDEX envelope_envelope_label_idx ON germplasm.envelope ( envelope_label );

COMMENT ON INDEX germplasm.envelope_envelope_label_idx IS 'An envelope can be retrieved by its envelope label.';

COMMENT ON TABLE germplasm.envelope IS 'Envelope: Smaller packets of seeds taken from a source seed package [ENVLP]';

COMMENT ON COLUMN germplasm.envelope.id IS 'Envelope ID: Database identifier of the envelope [ENVLP]';

COMMENT ON COLUMN germplasm.envelope.package_id IS 'Package ID: Reference to the package where the envelope originated [ENVLP_PKG_ID]';

COMMENT ON COLUMN germplasm.envelope.envelope_code IS 'Envelope Code: Textual identifier of the envelope [ENVLP_CODE]';

COMMENT ON COLUMN germplasm.envelope.envelope_label IS 'Envelope Label: Human-readable text printed on the envelope [ENVLP_LABEL]';

COMMENT ON COLUMN germplasm.envelope.order_number IS 'Envelope Order Number: Ordering of the envelope within the package [ENVLP_ORDERNO]';

CREATE  TABLE place.facility ( 
	id                   serial  NOT NULL ,
	facility_code        varchar(64)  NOT NULL ,
	facility_name        varchar(128)  NOT NULL ,
	facility_type        varchar(64)  NOT NULL ,
	facility_subtype     varchar(64)  NOT NULL ,
	geospatial_coordinates polygon   ,
	description          text   ,
	parent_facility_id   integer   ,
	root_facility_id     integer   ,
	CONSTRAINT pk_facility_id PRIMARY KEY ( id ),
	CONSTRAINT facility_facility_code_idx UNIQUE ( facility_code ) 
 );

COMMENT ON CONSTRAINT pk_facility_id ON place.facility IS 'A facility is identified by its unique database ID.';

COMMENT ON CONSTRAINT facility_facility_code_idx ON place.facility IS 'A facility can be retrieved by its unique facility code.';

CREATE INDEX facility_facility_name_idx ON place.facility ( facility_name );

COMMENT ON INDEX place.facility_facility_name_idx IS 'A facility can be retrieved by its facility name.';

CREATE INDEX facility_facility_type_idx ON place.facility ( facility_type );

COMMENT ON INDEX place.facility_facility_type_idx IS 'A facility can be retrieved by its facility type.';

CREATE INDEX facility_facility_subtype_idx ON place.facility ( facility_subtype );

COMMENT ON INDEX place.facility_facility_subtype_idx IS 'A facility can be retrieved by its facility subtype.';

CREATE INDEX facility_parent_facility_id_idx ON place.facility ( parent_facility_id );

COMMENT ON INDEX place.facility_parent_facility_id_idx IS 'A facility can be retrieved by its parent facility.';

CREATE INDEX facility_root_facility_id_idx ON place.facility ( root_facility_id );

COMMENT ON INDEX place.facility_root_facility_id_idx IS 'A facility can be retrieved by its root facility.';

CREATE INDEX facility_geospatial_coordinates_idx ON place.facility USING GIST ( geospatial_coordinates );

COMMENT ON INDEX place.facility_geospatial_coordinates_idx IS 'A facility can be retrieved by its geospatial coordinates.';

COMMENT ON TABLE place.facility IS 'Facility: Place where seed packages are stored [FAC]';

COMMENT ON COLUMN place.facility.id IS 'Facility ID: Database identifier of the facility [FAC_ID]';

COMMENT ON COLUMN place.facility.facility_code IS 'Facility Code: Textual identifier of the facility [FAC_CODE]';

COMMENT ON COLUMN place.facility.facility_name IS 'Facility Name: Full name of the facility [FAC_NAME]';

COMMENT ON COLUMN place.facility.facility_type IS 'Facility Type: Type of the facility {structure, container} [FAC_TYPE]';

COMMENT ON COLUMN place.facility.facility_subtype IS 'Facility Sub-Type: Sub-type of a facility {building, room, shelf, tray, megabin, etc.} [FAC_SUBTYPE]';

COMMENT ON COLUMN place.facility.geospatial_coordinates IS 'Geospatial Object Coordinates: Set of coordinates that determines the location of the geospatial object, which can form a central point (one pair of latitude-longitude) or a shape/polygon (three or more pairs of latitude-longitude) [FAC_COORDS]';

COMMENT ON COLUMN place.facility.description IS 'Facility Description: Additional information about the facility [FAC_DESC]';

COMMENT ON COLUMN place.facility.parent_facility_id IS 'Parent Facility ID: Reference to the parent facility of the facility ()e.g. room is within a building) [FAC_PARENT_ID]';

COMMENT ON COLUMN place.facility.root_facility_id IS 'Root Facility ID: Reference to the root facility of the facility (e.g. a shelf''s root facility is the seed warehouse building) [FAC_ROOT_ID]';

CREATE  TABLE place.geospatial_object ( 
	id                   serial  NOT NULL ,
	geospatial_object_code varchar(64)  NOT NULL ,
	geospatial_object_name varchar(128)  NOT NULL ,
	geospatial_object_type varchar(64)  NOT NULL ,
	geospatial_object_subtype varchar(64)  NOT NULL ,
	geospatial_coordinates polygon   ,
	altitude             float8   ,
	description          text   ,
	parent_geospatial_object_id integer   ,
	root_geospatial_object_id integer   ,
	CONSTRAINT pk_geospatial_object_id PRIMARY KEY ( id ),
	CONSTRAINT geospatial_object_geospatial_object_code_idx UNIQUE ( geospatial_object_code ) 
 );

COMMENT ON CONSTRAINT pk_geospatial_object_id ON place.geospatial_object IS 'A geospatial object is identified by its unique database ID.';

COMMENT ON CONSTRAINT geospatial_object_geospatial_object_code_idx ON place.geospatial_object IS 'A geospatial object can be retrieved by its unique geospatial object code.';

CREATE INDEX geospatial_object_geospatial_object_name_idx ON place.geospatial_object ( geospatial_object_name );

COMMENT ON INDEX place.geospatial_object_geospatial_object_name_idx IS 'A geospatial object can be retrieved by its geospatial object name.';

CREATE INDEX geospatial_object_geospatial_object_type_idx ON place.geospatial_object ( geospatial_object_type );

COMMENT ON INDEX place.geospatial_object_geospatial_object_type_idx IS 'A geospatial object can be retrieved by its geospatial object type.';

CREATE INDEX geospatial_object_geospatial_object_subtype_idx ON place.geospatial_object ( geospatial_object_subtype );

COMMENT ON INDEX place.geospatial_object_geospatial_object_subtype_idx IS 'A geospatial object can be retrieved by its geospatial object subtype.';

CREATE INDEX geospatial_object_parent_geospatial_object_id_idx ON place.geospatial_object ( parent_geospatial_object_id );

COMMENT ON INDEX place.geospatial_object_parent_geospatial_object_id_idx IS 'A geospatial object can be retrieved by its parent geospatial object.';

CREATE INDEX geospatial_object_root_geospatial_object_id_idx ON place.geospatial_object ( root_geospatial_object_id );

COMMENT ON INDEX place.geospatial_object_root_geospatial_object_id_idx IS 'A geospatial object can be retrieved by its root geospatial object.';

CREATE INDEX geospatial_object_geospatial_coordinates_idx ON place.geospatial_object USING GIST ( geospatial_coordinates );

COMMENT ON INDEX place.geospatial_object_geospatial_coordinates_idx IS 'A geospatial object can be retrieved by its geospatial coordinates.';

COMMENT ON TABLE place.geospatial_object IS 'Geospatial Object: Place on Earth which is identified by longitude and latitude coordinates (polygon or center) and sometimes altitude [GEO]';

COMMENT ON COLUMN place.geospatial_object.id IS 'Geospatial Object ID: Database identifier of the geospatial object [GEO_ID]';

COMMENT ON COLUMN place.geospatial_object.geospatial_object_code IS 'Geospatial Object Code: Textual identifier of the geospatial object [GEO_CODE]';

COMMENT ON COLUMN place.geospatial_object.geospatial_object_name IS 'Geospatial Object Name: Full name of the geospatial object [GEO_NAME]';

COMMENT ON COLUMN place.geospatial_object.geospatial_object_type IS 'Geospatial Object Type: Type of geospatial object {site, field, planting_area} [GEO_TYPE]';

COMMENT ON COLUMN place.geospatial_object.geospatial_object_subtype IS 'Geospatial Object Subtype: Subtype of the geospatial object {country, region, province, district, breeding location, international agricultural research center, national agricultural research center, etc.} [GEO_SUBTYPE]';

COMMENT ON COLUMN place.geospatial_object.geospatial_coordinates IS 'Geospatial Object Coordinates: Set of coordinates that determines the location of the geospatial object, which can form a central point (one pair of latitude-longitude) or a shape/polygon (three or more pairs of latitude-longitude) [GEO_COORDS]';

COMMENT ON COLUMN place.geospatial_object.altitude IS 'Geospatial Object Altitude: Distance of the place from the Earth''s sea/ground level [GEO_ALTITUDE]';

COMMENT ON COLUMN place.geospatial_object.description IS 'Geospatial Object Description: Additional information about the geospatial object [GEO_DESC]';

COMMENT ON COLUMN place.geospatial_object.parent_geospatial_object_id IS 'Geospatial Object Parent ID: Reference to the parent geospatial object of the geospatial object, which depends on its type [GEO_PARENT_GEO_ID]';

COMMENT ON COLUMN place.geospatial_object.root_geospatial_object_id IS 'Geospatial Object Root ID: Reference to the root geospatial object of the geospatial object [GEO_ROOT_GEO_ID]';

CREATE  TABLE place.geospatial_object_attribute ( 
	id                   serial  NOT NULL ,
	geospatial_object_id integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           varchar  NOT NULL ,
	CONSTRAINT pk_geospatial_object_attribute_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_geospatial_object_attribute_id ON place.geospatial_object_attribute IS 'A geospatial object attribute is identified by its unique database identifier.';

CREATE INDEX geospatial_object_attribute_geospatial_object_id_variable_id_idx ON place.geospatial_object_attribute ( geospatial_object_id, variable_id );

COMMENT ON INDEX place.geospatial_object_attribute_geospatial_object_id_variable_id_idx IS 'A geospatial object attribute can be retrieved by its variable within a geospatial object.';

CREATE INDEX geospatial_object_attribute_geospatial_object_id_variable_id_data_value_idx ON place.geospatial_object_attribute ( geospatial_object_id, variable_id, data_value );

COMMENT ON INDEX place.geospatial_object_attribute_geospatial_object_id_variable_id_data_value_idx IS 'A geospatial object attribute can be retrieved by its variable and value within a geospatial object.';

COMMENT ON TABLE place.geospatial_object_attribute IS 'Geospatial Object Attribute: Additional information about the geospatial object [GEOATTR]';

COMMENT ON COLUMN place.geospatial_object_attribute.id IS 'Geospatial Object Attribute ID: Database identifier of the geospatial object attribute [GEOATTR_ID]';

COMMENT ON COLUMN place.geospatial_object_attribute.geospatial_object_id IS 'Geospatial Object ID: Reference to the geospatial object having that attribute [GEOATTR_GEO_ID]';

COMMENT ON COLUMN place.geospatial_object_attribute.variable_id IS 'Variable ID: Reference to the variable of the geospatial object attribute [GEOATTR_VAR_ID]';

COMMENT ON COLUMN place.geospatial_object_attribute.data_value IS 'Geospatial Object Attribute Data Value: Value of the geospatial object attribute [GEOATTR_DATAVAL]';

CREATE  TABLE tenant.crop ( 
	id                   serial  NOT NULL ,
	crop_code            varchar(64)  NOT NULL ,
	crop_name            varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_crop_id PRIMARY KEY ( id ),
	CONSTRAINT crop_crop_code_idx UNIQUE ( crop_code ) 
 );

COMMENT ON CONSTRAINT pk_crop_id ON tenant.crop IS 'A crop is identified by its unique database ID.';

COMMENT ON CONSTRAINT crop_crop_code_idx ON tenant.crop IS 'A crop can be retrieved by its unique crop code.';

COMMENT ON TABLE tenant.crop IS 'Crop: Cultivated plant that is grown as food and is the subject of research and development conducted by various programs [CROP]';

COMMENT ON COLUMN tenant.crop.id IS 'Crop ID: Database identifier of the crop [CROP_ID]';

COMMENT ON COLUMN tenant.crop.crop_code IS 'Crop Code: Textual identifier of the crop [CROP_CODE]';

COMMENT ON COLUMN tenant.crop.crop_name IS 'Crop Name: Full name of the crop [CROP_NAME]';

COMMENT ON COLUMN tenant.crop.description IS 'Crop Description: Additional information about the crop [CROP_DESCRIPTION]';

CREATE  TABLE tenant.organization ( 
	id                   serial  NOT NULL ,
	organization_code    varchar(64)  NOT NULL ,
	organization_name    varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_organization_id PRIMARY KEY ( id ),
	CONSTRAINT organization_organization_code_idx UNIQUE ( organization_code ) ,
	CONSTRAINT organization_organization_name_idx UNIQUE ( organization_name ) 
 );

COMMENT ON CONSTRAINT pk_organization_id ON tenant.organization IS 'An organization is identified by its unique database ID.';

COMMENT ON CONSTRAINT organization_organization_code_idx ON tenant.organization IS 'An organization can be retrieved by its unique organization code.';

COMMENT ON CONSTRAINT organization_organization_name_idx ON tenant.organization IS 'An organization can be retrieved by its unique organization name.';

COMMENT ON TABLE tenant.organization IS 'Organization: Institutes, companies, and partners [ORG]';

COMMENT ON COLUMN tenant.organization.id IS 'Organization ID: Database identifier of the organization [ORG_ID]';

COMMENT ON COLUMN tenant.organization.organization_code IS 'Organization Code: Textual identifier of the organization [ORG_CODE]';

COMMENT ON COLUMN tenant.organization.organization_name IS 'Organization Name: Full name of the organization [ORG_NAME]';

COMMENT ON COLUMN tenant.organization.description IS 'Organization Description: Additional information about the organization [ORG_DESC]';

CREATE  TABLE tenant.person_role ( 
	id                   serial  NOT NULL ,
	person_role_code     varchar(64)  NOT NULL ,
	person_role_name     varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT idx_role_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT idx_role_id ON tenant.person_role IS 'A person role is identified by its unique database identifier.';

COMMENT ON TABLE tenant.person_role IS 'Person Role: Set of privileges granted to a person [PERSONROLE]';

COMMENT ON COLUMN tenant.person_role.id IS 'Person Role ID: Database identifier of the person role [PERSONROLE_ID]';

COMMENT ON COLUMN tenant.person_role.person_role_code IS 'Person Role Code: Textual identifier of the role [PERSONROLE_CODE]';

COMMENT ON COLUMN tenant.person_role.person_role_name IS 'Person Role Name: Full name of the role [PERSONROLE_NAME]';

COMMENT ON COLUMN tenant.person_role.description IS 'Person Role Description: Additional information about the role [PERSONROLE_DESC]';

CREATE  TABLE tenant.pipeline ( 
	id                   serial  NOT NULL ,
	pipeline_code        varchar(64)  NOT NULL ,
	pipeline_name        varchar(128)  NOT NULL ,
	pipeline_status      varchar(64)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_pipeline_id PRIMARY KEY ( id ),
	CONSTRAINT pipeline_pipeline_code_idx UNIQUE ( pipeline_code ) 
 );

COMMENT ON CONSTRAINT pk_pipeline_id ON tenant.pipeline IS 'A pipeline is identified by its unique database identifier.';

COMMENT ON CONSTRAINT pipeline_pipeline_code_idx ON tenant.pipeline IS 'A pipeline can be retrieved by its unique pipeline code.';

CREATE INDEX pipeline_pipeline_name_idx ON tenant.pipeline ( pipeline_name );

COMMENT ON INDEX tenant.pipeline_pipeline_name_idx IS 'A pipeline can be retrieved by its pipeline name.';

CREATE INDEX pipeline_pipeline_status_idx ON tenant.pipeline ( pipeline_status );

COMMENT ON INDEX tenant.pipeline_pipeline_status_idx IS 'A pipeline can be retrieved by its pipeline status.';

COMMENT ON TABLE tenant.pipeline IS 'Pipeline: Cross-project management of activities associated with a particular set of objectives [PIPE]';

COMMENT ON COLUMN tenant.pipeline.id IS 'Pipeline ID: Database identifier of the pipeline [PIPE_ID]';

COMMENT ON COLUMN tenant.pipeline.pipeline_code IS 'Pipeline Code: Textual identifier of the pipeline [PIPE_CODE]';

COMMENT ON COLUMN tenant.pipeline.pipeline_name IS 'Pipeline Name: Full name of the pipeline [PIPE_NAME]';

COMMENT ON COLUMN tenant.pipeline.pipeline_status IS 'Pipeline Status: Status of the pipeline {draft, active, archived} [PIPE_STATUS]';

COMMENT ON COLUMN tenant.pipeline.description IS 'Pipeline Description: Additional information about the pipeline [PIPE_DESC]';

CREATE  TABLE tenant.scheme ( 
	id                   serial  NOT NULL ,
	scheme_code          varchar(64)  NOT NULL ,
	scheme_name          varchar(128)  NOT NULL ,
	pipeline_id          integer  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_schema_id PRIMARY KEY ( id ),
	CONSTRAINT scheme_pipeline_id_scheme_code_unq UNIQUE ( pipeline_id, scheme_code ) 
 );

COMMENT ON CONSTRAINT pk_schema_id ON tenant.scheme IS 'A scheme is identified by its unique database ID.';

COMMENT ON CONSTRAINT scheme_pipeline_id_scheme_code_unq ON tenant.scheme IS 'A scheme can be retrieved by its unique scheme code within a pipeline.';

CREATE INDEX scheme_pipeline_id_scheme_name_idx ON tenant.scheme ( pipeline_id, scheme_name );

COMMENT ON INDEX tenant.scheme_pipeline_id_scheme_name_idx IS 'A scheme can be retrieved by its scheme name within a pipeline.';

COMMENT ON TABLE tenant.scheme IS 'Scheme: Ordered list of phases and stages that set the direction of the project [SCHEME]';

COMMENT ON COLUMN tenant.scheme.id IS 'Scheme ID: Database identifier of the scheme [SCHEME_ID]';

COMMENT ON COLUMN tenant.scheme.scheme_code IS 'Scheme Code: Textual identifier of the scheme [SCHEME_CODE]';

COMMENT ON COLUMN tenant.scheme.scheme_name IS 'Scheme Name: Full name of the scheme [SCHEME_NAME]';

COMMENT ON COLUMN tenant.scheme.pipeline_id IS 'Pipeline ID: Reference to the pipeline where the scheme belongs [SCHEME_PIPE_ID]';

COMMENT ON COLUMN tenant.scheme.description IS 'Scheme Description: Additional information about the scheme [SCHEME_DESC]';

CREATE  TABLE tenant.season ( 
	id                   serial  NOT NULL ,
	season_code          varchar(64)  NOT NULL ,
	season_name          varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_season_id PRIMARY KEY ( id ),
	CONSTRAINT idx_season_code UNIQUE ( season_code ) 
 );

COMMENT ON CONSTRAINT pk_season_id ON tenant.season IS 'Season ID is the primary key';

COMMENT ON CONSTRAINT idx_season_code ON tenant.season IS 'Season code is easily searchable and unique';

CREATE INDEX idx_season_name ON tenant.season ( season_name );

COMMENT ON INDEX tenant.idx_season_name IS 'Season name is easily searchable';

COMMENT ON TABLE tenant.season IS 'Season: Season or cycle at which the experiment is being evaluated, which depends on the location [SEASON]';

COMMENT ON COLUMN tenant.season.id IS 'Season ID: Database identifier of the season [SEASON_ID]';

COMMENT ON COLUMN tenant.season.season_code IS 'Season Code: Textual identifier of the season [SEASON_CODE]';

COMMENT ON COLUMN tenant.season.season_name IS 'Season Name: Full name of the season [SEASON_NAME]';

COMMENT ON COLUMN tenant.season.description IS 'Season Description: Additional information about the season [SEASON_DESC]';

CREATE  TABLE tenant.stage ( 
	id                   serial  NOT NULL ,
	stage_code           varchar(64)  NOT NULL ,
	stage_name           varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT stage_stage_code_idx UNIQUE ( stage_code ) ,
	CONSTRAINT pk_stage_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT stage_stage_code_idx ON tenant.stage IS 'A stage can be retrieved by its unique stage code.';

COMMENT ON CONSTRAINT pk_stage_id ON tenant.stage IS 'A stage is identified by its unique database ID.';

CREATE INDEX stage_stage_name_idx ON tenant.stage ( stage_name );

COMMENT ON INDEX tenant.stage_stage_name_idx IS 'A stage can be retrieved by its stage name.';

COMMENT ON TABLE tenant.stage IS 'Stage: Step or building block in the scheme [STAGE]';

COMMENT ON COLUMN tenant.stage.id IS 'Stage ID: Database identifier of the stage [STAGE_ID]';

COMMENT ON COLUMN tenant.stage.stage_code IS 'Stage Code: Textual identifier of the stage [STAGE_CODE]';

COMMENT ON COLUMN tenant.stage.stage_name IS 'Stage Name: Full name of the stage [STAGE_NAME]';

COMMENT ON COLUMN tenant.stage.description IS 'Stage Description: Additional information about the stage [STAGE_DESC]';

CREATE  TABLE tenant.team ( 
	id                   serial  NOT NULL ,
	team_code            varchar(64)  NOT NULL ,
	team_name            varchar(128)  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_team_id PRIMARY KEY ( id ),
	CONSTRAINT team_team_code_idx UNIQUE ( team_code ) 
 );

COMMENT ON CONSTRAINT pk_team_id ON tenant.team IS 'A team is identified by its unique database ID.';

COMMENT ON CONSTRAINT team_team_code_idx ON tenant.team IS 'A team can be retrieved by its unique team code.';

CREATE INDEX team_team_name_idx ON tenant.team ( team_name );

COMMENT ON INDEX tenant.team_team_name_idx IS 'A team can be retrieved by its team name.';

COMMENT ON TABLE tenant.team IS 'Team: Group of persons within the program [TEAM]';

COMMENT ON COLUMN tenant.team.id IS 'Team: Database identifier of the program [TEAM_ID]';

COMMENT ON COLUMN tenant.team.team_code IS 'Team Code: Textual identifier of the team [TEAM_CODE]';

COMMENT ON COLUMN tenant.team.team_name IS 'Team Name: Full name of the team [TEAM_NAME]';

COMMENT ON COLUMN tenant.team.description IS 'Team Description: Additional information about the team [TEAM_DESC]';

CREATE  TABLE tenant.breeding_zone ( 
	id                   serial  NOT NULL ,
	pipeline_id          integer  NOT NULL ,
	geospatial_object_id integer  NOT NULL ,
	CONSTRAINT pk_breeding_zone_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE tenant.breeding_zone IS 'Breeding Zone: Places where the pipeline will conduct their experiments [BREEDZONE]';

COMMENT ON COLUMN tenant.breeding_zone.id IS 'Breeding Zone ID: Database identifier of the breeding zone [BREEDZONE_ID]';

COMMENT ON COLUMN tenant.breeding_zone.pipeline_id IS 'Pipeline ID: Reference to the pipeline to refer the places [BREEDZONE_PIPE_ID]';

COMMENT ON COLUMN tenant.breeding_zone.geospatial_object_id IS 'Geospatial Object ID: Reference to the geospatial object, which are the breeeding zones of the pipeline [BREEDZONE_GEO_ID]';

CREATE  TABLE tenant.crop_program ( 
	id                   serial  NOT NULL ,
	crop_program_code    varchar(64)  NOT NULL ,
	crop_program_name    varchar(128)  NOT NULL ,
	description          text   ,
	organization_id      integer  NOT NULL ,
	crop_id              integer  NOT NULL ,
	CONSTRAINT pk_crop_program_id PRIMARY KEY ( id ),
	CONSTRAINT crop_program_crop_program_code_idx UNIQUE ( crop_program_code ) 
 );

COMMENT ON CONSTRAINT pk_crop_program_id ON tenant.crop_program IS 'A crop program is identified by its unique database ID.';

COMMENT ON CONSTRAINT crop_program_crop_program_code_idx ON tenant.crop_program IS 'A crop program can be retrieved by its unique crop program code.';

COMMENT ON TABLE tenant.crop_program IS 'Crop Program: Crop-specific objectives of a program [CROPPROG]';

COMMENT ON COLUMN tenant.crop_program.id IS 'Crop Program ID: Database identifier of the crop program [CROPPROG_ID]';

COMMENT ON COLUMN tenant.crop_program.crop_program_code IS 'Crop Program Code: Textual identifier of the crop program [CROPPROG_CODE]';

COMMENT ON COLUMN tenant.crop_program.crop_program_name IS 'Crop Program Name: Full name of the crop program [CROPPROG_NAME]';

COMMENT ON COLUMN tenant.crop_program.description IS 'Crop Program Description: Additional information about the crop program [CROPPROG_DESC]';

COMMENT ON COLUMN tenant.crop_program.organization_id IS 'Organization ID: Reference to the organization where the crop program is held [CROPPROG_ORG_ID]';

COMMENT ON COLUMN tenant.crop_program.crop_id IS 'Crop ID: Reference to the crop under research by the crop program [CROPPROG_CROP_ID]';

CREATE  TABLE tenant.crop_program_team ( 
	id                   serial  NOT NULL ,
	crop_program_id      integer  NOT NULL ,
	team_id              integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_crop_program_team_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_crop_program_team_id ON tenant.crop_program_team IS 'A crop program team is identified by its unique database ID.';

CREATE INDEX crop_program_team_crop_program_id_team_id_idx ON tenant.crop_program_team ( crop_program_id, team_id );

COMMENT ON INDEX tenant.crop_program_team_crop_program_id_team_id_idx IS 'A crop program team can be retrieved by its team within a program.';

CREATE INDEX crop_program_team_crop_program_id_order_number_idx ON tenant.crop_program_team ( crop_program_id, order_number );

COMMENT ON INDEX tenant.crop_program_team_crop_program_id_order_number_idx IS 'A crop program team can be retrieved by its order number within a crop program.';

COMMENT ON TABLE tenant.crop_program_team IS 'Crop Program Team: Teams belonging to one or more crop programs [CPTEAM]';

COMMENT ON COLUMN tenant.crop_program_team.id IS 'Crop Program Team ID: Database identifier of the program team [CPTEAM_ID]';

COMMENT ON COLUMN tenant.crop_program_team.crop_program_id IS 'Crop Program ID: Reference to the crop program where the team is associated [CPTEAM_CROPPROG_ID]';

COMMENT ON COLUMN tenant.crop_program_team.team_id IS 'Team ID: 	Reference to the team that is associated with the program [CPTEAM_TEAM_ID]';

COMMENT ON COLUMN tenant.crop_program_team.order_number IS 'Crop Program Team Order Number: Sequential order of the team within the program [CPTEAM_ORDERNO]';

CREATE  TABLE tenant.person ( 
	id                   serial  NOT NULL ,
	username             varchar(128)  NOT NULL ,
	first_name           varchar(128)  NOT NULL ,
	last_name            varchar(128)  NOT NULL ,
	person_name          varchar(256)  NOT NULL ,
	person_type          varchar(16)  NOT NULL ,
	email                varchar(128)   ,
	person_role_id       integer  NOT NULL ,
	person_status        varchar(64)  NOT NULL ,
	CONSTRAINT pk_person_id PRIMARY KEY ( id ),
	CONSTRAINT idx_person_email UNIQUE ( email ) ,
	CONSTRAINT idx_person_username UNIQUE ( username ) 
 );

COMMENT ON CONSTRAINT pk_person_id ON tenant.person IS 'Person ID is the primary key';

CREATE INDEX idx_person_first_name ON tenant.person ( first_name );

COMMENT ON INDEX tenant.idx_person_first_name IS 'Person first name is easily searchable';

CREATE INDEX idx_person_last_name ON tenant.person ( last_name );

COMMENT ON INDEX tenant.idx_person_last_name IS 'Person last name is easily searchable';

COMMENT ON CONSTRAINT idx_person_email ON tenant.person IS 'Email is easily searchable and unique';

CREATE INDEX idx_person_status ON tenant.person ( person_status );

COMMENT ON INDEX tenant.idx_person_status IS 'Person status is easily searchable';

COMMENT ON CONSTRAINT idx_person_username ON tenant.person IS 'Person username is easily searchable and unique';

COMMENT ON TABLE tenant.person IS 'Person: Individual who helps in the conduct of experiments and execution of the objectives of the programs, and may have access to log in to the system [PERSON]';

COMMENT ON COLUMN tenant.person.id IS 'Person ID: Database identifier of the person [PERSON_ID]';

COMMENT ON COLUMN tenant.person.username IS 'Username: Short textual identifier of the person [PERSON_USERNAME]';

COMMENT ON COLUMN tenant.person.first_name IS 'Person First Name: First/given name of the person [PERSON_FNAME]';

COMMENT ON COLUMN tenant.person.last_name IS 'Person Last Name: Last/family name of the person  [PERSON_LNAME]';

COMMENT ON COLUMN tenant.person.person_name IS 'Person Name: Full name of the person [PERSON_NAME]';

COMMENT ON COLUMN tenant.person.person_type IS 'Person Type: Type of person {admin, user, person} [PERSON_TYPE]';

COMMENT ON COLUMN tenant.person.email IS 'Email: Email address of the person, if available [PERSON_EMAIL]';

COMMENT ON COLUMN tenant.person.person_role_id IS 'Person Role ID: Reference to the role that defines the privileges of the person in the system [PERSON_PROLE_ID]';

COMMENT ON COLUMN tenant.person.person_status IS 'Person Status: Status of the person {active, inactive} [PERSON_STATUS]';

CREATE  TABLE tenant.phase ( 
	id                   serial  NOT NULL ,
	phase_code           varchar(64)  NOT NULL ,
	phase_name           varchar(128)  NOT NULL ,
	description          text   ,
	pipeline_id          integer  NOT NULL ,
	CONSTRAINT pk_phase_id PRIMARY KEY ( id ),
	CONSTRAINT phase_pipeline_id_phase_code_unq UNIQUE ( pipeline_id, phase_code ) 
 );

COMMENT ON CONSTRAINT pk_phase_id ON tenant.phase IS 'A phase is identified by its unique database ID.';

COMMENT ON CONSTRAINT phase_pipeline_id_phase_code_unq ON tenant.phase IS 'A phase can be retrieved by its phase code within a pipeline.';

CREATE INDEX phase_pipeline_id_phase_name_idx ON tenant.phase ( pipeline_id, phase_name );

COMMENT ON INDEX tenant.phase_pipeline_id_phase_name_idx IS 'A phase can be retrieved by its phase name within a pipeline.';

COMMENT ON TABLE tenant.phase IS 'Phase: Phase is a collection of schemes in a pipeline [PHASE]';

COMMENT ON COLUMN tenant.phase.id IS 'Phase ID: Database identifier of the phase [PHASE_ID]';

COMMENT ON COLUMN tenant.phase.phase_code IS 'Phase Code: Textual identifier of the phase [PHASE_CODE]';

COMMENT ON COLUMN tenant.phase.phase_name IS 'Phase Name: Full name of the phase [PHASE_NAME]';

COMMENT ON COLUMN tenant.phase.description IS 'Phase Description: Additional information about the phase [PHASE_DESC]';

COMMENT ON COLUMN tenant.phase.pipeline_id IS 'Pipeline ID: Reference to the pipeline where the phase belongs';

CREATE  TABLE tenant.phase_scheme ( 
	id                   serial  NOT NULL ,
	phase_id             integer  NOT NULL ,
	scheme_id            integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_phase_schema_id PRIMARY KEY ( id ),
	CONSTRAINT phase_scheme_phase_id_scheme_id_unq UNIQUE ( phase_id, scheme_id ) ,
	CONSTRAINT phase_scheme_phase_id_order_number_idx UNIQUE ( phase_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_phase_schema_id ON tenant.phase_scheme IS 'A phase scheme is identified by its unique database ID.';

COMMENT ON CONSTRAINT phase_scheme_phase_id_scheme_id_unq ON tenant.phase_scheme IS 'A phase scheme can be retrieved by its unique scheme within a phase.';

COMMENT ON CONSTRAINT phase_scheme_phase_id_order_number_idx ON tenant.phase_scheme IS 'A phase scheme can be retrieved by its unique order number within a phase.';

COMMENT ON TABLE tenant.phase_scheme IS 'Phase Scheme: Scheme that may be part of one or more phases in the pipeline';

COMMENT ON COLUMN tenant.phase_scheme.id IS 'Phase Scheme ID: Database identifier of the relationship of phase and scheme [PHSSCH_ID]';

COMMENT ON COLUMN tenant.phase_scheme.phase_id IS 'Phase ID: Reference to the phase where the scheme is a part [PHSSCH_PHASE_ID]';

COMMENT ON COLUMN tenant.phase_scheme.scheme_id IS 'Scheme ID: Reference to the scheme that belongs to a phase [PHSSCH_SCHEME_ID]';

COMMENT ON COLUMN tenant.phase_scheme.order_number IS 'Phase Scheme Order Number: Ordering of the scheme in the phase [PHSSCH_ORDERNO]';

CREATE  TABLE tenant.program ( 
	id                   serial  NOT NULL ,
	program_code         varchar(64)  NOT NULL ,
	program_name         varchar(128)  NOT NULL ,
	program_type         varchar(64)  NOT NULL ,
	program_status       varchar(64)  NOT NULL ,
	description          text   ,
	crop_program_id      integer  NOT NULL ,
	CONSTRAINT pk_program_id PRIMARY KEY ( id ),
	CONSTRAINT program_program_code_idx UNIQUE ( program_code ) 
 );

COMMENT ON CONSTRAINT pk_program_id ON tenant.program IS 'A program is identified by its unique database ID.';

COMMENT ON CONSTRAINT program_program_code_idx ON tenant.program IS 'A program can be retrieved by its unique program code.';

COMMENT ON TABLE tenant.program IS 'Program: Entity that conducts defined objectives for the production of the next generation of crops with desired characteristics [PROG]';

COMMENT ON COLUMN tenant.program.id IS 'Program ID: Database identifier of the program [PROG_ID]';

COMMENT ON COLUMN tenant.program.program_code IS 'Program Code: Textual identifier of the program [PROG_CODE]';

COMMENT ON COLUMN tenant.program.program_name IS 'Program Name: Full name of the program [PROG_NAME]';

COMMENT ON COLUMN tenant.program.program_type IS 'Program Type: Type of the program {breeding, molecular, seed health, etc.} [PROG_TYPE]';

COMMENT ON COLUMN tenant.program.program_status IS 'Program Status: Status of the program [PROG_STATUS]';

COMMENT ON COLUMN tenant.program.description IS 'Program Description: Additional information abou the program [PROG_DESC]';

COMMENT ON COLUMN tenant.program.crop_program_id IS 'Crop Program ID: Reference to the crop program where the program belongs  [PROG_CROPPROG_ID]';

CREATE  TABLE tenant.program_config ( 
	id                   serial  NOT NULL ,
	program_id           integer  NOT NULL ,
	config_id            integer  NOT NULL ,
	CONSTRAINT pk_program_config_id PRIMARY KEY ( id )
 );

COMMENT ON TABLE tenant.program_config IS 'Program Config: Configuration in use by a program [PROGCONF]';

COMMENT ON COLUMN tenant.program_config.id IS 'Program Config ID: Database identifier of the program config [PROGCONF_ID]';

COMMENT ON COLUMN tenant.program_config.program_id IS 'Program ID: Reference to the program that uses the config [PROGCONF_PROG_ID]';

COMMENT ON COLUMN tenant.program_config.config_id IS 'Config ID: Reference to the configuration that is used by the program [PROGCONF_CONF_ID]';

CREATE  TABLE tenant.program_team ( 
	id                   serial  NOT NULL ,
	program_id           integer  NOT NULL ,
	team_id              integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_program_team_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_program_team_id ON tenant.program_team IS 'A program team is identified by its unique database ID.';

COMMENT ON TABLE tenant.program_team IS 'Program Team: Teams belonging to one or more programs [PROGTEAM]';

COMMENT ON COLUMN tenant.program_team.id IS 'Program Team ID: Database identifier of the program team [PROGTEAM_ID]';

COMMENT ON COLUMN tenant.program_team.program_id IS 'Program ID: Reference to the program where the team is associated [PROGTEAM_PROG_ID]';

COMMENT ON COLUMN tenant.program_team.team_id IS 'Team ID: Reference to the team that is associated with the program [PROGTEAM_TEAM_ID]';

COMMENT ON COLUMN tenant.program_team.order_number IS 'Program Team Order Number: Sequential order of the team within the program [PROGTEAM_ORDERNO]';

CREATE  TABLE tenant.project ( 
	id                   serial  NOT NULL ,
	project_code         varchar(64)  NOT NULL ,
	project_name         varchar(128)  NOT NULL ,
	project_status       varchar(64)  NOT NULL ,
	description          text   ,
	program_id           integer  NOT NULL ,
	pipeline_id          integer  NOT NULL ,
	leader_id            integer  NOT NULL ,
	CONSTRAINT pk_project_id PRIMARY KEY ( id ),
	CONSTRAINT project_project_code_idx UNIQUE ( project_code ) 
 );

COMMENT ON CONSTRAINT pk_project_id ON tenant.project IS 'A project is identified by its unique database ID.';

COMMENT ON CONSTRAINT project_project_code_idx ON tenant.project IS 'A project can be retrieved by its unique project code.';

CREATE INDEX project_project_name_idx ON tenant.project ( project_name );

COMMENT ON INDEX tenant.project_project_name_idx IS 'A project can be retrieved by its project name.';

CREATE INDEX project_project_status_idx ON tenant.project ( project_status );

COMMENT ON INDEX tenant.project_project_status_idx IS 'A project can be retrieved by its project status.';

CREATE INDEX project_program_id_idx ON tenant.project ( program_id );

COMMENT ON INDEX tenant.project_program_id_idx IS 'A project can be retrieved by its program.';

COMMENT ON TABLE tenant.project IS 'Project: Collaborative enterprises that are planned and designed to achieve a set of goals [PROJ]';

COMMENT ON COLUMN tenant.project.id IS 'Project ID: Database identifier of the project [PROJ_ID]';

COMMENT ON COLUMN tenant.project.project_code IS 'Project Code: Textual identifier of the project [PROJ_CODE]';

COMMENT ON COLUMN tenant.project.project_name IS 'Project Name: Full name of the project [PROJ_NAME]';

COMMENT ON COLUMN tenant.project.project_status IS 'Project Status: Status of the project {draft, active, archived} [PROJ_STATUS]';

COMMENT ON COLUMN tenant.project.description IS 'Project Description: Additional information about the project [PROJ_DESC]';

COMMENT ON COLUMN tenant.project.program_id IS 'Program ID: Reference to the program where the project is created [PROJ_PROG_ID]';

COMMENT ON COLUMN tenant.project.pipeline_id IS 'Pipeline ID: Reference to the pipeline that will be used by the project [PROJ_PIPE_ID]';

COMMENT ON COLUMN tenant.project.leader_id IS 'Leader ID: Reference to the leader (person) who manages the project [PROJ_PERSON_ID]';

CREATE  TABLE tenant.protocol ( 
	id                   serial  NOT NULL ,
	protocol_code        varchar(64)  NOT NULL ,
	protocol_name        varchar(128)  NOT NULL ,
	protocol_type        varchar(64)  NOT NULL ,
	description          text   ,
	program_id           integer  NOT NULL ,
	CONSTRAINT pk_protocol_id PRIMARY KEY ( id ),
	CONSTRAINT protocol_protocol_code_unq UNIQUE ( protocol_code ) 
 );

COMMENT ON CONSTRAINT pk_protocol_id ON tenant.protocol IS 'A protocol is identified by its unique database ID.';

COMMENT ON CONSTRAINT protocol_protocol_code_unq ON tenant.protocol IS 'A protocol can be retrieved by its unique protocol code.';

COMMENT ON TABLE tenant.protocol IS 'Protocol: Set of instructions necessary to carry out specific functions in an experiment [PROT]';

COMMENT ON COLUMN tenant.protocol.id IS 'Protocol ID: Database identifier of the protocol [PROT_ID]';

COMMENT ON COLUMN tenant.protocol.protocol_code IS 'Protocol Code: Textual identifier of the protocol [PROT_CODE]';

COMMENT ON COLUMN tenant.protocol.protocol_name IS 'Protocol Name: Full name of the protocol [PROT_NAME]';

COMMENT ON COLUMN tenant.protocol.protocol_type IS 'Protocol type: Type of the protocol {planting, trait, sampling, genotyping, ...} [PROT_TYPE]';

COMMENT ON COLUMN tenant.protocol.description IS 'Protocol Description: Additional information about the protocol [PROT_DESC]';

COMMENT ON COLUMN tenant.protocol.program_id IS 'Program ID: Reference to the program that created the protocol';

CREATE  TABLE tenant.protocol_data ( 
	id                   serial  NOT NULL ,
	protocol_id          integer  NOT NULL ,
	variable_id          integer  NOT NULL ,
	data_value           integer  NOT NULL ,
	CONSTRAINT pk_protocol_data_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_protocol_data_id ON tenant.protocol_data IS 'A protocol data is identified by its unique database ID.';

CREATE INDEX protocol_data_protocol_id_variable_id_idx ON tenant.protocol_data ( protocol_id, variable_id );

COMMENT ON INDEX tenant.protocol_data_protocol_id_variable_id_idx IS 'A protocol data can be retrieved by its variable within a protocol.';

CREATE INDEX protocol_data_protocol_id_variable_id_data_value_idx ON tenant.protocol_data ( protocol_id, variable_id, data_value );

COMMENT ON INDEX tenant.protocol_data_protocol_id_variable_id_data_value_idx IS 'A protocol data can be retrieved by its variable and value within a protocol.';

COMMENT ON TABLE tenant.protocol_data IS 'Protocol Data: Data associated with a protocol [PROTDATA]';

COMMENT ON COLUMN tenant.protocol_data.id IS 'Protocol Data ID: Database identifier of the protocol data [PROTDATA_ID]';

COMMENT ON COLUMN tenant.protocol_data.protocol_id IS 'Protocol ID: Reference to the protocol having the attribute [PROTDATA_PROT_ID]';

COMMENT ON COLUMN tenant.protocol_data.variable_id IS 'Variable ID: Reference to the variable of the protocol data [PROTDATA_VAR_ID]';

COMMENT ON COLUMN tenant.protocol_data.data_value IS 'Protocol Data Value: Value of the protocol data [PROTDATA_DATAVAL]';

CREATE  TABLE tenant.scheme_stage ( 
	id                   serial  NOT NULL ,
	scheme_id            integer  NOT NULL ,
	stage_id             integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_schema_stage_relation_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_schema_stage_relation_id ON tenant.scheme_stage IS 'Scheme stage relation ID is the primary key';

COMMENT ON TABLE tenant.scheme_stage IS 'Scheme Stage: Collection of stages belonging to one or more schemes [SCHSTGREL]';

COMMENT ON COLUMN tenant.scheme_stage.id IS 'Scheme Stage ID: Database identifier of the schema stage relation [SCHSTGREL_ID]';

COMMENT ON COLUMN tenant.scheme_stage.scheme_id IS 'Scheme ID: Reference to the scheme that contains the stage [SCHSTG_SCHEME_ID]';

COMMENT ON COLUMN tenant.scheme_stage.stage_id IS 'Stage ID: Reference to the stage that belongs to the scheme [SCHSTG_STAGE_ID]';

COMMENT ON COLUMN tenant.scheme_stage.order_number IS 'Scheme Stage Order Number: Ordering of the stage within the scheme [SCHSTG_ORDERNO]';

CREATE  TABLE tenant.service_provider ( 
	id                   serial  NOT NULL ,
	service_provider_code varchar(64)  NOT NULL ,
	service_provider_name varchar(128)  NOT NULL ,
	description          text   ,
	crop_program_id      integer  NOT NULL ,
	organization_id      integer  NOT NULL ,
	CONSTRAINT pk_service_provider_id PRIMARY KEY ( id ),
	CONSTRAINT service_provider_service_provider_code_idx UNIQUE ( service_provider_code ) 
 );

COMMENT ON CONSTRAINT pk_service_provider_id ON tenant.service_provider IS 'A service provider is identified by its unique database ID.';

COMMENT ON CONSTRAINT service_provider_service_provider_code_idx ON tenant.service_provider IS 'A service provider can be retrieved by its unique service provider code.';

CREATE INDEX service_provider_service_provider_name_idx ON tenant.service_provider ( service_provider_name );

COMMENT ON INDEX tenant.service_provider_service_provider_name_idx IS 'A service provider can be retrieved by its service provider name.';

CREATE INDEX service_provider_crop_program_id_idx ON tenant.service_provider ( crop_program_id );

COMMENT ON INDEX tenant.service_provider_crop_program_id_idx IS 'A service provider can be retrieved by its crop program.';

CREATE INDEX service_provider_organization_id_idx ON tenant.service_provider ( organization_id );

COMMENT ON INDEX tenant.service_provider_organization_id_idx IS 'A service provider can be retrieved by its organization.';

COMMENT ON TABLE tenant.service_provider IS 'Service Provider: Entity that executes a service based on the request of its customers [SERVPROV]';

COMMENT ON COLUMN tenant.service_provider.id IS 'Service Provider ID: Database identifier of the service provider [SERVPROV_ID]';

COMMENT ON COLUMN tenant.service_provider.service_provider_code IS 'Service Provider Code: Textual identifier of the service provider [SERVPROV_CODE]';

COMMENT ON COLUMN tenant.service_provider.service_provider_name IS 'Service Provider Name: Full name of the service provider [SERVPROV_NAME]';

COMMENT ON COLUMN tenant.service_provider.description IS 'Service Provider Description: Additional information about the service provider [SERVPROV_DESC]';

COMMENT ON COLUMN tenant.service_provider.crop_program_id IS 'Crop Program ID: Reference to the crop program where the service provider belongs [SERVPROV_CROPPROG_ID]';

COMMENT ON COLUMN tenant.service_provider.organization_id IS 'Organization ID: Reference to the organization where the service provider belongs [SERVPROV_ORG_ID]';

CREATE  TABLE tenant.service_provider_team ( 
	id                   serial  NOT NULL ,
	service_provider_id  integer  NOT NULL ,
	team_id              integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_service_team_id PRIMARY KEY ( id )
 );

COMMENT ON CONSTRAINT pk_service_team_id ON tenant.service_provider_team IS 'A service provider team is identified by its unique database ID.';

COMMENT ON TABLE tenant.service_provider_team IS 'Service Provider Team: Team or set of teams who execute the activities of the service provider [SPTEAM]';

COMMENT ON COLUMN tenant.service_provider_team.id IS 'Service Provider Team ID: Database identifier of the service team [SPTEAM_ID]';

COMMENT ON COLUMN tenant.service_provider_team.service_provider_id IS 'Service Provider ID: Reference to the service provider that the team is associated [SPTEAM_SERVPROV_ID]';

COMMENT ON COLUMN tenant.service_provider_team.team_id IS 'Team ID: 	Reference to the team that executes the service [SPTEAM_TEAM_ID]';

COMMENT ON COLUMN tenant.service_provider_team.order_number IS 'Service Provider Team Order Number: Order of the team in the service [SPTEAM_ORDERNO]';

CREATE  TABLE tenant.team_member ( 
	id                   serial  NOT NULL ,
	team_id              integer  NOT NULL ,
	person_id            integer  NOT NULL ,
	person_role_id       integer  NOT NULL ,
	order_number         integer  NOT NULL ,
	CONSTRAINT pk_team_member_id PRIMARY KEY ( id ),
	CONSTRAINT team_member_team_id_person_id_idx UNIQUE ( team_id, person_id ) ,
	CONSTRAINT team_member_team_id_order_number_idx UNIQUE ( team_id, order_number ) 
 );

COMMENT ON CONSTRAINT pk_team_member_id ON tenant.team_member IS 'A team member is identified by its unique database ID.';

COMMENT ON CONSTRAINT team_member_team_id_person_id_idx ON tenant.team_member IS 'A team member can be retrieved by its unique person within a team.';

COMMENT ON CONSTRAINT team_member_team_id_order_number_idx ON tenant.team_member IS 'A team member can be retrieved by its unique order number within a team.';

COMMENT ON TABLE tenant.team_member IS 'Team Member: Membership of a person to one or more teams [TEAMMEM]';

COMMENT ON COLUMN tenant.team_member.id IS 'Team Member ID: Database identifier of the team member [TEAMMEM_ID]';

COMMENT ON COLUMN tenant.team_member.team_id IS 'Team ID: Reference to the team where the person is a member [TEAMMEM_TEAM_ID]';

COMMENT ON COLUMN tenant.team_member.person_id IS 'Person ID: Reference to the person belonging to the team [TEAMMEM_PERSON_ID]';

COMMENT ON COLUMN tenant.team_member.person_role_id IS 'Person Role ID: Reference to the role of the person in the team [TEAMMEM_PERSONROLE_ID]';

COMMENT ON COLUMN tenant.team_member.order_number IS 'Team Member Order Number: Sequential order of the person within the team [TEAMMEM_ORDERNO]';

CREATE  TABLE tenant.experiment_plan ( 
	id                   serial  NOT NULL ,
	code                 varchar(64)  NOT NULL ,
	name                 varchar(128)  NOT NULL ,
	description          text   ,
	status               varchar(64)  NOT NULL ,
	"year"               integer  NOT NULL ,
	project_id           integer  NOT NULL ,
	CONSTRAINT pk_experiment_plan_id PRIMARY KEY ( id ),
	CONSTRAINT idx_experiment_plan_code UNIQUE ( code ) 
 );

COMMENT ON CONSTRAINT pk_experiment_plan_id ON tenant.experiment_plan IS 'Experiment plan ID is the primary key';

COMMENT ON CONSTRAINT idx_experiment_plan_code ON tenant.experiment_plan IS 'Experiment plan code is easily searchable and unique';

COMMENT ON TABLE tenant.experiment_plan IS 'Planned experiments within a particular cycle';

COMMENT ON COLUMN tenant.experiment_plan.id IS 'Database identifier of the experiment plan';

COMMENT ON COLUMN tenant.experiment_plan.code IS 'Textual identifier of the experiment plan';

COMMENT ON COLUMN tenant.experiment_plan.name IS 'Full name of the experiment plan';

COMMENT ON COLUMN tenant.experiment_plan.description IS 'Additional information about the experiment plan';

COMMENT ON COLUMN tenant.experiment_plan.status IS 'Status of the experiment plan: active, inactive, archived';

COMMENT ON COLUMN tenant.experiment_plan."year" IS 'Year when the experiments in the experiment plan will be conducted';

COMMENT ON COLUMN tenant.experiment_plan.project_id IS 'Reference to the project where the experiment plan is created';

CREATE  TABLE tenant.service ( 
	id                   serial  NOT NULL ,
	service_code         varchar(64)  NOT NULL ,
	service_name         varchar(128)  NOT NULL ,
	service_provider_id  integer  NOT NULL ,
	description          text   ,
	CONSTRAINT pk_service_id PRIMARY KEY ( id ),
	CONSTRAINT service_service_code_idx UNIQUE ( service_code ) 
 );

COMMENT ON CONSTRAINT pk_service_id ON tenant.service IS 'A service is identified by its unique database ID.';

COMMENT ON CONSTRAINT service_service_code_idx ON tenant.service IS 'A service can be retrieved by its unique service code.';

COMMENT ON TABLE tenant.service IS 'Service: Activity or set of activities carried out by a service provider as a response to the request of its customers [SERV]';

COMMENT ON COLUMN tenant.service.id IS 'Service ID: Database identifier of the service [SERV_ID]';

COMMENT ON COLUMN tenant.service.service_code IS 'Service Code: Textual identifier of the service [SERV_CODE]';

COMMENT ON COLUMN tenant.service.service_name IS 'Service Name: Full name of the service [SERV_NAME]';

COMMENT ON COLUMN tenant.service.service_provider_id IS 'Service Provider ID: Reference to the service provider that provides the service [SERV_SERVPROV_ID]';

COMMENT ON COLUMN tenant.service.description IS 'Service Description: Additional information abou the service [SERV_DESC]';

ALTER TABLE experiment.entry ADD CONSTRAINT entry_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_germplasm_id_fk ON experiment.entry IS 'An entry refers to one germplasm. A germplasm can be used in zero or many entries.';

ALTER TABLE experiment.entry ADD CONSTRAINT entry_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_seed_id_fk ON experiment.entry IS 'An entry uses one seed of a germplasm. A seed of a germplasm can be used in zero or many entries.';

ALTER TABLE experiment.entry ADD CONSTRAINT entry_entry_list_id_fk FOREIGN KEY ( entry_list_id ) REFERENCES experiment.entry_list( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_entry_list_id_fk ON experiment.entry IS 'An entry is a part of one entry list. An entry list can have one or many entries.';

ALTER TABLE experiment.entry_data ADD CONSTRAINT entry_data_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_data_entry_id_fk ON experiment.entry_data IS 'An entry data is an attribute of one entry. An entry can be attributed with zero or many entry data.';

ALTER TABLE experiment.entry_data ADD CONSTRAINT entry_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_data_variable_id_fk ON experiment.entry_data IS 'An entry data refers to one variable. A variable can be referred by zero or many entry data.';

ALTER TABLE experiment.entry_list ADD CONSTRAINT entry_list_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT entry_list_experiment_id_fk ON experiment.entry_list IS 'An entry list is used in one experiment. An experiment uses exactly one entry list.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_program_id_fk ON experiment.experiment IS 'An experiment is conducted by one program. A program can conduct zero or many experiments.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_stage_id_fk FOREIGN KEY ( stage_id ) REFERENCES tenant.stage( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_stage_id_fk ON experiment.experiment IS 'An experiment is conducted at one specific stage in a pipeline. A stage in a pipeline can be referred to by zero or many experiments.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_steward_id_fk FOREIGN KEY ( steward_id ) REFERENCES tenant.person( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_steward_id_fk ON experiment.experiment IS 'An experiment is managed by one steward (person). A steward (person) can manage zero or many experiments.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_project_id_fk FOREIGN KEY ( project_id ) REFERENCES tenant.project( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_project_id_fk ON experiment.experiment IS 'An experiment can be based on zero or one project. A project can have zero or many experiments based on it.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_pipeline_id_fk FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_pipeline_id_fk ON experiment.experiment IS 'An experiment is conducted in one pipeline of its program. A pipeline can be used in zero or many experiments.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_experiment_plan_id_fk FOREIGN KEY ( experiment_plan_id ) REFERENCES experiment.experiment_plan( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_experiment_plan_id_fk ON experiment.experiment IS 'An experiment can be based on zero or one pre-defined experiment plan. An experiment plan can be used to create zero or many experiments.';

ALTER TABLE experiment.experiment ADD CONSTRAINT experiment_season_id_fk FOREIGN KEY ( season_id ) REFERENCES tenant.season( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_season_id_fk ON experiment.experiment IS 'An experiment is conducted within one season or breeding cycle. A season or breeding cycle can hold one or many experiments.';

ALTER TABLE experiment.experiment_block ADD CONSTRAINT experiment_block_parent_experiment_block_id_fk FOREIGN KEY ( parent_experiment_block_id ) REFERENCES experiment.experiment_block( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_block_parent_experiment_block_id_fk ON experiment.experiment_block IS 'An experiment block may be a part of another experiment block. An experiment block may be comprised of zero or many experiment blocks.';

ALTER TABLE experiment.experiment_block ADD CONSTRAINT experiment_block_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_block_experiment_id_fk ON experiment.experiment_block IS 'An experiment block refers to one experiment. An experiment can have zero or many experiment blocks.';

ALTER TABLE experiment.experiment_data ADD CONSTRAINT experiment_data_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_data_experiment_id_fk ON experiment.experiment_data IS 'An experiment data is attributed to one experiment. An experiment can have zero or many experiment data.';

ALTER TABLE experiment.experiment_data ADD CONSTRAINT experiment_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_data_variable_id_fk ON experiment.experiment_data IS 'An experiment data refers to one variable. A variable can be referred by zero or many experiment data.';

ALTER TABLE experiment.experiment_data ADD CONSTRAINT experiment_data_protocol_id_fk FOREIGN KEY ( protocol_id ) REFERENCES tenant.protocol( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_data_protocol_id_fk ON experiment.experiment_data IS 'An experiment data can be based on one protocol. A protocol can be referred by zero or many experiment data.';

ALTER TABLE experiment.experiment_design ADD CONSTRAINT experiment_design_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_design_plot_id_fk ON experiment.experiment_design IS 'An experiment design refers to one plot. A plot can have zero or many experiment design.';

ALTER TABLE experiment.experiment_design ADD CONSTRAINT experiment_design_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_design_occurrence_id_fk ON experiment.experiment_design IS 'An experiment design refers to one occurrence. An occurrence can have zero or many experiment designs.';

ALTER TABLE experiment.experiment_group_member ADD CONSTRAINT experiment_group_member_experiment_group_id_fx FOREIGN KEY ( experiment_group_id ) REFERENCES experiment.experiment_group( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_group_member_experiment_group_id_fx ON experiment.experiment_group_member IS 'An experiment group can have zero or many experiments.';

ALTER TABLE experiment.experiment_group_member ADD CONSTRAINT experiment_group_member_experiment_id_idx FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_group_member_experiment_id_idx ON experiment.experiment_group_member IS 'An experiment can belong to one or many experiment groups.';

ALTER TABLE experiment.experiment_plan ADD CONSTRAINT experiment_plan_project_id_fk FOREIGN KEY ( project_id ) REFERENCES tenant.project( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_plan_project_id_fk ON experiment.experiment_plan IS 'An experiment plan can be created by one project. A project can have zero or many experiment plans.';

ALTER TABLE experiment.experiment_protocol ADD CONSTRAINT experiment_protocol_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_protocol_experiment_id_fk ON experiment.experiment_protocol IS 'An experiment protocol refers to one experiment. An experiment can refer to one or many experiment protocols.';

ALTER TABLE experiment.experiment_protocol ADD CONSTRAINT experiment_protocol_protocol_id_fk FOREIGN KEY ( protocol_id ) REFERENCES tenant.protocol( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT experiment_protocol_protocol_id_fk ON experiment.experiment_protocol IS 'An experiment protocol refers to one protocol. A protocol can be referred by one or many experiment protocols.';

ALTER TABLE experiment."location" ADD CONSTRAINT location_geospatial_object_id_fk FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_geospatial_object_id_fk ON experiment."location" IS 'A location refers to one geospatial object (planting area). A geospatial object (planting area) can refer to one or many locations.';

ALTER TABLE experiment."location" ADD CONSTRAINT location_steward_id_fk FOREIGN KEY ( steward_id ) REFERENCES tenant.person( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_steward_id_fk ON experiment."location" IS 'A location is managed by one steward (person). A steward (person) can manage zero or many locations.';

ALTER TABLE experiment.location_data ADD CONSTRAINT location_data_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_data_location_id_fk ON experiment.location_data IS 'A location data is an attribute of one location. A location can be attributed with zero or many location data.';

ALTER TABLE experiment.location_data ADD CONSTRAINT location_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_data_variable_id_fk ON experiment.location_data IS 'A location data refers to one variable. A variable can be referred by zero or many location data.';

ALTER TABLE experiment.location_occurrence_group ADD CONSTRAINT location_occurrence_group_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_occurrence_group_location_id_fk ON experiment.location_occurrence_group IS 'A location has one or many occurrences of the same experiment or of different experiments. An occurrence is conducted in one location.';

ALTER TABLE experiment.location_occurrence_group ADD CONSTRAINT location_occurrence_group_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_occurrence_group_occurrence_id_fk ON experiment.location_occurrence_group IS 'A location has one or An occurrence is conducted in one location. more occurrences of the same experiment or coming from different experiments.';

ALTER TABLE experiment.location_status ADD CONSTRAINT location_status_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_status_location_id_fk ON experiment.location_status IS 'A location status refers to one location. A location can have zero or many location statuses.';

ALTER TABLE experiment.location_task ADD CONSTRAINT location_task_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_task_location_id_fk ON experiment.location_task IS 'A location task is carried out in one location. A location can have zero or many location tasks.';

ALTER TABLE experiment.location_task ADD CONSTRAINT location_task_task_id_fk FOREIGN KEY ( task_id ) REFERENCES master.item( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_task_task_id_fk ON experiment.location_task IS 'A location task refers to one task (item). A task (item) can be used in zero or many location tasks.';

ALTER TABLE experiment.location_task ADD CONSTRAINT location_task_protocol_id_fk FOREIGN KEY ( protocol_id ) REFERENCES tenant.protocol( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_task_protocol_id_fk ON experiment.location_task IS 'A location task can refer to one protocol. A protocol can be used in zero or many location tasks.';

ALTER TABLE experiment.location_task ADD CONSTRAINT location_task_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT location_task_occurrence_id_fk ON experiment.location_task IS 'A location task may refer to one occurrence where the task was carried out. An occurrence can have zero or many location tasks.';

ALTER TABLE experiment.occurrence ADD CONSTRAINT occurrence_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_experiment_id_fk ON experiment.occurrence IS 'An occurrence is an instance of one experiment. An experiment can have one or many occurrences.';

ALTER TABLE experiment.occurrence ADD CONSTRAINT occurrence_geospatial_object_id_fk FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_geospatial_object_id_fk ON experiment.occurrence IS 'An occurrence is planned to be conducted in one site or field (geospatial object). A site or field (geospatial object) can be used by zero or many occurrences of experiments.';

ALTER TABLE experiment.occurrence_data ADD CONSTRAINT occurrence_data_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_data_occurrence_id_fk ON experiment.occurrence_data IS 'An occurrence data is an attribute of one occurrence. An occurrence can be attributed with zero or many occurrence data.';

ALTER TABLE experiment.occurrence_data ADD CONSTRAINT occurrence_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_data_variable_id_fk ON experiment.occurrence_data IS 'An occurrence data refers to one variable. A variable can be referred by zero or many occurrence data.';

ALTER TABLE experiment.occurrence_group_member ADD CONSTRAINT occurrence_group_member_occurrence_group_id_fk FOREIGN KEY ( occurrence_group_id ) REFERENCES experiment.occurrence_group( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_group_member_occurrence_group_id_fk ON experiment.occurrence_group_member IS 'An occurrence group member belongs to one occurrence group. An occurrence group can have zero or many occurrences as its members.';

ALTER TABLE experiment.occurrence_group_member ADD CONSTRAINT occurrence_group_member_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_group_member_occurrence_id_fk ON experiment.occurrence_group_member IS 'An occurrence group member refers to one occurrence. An occurrence can be referred by zero or many occurrence group members.';

ALTER TABLE experiment.occurrence_layout ADD CONSTRAINT occurrence_layout_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT occurrence_layout_occurrence_id_fk ON experiment.occurrence_layout IS 'An occurrence layout refers to one occurrence. An occurrence can have zero or one occurrence layout.';

ALTER TABLE experiment.plant ADD CONSTRAINT plant_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plant_plot_id_fk ON experiment.plant IS 'A plant is part of a plot. A plot can have zero or many plants.';

ALTER TABLE experiment.plant ADD CONSTRAINT plant_subplot_id_fk FOREIGN KEY ( subplot_id ) REFERENCES experiment.subplot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plant_subplot_id_fk ON experiment.plant IS 'A plant may be part of a subplot. A subplot can have zero or many plants.';

ALTER TABLE experiment.plant_data ADD CONSTRAINT plant_data_plant_id_fk FOREIGN KEY ( plant_id ) REFERENCES experiment.plant( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plant_data_plant_id_fk ON experiment.plant_data IS 'A plant data is collected from one plant. A plant can be collected with zero or many plant data.';

ALTER TABLE experiment.plant_data ADD CONSTRAINT plant_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plant_data_variable_id_fk ON experiment.plant_data IS 'A plant data refers to one variable. A variable can be referred by zero or many plant data.';

ALTER TABLE experiment.planting_envelope ADD CONSTRAINT planting_envelope_planting_instruction_id_fk FOREIGN KEY ( planting_instruction_id ) REFERENCES experiment.planting_instruction( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_envelope_planting_instruction_id_fk ON experiment.planting_envelope IS 'A planting envelope refers to one planting instruction. A planting instruction can be allocated with one or many planting envelopes.';

ALTER TABLE experiment.planting_envelope ADD CONSTRAINT planting_envelope_envelope_id_fk FOREIGN KEY ( envelope_id ) REFERENCES germplasm.envelope( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_envelope_envelope_id_fk ON experiment.planting_envelope IS 'A planting envelope refers to the allocated envelope of a package. An envelope of a package is used in one planting envelope.';

ALTER TABLE experiment.planting_instruction ADD CONSTRAINT planting_instruction_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_instruction_plot_id_fk ON experiment.planting_instruction IS 'A planting instruction is directed to one plot. A plot can have one planting instruction.';

ALTER TABLE experiment.planting_instruction ADD CONSTRAINT planting_instruction_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_instruction_entry_id_fk ON experiment.planting_instruction IS 'A planting instruction refers to the entry of a plot. An entry can have one or many planting instructions depending on the number of its replication in the plot.';

ALTER TABLE experiment.planting_instruction ADD CONSTRAINT planting_instruction_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_instruction_germplasm_id_fk ON experiment.planting_instruction IS 'A planting instruction refers to the germplasm that is planted in the plot, which can either be the original germplasm defined in the experiment or its replacement germplasm. A germplasm can be used in one or many planting instructions of plots.';

ALTER TABLE experiment.planting_instruction ADD CONSTRAINT planting_instruction_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_instruction_seed_id_fk ON experiment.planting_instruction IS 'A planting instruction refers to one seed that is allocated for planting in the plot. A seed can be allocated in one or many planting instructions of plots.';

ALTER TABLE experiment.planting_instruction ADD CONSTRAINT planting_instruction_entry_id_entry_code_entry_number_fk FOREIGN KEY ( entry_id, entry_code, entry_number ) REFERENCES experiment.entry( id, entry_code, entry_number ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT planting_instruction_entry_id_entry_code_entry_number_fk ON experiment.planting_instruction IS 'A planting instruction refers to the combination of its entry ID, entry code, and entry number. A combination of the entry id, entry code, and entry number can refer to one or many planting instructions, which means that any updates in the entry will cascade to the planting instructions.';

ALTER TABLE experiment.plot ADD CONSTRAINT plot_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plot_location_id_fk ON experiment.plot IS 'A plot is observed in one location. A location can have one or many plots.';

ALTER TABLE experiment.plot ADD CONSTRAINT plot_occurrence_id_fk FOREIGN KEY ( occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plot_occurrence_id_fk ON experiment.plot IS 'A plot is part of one occurrence. An occurrence has one or many plots.';

ALTER TABLE experiment.plot ADD CONSTRAINT plot_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plot_entry_id_fk ON experiment.plot IS 'A plot refers to one entry. An entry can have one or many plots.';

ALTER TABLE experiment.plot_data ADD CONSTRAINT plot_data_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plot_data_plot_id_fk ON experiment.plot_data IS 'A plot data is collected from one plot. A plot can be collected with zero or many plot data.';

ALTER TABLE experiment.plot_data ADD CONSTRAINT plot_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT plot_data_variable_id_fk ON experiment.plot_data IS 'A plot data refers to one variable. A variable can be referred by zero or many plot data.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_entry_id_fk ON experiment.sample IS 'A sample may refer to an entry in the experiment. An entry can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_plot_id_fk ON experiment.sample IS 'A sample may be derived from a plot in the location of an experiment. A plot can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_subplot_id_fk FOREIGN KEY ( subplot_id ) REFERENCES experiment.subplot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_subplot_id_fk ON experiment.sample IS 'A sample may be derived from a subplot in the location of an experiment. A subplot can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_plant_id_fk FOREIGN KEY ( plant_id ) REFERENCES experiment.plant( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_plant_id_fk ON experiment.sample IS 'A sample may be derived from a specific plant within a plot in a location. A plant can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_geospatial_object_id_fk FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_geospatial_object_id_fk ON experiment.sample IS 'A sample may be located at a specific geospatial object. A geospatial object can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_location_id_fk FOREIGN KEY ( location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_location_id_fk ON experiment.sample IS 'A sample comes from a location in an experiment. A location can have zero or many samples.';

ALTER TABLE experiment.sample ADD CONSTRAINT sample_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_seed_id_fk ON experiment.sample IS 'A sample can refer to a seed. A seed can have zero or many samples.';

ALTER TABLE experiment.sample_measurement ADD CONSTRAINT sample_measurement_sample_id_fk FOREIGN KEY ( sample_id ) REFERENCES experiment.sample( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_measurement_sample_id_fk ON experiment.sample_measurement IS 'A sample measurement is taken from a sample. A sample can have zero or many sample measurements.';

ALTER TABLE experiment.sample_measurement ADD CONSTRAINT sample_measurement_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT sample_measurement_variable_id_fk ON experiment.sample_measurement IS 'A sample measurement refers to a variable. A variable can be referred by zero or many sample measurements.';

ALTER TABLE experiment.subplot ADD CONSTRAINT subplot_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT subplot_plot_id_fk ON experiment.subplot IS 'A subplot belongs to one plot. A plot can have zero or many subplots.';

ALTER TABLE experiment.subplot ADD CONSTRAINT subplot_parent_subplot_id_fk FOREIGN KEY ( parent_subplot_id ) REFERENCES experiment.subplot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT subplot_parent_subplot_id_fk ON experiment.subplot IS 'A subplot (child) can belong to zero or one subplot (parent). A subplot (parent) can have zero or many subplots (children) in it.';

ALTER TABLE experiment.subplot_data ADD CONSTRAINT subplot_data_subplot_id_fk FOREIGN KEY ( subplot_id ) REFERENCES experiment.subplot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT subplot_data_subplot_id_fk ON experiment.subplot_data IS 'A subplot data is collected from one subplot. A subplot can be collected with zero or many subplot data.';

ALTER TABLE experiment.subplot_data ADD CONSTRAINT subplot_data_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT subplot_data_variable_id_fk ON experiment.subplot_data IS 'A subplot data refers to one variable. A variable can be referred by zero or many subplot data.';

ALTER TABLE germplasm."cross" ADD CONSTRAINT cross_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_germplasm_id_fk ON germplasm."cross" IS 'A cross can be an outcome of a new germplasm. A germplasm came from one cross.';

ALTER TABLE germplasm."cross" ADD CONSTRAINT cross_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_experiment_id_fk ON germplasm."cross" IS 'A cross is found in one experiment. An experiment can have one or many crosses.';

ALTER TABLE germplasm."cross" ADD CONSTRAINT cross_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_entry_id_fk ON germplasm."cross" IS 'A cross is an entry in an experiment. An entry can refer to one cross.';

ALTER TABLE germplasm."cross" ADD CONSTRAINT cross_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_seed_id_fk ON germplasm."cross" IS 'A cross is created from the harvested seed of a germplasm. A seed is generated from a successful cross.';

ALTER TABLE germplasm.cross_attribute ADD CONSTRAINT cross_attribute_cross_id_fk FOREIGN KEY ( cross_id ) REFERENCES germplasm."cross"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_attribute_cross_id_fk ON germplasm.cross_attribute IS 'A cross attribute refers to one cross. A cross can have one or many cross attributes.';

ALTER TABLE germplasm.cross_attribute ADD CONSTRAINT cross_attribute_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_attribute_variable_id_fk ON germplasm.cross_attribute IS 'A cross attribute refers to one variable. A variable can be referred by one or many cross attributes.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_cross_id_fk FOREIGN KEY ( cross_id ) REFERENCES germplasm."cross"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_cross_id_fk ON germplasm.cross_parent IS 'A parent refers to a cross. A cross can have one or many parents.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_germplasm_id_fk ON germplasm.cross_parent IS 'A cross parent refers to a germplasm as the parent. A germplasm can be referred by a parent in a cross.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_seed_id_fk ON germplasm.cross_parent IS 'A parent refers to a seed. A seed can refer to one parent.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_experiment_id_fk FOREIGN KEY ( experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_experiment_id_fk ON germplasm.cross_parent IS 'A parent is added to the experiment. An experiment can have one or many parents.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_entry_id_fk FOREIGN KEY ( entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_entry_id_fk ON germplasm.cross_parent IS 'A parent is an entry in an experiment. An entry can be a parent of a cross.';

ALTER TABLE germplasm.cross_parent ADD CONSTRAINT cross_parent_plot_id_fk FOREIGN KEY ( plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT cross_parent_plot_id_fk ON germplasm.cross_parent IS 'A parent is retrieved from a specific plot. A plot can be a parent of a cross.';

ALTER TABLE germplasm.envelope ADD CONSTRAINT envelope_package_id_fk FOREIGN KEY ( package_id ) REFERENCES germplasm.package( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT envelope_package_id_fk ON germplasm.envelope IS 'An envelope is a portion of a package. A package can have one or many envelopes.';

ALTER TABLE germplasm.family ADD CONSTRAINT family_cross_id_fk FOREIGN KEY ( cross_id ) REFERENCES germplasm.cross( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT family_cross_id_fk ON germplasm.family IS 'A family refers to the cross where it originates. A cross can be referred by one or more family records.';

ALTER TABLE germplasm.family ADD CONSTRAINT family_parent_germplasm_id_fk FOREIGN KEY ( parent_germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT family_parent_germplasm_id_fk ON germplasm.family IS 'A family can refer to one germplasm (parent) as a direct ancestor, and it can have one or more direct descendants (children).';

ALTER TABLE germplasm.family ADD CONSTRAINT family_child_germplasm_id_fk FOREIGN KEY ( child_germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT family_child_germplasm_id_fk ON germplasm.family IS 'A family can refer to one germplasm (child) as a direct descendant, and it can have one direct ancestor (parent).';

ALTER TABLE germplasm.family ADD CONSTRAINT family_source_seed_id_fk FOREIGN KEY ( source_seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT family_source_seed_id_fk ON germplasm.family IS 'A family can refer to seed that is the source of the germplasm (child).';

ALTER TABLE germplasm.genetic_population ADD CONSTRAINT fk_genetic_population_germplasm_id FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_genetic_population_germplasm_id ON germplasm.genetic_population IS 'Genetic population refers to one or more germplasm';

ALTER TABLE germplasm.genetic_population ADD CONSTRAINT fk_genetic_population_seed_id FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_genetic_population_seed_id ON germplasm.genetic_population IS 'Genetic population refers to one or more seeds';

ALTER TABLE germplasm.genetic_population_attribute ADD CONSTRAINT fk_genetic_population_attribute_genetic_population_id FOREIGN KEY ( genetic_population_id ) REFERENCES germplasm.genetic_population( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_genetic_population_attribute_genetic_population_id ON germplasm.genetic_population_attribute IS 'Genetic population attribute is referring to one genetic population';

ALTER TABLE germplasm.germplasm ADD CONSTRAINT germplasm_product_profile_id_fk FOREIGN KEY ( product_profile_id ) REFERENCES germplasm.product_profile( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_product_profile_id_fk ON germplasm.germplasm IS 'A germplasm can refer to one product profile. A product profile can be referred by one or many germplasm.';

ALTER TABLE germplasm.germplasm ADD CONSTRAINT germplasm_taxonomy_id_fk FOREIGN KEY ( taxonomy_id ) REFERENCES germplasm.taxonomy( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_taxonomy_id_fk ON germplasm.germplasm IS 'A germplasm has one taxonomy. A taxonomy can refer to one or many germplasm.';

ALTER TABLE germplasm.germplasm ADD CONSTRAINT germplasm_crop_id_fk FOREIGN KEY ( crop_id ) REFERENCES tenant.crop( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_crop_id_fk ON germplasm.germplasm IS 'A germplasm refers to one crop. A crop can have one or many germplasm.';

ALTER TABLE germplasm.germplasm_attribute ADD CONSTRAINT germplasm_attribute_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_attribute_germplasm_id_fk ON germplasm.germplasm_attribute IS 'A germplasm attribute refers to one germplasm. A germplasm can have one or many germplasm attributes.';

ALTER TABLE germplasm.germplasm_attribute ADD CONSTRAINT germplasm_attribute_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_attribute_variable_id_fk ON germplasm.germplasm_attribute IS 'A germplasm attribute refers to one variable. A variable can be referred by one or many germplasm attributes.';

ALTER TABLE germplasm.germplasm_name ADD CONSTRAINT germplasm_name_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_name_germplasm_id_fk ON germplasm.germplasm_name IS 'A germplasm name is designated to one germplasm. A germplasm can be designated with one or many germplasm names.';

ALTER TABLE germplasm.germplasm_relation ADD CONSTRAINT germplasm_relation_parent_germplasm_id_fk FOREIGN KEY ( parent_germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_relation_parent_germplasm_id_fk ON germplasm.germplasm_relation IS 'A germplasm relation refers to one germplasm (parent), where the germplasm (child) is related.';

ALTER TABLE germplasm.germplasm_relation ADD CONSTRAINT germplasm_relation_child_germplasm_id_fk FOREIGN KEY ( child_germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_relation_child_germplasm_id_fk ON germplasm.germplasm_relation IS 'A germplasm relation refers to one or many germplasm (children), which is related to the another germplasm (parent).';

ALTER TABLE germplasm.germplasm_state_attribute ADD CONSTRAINT germplasm_state_attribute_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_state_attribute_germplasm_id_fk ON germplasm.germplasm_state_attribute IS 'A germplasm state attribute refers to one germplasm. A germplasm can have one or many germplasm state attributes.';

ALTER TABLE germplasm.germplasm_trait ADD CONSTRAINT germplasm_trait_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT germplasm_trait_germplasm_id_fk ON germplasm.germplasm_trait IS 'A germplasm trait is attributed to one germplasm. A germplasm can have one or many germplasm traits.';

ALTER TABLE germplasm.germplasm_trait ADD CONSTRAINT germplasm_trait_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id );

COMMENT ON CONSTRAINT germplasm_trait_variable_id_fk ON germplasm.germplasm_trait IS 'A germplasm trait refers to one variable. A variable can be referred by one or many germplasm traits.';

ALTER TABLE germplasm.package ADD CONSTRAINT package_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_seed_id_fk ON germplasm.package IS 'A package refers to one seed. A seed can have one or many packages.';

ALTER TABLE germplasm.package ADD CONSTRAINT package_geospatial_object_id_fk FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_geospatial_object_id_fk ON germplasm.package IS 'A package is stored in one geospatial object. A geospatial object can store one or many packages.';

ALTER TABLE germplasm.package ADD CONSTRAINT package_facility_id_fk FOREIGN KEY ( facility_id ) REFERENCES place.facility( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_facility_id_fk ON germplasm.package IS 'A package can be stored in one facility. A facility can store one or many packages.';

ALTER TABLE germplasm.package ADD CONSTRAINT package_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_program_id_fk ON germplasm.package IS 'A package is owned by one program. A program can own one or many packages.';

ALTER TABLE germplasm.package_relation ADD CONSTRAINT package_relation_source_package_id_fk FOREIGN KEY ( source_package_id ) REFERENCES germplasm.package( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_relation_source_package_id_fk ON germplasm.package_relation IS 'A package relation refers to the source package, which maybe the component of the single destination package (relation type = combined), or the origin of several destination packages (relation type = divided/subset).';

ALTER TABLE germplasm.package_relation ADD CONSTRAINT package_relation_destination_package_id_fk FOREIGN KEY ( destination_package_id ) REFERENCES germplasm.package( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_relation_destination_package_id_fk ON germplasm.package_relation IS 'A package relation refers to the destination package, which maybe the result of combining several source packages (relation type = combined), or the outcome of splitting up a single source package (relation type = divided/subset).';

ALTER TABLE germplasm.package_trait ADD CONSTRAINT package_trait_package_id_fk FOREIGN KEY ( package_id ) REFERENCES germplasm.package( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_trait_package_id_fk ON germplasm.package_trait IS 'A package trait refers to one package. A package can have one or many package traits.';

ALTER TABLE germplasm.package_trait ADD CONSTRAINT package_trait_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT package_trait_variable_id_fk ON germplasm.package_trait IS 'A package trait refers to one variable. A variable can be referred by one or many package traits.';

ALTER TABLE germplasm.product_profile ADD CONSTRAINT product_profile_pipeline_id_fk FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT product_profile_pipeline_id_fk ON germplasm.product_profile IS 'A product profile can be created from a pipeline. A pipeline can have one or many product profiles.';

ALTER TABLE germplasm.product_profile ADD CONSTRAINT product_profile_project_id_fk FOREIGN KEY ( project_id ) REFERENCES tenant.project( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT product_profile_project_id_fk ON germplasm.product_profile IS 'A product profile can be created by a project. A project can have one or many product profiles.';

ALTER TABLE germplasm.product_profile_attribute ADD CONSTRAINT product_profile_data_product_profile_id_fk FOREIGN KEY ( product_profile_id ) REFERENCES germplasm.product_profile( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT product_profile_data_product_profile_id_fk ON germplasm.product_profile_attribute IS 'A product profile attribute referst to one product profile. A product profile can have zero or many product profile attributes.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_germplasm_id_fk FOREIGN KEY ( germplasm_id ) REFERENCES germplasm.germplasm( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_germplasm_id_fk ON germplasm.seed IS 'A seed refers to one germplasm. A germplasm can have one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_source_experiment_id_fk FOREIGN KEY ( source_experiment_id ) REFERENCES experiment.experiment( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_source_experiment_id_fk ON germplasm.seed IS 'A seed is harvested from one experiment. An experiment can produce one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_source_entry_id_fk FOREIGN KEY ( source_entry_id ) REFERENCES experiment.entry( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_source_entry_id_fk ON germplasm.seed IS 'A seed is harvested from an entry. An entry can produce one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_source_plot_id_fk FOREIGN KEY ( source_plot_id ) REFERENCES experiment.plot( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_source_plot_id_fk ON germplasm.seed IS 'A seed is harvested from a plot. A plot can produce one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_source_location_id_fk FOREIGN KEY ( source_location_id ) REFERENCES experiment."location"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_source_location_id_fk ON germplasm.seed IS 'A seed is harvested from one location. A location can produce one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_program_id_fk ON germplasm.seed IS 'A seed is managed by one program. A program can manage one or many seeds.';

ALTER TABLE germplasm.seed ADD CONSTRAINT seed_source_occurrence_id_fk FOREIGN KEY ( source_occurrence_id ) REFERENCES experiment.occurrence( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_source_occurrence_id_fk ON germplasm.seed IS 'A seed may be harvested from a specific occurrence. An occurrence can produce one or many seeds.';

ALTER TABLE germplasm.seed_attribute ADD CONSTRAINT seed_attribute_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_attribute_seed_id_fk ON germplasm.seed_attribute IS 'A seed attribute refers to one seed. A seed can have one or many seed attributes.';

ALTER TABLE germplasm.seed_attribute ADD CONSTRAINT seed_attribute_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_attribute_variable_id_fk ON germplasm.seed_attribute IS 'A seed attribute refers to one variable. A variable can be referred by one or many seed attributes.';

ALTER TABLE germplasm.seed_relation ADD CONSTRAINT seed_relation_parent_seed_id_fk FOREIGN KEY ( parent_seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_relation_parent_seed_id_fk ON germplasm.seed_relation IS 'A seed relation may refer to a parent seed. A parent seed can have one or many child seeds.';

ALTER TABLE germplasm.seed_relation ADD CONSTRAINT seed_relation_child_id_fk FOREIGN KEY ( child_seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_relation_child_id_fk ON germplasm.seed_relation IS 'A seed relation can refer to a child seed. A child seed can refer to one parent seed.';

ALTER TABLE germplasm.seed_trait ADD CONSTRAINT seed_trait_seed_id_fk FOREIGN KEY ( seed_id ) REFERENCES germplasm.seed( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_trait_seed_id_fk ON germplasm.seed_trait IS 'A seed trait refers to one seed. A seed can have one or many seed traits.';

ALTER TABLE germplasm.seed_trait ADD CONSTRAINT seed_trait_variable_id_fk FOREIGN KEY ( variable_id ) REFERENCES master."variable"( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT seed_trait_variable_id_fk ON germplasm.seed_trait IS 'A seed trait refers to one variable. A variable can be referred by one or many seed traits.';

ALTER TABLE germplasm.taxonomy ADD CONSTRAINT taxonomy_crop_id_idx FOREIGN KEY ( crop_id ) REFERENCES tenant.crop( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT taxonomy_crop_id_idx ON germplasm.taxonomy IS 'A taxonomy refers to one crop. A crop can have one or many taxonomy.';

ALTER TABLE place.facility ADD CONSTRAINT facility_parent_facility_id_fk FOREIGN KEY ( parent_facility_id ) REFERENCES place.facility( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT facility_parent_facility_id_fk ON place.facility IS 'A facility can belong to a parent facility. A parent facility can have one or many sub-facilities.';

ALTER TABLE place.facility ADD CONSTRAINT facility_root_facility_id_fk FOREIGN KEY ( root_facility_id ) REFERENCES place.facility( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT facility_root_facility_id_fk ON place.facility IS 'A facility can belong to a root facility. A root facility can have one or many sub-facilities.';

ALTER TABLE place.geospatial_object ADD CONSTRAINT geospatial_object_geospatial_object_parent_id_fk FOREIGN KEY ( parent_geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT geospatial_object_geospatial_object_parent_id_fk ON place.geospatial_object IS 'A geospatial object may be part of another geospatial object (parent). A geospatial object (parent) can have zero or many other geospatial objects (children) under it.';

ALTER TABLE place.geospatial_object ADD CONSTRAINT geospatial_object_root_geospatial_object_id_idx FOREIGN KEY ( root_geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT geospatial_object_root_geospatial_object_id_idx ON place.geospatial_object IS 'A geospatial object may belong to a highest geospatial object (root). A geospatial object (root) can have zero or many geospatial objects (children) under it.';

ALTER TABLE place.geospatial_object_attribute ADD CONSTRAINT geospatial_object_attribute_geospatial_object_id_fk FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT geospatial_object_attribute_geospatial_object_id_fk ON place.geospatial_object_attribute IS 'A geospatial object attribute refers to a geospatial object. A geospatial object can have zero or many geospatial object attributes.';

ALTER TABLE tenant.breeding_zone ADD CONSTRAINT fk_breeding_zone_pipeline_id FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_breeding_zone_pipeline_id ON tenant.breeding_zone IS 'Breeding zone is related to at least one pipeline';

ALTER TABLE tenant.breeding_zone ADD CONSTRAINT fk_breeding_zone_geospatial_object_id FOREIGN KEY ( geospatial_object_id ) REFERENCES place.geospatial_object( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_breeding_zone_geospatial_object_id ON tenant.breeding_zone IS 'Breeding zone is a geospatial object';

ALTER TABLE tenant.crop_program ADD CONSTRAINT crop_program_organization_id_fk FOREIGN KEY ( organization_id ) REFERENCES tenant.organization( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT crop_program_organization_id_fk ON tenant.crop_program IS 'A crop program is under one organization. An organization can have zero or many crop programs.';

ALTER TABLE tenant.crop_program ADD CONSTRAINT crop_program_crop_id_fk FOREIGN KEY ( crop_id ) REFERENCES tenant.crop( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT crop_program_crop_id_fk ON tenant.crop_program IS 'A crop program researches one crop. A crop can be researched in one or many crop programs.';

ALTER TABLE tenant.crop_program_team ADD CONSTRAINT crop_program_team_team_id_fk FOREIGN KEY ( team_id ) REFERENCES tenant.team( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT crop_program_team_team_id_fk ON tenant.crop_program_team IS 'A team can belong to one or many crop programs.';

ALTER TABLE tenant.crop_program_team ADD CONSTRAINT crop_program_team_crop_program_id_fk FOREIGN KEY ( crop_program_id ) REFERENCES tenant.crop_program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT crop_program_team_crop_program_id_fk ON tenant.crop_program_team IS 'A crop program can have one or more teams.';

ALTER TABLE tenant.experiment_plan ADD CONSTRAINT fk_experiment_plan_project_id FOREIGN KEY ( project_id ) REFERENCES tenant.project( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_experiment_plan_project_id ON tenant.experiment_plan IS 'Experiment plan is created in the project';

ALTER TABLE tenant.person ADD CONSTRAINT fk_person_person_role_id FOREIGN KEY ( person_role_id ) REFERENCES tenant.person_role( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_person_person_role_id ON tenant.person IS 'Person is assigned with a role';

ALTER TABLE tenant.phase ADD CONSTRAINT phase_pipeline_id_fk FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT phase_pipeline_id_fk ON tenant.phase IS 'A phase belongs to one pipeline. A pipeline can have one or many phases.';

ALTER TABLE tenant.phase_scheme ADD CONSTRAINT phase_scheme_phase_id_fk FOREIGN KEY ( phase_id ) REFERENCES tenant.phase( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT phase_scheme_phase_id_fk ON tenant.phase_scheme IS 'A phase is composed of one or more schemes.';

ALTER TABLE tenant.phase_scheme ADD CONSTRAINT phase_scheme_scheme_id_fk FOREIGN KEY ( scheme_id ) REFERENCES tenant.scheme( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT phase_scheme_scheme_id_fk ON tenant.phase_scheme IS 'A scheme can belong to one or more phases.';

ALTER TABLE tenant.program ADD CONSTRAINT program_crop_program_id_fk FOREIGN KEY ( crop_program_id ) REFERENCES tenant.crop_program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT program_crop_program_id_fk ON tenant.program IS 'A program belongs to one crop program. A crop program can have one or many programs.';

ALTER TABLE tenant.program_config ADD CONSTRAINT fk_program_config_program_id FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_program_config_program_id ON tenant.program_config IS 'Program config refers the program that uses the config';

ALTER TABLE tenant.program_team ADD CONSTRAINT program_team_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT program_team_program_id_fk ON tenant.program_team IS 'A program can have one or many teams.';

ALTER TABLE tenant.program_team ADD CONSTRAINT program_team_team_id_fk FOREIGN KEY ( team_id ) REFERENCES tenant.team( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT program_team_team_id_fk ON tenant.program_team IS 'A team can belong to one or many programs.';

ALTER TABLE tenant.project ADD CONSTRAINT project_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT project_program_id_fk ON tenant.project IS 'A project is created and executed by one program. A program can have one or many projects.';

ALTER TABLE tenant.project ADD CONSTRAINT project_leader_id_fk FOREIGN KEY ( leader_id ) REFERENCES tenant.person( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT project_leader_id_fk ON tenant.project IS 'A project is managed by a leader (person). A leader (person) can manage zero or many projects.';

ALTER TABLE tenant.project ADD CONSTRAINT project_pipeline_id_fk FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT project_pipeline_id_fk ON tenant.project IS 'A project uses one pipeline. A pipeline can be used in one or many projects.';

ALTER TABLE tenant.protocol ADD CONSTRAINT protocol_program_id_fk FOREIGN KEY ( program_id ) REFERENCES tenant.program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT protocol_program_id_fk ON tenant.protocol IS 'A protocol is created by one program. A program can create zero or many protocols.';

ALTER TABLE tenant.protocol_data ADD CONSTRAINT protocol_data_protocol_id_fk FOREIGN KEY ( protocol_id ) REFERENCES tenant.protocol( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT protocol_data_protocol_id_fk ON tenant.protocol_data IS 'A protocol data refers to one protocol. A protocol can have zero or many protocol data.';

ALTER TABLE tenant.scheme ADD CONSTRAINT scheme_pipeline_id_fk FOREIGN KEY ( pipeline_id ) REFERENCES tenant.pipeline( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT scheme_pipeline_id_fk ON tenant.scheme IS 'A scheme refers to one pipeline. A pipeline can have one or many schemes.';

ALTER TABLE tenant.scheme_stage ADD CONSTRAINT fk_scheme_stage_scheme_id FOREIGN KEY ( scheme_id ) REFERENCES tenant.scheme( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_scheme_stage_scheme_id ON tenant.scheme_stage IS 'Scheme can have one or more stages';

ALTER TABLE tenant.scheme_stage ADD CONSTRAINT fk_scheme_stage_stage_id FOREIGN KEY ( stage_id ) REFERENCES tenant.stage( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT fk_scheme_stage_stage_id ON tenant.scheme_stage IS 'Stage can belong to one or more schemes';

ALTER TABLE tenant.service ADD CONSTRAINT service_service_provider_id_fk FOREIGN KEY ( service_provider_id ) REFERENCES tenant.service_provider( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT service_service_provider_id_fk ON tenant.service IS 'A service is offered by one service provider. A service provider can offer one or many services.';

ALTER TABLE tenant.service_provider ADD CONSTRAINT service_provider_crop_program_id_fk FOREIGN KEY ( crop_program_id ) REFERENCES tenant.crop_program( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT service_provider_crop_program_id_fk ON tenant.service_provider IS 'A service provider belongs to a crop program. A crop program can have zero or many service providers.';

ALTER TABLE tenant.service_provider ADD CONSTRAINT service_provider_organization_id_fk FOREIGN KEY ( organization_id ) REFERENCES tenant.organization( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT service_provider_organization_id_fk ON tenant.service_provider IS 'A service provider belongs to an organization. An organization can have zero or many service providers.';

ALTER TABLE tenant.service_provider_team ADD CONSTRAINT service_provider_team_team_id_fk FOREIGN KEY ( team_id ) REFERENCES tenant.team( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT service_provider_team_team_id_fk ON tenant.service_provider_team IS 'A team can belong to one or more service providers.';

ALTER TABLE tenant.service_provider_team ADD CONSTRAINT service_provider_team_service_provider_id_fk FOREIGN KEY ( service_provider_id ) REFERENCES tenant.service_provider( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT service_provider_team_service_provider_id_fk ON tenant.service_provider_team IS 'A service provider can consist of one or more teams.';

ALTER TABLE tenant.team_member ADD CONSTRAINT team_member_team_id_fk FOREIGN KEY ( team_id ) REFERENCES tenant.team( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT team_member_team_id_fk ON tenant.team_member IS 'A team member (person) can belong to one or more teams. A team can have one or more team members (persons).';

ALTER TABLE tenant.team_member ADD CONSTRAINT team_member_person_id_fk FOREIGN KEY ( person_id ) REFERENCES tenant.person( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT team_member_person_id_fk ON tenant.team_member IS 'A team member refers to one person. A person can be a team member in one or many teams.';

ALTER TABLE tenant.team_member ADD CONSTRAINT team_member_person_role_id_fk FOREIGN KEY ( person_role_id ) REFERENCES tenant.person_role( id ) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMENT ON CONSTRAINT team_member_person_role_id_fk ON tenant.team_member IS 'A team member (person) assumes one role within a team. A person role can be assumed by zero or many team members.';

--rollback DROP SCHEMA IF EXISTS experiment CASCADE;
--rollback DROP SCHEMA IF EXISTS germplasm CASCADE;
--rollback DROP SCHEMA IF EXISTS tenant CASCADE;
--rollback DROP SCHEMA IF EXISTS place CASCADE;