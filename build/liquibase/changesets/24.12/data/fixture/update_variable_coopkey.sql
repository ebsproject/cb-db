--liquibase formatted sql

--changeset postgres:update_variable_coopkey context:fixture splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM master.variable WHERE abbrev = 'COOPKEY') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-4031 CB-DB: Help adding the 'COOPKEY' variable in the DEV database for testing Shipment Outgoing CIMMYT



UPDATE
    master.variable
SET
    usage = 'management',
    data_level = 'occurrence'
WHERE
    abbrev = 'COOPKEY';



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     usage = 'shipment',
--rollback     data_level = 'shipment'
--rollback WHERE
--rollback     abbrev = 'COOPKEY';
