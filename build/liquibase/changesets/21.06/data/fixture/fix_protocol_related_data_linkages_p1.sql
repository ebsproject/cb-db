--liquibase formatted sql

--changeset postgres:fix_protocol_related_data_linkages_p1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-520 Fix protocol related data linkages p1



--fix protocol code and name
WITH t1 AS (
    SELECT 
        id,
        protocol_code,
        protocol_name,
        protocol_type
    FROM 
        tenant.protocol 
    WHERE 
        protocol_code 
    IN  
        ('TRAIT_PROTOCOL_TMP_AF4BT','MANAGEMENT_PROTOCOL_TMP_AF4BT','PLANTING_PROTOCOL_TMP_AF4BT','HARVEST_PROTOCOL_TMP_AF4BT')
    ORDER BY id 
), t2 AS (
    SELECT 
        t.protocol_code old_protocol_code,
        t.protocol_name old_protocol_name,
        concat(UPPER(t.protocol_type),'_PROTOCOL_',ee.experiment_code) new_protocol_code,
        concat(initcap(t.protocol_type),' Protocol ',ee.experiment_code) new_protocol_name
    FROM 
        t1 t, experiment.experiment ee
    WHERE
        ee.experiment_name = 'BT1-15k-ENTRIES-AYT-2021-DS'
)
UPDATE 
    tenant.protocol
SET
    protocol_code=t2.new_protocol_code,
    protocol_name=t2.new_protocol_name FROM t2
WHERE
    protocol_code=t2.old_protocol_code
;

--add list data
INSERT INTO
    platform.list (
        abbrev, name, display_name, type, entity_id, list_usage, status, list_sub_type, creator_id
    )
SELECT
    concat(REPLACE(UPPER(t.type),' ','_'),'_',(ee.experiment_code)) abbrev,
    concat(ee.experiment_name,' ',initcap(t.type),' (', ee.experiment_code, ')') pname,
    concat(ee.experiment_name,' ',initcap(t.type),' (', ee.experiment_code, ')') display_name,
    t.type, 
    de.id entity_id, 
    t.list_usage, 
    t.status, 
    t.list_sub_type,
    tp.id creator_id
FROM
    (
        VALUES 
           ('trait protocol','TRAIT','working list','created',NULL),
           ('management protocol','MANAGEMENT','working list','created',NULL)

    ) AS t (type, entity_abbrev, list_usage, status, list_sub_type)
JOIN
    experiment.experiment ee
ON
    ee.experiment_name='BT1-15k-ENTRIES-AYT-2021-DS'
JOIN
    dictionary.entity de
ON
    de.abbrev = t.entity_abbrev
JOIN
    tenant.person tp
ON
    tp.username='nicola.costa'
;

--fix data value
WITH t1 AS (
    SELECT
        ee.id experiment_id,
        mv.id variable_id,
        pl.id data_value,
        tpr.id protocol_id
    FROM
        (
            VALUES
                ('TRAIT_PROTOCOL_LIST_ID','N'),
                ('MANAGEMENT_PROTOCOL_LIST_ID','N')
        ) AS t (variable_abbrev, data_qc_code)
    INNER JOIN
        experiment.experiment ee
    ON
        ee.experiment_name = 'BT1-15k-ENTRIES-AYT-2021-DS'
    INNER JOIN
        master.variable mv
    ON
        mv.abbrev=t.variable_abbrev
    INNER JOIN
        platform.list pl
    ON
        pl.abbrev = (SELECT concat(REPLACE(t.variable_abbrev,'_LIST_ID',''),'_',ee.experiment_code))
    INNER JOIN
        tenant.protocol tpr
    ON
        tpr.protocol_code = (SELECT concat(REPLACE(t.variable_abbrev,'_LIST_ID',''),'_',ee.experiment_code))
    INNER JOIN
        tenant.person psn
    ON
        psn.username = 'nicola.costa'
)
UPDATE
    experiment.experiment_data ed
SET
    data_value = t1.data_value FROM t1
WHERE
    ed.experiment_id = t1.experiment_id
AND
    ed.variable_id = t1.variable_id
AND
    ed.protocol_id = t1.protocol_id
;



--rollback WITH t1 AS (
--rollback 	SELECT
--rollback 		ee.id experiment_id,
--rollback 		mv.id variable_id,
--rollback 		t.data_value,
--rollback 		tpr.id protocol_id
--rollback 	FROM
--rollback 		(
--rollback 			VALUES
--rollback 				('TRAIT_PROTOCOL_LIST_ID','N','1393'),
--rollback 				('MANAGEMENT_PROTOCOL_LIST_ID','N','1394')
--rollback 		) AS t (variable_abbrev, data_qc_code, data_value)
--rollback 	INNER JOIN
--rollback 		experiment.experiment ee
--rollback 	ON
--rollback 		ee.experiment_name = 'BT1-15k-ENTRIES-AYT-2021-DS'
--rollback 	INNER JOIN
--rollback 		master.variable mv
--rollback 	ON
--rollback 		mv.abbrev=t.variable_abbrev
--rollback 	INNER JOIN
--rollback 		platform.list pl
--rollback 	ON
--rollback 		pl.abbrev = (SELECT concat(REPLACE(t.variable_abbrev,'_LIST_ID',''),'_',ee.experiment_code))
--rollback 	INNER JOIN
--rollback 		tenant.protocol tpr
--rollback 	ON
--rollback 		tpr.protocol_code = (SELECT concat(REPLACE(t.variable_abbrev,'_LIST_ID',''),'_',ee.experiment_code))
--rollback 	INNER JOIN
--rollback 		tenant.person psn
--rollback 	ON
--rollback 		psn.username = 'nicola.costa'
--rollback )
--rollback UPDATE
--rollback 	experiment.experiment_data ed
--rollback SET
--rollback 	data_value = t1.data_value FROM t1
--rollback WHERE
--rollback 	ed.experiment_id = t1.experiment_id
--rollback AND
--rollback 	ed.variable_id = t1.variable_id
--rollback AND
--rollback 	ed.protocol_id = t1.protocol_id
--rollback ;

--rollback DELETE FROM platform.list WHERE abbrev LIKE (SELECT concat('%',experiment_code,'%') FROM experiment.experiment WHERE experiment_name='BT1-15k-ENTRIES-AYT-2021-DS');

--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         id,
--rollback         protocol_code,
--rollback         protocol_name,
--rollback         protocol_type
--rollback     FROM 
--rollback         tenant.protocol 
--rollback     WHERE 
--rollback         protocol_code 
--rollback     LIKE 
--rollback         (SELECT concat('%',experiment_code,'%') FROM experiment.experiment WHERE experiment_name='BT1-15k-ENTRIES-AYT-2021-DS')
--rollback     ORDER BY id 
--rollback ), t2 AS (
--rollback     SELECT 
--rollback         t.protocol_code old_protocol_code,
--rollback         t.protocol_name old_protocol_name,
--rollback         concat(UPPER(t.protocol_type),'_PROTOCOL_','TMP_AF4BT') new_protocol_code,
--rollback         concat(initcap(t.protocol_type),' Protocol ','Tmp AF4BT') new_protocol_name
--rollback     FROM 
--rollback         t1 t, experiment.experiment ee
--rollback     WHERE
--rollback         ee.experiment_name = 'BT1-15k-ENTRIES-AYT-2021-DS'
--rollback )
--rollback UPDATE 
--rollback     tenant.protocol
--rollback SET
--rollback     protocol_code=t2.new_protocol_code,
--rollback     protocol_name=t2.new_protocol_name FROM t2
--rollback WHERE
--rollback     protocol_code=t2.old_protocol_code
--rollback ;