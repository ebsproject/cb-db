--liquibase formatted sql

--changeset postgres:update_geospatial_object_subtype_breeding_location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-537 Update geospatial_object_subtype from breeding_location to breeding location



UPDATE 
    place.geospatial_object
SET
    geospatial_object_subtype = 'breeding location'
WHERE
    geospatial_object_subtype = 'breeding_location'
;



--rollback UPDATE 
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'breeding_location'
--rollback WHERE
--rollback     geospatial_object_name   
--rollback IN 
--rollback     (
--rollback         'TEST_1',
--rollback         'TEST_2'
--rollback     )
--rollback ;