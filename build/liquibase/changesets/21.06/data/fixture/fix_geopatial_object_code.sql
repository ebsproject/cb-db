--liquibase formatted sql

--changeset postgres:fix_geopatial_object_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-516 Fix geospatial_object_code



UPDATE 
    place.geospatial_object 
SET
    geospatial_object_code = 'DB-516-IRRIHQ-2021-DS-003'
WHERE
    geospatial_object_name = 'TEST_1'
;

UPDATE 
    place.geospatial_object 
SET
    geospatial_object_code = 'DB-516-IRRIHQ-2021-DS-004'
WHERE
    geospatial_object_name = 'BT1-15k-ENTRIES-AYT-2021-DS-TEST1'
;

UPDATE 
    place.geospatial_object 
SET
    geospatial_object_code = 'DB-516-MM_NY-2021-DS-002'
WHERE
    geospatial_object_name = 'BT1-15k-ENTRIES-AYT-2021-DS-TEST2'
;

UPDATE 
    place.geospatial_object 
SET
    geospatial_object_code = 'DB-516-IRRIHQ-2021-DS-005'
WHERE
    geospatial_object_name = 'BT2-15k-ENTRIES-AYT-2021-DS-TEST1'
;

UPDATE 
    place.geospatial_object 
SET
    geospatial_object_code = 'DB-516-MM_NY-2021-DS-003'
WHERE
    geospatial_object_name = 'BT2-15k-ENTRIES-AYT-2021-DS-TEST2'
;



--rollback UPDATE 
--rollback     place.geospatial_object 
--rollback SET
--rollback     geospatial_object_code = 'IRRIHQ-2021-DS-5'
--rollback WHERE
--rollback     geospatial_object_name = 'TEST_1'
--rollback ;
--rollback
--rollback UPDATE 
--rollback     place.geospatial_object 
--rollback SET
--rollback     geospatial_object_code = 'IRRIHQ-2021-DS-006'
--rollback WHERE
--rollback     geospatial_object_name = 'BT1-15k-ENTRIES-AYT-2021-DS-TEST1'
--rollback ;
--rollback 
--rollback UPDATE 
--rollback     place.geospatial_object 
--rollback SET
--rollback     geospatial_object_code = 'MM_NY-2021-DS-002'
--rollback WHERE
--rollback     geospatial_object_name = 'BT1-15k-ENTRIES-AYT-2021-DS-TEST2'
--rollback ;
--rollback 
--rollback UPDATE 
--rollback     place.geospatial_object 
--rollback SET
--rollback     geospatial_object_code = 'IRRIHQ-2021-DS-007'
--rollback WHERE
--rollback     geospatial_object_name = 'BT2-15k-ENTRIES-AYT-2021-DS-TEST1'
--rollback ;
--rollback 
--rollback 
--rollback UPDATE 
--rollback     place.geospatial_object 
--rollback SET
--rollback     geospatial_object_code = 'MM_NY-2021-DS-003'
--rollback WHERE
--rollback     geospatial_object_name = 'BT2-15k-ENTRIES-AYT-2021-DS-TEST2'
--rollback ;