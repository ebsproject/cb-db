--liquibase formatted sql

--changeset postgres:update_harvest_status_of_crosses_to_ready context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-545 Update harvest_status of crosses to ready



WITH t1 AS (
    SELECT
        crs.id, 
        crs.harvest_status
    FROM 
        germplasm.cross AS crs
    JOIN 
        experiment.experiment ee
    ON
        ee.id = crs.experiment_id
    WHERE
        ee.experiment_name IN (
            'KE-HB-2019-A-001',
            'KE-HB-2019-A-002',
            'KE-HB-2019-A-003'
        )
    ORDER BY
        crs.id
)
UPDATE 
    germplasm.cross AS gc
SET
    harvest_status = 'READY' FROM t1
WHERE
    gc.id = t1.id
;



--rollback WITH t1 AS (
--rollback 	SELECT
--rollback 		crs.id, 
--rollback 		crs.harvest_status
--rollback 	FROM 
--rollback 		germplasm.cross AS crs
--rollback 	JOIN 
--rollback 		experiment.experiment ee
--rollback 	ON
--rollback 		ee.id = crs.experiment_id
--rollback 	WHERE
--rollback 		ee.experiment_name IN (
--rollback 			'KE-HB-2019-A-001',
--rollback 			'KE-HB-2019-A-002',
--rollback 			'KE-HB-2019-A-003'
--rollback 		)
--rollback 	ORDER BY
--rollback 		crs.id
--rollback )
--rollback UPDATE 
--rollback     germplasm.cross AS gc
--rollback SET
--rollback     harvest_status = 'NO_HARVEST' FROM t1
--rollback WHERE
--rollback     gc.id = t1.id
--rollback ;