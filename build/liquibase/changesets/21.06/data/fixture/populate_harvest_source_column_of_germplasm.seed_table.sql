--liquibase formatted sql

--changeset postgres:populate_harvest_source_column_of_germplasm.seed_table context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-490 Populate harvest_source column of germplasm.seed table



-- update harvest_source of records that have an existing relation to a plot and NOT related to HB experiments to 'plot'
UPDATE
    germplasm.seed AS sd
SET
    harvest_source = 'plot'
FROM
    experiment.experiment AS ex
WHERE
	sd.source_experiment_id = ex.id
AND
	ex.experiment_name NOT ILIKE '%HB%'
AND
    source_plot_id IS NOT NULL
;


-- update harvest_source of records that have an existing relation to a plot and IS related to HB experiments to 'cross'
UPDATE
    germplasm.seed AS sd
SET
    harvest_source = 'cross'
FROM
    experiment.experiment AS ex
WHERE
    sd.source_experiment_id = ex.id
AND
    ex.experiment_name ILIKE '%HB%'
AND
    source_plot_id IS NOT NULL
;



-- revert changes
--rollback UPDATE germplasm.seed set harvest_source = NULL WHERE harvest_source IN ('plot', 'cross');