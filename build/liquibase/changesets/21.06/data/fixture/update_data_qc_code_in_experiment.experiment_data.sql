--liquibase formatted sql

--changeset postgres:update_data_qc_code_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-476 Update data_qc_code in experiment.experiment_data



-- update data_qc_code
UPDATE
    experiment.experiment_data
SET
    data_qc_code = 'Q'
WHERE
    data_qc_code = 'N'
;


-- revert changes
--rollback UPDATE
--rollback     experiment.experiment_data
--rollback SET
--rollback     data_qc_code = 'N'
--rollback WHERE
--rollback     id IN (
--rollback         196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215,
--rollback         216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235,
--rollback         236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
--rollback         256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 271, 272, 273, 274, 275, 276, 277,
--rollback         278, 279, 280, 281, 282, 283, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 269, 270,
--rollback         284, 285, 298, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 299, 300, 313, 316, 317,
--rollback         318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 314, 315
--rollback     )
--rollback ;
