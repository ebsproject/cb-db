--liquibase formatted sql

--changeset postgres:insert_5000_trial_experiments context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 5000 trial experiments



-- insert into experiment.experiment: 5000 experiments = 8 seasons from 2018DS to 2021WS x 625 trial experiments per breeding season
INSERT INTO
    experiment.experiment (
        program_id,
        pipeline_id,
        stage_id,
        project_id,
        experiment_year,
        season_id,
        planting_season,
        experiment_code,
        experiment_name,
        experiment_type,
        experiment_sub_type,
        experiment_sub_sub_type,
        experiment_design_type,
        experiment_status,
        steward_id,
        crop_id,
        data_process_id,
        creator_id,
        description,
        notes
    )
SELECT
    prog.id AS program_id,
    pipe.id AS pipeline_id,
    stg.id AS stage_id,
    proj.id AS project_id,
    expt.experiment_year::integer AS experiment_year,
    ssn.id AS season_id,
    (expt.experiment_year || ssn.season_code) AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    concat_ws('-', prog.program_code, stg.stage_code, expt.experiment_year, ssn.season_code, lpad(gs.experiment_number::text, 3, '0')) AS experiment_name,
    expt.experiment_type AS experiment_type,
    expt.experiment_sub_type AS experiment_sub_type,
    expt.experiment_sub_sub_type AS experiment_sub_sub_type,
    expt.experiment_design_type AS experiment_design_type,
    expt.experiment_status AS experiment_status,
    stwd.id AS steward_id,
    crop.id AS crop_id,
    dproc.id AS data_process_id,
    crtr.id AS creator_id,
    (expt.notes || ' ' || expt.description) AS description,
    expt.notes AS notes
FROM
    generate_series(1, 625) AS gs (experiment_number),
    (
        VALUES
        ('IRSEA', NULL, 'OYT', NULL, '2018', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'PYT', NULL, '2018', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'AYT', NULL, '2019', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'a.carlson', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'a.carlson', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'OYT', NULL, '2019', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'PYT', NULL, '2020', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'AYT', NULL, '2020', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'h.tamsin', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'h.tamsin', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'OYT', NULL, '2021', 'DS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'k.khadija', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'k.khadija', 'Create 5k trial experiments', 'DB-478'),
        ('IRSEA', NULL, 'PYT', NULL, '2021', 'WS', NULL, 'Breeding Trial', NULL, NULL, 'RCBD', 'planted', 'k.khadija', 'RICE', 'BREEDING_TRIAL_DATA_PROCESS', 'k.khadija', 'Create 5k trial experiments', 'DB-478')
    ) AS expt (
        program,
        pipeline,
        stage,
        project,
        experiment_year,
        season,
        planting_season,
        experiment_type,
        experiment_sub_type,
        experiment_sub_sub_type,
        experiment_design_type,
        experiment_status,
        steward,
        crop,
        data_process,
        creator,
        description,
        notes
    )
    JOIN tenant.program AS prog
        ON prog.program_code = expt.program
    LEFT JOIN tenant.pipeline AS pipe
        ON pipe.pipeline_code = expt.pipeline
    JOIN tenant.stage AS stg
        ON stg.stage_code = expt.stage
    LEFT JOIN tenant.project AS proj
        ON proj.project_code = expt.project
    JOIN tenant.season AS ssn
        ON ssn.season_code = expt.season
    JOIN tenant.person AS stwd
        ON stwd.username = expt.steward
    JOIN tenant.crop AS crop
        ON crop.crop_code = expt.crop
    JOIN master.item AS dproc
        ON dproc.abbrev = expt.data_process
    JOIN tenant.person AS crtr
        ON crtr.username = expt.creator
ORDER BY
    expt.experiment_year,
    expt.season,
    gs.experiment_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.notes LIKE 'DB-478%'
--rollback ;



--changeset postgres:insert_20000_protocols context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 20000 protocols


-- insert into tenant.protocol: 20000 protocols = 5000 experiments x 4 protocols per experiment
INSERT INTO
    tenant.protocol (
        protocol_code,
        protocol_name,
        protocol_type,
        program_id,
        creator_id
    )
SELECT
    (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
    (protocol_name_prefix || ' ' ||  initcap(expt.experiment_code)) AS protocol_name,
    prot.protocol_type,
    expt.program_id,
    expt.creator_id
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'Trait Protocol', 'trait'),
        ('MANAGEMENT_PROTOCOL', 'Management Protocol', 'management'),
        ('PLANTING_PROTOCOL', 'Planting Protocol', 'planting'),
        ('HARVEST_PROTOCOL', 'Harvest Protocol', 'harvest')
    ) AS prot (
        protocol_code_prefix,
        protocol_name_prefix,
        protocol_type
    ),
    experiment.experiment AS expt
WHERE
    expt.notes LIKE 'DB-478%'
ORDER BY
    expt.id
;



-- revert changes
--rollback DELETE FROM
--rollback     tenant.protocol AS prot
--rollback USING (
--rollback         SELECT
--rollback             (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL'),
--rollback                 ('PLANTING_PROTOCOL'),
--rollback                 ('HARVEST_PROTOCOL')
--rollback             ) AS prot (
--rollback                 protocol_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-478%'
--rollback     ) AS t
--rollback WHERE
--rollback     t.protocol_code = prot.protocol_code
--rollback ;



--changeset postgres:insert_65000_experiment_data_part_1 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 65000 experiment data - part 1



-- insert into experiment.experiment_data - part 1: 65000 experiment data = 5000 experiments x 13 metadata variables per experiment
INSERT INTO
    experiment.experiment_data (
        experiment_id,
        variable_id,
        data_value,
        data_qc_code,
        protocol_id,
        creator_id
    )
SELECT
    expt.id AS experiment_id,
    var.id AS variable_id,
    exptdata.data_value AS data_value,
    exptdata.data_qc_code AS data_qc_code,
    prot.id AS protocol_id,
    expt.creator_id AS creator_id
FROM
    (
        VALUES
        ('ESTABLISHMENT', 'transplanted', 'N', 'PLANTING_PROTOCOL'),
        ('PLANTING_TYPE', 'Flat', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_TYPE', '6R', 'N', 'PLANTING_PROTOCOL'),
        ('ROWS_PER_PLOT_CONT', '6', 'N', 'PLANTING_PROTOCOL'),
        ('DIST_BET_ROWS', '20', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_WIDTH', '1.2', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_LN', '20', 'N', 'PLANTING_PROTOCOL'),
        ('PLOT_AREA_2', '24', 'N', 'PLANTING_PROTOCOL'),
        ('ALLEY_LENGTH', '1', 'N', 'PLANTING_PROTOCOL'),
        ('SEEDING_RATE', 'Normal', 'N', 'PLANTING_PROTOCOL'),
        ('HV_METH_DISC', 'Bulk', 'N', 'HARVEST_PROTOCOL'),
        ('FIRST_PLOT_POSITION_VIEW', 'Top Left', 'N', NULL),
        ('PROTOCOL_TARGET_LEVEL', 'occurrence', 'N', NULL)
    ) AS exptdata (
        variable,
        data_value,
        data_qc_code,
        protocol_code_prefix
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-478%'
    JOIN master.variable AS var
        ON var.abbrev = exptdata.variable
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = (exptdata.protocol_code_prefix || '_' || expt.experiment_code)
ORDER BY
    expt.id,
    prot.id,
    var.id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment_data AS exptdata
--rollback USING
--rollback     experiment.experiment AS expt,
--rollback     master.variable AS var
--rollback WHERE
--rollback     expt.id = exptdata.experiment_id
--rollback     AND var.id = exptdata.variable_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback     AND var.abbrev IN (
--rollback         'ESTABLISHMENT',
--rollback         'PLANTING_TYPE',
--rollback         'PLOT_TYPE',
--rollback         'ROWS_PER_PLOT_CONT',
--rollback         'DIST_BET_ROWS',
--rollback         'PLOT_WIDTH',
--rollback         'PLOT_LN',
--rollback         'PLOT_AREA_2',
--rollback         'ALLEY_LENGTH',
--rollback         'SEEDING_RATE',
--rollback         'HV_METH_DISC',
--rollback         'FIRST_PLOT_POSITION_VIEW',
--rollback         'PROTOCOL_TARGET_LEVEL'
--rollback     )
--rollback ;



--changeset postgres:insert_10000_experiment_protocol_lists context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 10000 experiment protocol lists



-- insert into platform.list: 10000 experiment protocol lists = 5000 experiments x 2 protocol lists per experiment
INSERT INTO
    platform.list (
        abbrev,
        name,
        display_name,
        type,
        entity_id,
        creator_id,
        list_usage,
        status,
        is_active
    )
SELECT
    (list.list_code_prefix || '_' || expt.experiment_code) AS abbrev,
    (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS name,
    (expt.experiment_name || ' ' || list.list_name_prefix || ' (' || expt.experiment_code || ')') AS display_name,
    list.list_type AS type,
    entity.id AS entity_id,
    expt.creator_id,
    'working list' AS list_usage,
    'created' AS status,
    TRUE AS is_active
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'Trait Protocol', 'trait protocol', 'TRAIT'),
        ('MANAGEMENT_PROTOCOL', 'Management Protocol', 'management protocol', 'MANAGEMENT')
    ) AS list (
        list_code_prefix,
        list_name_prefix,
        list_type,
        entity
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-478%'
    JOIN dictionary.entity AS entity
        ON entity.abbrev = list.entity
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.list AS list
--rollback USING (
--rollback         SELECT
--rollback             (list_code_prefix || '_' || expt.experiment_code) AS list_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL')
--rollback             ) AS list (
--rollback                 list_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-478%'
--rollback     ) AS t
--rollback WHERE
--rollback     t.list_code = list.abbrev
--rollback ;



--changeset postgres:insert_115000_experiment_protocol_list_variables context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 115000 experiment protocol list variables



-- insert into platform.list_member: 115000 experiment protocol list variables = 5000 experiments x (10 variables for trait list protocol + 13 variables for management list protocol)
INSERT INTO
    platform.list_member (
        list_id,
        data_id,
        order_number,
        display_value,
        is_active,
        creator_id
    )
WITH t_list AS (
    SELECT
        (list_code_prefix || '_' || expt.experiment_code) AS list_code
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL')
        ) AS list (
            list_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-478%'
)
SELECT
    list.id AS list_id,
    var.id AS data_id,
    ROW_NUMBER() OVER (PARTITION BY list.id, listmem.list_type) AS order_number,
    var.label AS display_value,
    TRUE AS is_active,
    list.creator_id
FROM (
        VALUES
        ('TRAIT_PROTOCOL', 'AYLD_CONT'),
        ('TRAIT_PROTOCOL', 'BB_SES5_GH_SCOR_1_9'),
        ('TRAIT_PROTOCOL', 'FLW_DATE_CONT'),
        ('TRAIT_PROTOCOL', 'HT1_CONT'),
        ('TRAIT_PROTOCOL', 'HT2_CONT'),
        ('TRAIT_PROTOCOL', 'HT3_CONT'),
        ('TRAIT_PROTOCOL', 'HT_CAT'),
        ('TRAIT_PROTOCOL', 'HVDATE_CONT'),
        ('TRAIT_PROTOCOL', 'LG_SCOR_1_9'),
        ('TRAIT_PROTOCOL', 'MC_CONT'),
        ('MANAGEMENT_PROTOCOL', 'DIST_BET_ROWS'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_METH_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT1_TYPE_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT2_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT2_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_BRAND'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_DATE_CONT'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_METH_DISC'),
        ('MANAGEMENT_PROTOCOL', 'FERT3_TYPE_DISC'),
        ('MANAGEMENT_PROTOCOL', 'ROWS_PER_PLOT_CONT'),
        ('MANAGEMENT_PROTOCOL', 'SEEDING_DATE_CONT')
    ) AS listmem (
        list_type, variable
    )
    JOIN t_list AS t
        ON t.list_code LIKE listmem.list_type || '%'
    JOIN platform.list AS list
        ON list.abbrev = t.list_code
    JOIN master.variable AS var
        ON var.abbrev = listmem.variable
ORDER BY
    list.id,
    order_number
;



-- revert changes
--rollback DELETE FROM
--rollback     platform.list_member AS listmem
--rollback USING
--rollback     (
--rollback         SELECT
--rollback             (list_code_prefix || '_' || expt.experiment_code) AS list_code
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL')
--rollback             ) AS list (
--rollback                 list_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-478%'
--rollback     ) AS t
--rollback     JOIN platform.list AS list
--rollback         ON t.list_code = list.abbrev
--rollback WHERE
--rollback     list.id = listmem.list_id
--rollback ;



--changeset postgres:insert_10000_experiment_data_part_2 context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 10000 experiment data - part 2



-- insert into experiment.experiment_data - part 2: 10000 experiment protocol lists = 5000 experiments x 2 protocol lists per experiment
INSERT INTO
    experiment.experiment_data (
        experiment_id,
        variable_id,
        data_value,
        data_qc_code,
        protocol_id,
        creator_id
    )
WITH t_list AS (
    SELECT
        (list_code_prefix || '_' || expt.experiment_code) AS list_code
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL')
        ) AS list (
            list_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-478%'
)
SELECT
    expt.id AS experiment_id,
    var.id AS variable_id,
    list.id::varchar AS data_value,
    exptdata.data_qc_code AS data_qc_code,
    prot.id AS protocol_id,
    expt.creator_id AS creator_id
FROM
    (
        VALUES
        ('TRAIT_PROTOCOL_LIST_ID', NULL, 'N', 'TRAIT_PROTOCOL'),
        ('MANAGEMENT_PROTOCOL_LIST_ID', NULL, 'N', 'MANAGEMENT_PROTOCOL')
    ) AS exptdata (
        variable,
        data_value,
        data_qc_code,
        protocol_code_prefix
    )
    JOIN experiment.experiment AS expt
        ON expt.notes LIKE 'DB-478%'
    JOIN t_list AS t
        ON t.list_code = (exptdata.protocol_code_prefix || '_' || expt.experiment_code)
    JOIN master.variable AS var
        ON var.abbrev = exptdata.variable
    LEFT JOIN tenant.protocol AS prot
        ON prot.protocol_code = t.list_code
    JOIN platform.list AS list
        ON list.abbrev = t.list_code
ORDER BY
    expt.id,
    prot.id,
    var.id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment_data AS exptdata
--rollback USING
--rollback     experiment.experiment AS expt,
--rollback     master.variable AS var
--rollback WHERE
--rollback     expt.id = exptdata.experiment_id
--rollback     AND var.id = exptdata.variable_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback     AND var.abbrev IN (
--rollback         'TRAIT_PROTOCOL_LIST_ID',
--rollback         'MANAGEMENT_PROTOCOL_LIST_ID'
--rollback     )
--rollback ;



--changeset postgres:insert_20000_experiment_protocols context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 20000 experiment protocols



-- insert into experiment.experiment_protocol: 20000 experiment protocols = 5000 experiments x 4 protocols per experiment
INSERT INTO
    experiment.experiment_protocol (
        experiment_id,
        protocol_id,
        order_number,
        creator_id
    )
WITH t_protocol AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_code,
        expt.experiment_name,
        (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
        expt.creator_id
    FROM (
            VALUES
            ('TRAIT_PROTOCOL'),
            ('MANAGEMENT_PROTOCOL'),
            ('PLANTING_PROTOCOL'),
            ('HARVEST_PROTOCOL')
        ) AS prot (
            protocol_code_prefix
        )
        JOIN experiment.experiment AS expt
            ON expt.notes LIKE 'DB-478%'
)
SELECT
    t.experiment_id,
    prot.id AS protocol_id,
    ROW_NUMBER() OVER (PARTITION BY t.experiment_id) AS order_number,
    t.creator_id
FROM
    t_protocol AS t
    JOIN tenant.protocol AS prot
        ON t.protocol_code = prot.protocol_code
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment_protocol AS exptprot
--rollback USING (
--rollback         SELECT
--rollback             expt.id AS experiment_id,
--rollback             expt.experiment_code,
--rollback             expt.experiment_name,
--rollback             (protocol_code_prefix || '_' || expt.experiment_code) AS protocol_code,
--rollback             expt.creator_id
--rollback         FROM (
--rollback                 VALUES
--rollback                 ('TRAIT_PROTOCOL'),
--rollback                 ('MANAGEMENT_PROTOCOL'),
--rollback                 ('PLANTING_PROTOCOL'),
--rollback                 ('HARVEST_PROTOCOL')
--rollback             ) AS prot (
--rollback                 protocol_code_prefix
--rollback             )
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.notes LIKE 'DB-478%'
--rollback     ) AS t
--rollback     JOIN tenant.protocol AS prot
--rollback         ON t.protocol_code = prot.protocol_code
--rollback WHERE
--rollback     exptprot.protocol_id = prot.id
--rollback     AND t.experiment_id = exptprot.experiment_id
--rollback ;



--changeset postgres:insert_5000_entry_lists context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 5000 entry lists



-- insert into experiment.entry_list: 5000 entry lists = 5000 experiments x 1 entry list per experiment
INSERT INTO
    experiment.entry_list (
        entry_list_code,
        entry_list_name,
        entry_list_status,
        entry_list_type,
        experiment_id,
        creator_id
    )
SELECT
    (expt.experiment_code || '_ENTLIST') AS entry_list_code,
    (expt.experiment_name || ' Entry List') AS entry_list_name,
    'completed' AS entry_list_status,
    'entry list' AS entry_list_type,
    expt.id AS experiment_id,
    expt.creator_id AS creator_id
FROM
    experiment.experiment AS expt
WHERE
    expt.notes LIKE 'DB-478%'
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list AS entlist
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.id = entlist.experiment_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback ;



--changeset postgres:insert_500000_entries context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 500000 entries



-- insert into experiment.entry: 500000 entries = 5000 experiments x 100 entries per experiment

-- disable trigger and constraints to speed up data insertions
--ALTER TABLE experiment.entry DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- prepare statement to copy entries from another experiment
PREPARE
    copy_entries(
        varchar, varchar
        -- target_experiment_name_fragment, source_experiment_name
    )
AS
    INSERT INTO
        experiment.entry (
            entry_code,
            entry_number,
            entry_name,
            entry_type,
            entry_role,
            entry_class,
            entry_status,
            entry_list_id,
            germplasm_id,
            seed_id,
            package_id,
            creator_id
        )
    SELECT
        srcent.entry_number AS entry_code,
        srcent.entry_number,
        srcent.entry_name,
        srcent.entry_type,
        srcent.entry_role,
        srcent.entry_class,
        srcent.entry_status,
        entlist.id AS entry_list_id,
        srcent.germplasm_id,
        srcent.seed_id,
        srcent.package_id,
        expt.creator_id
    FROM
        (
            VALUES
            ($1, $2)
        ) AS t (
            target_experiment_name_fragment, source_experiment_name
        )
        JOIN experiment.experiment AS expt
            ON expt.experiment_name LIKE t.target_experiment_name_fragment || '%'
        JOIN experiment.entry_list AS entlist
            ON entlist.experiment_id = expt.id
        JOIN experiment.experiment AS srcexpt
            ON srcexpt.experiment_name = t.source_experiment_name
        JOIN experiment.entry_list AS srcentlist
            ON srcentlist.experiment_id = srcexpt.id
        JOIN experiment.entry AS srcent
            ON srcent.entry_list_id = srcentlist.id
    WHERE
        srcent.entry_number BETWEEN 1 AND 100
        AND expt.notes LIKE 'DB-478%'
    ORDER BY
        entlist.id,
        srcent.entry_number
;

-- copy entries for batch 1
EXECUTE copy_entries('IRSEA-OYT-2018-DS', 'IRSEA-OYT-2014-DS-001');
EXECUTE copy_entries('IRSEA-PYT-2018-WS', 'IRSEA-OYT-2014-DS-001');
EXECUTE copy_entries('IRSEA-AYT-2019-DS', 'IRSEA-OYT-2014-DS-001');

-- copy entries for batch 2
EXECUTE copy_entries('IRSEA-OYT-2019-WS', 'IRSEA-OYT-2016-DS-001');
EXECUTE copy_entries('IRSEA-PYT-2020-DS', 'IRSEA-OYT-2016-DS-001');
EXECUTE copy_entries('IRSEA-AYT-2020-WS', 'IRSEA-OYT-2016-DS-001');

-- copy entries for batch 3
EXECUTE copy_entries('IRSEA-OYT-2021-DS', 'IRSEA-OYT-2016-DS-003');
EXECUTE copy_entries('IRSEA-PYT-2021-WS', 'IRSEA-OYT-2016-DS-003');

-- restore triggers and constraints
--ALTER TABLE experiment.entry ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- drop prepared statement
DEALLOCATE copy_entries;



-- revert changes
--rollback --ALTER TABLE experiment.entry DISABLE TRIGGER ALL;
--rollback
--rollback DELETE FROM
--rollback     experiment.entry AS ent
--rollback USING
--rollback     experiment.entry_list AS entlist
--rollback     JOIN experiment.experiment AS expt
--rollback         ON expt.id = entlist.experiment_id
--rollback WHERE
--rollback     entlist.id = ent.entry_list_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback ;
--rollback
--rollback --ALTER TABLE experiment.entry ENABLE TRIGGER ALL;



--changeset postgres:insert_25000_occurrences context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 25000 occurrences



-- insert into experiment.occurrence: 250000 occurrences = 5000 experiments x 5 occurrences per experiment
INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
SELECT
    expt.experiment_code || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_code,
    expt.experiment_name || '-' || lpad(geo.occurrence_number::text, 3, '0') AS occurrence_name,
    occ.occurrence_status,
    expt.id AS experiment_id,
    site.id AS site_id,
    field.id AS field_id,
    occ.rep_count,
    geo.occurrence_number,
    expt.creator_id
FROM
    experiment.experiment AS expt,
    (
        VALUES
        -- a.carlson
        ('IRSEA-OYT-2018-DS', 'planted', 3),
        ('IRSEA-PYT-2018-WS', 'planted', 3),
        ('IRSEA-AYT-2019-DS', 'planted', 3),
        -- h.tamsin
        ('IRSEA-OYT-2019-WS', 'planted', 3),
        ('IRSEA-PYT-2020-DS', 'planted', 3),
        ('IRSEA-AYT-2020-WS', 'planted', 3),
        -- k.khadija
        ('IRSEA-OYT-2021-DS', 'planted', 3),
        ('IRSEA-PYT-2021-WS', 'planted', 3)
    ) AS occ (
        experiment_name_fragment,
        occurrence_status,
        rep_count
    ),
    (
        VALUES
        (1, 'IRRIHQ', 'UB1'),
        (2, 'BD_DA_GZ', 'BRRI_1'),
        (3, 'IN_TG_HY', 'HYDERABAD_FARM'),
        (4, 'MM_NY', 'YEZIN'),
        (5, 'LK_KT', 'BOMBUWELA_FARM')
    ) AS geo (
        occurrence_number, site, field
    )
    JOIN place.geospatial_object AS site
        ON site.geospatial_object_code = geo.site
    JOIN place.geospatial_object AS field
        ON field.geospatial_object_code = geo.field
WHERE
    expt.notes LIKE 'DB-478%'
    AND expt.experiment_name LIKE occ.experiment_name_fragment || '%'
ORDER BY
    expt.id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence AS occ
--rollback USING
--rollback     experiment.experiment AS expt
--rollback WHERE
--rollback     expt.id = occ.experiment_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback ;



--changeset postgres:insert_5000_planting_areas context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 5000 planting areas



-- insert into place.geospatial_object: 5000 planting areas = 125 locations per breeding season x 5 occurrences per location x 8 breeding seasons
INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        creator_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-478%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.location_occurrence_group_number % 125 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_pa AS (
    SELECT
        concat_ws('-', 'DB-478', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 3, '0')) AS geospatial_object_code,
        concat_ws('-', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 3, '0')) AS geospatial_object_name,
        'planting area' AS geospatial_object_type,
        'breeding location' AS geospatial_object_subtype,
        t.site_parent_id AS parent_geospatial_object_id,
        t.site_root_id AS root_geospatial_object_id,
        t.creator_id
    FROM
        t_loc2 AS t
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id,
        t.location_number
)
SELECT
    t.*
FROM
    t_pa AS t
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object AS geo
--rollback WHERE
--rollback     geo.geospatial_object_code LIKE ANY((
--rollback         SELECT
--rollback             array_agg(DISTINCT (concat_ws('-', 'DB-478', site.geospatial_object_code, expt.experiment_year, ssn.season_code) || '-%')::varchar) AS geospatial_object_code_fragments
--rollback         FROM
--rollback             experiment.occurrence AS occ
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.id = occ.experiment_id
--rollback             JOIN tenant.season AS ssn
--rollback                 ON ssn.id = expt.season_id
--rollback             JOIN place.geospatial_object AS site
--rollback                 ON site.id = occ.site_id
--rollback         WHERE
--rollback             expt.notes LIKE 'DB-478%'
--rollback     )::varchar[])
--rollback ;



--changeset postgres:insert_5000_locations context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 5000 locations



-- insert into experiment.location: 5000 locations = 125 locations per breeding season x 5 occurrences per location x 8 breeding seasons
INSERT INTO
    experiment.location (
        location_code,
        location_name,
        location_status,
        location_type,
        location_year,
        season_id,
        location_number,
        site_id,
        field_id,
        steward_id,
        creator_id,
        geospatial_object_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-478%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.location_occurrence_group_number % 125 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_loc3 AS (
    SELECT
        concat_ws('-', 'DB-478', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 3, '0')) AS location_code,
        concat_ws('-', t.site_code, t.experiment_year, t.season_code, lpad(t.location_number::TEXT, 3, '0')) AS location_name,
        'planted' AS location_status,
        'planting area' AS location_type,
        t.experiment_year AS location_year,
        t.season_id,
        t.location_number,
        t.site_id,
        t.field_id,
        t.creator_id AS steward_id,
        t.creator_id
    FROM
        t_loc2 AS t
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id,
        t.location_number
), t_loc4 AS (
    SELECT
        t.*,
        geo.id AS geospatial_object_id
    FROM
        t_loc3 AS t
        JOIN place.geospatial_object AS geo
            ON geo.geospatial_object_code = t.location_code
    ORDER BY
        t.location_year,
        t.season_id,
        t.site_id,
        t.location_number
)
SELECT
    t.*
FROM
    t_loc4 AS t
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     loc.location_code LIKE ANY((
--rollback         SELECT
--rollback             array_agg(DISTINCT (concat_ws('-', 'DB-478', site.geospatial_object_code, expt.experiment_year, ssn.season_code) || '-%')::varchar) AS location_code_fragments
--rollback         FROM
--rollback             experiment.occurrence AS occ
--rollback             JOIN experiment.experiment AS expt
--rollback                 ON expt.id = occ.experiment_id
--rollback             JOIN tenant.season AS ssn
--rollback                 ON ssn.id = expt.season_id
--rollback             JOIN place.geospatial_object AS site
--rollback                 ON site.id = occ.site_id
--rollback         WHERE
--rollback             expt.notes LIKE 'DB-478%'
--rollback     )::varchar[])
--rollback ;



--changeset postgres:insert_25000_location_occurrence_groups context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 25000 location-occurrence groups



-- insert into experiment.location_occurrence_group: 25000 location-occurence groups = 5000 locations x 5 occurrences per location
INSERT INTO
    experiment.location_occurrence_group (
        location_id,
        occurrence_id,
        order_number,
        creator_id
    )
WITH t_occ AS (
    SELECT
        expt.id AS experiment_id,
        expt.experiment_year,
        expt.season_id,
        ssn.season_code,
        occ.id AS occurrence_id,
        occ.occurrence_name,
        occ.site_id,
        occ.field_id,
        site.geospatial_object_code AS site_code,
        site.parent_geospatial_object_id AS site_parent_id,
        site.root_geospatial_object_id AS site_root_id,
        occ.creator_id
    FROM
        experiment.occurrence AS occ
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
        JOIN tenant.season AS ssn
            ON ssn.id = expt.season_id
        JOIN place.geospatial_object AS site
            ON site.id = occ.site_id
    WHERE
        expt.notes LIKE 'DB-478%'
    ORDER BY
        expt.id,
        occ.id
), t_logrp AS (
    SELECT
        occ.*,
        NTILE(5000) OVER (ORDER BY occ.experiment_year, occ.season_id, occ.site_id) AS location_occurrence_group_number -- total number of planting areas/locations
    FROM
        t_occ AS occ
    ORDER BY
        location_occurrence_group_number
), t_loc1 AS (
    SELECT
        t.location_occurrence_group_number,
        array_agg(t.occurrence_id) AS occurrence_id_list,
        (array_agg(DISTINCT t.experiment_year))[1] AS experiment_year,
        (array_agg(DISTINCT t.season_id))[1] AS season_id,
        (array_agg(DISTINCT t.season_code))[1] AS season_code,
        (array_agg(DISTINCT t.site_id))[1] AS site_id,
        (array_agg(DISTINCT t.site_code))[1] AS site_code,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_logrp AS t
    GROUP BY
        t.location_occurrence_group_number
), t_loc2 AS (
    SELECT
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.occurrence_id_list,
        ROW_NUMBER() OVER (PARTITION BY t.experiment_year, t.season_code, t.site_code) AS location_number,
        (array_agg(DISTINCT t.field_id))[1] AS field_id,
        (array_agg(DISTINCT t.site_parent_id))[1] AS site_parent_id,
        (array_agg(DISTINCT t.site_root_id))[1] AS site_root_id,
        (array_agg(DISTINCT t.creator_id))[1] AS creator_id
    FROM
        t_loc1 AS t
    GROUP BY
        t.experiment_year,
        t.season_id,
        t.season_code,
        t.site_id,
        t.site_code,
        t.occurrence_id_list,
        t.location_occurrence_group_number % 125 -- number of locations per breeding season
    ORDER BY
        t.experiment_year,
        t.season_id,
        t.site_id
), t_loc AS (
    SELECT
        loc.id AS location_id,
        occ.id AS occurrence_id,
        ROW_NUMBER() OVER (PARTITION BY loc.id ORDER BY occ.id) AS order_number,
        loc.creator_id
    FROM
        experiment.location AS loc
        JOIN t_loc2 AS t
            ON t.experiment_year = loc.location_year
            AND t.season_id = loc.season_id
            AND t.site_id = loc.site_id
            AND t.field_id = loc.field_id
            AND t.location_number = loc.location_number,
        UNNEST(t.occurrence_id_list) AS occ (id)
    WHERE
        loc.location_code LIKE 'DB-478%'
    ORDER BY
        loc.location_year,
        loc.season_id,
        loc.site_id,
        loc.location_number
)
SELECT
    t.*
FROM
    t_loc AS t
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location_occurrence_group AS logrp
--rollback USING
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     loc.id = logrp.location_id
--rollback     AND loc.location_code LIKE 'DB-478%'
--rollback ;



--changeset postgres:insert_7500000_plots context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 7500000 plots



-- insert into experiment.plot: 7500000 plots = 5000 experiments x 100 entries per experiment x 5 occurrences per experiment x 3 replicates per entry

-- prepare statement to populate plots and randomize design, planting area, and field coordinates
PREPARE
    populate_plots(
        integer, integer, integer, integer
        -- location_id, occurrence_count, x_dimension, y_dimension
    )
AS
    INSERT INTO
        experiment.plot (
            occurrence_id,
            location_id,
            entry_id,
            plot_code,
            plot_number,
            plot_type,
            rep,
            design_x,
            design_y,
            pa_x,
            pa_y,
            field_x,
            field_y,
            block_number,
            plot_qc_code,
            plot_status,
            harvest_status,
            creator_id
        )
    WITH t_plot1 AS (
        SELECT
            ROW_NUMBER() OVER () AS layout_order_number,
            logrp.occurrence_id,
            logrp.location_id,
            ent.id AS entry_id,
            ROW_NUMBER() OVER (PARTITION BY logrp.location_id, logrp.occurrence_id ORDER BY logrp.location_id, logrp.occurrence_id) AS plot_number,
            'plot' AS plot_type,
            gs.rep,
            logrp.order_number AS block_number,
            'Q' AS plot_qc_code,
            'active' AS plot_status,
            'NO_HARVEST' AS harvest_status,
            logrp.creator_id,
            logrp.order_number
        FROM
            experiment.location AS loc
            JOIN experiment.location_occurrence_group AS logrp
                ON logrp.location_id = loc.id
            JOIN experiment.occurrence AS occ
                ON occ.id = logrp.occurrence_id
            JOIN experiment.experiment AS expt
                ON expt.id = occ.experiment_id
            JOIN experiment.entry_list AS entlist
                ON entlist.experiment_id = expt.id
            JOIN experiment.entry AS ent
                ON ent.entry_list_id = entlist.id,
            generate_series(1, occ.rep_count) AS gs (rep)
        WHERE
            loc.id = $1
    )
    SELECT
        t.occurrence_id,
        t.location_id,
        t.entry_id,
        t.plot_number AS plot_code,
        t.plot_number,
        t.plot_type,
        t.rep,
        t_design.x AS design_x,
        t_design.y AS design_y,
        t_pa.x AS pa_x,
        t_pa.y AS pa_y,
        t_field.x AS field_x,
        t_field.y AS field_y,
        t.block_number,
        t.plot_qc_code,
        t.plot_status,
        t.harvest_status,
        t.creator_id
    FROM
        t_plot1 AS t,
        (
            SELECT
                o AS order_number,
                ROW_NUMBER() OVER (PARTITION BY o ORDER BY random()) AS plot_number,
                x,
                y
            FROM
                generate_series(1, $2) AS o,
                generate_series(1, $3) AS x,
                generate_series(1, $4) AS y
        ) AS t_design,
        (
            SELECT
                ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                x,
                y
            FROM
                generate_series(1, $3 * $2) AS x,
                generate_series(1, $4) AS y
        ) AS t_pa,
        (
            SELECT
                ROW_NUMBER() OVER (ORDER BY random()) AS plot_number,
                x,
                y
            FROM
                generate_series(1, $3 * $2) AS x,
                generate_series(1, $4) AS y
        ) AS t_field
    WHERE
        t_design.plot_number = t.plot_number
        AND t_design.order_number = t.order_number
        AND t_pa.plot_number = t.layout_order_number
        AND t_field.plot_number = t.layout_order_number
    ORDER BY
        t.location_id,
        t.occurrence_id,
        t.plot_number
;

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.plot DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
--UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = TRUE;

-- execute populate_plots to every experiment (eta: ~2m 15s)
WITH t_query AS (
    SELECT
        format($$EXECUTE populate_plots(%s, %s, %s, %s);$$,
            loc.id,
            5,
            (array[[10, 30], [12, 25], [20, 15], [4, 75], [6, 50]])[loc.id % 5 + 1][1],
            (array[[10, 30], [12, 25], [20, 15], [4, 75], [6, 50]])[loc.id % 5 + 1][2]
        ) AS query
    FROM
        experiment.location AS loc
    WHERE
        loc.location_code LIKE 'DB-478%'
    ORDER BY
        loc.id
)
SELECT
    platform.execute(t.query) AS execute_query,
    t.query
FROM
    t_query AS t
;

-- drop prepared statement
DEALLOCATE populate_plots;

-- restore indices
--UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~50s)
REINDEX TABLE experiment.plot;

-- restore triggers and constraints
--ALTER TABLE experiment.plot ENABLE TRIGGER ALL;
SET session_replication_role = origin;



-- revert changes
--rollback --ALTER TABLE experiment.plot DISABLE TRIGGER ALL;
--rollback
--rollback UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- eta: ~19s
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback     JOIN experiment.location_occurrence_group AS logrp
--rollback         ON logrp.occurrence_id = occ.id
--rollback     JOIN experiment.location AS loc
--rollback         ON loc.id = logrp.location_id
--rollback     JOIN experiment.experiment AS expt
--rollback         ON expt.id = occ.experiment_id
--rollback WHERE
--rollback     occ.id = plot.occurrence_id
--rollback     AND expt.notes LIKE 'DB-478%'
--rollback ;
--rollback
--rollback UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- eta: ~ 11s
--rollback REINDEX TABLE experiment.plot;
--rollback
--rollback --ALTER TABLE experiment.plot ENABLE TRIGGER ALL;



--changeset postgres:insert_15000000_plot_data context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 15000000 plot data



-- insert into experiment.plot_data: 15000000 plot data = 7500000 plots x 2 collected harvest data

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.plot_data DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
--UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = TRUE;

-- insert harvest method HV_METH_DISC (eta: ~17s)
INSERT INTO
    experiment.plot_data (
        plot_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    plot.id AS plot_id,
    var.id AS variable_id,
    plotdata.data_value,
    'Q' AS data_qc_code,
    plot.creator_id
FROM
    experiment.location AS loc
    JOIN experiment.plot AS plot
        ON plot.location_id = loc.id
    JOIN (
        VALUES
        ('HV_METH_DISC', 'Bulk')
    ) AS plotdata (
        variable, data_value
    )
        ON TRUE
    JOIN master.variable AS var
        ON var.abbrev = plotdata.variable
WHERE
    loc.location_code LIKE 'DB-478%'
;

-- insert harvest date HVDATE_CONT (eta: ~17s)
INSERT INTO
    experiment.plot_data (
        plot_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    plot.id AS plot_id,
    var.id AS variable_id,
    (loc.location_year || '-' || plotdata.data_value) AS data_value,
    'Q' AS data_qc_code,
    plot.creator_id
FROM
    experiment.location AS loc
    JOIN tenant.season AS ssn
        ON ssn.id = loc.season_id
    JOIN experiment.plot AS plot
        ON plot.location_id = loc.id
    JOIN (
        VALUES
        ('HVDATE_CONT', 'DS', '05-25'),
        ('HVDATE_CONT', 'WS', '10-25')
    ) AS plotdata (
        variable, season, data_value
    )
        ON plotdata.season = ssn.season_code
    JOIN master.variable AS var
        ON var.abbrev = plotdata.variable
WHERE
    loc.location_code LIKE 'DB-478%'
;

-- restore indices
--UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = FALSE;

-- reindex table (eta: ~1m 6s)
REINDEX TABLE experiment.plot_data;

-- restore triggers and constraints
--ALTER TABLE experiment.plot_data ENABLE TRIGGER ALL;
SET session_replication_role = origin;



-- revert changes
--rollback --ALTER TABLE experiment.plot_data DISABLE TRIGGER ALL;
--rollback
--rollback --UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- eta: ~41s
--rollback DELETE FROM
--rollback     experiment.plot_data AS plotdata
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id,
--rollback     master.variable AS var
--rollback WHERE
--rollback     plotdata.plot_id = plot.id
--rollback     AND var.id = plotdata.variable_id
--rollback     AND loc.location_code LIKE 'DB-478%'
--rollback     AND var.abbrev = 'HV_METH_DISC'
--rollback ;
--rollback
--rollback -- eta: ~44s
--rollback DELETE FROM
--rollback     experiment.plot_data AS plotdata
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id,
--rollback     master.variable AS var
--rollback WHERE
--rollback     plotdata.plot_id = plot.id
--rollback     AND var.id = plotdata.variable_id
--rollback     AND loc.location_code LIKE 'DB-478%'
--rollback     AND var.abbrev = 'HVDATE_CONT'
--rollback ;
--rollback
--rollback --UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.plot_data'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- eta: ~10s
--rollback REINDEX TABLE experiment.plot_data;
--rollback
--rollback --ALTER TABLE experiment.plot_data ENABLE TRIGGER ALL;



--changeset postgres:insert_7500000_planting_instructions context:fixture labels:stress,medium splitStatements:false rollbackSplitStatements:false
--comment: DB-478 Insert 7500000 planting instructions



-- insert into experiment.planting_instruction: 7500000 planting instructions = 7500000 plots x 1 planting instruction per plot

-- prepare statement to populate planting instructions of plots
PREPARE
    populate_planting_instructions(
        integer, integer
        -- limit_number, offset_number
    )
AS
    INSERT INTO
        experiment.planting_instruction (
            entry_code,
            entry_number,
            entry_name,
            entry_type,
            entry_role,
            entry_class,
            entry_status,
            entry_id,
            plot_id,
            germplasm_id,
            seed_id,
            package_id,
            package_log_id,
            creator_id
        )
    SELECT
        ent.entry_code,
        ent.entry_number,
        ent.entry_name,
        ent.entry_type,
        ent.entry_role,
        ent.entry_class,
        ent.entry_status,
        ent.id AS entry_id,
        plot.id AS plot_id,
        ent.germplasm_id,
        ent.seed_id,
        ent.package_id,
        NULL AS package_log_id,
        plot.creator_id
    FROM
        experiment.experiment AS expt
        JOIN experiment.occurrence AS occ
            ON occ.experiment_id = expt.id
        JOIN experiment.plot AS plot
            ON plot.occurrence_id = occ.id
        JOIN experiment.entry AS ent
            ON ent.id = plot.entry_id
    WHERE
        expt.notes LIKE 'DB-478%'
    ORDER BY
        plot.id
    LIMIT
        $1
    OFFSET
        $2
;

-- disable triggers and constraints to speed up data insertions
--ALTER TABLE experiment.planting_instruction DISABLE TRIGGER ALL;
SET session_replication_role = replica;

-- disable indices to speed up data insertions
--UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = TRUE;

-- eta: ~17s per transaction
EXECUTE populate_planting_instructions(2000000, 0);
EXECUTE populate_planting_instructions(2000000, 2000000);
EXECUTE populate_planting_instructions(2000000, 4000000);
EXECUTE populate_planting_instructions(2000000, 6000000);

-- drop prepared statement
DEALLOCATE populate_planting_instructions;

-- restore indices
--UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = FALSE;

-- reindex table takes (eta: ~36s)
REINDEX TABLE experiment.planting_instruction;

-- restore triggers and constraints
--ALTER TABLE experiment.planting_instruction ENABLE TRIGGER ALL;
SET session_replication_role = origin;

-- revert changes
--rollback --ALTER TABLE experiment.planting_instruction DISABLE TRIGGER ALL;
--rollback
--rollback --UPDATE pg_index SET indisready = FALSE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = TRUE;
--rollback
--rollback -- eta: ~22s
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.location AS loc
--rollback     JOIN experiment.plot AS plot
--rollback         ON plot.location_id = loc.id
--rollback WHERE
--rollback     plantinst.plot_id = plot.id
--rollback     AND loc.location_code LIKE 'DB-478%'
--rollback ;
--rollback
--rollback --UPDATE pg_index SET indisready = TRUE WHERE indrelid = 'experiment.planting_instruction'::regclass::oid AND indisready = FALSE;
--rollback
--rollback -- eta: ~6s
--rollback REINDEX TABLE experiment.planting_instruction;
--rollback
--rollback --ALTER TABLE experiment.planting_instruction ENABLE TRIGGER ALL;
