--liquibase formatted sql

--changeset postgres:insert_kiboko_fields_to_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Insert Kiboko fields in place.geospatial_object



-- insert Kiboko fields
INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        geospatial_coordinates,
        creator_id
    )
SELECT
    geo.geospatial_object_code,
    geo.geospatial_object_name,
    geo.geospatial_object_type,
    geo.geospatial_object_subtype,
    pgeo.id AS parent_geospatial_object_id,
    rgeo.id AS root_geospatial_object_id,
    geo.geospatial_coordinates::polygon,
    1 AS creator_id
FROM (
        VALUES
        ('KB_NS_1', 'KB_NS_1', 'field', 'block', 'KIO', 'KIO', '((37.724905014038086,-2.220023687093425),(37.725221514701836,-2.2208277456312397),(37.726911306381226,-2.2200826514010665),(37.72651433944702,-2.219294673641058),(37.724905014038086,-2.220023687093425))'),
        ('KB_NS_2', 'KB_NS_2', 'field', 'block', 'KIO', 'KIO', '((37.724416851997375,-2.2190105727130094),(37.72481918334961,-2.2199057584710937),(37.72650361061096,-2.21916066377614),(37.72606372833252,-2.2182922795571347),(37.724416851997375,-2.2190105727130094))'),
        ('KB_CFT_1', 'KB_CFT_1', 'field', 'block', 'KIO', 'KIO', '((37.72460460662842,-2.233011859027451),(37.72483795881271,-2.2338239509897333),(37.725709676742554,-2.233521091663553),(37.725398540496826,-2.23271704005221),(37.72460460662842,-2.233011859027451))'),
        ('KB_CFT_2', 'KB_CFT_2', 'field', 'block', 'KIO', 'KIO', '((37.725315392017365,-2.233722104497631),(37.72559970617294,-2.2345475969144584),(37.72600203752518,-2.2343948272489236),(37.72570699453354,-2.2335693347462344),(37.725315392017365,-2.233722104497631))')
    ) AS geo (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object,
        root_geospatial_object,
        geospatial_coordinates
    )
    LEFT JOIN place.geospatial_object AS pgeo
        ON pgeo.geospatial_object_code = geo.parent_geospatial_object
    LEFT JOIN place.geospatial_object AS rgeo
        ON rgeo.geospatial_object_code = geo.root_geospatial_object
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN ('KB_NS_1', 'KB_NS_2', 'KB_CFT_1', 'KB_CFT_2')
--rollback ;



--changeset postgres:update_kiboko_geospatial_coordinates context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Update Kiboko geospatial coordinates



-- update Kiboko geospatial coordinates
UPDATE
    place.geospatial_object
SET
    geospatial_coordinates = '((37.71456241607666,-2.2190266538990717),(37.72486209869385,-2.2348075733262664),(37.72623538970947,-2.234378746230578),(37.72953987121582,-2.2234436130458493),(37.72533416748047,-2.21336606560958),(37.71589279174805,-2.2113934312757104),(37.71456241607666,-2.2190266538990717))'::polygon
WHERE
    geospatial_object_code = 'KIO'
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_coordinates = NULL
--rollback WHERE
--rollback     geospatial_object_code = 'KIO'
--rollback ;



--changeset postgres:insert_el_batan_fields_to_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Insert El Batan fields in place.geospatial_object



-- insert El Batan fields
INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        geospatial_coordinates,
        creator_id
    )
SELECT
    geo.geospatial_object_code,
    geo.geospatial_object_name,
    geo.geospatial_object_type,
    geo.geospatial_object_subtype,
    pgeo.id AS parent_geospatial_object_id,
    rgeo.id AS root_geospatial_object_id,
    geo.geospatial_coordinates::polygon,
    1 AS creator_id
FROM (
        VALUES
        ('EBN_A1', 'EBN_A1', 'field', 'block', 'EBN', 'EBN', '((-98.85255575180054,19.52910426947476),(-98.85166525840759,19.528588575750607),(-98.85098934173583,19.52969074268971),(-98.85191202163696,19.530196321338416),(-98.85255575180054,19.52910426947476))'),
        ('EBN_B1', 'EBN_B1', 'field', 'block', 'EBN', 'EBN', '((-98.8515767455101,19.528570880348504),(-98.85069161653519,19.528125966744337),(-98.8500988483429,19.529169995027196),(-98.85096251964569,19.5296477684316),(-98.8515767455101,19.528570880348504))'),
        ('EBN_C1', 'EBN_C1', 'field', 'block', 'EBN', 'EBN', '((-98.8506057858467,19.528090575836842),(-98.84971529245377,19.527605214037553),(-98.84908497333527,19.528651773599435),(-98.85000765323639,19.529127020630558),(-98.8506057858467,19.528090575836842))'),
        ('EBN_D1', 'EBN_D1', 'field', 'block', 'EBN', 'EBN', '((-98.84968042373657,19.527577406806984),(-98.84874433279037,19.527069292023537),(-98.84814083576202,19.52813860635228),(-98.84905546903609,19.528631550290502),(-98.84968042373657,19.527577406806984))')
    ) AS geo (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object,
        root_geospatial_object,
        geospatial_coordinates
    )
    LEFT JOIN place.geospatial_object AS pgeo
        ON pgeo.geospatial_object_code = geo.parent_geospatial_object
    LEFT JOIN place.geospatial_object AS rgeo
        ON rgeo.geospatial_object_code = geo.root_geospatial_object
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN ('EBN_A1', 'EBN_B1', 'EBN_C1', 'EBN_D1')
--rollback ;



--changeset postgres:update_el_batan_geospatial_coordinates context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Update El Batan geospatial coordinates



-- update El Batan geospatial coordinates
UPDATE
    place.geospatial_object
SET
    geospatial_coordinates = '((-98.85286420583725,19.529283750727856),(-98.85286688804626,19.529086574129146),(-98.85181814432144,19.52850009872072),(-98.84713768959045,19.525997442662163),(-98.84700089693068,19.525774982469652),(-98.84638667106628,19.52545898735147),(-98.84625792503357,19.52552471438696),(-98.84520649909972,19.524976145617256),(-98.84137362241745,19.53217311866456),(-98.84003788232803,19.53186977537779),(-98.83962750434874,19.532903668075267),(-98.83971333503723,19.53330306739397),(-98.8398340344429,19.533679715213633),(-98.84035974740982,19.534905710422485),(-98.84097933769226,19.535497218184293),(-98.84240090847015,19.53569438695674),(-98.84399682283401,19.53610894615461),(-98.84710282087326,19.533689826552386),(-98.84954631328583,19.532274232966895),(-98.85263353586197,19.530024424775405),(-98.85289639234543,19.52946070388074),(-98.85286420583725,19.529283750727856))'::polygon
WHERE
    geospatial_object_code = 'EBN'
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_coordinates = NULL
--rollback WHERE
--rollback     geospatial_object_code = 'EBN'
--rollback ;



--changeset postgres:insert_ciudad_obregon_fields_to_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Insert Ciudad Obregon fields in place.geospatial_object



-- insert Ciudad Obregon fields
INSERT INTO
    place.geospatial_object (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object_id,
        root_geospatial_object_id,
        geospatial_coordinates,
        creator_id
    )
SELECT
    geo.geospatial_object_code,
    geo.geospatial_object_name,
    geo.geospatial_object_type,
    geo.geospatial_object_subtype,
    pgeo.id AS parent_geospatial_object_id,
    rgeo.id AS root_geospatial_object_id,
    geo.geospatial_coordinates::polygon,
    1 AS creator_id
FROM (
        VALUES
        ('CON_F1', 'CON_F1', 'field', 'block', 'CON', 'CON', '((-109.92175340652464,27.385714911957773),(-109.91233348846436,27.385714911957773),(-109.91233348846436,27.39082097472837),(-109.92175340652464,27.39082097472837),(-109.92175340652464,27.385714911957773))'),
        ('CON_E1', 'CON_E1', 'field', 'block', 'CON', 'CON', '((-109.91220474243163,27.38830607775158),(-109.9077844619751,27.38830607775158),(-109.9077844619751,27.390706662470198),(-109.91220474243163,27.390706662470198),(-109.91220474243163,27.38830607775158))'),
        ('CON_E2', 'CON_E2', 'field', 'block', 'CON', 'CON', '((-109.90764498710632,27.388353708907474),(-109.90325689315796,27.388353708907474),(-109.90325689315796,27.390735240545816),(-109.90764498710632,27.390735240545816),(-109.90764498710632,27.388353708907474))'),
        ('CON_E3', 'CON_E3', 'field', 'block', 'CON', 'CON', '((-109.9122154712677,27.38586733515555),(-109.9077522754669,27.38586733515555),(-109.9077522754669,27.38814413166806),(-109.9122154712677,27.38814413166806),(-109.9122154712677,27.38586733515555))'),
        ('CON_E4', 'CON_E4', 'field', 'block', 'CON', 'CON', '((-109.90764498710632,27.385848282267304),(-109.90333199501038,27.385848282267304),(-109.90333199501038,27.388229867859454),(-109.90764498710632,27.388229867859454),(-109.90764498710632,27.385848282267304))')
    ) AS geo (
        geospatial_object_code,
        geospatial_object_name,
        geospatial_object_type,
        geospatial_object_subtype,
        parent_geospatial_object,
        root_geospatial_object,
        geospatial_coordinates
    )
    LEFT JOIN place.geospatial_object AS pgeo
        ON pgeo.geospatial_object_code = geo.parent_geospatial_object
    LEFT JOIN place.geospatial_object AS rgeo
        ON rgeo.geospatial_object_code = geo.root_geospatial_object
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN ('CON_F1', 'CON_E1', 'CON_E2', 'CON_E3', 'CON_E4')
--rollback ;



--changeset postgres:update_ciudad_obregon_geospatial_coordinates context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-472 Update Ciudad Obregon geospatial coordinates



-- update Ciudad Obregon geospatial coordinates
UPDATE
    place.geospatial_object
SET
    geospatial_coordinates = '((-109.93297576904297,27.400270379372056),(-109.93263244628905,27.372910613515597),(-109.91263389587402,27.372758172465723),(-109.89169120788574,27.372605731205855),(-109.8918628692627,27.400041774277625),(-109.93297576904297,27.400270379372056))'::polygon
WHERE
    geospatial_object_code = 'CON'
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_coordinates = NULL
--rollback WHERE
--rollback     geospatial_object_code = 'CON'
--rollback ;
