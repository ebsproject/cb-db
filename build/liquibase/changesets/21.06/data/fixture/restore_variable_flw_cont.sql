--liquibase formatted sql

--changeset postgres:restore_variable_flw_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-476 Restore variable FLW_CONT



-- restore variable
UPDATE
    master.variable
SET
    is_void = FALSE
WHERE
    abbrev = 'FLW_CONT'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     is_void = TRUE
--rollback WHERE
--rollback     abbrev = 'FLW_CONT'
--rollback ;



--changeset postgres:restore_formula_for_variable_flw_cont context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-476 Restore formula for variable FLW_CONT



-- restore formula
UPDATE
    master.formula AS form
SET
    is_void = FALSE
FROM
    master.variable AS var
WHERE
    form.result_variable_id = var.id
    AND var.abbrev = 'FLW_CONT'
;



-- revert changes
--rollback UPDATE
--rollback     master.formula AS form
--rollback SET
--rollback     is_void = TRUE
--rollback FROM
--rollback     master.variable AS var
--rollback WHERE
--rollback     form.result_variable_id = var.id
--rollback     AND var.abbrev = 'FLW_CONT'
--rollback ;
