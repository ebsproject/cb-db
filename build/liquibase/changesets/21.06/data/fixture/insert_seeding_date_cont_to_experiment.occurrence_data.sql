--liquibase formatted sql

--changeset postgres:insert_seeding_date_cont_to_experiment.occurrence_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-476 Insert SEEDING_DATE_CONT to experiment.occurrence_data



-- insert occurrence data
INSERT INTO
    experiment.occurrence_data (
        occurrence_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
WITH t_flowering_date AS (
    SELECT
        plot.occurrence_id,
        (array_agg(DISTINCT plotdata.data_value::date ORDER BY plotdata.data_value::date))[1] AS earliest_flowering_date
    FROM
        experiment.plot AS plot
        JOIN experiment.plot_data AS plotdata
            ON plotdata.plot_id = plot.id
        JOIN master.variable AS var
            ON var.id = plotdata.variable_id
    WHERE
        var.abbrev IN ('FLW_DATE_CONT')
        AND plot.is_void = FALSE
        AND plotdata.is_void = FALSE
        AND var.is_void = FALSE
    GROUP BY
        plot.occurrence_id
), t_seeding_date AS (
    SELECT
        t.*,
        (t.earliest_flowering_date - INTERVAL '90 DAYS')::date AS predicted_seeding_date
    FROM
        t_flowering_date AS t
)
SELECT
    t.occurrence_id,
    var.id AS variable_id,
    t.predicted_seeding_date AS data_value,
    'Q' AS data_qc_code,
    occ.creator_id
FROM
    t_seeding_date AS t
    JOIN experiment.occurrence AS occ
        ON occ.id = t.occurrence_id
    JOIN master.variable AS var
        ON var.abbrev = 'SEEDING_DATE_CONT'
WHERE
    occ.is_void = FALSE
    AND var.is_void = FALSE
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence_data AS occdata
--rollback USING (
--rollback         SELECT
--rollback             DISTINCT
--rollback             plot.occurrence_id
--rollback         FROM
--rollback             experiment.plot AS plot
--rollback             JOIN experiment.plot_data AS plotdata
--rollback                 ON plotdata.plot_id = plot.id
--rollback             JOIN master.variable AS var
--rollback                 ON var.id = plotdata.variable_id
--rollback         WHERE
--rollback             var.abbrev IN ('FLW_DATE_CONT')
--rollback             AND plot.is_void = FALSE
--rollback             AND plotdata.is_void = FALSE
--rollback             AND var.is_void = FALSE
--rollback     ) AS t,
--rollback     master.variable AS var
--rollback WHERE
--rollback     occdata.occurrence_id = t.occurrence_id
--rollback     AND occdata.variable_id = var.id
--rollback     AND var.abbrev = 'SEEDING_DATE_CONT'
--rollback ;
