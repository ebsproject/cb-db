--liquibase formatted sql

--changeset postgres:update_parent_and_root_columns_of_geospatial_objects context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-33 Update parent and root columns of geospatial_objects



-- curate geospatial_objects with subtype 'country'
UPDATE
    place.geospatial_object
SET
    geospatial_object_subtype = 'breeding location'
WHERE
    geospatial_object_code IN (
        'BGM_BDI',
        'VN_TT_HE',
        'ID_KS_BB',
        'LA_VT_XT',
        'BD_DA_DH',
        'VN_CN_CD',
        'TZ_KL_MR'
    )
;


-- update root of geospatial_objects with subtype 'country'
UPDATE
    place.geospatial_object
SET
    root_geospatial_object_id = id
WHERE
    geospatial_object_subtype = 'country'
;


-- curate geospatial_objects with subtype 'continent'
UPDATE
    place.geospatial_object
SET
    geospatial_object_subtype = 'breeding location'
WHERE
    geospatial_object_code IN (
        'MM_ML',
        'MM_SA',
        'IN_DL',
        'BI_BU',
        'MM_SH_TR',
        'LAKKI_MARWAT',
        'LL_RJ_BD'
    )
;


-- update root of geospatial_objects where root is null but has a parent that is a root
UPDATE
    place.geospatial_object
SET
    root_geospatial_object_id = parent_geospatial_object_id
WHERE
    root_geospatial_object_id IS NULL
AND
    parent_geospatial_object_id IS NOT NULL
;


-- update parent and root of geospatial_objects where parent and root are both and with subtype 'planting area'
UPDATE
    place.geospatial_object
SET
    parent_geospatial_object_id = t.site_id,
    root_geospatial_object_id = t.site_id
FROM (
    SELECT geospatial_object_id,site_id FROM experiment."location" WHERE geospatial_object_id IN (
        SELECT id FROM place.geospatial_object WHERE parent_geospatial_object_id IS NULL AND root_geospatial_object_id IS NULL AND geospatial_object_type = 'planting area'
    )
) AS t
    (geospatial_object_id, site_id)
WHERE
    id = t.geospatial_object_id
;


-- create missing root geospatial_objects and set their root values
INSERT INTO
    place.geospatial_object
        (geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id)
VALUES
    ('MX', 'Mexico', 'site', 'country', 1),
    ('ZW', 'Zimbabwe', 'site', 'country', 1),
    ('UGA', 'Uganda', 'site', 'country', 1)
;

UPDATE
    place.geospatial_object
SET
    root_geospatial_object_id = id
WHERE
    geospatial_object_code IN ('MX', 'ZW', 'UGA')
;


-- update parent and root of geospatial_objects where parent and root are both null
UPDATE
    place.geospatial_object AS geo
SET
    parent_geospatial_object_id = pgeo.id,
    root_geospatial_object_id = rgeo.id
FROM (
    VALUES
    ('TOA', 'MX', 'MX'),
    ('UNKNOWN', 'UNKNOWN', 'UNKNOWN'),
    ('TLN', 'MX', 'MX'),
    ('AFA', 'MX', 'MX'),
    ('TN_DK', 'TZ', 'TZ'),
    ('BGM_BDI', 'BI', 'BI'),
    ('KHALA_SHAH_KAKU', 'PK', 'PK'),
    ('HAE', 'ZW', 'ZW'),
    ('HYD', 'IN', 'IN'),
    ('UG_KS_MU', 'UGA', 'UGA'),
    ('UG_BI_KI', 'UGA', 'UGA'),
    ('NJO', 'KE', 'KE'),
    ('MXII', 'MX', 'MX'),
    ('PH_MA_LB', 'PH', 'PH'),
    ('PH_MH_LB', 'PH', 'PH'),
    ('SINDH', 'PK', 'PK'),
    ('LAKKI_MARWAT', 'PK', 'PK'),
    ('SUKAMANDI', 'ID', 'ID'),
    ('LL_RJ_BD', 'BD', 'BD')
) AS t
    (geospatial_object_code, parent_code, root_code)
JOIN
    place.geospatial_object AS pgeo
        ON pgeo.geospatial_object_code = t.parent_code
JOIN
    place.geospatial_object AS rgeo
        ON rgeo.geospatial_object_code = t.root_code
WHERE
    geo.parent_geospatial_object_id IS NULL
AND
    geo.root_geospatial_object_id IS NULL
AND
    geo.geospatial_object_code = t.geospatial_object_code
;



-- revert changes
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     parent_geospatial_object_id = NULL,
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'TOA',
--rollback         'UNKNOWN',
--rollback         'TLN',
--rollback         'AFA',
--rollback         'TN_DK',
--rollback         'BGM_BDI',
--rollback         'KHALA_SHAH_KAKU',
--rollback         'HAE',
--rollback         'HYD',
--rollback         'UG_KS_MU',
--rollback         'UG_BI_KI',
--rollback         'NJO',
--rollback         'MXII',
--rollback         'PH_MA_LB',
--rollback         'PH_MH_LB',
--rollback         'SINDH',
--rollback         'LAKKI_MARWAT',
--rollback         'SUKAMANDI',
--rollback         'LL_RJ_BD'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geospatial_object_code IN ('MX', 'ZW', 'UGA')
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_code IN ('MX', 'ZW', 'UGA')
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     parent_geospatial_object_id = NULL,
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'GEO000000000016',
--rollback         'GEO000000000001',
--rollback         'GEO000000000002',
--rollback         'GEO000000000003',
--rollback         'GEO000000000004',
--rollback         'GEO000000000005',
--rollback         'GEO000000000006',
--rollback         'GEO000000000007',
--rollback         'GEO000000000008',
--rollback         'GEO000000000009',
--rollback         'GEO000000000010',
--rollback         'GEO000000000011',
--rollback         'GEO000000000012',
--rollback         'GEO000000000013',
--rollback         'GEO000000000014',
--rollback         'GEO000000000015'
--rollback     )
--rollback AND
--rollback     geospatial_object_name IN (
--rollback         'AF4-11_LOC1',
--rollback         'A-3_LOC1',
--rollback         'CBBW-1_LOC1',
--rollback         'CBBW-2_LOC1',
--rollback         'CBBW-3_LOC1',
--rollback         'CBBW-4_LOC1',
--rollback         'CBBW-5_LOC1',
--rollback         'A-2_LOC1',
--rollback         'F1SIMPLEBW-1_LOC1',
--rollback         'F1SIMPLEBW-2_LOC1',
--rollback         'F1SIMPLEBW-3_LOC1',
--rollback         'F1SIMPLEBW-4_LOC1',
--rollback         'F1SIMPLEBW-5_LOC1',
--rollback         'A-1_LOC1',
--rollback         'A-11_LOC1',
--rollback         'A-2-2_LOC1'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'ID_KS_BB',
--rollback         'LA_VT_XT',
--rollback         'MM_ML',
--rollback         'MZ_ZA_NI',
--rollback         'PH_TR_PN',
--rollback         'MZ_MP_BO_UM',
--rollback         'LA_VI',
--rollback         'MM_BA_TW',
--rollback         'VN_TT_HE',
--rollback         'VN_BD_PC',
--rollback         'PH_IN_BT',
--rollback         'VN_HD_GL',
--rollback         'MM_SA',
--rollback         'KH_PP_DK',
--rollback         'BI_CI_MU',
--rollback         'BD_KH_KL',
--rollback         'BD_CG_CT',
--rollback         'PK_PB_LH',
--rollback         'PK_PB_BW',
--rollback         'KE_KI_MW',
--rollback         'PH_CS_PL',
--rollback         'IN_DL',
--rollback         'PH_NE_KY',
--rollback         'VNM_139',
--rollback         'VNM_133',
--rollback         'VNM_140',
--rollback         'VNM_141',
--rollback         'EGY_010',
--rollback         'EGY_004',
--rollback         'IN_UP_VA_BHU',
--rollback         'IN_CT_RP_IGKV',
--rollback         'IN_BR_BG_BAU',
--rollback         'IN_UP_FZ_NDUAT',
--rollback         'IN_AS_JO_AAU',
--rollback         'IN_AD_WG_RARS',
--rollback         'IN_JH_HZ_CRURRS',
--rollback         'BD_DA_GZ_BRRI',
--rollback         'BD_KH_ST_BRRI',
--rollback         'BD_RS_NT_BRRI',
--rollback         'NP_MM_DN_NRRP',
--rollback         'NP_MP_BN',
--rollback         'IN_TR_WT_ICAR',
--rollback         'CD_GI',
--rollback         'MG_MA_BE_MA',
--rollback         'MZ_ZA_NC_MI',
--rollback         'MZ_ZA_NI_MU',
--rollback         'ZM_LP_MA',
--rollback         'TZ_MZ_MI_UK',
--rollback         'BD_DA_DH'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'continent'
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'MM_ML',
--rollback         'MM_SA',
--rollback         'IN_DL',
--rollback         'BI_BU',
--rollback         'MM_SH_TR',
--rollback         'LAKKI_MARWAT',
--rollback         'LL_RJ_BD'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     root_geospatial_object_id = NULL
--rollback WHERE
--rollback     geospatial_object_subtype = 'country'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     place.geospatial_object
--rollback SET
--rollback     geospatial_object_subtype = 'country'
--rollback WHERE
--rollback     geospatial_object_code IN (
--rollback         'BGM_BDI',
--rollback         'VN_TT_HE',
--rollback         'ID_KS_BB',
--rollback         'LA_VT_XT',
--rollback         'BD_DA_DH',
--rollback         'VN_CN_CD',
--rollback         'TZ_KL_MR'
--rollback     )
--rollback ;