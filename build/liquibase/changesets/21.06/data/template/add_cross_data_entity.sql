--liquibase formatted sql

--changeset postgres:add_cross_data_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-496 Add CROSS_DATA entity



INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.schema_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                (SELECT id FROM dictionary.schema WHERE abbrev='GERMPLASM'),
                'CROSS_DATA',
                'Cross Data',
                'Cross Data'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;


INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
SELECT 
    t.abbrev,
    t.name,
    t.description,
    t.table_id,
    1
FROM 
    (
        VALUES
            (
                'CROSS_DATA',
                'Cross Data',
                'Cross Data',
                (SELECT id FROM dictionary.table WHERE abbrev='CROSS_DATA')
            )
    ) AS t (abbrev,name,description,table_id)
;



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'CROSS_DATA';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'CROSS_DATA';