--liquibase formatted sql

--changeset postgres:add_variable_replacement_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-488 Add variable REPLACEMENT_TYPE



DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
    var_variable_set_id int;
    var_variable_set_member_order_number int;
    var_count_property_id int;
    var_count_method_id int;
    var_count_scale_id int;
    var_count_variable_set_id int;
    var_count_variable_set_member_id int;
BEGIN

    --PROPERTY

    SELECT count(id) FROM master.property WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_count_property_id;
    IF var_count_property_id > 0 THEN
        SELECT id FROM master.property WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_property_id;
    ELSE
        INSERT INTO
            master.property (abbrev,display_name,name) 
        VALUES 
            ('REPLACEMENT_TYPE','Replacement Type','Replacement Type') 
        RETURNING id INTO var_property_id;
    END IF;

    --METHOD

    SELECT count(id) FROM master.method WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_count_method_id;
    IF var_count_method_id > 0 THEN
        SELECT id FROM master.method WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_method_id;
    ELSE
        INSERT INTO
            master.method (name,abbrev,formula,description) 
        VALUES 
            ('Replacement Type','REPLACEMENT_TYPE',NULL,NULL) 
        RETURNING id INTO var_method_id;
    END IF;

    --SCALE

    SELECT count(id) FROM master.scale WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_count_scale_id;
    IF var_count_scale_id > 0 THEN
        SELECT id FROM master.scale WHERE ABBREV = 'REPLACEMENT_TYPE' INTO var_scale_id;
    ELSE
        INSERT INTO
            master.scale (abbrev,type,name,unit,level) 
        VALUES 
            ('REPLACEMENT_TYPE','categorical','Replacement Type',NULL,'nominal') 
        RETURNING id INTO var_scale_id;
    END IF;

    --SCALE VALUE

    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
    VALUES 
        (var_scale_id,'Replace with check',1,'Replace with check','Replace with check','REPLACEMENT_TYPE_REPLACE_WITH_CHECK','show');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
    VALUES 
        (var_scale_id,'Replace with filler',2,'Replace with filler','Replace with filler','REPLACEMENT_TYPE_REPLACE_WITH_FILLER','show');
    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev, scale_value_status) 
    VALUES 
        (var_scale_id,'Replace with package',3,'Replace with package','Replace with package','REPLACEMENT_TYPE_REPLACE_WITH_PACKAGE','show');
    
    --VARIABLE

    INSERT INTO
        master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
    VALUES 
        ('active','Replacement Type','Replacement Type','character varying','Replacement Type','Replacement Type','False','REPLACEMENT_TYPE','study','metadata','entry, package') 
    RETURNING id INTO var_variable_id;

    --UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

    UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

    --VARIABLE_SET_ID

    SELECT count(id) FROM master.variable_set WHERE ABBREV = 'PLANTING_JOB_INFO' INTO var_count_variable_set_id;
    IF var_count_variable_set_id > 0 THEN
        SELECT id FROM master.variable_set WHERE ABBREV = 'PLANTING_JOB_INFO' INTO var_variable_set_id;
    ELSE
        INSERT INTO
            master.variable_set (abbrev,name) 
        VALUES 
            ('PLANTING_JOB_INFO','Planting Job Information') 
        RETURNING id INTO var_variable_set_id;
    END IF;

    --GET THE LAST ORDER NUMBER

    SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
    IF var_count_variable_set_member_id > 0 THEN
        SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
    ELSE
        var_variable_set_member_order_number = 1;
    END IF;

    --ADD VARIABLE SET MEMBER

    INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE');

--rollback DELETE FROM master.variable WHERE abbrev = 'REPLACEMENT_TYPE';
