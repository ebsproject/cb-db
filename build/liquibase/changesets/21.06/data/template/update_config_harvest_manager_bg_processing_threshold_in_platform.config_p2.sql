--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_bg_processing_threshold_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-528 Update config HARVEST_MANAGER_BG_PROCESSING_THRESHOLD in platform.config p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "deleteHarvestData": {
                "size": "2000",
                "description": "Delete harvest data of plots or crosses"
            },
            "deleteSeeds": {
                "size": "500",
                "description": "Delete seedlots of plots or crosses"
            },
            "deletePlotData": {
                "size": "2000",
                "description": "Delete plot data of plots"
            },
            "deleteSeedlots": {
                "size": "500",
                "description": "Delete seedlots of plots"
            },
            "createList": {
                "size": "500",
                "description": "Create new germplasm, seed, or package list"
            }
        }
    ' 
WHERE 
    abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "deleteSeeds": {
--rollback                 "size": "500",
--rollback                 "description": "Delete seedlots of plots or crosses"
--rollback             },
--rollback             "deleteHarvestData": {
--rollback                 "size": "2000",
--rollback                 "description": "Delete harvest data of plots or crosses"
--rollback             }
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';