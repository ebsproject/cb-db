--liquibase formatted sql

--changeset postgres:add_config_hm_recurrent_parent_pattern_backcross_rice_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-519 Add config HM_RECURRENT_PARENT_PATTERN_BACKCROSS_RICE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
(
    'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_RICE_DEFAULT',
    'Harvest Manager Recurrent Parent Pattern for Rice Backcrosses',
    $$
        {
            "pattern": [
                [
                    {
                        "type": "field",
                        "entity": "recurrentParentGermplasm",
                        "field_name": "designation",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "*",
                        "order_number": 1
                    },
                    {
                        "type": "counter",
                        "order_number": 2
                    }
                ],
                [
                    {
                        "type": "counter",
                        "order_number": 0
                    },
                    {
                        "type": "delimiter",
                        "value": "*",
                        "order_number": 1
                    },
                    {
                        "type": "field",
                        "entity": "recurrentParentGermplasm",
                        "field_name": "designation",
                        "order_number": 2
                    }
                    
                ]
            ],
            "delimiter_parentage": "/",
            "delimiter_backcross_number": "*"
        }
    $$,
    1,
    'harvest_manager',
    1,
    'add configuration for recurrent parent of rice backcross'
);



--rollback DELETE FROM platform.config WHERE abbrev='HM_RECURRENT_PARENT_PATTERN_BACKCROSS_RICE_DEFAULT';