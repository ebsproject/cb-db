--liquibase formatted sql

--changeset postgres:update_config_hm_browser_config_rice_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-510 Update config HM_BROWSER_CONFIG_RICE in platform.config p2



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "name": "HM_BROWSER_CONFIG_RICE",
            "values": [
                {
                    "CROSS_METHOD_SELFING": {
                        "fixed": {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed": {
                            "harvest_method": [
                                "Bulk",
                                "Panicle Selection",
                                "Single Plant Selection",
                                "Single Plant Selection and Bulk",
                                "plant-specific",
                                "Plant-Specific and Bulk",
                                "Single Seed Descent"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        }
                    },
                    "CROSS_METHOD_SINGLE_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk",
                                "Single seed numbering"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk",
                                "Single seed numbering"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]                    
                        }
                    }, 
                    "CROSS_METHOD_THREE_WAY_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]                    
                        }
                    },  
                    "CROSS_METHOD_DOUBLE_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]                    
                        }
                    },
                    "CROSS_METHOD_COMPLEX_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]                    
                        }
                    },
                    "CROSS_METHOD_BACKCROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]                    
                        }
                    } 
                }
            ]
        }
    ' 
WHERE 
    abbrev = 'HM_BROWSER_CONFIG_RICE';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "HM_BROWSER_CONFIG_RICE",
--rollback             "values": [
--rollback                 {
--rollback                     "CROSS_METHOD_SELFING": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk",
--rollback                                 "Panicle Selection",
--rollback                                 "Single Plant Selection",
--rollback                                 "Single Plant Selection and Bulk",
--rollback                                 "plant-specific",
--rollback                                 "Plant-Specific and Bulk",
--rollback                                 "Single Seed Descent"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_DOUBLE_CROSS": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_SINGLE_CROSS": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_COMPLEX_CROSS": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_THREE_WAY_CROSS": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HM_BROWSER_CONFIG_RICE';