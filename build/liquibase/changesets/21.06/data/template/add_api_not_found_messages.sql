--liquibase formatted sql

--changeset postgres:add_api_not_found_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-553 Add API not found messages



WITH not_found_msgs AS (
    SELECT 
        msg,
        row_number() OVER () AS order_number
    FROM 
        unnest(
            ARRAY[
            'The germplasm you have requested does not exist.',
            'The cross data requested does not exist.',
            'The seed you have requested does not exist.',
            'The planting job entry you have provided does not exist.',
            'The germplasm ID you have provided does not exist.',
            'The package ID you have provided does not exist.',
            'The planting job you have requested does not exist.',
            'The experiment ID you have provided does not exist.',
            'The summary for the experiment ID does not exist.',
            'The summary that you are requesting cannot be retrieved since the planting job ID that you have provided does not exist.',
            'The planting job occurrence ID you have provided does not exist.'
            ]::text[]
        ) AS msg
)
INSERT INTO 
    api.messages
        (http_status_code,code,type,message,url)
SELECT 
    '404',
    (SELECT max(code)::int FROM api.messages m WHERE http_status_code=404)+order_number,
    'ERR',
    msg,
    '/responses.html#'||(SELECT max(code)::int FROM api.messages m WHERE http_status_code=404)+order_number
FROM 
    not_found_msgs
;



--rollback DELETE FROM
--rollback     api.messages
--rollback WHERE
--rollback     http_status_code='404'
--rollback AND
--rollback     message 
--rollback IN
--rollback     (
--rollback          'The germplasm you have requested does not exist.',
--rollback 			'The cross data requested does not exist.',
--rollback 			'The seed you have requested does not exist.',
--rollback 			'The planting job entry you have provided does not exist.',
--rollback 			'The germplasm ID you have provided does not exist.',
--rollback 			'The package ID you have provided does not exist.',
--rollback 			'The planting job you have requested does not exist.',
--rollback 			'The experiment ID you have provided does not exist.',
--rollback 			'The summary for the experiment ID does not exist.',
--rollback 			'The summary that you are requesting cannot be retrieved since the planting job ID that you have provided does not exist.',
--rollback 			'The planting job occurrence ID you have provided does not exist.'
--rollback     )
--rollback ;