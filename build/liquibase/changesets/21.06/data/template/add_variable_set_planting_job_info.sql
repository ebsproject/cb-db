--liquibase formatted sql

--changeset postgres:add_variable_set_planting_job_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-488 Add variable_set PLANTING_JOB_INFO



INSERT INTO
	master.variable_set (abbrev,name,creator_id) 
VALUES 
	('PLANTING_JOB_INFO','Planting Job Information',1);



--rollback DELETE FROM master.variable_set WHERE abbrev='PLANTING_JOB_INFO';