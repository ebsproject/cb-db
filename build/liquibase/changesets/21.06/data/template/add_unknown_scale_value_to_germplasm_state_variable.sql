--liquibase formatted sql

--changeset postgres:add_unknown_scale_value_to_germplasm_state_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-543 Add 'unknown' scale value to GERMPLASM_STATE variable



-- add unknown scale value to GERMPLASM_STATE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('GERMPLASM_STATE_UNKNOWN', 'unknown', 3, 'unknown', 'unknown', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'GERMPLASM_STATE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('GERMPLASM_STATE_UNKNOWN')
--rollback ;