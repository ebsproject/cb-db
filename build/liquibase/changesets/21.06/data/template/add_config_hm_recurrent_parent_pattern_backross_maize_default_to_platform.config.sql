--liquibase formatted sql

--changeset postgres:add_config_hm_recurrent_parent_pattern_backross_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-512 Add config HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT to platform.config



INSERT INTO
    platform.config (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
(
    'HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT',
    'Harvest Manager Recurrent Parent Pattern for Maize Backcrosses',
$$
    {
        "pattern": [
            [
                {
                    "type": "field",
                    "entity": "recurrentParentGermplasm",
                    "field_name": "designation",
                    "order_number": 0
                },
                {
                    "type": "delimiter",
                    "value": "<",
                    "order_number": 1
                },{
                    "type": "counter",
                    "order_number": 2
                }
            ],
            [
                {
                    "type": "field",
                    "entity": "recurrentParentGermplasm",
                    "field_name": "designation",
                    "order_number": 0
                },
                {
                    "type": "delimiter",
                    "value": "<",
                    "order_number": 1
                },{
                    "type": "counter",
                    "order_number": 2
                }
            ]
        ],
        "delimiter_backcross_number" : "<",
        "delimiter_parentage" : "/"
    }
$$,
    1,
    'harvest_manager',
    1,
    'add configuration for recurrent parent of backcross'
);



-- revert changes
--rollback DELETE FROM platform.config WHERE abbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT';