--liquibase formatted sql

--changeset postgres:void_border_and_filler_from_plot_type_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-549 Void border and filler from PLOT_TYPE scale values



-- void scale values
UPDATE
    master.scale_value
SET
    is_void = TRUE
WHERE
    abbrev IN (
        'PLOT_TYPE_BORDER',
        'PLOT_TYPE_FILLER'
    )
;



-- revert changes
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     is_void = FALSE
--rollback WHERE
--rollback     abbrev IN (
--rollback         'PLOT_TYPE_BORDER',
--rollback         'PLOT_TYPE_FILLER'
--rollback     )
--rollback ;
