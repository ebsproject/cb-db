--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-527 Update config HARVEST_MANAGER_BG_PROCESSING_THRESHOLD in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "deleteHarvestData": {
                "size": "2000",
                "description": "Delete harvest data of plots or crosses"
            },
            "deleteSeeds": {
                "size": "500",
                "description": "Delete seedlots of plots or crosses"
            }
        }
    ' 
WHERE 
    abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "deletePlotData": {
--rollback                 "size": "2000",
--rollback                 "description": "Delete plot data of plots"
--rollback             },
--rollback             "deleteSeedlots": {
--rollback                 "size": "500",
--rollback                 "description": "Delete seedlots of plots"
--rollback             }
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';