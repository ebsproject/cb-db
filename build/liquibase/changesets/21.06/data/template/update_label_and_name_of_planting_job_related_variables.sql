--liquibase formatted sql

--changeset postgres:update_label_and_name_of_planting_job_related_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-551 Update label and name of PLANTING JOB related variables



WITH t1 AS (
    SELECT 
        abbrev,
        label old_label,
        name old_name,
        REPLACE(label,'Planting Job','Packing Job') new_label,
        REPLACE(name,'Planting Job','Packing Job') new_name
    FROM
        master.variable 
    WHERE 
        abbrev ILIKE 'PLANTING_JOB%'
)
UPDATE
    master.variable mv
SET
    label = t1.new_label,
    name = t1.new_name FROM t1
WHERE
    mv.abbrev = t1.abbrev
;



--rollback WITH t1 AS (
--rollback 	SELECT 
--rollback 		abbrev,
--rollback 		label old_label,
--rollback 		name old_name,
--rollback 		REPLACE(label,'Packing Job','Planting Job') new_label,
--rollback 		REPLACE(name,'Packing Job','Planting Job') new_name
--rollback 	FROM
--rollback 		master.variable 
--rollback 	WHERE 
--rollback 		abbrev ILIKE 'PLANTING_JOB%'
--rollback )
--rollback UPDATE
--rollback     master.variable mv
--rollback SET
--rollback     label = t1.new_label,
--rollback     name = t1.new_name FROM t1
--rollback WHERE
--rollback     mv.abbrev = t1.abbrev
--rollback ;