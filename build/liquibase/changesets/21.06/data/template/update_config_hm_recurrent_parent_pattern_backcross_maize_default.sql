--liquibase formatted sql

--changeset postgres:update_config_hm_recurrent_parent_pattern_backcross_maize_default context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-540 Update config HM_RECURRENT_PARENT_PATTERN_BACKCROSS_MAIZE_DEFAULT



-- update config
UPDATE
    platform.config
SET
    abbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_MAIZE_DEFAULT'
WHERE
    abbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT'
;



-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     abbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKROSS_MAIZE_DEFAULT'
--rollback WHERE
--rollback     abbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_MAIZE_DEFAULT'
--rollback ;
