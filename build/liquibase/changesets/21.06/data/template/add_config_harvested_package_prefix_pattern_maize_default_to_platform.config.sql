--liquibase formatted sql

--changeset postgres:add_config_harvested_package_prefix_pattern_maize_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-506 Add config HARVESTED_PACKAGE_PREFIX_PATTERN_MAIZE_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HARVESTED_PACKAGE_PREFIX_PATTERN_MAIZE_DEFAULT',
        'HM: Harvested Package Prefix Pattern Maize Default',
        $$			      
            {
                "pattern" : [
                    {
                        "type": "field",
                        "field_name": "programCode",
                        "order_number": 0
                    },
                    {
                        "type": "field",
                        "field_name": "experimentYearYY",
                        "order_number": 1
                    },
                    {
                        "type": "field",
                        "field_name": "experimentSeasonCode",
                        "order_number": 2
                    }
                ]
            }
        $$,
        1,
        'harvest_manager',
        1,
        'CORB-1454 - Insert Harvested Package Prefix Pattern Maize Default'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HARVESTED_PACKAGE_PREFIX_PATTERN_MAIZE_DEFAULT';