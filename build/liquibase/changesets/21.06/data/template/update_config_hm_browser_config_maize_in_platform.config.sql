--liquibase formatted sql

--changeset postgres:update_config_hm_browser_config_maize_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-505 Update config HM_BROWSER_CONFIG_MAIZE in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "name" : "HM_BROWSER_CONFIG_MAIZE",
            "values": [
                {
                    "CROSS_METHOD_SELFING": {
                        "fixed" : {
                            "display_column" : [
                                "harvestDate",
                                "harvestMethod"
                            ],
                            "harvest_method" : [
                                "Bulk"
                            ]
                        },
                        "not_fixed" : {
                            "display_column" : [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ],
                            "harvest_method" : [
                                "Bulk",
                                "Individual ear"
                            ]
                        }
                    },
                    "CROSS_METHOD_SINGLE_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]                    
                        }
                    },
                    "CROSS_METHOD_HYBRID_FORMATION": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]                    
                        }
                    },
                    "CROSS_METHOD_THREE_WAY_CROSS": {
                        "fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed" : {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]                    
                        }
                    }
                }
            ]
        }
    ' 
WHERE 
    abbrev = 'HM_BROWSER_CONFIG_MAIZE';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "HM_BROWSER_CONFIG_MAIZE",
--rollback             "values": [
--rollback                 {
--rollback                     "CROSS_METHOD_SELFING": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod",
--rollback                                 "numericVar"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk",
--rollback                                 "Individual ear"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_SINGLE_CROSS": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     },
--rollback                     "CROSS_METHOD_HYBRID_FORMATION": {
--rollback                         "fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         },
--rollback                         "not_fixed": {
--rollback                             "display_column": [
--rollback                                 "harvestDate",
--rollback                                 "harvestMethod"
--rollback                             ],
--rollback                             "harvest_method": [
--rollback                                 "Bulk"
--rollback                             ]
--rollback                         }
--rollback                     }
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'HM_BROWSER_CONFIG_MAIZE';