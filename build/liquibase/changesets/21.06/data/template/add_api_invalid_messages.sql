--liquibase formatted sql

--changeset postgres:add_api_invalid_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-553 Add API invalid messages



WITH invalid_msgs AS (
    SELECT 
        msg,
        row_number() OVER () AS order_number
    FROM 
        unnest(
            ARRAY[
            'Invalid request, planting job entry ID must be an integer.',
            'Invalid value provided for isSufficient, value must be a valid boolean value: true | false',
            'Invalid value provided for isReplaced, value must be a valid boolean value: true | false',
            'Invalid request, unit value must be one of the following: kg, seeds, pan, g, panicles, plant.',
            'Invalid request, replacement type value must be one of the following: Replace with check, Replace with filler, Replace with package',
            'Invalid format, replacement germplasm ID must be an integer.',
            'Invalid format, replacement package ID must be an integer.',
            'Invalid format, replacement entry ID must be an integer.',
            'Required parameter is missing. Ensure that experimentDbId is not empty.',
            'Invalid request, planting job ID must be an integer.',
            'Invalid value for plantingJobStatus, make sure that it is one of the following: packing cancelled, packing on hold, packed, packing, ready for packing.',
            'Invalid value for plantingJobType, make sure that it is one of the following: planting job, packing job',
            'Invalid format, planting job due date must be in format YYYY-MM-DD.',
            'Invalid request, planting job occurrence ID must be an integer.',
            'Invalid format, noOfRanges field must be an integer.',
            'Invalid format, entryListIdList must be a valid JSON string.',
            'Invalid format, layoutOrderData field must be a valid JSON string.',
            'Required parameters are missing. Ensure that entryName, entryType, entryStatus, entryListDbId, and germplasmDbId fields are not empty.',
            'Invalid format, packageDbId must be an integer.',
            'Invalid request, you have provided an invalid value for entry role.',
            'Invalid format, seed ID must be an integer.',
            'Required parameters are missing. Ensure that the crossDbId, germplasmDbId, parentRole, and orderNumber fields are not empty.',
            'Invalid format, germplasm ID must be an integer.',
            'Invalid request, you have provided an invalid value for parent role.'
            'Invalid format, order number must be an integer.' ,
            'Invalid request, please provide a valid request body.',
            'Invalid request, occurrence ID must be an integer.',
            'Invalid request, a pattern for the experiment that you have provided does not exist.',
            'Required parameters are missing. Ensure that the offset field is not empty.',
            'Ensure that there is a pattern to generate plot code for occurrence ID: ',
            'Required parameters are missing. Ensure that offset field of the occurrenceDbId is not empty.',
            'Invalid request. User has provided an invalid format for germplasmDbId, it must be an integer.',
            'Invalid request. User has provided an invalid format for crossDataDbId, it must be an integer.',
            'Invalid request. User has provided an invalid format for seedDbId, it must be an integer.'
            ]::text[]
        ) AS msg
)
INSERT INTO 
    api.messages
        (http_status_code,code,type,message,url)
SELECT 
    '400',
    (SELECT max(code)::int FROM api.messages m WHERE http_status_code=400)+order_number,
    'ERR',
    msg,
    '/responses.html#'||(SELECT max(code)::int FROM api.messages m WHERE http_status_code=400)+order_number
FROM 
    invalid_msgs
;



--rollback DELETE FROM
--rollback     api.messages
--rollback WHERE
--rollback     http_status_code='400'
--rollback AND
--rollback     message 
--rollback IN
--rollback     (
--rollback             'Invalid request, planting job entry ID must be an integer.',
--rollback             'Invalid value provided for isSufficient, value must be a valid boolean value: true | false',
--rollback             'Invalid value provided for isReplaced, value must be a valid boolean value: true | false',
--rollback             'Invalid request, unit value must be one of the following: kg, seeds, pan, g, panicles, plant.',
--rollback             'Invalid request, replacement type value must be one of the following: Replace with check, Replace with filler, Replace with package',
--rollback             'Invalid format, replacement germplasm ID must be an integer.',
--rollback             'Invalid format, replacement package ID must be an integer.',
--rollback             'Invalid format, replacement entry ID must be an integer.',
--rollback             'Required parameter is missing. Ensure that experimentDbId is not empty.',
--rollback             'Invalid request, planting job ID must be an integer.',
--rollback             'Invalid value for plantingJobStatus, make sure that it is one of the following: packing cancelled, packing on hold, packed, packing, ready for packing.',
--rollback             'Invalid value for plantingJobType, make sure that it is one of the following: planting job, packing job',
--rollback             'Invalid format, planting job due date must be in format YYYY-MM-DD.',
--rollback             'Invalid request, planting job occurrence ID must be an integer.',
--rollback             'Invalid format, noOfRanges field must be an integer.',
--rollback             'Invalid format, entryListIdList must be a valid JSON string.',
--rollback             'Invalid format, layoutOrderData field must be a valid JSON string.',
--rollback             'Required parameters are missing. Ensure that entryName, entryType, entryStatus, entryListDbId, and germplasmDbId fields are not empty.',
--rollback             'Invalid format, packageDbId must be an integer.',
--rollback             'Invalid request, you have provided an invalid value for entry role.',
--rollback             'Invalid format, seed ID must be an integer.',
--rollback             'Required parameters are missing. Ensure that the crossDbId, germplasmDbId, parentRole, and orderNumber fields are not empty.',
--rollback             'Invalid format, germplasm ID must be an integer.',
--rollback             'Invalid request, you have provided an invalid value for parent role.'
--rollback             'Invalid format, order number must be an integer.' ,
--rollback             'Invalid request, please provide a valid request body.',
--rollback             'Invalid request, occurrence ID must be an integer.',
--rollback             'Invalid request, a pattern for the experiment that you have provided does not exist.',
--rollback             'Required parameters are missing. Ensure that the offset field is not empty.',
--rollback             'Ensure that there is a pattern to generate plot code for occurrence ID: ',
--rollback             'Required parameters are missing. Ensure that offset field of the occurrenceDbId is not empty.',
--rollback             'Invalid request. User has provided an invalid format for germplasmDbId, it must be an integer.',
--rollback             'Invalid request. User has provided an invalid format for crossDataDbId, it must be an integer.',
--rollback             'Invalid request. User has provided an invalid format for seedDbId, it must be an integer.'
--rollback     )
--rollback ;