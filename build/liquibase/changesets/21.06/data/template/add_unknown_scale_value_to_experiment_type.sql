--liquibase formatted sql

--changeset postgres:add_unknown_scale_value_to_experiment_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-518 Add unknown scale value to EXPERIMENT_TYPE variable



-- add scale values to EXPERIMENT_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status, is_void)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status,
    scalval.is_void
FROM
    master.variable AS var,
    (
        VALUES
        ('EXPERIMENT_TYPE_UNKNOWN', 'Unknown', 6, 'Unknown', 'Unknown', 1, 'hide', true)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status, is_void
    )
WHERE
    var.abbrev = 'EXPERIMENT_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('EXPERIMENT_TYPE_UNKNOWN')
--rollback ;