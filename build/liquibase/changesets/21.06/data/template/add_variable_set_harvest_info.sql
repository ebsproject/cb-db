--liquibase formatted sql

--changeset postgres:add_variable_set_harvest_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-481 Add variable_set HARVEST_INFO



INSERT INTO
    master.variable_set (abbrev,name,creator_id) 
VALUES 
    ('HARVEST_INFO','Harvest Information',1);



--rollback DELETE FROM master.variable_set WHERE abbrev='HARVEST_INFO';