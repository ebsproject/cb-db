--liquibase formatted sql

--changeset postgres:update_occurrence_data_level_of_some_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-480 Update occurrence data level of some variables



-- update data level of study variables with added occurrence
UPDATE
    master.variable AS var
SET
    data_level = var.data_level || ', occurrence'
WHERE
    var.data_level ILIKE '%study%'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = replace(var.data_level, ', occurrence', '')
--rollback WHERE
--rollback     var.data_level ILIKE '%occurrence%'
--rollback ;
