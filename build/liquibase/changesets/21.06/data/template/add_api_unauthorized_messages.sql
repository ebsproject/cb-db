--liquibase formatted sql

--changeset postgres:add_api_unauthorized_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-553 Add API unauthorized messages



WITH unauthorized_msgs AS (
    SELECT 
        msg,
        row_number() OVER () AS order_number
    FROM 
        unnest(
            ARRAY[
            'Unauthorized request. User is not a program team member.',
            'Unauthorized request. User is not a program team member or a producer.'
            ]::text[]
        ) AS msg
)
INSERT INTO 
    api.messages
        (http_status_code,code,type,message,url)
SELECT 
    '401',
    (SELECT max(code)::int FROM api.messages m WHERE http_status_code=401)+order_number,
    'ERR',
    msg,
    '/responses.html#'||(SELECT max(code)::int FROM api.messages m WHERE http_status_code=401)+order_number
FROM 
    unauthorized_msgs
;



--rollback DELETE FROM
--rollback     api.messages
--rollback WHERE
--rollback     http_status_code='401'
--rollback AND
--rollback     message 
--rollback IN
--rollback     (
--rollback         'Unauthorized request. User is not a program team member.',
--rollback         'Unauthorized request. User is not a program team member or a producer.'
--rollback     )
--rollback ;