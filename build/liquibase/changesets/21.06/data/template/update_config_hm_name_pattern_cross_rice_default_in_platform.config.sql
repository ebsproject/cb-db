--liquibase formatted sql

--changeset postgres:update_config_hm_name_pattern_cross_rice_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-526 Update config HM_NAME_PATTERN_CROSS_RICE_DEFAULT in platform.config




UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state/germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "sequence_name": "<sequence_name>",
                                "order_number": 4
                            }
                        ]
                    }
                }
            },
            "PLOT": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR XXXXX",
                                "order_number": 0
                            }
                        ]
                    }
                },
                "single cross": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "sequence_name": "cross_name_seq",
                                "order_number": 1
                            }
                        ],
                        "bulk": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "sequence_name": "cross_number_seq",
                                "order_number": 1
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "free-text",
                                "value": "IR ",
                                "order_number": 0
                            },
                            {
                                "type": "db-sequence",
                                "schema": "germplasm",
                                "sequence_name": "cross_number_seq",
                                "order_number": 1
                            }
                        ]
                    }
                }
            }
        }
    '
WHERE 
    abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT';



-- revert changes
--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "harvest_mode": {
--rollback                 "cross_method": {
--rollback                     "germplasm_state/germplasm_type": {
--rollback                         "harvest_method": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "ABC",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "<entity>",
--rollback                                 "field_name": "<field_name>",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "<schema>",
--rollback                                 "sequence_name": "<sequence_name>",
--rollback                                 "order_number": 4
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "PLOT": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR XXXXX",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "CROSS": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR ",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "germplasm",
--rollback                                 "sequence_name": "cross_name_seq",
--rollback                                 "order_number": 1
--rollback                             }
--rollback                         ],
--rollback                         "bulk": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "IR ",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "germplasm",
--rollback                                 "sequence_name": "cross_number_seq",
--rollback                                 "order_number": 1
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'HM_NAME_PATTERN_CROSS_RICE_DEFAULT';