--liquibase formatted sql

--changeset postgres:update_config_hm_label_pattern_package_wheat_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-507 Update config HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state/germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "sequence_name": "<sequence_name>",
                                "order_number": 4
                            }
                        ]
                    }
                }
            },
            "PLOT": {
                "default": {
                    "default" : {
                        "default" : [
                            {
                                "type" : "field",
                                "entity" : "seed",
                                "field_name" : "seedName",
                                "order_number":0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "P",
                                "order_number": 2
                            },
                            {
                                "type" : "field",
                                "entity" : "plot",
                                "field_name" : "plotNumber",
                                "order_number": 3
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 4
                            },
                            {
                                "type": "free-text",
                                "value": "PK",
                                "order_number": 5
                            },
                            {
                                "type": "counter",
                                "leading_zero" : "yes",
                                "max_digits" : 3,
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "",
                                "order_number": 0
                            }
                        ]
                    }
                },
                "single cross": {
                    "default" : {
                        "default" : [
                            {
                                "type": "field",
                                "entity": "seed",
                                "field_name": "seedName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "PK",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "leading_zero" : "yes",
                                "max_digits" : 3,
                                "order_number": 3
                            }
                        ]
                    }
                },
                "top cross": {
                    "default" : {
                        "default" : [
                            {
                                "type": "field",
                                "entity": "seed",
                                "field_name": "seedName",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "PK",
                                "order_number": 2
                            },
                            {
                                "type": "counter",
                                "leading_zero" : "yes",
                                "max_digits" : 3,
                                "order_number": 3
                            }
                        ]
                    }
                }
            }
        }
    ' 
WHERE 
    abbrev = 'HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "PLOT": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "seed",
--rollback                                 "field_name": "seedName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "P",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "plotNumber",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 4
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "PK",
--rollback                                 "order_number": 5
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "max_digits": 3,
--rollback                                 "leading_zero": "yes",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "CROSS": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 },
--rollback                 "single cross": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "seed",
--rollback                                 "field_name": "seedName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "PK",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "max_digits": 3,
--rollback                                 "leading_zero": "yes",
--rollback                                 "order_number": 3
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "harvest_mode": {
--rollback                 "cross_method": {
--rollback                     "germplasm_state/germplasm_type": {
--rollback                         "harvest_method": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "ABC",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "<entity>",
--rollback                                 "field_name": "<field_name>",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "<schema>",
--rollback                                 "order_number": 4,
--rollback                                 "sequence_name": "<sequence_name>"
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT';