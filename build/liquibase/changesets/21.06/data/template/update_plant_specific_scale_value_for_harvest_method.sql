--liquibase formatted sql

--changeset postgres:update_plant_specific_scale_value_for_harvest_method context:template splitstatements:false rollbacksplitstatements:false
--comment: DB-368 Update plant-specific scale value for harvest method



UPDATE
    master.scale_value
SET
    value = 'Plant-specific'
WHERE
    abbrev = 'HV_METH_DISC_PLANT_SPECIFIC'
;



-- rollback
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'plant-specific'
--rollback WHERE
--rollback     abbrev = 'HV_METH_DISC_PLANT_SPECIFIC'
--rollback ;