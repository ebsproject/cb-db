--liquibase formatted sql

--changeset postgres:add_api_forbidden_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-553 Add API forbidden messages



WITH forbidden_msgs AS (
    SELECT 
        msg,
        row_number() over () as order_number
    FROM 
        unnest(
            ARRAY[
            'User is not allowed to delete seeds and related data already used in an experiment.',
            'User is not allowed to delete seed relation if the seed is a parent seed.',
            'User is not allowed to delete germplasm relation if the germplasm is a parent germplasm.',
            'User is not allowed to delete seeds and related data of a germplasm already used in an experiment.',
            'User is not allowed to delete seeds with seed attribute and seed relation records.',
            'User is not allowed to delete the planting job record when the status is already packed.',
            'User is not allowed to delete the planting job occurrence record when the status is already packed.'
            ]::text[]
        ) as msg
)
INSERT INTO 
    api.messages
        (http_status_code, code,type,message,url)
SELECT 
    '403',
    (SELECT max(code)::int FROM api.messages m WHERE http_status_code=403)+ order_number,
    'ERR',
    msg,
    '/responses.html#'||(SELECT max(code)::int FROM api.messages m WHERE http_status_code=403)+order_number
FROM 
    forbidden_msgs
;



--rollback DELETE FROM
--rollback     api.messages
--rollback WHERE
--rollback     http_status_code='403'
--rollback AND
--rollback     message 
--rollback IN
--rollback     (
--rollback         'User is not allowed to delete seeds and related data already used in an experiment.',
--rollback         'User is not allowed to delete seed relation if the seed is a parent seed.',
--rollback         'User is not allowed to delete germplasm relation if the germplasm is a parent germplasm.',
--rollback         'User is not allowed to delete seeds and related data of a germplasm already used in an experiment.',
--rollback         'User is not allowed to delete seeds with seed attribute and seed relation records.',
--rollback         'User is not allowed to delete the planting job record when the status is already packed.',
--rollback         'User is not allowed to delete the planting job occurrence record when the status is already packed.'
--rollback     )
--rollback ;