--liquibase formatted sql

--changeset postgres:update_transaction_type_variable_to_package_transaction_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-459 Update TRANSACTION_TYPE variable to PACKAGE_TRANSACTION_TYPE



UPDATE
    master.variable
SET
    abbrev = 'PACKAGE_TRANSACTION_TYPE',
    label = 'PACKAGE_TRANSACTION_TYPE',
    name = 'package transaction type',
    display_name = 'Package Transaction Type',
    description = 'Package Transaction Type'
WHERE
    abbrev = 'TRANSACTION_TYPE'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     abbrev = 'TRANSACTION_TYPE',
--rollback     label = 'TRANSACTION_TYPE',
--rollback     name = 'transaction type',
--rollback     display_name = 'Transaction Type',
--rollback     description = null
--rollback WHERE
--rollback     abbrev = 'PACKAGE_TRANSACTION_TYPE'
--rollback ;



--changeset postgres:add_scale_values_withdraw_deposit_reserve context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-459 Add scale values: withdraw, deposit, reserve



-- add scale values to PACKAGE_TRANSACTION_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PACKAGE_TRANSACTION_TYPE_WITHDRAW', 'withdraw', 1, 'Withdraw', 'Withdraw', 1, 'show'),
        ('PACKAGE_TRANSACTION_TYPE_DEPOSIT', 'deposit', 2, 'Deposit', 'Deposit', 1, 'show'),
        ('PACKAGE_TRANSACTION_TYPE_RESERVE', 'reserve', 3, 'Reserve', 'Reserve', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PACKAGE_TRANSACTION_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('PACKAGE_TRANSACTION_TYPE_WITHDRAW','PACKAGE_TRANSACTION_TYPE_DEPOSIT','PACKAGE_TRANSACTION_TYPE_RESERVE')
--rollback ;