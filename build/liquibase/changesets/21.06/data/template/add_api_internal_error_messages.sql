--liquibase formatted sql

--changeset postgres:add_api_internal_error_messages context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-553 Add API internal error messages



WITH internal_error_msgs AS (
    SELECT 
        msg,
        row_number() OVER () AS order_number
    FROM 
        unnest(
            ARRAY[
            'Error encountered while retrieving valid replacement types.',
            'Error encountered while retrieving valid units.'
            ]::text[]
        ) AS msg
)
INSERT INTO 
    api.messages
        (http_status_code,code,type,message,url)
SELECT 
    '500',
    (SELECT max(code)::int FROM api.messages m WHERE http_status_code=500)+order_number,
    'ERR',
    msg,
    '/responses.html#'||(SELECT max(code)::int FROM api.messages m WHERE http_status_code=500)+order_number
FROM 
    internal_error_msgs
;



--rollback DELETE FROM
--rollback     api.messages
--rollback WHERE
--rollback     http_status_code='500'
--rollback AND
--rollback     message 
--rollback IN
--rollback     (
--rollback         'Error encountered while retrieving valid replacement types.',
--rollback         'Error encountered while retrieving valid units.'
--rollback     )
--rollback ;