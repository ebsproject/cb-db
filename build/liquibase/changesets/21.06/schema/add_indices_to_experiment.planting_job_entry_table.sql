--liquibase formatted sql

--changeset postgres:add_indices_to_experiment.planting_job_entry_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-547 Add indices to experiment.planting_job_entry table



-- add indices
CREATE INDEX
    planting_job_entry_is_sufficient_idx
ON
    experiment.planting_job_entry (
        is_sufficient
    )
;

CREATE INDEX
    planting_job_entry_is_replaced_idx
ON
    experiment.planting_job_entry (
        is_replaced
    )
;

CREATE INDEX
    planting_job_entry_replacement_type_idx
ON
    experiment.planting_job_entry (
        replacement_type
    )
;

CREATE INDEX
    planting_job_entry_replacement_entry_id_idx
ON
    experiment.planting_job_entry (
        replacement_entry_id
    )
;



-- revert changes
--rollback DROP INDEX experiment.planting_job_entry_is_sufficient_idx;
--rollback DROP INDEX experiment.planting_job_entry_is_replaced_idx;
--rollback DROP INDEX experiment.planting_job_entry_replacement_type_idx;
--rollback DROP INDEX experiment.planting_job_entry_replacement_entry_id_idx;
