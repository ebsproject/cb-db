--liquibase formatted sql

--changeset postgres:update_entry_code_and_plot_code_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-550 Update ENTRY_CODE and PLOT_CODE character length



ALTER TABLE 
    experiment.entry 
ALTER COLUMN 
    entry_code type character varying(128);

ALTER TABLE 
    experiment.planting_instruction 
ALTER COLUMN 
    entry_code type character varying(128);

ALTER TABLE 
    experiment.plot 
ALTER COLUMN 
    plot_code type character varying(128);



--rollback ALTER TABLE 
--rollback     experiment.entry 
--rollback ALTER COLUMN 
--rollback     entry_code type character varying(64);
--rollback 
--rollback ALTER TABLE 
--rollback     experiment.planting_instruction 
--rollback ALTER COLUMN 
--rollback     entry_code type character varying(64);
--rollback 
--rollback ALTER TABLE 
--rollback     experiment.plot 
--rollback ALTER COLUMN 
--rollback     plot_code type character varying(64);