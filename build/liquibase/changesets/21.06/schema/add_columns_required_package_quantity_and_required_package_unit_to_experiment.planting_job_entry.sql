--liquibase formatted sql

--changeset postgres:add_columns_required_package_quantity_and_required_package_unit_to_experiment.planting_job_entry context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-498 Add columns required_package_quantity and required_package_unit to experiment.planting_job_entry



ALTER TABLE experiment.planting_job_entry
    ADD COLUMN required_package_quantity double precision NOT NULL,
    ADD COLUMN required_package_unit varchar(32) NOT NULL;

COMMENT ON COLUMN experiment.planting_job_entry.required_package_quantity
    IS 'Required Package Quantity: Required weight or number of seeds in the package [PJOBENT_REQPKGQTY]';

COMMENT ON COLUMN experiment.planting_job_entry.required_package_unit
    IS 'Required Package Unit: Required unit of quantity of the seed package [PJOBENT_REQPKGUNIT]';



--rollback ALTER TABLE experiment.planting_job_entry DROP COLUMN required_package_quantity;
--rollback ALTER TABLE experiment.planting_job_entry DROP COLUMN required_package_unit;