--liquibase formatted sql

--changeset postgres:add_column_replacement_entry_id_to_experiment.planting_job_entry context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-531 Add column replacement_entry_id to experiment.planting_job_entry



ALTER TABLE experiment.planting_job_entry
    ADD COLUMN replacement_entry_id integer NULL,
    ADD CONSTRAINT planting_job_entry_replacement_entry_id_fk FOREIGN KEY (replacement_entry_id)
    REFERENCES experiment.entry (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE RESTRICT;

COMMENT ON COLUMN experiment.planting_job_entry.replacement_entry_id
    IS 'Entry ID: Reference to the replacement entry [PJOBENT_ENTRY_ID]';



--rollback ALTER TABLE experiment.planting_job_entry DROP COLUMN replacement_entry_id;