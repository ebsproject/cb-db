--liquibase formatted sql

--changeset postgres:add_transaction_id_and_collection_timestamp_columns_in_package_data_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-442 Add transaction_id and collection_timestamp columns to package_data table



ALTER TABLE
    germplasm.package_data
ADD COLUMN 
    transaction_id integer,
ADD COLUMN
    collection_timestamp timestamp,
ADD CONSTRAINT
    package_data_transaction_id_fk
FOREIGN KEY
    (transaction_id)
REFERENCES 
    data_terminal.transaction(id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN germplasm.package_data.collection_timestamp IS 'Collection Timestamp: Timestamp when the package data was collected [PKGDATA_COLLTSTAMP]';
COMMENT ON COLUMN germplasm.package_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [PKGDATA_TXN_ID]';



-- revert changes
--rollback ALTER TABLE germplasm.package_data
--rollback DROP COLUMN transaction_id,
--rollback DROP COLUMN collection_timestamp;



--changeset postgres:add_transaction_id_and_collection_timestamp_columns_in_seed_trait_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-442 Add transaction_id and collection_timestamp columns to seed_trait table



ALTER TABLE
    germplasm.seed_trait
ADD COLUMN 
    transaction_id integer,
ADD COLUMN
    collection_timestamp timestamp,
ADD CONSTRAINT
    package_data_transaction_id_fk
FOREIGN KEY
    (transaction_id)
REFERENCES 
    data_terminal.transaction(id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON COLUMN germplasm.seed_trait.collection_timestamp IS 'Collection Timestamp: Timestamp when the seed trait was collected [SEEDTRAIT_COLLTSTAMP]';
COMMENT ON COLUMN germplasm.seed_trait.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [SEEDTRAIT_TXN_ID]';



-- revert changes
--rollback ALTER TABLE germplasm.seed_trait
--rollback DROP COLUMN transaction_id,
--rollback DROP COLUMN collection_timestamp;