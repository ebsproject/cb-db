--liquibase formatted sql

--changeset postgres:rename_seed_trait_table_to_seed_data context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-443 Rename seed_trait table to seed_data



-- rename table
ALTER TABLE germplasm.seed_trait RENAME TO seed_data;
COMMENT ON TABLE germplasm.seed_data IS 'Seed Data: Data linked to the seed [SEEDDATA]';


-- rename column comments
COMMENT ON COLUMN germplasm.seed_data.id IS 'Seed Data ID: Database identifier of the seed data [SEEDDATA_ID]';
COMMENT ON COLUMN germplasm.seed_data.seed_id IS 'Seed ID: Reference to the seed having the data [SEEDDATA_SEED_ID]';
COMMENT ON COLUMN germplasm.seed_data.variable_id IS 'Variable ID: Reference to the variable of the seed data [SEEDDATA_VAR_ID]';
COMMENT ON COLUMN germplasm.seed_data.data_value IS 'Seed Data Data Value: Value of the seed data [SEEDDATA_DATAVAL]';
COMMENT ON COLUMN germplasm.seed_data.data_qc_code IS 'Seed Data Data QC Code: Status of the seed data {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDDATA_QCCODE]';
COMMENT ON COLUMN germplasm.seed_data.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [SEEDDATA_TXN_ID]';
COMMENT ON COLUMN germplasm.seed_data.collection_timestamp IS 'Collection Timestamp: Timestamp when the seed data was collected [SEEDDATA_COLLTSTAMP]';


-- rename constraint
ALTER INDEX germplasm.pk_seed_trait_id RENAME TO pk_seed_data_id;


-- rename foreign key constraints
ALTER TABLE germplasm.seed_data RENAME CONSTRAINT seed_trait_creator_id_fkey to seed_data_creator_id_fkey;
ALTER TABLE germplasm.seed_data RENAME CONSTRAINT seed_trait_modifier_id_fkey to seed_data_modifier_id_fkey;
ALTER TABLE germplasm.seed_data RENAME CONSTRAINT seed_trait_seed_id_fk to seed_data_seed_id_fk;
ALTER TABLE germplasm.seed_data RENAME CONSTRAINT seed_trait_variable_id_fk to seed_data_variable_id_fk;


-- modify foreign key constraints comments
COMMENT ON CONSTRAINT seed_data_seed_id_fk ON germplasm.seed_data IS 'A seed data refers to one seed. A seed can have one or many seed data.';
COMMENT ON CONSTRAINT seed_data_variable_id_fk ON germplasm.seed_data IS 'A seed data refers to one variable. A variable can be referred by one or many seed data.';


-- rename indexes
ALTER INDEX germplasm.seed_trait_creator_id_idx RENAME TO seed_data_creator_id_idx;
ALTER INDEX germplasm.seed_trait_is_void_idx RENAME TO seed_data_is_void_idx;
ALTER INDEX germplasm.seed_trait_modifier_id_idx RENAME TO seed_data_modifier_id_idx;
ALTER INDEX germplasm.seed_trait_seed_id_data_qc_code_idx RENAME TO seed_data_seed_id_data_qc_code_idx;
ALTER INDEX germplasm.seed_trait_seed_id_idx RENAME TO seed_data_seed_id_idx;
ALTER INDEX germplasm.seed_trait_seed_id_variable_id_data_value_idx RENAME TO seed_data_seed_id_variable_id_data_value_idx;
ALTER INDEX germplasm.seed_trait_seed_id_variable_id_idx RENAME TO seed_data_seed_id_variable_id_idx;
ALTER INDEX germplasm.seed_trait_variable_id_idx RENAME TO seed_data_variable_id_idx;


-- modify index comments
COMMENT ON INDEX germplasm.seed_data_seed_id_data_qc_code_idx IS 'Seed data can be retrieved by its data QC code within a seed.';
COMMENT ON INDEX germplasm.seed_data_seed_id_variable_id_data_value_idx IS 'Seed data can be retrieved by its variable and data value within a seed.';
COMMENT ON INDEX germplasm.seed_data_seed_id_variable_id_idx IS 'Seed data can be retrieved by its variable within a seed.';


-- rename trigger
ALTER TRIGGER seed_trait_event_log_tgr ON germplasm.seed_data RENAME TO seed_data_event_log_tgr;


-- make data_value column data type to varchar
ALTER TABLE germplasm.seed_data ALTER COLUMN data_value SET DATA TYPE varchar;



-- revert changes
--rollback ALTER TABLE germplasm.seed_data RENAME TO seed_trait;
--rollback COMMENT ON TABLE germplasm.seed_trait IS 'Seed Trait: Data linked to the seed [SEEDTRAIT]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.id IS 'Seed Trait ID: Database identifier of the seed trait [SEEDTRAIT_ID]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.seed_id IS 'Seed ID: Reference to the seed having the trait [SEEDTRAIT_SEED_ID]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.variable_id IS 'Variable ID: Reference to the variable of the seed trait [SEEDTRAIT_VAR_ID]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.data_value IS 'Seed Trait Data Value: Value of the seed trait [SEEDTRAIT_DATAVAL]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.data_qc_code IS 'Seed Trait Data QC Code: Status of the seed trait {N (new), G (good), Q (questionable), S (suppressed), M (missing value), B (bad value)} [SEEDTRAIT_QCCODE]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.transaction_id IS 'Transaction ID: Reference to the transaction where the collected data was uploaded and validated [SEEDTRAIT_TXN_ID]';
--rollback COMMENT ON COLUMN germplasm.seed_trait.collection_timestamp IS 'Collection Timestamp: Timestamp when the seed trait was collected [SEEDTRAIT_COLLTSTAMP]';
--rollback 
--rollback ALTER INDEX germplasm.pk_seed_data_id RENAME TO pk_seed_trait_id;
--rollback 
--rollback ALTER TABLE germplasm.seed_trait RENAME CONSTRAINT seed_data_creator_id_fkey to seed_trait_creator_id_fkey;
--rollback ALTER TABLE germplasm.seed_trait RENAME CONSTRAINT seed_data_modifier_id_fkey to seed_trait_modifier_id_fkey;
--rollback ALTER TABLE germplasm.seed_trait RENAME CONSTRAINT seed_data_seed_id_fk to seed_trait_seed_id_fk;
--rollback ALTER TABLE germplasm.seed_trait RENAME CONSTRAINT seed_data_variable_id_fk to seed_trait_variable_id_fk;
--rollback 
--rollback COMMENT ON CONSTRAINT seed_trait_seed_id_fk ON germplasm.seed_trait IS 'A seed trait refers to one seed. A seed can have one or many seed traits.';
--rollback COMMENT ON CONSTRAINT seed_trait_variable_id_fk ON germplasm.seed_trait IS 'A seed trait refers to one variable. A variable can be referred by one or many seed traits.';
--rollback 
--rollback ALTER INDEX germplasm.seed_data_creator_id_idx RENAME TO seed_trait_creator_id_idx;
--rollback ALTER INDEX germplasm.seed_data_is_void_idx RENAME TO seed_trait_is_void_idx;
--rollback ALTER INDEX germplasm.seed_data_modifier_id_idx RENAME TO seed_trait_modifier_id_idx;
--rollback ALTER INDEX germplasm.seed_data_seed_id_data_qc_code_idx RENAME TO seed_trait_seed_id_data_qc_code_idx;
--rollback ALTER INDEX germplasm.seed_data_seed_id_idx RENAME TO seed_trait_seed_id_idx;
--rollback ALTER INDEX germplasm.seed_data_seed_id_variable_id_data_value_idx RENAME TO seed_trait_seed_id_variable_id_data_value_idx;
--rollback ALTER INDEX germplasm.seed_data_seed_id_variable_id_idx RENAME TO seed_trait_seed_id_variable_id_idx;
--rollback ALTER INDEX germplasm.seed_data_variable_id_idx RENAME TO seed_trait_variable_id_idx;
--rollback 
--rollback COMMENT ON INDEX germplasm.seed_trait_seed_id_data_qc_code_idx IS 'A seed trait can be retrieved by its data QC code within a seed.';
--rollback COMMENT ON INDEX germplasm.seed_trait_seed_id_variable_id_data_value_idx IS 'A seed trait can be retrieved by its variable and data value within a seed.';
--rollback COMMENT ON INDEX germplasm.seed_trait_seed_id_variable_id_idx IS 'A seed trait can be retrieved by its variable within a seed.';
--rollback 
--rollback ALTER TRIGGER seed_data_event_log_tgr ON germplasm.seed_trait RENAME TO seed_trait_event_log_tgr;
--rollback ALTER TABLE germplasm.seed_trait ALTER COLUMN data_value TYPE integer USING (data_value::integer);