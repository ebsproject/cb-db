--liquibase formatted sql

--changeset postgres:add_foreign_key_constraint_for_package_log_id_in_planting_instruction_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-484 Add foreign key constraint for package_log_id in planting_instruction table



-- set package_log_id to null for fixture data
UPDATE
	experiment.planting_instruction
SET
	package_log_id = NULL
WHERE
	package_log_id NOT IN (SELECT id FROM germplasm.package_log);



-- add constraint
ALTER TABLE
    experiment.planting_instruction
ADD CONSTRAINT
    planting_instruction_package_log_id_fkey
FOREIGN KEY
    (package_log_id)
REFERENCES
    germplasm.package_log(id)
ON DELETE RESTRICT
ON UPDATE CASCADE;



-- revert changes
--rollback ALTER TABLE experiment.planting_instruction DROP CONSTRAINT planting_instruction_package_log_id_fkey;
--rollback 
--rollback UPDATE
--rollback     experiment.planting_instruction AS pi
--rollback SET
--rollback     package_log_id = t.package_log_id
--rollback FROM (
--rollback     VALUES
--rollback         (1107142, 2027411),
--rollback         (322639, 563397),
--rollback         (938247, 1670896),
--rollback         (938246, 1670897),
--rollback         (938232, 1670911),
--rollback         (938220, 1670923),
--rollback         (938211, 1670932),
--rollback         (938321, 1670820),
--rollback         (938314, 1670827),
--rollback         (938313, 1670828),
--rollback         (938377, 1670765),
--rollback         (938366, 1670776),
--rollback         (938365, 1670777),
--rollback         (938361, 1670781),
--rollback         (938358, 1670784),
--rollback         (938334, 1670807),
--rollback         (938433, 1670709),
--rollback         (938493, 1670650),
--rollback         (938488, 1670655),
--rollback         (938484, 1670659),
--rollback         (938483, 1670660),
--rollback         (938783, 1670643),
--rollback         (938781, 1670645),
--rollback         (938777, 1670649),
--rollback         (1107050, 2027320),
--rollback         (1107051, 2027321),
--rollback         (1107053, 2027323),
--rollback         (1107083, 2027353),
--rollback         (1107091, 2027360),
--rollback         (1107095, 2027364),
--rollback         (1107097, 2027366),
--rollback         (1107098, 2027367),
--rollback         (1107103, 2027372),
--rollback         (1107104, 2027373),
--rollback         (1107139, 2027408),
--rollback         (1107143, 2027412),
--rollback         (1107144, 2027413),
--rollback         (1107148, 2027417),
--rollback         (938309, 1670832),
--rollback         (938307, 1670834),
--rollback         (938394, 1670748),
--rollback         (938392, 1670750),
--rollback         (938391, 1670751),
--rollback         (938390, 1670752),
--rollback         (938387, 1670755),
--rollback         (938298, 1670843),
--rollback         (938296, 1670845),
--rollback         (938383, 1670759),
--rollback         (1107056, 2027326),
--rollback         (1107058, 2027328),
--rollback         (1107059, 2027329),
--rollback         (938476, 1670667),
--rollback         (1107062, 2027332),
--rollback         (1107069, 2027339),
--rollback         (1107071, 2027341),
--rollback         (1107075, 2027345),
--rollback         (938469, 1670674),
--rollback         (938467, 1670676),
--rollback         (1107076, 2027346),
--rollback         (1107105, 2027374),
--rollback         (1107108, 2027377),
--rollback         (1107109, 2027378),
--rollback         (938460, 1670683),
--rollback         (938459, 1670684),
--rollback         (1107151, 2027420),
--rollback         (1107152, 2027421),
--rollback         (938447, 1670696),
--rollback         (1107153, 2027422),
--rollback         (1107054, 2027324),
--rollback         (1107155, 2027424),
--rollback         (1107156, 2027425),
--rollback         (1107110, 2027379),
--rollback         (1107111, 2027380),
--rollback         (1107112, 2027381),
--rollback         (1107113, 2027382),
--rollback         (1107115, 2027384),
--rollback         (1107116, 2027385),
--rollback         (1107117, 2027386),
--rollback         (1107118, 2027387),
--rollback         (1107120, 2027389),
--rollback         (1107122, 2027391),
--rollback         (1107125, 2027394),
--rollback         (1107126, 2027395),
--rollback         (1107128, 2027397),
--rollback         (1107129, 2027398),
--rollback         (1107131, 2027400),
--rollback         (1107132, 2027401),
--rollback         (1107133, 2027402),
--rollback         (1107135, 2027404),
--rollback         (1107138, 2027407),
--rollback         (938292, 1670849),
--rollback         (938290, 1670851),
--rollback         (938288, 1670853),
--rollback         (938273, 1670871),
--rollback         (938332, 1670809),
--rollback         (938330, 1670811),
--rollback         (938428, 1670714),
--rollback         (938426, 1670716),
--rollback         (938423, 1670719),
--rollback         (938405, 1670737),
--rollback         (938404, 1670738),
--rollback         (938402, 1670740),
--rollback         (938397, 1670745)
--rollback ) AS t
--rollback     (id, package_log_id)
--rollback WHERE
--rollback     pi.id = t.id
--rollback ;