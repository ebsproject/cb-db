--liquibase formatted sql

--changeset postgres:add_index_to_germplasm_type_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-524 Add index to germplasm_type column



-- add index
CREATE INDEX
    germplasm_germplasm_type_idx
ON
    germplasm.germplasm
USING
    btree (
        germplasm_type
    )
;



-- revert changes
--rollback DROP INDEX germplasm.germplasm_germplasm_type_idx;



--changeset postgres:add_index_to_germplasm_normalized_name_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-524 Add index to germplasm_normalized_name column



-- add index
CREATE INDEX
    germplasm_germplasm_normalized_name_idx
ON
    germplasm.germplasm
USING
    btree (
        germplasm_normalized_name
    )
;



-- revert changes
--rollback DROP INDEX germplasm.germplasm_germplasm_normalized_name_idx;
