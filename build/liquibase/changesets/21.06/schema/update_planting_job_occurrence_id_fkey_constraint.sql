--liquibase formatted sql

--changeset postgres:update_planting_job_occurrence_id_fkey_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-499 Update planting_job_occurrence_id_fkey constraint



ALTER TABLE 
    experiment.planting_job_entry 
DROP CONSTRAINT 
    planting_job_occurrence_id_fkey;

ALTER TABLE experiment.planting_job_entry
    ADD CONSTRAINT planting_job_occurrence_id_fkey FOREIGN KEY (planting_job_occurrence_id)
    REFERENCES experiment.planting_job_occurrence (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE RESTRICT;



--rollback ALTER TABLE experiment.planting_job_entry DROP CONSTRAINT planting_job_occurrence_id_fkey;

--rollback ALTER TABLE experiment.planting_job_entry
--rollback     ADD CONSTRAINT planting_job_occurrence_id_fkey FOREIGN KEY (planting_job_occurrence_id)
--rollback     REFERENCES experiment.occurrence (id) MATCH SIMPLE
--rollback     ON UPDATE CASCADE
--rollback     ON DELETE RESTRICT;