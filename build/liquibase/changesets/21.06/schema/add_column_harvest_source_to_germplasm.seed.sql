--liquibase formatted sql

--changeset postgres:add_column_harvest_source_to_germplasm.seed context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-489 Add column harvest_source to germplasm.seed



ALTER TABLE germplasm.seed
    ADD COLUMN harvest_source varchar(32),
    ADD CONSTRAINT seed_harvest_source_chk CHECK (harvest_source::varchar = ANY (ARRAY['plot'::varchar, 'cross'::varchar]));

COMMENT ON COLUMN germplasm.seed.harvest_source
    IS 'Harvest Source: Entity where the seed was harvested [SEED_HARVSRC]';



--rollback ALTER TABLE germplasm.seed DROP COLUMN harvest_source;