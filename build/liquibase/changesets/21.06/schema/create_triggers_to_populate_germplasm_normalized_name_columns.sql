--liquibase formatted sql

--changeset postgres:create_trigger_to_populate_normalized_names_in_germplasm.germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-465 Create trigger to populate normalized names in germplasm.germplasm table



-- declare trigger function
CREATE OR REPLACE FUNCTION
    germplasm.populate_germplasm_normalized_name()
RETURNS
    TRIGGER
AS $func$
DECLARE
    
BEGIN
    -- this trigger must be ran on this particular event at this point in time
    IF (TG_WHEN <> 'BEFORE' OR NOT(ARRAY[TG_OP] <@ ARRAY['INSERT', 'UPDATE'])) THEN
        RAISE EXCEPTION 'ERROR Trigger function populate_germplasm_normalized_name() may only run on BEFORE INSERT OR UPDATE on germplasm.germplasm';
    END IF;
    
    IF OLD.designation IS NULL OR OLD.designation <> NEW.designation THEN
        -- normalize designation if there are changes
        NEW.germplasm_normalized_name = platform.normalize_text(NEW.designation);
    END IF;
    
    -- return updated record
    RETURN NEW;
END; $func$
LANGUAGE
    plpgsql
;


-- add trigger to table
CREATE TRIGGER
    germplasm_populate_germplasm_normalized_name_tgr
BEFORE
    INSERT OR UPDATE
ON
    germplasm.germplasm
FOR EACH ROW EXECUTE PROCEDURE
    germplasm.populate_germplasm_normalized_name()
;



-- revert changes
--rollback DROP TRIGGER germplasm_populate_germplasm_normalized_name_tgr ON germplasm.germplasm;
--rollback DROP FUNCTION germplasm.populate_germplasm_normalized_name();



--changeset postgres:create_trigger_to_populate_normalized_names_in_germplasm.germplasm_name_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-465 Create trigger to populate normalized names in germplasm.germplasm_name table



-- declare trigger function
CREATE OR REPLACE FUNCTION
    germplasm.populate_germplasm_name_normalized_name()
RETURNS
    TRIGGER
AS $func$
DECLARE
    
BEGIN
    -- this trigger must be ran on this particular event at this point in time
    IF (TG_WHEN <> 'BEFORE' OR NOT(ARRAY[TG_OP] <@ ARRAY['INSERT', 'UPDATE'])) THEN
        RAISE EXCEPTION 'ERROR Trigger function populate_germplasm_name_normalized_name() may only run on BEFORE INSERT OR UPDATE on germplasm.germplasm_name';
    END IF;
    
    IF OLD.name_value IS NULL OR OLD.name_value <> NEW.name_value THEN
        -- normalize name_value if there are changes
        NEW.germplasm_normalized_name = platform.normalize_text(NEW.name_value);
    END IF;
    
    -- return updated record
    RETURN NEW;
END; $func$
LANGUAGE
    plpgsql
;


-- add trigger to table
CREATE TRIGGER
    germplasm_populate_germplasm_name_normalized_name_tgr
BEFORE
    INSERT OR UPDATE
ON
    germplasm.germplasm_name
FOR EACH ROW EXECUTE PROCEDURE
    germplasm.populate_germplasm_name_normalized_name()
;



-- revert changes
--rollback DROP TRIGGER germplasm_populate_germplasm_name_normalized_name_tgr ON germplasm.germplasm_name;
--rollback DROP FUNCTION germplasm.populate_germplasm_name_normalized_name();
