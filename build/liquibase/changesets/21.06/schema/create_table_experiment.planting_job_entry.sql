--liquibase formatted sql

--changeset postgres:create_table_experiment.planting_job_entry context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-487 Create table experiment.planting_job_entry



-- Create table
CREATE TABLE experiment.planting_job_entry (
	id serial NOT NULL,
    planting_job_occurrence_id integer NOT NULL,
    entry_id integer NOT NULL,
    is_sufficient boolean NOT NULL DEFAULT false,
    is_replaced boolean NOT NULL DEFAULT false,
    replacement_type varchar(32),
    replacement_germplasm_id integer,
    replacement_package_id integer,
    creator_id integer NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modifier_id integer NULL,
	modification_timestamp timestamp NULL,
	is_void bool NOT NULL DEFAULT false,
	notes text NULL,
	event_log jsonb NULL,
	CONSTRAINT planting_job_entry_id_pk PRIMARY KEY (id)
);

COMMENT ON TABLE experiment.planting_job_entry
    IS 'Planting Job Entry: Entry managed in a packing or planting job transaction [PJOBENT]';

COMMENT ON COLUMN experiment.planting_job_entry.id
    IS 'Planting Job Entry ID: Database identifier of the planting job entry [PJOBENT_ID]';

COMMENT ON COLUMN experiment.planting_job_entry.planting_job_occurrence_id
    IS 'Planting Job Occurrence ID: Reference to the planting job and occurrence of the entry [PJOBENT_PJOBOCC_ID]';

COMMENT ON COLUMN experiment.planting_job_entry.entry_id
    IS 'Entry ID: Reference to the entry included in the planting job occurrence [PJOBENT_ENT_ID]';

COMMENT ON COLUMN experiment.planting_job_entry.is_sufficient
    IS 'Is Sufficient: Indicator whether the required package quantity for the entry is sufficient or not [PJOBENT_ISSUFFICIENT]';

COMMENT ON COLUMN experiment.planting_job_entry.is_replaced
    IS 'Is Replaced: Indicator whether the entry is replaced with another or not [PJOBENT_ISREPLACED]';

COMMENT ON COLUMN experiment.planting_job_entry.replacement_type
    IS 'Replacement Type: Type of entry replacement {Replace with check, Replace with filler, Replace with package} [PJOBENT_REPLACEMENTTYPE]';

COMMENT ON COLUMN experiment.planting_job_entry.replacement_germplasm_id
    IS 'Replacement Germplasm ID: Reference to the new germplasm that replaced the original germplasm of the entry [PJOBENT_REPLACEMENTGE_ID]';

COMMENT ON COLUMN experiment.planting_job_entry.replacement_package_id
    IS 'Replacement Package ID: Reference to the new package that replaced the original package of the entry [PJOBENT_REPLACEMENTPKG_ID]';

COMMENT ON COLUMN experiment.planting_job_entry.creator_id
    IS 'Creator ID: Reference to the person who created the record [CPERSON]';

COMMENT ON COLUMN experiment.planting_job_entry.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';

COMMENT ON COLUMN experiment.planting_job_entry.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';

COMMENT ON COLUMN experiment.planting_job_entry.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';

COMMENT ON COLUMN experiment.planting_job_entry.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';

COMMENT ON COLUMN experiment.planting_job_entry.notes
    IS 'Notes: Technical details about the record [NOTES]';

CREATE INDEX planting_job_entry_entry_id_idx ON experiment.planting_job_entry USING btree (entry_id);
CREATE INDEX planting_job_entry_replacement_germplasm_id_idx ON experiment.planting_job_entry USING btree (replacement_germplasm_id);
CREATE INDEX planting_job_entry_replacement_package_id_idx ON experiment.planting_job_entry USING btree (replacement_package_id);
CREATE INDEX planting_job_entry_creator_id_idx ON experiment.planting_job_entry USING btree (creator_id);
CREATE INDEX planting_job_entry_is_void_idx ON experiment.planting_job_entry USING btree (is_void);
CREATE INDEX planting_job_entry_modifier_id_idx ON experiment.planting_job_entry USING btree (modifier_id);

-- Add table triggers
CREATE trigger planting_job_entry_event_log_tgr BEFORE
INSERT OR UPDATE ON 
    experiment.planting_job_entry for each row execute function platform.log_record_event();

--  Add foreign keys
ALTER TABLE experiment.planting_job_entry ADD CONSTRAINT planting_job_occurrence_id_fkey FOREIGN KEY (planting_job_occurrence_id) REFERENCES experiment.occurrence(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_entry ADD CONSTRAINT planting_job_entry_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES experiment.entry(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_entry ADD CONSTRAINT planting_job_entry_replacement_germplasm_id_fkey FOREIGN KEY (replacement_germplasm_id) REFERENCES germplasm.germplasm(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_entry ADD CONSTRAINT planting_job_entry_replacement_package_id_fkey FOREIGN KEY (replacement_package_id) REFERENCES germplasm.package(id) ON DELETE RESTRICT ON UPDATE CASCADE;



--rollback DROP TABLE experiment.planting_job_entry;