--liquibase formatted sql

--changeset postgres:drop_not_null_constraint_on_germplasm_normalized_name_column_in_germplasm.germplasm_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-465 Drop not null constraint in germplasm_normalized_name column in germplasm.germplasm table



-- drop not null constraint
ALTER TABLE
    germplasm.germplasm
ALTER COLUMN
    germplasm_normalized_name
    DROP NOT NULL
;


-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm
--rollback ALTER COLUMN
--rollback     germplasm_normalized_name
--rollback     SET NOT NULL
--rollback ;



--changeset postgres:drop_not_null_constraint_on_germplasm_normalized_name_column_in_germplasm.germplasm_name_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-465 Drop not null constraint in germplasm_normalized_name column in germplasm.germplasm_name table



-- drop not null constraint
ALTER TABLE
    germplasm.germplasm_name
ALTER COLUMN
    germplasm_normalized_name
    DROP NOT NULL
;


-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.germplasm_name
--rollback ALTER COLUMN
--rollback     germplasm_normalized_name
--rollback     SET NOT NULL
--rollback ;
