--liquibase formatted sql

--changeset postgres:add_column_envelope_count_to_experiment.planting_job_entry_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-503 Add column envelope_count to experiment.planting_job_entry table



-- add column to table
ALTER TABLE experiment.planting_job_entry
    ADD COLUMN envelope_count integer NOT NULL DEFAULT 0;

COMMENT ON COLUMN experiment.planting_job_entry.envelope_count
    IS 'Envelope Count: Number of envelopes needed for an entry in a planting job transaction [PJOBENT_ENVLPCOUNT]';



--rollback ALTER TABLE experiment.planting_job_entry DROP COLUMN envelope_count;