--liquibase formatted sql

--changeset postgres:populate_variable_document_with_trigger_in_master.variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-832 Populate variable_document in master.variable



UPDATE 
    master.variable
SET
    modification_timestamp = now();



--rollback SELECT NULL;