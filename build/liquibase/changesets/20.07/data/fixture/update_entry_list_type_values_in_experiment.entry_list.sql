--liquibase formatted sql

--changeset postgres:update_entry_list_type_values_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-979 Update entry_list_type values in experiment.entry_list



UPDATE
    experiment.entry_list
SET
    entry_list_type = 'cross list'
WHERE
    entry_list_type = 'cross-list';
    
UPDATE
    experiment.entry_list
SET
    entry_list_type = 'entry list'
WHERE
    entry_list_type = 'entry-list';
    
UPDATE
    experiment.entry_list
SET
    entry_list_type = 'parent list'
WHERE
    entry_list_type = 'parent-list';



--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback    entry_list_type = 'cross-list'
--rollback WHERE
--rollback     entry_list_type = 'cross list';
--rollback     
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_type = 'entry-list'
--rollback WHERE
--rollback     entry_list_type = 'entry list';
--rollback     
--rollback UPDATE
--rollback     experiment.entry_list
--rollback SET
--rollback     entry_list_type = 'parent-list'
--rollback WHERE
--rollback     entry_list_type = 'parent list';