--liquibase formatted sql

--changeset postgres:update_occurrence_status_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-981 Update occurrence_status in experiment.occurrence



UPDATE
    experiment.occurrence
SET
    occurrence_status = 'planted'
WHERE
    occurrence_status = 'committed';



--rollback UPDATE
--rollback     experiment.occurrence
--rollback SET
--rollback     occurrence_status = 'committed'
--rollback WHERE
--rollback     occurrence_status = 'planted';