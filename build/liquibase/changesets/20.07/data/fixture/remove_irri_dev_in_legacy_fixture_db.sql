--liquibase formatted sql

--changeset postgres:remove_irri_dev_in_legacy_fixture_db context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-802 Remove IRRI dev in legacy fixture db



UPDATE
    tenant.person
SET
    username = 'h.tamsin',
    first_name = 'Henry',
    last_name = 'Tamsin',
    person_name = 'Tamsin, Henry',
    email = 'h.tamsin@gmail.com',
    person_type = 'user'
WHERE
    id = 4;

UPDATE
    tenant.person
SET
    username = 'a.carlson',
    first_name = 'Ansh',
    last_name = 'Carlson',
    person_name = 'Carlson, Ansh',
    email = 'a.carlson@gmail.com',
    person_type = 'user'
WHERE
    id = 6;

UPDATE
    tenant.person
SET
    username = 'a.ramos',
    first_name = 'Asiya',
    last_name = 'Ramos',
    person_name = 'Ramos, Asiya',
    email = 'a.ramos@gmail.com',
    person_type = 'user'
WHERE
    id = 7;

UPDATE
    tenant.person
SET
    username = 'k.khadija',
    first_name = 'Khadija',
    last_name = 'Goff',
    person_name = 'Goff, Khadija',
    email = 'k.khadija@gmail.com',
    person_type = 'user'
WHERE
    id = 10;

UPDATE 
    experiment.occurrence_data
SET
    data_value = 'Tamsin, Henry'
WHERE
    data_value = 'Gallardo, Larise';

--rollback UPDATE
--rollback     tenant.person
--rollback SET
--rollback     username = 'l.gallardo',
--rollback     first_name = 'Larise',
--rollback     last_name = 'Gallardo',
--rollback     person_name = 'Gallardo, Larise',
--rollback     email = 'l.gallardo@irri.org',
--rollback     person_type = 'admin'
--rollback WHERE
--rollback     id = 4;

--rollback UPDATE
--rollback     tenant.person
--rollback SET
--rollback     username = 'a.flores',
--rollback     first_name = 'Argem Gerald',
--rollback     last_name = 'Flores',
--rollback     person_name = 'Flores, Argem Gerald',
--rollback     email = 'a.flores@irri.org',
--rollback     person_type = 'admin'
--rollback WHERE
--rollback     id = 6;

--rollback UPDATE
--rollback     tenant.person
--rollback SET
--rollback     username = 'jp.ramos',
--rollback     first_name = 'Jahzeel',
--rollback     last_name = 'Ramos',
--rollback     person_name = 'Ramos, Jahzeel',
--rollback     email = 'jp.ramos@irri.org',
--rollback     person_type = 'admin'
--rollback WHERE
--rollback     id = 7;

--rollback UPDATE
--rollback     tenant.person
--rollback SET
--rollback     username = 'e.tenorio',
--rollback     first_name = 'Eugenia',
--rollback     last_name = 'Tenorio',
--rollback     person_name = 'Tenorio, Eugenia',
--rollback     email = 'e.tenorio@irri.org',
--rollback     person_type = 'admin'
--rollback WHERE
--rollback     id = 10;

--rollback UPDATE 
--rollback     experiment.occurrence_data
--rollback SET
--rollback     data_value = 'Gallardo, Larise'
--rollback WHERE
--rollback     data_value = 'Tamsin, Henry';