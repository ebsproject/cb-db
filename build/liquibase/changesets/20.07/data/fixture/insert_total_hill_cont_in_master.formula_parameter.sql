--liquibase formatted sql

--changeset postgres:insert_total_hill_cont_in_master.formula_parameter.sql context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-991 Insert TOTAL_HILL_CONT in master.formula_parameter



INSERT INTO 
    master.formula_parameter
        (id,formula_id,param_variable_id,data_level,creator_id,result_variable_id)
VALUES(
    503,
    6,
    293,
    'plot',
    1,
    412
);

SELECT SETVAL('master.formula_parameter_id_seq', COALESCE(MAX(id), 1)) FROM master.formula_parameter;



--rollback DELETE FROM master.formula_parameter WHERE id = 503;
--rollback SELECT SETVAL('master.formula_parameter_id_seq', COALESCE(MAX(id), 1)) FROM master.formula_parameter;