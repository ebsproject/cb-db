--liquibase formatted sql

--changeset postgres:populate_application_document_with_trigger_in_platform.application context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-833 Populate application_document in platform.application



UPDATE 
    platform.application
SET
    modification_timestamp = now();



--rollback SELECT NULL;