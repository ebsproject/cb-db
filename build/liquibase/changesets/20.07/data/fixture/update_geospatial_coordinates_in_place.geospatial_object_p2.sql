--liquibase formatted sql

--changeset postgres:update_geospatial_coordinates_in_place.geospatial_object_p2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: EBS-980 Update geospatial_coordinates in place.geospatial_object p2



--update IRRI farms
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=836;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=843;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=885;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=893;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=894;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=895;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=903;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=907;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=916;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=919;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1003;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1004;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1005;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1015;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1016;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1017;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1036;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1037;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1047;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1061;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1073;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1099;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1100;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1105;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1110;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1114;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1115;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1124;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=1125;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=2632;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=2633;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.2556636,14.16739)' WHERE id=2634;

--update 10001 PA
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=71;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=72;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=73;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=74;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=164;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=375;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=804;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=815;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=816;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1504;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1507;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1508;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1540;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1955;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=1956;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2026;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2059;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2177;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2178;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2179;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=2531;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3026;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3030;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3041;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3043;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3370;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3371;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3373;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3374;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3375;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.255438327789,14.1686890058262)',altitude=31.7702732086182 WHERE id=3376;

--update 10002_PA
UPDATE place.geospatial_object SET geospatial_coordinates='(120.9361332,15.7350678)',altitude=79.9904174804688 WHERE id=1793;
UPDATE place.geospatial_object SET geospatial_coordinates='(120.9361332,15.7350678)',altitude=79.9904174804688 WHERE id=1796;
UPDATE place.geospatial_object SET geospatial_coordinates='(120.9361332,15.7350678)',altitude=79.9904174804688 WHERE id=1888;
UPDATE place.geospatial_object SET geospatial_coordinates='(120.9361332,15.7350678)',altitude=79.9904174804688 WHERE id=1990;
UPDATE place.geospatial_object SET geospatial_coordinates='(120.9361332,15.7350678)',altitude=79.9904174804688 WHERE id=1991;

--update 10003_PA
UPDATE place.geospatial_object SET geospatial_coordinates='(125.586667,9.053611)',altitude=13.5693368911743 WHERE id=1988;
UPDATE place.geospatial_object SET geospatial_coordinates='(125.586667,9.053611)',altitude=13.5693368911743 WHERE id=1989;

--update 10005_PA
UPDATE place.geospatial_object SET geospatial_coordinates='(121.583333,16.883333)',altitude=84.3838119506836 WHERE id=1981;
UPDATE place.geospatial_object SET geospatial_coordinates='(121.583333,16.883333)',altitude=84.3838119506836 WHERE id=1982;

--update 10009_PA
UPDATE place.geospatial_object SET geospatial_coordinates='(124.1435427,9.8499911)',altitude=231.687225341797 WHERE id=1794;
UPDATE place.geospatial_object SET geospatial_coordinates='(124.1435427,9.8499911)',altitude=231.687225341797 WHERE id=1868;



--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=836;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=843;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=885;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=893;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=894;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=895;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=903;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=907;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=916;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=919;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1003;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1004;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1005;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1015;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1016;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1017;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1036;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1037;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1047;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1061;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1073;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1099;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1100;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1105;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1110;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1114;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1115;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1124;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=1125;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=2632;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=2633;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL WHERE id=2634;

--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=71;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=72;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=73;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=74;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=164;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=375;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=804;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=815;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=816;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1504;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1507;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1508;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1540;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1955;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1956;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2026;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2059;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2177;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2178;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2179;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=2531;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3026;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3030;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3041;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3043;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3370;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3371;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3373;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3374;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3375;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=3376;

--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1793;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1796;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1888;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1990;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1991;

--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1988;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1989;

--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1981;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1982;

--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1794;
--rollback UPDATE place.geospatial_object SET geospatial_coordinates=NULL,altitude=NULL WHERE id=1868;