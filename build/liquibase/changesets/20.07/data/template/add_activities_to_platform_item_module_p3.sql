--liquibase formatted sql

--changeset postgres:add_activities_to_platform_item_module_p3 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add protocols tab



INSERT INTO 
  platform.item_module
    (item_id,module_id,creator_id,notes)
SELECT
	id AS item_id,
	CASE
    WHEN abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT' then (SELECT id FROM platform.module WHERE abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD')
		WHEN abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT' then (SELECT id FROM platform.module WHERE abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD')
   	ELSE (SELECT id FROM platform.module WHERE abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD') 
  END as module_id,
	1,
	'added by j.antonio ' || now()
FROM
	master.item
WHERE
	abbrev 
IN 
  (
    'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT',
    'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'
  );



--rollback DELETE FROM
--rollback   platform.item_module
--rollback WHERE
--rollback   item_id 
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       master.item 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT',
--rollback         'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'
--rollback 	    )
--rollback   )
--rollback AND
--rollback   module_id
--rollback IN
--rollback   (
--rollback     SELECT 
--rollback       id 
--rollback     FROM 
--rollback       platform.module 
--rollback 	  WHERE 
--rollback       abbrev 
--rollback     IN 
--rollback       (
--rollback         'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD',
--rollback         'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD'
--rollback 	    )
--rollback   );