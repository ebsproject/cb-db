--liquibase formatted sql

--changeset postgres:update_parent_list_to_entry_list_of_seed_increase_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update parent list to entry list of seed increase



UPDATE 
    master.item 
SET 
    (name,description,display_name) = ('Specify Entry List','Specify Entry List', 'Entry List') 
WHERE 
    abbrev = 'EXPT_SEED_INCREASE_ENTRY_LIST_ACT';

UPDATE 
    master.item 
SET 
    (name,description,display_name) = ('Specify Entry List','Specify Entry List', 'Entry List') 
WHERE 
    abbrev = 'EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name,description,display_name) = ('Specify Parent List','Specify Parent List', 'Parent List') 
--rollback WHERE 
--rollback     abbrev = 'EXPT_SEED_INCREASE_ENTRY_LIST_ACT';

--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     (name,description,display_name) = ('Specify Parent List','Specify Parent List', 'Parent List') 
--rollback WHERE 
--rollback     abbrev = 'EXPT_SELECTION_ADVANCEMENT_ENTRY_LIST_ACT';