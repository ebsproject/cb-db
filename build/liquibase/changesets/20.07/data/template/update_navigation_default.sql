--liquibase formatted sql

--changeset postgres:update_navigation_default context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update navigation default



UPDATE 
	platform.space 
SET 
	menu_data = 
	'{
		"left_menu_items": [{
				"name": "experiment-creation",
				"label": "Experiment creation",
				"appAbbrev": "EXPERIMENT_CREATION"
			},
			{
				"name": "experiment-manager",
				"label": "Experiment manager",
				"appAbbrev": "OCCURRENCES"
			}, {
				"name": "data-collection-qc",
				"items": [{
					"name": "data-collection-qc-quality-control",
					"label": "Quality control",
					"appAbbrev": "QUALITY_CONTROL"
				}],
				"label": "Data collection & QC"
			}, {
				"name": "seeds",
				"items": [{
					"name": "seeds-find-seeds",
					"label": "Find seeds",
					"appAbbrev": "FIND_SEEDS"
				}, {
					"name": "seeds-harvest-manager",
					"label": "Harvest manager",
					"appAbbrev": "HARVEST_MANAGER"
				}],
				"label": "Seeds"
			}
		]
	}'
WHERE
	abbrev = 'DEFAULT';



--rollback UPDATE 
--rollback 	platform.space 
--rollback SET 
--rollback 	menu_data = 
--rollback 	'
--rollback 		{
--rollback 			"left_menu_items": [
--rollback 				{
--rollback 					"name": "experiment-creation",
--rollback 					"items": [
--rollback 						{
--rollback 							"name": "experiment-creation-experiments",
--rollback 							"label": "Experiments",
--rollback 							"appAbbrev": "EXPERIMENT_CREATION"
--rollback 						},
--rollback 						{
--rollback 							"name": "experiment-creation-occurrences",
--rollback 							"label": "Occurrences",
--rollback 							"appAbbrev": "OCCURRENCES"
--rollback 						},
--rollback 						{
--rollback 							"name": "experiment-creation-locations",
--rollback 							"label": "Locations",
--rollback 							"appAbbrev": "LOCATIONS"
--rollback 						}
--rollback 					],
--rollback 					"label": "Experiments"
--rollback 				},
--rollback 				{
--rollback 					"name": "data-collection-qc",
--rollback 					"items": [
--rollback 						{
--rollback 							"name": "data-collection-qc-quality-control",
--rollback 							"label": "Quality control",
--rollback 							"appAbbrev": "QUALITY_CONTROL"
--rollback 						}
--rollback 					],
--rollback 					"label": "Data collection & QC"
--rollback 				},
--rollback 				{
--rollback 					"name": "seeds",
--rollback 					"items": [
--rollback 						{
--rollback 							"name": "seeds-find-seeds",
--rollback 							"label": "Find seeds",
--rollback 							"appAbbrev": "FIND_SEEDS"
--rollback 						},
--rollback 						{
--rollback 							"name": "seeds-harvest-manager",
--rollback 							"label": "Harvest manager",
--rollback 							"appAbbrev": "HARVEST_MANAGER"
--rollback 						}
--rollback 					],
--rollback 					"label": "Seeds"
--rollback 				}
--rollback 			]
--rollback 		}
--rollback 	'
--rollback WHERE
--rollback 	abbrev = 'DEFAULT';