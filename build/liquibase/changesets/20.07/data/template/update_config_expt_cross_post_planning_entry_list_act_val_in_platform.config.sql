--liquibase formatted sql

--changeset postgres:update_config_expt_cross_post_planning_entry_list_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update config EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL in platform.config



UPDATE 
  platform.config 
SET 
  config_value = 
  '
    {
        "Name": "Required and default entry level metadata variables for cross pre-planning data process",
        "Values": [
            {
                "disabled": false,
                "required": "required",
                "variable_abbrev": "ENTRY_ROLE",
                "allowed_values": [
                    "ENTRY_ROLE_FEMALE",
                    "ENTRY_ROLE_FEMALE_AND_MALE",
                    "ENTRY_ROLE_MALE"
                ],
                "display_name": "Parent Role",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint": "entries",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification"
            },
            {
                "disabled": false,
                "required": "required",
                "variable_abbrev": "HYBRID_ROLE",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint": "entry-data",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "metadata"
            },
            {
                "disabled": false,
                "variable_abbrev": "DESCRIPTION",
                "target_column": "",
                "secondary_target_column":"",
                "target_value":"",
                "api_resource_method" : "",
                "api_resource_endpoint": "entries",
                "api_resource_filter" : "",
                "api_resource_sort": "", 
                "variable_type" : "identification"
            }
        ]
    }
  ' 
WHERE abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';



--rollback UPDATE 
--rollback   platform.config 
--rollback SET 
--rollback   config_value = 
--rollback   '
--rollback     {
--rollback         "Name": "Required and default entry level metadata variables for cross post-planning data process",
--rollback         "Values": [
--rollback             {
--rollback                 "disabled": false,
--rollback                 "target_value": "",
--rollback                 "target_column": "",
--rollback                 "variable_type": "identification",
--rollback                 "variable_abbrev": "DESCRIPTION",
--rollback                 "api_resource_sort": "",
--rollback                 "api_resource_filter": "",
--rollback                 "api_resource_method": "",
--rollback                 "api_resource_endpoint": "entries",
--rollback                 "secondary_target_column": ""
--rollback             }
--rollback         ]
--rollback     }
--rollback   ' 
--rollback WHERE abbrev = 'EXPT_CROSS_POST_PLANNING_ENTRY_LIST_ACT_VAL';