--liquibase formatted sql

--changeset postgres:update_data_level_of_seed_related_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-825 Update data level of seed-related variables



-- update variables
UPDATE master.variable SET data_level = 'plot,entry; seed' WHERE id = '79';
UPDATE master.variable SET data_level = 'entry,plot; seed' WHERE id = '123';
UPDATE master.variable SET data_level = 'entry,plot; seed' WHERE id = '141';
UPDATE master.variable SET data_level = 'entry; seed' WHERE id = '594';
UPDATE master.variable SET data_level = 'entry,plot; seed' WHERE id = '595';
UPDATE master.variable SET data_level = 'entry; seed' WHERE id = '597';
UPDATE master.variable SET data_level = 'entry; seed' WHERE id = '661';



-- revert changes
--rollback UPDATE master.variable SET data_level = 'plot,entry' WHERE id = '79';
--rollback UPDATE master.variable SET data_level = 'entry,plot' WHERE id = '123';
--rollback UPDATE master.variable SET data_level = 'entry,plot' WHERE id = '141';
--rollback UPDATE master.variable SET data_level = 'entry' WHERE id = '594';
--rollback UPDATE master.variable SET data_level = 'entry,plot' WHERE id = '595';
--rollback UPDATE master.variable SET data_level = 'entry' WHERE id = '597';
--rollback UPDATE master.variable SET data_level = 'entry' WHERE id = '661';
