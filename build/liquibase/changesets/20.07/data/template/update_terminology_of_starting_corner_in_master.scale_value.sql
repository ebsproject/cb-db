--liquibase formatted sql

--changeset postgres:update_terminology_of_starting_corner_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-869 Update terminology of starting corner in master.scale_value



--update Northwest
UPDATE
    master.scale_value
SET
    value = 'Top Left',
    description = 'Top Left',
    abbrev = 'STARTING_CORNER_TOP_LEFT'
WHERE
    id = 2145;

--update Northeast
UPDATE
    master.scale_value
SET
    value = 'Top Right',
    description = 'Top Right',
    abbrev = 'STARTING_CORNER_TOP_RIGHT'
WHERE
    id = 2146;

--update Southwest
UPDATE
    master.scale_value
SET
    value = 'Bottom Left',
    description = 'Bottom Left',
    abbrev = 'STARTING_CORNER_BOTTOM_LEFT'
WHERE
    id = 2147;

--update Southeast
UPDATE
    master.scale_value
SET
    value = 'Bottom Right',
    description = 'Bottom Right',
    abbrev = 'STARTING_CORNER_BOTTOM_RIGHT'
WHERE
    id = 2148;



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Northwest',
--rollback     description = 'Northwest',
--rollback     abbrev = 'STARTING_CORNER_NORTHWEST'
--rollback WHERE
--rollback     id = 2145;

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Northeast',
--rollback     description = 'Northeast',
--rollback     abbrev = 'STARTING_CORNER_NORTHEAST'
--rollback WHERE
--rollback     id = 2146;

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Southwest',
--rollback     description = 'Southwest',
--rollback     abbrev = 'STARTING_CORNER_SOUTHWEST'
--rollback WHERE
--rollback     id = 2147;

--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'Southeast',
--rollback     description = 'Southeast',
--rollback     abbrev = 'STARTING_CORNER_SOUTHEAST'
--rollback WHERE
--rollback     id = 2148;