--liquibase formatted sql

--changeset postgres:hide_cross_parent_template context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Hide cross parent template



UPDATE 
    master.item 
SET 
    is_void = true 
WHERE 
    abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS';



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS';