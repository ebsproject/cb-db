--liquibase formatted sql

--changeset postgres:update_variable_labels_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update variable labels in master.variable 



UPDATE master.variable
	SET label = 'YEAR'
WHERE
	abbrev = 'YEAR';
	
UPDATE master.variable
	SET label = 'SEASON'
WHERE
	abbrev = 'SEASON';

UPDATE master.variable
	SET label = 'STAGE'
WHERE
	abbrev = 'EVALUATION_STAGE';
	
UPDATE master.variable
	SET label = 'EXPERIMENT'
WHERE
	abbrev = 'EXPERIMENT_NAME';

UPDATE master.variable
	SET label = 'PLOT NO.'
WHERE
	abbrev = 'PLOTNO';

UPDATE master.variable
	SET label = 'ENTRY NO.'
WHERE
	abbrev = 'ENTNO';

UPDATE master.variable
	SET label = 'ENTRY CODE'
WHERE
	abbrev = 'ENTCODE';
	
UPDATE master.variable
	SET label = 'PACKAGE QTY.'
WHERE
	abbrev = 'VOLUME';
	
UPDATE master.variable
	SET label = 'GERMPLASM'
WHERE
	abbrev = 'DESIGNATION';

UPDATE master.variable
	SET label = 'SEED'
WHERE
	abbrev = 'GID';



--rollback UPDATE master.variable
--rollback 	SET label = 'Evaluation Year'
--rollback WHERE
--rollback 	abbrev = 'YEAR';
--rollback 	
--rollback UPDATE master.variable
--rollback 	SET label = 'Evaluation Season'
--rollback WHERE
--rollback 	abbrev = 'SEASON';
--rollback 
--rollback UPDATE master.variable
--rollback 	SET label = 'Evaluation Stage'
--rollback WHERE
--rollback 	abbrev = 'EVALUATION_STAGE';
--rollback 	
--rollback UPDATE master.variable
--rollback 	SET label = 'Experiment Name'
--rollback WHERE
--rollback 	abbrev = 'EXPERIMENT_NAME';
--rollback 
--rollback UPDATE master.variable
--rollback 	SET label = 'PLOTNO'
--rollback WHERE
--rollback 	abbrev = 'PLOTNO';
--rollback 
--rollback UPDATE master.variable
--rollback 	SET label = 'ENTNO'
--rollback WHERE
--rollback 	abbrev = 'ENTNO';
--rollback 
--rollback UPDATE master.variable
--rollback 	SET label = 'ENTCODE'
--rollback WHERE
--rollback 	abbrev = 'ENTCODE';
--rollback 	
--rollback UPDATE master.variable
--rollback 	SET label = 'VOLUME'
--rollback WHERE
--rollback 	abbrev = 'VOLUME';
--rollback 	
--rollback UPDATE master.variable
--rollback 	SET label = 'DESIGNATION'
--rollback WHERE
--rollback 	abbrev = 'DESIGNATION';
--rollback 
--rollback UPDATE master.variable
--rollback 	SET label = 'Germplasm ID (GID)'
--rollback WHERE
--rollback 	abbrev = 'GID';