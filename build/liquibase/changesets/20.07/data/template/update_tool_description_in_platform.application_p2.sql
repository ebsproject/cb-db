--liquibase formatted sql

--changeset postgres:update_tool_description_in_platform.application_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-876 Update tool description in platform.application p2



UPDATE 
    platform.application
SET 
    description = 'A general search tool that currently supports the searching of germplasm, experiments, seed packages, and people.'
WHERE
    abbrev = 'SEARCH';


UPDATE 
    platform.application
SET 
    description = 'A tool to manage configurations used for seed inventories.'
WHERE
    abbrev = 'SEED_INVENTORY_CONFIG';

UPDATE 
    platform.application
SET 
    description = 'Lists enable users to group together packages, germplasm, traits, locations, or other entities.'
WHERE
    abbrev = 'LISTS';



--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = 'The general search tool. Currently supports searching of products, studies, seeds, and users.'
--rollback WHERE
--rollback     abbrev = 'SEARCH';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = 'The tool to manage configuration used in seed inventory.'
--rollback WHERE
--rollback     abbrev = 'SEED_INVENTORY_CONFIG';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = 'A list is an entity in B4R that is used to grouped together etiher packages, germplasm, or traits. These lists can then be used for creating an experiment. The list management tool provides an interface for users to be able to manage their lists.'
--rollback WHERE
--rollback     abbrev = 'LISTS';