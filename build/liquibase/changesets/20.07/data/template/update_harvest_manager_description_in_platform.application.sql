--liquibase formatted sql

--changeset postgres:update_harvest_manager_description_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update harvest_manager description in platform.application



UPDATE 
    platform.application
SET 
    description = 'The tool to facilitate the harvest activity of experiments. This automates creation of new germplasm, seedlots and packages according to a harvest method used.'
WHERE
    abbrev = 'HARVEST_MANAGER';



--rollback SELECT NULL;