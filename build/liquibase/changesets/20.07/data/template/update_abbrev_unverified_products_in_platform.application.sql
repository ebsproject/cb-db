--liquibase formatted sql

--changeset postgres:update_abbrev_unverified_products_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-949 Update abbrev UNVERIFIED_PRODUCTS in platform.application



UPDATE 
    platform.application
SET 
    abbrev = 'UNVERIFIED_PRODUCTS'
WHERE
    id = 43;



--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     abbrev = 'UNVERIFIED_PRODUTCS'
--rollback WHERE
--rollback     id = 43;