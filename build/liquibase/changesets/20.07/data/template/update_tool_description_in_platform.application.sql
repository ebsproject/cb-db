--liquibase formatted sql

--changeset postgres:update_tool_description_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-876 Update tool description in platform.application 



UPDATE 
    platform.application
SET 
    description = 'The user''s profile information that includes the user''s affiliation to groups, programs or teams and their roles'
WHERE
    abbrev = 'PROFILE';

UPDATE 
    platform.application
SET 
    description = 'The general search tool. Currently supports searching of products, studies, seeds, and users.'
WHERE
    abbrev = 'SEARCH';

UPDATE 
    platform.application
SET 
    description = 'The tool to manage the seasons for a specifc B4R intance. This provides the browse,  create, update and delete functions of seasons within the system.'
WHERE
    abbrev = 'MANAGE_SEASONS';

UPDATE 
    platform.application
SET 
    description = 'Allows user to customize dashboard according to their preferences. User can add, remove, reorder, rename, minimize or expand and change layout of widgets in the dashboard page.'
WHERE
    abbrev = 'CONFIGURE_DASHBOARD';

UPDATE 
    platform.application
SET 
    description = 'Quality Control Tool facilitates the validation of uploaded trait measurements from the field and the committing of validated data to the experiment.'
WHERE
    abbrev = 'QUALITY_CONTROL';

UPDATE 
    platform.application
SET 
    description = 'An experiment is a study or a set of studies that are conducted to test or generate germplasm for specific qualities. The Nursery experiment tool is used to create new nurseries such as Parent nurseries, Crossing nurseries, Seed increases and Purifications and with Trial experiment tool user can create a multisite study.'
WHERE
    abbrev = 'EXPERIMENT_CREATION';

UPDATE 
    platform.application
SET 
    description = 'Module for searching available seed packages and creating seed lists.'
WHERE
    abbrev = 'FIND_SEEDS';

UPDATE 
    platform.application
SET 
    description = 'The tool to manage configuration used in seed inventory.'
WHERE
    abbrev = 'SEED_INVENTORY_CONFIG';

UPDATE 
    platform.application
SET 
    action_label = 'Browse status of B4R services',
    description = 'This page provides information about the status for each of B4R''s services and how they are responding at the moment.'
WHERE
    abbrev = 'STATUS';

UPDATE 
    platform.application
SET 
    description = 'The tool to facitlite the harvest activity of experiments. This automates creation of new germplasm, seedlots and packages according to a harvest method used.'
WHERE
    abbrev = 'HARVEST_MANAGER';

UPDATE 
    platform.application
SET 
    description = 'A list is an entity in B4R that is used to grouped together etiher packages, germplasm, or traits. These lists can then be used for creating an experiment. The list management tool provides an interface for users to be able to manage their lists.'
WHERE
    abbrev = 'LISTS';

UPDATE 
    platform.application
SET 
    description = 'This tool provides an interface for users to browse and filter the available germplasm in the database. They can also create germplasm lists from the browser.'
WHERE
    abbrev = 'GERMPLASM_CATALOG';

UPDATE 
    platform.application
SET 
    description = 'The tool to manage the users of B4R. This provides the browse, create, update and delete functions of users in the system.'
WHERE
    abbrev = 'PERSONS';

UPDATE 
    platform.application
SET 
    description = 'Experiment manager allows users to browse, search, view, manage, monitor and delete experiments, occurrences and locations.'
WHERE
    abbrev = 'OCCURRENCES';

UPDATE 
    platform.application
SET 
    description = 'The list of variables'
WHERE
    abbrev = 'TRAITS';



--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'PROFILE';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'SEARCH';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'MANAGE_SEASONS';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'CONFIGURE_DASHBOARD';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'QUALITY_CONTROL';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_CREATION';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'FIND_SEEDS';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'SEED_INVENTORY_CONFIG';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     action_label = 'This page provides information about the status for each of B4R''s services and how they are responding at the moment.',
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'STATUS';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'HARVEST_MANAGER';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'LISTS';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_CATALOG';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'PERSONS';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'OCCURRENCES';
--rollback 
--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     description = NULL
--rollback WHERE
--rollback     abbrev = 'TRAITS';