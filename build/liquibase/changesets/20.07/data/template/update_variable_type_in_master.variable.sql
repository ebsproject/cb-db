--liquibase formatted sql

--changeset postgres:update_variable_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update variable type in master.variable



UPDATE master.variable SET type = 'observation' WHERE abbrev IN ('HV_METH_DISC','DATE_CROSSED');

UPDATE master.variable SET type = 'metadata' WHERE abbrev = 'HARVEST_DATE';



--rollback UPDATE master.variable SET type = 'metadata' WHERE abbrev IN ('HV_METH_DISC','DATE_CROSSED');

--rollback UPDATE master.variable SET type = 'observation' WHERE abbrev = 'HARVEST_DATE';