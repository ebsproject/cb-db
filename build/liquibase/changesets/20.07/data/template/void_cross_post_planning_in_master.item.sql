--liquibase formatted sql

--changeset postgres:void_cross_post_planning_in_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Void EXPT_CROSS_POST_PLANNING_DATA_PROCESS in master.item



UPDATE 
  master.item 
SET 
  is_void = true 
WHERE 
  abbrev = 'EXPT_CROSS_POST_PLANNING_DATA_PROCESS';



--rollback UPDATE 
--rollback   master.item 
--rollback SET 
--rollback   is_void = false 
--rollback WHERE 
--rollback   abbrev = 'EXPT_CROSS_POST_PLANNING_DATA_PROCESS';