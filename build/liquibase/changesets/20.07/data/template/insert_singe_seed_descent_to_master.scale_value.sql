--liquibase formatted sql

--changeset postgres:insert_singe_seed_descent_to_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Insert single seed descent to master.scale_value
--validCheckSum: 8:0d814b8994205123b469d2eb446c80e9



INSERT INTO
    master.scale_value (
        scale_id,
        value,
        order_number,
        description,
        display_name,
        creation_timestamp,
        creator_id,
        is_void,
        scale_value_status,
        abbrev,
        notes
    )
VALUES
    (
        (SELECT scale_id FROM master.variable WHERE abbrev = 'HV_METH_DISC'),
        'Single Seed Descent',
        9,
        'Single Seed Descent method',
        'Single Seed Descent',
        now(),
        1,
        false,
        'show',
        'HV_METH_DISC_SINGLE_SEED_DESCENT',
        'added by j.bantay 2020-07-30'
    );



--rollback DELETE FROM master.scale_value WHERE abbrev = 'HV_METH_DISC_SINGLE_SEED_DESCENT' AND scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'HV_METH_DISC');