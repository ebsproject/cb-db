--liquibase formatted sql

--changeset postgres:update_variable_type_in_master.variable_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update variable type in master.variable p2



UPDATE master.variable SET type = 'observation' WHERE abbrev = 'HARVEST_DATE';



--rollback UPDATE master.variable SET type = 'metadata' WHERE abbrev = 'HARVEST_DATE';