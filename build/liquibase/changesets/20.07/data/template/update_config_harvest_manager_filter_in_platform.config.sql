--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_filter_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update config HARVEST_MANAGER_FILTERS in platform.config



UPDATE
	platform.config
SET
	config_value = $$
	{
		"name": "Harvest Manager Filters",
		"values": [
			{
				"disabled": "true",
				"required": "false",
				"input_type": "single",
				"field_label": "Program",
				"input_field": "selection",
				"order_number": "1",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "PROGRAM",
				"field_description": "Program that manages the experiment"
			},
			{
				"disabled": "false",
				"required": "false",
				"input_type": "single",
				"field_label": "Experiment",
				"input_field": "selection",
				"order_number": "3",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "EXPERIMENT_NAME",
				"field_description": "Name of the experiment"
			},
			{
				"disabled": "false",
				"required": "true",
				"input_type": "single",
				"field_label": "Location",
				"input_field": "selection",
				"order_number": "5",
				"default_value": "",
				"allowed_values": "",
				"basic_parameter": "true",
				"variable_abbrev": "LOCATION_NAME",
				"field_description": "Name of the Location"
			}
		]
	}
	
	$$
WHERE
    abbrev = 'HARVEST_MANAGER_FILTERS';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value =
--rollback     '
--rollback         {
--rollback             "name": "Harvest Manager Filters",
--rollback             "values": [
--rollback                 {
--rollback                     "disabled": "true",
--rollback                     "required": "false",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Breeding Program",
--rollback                     "input_field": "selection",
--rollback                     "order_number": "1",
--rollback                     "default_value": "",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "PROGRAM",
--rollback                     "field_description": "Program that manages the experiment"
--rollback                 },
--rollback                 {
--rollback                     "disabled": "false",
--rollback                     "required": "false",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Experiment Name",
--rollback                     "input_field": "selection",
--rollback                     "order_number": "3",
--rollback                     "default_value": "",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "EXPERIMENT_NAME",
--rollback                     "field_description": "Name of the experiment"
--rollback                 },
--rollback                 {
--rollback                     "disabled": "false",
--rollback                     "required": "true",
--rollback                     "input_type": "single",
--rollback                     "field_label": "Experiment Location",
--rollback                     "input_field": "selection",
--rollback                     "order_number": "5",
--rollback                     "default_value": "",
--rollback                     "allowed_values": "",
--rollback                     "basic_parameter": "true",
--rollback                     "variable_abbrev": "LOCATION_NAME",
--rollback                     "field_description": "Name of the Location"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HARVEST_MANAGER_FILTERS';