--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_traits_protocols_act_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT to master.item



INSERT INTO 
    master.item(abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT',
        'Traits Protocol',
        20,
        'Traits Protocol',
        'Traits',
        1,
        'active',
        'fa fa-random'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT';