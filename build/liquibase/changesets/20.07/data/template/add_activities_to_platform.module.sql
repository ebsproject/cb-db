--liquibase formatted sql

--changeset postgres:add_activities_to_platform.module context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add activities to platform.module



INSERT INTO 
    platform.module
        (abbrev,name,description,module_id,controller_id,action_id,creator_id,notes,required_status)
VALUES 
  ('EXPT_CROSS_PARENT_BASIC_INFO_ACT_MOD','Specify Basic Information','Specify Basic Information','experimentCreation','create','specify-basic-info',1,'added by j.antonio ' || now(), 'draft'),
  ('EXPT_CROSS_PARENT_ENTRY_LIST_ACT_MOD','Specify Parent List','Specify Parent List','experimentCreation','create','specify-entry-list',1,'added by j.antonio ' || now(), 'entry list created'),
  ('EXPT_CROSS_PARENT_CROSSES_ACT_MOD','Manage Crosses','Manage Crosses','experimentCreation','create','manage-crosses',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_MANAGE_ACT_MOD','Manage','Manage','experimentCreation','create','manage-crosslist',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_MATRIX_ACT_MOD','Matrix','Matrix','experimentCreation','create','manage-crossing-matrix',1,'added by j.antonio ' || now(), 'crosses created'),
  ('EXPT_CROSS_PARENT_PROTOCOLS_ACT_MOD','Protocols','Protocols','experimentCreation','create','specify-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD','Planting Protocols','Planting Protocols','experimentCreation','protocol','planting-protocols',1,'added by j.antonio ' || now(), 'protocol specified'),
  ('EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD','Traits Protocols','Traits Protocols','experimentCreation','protocol','traits-protocols',1,'added by j.antonio ' || now(), 'protocols specified'),
  ('EXPT_CROSS_PARENT_PLACE_ACT_MOD','Specify Occurrences','Specify Occurrences','experimentCreation','create','specify-occurrences',1,'added by j.antonio ' || now(), 'location rep studies created'),
  ('EXPT_CROSS_PARENT_REVIEW_ACT_MOD','Review','Review','experimentCreation','create','review',1,'added by j.antonio ' || now(), 'finalized');



--rollback DELETE FROM 
--rollback     platform.module 
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback        'EXPT_CROSS_PARENT_BASIC_INFO_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_ENTRY_LIST_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_CROSSES_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_MANAGE_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_MATRIX_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_PROTOCOLS_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_PLACE_ACT_MOD',
--rollback        'EXPT_CROSS_PARENT_REVIEW_ACT_MOD'
--rollback     );


