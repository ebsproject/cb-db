--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_entry_list_act_to_master.item.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_ENTRY_LIST_ACT to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_ENTRY_LIST_ACT',
        'Specify Parent List',
        30,
        'Specify Parent List',
        'Parent List',
        1,
        'active',
        'fa fa-list-ol'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_ENTRY_LIST_ACT';