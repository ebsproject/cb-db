--liquibase formatted sql

--changeset postgres:void_tools_in_platform.application_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-875 Void tools in platform.application p2



UPDATE 
    platform.application
SET 
    is_void = TRUE
WHERE 
    abbrev 
IN
    (
        'PRODUCT_CATALOG'
    );



--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     is_void = FALSE
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'PRODUCT_CATALOG'
--rollback     );