--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_package_pattern_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update config HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT in platform.config



UPDATE
    platform.config
SET
    config_value = $$			
        { "pattern": [
        {
            "type":"field",
            "reference_data_session":"program",
            "reference_data_key":"programCode",
            "order_number":0
        },
        {
            "type":"delimeter",
            "value":"-",
            "order_number":1
        },
        {
            "type":"free_text",
            "value":"abc",
            "order_number":2
        },
        {
            "type":"delimeter",
            "value":"-",
            "order_number":3
        },
        {
            "type":"field",
            "reference_data_session":"experiment",
            "reference_data_key":"experimentYear",
            "order_number":4
        },
        {
            "type":"counter",
            "reset_per_year":"false",
            "trailing_zero":"2",		
            "order_number":5
        }
        ] 
        }
    $$
WHERE
    abbrev = 'HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "pattern": [
--rollback                 {
--rollback                     "type": "field",
--rollback                     "order_number": 0,
--rollback                     "reference_data_key": "program_abbrev",
--rollback                     "reference_data_session": "program"
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "-",
--rollback                     "order_number": 1
--rollback                 },
--rollback                 {
--rollback                     "type": "free_text",
--rollback                     "value": "abc",
--rollback                     "order_number": 2
--rollback                 },
--rollback                 {
--rollback                     "type": "delimeter",
--rollback                     "value": "-",
--rollback                     "order_number": 3
--rollback                 },
--rollback                 {
--rollback                     "type": "field",
--rollback                     "order_number": 4,
--rollback                     "reference_data_key": "experiment_year",
--rollback                     "reference_data_session": "experiment"
--rollback                 },
--rollback                 {
--rollback                     "type": "counter",
--rollback                     "order_number": 5,
--rollback                     "trailing_zero": "2",
--rollback                     "reset_per_year": "false"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HARVEST_MANAGER_PACKAGE_PATTERN_DEFAULT';