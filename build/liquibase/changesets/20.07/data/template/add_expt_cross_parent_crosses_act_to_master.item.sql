--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_crosses_act_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_CROSSES_ACT to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_CROSSES_ACT',
        'Crosses',
        30,
        'Crosses',
        'Crosses',
        1,
        'active',
        'fa fa-random'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_CROSSES_ACT';