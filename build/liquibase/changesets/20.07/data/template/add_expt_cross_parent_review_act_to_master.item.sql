--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_review_act_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_REVIEW_ACT to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_REVIEW_ACT',
        'Review',
        30,
        'Review',
        'Review',
        1,
        'active',
        'fa fa-files-o'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_REVIEW_ACT';