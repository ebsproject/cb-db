--liquibase formatted sql

--changeset postgres:activate_sites_widget_in_platform.dashboard_widget context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1022 Activate SITES widget in platform.dashboard_widget



UPDATE platform.dashboard_widget SET is_void='false' where abbrev='SITES';



--rollback UPDATE platform.dashboard_widget SET is_void='true' where abbrev='SITES';