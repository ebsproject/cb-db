--liquibase formatted sql

--changeset postgres:add_protocols_tab_to_master.item_relation context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add protocols tab to master.item_relation



INSERT INTO 
    master.item_relation(root_id,parent_id,child_id,order_number,position,visible,creator_id,notes)
SELECT 
    (SELECT id FROM master.item WHERE abbrev='EXPT_CROSS_PARENT_DATA_PROCESS') AS root_id,
    (SELECT id FROM master.item WHERE abbrev='EXPT_CROSS_PARENT_PROTOCOLS_ACT') AS parent_id,
    id AS child_id,
    CASE 
        WHEN abbrev = 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT' THEN 1
        WHEN abbrev = 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT' THEN 2
        ELSE 3
    END AS order_number,
   0,
   1,
   1,
   'added by j.antonio ' || now()
FROM 
   master.item
WHERE 
   abbrev 
IN 
    (
        'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT',
        'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'
    );



--rollback DELETE FROM 
--rollback     master.item_relation 
--rollback WHERE 
--rollback     root_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS'
--rollback     )
--rollback AND
--rollback     parent_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             id
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev = 'EXPT_CROSS_PARENT_PROTOCOLS_ACT'
--rollback     )
--rollback AND
--rollback     child_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback             id 
--rollback         FROM
--rollback             master.item
--rollback         WHERE
--rollback             abbrev
--rollback         IN
--rollback             (
--rollback                 'EXPT_CROSS_PARENT_PLANTING_PROTOCOLS_ACT',
--rollback                 'EXPT_CROSS_PARENT_TRAITS_PROTOCOLS_ACT'
--rollback             )
--rollback     );