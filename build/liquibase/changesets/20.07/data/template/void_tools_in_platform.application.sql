--liquibase formatted sql

--changeset postgres:void_tools_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-875 Void tools in platform.application



UPDATE 
    platform.application
SET 
    is_void = TRUE
WHERE 
    abbrev 
IN
    (
        'DRAFT_STUDIES',
        'LAYOUTS',
        'OPERATIONAL_STUDIES',
        'TASKS',
        'DATA_EXPORT',
        'PRINTOUTS',
        'PROJECTS',
        'LOCATIONS'
    );



--rollback UPDATE 
--rollback     platform.application
--rollback SET 
--rollback     is_void = FALSE
--rollback WHERE 
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'DRAFT_STUDIES',
--rollback         'LAYOUTS',
--rollback         'OPERATIONAL_STUDIES',
--rollback         'TASKS',
--rollback         'DATA_EXPORT',
--rollback         'PRINTOUTS',
--rollback         'PROJECTS',
--rollback         'LOCATIONS'
--rollback     );