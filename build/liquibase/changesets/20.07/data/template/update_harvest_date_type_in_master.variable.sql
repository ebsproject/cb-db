--liquibase formatted sql

--changeset postgres:update_harvest_date_type_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update harvest_date type in master.variable



UPDATE master.variable SET type = 'observation' WHERE abbrev = 'HARVEST_DATE';



--rollback UPDATE master.variable SET type = 'metadata' WHERE abbrev = 'HARVEST_DATE';