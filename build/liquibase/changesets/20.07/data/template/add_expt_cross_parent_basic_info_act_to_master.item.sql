--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_basic_info_act_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_BASIC_INFO_ACT to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_BASIC_INFO_ACT',
        'Specify Basic Information',
        30,
        'Specify Basic Information',
        'Basic',
        1,
        'active',
        'fa fa-file-text'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_BASIC_INFO_ACT';