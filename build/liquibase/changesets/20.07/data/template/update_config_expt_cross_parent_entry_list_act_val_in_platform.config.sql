--liquibase formatted sql

--changeset postgres:update_config_expt_cross_parent_entry_list_act_val_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update config EXPT_CROSS_PARENT_ENTRY_LIST_ACT_VAL in platform.config



INSERT INTO
    platform.config 
        (abbrev, name, config_value, rank, usage, creator_id, notes)
VALUES
    (
        'EXPT_CROSS_PARENT_ENTRY_LIST_ACT_VAL',
        'Cross Parent Entry List',
        '
            {
                "Name": "Required and default entry level metadata variables for IRRI Nursery Cross-list data process",
                "Values": [{
                        "fixed": true,
                        "default": false,
                        "required": "required",
                        "variable_abbrev": "ENTRY_ROLE",
                        "allowed_values": [
                            "ENTRY_ROLE_FEMALE",
                            "ENTRY_ROLE_FEMALE_AND_MALE",
                            "ENTRY_ROLE_MALE"
                        ],
                        "display_name": "Parent Role",
                        "target_column": "",
                        "secondary_target_column":"",
                        "target_value":"",
                        "api_resource_method" : "",
                        "api_resource_endpoint": "entries",
                        "api_resource_filter" : "",
                        "api_resource_sort": "", 
                        "variable_type" : "identification"
                    },
                    {
                        "disabled": false,
                        "variable_abbrev": "DESCRIPTION",
                        "target_column": "",
                        "secondary_target_column":"",
                        "target_value":"",
                        "api_resource_method" : "",
                        "api_resource_endpoint": "entries",
                        "api_resource_filter" : "",
                        "api_resource_sort": "", 
                        "variable_type" : "identification"
                    },
                    {
                    "disabled": false,
                    "is_hidden": true,
                    "default": "cross list",
                    "variable_abbrev": "ENTRY_LIST_TYPE",
                    "target_column": "",
                    "secondary_target_column":"",
                    "target_value":"",
                    "api_resource_method" : "",
                    "api_resource_endpoint": "",
                    "api_resource_filter" : "",
                    "api_resource_sort": "", 
                    "variable_type" : ""
                    }
                ]
            }
        ',
        1,
        'experiment_creation',
        1,
        'added by j.antonio'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'EXPT_CROSS_PARENT_ENTRY_LIST_ACT_VAL';