--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_matrix_act_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_MATRIX_ACT to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,item_status,item_icon)
VALUES
    (
        'EXPT_CROSS_PARENT_MATRIX_ACT',
        'Matrix',
        20,
        'Matrix',
        'Matrix',
        1,
        'active',
        'fa fa-random'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_MATRIX_ACT';