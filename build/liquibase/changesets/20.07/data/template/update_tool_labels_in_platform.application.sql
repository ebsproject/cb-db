--liquibase formatted sql

--changeset postgres:update_tool_labels_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Update tool labels in platform.application 



UPDATE platform.application SET label = 'Experiment creation', action_label = 'Create Experiments' WHERE abbrev = 'EXPERIMENT_CREATION';

UPDATE platform.application SET label = 'Persons', action_label = 'Manage Persons' WHERE abbrev = 'PERSONS';

UPDATE platform.application SET label = 'Experiment manager', action_label = 'Manage Experiments' WHERE abbrev = 'OCCURRENCES';



--rollback UPDATE platform.application SET label = 'Experiments', action_label = 'Experiments' WHERE abbrev = 'EXPERIMENT_CREATION';

--rollback UPDATE platform.application SET label = 'Users', action_label = 'Users' WHERE abbrev = 'PERSONS';

--rollback  UPDATE platform.application SET label = 'Occurrences', action_label = 'Occurrences' WHERE abbrev = 'OCCURRENCES';
