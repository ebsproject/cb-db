--liquibase formatted sql

--changeset postgres:update_is_computed_value_in_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1150 Update is_computed value in master.variable



UPDATE 
    master.variable
SET
    is_computed = true
WHERE
    abbrev 
IN
    (
        'ADJAYLD_G_CONT',
        'YLD_0_CONT2'
    );



--rollback UPDATE 
--rollback     master.variable
--rollback SET
--rollback     is_computed = false
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'ADJAYLD_G_CONT',
--rollback         'YLD_0_CONT2'
--rollback     );