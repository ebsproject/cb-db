--liquibase formatted sql

--changeset postgres:add_expt_cross_parent_data_process_to_master.item context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Add EXPT_CROSS_PARENT_DATA_PROCESS to master.item



INSERT INTO 
    master.item
        (abbrev,name,type,description,display_name,creator_id,process_type,item_status,item_icon,item_usage) 
VALUES 
    (
        'EXPT_CROSS_PARENT_DATA_PROCESS',
        'Cross-Parent Experiment',
        40,
        'Parent role (Female or Male) is unknown before planting and Cross List is prepared with assigned Parent Role only before making crosses.',
        'Cross-Parent Experiment',
        1,
        'experiment_creation_data_process',
        'active',
        'fa fa-th-list',
        'experiment_creation'
    );



--rollback DELETE FROM master.item WHERE abbrev = 'EXPT_CROSS_PARENT_DATA_PROCESS';