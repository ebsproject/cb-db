--liquibase formatted sql

--changeset postgres:update_site_widget_description context:template splitStatements:false rollbackSplitStatements:false
--comment: EBS-1022 Update SITES widget description



UPDATE platform.dashboard_widget SET description='Displays occurrence locations on a map.' WHERE abbrev='SITES';



--rollback UPDATE platform.dashboard_widget SET description='Displays study locations on a map.' WHERE abbrev='SITES';