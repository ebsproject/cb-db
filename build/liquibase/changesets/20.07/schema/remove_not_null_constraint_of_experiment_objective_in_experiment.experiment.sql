--liquibase formatted sql

--changeset postgres:remove_not_null_constraint_of_experiment_objective_in_experiment.experiment context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-865 Remove not null constraint of experiment_objective in experiment.experiment



ALTER TABLE
    experiment.experiment
ALTER COLUMN
    experiment_objective DROP NOT NULL;



--rollback ALTER TABLE
--rollback     experiment.experiment
--rollback ALTER COLUMN
--rollback     experiment_objective SET NOT NULL;