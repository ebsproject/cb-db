--liquibase formatted sql

--changeset postgres:add_sequence_nuid_to_platform context:schema splitStatements:false
--comment: EBS-806 Add sequence_nuid to platform



CREATE SEQUENCE platform.nuid_seq
    INCREMENT 1
    START 2305724
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;



--rollback DROP SEQUENCE platform.nuid_seq;