--liquibase formatted sql

--changeset postgres:create_trigger_master.update_variable_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-838 Create trigger master.update_variable_document_column



CREATE TRIGGER variable_update_variable_document_tgr
    BEFORE INSERT OR UPDATE 
    ON master.variable
    FOR EACH ROW
    EXECUTE PROCEDURE master.update_document_column_for_master_variable();



--rollback DROP TRIGGER variable_update_variable_document_tgr ON master.variable;