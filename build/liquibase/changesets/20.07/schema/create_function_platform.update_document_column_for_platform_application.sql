--liquibase formatted sql

--changeset postgres:create_function_platform.update_document_column_for_platform_application context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-837 Create function platform.update_document_column_platform_application



CREATE OR REPLACE FUNCTION platform.update_document_column_for_platform_application()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
		IF (TG_OP = 'UPDATE') THEN

			SELECT	
				concat(
					setweight(to_tsvector(new.abbrev),'A'),' ',
					setweight(to_tsvector(new.label),'A'),' ',
					setweight(to_tsvector(new.action_label),'B'),' ',
					setweight(to_tsvector(new.description),'B')
				) INTO var_document
			FROM
				platform.application pa
			WHERE
				pa.id = new.id;			
            
        ELSE
			
			SELECT	
				concat(
					setweight(to_tsvector(new.abbrev),'A'),' ',
					setweight(to_tsvector(new.label),'A'),' ',
					setweight(to_tsvector(new.action_label),'B'),' ',
					setweight(to_tsvector(new.description),'B')
				) INTO var_document
			FROM
				platform.application pa;
			
        END IF;

        new.application_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION platform.update_document_column_for_platform_application();