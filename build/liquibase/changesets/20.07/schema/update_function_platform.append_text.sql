--liquibase formatted sql

--changeset postgres:update_function_platform.append_text context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-380 Update function platform.append_text


-- update function
CREATE OR REPLACE FUNCTION
    platform.append_text(notes text, new_notes text)
RETURNS text
LANGUAGE plpgsql
AS $function$
BEGIN
    return platform.append_text(notes, new_notes, '; ');
END; $function$;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION
--rollback     platform.append_text(notes text, new_notes text)
--rollback RETURNS text
--rollback LANGUAGE plpgsql
--rollback AS $function$
--rollback BEGIN
--rollback     return z_admin.append_text(notes, new_notes, '; ');
--rollback END; $function$;
