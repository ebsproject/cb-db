--liquibase formatted sql

--changeset postgres:add_column_application_document_in_platform.application context:schema splitStatements:false
--comment: EBS-833 Add column application_document in platform.application



--add column application document
ALTER TABLE
	platform.application 
ADD COLUMN
	application_document tsvector;

COMMENT ON COLUMN 
    platform.application.application_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';

--add variable document index
CREATE INDEX
	application_application_document_idx 
ON 
	platform.application 
USING 
	gin ( application_document );



--rollback ALTER TABLE platform.application
--rollback DROP COLUMN application_document;