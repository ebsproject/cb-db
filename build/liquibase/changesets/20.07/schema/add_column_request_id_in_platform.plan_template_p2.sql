--liquibase formatted sql

--changeset postgres:add_column_request_id_in_platform.plan_template context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-951 Add column request_id in platform.plan_template p2



ALTER TABLE platform.plan_template
DROP COLUMN IF EXISTS request_id;

ALTER TABLE platform.plan_template
ADD COLUMN request_id character varying;

COMMENT ON COLUMN platform.plan_template.request_id
IS 'Request ID: Reference to the AF service request [PLANTMP_REQUEST_ID]';

CREATE INDEX plan_template_request_id_idx
ON platform.plan_template (request_id);



--rollback ALTER TABLE platform.plan_template
--rollback DROP COLUMN request_id;