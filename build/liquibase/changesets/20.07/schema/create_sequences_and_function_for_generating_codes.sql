--liquibase formatted sql

--changeset postgres:create_gid_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-982 Create sequences for generating seeds; seed_name for IRRI GID



-- create sequence for germplasm.seed.seed_name
CREATE SEQUENCE germplasm.gid_seq
    INCREMENT BY 1
    MINVALUE 301499723
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE germplasm.gid_seq;



--changeset postgres:create_seed_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-982 Create sequences for generating seeds; seed_code default value



-- create sequence for germplasm.seed.seed_code
CREATE SEQUENCE germplasm.seed_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE germplasm.seed_code_seq;



--changeset postgres:create_package_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-982 Create sequences for generating packages; package_code default value



-- create sequence for germplasm.package.package_code
CREATE SEQUENCE germplasm.package_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE germplasm.package_code_seq;



--changeset postgres:create_code_generator_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-982 Create function for generating germplasm-related codes



-- create function to generate code
CREATE OR REPLACE FUNCTION germplasm.generate_code(IN entity varchar, OUT code varchar)
    RETURNS varchar
    LANGUAGE plpgsql
AS $$
DECLARE
    seq_id bigint;
BEGIN
    SELECT nextval('germplasm.' || entity || '_code_seq') INTO seq_id;
    
    code := upper(entity) || lpad(seq_id::varchar, 16, '0');
END; $$
;



-- revert changes
--rollback DROP FUNCTION germplasm.generate_code(IN varchar, OUT varchar);
