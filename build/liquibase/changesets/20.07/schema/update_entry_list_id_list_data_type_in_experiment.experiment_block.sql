--liquibase formatted sql

--changeset postgres:update_entry_list_id_list_data_type_in_experiment.experiment_block context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-988 Update entry_list_id_list data type in experiment.experiment_block



ALTER TABLE 
    experiment.experiment_block
DROP COLUMN
    entry_list_id_list;

ALTER TABLE 
    experiment.experiment_block
ADD COLUMN
    entry_list_id_list jsonb;

COMMENT ON COLUMN
    experiment.experiment_block.entry_list_id_list
IS
    'Entry ID List: Array of entry list IDs belonging to the experiment block [EXPTBLK_ENTRYIDLIST]'
;



--rollback ALTER TABLE 
--rollback     experiment.experiment_block
--rollback DROP COLUMN
--rollback     entry_list_id_list;
--rollback 
--rollback ALTER TABLE 
--rollback     experiment.experiment_block
--rollback ADD COLUMN
--rollback     entry_list_id_list integer[];
--rollback 
--rollback COMMENT ON COLUMN
--rollback     experiment.experiment_block.entry_list_id_list
--rollback IS
--rollback     'Entry ID List: Array of entry list IDs belonging to the experiment block [EXPTBLK_ENTRYIDLIST]'
--rollback ;