--liquibase formatted sql

--changeset postgres:create_trigger_platform.update_application_document_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-837 Create trigger platform.update_application_document_column



CREATE TRIGGER application_update_application_document_tgr
    BEFORE INSERT OR UPDATE 
    ON platform.application
    FOR EACH ROW
    EXECUTE PROCEDURE platform.update_document_column_for_platform_application();



--rollback DROP TRIGGER application_update_application_document_tgr ON platform.application;