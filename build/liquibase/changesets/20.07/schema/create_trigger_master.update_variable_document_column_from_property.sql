--liquibase formatted sql

--changeset postgres:create_trigger_master.update_variable_document_column_from_property context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-838 Create trigger master.update_variable_document_column_from_property



CREATE TRIGGER property_update_variable_document_from_property_tgr
    AFTER INSERT OR UPDATE 
	ON master.property
    FOR EACH ROW
    EXECUTE PROCEDURE master.update_document_column_for_master_variable_from_property();



--rollback DROP TRIGGER property_update_variable_document_from_property_tgr ON master."property";
