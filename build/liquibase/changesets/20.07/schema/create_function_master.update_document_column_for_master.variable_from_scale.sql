--liquibase formatted sql

--changeset postgres:create_function_master.update_document_column_for_master.variable_from_scale context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-838 Create function master.update_document_column_for_master.variable_from_scale
--validCheckSum: 8:ec1983b89fbce9130cbecba704fbd3b3



-- transferred from another changeset
CREATE OR REPLACE FUNCTION master.update_document_column_for_master_variable_from_property()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE master.variable SET modification_timestamp = now() WHERE property_id = new.id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;

-- transferred from another changeset
CREATE TRIGGER property_update_variable_document_from_property_tgr
    AFTER INSERT OR UPDATE 
	ON master.property
    FOR EACH ROW
    EXECUTE PROCEDURE master.update_document_column_for_master_variable_from_property();

CREATE OR REPLACE FUNCTION master.update_document_column_for_master_variable_from_scale()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN
 	
	IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
		UPDATE master.variable SET modification_timestamp = now() WHERE scale_id = new.id;    
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION master.update_document_column_for_master_variable_from_scale() CASCADE;

--rollback DROP TRIGGER property_update_variable_document_from_property_tgr ON master."property";

--rollback DROP FUNCTION master.update_document_column_for_master_variable_from_property() CASCADE;
