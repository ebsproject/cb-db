--liquibase formatted sql

--changeset postgres:add_column_variable_document_in_master.variable context:schema splitStatements:false
--comment: EBS-832 Add column variable_document in master.variable



--add column variable document
ALTER TABLE
	master.variable 
ADD COLUMN
	variable_document tsvector;

COMMENT ON COLUMN 
    master.variable.variable_document 
IS 'Sorted list of distinct lexemes which are normalized; used in search query';

--add variable document index
CREATE INDEX
	variable_variable_document_idx 
ON 
	master.variable 
USING 
	gin ( variable_document );



--rollback ALTER TABLE master.variable
--rollback DROP COLUMN variable_document;