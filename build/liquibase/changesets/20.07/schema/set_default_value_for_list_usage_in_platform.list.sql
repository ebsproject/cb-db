--liquibase formatted sql

--changeset postgres:set_default_value_for_list_usage_in_platform.list context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-973 Set default value for list_usage in platform.list



ALTER TABLE
    platform.list
ALTER COLUMN
    list_usage
SET DEFAULT 'final list',
ALTER COLUMN
    list_usage
SET NOT NULL;



--rollback ALTER TABLE
--rollback     platform.list
--rollback ALTER COLUMN
--rollback     list_usage
--rollback DROP DEFAULT,
--rollback ALTER COLUMN
--rollback     list_usage
--rollback DROP NOT NULL;