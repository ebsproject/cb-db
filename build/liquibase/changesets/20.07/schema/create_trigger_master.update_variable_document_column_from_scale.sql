--liquibase formatted sql

--changeset postgres:create_trigger_master.update_variable_document_column_from_scale context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-838 Create trigger master.update_variable_document_column_from_scale



CREATE TRIGGER scale_update_variable_document_from_scale_tgr
    AFTER INSERT OR UPDATE 
	ON master.scale
    FOR EACH ROW
    EXECUTE PROCEDURE master.update_document_column_for_master_variable_from_scale();



--rollback DROP TRIGGER scale_update_variable_document_from_scale_tgr ON master.scale;