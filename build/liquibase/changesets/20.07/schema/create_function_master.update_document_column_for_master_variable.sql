--liquibase formatted sql

--changeset postgres:create_function_master.update_document_column_for_master_variable context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-838 Create function master.update_document_column_master_variable



CREATE OR REPLACE FUNCTION master.update_document_column_for_master_variable()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
		IF (TG_OP = 'UPDATE') THEN

			SELECT 
				concat(
					setweight(to_tsvector(new.abbrev),'A'),' ',
					setweight(to_tsvector(new.label),'A'),' ',
					setweight(to_tsvector(unaccent(new.name)),'A'),' ',
					setweight(to_tsvector(mp.abbrev),'B'),' ',
					setweight(to_tsvector(unaccent(mp.name)),'B'),' ',
					setweight(to_tsvector(ms.abbrev),'B'),' ',
					setweight(to_tsvector(unaccent(ms.name)),'B'),' ',
					setweight(to_tsvector(new.data_type),'C'),' ',
					setweight(to_tsvector(new.type),'C'),' ',
					setweight(to_tsvector(new.data_level),'C'),' ',
					setweight(to_tsvector(new.status),'C')
				) INTO var_document
			FROM 
				master.variable mv
			LEFT JOIN
				master.property mp
			ON	
				mp.id = new.property_id
			LEFT JOIN
				master.scale ms
			ON
				ms.id = new.scale_id
			WHERE
				mv.id = new.id;
            
        ELSE
			
			SELECT 
				concat(
					setweight(to_tsvector(new.abbrev),'A'),' ',
					setweight(to_tsvector(new.label),'A'),' ',
					setweight(to_tsvector(unaccent(new.name)),'A'),' ',
					setweight(to_tsvector(mp.abbrev),'B'),' ',
					setweight(to_tsvector(unaccent(mp.name)),'B'),' ',
					setweight(to_tsvector(ms.abbrev),'B'),' ',
					setweight(to_tsvector(unaccent(ms.name)),'B'),' ',
					setweight(to_tsvector(new.data_type),'C'),' ',
					setweight(to_tsvector(new.type),'C'),' ',
					setweight(to_tsvector(new.data_level),'C'),' ',
					setweight(to_tsvector(new.status),'C')
				) INTO var_document
			FROM 
				master.variable mv
			LEFT JOIN
				master.property mp
			ON	
				mp.id = new.property_id
			LEFT JOIN
				master.scale ms
			ON
				ms.id = new.scale_id;
				
        END IF;

        new.variable_document = var_document;
        
    END IF;
    
    RETURN NEW;
END;
$BODY$;



--rollback DROP FUNCTION master.update_document_column_for_master_variable();