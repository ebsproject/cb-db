--liquibase formatted sql

--changeset postgres:create_function_platform.generate_nuid context:schema splitStatements:false rollbackSplitStatements:false
--comment: EBS-806 Create function platform.generate_nuid



CREATE OR REPLACE FUNCTION platform.generate_nuid(
	shard_id integer,
	OUT nuid bigint)
    RETURNS bigint
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
    our_epoch bigint := 1487347200000; 
    seq_id bigint;
    now_millis bigint;
    
BEGIN
    
    SELECT nextval('platform.nuid_seq') % 1024 INTO seq_id;
    
    SELECT floor(extract(epoch from clock_timestamp()) * 1000) INTO now_millis;
    nuid := (now_millis - our_epoch) << 23;
    nuid := nuid | (shard_id << 10);
    nuid := nuid | (seq_id);

END; $BODY$;



--rollback CREATE OR REPLACE FUNCTION platform.generate_nuid(
--rollback 	shard_id integer,
--rollback 	OUT nuid bigint)
--rollback     RETURNS bigint
--rollback     LANGUAGE 'plpgsql'
--rollback 
--rollback     COST 100
--rollback     VOLATILE 
--rollback AS $BODY$
--rollback DECLARE
--rollback     our_epoch bigint := 1487347200000; 
--rollback     seq_id bigint;
--rollback     now_millis bigint;
--rollback     
--rollback BEGIN
--rollback     
--rollback     SELECT nextval('z_admin.nuid_seq') % 1024 INTO seq_id;
--rollback     
--rollback     SELECT floor(extract(epoch from clock_timestamp()) * 1000) INTO now_millis;
--rollback     nuid := (now_millis - our_epoch) << 23;
--rollback     nuid := nuid | (shard_id << 10);
--rollback     nuid := nuid | (seq_id);
--rollback     
--rollback END; $BODY$;
