--liquibase formatted sql

--changeset postgres:add_shu_to_team context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1328 Add SHU to team



INSERT INTO
    tenant.team
    (team_code, team_name, description, creator_id)
SELECT
    tbl.team_code,
    tbl.team_name,
    tbl.description,
    tbl.creator_id
FROM
    (
        VALUES
            ('SHU_TEAM', 'Seed Health Unit Team', 'Seed Health Unit Team', 1)
    ) as tbl (team_code, team_name, description, creator_id)
;



--rollback DELETE FROM tenant.team WHERE team_code='SHU_TEAM';



--changeset postgres:add_shu_to_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1328 Add SHU to program



INSERT INTO
    tenant.program
    (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
SELECT
    tbl.program_code,
    tbl.program_name,
    tbl.program_type,
    tbl.program_status,
    tbl.description,
    tbl.crop_program_id,
    tbl.creator_id
FROM
    (
        VALUES
            (
                'SHU', 
                'Seed Health Unit', 
                'breeding', 
                'active', 
                'Seed health unit program', 
                (SELECT id FROM tenant.crop_program WHERE crop_program_code='RICE_PROG'), 
                1
            )
    ) as tbl (program_code, program_name, program_type, program_status, description, crop_program_id, creator_id)
;



--rollback DELETE FROM tenant.program WHERE program_code='SHU';



--changeset postgres:add_shu_to_program_team context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1328 Add SHU to program_team



INSERT INTO
    tenant.program_team
    (program_id, team_id, order_number, creator_id)
SELECT
    tbl.program_id,
    tbl.team_id,
    tbl.order_number,
    tbl.creator_id
FROM
    (
        VALUES
            (
                (SELECT id FROM tenant.program WHERE program_code='SHU'),
                (SELECT id FROM tenant.team WHERE team_code='SHU_TEAM'), 
                1,
                1
            )
    ) as tbl (program_id, team_id, order_number, creator_id)
;



--rollback DELETE FROM tenant.program_team 
--rollback WHERE program_id=(SELECT id FROM tenant.program WHERE program_code='SHU')
--rollback AND team_id=(SELECT id FROM tenant.team WHERE team_code='SHU_TEAM');