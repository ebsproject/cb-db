--liquibase formatted sql

--changeset postgres:create_affected_plants_percentage_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create AFFECTED_PLANTS_PERCENTAGE variable



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('AFFECTED_PLANTS_PERCENTAGE', 'AFFECTED PLANTS PERCENTAGE', 'percentage of affected plants', 'float', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'AFFECTED_PLANTS_PERCENTAGE' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('AFFECTED_PLANTS_PERCENTAGE', 'Affected Plants Percentage', 'Affected Plants Percentage') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('AFFECTED_PLANTS_PERCENTAGE_METHOD', 'percentage of affected plants method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('AFFECTED_PLANTS_PERCENTAGE_SCALE', 'AFFECTED PLANTS PERCENTAGE scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback ;



--changeset postgres:create_affected_plants_percentage_formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create AFFECTED_PLANTS_PERCENTAGE formula



INSERT INTO master.formula
    (formula, result_variable_id, method_id, data_level, function_name, database_formula, decimal_place)
SELECT
    'AFFECTED_PLANTS_PERCENTAGE = (PSTAND_CT_PLNTPLOT_TOTAL / (PSTANDHV_CT_PLNTPLOT OR PSTANDTH_CT_PLNTPLOT))::float' AS formula,
    var.id AS result_variable_id,
    var.method_id,
    'plot' AS data_level,
    'master.formula_affected_plants_percentage(pstand_ct_plntplot_total, pstandhv_ct_plntplot, pstandth_ct_plntplot)' AS function_name,
    $$
create or replace function master.formula_affected_plants_percentage(
    pstand_ct_plntplot_total int,
    pstandhv_ct_plntplot int,
    pstandth_ct_plntplot int
) returns float as
$body$
declare
    affected_plants_percentage float;
    local_plant_stand_count int;
BEGIN
    IF pstandhv_ct_plntplot IS NULL THEN
        local_plant_stand_count = pstandth_ct_plntplot;
    ELSE
        local_plant_stand_count = pstandhv_ct_plntplot;
    END IF;
    
    IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
        affected_plants_percentage = 0;
    ELSE
        affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
    END IF;
    
    return round(affected_plants_percentage::numeric, 2);
end
$body$
language plpgsql;
    $$ AS database_formula,
    2 AS decimal_place
FROM
    master.variable AS var
WHERE
    var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.formula AS form
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.id = form.result_variable_id
--rollback     AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback ;



--changeset postgres:create_affected_plants_percentage_formula_parameters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create AFFECTED_PLANTS_PERCENTAGE formula parameters



INSERT INTO master.formula_parameter
    (formula_id, param_variable_id, data_level, result_variable_id)
SELECT
    form.id AS formula_id,
    pvar.id AS param_variable_id,
    pvar.data_level,
    form.result_variable_id
FROM
    master.formula AS form
    INNER JOIN master.variable AS var
        ON var.id = form.result_variable_id,
    master.variable AS pvar
WHERE
    pvar.abbrev IN ('PSTAND_CT_PLNTPLOT_TOTAL', 'PSTANDHV_CT_PLNTPLOT', 'PSTANDTH_CT_PLNTPLOT')
    AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.formula_parameter AS fparam
--rollback USING
--rollback     master.variable AS var,
--rollback     master.formula AS form
--rollback WHERE
--rollback     var.id = form.result_variable_id
--rollback     AND form.id = fparam.formula_id
--rollback     AND var.abbrev = 'AFFECTED_PLANTS_PERCENTAGE'
--rollback ;



--changeset postgres:create_affected_plants_percentage_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create master.formula_affected_plants_percentage() function



create or replace function master.formula_affected_plants_percentage(
    pstand_ct_plntplot_total int,
    pstandhv_ct_plntplot int,
    pstandth_ct_plntplot int
) returns float as
$body$
declare
    affected_plants_percentage float;
    local_plant_stand_count int;
BEGIN
    IF pstandhv_ct_plntplot IS NULL THEN
        local_plant_stand_count = pstandth_ct_plntplot;
    ELSE
        local_plant_stand_count = pstandhv_ct_plntplot;
    END IF;
    
    IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
        affected_plants_percentage = 0;
    ELSE
        affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
    END IF;
    
    return round(affected_plants_percentage::numeric * 100, 2);
end
$body$
language plpgsql;



-- revert changes
--rollback DROP FUNCTION master.formula_affected_plants_percentage(int, int, int);



--changeset postgres:reorder_parameters_in_affected_plants_percentage_function context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1333 Reorder parameters in master.formula_affected_plants_percentage() function


DROP FUNCTION master.formula_affected_plants_percentage(int, int, int);

CREATE OR REPLACE FUNCTION master.formula_affected_plants_percentage(
    pstandhv_ct_plntplot int,
    pstandth_ct_plntplot int,
    pstand_ct_plntplot_total int
) returns float as
$body$
declare
    affected_plants_percentage float;
    local_plant_stand_count int;
BEGIN
    IF pstandhv_ct_plntplot IS NULL THEN
        local_plant_stand_count = pstandth_ct_plntplot;
    ELSE
        local_plant_stand_count = pstandhv_ct_plntplot;
    END IF;
    
    IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
        affected_plants_percentage = 0;
    ELSE
        affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
    END IF;
    
    return round(affected_plants_percentage::numeric * 100, 2);
end
$body$
language plpgsql;



-- revert changes
--rollback DROP FUNCTION master.formula_affected_plants_percentage(int, int, int);
--rollback 
--rollback CREATE OR REPLACE FUNCTION master.formula_affected_plants_percentage(
--rollback     pstand_ct_plntplot_total int,
--rollback     pstandhv_ct_plntplot int,
--rollback     pstandth_ct_plntplot int
--rollback ) returns float as
--rollback $body$
--rollback declare
--rollback     affected_plants_percentage float;
--rollback     local_plant_stand_count int;
--rollback BEGIN
--rollback     IF pstandhv_ct_plntplot IS NULL THEN
--rollback         local_plant_stand_count = pstandth_ct_plntplot;
--rollback     ELSE
--rollback         local_plant_stand_count = pstandhv_ct_plntplot;
--rollback     END IF;
--rollback     
--rollback     IF pstand_ct_plntplot_total IS NULL OR pstand_ct_plntplot_total = 0 THEN
--rollback         affected_plants_percentage = 0;
--rollback     ELSE
--rollback         affected_plants_percentage = local_plant_stand_count/pstand_ct_plntplot_total::float;
--rollback     END IF;
--rollback     
--rollback     return round(affected_plants_percentage::numeric * 100, 2);
--rollback end
--rollback $body$
--rollback language plpgsql;
