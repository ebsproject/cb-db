--liquibase formatted sql

--changeset postgres:create_pstandhv_ct_plntplot_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create PSTANDHV_CT_PLNTPLOT variable



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('PSTANDHV_CT_PLNTPLOT', 'PLANT STAND COUNT AT HARVEST', 'plant stand count at harvest', 'integer', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PSTANDHV_CT_PLNTPLOT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PSTANDHV_CT_PLNTPLOT', 'Plant Stand Count at Harvest', 'Plant Stand Count at Harvest') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PSTANDHV_CT_PLNTPLOT_METHOD', 'plant stand count at harvest method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PSTANDHV_CT_PLNTPLOT_SCALE', 'PLANT STAND COUNT AT HARVEST scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback ;


--liquibase formatted sql

--changeset postgres:create_pstandth_ct_plntplot_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create PSTANDTH_CT_PLNTPLOT variable



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('PSTANDTH_CT_PLNTPLOT', 'PLANT STAND COUNT AT THINNING', 'plant stand count at thinning', 'integer', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PSTANDTH_CT_PLNTPLOT' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PSTANDTH_CT_PLNTPLOT', 'Plant Stand Count at Thinning', 'Plant Stand Count at Thinning') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PSTANDTH_CT_PLNTPLOT_METHOD', 'plant stand count at thinning method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PSTANDTH_CT_PLNTPLOT_SCALE', 'PLANT STAND COUNT AT THINNING scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback ;


--liquibase formatted sql

--changeset postgres:create_pstand_ct_plntplot_total_variable context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Create PSTAND_CT_PLNTPLOT_TOTAL variable



-- create variable
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('PSTAND_CT_PLNTPLOT_TOTAL', 'PLANT STAND COUNT TOTAL', 'plant stand count total', 'integer', false, 'observation', 'plot', 'occurrence', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('PSTAND_CT_PLNTPLOT_TOTAL', 'Plant Stand Count Total', 'Plant Stand Count Total') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('PSTAND_CT_PLNTPLOT_TOTAL_METHOD', 'plant stand count total method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('PSTAND_CT_PLNTPLOT_TOTAL_SCALE', 'PLANT STAND COUNT TOTAL scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
--rollback ;
