--liquibase formatted sql

--changeset postgres:update_entry_seeds_and_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1314 Update entry seeds and packages



WITH t_ent AS (
    SELECT
        ent.id AS entry_id,
        ent.germplasm_id,
        ent.entry_name,
        ent.seed_id,
        ent.package_id,
        sd.new_seed_id,
        pkg.new_package_id
    FROM
        experiment.entry AS ent
        LEFT JOIN LATERAL (
                SELECT
                    sd.id AS new_seed_id
                FROM
                    germplasm.seed AS sd
                WHERE
                    sd.germplasm_id = ent.germplasm_id
                    AND sd.is_void = FALSE
                LIMIT
                    1
            ) AS sd
                ON TRUE
        LEFT JOIN LATERAL (
                SELECT
                    pkg.id AS new_package_id
                FROM
                    germplasm.package AS pkg
                WHERE
                    pkg.seed_id = sd.new_seed_id
                    AND pkg.is_void = FALSE
                LIMIT
                    1
            ) AS pkg
                ON TRUE
    WHERE
        ent.seed_id IS NULL
        OR ent.package_id IS NULL
)
UPDATE
    experiment.entry AS ent
SET
    seed_id = t.new_seed_id,
    package_id = t.new_package_id,
    notes = platform.append_text(ent.notes, 'DB-1314 entry')
FROM
    t_ent AS t
WHERE
    t.new_seed_id IS NOT NULL
    AND t.new_package_id IS NOT NULL
    AND ent.id = t.entry_id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.entry AS ent
--rollback SET
--rollback     seed_id = NULL,
--rollback     package_id = NULL,
--rollback     notes = platform.detach_text(ent.notes, 'DB-1314 entry')
--rollback WHERE
--rollback     ent.notes ILIKE '%DB-1314 entry%'
--rollback ;
