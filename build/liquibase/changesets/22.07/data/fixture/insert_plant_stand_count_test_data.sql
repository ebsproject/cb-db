--liquibase formatted sql

--changeset postgres:insert_plant_stand_count_total_test_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Insert plant stand count total test data



INSERT INTO experiment.plot_data
    (plot_id, variable_id, data_value, data_qc_code, creator_id)
SELECT
    plt.id AS plot_id,
    var.id AS variable_id,
    '100' AS data_value,
    'G' AS data_qc_code,
    plt.creator_id
FROM
    experiment.plot AS plt
    INNER JOIN experiment.occurrence AS occ
        ON occ.id = plt.occurrence_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
WHERE
    occ.occurrence_name IN (
        'AF2021_001#OCC-01',
        'AF2021_002#OCC-02'
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot_data AS pltdata
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback     INNER JOIN experiment.plot AS plt
--rollback         ON plt.occurrence_id = occ.id
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.abbrev = 'PSTAND_CT_PLNTPLOT_TOTAL'
--rollback WHERE
--rollback     occ.occurrence_name IN (
--rollback         'AF2021_001#OCC-01',
--rollback         'AF2021_002#OCC-02'
--rollback     )
--rollback     AND var.id = pltdata.variable_id
--rollback ;



--changeset postgres:insert_plant_stand_count_at_harvest_test_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Insert plant stand count at harvest test data



INSERT INTO experiment.plot_data
    (plot_id, variable_id, data_value, data_qc_code, creator_id)
SELECT
    plt.id AS plot_id,
    var.id AS variable_id,
    round(random() * 85 + 10)::integer AS data_value,
    'G' AS data_qc_code,
    plt.creator_id
FROM
    experiment.plot AS plt
    INNER JOIN experiment.occurrence AS occ
        ON occ.id = plt.occurrence_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PSTANDHV_CT_PLNTPLOT'
WHERE
    occ.occurrence_name IN (
        'AF2021_001#OCC-01',
        'AF2021_002#OCC-02'
    )
    AND plt.id % 2 = 0
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot_data AS pltdata
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback     INNER JOIN experiment.plot AS plt
--rollback         ON plt.occurrence_id = occ.id
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.abbrev = 'PSTANDHV_CT_PLNTPLOT'
--rollback WHERE
--rollback     occ.occurrence_name IN (
--rollback         'AF2021_001#OCC-01',
--rollback         'AF2021_002#OCC-02'
--rollback     )
--rollback     AND var.id = pltdata.variable_id
--rollback ;



--changeset postgres:insert_plant_stand_count_at_thinning_test_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1319 Insert plant stand count at thinning test data



INSERT INTO experiment.plot_data
    (plot_id, variable_id, data_value, data_qc_code, creator_id)
SELECT
    plt.id AS plot_id,
    var.id AS variable_id,
    round(random() * 85 + 10)::integer AS data_value,
    'G' AS data_qc_code,
    plt.creator_id
FROM
    experiment.plot AS plt
    INNER JOIN experiment.occurrence AS occ
        ON occ.id = plt.occurrence_id
    INNER JOIN master.variable AS var
        ON var.abbrev = 'PSTANDTH_CT_PLNTPLOT'
WHERE
    occ.occurrence_name IN (
        'AF2021_001#OCC-01',
        'AF2021_002#OCC-02'
    )
    AND plt.id % 2 <> 0
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.plot_data AS pltdata
--rollback USING
--rollback     experiment.occurrence AS occ
--rollback     INNER JOIN experiment.plot AS plt
--rollback         ON plt.occurrence_id = occ.id
--rollback     INNER JOIN master.variable AS var
--rollback         ON var.abbrev = 'PSTANDTH_CT_PLNTPLOT'
--rollback WHERE
--rollback     occ.occurrence_name IN (
--rollback         'AF2021_001#OCC-01',
--rollback         'AF2021_002#OCC-02'
--rollback     )
--rollback     AND var.id = pltdata.variable_id
--rollback ;
