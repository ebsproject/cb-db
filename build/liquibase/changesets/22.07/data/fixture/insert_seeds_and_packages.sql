--liquibase formatted sql

--changeset postgres:insert_maize_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1314 Insert maize seeds



WITH t_ent AS (
    SELECT
        ent.id AS entry_id,
        ent.germplasm_id,
        ent.entry_name,
        ent.seed_id,
        ent.package_id,
        sd.new_seed_id,
        pkg.new_package_id
    FROM
        experiment.entry AS ent
        LEFT JOIN LATERAL (
                SELECT
                    sd.id AS new_seed_id
                FROM
                    germplasm.seed AS sd
                WHERE
                    sd.germplasm_id = ent.germplasm_id
                    AND sd.is_void = FALSE
                LIMIT
                    1
            ) AS sd
                ON TRUE
        LEFT JOIN LATERAL (
                SELECT
                    pkg.id AS new_package_id
                FROM
                    germplasm.package AS pkg
                WHERE
                    pkg.seed_id = sd.new_seed_id
                    AND pkg.is_void = FALSE
                LIMIT
                    1
            ) AS pkg
                ON TRUE
    WHERE
        (
            ent.seed_id IS NULL
            OR ent.package_id IS NULL
        ) AND (
            sd.new_seed_id IS NULL
            OR pkg.new_package_id IS NULL
        )
)
INSERT INTO germplasm.seed
   (seed_code, seed_name, germplasm_id, program_id, creator_id, notes)
SELECT
    germplasm.generate_code('seed') AS seed_code,
    'WE-KIB-17B-26-866' || ROW_NUMBER() OVER () AS seed_name,
    t.germplasm_id,
    prog.id AS program_id,
    1 AS creator_id,
    'DB-1314 seed' AS notes
FROM
    t_ent AS t
    INNER JOIN tenant.program AS prog
        ON prog.program_code = 'KE'
    INNER JOIN germplasm.germplasm AS ge
        ON ge.id = t.germplasm_id
    INNER JOIN tenant.crop AS crp
        ON crp.id = ge.crop_id
WHERE
    crp.crop_code = 'MAIZE'
    AND t.new_seed_id IS NULL
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.seed
--rollback WHERE
--rollback     notes = 'DB-1314 seed'
--rollback ;



--changeset postgres:insert_maize_and_rice_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1314 Insert maize packages



INSERT INTO germplasm.package
    (package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id, notes)
SELECT
    germplasm.generate_code('package') AS package_code,
    sd.seed_name AS package_label,
    0 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    sd.id AS seed_id,
    sd.program_id,
    sd.creator_id,
    'DB-1314 package' AS notes
FROM
    germplasm.seed AS sd
WHERE
    sd.notes = 'DB-1314 seed'
UNION ALL
    SELECT
        germplasm.generate_code('package') AS package_code,
        sd.seed_name AS package_label,
        0 AS package_quantity,
        'g' AS package_unit,
        'active' AS package_status,
        sd.id AS seed_id,
        sd.program_id,
        sd.creator_id,
        'DB-1314 package' AS notes
    FROM
        germplasm.germplasm AS ge
        INNER JOIN germplasm.seed AS sd
            ON sd.germplasm_id = ge.id
    WHERE
        ge.designation = 'IR 117633'
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package
--rollback WHERE
--rollback     notes = 'DB-1314 package'
--rollback ;
