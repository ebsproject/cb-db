--liquibase formatted sql

--changeset postgres:update_planting_instruction_seeds_and_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1314 Update planting instruction seeds and packages



WITH t_plin AS (
    SELECT
        ent.id AS entry_id,
        ent.germplasm_id,
        ent.seed_id AS new_seed_id,
        ent.package_id AS new_package_id,
        plin.id AS planting_instruction_id,
        plin.germplasm_id,
        plin.seed_id,
        plin.package_id
    FROM
        experiment.entry AS ent
        INNER JOIN experiment.planting_instruction AS plin
            ON plin.entry_id = ent.id
    WHERE
        ent.notes ILIKE '%DB-1314 entry%'
)
UPDATE
    experiment.planting_instruction AS plin
SET
    seed_id = t.new_seed_id,
    package_id = t.new_package_id,
    notes = platform.append_text(plin.notes, 'DB-1314 planting_instruction')
FROM
    t_plin AS t
WHERE
    plin.id = t.planting_instruction_id
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_instruction AS plin
--rollback SET
--rollback     seed_id = NULL,
--rollback     package_id = NULL,
--rollback     notes = platform.detach_text(plin.notes, 'DB-1314 planting_instruction')
--rollback WHERE
--rollback     plin.notes ILIKE '%DB-1314 planting_instruction%'
--rollback ;
