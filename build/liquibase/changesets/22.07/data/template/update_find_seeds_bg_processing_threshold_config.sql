--liquibase formatted sql

--changeset postgres:update_find_seeds_bg_processing_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3754 SS DB: Add exportWorkingList BG processing threshold value


UPDATE
	platform.config
SET
	config_value = '{
		"addWorkingList": {
			"size": "500",
			"description": "Update entries of an Experiment"
		},
		"inputListLimit": {
			"size": "500",
			"description": "Maximum number of input list items to be processed"
		},
		"deleteWorkingList": {
			"size": "500",
			"description": "Delete entries of an Experiment"
		},
		"exportWorkingList": {
			"size": "1000",
			"description": "Minimum threshold to trigger background job"
		}
	}'
WHERE
	abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD';


--rollback  UPDATE
--rollback  	platform.config
--rollback  SET
--rollback  	config_value = '{
--rollback  		"addWorkingList": {
--rollback  			"size": "500",
--rollback  			"description": "Update entries of an Experiment"
--rollback  		},
--rollback  		"inputListLimit": {
--rollback  			"size": "500",
--rollback  			"description": "Maximum number of input list items to be processed"
--rollback  		},
--rollback  		"deleteWorkingList": {
--rollback  			"size": "500",
--rollback  			"description": "Delete entries of an Experiment"
--rollback  		}
--rollback  	}'
--rollback  WHERE
--rollback  	abbrev = 'FIND_SEEDS_BG_PROCESSING_THRESHOLD';