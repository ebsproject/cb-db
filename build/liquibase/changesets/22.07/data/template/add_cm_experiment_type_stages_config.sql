--liquibase formatted sql

--changeset postgres:add_cm_experiment_type_stages_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3070 Create a new configuration for eligible experiment types and stages per crop for crossing



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'CM_CROSS_LIST_ADD_PARENTS',
        'Cross Manager Add Parents tab configuration',
        $$
            {
                "Name": "Cross Manager Add Parents tab default filters",
                "Values": [
                    {
                        "experiment_type_stages": {
                            "DEFAULT": {
                                "Breeding Trial": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Cross Parent Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Generation Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Intentional Crossing Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Observation": {
                                    "disabled": false,
                                    "stages": null
                                }
                            },
                            "IRSEA": {
                                "Breeding Trial": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Cross Parent Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Generation Nursery": {
                                    "disabled": false,
                                    "stages": [
                                        "RGA"
                                    ]
                                },
                                "Intentional Crossing Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Observation": {
                                    "disabled": false,
                                    "stages": [
                                        "BRE",
                                        "SEM"
                                    ]
                                }
                            },
                            "BW": {
                                "Breeding Trial": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Cross Parent Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Generation Nursery": {
                                    "disabled": true,
                                    "stages": null
                                },
                                "Intentional Crossing Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Observation": {
                                    "disabled": false,
                                    "stages": null
                                }
                            },
                            "KE": {
                                "Breeding Trial": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Cross Parent Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Generation Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Intentional Crossing Nursery": {
                                    "disabled": false,
                                    "stages": null
                                },
                                "Observation": {
                                    "disabled": false,
                                    "stages": null
                                }
                            }
                        }
                    }
                ]
            }
        $$,
        1,
        'cross manager tool',
        1,
        'added by d.gepte'
    );



--rollback DELETE FROM platform.config WHERE abbrev='CM_CROSS_LIST_ADD_PARENTS';