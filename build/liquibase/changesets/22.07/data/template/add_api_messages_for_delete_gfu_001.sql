--liquibase formatted sql

--changeset postgres:add_api_messages_for_delete_gfu_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3428 Add new API messages for DELETE germplasm-file-uploads/:id



INSERT INTO
    api.messages
    (
        http_status_code,
        code,
        type,
        message,
        creator_id,
        url
    )
VALUES
    (
        400,
        400249,
        'ERR',
        'Invalid request. User has provided an invalid format for germplasm file upload ID, it must be an integer.',
        1,
        '/responses.html#400249'
    ),
    (
        401,
        401027,
        'ERR',
        'You do not have access to delete the germplasm file upload record.',
        1,
        '/responses.html#401027'
    ),
    (
        404,
        404055,
        'ERR',
        'The germplasm file upload record you have requested does not exist.',
        1,
        '/responses.html#404055'
    )
    ;



-- rollback DELETE FROM api.messages WHERE code IN (400249, 401027, 404055);