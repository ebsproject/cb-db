--liquibase formatted sql

--changeset postgres:insert_default_timetable_migration_records context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM timetable.migration LIMIT 1) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1322 Insert default timetable.migration records



INSERT INTO timetable.migration
    (id, "version")
VALUES
    (0,'00259 Restart migrations for v4'),
    (1,'00305 Fix timetable.is_cron_in_time'),
    (2,'00323 Append timetable.delete_job function'),
    (3,'00329 Migration required for some new added functions'),
    (4,'00334 Refactor timetable.task as plain schema without tree-like dependencies'),
    (5,'00381 Rewrite active chain handling'),
    (6,'00394 Add started_at column to active_session and active_chain tables'),
    (7,'00417 Rename LOG database log level to INFO')
;



-- revert changes
--rollback DELETE FROM timetable.migration;
