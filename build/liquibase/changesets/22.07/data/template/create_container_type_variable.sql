--liquibase formatted sql

--changeset postgres:create_container_type_variable context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.variable WHERE abbrev = 'CONTAINER_TYPE') WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1338 Create CONTAINER_TYPE variable



-- create variable if not existing, else skip this changeset (use precondition to check if variable exists)
DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
BEGIN
    -- variable
    INSERT INTO
        master.variable (abbrev, label, name, data_type, not_null, type, data_level, usage, description, status, creator_id) 
    VALUES 
        ('CONTAINER_TYPE', 'CONTAINER TYPE', 'container type', 'character varying', false, 'metadata', 'sample, package', 'application', NULL, 'active', '1')
    RETURNING id INTO var_variable_id;

    -- property
    SELECT id FROM master.property WHERE abbrev = 'CONTAINER_TYPE' INTO var_property_id;
    IF var_property_id IS NULL THEN
        INSERT INTO
            master.property (abbrev, name, display_name)
        VALUES
            ('CONTAINER_TYPE', 'Container Type', 'Container Type') 
        RETURNING id INTO var_property_id;
    END IF;

    -- method
    INSERT INTO
        master.method (abbrev, name) 
    VALUES
        ('CONTAINER_TYPE_METHOD', 'container type method')
    RETURNING id INTO var_method_id;

    -- scale
    INSERT INTO
        master.scale (abbrev, name, unit, scale_default_value, min_value, max_value)
    VALUES
        ('CONTAINER_TYPE_SCALE', 'CONTAINER TYPE scale', NULL, NULL, NULL, NULL)
    RETURNING id INTO var_scale_id;

    -- update references
    UPDATE
        master.variable
    SET
        property_id = var_property_id,
        method_id = var_method_id,
        scale_id = var_scale_id
    WHERE
        id = var_variable_id
    ;
END; $$;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'CONTAINER_TYPE'
--rollback     AND t.scale_id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.scale AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'CONTAINER_TYPE'
--rollback     AND t.id = var.scale_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.method AS t
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.abbrev = 'CONTAINER_TYPE'
--rollback     AND t.id = var.method_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     master.variable AS t
--rollback WHERE
--rollback     t.abbrev = 'CONTAINER_TYPE'
--rollback ;



--changeset postgres:create_container_type_scale_values context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev IN ('CONTAINER_TYPE_BAG', 'CONTAINER_TYPE_96_WELL_PLATE', 'CONTAINER_TYPE_TUBE')) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1338 Add scale values to CONTAINER_TYPE variable



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'CONTAINER_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('bag', 'bag', 'bag', 'BAG', 'show'),
        ('96-well plate', '96-well plate', '96-well plate', '96_WELL_PLATE', 'show'),
        ('tube', 'tube', 'tube', 'TUBE', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'CONTAINER_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'CONTAINER_TYPE_BAG',
--rollback         'CONTAINER_TYPE_96_WELL_PLATE',
--rollback         'CONTAINER_TYPE_TUBE'
--rollback     )
--rollback ;



--changeset postgres:set_bag_as_default_container_type_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1338 Set bag as default CONTAINER_TYPE scale value



-- set default scale value of a variable
UPDATE
    master.scale AS scal
SET
    scale_default_value = 'bag'
FROM
    master.variable AS var
WHERE
    scal.id = var.scale_id
    AND var.abbrev = 'CONTAINER_TYPE'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale AS scal
--rollback SET
--rollback     scale_default_value = NULL
--rollback FROM
--rollback     master.variable AS var
--rollback WHERE
--rollback     scal.id = var.scale_id
--rollback     AND var.abbrev = 'CONTAINER_TYPE'
--rollback ;
