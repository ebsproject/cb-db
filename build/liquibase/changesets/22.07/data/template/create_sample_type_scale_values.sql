--liquibase formatted sql

--changeset postgres:create_sample_type_scale_values context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM master.scale_value WHERE abbrev IN ('SAMPLE_TYPE_SEED', 'SAMPLE_TYPE_DNA', 'SAMPLE_TYPE_LEAF')) WHEN TRUE THEN 1 ELSE 0 END;
--comment: DB-1338 Create scale values for SAMPLE_TYPE variable



-- add scale value/s to a variable if not existing, else skip this changeset (use precondition to check if scale values exist)
WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'SAMPLE_TYPE'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('seed', 'seed', 'seed', 'SEED', 'show'),
        ('DNA', 'DNA', 'DNA', 'DNA', 'show'),
        ('leaf', 'leaf', 'leaf', 'LEAF', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'SAMPLE_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'SAMPLE_TYPE_SEED',
--rollback         'SAMPLE_TYPE_DNA',
--rollback         'SAMPLE_TYPE_LEAF'
--rollback     )
--rollback ;



--changeset postgres:set_seed_as_default_sample_type_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1338 Set seed as the default scale value for SAMPLE_TYPE variable



-- set default scale value of a variable
UPDATE
    master.scale AS scal
SET
    scale_default_value = 'seed'
FROM
    master.variable AS var
WHERE
    scal.id = var.scale_id
    AND var.abbrev = 'SAMPLE_TYPE'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale AS scal
--rollback SET
--rollback     scale_default_value = NULL
--rollback FROM
--rollback     master.variable AS var
--rollback WHERE
--rollback     scal.id = var.scale_id
--rollback     AND var.abbrev = 'SAMPLE_TYPE'
--rollback ;
