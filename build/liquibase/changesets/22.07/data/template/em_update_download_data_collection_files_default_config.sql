--liquibase formatted sql

--changeset postgres:em_update_download_data_collection_files_default_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3717 Change DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS REP attribute from plotRep to rep



UPDATE
    platform.config
SET
    config_value = '{
            "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
                {
                    "attribute": "plotCode",
                    "abbrev": "PLOT_CODE"
                },
                {
                    "attribute": "plotNumber",
                    "abbrev": "PLOTNO"
                },
                {
                    "attribute": "rep",
                    "abbrev": "REP"
                },
                {
                    "attribute": "entryNumber",
                    "abbrev": "ENTNO"
                },
                {
                    "attribute": "entryName",
                    "abbrev": "DESIGNATION"
                },
                {
                    "attribute": "parentage",
                    "abbrev": "PARENTAGE"
                },
                {
                    "attribute": "fieldX",
                    "abbrev": "FIELD_X"
                },
                {
                    "attribute": "fieldY",
                    "abbrev": "FIELD_Y"
                },
                {
                    "attribute": "occurrenceDbId",
                    "abbrev": "OCCURRENCE_ID"
                },
                {
                    "attribute": "occurrenceName",
                    "abbrev": "OCCURRENCE_NAME"
                },
                {
                    "attribute": "occurrenceCode",
                    "abbrev": "OCCURRENCE_CODE"
                },
                {
                    "attribute": "locationDbId",
                    "abbrev": "LOCATION_ID"
                },
                {
                    "attribute": "locationName",
                    "abbrev": "LOCATION_NAME"
                },
                {
                    "attribute": "locationCode",
                    "abbrev": "LOCATION_CODE"
                }
            ]
        }'
WHERE
    abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = '{
--rollback             "DOWNLOAD_FOR_FIELD_BOOK_CSV_COLUMNS": [
--rollback                 {
--rollback                     "attribute": "plotCode",
--rollback                     "abbrev": "PLOT_CODE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "plotNumber",
--rollback                     "abbrev": "PLOTNO"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "plotRep",
--rollback                     "abbrev": "REP"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryNumber",
--rollback                     "abbrev": "ENTNO"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "entryName",
--rollback                     "abbrev": "DESIGNATION"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "parentage",
--rollback                     "abbrev": "PARENTAGE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "fieldX",
--rollback                     "abbrev": "FIELD_X"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "fieldY",
--rollback                     "abbrev": "FIELD_Y"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "occurrenceDbId",
--rollback                     "abbrev": "OCCURRENCE_ID"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "occurrenceName",
--rollback                     "abbrev": "OCCURRENCE_NAME"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "occurrenceCode",
--rollback                     "abbrev": "OCCURRENCE_CODE"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "locationDbId",
--rollback                     "abbrev": "LOCATION_ID"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "locationName",
--rollback                     "abbrev": "LOCATION_NAME"
--rollback                 },
--rollback                 {
--rollback                     "attribute": "locationCode",
--rollback                     "abbrev": "LOCATION_CODE"
--rollback                 }
--rollback             ]
--rollback         }'
--rollback WHERE
--rollback     abbrev = 'DOWNLOAD_DATA_COLLECTION_FILES_DEFAULT';
