--liquibase formatted sql

--changeset postgres:update_hm_browser_config_wheat_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-3518 HM DB: Update wheat browser configurations



-- update browser config
UPDATE platform.config
SET
    config_value = '{
        "default": {
            "default": {
                "default": {
                    "input_columns": [],
                    "numeric_variables": [],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_SELFING": {
            "fixed": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "GRAIN_COLOR",
                            "placeholder": "Grain Color",
                            "required": false,
                            "retrieve_scale": true,
                            "column_name": "grainColor"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "GRAIN_COLOR",
                            "placeholder": "Grain Color",
                            "required": false,
                            "retrieve_scale": true,
                            "column_name": "grainColor"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "unknown": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "GRAIN_COLOR",
                            "placeholder": "Grain Color",
                            "required": false,
                            "retrieve_scale": true,
                            "column_name": "grainColor"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                ""
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            },
            "not_fixed": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        },
                        {
                            "abbrev": "<none>",
                            "placeholder": "Not applicable",
                            "required": null,
                            "retrieve_scale": false,
                            "column_name": "numericVar"
                        },
                        {
                            "abbrev": "GRAIN_COLOR",
                            "placeholder": "Grain Color",
                            "required": false,
                            "retrieve_scale": true,
                            "column_name": "grainColor"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK",
                                "HV_METH_DISC_SELECTED_BULK"
                            ]
                        },
                        {
                            "min": 1,
                            "type": "number",
                            "abbrev": "NO_OF_PLANTS",
                            "sub_type": "single_int",
                            "field_name": "noOfPlant",
                            "placeholder": "No. of plants",
                            "harvest_methods": [
                                "HV_METH_DISC_SELECTED_BULK",
                                "HV_METH_DISC_SINGLE_PLANT_SELECTION",
                                "HV_METH_DISC_INDIVIDUAL_SPIKE"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_BACKCROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_TOP_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_DOUBLE_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        },
        "CROSS_METHOD_SINGLE_CROSS": {
            "default": {
                "default": {
                    "input_columns": [
                        {
                            "abbrev": "HVDATE_CONT",
                            "placeholder": "Harvest Date",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestDate"
                        },
                        {
                            "abbrev": "HV_METH_DISC",
                            "placeholder": "Harvest Method",
                            "required": true,
                            "retrieve_scale": false,
                            "column_name": "harvestMethod"
                        }
                    ],
                    "numeric_variables": [
                        {
                            "min": null,
                            "type": "number",
                            "abbrev": "<none>",
                            "sub_type": "single_int",
                            "field_name": "<none>",
                            "placeholder": "Not applicable",
                            "harvest_methods": [
                                "",
                                "HV_METH_DISC_BULK"
                            ]
                        }
                    ],
                    "additional_required_variables": []
                }
            }
        }
    }
    '
WHERE
    abbrev = 'HM_DATA_BROWSER_CONFIG_WHEAT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = '{
--rollback         "default": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [],
--rollback                     "numeric_variables": [],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS_METHOD_SELFING": {
--rollback             "fixed": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             },
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 ""
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             },
--rollback             "unknown": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 ""
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             },
--rollback             "not_fixed": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "<none>",
--rollback                             "required": null,
--rollback                             "column_name": "numericVar"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK",
--rollback                                 "HV_METH_DISC_SELECTED_BULK"
--rollback                             ]
--rollback                         },
--rollback                         {
--rollback                             "min": 1,
--rollback                             "type": "number",
--rollback                             "abbrev": "NO_OF_PLANTS",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "noOfPlant",
--rollback                             "placeholder": "No. of plants",
--rollback                             "harvest_methods": [
--rollback                                 "HV_METH_DISC_SELECTED_BULK",
--rollback                                 "HV_METH_DISC_SINGLE_PLANT_SELECTION",
--rollback                                 "HV_METH_DISC_INDIVIDUAL_SPIKE"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS_METHOD_BACKCROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS_METHOD_TOP_CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS_METHOD_DOUBLE_CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS_METHOD_SINGLE_CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "input_columns": [
--rollback                         {
--rollback                             "abbrev": "HVDATE_CONT",
--rollback                             "required": true,
--rollback                             "column_name": "harvestDate"
--rollback                         },
--rollback                         {
--rollback                             "abbrev": "HV_METH_DISC",
--rollback                             "required": true,
--rollback                             "column_name": "harvestMethod"
--rollback                         }
--rollback                     ],
--rollback                     "numeric_variables": [
--rollback                         {
--rollback                             "min": null,
--rollback                             "type": "number",
--rollback                             "abbrev": "<none>",
--rollback                             "sub_type": "single_int",
--rollback                             "field_name": "<none>",
--rollback                             "placeholder": "Not applicable",
--rollback                             "harvest_methods": [
--rollback                                 "",
--rollback                                 "HV_METH_DISC_BULK"
--rollback                             ]
--rollback                         }
--rollback                     ],
--rollback                     "additional_required_variables": []
--rollback                 }
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HM_DATA_BROWSER_CONFIG_WHEAT'
--rollback ;