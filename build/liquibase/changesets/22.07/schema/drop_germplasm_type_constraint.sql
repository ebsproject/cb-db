--liquibase formatted sql

--changeset postgres:drop_germplasm_type_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1344 Drop germplasm_type check constraint



ALTER TABLE germplasm.germplasm
    DROP CONSTRAINT germplasm_germplasm_type_chk
;



-- revert changes
--rollback ALTER TABLE germplasm.germplasm
--rollback     ADD CONSTRAINT germplasm_germplasm_type_chk 
--rollback     CHECK (germplasm_type::text = ANY (ARRAY['accession'::text, 'backcross'::text, 'CMS_line'::text, 'doubled_haploid'::text, 'F1'::text, 'F1F'::text, 'F1TOP'::text, 'fixed_inbred_line'::text, 'fixed_line'::text, 'gene_construct'::text, 'genome_edited'::text, 'haploid'::text, 'hybrid'::text, 'inbred_line'::text, 'progeny'::text, 'segregating'::text, 'transgenic'::text, 'experimental_variety'::text, 'landrace'::text, 'population'::text, 'synthetic'::text, 'unknown'::text]));
