--liquibase formatted sql

--changeset postgres:add_sample_type_column_to_shipment_item_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1338 Add sample_type column to inventory.shipment_item table



-- add column
ALTER TABLE
    inventory.shipment_item
ADD COLUMN
    sample_type varchar(64)
    DEFAULT 'seed'
;

COMMENT ON COLUMN inventory.shipment_item.sample_type
    IS 'Sample Type: Type of the sample added as a shipment item {default: seed} [SHIPITEM_SAMPLETYPE]';

-- create index
CREATE INDEX shipment_item_sample_type_idx
    ON inventory.shipment_item USING btree (sample_type)
;



-- revert changes
--rollback ALTER TABLE
--rollback     inventory.shipment_item
--rollback DROP COLUMN
--rollback     sample_type
--rollback ;



--changeset postgres:add_container_type_column_to_shipment_item_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1338 Add container_type column to inventory.shipment_item table



-- add column
ALTER TABLE
    inventory.shipment_item
ADD COLUMN
    container_type varchar(64)
    DEFAULT 'bag'
;

COMMENT ON COLUMN inventory.shipment_item.container_type
    IS 'Container Type: Type of container where the shipment item is stored {default: bag} [SHIPITEM_CONTAINERTYPE]';

-- create index
CREATE INDEX shipment_item_container_type_idx
    ON inventory.shipment_item USING btree (container_type)
;



-- revert changes
--rollback ALTER TABLE
--rollback     inventory.shipment_item
--rollback DROP COLUMN
--rollback     container_type
--rollback ;
