--liquibase formatted sql

--changeset postgres:drop_cross_id_from_germplasm_family_member context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1320 Drop cross_id from germplasm.family_member



ALTER TABLE
    germplasm.family_member
DROP COLUMN
    cross_id
;



--rollback ALTER TABLE 
--rollback     germplasm.family_member
--rollback ADD COLUMN 
--rollback     cross_id integer
--rollback ;



--changeset postgres:add_cross_id_to_germplasm_family context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1320 Add cross_id to germplasm.family



ALTER TABLE
    germplasm.family
ADD COLUMN
    cross_id integer;

CREATE INDEX family_cross_id_idx
    ON germplasm.family USING btree (cross_id)
;

COMMENT ON INDEX germplasm.family_cross_id_idx
    IS 'A family can be retrieved by its cross.';

ALTER TABLE
    germplasm.family
ADD CONSTRAINT
    family_cross_id_fk FOREIGN KEY (cross_id)
    REFERENCES germplasm.cross (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE RESTRICT;

COMMENT ON CONSTRAINT family_cross_id_fk ON germplasm.family
    IS 'A family may refer to zero or one cross. A cross may have zero or more families.';



--rollback ALTER TABLE
--rollback     germplasm.family
--rollback DROP COLUMN
--rollback     cross_id
--rollback ;
