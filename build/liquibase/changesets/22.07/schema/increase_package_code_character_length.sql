--liquibase formatted sql

--changeset postgres:increase_package_code_character_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1331 Increase package_code character length to 128



ALTER TABLE
    germplasm.package
ALTER COLUMN
    package_code
    TYPE varchar(128)
;



-- revert changes
--rollback ALTER TABLE
--rollback     germplasm.package
--rollback ALTER COLUMN
--rollback     package_code
--rollback     TYPE varchar(64)
--rollback ;
