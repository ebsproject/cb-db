--liquibase formatted sql

--changeset postgres:create_update_occurrence_entry_and_plot_counts_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1322 Create experiment.update_occurrence_entry_and_plot_counts() function



-- delete existing function if any
DROP FUNCTION IF EXISTS
    experiment.update_occurrence_entry_and_plot_counts()
;

-- create function
CREATE OR REPLACE FUNCTION
    experiment.update_occurrence_entry_and_plot_counts()
RETURNS
    VOID
AS $BODY$
DECLARE
    yesterday date;
    yesterday_start timestamp;
    yesterday_end timestamp;
BEGIN
    -- get yesterday's start and end times
    yesterday := current_date - INTEGER '1';
    yesterday_start := (yesterday || ' 00:00:00')::timestamp;
    yesterday_end := (yesterday || ' 23:59:59')::timestamp;
    
    -- update counts for occurrences where planting instruction records where created/modified yesterday
    WITH t_counts AS (
        SELECT
            occ.id AS occurrence_id,
            (
                SELECT
                    count(DISTINCT plot.entry_id)
                FROM
                    experiment.plot AS plot
                    JOIN experiment.planting_instruction AS plantinst
                        ON plantinst.plot_id = plot.id
                    JOIN experiment.entry AS ent
                        ON ent.id = plot.entry_id
                WHERE
                    plot.occurrence_id = occ.id
                    AND plot.is_void = FALSE
                    AND plantinst.is_void = FALSE
                    AND ent.is_void = FALSE
            ) AS entry_count,
            (
                SELECT
                    count(*)
                FROM
                    experiment.plot AS plot
                    JOIN experiment.planting_instruction AS plantinst
                        ON plantinst.plot_id = plot.id
                    JOIN experiment.entry AS ent
                        ON ent.id = plot.entry_id
                WHERE
                    plot.occurrence_id = occ.id
                    AND plot.is_void = FALSE
                    AND plantinst.is_void = FALSE
                    AND ent.is_void = FALSE
            ) AS plot_count
        FROM
            experiment.occurrence AS occ
        WHERE
            occ.id IN (
                SELECT
                    DISTINCT plt.occurrence_id
                FROM
                    experiment.plot AS plt
                    INNER JOIN experiment.planting_instruction AS plin
                        ON plin.plot_id = plt.id
                WHERE
                    plin.creation_timestamp BETWEEN yesterday_start AND yesterday_end
                UNION
                    SELECT
                        DISTINCT plt.occurrence_id
                    FROM
                        experiment.plot AS plt
                        INNER JOIN experiment.planting_instruction AS plin
                            ON plin.plot_id = plt.id
                    WHERE
                        plin.modification_timestamp BETWEEN yesterday_start AND yesterday_end
            )
    )
    UPDATE
        experiment.occurrence AS occ
    SET
        entry_count = t.entry_count,
        plot_count = t.plot_count
    FROM
        t_counts AS t
    WHERE
        occ.id = t.occurrence_id
    ;
END; $BODY$
LANGUAGE
    PLPGSQL
;



-- revert changes
--rollback DROP FUNCTION IF EXISTS
--rollback     experiment.update_occurrence_entry_and_plot_counts()
--rollback ;



--changeset postgres:grant_privileges_to_objects context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1322 Grant privileges to objects



GRANT UPDATE ON TABLE experiment.occurrence TO scheduler;
GRANT EXECUTE ON FUNCTION experiment.update_occurrence_entry_and_plot_counts() TO scheduler;



-- revert changes
--rollback REVOKE UPDATE ON TABLE experiment.occurrence FROM scheduler;
--rollback REVOKE EXECUTE ON FUNCTION experiment.update_occurrence_entry_and_plot_counts() FROM scheduler;



--changeset postgres:add_update_occurrence_entry_and_plot_counts_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1322 Add experiment.update_occurrence_entry_and_plot_counts job



-- run at 01:00 on every day-of-week from Monday through Saturday (https://crontab.guru/#0_1_*_*_1-6)
SELECT timetable.add_job('experiment.update_occurrence_entry_and_plot_counts', '0 1 * * 1-6', 'SELECT experiment.update_occurrence_entry_and_plot_counts()');



-- revert changes
--rollback SELECT timetable.delete_job('experiment.update_occurrence_entry_and_plot_counts');
