--liquibase formatted sql

--changeset postgres:create_planting_instruction_creation_timestamp_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1322 Create experiment.planting_instruction creation_timestamp index



-- create index for regular column
CREATE INDEX planting_instruction_creation_timestamp_idx
    ON experiment.planting_instruction USING btree (creation_timestamp)
;



-- revert changes
--rollback DROP INDEX experiment.planting_instruction_creation_timestamp_idx;



--changeset postgres:create_planting_instruction_modification_timestamp_index context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1322 Create experiment.planting_instruction modification_timestamp index



-- create index for regular column
CREATE INDEX planting_instruction_modification_timestamp_idx
    ON experiment.planting_instruction USING btree (modification_timestamp)
;



-- revert changes
--rollback DROP INDEX experiment.planting_instruction_modification_timestamp_idx;
