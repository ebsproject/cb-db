--liquibase formatted sql

--changeset postgres:drop_data_terminal_background_process context:schema splitStatements:false rollbackSplitStatements:false
--comment: CORB-6541 DC-DB: Remove data_terminal.background_process table



-- drop table
DROP TABLE data_terminal.background_process;



--rollback CREATE TABLE IF NOT EXISTS data_terminal.background_process
--rollback (
--rollback   id serial NOT NULL,
--rollback   transaction_id integer NOT NULL,
--rollback   process_id character varying,
--rollback   process_name character varying,
--rollback   status character varying(20),
--rollback   message text,
--rollback   start_time timestamp without time zone,
--rollback   end_time timestamp without time zone,
--rollback   is_seen boolean NOT NULL DEFAULT false, -- Indicates whether the record is seen by the user
--rollback   creator_id integer, -- Id of the user who added the record.
--rollback   creation_timestamp timestamp without time zone NOT NULL DEFAULT now(), -- Timestamp when the record is added
--rollback   modifier_id integer,
--rollback   modification_timestamp timestamp without time zone,
--rollback   is_void boolean NOT NULL DEFAULT false, -- Indicates whether the record is deleted or not
--rollback   remarks character varying, -- Additional details about the record
--rollback   notes text,
--rollback   CONSTRAINT background_process_transaction_id_fkey FOREIGN KEY (transaction_id)
--rollback       REFERENCES data_terminal.transaction (id) MATCH SIMPLE
--rollback       ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the transaction_id column, which refers to the id column of data_terminal.transaction table
--rollback   CONSTRAINT background_process_creator_id_fkey FOREIGN KEY (creator_id)
--rollback       REFERENCES tenant.person (id) MATCH SIMPLE
--rollback       ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the creator_id column, which refers to the id column of master.user table
--rollback   CONSTRAINT background_process_modifier_id_fkey FOREIGN KEY (modifier_id)
--rollback       REFERENCES tenant.person (id) MATCH SIMPLE
--rollback       ON UPDATE CASCADE ON DELETE CASCADE, -- Foreign key constraint for the modifier_id column, which refers to the id column of the master.user table
--rollback   CONSTRAINT background_process_id_pk PRIMARY KEY (id)
--rollback )
--rollback WITH (
--rollback   OIDS=FALSE
--rollback );
--rollback COMMENT ON TABLE data_terminal.background_process
--rollback   IS 'Stores the transactions of background processes';
