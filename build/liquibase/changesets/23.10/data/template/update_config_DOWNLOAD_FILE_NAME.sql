--liquibase formatted sql

--changeset postgres:update_config_DOWNLOAD_FILE_NAME context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6489 CB UI: Develop download-to-CSV handling based on selected variables



UPDATE platform.config
SET
    config_value = $$
		{
			"DEFAULT": {
				"suffix": "File",
				"attribute": "occurrenceName"
			},
			"FIELD_LAYOUT": {
				"suffix": "Field_Layout",
				"attribute": "locationName"
			},
			"DESIGN_LAYOUT": {
				"suffix": "Design_Layout",
				"attribute": "occurrenceName"
			},
			"MAPPING_FILES": {
				"suffix": "Mapping_Files",
				"attribute": "occurrenceName"
			},
			"PLOT_LIST_LOCATION": {
				"suffix": "Plot_List",
				"attribute": "locationName"
			},
			"PLOT_LIST_OCCURRENCE": {
				"suffix": "Plot_List",
				"attribute": "occurrenceName"
			},
			"EASY_HARVEST_LOCATION": {
				"suffix": "Easy_Harvest",
				"attribute": "locationName"
			},
			"ENTRY_LIST_OCCURRENCE": {
				"suffix": "Entry_List",
				"attribute": "occurrenceName"
			},
			"KSU_INVENTORY_LOCATION": {
				"suffix": "KSU_Inventory",
				"attribute": "locationName"
			},
			"EASY_HARVEST_OCCURRENCE": {
				"suffix": "Easy_Harvest",
				"attribute": "occurrenceName"
			},
			"KSU_INVENTORY_OCCURRENCE": {
				"suffix": "KSU_Inventory",
				"attribute": "occurrenceName"
			},
			"DATA_COLLECTION_SUMMARIES": {
				"suffix": "Data_Collection_Summaries",
				"attribute": "occurrenceName"
			},
			"MOISTURE_METER_1_LOCATION": {
				"suffix": "Moisture_Meter_1",
				"attribute": "locationName"
			},
			"MOISTURE_METER_1_OCCURRENCE": {
				"suffix": "Moisture_Meter_1",
				"attribute": "occurrenceName"
			},
			"PLANTING_INSTRUCTIONS_LOCATION": {
				"suffix": "Planting_Instructions",
				"attribute": "locationName"
			},
			"TRAIT_DATA_COLLECTION_LOCATION": {
				"suffix": "Trait_Data_Collection",
				"attribute": "locationName"
			},
			"PLANTING_INSTRUCTIONS_OCCURRENCE": {
				"suffix": "Planting_Instructions",
				"attribute": "occurrenceName"
			},
			"TRAIT_DATA_COLLECTION_OCCURRENCE": {
				"suffix": "Trait_Data_Collection",
				"attribute": "occurrenceName"
			},
			"METADATA_AND_PLOT_DATA": {
				"suffix": "Metadata_and_Plot_Data",
				"attribute": "occurrenceName"
			},
			"OCCURRENCE_DATA_COLLECTION": {
				"suffix": "Occurrence_Data_Collection",
				"attribute": "occurrenceName"
			}
		}
	$$
	WHERE
		abbrev = 'DOWNLOAD_FILE_NAME';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback "DEFAULT": {
--rollback 	"suffix": "File",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "FIELD_LAYOUT": {
--rollback 	"suffix": "Field_Layout",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "DESIGN_LAYOUT": {
--rollback 	"suffix": "Design_Layout",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "MAPPING_FILES": {
--rollback 	"suffix": "Mapping_Files",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "PLOT_LIST_LOCATION": {
--rollback 	"suffix": "Plot_List",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "PLOT_LIST_OCCURRENCE": {
--rollback 	"suffix": "Plot_List",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "EASY_HARVEST_LOCATION": {
--rollback 	"suffix": "Easy_Harvest",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "ENTRY_LIST_OCCURRENCE": {
--rollback 	"suffix": "Entry_List",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "KSU_INVENTORY_LOCATION": {
--rollback 	"suffix": "KSU_Inventory",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "EASY_HARVEST_OCCURRENCE": {
--rollback 	"suffix": "Easy_Harvest",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "KSU_INVENTORY_OCCURRENCE": {
--rollback 	"suffix": "KSU_Inventory",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "DATA_COLLECTION_SUMMARIES": {
--rollback 	"suffix": "Data_Collection_Summaries",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "MOISTURE_METER_1_LOCATION": {
--rollback 	"suffix": "Moisture_Meter_1",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "MOISTURE_METER_1_OCCURRENCE": {
--rollback 	"suffix": "Moisture_Meter_1",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "PLANTING_INSTRUCTIONS_LOCATION": {
--rollback 	"suffix": "Planting_Instructions",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "TRAIT_DATA_COLLECTION_LOCATION": {
--rollback 	"suffix": "Trait_Data_Collection",
--rollback 	"attribute": "locationName"
--rollback },
--rollback "PLANTING_INSTRUCTIONS_OCCURRENCE": {
--rollback 	"suffix": "Planting_Instructions",
--rollback 	"attribute": "occurrenceName"
--rollback },
--rollback "TRAIT_DATA_COLLECTION_OCCURRENCE": {
--rollback 	"suffix": "Trait_Data_Collection",
--rollback 	"attribute": "occurrenceName"
--rollback }
--rollback }
--rollback 		$$
--rollback 	WHERE
--rollback 		abbrev = 'DOWNLOAD_FILE_NAME';