--liquibase formatted sql

--changeset postgres:add_cb_export_variable_config_GLOBAL context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6432 CB-DB: Create access-specific variable configuration for Download widget



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    -- GLOBAL
    (
        'CB_GLOBAL_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS',
        'Core Breeding Global Export Template Variable Configuration',
        $${
            "experiment":[
                {
                    "abbrev": "EXPERIMENT_CODE",
                    "label": "Experiment Code",
                    "attribute": "experimentCode",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_NAME",
                    "label": "Experiment Name",
                    "attribute": "experimentName",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_YEAR",
                    "label": "Experiment Year",
                    "attribute": "experimentYear",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "SEASON",
                    "label": "Season",
                    "attribute": "experimentSeason",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "STAGE_CODE",
                    "label": "Stage Code",
                    "attribute": "experimentStageCode",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PROJECT",
                    "label": "Project",
                    "attribute": "experimentProject",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PIPELINE",
                    "label": "Pipeline",
                    "attribute": "experimentPipeline",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_OBJECTIVE",
                    "label": "Experiment Objective",
                    "attribute": "experimentObjective",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_TYPE",
                    "label": "Experiment Type",
                    "attribute": "experimentType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_SUB_TYPE",
                    "label": "Experiment Subtype",
                    "attribute": "experimentSubtype",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_DESIGN_TYPE",
                    "label": "Experiment Design Type",
                    "attribute": "experimentDesignType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_STATUS",
                    "label": "Experiment Status",
                    "attribute": "experimentStatus",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "EXPERIMENT_STEWARD",
                    "label": "Experiment Steward",
                    "attribute": "experimentSteward",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLANTING_SEASON",
                    "label": "Planting Season",
                    "attribute": "plantingSeason",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "MARKET_SEGMENT",
                    "label": "Market Segment",
                    "attribute": "marketSegment",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "ECOSYSTEM",
                    "label": "Ecosystem",
                    "attribute": "ecosystem",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "REMARKS",
                    "label": "Remarks",
                    "attribute": "experimentRemarks",
                    "required": "false",
                    "is_selected": "false"
                }
            ],
            "occurrence":[
                {
                    "abbrev": "OCCURRENCE_CODE",
                    "label": "Occurrence Code",
                    "attribute": "occurrenceCode",
                    "required": "true",
                    "is_selected": "true"
                },
                {
                    "abbrev": "OCCURRENCE_NAME",
                    "label": "Occurrence Name",
                    "attribute": "occurrenceName",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "OCCURRENCE_NUMBER",
                    "label": "Occurrence Number",
                    "attribute": "occurrenceCount",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "SITE_CODE",
                    "label": "Site Code",
                    "attribute": "siteCode",
                    "required": "true",
                    "is_selected": "true"
                },
                {
                    "abbrev": "SITE",
                    "label": "Site Name",
                    "attribute": "siteName",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "LOCATION_CODE",
                    "label": "Location Code",
                    "attribute": "locationCode",
                    "required": "true",
                    "is_selected": "true"
                },
                {
                    "abbrev": "LOCATION_NAME",
                    "label": "Location Name",
                    "attribute": "locationName",
                    "required": "true",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLANTING_DATE",
                    "label": "Planting Date",
                    "attribute": "plantingDate",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "GEOSPATIAL_COORDS",
                    "label": "Geospatial Coordinates",
                    "attribute": "geospatialCoordinates",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "LONGITUDE",
                    "label": "Longitude",
                    "attribute": "longitude",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "LATITUDE",
                    "label": "Latitude",
                    "attribute": "latitude",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ECOSYSTEM",
                    "label": "Ecosystem",
                    "attribute": "ecosystem",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "DESCRIPTION",
                    "label": "Description",
                    "attribute": "description",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "CONTCT_PERSON_CONT",
                    "label": "Contact Person",
                    "attribute": "contactPerson",
                    "required": "false",
                    "is_selected": "true"
                }
            ],
            "planting_protocol":[
                {
                    "abbrev": "ESTABLISHMENT",
                    "label": "Crop Establishment",
                    "attribute": "cropEstablishment",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLANTING_TYPE",
                    "label": "Planting Type",
                    "attribute": "plantingType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_TYPE",
                    "label": "Plot Type",
                    "attribute": "plotType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_LN",
                    "label": "Plot Length",
                    "attribute": "plotLength",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_WIDTH",
                    "label": "Plot Width",
                    "attribute": "plotWidth",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_AREA_SQM_CONT",
                    "label": "Plot Area sqm.",
                    "attribute": "plotAreaSQM",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ALLEY_LENGTH",
                    "label": "Alley Length",
                    "attribute": "alleyLength",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "TRANS_DATE_CONT",
                    "label": "Transplanting Date",
                    "attribute": "transplantingDate",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "SEEDING_RATE",
                    "label": "Seeding Density",
                    "attribute": "seedingRate",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLANTING_INSTRUCTIONS",
                    "label": "Planting Instructions",
                    "attribute": "plantingInstructions",
                    "required": "false",
                    "is_selected": "true"
                }
            ],
            "management_protocol":[
                {
                    "abbrev": "DIST_BET_ROWS",
                    "label": "Distance Between Rows",
                    "attribute": "distBetRows",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ROWS_PER_PLOT_CONT",
                    "label": "Rows per Plot",
                    "attribute": "rowsPerPlot",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "SEEDING_DATE_CONT",
                    "label": "Seeding Date",
                    "attribute": "seedingDate",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT1_TYPE_DISC",
                    "label": "Fertilizer Type - 1st Application",
                    "attribute": "fert1Type",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT1_DATE_CONT",
                    "label": "Fertilizer Date - 1st Application",
                    "attribute": "fert1Date",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT1_BRAND",
                    "label": "Fertilizer Brand - 1st Application",
                    "attribute": "fert1Brand",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT1_METH_DISC",
                    "label": "Fertilizer Method - 1st Application",
                    "attribute": "fert1Method",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT2_TYPE_DISC",
                    "label": "Fertilizer Type - 2nd Application",
                    "attribute": "fert2Type",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT2_DATE_CONT",
                    "label": "Fertilizer Date - 2nd Application",
                    "attribute": "fert2Date",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT2_BRAND",
                    "label": "Fertilizer Brand - 2nd Application",
                    "attribute": "fert2Brand",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT2_METH_DISC",
                    "label": "Fertilizer Method - 2nd Application",
                    "attribute": "fert2Method",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT3_TYPE_DISC",
                    "label": "Fertilizer Type - 3rd Application",
                    "attribute": "fert3Type",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT3_DATE_CONT",
                    "label": "Fertilizer Date - 3rd Application",
                    "attribute": "fert3Date",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT3_BRAND",
                    "label": "Fertilizer Brand - 3rd Application",
                    "attribute": "fert3Brand",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "FERT3_METH_DISC",
                    "label": "Fertilizer Method - 3rd Application",
                    "attribute": "fert3Method",
                    "required": "false",
                    "is_selected": "true"
                }
            ],
            "entry":[
                {
                    "abbrev": "ENTRY_NUMBER",
                    "label": "Entry Number",
                    "attribute": "entryNumber",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ENTRY_CODE",
                    "label": "Entry Code",
                    "attribute": "entryCode",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ENTRY_TYPE",
                    "label": "Entry Type",
                    "attribute": "entryType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ENTRY_CLASS",
                    "label": "Entry Class",
                    "attribute": "entryClass",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "ENTRY_ROLE",
                    "label": "Entry Role",
                    "attribute": "entryRole",
                    "required": "false",
                    "is_selected": "true"
                }
            ],
            "plot":[
                {
                    "abbrev": "PLOTNO",
                    "label": "Plot Number",
                    "attribute": "plotNumber",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_CODE",
                    "label": "Plot Code",
                    "attribute": "plotCode",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PLOT_TYPE",
                    "label": "Plot Type",
                    "attribute": "plotType",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "REP",
                    "label": "Replication",
                    "attribute": "rep",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "NO_OF_BLOCKS",
                    "label": "Blocks",
                    "attribute": "blockCount",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PA_X",
                    "label": "PA X",
                    "attribute": "paX",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "PA_Y",
                    "label": "PA Y",
                    "attribute": "paY",
                    "required": "false",
                    "is_selected": "true"
                }
            ],
            "germplasm":[
                {
                    "abbrev": "GERMPLASM_CODE",
                    "label": "Germplasm Code",
                    "attribute": "germplasmCode",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "GERMPLASM_NAME",
                    "label": "Germplasm Name",
                    "attribute": "germplasmName",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "GERMPLASM_STATE",
                    "label": "Germplasm State",
                    "attribute": "germplasmState",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "TAXONOMY_NAME",
                    "label": "Taxonomy Name",
                    "attribute": "taxonomyName",
                    "required": "false",
                    "is_selected": "true"
                },
                {
                    "abbrev": "GERMPLASM_CODE",
                    "label": "Parent 1 Germplasm Code",
                    "attribute": "p1GermplasmCode",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "GERMPLASM_CODE",
                    "label": "Parent 2 Germplasm Code",
                    "attribute": "p2GermplasmCode",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PARENTAGE",
                    "label": "Parentage",
                    "attribute": "parentage",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "GERMPLASM_TYPE",
                    "label": "Germplasm Type",
                    "attribute": "germplasmType",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "GERMPLASM_NAME_TYPE",
                    "label": "Germplasm Name Type",
                    "attribute": "germplasmNameType",
                    "required": "false",
                    "is_selected": "false"
                }
            ],
            "trait":[
                {
                    "abbrev": "BL_NURS_0_9",
                    "label": "Bl1 0-9",
                    "attribute": "BL_NURS_0_9",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "YLD_CONT2",
                    "label": "YLD_CONT2",
                    "attribute": "YLD_CONT2",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "YLD_CONT_TON",
                    "label": "YLD_TON",
                    "attribute": "YLD_CONT_TON",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "TIL_AVE_CONT",
                    "label": "TILLER_AVG",
                    "attribute": "TIL_AVE_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "NO_OF_SEED",
                    "label": "NO OF SEED",
                    "attribute": "NO_OF_SEED",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PACP_SCOR_1_9",
                    "label": "Pacp 1-9",
                    "attribute": "PACP_SCOR_1_9",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "FLW50",
                    "label": "FLW50",
                    "attribute": "FLW50",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "DATE_CROSSED",
                    "label": "CROSSING DATE",
                    "attribute": "DATE_CROSSED",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "TRANSFORMATION_DATE",
                    "label": "TRANSFORMATION DATE",
                    "attribute": "TRANSFORMATION_DATE",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HVDATE_CONT",
                    "label": "HARVEST DATE",
                    "attribute": "HVDATE_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "RLODGINC_CT_PLNTPLOT",
                    "label": "RLODGINC CT PLNTPLOT",
                    "attribute": "RLODGINC_CT_PLNTPLOT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PSTANDHV_CT_PLNTPLOT",
                    "label": "PSTANDHV CT PLNTPLOT",
                    "attribute": "PSTANDHV_CT_PLNTPLOT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HVHILL_CONT",
                    "label": "HV HILL",
                    "attribute": "HVHILL_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "TOTAL_HILL_CONT",
                    "label": "TOTAL HILLS",
                    "attribute": "TOTAL_HILL_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PSTANDTH_CT_PLNTPLOT",
                    "label": "PSTANDTH CT PLNTPLOT",
                    "attribute": "PSTANDTH_CT_PLNTPLOT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "RLODGINC_CMP_PCT",
                    "label": "RLODGINC CMP PCT",
                    "attribute": "RLODGINC_CMP_PCT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "ASI_TRAIT",
                    "label": "ASI Trait",
                    "attribute": "ASI_TRAIT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT4_CONT",
                    "label": "Ht4",
                    "attribute": "HT4_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "CML5_CONT",
                    "label": "CL5",
                    "attribute": "CML5_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "AYLD_CONT",
                    "label": "AYLD_G",
                    "attribute": "AYLD_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT11_CONT",
                    "label": "Ht11",
                    "attribute": "HT11_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT_CONT",
                    "label": "HT_AVG",
                    "attribute": "HT_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PNL5_CONT",
                    "label": "PnL 5",
                    "attribute": "PNL5_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT_CONT_ENT",
                    "label": "HT_AVG_ENT",
                    "attribute": "HT_CONT_ENT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT_CAT",
                    "label": "HT",
                    "attribute": "HT_CAT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "MAT_DATE_CONT",
                    "label": "Maturity date",
                    "attribute": "MAT_DATE_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT12_CONT",
                    "label": "Ht12",
                    "attribute": "HT12_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "BB_SES5_GH_SCOR_1_9",
                    "label": "BB GH 1-9 - SES5",
                    "attribute": "BB_SES5_GH_SCOR_1_9",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "FLW_DATE_CONT",
                    "label": "FLW DATE",
                    "attribute": "FLW_DATE_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "LG_SCOR_1_9",
                    "label": "Lg 1-9",
                    "attribute": "LG_SCOR_1_9",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT2_CONT",
                    "label": "Ht2",
                    "attribute": "HT2_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "GRAIN_COLOR",
                    "label": "Grain Color",
                    "attribute": "GRAIN_COLOR",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "FLW_CONT",
                    "label": "FLW",
                    "attribute": "FLW_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT9_CONT",
                    "label": "HT9",
                    "attribute": "HT9_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT8_CONT",
                    "label": "HT8",
                    "attribute": "HT8_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PNL6_CONT",
                    "label": "PnL 6",
                    "attribute": "PNL6_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT6_CONT",
                    "label": "Ht6",
                    "attribute": "HT6_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "NO_OF_EARS",
                    "label": "Number of Ears Selected",
                    "attribute": "NO_OF_EARS",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "MC_CONT",
                    "label": "MC",
                    "attribute": "MC_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "SPECIFIC_PLANT",
                    "label": "Specific plant number selected",
                    "attribute": "SPECIFIC_PLANT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "NO_OF_PLANTS",
                    "label": "NO OF PLANTS SELECTED",
                    "attribute": "NO_OF_PLANTS",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "CML6_CONT",
                    "label": "CL6",
                    "attribute": "CML6_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT10_CONT",
                    "label": "HT10",
                    "attribute": "HT10_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT7_CONT",
                    "label": "HT7",
                    "attribute": "HT7_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT5_CONT",
                    "label": "Ht5",
                    "attribute": "HT5_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "PANNO_SEL",
                    "label": "PANSEL",
                    "attribute": "PANNO_SEL",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT3_CONT",
                    "label": "Ht3",
                    "attribute": "HT3_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HETEROTIC_GROUP",
                    "label": "Heterotic Group",
                    "attribute": "HETEROTIC_GROUP",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HT1_CONT",
                    "label": "Ht1",
                    "attribute": "HT1_CONT",
                    "required": "false",
                    "is_selected": "false"
                },
                {
                    "abbrev": "HV_METH_DISC",
                    "label": "HARV METH",
                    "attribute": "HV_METH_DISC",
                    "required": "false",
                    "is_selected": "false"
                }                
            ]
        }$$,
        1,
        'cross_manager',
        1,
        'CORB-6432 CB-DB: Create access-specific variable configuration for Download widget'
    );



--rollback DELETE FROM platform.config WHERE abbrev='CB_GLOBAL_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS';