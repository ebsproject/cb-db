--liquibase formatted sql

--changeset postgres:update_harvest_status_scale_value_queued_for_harvest context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6479 CB-HM DB: Add/update scale values for the HARVEST_STATUS variable



UPDATE
	master.scale_value
SET
	value = 'QUEUED_FOR_HARVEST',
	description = 'Queued For Harvest',
	display_name = 'Queued For Harvest',
	abbrev = 'HARVEST_STATUS_QUEUED_FOR_HARVEST'
WHERE
	abbrev = 'HARVEST_STATUS_IN_QUEUE';



--rollback UPDATE
--rollback 	master.scale_value
--rollback SET
--rollback 	value = 'IN_QUEUE',
--rollback 	description = 'In Queue',
--rollback 	display_name = 'In Queue',
--rollback 	abbrev = 'HARVEST_STATUS_IN_QUEUE'
--rollback WHERE
--rollback 	abbrev = 'HARVEST_STATUS_QUEUED_FOR_HARVEST';