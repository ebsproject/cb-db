--liquibase formatted sql

--changeset postgres:add_terminal_transaction_to_dictionary.entity context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6448 DC: Store deletion of transaction background worker info in api.background_job instead of data_terminal.background_process table



INSERT INTO 
    dictionary.entity (abbrev, name, creator_id, table_id)
VALUES
    ('TERMINAL_TRANSACTION', 'terminal_transaction', 1, (SELECT id FROM dictionary.table WHERE abbrev = 'DATA_COLLECTION'));



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'TERMINAL_TRANSACTION';