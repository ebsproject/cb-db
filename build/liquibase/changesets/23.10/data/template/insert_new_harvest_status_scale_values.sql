--liquibase formatted sql

--changeset postgres:insert_new_harvest_status_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6479 CB-HM DB: Add/update scale values for the HARVEST_STATUS variable



WITH t_last_order_number AS (
    SELECT
        COALESCE(MAX(scalval.order_number), 0) AS last_order_number
    FROM
        master.variable AS var
        LEFT JOIN master.scale_value AS scalval
            ON scalval.scale_id = var.scale_id
    WHERE
        var.abbrev = 'HARVEST_STATUS'
        AND var.is_void = FALSE
        AND scalval.is_void = FALSE
)
INSERT INTO master.scale_value
    (scale_id, value, order_number, description, display_name, scale_value_status, abbrev, creator_id)
SELECT
    var.scale_id,
    scalval.value,
    t.last_order_number + ROW_NUMBER() OVER () AS order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    var.abbrev || '_' || scalval.abbrev AS abbrev,
    var.creator_id
FROM
    master.variable AS var,
    t_last_order_number AS t,
    (
        VALUES
        ('QUEUED_FOR_UPDATE', 'Queued For Update', 'Queued For Update', 'QUEUED_FOR_UPDATE', 'show'),
		('UPDATE_IN_PROGRESS', 'Update In Progress', 'Update In Progress', 'UPDATE_IN_PROGRESS', 'show'),
		('FAILED', 'Failed', 'Failed', 'FAILED', 'show'),
		('REVERT_IN_PROGRESS', 'Revert In Progress', 'Revert In Progress', 'REVERT_IN_PROGRESS', 'show'),
		('BAD_QC_CODE', 'Bad QC Code', 'Bad QC Code', 'BAD_QC_CODE', 'show'),
		('CONFLICT', 'Conflict', 'Conflict', 'CONFLICT', 'show')
    ) AS scalval (
        value, description, display_name, abbrev, scale_value_status
    )
WHERE
    var.abbrev = 'HARVEST_STATUS'
;



--rollback DELETE FROM master.scale_value
--rollback WHERE abbrev in (
--rollback 	'HARVEST_STATUS_QUEUED_FOR_UPDATE', 'HARVEST_STATUS_UPDATE_IN_PROGRESS', 'HARVEST_STATUS_FAILED',
--rollback 	'HARVEST_STATUS_REVERT_IN_PROGRESS', 'HARVEST_STATUS_BAD_QC_CODE', 'HARVEST_STATUS_CONFLICT'
--rollback );