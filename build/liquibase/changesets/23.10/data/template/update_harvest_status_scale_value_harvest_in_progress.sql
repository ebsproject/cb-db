--liquibase formatted sql

--changeset postgres:update_harvest_status_scale_value_harvest_in_progress context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6479 CB-HM DB: Add/update scale values for the HARVEST_STATUS variable



UPDATE
	master.scale_value
SET
	value = 'HARVEST_IN_PROGRESS',
	description = 'Harvest In Progress',
	display_name = 'Harvest In Progress',
	abbrev = 'HARVEST_STATUS_HARVEST_IN_PROGRESS'
WHERE
	abbrev = 'HARVEST_STATUS_IN_PROGRESS';



--rollback UPDATE
--rollback 	master.scale_value
--rollback SET
--rollback 	value = 'IN_PROGRESS',
--rollback 	description = 'In Progress',
--rollback 	display_name = 'In Progress',
--rollback 	abbrev = 'HARVEST_STATUS_IN_PROGRESS'
--rollback WHERE
--rollback 	abbrev = 'HARVEST_STATUS_HARVEST_IN_PROGRESS';