--liquibase formatted sql

--changeset postgres:insert_background_process_table_records_to_background_job_table context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6449 DC-DB: Move background_process records to background_job table



-- DataCollectionUploader records
INSERT INTO 
    api.background_job (worker_name, description, job_status, message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, event_log, is_seen, entity_id, entity, endpoint_entity, application, method, remarks)
SELECT 
    process_name, 'Upload Data Collection File',
    CASE
        WHEN dt.status = 'sent' THEN 'IN_QUEUE'
        WHEN dt.status = 'in progress' THEN 'IN_PROGRESS'
        WHEN dt.status = 'success' THEN 'DONE'
        ELSE 'FAILED'
    END AS job_status,
    message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, NULL as event_log, is_seen, transaction_id, 'TERMINAL_TRANSACTION', 'TERMINAL_TRANSACTION', 'QUALITY_CONTROL', 'POST', 'Migrated from data_terminal.background_process (23.10.23)'
FROM 
    data_terminal.background_process dt
WHERE 
    process_name = 'DataCollectionUploader';

-- QualityControlSuppressor records
INSERT INTO 
    api.background_job (worker_name, description, job_status, message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, event_log, is_seen, entity_id, entity, endpoint_entity, application, method, remarks)
SELECT 
    process_name, 'Suppression of records for transaction ID: ' || transaction_id, 
     CASE
        WHEN dt.status = 'sent' THEN 'IN_QUEUE'
        WHEN dt.status = 'in progress' THEN 'IN_PROGRESS'
        WHEN dt.status = 'success' THEN 'DONE'
        ELSE 'FAILED'
    END AS job_status,
    message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, NULL as event_log, is_seen, transaction_id, 'TERMINAL_TRANSACTION', 'TERMINAL_TRANSACTION', 'QUALITY_CONTROL', 'POST', 'Migrated from data_terminal.background_process (23.10.23)'
FROM 
    data_terminal.background_process dt
WHERE 
    process_name = 'QualityControlSuppressor';

-- DownloadDataset records
INSERT INTO 
    api.background_job (worker_name, description, job_status, message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, event_log, is_seen, entity_id, entity, endpoint_entity, application, method, remarks)
SELECT 
    'BuildCsvData', 'Dataset download', 
    CASE
        WHEN dt.status = 'sent' THEN 'IN_QUEUE'
        WHEN dt.status = 'in progress' THEN 'IN_PROGRESS'
        WHEN dt.status = 'success' THEN 'DONE'
        ELSE 'FAILED'
    END AS job_status, 
    message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, NULL as event_log, is_seen, transaction_id, 'TERMINAL_TRANSACTION', 'TERMINAL_TRANSACTION', 'QUALITY_CONTROL', 'POST', 'Migrated from data_terminal.background_process (23.10.23)'
FROM 
    data_terminal.background_process dt
WHERE 
    process_name = 'DownloadDataset';

-- DeleteTransactionRecords records
INSERT INTO 
    api.background_job (worker_name, description, job_status, message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, event_log, is_seen, entity_id, entity, endpoint_entity, application, method, remarks)
SELECT 
    process_name, 'Delete records for transaction ID: ' || transaction_id, 
    CASE
        WHEN dt.status = 'sent' THEN 'IN_QUEUE'
        WHEN dt.status = 'in progress' THEN 'IN_PROGRESS'
        ELSE 'FAILED'
    END AS job_status,
    message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, NULL as event_log, is_seen, transaction_id, 'TERMINAL_TRANSACTION', 'TERMINAL_TRANSACTION', 'QUALITY_CONTROL', 'POST', 'Migrated from data_terminal.background_process (23.10.23)'
FROM 
    data_terminal.background_process dt
WHERE 
    process_name = 'DeleteTransactionRecords';

-- QualityControlCommitter records
INSERT INTO 
    api.background_job (worker_name, description, job_status, message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, event_log, is_seen, entity_id, entity, endpoint_entity, application, method, remarks)
SELECT 
    process_name, 'Commit records for transaction ID: ' || transaction_id,
    CASE
        WHEN dt.status = 'sent' THEN 'IN_QUEUE'
        WHEN dt.status = 'in progress' THEN 'IN_PROGRESS'
        WHEN dt.status = 'success' THEN 'DONE'
        ELSE 'FAILED'
    END AS job_status,
    message, start_time, end_time, creator_id, creation_timestamp, modifier_id, modification_timestamp, is_void, notes, NULL as event_log, is_seen, transaction_id, 'TERMINAL_TRANSACTION', 'TERMINAL_TRANSACTION', 'QUALITY_CONTROL', 'POST', 'Migrated from data_terminal.background_process (23.10.25)'
FROM 
    data_terminal.background_process dt
WHERE 
    process_name = 'QualityControlCommitter';



--rollback DELETE FROM api.background_job WHERE remarks = 'Migrated from data_terminal.background_process (23.10.23)' AND worker_name = 'DataCollectionUploader';
--rollback DELETE FROM api.background_job WHERE remarks = 'Migrated from data_terminal.background_process (23.10.23)' AND worker_name = 'QualityControlSuppressor';
--rollback DELETE FROM api.background_job WHERE remarks = 'Migrated from data_terminal.background_process (23.10.23)' AND worker_name = 'BuildCsvData';
--rollback DELETE FROM api.background_job WHERE remarks = 'Migrated from data_terminal.background_process (23.10.23)' AND worker_name = 'DeleteTransactionRecords';
--rollback DELETE FROM api.background_job WHERE remarks = 'Migrated from data_terminal.background_process (23.10.25)' AND worker_name = 'QualityControlCommitter';