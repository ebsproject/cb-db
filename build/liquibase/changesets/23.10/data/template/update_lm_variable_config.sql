--liquibase formatted sql

--changeset postgres:update_lm_variable_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6328 CB-LM: Need for up to 21 plants on sample list



-- update lm variable configuration
UPDATE
	platform.config
SET
	config_value = $${
        "splitList": {
            "size": "500",
            "description": "Splitting of list."
        },
        "mergeLists": {
            "size": "500",
            "description": "Merging of lists."
        },
        "reorderAllBySort": {
            "size": "200",
            "description": "Update order number of list members."
        },
        "maxReplicate": {
            "size": "21",
            "description": "Maximum number of replicates that can be created at a time."
        }
    }$$
WHERE
	abbrev = 'LISTS_BG_PROCESSING_THRESHOLD';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback        "splitList": {
--rollback            "size": "500",
--rollback            "description": "Splitting of list."
--rollback        },
--rollback        "mergeLists": {
--rollback            "size": "500",
--rollback            "description": "Merging of lists."
--rollback        },
--rollback        "reorderAllBySort": {
--rollback            "size": "200",
--rollback            "description": "Update order number of list members."
--rollback        }
--rollback    }$$
--rollback WHERE
--rollback 	abbrev = 'LISTS_BG_PROCESSING_THRESHOLD';