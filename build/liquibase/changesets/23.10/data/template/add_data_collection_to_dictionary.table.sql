--liquibase formatted sql

--changeset postgres:add_data_collection_to_dictionary.table context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6448 DC: Store deletion of transaction background worker info in api.background_job instead of data_terminal.background_process table



INSERT INTO 
    dictionary.table (database_id, schema_id, abbrev, name, creator_id)
VALUES 
    (1, (SELECT id FROM dictionary.schema WHERE abbrev='DATA_TERMINAL'), 'DATA_COLLECTION', 'data_collection', 1);



--rollback DELETE FROM dictionary.table WHERE abbrev='DATA_COLLECTION';