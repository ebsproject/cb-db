--liquibase formatted sql

--changeset postgres:update_hm_bgprocess_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6478 CB-HM DB: Update HM Bulk Update threshold value from 5000 to 2500



UPDATE
	platform.config
SET
	config_value = $${
        "createList": {
            "size": "10000",
            "description": "Create new germplasm, seed, or package list"
        },
        "deleteSeeds": {
            "size": "200",
            "description": "Delete seeds of plots or crosses"
        },
        "previewList": {
            "size": "100",
            "description": "Preview germplasm, seed, or package list"
        },
        "deletePackages": {
            "size": "200",
            "description": "Delete packages and related records"
        },
        "exportPackages": {
            "size": "2000",
            "description": "Export created germplasm, seed, and package records"
        },
        "deleteHarvestData": {
            "size": "2000",
            "description": "Delete harvest data of plots or crosses"
        },
        "updateHarvestData": {
            "size": "2500",
            "description": "Update harvest data of plots or crosses"
        }
    }$$
WHERE
	abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback  "createList": {
--rollback      "size": "10000",
--rollback      "description": "Create new germplasm, seed, or package list"
--rollback  },
--rollback  "deleteSeeds": {
--rollback      "size": "200",
--rollback      "description": "Delete seeds of plots or crosses"
--rollback  },
--rollback  "previewList": {
--rollback      "size": "100",
--rollback      "description": "Preview germplasm, seed, or package list"
--rollback  },
--rollback  "deletePackages": {
--rollback      "size": "200",
--rollback      "description": "Delete packages and related records"
--rollback  },
--rollback  "exportPackages": {
--rollback      "size": "2000",
--rollback      "description": "Export created germplasm, seed, and package records"
--rollback  },
--rollback  "deleteHarvestData": {
--rollback      "size": "2000",
--rollback      "description": "Delete harvest data of plots or crosses"
--rollback  },
--rollback  "updateHarvestData": {
--rollback      "size": "5000",
--rollback      "description": "Update harvest data of plots or crosses"
--rollback  }
--rollback }$$
--rollback WHERE
--rollback 	abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';