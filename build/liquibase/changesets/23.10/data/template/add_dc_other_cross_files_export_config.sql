--liquibase formatted sql

--changeset postgres:add_dc_other_cross_files_export_config.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-6484: Add Cross List OTHER FILES export columns in configuration for KSU Fieldbook files in platform.config table



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'DC_GLOBAL_OTHER_CROSS_FILES_SETTINGS',
        'DC Global Other Cross File Settings',
        '{
            "Test Cross Interface 1": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Entry Number",
                        "attribute": "femaleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Entry Number",
                        "attribute": "maleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence",
                        "attribute": "femaleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence",
                        "attribute": "maleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence Code",
                        "attribute": "femaleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence Code",
                        "attribute": "maleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 2": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Entry Number",
                        "attribute": "femaleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Entry Number",
                        "attribute": "maleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence",
                        "attribute": "femaleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence",
                        "attribute": "maleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 3": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Entry Number",
                        "attribute": "femaleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Entry Number",
                        "attribute": "maleParentEntryNumber",
                        "abbrev": "ENTRY_NUMBER",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence Code",
                        "attribute": "femaleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence Code",
                        "attribute": "maleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            }
        }',
        'data_collection'
    ),
    (
        'DC_PROGRAM_IRSEA_OTHER_CROSS_FILES_SETTINGS',
        'DC Program IRSEA Other Cross File Settings',
        '{
            "Test Cross Interface 1": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence",
                        "attribute": "femaleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence",
                        "attribute": "maleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence Code",
                        "attribute": "femaleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence Code",
                        "attribute": "maleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 2": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence",
                        "attribute": "femaleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence",
                        "attribute": "maleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 3": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence Code",
                        "attribute": "femaleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence Code",
                        "attribute": "maleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            }
        }',
        'data_collection'
    ),
    (
        'DC_ROLE_COLLABORATOR_OTHER_CROSS_FILES_SETTINGS',
        'DC Role Collaborator Other Cross File Settings',
        '{
            "Test Cross Interface 1": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence",
                        "attribute": "femaleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence",
                        "attribute": "maleOccurrenceName",
                        "abbrev": "OCCURRENCE_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Occurrence Code",
                        "attribute": "femaleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Occurrence Code",
                        "attribute": "maleOccurrenceCode",
                        "abbrev": "OCCURRENCE_CODE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 2": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parentage",
                        "attribute": "femaleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parentage",
                        "attribute": "maleParentage",
                        "abbrev": "PARENTAGE",
                        "visible": true,
                        "identifier": false
                    }
                ]
            },
            "Test Cross Interface 3": {
                "Export": [
                    {
                        "header": "CROSS ID",
                        "attribute": "crossDbId",
                        "abbrev": "CROSS_ID",
                        "visible": false,
                        "identifier": true
                    },
                    {
                        "header": "Cross name",
                        "attribute": "crossName",
                        "abbrev": "CROSS_NAME",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Female Parent",
                        "attribute": "crossFemaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Male Parent",
                        "attribute": "crossMaleParent",
                        "abbrev": "DESIGNATION",
                        "visible": true,
                        "identifier": false
                    },
                    {
                        "header": "Method",
                        "attribute": "crossMethod",
                        "abbrev": "CROSS_METHOD",
                        "visible": true,
                        "identifier": false
                    }
                ]
            }
        }',
        'data_collection'
    );



--rollback DELETE FROM platform.config WHERE abbrev = 'DC_GLOBAL_OTHER_CROSS_FILES_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_PROGRAM_IRSEA_OTHER_CROSS_FILES_SETTINGS';
--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ROLE_COLLABORATOR_OTHER_CROSS_FILES_SETTINGS';