--liquibase formatted sql

--changeset postgres:12_populate_platform_list1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0049833','IRSEA-HB-2023-DS-005 Trait Protocol (EXP0049833)','IRSEA-HB-2023-DS-005 Trait Protocol (EXP0049833)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'829cb9a4-1aa9-4c13-ba0a-8b4624a426d2','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0049833','IRSEA-HB-2023-DS-005 Management Protocol (EXP0049833)','IRSEA-HB-2023-DS-005 Management Protocol (EXP0049833)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'ebec04f5-642a-4f74-9155-690522648894','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0049833-001','IRSEA-HB-2023-DS-005 Trait Protocol (EXP0049833)','IRSEA-HB-2023-DS-005 Trait Protocol (EXP0049833)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'3a9c80f9-079a-4430-bc77-18cd7f388ce8','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0049833-001','IRSEA-HB-2023-DS-005-001 Management Protocol (EXP0049833-001)','IRSEA-HB-2023-DS-005-001 Management Protocol (EXP0049833-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'7d2b1f7c-cfbb-4544-bc92-4a0ca556fcbc','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0049833',
--rollback        'MANAGEMENT_PROTOCOL_EXP0049833',
--rollback        'TRAIT_PROTOCOL_EXP0049833-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0049833-001'
--rollback    )
--rollback ;