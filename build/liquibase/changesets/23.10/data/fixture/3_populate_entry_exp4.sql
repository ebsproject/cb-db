--liquibase formatted sql

--changeset postgres:3_populate_entry_exp4 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.entry
        (entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id)
SELECT
    entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id
FROM
    (
        VALUES
            ('1',1,'IR 108008-B-B-B','test','female-and-male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024343'),(SELECT id FROM germplasm.seed WHERE seed_code='300080478'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000111')),
            ('2',2,'IR 108018-B-B-B','test','female-and-male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024344'),(SELECT id FROM germplasm.seed WHERE seed_code='300080479'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000211')),
            ('3',3,'IR 107989-B-B-B','test','female-and-male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024348'),(SELECT id FROM germplasm.seed WHERE seed_code='300080483'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000611')),
            ('4',4,'IR 107980-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024350'),(SELECT id FROM germplasm.seed WHERE seed_code='300080485'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000811')),
            ('5',5,'IR 107982-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024351'),(SELECT id FROM germplasm.seed WHERE seed_code='300080486'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000911')),
            ('6',6,'IR 107984-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024352'),(SELECT id FROM germplasm.seed WHERE seed_code='300080487'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121001011')),
            ('7',7,'IR 107995-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024345'),(SELECT id FROM germplasm.seed WHERE seed_code='300080480'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000311')),
            ('8',8,'IR 108000-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024346'),(SELECT id FROM germplasm.seed WHERE seed_code='300080481'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000411')),
            ('9',9,'IR 107971-B-B-B','test','female-and-male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024347'),(SELECT id FROM germplasm.seed WHERE seed_code='300080482'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000511')),
            ('10',10,'IR 107976-B-B-B','test','female-and-male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000024349'),(SELECT id FROM germplasm.seed WHERE seed_code='300080484'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='10110001109201511121000711')))
     t (entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id)
;



--rollback DELETE FROM
--rollback    experiment.entry
--rollback WHERE
--rollback    entry_list_id = (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00017583')
--rollback ;