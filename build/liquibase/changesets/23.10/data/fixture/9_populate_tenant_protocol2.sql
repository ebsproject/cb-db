--liquibase formatted sql

--changeset postgres:9_populate_tenant_protocol2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0049836','Trait Protocol EXP0049836','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('POLLINATION_PROTOCOL_EXP0049836','Pollination Protocol EXP0049836','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('PLANTING_PROTOCOL_EXP0049836','Planting Protocol EXP0049836','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('HARVEST_PROTOCOL_EXP0049836','Harvest Protocol EXP0049836','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0049836','Management Protocol EXP0049836','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0049836',
--rollback        'PLANTING_PROTOCOL_EXP0049836',
--rollback        'HARVEST_PROTOCOL_EXP0049836',
--rollback        'MANAGEMENT_PROTOCOL_EXP0049836',
--rollback        'POLLINATION_PROTOCOL_EXP0049836'
--rollback    )
--rollback ;