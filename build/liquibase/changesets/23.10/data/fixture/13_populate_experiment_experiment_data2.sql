--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.experiment_data
        (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
SELECT
    experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='PA_WL_SELECTED_INDEX'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW'),'Top Left','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='ENTRY_LIST_TIMES_REP'),'{"5825397":{"repno":1,"replicates":{"1":null}},"5825398":{"repno":1,"replicates":{"1":null}},"5825399":{"repno":1,"replicates":{"1":null}},"5825400":{"repno":1,"replicates":{"1":null}},"5825401":{"repno":1,"replicates":{"1":null}},"5825402":{"repno":1,"replicates":{"1":null}},"5825403":{"repno":1,"replicates":{"1":null}},"5825404":{"repno":1,"replicates":{"1":null}},"5825405":{"repno":1,"replicates":{"1":null}},"5825406":{"repno":1,"replicates":{"1":null}}}','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='PA_WL_ALL_FILTERED_INDEX'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='PA_ENTRY_WORKING_LIST'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'190304','N',(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0049836'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'190305','N',(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0049836'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'),(SELECT id FROM master.variable WHERE abbrev='PROTOCOL_TARGET_LEVEL'),'occurrence','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_data
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049836'
--rollback    )
--rollback ;