--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0047522','IRSEA-F1-2023-DS-005 Trait Protocol (EXP0047522)','IRSEA-F1-2023-DS-005 Trait Protocol (EXP0047522)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'581065ba-967b-406b-b51c-ef5ff1d2df7c','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0047522','IRSEA-F1-2023-DS-005 Management Protocol (EXP0047522)','IRSEA-F1-2023-DS-005 Management Protocol (EXP0047522)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'942377d4-57d6-4d02-bcb4-020368dfa08a','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0047522-001','IRSEA-F1-2023-DS-005-001 Trait Protocol (EXP0047522-001)','IRSEA-F1-2023-DS-005-001 Trait Protocol (EXP0047522-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'8cb5da9c-88bf-467e-813a-83e815bd05d4','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0047522-001','IRSEA-F1-2023-DS-005-001 Management Protocol (EXP0047522-001)','IRSEA-F1-2023-DS-005-001 Management Protocol (EXP0047522-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'dce0b645-019c-42a2-8157-265d991accdc','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0047522',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047522',
--rollback        'TRAIT_PROTOCOL_EXP0047522-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047522-001'
--rollback    )
--rollback ;