--liquibase formatted sql

--changeset postgres:8_populate_experiment_location_occurence_group3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.location_occurrence_group
        (location_id,occurrence_id,order_number,creator_id)
SELECT
    location_id,occurrence_id,order_number,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-003'),(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin')))
     t (location_id,occurrence_id,order_number,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.location_occurrence_group
--rollback WHERE
--rollback    location_id
--rollback IN
--rollback    (
--rollback        (SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-003')
--rollback    )
--rollback ;