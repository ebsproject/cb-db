--liquibase formatted sql

--changeset postgres:1_update_experiment_data_BA2023_AugRCBD_T206 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3048 Update experiment code of BA2023_AugRCBD_T206



UPDATE 
    experiment.experiment 
SET 
    experiment_code = (experiment.generate_code('experiment'))
WHERE experiment_name = 'BA2023_AugRCBD_T206';



--rollback UPDATE
--rollback 	experiment.experiment
--rollback SET
--rollback  experiment_code = 'EXP0047919'
--rollback
--rollback WHERE
--rollback 	experiment_name = 'BA2023_AugRCBD_T206';