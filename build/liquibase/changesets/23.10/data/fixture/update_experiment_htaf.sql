--liquibase formatted sql

--changeset postgres:update_experiment_htaf context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3013 Update generated fixture data for HTAF test



UPDATE 
    experiment.experiment 
SET 
    experiment_design_type = 'Systematic Arrangement', 
    stage_id = (SELECT id FROM tenant.stage WHERE stage_code='F2') 
WHERE experiment_name = 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)';



--rollback UPDATE
--rollback 	experiment.experiment
--rollback SET
--rollback 	experiment_design_type = 'Alpha-lattice', 
--rollback  stage_id = (SELECT id FROM tenant.stage WHERE stage_code='AYT') 
--rollback WHERE
--rollback 	experiment_name = 'KE_MAIZE_EXPERIMENT(GENERATION_NURSERY)';