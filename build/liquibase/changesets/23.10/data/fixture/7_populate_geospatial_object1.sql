--liquibase formatted sql

--changeset postgres:7_populate_geospatial_object1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    place.geospatial_object
        (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
SELECT
    geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates::polygon,altitude::float,description,creator_id
FROM
    (
        VALUES
            ('IRRIHQ-2023-DS-006','IRRIHQ-2023-DS-006','planting area','breeding location',NULL,NULL,NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (geospatial_object_code,geospatial_object_name,geospatial_object_type,geospatial_object_subtype,geospatial_coordinates,altitude,description,creator_id)
;

UPDATE place.geospatial_object SET parent_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ') WHERE geospatial_object_code='IRRIHQ-2023-DS-006';
UPDATE place.geospatial_object SET root_geospatial_object_id=(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='PH') WHERE geospatial_object_code='IRRIHQ-2023-DS-006';



--rollback DELETE FROM
--rollback    place.geospatial_object
--rollback WHERE
--rollback    geospatial_object_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2023-DS-006'
--rollback    )
--rollback ;