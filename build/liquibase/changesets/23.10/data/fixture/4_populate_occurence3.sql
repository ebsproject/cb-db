--liquibase formatted sql

--changeset postgres:3_populate_occurence3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.occurrence
        (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
SELECT
    occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id::int,creator_id,rep_count,site_id,field_id::int,occurrence_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('EXP0047456-001','IRSEA-F1-2023-DS-003-001','planted',NULL,(SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047456'),NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,1,NULL,10,10))
     t (occurrence_code,occurrence_name,occurrence_status,description,experiment_id,geospatial_object_id,creator_id,rep_count,site_id,field_id,occurrence_number,remarks,entry_count,plot_count)
;



--rollback DELETE FROM
--rollback    experiment.occurrence
--rollback WHERE
--rollback    occurrence_code
--rollback IN
--rollback    (
--rollback        'EXP0047456-001'
--rollback    )
--rollback ;