--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.experiment_data
        (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
SELECT
    experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW'),'Top Left','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='PA_WL_ALL_FILTERED_INDEX'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='PA_WL_SELECTED_INDEX'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='ENTRY_LIST_TIMES_REP'),'{"5825357":{"repno":1,"replicates":{"1":null}},"5825358":{"repno":1,"replicates":{"1":null}},"5825359":{"repno":1,"replicates":{"1":null}},"5825360":{"repno":1,"replicates":{"1":null}},"5825361":{"repno":1,"replicates":{"1":null}},"5825362":{"repno":1,"replicates":{"1":null}},"5825363":{"repno":1,"replicates":{"1":null}},"5825364":{"repno":1,"replicates":{"1":null}},"5825365":{"repno":1,"replicates":{"1":null}},"5825366":{"repno":1,"replicates":{"1":null}}}','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='PA_ENTRY_WORKING_LIST'),'[]','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'190294','N',(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0049833'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'190295','N',(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0049833'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'),(SELECT id FROM master.variable WHERE abbrev='PROTOCOL_TARGET_LEVEL'),'occurrence','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_data
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0049833'
--rollback    )
--rollback ;