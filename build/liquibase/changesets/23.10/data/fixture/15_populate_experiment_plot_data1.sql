--liquibase formatted sql

--changeset postgres:15_populate_experiment_plot_data1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.plot_data
        (plot_id,variable_id,data_value,data_qc_code,transaction_id,creator_id,remarks,notes)
SELECT
    plot_id,variable_id,data_value,data_qc_code,transaction_id,creator_id,remarks,'DEVOPS-2852 Populate experiment to dev and qa1'
FROM
    (
        VALUES
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='1'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='2'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='2'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='3'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='3'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='4'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='4'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='5'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='5'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='6'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='6'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='7'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='7'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='8'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='8'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='9'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='9'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='10'),(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT'),'2023-07-28','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL),
            ((SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0017556-003') AND plot_number='10'),(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC'),'Bulk','Q',12132147,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),NULL))
     t (plot_id,variable_id,data_value,data_qc_code,transaction_id,creator_id,remarks)
;



--rollback DELETE FROM
--rollback    experiment.plot_data
--rollback WHERE
--rollback    notes = 'DEVOPS-2852 Populate experiment to dev and qa1'
--rollback ;