--liquibase formatted sql

--changeset postgres:3_populate_entry_exp2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.entry
        (entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id)
SELECT
    entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id
FROM
    (
        VALUES
            ('1',1,'IR06M139','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004370'),(SELECT id FROM germplasm.seed WHERE seed_code='4242092'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512107')),
            ('2',2,'IR10G102','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000000177'),(SELECT id FROM germplasm.seed WHERE seed_code='1838118'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512106')),
            ('3',3,'IR10G107','test','female',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004372'),(SELECT id FROM germplasm.seed WHERE seed_code='4242109'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512108')),
            ('4',4,'IRRI 102','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000053000'),(SELECT id FROM germplasm.seed WHERE seed_code='4039766'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512109')),
            ('5',5,'IRRI 216','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000053023'),(SELECT id FROM germplasm.seed WHERE seed_code='3173031'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512110')),
            ('6',6,'IRRI 221','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000003549'),(SELECT id FROM germplasm.seed WHERE seed_code='2972813'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512111')),
            ('7',7,'IRRI 222','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004503'),(SELECT id FROM germplasm.seed WHERE seed_code='3768919'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512112')),
            ('8',8,'IRRI 224','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004020'),(SELECT id FROM germplasm.seed WHERE seed_code='4527746'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512113')),
            ('9',9,'IRRI 233','test','male',NULL,'active',NULL,  (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004414'),(SELECT id FROM germplasm.seed WHERE seed_code='3887748'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512114')),
            ('10',10,'IRRI 234','test','male',NULL,'active',NULL,(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802'),(SELECT id FROM germplasm.germplasm WHERE germplasm_code='GE000000004403'),(SELECT id FROM germplasm.seed WHERE seed_code='4341197'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),(SELECT id FROM germplasm.package WHERE package_code='PKG000020512115')))
     t (entry_code,entry_number,entry_name,entry_type,entry_role,entry_class,entry_status,description,entry_list_id,germplasm_id,seed_id,creator_id,package_id)
;



--rollback DELETE FROM
--rollback    experiment.entry
--rollback WHERE
--rollback    entry_list_id = (SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019802')
--rollback ;