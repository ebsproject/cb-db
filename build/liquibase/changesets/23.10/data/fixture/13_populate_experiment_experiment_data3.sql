--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.experiment_data
        (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
SELECT
    experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id
FROM
    (
        VALUES
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047456'),(SELECT id FROM master.variable WHERE abbrev='FIRST_PLOT_POSITION_VIEW'),'Top Left','N',NULL,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047456'),(SELECT id FROM master.variable WHERE abbrev='TRAIT_PROTOCOL_LIST_ID'),'188115','N',(SELECT id FROM tenant.protocol WHERE protocol_code='TRAIT_PROTOCOL_EXP0047456'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ((SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047456'),(SELECT id FROM master.variable WHERE abbrev='MANAGEMENT_PROTOCOL_LIST_ID'),'188114','N',(SELECT id FROM tenant.protocol WHERE protocol_code='MANAGEMENT_PROTOCOL_EXP0047456'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (experiment_id,variable_id,data_value,data_qc_code,protocol_id,creator_id)
;



--rollback DELETE FROM
--rollback    experiment.experiment_data
--rollback WHERE
--rollback    experiment_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.experiment WHERE experiment_code='EXP0047456'
--rollback    )
--rollback ;