--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0047456','IRSEA-F1-2023-DS-003 Trait Protocol (EXP0047456)','IRSEA-F1-2023-DS-003 Trait Protocol (EXP0047456)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'4ded74fc-b2e0-4041-acc3-4e587c6abd94','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0047456','IRSEA-F1-2023-DS-003 Management Protocol (EXP0047456)','IRSEA-F1-2023-DS-003 Management Protocol (EXP0047456)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'432dfffa-e5aa-490a-b664-0e5922a86f77','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0047456-001','IRSEA-F1-2023-DS-003-001 Trait Protocol (EXP0047456-001)','IRSEA-F1-2023-DS-003-001 Trait Protocol (EXP0047456-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'33f545e3-dd65-49c2-882a-320901270e13','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0047456-001','IRSEA-F1-2023-DS-003-001 Management Protocol (EXP0047456-001)','IRSEA-F1-2023-DS-003-001 Management Protocol (EXP0047456-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'64e82218-5016-4a89-a995-6bf8c909b75f','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0047456',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047456',
--rollback        'TRAIT_PROTOCOL_EXP0047456-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047456-001'
--rollback    )
--rollback ;