--liquibase formatted sql

--changeset postgres:update_experiment_code_exp_4 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3049 Update experiment code of 'IRSEA-F1-2023-DS-005'



UPDATE 
    experiment.experiment 
SET 
    experiment_code = (experiment.generate_code('experiment'))
WHERE experiment_name = 'IRSEA-F1-2023-DS-005';



--rollback UPDATE
--rollback 	experiment.experiment
--rollback SET
--rollback  experiment_code = 'EXP0047522'
--rollback
--rollback WHERE
--rollback 	experiment_name = 'IRSEA-F1-2023-DS-005' ;