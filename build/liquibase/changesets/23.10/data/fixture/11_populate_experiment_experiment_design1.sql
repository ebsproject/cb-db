--liquibase formatted sql

--changeset postgres:11_populate_experiment_experiment_design1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.experiment_design
        (occurrence_id,design_id,plot_id,block_type,block_value,block_level_number,creator_id,block_name)
SELECT
    occurrence_id,design_id,plot_id,block_type,block_value,block_level_number,creator_id,block_name
FROM
    (
        VALUES
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='1'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='2'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='3'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='4'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='5'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='6'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='7'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='8'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='9'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'),1,(SELECT id FROM experiment.plot WHERE occurrence_id=(SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001') AND plot_number='10'),'replication block',1,1,(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'replicate'))
            
    t (occurrence_id,design_id,plot_id,block_type,block_value,block_level_number,creator_id,block_name)
;



--rollback DELETE FROM
--rollback    experiment.experiment_design
--rollback WHERE
--rollback    occurrence_id
--rollback IN
--rollback    (
--rollback        SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0047456-001'
--rollback    )
--rollback ;