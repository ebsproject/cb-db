--liquibase formatted sql

--changeset postgres:9_populate_tenant_protocol3 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    tenant.protocol
        (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
SELECT
    protocol_code,protocol_name,protocol_type,description,program_id,creator_id
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0047456','Trait Protocol EXP0047456','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('POLLINATION_PROTOCOL_EXP0047456','Pollination Protocol EXP0047456','trait',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),            
            ('PLANTING_PROTOCOL_EXP0047456','Planting Protocol EXP0047456','planting',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('HARVEST_PROTOCOL_EXP0047456','Harvest Protocol EXP0047456','harvest',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')),
            ('MANAGEMENT_PROTOCOL_EXP0047456','Management Protocol EXP0047456','management',NULL,(SELECT id FROM tenant.program WHERE program_code='IRSEA'),(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin')))
     t (protocol_code,protocol_name,protocol_type,description,program_id,creator_id)
;



--rollback DELETE FROM
--rollback    tenant.protocol
--rollback WHERE
--rollback    protocol_code
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0047456',
--rollback        'PLANTING_PROTOCOL_EXP0047456',
--rollback        'HARVEST_PROTOCOL_EXP0047456',
--rollback        'MANAGEMENT_PROTOCOL_EXP0047456',
--rollback        'POLLINATION_PROTOCOL_EXP0047456'
--rollback    )
--rollback ;