--liquibase formatted sql

--changeset postgres:5_populate_location1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.location
        (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
SELECT
    location_code,location_name,location_status,location_type,description,steward_id,location_planting_date::date,location_harvest_date::date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id::int,location_number,remarks,entry_count,plot_count
FROM
    (
        VALUES
            ('IRRIHQ-2023-DS-007','IRRIHQ-2023-DS-007','planted','planting area',NULL,(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),NULL,NULL,(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),(SELECT id FROM tenant.person WHERE person_name='EBS, Admin'),2023,(SELECT id FROM tenant.season WHERE season_code='DS'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ'),NULL,7,NULL,0,10))
     t (location_code,location_name,location_status,location_type,description,steward_id,location_planting_date,location_harvest_date,geospatial_object_id,creator_id,location_year,season_id,site_id,field_id,location_number,remarks,entry_count,plot_count)
;



--rollback UPDATE experiment.plot p
--rollback SET    location_id = NULL
--rollback WHERE  location_id = (SELECT id
--rollback                       FROM   experiment.location
--rollback                       WHERE  location_code = 'IRRIHQ-2023-DS-007'); 
--rollback 
--rollback DELETE FROM
--rollback    experiment.location
--rollback WHERE
--rollback    location_code
--rollback IN
--rollback    (
--rollback        'IRRIHQ-2023-DS-007'
--rollback    )
--rollback ;