--liquibase formatted sql

--changeset postgres:6_populate_plot1 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    experiment.plot
        (occurrence_id,location_id,entry_id,plot_code,plot_number,plot_type,rep,design_x,design_y,plot_order_number,pa_x,pa_y,field_x,field_y,plot_status,plot_qc_code,creator_id,block_number,harvest_status)
SELECT
    occurrence_id,location_id,entry_id,plot_code,plot_number,plot_type,rep::int,design_x::int,design_y::int,plot_order_number::int,pa_x::int,pa_y::int,field_x::int,field_y::int,plot_status,plot_qc_code,creator_id,block_number,harvest_status
FROM
    (
        VALUES
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='1'AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'1',1,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='2' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'2',2,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='3'AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'3',3,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='4'AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'4',4,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='5' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'5',5,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='6' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'6',6,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='7'AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'7',7,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='8' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'8',8,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='9' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'9',9,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'),
            ((SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'),(SELECT id FROM experiment.location WHERE location_code='IRRIHQ-2023-DS-006'),(SELECT id FROM experiment.entry WHERE entry_number='10' AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_code='ENTLIST00019796')),'10',10,'plot',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active','G',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),1,'COMPLETED'))
            
     t (occurrence_id,location_id,entry_id,plot_code,plot_number,plot_type,rep,design_x,design_y,plot_order_number,pa_x,pa_y,field_x,field_y,plot_status,plot_qc_code,creator_id,block_number,harvest_status)
;



--rollback DELETE FROM
--rollback    experiment.plot
--rollback WHERE
--rollback    occurrence_id
--rollback IN
--rollback    (
--rollback       SELECT id FROM experiment.occurrence WHERE occurrence_code='EXP0049833-001'
--rollback    )
--rollback ;