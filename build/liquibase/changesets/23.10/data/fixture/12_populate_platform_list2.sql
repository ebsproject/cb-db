--liquibase formatted sql

--changeset postgres:12_populate_platform_list2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2852 Populate experiment to dev and qa



INSERT INTO 
    platform.list
        (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
SELECT
    abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid::uuid,list_usage,status,is_active,list_sub_type
FROM
    (
        VALUES
            ('TRAIT_PROTOCOL_EXP0049836','IRSEA-F1-2023-DS-002 Trait Protocol (EXP0049836)','IRSEA-F1-2023-DS-002 Trait Protocol (EXP0049836)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'4637f5f2-af04-46e9-859f-f8c84acf991a','working list','created',True,'trait protocol'),
            ('MANAGEMENT_PROTOCOL_EXP0049836','IRSEA-F1-2023-DS-002 Management Protocol (EXP0049836)','IRSEA-F1-2023-DS-002 Management Protocol (EXP0049836)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'11d7c49d-e1aa-425d-bebf-1aafc87c3559','working list','created',True,'management protocol'),
            ('TRAIT_PROTOCOL_EXP0049836-001','IRSEA-F1-2023-DS-002-001 Trait Protocol (EXP0049836-001)','IRSEA-F1-2023-DS-002-001 Trait Protocol (EXP0049836-001)','trait',(SELECT id FROM dictionary.entity WHERE abbrev='TRAIT'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'88899e4c-9a5b-459b-8581-7fb34e5cc004','working list','created',True,NULL),
            ('MANAGEMENT_PROTOCOL_EXP0049836-001','IRSEA-F1-2023-DS-002-001 Management Protocol (EXP0049836-001)','IRSEA-F1-2023-DS-002-001 Management Protocol (EXP0049836-001)','variable',(SELECT id FROM dictionary.entity WHERE abbrev='VARIABLE'),NULL,'created using Experiment Creation tool',(SELECT id FROM tenant.person WHERE email='admin@ebsproject.org' AND username='admin'),'3fa26a77-d583-464c-ac1b-823488816ae7','working list','created',True,NULL))
     t (abbrev,name,display_name,type,entity_id,description,remarks,creator_id,record_uuid,list_usage,status,is_active,list_sub_type)
;



--rollback DELETE FROM
--rollback    platform.list
--rollback WHERE
--rollback    abbrev
--rollback IN
--rollback    (
--rollback        'TRAIT_PROTOCOL_EXP0049836',
--rollback        'MANAGEMENT_PROTOCOL_EXP0049836',
--rollback        'TRAIT_PROTOCOL_EXP0049836-001',
--rollback        'MANAGEMENT_PROTOCOL_EXP0049836-001'
--rollback    )
--rollback ;