--liquibase formatted sql

--changeset postgres:update_experiment_code_exp_2 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-3049 Update experiment code of 'IRSEA-F1-2023-DS-002'



UPDATE 
    experiment.experiment 
SET 
    experiment_code = (experiment.generate_code('experiment'))
WHERE experiment_name = 'IRSEA-F1-2023-DS-002';



--rollback UPDATE
--rollback 	experiment.experiment
--rollback SET
--rollback  experiment_code = 'EXP0049836'
--rollback
--rollback WHERE
--rollback 	experiment_name = 'IRSEA-F1-2023-DS-002' ;