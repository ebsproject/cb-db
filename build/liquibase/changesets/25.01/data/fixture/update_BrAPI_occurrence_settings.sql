--liquibase formatted sql

--changeset postgres:update_BrAPI_occurrence_settings context:fixture splitStatements:False rollbackSplitStatements:False
--comment: BDS-4571 Update BrAPI Occurrence Settings - experiment year metadata



UPDATE
    platform.config
SET
    config_value = $${
        "traits": [
            "MC_CONT",
            "AYLD_CONT"
        ],
        "metadata": {
            "SEASON": [
                "DS",
                "WS"
            ],
            "EXPERIMENT_YEAR": [
                "2025",
                "2024"
            ]
        },
        "variable": {
            "configureTraits": false
        },
        "germplasm": {
            "showPedigree": true,
            "exportData": [
                "pedigree",
                "synonym"
            ]
        }
    }$$
WHERE
    abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';

UPDATE
    platform.config
SET
    config_value = $${
        "traits": [
            "MC_CONT",
            "AYLD_CONT"
        ],
        "metadata": {
            "SEASON": [
                "DS"
            ],
            "EXPERIMENT_YEAR": [
                "2025",
                "2024"
            ]
        },
        "variable": {
            "configureTraits": false
        },
        "germplasm": {
            "showPedigree": true,
            "exportData": [
                "pedigree",
                "synonym"
            ]
        }
    }$$
WHERE
    abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';

UPDATE
	platform.config
SET
	config_value = $${
        "traits": 
            [
                "HT1_CONT", 
                "HT2_CONT"
            ], 
        "metadata": {
            "SEASON": [
                "DS",
                "WS"
            ], 
            "EXPERIMENT_YEAR": [
                "2025",
                "2024"
            ]
        },
        "germplasm": {
            "showPedigree": false
        },
        "variable": {
            "configureTraits": false
        }
    }$$
WHERE
	abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "traits": [
--rollback             "MC_CONT",
--rollback             "AYLD_CONT"
--rollback         ],
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS",
--rollback                 "WS"
--rollback             ],
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2024",
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "variable": {
--rollback             "configureTraits": false
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": true,
--rollback             "exportData": [
--rollback                 "pedigree",
--rollback                 "synonym"
--rollback             ]
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'BRAPI_GLOBAL_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "traits": [
--rollback             "MC_CONT",
--rollback             "AYLD_CONT"
--rollback         ],
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS"
--rollback             ],
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2024",
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "variable": {
--rollback             "configureTraits": false
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": true,
--rollback             "exportData": [
--rollback                 "pedigree",
--rollback                 "synonym"
--rollback             ]
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'BRAPI_IRSEA_OCCURRENCE_SETTINGS';
--rollback 
--rollback UPDATE
--rollback 	platform.config
--rollback SET
--rollback 	config_value = $${
--rollback         "traits": 
--rollback             [
--rollback                 "HT1_CONT", 
--rollback                 "HT2_CONT"
--rollback             ], 
--rollback         "metadata": {
--rollback             "SEASON": [
--rollback                 "DS",
--rollback                 "WS"
--rollback             ], 
--rollback             "EXPERIMENT_YEAR": [
--rollback                 "2024",
--rollback                 "2023"
--rollback             ]
--rollback         },
--rollback         "germplasm": {
--rollback             "showPedigree": false
--rollback         },
--rollback         "variable": {
--rollback             "configureTraits": false
--rollback         }
--rollback     }$$
--rollback WHERE
--rollback 	abbrev = 'BRAPI_COLLABORATOR_OCCURRENCE_SETTINGS';



