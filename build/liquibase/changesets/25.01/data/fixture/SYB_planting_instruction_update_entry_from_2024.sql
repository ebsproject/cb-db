--liquibase formatted sql

--changeset postgres:SYB_planting_instruction_update_entry_from_2024 context:fixture splitStatements:False rollbackSplitStatements:False
--comment: BDS-4531 update Soybean fixture data entry_id on experiment.planting_instruction where entry_list has status cross_list_specified


UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0003' AND is_void= False LIMIT 1)
WHERE
entry_code = '3' AND 
entry_number = 3 AND 
entry_name = 'SYB_fixed_0003' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '3' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0006' AND is_void= False LIMIT 1)
WHERE
entry_code = '6' AND 
entry_number = 6 AND 
entry_name = 'SYB_fixed_0006' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '6' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001'  AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0005' AND is_void= False LIMIT 1)
WHERE
entry_code = '5' AND 
entry_number = 5 AND 
entry_name = 'SYB_fixed_0005' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '5' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0001' AND is_void= False LIMIT 1)
WHERE
entry_code = '1' AND 
entry_number = 1 AND 
entry_name = 'SYB_fixed_0001' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '1' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0010' AND is_void= False LIMIT 1)
WHERE
entry_code = '10' AND 
entry_number = 10 AND 
entry_name = 'SYB_fixed_0010' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '10' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0007' AND is_void= False LIMIT 1)
WHERE
entry_code = '7' AND 
entry_number = 7 AND 
entry_name = 'SYB_fixed_0007' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '7' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' ) AND entry_name= 'SYB_fixed_0009'  AND is_void= false LIMIT 1)
WHERE
entry_code = '9' AND 
entry_number = 9 AND 
entry_name = 'SYB_fixed_0009' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '9' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' ) AND entry_name= 'SYB_fixed_0004' AND is_void= false LIMIT 1)
WHERE
entry_code = '4' AND 
entry_number = 4 AND 
entry_name = 'SYB_fixed_0004' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '4' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified') AND entry_name= 'SYB_fixed_0002' AND is_void= false LIMIT 1)
WHERE
entry_code = '2' AND 
entry_number = 2 AND 
entry_name = 'SYB_fixed_0002' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '2' LIMIT 1); 

UPDATE experiment.planting_instruction
SET
entry_id = (SELECT id FROM experiment.entry WHERE entry_list_id =(SELECT id from experiment.entry_list where entry_list_name = 'SYB-HB-2024-DS-001' AND entry_list_status = 'cross list specified' ) AND entry_name= 'SYB_fixed_0008' AND is_void= false LIMIT 1)
WHERE
entry_code = '8' AND 
entry_number = 8 AND 
entry_name = 'SYB_fixed_0008' AND 
plot_id IN (SELECT id FROM experiment.plot WHERE occurrence_id = (SELECT id from experiment.occurrence where occurrence_name = 'SYB-HB-2018-DS-001-001') AND plot_code = '8' LIMIT 1);



--rollback SELECT NULL;