--liquibase formatted sql

--changeset postgres:update_gm_threshold_config_select_all context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3945 CB-GM: Selecting all search results automatically grays out the printout button even for a small number of results



UPDATE
    platform.config
SET
    config_value = $${
        "searchCharMin": {
            "size": "3",
            "description": "Minimum number of characters when searching."
        },
        "germplasmCodingMax": {
            "size": "5",
            "description": "Maximum threshold value when processing germplasm coding"
        },
        "ancestryExportDepth": {
            "size": "10",
            "description": "Maximum depth of exported germplasm ancestry information."
        },
        "printoutsMaxCount": {
            "size": "1000",
            "description": "Maximum number of germplasm records to be processed."
        }
    }$$
WHERE
    abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback  UPDATE
--rollback      platform.config
--rollback  SET
--rollback      config_value = $${
--rollback    "searchCharMin": {
--rollback      "size": "3",
--rollback      "description": "Minimum number of characters when searching."
--rollback    },
--rollback    "germplasmCodingMax": {
--rollback      "size": "5",
--rollback      "description": "Maximum threshold value when processing germplasm coding"
--rollback    },
--rollback    "ancestryExportDepth": {
--rollback      "size": "10",
--rollback      "description": "Maximum depth of exported germplasm ancestry information."
--rollback    }
--rollback  }$$
--rollback  WHERE
--rollback      abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';

