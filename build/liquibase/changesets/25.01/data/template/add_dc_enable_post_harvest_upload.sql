--liquibase formatted sql

--changeset postgres:add_dc_enable_post_harvest_upload context:template splitStatements:False rollbackSplitStatements:False
--comment: BDS-2486 CB-DC: Add toggle in post harvest upload for plots and crosses



INSERT INTO platform.config (
    abbrev, 
    name, 
    config_value, 
    rank, 
    usage, 
    creator_id, 
    notes
)
VALUES(
    'DC_ENABLE_POST_HARVEST_UPLOAD',
    'Data Collection Enable Post Harvest Upload',
    '{
        "plotPostHarvestData": true,
        "crossPostHarvestData": true
    }',
    1,
    'data collection',
    1,
    'BDS-2486 CB-DC: Add toggle in post harvest upload for plots and crosses'
);



--rollback DELETE FROM platform.config WHERE abbrev = 'DC_ENABLE_POST_HARVEST_UPLOAD';
