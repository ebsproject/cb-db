--liquibase formatted sql

--changeset postgres:update_hm_package_label_config_rice_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-3772 CB-HM: Update HM package label config for rice (SSN Single Cross)



--update germplasm name config
UPDATE platform.config
SET
	config_value = '{
        "PLOT": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "plotCode",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "CROSS": {
            "default": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "single cross": {
                "default": {
                    "default": {
                        "bulk": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "transgenesis": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "genome editing": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "crossName",
                                "order_number": 0
                            }
                        ],
                        "single seed numbering": [
                            {
                                "type": "field",
                                "entity": "germplasm",
                                "field_name": "designation",
                                "order_number": 0
                            }
                        ]
                    }
                }
            }
        },
        "harvest_mode": {
            "cross_method": {
                "germplasm_state": {
                    "germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    }
    '
WHERE
	abbrev = 'HM_LABEL_PATTERN_PACKAGE_RICE_DEFAULT'
;



--rollback UPDATE platform.config
--rollback SET
--rollback 	config_value = '{
--rollback         "PLOT": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "plotCode",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "CROSS": {
--rollback             "default": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "single cross": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "bulk": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": ":",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "transgenesis": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "germplasm",
--rollback                                 "field_name": "designation",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "genome editing": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "cross",
--rollback                                 "field_name": "crossName",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "single seed numbering": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "germplasm",
--rollback                                 "field_name": "designation",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         },
--rollback         "harvest_mode": {
--rollback             "cross_method": {
--rollback                 "germplasm_state": {
--rollback                     "germplasm_type": {
--rollback                         "harvest_method": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "ABC",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "<entity>",
--rollback                                 "field_name": "<field_name>",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "<schema>",
--rollback                                 "order_number": 4,
--rollback                                 "sequence_name": "<sequence_name>"
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     }
--rollback     '
--rollback WHERE
--rollback 	abbrev = 'HM_LABEL_PATTERN_PACKAGE_RICE_DEFAULT'
--rollback ;