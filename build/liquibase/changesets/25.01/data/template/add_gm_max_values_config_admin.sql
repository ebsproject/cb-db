--liquibase formatted sql

--changeset postgres:add_gm_max_values_config_admin.sql context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'GM_ROLE_ADMIN_PROCESS_THRESHOLD_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-3945: CB-GM: Selecting all search results automatically grays out the printout button even for a small number of results



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'GM_ROLE_ADMIN_PROCESS_THRESHOLD_CONFIG',
        'Admin role config for threshold values of different Germplasm Manager processes',
        $${
            "GM_MAX_NO_OF_ROWS_FOR_PRINTOUT":"1000"
        }$$,
        1,
        'Config for germplasm manager process thresholds',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-3945: CB-GM: Selecting all search results automatically grays out the printout button even for a small number of results'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='GM_ROLE_ADMIN_PROCESS_THRESHOLD_CONFIG';