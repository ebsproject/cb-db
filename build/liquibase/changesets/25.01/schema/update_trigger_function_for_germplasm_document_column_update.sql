--liquibase formatted sql

--changeset postgres:update_trigger_function_for_germplasm_document_column_update context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS-4244 CB-GM: Coding - Proposed germplasm name upon search is returning a different record



CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
        SELECT
            concat(
                setweight(to_tsvector(unaccent(gn.germplasm_normalized_name)),'A'),' ',
                setweight(to_tsvector(unaccent(gn.name_value)),'A'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
                setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
                setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
                setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C'),' ',
                setweight(to_tsvector(unaccent(new.germplasm_type)),'C')
            ) into var_document
        FROM (
                SELECT
                    regexp_replace((array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name)::varchar),'[^a-zA-Y0-9-]',' ','g'),
                    regexp_replace((array_agg(gn.name_value order by gn.name_value)::varchar),'[^a-zA-Y0-9-]',' ','g')
                FROM
                    germplasm.germplasm_name gn
                WHERE
                    gn.germplasm_id = new.id
                    AND gn.is_void = FALSE
                    AND gn.germplasm_name_status != $$draft$$
            ) AS gn (
                germplasm_normalized_name,
                name_value
            )
        ;
        
        new.germplasm_document = var_document;
    END IF;
    
    RETURN NEW;
END;
$function$
;

DROP TRIGGER IF EXISTS germplasm_update_germplasm_document_tgr ON germplasm.germplasm;

CREATE TRIGGER germplasm_update_germplasm_document_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm FOR EACH ROW EXECUTE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
;

CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm_from_germp_name()
    RETURNS trigger
    LANGUAGE plpgsql
AS $function$
DECLARE
    var_document varchar;
BEGIN

    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        IF (TG_OP = 'UPDATE') THEN

			IF old.germplasm_id <> new.germplasm_id THEN

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = new.germplasm_id;

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = old.germplasm_id;

			ELSE

				UPDATE 
					germplasm.germplasm
				SET 
					modification_timestamp = now() 
				WHERE 
					id = new.germplasm_id;

			END IF;

         ELSE
			
			UPDATE 
            	germplasm.germplasm
        	SET 
            	modification_timestamp = now() 
        	WHERE 
            	id = new.germplasm_id;

		END IF;
		
    END IF;
    
    RETURN NEW;
END;
$function$
;

DROP TRIGGER IF EXISTS germplasm_name_update_germplasm_document_from_germp_name_tgr ON germplasm.germplasm_name;

CREATE TRIGGER germplasm_name_update_germplasm_document_from_germp_name_tgr BEFORE
INSERT OR UPDATE
    ON
    germplasm.germplasm_name FOR EACH ROW EXECUTE FUNCTION germplasm.update_document_column_for_germplasm_germplasm_from_germp_name()
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm()
--rollback     RETURNS trigger
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN
--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        
--rollback         SELECT
--rollback             concat(
--rollback                 setweight(to_tsvector(unaccent(gn.germplasm_normalized_name)),'A'),' ',
--rollback                 setweight(to_tsvector(unaccent(gn.name_value)),'A'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.germplasm_normalized_name)),'A'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.designation)),'B'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.parentage)),'B'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.generation)),'B'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.germplasm_state)),'C'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.germplasm_name_type)),'C'),' ',
--rollback                 setweight(to_tsvector(unaccent(new.germplasm_type)),'C')
--rollback             ) into var_document
--rollback         FROM (
--rollback                 SELECT
--rollback                     regexp_replace((array_agg(gn.germplasm_normalized_name order by gn.germplasm_normalized_name)::varchar),'[^a-zA-Y0-9-]',' ','g'),
--rollback                     regexp_replace((array_agg(gn.name_value order by gn.name_value)::varchar),'[^a-zA-Y0-9-]',' ','g')
--rollback                 FROM
--rollback                     germplasm.germplasm_name gn
--rollback                 WHERE
--rollback                     gn.germplasm_id = new.id
--rollback                     AND gn.is_void = FALSE
--rollback             ) AS gn (
--rollback                 germplasm_normalized_name,
--rollback                 name_value
--rollback             )
--rollback         ;
        
--rollback         new.germplasm_document = var_document;
--rollback     END IF;
    
--rollback     RETURN NEW;
--rollback END;
--rollback $function$
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION germplasm.update_document_column_for_germplasm_germplasm_from_germp_name()
--rollback     RETURNS trigger
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_document varchar;
--rollback BEGIN

--rollback     IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
--rollback         IF (TG_OP = 'UPDATE') THEN

--rollback 			IF old.germplasm_id <> new.germplasm_id THEN

--rollback 				UPDATE 
--rollback 					germplasm.germplasm
--rollback 				SET 
--rollback 					modification_timestamp = now() 
--rollback 				WHERE 
--rollback 					id = new.germplasm_id;

--rollback 				UPDATE 
--rollback 					germplasm.germplasm
--rollback 				SET 
--rollback 					modification_timestamp = now() 
--rollback 				WHERE 
--rollback 					id = old.germplasm_id;

--rollback 			ELSE

--rollback 				UPDATE 
--rollback 					germplasm.germplasm
--rollback 				SET 
--rollback 					modification_timestamp = now() 
--rollback 				WHERE 
--rollback 					id = new.germplasm_id;

--rollback 			END IF;

--rollback          ELSE
			
--rollback 			UPDATE 
--rollback             	germplasm.germplasm
--rollback         	SET 
--rollback             	modification_timestamp = now() 
--rollback         	WHERE 
--rollback             	id = new.germplasm_id;

--rollback 		END IF;
		
--rollback     END IF;
    
--rollback     RETURN NEW;
--rollback END;
--rollback $function$
--rollback ;