--liquibase formatted sql

--changeset postgres:drop_constraint_occurrence_id_plot_code_in_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-663 Drop constraint occurrence_id + plot_code in experiment.plot



ALTER TABLE experiment.plot DROP CONSTRAINT plot_occurrence_id_plot_code_idx;



--rollback ALTER TABLE experiment.plot
--rollback     ADD CONSTRAINT plot_occurrence_id_plot_code_idx UNIQUE (occurrence_id, plot_code);
--rollback 
--rollback COMMENT ON CONSTRAINT plot_occurrence_id_plot_code_idx ON experiment.plot
--rollback     IS 'A plot can be retrieved by its unique occurrence and plot code.';