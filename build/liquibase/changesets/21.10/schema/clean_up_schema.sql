--liquibase formatted sql

--changeset postgres:clean_up_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-713 Clean up schema



DROP TABLE IF EXISTS dictionary.column CASCADE;

DROP TABLE IF EXISTS api.authorization CASCADE;
DROP TABLE IF EXISTS api.oauth_access_tokens CASCADE;

DROP TABLE IF EXISTS master.service_relation CASCADE;
DROP TABLE IF EXISTS master.service_required_variables CASCADE;
DROP TABLE IF EXISTS master.service_team CASCADE;

DROP TABLE IF EXISTS master.timing_rule CASCADE;

DROP TABLE IF EXISTS master.user_filter CASCADE;
DROP TABLE IF EXISTS master.user_session CASCADE;

DROP TABLE IF EXISTS operational.platform CASCADE;
DROP TABLE IF EXISTS operational.printouts CASCADE;
DROP TABLE IF EXISTS operational.service_request CASCADE;
DROP TABLE IF EXISTS operational.service_request_metadata CASCADE;
DROP TABLE IF EXISTS operational.study_task_definition CASCADE;
DROP TABLE IF EXISTS operational.vendor CASCADE;

DROP TABLE IF EXISTS seed_warehouse.plate CASCADE;
DROP TABLE IF EXISTS seed_warehouse.plate_layout CASCADE;
DROP TABLE IF EXISTS seed_warehouse.sample CASCADE;
DROP TABLE IF EXISTS seed_warehouse.sample_metadata CASCADE;
DROP TABLE IF EXISTS seed_warehouse.temp_sample CASCADE;

DROP SCHEMA IF EXISTS seed_warehouse CASCADE;

DROP TABLE IF EXISTS tenant.service CASCADE;
DROP TABLE IF EXISTS tenant.service_provider CASCADE;
DROP TABLE IF EXISTS tenant.service_provider_team CASCADE;



--rollback SELECT NULL;