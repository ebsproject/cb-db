--liquibase formatted sql

--changeset postgres:fix_time_without_time_zone_columns context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-722 Fix time without time zone columns



-- change column data type
ALTER TABLE
    platform.module
ALTER COLUMN
    modification_timestamp
SET
    DATA TYPE timestamp without time zone
    USING (creation_timestamp::timestamp without time zone)
;

-- change column data type
ALTER TABLE
    platform.item_module
ALTER COLUMN
    modification_timestamp
SET
    DATA TYPE timestamp without time zone
    USING (creation_timestamp::timestamp without time zone)
;



-- revert changes
--rollback ALTER TABLE
--rollback     platform.module
--rollback ALTER COLUMN
--rollback     modification_timestamp
--rollback SET
--rollback     DATA TYPE time without time zone
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     platform.item_module
--rollback ALTER COLUMN
--rollback     modification_timestamp
--rollback SET
--rollback     DATA TYPE time without time zone
--rollback ;
