--liquibase formatted sql

--changeset postgres:add_column_block_no_to_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-724 Add column column_block_no to experiment.plot



ALTER TABLE experiment.plot
    ADD COLUMN column_block_no integer;

COMMENT ON COLUMN experiment.plot.column_block_no
    IS 'Column Block Number: Column block information of plot [PLOT_COLBLKNO]';



--rollback ALTER TABLE experiment.plot DROP COLUMN column_block_no;
