--liquibase formatted sql

--changeset postgres:add_program_id_column_to_experiment_planting_job_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-727 Add program_id column to experiment.planting_job table



-- add column
ALTER TABLE
    experiment.planting_job
ADD COLUMN
    program_id integer
;

-- comment column
COMMENT ON COLUMN
    experiment.planting_job.program_id
IS
    'Program ID: Reference to the program where the planting job is created [PJOB_PROG_ID]'
;

-- add foreign key constraint
ALTER TABLE experiment.planting_job
    ADD CONSTRAINT planting_job_program_id_fk
    FOREIGN KEY (program_id)
    REFERENCES tenant.program (id)
    ON UPDATE CASCADE ON DELETE RESTRICT
;



-- revert changes
--rollback ALTER TABLE experiment.planting_job
--rollback     DROP CONSTRAINT planting_job_program_id_fk
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.planting_job
--rollback DROP COLUMN
--rollback     program_id
--rollback ;
