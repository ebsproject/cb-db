--liquibase formatted sql

--changeset postgres:enable_person_update_person_document_tgr context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-739 Enable trigger person_update_person_document_tgr



ALTER TABLE tenant.person ENABLE TRIGGER person_update_person_document_tgr;



--rollback ALTER TABLE tenant.person DISABLE TRIGGER person_update_person_document_tgr;