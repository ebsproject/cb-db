--liquibase formatted sql

--changeset postgres:add_row_block_no_to_experiment.plot context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-724 Add column row_block_no to experiment.plot



ALTER TABLE experiment.plot
    ADD COLUMN row_block_no integer;

COMMENT ON COLUMN experiment.plot.row_block_no
    IS 'Row Block Number: Row block information of plot [PLOT_ROWBLKNO]';



--rollback ALTER TABLE experiment.plot DROP COLUMN row_block_no;
