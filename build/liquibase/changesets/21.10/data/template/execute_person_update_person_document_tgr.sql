--liquibase formatted sql

--changeset postgres:execute_person_update_person_document_tgr context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-739 Execute person_update_person_document_tgr



UPDATE tenant.person SET is_void = False WHERE is_void = False;



--rollback UPDATE tenant.person SET is_void = False WHERE is_void = False;