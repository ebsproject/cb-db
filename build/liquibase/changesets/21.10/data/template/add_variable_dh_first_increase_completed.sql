--liquibase formatted sql

--changeset postgres:add_variable_dh_first_increase_completed context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-730 Add variable DH_FIRST_INCREASE_COMPLETED



DO $$
DECLARE
	var_property_id int;
	var_method_id int;
	var_scale_id int;
	var_variable_id int;
	var_variable_set_id int;
	var_variable_set_member_order_number int;
	var_count_property_id int;
	var_count_method_id int;
	var_count_scale_id int;
	var_count_variable_set_id int;
	var_count_variable_set_member_id int;
BEGIN

	--PROPERTY

	SELECT count(id) FROM master.property WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_count_property_id;
	IF var_count_property_id > 0 THEN
		SELECT id FROM master.property WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_property_id;
	ELSE
		INSERT INTO
			master.property (abbrev,display_name,name) 
		VALUES 
			('DH_FIRST_INCREASE_COMPLETED','DH First Increase Completed','DH First Increase Completed') 
		RETURNING id INTO var_property_id;
	END IF;

	--METHOD

	SELECT count(id) FROM master.method WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_count_method_id;
	IF var_count_method_id > 0 THEN
		SELECT id FROM master.method WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_method_id;
	ELSE
		INSERT INTO
			master.method (name,abbrev,formula,description) 
		VALUES 
			('DH First Increase Completed','DH_FIRST_INCREASE_COMPLETED',NULL,NULL) 
		RETURNING id INTO var_method_id;
	END IF;

	--SCALE

	SELECT count(id) FROM master.scale WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_count_scale_id;
	IF var_count_scale_id > 0 THEN
		SELECT id FROM master.scale WHERE ABBREV = 'DH_FIRST_INCREASE_COMPLETED' INTO var_scale_id;
	ELSE
		INSERT INTO
			master.scale (abbrev,type,name,unit,level) 
		VALUES 
			('DH_FIRST_INCREASE_COMPLETED','categorical','DH First Increase Completed',NULL,'nominal') 
		RETURNING id INTO var_scale_id;
	END IF;

	--SCALE VALUE

	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'true',1,'true','true','DH_FIRST_INCREASE_COMPLETED_TRUE');
	INSERT INTO 
		master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
	VALUES 
		(var_scale_id,'false',2,'false','false','DH_FIRST_INCREASE_COMPLETED_FALSE');

	--VARIABLE

    INSERT INTO
        master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
    VALUES 
        ('active','DH First Increase Completed','DH First Increase Completed','boolean','Describes if the DH material already went through a seed increase.','DH First Increase Completed','True','DH_FIRST_INCREASE_COMPLETED','study','system','experiment, study') 
    RETURNING id INTO var_variable_id;

	--UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

	UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

	--VARIABLE_SET_ID

	SELECT count(id) FROM master.variable_set WHERE ABBREV = 'HARVEST_INFO' INTO var_count_variable_set_id;
	IF var_count_variable_set_id > 0 THEN
		SELECT id FROM master.variable_set WHERE ABBREV = 'HARVEST_INFO' INTO var_variable_set_id;
	ELSE
		INSERT INTO
			master.variable_set (abbrev,name) 
		VALUES 
			('HARVEST_INFO','Harvest Information') 
		RETURNING id INTO var_variable_set_id;
	END IF;

	--GET THE LAST ORDER NUMBER

	SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
	IF var_count_variable_set_member_id > 0 THEN
		SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
	ELSE
		var_variable_set_member_order_number = 1;
	END IF;

	--ADD VARIABLE SET MEMBER

	INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED');

--rollback DELETE FROM master.variable WHERE abbrev = 'DH_FIRST_INCREASE_COMPLETED';
