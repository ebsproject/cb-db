--liquibase formatted sql

--changeset postgres:update_bims_irri_account_to_admin context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-559 Update bims.irri to admin account



UPDATE
    tenant.person
SET
    username='admin',
    first_name='Admin',
    last_name='EBS',
    person_name='EBS, Admin',
    email='admin@ebsproject.org'
WHERE
    id = 1;



--rollback UPDATE
--rollback     tenant.person
--rollback SET
--rollback     username='bims.irri',
--rollback     first_name='Bims',
--rollback     last_name='Irri',
--rollback     person_name='Irri, Bims',
--rollback     email='bims.irri@gmail.com'
--rollback WHERE
--rollback     id = 1;