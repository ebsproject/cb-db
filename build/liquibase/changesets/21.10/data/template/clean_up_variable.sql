--liquibase formatted sql

--changeset postgres:clean_up_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-713 Clean up variable



DO $$
DECLARE
    var_id int;
    frm_id int;
    record_count int;
    deleted_var_id int;
    deleted_frm_id int;
BEGIN

    -- select ids of variables
    FOR var_id IN (
        SELECT 
            id
        FROM 
            master.variable v 
        WHERE
            abbrev 
        IN 
            (
                'AC_CO_SCOR_0_5',
                'AC_SES4_SCOR_1_2',
                'ADJAYLD_G_CONT',
                'ADJAYLD_G_CONT2',
                'ADJAYLD_G_CONT3',
                'ADJAYLD_KG_CONT',
                'ADJAYLD_KG_CONT3',
                'ADJAYLD_KG_CONT4',
                'ADJAYLD_KG_CONT5',
                'ADJAYLD_KG_CONT6',
                'ADJYLD3_CONT',
                'AMY_CONT_PERC',
                'AREA_FACT',
                'AYLD_KG_CONT',
                'BB1_S',
                'BB1_SCOR_1_9',
                'BB2_S',
                'BB2_SCOR_1_9',
                'BB_FLD_SCOR_1_9',
                'BL_NURS_0_9',
                'BL_SCOR2_0_9',
                'BLS_SCOR_0_9',
                'BPH_GH_SCOR_SEGRE',
                'BPH_GRNH_0_9',
                'BPH_SES5_GH_SCOR_0_9',
                'BRSC',
                'CLK_0_10_PERC',
                'CLK_10_25_PERC',
                'CLK_25_50_PERC',
                'CLK_50_75_PERC',
                'CLK_GT_75_PERC',
                'CLK_PERC',
                'CLK_SCOR_0_9',
                'CML10_CONT',
                'CML1_CONT',
                'CML2_CONT',
                'CML3_CONT',
                'CML4_CONT',
                'CML7_CONT',
                'CML8_CONT',
                'CML9_CONT',
                'CML_CONT',
                'DIS',
                'DISEASE_INDEX',
                'DRYING_TEMPERATURE',
                'DTH_CONT',
                'FLW_100DATE_CONT',
                'FLW_1STDATE_CONT',
                'FLW_50DATE_CONT',
                'FLW_CONT_ENT',
                'FSM_SCOR_0_9',
                'GELC_PERC',
                'GELT_SCOR_CHAR',
                'GLH_SCOR_0_9',
                'GLH_SCOR_SEGRE',
                'GRL_SES_CONT_MM',
                'GRL_STDDEV_CONT',
                'GRNWT_PLANT_CONT',
                'GRNWT_PS10_CONT',
                'GRNWT_PS15_CONT',
                'GRNWT_PS20_CONT',
                'GRNWT_PS25_CONT',
                'GRNWT_PS50_CONT',
                'GRNWT_PS5_CONT',
                'GRNYLD_HARV_CON',
                'GRW_CONT_MM',
                'GRW_STDDEV_CONT',
                'HARVEST_DATE',
                'HARVEST_UNIT',
                'HARVEST_UNIT_MULTIPLE',
                'HDR_PERC',
                'HVHILL_CONT',
                'LG_CAT',
                'LG_PCT',
                'MAT_1STDATE_CONT',
                'MAT_50DATE_CONT',
                'MAT_CONT',
                'MAT_CONT2',
                'MC_HARV_CONT',
                'MF_CONT',
                'MF_HARV_CONT',
                'MISS_HILL_CONT',
                'MPS_SCOR_1_3',
                'NBLS_CAT_2',
                'OTS',
                'PAN_CROSS',
                'PANNO10_CONT',
                'PANNO11_CONT',
                'PANNO12_CONT',
                'PANNO13_CONT',
                'PANNO14_CONT',
                'PANNO15_CONT',
                'PANNO1_CONT',
                'PANNO2_CONT',
                'PANNO3_CONT',
                'PANNO4_CONT',
                'PANNO5_CONT',
                'PANNO6_CONT',
                'PANNO7_CONT',
                'PANNO8_CONT',
                'PANNO9_CONT',
                'PANNO_CONT',
                'PANNO_CONT_ENT',
                'PBR_PERC',
                'PHG_SCOR_1_9',
                'PHPN_SCOR_1_9',
                'PHP_SCOR_1_9',
                'PLANT_TESTED_SCORE1',
                'PLANT_TESTED_SCORE3',
                'PLANT_TESTED_SCORE5',
                'PLANT_TESTED_SCORE7',
                'PLANT_TESTED_SCORE9',
                'PLOT_COUNT_CONT',
                'PMR_PERC',
                'PNL10_CONT',
                'PNL11_CONT',
                'PNL12_CONT',
                'PNL13_CONT',
                'PNL14_CONT',
                'PNL15_CONT',
                'PNL16_CONT',
                'PNL17_CONT',
                'PNL18_CONT',
                'PNL19_CONT',
                'PNL1_CONT',
                'PNL20_CONT',
                'PNL21_CONT',
                'PNL22_CONT',
                'PNL23_CONT',
                'PNL24_CONT',
                'PNL25_CONT',
                'PNL2_CONT',
                'PNL3_CONT',
                'PNL4_CONT',
                'PNL7_CONT',
                'PNL8_CONT',
                'PNL9_CONT',
                'PNL_CONT',
                'RI_SCOR_0_9',
                'RTD_FLD_SCOR_1_9',
                'SEEDLING_COUNT',
                'SEED_USED',
                'SELECTION_TYPE',
                'SUB_TOL_14DAD',
                'SUB_TOL_4DAD',
                'SUB_TOL_7DAD',
                'THRESHDATE_HARV_CONT',
                'TIL_AVE_CONT',
                'TIL_AVE_CONT_ENT',
                'TILL10_CONT',
                'TILL1_CONT',
                'TILL2_CONT',
                'TILL3_CONT',
                'TILL4_CONT',
                'TILL5_CONT',
                'TILL6_CONT',
                'TILL7_CONT',
                'TILL8_CONT',
                'TILL9_CONT',
                'TILLER_AVE',
                'TOTAL_HILL_CONT',
                'TOTAL_PLANT_TESTED',
                'TOT_GRN_CONT',
                'TOTILL_CONT',
                'TPLYLD_CONT_PS',
                'VVG_SCOR_1_9',
                'WBR_CONT',
                'WHDR_CONT',
                'WMR_CONT',
                'WPDY_CONT',
                'YLD_0_CONT1',
                'YLD_0_CONT1_ENT',
                'YLD_0_CONT2',
                'YLD_CONT1',
                'YLD_CONT1_ENT',
                'YLD_CONT2',
                'YLD_CONT2_ENT',
                'YLD_CONT3',
                'YLD_CONT_TON',
                'YLD_CONT_TON2',
                'YLD_CONT_TON3',
                'YLD_CONT_TON5',
                'YLD_CONT_TON6',
                'YLD_CONT_TON7',
                'YLD_CONT_TON_ENT',
                'YLD_TON_HA'
                )
            ORDER BY abbrev 
        )
    LOOP
        -- count experiment.entry_data record
        SELECT count(*) FROM experiment.entry_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.entry_data 
            DELETE FROM experiment.entry_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.entry_data %', record_count;
        END IF;
    
        -- count experiment.experiment_data record
        SELECT count(*) FROM experiment.experiment_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.experiment_data 
            DELETE FROM experiment.experiment_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.experiment_data %', record_count;
        END IF;
    
        -- count experiment.location_data record
        SELECT count(*) FROM experiment.location_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.location_data 
            DELETE FROM experiment.location_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.location_data %', record_count;
        END IF;
    
        -- count experiment.occurrence_data record
        SELECT count(*) FROM experiment.occurrence_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.occurrence_data 
            DELETE FROM experiment.location_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.location_data %', record_count;
        END IF;
    
        -- count experiment.plant_data record
        SELECT count(*) FROM experiment.plant_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.occurrence_data 
            DELETE FROM experiment.plant_data WHERE variable_id = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.plant_data %', record_count;
        END IF;
    
        -- count experiment.plot_data record
        SELECT count(*) FROM experiment.plot_data WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            DELETE FROM experiment.plot_data WHERE variable_id  = var_id;
            RAISE  NOTICE '!TOTAL DELETED RECORDS FROM experiment.plot_data %', record_count;
        END IF;
    
        -- count experiment.sample_measurement record
        SELECT count(*) FROM experiment.sample_measurement  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.sample_measurement 
            DELETE FROM experiment.sample_measurement WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.sample_measurement %', record_count;
        END IF;
    
        -- count experiment.subplot_data record
        SELECT count(*) FROM experiment.subplot_data  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete experiment.subplot_data 
            DELETE FROM experiment.subplot_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM experiment.subplot_data %', record_count;
        END IF;
    
        -- count germplasm.cross_attribute record
        SELECT count(*) FROM germplasm.cross_attribute  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.cross_attribute 
            DELETE FROM germplasm.cross_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.cross_attribute %', record_count;
        END IF;
    
        -- count germplasm.cross_data record
        SELECT count(*) FROM germplasm.cross_data  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.cross_data 
            DELETE FROM germplasm.cross_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.cross_data %', record_count;
        END IF;
    
        -- count germplasm.germplasm_attribute record
        SELECT count(*) FROM germplasm.germplasm_attribute  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.germplasm_attribute 
            DELETE FROM germplasm.germplasm_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.germplasm_attribute %', record_count;
        END IF;
    
        -- count germplasm.germplasm_trait record
        SELECT count(*) FROM germplasm.germplasm_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.germplasm_trait 
            DELETE FROM germplasm.germplasm_trait WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.germplasm_trait %', record_count;
        END IF;
    
        -- count germplasm.package_data record
        SELECT count(*) FROM germplasm.package_data  WHERE variable_id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete germplasm.package_data 
            DELETE FROM germplasm.package_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.package_data %', record_count;
        END IF;
    
        -- count germplasm.package_trait record
        SELECT count(*) FROM germplasm.package_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.package_trait 
            DELETE FROM germplasm.package_trait WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.package_trait %', record_count;
        END IF;
    
        -- count germplasm.seed_attribute record
        SELECT count(*) FROM germplasm.package_trait  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.seed_attribute 
            DELETE FROM germplasm.seed_attribute WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.seed_attribute %', record_count;
        END IF;
    
        -- count germplasm.seed_data record
        SELECT count(*) FROM germplasm.seed_data  WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete germplasm.seed_data 
            DELETE FROM germplasm.seed_data WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM germplasm.seed_data %', record_count;
        END IF;
    
        -- count master.formula record
        SELECT count(*) FROM master.formula  WHERE result_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
                
            SELECT id FROM master.formula WHERE result_variable_id = var_id INTO frm_id;
            
            -- delete master.formula_parameter 
            DELETE FROM master.formula_parameter WHERE formula_id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula_parameter %', record_count;
        
            -- delete master.formula
            DELETE FROM master.formula WHERE id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula %', record_count;
            
            -- delete master.method 
            DELETE FROM master.method WHERE formula_id = frm_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.method %', record_count;
    
        END IF;
    
        SELECT count(*) FROM master.formula_parameter WHERE param_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.formula_parameter WHERE param_variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.formula_parameter %', record_count;
        END IF;
    
        SELECT count(*) FROM master.item_metadata WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.item_metadata WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.item_metadata %', record_count;
        END IF;
    
        SELECT count(*) FROM master.item_metadata WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.record_variable
            DELETE FROM master.record_variable WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETED RECORDS FROM master.record_variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE member_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- update master.record_variable
            UPDATE master.variable SET member_variable_id = NULL;
            RAISE NOTICE '!TOTAL UPDATED RECORDS FROM master.variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE target_variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- update master.record_variable
            UPDATE master.variable SET target_variable_id = NULL;
            RAISE NOTICE '!TOTAL UPDATED RECORDS FROM master.variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable_mapping vm WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable_mapping WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.variable_mapping %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable_set_member WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable_set_member WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.variable_set_member %', record_count;
        END IF;
    
        SELECT count(*) FROM master.record_variable WHERE variable_id = var_id INTO record_count;
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.record_variable WHERE variable_id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.record_variable %', record_count;
        END IF;
    
        SELECT count(*) FROM master.variable WHERE id = var_id INTO record_count;	
        IF record_count > 0 THEN 
            -- delete master.formula_parameter
            DELETE FROM master.variable WHERE id = var_id;
            RAISE NOTICE '!TOTAL DELETE RECORDS FROM master.record_variable %', record_count;
        END IF;
    
    END LOOP;
    
END;
$$



--rollback SELECT NULL;
