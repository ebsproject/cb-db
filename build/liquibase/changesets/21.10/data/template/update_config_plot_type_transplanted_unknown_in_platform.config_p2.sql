--liquibase formatted sql

--changeset postgres:update_config_plot_type_transplanted_unknown_in_platform.config_p2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-760 Update config PLOT_TYPE_TRANSPLANTED_UNKNOWN p2



UPDATE
    platform.config 
SET 
    config_value = 
    '
       {
            "Name": "Required experiment level protocol plot type variables",
            "Values": [
                {
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "ROWS_PER_PLOT_CONT"
                },
                {
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "HILLS_PER_ROW_CONT"
                },
                {
                "unit": "cm",
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "DIST_BET_ROWS"
                },
                {
                "unit": "cm",
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "DIST_BET_HILLS"
                },
                {
                "unit": "m",
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "PLOT_LN_1"
                },
                {
                "unit": "m",
                "default": false,
                "required": "required",
                "disabled": false,
                "variable_abbrev": "PLOT_WIDTH_RICE"
                },
                {
                "unit": "sqm",
                "default": false,
                "disabled": false,
                "required": "required",
                "variable_abbrev": "PLOT_AREA_1"
                },
                {
                "default": false,
                "disabled": false,
                "variable_abbrev": "ALLEY_LENGTH"
                },
                {
                "unit": "m",
                "default": false,
                "disabled": false,
                "allow_new_val": true,
                "variable_abbrev": "SEEDING_RATE"
                },
                {
                "default": false,
                "disabled": false,
                "variable_abbrev": "PLANTING_INSTRUCTIONS"
                }
            ]
        }
    '  
WHERE 
    abbrev ='PLOT_TYPE_TRANSPLANTED_UNKNOWN';



--rollback UPDATE
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "Name": "Required experiment level protocol plot type variables",
--rollback             "Values": [
--rollback                 {
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "ROWS_PER_PLOT_CONT"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "HILLS_PER_ROW_CONT"
--rollback                 },
--rollback                 {
--rollback                     "unit": "cm",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DIST_BET_ROWS"
--rollback                 },
--rollback                 {
--rollback                     "unit": "cm",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "DIST_BET_HILLS"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "PLOT_LN_1"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "PLOT_WIDTH_RICE"
--rollback                 },
--rollback                 {
--rollback                     "unit": "sqm",
--rollback                     "default": false,
--rollback                     "computed": "computed",
--rollback                     "disabled": true,
--rollback                     "variable_abbrev": "PLOT_AREA_1"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "ALLEY_LENGTH"
--rollback                 },
--rollback                 {
--rollback                     "unit": "m",
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "allow_new_val": true,
--rollback                     "variable_abbrev": "SEEDING_RATE"
--rollback                 },
--rollback                 {
--rollback                     "default": false,
--rollback                     "disabled": false,
--rollback                     "variable_abbrev": "PLANTING_INSTRUCTIONS"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '  
--rollback WHERE 
--rollback     abbrev ='PLOT_TYPE_TRANSPLANTED_UNKNOWN';