--liquibase formatted sql

--changeset postgres:update_scale_values_dh_to_doubled_haploid context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-582 Update DH to doubled_haploid



UPDATE 
    master.scale_value
SET
    abbrev='GERMPLASM_TYPE_DOUBLED_HAPLOID',
    value='doubled_haploid',
    description='doubled haploid',
    display_name='doubled_haploid'
WHERE
    scale_id = (
        SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_TYPE'
    )
AND
    abbrev='GERMPLASM_TYPE_DH'
;



--rollback UPDATE 
--rollback     master.scale_value
--rollback SET
--rollback     abbrev='GERMPLASM_TYPE_DH',
--rollback     value='DH',
--rollback     description='Double Haploid',
--rollback     display_name='DH'
--rollback WHERE
--rollback     scale_id = (
--rollback         SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_TYPE'
--rollback     )
--rollback AND
--rollback     abbrev='GERMPLASM_TYPE_DOUBLED_HAPLOID'
--rollback ;