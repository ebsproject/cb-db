--liquibase formatted sql

--changeset postgres:clean_up_dictionary_records context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-352 Clean up dictionary records



--remove entity
DELETE FROM
    dictionary.entity
WHERE
    abbrev 
IN
    (
        'PRODUCT',
        'ENTRY_METADATA',
        'PLOT_METADATA',
        -- 'SEED_STORAGE',
        -- 'STUDY',
        -- 'STUDY_METADATA'
        -- 'FILE_CABINET',
        'PLACE'
    )
;

--update schema id
UPDATE 
    dictionary.table
SET
    schema_id = (
        SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'
    )
WHERE
    abbrev 
IN
    (
        'PLOT',
        'ENTRY',
        'ENTRY_DATA',
        'PLOT_DATA'
    )
;

--remove table
DELETE FROM
    dictionary.table
WHERE
    abbrev 
IN
    (
        'PRODUCT',
        'ENTRY_METADATA',
        'PLOT_METADATA',
        -- 'SEED_STORAGE',
        -- 'STUDY',
        -- 'STUDY_METADATA',
        -- 'FILE_CABINET',
        'PLACE'
    )
;

--remove schema
DELETE FROM
    dictionary.schema
WHERE
    abbrev 
IN
    (
        -- 'OPERATIONAL',
        'SEED_WAREHOUSE'
    )
;



--rollback SELECT NULL;