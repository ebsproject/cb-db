--liquibase formatted sql

--changeset postgres:add_germplasm_attribute_for_dh_first_increase_completed context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Add germplasm attribute for DH_FIRST_INCREASE_COMPLETED



-- add germplasm attribute DH_FIRST_INCREASE_COMPLETED
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id,
        ge.creator_id
    FROM (
            VALUES
            ('(ABDHL0089/ABDHL120668)@195-B'),
            ('(ABDHL0089/ABDHL120668)@19-B'),
            ('(ABDHL0089/ABDHL120918)@200-B'),
            ('(ABDHL0089/ABDHL120918)@52-B'),
            ('(ABDHL0089/ABDHL120918)@63-B'),
            ('(ABDHL0089/ABLTI0136)@220-B'),
            ('ABDHL0221'),
            ('ABDHL120312'),
            ('ABDHL120918'),
            ('ABDHL163943'),
            ('ABDHL164302'),
            ('ABDHL164665'),
            ('ABDHL164672'),
            ('ABDHL165891'),
            ('(ABLTI0139/ABDHL120918)@246-B'),
            ('(ABLTI0139/ABDHL120918)@299-B'),
            ('(ABLTI0139/ABDHL120918)@373-B'),
            ('(ABLTI0139/ABDHL120918)@393-B'),
            ('(ABLTI0139/ABDHL120918)@479-B'),
            ('(ABLTI0139/ABDHL120918)@553-B'),
            ('(ABLTI0139/ABDHL120918)@653-B'),
            ('(ABLTI0139/ABDHL120918)@95-B'),
            ('(ABLTI0139/ABLTI0335)@107-B'),
            ('(ABLTI0139/ABLTI0335)@14-B'),
            ('(ABLTI0139/CML543)@140-B'),
            ('MKL154043'),
            ('MKL154050'),
            ('MKL1500041'),
            ('MKL1500099'),
            ('MKL1500186'),
            ('MKL1500215'),
            ('MKL1500261'),
            ('MKL150334'),
            ('MKL150339'),
            ('MKL150342'),
            ('MKL150390'),
            ('MKL150399'),
            ('MKL150421'),
            ('MKL150431'),
            ('MKL150694'),
            ('MKL150893'),
            ('MKL150961'),
            ('MKL150968'),
            ('MKL151030'),
            ('MKL151147'),
            ('MKL151294'),
            ('MKL151780'),
            ('MKL151787'),
            ('MKL151822'),
            ('MKL151830'),
            ('MKL151839'),
            ('MKL151841'),
            ('MKL151845'),
            ('MKL151851'),
            ('MKL151920'),
            ('MKL151967'),
            ('MKL151969'),
            ('MKL151972'),
            ('MKL151973'),
            ('MKL152043'),
            ('MKL152076'),
            ('MKL152092'),
            ('MKL152098'),
            ('MKL152140'),
            ('MKL152144'),
            ('MKL152149'),
            ('MKL152366'),
            ('MKL152395'),
            ('MKL152503'),
            ('MKL152554'),
            ('MKL152561'),
            ('MKL152563'),
            ('MKL152579'),
            ('MKL152591'),
            ('MKL152601'),
            ('MKL152616'),
            ('MKL152617'),
            ('MKL152653'),
            ('MKL152658'),
            ('MKL152682'),
            ('MKL152748'),
            ('MKL152769'),
            ('MKL152773'),
            ('MKL152777'),
            ('MKL152778'),
            ('MKL152811'),
            ('MKL152847'),
            ('MKL152857'),
            ('MKL152862'),
            ('MKL152921'),
            ('MKL152929'),
            ('MKL152976'),
            ('MKL152994'),
            ('MKL153050'),
            ('MKL153193'),
            ('MKL153222'),
            ('MKL153223'),
            ('MKL153236'),
            ('MKL153907'),
            ('MKL153908'),
            ('MKL153939'),
            ('MKL153946'),
            ('MKL153984'),
            ('MKL154091'),
            ('MKL154102')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    t.germplasm_id,
    var.id AS variable_id,
    'TRUE' AS data_value,
    'G' AS data_qc_code,
    t.creator_id
FROM
    t_germplasm AS t
    JOIN master.variable AS var
        ON var.abbrev = 'DH_FIRST_INCREASE_COMPLETED'
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id,
--rollback         ge.creator_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('(ABDHL0089/ABDHL120668)@195-B'),
--rollback             ('(ABDHL0089/ABDHL120668)@19-B'),
--rollback             ('(ABDHL0089/ABDHL120918)@200-B'),
--rollback             ('(ABDHL0089/ABDHL120918)@52-B'),
--rollback             ('(ABDHL0089/ABDHL120918)@63-B'),
--rollback             ('(ABDHL0089/ABLTI0136)@220-B'),
--rollback             ('ABDHL0221'),
--rollback             ('ABDHL120312'),
--rollback             ('ABDHL120918'),
--rollback             ('ABDHL163943'),
--rollback             ('ABDHL164302'),
--rollback             ('ABDHL164665'),
--rollback             ('ABDHL164672'),
--rollback             ('ABDHL165891'),
--rollback             ('(ABLTI0139/ABDHL120918)@246-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@299-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@373-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@393-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@479-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@553-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@653-B'),
--rollback             ('(ABLTI0139/ABDHL120918)@95-B'),
--rollback             ('(ABLTI0139/ABLTI0335)@107-B'),
--rollback             ('(ABLTI0139/ABLTI0335)@14-B'),
--rollback             ('(ABLTI0139/CML543)@140-B'),
--rollback             ('MKL154043'),
--rollback             ('MKL154050'),
--rollback             ('MKL1500041'),
--rollback             ('MKL1500099'),
--rollback             ('MKL1500186'),
--rollback             ('MKL1500215'),
--rollback             ('MKL1500261'),
--rollback             ('MKL150334'),
--rollback             ('MKL150339'),
--rollback             ('MKL150342'),
--rollback             ('MKL150390'),
--rollback             ('MKL150399'),
--rollback             ('MKL150421'),
--rollback             ('MKL150431'),
--rollback             ('MKL150694'),
--rollback             ('MKL150893'),
--rollback             ('MKL150961'),
--rollback             ('MKL150968'),
--rollback             ('MKL151030'),
--rollback             ('MKL151147'),
--rollback             ('MKL151294'),
--rollback             ('MKL151780'),
--rollback             ('MKL151787'),
--rollback             ('MKL151822'),
--rollback             ('MKL151830'),
--rollback             ('MKL151839'),
--rollback             ('MKL151841'),
--rollback             ('MKL151845'),
--rollback             ('MKL151851'),
--rollback             ('MKL151920'),
--rollback             ('MKL151967'),
--rollback             ('MKL151969'),
--rollback             ('MKL151972'),
--rollback             ('MKL151973'),
--rollback             ('MKL152043'),
--rollback             ('MKL152076'),
--rollback             ('MKL152092'),
--rollback             ('MKL152098'),
--rollback             ('MKL152140'),
--rollback             ('MKL152144'),
--rollback             ('MKL152149'),
--rollback             ('MKL152366'),
--rollback             ('MKL152395'),
--rollback             ('MKL152503'),
--rollback             ('MKL152554'),
--rollback             ('MKL152561'),
--rollback             ('MKL152563'),
--rollback             ('MKL152579'),
--rollback             ('MKL152591'),
--rollback             ('MKL152601'),
--rollback             ('MKL152616'),
--rollback             ('MKL152617'),
--rollback             ('MKL152653'),
--rollback             ('MKL152658'),
--rollback             ('MKL152682'),
--rollback             ('MKL152748'),
--rollback             ('MKL152769'),
--rollback             ('MKL152773'),
--rollback             ('MKL152777'),
--rollback             ('MKL152778'),
--rollback             ('MKL152811'),
--rollback             ('MKL152847'),
--rollback             ('MKL152857'),
--rollback             ('MKL152862'),
--rollback             ('MKL152921'),
--rollback             ('MKL152929'),
--rollback             ('MKL152976'),
--rollback             ('MKL152994'),
--rollback             ('MKL153050'),
--rollback             ('MKL153193'),
--rollback             ('MKL153222'),
--rollback             ('MKL153223'),
--rollback             ('MKL153236'),
--rollback             ('MKL153907'),
--rollback             ('MKL153908'),
--rollback             ('MKL153939'),
--rollback             ('MKL153946'),
--rollback             ('MKL153984'),
--rollback             ('MKL154091'),
--rollback             ('MKL154102')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS t
--rollback USING
--rollback     t_germplasm AS u,
--rollback     master.variable AS var
--rollback WHERE
--rollback     t.germplasm_id = u.germplasm_id
--rollback     AND t.variable_id = var.id
--rollback     AND var.abbrev = 'DH_FIRST_INCREASE_COMPLETED'
--rollback ;
