--liquibase formatted sql

--changeset postgres:fix_mismatch_season_in_experiment_and_location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-446 Fix mismatch season in experiment and location



WITH t1 AS (
    SELECT
        ee.id experiment_id,
        elc.id location_id,
        ee.season_id experiment_season_id,
        elc.season_id location_season_id
    FROM 
        experiment.occurrence eo
    LEFT  JOIN
        experiment.experiment ee
    ON
        eo.experiment_id = ee.id
    LEFT  JOIN
        experiment.location_occurrence_group el
    ON
        el.occurrence_id = eo.id
    LEFT  JOIN
        experiment.location elc
    ON
        el.location_id = elc.id
    WHERE
        ee.season_id <> elc.season_id
    ORDER BY
        ee.id
)

UPDATE experiment.location el SET season_id = t1.experiment_season_id FROM t1 WHERE el.id = t1.location_id;



--rollback UPDATE 
--rollback     experiment.location el
--rollback SET
--rollback     season_id = 15
--rollback WHERE
--rollback     id 
--rollback IN
--rollback     (
--rollback         3384,
--rollback         3385,
--rollback         3386,
--rollback         3387,
--rollback         3389,
--rollback         3390
--rollback     )
--rollback ;