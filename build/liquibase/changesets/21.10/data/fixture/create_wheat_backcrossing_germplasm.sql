--liquibase formatted sql

--changeset postgres:create_wheat_backcrossing_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing germplasm



--# create wheat backcrossing germplasm
INSERT INTO
    germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        germplasm_type,
        crop_id,
        creator_id,
        taxonomy_id,
        germplasm_normalized_name
    )
SELECT
    t.designation,
    t.parentage,
    t.generation,
    t.germplasm_state,
    t.germplasm_name_type,
    t.germplasm_type,
    crop.id AS crop_id,
    crtr.id AS creator_id,
    taxon.id AS taxonomy_id,
    platform.normalize_text(t.designation) AS germplasm_normalized_name
FROM (
        VALUES
        ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'ESTOC', 'F7', 'fixed', 'selection_history', 'inbred_line', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMSS16Y00012SF', 'ESTOC*2/3/FRANCOLIN #1/CHONTE//FRNCLN', 'BC1F1', 'not_fixed', 'bcid', 'backcross', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMWW16Y04412SS', 'FRZN/WINTERWONDER/5/KZAM/FROSTY//ELSA2/3/ANNA/4/OLAF', 'F1', 'not_fixed', 'bcid', 'F1F', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'FRZN/WINTERWONDER', 'F6', 'fixed', 'selection_history', 'inbred_line', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMWW17B04212SF', 'FRZN/WINTERWONDER*2/5/KZAM/FROSTY//ELSA2/3/ANNA/4/OLAF', 'BC1F1', 'not_fixed', 'bcid', 'backcross', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMWW17Y06812SF', 'FRZN/WINTERWONDER*2/5/KZAM/FROSTY//ELSA2/3/ANNA/4/OLAF', 'BC2F1', 'not_fixed', 'bcid', 'backcross', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'CROCUS/DAFODIL//TLIP/3/LILAC', 'F8', 'fixed', 'selection_history', 'inbred_line', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CMWS14B00122S', 'FRZN/WINTERWONDER/4/CROCUS/DAFODIL//TLIP/3/LILAC', 'F1', 'not_fixed', 'bcid', 'F1F', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CM14Y06122M', 'FRZN/WINTERWONDER/4/2*CROCUS/DAFODIL//TLIP/3/LILAC', 'BC1F1', 'not_fixed', 'bcid', 'backcross', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum'),
        ('CM15Y00828M', 'FRZN/WINTERWONDER/4/3*CROCUS/DAFODIL//TLIP/3/LILAC', 'BC2F1', 'not_fixed', 'bcid', 'backcross', 'WHEAT', 'admin', 'Triticum aestivum L. subsp. aestivum')
    ) AS t (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        germplasm_type,
        crop,
        creator,
        taxonomy
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop
    JOIN germplasm.taxonomy AS taxon
        ON taxon.taxonomy_name = t.taxonomy
    JOIN tenant.person AS crtr
        ON crtr.username = t.creator
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm
--rollback WHERE
--rollback     designation IN (
--rollback         'CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M',
--rollback         'CMSS16Y00012SF',
--rollback         'CMWW16Y04412SS',
--rollback         'CMWW11M00254S-099M-099NJ-6WGY-0M',
--rollback         'CMWW17B04212SF',
--rollback         'CMWW17Y06812SF',
--rollback         'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M',
--rollback         'CMWS14B00122S',
--rollback         'CM14Y06122M',
--rollback         'CM15Y00828M'
--rollback     )
--rollback ;



--changeset postgres:create_wheat_backcrossing_germplasm_names context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing germplasm names



--# create wheat backcrossing germplasm names
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    t.name_value,
    t.germplasm_name_type,
    t.germplasm_name_status,
    platform.normalize_text(t.name_value) AS germplasm_normalized_name,
    ge.creator_id
FROM (
        VALUES
        ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'selection_history', 'standard'),
        ('CMSS16Y00012SF', 'bcid', 'standard'),
        ('CMWW16Y04412SS', 'bcid', 'standard'),
        ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'selection_history', 'standard'),
        ('CMWW17B04212SF', 'bcid', 'standard'),
        ('CMWW17Y06812SF', 'bcid', 'standard'),
        ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'selection_history', 'standard'),
        ('CMWS14B00122S', 'bcid', 'standard'),
        ('CM14Y06122M', 'bcid', 'standard'),
        ('CM15Y00828M', 'bcid', 'standard')
    ) AS t (
        name_value,
        germplasm_name_type,
        germplasm_name_status
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.name_value
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING (
--rollback         VALUES
--rollback         ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M'),
--rollback         ('CMSS16Y00012SF'),
--rollback         ('CMWW16Y04412SS'),
--rollback         ('CMWW11M00254S-099M-099NJ-6WGY-0M'),
--rollback         ('CMWW17B04212SF'),
--rollback         ('CMWW17Y06812SF'),
--rollback         ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M'),
--rollback         ('CMWS14B00122S'),
--rollback         ('CM14Y06122M'),
--rollback         ('CM15Y00828M')
--rollback     ) AS t (
--rollback         designation
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback WHERE
--rollback     gename.germplasm_id = ge.id
--rollback ;



--changeset postgres:create_wheat_backcrossing_germplasm_attributes context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing germplasm attributes



-- add germplasm attributes
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id,
        ge.creator_id
    FROM (
            VALUES
            ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'CROSS_NUMBER', '0'),
            ('CMSS16Y00012SF', 'CROSS_NUMBER', '3'),
            ('CMWW16Y04412SS', 'CROSS_NUMBER', '5'),
            ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'CROSS_NUMBER', '1'),
            ('CMWW17B04212SF', 'CROSS_NUMBER', '5'),
            ('CMWW17Y06812SF', 'CROSS_NUMBER', '5'),
            ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'CROSS_NUMBER', '2'),
            ('CMWS14B00122S', 'CROSS_NUMBER', '4'),
            ('CM14Y06122M', 'CROSS_NUMBER', '4'),
            ('CM15Y00828M', 'CROSS_NUMBER', '4'),
            ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'GROWTH_HABIT', 'S'),
            ('CMSS16Y00012SF', 'GROWTH_HABIT', 'S'),
            ('CMWW16Y04412SS', 'GROWTH_HABIT', 'W'),
            ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'GROWTH_HABIT', 'W'),
            ('CMWW17B04212SF', 'GROWTH_HABIT', 'W'),
            ('CMWW17Y06812SF', 'GROWTH_HABIT', 'W'),
            ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'GROWTH_HABIT', 'S')
        ) AS t (
            designation,
            variable,
            data_value
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    t.germplasm_id,
    var.id AS variable_id,
    t.data_value AS data_value,
    'G' AS data_qc_code,
    t.creator_id
FROM
    t_germplasm AS t
    JOIN master.variable AS var
        ON var.abbrev = t.variable
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id,
--rollback         ge.creator_id,
--rollback         var.id AS variable_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'CROSS_NUMBER'),
--rollback             ('CMSS16Y00012SF', 'CROSS_NUMBER'),
--rollback             ('CMWW16Y04412SS', 'CROSS_NUMBER'),
--rollback             ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'CROSS_NUMBER'),
--rollback             ('CMWW17B04212SF', 'CROSS_NUMBER'),
--rollback             ('CMWW17Y06812SF', 'CROSS_NUMBER'),
--rollback             ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'CROSS_NUMBER'),
--rollback             ('CMWS14B00122S', 'CROSS_NUMBER'),
--rollback             ('CM14Y06122M', 'CROSS_NUMBER'),
--rollback             ('CM15Y00828M', 'CROSS_NUMBER'),
--rollback             ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'GROWTH_HABIT'),
--rollback             ('CMSS16Y00012SF', 'GROWTH_HABIT'),
--rollback             ('CMWW16Y04412SS', 'GROWTH_HABIT'),
--rollback             ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'GROWTH_HABIT'),
--rollback             ('CMWW17B04212SF', 'GROWTH_HABIT'),
--rollback             ('CMWW17Y06812SF', 'GROWTH_HABIT'),
--rollback             ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'GROWTH_HABIT')
--rollback         ) AS t (
--rollback             designation,
--rollback             variable
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback         JOIN master.variable AS var
--rollback             ON var.abbrev = t.variable
--rollback )
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS t
--rollback USING
--rollback     t_germplasm AS u
--rollback WHERE
--rollback     t.germplasm_id = u.germplasm_id
--rollback     AND t.variable_id = u.variable_id
--rollback ;



--changeset postgres:create_wheat_backcrossing_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing seeds



--# create wheat backcrossing seeds
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    t.seed_name AS seed_name,
    NULL::date AS harvest_date,
    t.harvest_method AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    'plot' AS harvest_source,
    ge.creator_id
FROM
    (
        VALUES
        ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18', 'Bulk', 'BW'),
        ('CMSS16Y00012SF', 'Y16B-CBBW-2/14', 'Bulk', 'BW'),
        ('CMWW16Y04412SS', 'Y16B-CBBW-8/20', 'Bulk', 'BW'),
        ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102', 'Bulk', 'BW'),
        ('CMWW17B04212SF', 'B17B-CBBW-10/20', 'Bulk', 'BW'),
        ('CMWW17Y06812SF', 'Y18B-BC1N-8/12', 'Bulk', 'BW'),
        ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20', 'Bulk', 'BW'),
        ('CMWS14B00122S', 'B14A-CBBW-6/4', 'Bulk', 'BW'),
        ('CM14Y06122M', 'Y14B-BC1SW-12/18', 'Bulk', 'BW'),
        ('CM15Y00828M', 'Y15B-BC2BC-100/4', 'Bulk', 'BW')
    ) AS t (
        designation,
        seed_name,
        harvest_method,
        program
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN tenant.program AS prog
        ON prog.program_code = t.program
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.seed AS sd
--rollback USING (
--rollback         VALUES
--rollback         ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18'),
--rollback         ('CMSS16Y00012SF', 'Y16B-CBBW-2/14'),
--rollback         ('CMWW16Y04412SS', 'Y16B-CBBW-8/20'),
--rollback         ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102'),
--rollback         ('CMWW17B04212SF', 'B17B-CBBW-10/20'),
--rollback         ('CMWW17Y06812SF', 'Y18B-BC1N-8/12'),
--rollback         ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20'),
--rollback         ('CMWS14B00122S', 'B14A-CBBW-6/4'),
--rollback         ('CM14Y06122M', 'Y14B-BC1SW-12/18'),
--rollback         ('CM15Y00828M', 'Y15B-BC2BC-100/4')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback WHERE
--rollback     sd.germplasm_id = ge.id
--rollback     AND sd.seed_name = t.seed_name
--rollback ;



--changeset postgres:create_wheat_backcrossing_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing packages



--# create wheat backcrossing packages
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    t.package_label,
    t.package_quantity::float,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    ge.creator_id AS creator_id
FROM (
        VALUES
        ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18', 'BW11-038-0886', '200', 'g', 'active', 'BW'),
        ('CMSS16Y00012SF', 'Y16B-CBBW-2/14', 'BW16-044-0026', '232', 'seeds', 'active', 'BW'),
        ('CMWW16Y04412SS', 'Y16B-CBBW-8/20', 'BW16-044-0027', '58', 'seeds', 'active', 'BW'),
        ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102', 'BW11-002-0102', '120', 'g', 'active', 'BW'),
        ('CMWW17B04212SF', 'B17B-CBBW-10/20', 'BW17-016-0044', '148', 'g', 'active', 'BW'),
        ('CMWW17Y06812SF', 'Y18B-BC1N-8/12', 'BW18-100-0028', '124', 'g', 'active', 'BW'),
        ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20', 'BW12-192-0002', '400', 'g', 'active', 'BW'),
        ('CMWS14B00122S', 'B14A-CBBW-6/4', 'BW14-180-0012', '200', 'seeds', 'active', 'BW'),
        ('CM14Y06122M', 'Y14B-BC1SW-12/18', 'BW14-188-0012', '288', 'seeds', 'active', 'BW'),
        ('CM15Y00828M', 'Y15B-BC2BC-100/4', 'BW15-002-0016', '236', 'seeds', 'active', 'BW')
    ) AS t (
        designation,
        seed_name,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        program
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN germplasm.seed AS sd
        ON sd.seed_name = t.seed_name
    JOIN tenant.program AS prog
        ON prog.program_code = t.program
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     (
--rollback         VALUES
--rollback         ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18', 'BW11-038-0886'),
--rollback         ('CMSS16Y00012SF', 'Y16B-CBBW-2/14', 'BW16-044-0026'),
--rollback         ('CMWW16Y04412SS', 'Y16B-CBBW-8/20', 'BW16-044-0027'),
--rollback         ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102', 'BW11-002-0102'),
--rollback         ('CMWW17B04212SF', 'B17B-CBBW-10/20', 'BW17-016-0044'),
--rollback         ('CMWW17Y06812SF', 'Y18B-BC1N-8/12', 'BW18-100-0028'),
--rollback         ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20', 'BW12-192-0002'),
--rollback         ('CMWS14B00122S', 'B14A-CBBW-6/4', 'BW14-180-0012'),
--rollback         ('CM14Y06122M', 'Y14B-BC1SW-12/18', 'BW14-188-0012'),
--rollback         ('CM15Y00828M', 'Y15B-BC2BC-100/4', 'BW15-002-0016')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name,
--rollback         package_label
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.seed_name = t.seed_name
--rollback         AND sd.germplasm_id = ge.id
--rollback WHERE
--rollback     pkg.seed_id = sd.id
--rollback     AND pkg.package_label = t.package_label
--rollback ;



--changeset postgres:create_wheat_backcrossing_package_logs context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing package logs



--# create wheat backcrossing package logs
INSERT INTO
    germplasm.package_log (
        package_id,
        package_quantity,
        package_unit,
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
SELECT
    pkg.id AS package_id,
    pkg.package_quantity,
    pkg.package_unit,
    t.package_transaction_type,
    enty.id AS entity_id,
    t.data_id::integer AS data_id,
    pkg.creator_id
FROM (
        VALUES
        ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18', 'BW11-038-0886', 'deposit', 'PLOT', NULL),
        ('CMSS16Y00012SF', 'Y16B-CBBW-2/14', 'BW16-044-0026', 'deposit', 'PLOT', NULL),
        ('CMWW16Y04412SS', 'Y16B-CBBW-8/20', 'BW16-044-0027', 'deposit', 'PLOT', NULL),
        ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102', 'BW11-002-0102', 'deposit', 'PLOT', NULL),
        ('CMWW17B04212SF', 'B17B-CBBW-10/20', 'BW17-016-0044', 'deposit', 'PLOT', NULL),
        ('CMWW17Y06812SF', 'Y18B-BC1N-8/12', 'BW18-100-0028', 'deposit', 'PLOT', NULL),
        ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20', 'BW12-192-0002', 'deposit', 'PLOT', NULL),
        ('CMWS14B00122S', 'B14A-CBBW-6/4', 'BW14-180-0012', 'deposit', 'PLOT', NULL),
        ('CM14Y06122M', 'Y14B-BC1SW-12/18', 'BW14-188-0012', 'deposit', 'PLOT', NULL),
        ('CM15Y00828M', 'Y15B-BC2BC-100/4', 'BW15-002-0016', 'deposit', 'PLOT', NULL)
    ) AS t (
        designation,
        seed_name,
        package_label,
        package_transaction_type,
        entity_id,
        data_id
    )
    JOIN dictionary.entity AS enty
        ON enty.abbrev = t.entity_id
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN germplasm.seed AS sd
        ON sd.seed_name = t.seed_name
    JOIN germplasm.package AS pkg
        ON pkg.package_label = t.package_label
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         VALUES
--rollback         ('CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M', 'M11A-F5INC-18', 'BW11-038-0886'),
--rollback         ('CMSS16Y00012SF', 'Y16B-CBBW-2/14', 'BW16-044-0026'),
--rollback         ('CMWW16Y04412SS', 'Y16B-CBBW-8/20', 'BW16-044-0027'),
--rollback         ('CMWW11M00254S-099M-099NJ-6WGY-0M', 'M11A-F4INC-102', 'BW11-002-0102'),
--rollback         ('CMWW17B04212SF', 'B17B-CBBW-10/20', 'BW17-016-0044'),
--rollback         ('CMWW17Y06812SF', 'Y18B-BC1N-8/12', 'BW18-100-0028'),
--rollback         ('CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M', 'M12A-CBBW-18/20', 'BW12-192-0002'),
--rollback         ('CMWS14B00122S', 'B14A-CBBW-6/4', 'BW14-180-0012'),
--rollback         ('CM14Y06122M', 'Y14B-BC1SW-12/18', 'BW14-188-0012'),
--rollback         ('CM15Y00828M', 'Y15B-BC2BC-100/4', 'BW15-002-0016')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name,
--rollback         package_label
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback         AND sd.seed_name = t.seed_name
--rollback     JOIN germplasm.package AS pkg
--rollback         ON pkg.seed_id = sd.id
--rollback         AND pkg.package_label = t.package_label
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback ;



--changeset postgres:create_wheat_backcrossing_germplasm_relations context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-738 Create wheat backcrossing germplasm relations



-- create wheat backcrossing germplasm relations
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id,
        ge.creator_id,
        ge1.id AS parent_germplasm_id_1,
        ge2.id AS parent_germplasm_id_2
    FROM (
            VALUES
            ('CMSS16Y00012SF', 'CMSS14Y00771S', 'CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M'),
            ('CMWW17B04212SF', 'CMWW16Y04412SS', 'CMWW11M00254S-099M-099NJ-6WGY-0M'),
            ('CMWW17Y06812SF', 'CMWW17Y06812SF', 'CMWW11M00254S-099M-099NJ-6WGY-0M'),
            ('CMWS14B00122S', 'CMWW11M00254S-099M-099NJ-6WGY-0M', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M'),
            ('CM14Y06122M', 'CMWS14B00122S', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M'),
            ('CM15Y00828M', 'CM14Y06122M', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M')
        ) AS t (
            designation,
            parent_designation_1,
            parent_designation_2
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
        JOIN germplasm.germplasm AS ge1
            ON ge1.designation = t.parent_designation_1
        JOIN germplasm.germplasm AS ge2
            ON ge2.designation = t.parent_designation_2
)
INSERT INTO
    germplasm.germplasm_relation (
        parent_germplasm_id,
        child_germplasm_id,
        order_number,
        creator_id
    )
SELECT
    t.parent_germplasm_id_1 AS parent_germplasm_id,
    t.germplasm_id AS child_germplasm_id,
    1 AS order_number,
    t.creator_id
FROM
    t_germplasm AS t
UNION ALL
    SELECT
        t.parent_germplasm_id_2 AS parent_germplasm_id,
        t.germplasm_id AS child_germplasm_id,
        2 AS order_number,
        t.creator_id
    FROM
        t_germplasm AS t
;



-- revert change
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id,
--rollback         ge.creator_id,
--rollback         ge1.id AS parent_germplasm_id_1,
--rollback         ge2.id AS parent_germplasm_id_2
--rollback     FROM (
--rollback             VALUES
--rollback             ('CMSS16Y00012SF', 'CMSS14Y00771S', 'CMSS11M00344S-099M-099NJ-099NJ-3WGY-0M'),
--rollback             ('CMWW17B04212SF', 'CMWW16Y04412SS', 'CMWW11M00254S-099M-099NJ-6WGY-0M'),
--rollback             ('CMWW17Y06812SF', 'CMWW17Y06812SF', 'CMWW11M00254S-099M-099NJ-6WGY-0M'),
--rollback             ('CMWS14B00122S', 'CMWW11M00254S-099M-099NJ-6WGY-0M', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M'),
--rollback             ('CM14Y06122M', 'CMWS14B00122S', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M'),
--rollback             ('CM15Y00828M', 'CM14Y06122M', 'CMSS12M00444S-099M-099NJ-099NJ--040M-3WGY-0M')
--rollback         ) AS t (
--rollback             designation,
--rollback             parent_designation_1,
--rollback             parent_designation_2
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback         JOIN germplasm.germplasm AS ge1
--rollback             ON ge1.designation = t.parent_designation_1
--rollback         JOIN germplasm.germplasm AS ge2
--rollback             ON ge2.designation = t.parent_designation_2
--rollback )
--rollback DELETE FROM
--rollback     germplasm.germplasm_relation AS u
--rollback USING (
--rollback         SELECT
--rollback             t.parent_germplasm_id_1 AS parent_germplasm_id,
--rollback             t.germplasm_id AS child_germplasm_id,
--rollback             1 AS order_number
--rollback         FROM
--rollback             t_germplasm AS t
--rollback         UNION ALL
--rollback             SELECT
--rollback                 t.parent_germplasm_id_2 AS parent_germplasm_id,
--rollback                 t.germplasm_id AS child_germplasm_id,
--rollback                 2 AS order_number
--rollback             FROM
--rollback                 t_germplasm AS t
--rollback     ) AS t
--rollback WHERE
--rollback     t.parent_germplasm_id = u.parent_germplasm_id
--rollback     AND t.child_germplasm_id = u.child_germplasm_id
--rollback     AND t.order_number = u.order_number
--rollback ;
