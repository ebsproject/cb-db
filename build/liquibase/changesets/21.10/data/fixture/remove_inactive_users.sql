--liquibase formatted sql

--changeset postgres:remove_inactive_users context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-719 Remove inactive users



WITH t1 AS (
    SELECT 
        id
    FROM 
        tenant.person tp
    WHERE
        username 
    IN 
        (
            'j.antonio',
            'j.taguines',
            'n.carumba',
            's.gao',
            't.hagen',
            'v.calaminos'
        )
), t2 AS (
    DELETE FROM
        tenant.team_member tm 
    WHERE 
        tm.person_id 
    IN
        (
            SELECT id FROM t1 
        )
    RETURNING 'Users have been removed from team member table'
),
t3 AS (
    DELETE FROM
        tenant.person tp
    WHERE
        tp.id 
    IN 
        (
            SELECT id FROM t1 
        )
    RETURNING 'Users have been removed from person table'
)
SELECT t.* FROM t2, t3 t;



--rollback INSERT INTO 
--rollback 		tenant.person 
--rollback 			(username, first_name, last_name, person_name, person_type, email, person_role_id, person_status, creator_id)
--rollback 	VALUES 
--rollback 		('j.antonio', 'Joanie', 'Antonio', 'Antonio, Joanie', 'admin', 'j.antonio@irri.org', '1', 'active', '1'),
--rollback 		('j.taguines', 'Jay', 'Taguines', 'Taguines, Jay', 'user', 'j.taguines@irri.org', '16', 'active', '1'),
--rollback 		('n.carumba', 'Nikki', 'Carumba', 'Carumba, Nikki', 'admin', 'n.carumba@irri.org', '1', 'active', '1'),
--rollback 		('s.gao', 'Star', 'Gao', 'Gao, Star', 'user', 's.gao@cimmyt.org', '18', 'active', '1'),
--rollback 		('t.hagen', 'Tom', 'Hagen', 'Hagen, Tom', 'admin', 't.hagen@cimmyt.org', '18', 'active', '1'),
--rollback 		('v.calaminos', 'Viana Carla', 'Calaminos', 'Calaminos, Viana Carla', 'admin', 'v.calaminos@irri.org', '1', 'active', '1');
--rollback 
--rollback INSERT INTO 
--rollback 		tenant.team_member 
--rollback 			(team_id,person_id,person_role_id,order_number,creator_id)
--rollback     VALUES 
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'IRSEA'), (SELECT id FROM tenant.person WHERE username='j.antonio'), 1,48 , 1),
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'IRSEA'), (SELECT id FROM tenant.person WHERE username='j.taguines'), 1, 159, 1),
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'IRSEA'), (SELECT id FROM tenant.person WHERE username='n.carumba'), 1, 55, 1),
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'BW_TEAM'), (SELECT id FROM tenant.person WHERE username='s.gao'), 1, 103, 1),
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'BW_TEAM'), (SELECT id FROM tenant.person WHERE username='t.hagen'), 1, 113, 1),
--rollback     	((SELECT id FROM tenant.team WHERE team_code = 'IRSEA'), (SELECT id FROM tenant.person WHERE username='v.calaminos'), 1, 59, 1);