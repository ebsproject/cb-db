--liquibase formatted sql

--changeset postgres:populate_program_id_of_planting_job_records context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-727 Update program_id of planting_job records



-- update program_id column
WITH t_pjobs AS (
    SELECT
        DISTINCT
        pjocc.planting_job_id,
        expt.program_id
    FROM
        experiment.planting_job_occurrence AS pjocc
        JOIN experiment.occurrence AS occ
            ON occ.id = pjocc.occurrence_id
        JOIN experiment.experiment AS expt
            ON expt.id = occ.experiment_id
)
UPDATE
    experiment.planting_job AS pjob
SET
    program_id = t.program_id
FROM
    t_pjobs AS t
WHERE
    pjob.id = t.planting_job_id
    AND pjob.is_void = FALSE
;



-- revert changes
--rollback UPDATE
--rollback     experiment.planting_job
--rollback SET
--rollback     program_id = NULL
--rollback WHERE
--rollback     is_void = FALSE
--rollback ;
