--liquibase formatted sql

--changeset postgres:void_maize_dh_entries context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH entries



-- void entries
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    experiment.entry AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.germplasm_id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     experiment.entry AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.germplasm_id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_planting_instructions context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH planting instructions



-- void planting instructions
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    experiment.planting_instruction AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.germplasm_id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     experiment.planting_instruction AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.germplasm_id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_cross_parents context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH cross parents



-- void cross parents
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    germplasm.cross_parent AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.germplasm_id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     germplasm.cross_parent AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.germplasm_id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_germplasm_names context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH germplasm names



-- void germplasm names
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    germplasm.germplasm_name AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.germplasm_id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     germplasm.germplasm_name AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.germplasm_id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH packages



-- void packages
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    germplasm.package AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
    JOIN germplasm.seed AS v
        ON v.germplasm_id = t.germplasm_id
WHERE
    u.seed_id = v.id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     germplasm.package AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback     JOIN germplasm.seed AS v
--rollback         ON v.germplasm_id = t.germplasm_id
--rollback WHERE
--rollback     u.seed_id = v.id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH seeds



-- void seeds
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    germplasm.seed AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.germplasm_id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     germplasm.seed AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.germplasm_id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;



--changeset postgres:void_maize_dh_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Void maize DH germplasm



-- void germplasm
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id
    FROM (
            VALUES
            ('CLRCY034'),
            ('CLYN261'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464'),
            ('((CML442/KS23-6)-B)@103-B'),
            ('((CML537/KS523-5)-B)@10-B'),
            ('CML204'),
            ('CML442'),
            ('CML444'),
            ('CML494'),
            ('CML536'),
            ('CML539'),
            ('CML543'),
            ('CML463'),
            ('CML495'),
            ('CML547'),
            ('CML464')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
UPDATE
    germplasm.germplasm AS u
SET
    is_void = TRUE
FROM
    t_germplasm AS t
WHERE
    u.id = t.germplasm_id
    AND u.is_void = FALSE
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('CLRCY034'),
--rollback             ('CLYN261'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464'),
--rollback             ('((CML442/KS23-6)-B)@103-B'),
--rollback             ('((CML537/KS523-5)-B)@10-B'),
--rollback             ('CML204'),
--rollback             ('CML442'),
--rollback             ('CML444'),
--rollback             ('CML494'),
--rollback             ('CML536'),
--rollback             ('CML539'),
--rollback             ('CML543'),
--rollback             ('CML463'),
--rollback             ('CML495'),
--rollback             ('CML547'),
--rollback             ('CML464')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback UPDATE
--rollback     germplasm.germplasm AS u
--rollback SET
--rollback     is_void = FALSE
--rollback FROM
--rollback     t_germplasm AS t
--rollback WHERE
--rollback     u.id = t.germplasm_id
--rollback     AND u.is_void = TRUE
--rollback ;
