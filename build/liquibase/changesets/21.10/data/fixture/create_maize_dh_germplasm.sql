--liquibase formatted sql

--changeset postgres:create_maize_dh_germplasm context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH germplasm



--# create maize DH germplasm
INSERT INTO
    germplasm.germplasm (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        germplasm_type,
        crop_id,
        creator_id,
        taxonomy_id,
        germplasm_normalized_name
    )
SELECT
    t.designation,
    t.parentage,
    t.generation,
    t.germplasm_state,
    t.germplasm_name_type,
    t.germplasm_type,
    crop.id AS crop_id,
    crtr.id AS creator_id,
    taxon.id AS taxonomy_id,
    platform.normalize_text(t.designation) AS germplasm_normalized_name
FROM (
        VALUES
        ('(FHWL42/FHWL120668)@14', 'FHWL42/FHWL120668', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL120668)@16', 'FHWL42/FHWL120668', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL12618)@200', 'FHWL42/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL12618)@50', 'FHWL42/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL12618)@62', 'FHWL42/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL12618)@63', 'FHWL42/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(FHWL42/FHWL12618)@64', 'FHWL42/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@96', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@97', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@98', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@99', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@100', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@101', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@102', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@103', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(ABLTI0139/FHWL12618)@104', 'ABLTI0139/FHWL12618', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@22', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@23', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@24', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@25', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@26', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@27', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@28', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@29', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@30', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@31', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(BBYI0139/ABLTI0338)@32', 'BBYI0139/ABLTI0338', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@2', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@3', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@4', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@5', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@6', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@7', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@8', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@9', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@10', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@11', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@12', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@13', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@14', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@15', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays'),
        ('(YAL56/YBL400)-B@16', 'YAL56/YBL400', 'DH', 'fixed', 'pedigree', 'doubled_haploid', 'MAIZE', 'admin', 'Zea mays L. subsp. mays')
    ) AS t (
        designation,
        parentage,
        generation,
        germplasm_state,
        germplasm_name_type,
        germplasm_type,
        crop,
        creator,
        taxonomy
    )
    JOIN tenant.crop AS crop
        ON crop.crop_code = t.crop
    JOIN germplasm.taxonomy AS taxon
        ON taxon.taxonomy_name = t.taxonomy
    JOIN tenant.person AS crtr
        ON crtr.username = t.creator
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm
--rollback WHERE
--rollback     designation IN (
--rollback         '(FHWL42/FHWL120668)@14',
--rollback         '(FHWL42/FHWL120668)@16',
--rollback         '(FHWL42/FHWL12618)@200',
--rollback         '(FHWL42/FHWL12618)@50',
--rollback         '(FHWL42/FHWL12618)@62',
--rollback         '(FHWL42/FHWL12618)@63',
--rollback         '(FHWL42/FHWL12618)@64',
--rollback         '(ABLTI0139/FHWL12618)@96',
--rollback         '(ABLTI0139/FHWL12618)@97',
--rollback         '(ABLTI0139/FHWL12618)@98',
--rollback         '(ABLTI0139/FHWL12618)@99',
--rollback         '(ABLTI0139/FHWL12618)@100',
--rollback         '(ABLTI0139/FHWL12618)@101',
--rollback         '(ABLTI0139/FHWL12618)@102',
--rollback         '(ABLTI0139/FHWL12618)@103',
--rollback         '(ABLTI0139/FHWL12618)@104',
--rollback         '(BBYI0139/ABLTI0338)@22',
--rollback         '(BBYI0139/ABLTI0338)@23',
--rollback         '(BBYI0139/ABLTI0338)@24',
--rollback         '(BBYI0139/ABLTI0338)@25',
--rollback         '(BBYI0139/ABLTI0338)@26',
--rollback         '(BBYI0139/ABLTI0338)@27',
--rollback         '(BBYI0139/ABLTI0338)@28',
--rollback         '(BBYI0139/ABLTI0338)@29',
--rollback         '(BBYI0139/ABLTI0338)@30',
--rollback         '(BBYI0139/ABLTI0338)@31',
--rollback         '(BBYI0139/ABLTI0338)@32',
--rollback         '(YAL56/YBL400)-B@2',
--rollback         '(YAL56/YBL400)-B@3',
--rollback         '(YAL56/YBL400)-B@4',
--rollback         '(YAL56/YBL400)-B@5',
--rollback         '(YAL56/YBL400)-B@6',
--rollback         '(YAL56/YBL400)-B@7',
--rollback         '(YAL56/YBL400)-B@8',
--rollback         '(YAL56/YBL400)-B@9',
--rollback         '(YAL56/YBL400)-B@10',
--rollback         '(YAL56/YBL400)-B@11',
--rollback         '(YAL56/YBL400)-B@12',
--rollback         '(YAL56/YBL400)-B@13',
--rollback         '(YAL56/YBL400)-B@14',
--rollback         '(YAL56/YBL400)-B@15',
--rollback         '(YAL56/YBL400)-B@16'
--rollback     )
--rollback ;



--changeset postgres:create_maize_dh_germplasm_names context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH germplasm names



--# create maize DH germplasm names
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,
        name_value,
        germplasm_name_type,
        germplasm_name_status,
        germplasm_normalized_name,
        creator_id
    )
SELECT
    ge.id AS germplasm_id,
    t.name_value,
    t.germplasm_name_type,
    t.germplasm_name_status,
    platform.normalize_text(t.name_value) AS germplasm_normalized_name,
    ge.creator_id
FROM (
        VALUES
        ('(FHWL42/FHWL120668)@14', 'pedigree', 'standard'),
        ('(FHWL42/FHWL120668)@16', 'pedigree', 'standard'),
        ('(FHWL42/FHWL12618)@200', 'pedigree', 'standard'),
        ('(FHWL42/FHWL12618)@50', 'pedigree', 'standard'),
        ('(FHWL42/FHWL12618)@62', 'pedigree', 'standard'),
        ('(FHWL42/FHWL12618)@63', 'pedigree', 'standard'),
        ('(FHWL42/FHWL12618)@64', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@96', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@97', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@98', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@99', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@100', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@101', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@102', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@103', 'pedigree', 'standard'),
        ('(ABLTI0139/FHWL12618)@104', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@22', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@23', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@24', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@25', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@26', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@27', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@28', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@29', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@30', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@31', 'pedigree', 'standard'),
        ('(BBYI0139/ABLTI0338)@32', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@2', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@3', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@4', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@5', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@6', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@7', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@8', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@9', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@10', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@11', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@12', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@13', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@14', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@15', 'pedigree', 'standard'),
        ('(YAL56/YBL400)-B@16', 'pedigree', 'standard')
    ) AS t (
        name_value,
        germplasm_name_type,
        germplasm_name_status
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.name_value
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING (
--rollback         VALUES
--rollback         ('(FHWL42/FHWL120668)@14'),
--rollback         ('(FHWL42/FHWL120668)@16'),
--rollback         ('(FHWL42/FHWL12618)@200'),
--rollback         ('(FHWL42/FHWL12618)@50'),
--rollback         ('(FHWL42/FHWL12618)@62'),
--rollback         ('(FHWL42/FHWL12618)@63'),
--rollback         ('(FHWL42/FHWL12618)@64'),
--rollback         ('(ABLTI0139/FHWL12618)@96'),
--rollback         ('(ABLTI0139/FHWL12618)@97'),
--rollback         ('(ABLTI0139/FHWL12618)@98'),
--rollback         ('(ABLTI0139/FHWL12618)@99'),
--rollback         ('(ABLTI0139/FHWL12618)@100'),
--rollback         ('(ABLTI0139/FHWL12618)@101'),
--rollback         ('(ABLTI0139/FHWL12618)@102'),
--rollback         ('(ABLTI0139/FHWL12618)@103'),
--rollback         ('(ABLTI0139/FHWL12618)@104'),
--rollback         ('(BBYI0139/ABLTI0338)@22'),
--rollback         ('(BBYI0139/ABLTI0338)@23'),
--rollback         ('(BBYI0139/ABLTI0338)@24'),
--rollback         ('(BBYI0139/ABLTI0338)@25'),
--rollback         ('(BBYI0139/ABLTI0338)@26'),
--rollback         ('(BBYI0139/ABLTI0338)@27'),
--rollback         ('(BBYI0139/ABLTI0338)@28'),
--rollback         ('(BBYI0139/ABLTI0338)@29'),
--rollback         ('(BBYI0139/ABLTI0338)@30'),
--rollback         ('(BBYI0139/ABLTI0338)@31'),
--rollback         ('(BBYI0139/ABLTI0338)@32'),
--rollback         ('(YAL56/YBL400)-B@2'),
--rollback         ('(YAL56/YBL400)-B@3'),
--rollback         ('(YAL56/YBL400)-B@4'),
--rollback         ('(YAL56/YBL400)-B@5'),
--rollback         ('(YAL56/YBL400)-B@6'),
--rollback         ('(YAL56/YBL400)-B@7'),
--rollback         ('(YAL56/YBL400)-B@8'),
--rollback         ('(YAL56/YBL400)-B@9'),
--rollback         ('(YAL56/YBL400)-B@10'),
--rollback         ('(YAL56/YBL400)-B@11'),
--rollback         ('(YAL56/YBL400)-B@12'),
--rollback         ('(YAL56/YBL400)-B@13'),
--rollback         ('(YAL56/YBL400)-B@14'),
--rollback         ('(YAL56/YBL400)-B@15'),
--rollback         ('(YAL56/YBL400)-B@16')
--rollback     ) AS t (
--rollback         designation
--rollback     ),
--rollback     germplasm.germplasm AS ge
--rollback WHERE
--rollback     ge.designation = t.designation
--rollback     AND ge.id = gename.germplasm_id
--rollback ;



--changeset postgres:create_maize_dh_germplasm_attributes context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH germplasm attributes



-- add germplasm attribute DH_FIRST_INCREASE_COMPLETED
WITH t_germplasm AS (
    SELECT
        t.*,
        ge.id AS germplasm_id,
        ge.creator_id
    FROM (
            VALUES
            ('(FHWL42/FHWL120668)@14'),
            ('(FHWL42/FHWL120668)@16'),
            ('(FHWL42/FHWL12618)@200'),
            ('(FHWL42/FHWL12618)@50'),
            ('(FHWL42/FHWL12618)@62'),
            ('(FHWL42/FHWL12618)@63'),
            ('(FHWL42/FHWL12618)@64'),
            ('(ABLTI0139/FHWL12618)@96'),
            ('(ABLTI0139/FHWL12618)@97'),
            ('(ABLTI0139/FHWL12618)@98'),
            ('(ABLTI0139/FHWL12618)@99'),
            ('(ABLTI0139/FHWL12618)@100'),
            ('(ABLTI0139/FHWL12618)@101'),
            ('(ABLTI0139/FHWL12618)@102'),
            ('(ABLTI0139/FHWL12618)@103'),
            ('(ABLTI0139/FHWL12618)@104'),
            ('(BBYI0139/ABLTI0338)@22'),
            ('(BBYI0139/ABLTI0338)@23'),
            ('(BBYI0139/ABLTI0338)@24'),
            ('(BBYI0139/ABLTI0338)@25'),
            ('(BBYI0139/ABLTI0338)@26'),
            ('(BBYI0139/ABLTI0338)@27'),
            ('(BBYI0139/ABLTI0338)@28'),
            ('(BBYI0139/ABLTI0338)@29'),
            ('(BBYI0139/ABLTI0338)@30'),
            ('(BBYI0139/ABLTI0338)@31'),
            ('(BBYI0139/ABLTI0338)@32'),
            ('(YAL56/YBL400)-B@2'),
            ('(YAL56/YBL400)-B@3'),
            ('(YAL56/YBL400)-B@4'),
            ('(YAL56/YBL400)-B@5'),
            ('(YAL56/YBL400)-B@6'),
            ('(YAL56/YBL400)-B@7'),
            ('(YAL56/YBL400)-B@8'),
            ('(YAL56/YBL400)-B@9'),
            ('(YAL56/YBL400)-B@10'),
            ('(YAL56/YBL400)-B@11'),
            ('(YAL56/YBL400)-B@12'),
            ('(YAL56/YBL400)-B@13'),
            ('(YAL56/YBL400)-B@14'),
            ('(YAL56/YBL400)-B@15'),
            ('(YAL56/YBL400)-B@16')
        ) AS t (
            designation
        )
        JOIN germplasm.germplasm AS ge
            ON ge.designation = t.designation
)
INSERT INTO
    germplasm.germplasm_attribute (
        germplasm_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id
    )
SELECT
    t.germplasm_id,
    var.id AS variable_id,
    'FALSE' AS data_value,
    'G' AS data_qc_code,
    t.creator_id
FROM
    t_germplasm AS t
    JOIN master.variable AS var
        ON var.abbrev = 'DH_FIRST_INCREASE_COMPLETED'
;



-- revert changes
--rollback WITH t_germplasm AS (
--rollback     SELECT
--rollback         t.*,
--rollback         ge.id AS germplasm_id,
--rollback         ge.creator_id
--rollback     FROM (
--rollback             VALUES
--rollback             ('(FHWL42/FHWL120668)@14'),
--rollback             ('(FHWL42/FHWL120668)@16'),
--rollback             ('(FHWL42/FHWL12618)@200'),
--rollback             ('(FHWL42/FHWL12618)@50'),
--rollback             ('(FHWL42/FHWL12618)@62'),
--rollback             ('(FHWL42/FHWL12618)@63'),
--rollback             ('(FHWL42/FHWL12618)@64'),
--rollback             ('(ABLTI0139/FHWL12618)@96'),
--rollback             ('(ABLTI0139/FHWL12618)@97'),
--rollback             ('(ABLTI0139/FHWL12618)@98'),
--rollback             ('(ABLTI0139/FHWL12618)@99'),
--rollback             ('(ABLTI0139/FHWL12618)@100'),
--rollback             ('(ABLTI0139/FHWL12618)@101'),
--rollback             ('(ABLTI0139/FHWL12618)@102'),
--rollback             ('(ABLTI0139/FHWL12618)@103'),
--rollback             ('(ABLTI0139/FHWL12618)@104'),
--rollback             ('(BBYI0139/ABLTI0338)@22'),
--rollback             ('(BBYI0139/ABLTI0338)@23'),
--rollback             ('(BBYI0139/ABLTI0338)@24'),
--rollback             ('(BBYI0139/ABLTI0338)@25'),
--rollback             ('(BBYI0139/ABLTI0338)@26'),
--rollback             ('(BBYI0139/ABLTI0338)@27'),
--rollback             ('(BBYI0139/ABLTI0338)@28'),
--rollback             ('(BBYI0139/ABLTI0338)@29'),
--rollback             ('(BBYI0139/ABLTI0338)@30'),
--rollback             ('(BBYI0139/ABLTI0338)@31'),
--rollback             ('(BBYI0139/ABLTI0338)@32'),
--rollback             ('(YAL56/YBL400)-B@2'),
--rollback             ('(YAL56/YBL400)-B@3'),
--rollback             ('(YAL56/YBL400)-B@4'),
--rollback             ('(YAL56/YBL400)-B@5'),
--rollback             ('(YAL56/YBL400)-B@6'),
--rollback             ('(YAL56/YBL400)-B@7'),
--rollback             ('(YAL56/YBL400)-B@8'),
--rollback             ('(YAL56/YBL400)-B@9'),
--rollback             ('(YAL56/YBL400)-B@10'),
--rollback             ('(YAL56/YBL400)-B@11'),
--rollback             ('(YAL56/YBL400)-B@12'),
--rollback             ('(YAL56/YBL400)-B@13'),
--rollback             ('(YAL56/YBL400)-B@14'),
--rollback             ('(YAL56/YBL400)-B@15'),
--rollback             ('(YAL56/YBL400)-B@16')
--rollback         ) AS t (
--rollback             designation
--rollback         )
--rollback         JOIN germplasm.germplasm AS ge
--rollback             ON ge.designation = t.designation
--rollback )
--rollback DELETE FROM
--rollback     germplasm.germplasm_attribute AS t
--rollback USING
--rollback     t_germplasm AS u,
--rollback     master.variable AS var
--rollback WHERE
--rollback     t.germplasm_id = u.germplasm_id
--rollback     AND t.variable_id = var.id
--rollback     AND var.abbrev = 'DH_FIRST_INCREASE_COMPLETED'
--rollback ;



--changeset postgres:create_maize_dh_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH seeds



--# create maize DH seeds
INSERT INTO
    germplasm.seed (
        seed_code,
        seed_name,
        harvest_date,
        harvest_method,
        germplasm_id,
        program_id,
        source_experiment_id,
        source_entry_id,
        source_occurrence_id,
        source_location_id,
        source_plot_id,
        cross_id,
        harvest_source,
        creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    t.seed_name AS seed_name,
    NULL::date AS harvest_date,
    t.harvest_method AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL::integer AS source_experiment_id,
    NULL::integer AS source_entry_id,
    NULL::integer AS source_occurrence_id,
    NULL::integer AS source_location_id,
    NULL::integer AS source_plot_id,
    NULL::integer AS cross_id,
    'plot' AS harvest_source,
    ge.creator_id
FROM
    (
        VALUES
        ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1', 'Bulk', 'KE'),
        ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2', 'Bulk', 'KE'),
        ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3', 'Bulk', 'KE'),
        ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4', 'Bulk', 'KE'),
        ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5', 'Bulk', 'KE'),
        ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6', 'Bulk', 'KE'),
        ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8', 'Bulk', 'KE'),
        ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19', 'Bulk', 'KE'),
        ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14', 'Bulk', 'KE'),
        ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15', 'Bulk', 'KE')
    ) AS t (
        designation,
        seed_name,
        harvest_method,
        program
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN tenant.program AS prog
        ON prog.program_code = t.program
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.seed AS sd
--rollback USING (
--rollback         VALUES
--rollback         ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1'),
--rollback         ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2'),
--rollback         ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3'),
--rollback         ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4'),
--rollback         ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5'),
--rollback         ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6'),
--rollback         ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7'),
--rollback         ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1'),
--rollback         ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2'),
--rollback         ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3'),
--rollback         ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4'),
--rollback         ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5'),
--rollback         ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6'),
--rollback         ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7'),
--rollback         ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8'),
--rollback         ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9'),
--rollback         ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10'),
--rollback         ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11'),
--rollback         ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12'),
--rollback         ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13'),
--rollback         ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14'),
--rollback         ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15'),
--rollback         ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16'),
--rollback         ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17'),
--rollback         ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18'),
--rollback         ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19'),
--rollback         ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20'),
--rollback         ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1'),
--rollback         ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2'),
--rollback         ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3'),
--rollback         ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4'),
--rollback         ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5'),
--rollback         ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6'),
--rollback         ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7'),
--rollback         ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8'),
--rollback         ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9'),
--rollback         ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10'),
--rollback         ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11'),
--rollback         ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12'),
--rollback         ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13'),
--rollback         ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14'),
--rollback         ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name
--rollback     ),
--rollback     germplasm.germplasm AS ge
--rollback WHERE
--rollback     ge.designation = t.designation
--rollback     AND ge.id = sd.germplasm_id
--rollback     AND sd.seed_name = t.seed_name
--rollback ;



--changeset postgres:create_maize_dh_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH packages



--# create maize DH packages
INSERT INTO
    germplasm.package (
        package_code,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        seed_id,
        program_id,
        geospatial_object_id,
        facility_id,
        creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    t.package_label,
    t.package_quantity::float,
    t.package_unit,
    t.package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    NULL AS geospatial_object_id,
    NULL AS facility_id,
    ge.creator_id AS creator_id
FROM (
        VALUES
        ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1', 'KE20B-80-0001', '241', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2', 'KE20B-80-0002', '79', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3', 'KE20B-80-0003', '115', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4', 'KE20B-80-0004', '251', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5', 'KE20B-80-0005', '258', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6', 'KE20B-80-0006', '179', 'seed', 'active', 'KE'),
        ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7', 'KE20B-80-0007', '157', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1', 'KE20B-12-0001', '50', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2', 'KE20B-12-0002', '146', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3', 'KE20B-12-0003', '68', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4', 'KE20B-12-0004', '104', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5', 'KE20B-12-0005', '223', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6', 'KE20B-12-0006', '79', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7', 'KE20B-12-0007', '164', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8', 'KE20B-12-0008', '136', 'seed', 'active', 'KE'),
        ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9', 'KE20B-12-0009', '268', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10', 'KE20B-12-0010', '254', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11', 'KE20B-12-0011', '73', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12', 'KE20B-12-0012', '133', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13', 'KE20B-12-0013', '158', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14', 'KE20B-12-0014', '53', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15', 'KE20B-12-0015', '230', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16', 'KE20B-12-0016', '71', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17', 'KE20B-12-0017', '134', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18', 'KE20B-12-0018', '136', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19', 'KE20B-12-0019', '56', 'seed', 'active', 'KE'),
        ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20', 'KE20B-12-0020', '65', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1', 'KE18A-46-0001', '201', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2', 'KE18A-46-0002', '231', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3', 'KE18A-46-0003', '152', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4', 'KE18A-46-0004', '182', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5', 'KE18A-46-0005', '70', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6', 'KE18A-46-0006', '254', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7', 'KE18A-46-0007', '238', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8', 'KE18A-46-0008', '216', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9', 'KE18A-46-0009', '171', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10', 'KE18A-46-0010', '219', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11', 'KE18A-46-0011', '122', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12', 'KE18A-46-0012', '206', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13', 'KE18A-46-0013', '141', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14', 'KE18A-46-0014', '235', 'seed', 'active', 'KE'),
        ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15', 'KE18A-46-0015', '64', 'seed', 'active', 'KE')
    ) AS t (
        designation,
        seed_name,
        package_label,
        package_quantity,
        package_unit,
        package_status,
        program
    )
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN germplasm.seed AS sd
        ON sd.seed_name = t.seed_name
    JOIN tenant.program AS prog
        ON prog.program_code = t.program
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     (
--rollback         VALUES
--rollback         ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1', 'KE20B-80-0001'),
--rollback         ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2', 'KE20B-80-0002'),
--rollback         ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3', 'KE20B-80-0003'),
--rollback         ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4', 'KE20B-80-0004'),
--rollback         ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5', 'KE20B-80-0005'),
--rollback         ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6', 'KE20B-80-0006'),
--rollback         ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7', 'KE20B-80-0007'),
--rollback         ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1', 'KE20B-12-0001'),
--rollback         ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2', 'KE20B-12-0002'),
--rollback         ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3', 'KE20B-12-0003'),
--rollback         ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4', 'KE20B-12-0004'),
--rollback         ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5', 'KE20B-12-0005'),
--rollback         ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6', 'KE20B-12-0006'),
--rollback         ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7', 'KE20B-12-0007'),
--rollback         ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8', 'KE20B-12-0008'),
--rollback         ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9', 'KE20B-12-0009'),
--rollback         ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10', 'KE20B-12-0010'),
--rollback         ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11', 'KE20B-12-0011'),
--rollback         ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12', 'KE20B-12-0012'),
--rollback         ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13', 'KE20B-12-0013'),
--rollback         ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14', 'KE20B-12-0014'),
--rollback         ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15', 'KE20B-12-0015'),
--rollback         ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16', 'KE20B-12-0016'),
--rollback         ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17', 'KE20B-12-0017'),
--rollback         ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18', 'KE20B-12-0018'),
--rollback         ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19', 'KE20B-12-0019'),
--rollback         ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20', 'KE20B-12-0020'),
--rollback         ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1', 'KE18A-46-0001'),
--rollback         ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2', 'KE18A-46-0002'),
--rollback         ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3', 'KE18A-46-0003'),
--rollback         ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4', 'KE18A-46-0004'),
--rollback         ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5', 'KE18A-46-0005'),
--rollback         ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6', 'KE18A-46-0006'),
--rollback         ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7', 'KE18A-46-0007'),
--rollback         ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8', 'KE18A-46-0008'),
--rollback         ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9', 'KE18A-46-0009'),
--rollback         ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10', 'KE18A-46-0010'),
--rollback         ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11', 'KE18A-46-0011'),
--rollback         ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12', 'KE18A-46-0012'),
--rollback         ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13', 'KE18A-46-0013'),
--rollback         ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14', 'KE18A-46-0014'),
--rollback         ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15', 'KE18A-46-0015')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name,
--rollback         package_label
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.seed_name = t.seed_name
--rollback         AND sd.germplasm_id = ge.id
--rollback WHERE
--rollback     pkg.seed_id = sd.id
--rollback     AND pkg.package_label = t.package_label
--rollback ;



--changeset postgres:create_maize_dh_package_logs context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-737 Create maize DH package logs



--# create maize DH package logs
INSERT INTO
    germplasm.package_log (
        package_id,
        package_quantity,
        package_unit,
        package_transaction_type,
        entity_id,
        data_id,
        creator_id
    )
SELECT
    pkg.id AS package_id,
    pkg.package_quantity,
    pkg.package_unit,
    t.package_transaction_type,
    enty.id AS entity_id,
    t.data_id::integer AS data_id,
    pkg.creator_id
FROM (
        VALUES
        ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1', 'KE20B-80-0001', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2', 'KE20B-80-0002', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3', 'KE20B-80-0003', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4', 'KE20B-80-0004', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5', 'KE20B-80-0005', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6', 'KE20B-80-0006', 'deposit', 'PLOT', NULL),
        ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7', 'KE20B-80-0007', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1', 'KE20B-12-0001', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2', 'KE20B-12-0002', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3', 'KE20B-12-0003', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4', 'KE20B-12-0004', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5', 'KE20B-12-0005', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6', 'KE20B-12-0006', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7', 'KE20B-12-0007', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8', 'KE20B-12-0008', 'deposit', 'PLOT', NULL),
        ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9', 'KE20B-12-0009', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10', 'KE20B-12-0010', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11', 'KE20B-12-0011', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12', 'KE20B-12-0012', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13', 'KE20B-12-0013', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14', 'KE20B-12-0014', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15', 'KE20B-12-0015', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16', 'KE20B-12-0016', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17', 'KE20B-12-0017', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18', 'KE20B-12-0018', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19', 'KE20B-12-0019', 'deposit', 'PLOT', NULL),
        ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20', 'KE20B-12-0020', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1', 'KE18A-46-0001', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2', 'KE18A-46-0002', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3', 'KE18A-46-0003', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4', 'KE18A-46-0004', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5', 'KE18A-46-0005', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6', 'KE18A-46-0006', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7', 'KE18A-46-0007', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8', 'KE18A-46-0008', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9', 'KE18A-46-0009', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10', 'KE18A-46-0010', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11', 'KE18A-46-0011', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12', 'KE18A-46-0012', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13', 'KE18A-46-0013', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14', 'KE18A-46-0014', 'deposit', 'PLOT', NULL),
        ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15', 'KE18A-46-0015', 'deposit', 'PLOT', NULL)
    ) AS t (
        designation,
        seed_name,
        package_label,
        package_transaction_type,
        entity_id,
        data_id
    )
    JOIN dictionary.entity AS enty
        ON enty.abbrev = t.entity_id
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
    JOIN germplasm.seed AS sd
        ON sd.seed_name = t.seed_name
    JOIN germplasm.package AS pkg
        ON pkg.package_label = t.package_label
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING (
--rollback         VALUES
--rollback         ('(FHWL42/FHWL120668)@14', 'KB20B-DHN02-1', 'KE20B-80-0001'),
--rollback         ('(FHWL42/FHWL120668)@16', 'KB20B-DHN02-2', 'KE20B-80-0002'),
--rollback         ('(FHWL42/FHWL12618)@200', 'KB20B-DHN02-3', 'KE20B-80-0003'),
--rollback         ('(FHWL42/FHWL12618)@50', 'KB20B-DHN02-4', 'KE20B-80-0004'),
--rollback         ('(FHWL42/FHWL12618)@62', 'KB20B-DHN02-5', 'KE20B-80-0005'),
--rollback         ('(FHWL42/FHWL12618)@63', 'KB20B-DHN02-6', 'KE20B-80-0006'),
--rollback         ('(FHWL42/FHWL12618)@64', 'KB20B-DHN02-7', 'KE20B-80-0007'),
--rollback         ('(ABLTI0139/FHWL12618)@96', 'KB20B-DHN04-1', 'KE20B-12-0001'),
--rollback         ('(ABLTI0139/FHWL12618)@97', 'KB20B-DHN04-2', 'KE20B-12-0002'),
--rollback         ('(ABLTI0139/FHWL12618)@98', 'KB20B-DHN04-3', 'KE20B-12-0003'),
--rollback         ('(ABLTI0139/FHWL12618)@99', 'KB20B-DHN04-4', 'KE20B-12-0004'),
--rollback         ('(ABLTI0139/FHWL12618)@100', 'KB20B-DHN04-5', 'KE20B-12-0005'),
--rollback         ('(ABLTI0139/FHWL12618)@101', 'KB20B-DHN04-6', 'KE20B-12-0006'),
--rollback         ('(ABLTI0139/FHWL12618)@102', 'KB20B-DHN04-7', 'KE20B-12-0007'),
--rollback         ('(ABLTI0139/FHWL12618)@103', 'KB20B-DHN04-8', 'KE20B-12-0008'),
--rollback         ('(ABLTI0139/FHWL12618)@104', 'KB20B-DHN04-9', 'KE20B-12-0009'),
--rollback         ('(BBYI0139/ABLTI0338)@22', 'KB20B-DHN04-10', 'KE20B-12-0010'),
--rollback         ('(BBYI0139/ABLTI0338)@23', 'KB20B-DHN04-11', 'KE20B-12-0011'),
--rollback         ('(BBYI0139/ABLTI0338)@24', 'KB20B-DHN04-12', 'KE20B-12-0012'),
--rollback         ('(BBYI0139/ABLTI0338)@25', 'KB20B-DHN04-13', 'KE20B-12-0013'),
--rollback         ('(BBYI0139/ABLTI0338)@26', 'KB20B-DHN04-14', 'KE20B-12-0014'),
--rollback         ('(BBYI0139/ABLTI0338)@27', 'KB20B-DHN04-15', 'KE20B-12-0015'),
--rollback         ('(BBYI0139/ABLTI0338)@28', 'KB20B-DHN04-16', 'KE20B-12-0016'),
--rollback         ('(BBYI0139/ABLTI0338)@29', 'KB20B-DHN04-17', 'KE20B-12-0017'),
--rollback         ('(BBYI0139/ABLTI0338)@30', 'KB20B-DHN04-18', 'KE20B-12-0018'),
--rollback         ('(BBYI0139/ABLTI0338)@31', 'KB20B-DHN04-19', 'KE20B-12-0019'),
--rollback         ('(BBYI0139/ABLTI0338)@32', 'KB20B-DHN04-20', 'KE20B-12-0020'),
--rollback         ('(YAL56/YBL400)-B@2', 'KB18A-DHN01-1', 'KE18A-46-0001'),
--rollback         ('(YAL56/YBL400)-B@3', 'KB18A-DHN01-2', 'KE18A-46-0002'),
--rollback         ('(YAL56/YBL400)-B@4', 'KB18A-DHN01-3', 'KE18A-46-0003'),
--rollback         ('(YAL56/YBL400)-B@5', 'KB18A-DHN01-4', 'KE18A-46-0004'),
--rollback         ('(YAL56/YBL400)-B@6', 'KB18A-DHN01-5', 'KE18A-46-0005'),
--rollback         ('(YAL56/YBL400)-B@7', 'KB18A-DHN01-6', 'KE18A-46-0006'),
--rollback         ('(YAL56/YBL400)-B@8', 'KB18A-DHN01-7', 'KE18A-46-0007'),
--rollback         ('(YAL56/YBL400)-B@9', 'KB18A-DHN01-8', 'KE18A-46-0008'),
--rollback         ('(YAL56/YBL400)-B@10', 'KB18A-DHN01-9', 'KE18A-46-0009'),
--rollback         ('(YAL56/YBL400)-B@11', 'KB18A-DHN01-10', 'KE18A-46-0010'),
--rollback         ('(YAL56/YBL400)-B@12', 'KB18A-DHN01-11', 'KE18A-46-0011'),
--rollback         ('(YAL56/YBL400)-B@13', 'KB18A-DHN01-12', 'KE18A-46-0012'),
--rollback         ('(YAL56/YBL400)-B@14', 'KB18A-DHN01-13', 'KE18A-46-0013'),
--rollback         ('(YAL56/YBL400)-B@15', 'KB18A-DHN01-14', 'KE18A-46-0014'),
--rollback         ('(YAL56/YBL400)-B@16', 'KB18A-DHN01-15', 'KE18A-46-0015')
--rollback     ) AS t (
--rollback         designation,
--rollback         seed_name,
--rollback         package_label
--rollback     )
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.designation = t.designation
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.germplasm_id = ge.id
--rollback         AND sd.seed_name = t.seed_name
--rollback     JOIN germplasm.package AS pkg
--rollback         ON pkg.seed_id = sd.id
--rollback         AND pkg.package_label = t.package_label
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback ;
