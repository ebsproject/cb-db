--liquibase formatted sql

--changeset postgres:update_mismatch_rep_count_in_occurrences_and_plots context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-492 Update mismatch rep_count in occurrences and plots



-- update occurrence rep_count
WITH t_occs AS (
    -- get all occurrences and rep_count values
    SELECT
        occ.id AS occurrence_id,
        occ.rep_count
    FROM
        experiment.occurrence AS occ
), t_max_reps AS (
    -- get max rep of each occurrence from plots
    SELECT
        plot.occurrence_id,
        max(plot.rep) AS max_rep
    FROM
        experiment.plot AS plot
        JOIN experiment.entry AS ent
            ON plot.entry_id = ent.id
    WHERE
        ent.entry_type <> 'check'
    GROUP BY
        plot.occurrence_id
), t_update_occs AS (
    -- check which occurrences has mismatch max rep with rep_count
    -- where entry_type <> 'check'
    SELECT
        t.occurrence_id,
        t.rep_count,
        u.max_rep
    FROM
        t_occs AS t
        JOIN t_max_reps AS u
            ON u.occurrence_id = t.occurrence_id
    WHERE
        t.rep_count <> u.max_rep
)
UPDATE
    experiment.occurrence AS occ
SET
    rep_count = t.max_rep
FROM
    t_update_occs AS t
WHERE
    occ.id = t.occurrence_id
    AND occ.rep_count <> t.max_rep
;



-- revert changes
--rollback UPDATE
--rollback     experiment.occurrence AS occ
--rollback SET
--rollback     rep_count = t.rep_count
--rollback FROM (
--rollback         VALUES
--rollback         (74, 'IRSEA-RGA-2013-WS-002-001', 0),
--rollback         (375, 'IRSEA-RGA-2014-WS-002-001', 0),
--rollback         (390, 'IRSEA-PYT-2014-WS-002-001', 6),
--rollback         (700, 'IRSEA-OYT-2014-WS-002-001', 1),
--rollback         (705, 'IRSEA-OYT-2014-WS-002-002', 1),
--rollback         (706, 'IRSEA-OYT-2014-DS-001-001', 2),
--rollback         (737, 'IRSEA-PN-2014-DS-004-001', 1),
--rollback         (804, 'IRSEA-RGA-2014-WS-001-001', 0),
--rollback         (815, 'IRSEA-RGA-2013-WS-003-001', 0),
--rollback         (816, 'IRSEA-RGA-2014-DS-002-001', 0),
--rollback         (821, 'IRSEA-OYT-2014-WS-001-001', 1),
--rollback         (826, 'IRSEA-PYT-2014-WS-001-001', 1),
--rollback         (843, 'IRSEA-PN-2014-WS-003-001', 1),
--rollback         (1003, 'IRSEA-F3-2015-DS-002-001', 1),
--rollback         (1005, 'IRSEA-PN-2015-DS-004-001', 1),
--rollback         (1017, 'IRSEA-OYT-2015-DS-004-001', 1),
--rollback         (1028, 'IRSEA-OYT-2015-DS-008-001', 1),
--rollback         (1085, 'IRSEA-PN-2015-DS-002-001', 1),
--rollback         (1105, 'IRSEA-PYT-2015-DS-008-001', 4),
--rollback         (1129, 'IRSEA-OYT-2015-DS-003-001', 1),
--rollback         (1131, 'IRSEA-OYT-2015-DS-002-001', 1),
--rollback         (1384, 'IRSEA-OYT-2015-DS-007-001', 4),
--rollback         (1389, 'IRSEA-OYT-2015-DS-001-001', 1),
--rollback         (1397, 'IRSEA-OYT-2015-DS-005-001', 1),
--rollback         (1405, 'IRSEA-AGR-2015-DS-008-001', 3),
--rollback         (1411, 'IRSEA-PYT-2015-DS-010-001', 4),
--rollback         (1491, 'IRSEA-RGA-2014-WS-003-001', 0),
--rollback         (1504, 'IRSEA-RGA-2014-WS-004-001', 0),
--rollback         (1507, 'IRSEA-RGA-2014-WS-006-001', 0),
--rollback         (1508, 'IRSEA-RGA-2014-WS-005-001', 0),
--rollback         (1540, 'IRSEA-RGA-2015-DS-001-001', 0),
--rollback         (1621, 'IRSEA-F1-2015-WS-003-001', 2),
--rollback         (1763, 'IRSEA-PYT-2015-WS-005-001', 4),
--rollback         (1765, 'IRSEA-AYT-2015-WS-006-001', 4),
--rollback         (1766, 'IRSEA-AYT-2015-WS-003-001', 4),
--rollback         (1822, 'IRSEA-RGA-2015-WS-001-001', 1),
--rollback         (1955, 'IRSEA-PYT-2015-WS-013-001', 1),
--rollback         (1956, 'IRSEA-PYT-2015-WS-012-001', 1),
--rollback         (1981, 'IRSEA-AYT-2015-WS-007-001', 4),
--rollback         (1982, 'IRSEA-AYT-2015-WS-004-001', 3),
--rollback         (1988, 'IRSEA-AYT-2015-WS-007-002', 4),
--rollback         (1989, 'IRSEA-AYT-2015-WS-004-002', 3),
--rollback         (1990, 'IRSEA-AYT-2015-WS-007-003', 4),
--rollback         (1991, 'IRSEA-AYT-2015-WS-004-003', 3),
--rollback         (2016, 'IRSEA-RGA-2016-DS-001-001', 600),
--rollback         (2134, 'IRSEA-PYT-2016-DS-002-001', 1),
--rollback         (2163, 'IRSEA-RGA-2015-DS-002-001', 1),
--rollback         (2221, 'IRSEA-F2-2016-DS-004-001', 416),
--rollback         (2223, 'IRSEA-F2-2016-DS-001-001', 416),
--rollback         (2226, 'IRSEA-F2-2016-DS-005-001', 416),
--rollback         (2227, 'IRSEA-F2-2016-DS-002-001', 416),
--rollback         (2232, 'IRSEA-OYT-2016-DS-004-001', 2),
--rollback         (2249, 'IRSEA-AYT-2016-DS-003-001', 1),
--rollback         (2314, 'IRSEA-PYT-2016-DS-001-001', 1),
--rollback         (2338, 'IRSEA-AYT-2016-DS-001-001', 4),
--rollback         (2412, 'IRSEA-PYT-2016-DS-003-001', 1),
--rollback         (2441, 'IRSEA-F2-2016-WS-009-001', 416),
--rollback         (2443, 'IRSEA-F2-2016-WS-001-001', 416),
--rollback         (2446, 'IRSEA-F2-2016-WS-010-001', 416),
--rollback         (2465, 'IRSEA-AYT-2016-DS-005-001', 1),
--rollback         (2466, 'IRSEA-AYT-2016-DS-001-002', 1),
--rollback         (2467, 'IRSEA-AYT-2016-DS-005-002', 1),
--rollback         (2468, 'IRSEA-AYT-2016-DS-001-003', 1),
--rollback         (2632, 'IRSEA-OYT-2016-WS-003-001', 1),
--rollback         (2675, 'IRSEA-PYT-2016-WS-001-001', 1),
--rollback         (2676, 'IRSEA-AYT-2016-WS-005-001', 1),
--rollback         (2701, 'IRSEA-AYT-2016-WS-006-001', 1),
--rollback         (2702, 'IRSEA-AYT-2016-WS-006-002', 1),
--rollback         (2739, 'IRSEA-PYT-2016-WS-002-001', 1),
--rollback         (2740, 'IRSEA-PYT-2016-WS-002-002', 1),
--rollback         (2837, 'IRSEA-AYT-2016-WS-001-001', 1),
--rollback         (3382, 'KE-HB-2019-A-001-001', 3),
--rollback         (3391, 'KE-HB-2019-A-002-001', 3),
--rollback         (3392, 'KE-HB-2019-A-003-001', 3)
--rollback     ) AS t (
--rollback         occurrence_id,
--rollback         occurrence_name,
--rollback         rep_count
--rollback     )
--rollback WHERE
--rollback     occ.id = t.occurrence_id
--rollback     AND occ.occurrence_name = t.occurrence_name
--rollback     AND occ.rep_count <> t.rep_count
--rollback ;
