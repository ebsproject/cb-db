--liquibase formatted sql

--changeset postgres:create_plot_field_tags_view context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2655 Create a materialized view of Plot Field Tags API call



CREATE MATERIALIZED VIEW IF NOT EXISTS experiment.plot_field_tags_view
TABLESPACE pg_default
AS SELECT pi.plot_id AS "plotDbId",
    plot.plot_code AS "plotCode",
    plot.plot_number AS "plotNumber",
    plot.field_x AS "fieldX",
    plot.field_y AS "fieldY",
    pi.entry_id AS "entryDbId",
    pi.entry_code AS "entryCode",
    pi.entry_number AS "entryNumber",
    e.id AS "experimentDbId",
    e.experiment_code AS "experimentCode",
    e.experiment_name AS "experimentName",
    e.experiment_year AS "experimentYear",
    season.id AS "experimentSeasonDbId",
    season.season_code AS "experimentSeasonCode",
    season.season_name AS "experimentSeason",
    g.id AS "germplasmDbId",
    g.germplasm_code AS "germplasmCode",
    g.designation AS "germplasmName",
    g.parentage AS "germplasmParentage",
    o.id AS "occurrenceDbId",
    o.occurrence_code AS "occurrenceCode",
    o.occurrence_name AS "occurrenceName",
    o.occurrence_number AS "occurrenceNumber",
    pr.id AS "programDbId",
    pr.program_code AS "programCode",
    pr.program_name AS "programName",
    ( SELECT od.data_value
           FROM experiment.occurrence_data od
          WHERE od.occurrence_id = o.id AND od.variable_id = (( SELECT variable.id
                   FROM master.variable
                  WHERE variable.abbrev::text = 'SEEDING_DATE_CONT'::text)) AND od.is_void = false
         LIMIT 1) AS "seedingDate",
    site.id AS "siteDbId",
    site.geospatial_object_code AS "siteCode",
    site.geospatial_object_name AS site
   FROM experiment.plot plot
     JOIN experiment.planting_instruction pi ON pi.plot_id = plot.id
     JOIN germplasm.germplasm g ON g.id = pi.germplasm_id
     LEFT JOIN experiment.occurrence o ON o.id = plot.occurrence_id
     JOIN experiment.experiment e ON e.id = o.experiment_id
     LEFT JOIN place.geospatial_object site ON site.id = o.site_id
     JOIN tenant.season season ON season.id = e.season_id
     JOIN tenant.program pr ON pr.id = e.program_id
  WHERE o.is_void = false AND e.is_void = false AND pi.is_void = false AND g.is_void = false AND plot.is_void = false
WITH DATA;

CREATE INDEX IF NOT EXISTS plot_field_tags_view_occurrencedbid_idx ON experiment.plot_field_tags_view USING btree ("occurrenceDbId");



-- revert changes
--rollback DROP MATERIALIZED VIEW IF EXISTS experiment.plot_field_tags_view
--rollback ;
--rollback DROP INDEX IF EXISTS plot_field_tags_view_occurrencedbid_idx
--rollback ;