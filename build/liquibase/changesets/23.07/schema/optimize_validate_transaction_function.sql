--liquibase formatted sql

--changeset postgres:optimize_validate_transaction_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2577 DC-DB: Optimize data_terminal.validate_transaction function query



ALTER TABLE data_terminal.transaction_dataset ALTER COLUMN value DROP DEFAULT;

ALTER TABLE data_terminal.transaction_dataset
    ALTER COLUMN status SET DEFAULT 'new';

CREATE OR REPLACE FUNCTION data_terminal.validate_transaction(
	var_transaction_id integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    r_variable RECORD;
    r_data_unit RECORD;
BEGIN
    DELETE FROM data_terminal.transaction_dataset
    WHERE
        transaction_id = var_transaction_id
        AND (value is null or trim(value) = '')
        AND is_generated = false;
    for r_variable in
        SELECT
            v.id as variable_id,
            v.abbrev,
            v.scale_id,
            v.data_type,
            NULLIF(regexp_replace(min_value, '[^-0-9.]*','','g'), '')::numeric AS numeric_min_value,
            NULLIF(regexp_replace(max_value, '[^-0-9.]*','','g'), '')::numeric AS numeric_max_value
        FROM
            master.variable v
        LEFT JOIN master.scale s ON s.id = v.scale_id
        WHERE
            v.is_void = false
            AND v.id in (
                SELECT
                    distinct variable_id
                FROM
                    data_terminal.transaction_dataset
                WHERE
                    transaction_id = var_transaction_id
            )
            AND s.id = v.scale_id
    LOOP
    IF r_variable.data_type = 'integer'
    THEN
        UPDATE data_terminal.transaction_dataset
        SET status= 'new'
        WHERE
            value ~ '^-?\d+$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
			AND status <> 'new';
        UPDATE data_terminal.transaction_dataset
        SET status = 'invalid'
        WHERE
            value !~ '^-?\d+$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision' or r_variable.data_type = 'numeric')
    THEN
        UPDATE data_terminal.transaction_dataset
        SET status= 'new'
        WHERE
            value ~ '^[-+]?[0-9]*\.?[0-9]+$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
			AND status <> 'new';
        UPDATE data_terminal.transaction_dataset
        SET status = 'invalid'
        WHERE
            value !~ '^[-+]?[0-9]*\.?[0-9]+$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' )
    THEN
        IF r_variable.abbrev = 'SPECIFIC_PLANT' THEN
          UPDATE data_terminal.transaction_dataset
          SET status= 'new'
          WHERE
              REPLACE(value, ' ', '') ~ '^[0-9]+(,[0-9]+)*$'
              AND transaction_id = var_transaction_id
              AND variable_id = r_variable.variable_id
			  AND status <> 'new';
          UPDATE data_terminal.transaction_dataset
          SET status = 'invalid'
          WHERE
              REPLACE(value, ' ', '') !~ '^[0-9]+(,[0-9]+)*$'
              AND transaction_id = var_transaction_id
              AND variable_id = r_variable.variable_id
              AND status <> 'invalid';
        END IF;
    ELSIF r_variable.data_type = 'date' THEN
        UPDATE data_terminal.transaction_dataset
        SET status= 'new'
        WHERE
            value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
			AND status <> 'new';
        UPDATE data_terminal.transaction_dataset
        SET value=
            substring( value from '^\d+\/\d+\/((19|20)\d\d)$')  || '-' ||
            substring( value from '^\d+\/(0[1-9]|1[012])\/\d+$')  || '-' ||
            substring( value from '^((0[1-9]|[12][0-9]|3[01]))\/(.)+$')
        WHERE
            value ~ '^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id;
        UPDATE data_terminal.transaction_dataset
        SET status= 'invalid'
        WHERE
            value !~ '^((19|20)[0-9]{2}\-(01|0[3-9]|1[0-2])\-(29|30))|((19|20)[0-9]{2})\-(0[13578]|1[02])\-31|((19|20)[0-9]{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1[0-9]|2[0-8]))|((((19|20)(04|08|[2468][048]|[13579][26]))|2000)\-(02)\-29)$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    ELSIF r_variable.data_type = 'time'
    THEN
        UPDATE data_terminal.transaction_dataset
        SET status= 'new'
        WHERE
            value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
			AND status <> 'new';
        UPDATE data_terminal.transaction_dataset
        SET status= 'invalid'
        WHERE
            value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
    END IF;
    IF r_variable.data_type IN ('float', 'double precision', 'numeric', 'real', 'decimal','integer', 'smallint','bigint') THEN
        UPDATE data_terminal.transaction_dataset
        SET status = 'invalid'
        WHERE
            (
                (r_variable.numeric_min_value is not null
                AND NULLIF(regexp_replace(value, '[^-0-9.]*','','g'), '')::numeric < r_variable.numeric_min_value)
            OR
                (r_variable.numeric_max_value is not null
                AND NULLIF(regexp_replace(value, '[^-0-9.]*','','g'), '')::numeric > r_variable.numeric_max_value)
            )
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
			AND status <> 'invalid';
    END IF;
    IF r_variable.scale_id is not NULL AND EXISTS (SELECT 1 FROM master.scale_value where scale_id = r_variable.scale_id) THEN
        UPDATE data_terminal.transaction_dataset
            SET status = 'invalid'
        WHERE
            value NOT IN (
                SELECT value
                from
                    master.scale_value
                WHERE
                scale_id = r_variable.scale_id
                AND is_void = false
            )
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'invalid';
        UPDATE data_terminal.transaction_dataset
            SET status = 'new'
        WHERE
            value IN (
                SELECT value
                from
                    master.scale_value
                WHERE
                scale_id = r_variable.scale_id
                AND is_void = false
            )
            AND transaction_id = var_transaction_id
            AND variable_id = r_variable.variable_id
            AND status <> 'new';
    END IF;
	
	 UPDATE data_terminal.transaction_dataset
	 SET status = 'missing',
		value = trim(value)
	 WHERE
		  transaction_id = var_transaction_id
		  AND variable_id = r_variable.variable_id
		  AND value = 'NA'
          AND status <> 'missing';
    END LOOP;
    for r_data_unit in
        SELECT
            t.entity
        FROM
            data_terminal.transaction_dataset t
        WHERE
            t.transaction_id = var_transaction_id
        GROUP by t.entity
    LOOP
        IF r_data_unit.entity = 'plot_data' THEN
            UPDATE data_terminal.transaction_dataset t
            SET status =
				CASE
					WHEN ((t.value = 'NA' AND pd.data_value is null) OR status <> 'updated') THEN status
					WHEN status = 'new' THEN 'updated'
					ELSE status || ';updated'
				END,
				remarks =
				CASE
					WHEN t.value = 'NA' AND pd.data_value is null THEN 'missing'
				END
            FROM
                experiment.plot_data pd
            WHERE
                pd.variable_id = t.variable_id
                AND t.transaction_id = var_transaction_id
                AND pd.plot_id = t.entity_id
                AND pd.is_void = false
                AND t.entity = 'plot_data';
		 	DELETE FROM data_terminal.transaction_dataset
			WHERE
				transaction_id = var_transaction_id
				AND remarks = 'missing';
        ELSIF r_data_unit.entity = 'cross_data' THEN
            UPDATE data_terminal.transaction_dataset t
            SET status = 'updated'
            FROM
                germplasm.cross_data cd
            WHERE
                cd.variable_id = t.variable_id
                AND t.transaction_id = var_transaction_id
                AND cd.cross_id = t.entity_id
                AND cd.is_void = false
                AND t.entity = 'cross_data'
                AND t.status <> 'invalid';
        END IF;
    END LOOP;
RETURN 'success';
END;
$BODY$;
-- revert changes
--rollback CREATE OR REPLACE FUNCTION data_terminal.validate_transaction(
--rollback 	var_transaction_id integer)
--rollback     RETURNS character varying
--rollback     LANGUAGE 'plpgsql'
--rollback     COST 100
--rollback     VOLATILE PARALLEL UNSAFE
--rollback AS $BODY$
--rollback DECLARE
--rollback     r_variable RECORD;
--rollback     r_data_unit RECORD;
--rollback BEGIN
--rollback     DELETE FROM data_terminal.transaction_dataset 
--rollback     WHERE 
--rollback         transaction_id = var_transaction_id
--rollback         AND (value is null or trim(value) = '')
--rollback         AND is_generated = false;
--rollback 
--rollback     for r_variable in
--rollback         SELECT 
--rollback             v.id as variable_id,
--rollback             v.abbrev,
--rollback             v.scale_id,
--rollback             v.data_type,
--rollback             NULLIF(regexp_replace(min_value, '[^-0-9.]*','','g'), '')::numeric AS numeric_min_value,
--rollback             NULLIF(regexp_replace(max_value, '[^-0-9.]*','','g'), '')::numeric AS numeric_max_value
--rollback         FROM 
--rollback             master.variable v
--rollback         LEFT JOIN master.scale s ON s.id = v.scale_id
--rollback         WHERE 
--rollback             v.is_void = false 
--rollback             AND v.id in (
--rollback                 SELECT
--rollback                     distinct variable_id 
--rollback                 FROM 
--rollback                     data_terminal.transaction_dataset 
--rollback                 WHERE 
--rollback                     transaction_id = var_transaction_id
--rollback             )
--rollback             AND s.id = v.scale_id
--rollback     LOOP
--rollback 
--rollback     IF r_variable.data_type = 'integer'
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^-?\d+$' 
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^-?\d+$' 
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback     ELSIF (r_variable.data_type = 'float' or r_variable.data_type = 'double precision' or r_variable.data_type = 'numeric') 
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^[-+]?[0-9]*\.?[0-9]+$' 
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid' 
--rollback         WHERE 
--rollback             value !~ '^[-+]?[0-9]*\.?[0-9]+$' 
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback     ELSIF (r_variable.data_type = 'character varying' or r_variable.data_type = 'text' or r_variable.data_type = 'varchar' ) 
--rollback     THEN
--rollback         IF r_variable.abbrev = 'SPECIFIC_PLANT' THEN
--rollback 
--rollback           UPDATE data_terminal.transaction_dataset 
--rollback           SET status= 'new' 
--rollback           WHERE 
--rollback               REPLACE(value, ' ', '') ~ '^[0-9]+(,[0-9]+)*$'
--rollback               AND transaction_id = var_transaction_id 
--rollback               AND variable_id = r_variable.variable_id;
--rollback 
--rollback           UPDATE data_terminal.transaction_dataset 
--rollback           SET status= 'invalid' 
--rollback           WHERE 
--rollback               REPLACE(value, ' ', '') !~ '^[0-9]+(,[0-9]+)*$'
--rollback               AND transaction_id = var_transaction_id 
--rollback               AND variable_id = r_variable.variable_id;
--rollback          
--rollback         END IF;
--rollback 
--rollback     ELSIF r_variable.data_type = 'date' THEN
--rollback     
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$'
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback     
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET value=
--rollback             substring( value from '^\d+\/\d+\/((19|20)\d\d)$')  || '-' ||
--rollback             substring( value from '^\d+\/(0[1-9]|1[012])\/\d+$')  || '-' ||
--rollback             substring( value from '^((0[1-9]|[12][0-9]|3[01]))\/(.)+$') 
--rollback         WHERE 
--rollback             value ~ '^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$'
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^((19|20)[0-9]{2}\-(01|0[3-9]|1[0-2])\-(29|30))|((19|20)[0-9]{2})\-(0[13578]|1[02])\-31|((19|20)[0-9]{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1[0-9]|2[0-8]))|((((19|20)(04|08|[2468][048]|[13579][26]))|2000)\-(02)\-29)$' 
--rollback             AND transaction_id = var_transaction_id 
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback     ELSIF r_variable.data_type = 'time'
--rollback     THEN
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'new' 
--rollback         WHERE 
--rollback             value ~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
--rollback             AND transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status= 'invalid'  
--rollback         WHERE 
--rollback             value !~ '^(([01]?[0-9]|2[0-3]):[012345][0-9]:[012345][0-9])$'
--rollback             AND transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id;
--rollback             
--rollback     END IF;
--rollback 
--rollback     IF r_variable.data_type IN ('float', 'double precision', 'numeric', 'real', 'decimal','integer', 'smallint','bigint') THEN
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET status = 'invalid' 
--rollback         WHERE 
--rollback             (
--rollback                 (r_variable.numeric_min_value is not null 
--rollback                 AND NULLIF(regexp_replace(value, '[^-0-9.]*','','g'), '')::numeric < r_variable.numeric_min_value) 
--rollback             OR
--rollback                 (r_variable.numeric_max_value is not null 
--rollback                 AND NULLIF(regexp_replace(value, '[^-0-9.]*','','g'), '')::numeric > r_variable.numeric_max_value) 
--rollback             )
--rollback             AND transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id
--rollback 			AND status <> 'invalid';
--rollback 
--rollback     END IF;
--rollback 
--rollback     IF r_variable.scale_id is not NULL THEN
--rollback 
--rollback         IF EXISTS (SELECT 1 FROM master.scale_value where scale_id = r_variable.scale_id) THEN
--rollback             UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'invalid' 
--rollback             WHERE 
--rollback                 value NOT IN (
--rollback                     SELECT value 
--rollback                     from 
--rollback                         master.scale_value 
--rollback                     WHERE 
--rollback                     scale_id = r_variable.scale_id
--rollback                     AND is_void = false
--rollback                 )
--rollback                 AND transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id;
--rollback 
--rollback             UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'new'
--rollback             WHERE 
--rollback                 value IN (
--rollback                     SELECT value 
--rollback                     from 
--rollback                         master.scale_value 
--rollback                     WHERE 
--rollback                     scale_id = r_variable.scale_id
--rollback                     AND is_void = false
--rollback                 )
--rollback                 AND transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id;
--rollback         ELSE
--rollback              UPDATE data_terminal.transaction_dataset 
--rollback                 SET status = 'new' 
--rollback             WHERE 
--rollback                 transaction_id = var_transaction_id
--rollback                 AND variable_id = r_variable.variable_id
--rollback                 AND status IS NULL;
--rollback         END IF;
--rollback     
--rollback     ELSE
--rollback 
--rollback         UPDATE data_terminal.transaction_dataset 
--rollback         SET is_data_value_valid = 'new' 
--rollback         WHERE 
--rollback             transaction_id = var_transaction_id
--rollback             AND variable_id = r_variable.variable_id
--rollback             AND status IS NULL;
--rollback     END IF;
--rollback 	
--rollback 	 UPDATE data_terminal.transaction_dataset 
--rollback 	 SET status = 'missing',
--rollback 		value = trim(value)
--rollback 	 WHERE 
--rollback 		  transaction_id = var_transaction_id
--rollback 		  AND variable_id = r_variable.variable_id
--rollback 		  AND value = 'NA';
--rollback 
--rollback     END LOOP;
--rollback 
--rollback     for r_data_unit in
--rollback         SELECT 
--rollback             t.entity
--rollback         FROM 
--rollback             data_terminal.transaction_dataset t
--rollback         WHERE 
--rollback             t.transaction_id = var_transaction_id
--rollback         GROUP by t.entity
--rollback     LOOP
--rollback         IF r_data_unit.entity = 'plot_data' THEN
--rollback             UPDATE data_terminal.transaction_dataset t
--rollback             SET status = 
--rollback 				CASE 
--rollback 					WHEN t.value = 'NA' AND pd.data_value is null THEN status
--rollback 					WHEN status = 'new' THEN 'updated'
--rollback 					ELSE status || ';updated'
--rollback 				END,
--rollback 				remarks = 
--rollback 				CASE 
--rollback 					WHEN t.value = 'NA' AND pd.data_value is null THEN 'missing'
--rollback 				END
--rollback             FROM 
--rollback                 experiment.plot_data pd
--rollback             WHERE 
--rollback                 pd.variable_id = t.variable_id
--rollback                 AND t.transaction_id = var_transaction_id
--rollback                 AND pd.plot_id = t.entity_id
--rollback                 AND pd.is_void = false
--rollback                 AND t.entity = 'plot_data';
--rollback 		 	DELETE FROM data_terminal.transaction_dataset 
--rollback 			WHERE 
--rollback 				transaction_id = var_transaction_id
--rollback 				AND remarks = 'missing';
--rollback         ELSIF r_data_unit.entity = 'cross_data' THEN
--rollback             UPDATE data_terminal.transaction_dataset t
--rollback             SET status = 'updated'
--rollback             FROM 
--rollback                 germplasm.cross_data cd
--rollback             WHERE 
--rollback                 cd.variable_id = t.variable_id
--rollback                 AND t.transaction_id = var_transaction_id
--rollback                 AND cd.cross_id = t.entity_id
--rollback                 AND cd.is_void = false
--rollback                 AND t.entity = 'cross_data'
--rollback                 AND t.status <> 'invalid';
--rollback 
--rollback         END IF;
--rollback 
--rollback     END LOOP;
--rollback 
--rollback RETURN 'success';
--rollback 
--rollback END;
--rollback $BODY$;
--rollback ALTER TABLE data_terminal.transaction_dataset
--rollback     ALTER COLUMN status DROP DEFAULT;