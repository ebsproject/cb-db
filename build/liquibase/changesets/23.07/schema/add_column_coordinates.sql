--liquibase formatted sql

--changeset postgres:add_column_coordinates context:schema splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2712 Add column coordinates to place.geospatial_object



ALTER TABLE
    place.geospatial_object
ADD COLUMN
    coordinates jsonb
;

COMMENT ON COLUMN place.geospatial_object.coordinates
    IS 'Coordinates: Sets of values that define the position or location of a point in a specific reference system [GEO_COORDINATES]';



-- revert changes
--rollback ALTER TABLE
--rollback     place.geospatial_object
--rollback DROP COLUMN
--rollback     coordinates
--rollback ;