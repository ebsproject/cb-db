--liquibase formatted sql

--changeset postgres:populate_fixture_file_upload_entity_id context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CORB-5917 GM: view completed merge transaction showing GERMPLASM-SEED-PACKAGE tabs



-- curate germplasm.file_upload.entity_id for merge germplasm fixture data
WITH t AS (
    SELECT
        file_upload.id AS file_upload_id,
		file_upload.file_upload_action AS file_upload_action,
		file_upload.entity_id AS entity_id
    FROM
        germplasm.file_upload file_upload
    WHERE
        file_upload.is_void = FALSE
        AND file_upload.file_upload_action = 'merge'
        AND file_upload.entity_id IS NULL
)
UPDATE
    germplasm.file_upload AS file_upload
SET
    entity_id = (
		SELECT 
			id 
		FROM 
			dictionary.entity 
		WHERE 
			abbrev = 'GERMPLASM'
	),
    notes = platform.append_text(
		file_upload.notes,
		'CORB-5917 GM: view completed merge transaction showing GERMPLASM-SEED-PACKAGE tabs'
	)
FROM
    t
WHERE
	t.entity_id IS NULL
;



-- revert changes
--rollback UPDATE germplasm.file_upload SET entity_id = NULL WHERE notes LIKE '%CORB-5917%';