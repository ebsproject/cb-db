--liquibase formatted sql

--changeset postgres:correct_fixture_mapped_plots_irsea_001 context:fixture splitStatements:false rollbackSplitStatements:false
--comment: CORB-6019 HM-DB: Correct location mapping of plots of some fixture occurrences


-- update location id of plots of occurrence id 64415
UPDATE 
	experiment.plot 
SET 
	location_id = 44415
WHERE 
	occurrence_id = 64415;

-- update location id of plots of occurrence id 64416
UPDATE 
	experiment.plot 
SET 
	location_id = 44416
WHERE 
	occurrence_id = 64416;

-- update location id of plots of occurrence id 64417
UPDATE 
	experiment.plot 
SET 
	location_id = 44417
WHERE 
	occurrence_id = 64417;

-- update location id of plots of occurrence id 64418
UPDATE 
	experiment.plot 
SET 
	location_id = 44418
WHERE 
	occurrence_id = 64418;


--rollback -- update location id of plots of occurrence id 64415
--rollback UPDATE 
--rollback 	experiment.plot 
--rollback SET 
--rollback 	location_id = NULL
--rollback WHERE 
--rollback 	occurrence_id = 64415;
--rollback 
--rollback -- update location id of plots of occurrence id 64416
--rollback UPDATE 
--rollback 	experiment.plot 
--rollback SET 
--rollback 	location_id = NULL
--rollback WHERE 
--rollback 	occurrence_id = 64416;
--rollback 
--rollback -- update location id of plots of occurrence id 64417
--rollback UPDATE 
--rollback 	experiment.plot 
--rollback SET 
--rollback 	location_id = NULL
--rollback WHERE 
--rollback 	occurrence_id = 64417;
--rollback 
--rollback -- update location id of plots of occurrence id 64418
--rollback UPDATE 
--rollback 	experiment.plot 
--rollback SET 
--rollback 	location_id = NULL
--rollback WHERE 
--rollback 	occurrence_id = 64418;