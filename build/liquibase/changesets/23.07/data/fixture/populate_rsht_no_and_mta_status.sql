--liquibase formatted sql

--changeset postgres:populate_rsht_no_and_mta_status context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DEVOPS-2659 Populate RSHT_NO and MTA_STATUS



INSERT INTO
    germplasm.seed_data 
        (seed_id, variable_id, data_value, data_qc_code, creator_id, notes)
SELECT 
    s.id seed_id,
    v.id variable_id,
    e.experiment_name || p.package_code data_value,
    'G' data_qc_code,
    1 creator_id,
    'DEVOPS-2659 Populate seed data' notes
FROM germplasm.seed s
JOIN
    experiment.experiment e 
ON
    s.source_experiment_id = e.id 
JOIN
    germplasm.package p 
ON
    p.seed_id = s.id
JOIN 
    master.variable v 
ON
    v.abbrev = 'RSHT_NO'
WHERE 
    e.experiment_year  = 2016
ORDER BY s.id
;

--populate mta_status
INSERT INTO
    germplasm.germplasm_mta
        (germplasm_id, seed_id, mta_status, use, creator_id, notes)
SELECT 
	DISTINCT ON (germplasm_id, seed_id)
	g.id germplasm_id,
	s.id seed_id,
	'PUD2' mta_status,
	'Active' use,
	1 creator_id,
	'DEVOPS-2659 Populate mta status' notes
FROM germplasm.seed s
JOIN
	experiment.experiment e 
ON
	s.source_experiment_id = e.id 
JOIN
	germplasm.package p 
ON
	p.seed_id = s.id
JOIN 
	germplasm.germplasm g 
ON
	s.germplasm_id  = g.id
WHERE 
	e.experiment_year  = 2016
ORDER BY s.id 
;


-- revert changes
--rollback DELETE FROM germplasm.seed_data WHERE notes = 'DEVOPS-2659 Populate seed data';
--rollback DELETE FROM germplasm.germplasm_mta WHERE notes = 'DEVOPS-2659 Populate mta status';