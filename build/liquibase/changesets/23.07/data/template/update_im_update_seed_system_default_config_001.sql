--liquibase formatted sql

--changeset postgres:update_im_update_seed_system_default_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5169 IM-DB: Change seed-packages-search API call to seeds-search?isBasic=true for all seed code and seed name related retrievals



-- update config
UPDATE platform.config
SET
    config_value = $$
    {
        "values": [
            {
                "name": "Seed Code",
                "type": "column",
                "view": {
                    "visible": "false",
                    "entities": []
                },
                "usage": "required",
                "abbrev": "SEED_CODE",
                "entity": "seed",
                "required": "true",
                "api_field": "seedDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "seedCode",
                "retrieve_db_id": "true",
                "url_parameters": "isBasic=true&limit=1",
                "db_id_api_field": "seedDbId",
                "search_endpoint": "seeds-search",
                "additional_filters": {}
            },
            {
                "name": "Seed Name",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "SEED_NAME",
                "entity": "seed",
                "required": "false",
                "api_field": "",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "true",
                "value_filter": "seedName",
                "retrieve_db_id": "true",
                "url_parameters": "isBasic=true&limit=1",
                "db_id_api_field": "seedDbId",
                "search_endpoint": "seeds-search",
                "additional_filters": {}
            },
            {
                "name": "Program Code",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "required",
                "abbrev": "PROGRAM_CODE",
                "entity": "seed",
                "required": "false",
                "api_field": "programDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "programCode",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "programDbId",
                "search_endpoint": "programs-search",
                "additional_filters": {}
            },
            {
                "name": "Harvest Date",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "HVDATE_CONT",
                "entity": "seed",
                "required": "false",
                "api_field": "harvestDate",
                "data_type": "date",
                "http_method": "",
                "skip_update": "false",
                "value_filter": "",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Harvest Method",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "HV_METH_DISC",
                "entity": "seed",
                "required": "false",
                "api_field": "harvestMethod",
                "data_type": "string",
                "http_method": "",
                "skip_update": "false",
                "value_filter": "",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            },
            {
                "name": "Source Experiment Code",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "EXPERIMENT_CODE",
                "entity": "seed",
                "required": "false",
                "api_field": "sourceExperimentDbId",
                "data_type": "string",
                "http_method": "POST",
                "skip_update": "false",
                "value_filter": "experimentCode",
                "retrieve_db_id": "true",
                "url_parameters": "limit=1",
                "db_id_api_field": "experimentDbId",
                "search_endpoint": "experiments-search",
                "additional_filters": {}
            },
            {
                "name": "Description",
                "type": "column",
                "view": {
                    "visible": "true",
                    "entities": [
                    "seed"
                    ]
                },
                "usage": "optional",
                "abbrev": "DESCRIPTION",
                "entity": "seed",
                "required": "false",
                "api_field": "description",
                "data_type": "string",
                "http_method": "",
                "skip_update": "false",
                "value_filter": "",
                "retrieve_db_id": "false",
                "url_parameters": "",
                "db_id_api_field": "",
                "search_endpoint": "",
                "additional_filters": {}
            }
        ]
    }
    $$
WHERE
    abbrev = 'IM_UPDATE_SEED_SYSTEM_DEFAULT';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $$
--rollback     {
--rollback         "values": [
--rollback             {
--rollback                 "name": "Seed Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "false",
--rollback                     "entities": []
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "SEED_CODE",
--rollback                 "entity": "seed",
--rollback                 "required": "true",
--rollback                 "api_field": "seedDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "seedCode",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "dataLevel=all&limit=1",
--rollback                 "db_id_api_field": "seedDbId",
--rollback                 "search_endpoint": "seed-packages-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Seed Name",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "SEED_NAME",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "true",
--rollback                 "value_filter": "seedName",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "dataLevel=all&limit=1",
--rollback                 "db_id_api_field": "seedDbId",
--rollback                 "search_endpoint": "seed-packages-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Program Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "abbrev": "PROGRAM_CODE",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "programDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "programCode",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "programDbId",
--rollback                 "search_endpoint": "programs-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Date",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HVDATE_CONT",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestDate",
--rollback                 "data_type": "date",
--rollback                 "http_method": "",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Harvest Method",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "HV_METH_DISC",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "harvestMethod",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Source Experiment Code",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "EXPERIMENT_CODE",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "sourceExperimentDbId",
--rollback                 "data_type": "string",
--rollback                 "http_method": "POST",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "experimentCode",
--rollback                 "retrieve_db_id": "true",
--rollback                 "url_parameters": "limit=1",
--rollback                 "db_id_api_field": "experimentDbId",
--rollback                 "search_endpoint": "experiments-search",
--rollback                 "additional_filters": {}
--rollback             },
--rollback             {
--rollback                 "name": "Description",
--rollback                 "type": "column",
--rollback                 "view": {
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                     "seed"
--rollback                     ]
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "abbrev": "DESCRIPTION",
--rollback                 "entity": "seed",
--rollback                 "required": "false",
--rollback                 "api_field": "description",
--rollback                 "data_type": "string",
--rollback                 "http_method": "",
--rollback                 "skip_update": "false",
--rollback                 "value_filter": "",
--rollback                 "retrieve_db_id": "false",
--rollback                 "url_parameters": "",
--rollback                 "db_id_api_field": "",
--rollback                 "search_endpoint": "",
--rollback                 "additional_filters": {}
--rollback             }
--rollback         ]
--rollback     }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'IM_UPDATE_SEED_SYSTEM_DEFAULT';