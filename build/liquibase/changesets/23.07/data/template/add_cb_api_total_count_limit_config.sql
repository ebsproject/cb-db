--liquibase formatted sql

--changeset postgres:add_cb_api_total_count_limit_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5932 Add CB API total count limit



INSERT INTO
    platform.config
    (
        abbrev,
        name,
        config_value,
        usage
    )
VALUES
    (
        'CB_API_TOTAL_COUNT_LIMIT',
        'Add limit on the total count of data to be retrieved in CB API',
        $$500000$$,
        'api'
    );


--rollback DELETE FROM platform.config WHERE abbrev = 'CB_API_TOTAL_COUNT_LIMIT';