--liquibase formatted sql

--changeset postgres:update_gm_update_variable_global_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5822 GM: The germplasm_relation export file for more than one selected germplasms contained the result of single germplasm with duplicated rows



-- update GLOBAL config
UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"CROSS_NUMBER",
                "display_value":"Cross Number",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"GROWTH_HABIT",
                "display_value":"Growth Habit",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"GRAIN_COLOR",
                "display_value":"Grain Color",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"MTA_STATUS",
                "display_value":"MTA Status",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-mta-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmMtaDbId",
                    "search_filter":"mtaStatus",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"HETEROTIC_GROUP",
                "display_value":"Heterotic Group",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            },
            {
                "abbrev":"DH_FIRST_INCREASE_COMPLETED",
                "display_value":"Double Haploid First Increase Completed",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm"
                    ]
                }
            }
        ]
    }$$
WHERE
    abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';



--rollback  UPDATE platform.config
--rollback  SET
--rollback      config_value = $${
--rollback      "values": [
--rollback          {
--rollback              "abbrev":"GERMPLASM_CODE",
--rollback              "display_value":"Germplasm Code",
--rollback              "type":"basic",
--rollback              "entity":"germplasm",
--rollback              "retrieve_db_id": "true",
--rollback              "api_resource":{
--rollback                  "http_method":"POST",
--rollback                  "search_endpoint":"germplasm-search",
--rollback                  "search_params":"limit=1",
--rollback                  "search_api_field":"germplasmDbId",
--rollback                  "search_filter":"germplasmCode",
--rollback                  "additional_filters":{}
--rollback              },
--rollback              "usage": "required",
--rollback              "in_export": "true",
--rollback              "in_upload": "true",
--rollback              "in_browser": "true",
--rollback              "in_summary": "false",
--rollback              "view":{
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                      "germplasm"
--rollback                  ]
--rollback              }
--rollback          },
--rollback          {
--rollback              "abbrev":"DESIGNATION",
--rollback              "display_value":"Designation",
--rollback              "type":"basic",
--rollback              "entity":"germplasm",
--rollback              "retrieve_db_id": "true",
--rollback              "api_resource":{
--rollback                  "http_method":"POST",
--rollback                  "search_endpoint":"germplasm-search",
--rollback                  "search_params":"limit=1",
--rollback                  "search_api_field":"germplasmDbId",
--rollback                  "search_filter":"designation",
--rollback                  "additional_filters":{}
--rollback              },
--rollback              "usage": "optional",
--rollback              "in_export": "true",
--rollback              "in_upload": "true",
--rollback              "in_browser": "true",
--rollback              "in_summary": "false",
--rollback              "view":{
--rollback                  "visible": "true",
--rollback                  "entities": [
--rollback                      "germplasm"
--rollback                  ]
--rollback              }
--rollback          }
--rollback      ]
--rollback  }$$
--rollback  WHERE
--rollback      abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';
