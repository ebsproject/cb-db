--liquibase formatted sql

--changeset postgres:update_gm_bg_threshold_config_002 context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5739 GM: Enable download of template from saved germplasm list



UPDATE platform.config
SET
    config_value = $${
        "ancestryExportDepth": {
            "size": "10",
            "description": "Maximum depth of exported germplasm ancestry information."
        },
        "searchCharMin":{
            "size": "3",
            "description": "Minimum number of characters when searching."
        }
    }$$
WHERE
    abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback     "ancestryExportDepth": {
--rollback         "size": "6",
--rollback         "description": "Maximum depth of exported germplasm ancestry information."
--rollback     },
--rollback     "searchCharMin":{
--rollback     	"size": "3",
--rollback     	"description": "Minimum number of characters when searching."
--rollback   }
--rollback }$$
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';