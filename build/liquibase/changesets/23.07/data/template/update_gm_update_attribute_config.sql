--liquibase formatted sql

--changeset postgres:update_gm_update_variable_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5860 GM: Germplasm Code column in File Upload Summary page for Update showing not set values

UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            }
        ]
    }$$
WHERE
    abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';

UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"CROSS_NUMBER",
                "display_value":"Cross Number",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"GROWTH_HABIT",
                "display_value":"Growth Habit",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"GRAIN_COLOR",
                "display_value":"Grain Color",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"MTA_STATUS",
                "display_value":"MTA Status",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-mta-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmMtaDbId",
                    "search_filter":"mtaStatus",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            }
        ]
    }$$
WHERE
    abbrev = 'GM_CROP_WHEAT_GERMPLASM_UPDATE_CONFIG';

UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"HETEROTIC_GROUP",
                "display_value":"Heterotic Group",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"DH_FIRST_INCREASE_COMPLETED",
                "display_value":"Double Haploid First Increase Completed",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"GET",
                    "search_endpoint":"germplasm/{id}/attributes",
                    "search_params":"",
                    "search_api_field":"",
                    "search_filter":"",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"MTA_STATUS",
                "display_value":"MTA Status",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-mta-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmMtaDbId",
                    "search_filter":"mtaStatus",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            }
        ]
    }$$
WHERE
    abbrev = 'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG';

UPDATE platform.config
SET
    config_value = $${
        "values": [
            {
                "abbrev":"GERMPLASM_CODE",
                "display_value":"Germplasm Code",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"germplasmCode",
                    "additional_filters":{}
                },
                "usage": "required",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"DESIGNATION",
                "display_value":"Designation",
                "type":"basic",
                "entity":"germplasm",
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmDbId",
                    "search_filter":"designation",
                    "additional_filters":{}
                },
                "usage": "optional",
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "false",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            },
            {
                "abbrev":"MTA_STATUS",
                "display_value":"MTA Status",
                "type":"attribute", 
                "entity":"germplasm_attribute", 
                "retrieve_db_id": "true",
                "api_resource":{
                    "http_method":"POST",
                    "search_endpoint":"germplasm-mta-search",
                    "search_params":"limit=1",
                    "search_api_field":"germplasmMtaDbId",
                    "search_filter":"mtaStatus",
                    "additional_filters":{}
                },
                "usage": "optional", 
                "in_export": "true",
                "in_upload": "true",
                "in_browser": "true",
                "in_summary": "true",
                "view":{
                    "visible": "true",
                    "entities": [
                        "germplasm",
                        "germplasm_attribute"
                    ]
                }
            }
        ]
    }$$

WHERE
    abbrev = 'GM_CROP_RICE_GERMPLASM_UPDATE_CONFIG';

--rollback changes
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "abbrev":"GERMPLASM_CODE",
--rollback                 "display_value":"Germplasm Code",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"germplasmCode",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"DESIGNATION",
--rollback                 "display_value":"Designation",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"designation",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_GLOBAL_GERMPLASM_UPDATE_CONFIG';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "abbrev":"GERMPLASM_CODE",
--rollback                 "display_value":"Germplasm Code",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"germplasmCode",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"DESIGNATION",
--rollback                 "display_value":"Designation",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"designation",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"CROSS_NUMBER",
--rollback                 "display_value":"Cross Number",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"GET",
--rollback                     "search_endpoint":"germplasm/{id}/attributes",
--rollback                     "search_params":"",
--rollback                     "search_api_field":"",
--rollback                     "search_filter":"",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"GROWTH_HABIT",
--rollback                 "display_value":"Growth Habit",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"GET",
--rollback                     "search_endpoint":"germplasm/{id}/attributes",
--rollback                     "search_params":"",
--rollback                     "search_api_field":"",
--rollback                     "search_filter":"",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"GRAIN_COLOR",
--rollback                 "display_value":"Grain Color",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"GET",
--rollback                     "search_endpoint":"germplasm/{id}/attributes",
--rollback                     "search_params":"",
--rollback                     "search_api_field":"",
--rollback                     "search_filter":"",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"MTA_STATUS",
--rollback                 "display_value":"MTA Status",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-mta-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmMtaDbId",
--rollback                     "search_filter":"mtaStatus",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_CROP_WHEAT_GERMPLASM_UPDATE_CONFIG';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "abbrev":"GERMPLASM_CODE",
--rollback                 "display_value":"Germplasm Code",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"germplasmCode",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"DESIGNATION",
--rollback                 "display_value":"Designation",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"designation",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"HETEROTIC_GROUP",
--rollback                 "display_value":"Heterotic Group",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"GET",
--rollback                     "search_endpoint":"germplasm/{id}/attributes",
--rollback                     "search_params":"",
--rollback                     "search_api_field":"",
--rollback                     "search_filter":"",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"DH_FIRST_INCREASE_COMPLETED",
--rollback                 "display_value":"Double Haploid First Increase Completed",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"GET",
--rollback                     "search_endpoint":"germplasm/{id}/attributes",
--rollback                     "search_params":"",
--rollback                     "search_api_field":"",
--rollback                     "search_filter":"",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"MTA_STATUS",
--rollback                 "display_value":"MTA Status",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-mta-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmMtaDbId",
--rollback                     "search_filter":"mtaStatus",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_CROP_MAIZE_GERMPLASM_UPDATE_CONFIG';
--rollback 
--rollback UPDATE platform.config
--rollback SET
--rollback     config_value = $${
--rollback         "values": [
--rollback             {
--rollback                 "abbrev":"GERMPLASM_CODE",
--rollback                 "display_value":"Germplasm Code",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"germplasmCode",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "required",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"DESIGNATION",
--rollback                 "display_value":"Designation",
--rollback                 "type":"basic",
--rollback                 "entity":"germplasm",
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmDbId",
--rollback                     "search_filter":"designation",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional",
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "false",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "abbrev":"MTA_STATUS",
--rollback                 "display_value":"MTA Status",
--rollback                 "type":"attribute", 
--rollback                 "entity":"germplasm_attribute", 
--rollback                 "retrieve_db_id": "true",
--rollback                 "api_resource":{
--rollback                     "http_method":"POST",
--rollback                     "search_endpoint":"germplasm-mta-search",
--rollback                     "search_params":"limit=1",
--rollback                     "search_api_field":"germplasmMtaDbId",
--rollback                     "search_filter":"mtaStatus",
--rollback                     "additional_filters":{}
--rollback                 },
--rollback                 "usage": "optional", 
--rollback                 "in_export": "true",
--rollback                 "in_upload": "true",
--rollback                 "in_browser": "true",
--rollback                 "in_summary": "true",
--rollback                 "view":{
--rollback                     "visible": "true",
--rollback                     "entities": [
--rollback                         "germplasm"
--rollback                     ]
--rollback                 }
--rollback             }
--rollback         ]
--rollback     }$$
--rollback WHERE
--rollback     abbrev = 'GM_CROP_RICE_GERMPLASM_UPDATE_CONFIG';