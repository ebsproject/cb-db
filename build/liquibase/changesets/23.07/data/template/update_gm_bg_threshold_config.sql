--liquibase formatted sql

--changeset postgres:update_gm_bg_threshold_config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-5601 Make the minimum number of characters in Germplasm search filter for name configurable



UPDATE
    platform.config
SET
    config_value = $$
{
    "ancestryExportDepth": {
        "size": "6",
        "description": "Maximum depth of exported germplasm ancestry information."
    },
    "searchCharMin":{
    	"size": "3",
    	"description": "Minimum number of characters when searching."
  }
}
$$
WHERE
    abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';

--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback {
--rollback    "ancestryExportDepth": {
--rollback        "size": "6",
--rollback        "description": "Maximum depth of exported germplasm ancestry information."
--rollback    }
--rollback }
--rollback $$
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD';