--liquibase formatted sql

--changeset postgres:add_index_to_list_id_column_in_platform.list_member_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-834 Add index to list_id column in platform.list_member table



-- add index
CREATE INDEX list_member_list_id_idx
    ON platform.list_member USING btree (list_id);



-- revert changes
--rollback DROP INDEX platform.list_member_list_id_idx;



--changeset postgres:add_index_to_data_id_column_in_platform.list_member_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-834 Add index to data_id column in platform.list_member table


-- add index
CREATE INDEX list_member_data_id_idx
    ON platform.list_member USING btree (data_id);



-- revert changes
--rollback DROP INDEX platform.list_member_data_id_idx;



--changeset postgres:add_index_to_multiple_columns_in_platform.list_member_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-834 Add index to list_id, data_id, is_active, and is_void columns in platform.list_member table


-- add index
CREATE INDEX list_member_list_id_data_id_is_active_is_void_idx
    ON platform.list_member USING btree (list_id, data_id, is_active, is_void)



-- revert changes
--rollback DROP INDEX platform.list_member_list_id_data_id_is_active_is_void_idx;
