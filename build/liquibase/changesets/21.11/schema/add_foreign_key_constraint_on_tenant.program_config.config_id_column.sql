--liquibase formatted sql

--changeset postgres:add_foreign_key_constraint_on_tenant.program_config.config_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-772 Add foreign key constraint on config_id column in tenant.program_config table



-- add foreign key constraint
ALTER TABLE
    tenant.program_config
ADD CONSTRAINT
    program_config_config_id_fk
FOREIGN KEY (
        config_id
    )
REFERENCES
    platform.config (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    program_config_config_id_fk
ON
    tenant.program_config
IS
    'A config can be used by one or more programs.'
;



-- revert changes
--rollback ALTER TABLE
--rollback     tenant.program_config
--rollback DROP CONSTRAINT
--rollback     program_config_config_id_fk
--rollback ;
