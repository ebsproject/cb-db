--liquibase formatted sql

--changeset postgres:update_usage_char_length context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-816 Update usage char length



ALTER TABLE master.variable ALTER COLUMN usage type character varying(32);



--rollback ALTER TABLE master.variable ALTER COLUMN usage type character varying(16);