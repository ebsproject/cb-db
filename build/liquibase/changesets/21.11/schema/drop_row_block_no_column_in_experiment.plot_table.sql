--liquibase formatted sql

--changeset postgres:drop_row_block_no_column_in_experiment.plot_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-795 Drop row_block_no column in experiment.plot table



-- drop column
ALTER TABLE experiment.plot DROP COLUMN row_block_no;



-- revert changes
--rollback ALTER TABLE experiment.plot
--rollback     ADD COLUMN row_block_no integer;
--rollback 
--rollback COMMENT ON COLUMN experiment.plot.row_block_no
--rollback     IS 'Row Block Number: Row block information of plot [PLOT_ROWBLKNO]';
