--liquibase formatted sql

--changeset postgres:drop_not_null_constraint_in_geospatial_object_subtype_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-63 Drop not null constraint in geospatial_object_subtype column



-- drop not null constraint
ALTER TABLE
    place.geospatial_object
ALTER COLUMN
    geospatial_object_subtype
    DROP NOT NULL
;



-- revert changes
--rollback ALTER TABLE
--rollback     place.geospatial_object
--rollback ALTER COLUMN
--rollback     geospatial_object_subtype
--rollback     SET NOT NULL
--rollback ;
