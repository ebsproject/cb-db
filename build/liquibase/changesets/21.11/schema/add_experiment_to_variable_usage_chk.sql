--liquibase formatted sql

--changeset postgres:add_experiment_to_variable_usage_chk context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-777 Add experiment to variable_usage_chk



ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment']));



--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;

--rollback ALTER TABLE master.variable
--rollback   ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'study', 'undefined', 'management']));