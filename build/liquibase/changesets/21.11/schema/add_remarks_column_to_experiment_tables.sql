--liquibase formatted sql

--changeset postgres:add_remarks_column_to_experiment.experiment_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-793 Add remarks column to experiment.experiment table



-- add remarks column
ALTER TABLE
    experiment.experiment
ADD COLUMN
    remarks text
;

COMMENT ON COLUMN
    experiment.experiment.remarks
IS
    'Remarks: Additional remarks about the experiment [REMARKS]'
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.experiment
--rollback DROP COLUMN
--rollback     remarks
--rollback ;



--changeset postgres:add_remarks_column_to_experiment.occurrence_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-793 Add remarks column to experiment.occurrence table



-- add remarks column
ALTER TABLE
    experiment.occurrence
ADD COLUMN
    remarks text
;

COMMENT ON COLUMN
    experiment.occurrence.remarks
IS
    'Remarks: Additional remarks about the occurrence [REMARKS]'
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.occurrence
--rollback DROP COLUMN
--rollback     remarks
--rollback ;



--changeset postgres:add_remarks_column_to_experiment.location_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-793 Add remarks column to experiment.location table



-- add remarks column
ALTER TABLE
    experiment.location
ADD COLUMN
    remarks text
;

COMMENT ON COLUMN
    experiment.location.remarks
IS
    'Remarks: Additional remarks about the location [REMARKS]'
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.location
--rollback DROP COLUMN
--rollback     remarks
--rollback ;
