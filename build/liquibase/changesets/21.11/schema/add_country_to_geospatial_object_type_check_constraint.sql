--liquibase formatted sql

--changeset postgres:add_country_to_geospatial_object_type_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-63 Add country to geospatial_object_type check constraint


-- drop check constraint
ALTER TABLE
    place.geospatial_object
DROP CONSTRAINT
    geospatial_object_geospatial_object_type_chk
;

-- add country to list
ALTER TABLE
    place.geospatial_object
ADD CONSTRAINT
    geospatial_object_geospatial_object_type_chk CHECK (geospatial_object_type = ANY (ARRAY['country', 'site', 'field', 'planting area']))
;



-- revert changes
--rollback ALTER TABLE
--rollback     place.geospatial_object
--rollback DROP CONSTRAINT
--rollback     geospatial_object_geospatial_object_type_chk
--rollback ;
--rollback
--rollback ALTER TABLE
--rollback     place.geospatial_object
--rollback ADD CONSTRAINT
--rollback     geospatial_object_geospatial_object_type_chk CHECK (geospatial_object_type = ANY (ARRAY['site', 'field', 'planting area']))
--rollback ;
