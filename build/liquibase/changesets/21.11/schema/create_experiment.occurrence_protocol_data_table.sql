--liquibase formatted sql

--changeset postgres:create_experiment.occurrence_protocol_data_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-792 Create experiment.occurrence_protocol_data table



-- create table
CREATE TABLE experiment.occurrence_protocol_data (
    -- primary key column
    id serial NOT NULL,
    
    -- columns
    occurrence_id integer NOT NULL,
    variable_id integer NOT NULL,
    data_value varchar NOT NULL,
    list_id integer,
    
    -- audit columns
    remarks text,
    creation_timestamp timestamptz NOT NULL DEFAULT now(),
    creator_id integer NOT NULL,
    modification_timestamp timestamptz,
    modifier_id integer,
    notes text,
    is_void boolean NOT NULL DEFAULT FALSE,
    event_log jsonb,
    
    -- primary key constraint
    CONSTRAINT occurrence_protocol_data_id_pk PRIMARY KEY (id),
    
    -- audit foreign key constraints
    CONSTRAINT occurrence_protocol_data_creator_id_fk FOREIGN KEY (creator_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT occurrence_protocol_data_modifier_id_fk FOREIGN KEY (modifier_id)
        REFERENCES tenant.person (id)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

-- constraints
ALTER TABLE
    experiment.occurrence_protocol_data
ADD CONSTRAINT
    occurrence_protocol_data_occurrence_id_fk
FOREIGN KEY (
        occurrence_id
    )
REFERENCES
    experiment.occurrence (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    occurrence_protocol_data_occurrence_id_fk
ON
    experiment.occurrence_protocol_data
IS
    'An occurrence can have zero or more protocol data.'
;

ALTER TABLE
    experiment.occurrence_protocol_data
ADD CONSTRAINT
    occurrence_protocol_data_variable_id_fk
FOREIGN KEY (
        variable_id
    )
REFERENCES
    master.variable (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    occurrence_protocol_data_variable_id_fk
ON
    experiment.occurrence_protocol_data
IS
    'A protocol data refers to exactly one variable.'
;

ALTER TABLE
    experiment.occurrence_protocol_data
ADD CONSTRAINT
    occurrence_protocol_data_list_id_fk
FOREIGN KEY (
        list_id
    )
REFERENCES
    platform.list (
        id
    )
    ON UPDATE CASCADE
    ON DELETE RESTRICT
;

COMMENT ON CONSTRAINT
    occurrence_protocol_data_list_id_fk
ON
    experiment.occurrence_protocol_data
IS
    'A protocol data can refer to zero or exactly one list.'
;

-- table comment
COMMENT ON TABLE experiment.occurrence_protocol_data
    IS 'Occurrence Protocol Data: Values for protocols recorded in an occurrence [OCCPROTDATA]';

-- column comments
COMMENT ON COLUMN experiment.occurrence_protocol_data.occurrence_id
    IS 'Occurrence ID: Reference to the occurrence where the protocol data is linked [OCCPROTDATA_OCC_ID]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.variable_id
    IS 'Variable ID: Reference to the variable of the protocol data [OCCPROTDATA_VAR_ID]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.data_value
    IS 'Data Value: Value of the occurrence protocol data [OCCPROTDATA_DATAVAL]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.list_id
    IS 'List ID: Reference to the protocol list of the data [OCCPROTDATA_LIST_ID]';

-- audit column comments
COMMENT ON COLUMN experiment.occurrence_protocol_data.id
    IS 'ID: Database identifier of the record [ID]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.remarks
    IS 'Remarks: Additional notes to record [REMARKS]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.creation_timestamp
    IS 'Creation Timestamp: Timestamp when the record was created [CTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.creator_id
    IS 'Creator ID: Reference to the person who created the record [CPERSON]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.modification_timestamp
    IS 'Modification Timestamp: Timestamp when the record was last modified [MTSTAMP]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.modifier_id
    IS 'Modifier ID: Reference to the person who last modified the record [MPERSON]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.notes
    IS 'Notes: Technical details about the record [NOTES]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.is_void
    IS 'Is Void: Indicates whether the record is still operational or already soft-deleted [ISVOID]';
COMMENT ON COLUMN experiment.occurrence_protocol_data.event_log
    IS 'Event Log: Event Log: Historical transactions of the record [EVENTLOG]';

-- indices
CREATE INDEX occurrence_protocol_data_occurrence_id_idx
    ON experiment.occurrence_protocol_data USING btree (occurrence_id);
CREATE INDEX occurrence_protocol_data_variable_id_idx
    ON experiment.occurrence_protocol_data USING btree (variable_id);
CREATE INDEX occurrence_protocol_data_occurrence_variable_value_idx
    ON experiment.occurrence_protocol_data USING btree (occurrence_id, variable_id, data_value);
CREATE INDEX occurrence_protocol_data_list_id_idx
    ON experiment.occurrence_protocol_data USING btree (list_id);

-- audit indices
CREATE INDEX occurrence_protocol_data_creator_id_idx ON experiment.occurrence_protocol_data USING btree (creator_id);
CREATE INDEX occurrence_protocol_data_modifier_id_idx ON experiment.occurrence_protocol_data USING btree (modifier_id);
CREATE INDEX occurrence_protocol_data_is_void_idx ON experiment.occurrence_protocol_data USING btree (is_void);

-- triggers
CREATE TRIGGER occurrence_protocol_data_event_log_tgr BEFORE INSERT OR UPDATE
    ON experiment.occurrence_protocol_data FOR EACH ROW EXECUTE FUNCTION platform.log_record_event();



-- revert changes
--rollback DROP TABLE
--rollback     experiment.occurrence_protocol_data
--rollback ;
