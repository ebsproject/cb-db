--liquibase formatted sql

--changeset postgres:update_platform.append_text_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-779 Update platform.append_text function



-- update function
CREATE OR REPLACE FUNCTION platform.append_text(notes text, new_notes text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
    updated_notes text = '';
begin
    return platform.append_text(notes, new_notes, ',');
end; $function$
;



-- revert changes
--rollback CREATE OR REPLACE FUNCTION platform.append_text(notes text, new_notes text)
--rollback  RETURNS text
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback declare
--rollback     updated_notes text = '';
--rollback begin
--rollback     return platform.append_text(notes, new_notes, '; ');
--rollback end; $function$
--rollback ;
