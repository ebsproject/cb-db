--liquibase formatted sql

--changeset postgres:add_experiment_manager_to_variable_usage_chk context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-816 Add experiment manager to variable_usage_chk



ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment', 'experiment_manager']));



--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_usage_chk;
--rollback 
--rollback ALTER TABLE master.variable
--rollback     ADD CONSTRAINT variable_usage_chk CHECK (usage::text = ANY (ARRAY['application', 'study', 'undefined', 'management', 'experiment']));