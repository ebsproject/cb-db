--liquibase formatted sql

--changeset postgres:add_polygon_to_variable_data_type_chk context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-798 Add polygon to variable_data_type_chk constraint



ALTER TABLE master.variable DROP CONSTRAINT variable_data_type_chk;

ALTER TABLE master.variable
    ADD CONSTRAINT variable_data_type_chk CHECK (data_type::text = ANY (ARRAY['character varying', 'text', 'integer', 'float', 'numeric', 'date', 'timestamp', 'time', 'month', 'day', 'year', 'boolean', 'smallint', 'inet', 'name', 'bigint', 'oid', 'json', 'polygon']));



--rollback ALTER TABLE master.variable DROP CONSTRAINT variable_data_type_chk;
--rollback 
--rollback ALTER TABLE master.variable
--rollback     ADD CONSTRAINT variable_data_type_chk CHECK (data_type::text = ANY (ARRAY['character varying', 'text', 'integer', 'float', 'numeric', 'date', 'timestamp', 'time', 'month', 'day', 'year', 'boolean', 'smallint', 'inet', 'name', 'bigint', 'oid', 'json']));