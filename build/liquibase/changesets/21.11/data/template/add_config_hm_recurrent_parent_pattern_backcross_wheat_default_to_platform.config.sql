--liquibase formatted sql

--changeset postgres:add_config_hm_recurrent_parent_pattern_backcross_wheat_default_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-755 Add config HM_RECURRENT_PARENT_PATTERN_BACKCROSS_WHEAT_DEFAULT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_WHEAT_DEFAULT',
        'Harvest Manager Recurrent Parent Pattern for Wheat Backcrosses',
        $$			      
            {
                "pattern": [
                    [
                        {
                            "type": "field",
                            "entity": "recurrentParentGermplasm",
                            "field_name": "parentage",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "*",
                            "order_number": 1
                        },
                        {
                            "type": "counter",
                            "order_number": 2
                        }
                    ],
                    [
                        {
                            "type": "counter",
                            "order_number": 0
                        },
                        {
                            "type": "delimiter",
                            "value": "*",
                            "order_number": 1
                        },
                        {
                            "type": "field",
                            "entity": "recurrentParentGermplasm",
                            "field_name": "parentage",
                            "order_number": 2
                        }
                    ]
                ],
                "delimiter_parentage": "/",
                "delimiter_backcross_number": "*"
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-755 Add config HM_RECURRENT_PARENT_PATTERN_BACKCROSS_WHEAT_DEFAULT to platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_RECURRENT_PARENT_PATTERN_BACKCROSS_WHEAT_DEFAULT';