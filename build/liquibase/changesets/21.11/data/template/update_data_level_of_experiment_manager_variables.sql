--liquibase formatted sql

--changeset postgres:update_data_level_of_em_experiment_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-779 Update data level of EM experiment variables



-- update experiment-level variables
WITH t_variables AS (
    SELECT
        var.abbrev,
        (CASE
            WHEN t.to_replace THEN t.data_level
            ELSE platform.append_text(array_to_string(array_remove(string_to_array(REPLACE(var.data_level, ', ', ','), ','), 'study'), ','), t.data_level, ',')
        END) AS new_data_level
    FROM
        master.variable AS var
        JOIN (
            VALUES
            ('PROJECT', 'experiment', TRUE),
            ('PIPELINE', 'experiment', TRUE),
            ('PIPELINE_ID', 'experiment', TRUE),
            ('DESCRIPTION', 'experiment,occurrence,location', FALSE),
            ('EXPERIMENT_OBJECTIVE', 'experiment', TRUE),
            ('EXPERIMENT_STEWARD', 'experiment', TRUE),
            ('EXPERIMENT_YEAR', 'experiment', TRUE),
            ('SEASON', 'experiment', TRUE),
            ('SEASON_ID', 'experiment', TRUE),
            ('STAGE', 'experiment', TRUE),
            ('ECOSYSTEM', 'experiment,occurrence,location', TRUE),
            ('REMARKS', 'experiment', TRUE)
            --('PROJECT_ID', 'experiment', TRUE),
            --('STEWARD_ID', 'experiment', FALSE),
            --('STAGE_ID', 'experiment', TRUE),
            --('MARKET_SEGMENT', 'experiment', TRUE)
        ) AS t (
            abbrev, data_level, to_replace
        )
            ON var.abbrev = t.abbrev
)
UPDATE
    master.variable AS var
SET
    data_level = t.new_data_level
FROM
    t_variables AS t
WHERE
    t.abbrev = var.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('PROJECT', 'experiment, study, occurrence'),
--rollback         ('PIPELINE', 'experiment, study, occurrence'),
--rollback         ('PIPELINE_ID', NULL),
--rollback         ('DESCRIPTION', NULL),
--rollback         ('EXPERIMENT_OBJECTIVE', 'experiment, study, occurrence'),
--rollback         ('EXPERIMENT_STEWARD', 'experiment, study, occurrence'),
--rollback         ('EXPERIMENT_YEAR', 'experiment, study, occurrence'),
--rollback         ('SEASON', 'study, occurrence'),
--rollback         ('SEASON_ID', NULL),
--rollback         ('STAGE', 'experiment, study, occurrence'),
--rollback         ('ECOSYSTEM', 'study, occurrence'),
--rollback         ('REMARKS', 'study,entry,plot, occurrence')
--rollback     ) AS t (
--rollback         abbrev, old_data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;



--changeset postgres:update_data_level_of_em_occurrence_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-779 Update data level of EM occurrence variables



-- update occurrence-level variables
WITH t_variables AS (
    SELECT
        var.abbrev,
        (CASE
            WHEN t.to_replace THEN t.data_level
            ELSE platform.append_text(array_to_string(array_remove(string_to_array(REPLACE(var.data_level, ', ', ','), ','), 'study'), ','), t.data_level, ',')
        END) AS new_data_level
    FROM
        master.variable AS var
        JOIN (
            VALUES
            ('CONTCT_PERSON_CONT', 'occurrence', TRUE),
            ('SITE', 'occurrence', TRUE),
            ('FIELD', 'occurrence', TRUE)
            --('SITE_ID', 'occurrence', TRUE),
            --('FIELD_ID', 'occurrence', TRUE),
            --('GEOSPATIAL_COORDS', 'experiment', TRUE),
            --('PLANTING_DATE', 'occurrence', TRUE)
        ) AS t (
            abbrev, data_level, to_replace
        )
            ON var.abbrev = t.abbrev
)
UPDATE
    master.variable AS var
SET
    data_level = t.new_data_level
FROM
    t_variables AS t
WHERE
    t.abbrev = var.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('CONTCT_PERSON_CONT', 'study, occurrence'),
--rollback         ('SITE', 'experiment, study, occurrence'),
--rollback         ('FIELD', 'experiment, study, occurrence')
--rollback     ) AS t (
--rollback         abbrev, old_data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;



--changeset postgres:fix_data_level_for_swapped_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-779 Fix data level for swapped variables



-- update data levels
UPDATE
    master.variable AS var
SET
    data_level = t.data_level
FROM (
        VALUES
        ('ECOSYSTEM', 'experiment'),
        ('REMARKS', 'experiment,occurrence,location')
    ) AS t (
        abbrev, data_level        
    )
WHERE
    var.abbrev = t.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('ECOSYSTEM', 'experiment,occurrence,location'),
--rollback         ('REMARKS', 'experiment')
--rollback     ) AS t (
--rollback         abbrev, data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;
