--liquibase formatted sql

--changeset postgres:update_type_of_planting_date_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-814 Update type of PLANTING_DATE variable



-- update variable
UPDATE
    master.variable
SET
    type = 'metadata'
WHERE
    abbrev = 'PLANTING_DATE'
;



-- revert changes
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     type = 'observation'
--rollback WHERE
--rollback     abbrev = 'PLANTING_DATE'
--rollback ;
