--liquibase formatted sql

--changeset postgres:remove_experiment_data_level_in_planting_date_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-844 Remove experiment data level in PLANTING_DATE variable



-- update PLANTING_DATE data level
WITH t_variables AS (
    SELECT
        var.abbrev,
        (CASE
            WHEN t.to_replace THEN t.data_level
            ELSE platform.append_text(array_to_string(array_remove(string_to_array(REPLACE(var.data_level, ', ', ','), ','), 'study'), ','), t.data_level, ',')
        END) AS new_data_level
    FROM
        master.variable AS var
        JOIN (
            VALUES
            ('PLANTING_DATE', 'occurrence,location', TRUE)
        ) AS t (
            abbrev, data_level, to_replace
        )
            ON var.abbrev = t.abbrev
)
UPDATE
    master.variable AS var
SET
    data_level = t.new_data_level
FROM
    t_variables AS t
WHERE
    t.abbrev = var.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('PLANTING_DATE', 'experiment,occurrence,location')
--rollback     ) AS t (
--rollback         abbrev, old_data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;
