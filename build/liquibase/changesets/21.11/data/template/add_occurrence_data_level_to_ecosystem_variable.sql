--liquibase formatted sql

--changeset postgres:add_occurrence_data_level_to_ecosystem_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-836 Add occurrence data level to ECOSYSTEM variable



-- update experiment-level variables
WITH t_variables AS (
    SELECT
        var.abbrev,
        (CASE
            WHEN t.to_replace THEN t.data_level
            ELSE platform.append_text(array_to_string(array_remove(string_to_array(REPLACE(var.data_level, ', ', ','), ','), 'study'), ','), t.data_level, ',')
        END) AS new_data_level
    FROM
        master.variable AS var
        JOIN (
            VALUES
            ('ECOSYSTEM', 'experiment,occurrence', TRUE)
        ) AS t (
            abbrev, data_level, to_replace
        )
            ON var.abbrev = t.abbrev
)
UPDATE
    master.variable AS var
SET
    data_level = t.new_data_level
FROM
    t_variables AS t
WHERE
    t.abbrev = var.abbrev
;



-- revert changes
--rollback UPDATE
--rollback     master.variable AS var
--rollback SET
--rollback     data_level = t.old_data_level
--rollback FROM (
--rollback         VALUES
--rollback         ('ECOSYSTEM', 'experiment')
--rollback     ) AS t (
--rollback         abbrev, old_data_level
--rollback     )
--rollback WHERE
--rollback     var.abbrev = t.abbrev
--rollback ;
