--liquibase formatted sql

--changeset postgres:void_partially_replicated_and_augmented_design_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-857 Void partially replicated and augemented design scale values



UPDATE 
    master.scale_value 
SET 
    is_void = true 
WHERE 
    value 
IN 
    (
        'Partially Replicated', 
        'Augmented Design'
    ) 
AND 
    scale_id 
IN 
    (
        SELECT 
            scale_id 
        FROM 
            master.variable 
        WHERE 
            abbrev = 'DESIGN'
    )
;



--rollback UPDATE 
--rollback     master.scale_value 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     value 
--rollback IN 
--rollback     (
--rollback         'Partially Replicated', 
--rollback         'Augmented Design'
--rollback     ) 
--rollback AND 
--rollback     scale_id 
--rollback IN 
--rollback     (
--rollback         SELECT 
--rollback             scale_id 
--rollback         FROM 
--rollback             master.variable 
--rollback         WHERE 
--rollback             abbrev = 'DESIGN'
--rollback     )
--rollback ;