--liquibase formatted sql

--changeset postgres:add_export_record_threshold_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-840 Add Export Record Threshold Value in EM



UPDATE
    platform.config
SET
    config_value = 
    '
        {
            "saveListMembers": {
                "size": "1000",
                "description": "Save list members"
            },
            "generateLocation": {
                "size": "1000",
                "description": "Generate location for an Occurrence"
            },
            "uploadPlantingArrays": {
                "size": "1000",
                "description": "Upload planting arrays for an Occurrence"
            },
            "selectOccurrences": {
                "size": "300",
                "description": "Limit for selecting Occurrences across pages"
            },
            "exportRecords": {
                "size": "5000",
                "description": "Minimum amount of records to trigger background processing"
            }
        }
    '
WHERE
    abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "saveListMembers": {
--rollback                 "size": "1000",
--rollback                 "description": "Save list members"
--rollback             },
--rollback             "generateLocation": {
--rollback                 "size": "1000",
--rollback                 "description": "Generate location for an Occurrence"
--rollback             },
--rollback             "uploadPlantingArrays": {
--rollback                 "size": "1000",
--rollback                 "description": "Upload planting arrays for an Occurrence"
--rollback             },
--rollback            "selectOccurrences": {
--rollback                "size": "300",
--rollback                "description": "Limit for selecting Occurrences across pages"
--rollback            }
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'EXPERIMENT_MANAGER_BG_PROCESSING_THRESHOLD';