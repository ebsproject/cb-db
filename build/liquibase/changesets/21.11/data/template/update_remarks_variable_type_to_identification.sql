--liquibase formatted sql

--changeset postgres:update_remarks_variable_type_to_identification context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-839 Update REMARKS variable type to identification



UPDATE master.variable SET type='identification' WHERE abbrev='REMARKS';



--rollback UPDATE master.variable SET type='metadata' WHERE abbrev='REMARKS';