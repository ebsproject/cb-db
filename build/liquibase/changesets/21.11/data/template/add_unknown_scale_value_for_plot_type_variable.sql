--liquibase formatted sql

--changeset postgres:add_unknown_scale_value_for_plot_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-49 Add unknown scale value for PLOT_TYPE variable



INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        (
            'PLOT_TYPE_UNKNOWN', 
            'Unknown', 
            (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PLOT_TYPE')), 
            'Unknown', 
            'Unknown', 
            1, 
            'show'
        )
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PLOT_TYPE'
;



--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('PLOT_TYPE_UNKNOWN')
--rollback ;
