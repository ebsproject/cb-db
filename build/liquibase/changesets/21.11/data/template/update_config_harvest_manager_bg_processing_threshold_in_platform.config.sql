--liquibase formatted sql

--changeset postgres:update_config_harvest_manager_bg_processing_threshold_in_platform.config_ context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-818 Update config HARVEST_MANAGER_BG_PROCESSING_THRESHOLD in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "createList": {
                "size": "500",
                "description": "Create new germplasm, seed, or package list"
            },
            "deleteSeeds": {
                "size": "200",
                "description": "Delete seeds of plots or crosses"
            },
            "deletePackages": {
                "size": "200",
                "description": "Delete packages and related records"
            },
            "deleteHarvestData": {
                "size": "2000",
                "description": "Delete harvest data of plots or crosses"
            },
            "updateHarvestData": {
                "size": "5000",
                "description": "Update harvest data of plots or crosses"
            }
        }
    ' 
WHERE 
    abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';

--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "createList": {
--rollback                 "size": "500",
--rollback                 "description": "Create new germplasm, seed, or package list"
--rollback             },
--rollback             "deleteSeeds": {
--rollback                 "size": "200",
--rollback                 "description": "Delete seeds of plots or crosses"
--rollback             },
--rollback             "deletePackages": {
--rollback                 "size": "200",
--rollback                 "description": "Delete packages and related records"
--rollback             },
--rollback             "deleteHarvestData": {
--rollback                 "size": "2000",
--rollback                 "description": "Delete harvest data of plots or crosses"
--rollback             }
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';