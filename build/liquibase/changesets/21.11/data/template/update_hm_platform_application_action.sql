--liquibase formatted sql

--changeset postgres:update_hm_platform_application_action context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-736 Update platform application action and remove harvest_manager_v2 record



UPDATE 
	platform.application_action
SET
	module = 'harvestManager',
	controller = 'occurrence-selection',
	action = 'index'
WHERE
	application_id = (
		SELECT id 
		FROM platform.application 
		WHERE abbrev = 'HARVEST_MANAGER'
	)
;



--rollback UPDATE 
--rollback 	platform.application_action
--rollback SET
--rollback 	module = 'harvestManager',
--rollback 	controller = 'occurrence-selection',
--rollback 	action = 'index'
--rollback WHERE
--rollback 	application_id = (
--rollback 		SELECT id 
--rollback 		FROM platform.application 
--rollback 		WHERE abbrev = 'HARVEST_MANAGER'
--rollback 	)
--rollback ;
