--liquibase formatted sql

--changeset postgres:update_hm_platform_application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-736 Update platform application action and remove harvest_manager_v2 record



UPDATE 
	platform.application
SET
	icon = 'agriculture'
WHERE
	abbrev = 'HARVEST_MANAGER';



--rollback UPDATE 
--rollback 	platform.application
--rollback SET
--rollback 	icon = 'agriculture'
--rollback WHERE
--rollback 	abbrev = 'HARVEST_MANAGER';
