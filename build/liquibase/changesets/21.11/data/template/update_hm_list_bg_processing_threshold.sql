--liquibase formatted sql

--changeset postgres:update_config_set_list_preview_and_creation_thresholds context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-833 Update config set list preview and creation thresholds



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "previewList": {
                "size": "100",
                "description": "Preview germplasm, seed, or package list"
            },
            "createList": {
                "size": "10000",
                "description": "Create new germplasm, seed, or package list"
            },
            "deleteSeeds": {
                "size": "200",
                "description": "Delete seeds of plots or crosses"
            },
            "deletePackages": {
                "size": "200",
                "description": "Delete packages and related records"
            },
            "deleteHarvestData": {
                "size": "2000",
                "description": "Delete harvest data of plots or crosses"
            },
            "updateHarvestData": {
                "size": "5000",
                "description": "Update harvest data of plots or crosses"
            }
        }
    ' 
WHERE 
    abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "createList": {
--rollback                 "size": "500",
--rollback                 "description": "Create new germplasm, seed, or package list"
--rollback             },
--rollback             "deleteSeeds": {
--rollback                 "size": "200",
--rollback                 "description": "Delete seeds of plots or crosses"
--rollback             },
--rollback             "deletePackages": {
--rollback                 "size": "200",
--rollback                 "description": "Delete packages and related records"
--rollback             },
--rollback             "deleteHarvestData": {
--rollback                 "size": "2000",
--rollback                 "description": "Delete harvest data of plots or crosses"
--rollback             },
--rollback             "updateHarvestData": {
--rollback                 "size": "5000",
--rollback                 "description": "Update harvest data of plots or crosses"
--rollback             }
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HARVEST_MANAGER_BG_PROCESSING_THRESHOLD';