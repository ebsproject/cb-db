--liquibase formatted sql

--changeset postgres:add_repeated_check_scale_value_to_entry_role_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-837 Add repeated check scale value to ENTRY_ROLE variable



-- add scale value
INSERT INTO master.scale_value
    (scale_id, abbrev, value, description, display_name, order_number, scale_value_status, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.description,
    scalval.display_name,
    scalval.order_number,
    scalval.scale_value_status,
    scalval.creator_id
FROM
    master.variable AS var
    JOIN LATERAL (
        SELECT
            t.*,
            (SELECT max(order_number) + 1 FROM master.scale_value WHERE scale_id = var.scale_id AND is_void = FALSE) AS order_number
        FROM 
            (
                VALUES
                (
                    'ENTRY_ROLE_REPEATED_CHECK',
                    'repeated check',
                    'repeated check',
                    'repeated check',
                    NULL,
                    1
                )
            ) AS t (
                abbrev, value, description, display_name, scale_value_status, creator_id
            )
    ) AS scalval
        ON TRUE
WHERE
    var.abbrev = 'ENTRY_ROLE'
;



--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('ENTRY_ROLE_REPEATED_CHECK')
--rollback ;
