--liquibase formatted sql

--changeset postgres:add_augmented_design_scale_value_design_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-813 Add Augmented Design scale value to DESIGN variable



-- add scale value
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        (
            'DESIGN_AUGMENTED_DESIGN',
            'Augmented Design',
            (SELECT max(order_number)+1 FROM master.scale_value WHERE scale_id = (SELECT scale_id FROM master.variable WHERE abbrev = 'DESIGN')),
            'Augmented Design',
            'Augmented Design',
            1,
            'show'
        )
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'DESIGN'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('DESIGN_AUGMENTED_DESIGN')
--rollback ;



--changeset postgres:update_p_rep_scale_value_of_design_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-813 Update P-REP scale value of DESIGN variable



-- update scale value
UPDATE
    master.scale_value
SET
    value = 'Partially Replicated',
    description = 'Partially Replicated'
WHERE
    abbrev = 'DESIGN_P_REP'
;



-- revert changes
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'P-REP',
--rollback     description = 'Partially replicated'
--rollback WHERE
--rollback     abbrev = 'DESIGN_P_REP'
--rollback ;
