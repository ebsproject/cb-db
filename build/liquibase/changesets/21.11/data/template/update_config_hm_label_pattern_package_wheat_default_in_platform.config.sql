--liquibase formatted sql

--changeset postgres:update_config_hm_label_pattern_package_wheat_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-743 Update config HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT in platform.config

UPDATE 
    platform.config 
SET 
    config_value = 
    '
       {
            "PLOT": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "harvestedPackagePrefix",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "max_digits": 4,
                                "leading_zero": "yes",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "field",
                                "entity": "cross",
                                "field_name": "harvestedPackagePrefix",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "max_digits": 4,
                                "leading_zero": "yes",
                                "order_number": 2
                            }
                        ]
                    }
                }
            },
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state/germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            }
                        ]
                    }
                }
            }
        }
    ' 
WHERE 
    abbrev = 'HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT';

-- rollback UPDATE 
-- rollback     platform.config 
-- rollback SET 
-- rollback     config_value = 
-- rollback     '
-- rollback        {
-- rollback             "harvest_mode": {
-- rollback                 "cross_method": {
-- rollback                     "germplasm_state/germplasm_type": {
-- rollback                         "harvest_method": [
-- rollback                             {
-- rollback                                 "type": "free-text",
-- rollback                                 "value": "ABC",
-- rollback                                 "order_number": 0
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "field",
-- rollback                                 "entity": "<entity>",
-- rollback                                 "field_name": "<field_name>",
-- rollback                                 "order_number": 1
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "delimiter",
-- rollback                                 "value": "-",
-- rollback                                 "order_number": 1
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "counter",
-- rollback                                 "order_number": 3
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "db-sequence",
-- rollback                                 "schema": "<schema>",
-- rollback                                 "sequence_name": "<sequence_name>",
-- rollback                                 "order_number": 4
-- rollback                             }
-- rollback                         ]
-- rollback                     }
-- rollback                 }
-- rollback             },
-- rollback             "PLOT": {
-- rollback                 "default": {
-- rollback                     "default" : {
-- rollback                         "default" : [
-- rollback                             {
-- rollback                                 "type" : "field",
-- rollback                                 "entity" : "seed",
-- rollback                                 "field_name" : "seedName",
-- rollback                                 "order_number":0
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "delimiter",
-- rollback                                 "value": "-",
-- rollback                                 "order_number": 1
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "free-text",
-- rollback                                 "value": "P",
-- rollback                                 "order_number": 2
-- rollback                             },
-- rollback                             {
-- rollback                                 "type" : "field",
-- rollback                                 "entity" : "plot",
-- rollback                                 "field_name" : "plotNumber",
-- rollback                                 "order_number": 3
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "delimiter",
-- rollback                                 "value": "-",
-- rollback                                 "order_number": 4
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "free-text",
-- rollback                                 "value": "PK",
-- rollback                                 "order_number": 5
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "counter",
-- rollback                                 "leading_zero" : "yes",
-- rollback                                 "max_digits" : 3,
-- rollback                                 "order_number": 2
-- rollback                             }
-- rollback                         ]
-- rollback                     }
-- rollback                 }
-- rollback             },
-- rollback             "CROSS": {
-- rollback                 "default": {
-- rollback                     "default": {
-- rollback                         "default": [
-- rollback                             {
-- rollback                                 "type": "free-text",
-- rollback                                 "value": "",
-- rollback                                 "order_number": 0
-- rollback                             }
-- rollback                         ]
-- rollback                     }
-- rollback                 },
-- rollback                 "single cross": {
-- rollback                     "default" : {
-- rollback                         "default" : [
-- rollback                             {
-- rollback                                 "type": "field",
-- rollback                                 "entity": "seed",
-- rollback                                 "field_name": "seedName",
-- rollback                                 "order_number": 0
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "delimiter",
-- rollback                                 "value": "-",
-- rollback                                 "order_number": 1
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "free-text",
-- rollback                                 "value": "PK",
-- rollback                                 "order_number": 2
-- rollback                             },
-- rollback                             {
-- rollback                                 "type": "counter",
-- rollback                                 "leading_zero" : "yes",
-- rollback                                 "max_digits" : 3,
-- rollback                                 "order_number": 3
-- rollback                             }
-- rollback                         ]
-- rollback                     }
-- rollback                 }
-- rollback             }
-- rollback         }
-- rollback     ' 
-- rollback WHERE 
-- rollback     abbrev = 'HM_LABEL_PATTERN_PACKAGE_WHEAT_DEFAULT';