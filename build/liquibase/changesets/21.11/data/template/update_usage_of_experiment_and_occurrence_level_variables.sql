--liquibase formatted sql

--changeset postgres:update_usage_of_experiment_and_occurrence_level_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-816 Update usage of experiment and occurrence level variables



UPDATE
    master.variable
SET
    usage = 'experiment_manager'
WHERE
    abbrev
IN 
    (
        'ECOSYSTEM',
        'MARKET_SEGMENT',
        'REMARKS',
        'CONTCT_PERSON_CONT',
        'PLANTING_DATE',
        'GEOSPATIAL_COORDS'
    )
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     usage = 'experiment'
--rollback WHERE
--rollback     abbrev
--rollback IN 
--rollback     (
--rollback         'GEOSPATIAL_COORDS',
--rollback         'MARKET_SEGMENT',
--rollback         'PLANTING_DATE'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     usage = 'study'
--rollback WHERE
--rollback     abbrev
--rollback IN 
--rollback     (
--rollback         'CONTCT_PERSON_CONT',
--rollback         'ECOSYSTEM',
--rollback         'REMARKS'
--rollback     )
--rollback ;