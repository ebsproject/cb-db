--liquibase formatted sql

--changeset postgres:delete_hmv2_records context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-736 Update platform application action and remove harvest_manager_v2 record



DELETE FROM platform.application_action 
WHERE application_id = (
		SELECT id
		FROM platform.application
		WHERE abbrev='HARVEST_MANAGER_V2'
	)
;

--rollback DELETE FROM platform.application_action 
--rollback WHERE application_id = (
--rollback 		SELECT id
--rollback 		FROM platform.application
--rollback 		WHERE abbrev='HARVEST_MANAGER_V2'
--rollback 	)
--rollback ;



DELETE FROM platform.application WHERE abbrev='HARVEST_MANAGER_V2';
--rollback DELETE FROM platform.application WHERE abbrev='HARVEST_MANAGER_V2';