--liquibase formatted sql

--changeset postgres:copy_ecosystem_from_occurrence_to_experiment_data_level context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-815 Copy ECOSYSTEM from occurrence data to experiment data level



-- copy ECOSYSTEM data
INSERT INTO
    experiment.experiment_data (
        experiment_id,
        variable_id,
        data_value,
        data_qc_code,
        creator_id,
        notes
    )
SELECT
    DISTINCT ON (
        expt.id
    )
    expt.id AS experiment_id,
    var.id AS variable_id,
    occdata.data_value,
    occdata.data_qc_code,
    occdata.creator_id,
    'DB-815' AS notes
FROM
    experiment.experiment AS expt
    JOIN experiment.occurrence AS occ
        ON occ.experiment_id = expt.id
    JOIN experiment.occurrence_data AS occdata
        ON occdata.occurrence_id = occ.id
    JOIN master.variable AS var
        ON var.id = occdata.variable_id
    LEFT JOIN experiment.experiment_data AS exptdata
        ON exptdata.experiment_id = expt.id
        AND var.id = exptdata.variable_id
WHERE
    var.abbrev = 'ECOSYSTEM'
    AND exptdata.id IS NULL
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment_data AS exptdata
--rollback USING
--rollback     master.variable AS var
--rollback WHERE
--rollback     var.id = exptdata.variable_id
--rollback     AND var.abbrev = 'ECOSYSTEM'
--rollback     AND exptdata.notes LIKE 'DB-815%'
--rollback ;
