--liquibase formatted sql

--changeset postgres:copy_trait_protocol_lists_from_experiment_to_occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-834 Copy trait protocol lists from experiment to occurrence data level



-- insert lists, list members, and occurence mapping
WITH t_exptdata AS (
    SELECT
        DISTINCT ON (
            exptdata.experiment_id
        )
        exptdata.experiment_id,
        exptdata.variable_id,
        exptdata.data_value::integer,
        exptdata.data_qc_code
    FROM
        experiment.experiment_data AS exptdata
        JOIN master.variable AS var
            ON var.id = exptdata.variable_id
    WHERE
        var.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
        AND exptdata.is_void = FALSE
        AND var.is_void = FALSE
), t_occ_list AS (
    INSERT INTO
        platform.list (
            abbrev,
            name,
            display_name,
            entity_id,
            type,
            list_sub_type,
            list_usage,
            status,
            is_active,
            access_data,
            creator_id,
            remarks,
            notes
        )
    SELECT
        'TRAIT_PROTOCOL_' || occ.occurrence_code AS abbrev, -- srclist.abbrev, -- TRAIT_PROTOCOL_EXP00000311
        occ.occurrence_name || ' Trait Protocol (' || occ.occurrence_code || ')' AS name, -- srclist.name, -- IRSEA-OYT-2018-DS-062 Trait Protocol (EXP00000311)
        occ.occurrence_name || ' Trait Protocol (' || occ.occurrence_code || ')' AS display_name, -- srclist.display_name -- IRSEA-OYT-2018-DS-062 Trait Protocol (EXP00000311)
        srclist.entity_id, -- 18 (TRAIT)
        srclist.type, -- trait protocol
        srclist.list_sub_type,
        srclist.list_usage, -- working list
        srclist.status, -- created
        srclist.is_active, -- true
        srclist.access_data,
        srclist.creator_id,
        srclist.id || ',' || occ.id AS remarks,
        'DB-834' AS notes
    FROM
        platform.list AS srclist
        JOIN t_exptdata AS exptdata
            ON exptdata.data_value = srclist.id
        JOIN experiment.occurrence AS occ
            ON occ.experiment_id = exptdata.experiment_id
        LEFT JOIN experiment.occurrence_data AS occdata
            ON occdata.occurrence_id = occ.id
            AND occdata.variable_id = exptdata.variable_id
    WHERE
        srclist.type = 'trait protocol'
        AND occdata.id IS NULL
        AND srclist.is_void = FALSE
        AND occ.is_void = FALSE
    RETURNING
        id AS list_id,
        (string_to_array(remarks, ','))[1]::integer AS source_list_id,
        (string_to_array(remarks, ','))[2]::integer AS occurrence_id,
        creator_id
), t_occdata AS (
    INSERT INTO
        experiment.occurrence_data (
            occurrence_id,
            variable_id,
            data_value,
            data_qc_code,
            creator_id,
            notes
        )
    SELECT
        occlist.occurrence_id,
        var.id AS variable_id,
        occlist.list_id AS data_value,
        'G' AS data_qc_code,
        occlist.creator_id,
        'DB-834' AS notes
    FROM
        t_occ_list AS occlist
        JOIN master.variable AS var
            ON var.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
    WHERE
        var.is_void = FALSE
), t_occ_listmem AS (
    INSERT INTO
        platform.list_member (
            list_id,
            data_id,
            order_number,
            display_value,
            creator_id,
            is_active,
            notes
        )
    SELECT
        occlist.list_id,
        listmem.data_id,
        listmem.order_number,
        listmem.display_value,
        listmem.creator_id,
        listmem.is_active,
        'DB-834' AS notes
    FROM
        platform.list_member AS listmem
        JOIN t_occ_list AS occlist
            ON occlist.source_list_id = listmem.list_id
    WHERE
        listmem.is_void = FALSE
    RETURNING
        *
)
SELECT
    count(1)
FROM
    t_occ_listmem AS t
;

-- empty remarks
UPDATE
    platform.list
SET
    remarks = NULL
WHERE
    notes LIKE 'DB-834%'
;



-- revert changes
--rollback DELETE FROM platform.list_member WHERE notes LIKE 'DB-834%';
--rollback DELETE FROM platform.list WHERE notes LIKE 'DB-834%';
--rollback DELETE FROM experiment.occurrence_data WHERE notes LIKE 'DB-834%';
