--liquibase formatted sql

--changeset postgres:add_manage_experiment_data_config_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-810 Add manage experiment data configuration for IRSEA program



INSERT INTO 
	platform.config(abbrev, name, config_value, rank, usage, creator_id)
VALUES (
	'MANAGE_EXPERIMENT_DATA_IRSEA', 
	'Manage Experiment data for IRSEA program', 
	'{
		"Name": "Configuration for manage experiment-level data for ISREA program",
		"Values": [{
			"BREEDING_TRIAL_DATA_PROCESS": [{
					"target_column": "ecosystem",
					"variable_type": "metadata",
					"variable_abbrev": "ECOSYSTEM"
				}
			]
		}]
	}'::json, 
	1, 
	'experiment_manager', 
	1
);


--rollback DELETE FROM platform.config WHERE abbrev = 'MANAGE_EXPERIMENT_DATA_IRSEA';



--changeset postgres:link_config_to_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-810 Link configuration to program



INSERT INTO 
	tenant.program_config (program_id, config_id, creator_id)
SELECT
	(SELECT id FROM tenant.program WHERE program_code = 'IRSEA'),
	id,
	1
FROM
	platform.config
WHERE
	abbrev = 'MANAGE_EXPERIMENT_DATA_IRSEA';


--rollback DELETE FROM tenant.program_config WHERE config_id = (SELECT id FROM platform.config WHERE abbrev = 'MANAGE_EXPERIMENT_DATA_IRSEA');
