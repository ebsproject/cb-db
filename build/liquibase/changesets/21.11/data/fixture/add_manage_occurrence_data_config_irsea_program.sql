--liquibase formatted sql

--changeset postgres:add_manage_occurrence_data_config_irsea_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-810 Add manage occurrence data configuration for IRSEA program



INSERT INTO 
	platform.config(abbrev, name, config_value, rank, usage, creator_id)
VALUES (
	'MANAGE_OCCURRENCE_DATA_IRSEA', 
	'Manage Occurrence data for IRSEA program', 
	'{
		"Name": "Default configuration for manage occurrence-level data",
		"Values": [{
			"BREEDING_TRIAL_DATA_PROCESS": [{
					"required": "required",
					"target_column": "occurrenceName",
					"variable_type": "identification",
					"variable_abbrev": "OCCURRENCE_NAME"
				},
				{
					"required": "required",
					"target_column": "siteDbId",
					"secondary_target_column": "geospatialObjectDbId",
					"target_value": "geospatialObjectName",
					"api_resource_method": "POST",
					"api_resource_endpoint": "geospatial-objects-search",
					"api_resource_filter": {
						"geospatialObjectType": "site"
					},
					"variable_type": "identification",
					"variable_abbrev": "SITE"
				},
				{
					"target_column": "fieldDbId",
					"secondary_target_column": "geospatialObjectDbId",
					"target_value": "scaleName",
					"api_resource_method": "POST",
					"api_resource_endpoint": "geospatial-objects-search",
					"api_resource_filter": {
						"geospatialObjectType": "field"
					},
					"variable_type": "identification",
					"variable_abbrev": "FIELD"
				},
				{
					"target_column": "description",
					"variable_type": "identification",
					"variable_abbrev": "DESCRIPTION"
				},
				{
					"allow_new_val": true,
					"target_column": "contactPerson",
					"secondary_target_column": "personDbId",
					"target_value": "personName",
					"api_resource_sort": "sort=personName",
					"api_resource_method": "GET",
					"api_resource_endpoint": "persons",
					"variable_type": "metadata",
					"variable_abbrev": "CONTCT_PERSON_CONT"
				},
				{
					"target_column": "plantingDate",
					"variable_type": "metadata",
					"variable_abbrev": "PLANTING_DATE"
				}
			]
		}]
	}'::json, 
	1, 
	'experiment_manager', 
	1
);


--rollback DELETE FROM platform.config WHERE abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA';



--changeset postgres:link_config_to_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-810 Link configuration to program



INSERT INTO 
	tenant.program_config (program_id, config_id, creator_id)
SELECT
	(SELECT id FROM tenant.program WHERE program_code = 'IRSEA'),
	id,
	1
FROM
	platform.config
WHERE
	abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA';


--rollback DELETE FROM tenant.program_config WHERE config_id = (SELECT id FROM platform.config WHERE abbrev = 'MANAGE_OCCURRENCE_DATA_IRSEA');
