--liquibase formatted sql

--changeset postgres:delete_master_template_data context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Delete master template data



-- delete template data
DELETE FROM master.cross_method;
DELETE FROM master.entity_record;
DELETE FROM master.entity;
DELETE FROM master.variable_relation_value;
DELETE FROM master.variable_relation;



--rollback INSERT INTO master.cross_method (id,abbrev,"name",description,display_name,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,event_log) VALUES 
--rollback (1,'SC','single cross','Single cross','Single cross',NULL,'2014-03-25 11:40:04.214167',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (2,'BC','backcross','Backcross','Backcross',NULL,'2014-03-25 11:40:04.214167',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (3,'CCX','complex cross','Complex Cross','Complex cross',NULL,'2014-03-25 11:40:04.214167',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (4,'C3W','three-way cross','Three-way cross','Three-way cross',NULL,'2014-03-25 11:40:04.214167',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (5,'C2W','double cross','Double cross','Double cross',NULL,'2014-03-25 11:40:04.214167',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (7,'VHY','hybrid formation','Hybrid formation','Hybrid formation',NULL,'2014-07-09 12:02:51.095853',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (8,'SLF','selfing','Selfing a single plant or population','Selfing',NULL,'2019-12-09 03:13:04.016439',1,'2020-04-18 20:32:12.662273',1,NULL,false,'[{"actor_id": "1", "new_data": {"id": 8, "name": "selfing", "notes": null, "abbrev": "SLF", "is_void": false, "remarks": null, "creator_id": 1, "description": "Selfing a single plant or population", "modifier_id": null, "display_name": "Selfing", "creation_timestamp": "2019-12-09 03:13:04.016439", "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2019-12-09 03:13:04.016439+00", "transaction_id": "1173009"}, {"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]')
--rollback ;

--rollback INSERT INTO master.entity (id,abbrev,"name",description,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,target_variable_id,entity_model,display_name,event_log) VALUES 
--rollback (1,'STUDY','study','Study',NULL,'2014-11-12 01:38:53.191428',1,'2015-01-22 01:52:00.154314',1,NULL,false,904,'Study',NULL,NULL),
--rollback (2,'ENTRY','entry','Entry',NULL,'2014-11-12 01:38:53.191428',1,'2015-01-22 01:52:00.154314',1,NULL,false,938,'Entry',NULL,NULL),
--rollback (3,'PLOT','plot','Plot',NULL,'2014-11-12 01:38:53.191428',1,'2015-01-22 01:52:00.154314',1,NULL,false,1029,'Plot',NULL,NULL),
--rollback (4,'SEED_STORAGE','seed storage','Seed storage',NULL,'2014-11-12 01:38:53.191428',1,'2015-01-22 01:52:00.154314',1,NULL,false,936,'SeedStorage',NULL,NULL),
--rollback (5,'PRODUCT','product','product',NULL,'2014-11-12 01:38:53.191428',1,'2015-01-22 01:52:00.154314',1,NULL,false,221,'Product',NULL,NULL),
--rollback (6,'LOCATION','location','location',NULL,'2014-11-22 03:29:37.774423',1,'2015-01-22 01:52:00.154314',1,NULL,false,NULL,'Location',NULL,NULL),
--rollback (7,'SERVICE','service','Service',NULL,'2015-04-17 11:50:32.130779',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'Service','Service','[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (8,'EXPERIMENT','experiment',NULL,NULL,'2019-08-13 12:31:06.792525',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'Experiment',NULL,'[{"actor_id": "1", "new_data": {"id": 8, "name": "experiment", "notes": null, "abbrev": "EXPERIMENT", "is_void": false, "remarks": null, "creator_id": 1, "description": null, "modifier_id": null, "display_name": null, "entity_model": "Experiment", "creation_timestamp": "2019-08-13 12:31:06.792525", "target_variable_id": null, "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2019-08-13 12:31:06.792525+00", "transaction_id": "732310"}, {"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]')
--rollback ;

--rollback INSERT INTO master.entity_record (id,entity_id,record_id,order_number,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,variable_set_id,issue_key,issue_type) VALUES 
--rollback (1,1,11,1,'2014-11-12 01:38:59.153575',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (2,2,12,1,'2014-11-12 01:39:05.415819',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (3,3,13,1,'2014-11-12 01:39:12.640136',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (4,1,14,5,'2014-11-12 01:39:22.337355',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (5,2,15,5,'2014-11-12 01:39:28.15942',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (6,3,16,4,'2014-11-12 01:39:34.160105',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (7,4,17,1,'2014-11-12 01:39:44.336854',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (8,5,18,1,'2014-11-12 01:39:53.824318',6,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (9,1,NULL,2,'2014-11-12 06:27:10.715408',6,NULL,NULL,NULL,false,362,NULL,NULL),
--rollback (10,1,NULL,3,'2014-11-12 06:32:57.834879',6,NULL,NULL,NULL,false,363,NULL,NULL),
--rollback (13,1,21,6,'2014-11-18 16:57:51.928376',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (14,5,21,2,'2014-11-22 02:58:13.186861',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (15,6,22,1,'2014-11-22 05:11:35.969081',1,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (16,6,23,2,'2014-11-22 05:14:02.343642',1,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (17,6,25,3,'2014-11-22 05:14:10.499744',1,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (19,2,11,2,'2014-11-22 05:22:42.95072',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (20,2,18,3,'2014-11-22 05:22:46.063236',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (21,2,17,4,'2014-11-22 05:22:51.05466',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (23,4,28,2,'2014-11-22 09:00:32.301147',1,NULL,NULL,'',false,NULL,NULL,NULL),
--rollback (25,3,12,2,'2014-11-24 02:34:33.699695',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (26,3,11,3,'2014-11-24 02:37:18.67382',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (27,2,21,7,'2014-11-24 02:52:49.44813',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (28,3,21,6,'2014-11-24 02:52:49.44813',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (29,4,21,3,'2014-11-24 02:52:49.44813',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (30,6,21,5,'2014-11-24 02:52:49.44813',5,NULL,NULL,NULL,false,NULL,NULL,NULL),
--rollback (31,3,18,7,'2015-03-17 06:48:35.153181',5,NULL,NULL,NULL,false,NULL,NULL,NULL)
--rollback ;

--rollback INSERT INTO master.variable_relation (id,abbrev,"name",description,variable_a_id,variable_b_id,relation_type,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void) VALUES 
--rollback (1,'PLACE_HAS_SEASON_REL','place has season relation','Places can have varying growing seasons',313,315,'HAS',NULL,'2015-01-20 01:37:13.015818',1,NULL,NULL,NULL,false),
--rollback (2,'PHASE_HAS_STUDY_TYPE_REL','phase has study type relation','Breeding stages can have different study types',312,578,'HAS',NULL,'2015-01-20 01:37:13.015818',1,NULL,NULL,NULL,false),
--rollback (4,'STUDY_TYPE_HAS_PROCESS_PATH_REL','study type has process path relation','Study types have applicable process paths',578,1325,'HAS',NULL,'2015-05-06 16:40:24.498843',1,NULL,NULL,NULL,false)
--rollback ;

--rollback INSERT INTO master.variable_relation_value (id,variable_relation_id,variable_a_value,variable_b_value,variable_a_scale_value_id,variable_b_scale_value_id,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void) VALUES 
--rollback (3,1,'10001','11',NULL,NULL,NULL,'2015-01-22 01:51:48.660541',1,NULL,NULL,NULL,false),
--rollback (4,1,'10001','12',NULL,NULL,NULL,'2015-01-22 01:51:48.660541',1,NULL,NULL,NULL,false),
--rollback (5,2,'105','Crossing',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (6,2,'106','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (7,2,'108','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (8,2,'109','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (9,2,'110','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (10,2,'111','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (11,2,'112','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (12,2,'113','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (13,2,'114','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (14,2,'115','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (15,2,'131','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (16,2,'104','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (17,2,'128','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (18,2,'129','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (19,2,'101','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (20,2,'116','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (21,2,'102','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (22,2,'103','Trial',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,false),
--rollback (23,2,'107','Nursery',NULL,NULL,NULL,'2015-01-27 07:55:37.21718',1,NULL,NULL,NULL,true),
--rollback (24,2,'136','Trial',NULL,NULL,NULL,'2015-02-15 10:07:48.391505',1,NULL,NULL,NULL,false),
--rollback (25,2,'107','Glasshouse',NULL,NULL,NULL,'2015-02-18 08:50:33.071927',1,NULL,NULL,NULL,true),
--rollback (26,2,'101','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (27,2,'102','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (28,2,'103','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (29,2,'104','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (30,2,'116','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (31,2,'128','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (32,2,'129','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (33,2,'136','Abiotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (34,2,'101','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (35,2,'102','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (36,2,'103','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (37,2,'104','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (38,2,'116','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (39,2,'128','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (40,2,'129','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (41,2,'136','Biotic stress screening',NULL,NULL,NULL,'2015-03-17 16:08:09.247939',1,NULL,NULL,'added by jpramos',false),
--rollback (42,2,'131','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (43,2,'115','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (44,2,'114','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (45,2,'113','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (46,2,'112','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (47,2,'111','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (48,2,'110','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (49,2,'109','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (50,2,'108','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (51,2,'106','Abiotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:10.585508',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (52,2,'131','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (53,2,'115','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (54,2,'114','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (55,2,'113','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (56,2,'112','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (57,2,'111','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (58,2,'110','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (59,2,'109','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (60,2,'108','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (61,2,'106','Biotic stress screening',NULL,NULL,NULL,'2015-09-24 11:15:12.351294',1,NULL,NULL,'added by vcalaminos RBIMS-2631',false),
--rollback (62,2,'105','Nursery',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos from document from marko2016-12-09 17:34:00.406116+08',false),
--rollback (63,2,'106','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (64,2,'108','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (65,2,'109','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (66,2,'110','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (67,2,'111','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (68,2,'112','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (69,2,'113','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (70,2,'114','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (71,2,'115','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (72,2,'105','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (73,2,'131','Glasshouse',NULL,NULL,NULL,'2016-12-09 17:34:00.406116',1,NULL,NULL,'added by jp.ramos',false),
--rollback (74,2,'138','Plant-sample',NULL,NULL,NULL,'2016-12-09 18:14:11.91165',1,NULL,NULL,'for GSL defaults',false)
--rollback ;
