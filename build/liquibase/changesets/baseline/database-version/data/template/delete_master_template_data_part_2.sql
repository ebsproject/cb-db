--liquibase formatted sql

--changeset postgres:delete_master_template_data_part_2 context:template splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Delete master template data (part 2)



-- delete template data
DELETE FROM master.facility_type;
DELETE FROM master.phase;
DELETE FROM master.user_metadata;
DELETE FROM master.user;
DELETE FROM master.role;



--rollback INSERT INTO master.facility_type (id,abbrev,"name",is_mappable,is_void,creation_timestamp,creator_id,modification_timestamp,modifier_id) VALUES 
--rollback (1,'BLOCK','Block',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (2,'ROOM','Room',false,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (3,'PLOT','Plot',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (4,'SEED BED','Seed bed',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (5,'BUILDING','Building',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (6,'GREEN HOUSE','Green house',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (7,'RESERVOIR','Reservoir',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (8,'GROWTH CHAMBER','Growth chamber',false,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (9,'FARM','Farm',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (10,'SCREEN HOUSE','Screen house',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (11,'SHELF COLUMN','Shelf Column',false,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (12,'FARM HOUSE','Farm House',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (13,'SHELF','Shelf',false,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (14,'GLASS HOUSE','Glass house',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (15,'SEED WAREHOUSE','Seed Warehouse',true,false,'2016-07-20 15:22:57.541979',1,'2016-07-20 15:22:57.541979',1),
--rollback (16,'CABINET','CABINET',false,false,'2016-08-23 14:23:09.524026',1,'2016-08-23 14:23:09.524026',1),
--rollback (17,'COMPACTUS','Compactus',false,false,'2017-01-17 10:17:05.082153',1,NULL,1)
--rollback ;

--rollback INSERT INTO master.phase (id,abbrev,"name",description,display_name,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,"rank",event_log) VALUES 
--rollback (101,'OYT','Observation Yield Trial','Observation Yield Trial','OYT - Observation Yield Trial',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,14,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (102,'RYT','Replicated Yield Trial','Replicated Yield Trial','RYT - Replicated Yield Trial',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,'Inactive; For historical studies',false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (107,'RGA','RGA - Rapid Generation Advance','Rapid Generation Advance','RGA - Rapid Generation Advance','','2014-03-25 11:40:03.563471',1,'2014-05-22 11:57:40.477704',1,NULL,false,12,NULL),
--rollback (140,'QC','quality control',NULL,'Quality Control',NULL,'2018-06-14 11:36:03.344959',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (141,'MAS','marker assisted selection',NULL,'Marker Assisted Selection',NULL,'2018-06-14 11:36:03.344959',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (142,'GS','genomic selection',NULL,'Genomic Selection',NULL,'2018-06-14 11:36:03.344959',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (103,'AYT','Advanced Yield Trial','Advanced Yield Trial','AYT - Advanced Yield Trial',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,16,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (104,'MET0','MET0 - Multi-Environment Yield Trial','MET0 - Multi-Environment Yield Trial','MET0 - Multi-Environment Yield Trial',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,true,17,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": 12}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (105,'HB','Hybridization','Hybridization','HB - Hybridization',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,1,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (106,'F1','F1','F1','F1',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,2,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (108,'F2','F2','F2','F2',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,3,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (109,'F3','F3','F3','F3',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,4,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (110,'F4','F4','F4','F4',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,5,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (111,'F5','F5','F5','F5',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,6,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (112,'F6','F6','F6','F6',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,7,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (113,'F7','F7','F7','F7',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,8,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (114,'F8','F8','F8','F8',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,9,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (115,'F9','F9','F9','F9',NULL,'2014-03-25 11:40:03.563471',1,'2020-04-18 20:32:12.662273',1,NULL,false,10,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (116,'PYT','Preliminary yield trial','Preliminary yield trial','PYT - Preliminary yield trial','','2014-04-29 08:08:28.484917',1,'2020-04-18 20:32:12.662273',1,NULL,false,15,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (128,'MET1','MET1 - Multi-Environment Yield Trial','MET0 - Multi-Environment Yield Trial','MET1 - Multi-Environment Yield Trial',NULL,'2014-05-22 11:34:57.623437',1,'2020-04-18 20:32:12.662273',1,NULL,false,18,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (129,'MET2','MET2 - Multi-Environment Yield Trial','MET2 - Multi-Environment Yield Trial','MET2 - Multi-Environment Yield Trial',NULL,'2014-05-22 11:35:11.993307',1,'2020-04-18 20:32:12.662273',1,NULL,false,19,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (130,'BRE','Breeders Seed Production','The step in the breeding process that produces Breeders Seeds.','Breeders Seed Production',NULL,'2014-05-22 11:36:24.642386',1,'2014-09-25 05:55:28.880101',1,NULL,false,NULL,NULL),
--rollback (131,'PN','Pedigree Nursery','Pedigree Nursery','PN - Pedigree Nursery',NULL,'2014-05-26 17:28:56.613877',1,'2020-04-18 20:32:12.662273',1,NULL,false,11,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (132,'AGR','Agronomical Traits',NULL,'Agronomical Traits',NULL,'2014-06-12 16:48:36.087206',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (133,'MAP','Map','The step in the breeding process that produces mapping populations','Map','Added in September 2014 resulting to curation of existing studies.','2014-09-25 05:46:14.611051',1,'2014-09-25 05:53:21.612906',1,NULL,false,NULL,NULL),
--rollback (135,'SEM','Seed Increase','Seed Increase','Seed Increase',NULL,'2014-10-15 01:01:12.375599',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (136,'IYT','Initial Yield Trial','Initial Yield Trial','IYT - Initial Yield Trial',NULL,'2015-01-29 08:15:50.631913',1,'2020-04-18 20:32:12.662273',1,NULL,false,13,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (137,'CRS','Crosslist','Studies that are mapped in cross list','Crosslist',NULL,'2015-11-02 13:54:18.284541',1,'2020-04-18 20:32:12.662273',1,NULL,false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]'),
--rollback (138,'NA','Not applicable','Not applicable','NA',NULL,'2016-12-12 16:17:36.060508',1,'2020-04-18 20:32:12.662273',1,'created for genotypic studies',false,NULL,'[{"actor_id": "1", "new_data": {"modifier_id": 1}, "row_data": {"modifier_id": null}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 20:32:12.662273+08", "transaction_id": "1795500"}]')
--rollback ;

--rollback INSERT INTO master."user" (id,email,username,user_type,status,last_name,first_name,middle_name,display_name,salutation,valid_start_date,valid_end_date,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,is_person,uuid,"document",record_uuid,event_log) VALUES 
--rollback (0,'postgres','postgres',0,1,'Postgres','Postgres','Postgres','Postgres, Postgres',NULL,NULL,'2020-01-01','','2014-05-05 16:34:01',1,NULL,NULL,NULL,true,false,'6f68db36-cf91-4a6a-bf64-c7d9775a6826','''postgr'':1A','2c804651-8787-408d-9f2b-2c4bcaf52b49',NULL),
--rollback (1,'bims.irri@gmail.com','bims.irri',1,1,'Irri','Bims',NULL,'Irri, Bims',NULL,NULL,'2020-01-29',NULL,'2014-03-25 11:40:02.749186',1,NULL,NULL,NULL,false,false,'0e5543c2-ebf6-4da9-a820-7ddf7d1ffc5e','''bim'':1C ''bims.irri'':1A ''bims.irri@gmail.com'':1B ''irri'':1C','be92713a-01e6-43be-966e-bf4340c445ab',NULL)
--rollback ;

--rollback INSERT INTO master.user_metadata (id,user_id,variable_id,value,remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,event_log) VALUES 
--rollback (2,1,582,'2',NULL,'2020-04-18 17:06:36.979757',1,'2020-04-18 18:51:24.607718',NULL,NULL,false,'[{"actor_id": "1", "new_data": {"id": 2, "notes": null, "value": 1, "is_void": false, "remarks": null, "user_id": 1, "creator_id": 1, "modifier_id": null, "variable_id": 582, "creation_timestamp": "2020-04-18 17:06:36.979757", "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2020-04-18 17:06:36.979757+08", "transaction_id": "1795069"}, {"actor_id": "0", "new_data": {"value": 2}, "row_data": {"value": 1}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 18:51:24.607718+08", "transaction_id": "1795472"}]'),
--rollback (1,0,582,'1',NULL,'2020-04-18 17:06:36.979757',1,'2020-04-18 22:38:57.898275',NULL,NULL,false,'[{"actor_id": "1", "new_data": {"id": 1, "notes": null, "value": 1, "is_void": false, "remarks": null, "user_id": 0, "creator_id": 1, "modifier_id": null, "variable_id": 582, "creation_timestamp": "2020-04-18 17:06:36.979757", "modification_timestamp": null}, "row_data": null, "action_type": "INSERT", "log_timestamp": "2020-04-18 17:06:36.979757+08", "transaction_id": "1795069"}, {"actor_id": "0", "new_data": {"value": 2}, "row_data": {"value": 1}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 17:10:13.395987+08", "transaction_id": "1795071"}, {"actor_id": "0", "new_data": {"value": 1}, "row_data": {"value": 2}, "action_type": "UPDATE", "log_timestamp": "2020-04-18 22:38:57.898275+08", "transaction_id": "1795537"}]')
--rollback ;

--rollback INSERT INTO master."role" (id,abbrev,"name",description,display_name,"rank",remarks,creation_timestamp,creator_id,modification_timestamp,modifier_id,notes,is_void,record_uuid,event_log) VALUES 
--rollback (1,'ADMIN','administrator','Authorized user who has full access to the entire system','Administrator',1,NULL,'2014-03-25 11:40:04.311223',1,NULL,NULL,NULL,false,'66552ca0-cb63-4291-824b-488c76cfa406',NULL),
--rollback (14,'TM','member','Executes tasks and steps
--rollback ','Team Member',6,NULL,'2014-03-25 11:40:04.311223',1,NULL,NULL,NULL,false,'f5522d16-bc0e-416f-af5a-0ae386d168af',NULL),
--rollback (15,'TL','Team Leader','- Is responsible for management of a Process (financial and operational lead)
--rollback - Is responsible of the development and improvement of a Process
--rollback - Has user rights to all the data and data tools of a Process
--rollback - Has a profound understanding of the Process, Sub-Processes, Activities and technical steps within a Activity (Tasks)
--rollback ','Team Leader',1,NULL,'2014-03-25 11:40:04.311223',1,NULL,NULL,NULL,false,'05baac68-a34d-441e-bc3d-9f139399d78f',NULL),
--rollback (16,'ETM','experienced team member','- Is responsible for executing Process'' daily Activities, Tasks and Steps
--rollback - Has a good understanding of Process Activities and Tasks
--rollback ','Experienced Team Member',1,NULL,'2014-03-25 11:40:04.311223',1,NULL,NULL,NULL,false,'d8358d5a-9f44-4cf1-939e-98f880df4b5a',NULL),
--rollback (18,'DM','data manager','The role for data admin.','Data Manager',7,NULL,'2014-03-25 11:40:04.311223',1,NULL,NULL,NULL,false,'819ae255-9385-4833-bf97-30b668f925b7',NULL),
--rollback (21,'B4R_USER','B4R User','Default role assigned to all Breeding4Rice users','Breeding4Rice User',1,NULL,'2015-09-21 08:57:47.128106',1,NULL,NULL,NULL,false,'33fe204d-0294-4037-820d-338877018b2e',NULL),
--rollback (25,'DATA_OWNER','data owner','Data owners are users who possess the ownership of a record. As a default value, they are the creators of the record, unless explicitly specified.
--rollback 
--rollback - select: view information of own record
--rollback - insert: add information to own record
--rollback - update: change existing information of own record
--rollback - void: void existing information of own record
--rollback - authorize: add, change, and remove access of users, programs or teams to own record','Data Owner',-1,NULL,'2019-06-28 02:27:01.501694',1,NULL,NULL,NULL,false,'4f1883c1-e6c4-45c8-9afd-230c08496fb2',NULL),
--rollback (26,'DATA_PRODUCER','data producer','Data producer
--rollback 
--rollback Data producers are users who are granted by the data owner with a limited set of privileges to their records. They are mainly responsible in contributing additional details to the records or modifying any of its existing information.
--rollback 
--rollback - view: view information of a record
--rollback - add: add information to a record
--rollback - modify: change existing information of a record','Data Producer',-1,NULL,'2019-06-28 02:27:01.501694',1,NULL,NULL,NULL,false,'6c1eb2c6-52a3-417b-ab76-773cdea34df1',NULL),
--rollback (27,'DATA_CONSUMER','data consumer','Data consumers have the basic access to a record and that is only to view its information.
--rollback 
--rollback - view: view information of record','Data Consumer',-1,NULL,'2019-06-28 02:27:01.501694',1,NULL,NULL,NULL,false,'1d2a6590-574b-48ff-9271-349219346456',NULL)
--rollback ;