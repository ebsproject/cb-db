--liquibase formatted sql

--changeset postgres:clean_warehouse_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean warehouse schema



-- drop constraints
ALTER TABLE warehouse.entry DROP CONSTRAINT entry_study_id_fkey;
ALTER TABLE warehouse.plot DROP CONSTRAINT plot_entry_id_fkey;
ALTER TABLE warehouse.plot DROP CONSTRAINT plot_study_id_fkey;
ALTER TABLE warehouse.subplot DROP CONSTRAINT subplot_entry_id_fkey;
ALTER TABLE warehouse.subplot DROP CONSTRAINT subplot_plot_id_fkey;
ALTER TABLE warehouse.subplot DROP CONSTRAINT subplot_study_id_fkey;
ALTER TABLE warehouse_terminal.study DROP CONSTRAINT study_warehouse_study_id_fkey;


-- drop tables
DROP TABLE warehouse.subplot;
DROP TABLE warehouse.plot;
DROP TABLE warehouse.entry;
DROP TABLE warehouse.study;


-- drop schema
DROP SCHEMA warehouse;



--rollback CREATE SCHEMA warehouse;
--rollback 
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE warehouse.study;
--rollback 
--rollback CREATE TABLE warehouse.study (
--rollback     id serial NOT NULL,
--rollback     "key" numeric NOT NULL,
--rollback     program_id int4 NOT NULL,
--rollback     place_id int4 NOT NULL,
--rollback     phase_id int4 NOT NULL,
--rollback     "year" int4 NOT NULL,
--rollback     season_id int4 NOT NULL,
--rollback     "number" int4 NOT NULL,
--rollback     "name" varchar(256) NOT NULL,
--rollback     title varchar NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     project varchar NULL,
--rollback     evaluation_stage varchar NULL,
--rollback     experiment_name varchar NULL,
--rollback     percent_check_plots float8 NULL,
--rollback     location_reps int4 NULL,
--rollback     location_blocks int4 NULL,
--rollback     has_experiment_group bool NULL,
--rollback     crop varchar NULL,
--rollback     experiment_type varchar NULL,
--rollback     ecosystem varchar NULL,
--rollback     no_of_rows_per_entry_multiple json NULL,
--rollback     no_of_plants_per_row_multiple json NULL,
--rollback     study_status varchar NULL,
--rollback     "row" int4 NULL,
--rollback     experiment_template varchar NULL,
--rollback     experiment_code varchar NULL,
--rollback     experiment_sub_subtype varchar NULL,
--rollback     experiment_steward int4 NULL,
--rollback     sample_type varchar NULL,
--rollback     plate_layout_id int4 NULL,
--rollback     list_type varchar NULL,
--rollback     genotype_results json NULL,
--rollback     CONSTRAINT study_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT study_key_ukey UNIQUE (key),
--rollback     CONSTRAINT study_name_ukey UNIQUE (name),
--rollback     CONSTRAINT study_number_chk CHECK ((number >= 1)),
--rollback     CONSTRAINT study_program_id_place_id_phase_id_year_season_id_number_ukey UNIQUE (program_id, place_id, phase_id, year, season_id, number),
--rollback     CONSTRAINT study_year_chk CHECK (((year >= 1959) AND (year <= 9999))),
--rollback     CONSTRAINT study_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT study_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT study_phase_id_fkey FOREIGN KEY (phase_id) REFERENCES master.phase(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT study_place_id_fkey FOREIGN KEY (place_id) REFERENCES master.place(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT study_program_id_fkey FOREIGN KEY (program_id) REFERENCES master.program(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT study_season_id_fkey FOREIGN KEY (season_id) REFERENCES master.season(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX study_is_void_idx ON warehouse.study USING btree (is_void);
--rollback COMMENT ON INDEX warehouse.study_is_void_idx IS 'Index for the is_void column';
--rollback CREATE UNIQUE INDEX study_key_idx ON warehouse.study USING btree (key);
--rollback CREATE INDEX study_name_idx ON warehouse.study USING btree (name);
--rollback CREATE INDEX study_phase_id_idx ON warehouse.study USING btree (phase_id);
--rollback CREATE INDEX study_place_id_idx ON warehouse.study USING btree (place_id);
--rollback CREATE INDEX study_program_id_idx ON warehouse.study USING btree (program_id);
--rollback CREATE INDEX study_program_id_place_id_phase_id_year_season_id_number_idx ON warehouse.study USING btree (program_id, place_id, phase_id, year, season_id, number);
--rollback CREATE INDEX study_season_id_idx ON warehouse.study USING btree (season_id);
--rollback CREATE INDEX study_year_idx ON warehouse.study USING btree (year);
--rollback CREATE INDEX study_year_season_id_idx ON warehouse.study USING btree (year, season_id);
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE warehouse.entry;
--rollback 
--rollback CREATE TABLE warehouse.entry (
--rollback     id serial NOT NULL,
--rollback     "key" numeric NOT NULL,
--rollback     study_id int4 NOT NULL,
--rollback     "number" int4 NOT NULL DEFAULT 1,
--rollback     code varchar NULL,
--rollback     product_id int4 NULL,
--rollback     product_gid int4 NOT NULL,
--rollback     product_name varchar(256) NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     entry_type varchar NULL,
--rollback     entry_class varchar NULL,
--rollback     times_rep int4 NULL,
--rollback     CONSTRAINT entry_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT entry_key_ukey UNIQUE (key),
--rollback     CONSTRAINT entry_study_id_number_ukey UNIQUE (study_id, number),
--rollback     CONSTRAINT entry_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT entry_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT entry_product_id_fkey FOREIGN KEY (product_id) REFERENCES master.product(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT entry_study_id_fkey FOREIGN KEY (study_id) REFERENCES warehouse.study(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX entry_is_void_idx ON warehouse.entry USING btree (is_void);
--rollback COMMENT ON INDEX warehouse.entry_is_void_idx IS 'Index for the is_void column';
--rollback CREATE UNIQUE INDEX entry_key_idx ON warehouse.entry USING btree (key);
--rollback CREATE INDEX entry_product_gid_idx ON warehouse.entry USING btree (product_gid);
--rollback CREATE INDEX entry_product_id_idx ON warehouse.entry USING btree (product_id);
--rollback COMMENT ON INDEX warehouse.entry_product_id_idx IS 'Index for the product_id column';
--rollback CREATE INDEX entry_study_id_idx ON warehouse.entry USING btree (study_id);
--rollback COMMENT ON INDEX warehouse.entry_study_id_idx IS 'Index for the study_id column';
--rollback CREATE INDEX entry_study_id_number_idx ON warehouse.entry USING btree (study_id, number);
--rollback CREATE INDEX entry_study_id_product_id_idx ON warehouse.entry USING btree (study_id, product_id);
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE warehouse.plot;
--rollback 
--rollback CREATE TABLE warehouse.plot (
--rollback     id serial NOT NULL,
--rollback     "key" numeric NOT NULL,
--rollback     study_id int4 NOT NULL,
--rollback     entry_id int4 NOT NULL,
--rollback     rep int4 NULL,
--rollback     code varchar NULL,
--rollback     description text NULL,
--rollback     display_name varchar(256) NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     plotno int4 NOT NULL,
--rollback     qr_code int4 NULL,
--rollback     no_of_bags int4 NULL,
--rollback     CONSTRAINT plot_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT plot_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT plot_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES warehouse.entry(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT plot_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT plot_study_id_fkey FOREIGN KEY (study_id) REFERENCES warehouse.study(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX plot_entry_id_idx ON warehouse.plot USING btree (entry_id);
--rollback COMMENT ON INDEX warehouse.plot_entry_id_idx IS 'Index for the entry_id column';
--rollback CREATE INDEX plot_is_void_idx ON warehouse.plot USING btree (is_void);
--rollback COMMENT ON INDEX warehouse.plot_is_void_idx IS 'Index for the is_void column';
--rollback CREATE INDEX plot_key_idx ON warehouse.plot USING btree (key);
--rollback CREATE INDEX plot_study_id_entry_id_rep_idx ON warehouse.plot USING btree (study_id, entry_id, rep);
--rollback CREATE INDEX plot_study_id_idx ON warehouse.plot USING btree (study_id);
--rollback COMMENT ON INDEX warehouse.plot_study_id_idx IS 'Index for the study_id column';
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE warehouse.subplot;
--rollback 
--rollback CREATE TABLE warehouse.subplot (
--rollback     id serial NOT NULL,
--rollback     "key" numeric NOT NULL,
--rollback     study_id int4 NOT NULL,
--rollback     entry_id int4 NOT NULL,
--rollback     plot_id int4 NOT NULL,
--rollback     "number" int4 NOT NULL DEFAULT 1,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     subplotno int4 NOT NULL DEFAULT 1,
--rollback     CONSTRAINT subplot_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT subplot_key_ukey UNIQUE (key),
--rollback     CONSTRAINT subplot_study_id_entry_id_plot_id_number_ukey UNIQUE (study_id, entry_id, plot_id, number),
--rollback     CONSTRAINT subplot_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT subplot_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES warehouse.entry(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT subplot_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT subplot_plot_id_fkey FOREIGN KEY (plot_id) REFERENCES warehouse.plot(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT subplot_study_id_fkey FOREIGN KEY (study_id) REFERENCES warehouse.study(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX subplot_entry_id_idx ON warehouse.subplot USING btree (entry_id);
--rollback COMMENT ON INDEX warehouse.subplot_entry_id_idx IS 'Index for the entry_id column';
--rollback CREATE INDEX subplot_is_void_idx ON warehouse.subplot USING btree (is_void);
--rollback COMMENT ON INDEX warehouse.subplot_is_void_idx IS 'Index for the is_void column';
--rollback CREATE UNIQUE INDEX subplot_key_idx ON warehouse.subplot USING btree (key);
--rollback CREATE INDEX subplot_plot_id_idx ON warehouse.subplot USING btree (plot_id);
--rollback COMMENT ON INDEX warehouse.subplot_plot_id_idx IS 'Index for the plot_id column';
--rollback CREATE INDEX subplot_study_id_entry_id_plot_id_number_idx ON warehouse.subplot USING btree (study_id, entry_id, plot_id, number);
--rollback CREATE INDEX subplot_study_id_idx ON warehouse.subplot USING btree (study_id);
--rollback COMMENT ON INDEX warehouse.subplot_study_id_idx IS 'Index for the study_id column';
--rollback 
--rollback 
--rollback ALTER TABLE warehouse_terminal.study ADD CONSTRAINT study_warehouse_study_id_fkey FOREIGN KEY (warehouse_study_id) REFERENCES warehouse.study(id) ON UPDATE CASCADE ON DELETE SET NULL;
