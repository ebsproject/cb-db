--liquibase formatted sql

--changeset postgres:update_platform_functions context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Update platform functions



-- update function
CREATE OR REPLACE FUNCTION platform.audit_table(in_table_name text, in_schema_name text DEFAULT NULL::text, in_remove_audit boolean DEFAULT false, in_force_overwrite boolean DEFAULT false)
 RETURNS TABLE(success boolean, table_name text, message text)
 LANGUAGE plpgsql
AS $function$
DECLARE
    var_success BOOLEAN DEFAULT FALSE;
    var_drop_trigger_query text;
    var_create_trigger_query text;
    var_table record;
    var_query_response RECORD;
    var_trigger_function_name text;
    var_check_trigger_count integer;
    var_message text;
    var_continue boolean = true;
BEGIN
    /**
        B4R-315
        
        https://riceinfo.atlassian.net/browse/B4R-315
        Audit trailt: Save snapshot of changes in the event_log column
        
        Use the event_log column on the main tables in B4R to save changes made per record.Optimize the saving and updating of the data in the event_log column.Create stored procedures and indices to speed up the saving and viewing of these changes.
        
        @return TRIGGER Trigger results
        
        @author Argem Gerald Flores <a.flores@irri.org>
        @date 2018-08-31 09:10:29
     */
    
    if (platform.table_exists(in_table_name := in_table_name, in_schema_name := in_schema_name)) then
        select t.* into var_table from platform.get_table(in_table_name := in_table_name, in_schema_name := in_schema_name) t;
        
        if (var_table.table_name is not null) then
            SELECT
                count(1)
            into
                var_check_trigger_count
            FROM
                information_schema.triggers istr
            WHERE
                istr.event_object_schema = var_table.table_schema
                AND istr.event_object_table = var_table.table_name
                and istr.trigger_name = var_table.table_name || '_event_log_tgr'
            ;

            if (in_remove_audit = false) then
                if (var_check_trigger_count > 0) then
                    if (in_force_overwrite = true) then
                        -- remove trigger event if exists
                        var_drop_trigger_query := format($$
                            DROP TRIGGER IF EXISTS
                                %1$s_event_log_tgr
                            ON
                                %2$s
                        $$,
                            var_table.table_name,
                            var_table.schema_qualified_table_name
                        );
                        
                        PERFORM platform.execute(var_drop_trigger_query);
                        
                        var_check_trigger_count = 0;
                    else
                        var_success = false;
                        var_message = 'Audit trigger already exists';
                        
                        raise notice '! Audit trigger already exists: set in_force_overwrite = true';
                        var_continue = false;
                    end if;
                end if;
            else
                var_continue = true;
            end if;
            
            if (var_continue = true) then
                if (in_remove_audit = false) then
                    if (not platform.column_exists('event_log', var_table.schema_qualified_table_name)) then
                        perform platform.add_column('event_log', 'jsonb', var_table.schema_qualified_table_name);
                        
                        raise notice '+ Column event_log added: %', var_table.schema_qualified_table_name;
                        
                        /*perform platform.execute(format($$
                            create index
                                %1$s_event_log_idx
                            on
                                %2$s (
                                    event_log
                                )
                        $$,
                            var_table.table_name,
                            var_table.schema_qualified_table_name
                        ));*/
                        
                        -- raise notice '+ Column event_log indexed: %', var_table.schema_qualified_table_name;
                    end if;
                    
                    if (var_table.table_schema = 'gms') then
                        var_trigger_function_name = 'platform.log_record_event_gms()';
                    else
                        var_trigger_function_name = 'platform.log_record_event()';
                    end if;
                    
                    -- add trigger to table
                    var_create_trigger_query := format($$
                        CREATE TRIGGER
                            %1$s_event_log_tgr
                        BEFORE
                            INSERT OR UPDATE
                        ON
                            %2$s
                        FOR EACH ROW EXECUTE PROCEDURE
                            %3$s
                    $$,
                        var_table.table_name,
                        var_table.schema_qualified_table_name,
                        var_trigger_function_name
                    );
                    
                    perform platform.execute(var_create_trigger_query);
                    
                    var_message = 'Audit trigger set';
                    
                    raise notice '+ Table audit defined: %', var_table.schema_qualified_table_name;
                    
                    var_success = true;
                else
                    if (var_check_trigger_count > 0 or in_force_overwrite = true) then
                        -- drop index from table
                        perform platform.execute(format($$
                            DROP index
                                IF EXISTS
                                %1$s_event_log_idx
                        $$,
                            var_table.table_name,
                            var_table.schema_qualified_table_name
                        ));
                        
                        -- drop trigger from table
                        var_drop_trigger_query := format($$
                            DROP TRIGGER
                                IF EXISTS
                                %1$s_event_log_tgr
                            ON
                                %2$s
                                CASCADE
                        $$,
                            var_table.table_name,
                            var_table.schema_qualified_table_name
                        );
                        
                        perform platform.execute(var_drop_trigger_query);
                        
                        var_message = 'Audit trigger removed';
                        
                        raise notice '+ Table audit removed: %', in_table_name;
                        var_success = true;
                    else
                        var_success = false;
                        var_message = 'Audit trigger not found';
                    end if;
                END IF;
            end if;
        else
            raise notice '! Table name not found: %', in_table_name;
            
            var_success = false;
        end if;
    else
        raise notice '! Table not found: %', in_table_name;
        
        var_success = false;
    end if;
    
    RETURN QUERY (
        SELECT
            var_success,
            in_table_name,
            var_message
    );
END; $function$
;


CREATE OR REPLACE FUNCTION platform.column_exists(var_in_column text, var_in_table text, var_in_schema text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
declare
    var_response text;
    var_table text;
    var_elements text[];
    var_command text;
    var_column_count integer = 0;
begin
    

     if (var_in_table ilike '%.%') then
        var_elements = string_to_array(var_in_table, '.');

        if (array_length(var_elements, 1) = 2) then
            var_in_schema = var_elements[1];
            var_in_table = var_elements[2];
        else
            return false;
        end if;
    end if;

    var_table = platform.construct_table_name(var_in_table, var_in_schema);

    if (platform.table_exists(var_table)) then
        var_command = format($$
            select
                count(1)
            from
                information_schema.columns isc
            where
                isc.table_schema = '%s'
                and isc.table_name = '%s'
                and isc.column_name = '%s'
        $$,
            var_in_schema,
            var_in_table,
            var_in_column
        );
        
        execute var_command into var_column_count;

        return var_column_count > 0;
    end if;
    
    return false;
end; $function$
;

CREATE OR REPLACE FUNCTION platform.column_exists(var_in_column text, var_in_table text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
begin
    return platform.column_exists(var_in_column, var_in_table, null);
end; $function$
;



CREATE OR REPLACE FUNCTION platform.add_column(var_in_column text, var_in_column_def text, var_in_table text, var_in_schema text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
    var_response text;
    var_elements text[];
    var_table text;
    var_command text;
begin
    /**
     * Add column to table
     * 
     * Adds a column to a table
     * 
     * @param var_in_user text User identifier
     * @return text Response
     * 
     * @author Argem Gerald R. Flores <a.flores@irri.org>
     * @since 2015-09-07 09:55:30
     */
    if (var_in_table ilike '%.%') then
        var_elements = string_to_array(var_in_table, '.');

        if (array_length(var_elements, 1) = 2) then
            var_in_schema = var_elements[1];
            var_in_table = var_elements[2];
        else
            return false;
        end if;
    end if;

    var_table = platform.construct_table_name(var_in_table, var_in_schema);

    if (platform.table_exists(var_table)) then
        raise notice '+ Table found: %', var_table;

        if (not platform.column_exists(var_in_column, var_table)) then
            raise notice '+ Column to be added: %', var_in_column;

            var_command = format($$
                alter table
                    %s
                add column
                    %s
                    %s
            $$,
                var_table,
                var_in_column,
                var_in_column_def
            );

            execute var_command;

            return format($$+ Column added: %s - %s$$, var_in_column, var_table);
        else
            raise exception '%', format($$! Column found: %s - %s$$, var_in_column, var_table);
        end if;
    else
        raise exception '%', format($$! Table not found: %s$$, var_table);
    end if;

    
    return var_response;
end; $function$
;

CREATE OR REPLACE FUNCTION platform.add_column(var_in_column text, var_in_column_def text, var_in_table text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
begin
    return platform.add_column(var_in_column, var_in_column_def, var_in_table, null);
end; $function$
;




--rollback CREATE OR REPLACE FUNCTION platform.audit_table(in_table_name text, in_schema_name text DEFAULT NULL::text, in_remove_audit boolean DEFAULT false, in_force_overwrite boolean DEFAULT false)
--rollback  RETURNS TABLE(success boolean, table_name text, message text)
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     var_success BOOLEAN DEFAULT FALSE;
--rollback     var_drop_trigger_query text;
--rollback     var_create_trigger_query text;
--rollback     var_table record;
--rollback     var_query_response RECORD;
--rollback     var_trigger_function_name text;
--rollback     var_check_trigger_count integer;
--rollback     var_message text;
--rollback     var_continue boolean = true;
--rollback BEGIN
--rollback     /**
--rollback         B4R-315
--rollback         
--rollback         https://riceinfo.atlassian.net/browse/B4R-315
--rollback         Audit trailt: Save snapshot of changes in the event_log column
--rollback         
--rollback         Use the event_log column on the main tables in B4R to save changes made per record.Optimize the saving and updating of the data in the event_log column.Create stored procedures and indices to speed up the saving and viewing of these changes.
--rollback         
--rollback         @return TRIGGER Trigger results
--rollback         
--rollback         @author Argem Gerald Flores <a.flores@irri.org>
--rollback         @date 2018-08-31 09:10:29
--rollback      */
--rollback     
--rollback     if (platform.table_exists(in_table_name := in_table_name, in_schema_name := in_schema_name)) then
--rollback         select t.* into var_table from platform.get_table(in_table_name := in_table_name, in_schema_name := in_schema_name) t;
--rollback         
--rollback         if (var_table.table_name is not null) then
--rollback             SELECT
--rollback                 count(1)
--rollback             into
--rollback                 var_check_trigger_count
--rollback             FROM
--rollback                 information_schema.triggers istr
--rollback             WHERE
--rollback                 istr.event_object_schema = var_table.table_schema
--rollback                 AND istr.event_object_table = var_table.table_name
--rollback                 and istr.trigger_name = var_table.table_name || '_event_log_tgr'
--rollback             ;
--rollback 
--rollback             if (in_remove_audit = false) then
--rollback                 if (var_check_trigger_count > 0) then
--rollback                     if (in_force_overwrite = true) then
--rollback                         -- remove trigger event if exists
--rollback                         var_drop_trigger_query := format($$
--rollback                             DROP TRIGGER IF EXISTS
--rollback                                 %1$s_event_log_tgr
--rollback                             ON
--rollback                                 %2$s
--rollback                         $$,
--rollback                             var_table.table_name,
--rollback                             var_table.schema_qualified_table_name
--rollback                         );
--rollback                         
--rollback                         PERFORM platform.execute(var_drop_trigger_query);
--rollback                         
--rollback                         var_check_trigger_count = 0;
--rollback                     else
--rollback                         var_success = false;
--rollback                         var_message = 'Audit trigger already exists';
--rollback                         
--rollback                         raise notice '! Audit trigger already exists: set in_force_overwrite = true';
--rollback                         var_continue = false;
--rollback                     end if;
--rollback                 end if;
--rollback             else
--rollback                 var_continue = true;
--rollback             end if;
--rollback             
--rollback             if (var_continue = true) then
--rollback                 if (in_remove_audit = false) then
--rollback                     if (not platform.column_exists('event_log', var_table.schema_qualified_table_name)) then
--rollback                         perform platform.add_column('event_log', 'jsonb', var_table.schema_qualified_table_name);
--rollback                         
--rollback                         raise notice '+ Column event_log added: %', var_table.schema_qualified_table_name;
--rollback                         
--rollback                         /*perform platform.execute(format($$
--rollback                             create index
--rollback                                 %1$s_event_log_idx
--rollback                             on
--rollback                                 %2$s (
--rollback                                     event_log
--rollback                                 )
--rollback                         $$,
--rollback                             var_table.table_name,
--rollback                             var_table.schema_qualified_table_name
--rollback                         ));*/
--rollback                         
--rollback                         -- raise notice '+ Column event_log indexed: %', var_table.schema_qualified_table_name;
--rollback                     end if;
--rollback                     
--rollback                     if (var_table.table_schema = 'gms') then
--rollback                         var_trigger_function_name = 'platform.log_record_event_gms()';
--rollback                     else
--rollback                         var_trigger_function_name = 'platform.log_record_event()';
--rollback                     end if;
--rollback                     
--rollback                     -- add trigger to table
--rollback                     var_create_trigger_query := format($$
--rollback                         CREATE TRIGGER
--rollback                             %1$s_event_log_tgr
--rollback                         BEFORE
--rollback                             INSERT OR UPDATE
--rollback                         ON
--rollback                             %2$s
--rollback                         FOR EACH ROW EXECUTE PROCEDURE
--rollback                             %3$s
--rollback                     $$,
--rollback                         var_table.table_name,
--rollback                         var_table.schema_qualified_table_name,
--rollback                         var_trigger_function_name
--rollback                     );
--rollback                     
--rollback                     perform platform.execute(var_create_trigger_query);
--rollback                     
--rollback                     var_message = 'Audit trigger set';
--rollback                     
--rollback                     raise notice '+ Table audit defined: %', var_table.schema_qualified_table_name;
--rollback                     
--rollback                     var_success = true;
--rollback                 else
--rollback                     if (var_check_trigger_count > 0 or in_force_overwrite = true) then
--rollback                         -- drop index from table
--rollback                         perform platform.execute(format($$
--rollback                             DROP index
--rollback                                 IF EXISTS
--rollback                                 %1$s_event_log_idx
--rollback                         $$,
--rollback                             var_table.table_name,
--rollback                             var_table.schema_qualified_table_name
--rollback                         ));
--rollback                         
--rollback                         -- drop trigger from table
--rollback                         var_drop_trigger_query := format($$
--rollback                             DROP TRIGGER
--rollback                                 IF EXISTS
--rollback                                 %1$s_event_log_tgr
--rollback                             ON
--rollback                                 %2$s
--rollback                                 CASCADE
--rollback                         $$,
--rollback                             var_table.table_name,
--rollback                             var_table.schema_qualified_table_name
--rollback                         );
--rollback                         
--rollback                         perform platform.execute(var_drop_trigger_query);
--rollback                         
--rollback                         var_message = 'Audit trigger removed';
--rollback                         
--rollback                         raise notice '+ Table audit removed: %', in_table_name;
--rollback                         var_success = true;
--rollback                     else
--rollback                         var_success = false;
--rollback                         var_message = 'Audit trigger not found';
--rollback                     end if;
--rollback                 END IF;
--rollback             end if;
--rollback         else
--rollback             raise notice '! Table name not found: %', in_table_name;
--rollback             
--rollback             var_success = false;
--rollback         end if;
--rollback     else
--rollback         raise notice '! Table not found: %', in_table_name;
--rollback         
--rollback         var_success = false;
--rollback     end if;
--rollback     
--rollback     RETURN QUERY (
--rollback         SELECT
--rollback             var_success,
--rollback             in_table_name,
--rollback             var_message
--rollback     );
--rollback END; $function$
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION platform.column_exists(var_in_column text, var_in_table text, var_in_schema text)
--rollback  RETURNS boolean
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback declare
--rollback     var_response text;
--rollback     var_table text;
--rollback     var_elements text[];
--rollback     var_command text;
--rollback     var_column_count integer = 0;
--rollback begin
--rollback     
--rollback 
--rollback      if (var_in_table ilike '%.%') then
--rollback         var_elements = string_to_array(var_in_table, '.');
--rollback 
--rollback         if (array_length(var_elements, 1) = 2) then
--rollback             var_in_schema = var_elements[1];
--rollback             var_in_table = var_elements[2];
--rollback         else
--rollback             return false;
--rollback         end if;
--rollback     end if;
--rollback 
--rollback     var_table = platform.construct_table_name(var_in_table, var_in_schema);
--rollback 
--rollback     if (platform.table_exists(var_table)) then
--rollback         var_command = format($$
--rollback             select
--rollback                 count(1)
--rollback             from
--rollback                 information_schema.columns isc
--rollback             where
--rollback                 isc.table_schema = '%s'
--rollback                 and isc.table_name = '%s'
--rollback                 and isc.column_name = '%s'
--rollback         $$,
--rollback             var_in_schema,
--rollback             var_in_table,
--rollback             var_in_column
--rollback         );
--rollback         
--rollback         execute var_command into var_column_count;
--rollback 
--rollback         return var_column_count > 0;
--rollback     end if;
--rollback     
--rollback     return false;
--rollback end; $function$
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION platform.column_exists(var_in_column text, var_in_table text)
--rollback  RETURNS boolean
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback begin
--rollback     return platform.column_exists(var_in_column, var_in_table, null);
--rollback end; $function$
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION platform.add_column(var_in_column text, var_in_column_def text, var_in_table text, var_in_schema text)
--rollback  RETURNS text
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback declare
--rollback     var_response text;
--rollback     var_elements text[];
--rollback     var_table text;
--rollback     var_command text;
--rollback begin
--rollback     /**
--rollback      * Add column to table
--rollback      * 
--rollback      * Adds a column to a table
--rollback      * 
--rollback      * @param var_in_user text User identifier
--rollback      * @return text Response
--rollback      * 
--rollback      * @author Argem Gerald R. Flores <a.flores@irri.org>
--rollback      * @since 2015-09-07 09:55:30
--rollback      */
--rollback     if (var_in_table ilike '%.%') then
--rollback         var_elements = string_to_array(var_in_table, '.');
--rollback 
--rollback         if (array_length(var_elements, 1) = 2) then
--rollback             var_in_schema = var_elements[1];
--rollback             var_in_table = var_elements[2];
--rollback         else
--rollback             return false;
--rollback         end if;
--rollback     end if;
--rollback 
--rollback     var_table = platform.construct_table_name(var_in_table, var_in_schema);
--rollback 
--rollback     if (platform.table_exists(var_table)) then
--rollback         raise notice '+ Table found: %', var_table;
--rollback 
--rollback         if (not platform.column_exists(var_in_column, var_table)) then
--rollback             raise notice '+ Column to be added: %', var_in_column;
--rollback 
--rollback             var_command = format($$
--rollback                 alter table
--rollback                     %s
--rollback                 add column
--rollback                     %s
--rollback                     %s
--rollback             $$,
--rollback                 var_table,
--rollback                 var_in_column,
--rollback                 var_in_column_def
--rollback             );
--rollback 
--rollback             execute var_command;
--rollback 
--rollback             return format($$+ Column added: %s - %s$$, var_in_column, var_table);
--rollback         else
--rollback             raise exception '%', format($$! Column found: %s - %s$$, var_in_column, var_table);
--rollback         end if;
--rollback     else
--rollback         raise exception '%', format($$! Table not found: %s$$, var_table);
--rollback     end if;
--rollback 
--rollback     
--rollback     return var_response;
--rollback end; $function$
--rollback ;
--rollback 
--rollback CREATE OR REPLACE FUNCTION platform.add_column(var_in_column text, var_in_column_def text, var_in_table text)
--rollback  RETURNS text
--rollback  LANGUAGE plpgsql
--rollback AS $function$
--rollback begin
--rollback     return platform.add_column(var_in_column, var_in_column_def, var_in_table, null);
--rollback end; $function$
--rollback ;
