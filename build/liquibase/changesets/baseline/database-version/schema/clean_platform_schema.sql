--liquibase formatted sql

--changeset postgres:clean_platform_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean platform schema



-- drop constraints
ALTER TABLE platform.saved_list_access DROP CONSTRAINT saved_list_access_saved_list_id_fkey;
ALTER TABLE platform.saved_list_member DROP CONSTRAINT saved_list_metadata_saved_list_id_fkey;
ALTER TABLE platform.saved_list_member_metadata DROP CONSTRAINT saved_list_member_metadata_saved_list_id_fkey;
ALTER TABLE platform.saved_list_member_metadata DROP CONSTRAINT saved_list_member_metadata_saved_list_member_id_fkey;
ALTER TABLE platform.saved_list_metadata DROP CONSTRAINT saved_list_metadata_saved_list_id_fkey;


-- drop triggers
DROP TRIGGER saved_list_event_log_tgr ON platform.saved_list;
DROP TRIGGER saved_list_member_event_log_tgr ON platform.saved_list_member;


-- drop functions


-- drop views


-- drop materialized views


-- drop sequences


-- drop tables
DROP TABLE platform.access_control_list;
DROP TABLE platform.plan_template_metadata;
DROP TABLE platform.saved_list;
DROP TABLE platform.saved_list_access;
DROP TABLE platform.saved_list_member;
DROP TABLE platform.saved_list_member_metadata;
DROP TABLE platform.saved_list_metadata;
DROP TABLE platform.viewer_config;



--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.access_control_list;
--rollback 
--rollback CREATE TABLE platform.access_control_list (
--rollback     id serial NOT NULL,
--rollback     entity_name varchar(256) NOT NULL,
--rollback     entity_id int4 NOT NULL,
--rollback     subject_entity_name varchar(256) NOT NULL,
--rollback     access_data jsonb NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT access_control_list_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT access_control_list_creator_id FOREIGN KEY (creator_id) REFERENCES master."user"(id),
--rollback     CONSTRAINT access_control_list_modifier_id FOREIGN KEY (modifier_id) REFERENCES master."user"(id)
--rollback );
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.plan_template_metadata;
--rollback 
--rollback CREATE TABLE platform.plan_template_metadata (
--rollback     id serial NOT NULL,
--rollback     plan_template_id int4 NOT NULL,
--rollback     variable_id int4 NOT NULL,
--rollback     value varchar NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NULL,
--rollback     creator_id int4 NULL,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT plan_template_metadata_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT plan_template_metadata_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id),
--rollback     CONSTRAINT plan_template_metadata_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id),
--rollback     CONSTRAINT plan_template_metadata_plan_template_id_fkey FOREIGN KEY (plan_template_id) REFERENCES platform.plan_template(id)
--rollback );
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.saved_list;
--rollback 
--rollback CREATE TABLE platform.saved_list (
--rollback     id serial NOT NULL,
--rollback     abbrev varchar(128) NOT NULL,
--rollback     "name" varchar(256) NOT NULL,
--rollback     list_type varchar(64) NOT NULL,
--rollback     description text NULL,
--rollback     display_name varchar(256) NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     event_log jsonb NULL,
--rollback     CONSTRAINT saved_list_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT saved_list_list_type_chk CHECK (((list_type)::text = ANY (ARRAY['product_list'::text, 'variable_list'::text, 'study_list'::text, 'seed_lot_list'::text, 'location_list'::text]))),
--rollback     CONSTRAINT saved_list_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX saved_list_abbrev_idx ON platform.saved_list USING btree (abbrev);
--rollback CREATE INDEX saved_list_event_log_idx ON platform.saved_list USING btree (event_log);
--rollback CREATE INDEX saved_list_is_void_idx ON platform.saved_list USING btree (is_void);
--rollback CREATE INDEX saved_list_list_type_idx ON platform.saved_list USING btree (list_type);
--rollback CREATE INDEX saved_list_name_idx ON platform.saved_list USING btree (name);
--rollback 
--rollback -- Table Triggers
--rollback 
--rollback -- DROP TRIGGER saved_list_event_log_tgr ON platform.saved_list;
--rollback 
--rollback CREATE TRIGGER saved_list_event_log_tgr BEFORE
--rollback INSERT
--rollback     OR
--rollback UPDATE
--rollback     ON
--rollback     platform.saved_list FOR EACH ROW EXECUTE PROCEDURE z_admin.log_record_event();
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.saved_list_access;
--rollback 
--rollback CREATE TABLE platform.saved_list_access (
--rollback     id serial NOT NULL,
--rollback     saved_list_id int4 NOT NULL,
--rollback     user_id int4 NOT NULL,
--rollback     "permission" varchar NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT saved_list_access_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT saved_list_access_permission_chk CHECK (((permission)::text = ANY (ARRAY['read'::text, 'read_write'::text]))),
--rollback     CONSTRAINT saved_list_access_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_access_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_access_saved_list_id_fkey FOREIGN KEY (saved_list_id) REFERENCES platform.saved_list(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_access_user_id_fkey FOREIGN KEY (user_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX saved_list_access_is_void_idx ON platform.saved_list_access USING btree (is_void);
--rollback CREATE INDEX saved_list_access_permission_idx ON platform.saved_list_access USING btree (permission);
--rollback CREATE INDEX saved_list_access_saved_list_id_idx ON platform.saved_list_access USING btree (saved_list_id);
--rollback CREATE INDEX saved_list_access_saved_list_id_user_id_idx ON platform.saved_list_access USING btree (saved_list_id, user_id);
--rollback CREATE INDEX saved_list_access_user_id_idx ON platform.saved_list_access USING btree (user_id);
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.saved_list_member;
--rollback 
--rollback CREATE TABLE platform.saved_list_member (
--rollback     id serial NOT NULL,
--rollback     saved_list_id int4 NOT NULL,
--rollback     entity_id int4 NOT NULL,
--rollback     order_number int4 NOT NULL DEFAULT 1,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     data_object_id int4 NULL,
--rollback     event_log jsonb NULL,
--rollback     CONSTRAINT saved_list_member_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT saved_list_member_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_member_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_metadata_saved_list_id_fkey FOREIGN KEY (saved_list_id) REFERENCES platform.saved_list(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX saved_list_member_entity_id_idx ON platform.saved_list_member USING btree (entity_id);
--rollback CREATE INDEX saved_list_member_event_log_idx ON platform.saved_list_member USING btree (event_log);
--rollback CREATE INDEX saved_list_member_is_void_idx ON platform.saved_list_member USING btree (is_void);
--rollback CREATE INDEX saved_list_member_order_number_idx ON platform.saved_list_member USING btree (order_number);
--rollback CREATE INDEX saved_list_member_saved_list_id_entity_id_idx ON platform.saved_list_member USING btree (saved_list_id, entity_id);
--rollback CREATE INDEX saved_list_member_saved_list_id_idx ON platform.saved_list_member USING btree (saved_list_id);
--rollback 
--rollback -- Table Triggers
--rollback 
--rollback -- DROP TRIGGER saved_list_member_event_log_tgr ON platform.saved_list_member;
--rollback 
--rollback CREATE TRIGGER saved_list_member_event_log_tgr BEFORE
--rollback INSERT
--rollback     OR
--rollback UPDATE
--rollback     ON
--rollback     platform.saved_list_member FOR EACH ROW EXECUTE PROCEDURE z_admin.log_record_event();
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.saved_list_member_metadata;
--rollback 
--rollback CREATE TABLE platform.saved_list_member_metadata (
--rollback     id serial NOT NULL,
--rollback     saved_list_id int4 NOT NULL,
--rollback     saved_list_member_id int4 NOT NULL,
--rollback     variable_id int4 NOT NULL,
--rollback     value varchar NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT saved_list_member_metadata_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT saved_list_member_metadata_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_member_metadata_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_member_metadata_saved_list_id_fkey FOREIGN KEY (saved_list_id) REFERENCES platform.saved_list(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_member_metadata_saved_list_member_id_fkey FOREIGN KEY (saved_list_member_id) REFERENCES platform.saved_list_member(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_member_metadata_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES master.variable(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX saved_list_member_metadata_is_void_idx ON platform.saved_list_member_metadata USING btree (is_void);
--rollback CREATE INDEX saved_list_member_metadata_saved_list_id_idx ON platform.saved_list_member_metadata USING btree (saved_list_id);
--rollback CREATE INDEX saved_list_member_metadata_saved_list_id_saved_list_member_id_i ON platform.saved_list_member_metadata USING btree (saved_list_id, saved_list_member_id);
--rollback CREATE INDEX saved_list_member_metadata_saved_list_member_id_idx ON platform.saved_list_member_metadata USING btree (saved_list_member_id);
--rollback CREATE INDEX saved_list_member_metadata_saved_list_member_id_variable_id_idx ON platform.saved_list_member_metadata USING btree (saved_list_member_id, variable_id);
--rollback CREATE INDEX saved_list_member_metadata_variable_id_idx ON platform.saved_list_member_metadata USING btree (variable_id);
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.saved_list_metadata;
--rollback 
--rollback CREATE TABLE platform.saved_list_metadata (
--rollback     id serial NOT NULL,
--rollback     saved_list_id int4 NOT NULL,
--rollback     variable_id int4 NOT NULL,
--rollback     value varchar NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT saved_list_metadata_id_pkey PRIMARY KEY (id),
--rollback     CONSTRAINT saved_list_metadata_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_metadata_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_metadata_saved_list_id_fkey FOREIGN KEY (saved_list_id) REFERENCES platform.saved_list(id) ON UPDATE CASCADE ON DELETE CASCADE,
--rollback     CONSTRAINT saved_list_metadata_variable_id_fkey FOREIGN KEY (variable_id) REFERENCES master.variable(id) ON UPDATE CASCADE ON DELETE CASCADE
--rollback );
--rollback CREATE INDEX saved_list_metadata_is_void_idx ON platform.saved_list_metadata USING btree (is_void);
--rollback CREATE INDEX saved_list_metadata_saved_list_id_idx ON platform.saved_list_metadata USING btree (saved_list_id);
--rollback CREATE INDEX saved_list_metadata_saved_list_id_variable_id_idx ON platform.saved_list_metadata USING btree (saved_list_id, variable_id);
--rollback CREATE INDEX saved_list_metadata_variable_id_idx ON platform.saved_list_metadata USING btree (variable_id);
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE platform.viewer_config;
--rollback 
--rollback CREATE TABLE platform.viewer_config (
--rollback     id serial NOT NULL,
--rollback     "name" varchar(255) NOT NULL,
--rollback     "data" jsonb NOT NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes varchar(600) NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     CONSTRAINT pk_viewer_config PRIMARY KEY (id),
--rollback     CONSTRAINT viewer_config_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES master."user"(id),
--rollback     CONSTRAINT viewer_config_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES master."user"(id)
--rollback );
--rollback CREATE INDEX idx_viewer_config_creator ON platform.viewer_config USING btree (creator_id);
--rollback CREATE INDEX idx_viewer_config_modifier ON platform.viewer_config USING btree (modifier_id);
--rollback CREATE INDEX viewer_config_is_void_idx ON platform.viewer_config USING btree (is_void);
--rollback COMMENT ON INDEX platform.viewer_config_is_void_idx IS 'Index for the is_void column';
