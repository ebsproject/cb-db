--liquibase formatted sql

--changeset postgres:clean_api_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean api schema



COMMENT ON SCHEMA api IS 'Application programming interface for B4R';


DROP TABLE IF EXISTS api.oauth_access_token_scopes;


DROP TABLE IF EXISTS api.oauth_authorization_codes;


DROP TABLE IF EXISTS api.oauth_clients;


DROP TABLE IF EXISTS api.oauth_refresh_tokens;


DROP TABLE IF EXISTS api.oauth_scopes;


DROP TABLE IF EXISTS api.oauth_sessions;


DROP TABLE IF EXISTS api.resource_data;


DROP TABLE IF EXISTS api.resource;


DROP TABLE IF EXISTS api.transaction;



--rollback COMMENT ON SCHEMA api IS 'Application programming interface for BIMS web services';

--rollback CREATE TABLE api.oauth_access_token_scopes (
--rollback     access_token text NULL,
--rollback     "scope" text NULL
--rollback );

--rollback CREATE TABLE api.oauth_authorization_codes (
--rollback     authorization_code varchar(40) NOT NULL,
--rollback     client_id varchar(80) NOT NULL,
--rollback     user_id varchar(255) NULL,
--rollback     redirect_uri varchar(2000) NULL,
--rollback     expires timestamp NOT NULL,
--rollback     "scope" varchar(2000) NULL,
--rollback     access_token varchar(250) NULL,
--rollback     session_id int4 NULL,
--rollback     CONSTRAINT oauth_authorization_codes_authorization_code_pkey PRIMARY KEY (authorization_code)
--rollback );

--rollback CREATE TABLE api.oauth_clients (
--rollback     client_id varchar(80) NOT NULL,
--rollback     client_secret varchar(80) NOT NULL,
--rollback     redirect_uri varchar(2000) NOT NULL,
--rollback     grant_types varchar(80) NULL,
--rollback     "scope" varchar(100) NULL,
--rollback     user_id varchar(80) NULL,
--rollback     CONSTRAINT oauth_clients_client_id_pkey PRIMARY KEY (client_id)
--rollback );

--rollback CREATE TABLE api.oauth_refresh_tokens (
--rollback     refresh_token varchar(40) NOT NULL,
--rollback     client_id varchar(80) NOT NULL,
--rollback     user_id varchar(255) NULL,
--rollback     expires timestamp NOT NULL,
--rollback     "scope" varchar(2000) NULL,
--rollback     access_token varchar(50) NULL,
--rollback     CONSTRAINT oauth_refresh_tokens_refresh_token_pkey PRIMARY KEY (refresh_token)
--rollback );

--rollback CREATE TABLE api.oauth_scopes (
--rollback     id text NULL,
--rollback     description text NULL
--rollback );

--rollback CREATE TABLE api.oauth_sessions (
--rollback     id serial NOT NULL,
--rollback     owner_type varchar(250) NULL,
--rollback     owner_id varchar(250) NULL,
--rollback     client_id varchar(250) NULL,
--rollback     CONSTRAINT oauth_sessions_pkey PRIMARY KEY (id)
--rollback );

--rollback CREATE TABLE api.resource (
--rollback     id serial NOT NULL,
--rollback     "name" varchar(128) NULL,
--rollback     description text NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     "version" int4 NOT NULL,
--rollback     CONSTRAINT resource_id_pkey PRIMARY KEY (id)
--rollback );
--rollback CREATE INDEX fki_resource_creator_id_fkey ON api.resource USING btree (creator_id);
--rollback CREATE INDEX fki_resource_modifier_id_fkey ON api.resource USING btree (modifier_id);

--rollback CREATE TABLE api.resource_data (
--rollback     id serial NOT NULL,
--rollback     resource_id int4 NOT NULL DEFAULT 1,
--rollback     "attribute" varchar(128) NOT NULL DEFAULT 1,
--rollback     description text NULL,
--rollback     remarks text NULL,
--rollback     creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     modification_timestamp timestamp NULL,
--rollback     modifier_id int4 NULL,
--rollback     notes text NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     data_type varchar(128) NULL,
--rollback     field_order int4 NULL,
--rollback     "type" varchar(128) NULL
--rollback );
--rollback CREATE INDEX fki_resource_data_creator_id_fkey ON api.resource_data USING btree (creator_id);
--rollback CREATE INDEX fki_resource_data_modifier_id_fkey ON api.resource_data USING btree (modifier_id);
--rollback CREATE INDEX fki_resource_data_resource_id_fkey ON api.resource_data USING btree (resource_id);
--rollback 
--rollback 
--rollback -- api.resource_data foreign keys
--rollback 
--rollback ALTER TABLE api.resource_data ADD CONSTRAINT resource_data_resource_id_fkey FOREIGN KEY (resource_id) REFERENCES api.resource(id);