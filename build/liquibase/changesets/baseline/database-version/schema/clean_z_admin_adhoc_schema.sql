--liquibase formatted sql

--changeset postgres:clean_z_admin_adhoc_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean z_admin_adhoc schema



-- drop schema
DROP SCHEMA z_admin_adhoc;



--rollback CREATE SCHEMA z_admin_adhoc;
