--liquibase formatted sql

--changeset postgres:clean_gme_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean gme schema



-- drop tables
DROP TABLE gme.attributes;
DROP TABLE gme.nodes;
DROP TABLE gme.polygons;


-- drop schema
DROP SCHEMA gme;



--rollback CREATE SCHEMA gme;
--rollback 
--rollback 
--rollback 
--rollback SET default_tablespace = '';
--rollback 
--rollback SET default_with_oids = false;
--rollback 
--rollback 
--rollback CREATE TABLE gme.attributes (
--rollback     id integer NOT NULL,
--rollback     shape_id integer,
--rollback     object_id integer
--rollback );
--rollback 
--rollback 
--rollback 
--rollback 
--rollback CREATE SEQUENCE gme.attributes_id_seq
--rollback     START WITH 1
--rollback     INCREMENT BY 1
--rollback     NO MINVALUE
--rollback     NO MAXVALUE
--rollback     CACHE 1;
--rollback 
--rollback 
--rollback 
--rollback 
--rollback ALTER SEQUENCE gme.attributes_id_seq OWNED BY gme.attributes.id;
--rollback 
--rollback 
--rollback CREATE TABLE gme.nodes (
--rollback     id integer NOT NULL,
--rollback     node_id integer,
--rollback     shape_id integer,
--rollback     x numeric,
--rollback     y numeric
--rollback );
--rollback 
--rollback 
--rollback 
--rollback 
--rollback CREATE SEQUENCE gme.nodes_id_seq
--rollback     START WITH 1
--rollback     INCREMENT BY 1
--rollback     NO MINVALUE
--rollback     NO MAXVALUE
--rollback     CACHE 1;
--rollback 
--rollback 
--rollback 
--rollback 
--rollback ALTER SEQUENCE gme.nodes_id_seq OWNED BY gme.nodes.id;
--rollback 
--rollback 
--rollback CREATE TABLE gme.polygons (
--rollback     id integer NOT NULL,
--rollback     object_id integer,
--rollback     polygon polygon
--rollback );
--rollback 
--rollback 
--rollback 
--rollback 
--rollback CREATE SEQUENCE gme.polygons_id_seq
--rollback     START WITH 1
--rollback     INCREMENT BY 1
--rollback     NO MINVALUE
--rollback     NO MAXVALUE
--rollback     CACHE 1;
--rollback 
--rollback 
--rollback 
--rollback 
--rollback ALTER SEQUENCE gme.polygons_id_seq OWNED BY gme.polygons.id;
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.attributes ALTER COLUMN id SET DEFAULT nextval('gme.attributes_id_seq'::regclass);
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.nodes ALTER COLUMN id SET DEFAULT nextval('gme.nodes_id_seq'::regclass);
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.polygons ALTER COLUMN id SET DEFAULT nextval('gme.polygons_id_seq'::regclass);
--rollback 
--rollback 
--rollback SELECT pg_catalog.setval('gme.attributes_id_seq', 1, false);
--rollback 
--rollback 
--rollback SELECT pg_catalog.setval('gme.nodes_id_seq', 1, false);
--rollback 
--rollback 
--rollback SELECT pg_catalog.setval('gme.polygons_id_seq', 1, false);
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.attributes
--rollback     ADD CONSTRAINT attributes_id_pkey PRIMARY KEY (id);
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.nodes
--rollback     ADD CONSTRAINT nodes_id_pkey PRIMARY KEY (id);
--rollback 
--rollback 
--rollback ALTER TABLE ONLY gme.polygons
--rollback     ADD CONSTRAINT polygon_id_pkey PRIMARY KEY (id);
