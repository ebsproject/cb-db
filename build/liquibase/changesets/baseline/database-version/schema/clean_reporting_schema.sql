--liquibase formatted sql

--changeset postgres:clean_reporting_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean reporting schema



-- drop tables
DROP TABLE reporting.operational_study_data_report_type;


-- drop schema
DROP SCHEMA reporting;



--rollback CREATE SCHEMA reporting;
--rollback 
--rollback -- Drop table
--rollback 
--rollback -- DROP TABLE reporting.operational_study_data_report_type;
--rollback 
--rollback CREATE TABLE reporting.operational_study_data_report_type (
--rollback     id serial NOT NULL,
--rollback     data_level int4 NULL,
--rollback     "label" varchar(255) NULL,
--rollback     description text NULL,
--rollback     creator_id int4 NOT NULL DEFAULT 1,
--rollback     creation_timestamp timestamp NULL DEFAULT now(),
--rollback     modifier_id int4 NULL,
--rollback     modification_timestamp timestamp NULL,
--rollback     is_void bool NOT NULL DEFAULT false,
--rollback     notes text NULL,
--rollback     CONSTRAINT operational_study_data_report_type_id_pk PRIMARY KEY (id)
--rollback );
