--liquibase formatted sql

--changeset postgres:clean_public_schema context:schema splitStatements:false rollbackSplitStatements:false
--comment: B4R-5330 Clean public schema



-- drop views
DROP VIEW public.seed_storage_source;


-- drop sequences
DROP SEQUENCE public.b4r_mobile_sync_id_seq;
DROP SEQUENCE public.changes_cid_seq;



--rollback CREATE OR REPLACE VIEW public.seed_storage_source
--rollback AS WITH oss AS (
--rollback          SELECT ossm.seed_storage_id,
--rollback             ossm.source_study,
--rollback             ossm.source_harv_year,
--rollback             ossm.source_season,
--rollback             ossm.source_entry,
--rollback             ossm.source_plot_no,
--rollback             oss_1.gid,
--rollback             oss_1.product_id,
--rollback             oss_1.seed_lot_id,
--rollback             oss_1.key_type,
--rollback             oss_1.product_gid_id
--rollback            FROM ( SELECT crosstab.seed_storage_id,
--rollback                     crosstab.source_study,
--rollback                     crosstab.source_harv_year,
--rollback                     crosstab.source_entry,
--rollback                     crosstab.source_season,
--rollback                     crosstab.source_plot_no
--rollback                    FROM crosstab('
--rollback                         select
--rollback                             seed_storage_id,
--rollback                             variable_id,
--rollback                             value
--rollback                         from
--rollback                             operational.seed_storage_metadata
--rollback                         where
--rollback                             variable_id in (594, 597, 595, 661, 596)
--rollback                             and is_void = false
--rollback                         order by
--rollback                             seed_storage_id
--rollback                     '::text, '
--rollback                         select
--rollback                             unnest(array[594, 597, 595, 661, 596])
--rollback                     '::text) crosstab(seed_storage_id integer, source_study character varying, source_harv_year integer, source_entry character varying, source_season character varying, source_plot_no integer)) ossm,
--rollback             operational.seed_storage oss_1
--rollback           WHERE ossm.seed_storage_id = oss_1.id AND oss_1.is_void = false
--rollback         )
--rollback  SELECT oss.seed_storage_id,
--rollback     oss.source_study,
--rollback     oss.source_harv_year,
--rollback     oss.source_season,
--rollback     oss.source_entry,
--rollback     oss.source_plot_no,
--rollback     oss.gid,
--rollback     oss.product_id,
--rollback     oss.seed_lot_id,
--rollback     oss.key_type,
--rollback     oss.product_gid_id
--rollback    FROM oss;
--rollback 
--rollback -- DROP SEQUENCE public.b4r_mobile_sync_id_seq;
--rollback 
--rollback CREATE SEQUENCE public.b4r_mobile_sync_id_seq
--rollback     INCREMENT BY 1
--rollback     MINVALUE 1
--rollback     MAXVALUE 9223372036854775807
--rollback     CACHE 1
--rollback     NO CYCLE;
--rollback 
--rollback 
--rollback -- DROP SEQUENCE public.changes_cid_seq;
--rollback 
--rollback CREATE SEQUENCE public.changes_cid_seq
--rollback     INCREMENT BY 1
--rollback     MINVALUE 1
--rollback     MAXVALUE 9223372036854775807
--rollback     CACHE 1
--rollback     NO CYCLE;
