--liquibase formatted sql

--changeset postgres:drop_entry_foreign_key_constraint_in_planting_instruction_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-401 Drop entry foreign key constraint in planting_instruction table



-- drop foreign key constraint planting_instruction_entry_id_entry_code_entry_number_fk
ALTER TABLE
    experiment.planting_instruction
DROP CONSTRAINT
    planting_instruction_entry_id_entry_code_entry_number_fk
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.planting_instruction
--rollback ADD CONSTRAINT
--rollback     planting_instruction_entry_id_entry_code_entry_number_fk
--rollback FOREIGN KEY (
--rollback     entry_id,
--rollback     entry_code,
--rollback     entry_number
--rollback )
--rollback REFERENCES
--rollback     experiment.entry (
--rollback         id,
--rollback         entry_code,
--rollback         entry_number
--rollback     )
--rollback     ON UPDATE CASCADE
--rollback     ON DELETE RESTRICT
--rollback ;
--rollback 
--rollback COMMENT ON CONSTRAINT
--rollback     planting_instruction_entry_id_entry_code_entry_number_fk
--rollback ON
--rollback     experiment.planting_instruction
--rollback IS
--rollback     'A planting instruction refers to the combination of its entry ID, entry code, and entry number. A combination of the entry id, entry code, and entry number can refer to one or many planting instructions, which means that any updates in the entry will cascade to the planting instructions.'
--rollback ;
