--liquibase formatted sql

--changeset postgres:create_table_experiment.planting_job_occurrence context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-330 Create table experiment.planting_job_occurrence



-- Create table
CREATE TABLE experiment.planting_job_occurrence (
	id serial NOT NULL,
    planting_job_id integer NOT NULL,
    occurrence_id integer NOT NULL,
    seeds_per_envelope float NOT NULL DEFAULT 1,
    package_unit varchar(16) NOT NULL DEFAULT 'g',
    envelopes_per_plot integer NOT NULL DEFAULT 1,	
    creator_id integer NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modifier_id integer NULL,
	modification_timestamp timestamp NULL,
	is_void bool NOT NULL DEFAULT false,
	notes text NULL,
	event_log jsonb NULL,
	CONSTRAINT planting_job_occurrence_id_pk PRIMARY KEY (id)
);
CREATE INDEX planting_job_occurrence_planting_job_id_idx ON experiment.planting_job_occurrence USING btree (planting_job_id);
CREATE INDEX planting_job_occurrence_creator_id_idx ON experiment.planting_job_occurrence USING btree (creator_id);
CREATE INDEX planting_job_occurrence_is_void_idx ON experiment.planting_job_occurrence USING btree (is_void);
CREATE INDEX planting_job_occurrence_modifier_id_idx ON experiment.planting_job_occurrence USING btree (modifier_id);

-- Add table triggers
CREATE trigger planting_job_occurrence_event_log_tgr BEFORE
INSERT 
    OR
UPDATE 
    ON
    experiment.planting_job_occurrence for each row execute function platform.log_record_event();

--  Add foreign keys
ALTER TABLE experiment.planting_job_occurrence ADD CONSTRAINT planting_job_occurrence_planting_job_id_fkey FOREIGN KEY (planting_job_id) REFERENCES experiment.planting_job(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_occurrence ADD CONSTRAINT planting_job_occurrence_occurrence_id_fkey FOREIGN KEY (occurrence_id) REFERENCES experiment.occurrence(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_occurrence ADD CONSTRAINT planting_job_occurrence_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job_occurrence ADD CONSTRAINT planting_job_occurrence_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;



--rollback DROP TABLE experiment.planting_job_occurrence;