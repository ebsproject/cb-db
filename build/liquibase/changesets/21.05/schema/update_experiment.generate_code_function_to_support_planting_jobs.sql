--liquibase formatted sql

--changeset postgres:update_experiment.generate_code_function_to_support_planting_jobs context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-347 Update experiment.generate_code function to support planting jobs



-- update experiment.generate_code function
DROP FUNCTION experiment.generate_code(varchar, varchar);

CREATE OR REPLACE FUNCTION experiment.generate_code(
    entity character varying,
    custom_prefix character varying DEFAULT NULL::character varying,
    custom_value character varying DEFAULT NULL::character varying,
    custom_digit integer DEFAULT NULL::integer,
    OUT code character varying
)
    RETURNS character varying
    LANGUAGE plpgsql
AS $function$
DECLARE
    seq_id bigint;
    digit_count integer DEFAULT 8;
    entity_code varchar;
BEGIN
    SELECT nextval('experiment.' || lower(entity) || '_code_seq') INTO seq_id;
    
    IF (upper(entity) = 'EXPERIMENT') THEN
        entity_code := 'EXP';
    ELSIF (upper(entity) = 'ENTRY_LIST') THEN
        entity_code := 'ENTLIST';
    ELSIF (upper(entity) = 'LOCATION') THEN
        entity_code := 'LOC';
    ELSIF (upper(entity) = 'OCCURRENCE') THEN
        entity_code := 'OCC';
    ELSIF (upper(entity) = 'PLANTING_JOB') THEN
        entity_code := 'PJOB';
        digit_count := 6;
    END IF;
    
    IF (custom_digit IS NOT NULL) THEN
        digit_count := custom_digit;
    END IF;
    
    IF (entity_code IS NOT NULL) THEN
        IF (custom_value IS NULL) THEN
            code := entity_code || lpad(seq_id::varchar, digit_count, '0');
        ELSE
            code := entity_code || custom_value || lpad(seq_id::varchar, digit_count, '0');
        END IF;
    ELSE
        code := NULL;
    END IF;
END; $function$
;



-- revert changes
--rollback DROP FUNCTION experiment.generate_code(varchar, varchar, varchar, integer);
--rollback 
--rollback CREATE OR REPLACE FUNCTION experiment.generate_code(entity character varying, custom_value character varying DEFAULT NULL::character varying, OUT code character varying)
--rollback     RETURNS character varying
--rollback     LANGUAGE plpgsql
--rollback AS $function$
--rollback DECLARE
--rollback     seq_id bigint;
--rollback     entity_code varchar;
--rollback BEGIN
--rollback     SELECT nextval('experiment.' || lower(entity) || '_code_seq') INTO seq_id;
--rollback     
--rollback     IF (upper(entity) = 'EXPERIMENT') THEN
--rollback         entity_code := 'EXPT';
--rollback     ELSIF (upper(entity) = 'ENTRY_LIST') THEN
--rollback         entity_code := 'ENTLIST';
--rollback     ELSIF (upper(entity) = 'LOCATION') THEN
--rollback         entity_code := 'LOC';
--rollback     ELSIF (upper(entity) = 'OCCURRENCE') THEN
--rollback         entity_code := 'OCC';
--rollback     END IF;
--rollback     
--rollback     IF (entity_code IS NOT NULL) THEN
--rollback         IF (custom_value IS NULL) THEN
--rollback             code := entity_code || lpad(seq_id::varchar, 8, '0');
--rollback         ELSE
--rollback             code := entity_code || custom_value || lpad(seq_id::varchar, 8, '0');
--rollback         END IF;
--rollback     ELSE
--rollback         code := NULL;
--rollback     END IF;
--rollback END; $function$
--rollback ;
