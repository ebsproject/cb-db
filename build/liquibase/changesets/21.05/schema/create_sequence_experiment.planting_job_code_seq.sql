--liquibase formatted sql

--changeset postgres:create_sequence_experiment.planting_job_code_seq context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-347 Create sequence for planting job codes



-- create sequence experiment.planting_job_code_seq
CREATE SEQUENCE experiment.planting_job_code_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;



-- revert changes
--rollback DROP SEQUENCE experiment.planting_job_code_seq;
