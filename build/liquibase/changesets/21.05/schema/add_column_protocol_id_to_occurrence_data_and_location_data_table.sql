--liquibase formatted sql

--changeset postgres:add_column_protocol_id_to_occurrence_data_and_location_data_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-328 Add column protocol_id to occurrence_data and location_data table



-- Add protocol_id to experiment.occurrence_data
ALTER TABLE 
    experiment.occurrence_data 
ADD 
    protocol_id integer NULL,
ADD CONSTRAINT 
    occurrence_data_protocol_id_fk 
FOREIGN KEY 
    (protocol_id) 
REFERENCES 
    tenant.protocol(id) 
ON UPDATE 
    CASCADE 
ON DELETE 
    RESTRICT;

COMMENT ON COLUMN 
    experiment.occurrence_data.protocol_id IS 'Protocol ID: Reference to the protocol of the occurrence [OCCDATA_PROTOCOL_ID]';



--rollback ALTER TABLE experiment.occurrence_data DROP COLUMN protocol_id;



-- Add protocol_id to experiment.location_data
ALTER TABLE 
    experiment.location_data 
ADD 
    protocol_id integer NULL,
ADD CONSTRAINT 
    location_data_protocol_id_fk 
FOREIGN KEY 
    (protocol_id) 
REFERENCES 
    tenant.protocol(id) 
ON UPDATE 
    CASCADE 
ON DELETE 
    RESTRICT;

COMMENT ON COLUMN 
    experiment.location_data.protocol_id IS 'Protocol ID: Reference to the protocol of the location [LOCDATA_PROTOCOL_ID]';



--rollback ALTER TABLE experiment.location_data DROP COLUMN protocol_id;