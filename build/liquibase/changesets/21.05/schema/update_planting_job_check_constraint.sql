--liquibase formatted sql

--changeset postgres:update_planting_job_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-369 Update planting job check constraint



ALTER TABLE experiment.planting_job DROP CONSTRAINT planting_job_status_chk;

ALTER TABLE experiment.planting_job
    ADD CONSTRAINT planting_job_status_chk CHECK (planting_job_status::text = ANY (ARRAY['draft'::text, 'ready for packing'::text, 'packing'::text, 'packed'::text, 'packing on hold'::text, 'packing cancelled'::text]));



--rollback ALTER TABLE experiment.planting_job DROP CONSTRAINT planting_job_status_chk;
--rollback ALTER TABLE experiment.planting_job
--rollback     ADD CONSTRAINT planting_job_status_chk CHECK (planting_job_status::text = ANY (ARRAY['ready for packing'::text, 'packing'::text, 'packed'::text, 'packing on hold'::text, 'packing cancelled'::text]));