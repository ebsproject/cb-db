--liquibase formatted sql

--changeset postgres:create_table_experiment.planting_job context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-329 Create table experiment.planting_job



-- Create table
CREATE TABLE experiment.planting_job (
	id serial NOT NULL,
    planting_job_code varchar(64) NOT NULL DEFAULT experiment.generate_code('planting_job'),
    planting_job_status varchar(32) NOT NULL,
    planting_job_type varchar(32) NOT NULL,
    planting_job_due_date date NULL,
    planting_job_instructions text NULL,
    facility_id integer NULL,
    creator_id integer NOT NULL,
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modifier_id integer NULL,
	modification_timestamp timestamp NULL,
	is_void bool NOT NULL DEFAULT false,
	notes text NULL,
	event_log jsonb NULL,
	CONSTRAINT planting_job_id_pk PRIMARY KEY (id),
    CONSTRAINT planting_job_status_chk CHECK (planting_job_status::text = ANY (ARRAY['ready for packing'::text, 'packing'::text, 'packed'::text, 'packing on hold'::text, 'packing cancelled'::text])),
    CONSTRAINT planting_job_type_chk CHECK (planting_job_type::text = ANY (ARRAY['packing job'::text, 'planting job'::text]))
);
CREATE INDEX planting_job_facility_id_idx ON experiment.planting_job USING btree (facility_id);
CREATE INDEX planting_job_creator_id_idx ON experiment.planting_job USING btree (creator_id);
CREATE INDEX planting_job_is_void_idx ON experiment.planting_job USING btree (is_void);
CREATE INDEX planting_job_modifier_id_idx ON experiment.planting_job USING btree (modifier_id);

-- Add table triggers
CREATE trigger planting_job_event_log_tgr BEFORE
INSERT 
    OR
UPDATE 
    ON
    experiment.planting_job for each row execute function platform.log_record_event();

--  Add foreign keys
ALTER TABLE experiment.planting_job ADD CONSTRAINT planting_job_facility_id_fkey FOREIGN KEY (facility_id) REFERENCES place.facility(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job ADD CONSTRAINT planting_job_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE experiment.planting_job ADD CONSTRAINT planting_job_modifier_id_fkey FOREIGN KEY (modifier_id) REFERENCES tenant.person(id) ON DELETE RESTRICT ON UPDATE CASCADE;



--rollback DROP TABLE experiment.planting_job;