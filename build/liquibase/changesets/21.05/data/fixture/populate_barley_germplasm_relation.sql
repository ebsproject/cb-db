--liquibase formatted sql

--changeset postgres:populate_barley_germplasm_relation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.germplasm_relation



-- populate barley germplasm relations
INSERT INTO
    germplasm.germplasm_relation (
        parent_germplasm_id, child_germplasm_id, order_number, creator_id
    )
SELECT
    pge.id AS parent_germplasm_id,
    cge.id AS child_germplasm_id,
    ROW_NUMBER() OVER (PARTITION BY pge.id, crs.id ORDER BY cge.id) AS order_number,
    cge.creator_id
FROM
    germplasm.cross AS crs
    JOIN germplasm.cross_parent AS crspar
        ON crs.id = crspar.cross_id
    JOIN germplasm.germplasm AS cge
        ON crs.cross_name = cge.parentage
    JOIN germplasm.germplasm AS pge
        ON pge.id = crspar.germplasm_id
    JOIN tenant.crop AS crop
        ON crop.id = cge.crop_id
WHERE
    crop.crop_code = 'BARLEY'
ORDER BY
    child_germplasm_id ASC,
    order_number ASC
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm_relation AS gerel
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     gerel.child_germplasm_id = ge.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
