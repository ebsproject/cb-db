--liquibase formatted sql

--changeset postgres:update_data_qc_code_values_from_g_to_q_in_germplasm.cross_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-341 Update data_qc_code values from G to Q in germplasm.cross_data



UPDATE germplasm.cross_data SET data_qc_code = 'Q' WHERE data_qc_code = 'G';



-- rollback
--rollback UPDATE germplasm.cross_data SET data_qc_code = 'G' WHERE data_qc_code = 'Q';