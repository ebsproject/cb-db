--liquibase formatted sql

--changeset postgres:create_btp_team_in_tenant_team context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP_TEAM in tenant.team



-- create BTP_TEAM team
INSERT INTO tenant.team
    (team_code,team_name,creator_id)
VALUES
    ('BTP_TEAM','BTP Breeding Team',1)
;

-- update sequence
SELECT SETVAL('tenant.team_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.team;



-- revert changes
--rollback DELETE FROM tenant.team WHERE team_code = 'BTP_TEAM';

--rollback SELECT SETVAL('tenant.team_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.team;
