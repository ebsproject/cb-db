--liquibase formatted sql

--changeset postgres:update_geospatial_object_code_sites_to_be_different_from_their_origin_site_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-336 Update geospatial_object_code sites to be different from their origin_site_code



WITH t1 AS (
    SELECT 
        id,
        geospatial_object_code geocode,
        geospatial_object_name geoname
    FROM 
        place.geospatial_object 
    WHERE 
        geospatial_object_code 
    IN
        (
            'MXI', 
            'NJ', 
            'HY', 
            'HA', 
            'AF', 
            'TL', 
            'KI', 
            'TO', 
            'EB', 
            'CO'
        )
), t2 AS (
    SELECT
        t.id,
        t.geocode,
        t.geoname,
        t.geocode||upper(right(t.geoname,1)) AS new_geocode
    FROM 
        t1 t
)
UPDATE place.geospatial_object pg SET geospatial_object_code = t2.new_geocode FROM t2 WHERE pg.id = t2.id;



--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         id,
--rollback         geospatial_object_code geocode,
--rollback         geospatial_object_name geoname
--rollback     FROM 
--rollback         place.geospatial_object 
--rollback     WHERE 
--rollback         geospatial_object_code 
--rollback     IN
--rollback         (
--rollback             'MXII',
--rollback             'NJO',
--rollback             'HYD',
--rollback             'HAE',
--rollback             'AFA',
--rollback             'TLN',
--rollback             'KIO',
--rollback             'TOA',
--rollback             'EBN',
--rollback             'CON'
--rollback         )
--rollback ), t2 AS (
--rollback     SELECT
--rollback         t.id,
--rollback         t.geocode,
--rollback         t.geoname,
--rollback         substr(t.geocode, 0, LENGTH(t.geocode)) AS old_geocode
--rollback     FROM 
--rollback         t1 t
--rollback )
--rollback UPDATE place.geospatial_object pg SET geospatial_object_code = t2.old_geocode FROM t2 WHERE pg.id = t2.id;