--liquibase formatted sql

--changeset postgres:update_parentage_of_cml_coded_germplasm_to_unknown context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-373 Update parentage of CML coded germplasm to Unknown



-- set to Unknown parentage all germplasm that has CML_code as name type
UPDATE
    germplasm.germplasm AS ge
SET
    parentage = 'Unknown'
WHERE
    ge.germplasm_name_type = 'CML_code'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.germplasm AS ge
--rollback SET
--rollback     parentage = t.parentage
--rollback FROM (
--rollback         VALUES
--rollback         ('CML547', 'DRB'),
--rollback         ('CML204', 'XGG893/YLP564'),
--rollback         ('CML442', 'XGG379/YLP609'),
--rollback         ('CML444', 'XGG496/YLP431'),
--rollback         ('CML463', 'G9A-C6'),
--rollback         ('CML464', 'XGG795/YLP891'),
--rollback         ('CML494', 'XGG725/YLP939'),
--rollback         ('CML495', 'PNVABCOSD/NPH-28-1'),
--rollback         ('CML536', 'XGG491/YLP342'),
--rollback         ('CML539', 'XGG523/YLP492'),
--rollback         ('CML543', 'XGG661/YLP665')
--rollback     ) AS t (
--rollback         designation, parentage
--rollback     )
--rollback WHERE
--rollback     ge.designation = t.designation
--rollback ;
