--liquibase formatted sql

--changeset postgres:populate_barley_package_logs context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.package_log



-- populate barley package logs
INSERT INTO
    germplasm.package_log (
        package_id, package_quantity, package_unit, package_transaction_type, creator_id
    )
SELECT
    pkg.id AS package_id,
    pkg.package_quantity,
    pkg.package_unit,
    'deposit' AS package_transaction_type,
    pkg.creator_id
FROM
    germplasm.package AS pkg
    JOIN germplasm.seed AS sd
        ON sd.id = pkg.seed_id
    JOIN germplasm.germplasm AS ge
        ON ge.id = sd.germplasm_id
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
WHERE
    crop.crop_code = 'BARLEY'
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package_log AS pkglog
--rollback USING
--rollback     germplasm.package AS pkg
--rollback     JOIN germplasm.seed AS sd
--rollback         ON sd.id = pkg.seed_id
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.id = sd.germplasm_id
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     pkglog.package_id = pkg.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
