--liquibase formatted sql

--changeset postgres:update_plot_type_column_values_to_plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-397 Update plot_type column values to plot



-- update plot_type column
UPDATE
    experiment.plot AS plot
SET
    plot_type = 'plot'
WHERE
    plot_type = 'plotType'
;



-- revert changes
--rollback UPDATE
--rollback     experiment.plot AS plot
--rollback SET
--rollback     plot_type = 'plotType'
--rollback FROM
--rollback     experiment.occurrence AS occ
--rollback WHERE
--rollback     occ.id = plot.occurrence_id
--rollback     AND occ.occurrence_name IN (
--rollback         'IRSEA-AYT-2021-DS-1_OCC1',
--rollback         'AF2021_001#OCC-01',
--rollback         'AF2021_001#OCC-02',
--rollback         'AF2021_001#OCC-03',
--rollback         'AF2021_002#OCC-01',
--rollback         'AF2021_002#OCC-02',
--rollback         'AF2021_002#OCC-03'
--rollback     )
--rollback ;
    