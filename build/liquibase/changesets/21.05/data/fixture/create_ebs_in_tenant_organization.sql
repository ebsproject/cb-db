--liquibase formatted sql

--changeset postgres:create_ebs_in_tenant_organization context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create EBS in tenant.organization



-- create EBS organization
INSERT INTO tenant.organization
    (organization_code,organization_name,description,creator_id)
VALUES
    ('EBS','Enterprise Breeding System',NULL,1)
;

-- update sequence
SELECT SETVAL('tenant.organization_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.organization;



-- revert changes
--rollback DELETE FROM tenant.organization WHERE organization_code = 'EBS';

--rollback SELECT SETVAL('tenant.organization_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.organization;
