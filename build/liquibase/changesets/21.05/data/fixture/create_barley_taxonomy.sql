--liquibase formatted sql

--changeset postgres:create_barley_taxonomy context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Create barley in germplasm.taxonomy



-- create barley taxonomy
INSERT INTO germplasm.taxonomy
    (taxon_id,taxonomy_name,description,creator_id,crop_id)
VALUES
    ('4513','Hordeum vulgare  L.',NULL,1,(SELECT id FROM tenant.crop WHERE crop_code = 'BARLEY'))
;



-- revert changes
--rollback DELETE FROM germplasm.taxonomy WHERE taxon_id = '4513';
