--liquibase formatted sql

--changeset postgres:void_duplicate_records_in_experiment.occurrence_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-365 Void duplicate records in experiment.occurrence_data



WITH t1 AS (
    SELECT 
        count(1),
        occurrence_id, 
        variable_id,
        array_agg(id order by id) duplicate
    FROM 
        experiment.occurrence_data 
    WHERE 
        is_void=false
    AND
        variable_id 
    IN
        (
            SELECT 
                id
            FROM
                master.variable
            WHERE
                abbrev='CONTCT_PERSON_CONT'
            AND
                is_void=false
        ) 
    GROUP BY 
        (occurrence_id,variable_id)
    HAVING COUNT(1) > 1
    ORDER BY 
        occurrence_id
), t2 AS (
    SELECT 
        tdp AS id
    FROM
        t1 t, unnest(t.duplicate) tdp
     WHERE
         tdp 
     NOT IN 
         (t.duplicate[1])
)
UPDATE 
    experiment.occurrence_data AS eo
SET
    is_void=true
FROM
    t2 AS t
WHERE
    eo.id = t.id
;



--rollback WITH t1 AS (
--rollback 	SELECT 
--rollback 		count(1),
--rollback 		occurrence_id, 
--rollback 		variable_id,
--rollback 		array_agg(id order by id) duplicate
--rollback 	FROM 
--rollback 		experiment.occurrence_data 
--rollback 	WHERE 
--rollback 		is_void=true
--rollback  AND
--rollback          variable_id 
--rollback      IN
--rollback          (
--rollback              SELECT 
--rollback                  id
--rollback              FROM
--rollback                  master.variable
--rollback              WHERE
--rollback                  abbrev='CONTCT_PERSON_CONT'
--rollback              AND
--rollback                  is_void=false
--rollback          ) 
--rollback 	GROUP BY 
--rollback 		(occurrence_id,variable_id)
--rollback 	ORDER BY 
--rollback 		occurrence_id
--rollback ), t2 AS (
--rollback 	SELECT 
--rollback 		tdp AS id
--rollback 	FROM
--rollback 		t1 t, unnest(t.duplicate) tdp
--rollback )
--rollback UPDATE 
--rollback     experiment.occurrence_data AS eo
--rollback SET
--rollback     is_void=false
--rollback FROM
--rollback     t2 AS t
--rollback WHERE
--rollback     eo.id = t.id
--rollback ;