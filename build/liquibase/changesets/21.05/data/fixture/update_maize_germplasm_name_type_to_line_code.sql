--liquibase formatted sql

--changeset postgres:update_maize_germplasm_name_type_to_line_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-373 Update maize germplasm_name_type to line_code



-- update germplasm_name_type from line_code_1 to line_code
UPDATE
    germplasm.germplasm AS ge
SET
    germplasm_name_type = 'line_code'
WHERE
    ge.germplasm_name_type = 'line_code_1'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.germplasm AS ge
--rollback SET
--rollback     germplasm_name_type = 'line_code_1'
--rollback WHERE
--rollback     ge.designation IN (
--rollback         'MKL1500186',
--rollback         'MKL1500215',
--rollback         'MKL1500099',
--rollback         'MKL1500041',
--rollback         'MKL1500261',
--rollback         'MKL150334',
--rollback         'MKL150339',
--rollback         'MKL150342',
--rollback         'MKL150390',
--rollback         'MKL150399',
--rollback         'MKL150421',
--rollback         'MKL150431',
--rollback         'MKL150694',
--rollback         'MKL150893',
--rollback         'MKL150961',
--rollback         'MKL150968',
--rollback         'MKL151030',
--rollback         'MKL151147',
--rollback         'MKL151294',
--rollback         'MKL151780',
--rollback         'MKL151787',
--rollback         'MKL151822',
--rollback         'MKL151830',
--rollback         'MKL151839',
--rollback         'MKL151841',
--rollback         'MKL151845',
--rollback         'MKL151851',
--rollback         'MKL151920',
--rollback         'MKL151967',
--rollback         'MKL151969',
--rollback         'MKL151972',
--rollback         'MKL151973',
--rollback         'MKL152043',
--rollback         'MKL152076',
--rollback         'MKL152092',
--rollback         'MKL152098',
--rollback         'MKL152140',
--rollback         'MKL152144',
--rollback         'MKL152149',
--rollback         'MKL152366',
--rollback         'MKL152395',
--rollback         'MKL152503',
--rollback         'MKL152554',
--rollback         'MKL152561',
--rollback         'MKL152563',
--rollback         'MKL152579',
--rollback         'MKL152591',
--rollback         'MKL152601',
--rollback         'MKL152616',
--rollback         'MKL152617',
--rollback         'MKL152653',
--rollback         'MKL152658',
--rollback         'MKL152682',
--rollback         'MKL152748',
--rollback         'MKL152769',
--rollback         'MKL152773',
--rollback         'MKL152777',
--rollback         'MKL152778',
--rollback         'MKL152811',
--rollback         'MKL152847',
--rollback         'MKL152857',
--rollback         'MKL152862',
--rollback         'MKL152921',
--rollback         'MKL152929',
--rollback         'MKL152976',
--rollback         'MKL152994',
--rollback         'MKL153050',
--rollback         'MKL153193',
--rollback         'MKL153222',
--rollback         'MKL153223',
--rollback         'MKL153236',
--rollback         'MKL153907',
--rollback         'MKL153908',
--rollback         'MKL153939',
--rollback         'MKL153946',
--rollback         'MKL153984',
--rollback         'MKL154043',
--rollback         'MKL154102',
--rollback         'MKL154050',
--rollback         'MKL154091'
--rollback     )
--rollback ;
