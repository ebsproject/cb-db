--liquibase formatted sql

--changeset postgres:create_btcp_in_tenant_crop_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTCP in tenant.crop_program



-- create BTCP crop program
INSERT INTO tenant.crop_program
    (crop_program_code,crop_program_name,description,creator_id,organization_id,crop_id)
VALUES
    ('BTCP','Barley Test Crop Program',NULL,1,(SELECT id FROM tenant.organization WHERE organization_code = 'EBS'),(SELECT id FROM tenant.crop WHERE crop_code = 'BARLEY'))
;

-- update sequence
SELECT SETVAL('tenant.crop_program_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.crop_program;



-- revert changes
--rollback DELETE FROM tenant.crop_program WHERE crop_program_code = 'BTCP';

--rollback SELECT SETVAL('tenant.crop_program_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.crop_program;
