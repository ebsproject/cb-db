--liquibase formatted sql

--changeset postgres:populate_planting_job_and_planting_job_occurrence_tables context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-335 Populate planting_job and planting_job_occurrence tables



-- create planting_job records
INSERT INTO
    experiment.planting_job
        (planting_job_code, planting_job_status, planting_job_type, creator_id, notes)
SELECT
    t.planting_job_code,
    t.planting_job_status,
    'packing job' AS planting_job_type,
    (SELECT id FROM tenant.person WHERE username = t.username) AS creator_id,
    'DB-335 21.05' AS notes
FROM (
    VALUES
        ((SELECT experiment.generate_code('planting_job')),'ready for packing', 'a.carlson'),
        ((SELECT experiment.generate_code('planting_job')),'packing', 'k.khadija'),
        ((SELECT experiment.generate_code('planting_job')),'packed', 'h.tamsin'),
        ((SELECT experiment.generate_code('planting_job')),'packing on hold', 'a.ramos'),
        ((SELECT experiment.generate_code('planting_job')),'packing cancelled', 'elly.donelly')
    ) AS t (
        planting_job_code, planting_job_status, username
    )
;


-- create planting_job_occurrence records
INSERT INTO
    experiment.planting_job_occurrence
        (planting_job_id, occurrence_id, seeds_per_envelope, envelopes_per_plot, creator_id)
SELECT
    pjob.id,
    occ.id,
    t.seeds_per_envelope,
    t.envelopes_per_plot,
    (SELECT id FROM tenant.person WHERE username = t.username) AS pjo_creator_id
FROM (
    VALUES
        ('ready for packing', 'IRSEA-IRRIHQ-SEM-2015-DS-1', 58, 50, 'a.carlson'),
        ('ready for packing', 'IRSEA-IRRIHQ-SEM-2015-DS-2', 27, 10, 'a.carlson'),
        ('ready for packing', 'IRSEA-IRRIHQ-SEM-2015-DS-3', 77, 61, 'a.carlson'),
        ('packing', 'IRSEA-IRRIHQ-PN-2015-DS-1', 34, 79, 'k.khadija'),
        ('packing', 'IRSEA-IRRIHQ-PN-2015-DS-2', 1, 71, 'k.khadija'),
        ('packing', 'IRSEA-IRRIHQ-PN-2015-DS-3', 40, 11, 'k.khadija'),
        ('packed', 'IRSEA-IRRIHQ-PN-2014-DS-1', 84, 87, 'h.tamsin'),
        ('packed', 'IRSEA-IRRIHQ-PN-2014-DS-2', 8, 34, 'h.tamsin'),
        ('packed', 'IRSEA-IRRIHQ-PN-2014-DS-3', 55, 76, 'h.tamsin'),
        ('packing on hold', 'IRSEA-IRRIHQ-OYT-2015-WS-3', 13, 14, 'a.ramos'),
        ('packing on hold', 'IRSEA-IRRIHQ-OYT-2015-WS-5', 46, 72, 'a.ramos'),
        ('packing on hold', 'IRSEA-IRRIHQ-OYT-2015-WS-6', 77, 42, 'a.ramos'),
        ('packing cancelled', 'IRSEA-IRRIHQ-IYT-2016-DS-2', 56, 7, 'elly.donelly'),
        ('packing cancelled', 'IRSEA-IRRIHQ-IYT-2016-DS-3', 82, 20, 'elly.donelly'),
        ('packing cancelled', 'IRSEA-IRRIHQ-IYT-2016-DS-4', 48, 86, 'elly.donelly')
    ) AS t (
        planting_job_status, occurrence_code, seeds_per_envelope, envelopes_per_plot, username
    )
    JOIN experiment.planting_job AS pjob
        ON pjob.notes = 'DB-335 21.05'
        AND pjob.planting_job_status = t.planting_job_status
    JOIN experiment.occurrence AS occ
        ON occ.occurrence_code = t.occurrence_code
GROUP BY
    pjob.id, occ.id, t.seeds_per_envelope, t.envelopes_per_plot, pjo_creator_id
;


-- update related occurrences to append planting_job_status to occurrence_status
UPDATE
    experiment.occurrence AS occ
SET
    occurrence_status = t1.occ_status
FROM (
    SELECT
        pj.id,
        concat(
            occ.occurrence_status,
            ';',
            pj.planting_job_status
        ) AS occ_status,
        pjo.id AS pjo_id,
        pjo.occurrence_id AS pjo_occ_id
    FROM
        experiment.planting_job AS pj
    INNER JOIN experiment.planting_job_occurrence AS pjo
        ON pjo.planting_job_id = pj.id
    INNER JOIN experiment.occurrence AS occ
        ON occ.id = pjo.occurrence_id
    WHERE
        occ.occurrence_status not ilike '%;' || pj.planting_job_status || '%'
    ) AS t1
WHERE
    occ.id = t1.pjo_occ_id
;



-- rollback
--rollback UPDATE
--rollback     experiment.occurrence AS occ
--rollback SET
--rollback     occurrence_status = t1.occ_status
--rollback FROM (
--rollback     SELECT
--rollback         pj.id,
--rollback         replace(
--rollback             occ.occurrence_status,
--rollback             concat(';',pj.planting_job_status),
--rollback             ''
--rollback         ) AS occ_status,
--rollback         pjo.id AS pjo_id,
--rollback         pjo.occurrence_id AS pjo_occ_id
--rollback     FROM
--rollback         experiment.planting_job AS pj
--rollback     INNER JOIN experiment.planting_job_occurrence AS pjo
--rollback         ON pjo.planting_job_id = pj.id
--rollback     INNER JOIN experiment.occurrence AS occ
--rollback         ON occ.id = pjo.occurrence_id
--rollback     ) AS t1
--rollback WHERE
--rollback     occ.id = t1.pjo_occ_id
--rollback ;
--rollback 
--rollback DELETE FROM
--rollback     experiment.planting_job_occurrence
--rollback WHERE
--rollback     id IN (
--rollback         SELECT
--rollback             id
--rollback         FROM
--rollback             experiment.planting_job_occurrence
--rollback         WHERE
--rollback             planting_job_id IN (
--rollback                 SELECT
--rollback                     id
--rollback                 FROM
--rollback                     experiment.planting_job
--rollback                 WHERE
--rollback                     notes ILIKE '%DB-335 21.05%'
--rollback             )
--rollback     )
--rollback ;
--rollback 
--rollback 
--rollback DELETE FROM
--rollback     experiment.planting_job
--rollback WHERE
--rollback     notes ILIKE '%DB-335 21.05%'
--rollback ;