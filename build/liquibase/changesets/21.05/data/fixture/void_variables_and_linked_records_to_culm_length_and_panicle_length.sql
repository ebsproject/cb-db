--liquibase formatted sql

--changeset postgres:void_variables_and_linked_records_to_culm_length_and_panicle_length context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-412 Void variables and linked records to Culm Length and Panicle Length



-- void variables
UPDATE
    master.variable
SET
    is_void = TRUE
WHERE
    abbrev IN (
        'PNL1_CONT',
        'PNL2_CONT',
        'PNL3_CONT',
        'PNL4_CONT',
        'CML1_CONT',
        'CML2_CONT',
        'CML3_CONT',
        'CML4_CONT'
    )
;



-- rollback
--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     is_void = FALSE
--rollback WHERE
--rollback     abbrev IN (
--rollback         'PNL1_CONT',
--rollback         'PNL2_CONT',
--rollback         'PNL3_CONT',
--rollback         'PNL4_CONT',
--rollback         'CML1_CONT',
--rollback         'CML2_CONT',
--rollback         'CML3_CONT',
--rollback         'CML4_CONT'
--rollback     )
--rollback ;