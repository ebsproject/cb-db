--liquibase formatted sql

--changeset postgres:void_records_in_master.formula context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-431 Void records in master.formula



-- void variables in master.formula
UPDATE
    master.formula
SET
    is_void = TRUE
WHERE 
    result_variable_id 
IN (
    SELECT id FROM master.variable WHERE abbrev IN (
        'HT1_CONT',
        'HT2_CONT',
        'HT3_CONT',
        'HT4_CONT',
        'HT5_CONT',
        'HT6_CONT'
    )
);

-- void void records linked to variables
UPDATE
    master.formula_parameter
SET
    is_void = TRUE
WHERE
    formula_id IN (
        SELECT id FROM master.formula WHERE result_variable_id IN (
            SELECT id FROM master.variable WHERE abbrev IN (
                'HT1_CONT',
                'HT2_CONT',
                'HT3_CONT',
                'HT4_CONT',
                'HT5_CONT',
                'HT6_CONT'
            )
        )
    )
;



-- rollback
--rollback UPDATE
--rollback     master.formula
--rollback SET
--rollback     is_void = FALSE
--rollback WHERE 
--rollback     result_variable_id 
--rollback IN (
--rollback     SELECT id FROM master.variable WHERE abbrev IN (
--rollback         'HT1_CONT',
--rollback         'HT2_CONT',
--rollback         'HT3_CONT',
--rollback         'HT4_CONT',
--rollback         'HT5_CONT',
--rollback         'HT6_CONT'
--rollback     )
--rollback );
--rollback 
--rollback UPDATE
--rollback     master.formula_parameter
--rollback SET
--rollback     is_void = FALSE
--rollback WHERE
--rollback     formula_id IN (
--rollback         SELECT id FROM master.formula WHERE result_variable_id IN (
--rollback             SELECT id FROM master.variable WHERE abbrev IN (
--rollback                 'HT1_CONT',
--rollback                 'HT2_CONT',
--rollback                 'HT3_CONT',
--rollback                 'HT4_CONT',
--rollback                 'HT5_CONT',
--rollback                 'HT6_CONT'
--rollback             )
--rollback         )
--rollback     )
--rollback ;