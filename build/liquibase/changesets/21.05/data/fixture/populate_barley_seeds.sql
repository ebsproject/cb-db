--liquibase formatted sql

--changeset postgres:populate_barley_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.seed



-- populate barley seeds
INSERT INTO
    germplasm.seed (
        seed_code,seed_name,harvest_date,harvest_method,germplasm_id,program_id,
        source_experiment_id,source_entry_id,source_occurrence_id,source_location_id,source_plot_id,cross_id,creator_id
    )
SELECT
    germplasm.generate_code('seed') AS seed_code,
    'SD-' || ge.designation AS seed_name,
    NULL AS harvest_date,
    NULL AS harvest_method,
    ge.id AS germplasm_id,
    prog.id AS program_id,
    NULL AS source_experiment_id,
    NULL AS source_entry_id,
    NULL AS source_occurrence_id,
    NULL AS source_location_id,
    NULL AS source_plot_id,
    NULL AS cross_id,
    ge.creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'BTP'
WHERE
    crop.crop_code = 'BARLEY'
;


-- revert changes
--rollback DELETE FROM
--rollback     germplasm.seed AS sd
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     sd.germplasm_id = ge.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
