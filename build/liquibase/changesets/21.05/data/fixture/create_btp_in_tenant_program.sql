--liquibase formatted sql

--changeset postgres:create_btp_in_tenant_program context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP to tenant.program



-- create BTP program
INSERT INTO tenant.program
    (program_code,program_name,program_type,program_status,description,crop_program_id,creator_id)
VALUES
    ('BTP','Barley Test Program','breeding','active',NULL,(SELECT id FROM tenant.crop_program WHERE crop_program_code = 'BTCP'),1)
;

-- update sequence
SELECT SETVAL('tenant.program_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.program;



-- revert changes
--rollback DELETE FROM tenant.program WHERE program_code = 'BTP';

--rollback SELECT SETVAL('tenant.program_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.program;
