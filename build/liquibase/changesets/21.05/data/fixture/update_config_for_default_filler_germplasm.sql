--liquibase formatted sql

--changeset postgres:update_config_for_default_filler_germplasm_for_all_crops context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-348 Update config for DEFAULT_FILLER_GERMPLASM for all crops



-- update config for DEFAULT_FILLER_GERMPLASM for all crops
WITH t_config AS (
    SELECT
        crop.id AS crop_id,
        min(ge.id) AS germplasm_id
    FROM
        germplasm.germplasm AS ge
        JOIN tenant.crop AS crop
            ON crop.id = ge.crop_id
    WHERE
        ge.germplasm_state = 'fixed'
        AND ge.is_void = FALSE
    GROUP BY
        crop.id
), t_config_value AS (
    SELECT
        ('{' || string_agg(format($$"%s": {"germplasm_id": "%s"}$$, t.crop_id, t.germplasm_id), ', ') || '}')::jsonb AS config_value
    FROM
        t_config AS t
)    
UPDATE
    platform.config
SET
    config_value = t.config_value
FROM
    t_config_value AS t
WHERE
    abbrev = 'DEFAULT_FILLER_GERMPLASM'
;



-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = '{
--rollback         "0": {
--rollback             "germplasm_id": "0"
--rollback         }
--rollback     }'
--rollback WHERE
--rollback     abbrev = 'DEFAULT_FILLER_GERMPLASM'
--rollback ;
