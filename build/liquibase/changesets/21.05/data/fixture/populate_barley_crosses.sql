--liquibase formatted sql

--changeset postgres:populate_barley_crosses context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.cross



-- populate barley crosses
WITH t_family AS (
    SELECT
        ge.parentage,
        array_agg(ge.designation) AS child_designation_list
    FROM
        germplasm.germplasm AS ge
        JOIN tenant.crop AS crop
            ON ge.crop_id = crop.id
    WHERE
        crop.crop_code = 'BARLEY'
        AND ge.parentage <> '?/?'
    GROUP BY
        ge.parentage
)
INSERT INTO
    germplasm.cross (
        cross_name,cross_method,experiment_id,creator_id,is_method_autofilled,harvest_status
    )
SELECT
    t.parentage AS cross_name,
    'unknown' AS cross_method,
    NULL AS experiment_id,
    1 AS creator_id,
    TRUE AS is_method_autofilled,
    'NO_HARVEST' AS harvest_status
FROM
    t_family AS t
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross AS crs
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     crs.cross_name = ge.parentage
--rollback     AND ge.parentage <> '?/?'
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
