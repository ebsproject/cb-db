--liquibase formatted sql

--changeset postgres:create_barley_in_tenant_crop context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BARLEY in tenant.crop



-- create BARLEY crop
INSERT INTO tenant.crop
    (crop_code,crop_name,description,creator_id)
VALUES
    ('BARLEY','Barley','Barley crop',1)
;

-- update sequence
SELECT SETVAL('tenant.crop_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.crop;



-- revert changes
--rollback DELETE FROM tenant.crop WHERE crop_code = 'BARLEY';

--rollback SELECT SETVAL('tenant.crop_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.crop;
