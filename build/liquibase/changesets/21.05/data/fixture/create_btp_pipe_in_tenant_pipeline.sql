--liquibase formatted sql

--changeset postgres:create_btp_pipe_in_tenant_pipeline context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP_PIPE in tenant.pipeline



-- create BTP_PIPE pipeline
INSERT INTO tenant.pipeline
    (pipeline_code,pipeline_name,pipeline_status,description,creator_id)
VALUES
    ('BTP_PIPE','Barley Test Program','active',NULL,1)
;

-- update sequence
SELECT SETVAL('tenant.pipeline_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.pipeline;



-- revert changes
--rollback DELETE FROM tenant.pipeline WHERE pipeline_code = 'BTP_PIPE';

--rollback SELECT SETVAL('tenant.pipeline_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.pipeline;
