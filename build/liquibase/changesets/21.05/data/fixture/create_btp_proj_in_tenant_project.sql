--liquibase formatted sql

--changeset postgres:create_btp_proj_in_tenant_project context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP_PROJ to tenant.project



-- create BTP_PROJ project
INSERT INTO tenant.project
    (project_code,project_name,project_status,description,program_id,pipeline_id,leader_id,creator_id)
VALUES
    ('BTP_PROJ','BTP Project','active',NULL,(SELECT id FROM tenant.program WHERE program_code = 'BTP'),(SELECT id FROM tenant.pipeline WHERE pipeline_code = 'BTP_PIPE'),1,1)
;

-- update sequence
SELECT SETVAL('tenant.project_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.project;



-- revert changes
--rollback DELETE FROM tenant.project WHERE project_code = 'BTP_PROJ';

--rollback SELECT SETVAL('tenant.project_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.project;
