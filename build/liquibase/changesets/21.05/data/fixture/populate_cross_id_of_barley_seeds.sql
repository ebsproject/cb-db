--liquibase formatted sql

--changeset postgres:populate_cross_id_of_barley_seeds context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate cross_id of barley in germplasm.seed



-- Populate cross_id column of barley seeds
UPDATE
    germplasm.seed AS sd
SET
    cross_id = crs.id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN germplasm.cross AS crs
        ON crs.cross_name = ge.parentage
WHERE
    sd.germplasm_id = ge.id
    AND crop.crop_code = 'BARLEY'
    AND sd.cross_id IS NULL
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.seed AS sd
--rollback SET
--rollback     cross_id = NULL
--rollback FROM
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback     JOIN germplasm.cross AS crs
--rollback         ON crs.cross_name = ge.parentage
--rollback WHERE
--rollback     sd.germplasm_id = ge.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback     AND sd.cross_id IS NOT NULL
--rollback ;
