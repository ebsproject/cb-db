--liquibase formatted sql

--changeset postgres:update_maize_germplasm_type_to_segregating context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-373 Update maize germplasm_type to segregating



-- update germplasm_type from segregating_line to segregating
UPDATE
    germplasm.germplasm AS ge
SET
    germplasm_type = 'segregating'
WHERE
    ge.germplasm_type = 'segregating_line'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.germplasm AS ge
--rollback SET
--rollback     germplasm_type = 'segregating_line'
--rollback WHERE
--rollback     ge.designation IN (
--rollback         '(FBM239/LLTR)-B-29-1',
--rollback         '(FBM239/LLTR)-B-55-1',
--rollback         '(FBM239/LLTR)-B-74-1',
--rollback         '(FBM239/LLTR)-B-79-1',
--rollback         '(FBM239/LLTR)-B-93-1',
--rollback         '(FBM239/LLTR)-B-97-1',
--rollback         '(FBM239/LLTR)-B-98-1',
--rollback         '(FBM239/RHHB9)-B-109-1',
--rollback         '(FBM239/RHHB9)-B-123-1',
--rollback         '(FBM239/RHHB9)-B-125-1',
--rollback         '(FBM239/RHHB9)-B-126-1',
--rollback         '(FBM239/RHHB9)-B-131-1',
--rollback         '(FBM239/RHHB9)-B-135-1',
--rollback         '(FBM239/RHHB9)-B-158-1',
--rollback         '(FBM239/RHHB9)-B-171-1',
--rollback         '(FBM239/RHM52)-B-134-1',
--rollback         '(FBM239/RHM52)-B-17-1',
--rollback         '(FBM239/RHM52)-B-39-1',
--rollback         '(FBM239/RHM52)-B-79-1',
--rollback         '(FBM239/RHT11)-B-101-1',
--rollback         '(FBM239/RHT11)-B-89-1',
--rollback         '(FBM239/RHT11)-B-107-1',
--rollback         '(FBM239/RHT11)-B-13-1',
--rollback         '(FBM239/RHT11)-B-22-1',
--rollback         '(FBM239/RHT11)-B-71-1',
--rollback         '(FBM239/RHT11)-B-82-1',
--rollback         '(HTL437/ABHB9)-B-108-1-1-B',
--rollback         '(HTL437/ABHB9)-B-119-1-1-B',
--rollback         '(HTL437/ABHB9)-B-120-1-1-B',
--rollback         '(HTL437/ABHB9)-B-168-1-1-B',
--rollback         '(HTL437/ABHB9)-B-168-1-2-B',
--rollback         '(HTL437/ABHB9)-B-17-1-2-B',
--rollback         '(HTL437/ABHB9)-B-183-1-2-B',
--rollback         '(HTL437/ABHB9)-B-31-1-1-B',
--rollback         '(HTL437/ABHB9)-B-31-1-2-B',
--rollback         '(HTL437/ABHB9)-B-3-1-1-B',
--rollback         '(HTL437/ABHB9)-B-73-1-2-B',
--rollback         '(HTL437/ABP38)-B-157-1-1-B',
--rollback         '(HTL437/ABP38)-B-157-1-2-B',
--rollback         '(HTL437/ABP38)-B-178-1-1-B',
--rollback         '(HTL437/ABP38)-B-178-1-2-B',
--rollback         '(HTL437/ABP38)-B-188-1-2-B',
--rollback         '(HTL437/ABP38)-B-189-1-2-B',
--rollback         '(HTL437/ABW52)-B-145-1-1-B',
--rollback         '(HTL437/ABW52)-B-17-1-2-B',
--rollback         '(HTL437/ABW52)-B-3-1-1-B',
--rollback         '(HTL437/ABW52)-B-3-1-2-B',
--rollback         '(HTL437/ABW52)-B-71-1-1-B',
--rollback         '(HTL437/ABW52//HTL437)-96-1-1-1-B',
--rollback         '(HTL437/ABW52//HTL437)-96-1-1-2-B',
--rollback         '(HTL437/ABW52//HTL437)-98-1-1-1-B',
--rollback         '(HTL437/ABW52//HTL437)-98-1-1-2-B',
--rollback         '(HTL437/RK132)-B-51-1-2-B',
--rollback         '(HTL437/RK132)-B-86-1-1-B',
--rollback         '(HTL437/RK132)-B-89-1-2-B',
--rollback         '(HTL437/RK132)-B-9-1-1-B',
--rollback         '(LTL812/ABT11)-B-16-1-1-B',
--rollback         '(LTL812/ABT11)-B-16-1-2-B',
--rollback         '(LTL812/ABT11)-B-51-1-2-B',
--rollback         '(LTL812/ABT11)-B-8-1-2-B',
--rollback         '(LTL812/ABW52)-B-113-1-2-B',
--rollback         '(LTL812/ABW52)-B-124-1-2-B',
--rollback         '(LTL812/ABW52)-B-25-1-1-B',
--rollback         '(LTL812/ABW52)-B-65-1-1-B',
--rollback         '(LTL812/ABW52)-B-65-1-2-B',
--rollback         '(LTL812/ABW52)-B-74-1-2-B'
--rollback     )
--rollback ;
