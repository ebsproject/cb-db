--liquibase formatted sql

--changeset postgres:curate_location_code context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-444 Curate location code



WITH t1 AS (
    SELECT 
        el.id,
        concat(pg.geospatial_object_code,'-',el.location_year,'-',ts.season_code,'-',el.location_number) new_loc_code
    FROM 
        experiment.location el
    JOIN	
        place.geospatial_object pg
    ON	
        pg.id = el.site_id
    JOIN
        tenant.season ts
    ON
        ts.id = el.season_id
    ORDER BY 
        el.id 
)
UPDATE 
    experiment.location el
SET
    location_code = t1.new_loc_code
    FROM t1
WHERE
    el.id = t1.id
;



--rollback UPDATE experiment.location SET location_code='RGA2013DS-1_10001' WHERE id=71;
--rollback UPDATE experiment.location SET location_code='RGA2013DS-2_10001' WHERE id=72;
--rollback UPDATE experiment.location SET location_code='RGA2013WS-1_10001' WHERE id=73;
--rollback UPDATE experiment.location SET location_code='RGA2013WS-2_10001' WHERE id=74;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-DS-8_10001' WHERE id=164;
--rollback UPDATE experiment.location SET location_code='F12014DS_B27' WHERE id=170;
--rollback UPDATE experiment.location SET location_code='2014DSBULK_602-610' WHERE id=205;
--rollback UPDATE experiment.location SET location_code='2014DS SPS_611' WHERE id=206;
--rollback UPDATE experiment.location SET location_code='2014DS LINEXTESTER_B37' WHERE id=228;
--rollback UPDATE experiment.location SET location_code='2014DSION_B36b2' WHERE id=230;
--rollback UPDATE experiment.location SET location_code='2014DSAWD_417' WHERE id=231;
--rollback UPDATE experiment.location SET location_code='2014DS_KOR_B36b1' WHERE id=232;
--rollback UPDATE experiment.location SET location_code='2014DS DSR_916' WHERE id=233;
--rollback UPDATE experiment.location SET location_code='2014DSC35A_C35a' WHERE id=234;
--rollback UPDATE experiment.location SET location_code='2014DSERA_C28' WHERE id=235;
--rollback UPDATE experiment.location SET location_code='2014DSGWAS_J3-J5' WHERE id=236;
--rollback UPDATE experiment.location SET location_code='2014DSPYT_B34' WHERE id=323;
--rollback UPDATE experiment.location SET location_code='HB2014DS_HB' WHERE id=340;
--rollback UPDATE experiment.location SET location_code='F12014WS_B27-B28' WHERE id=359;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-3_10001' WHERE id=375;
--rollback UPDATE experiment.location SET location_code='HB2014WS' WHERE id=378;
--rollback UPDATE experiment.location SET location_code='2014WSAYT_B36b ' WHERE id=385;
--rollback UPDATE experiment.location SET location_code='2014WSPYT_B34' WHERE id=390;
--rollback UPDATE experiment.location SET location_code='2014WS SPS_606B2-612B1' WHERE id=421;
--rollback UPDATE experiment.location SET location_code='2014WSERA_C28' WHERE id=463;
--rollback UPDATE experiment.location SET location_code='2014WS LXT_J5' WHERE id=473;
--rollback UPDATE experiment.location SET location_code='2014WSDSR_116-117' WHERE id=511;
--rollback UPDATE experiment.location SET location_code='2014WSION_J4b' WHERE id=524;
--rollback UPDATE experiment.location SET location_code='2014WSION_2_C37' WHERE id=557;
--rollback UPDATE experiment.location SET location_code='2014WSPYT_Pur_C37' WHERE id=698;
--rollback UPDATE experiment.location SET location_code='2014WSOYT_Pur_IRRI UJ-3' WHERE id=700;
--rollback UPDATE experiment.location SET location_code='2014WSOYT_C29' WHERE id=705;
--rollback UPDATE experiment.location SET location_code='2014DSOYT_C29' WHERE id=706;
--rollback UPDATE experiment.location SET location_code='2014WSBULK_603-606B1' WHERE id=714;
--rollback UPDATE experiment.location SET location_code='2014WS STU_B37' WHERE id=715;
--rollback UPDATE experiment.location SET location_code='2014WS SUB_J5' WHERE id=716;
--rollback UPDATE experiment.location SET location_code='2014DSOYT-BC_J6' WHERE id=720;
--rollback UPDATE experiment.location SET location_code='2014WSOYT-BC_J5' WHERE id=721;
--rollback UPDATE experiment.location SET location_code='2014DS LST_411-414a' WHERE id=737;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-4_10001' WHERE id=804;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2013-WS-3_10001' WHERE id=815;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-DS-12_10001' WHERE id=816;
--rollback UPDATE experiment.location SET location_code='2014WSOYT_MMR' WHERE id=821;
--rollback UPDATE experiment.location SET location_code='2014WSPYT_MMR' WHERE id=826;
--rollback UPDATE experiment.location SET location_code='HB2014WS_IRSEA_IRRI' WHERE id=836;
--rollback UPDATE experiment.location SET location_code='2014WS LST_IRRI' WHERE id=843;
--rollback UPDATE experiment.location SET location_code='F12015DS_IRSEA_IRRI' WHERE id=885;
--rollback UPDATE experiment.location SET location_code='2015DS_STU_IRRI' WHERE id=893;
--rollback UPDATE experiment.location SET location_code='2015DS_LxT2_IRRI' WHERE id=894;
--rollback UPDATE experiment.location SET location_code='2015DS_LxT1_IRRI' WHERE id=895;
--rollback UPDATE experiment.location SET location_code='2015DS_ION_IRRI' WHERE id=903;
--rollback UPDATE experiment.location SET location_code='2015DS_BCN_IRRI' WHERE id=907;
--rollback UPDATE experiment.location SET location_code='HB2015DS_IRSEA_IRRI' WHERE id=916;
--rollback UPDATE experiment.location SET location_code='2015DS LINE SELECTION_IRRI' WHERE id=919;
--rollback UPDATE experiment.location SET location_code='2015DS BMCE.PN_IRRI' WHERE id=1003;
--rollback UPDATE experiment.location SET location_code='2015DS BMCE.BLK_IRRI' WHERE id=1004;
--rollback UPDATE experiment.location SET location_code='2015DS SDLST_IRRI' WHERE id=1005;
--rollback UPDATE experiment.location SET location_code='2015DS SDBLK_IRRI' WHERE id=1015;
--rollback UPDATE experiment.location SET location_code='2015DS SDYT_IRRI' WHERE id=1016;
--rollback UPDATE experiment.location SET location_code='2015DS SDOT_IRRI' WHERE id=1017;
--rollback UPDATE experiment.location SET location_code='2015DS OYT2_B36' WHERE id=1028;
--rollback UPDATE experiment.location SET location_code='2015DS HiN.LST_IRRI' WHERE id=1036;
--rollback UPDATE experiment.location SET location_code='2015DS HiN.BLK_IRRI' WHERE id=1037;
--rollback UPDATE experiment.location SET location_code='2015DS.SPS_IRRI farms' WHERE id=1047;
--rollback UPDATE experiment.location SET location_code='2015DS SI_IRRI' WHERE id=1061;
--rollback UPDATE experiment.location SET location_code='2015DS normal.BLK_IRRI' WHERE id=1073;
--rollback UPDATE experiment.location SET location_code='2015DS LST_IRRIHQ' WHERE id=1085;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_MOD2_IRRI' WHERE id=1099;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_MOD1_IRRI' WHERE id=1100;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_GRP1_IRRI' WHERE id=1105;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_GRP2_IRRI' WHERE id=1110;
--rollback UPDATE experiment.location SET location_code='2015DSERA_GRP1_IRRI' WHERE id=1114;
--rollback UPDATE experiment.location SET location_code='2015DSERA_GRP2_IRRI' WHERE id=1115;
--rollback UPDATE experiment.location SET location_code='2015DSAWD_GRP1_IRRI' WHERE id=1124;
--rollback UPDATE experiment.location SET location_code='2015DSAWD_GRP2_IRRI' WHERE id=1125;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_GRP1_C29' WHERE id=1129;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_GRP2_C29' WHERE id=1131;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_AGUSAN_MOD2' WHERE id=1191;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_ISABELA_MOD2' WHERE id=1193;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_ISABELA_MOD1' WHERE id=1200;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_AGUSAN_MOD1' WHERE id=1201;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_N.ECIJA_MOD2' WHERE id=1217;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_N.ECIJA_MOD1' WHERE id=1218;
--rollback UPDATE experiment.location SET location_code='2015DSGWAS_1_415' WHERE id=1313;
--rollback UPDATE experiment.location SET location_code='2015DSGWAS_2_415' WHERE id=1314;
--rollback UPDATE experiment.location SET location_code='2015DSDSR_Grp1_UD4' WHERE id=1358;
--rollback UPDATE experiment.location SET location_code='2015DSDSR_Grp2_UD4' WHERE id=1360;
--rollback UPDATE experiment.location SET location_code='2015DSDSR_TP_Grp1_UD4' WHERE id=1361;
--rollback UPDATE experiment.location SET location_code='2015DSDSR_TP_Grp2_UD4' WHERE id=1362;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_CMU_CMU' WHERE id=1367;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_Bohol_Grp1_Sagbayan' WHERE id=1377;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_N.Ecija_Grp1' WHERE id=1378;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_Bohol_Grp2_Sagbayan' WHERE id=1380;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_N.Ecija_Grp2' WHERE id=1381;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_Myanmar_Grp1' WHERE id=1382;
--rollback UPDATE experiment.location SET location_code='2015DSPYT_Myanmar_Grp2' WHERE id=1383;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_Myanmar_Grp1' WHERE id=1384;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_N.Ecija_Grp2' WHERE id=1389;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_Myanmar_Grp2' WHERE id=1396;
--rollback UPDATE experiment.location SET location_code='2015DSOYT_N.Ecija_Grp1' WHERE id=1397;
--rollback UPDATE experiment.location SET location_code='2015DSSubScreening_ASTEC_IRSEA_G10b' WHERE id=1405;
--rollback UPDATE experiment.location SET location_code='2015DSAYT_Myanmar' WHERE id=1408;
--rollback UPDATE experiment.location SET location_code='BSREC_2015DSPYT_GRP1_IRRIHQ' WHERE id=1411;
--rollback UPDATE experiment.location SET location_code='BSREC_2015DSPYT_GRP2_IRRIHQ' WHERE id=1412;
--rollback UPDATE experiment.location SET location_code='BSREC_2015DSAYT_MOD1_IRRIHQ' WHERE id=1413;
--rollback UPDATE experiment.location SET location_code='BSREC_2015DSAYT_MOD2_IRRIHQ' WHERE id=1414;
--rollback UPDATE experiment.location SET location_code='2015WSOYT_CMU_CMU' WHERE id=1489;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-5_CS' WHERE id=1491;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-6_10001' WHERE id=1504;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-7_10001' WHERE id=1507;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2014-WS-8_10001' WHERE id=1508;
--rollback UPDATE experiment.location SET location_code='2015WS_OYT2_BG-09 ' WHERE id=1520;
--rollback UPDATE experiment.location SET location_code='2015WS_STU_B36' WHERE id=1521;
--rollback UPDATE experiment.location SET location_code='HB2015DS_PARENT' WHERE id=1527;
--rollback UPDATE experiment.location SET location_code='IRSEA-IRRIHQ-RGA-2015-DS-9_10001' WHERE id=1540;
--rollback UPDATE experiment.location SET location_code='2015WS HB Parents_U' WHERE id=1567;
--rollback UPDATE experiment.location SET location_code='2014WSRFXT_IRRIHQ' WHERE id=1572;
--rollback UPDATE experiment.location SET location_code='LXT2_2015WS_B36' WHERE id=1603;
--rollback UPDATE experiment.location SET location_code='2015WS_LxT1_B36' WHERE id=1621;
--rollback UPDATE experiment.location SET location_code='2015WSOYT_Grp1_C29' WHERE id=1697;
--rollback UPDATE experiment.location SET location_code='F12015WS_IRSEA_SC_B27, B28' WHERE id=1746;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_Grp1_B34' WHERE id=1763;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_Grp2_B34' WHERE id=1764;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_Grp1_B32' WHERE id=1765;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_Grp2_B32' WHERE id=1766;
--rollback UPDATE experiment.location SET location_code='2015WSDSR_Grp1_UD34' WHERE id=1769;
--rollback UPDATE experiment.location SET location_code='2015WSDSR_Grp2_UD33' WHERE id=1771;
--rollback UPDATE experiment.location SET location_code='2015WSDSR-TP_Grp1_UD34' WHERE id=1785;
--rollback UPDATE experiment.location SET location_code='2015WSDSR-TP_Grp2_UD34' WHERE id=1786;
--rollback UPDATE experiment.location SET location_code='2015WSAWD_Grp1_417' WHERE id=1788;
--rollback UPDATE experiment.location SET location_code='2015WSAWD_Grp2_417' WHERE id=1789;
--rollback UPDATE experiment.location SET location_code='2015WSOYT_Grp2_C29' WHERE id=1792;
--rollback UPDATE experiment.location SET location_code='2015WSOYT_NE_10002' WHERE id=1793;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_BOHOL_Grp1_10009' WHERE id=1794;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_NE_Grp1_10002' WHERE id=1796;
--rollback UPDATE experiment.location SET location_code='2015WSSPS_413, C28' WHERE id=1797;
--rollback UPDATE experiment.location SET location_code='2015WSLST_412' WHERE id=1798;
--rollback UPDATE experiment.location SET location_code='2015WSOYT_MMR' WHERE id=1799;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_MMR_Grp1' WHERE id=1800;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_MMR_Grp2' WHERE id=1801;
--rollback UPDATE experiment.location SET location_code='TRB-GSR Project_CS' WHERE id=1822;
--rollback UPDATE experiment.location SET location_code='2015WSM_C29' WHERE id=1825;
--rollback UPDATE experiment.location SET location_code='2015WSP_C29' WHERE id=1840;
--rollback UPDATE experiment.location SET location_code='2015WSRFxT_C28' WHERE id=1841;
--rollback UPDATE experiment.location SET location_code='2015WSLST2_412' WHERE id=1855;
--rollback UPDATE experiment.location SET location_code='2015WSLST3_UN21' WHERE id=1861;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_BOHOL_Grp2_10009' WHERE id=1868;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_NE_Grp2_10002' WHERE id=1888;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_MMR' WHERE id=1889;
--rollback UPDATE experiment.location SET location_code='2015WSPYT Grp1 Sub screening_ASTEC_IRSEA_10001' WHERE id=1955;
--rollback UPDATE experiment.location SET location_code='2015WSPYT Grp2 Sub screening_ASTEC_IRSEA_10001' WHERE id=1956;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_ISABELA_Mod1_10005' WHERE id=1981;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_ISABELA_Mod2_10005' WHERE id=1982;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_AGUSAN_Mod1_10003' WHERE id=1988;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_AGUSAN_Mod2_10003' WHERE id=1989;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_NE_Mod1_10002' WHERE id=1990;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_NE_Mod2_10002' WHERE id=1991;
--rollback UPDATE experiment.location SET location_code='Improvement of Tong-il type rice_CS' WHERE id=2016;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_Grp1 - Blast screening_10001' WHERE id=2026;
--rollback UPDATE experiment.location SET location_code='F12015WS_IRSEA_SC2_10001' WHERE id=2059;
--rollback UPDATE experiment.location SET location_code='BCN_4 Popn_2015WS_CS' WHERE id=2125;
--rollback UPDATE experiment.location SET location_code='2016DSPYT_408' WHERE id=2134;
--rollback UPDATE experiment.location SET location_code='Jean"s population_CS' WHERE id=2163;
--rollback UPDATE experiment.location SET location_code='HB2016DS_IRSEA' WHERE id=2166;
--rollback UPDATE experiment.location SET location_code='2015WSPYT_Grp2-Blast screening_10001' WHERE id=2177;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_Grp1-Blast screening_10001' WHERE id=2178;
--rollback UPDATE experiment.location SET location_code='2015WSAYT_Grp2-Blast screening_10001' WHERE id=2179;
--rollback UPDATE experiment.location SET location_code='2016DSOYT_410, 411, 412, 413' WHERE id=2181;
--rollback UPDATE experiment.location SET location_code='2016DS STU2_405' WHERE id=2183;
--rollback UPDATE experiment.location SET location_code='2016DS_HLYT_405' WHERE id=2184;
--rollback UPDATE experiment.location SET location_code='RGA LXT_From_ 2014WS - Batch 1_CS' WHERE id=2221;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite)  from 2015DS - Batch 1_CS' WHERE id=2223;
--rollback UPDATE experiment.location SET location_code='2016DSPLC_405' WHERE id=2224;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite)  from 2015WS - Batch 1_CS' WHERE id=2225;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace)  from 2015DS - Batch 1_CS' WHERE id=2226;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace)  from 2015WS - Batch 1_CS' WHERE id=2227;
--rollback UPDATE experiment.location SET location_code='2016DS_OYT2_E22, E23' WHERE id=2232;
--rollback UPDATE experiment.location SET location_code='2016DSLxT_E21' WHERE id=2233;
--rollback UPDATE experiment.location SET location_code='2016DS IRSEA_NEW Xa and Tungro Source_E23' WHERE id=2235;
--rollback UPDATE experiment.location SET location_code='2016DS DSR_Grp1_UD41' WHERE id=2247;
--rollback UPDATE experiment.location SET location_code='2016DS DSR_Grp2_UD41' WHERE id=2248;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_DSR_UD41' WHERE id=2249;
--rollback UPDATE experiment.location SET location_code='2016DS_BMCE BLK_417' WHERE id=2254;
--rollback UPDATE experiment.location SET location_code='2016DS_BMCE BMS_417' WHERE id=2255;
--rollback UPDATE experiment.location SET location_code='2016DSOYT_CMU_CMU' WHERE id=2312;
--rollback UPDATE experiment.location SET location_code='2016DSOYT_NE' WHERE id=2313;
--rollback UPDATE experiment.location SET location_code='2016DSPYT_NE_Grp1' WHERE id=2314;
--rollback UPDATE experiment.location SET location_code='2016DSSDYT_B9' WHERE id=2323;
--rollback UPDATE experiment.location SET location_code='2016DSLST1_414' WHERE id=2324;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_Grp1_406' WHERE id=2332;
--rollback UPDATE experiment.location SET location_code='2016DS DSR-TP_Grp1_UD34' WHERE id=2335;
--rollback UPDATE experiment.location SET location_code='2016DS DSR-TP_Grp2_UD34' WHERE id=2336;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_Grp2_407' WHERE id=2338;
--rollback UPDATE experiment.location SET location_code='F12016DS_IRSEA' WHERE id=2359;
--rollback UPDATE experiment.location SET location_code='2016DSPYT_NE_Grp2' WHERE id=2412;
--rollback UPDATE experiment.location SET location_code='RGA LXT_From_ 2014WS - Batch 2_CS' WHERE id=2441;
--rollback UPDATE experiment.location SET location_code='2016DSLST2_414' WHERE id=2442;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite)  from 2015DS - Batch 2_CS' WHERE id=2443;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite) from 2015WS - Batch 2_CS' WHERE id=2445;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace)  from 2015DS - Batch 2_CS' WHERE id=2446;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace) from 2015WS - Batch 2_CS' WHERE id=2449;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_Agusan_Grp1_MET' WHERE id=2465;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_Agusan_Grp2_MET' WHERE id=2466;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_NE_Grp1' WHERE id=2467;
--rollback UPDATE experiment.location SET location_code='2016DSAYT_NE_Grp2' WHERE id=2468;
--rollback UPDATE experiment.location SET location_code='2016EWSSDYT_B9' WHERE id=2527;
--rollback UPDATE experiment.location SET location_code='IRSEA_2016DSCROSS_FEMALE_10001' WHERE id=2531;
--rollback UPDATE experiment.location SET location_code='HB2016WS_IRSEA' WHERE id=2608;
--rollback UPDATE experiment.location SET location_code='2016WSOYT_LB_IRRI' WHERE id=2632;
--rollback UPDATE experiment.location SET location_code='2016WS_BMCE BLK_IRRI' WHERE id=2633;
--rollback UPDATE experiment.location SET location_code='2016WS_BMCE BMS_IRRI' WHERE id=2634;
--rollback UPDATE experiment.location SET location_code='2016WS_PYT2_E21' WHERE id=2666;
--rollback UPDATE experiment.location SET location_code='2016WS_OYT2_E23' WHERE id=2674;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_LB_408' WHERE id=2675;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_LB_406, 407' WHERE id=2676;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_NE' WHERE id=2701;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_AG' WHERE id=2702;
--rollback UPDATE experiment.location SET location_code='2016WSLST_414, 415' WHERE id=2707;
--rollback UPDATE experiment.location SET location_code='2016WSOYT_NE' WHERE id=2738;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_NE' WHERE id=2739;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_MMR' WHERE id=2740;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_MMR' WHERE id=2741;
--rollback UPDATE experiment.location SET location_code='2016WS DSR_Grp1_UD33' WHERE id=2795;
--rollback UPDATE experiment.location SET location_code='2016WS DSR_Grp2_UD33' WHERE id=2797;
--rollback UPDATE experiment.location SET location_code='2016WS DSR_AYT_UD44' WHERE id=2837;
--rollback UPDATE experiment.location SET location_code='2016WS  Best AYT/PYT_409' WHERE id=2842;
--rollback UPDATE experiment.location SET location_code='F12016WS_IRSEA' WHERE id=2846;
--rollback UPDATE experiment.location SET location_code='2016WS_BCN_E23' WHERE id=2918;
--rollback UPDATE experiment.location SET location_code='2016WS_STU_E21' WHERE id=2929;
--rollback UPDATE experiment.location SET location_code='2016WS_F1 NURSERY_E21' WHERE id=2932;
--rollback UPDATE experiment.location SET location_code='2016WSPRD_414' WHERE id=2939;
--rollback UPDATE experiment.location SET location_code='2016LWSSDYT_B9' WHERE id=2942;
--rollback UPDATE experiment.location SET location_code='OYT2_RTSV_98 lines_REP__BG' WHERE id=3024;
--rollback UPDATE experiment.location SET location_code='Blast_OYT2_98_lines_10001' WHERE id=3026;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_LB_RTV_Screening_BG' WHERE id=3028;
--rollback UPDATE experiment.location SET location_code='Sub_OYT2_104_Lines_10001' WHERE id=3030;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_LB_BPH_GLH_Screening_BG-07' WHERE id=3040;
--rollback UPDATE experiment.location SET location_code='2016WSAYT_LB_BLAST_Screening_10001' WHERE id=3041;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_LB_BPH_GLH_Screening_BG-01' WHERE id=3042;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_LB_BLAST_Screening_10001' WHERE id=3043;
--rollback UPDATE experiment.location SET location_code='2016WSPYT_LB_RTV_Screening_BG' WHERE id=3044;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite) from 2015WS - Batch 3.1_10001' WHERE id=3370;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite) from 2015WS - Batch 3.2_10001' WHERE id=3371;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite) from 2015WS - Batch 3.3_10001' WHERE id=3373;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace) from 2015WS - Batch 3.1_10001' WHERE id=3374;
--rollback UPDATE experiment.location SET location_code='RGA LXT_2 (Elite by landrace) from 2015WS - Batch 3.2_10001' WHERE id=3375;
--rollback UPDATE experiment.location SET location_code='RGA LXT_1 (Elite by elite) from 2015WS - Batch 3.4_10001' WHERE id=3376;
--rollback UPDATE experiment.location SET location_code='LOC00000001' WHERE id=3377;
--rollback UPDATE experiment.location SET location_code='LOC00000002' WHERE id=3378;
--rollback UPDATE experiment.location SET location_code='LOC00000003' WHERE id=3379;
--rollback UPDATE experiment.location SET location_code='LOC00000004' WHERE id=3380;
--rollback UPDATE experiment.location SET location_code='LOC00000005' WHERE id=3381;
--rollback UPDATE experiment.location SET location_code='LOC00000006' WHERE id=3382;
--rollback UPDATE experiment.location SET location_code='LOC00000007' WHERE id=3383;
--rollback UPDATE experiment.location SET location_code='LOC00000008' WHERE id=3384;
--rollback UPDATE experiment.location SET location_code='LOC00000009' WHERE id=3385;
--rollback UPDATE experiment.location SET location_code='LOC00000010' WHERE id=3386;
--rollback UPDATE experiment.location SET location_code='LOC00000011' WHERE id=3387;
--rollback UPDATE experiment.location SET location_code='LOC00000012' WHERE id=3388;
--rollback UPDATE experiment.location SET location_code='LOC00000013' WHERE id=3389;
--rollback UPDATE experiment.location SET location_code='LOC00000014' WHERE id=3390;
--rollback UPDATE experiment.location SET location_code='LOC00000015' WHERE id=3391;
--rollback UPDATE experiment.location SET location_code='LOC00000016' WHERE id=3392;
--rollback UPDATE experiment.location SET location_code='LOC-2021-A-7' WHERE id=3393;
--rollback UPDATE experiment.location SET location_code='LOC-2021-A-8' WHERE id=3394;
--rollback UPDATE experiment.location SET location_code='LOC-2021-A-9' WHERE id=3395;
--rollback UPDATE experiment.location SET location_code='LOC-2021-WS-11' WHERE id=3396;
--rollback UPDATE experiment.location SET location_code='LOC-2021-WS-12' WHERE id=3397;
--rollback UPDATE experiment.location SET location_code='LOC-2021-WS-13' WHERE id=3398;
--rollback UPDATE experiment.location SET location_code='LOC00000017' WHERE id=3399;
--rollback UPDATE experiment.location SET location_code='LOC00000018' WHERE id=3400;
--rollback UPDATE experiment.location SET location_code='IRRIHQ-2021-DS-005' WHERE id=3401;
--rollback UPDATE experiment.location SET location_code='MM_NY-2021-DS-001' WHERE id=3402;