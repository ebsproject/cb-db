--liquibase formatted sql

--changeset postgres:populate_barley_packages context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.package



-- populate barley packages
INSERT INTO
    germplasm.package (
        package_code, package_label, package_quantity, package_unit, package_status, seed_id, program_id, creator_id
    )
SELECT
    germplasm.generate_code('package') AS package_code,
    'PKG-' || ge.designation AS package_label,
    1000 AS package_quantity,
    'g' AS package_unit,
    'active' AS package_status,
    sd.id AS seed_id,
    prog.id AS program_id,
    sd.creator_id
FROM
    germplasm.seed AS sd
    JOIN germplasm.germplasm AS ge
        ON ge.id = sd.germplasm_id
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
    JOIN tenant.program AS prog
        ON prog.program_code = 'BTP'
WHERE
    crop.crop_code = 'BARLEY'
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.package AS pkg
--rollback USING
--rollback     germplasm.seed AS sd
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.id = sd.germplasm_id
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     pkg.seed_id = sd.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
