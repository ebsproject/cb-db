--liquibase formatted sql

--changeset postgres:create_btp_relation_in_tenant_program_team context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP relation in tenant.program_team



-- create BTP program-team relation
INSERT INTO tenant.program_team
    (program_id,team_id,order_number,creator_id)
VALUES
    ((SELECT id FROM tenant.program WHERE program_code = 'BTP'),(SELECT id FROM tenant.team WHERE team_code = 'BTP_TEAM'),1,1)
;

-- update sequence
SELECT SETVAL('tenant.program_team_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.program_team;



-- revert changes
--rollback DELETE FROM tenant.program_team WHERE program_id = (SELECT id FROM tenant.program WHERE program_code = 'BTP');

--rollback SELECT SETVAL('tenant.program_team_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.program_team;
