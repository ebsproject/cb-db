--liquibase formatted sql

--changeset postgres:update_maize_generation_and_germplasm_type_to_dh context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-373 Update maize generation and germplasm_type to DH



-- update generation to DH and germplasm_type to doubled_haploid from DH_line
UPDATE
    germplasm.germplasm AS ge
SET
    generation = 'DH',
    germplasm_type = 'doubled_haploid'
WHERE
    ge.generation = 'DH_line'
;

UPDATE
    germplasm.germplasm AS ge
SET
    germplasm_type = 'doubled_haploid'
WHERE
    ge.designation = 'CML464'
;



-- revert changes
--rollback UPDATE
--rollback     germplasm.germplasm AS ge
--rollback SET
--rollback     generation = 'DH_line',
--rollback     germplasm_type = 'DH_line'
--rollback WHERE
--rollback     ge.designation IN (
--rollback         'ABDHL164302',
--rollback         '(ABDHL0089/ABDHL120918)@200-B',
--rollback         '(ABDHL0089/ABDHL120918)@52-B',
--rollback         '(ABDHL0089/ABDHL120918)@63-B',
--rollback         '(ABDHL0089/ABLTI0136)@220-B',
--rollback         'ABDHL0221',
--rollback         'ABDHL120312',
--rollback         'ABDHL120918',
--rollback         'ABDHL163943',
--rollback         'ABDHL164665',
--rollback         'ABDHL164672',
--rollback         'ABDHL165891',
--rollback         '(ABLTI0139/ABDHL120918)@246-B',
--rollback         '(ABLTI0139/ABDHL120918)@299-B',
--rollback         '(ABLTI0139/ABDHL120918)@373-B',
--rollback         '(ABLTI0139/ABDHL120918)@393-B',
--rollback         '(ABLTI0139/ABDHL120918)@479-B',
--rollback         '(ABLTI0139/ABDHL120918)@553-B',
--rollback         '(ABLTI0139/ABDHL120918)@653-B',
--rollback         '(ABLTI0139/ABDHL120918)@95-B',
--rollback         '(ABLTI0139/ABLTI0335)@107-B',
--rollback         '(ABLTI0139/ABLTI0335)@14-B',
--rollback         '(ABLTI0139/CML543)@140-B',
--rollback         '((CML442/KS23-6)-B)@103-B',
--rollback         '((CML537/KS523-5)-B)@10-B',
--rollback         'MKL1500186',
--rollback         'MKL1500215',
--rollback         'MKL1500099',
--rollback         'MKL1500041',
--rollback         'MKL1500261',
--rollback         'MKL150334',
--rollback         'MKL150339',
--rollback         'MKL150342',
--rollback         'MKL150390',
--rollback         'MKL150399',
--rollback         'MKL150421',
--rollback         'MKL150431',
--rollback         'MKL150694',
--rollback         'MKL150893',
--rollback         'MKL150961',
--rollback         'MKL150968',
--rollback         'MKL151030',
--rollback         'MKL151147',
--rollback         'MKL151294',
--rollback         'MKL151780',
--rollback         'MKL151787',
--rollback         'MKL151822',
--rollback         'MKL151830',
--rollback         'MKL151839',
--rollback         'MKL151841',
--rollback         'MKL151845',
--rollback         'MKL151851',
--rollback         'MKL151920',
--rollback         'MKL151967',
--rollback         'MKL151969',
--rollback         'MKL151972',
--rollback         'MKL151973',
--rollback         'MKL152043',
--rollback         'MKL152076',
--rollback         'MKL152092',
--rollback         'MKL152098',
--rollback         'MKL152140',
--rollback         'MKL152144',
--rollback         'MKL152149',
--rollback         'MKL152366',
--rollback         'MKL152395',
--rollback         'MKL152503',
--rollback         'MKL152554',
--rollback         'MKL152561',
--rollback         'MKL152563',
--rollback         'MKL152579',
--rollback         'MKL152591',
--rollback         'MKL152601',
--rollback         'MKL152616',
--rollback         'MKL152617',
--rollback         'MKL152653',
--rollback         'MKL152658',
--rollback         'MKL152682',
--rollback         'MKL152748',
--rollback         'MKL152769',
--rollback         'MKL152773',
--rollback         'MKL152777',
--rollback         'MKL152778',
--rollback         'MKL152811',
--rollback         'MKL152847',
--rollback         'MKL152857',
--rollback         'MKL152862',
--rollback         'MKL152921',
--rollback         'MKL152929',
--rollback         'MKL152976',
--rollback         'MKL152994',
--rollback         'MKL153050',
--rollback         'MKL153193',
--rollback         'MKL153222',
--rollback         'MKL153223',
--rollback         'MKL153236',
--rollback         'MKL153907',
--rollback         'MKL153908',
--rollback         'MKL153939',
--rollback         'MKL153946',
--rollback         'MKL153984',
--rollback         'MKL154043',
--rollback         'CLRCY034',
--rollback         'CLYN261',
--rollback         'CML463',
--rollback         'CML495',
--rollback         '(ABDHL0089/ABDHL120668)@195-B',
--rollback         '(ABDHL0089/ABDHL120668)@19-B',
--rollback         'MKL154050',
--rollback         'MKL154091',
--rollback         'MKL154102',
--rollback         'CML547'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     germplasm.germplasm AS ge
--rollback SET
--rollback     germplasm_type = 'DH_line'
--rollback WHERE
--rollback     ge.designation IN (
--rollback         'CML464'
--rollback     )
--rollback ;
