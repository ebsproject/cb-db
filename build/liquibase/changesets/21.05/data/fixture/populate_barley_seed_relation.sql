--liquibase formatted sql

--changeset postgres:populate_barley_seed_relation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.seed_relation



-- populate barley germplasm relations
INSERT INTO
    germplasm.seed_relation (
        parent_seed_id, child_seed_id, order_number, creator_id
    )
SELECT
    psd.id AS parent_seed_id,
    csd.id AS child_seed_id,
    ROW_NUMBER() OVER (PARTITION BY pge.id, crs.id ORDER BY cge.id) AS order_number,
    cge.creator_id
FROM
    germplasm.cross AS crs
    JOIN germplasm.cross_parent AS crspar
        ON crs.id = crspar.cross_id
    JOIN germplasm.germplasm AS cge
        ON crs.cross_name = cge.parentage
    JOIN germplasm.germplasm AS pge
        ON pge.id = crspar.germplasm_id
    JOIN germplasm.seed AS csd
        ON csd.germplasm_id = cge.id
    JOIN germplasm.seed AS psd
        ON psd.germplasm_id = pge.id
    JOIN tenant.crop AS crop
        ON crop.id = cge.crop_id
WHERE
    crop.crop_code = 'BARLEY'
ORDER BY
    child_seed_id ASC,
    order_number ASC
;


-- revert changes
--rollback DELETE FROM
--rollback     germplasm.seed_relation AS sdrel
--rollback USING
--rollback     germplasm.seed AS sd
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON sd.germplasm_id = ge.id
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     sdrel.child_seed_id = sd.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
