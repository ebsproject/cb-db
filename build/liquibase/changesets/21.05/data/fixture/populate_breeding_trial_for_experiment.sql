--liquibase formatted sql

--changeset postgres:populate_breeding_trial_for_experiment context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial experiment



INSERT INTO
    experiment.experiment (
        program_id, pipeline_id, stage_id, project_id, experiment_year, season_id, planting_season, experiment_code, experiment_name,
        experiment_type, experiment_sub_type, experiment_sub_sub_type, experiment_design_type, experiment_status,
        steward_id, creator_id, is_void, data_process_id, crop_id
    )
SELECT
    (SELECT id FROM tenant.program WHERE program_code = 'KE') AS program_id,
    NULL pipeline_id,
    (SELECT id FROM tenant.stage WHERE stage_code = 'AYT') AS stage_id,
    NULL project_id,
    2021 AS experiment_year,
    (SELECT id FROM tenant.season WHERE season_code = 'DS') AS season_id,
    'DS' AS planting_season,
    experiment.generate_code('experiment') AS experiment_code,
    'KE-AYT-2021-DS-001' AS experiment_name,
    'Breeding Trial' AS experiment_type,
    'Yield Trial' AS experiment_sub_type,
    'First Year' AS experiment_sub_sub_type,
    'RCBD' AS experiment_design_type,
    'planted' AS experiment_status,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS steward_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    (SELECT id FROM master.item WHERE abbrev = 'BREEDING_TRIAL_DATA_PROCESS') AS data_process_id,
    (SELECT id FROM tenant.crop WHERE crop_code = 'MAIZE') AS crop_id
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.experiment
--rollback WHERE
--rollback     experiment_name = 'KE-AYT-2021-DS-001'
--rollback ;



--changeset postgres:populate_breeding_trial_in_tenant.protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial experiment in tenant.protocol



INSERT INTO
    tenant.protocol (
        protocol_code, protocol_name, protocol_type, program_id, creator_id
    )
VALUES 
    ('TRAIT_PROTOCOL_TMP_AF3BT','Trait Protocol Tmp AF3BT','trait',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('MANAGEMENT_PROTOCOL_TMP_AF3BT','Management Protocol Tmp AF3BT','management',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('PLANTING_PROTOCOL_TMP_AF3BT','Planting Protocol Tmp AF3BT','planting',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('HARVEST_PROTOCOL_TMP_AF3BT','Harvest Protocol Tmp AF3BT','harvest',(SELECT id FROM tenant.program WHERE program_code='KE'), (SELECT id FROM tenant.person WHERE username = 'nicola.costa'));



--rollback DELETE FROM tenant.protocol WHERE protocol_code IN ('TRAIT_PROTOCOL_TMP_AF3BT','MANAGEMENT_PROTOCOL_TMP_AF3BT','PLANTING_PROTOCOL_TMP_AF3BT','HARVEST_PROTOCOL_TMP_AF3BT');



--changeset postgres:populate_breeding_trial_in_experiment.experiment_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.experiment_data



INSERT INTO
    experiment.experiment_data (
        experiment_id, variable_id, data_value, data_qc_code, protocol_id, creator_id
    )
SELECT
    exp.id AS experiment_id,
    (SELECT id FROM master.variable WHERE abbrev=t.variable_abbrev) AS variable_id,
    t.data_value AS data_value,
    t.data_qc_code AS data_qc_code,
    (SELECT id FROM tenant.protocol WHERE protocol_name=t.protocol_name) AS protocol_id,
    psn.id AS creator_id
FROM
    (
        VALUES
            ('FIRST_PLOT_POSITION_VIEW','Top Left','N',NULL),
            ('TRAIT_PROTOCOL_LIST_ID','1393','N','Trait Protocol Tmp AF3BT'),
            ('MANAGEMENT_PROTOCOL_LIST_ID','1394','N','Management Protocol Tmp AF3BT'),
            ('ESTABLISHMENT','transplanted','N','Planting Protocol Tmp AF3BT'),
            ('PLANTING_TYPE','Flat','N','Planting Protocol Tmp AF3BT'),
            ('PLOT_TYPE','6R','N','Planting Protocol Tmp AF3BT'),
            ('ROWS_PER_PLOT_CONT','6','N','Planting Protocol Tmp AF3BT'),
            ('DIST_BET_ROWS','20','N','Planting Protocol Tmp AF3BT'),
            ('PLOT_WIDTH','1.2','N','Planting Protocol Tmp AF3BT'),
            ('PLOT_LN','20','N','Planting Protocol Tmp AF3BT'),
            ('PLOT_AREA_2','24','N','Planting Protocol Tmp AF3BT'),
            ('ALLEY_LENGTH','1','N','Planting Protocol Tmp AF3BT'),
            ('SEEDING_RATE','Normal','N','Planting Protocol Tmp AF3BT'),
            ('PROTOCOL_TARGET_LEVEL','occurrence','N',NULL),
            ('HV_METH_DISC','Bulk','N','Harvest Protocol Tmp AF3BT')
    ) AS t (variable_abbrev, data_value, data_qc_code, protocol_name)
INNER JOIN
    experiment.experiment exp
ON
    exp.experiment_name = 'KE-AYT-2021-DS-001'
INNER JOIN
    tenant.person psn
ON
    psn.username = 'nicola.costa'



--rollback DELETE FROM experiment.experiment_data WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001');



--changeset postgres:populate_breeding_trial_in_experiment.experiment_protocol context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.experiment_protocol



INSERT INTO
    experiment.experiment_protocol (
        experiment_id, protocol_id, order_number, creator_id
    )
VALUES    
    ((SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Trait Protocol Tmp AF3BT'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Management Protocol Tmp AF3BT'),2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Planting Protocol Tmp AF3BT'),3,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM tenant.protocol WHERE protocol_name='Harvest Protocol Tmp AF3BT'),4,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



--rollback DELETE FROM experiment.experiment_protocol WHERE experiment_id = (SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001');



--changeset postgres:populate_breeding_trial_in_experiment.entry_list context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.entry_list



INSERT INTO
    experiment.entry_list (
        entry_list_code, entry_list_name, entry_list_status, experiment_id, creator_id, is_void, entry_list_type
    )
SELECT
    experiment.generate_code('entry_list') AS entry_list_code,
    'KE-AYT-2021-DS Entry List' AS entry_list_name,
    'completed' AS entry_list_status,
    (SELECT id FROM experiment.experiment WHERE experiment_name = 'KE-AYT-2021-DS-001') AS experiment_id,
    (SELECT id FROM tenant.person WHERE username = 'nicola.costa') AS creator_id,
    FALSE AS is_void,
    'entry list' AS entry_list_type
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry_list
--rollback WHERE
--rollback     entry_list_name = 'KE-AYT-2021-DS Entry List';



--changeset postgres:populate_breeding_trial_in_experiment.entry context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.entry



INSERT INTO
   experiment.entry (
       entry_code, entry_number, entry_name, entry_type, entry_class, entry_role, entry_status,
       entry_list_id, germplasm_id, seed_id, creator_id, is_void
   )
SELECT
    t.entry_number AS entry_code,
    t.entry_number,
    t.designation AS entry_name,
    t.entry_type AS entry_type,
    t.entry_class AS entry_class,
    NULL AS entry_role,
    'active' AS entry_status,
    entlist.id AS entry_list_id,
    ge.id AS germplasm_id,
    seed.id AS seed_id,
    person.id AS creator_id,
    FALSE AS is_void
FROM
    (
        VALUES        
        (1,'ABDHL0221','WE-KIB-17A-48-4','test','test'),
        (2,'ABLMARSI0037','ST-KIB-18B-34-6','test','test'),
        (3,'ABLTI0137','WE-KIB-17A-48-11','test','test'),
        (4,'ABLTI0330','WE-KIB-17A-47-50','test','test'),
        (5,'ABSBL10020','WE-KIB-17A-33-6','test','test'),
        (6,'ABSBL10060','ST-KIB-18B-34-3','test','test'),
        (7,'CML204','WE-KIB-17A-33-14','test','test'),
        (8,'CML442','ST-KIB-18B-48-70','test','test'),
        (9,'CML444','ST-KIB-18B-48-23','test','test'),
        (10,'CML464','ST-KIB-18B-34-2','test','test'),
        (11,'CML494','ST-KIB-18B-34-11','test','test'),
        (12,'ABDHL120312','ST-KIB-18B-34-12','test','test'),
        (13,'CML536','ST-KIB-18B-46-2','test','test'),
        (14,'CML539','ST-KIB-18B-48-18','test','test'),
        (15,'CML543','ST-KIB-18B-34-1','test','test'),
        (16,'ABDHL120918','ST-KIB-18B-34-5','check','test'),
        (17,'ABDHL163943','ST-KIB-18B-48-50','check','test'),
        (18,'ABDHL164302','ST-KIB-18B-53-39','check','test'),
        (19,'ABDHL164665','ST-KIB-18B-53-41','check','test'),
        (20,'ABDHL164672','ST-KIB-18B-53-42','check','test')
    ) AS t (
        entry_number, designation, seed_name, entry_type, entry_class
    )
    INNER JOIN germplasm.germplasm AS ge
        ON t.designation = ge.designation
    INNER JOIN germplasm.seed AS seed
        ON ge.id = seed.germplasm_id
        AND t.seed_name = seed.seed_name
    INNER JOIN experiment.entry_list as entlist
        ON entlist.entry_list_name = 'KE-AYT-2021-DS Entry List'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.entry_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.entry AS e
--rollback USING
--rollback     experiment.entry_list AS el
--rollback WHERE
--rollback     e.entry_list_id = el.id
--rollback     AND el.entry_list_name = 'KE-AYT-2021-DS Entry List'
--rollback ;



--changeset postgres:populate_breeding_trial_in_experiment.occurrence context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.occurrence



INSERT INTO
    experiment.occurrence (
        occurrence_code,
        occurrence_name,
        occurrence_status,
        experiment_id,
        site_id,
        field_id,
        rep_count,
        occurrence_number,
        creator_id
    )
VALUES 
    ('KE-AYT-2021-DS-001-001','KE-AYT-2021-DS-001-001','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI, Los Baños, Laguna, Philippines'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='400'),NULL,1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('KE-AYT-2021-DS-001-002','KE-AYT-2021-DS-001-002','planted',(SELECT id FROM experiment.experiment WHERE experiment_name='KE-AYT-2021-DS-001'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Yezin, Naypyitaw, Myanmar'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='A2'),NULL,2,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.occurrence
--rollback WHERE
--rollback     occurrence_name IN ('KE-AYT-2021-DS-001-001','KE-AYT-2021-DS-001-002')
--rollback ;



--changeset postgres:populate_rice_breeding_trial_in_place.geospatial_object context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate rice breeding trial in place.geospatial_object



INSERT INTO
    place.geospatial_object (
        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, creator_id, parent_geospatial_object_id, root_geospatial_object_id
    )
VALUES
    (
        'IRRIHQ-2021-DS-005', 
        'TEST_1', 
        'planting area', 
        'breeding_location', 
        '1', 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='400'), 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='IRRIHQ')
    ),
    (
        'MM_NY-2021-DS-001', 
        'TEST_2', 
        'planting area', 
        'breeding_location', 
        '1', 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='A2'), 
        (SELECT id FROM place.geospatial_object WHERE geospatial_object_code='MM_NY')
    )
;



-- revert changes
--rollback DELETE FROM
--rollback     place.geospatial_object
--rollback WHERE
--rollback     geospatial_object_name IN ('TEST_1','TEST_2')
--rollback ;



--changeset postgres:populate_breeding_trial_in_experiment.location context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.location



INSERT INTO
   experiment.location (
       location_code, location_name, location_status, location_type,
       location_year, season_id, location_number, site_id, field_id,
       steward_id, geospatial_object_id, creator_id
   )
VALUES
    ('IRRIHQ-2021-DS-005','TEST_1','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='DS'),5,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='IRRI, Los Baños, Laguna, Philippines'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='400'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='TEST_1'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ('MM_NY-2021-DS-001','TEST_2','planted','planting area',2021,(SELECT id FROM tenant.season WHERE season_code='DS'),1,(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='Yezin, Naypyitaw, Myanmar'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='A2'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'),(SELECT id FROM place.geospatial_object WHERE geospatial_object_name='TEST_2'),(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.location
--rollback WHERE
--rollback     location_name IN ('TEST_1','TEST_2')
--rollback ;



--changeset postgres:populate_breeding_trial_in_experiment.location_occurrence_group context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.location_occurrence_group



INSERT INTO 
    experiment.location_occurrence_group (
        location_id,occurrence_id,order_number,creator_id
    )
VALUES
    ((SELECT id FROM experiment.location WHERE location_name='TEST_1'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='KE-AYT-2021-DS-001-001'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa')),
    ((SELECT id FROM experiment.location WHERE location_name='TEST_2'),(SELECT id FROM experiment.occurrence WHERE occurrence_name='KE-AYT-2021-DS-001-002'),1,(SELECT id FROM tenant.person WHERE username = 'nicola.costa'))
;



--rollback DELETE FROM 
--rollback     experiment.location_occurrence_group
--rollback WHERE
--rollback     location_id IN (SELECT id FROM experiment.location WHERE location_name IN ('TEST_1','TEST_2'))
--rollback AND
--rollback     occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('KE-AYT-2021-DS-001-001','KE-AYT-2021-DS-001-002'));



--changeset postgres:populate_breeding_trial_in_experiment.plot context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial in experiment.plot



--set 1
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES  
            ('420',80,4,4,'ABLTI0330',10,7,10,7,10,7,NULL,4),
            ('419',79,4,11,'CML494',10,8,10,8,10,8,NULL,4),
            ('418',78,4,3,'ABLTI0137',9,8,9,8,9,8,NULL,4),
            ('417',77,4,10,'CML464',9,7,9,7,9,7,NULL,4),
            ('416',76,4,5,'ABSBL10020',8,7,8,7,8,7,NULL,4),
            ('415',75,4,14,'CML539',8,8,8,8,8,8,NULL,4),
            ('414',74,4,19,'ABDHL164665',7,8,7,8,7,8,NULL,4),
            ('413',73,4,13,'CML536',7,7,7,7,7,7,NULL,4),
            ('412',72,4,2,'ABLMARSI0037',6,7,6,7,6,7,NULL,4),
            ('411',71,4,8,'CML442',6,8,6,8,6,8,NULL,4),
            ('410',70,4,7,'CML204',5,8,5,8,5,8,NULL,4),
            ('409',69,4,12,'ABDHL120312',5,7,5,7,5,7,NULL,4),
            ('408',68,4,6,'ABSBL10060',4,7,4,7,4,7,NULL,4),
            ('407',67,4,9,'CML444',4,8,4,8,4,8,NULL,4),
            ('406',66,4,15,'CML543',3,8,3,8,3,8,NULL,4),
            ('405',65,4,16,'ABDHL120918',3,7,3,7,3,7,NULL,4),
            ('404',64,4,1,'ABDHL0221',2,7,2,7,2,7,NULL,4),
            ('403',63,4,20,'ABDHL164672',2,8,2,8,2,8,NULL,4),
            ('402',62,4,18,'ABDHL164302',1,8,1,8,1,8,NULL,4),
            ('401',61,4,17,'ABDHL163943',1,7,1,7,1,7,NULL,4),
            ('320',60,3,12,'ABDHL120312',10,5,10,5,10,5,NULL,3),
            ('319',59,3,17,'ABDHL163943',10,6,10,6,10,6,NULL,3),
            ('318',58,3,14,'CML539',9,6,9,6,9,6,NULL,3),
            ('317',57,3,15,'CML543',9,5,9,5,9,5,NULL,3),
            ('316',56,3,4,'ABLTI0330',8,5,8,5,8,5,NULL,3),
            ('315',55,3,2,'ABLMARSI0037',8,6,8,6,8,6,NULL,3),
            ('314',54,3,6,'ABSBL10060',7,6,7,6,7,6,NULL,3),
            ('313',53,3,1,'ABDHL0221',7,5,7,5,7,5,NULL,3),
            ('312',52,3,11,'CML494',6,5,6,5,6,5,NULL,3),
            ('311',51,3,16,'ABDHL120918',6,6,6,6,6,6,NULL,3),
            ('310',50,3,9,'CML444',5,6,5,6,5,6,NULL,3),
            ('309',49,3,20,'ABDHL164672',5,5,5,5,5,5,NULL,3),
            ('308',48,3,5,'ABSBL10020',4,5,4,5,4,5,NULL,3),
            ('307',47,3,7,'CML204',4,6,4,6,4,6,NULL,3),
            ('306',46,3,10,'CML464',3,6,3,6,3,6,NULL,3),
            ('305',45,3,13,'CML536',3,5,3,5,3,5,NULL,3),
            ('304',44,3,3,'ABLTI0137',2,5,2,5,2,5,NULL,3),
            ('303',43,3,18,'ABDHL164302',2,6,2,6,2,6,NULL,3),
            ('302',42,3,19,'ABDHL164665',1,6,1,6,1,6,NULL,3),
            ('301',41,3,8,'CML442',1,5,1,5,1,5,NULL,3),
            ('220',40,2,4,'ABLTI0330',10,3,10,3,10,3,NULL,2),
            ('219',39,2,9,'CML444',10,4,10,4,10,4,NULL,2),
            ('218',38,2,20,'ABDHL164672',9,4,9,4,9,4,NULL,2),
            ('217',37,2,6,'ABSBL10060',9,3,9,3,9,3,NULL,2),
            ('216',36,2,11,'CML494',8,3,8,3,8,3,NULL,2),
            ('215',35,2,7,'CML204',8,4,8,4,8,4,NULL,2),
            ('214',34,2,19,'ABDHL164665',7,4,7,4,7,4,NULL,2),
            ('213',33,2,18,'ABDHL164302',7,3,7,3,7,3,NULL,2),
            ('212',32,2,12,'ABDHL120312',6,3,6,3,6,3,NULL,2),
            ('211',31,2,14,'CML539',6,4,6,4,6,4,NULL,2),
            ('210',30,2,13,'CML536',5,4,5,4,5,4,NULL,2),
            ('209',29,2,3,'ABLTI0137',5,3,5,3,5,3,NULL,2),
            ('208',28,2,1,'ABDHL0221',4,3,4,3,4,3,NULL,2),
            ('207',27,2,2,'ABLMARSI0037',4,4,4,4,4,4,NULL,2),
            ('206',26,2,8,'CML442',3,4,3,4,3,4,NULL,2),
            ('205',25,2,17,'ABDHL163943',3,3,3,3,3,3,NULL,2),
            ('204',24,2,16,'ABDHL120918',2,3,2,3,2,3,NULL,2),
            ('203',23,2,15,'CML543',2,4,2,4,2,4,NULL,2),
            ('202',22,2,5,'ABSBL10020',1,4,1,4,1,4,NULL,2),
            ('201',21,2,10,'CML464',1,3,1,3,1,3,NULL,2),
            ('120',20,1,10,'CML464',10,1,10,1,10,1,NULL,1),
            ('119',19,1,9,'CML444',10,2,10,2,10,2,NULL,1),
            ('118',18,1,1,'ABDHL0221',9,2,9,2,9,2,NULL,1),
            ('117',17,1,4,'ABLTI0330',9,1,9,1,9,1,NULL,1),
            ('116',16,1,2,'ABLMARSI0037',8,1,8,1,8,1,NULL,1),
            ('115',15,1,16,'ABDHL120918',8,2,8,2,8,2,NULL,1),
            ('114',14,1,7,'CML204',7,2,7,2,7,2,NULL,1),
            ('113',13,1,15,'CML543',7,1,7,1,7,1,NULL,1),
            ('112',12,1,11,'CML494',6,1,6,1,6,1,NULL,1),
            ('111',11,1,14,'CML539',6,2,6,2,6,2,NULL,1),
            ('110',10,1,18,'ABDHL164302',5,2,5,2,5,2,NULL,1),
            ('109',9,1,13,'CML536',5,1,5,1,5,1,NULL,1),
            ('108',8,1,12,'ABDHL120312',4,1,4,1,4,1,NULL,1),
            ('107',7,1,19,'ABDHL164665',4,2,4,2,4,2,NULL,1),
            ('106',6,1,6,'ABSBL10060',3,2,3,2,3,2,NULL,1),
            ('105',5,1,3,'ABLTI0137',3,1,3,1,3,1,NULL,1),
            ('104',4,1,17,'ABDHL163943',2,1,2,1,2,1,NULL,1),
            ('103',3,1,20,'ABDHL164672',2,2,2,2,2,2,NULL,1),
            ('102',2,1,8,'CML442',1,2,1,2,1,2,NULL,1),
            ('101',1,1,5,'ABSBL10020',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-AYT-2021-DS Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'KE-AYT-2021-DS-001-001'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'TEST_1'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;

--set 2
INSERT INTO
   experiment.plot (
       occurrence_id, location_id, entry_id,
       plot_code, plot_number, plot_type, rep,
       design_x, design_y, pa_x, pa_y, field_x, field_y,
       block_number, plot_status, plot_qc_code, creator_id, harvest_status
   )
SELECT
    occ.id AS occurrence_id,
    loc.id AS location_id,
    ent.id AS entry_id,
    t.plot_code AS plot_code,
    t.plot_number AS plot_number,
    'plotType' AS plot_type,
    t.rep AS rep,
    t.design_x AS design_x,
    t.design_y AS design_y,
    t.pa_x AS pa_x,
    t.pa_y AS pa_y, 
    t.field_x AS field_x,  
    t.field_y AS field_y,
    t.block_number AS block_number,
    'active' AS plot_status,
    t.plot_qc_code AS plot_qc_code,
    person.id AS creator_id,
    'NO_HARVEST' AS harvest_status
FROM
    (
        VALUES      
            ('420',80,4,12,'ABDHL120312',10,7,10,7,10,7,NULL,4),
            ('419',79,4,17,'ABDHL163943',10,8,10,8,10,8,NULL,4),
            ('418',78,4,4,'ABLTI0330',9,8,9,8,9,8,NULL,4),
            ('417',77,4,3,'ABLTI0137',9,7,9,7,9,7,NULL,4),
            ('416',76,4,15,'CML543',8,7,8,7,8,7,NULL,4),
            ('415',75,4,19,'ABDHL164665',8,8,8,8,8,8,NULL,4),
            ('414',74,4,6,'ABSBL10060',7,8,7,8,7,8,NULL,4),
            ('413',73,4,18,'ABDHL164302',7,7,7,7,7,7,NULL,4),
            ('412',72,4,13,'CML536',6,7,6,7,6,7,NULL,4),
            ('411',71,4,10,'CML464',6,8,6,8,6,8,NULL,4),
            ('410',70,4,11,'CML494',5,8,5,8,5,8,NULL,4),
            ('409',69,4,2,'ABLMARSI0037',5,7,5,7,5,7,NULL,4),
            ('408',68,4,7,'CML204',4,7,4,7,4,7,NULL,4),
            ('407',67,4,1,'ABDHL0221',4,8,4,8,4,8,NULL,4),
            ('406',66,4,16,'ABDHL120918',3,8,3,8,3,8,NULL,4),
            ('405',65,4,20,'ABDHL164672',3,7,3,7,3,7,NULL,4),
            ('404',64,4,9,'CML444',2,7,2,7,2,7,NULL,4),
            ('403',63,4,14,'CML539',2,8,2,8,2,8,NULL,4),
            ('402',62,4,8,'CML442',1,8,1,8,1,8,NULL,4),
            ('401',61,4,5,'ABSBL10020',1,7,1,7,1,7,NULL,4),
            ('320',60,3,6,'ABSBL10060',10,5,10,5,10,5,NULL,3),
            ('319',59,3,15,'CML543',10,6,10,6,10,6,NULL,3),
            ('318',58,3,19,'ABDHL164665',9,6,9,6,9,6,NULL,3),
            ('317',57,3,4,'ABLTI0330',9,5,9,5,9,5,NULL,3),
            ('316',56,3,11,'CML494',8,5,8,5,8,5,NULL,3),
            ('315',55,3,1,'ABDHL0221',8,6,8,6,8,6,NULL,3),
            ('314',54,3,5,'ABSBL10020',7,6,7,6,7,6,NULL,3),
            ('313',53,3,12,'ABDHL120312',7,5,7,5,7,5,NULL,3),
            ('312',52,3,20,'ABDHL164672',6,5,6,5,6,5,NULL,3),
            ('311',51,3,7,'CML204',6,6,6,6,6,6,NULL,3),
            ('310',50,3,13,'CML536',5,6,5,6,5,6,NULL,3),
            ('309',49,3,14,'CML539',5,5,5,5,5,5,NULL,3),
            ('308',48,3,17,'ABDHL163943',4,5,4,5,4,5,NULL,3),
            ('307',47,3,2,'ABLMARSI0037',4,6,4,6,4,6,NULL,3),
            ('306',46,3,16,'ABDHL120918',3,6,3,6,3,6,NULL,3),
            ('305',45,3,8,'CML442',3,5,3,5,3,5,NULL,3),
            ('304',44,3,18,'ABDHL164302',2,5,2,5,2,5,NULL,3),
            ('303',43,3,10,'CML464',2,6,2,6,2,6,NULL,3),
            ('302',42,3,3,'ABLTI0137',1,6,1,6,1,6,NULL,3),
            ('301',41,3,9,'CML444',1,5,1,5,1,5,NULL,3),
            ('220',40,2,9,'CML444',10,3,10,3,10,3,NULL,2),
            ('219',39,2,19,'ABDHL164665',10,4,10,4,10,4,NULL,2),
            ('218',38,2,1,'ABDHL0221',9,4,9,4,9,4,NULL,2),
            ('217',37,2,10,'CML464',9,3,9,3,9,3,NULL,2),
            ('216',36,2,13,'CML536',8,3,8,3,8,3,NULL,2),
            ('215',35,2,12,'ABDHL120312',8,4,8,4,8,4,NULL,2),
            ('214',34,2,17,'ABDHL163943',7,4,7,4,7,4,NULL,2),
            ('213',33,2,6,'ABSBL10060',7,3,7,3,7,3,NULL,2),
            ('212',32,2,15,'CML543',6,3,6,3,6,3,NULL,2),
            ('211',31,2,11,'CML494',6,4,6,4,6,4,NULL,2),
            ('210',30,2,7,'CML204',5,4,5,4,5,4,NULL,2),
            ('209',29,2,3,'ABLTI0137',5,3,5,3,5,3,NULL,2),
            ('208',28,2,14,'CML539',4,3,4,3,4,3,NULL,2),
            ('207',27,2,5,'ABSBL10020',4,4,4,4,4,4,NULL,2),
            ('206',26,2,4,'ABLTI0330',3,4,3,4,3,4,NULL,2),
            ('205',25,2,20,'ABDHL164672',3,3,3,3,3,3,NULL,2),
            ('204',24,2,18,'ABDHL164302',2,3,2,3,2,3,NULL,2),
            ('203',23,2,8,'CML442',2,4,2,4,2,4,NULL,2),
            ('202',22,2,16,'ABDHL120918',1,4,1,4,1,4,NULL,2),
            ('201',21,2,2,'ABLMARSI0037',1,3,1,3,1,3,NULL,2),
            ('120',20,1,7,'CML204',10,1,10,1,10,1,NULL,1),
            ('119',19,1,10,'CML464',10,2,10,2,10,2,NULL,1),
            ('118',18,1,20,'ABDHL164672',9,2,9,2,9,2,NULL,1),
            ('117',17,1,11,'CML494',9,1,9,1,9,1,NULL,1),
            ('116',16,1,12,'ABDHL120312',8,1,8,1,8,1,NULL,1),
            ('115',15,1,14,'CML539',8,2,8,2,8,2,NULL,1),
            ('114',14,1,18,'ABDHL164302',7,2,7,2,7,2,NULL,1),
            ('113',13,1,5,'ABSBL10020',7,1,7,1,7,1,NULL,1),
            ('112',12,1,13,'CML536',6,1,6,1,6,1,NULL,1),
            ('111',11,1,6,'ABSBL10060',6,2,6,2,6,2,NULL,1),
            ('110',10,1,3,'ABLTI0137',5,2,5,2,5,2,NULL,1),
            ('109',9,1,17,'ABDHL163943',5,1,5,1,5,1,NULL,1),
            ('108',8,1,2,'ABLMARSI0037',4,1,4,1,4,1,NULL,1),
            ('107',7,1,1,'ABDHL0221',4,2,4,2,4,2,NULL,1),
            ('106',6,1,9,'CML444',3,2,3,2,3,2,NULL,1),
            ('105',5,1,19,'ABDHL164665',3,1,3,1,3,1,NULL,1),
            ('104',4,1,8,'CML442',2,1,2,1,2,1,NULL,1),
            ('103',3,1,16,'ABDHL120918',2,2,2,2,2,2,NULL,1),
            ('102',2,1,15,'CML543',1,2,1,2,1,2,NULL,1),
            ('101',1,1,4,'ABLTI0330',1,1,1,1,1,1,NULL,1)
    ) AS t (
        plot_code, plot_number, rep, entry_number, designation, design_x, design_y, pa_x, pa_y, field_x, field_y, plot_qc_code, block_number
    )
    INNER JOIN experiment.entry AS ent
        ON t.entry_number = ent.entry_number
        AND t.designation = ent.entry_name
    INNER JOIN experiment.entry_list AS entlist
        ON ent.entry_list_id = entlist.id
        AND entlist.entry_list_name = 'KE-AYT-2021-DS Entry List'
    INNER JOIN experiment.occurrence AS occ
        ON occ.occurrence_name = 'KE-AYT-2021-DS-001-002'
    INNER JOIN experiment.location AS loc
        ON loc.location_name = 'TEST_2'
    INNER JOIN tenant.person AS person
        ON person.username = 'nicola.costa'
ORDER BY
    t.plot_number
;



-- revert changes OCC
--rollback DELETE FROM
--rollback     experiment.plot AS plot
--rollback USING
--rollback     experiment.occurrence AS occ,
--rollback     experiment.location AS loc
--rollback WHERE
--rollback     plot.occurrence_id = occ.id
--rollback     AND occ.occurrence_name IN ('KE-AYT-2021-DS-001-001','KE-AYT-2021-DS-001-002')
--rollback     AND loc.location_name IN ('TEST_1','TEST_2')
--rollback ;



--changeset postgres:add_plot_data_records_in_experiment.plot_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Add plot data records in experiment.plot_data



INSERT INTO
    experiment.plot_data 
        (plot_id,variable_id,data_value,data_qc_code,transaction_id,creator_id)
SELECT
	 ep.id AS plot_id,
     mv.id AS variable_id,
     t.data_value AS data_value,
     t.data_qc_code AS data_qc_code,
     t.transaction_id AS transaction_id,
     tp.id AS creator_id
FROM
    (
        VALUES
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HT3_CONT','90','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HT_CONT','82','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HT1_CONT','82','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HT_CONT','78.33','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HT_CONT','82','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HT_CONT','78.67','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','HT_CONT','76','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','HT_CONT','80','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HT_CONT','86','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HT_CONT','80.67','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','HT_CONT','81.5','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HT_CONT','78.67','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HT_CONT','85.67','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','FLW_DATE_CONT','2021-04-29','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','HT3_CONT','76','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HT3_CONT','77','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','HT3_CONT','80','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HT3_CONT','81','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HT3_CONT','81','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HT3_CONT','82','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HT3_CONT','83','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HT3_CONT','85','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HT3_CONT','87','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HT2_CONT','73','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HT2_CONT','75','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','HT2_CONT','76','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','HT2_CONT','80','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HT2_CONT','82','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HT2_CONT','84','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HT2_CONT','87','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HT2_CONT','87','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','HT2_CONT','89','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HT2_CONT','90','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HT2_CONT','90','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HT1_CONT','70','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HT1_CONT','71','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HT1_CONT','72','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','HT1_CONT','74','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HT1_CONT','77','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HT1_CONT','81','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HT1_CONT','83','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HT1_CONT','86','Q',182),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','HV_METH_DISC','Single Plant Selection','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HV_METH_DISC','plant-specific','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HV_METH_DISC','Bulk','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111','HVDATE_CONT','2021-04-29','Q',183),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113','SPECIFIC_PLANT','2,3,5','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112','NO_OF_PLANTS','3','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113','NO_OF_PLANTS','3','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112','HVDATE_CONT','2021-04-29','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113','HVDATE_CONT','2021-04-29','Q',223),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','420','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','419','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','418','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','417','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','416','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','415','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','414','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','413','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','412','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','411','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','410','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','409','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','407','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','406','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','405','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','404','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','403','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','402','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','401','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','320','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','319','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','318','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','317','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','316','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','315','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','314','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','313','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','311','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','310','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','309','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','308','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','307','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','306','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','305','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','304','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','303','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','302','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','301','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','220','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','219','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','218','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','217','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','216','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','213','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','212','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','211','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','210','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','209','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','208','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','207','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','206','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','205','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','204','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','203','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','202','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','201','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','120','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','119','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','118','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','117','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','116','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','115','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','114','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','214','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','215','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','312','HV_METH_DISC','Bulk','Q',225),
            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','408','HV_METH_DISC','Bulk','Q',225)
    ) AS t (occurrence_name, location_name, entry_name, plot_code, variable_abbrev, data_value, data_qc_code, transaction_id)
INNER JOIN
	experiment.plot ep
ON
	ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=t.occurrence_name)
AND
	ep.location_id = (SELECT id FROM experiment.location WHERE location_name=t.location_name)
AND
 	ep.entry_id = (SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='KE-AYT-2021-DS Entry List'))
AND
	ep.plot_code = t.plot_code
INNER JOIN
	master.variable mv
ON
	mv.abbrev=t.variable_abbrev
INNER JOIN
	tenant.person tp
ON
	username='nicola.costa';



--rollback DELETE FROM 
--rollback     experiment.plot_data
--rollback WHERE 
--rollback     plot_id 
--rollback IN
--rollback     (
--rollback         SELECT
--rollback 	          ep.id
--rollback         FROM
--rollback         (
--rollback         VALUES
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','101'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','102'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','103'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','104'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','105'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','106'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','107'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','108'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','109'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','110'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','111'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','112'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','113'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','420'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','419'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','418'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','417'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','416'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','415'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','414'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','413'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','412'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','411'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','410'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','409'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','407'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','406'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','405'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','404'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','403'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','402'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','401'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','320'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','319'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','318'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','317'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','316'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','315'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','314'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','313'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','311'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','310'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','309'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','308'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','307'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','306'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','305'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','304'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','303'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','302'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','301'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','220'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','219'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164672','218'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','217'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','216'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164302','213'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120312','212'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML539','211'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML536','210'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0137','209'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','208'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','207'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML442','206'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL163943','205'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','204'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML543','203'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10020','202'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','201'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML464','120'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML444','119'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL0221','118'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLTI0330','117'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABLMARSI0037','116'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL120918','115'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','114'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABDHL164665','214'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML204','215'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','CML494','312'),
--rollback            ('KE-AYT-2021-DS-001-001','TEST_1','ABSBL10060','408')
--rollback     ) AS t (occurrence_name, location_name, entry_name, plot_code)
--rollback INNER JOIN
--rollback 	experiment.plot ep
--rollback ON
--rollback 	ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=t.occurrence_name)
--rollback AND
--rollback 	ep.location_id = (SELECT id FROM experiment.location WHERE location_name=t.location_name)
--rollback AND
--rollback  	ep.entry_id = (SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name='KE-AYT-2021-DS Entry List'))
--rollback AND
--rollback 	ep.plot_code = t.plot_code);



--changeset postgres:populate_breeding_trial_for_af_in_experiment.planting_instruction context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial for AF in experiment.planting_instruction



INSERT INTO
   experiment.planting_instruction (
       entry_code, entry_number, entry_name, entry_type, entry_role, entry_status, entry_id,
       plot_id, germplasm_id, seed_id, creator_id
   )
SELECT
    ent.entry_code,
    ent.entry_number,
    ent.entry_name,
    ent.entry_type,
    ent.entry_role,
    ent.entry_status,
    ent.id AS entry_id,
    plot.id AS plot_id,
    ent.germplasm_id,
    ent.seed_id,
    person.id AS creator_id
FROM
    experiment.entry_list AS entlist
    INNER JOIN experiment.entry AS ent
        ON entlist.id = ent.entry_list_id
     INNER JOIN experiment.plot AS plot
         ON plot.entry_id = ent.id
     INNER JOIN tenant.person AS person
         ON person.username = 'nicola.costa'
WHERE
    entlist.entry_list_name = 'KE-AYT-2021-DS Entry List'
ORDER BY
     plot.plot_number
;



-- revert changes
--rollback DELETE FROM
--rollback     experiment.planting_instruction AS plantinst
--rollback USING
--rollback     experiment.entry AS ent
--rollback     INNER JOIN experiment.entry_list AS entlist
--rollback         ON ent.entry_list_id = entlist.id
--rollback WHERE
--rollback     plantinst.entry_id = ent.id
--rollback     AND entlist.entry_list_name = 'KE-AYT-2021-DS Entry List';



--changeset postgres:populate_breeding_trial_for_af_in_experiment.experiment_design context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-361 Populate breeding trial for AF in experiment.experiment_design



INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES
            (1,'ABSBL10020','101',1,'replication block',1,1,'replicate'),
            (1,'CML442','102',2,'replication block',1,1,'replicate'),
            (1,'ABDHL164672','103',3,'replication block',1,1,'replicate'),
            (1,'ABDHL163943','104',4,'replication block',1,1,'replicate'),
            (1,'ABLTI0137','105',5,'replication block',1,1,'replicate'),
            (1,'ABSBL10060','106',6,'replication block',1,1,'replicate'),
            (1,'ABDHL164665','107',7,'replication block',1,1,'replicate'),
            (1,'ABDHL120312','108',8,'replication block',1,1,'replicate'),
            (1,'CML536','109',9,'replication block',1,1,'replicate'),
            (1,'ABDHL164302','110',10,'replication block',1,1,'replicate'),
            (1,'CML539','111',11,'replication block',1,1,'replicate'),
            (1,'CML494','112',12,'replication block',1,1,'replicate'),
            (1,'CML543','113',13,'replication block',1,1,'replicate'),
            (1,'CML204','114',14,'replication block',1,1,'replicate'),
            (1,'ABDHL120918','115',15,'replication block',1,1,'replicate'),
            (1,'ABLMARSI0037','116',16,'replication block',1,1,'replicate'),
            (1,'ABLTI0330','117',17,'replication block',1,1,'replicate'),
            (1,'ABDHL0221','118',18,'replication block',1,1,'replicate'),
            (1,'CML444','119',19,'replication block',1,1,'replicate'),
            (1,'CML464','120',20,'replication block',1,1,'replicate'),
            (2,'CML464','201',21,'replication block',2,1,'replicate'),
            (2,'ABSBL10020','202',22,'replication block',2,1,'replicate'),
            (2,'CML543','203',23,'replication block',2,1,'replicate'),
            (2,'ABDHL120918','204',24,'replication block',2,1,'replicate'),
            (2,'ABDHL163943','205',25,'replication block',2,1,'replicate'),
            (2,'CML442','206',26,'replication block',2,1,'replicate'),
            (2,'ABLMARSI0037','207',27,'replication block',2,1,'replicate'),
            (2,'ABDHL0221','208',28,'replication block',2,1,'replicate'),
            (2,'ABLTI0137','209',29,'replication block',2,1,'replicate'),
            (2,'CML536','210',30,'replication block',2,1,'replicate'),
            (2,'CML539','211',31,'replication block',2,1,'replicate'),
            (2,'ABDHL120312','212',32,'replication block',2,1,'replicate'),
            (2,'ABDHL164302','213',33,'replication block',2,1,'replicate'),
            (2,'ABDHL164665','214',34,'replication block',2,1,'replicate'),
            (2,'CML204','215',35,'replication block',2,1,'replicate'),
            (2,'CML494','216',36,'replication block',2,1,'replicate'),
            (2,'ABSBL10060','217',37,'replication block',2,1,'replicate'),
            (2,'ABDHL164672','218',38,'replication block',2,1,'replicate'),
            (2,'CML444','219',39,'replication block',2,1,'replicate'),
            (2,'ABLTI0330','220',40,'replication block',2,1,'replicate'),
            (3,'CML442','301',41,'replication block',3,1,'replicate'),
            (3,'ABDHL164665','302',42,'replication block',3,1,'replicate'),
            (3,'ABDHL164302','303',43,'replication block',3,1,'replicate'),
            (3,'ABLTI0137','304',44,'replication block',3,1,'replicate'),
            (3,'CML536','305',45,'replication block',3,1,'replicate'),
            (3,'CML464','306',46,'replication block',3,1,'replicate'),
            (3,'CML204','307',47,'replication block',3,1,'replicate'),
            (3,'ABSBL10020','308',48,'replication block',3,1,'replicate'),
            (3,'ABDHL164672','309',49,'replication block',3,1,'replicate'),
            (3,'CML444','310',50,'replication block',3,1,'replicate'),
            (3,'ABDHL120918','311',51,'replication block',3,1,'replicate'),
            (3,'CML494','312',52,'replication block',3,1,'replicate'),
            (3,'ABDHL0221','313',53,'replication block',3,1,'replicate'),
            (3,'ABSBL10060','314',54,'replication block',3,1,'replicate'),
            (3,'ABLMARSI0037','315',55,'replication block',3,1,'replicate'),
            (3,'ABLTI0330','316',56,'replication block',3,1,'replicate'),
            (3,'CML543','317',57,'replication block',3,1,'replicate'),
            (3,'CML539','318',58,'replication block',3,1,'replicate'),
            (3,'ABDHL163943','319',59,'replication block',3,1,'replicate'),
            (3,'ABDHL120312','320',60,'replication block',3,1,'replicate'),
            (4,'ABDHL163943','401',61,'replication block',4,1,'replicate'),
            (4,'ABDHL164302','402',62,'replication block',4,1,'replicate'),
            (4,'ABDHL164672','403',63,'replication block',4,1,'replicate'),
            (4,'ABDHL0221','404',64,'replication block',4,1,'replicate'),
            (4,'ABDHL120918','405',65,'replication block',4,1,'replicate'),
            (4,'CML543','406',66,'replication block',4,1,'replicate'),
            (4,'CML444','407',67,'replication block',4,1,'replicate'),
            (4,'ABSBL10060','408',68,'replication block',4,1,'replicate'),
            (4,'ABDHL120312','409',69,'replication block',4,1,'replicate'),
            (4,'CML204','410',70,'replication block',4,1,'replicate'),
            (4,'CML442','411',71,'replication block',4,1,'replicate'),
            (4,'ABLMARSI0037','412',72,'replication block',4,1,'replicate'),
            (4,'CML536','413',73,'replication block',4,1,'replicate'),
            (4,'ABDHL164665','414',74,'replication block',4,1,'replicate'),
            (4,'CML539','415',75,'replication block',4,1,'replicate'),
            (4,'ABSBL10020','416',76,'replication block',4,1,'replicate'),
            (4,'CML464','417',77,'replication block',4,1,'replicate'),
            (4,'ABLTI0137','418',78,'replication block',4,1,'replicate'),
            (4,'CML494','419',79,'replication block',4,1,'replicate'),
            (4,'ABLTI0330','420',80,'replication block',4,1,'replicate')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='KE-AYT-2021-DS-001-001'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-AYT-2021-DS Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';

--set 2
INSERT INTO
   experiment.experiment_design (
       occurrence_id, design_id, plot_id, block_type, block_value,
       block_level_number, creator_id, block_name
   )
SELECT
    occ.id AS occurrence_id,
    t.design_id AS design_id,
    (SELECT ep.id FROM experiment.plot ep WHERE entry_id=(SELECT id FROM experiment.entry WHERE entry_name=t.entry_name AND entry_list_id=(SELECT id FROM experiment.entry_list WHERE entry_list_name=els.entry_list_name)) AND ep.plot_code = t.plot_code AND ep.plot_number = t.plot_number AND ep.occurrence_id = (SELECT id FROM experiment.occurrence WHERE occurrence_name=occ.occurrence_name)) AS plot_id,
    t.block_type AS block_type,
    t.block_value AS block_value,
    t.block_level_number AS block_level_number,
    psn.id AS creator_id,
    t.block_name AS block_name
FROM
    (
        VALUES
            (1,'ABLTI0330','101',1,'replication block',1,1,'replicate'),
            (1,'CML543','102',2,'replication block',1,1,'replicate'),
            (1,'ABDHL120918','103',3,'replication block',1,1,'replicate'),
            (1,'CML442','104',4,'replication block',1,1,'replicate'),
            (1,'ABDHL164665','105',5,'replication block',1,1,'replicate'),
            (1,'CML444','106',6,'replication block',1,1,'replicate'),
            (1,'ABDHL0221','107',7,'replication block',1,1,'replicate'),
            (1,'ABLMARSI0037','108',8,'replication block',1,1,'replicate'),
            (1,'ABDHL163943','109',9,'replication block',1,1,'replicate'),
            (1,'ABLTI0137','110',10,'replication block',1,1,'replicate'),
            (1,'ABSBL10060','111',11,'replication block',1,1,'replicate'),
            (1,'CML536','112',12,'replication block',1,1,'replicate'),
            (1,'ABSBL10020','113',13,'replication block',1,1,'replicate'),
            (1,'ABDHL164302','114',14,'replication block',1,1,'replicate'),
            (1,'CML539','115',15,'replication block',1,1,'replicate'),
            (1,'ABDHL120312','116',16,'replication block',1,1,'replicate'),
            (1,'CML494','117',17,'replication block',1,1,'replicate'),
            (1,'ABDHL164672','118',18,'replication block',1,1,'replicate'),
            (1,'CML464','119',19,'replication block',1,1,'replicate'),
            (1,'CML204','120',20,'replication block',1,1,'replicate'),
            (2,'ABLMARSI0037','201',21,'replication block',2,1,'replicate'),
            (2,'ABDHL120918','202',22,'replication block',2,1,'replicate'),
            (2,'CML442','203',23,'replication block',2,1,'replicate'),
            (2,'ABDHL164302','204',24,'replication block',2,1,'replicate'),
            (2,'ABDHL164672','205',25,'replication block',2,1,'replicate'),
            (2,'ABLTI0330','206',26,'replication block',2,1,'replicate'),
            (2,'ABSBL10020','207',27,'replication block',2,1,'replicate'),
            (2,'CML539','208',28,'replication block',2,1,'replicate'),
            (2,'ABLTI0137','209',29,'replication block',2,1,'replicate'),
            (2,'CML204','210',30,'replication block',2,1,'replicate'),
            (2,'CML494','211',31,'replication block',2,1,'replicate'),
            (2,'CML543','212',32,'replication block',2,1,'replicate'),
            (2,'ABSBL10060','213',33,'replication block',2,1,'replicate'),
            (2,'ABDHL163943','214',34,'replication block',2,1,'replicate'),
            (2,'ABDHL120312','215',35,'replication block',2,1,'replicate'),
            (2,'CML536','216',36,'replication block',2,1,'replicate'),
            (2,'CML464','217',37,'replication block',2,1,'replicate'),
            (2,'ABDHL0221','218',38,'replication block',2,1,'replicate'),
            (2,'ABDHL164665','219',39,'replication block',2,1,'replicate'),
            (2,'CML444','220',40,'replication block',2,1,'replicate'),
            (3,'CML444','301',41,'replication block',3,1,'replicate'),
            (3,'ABLTI0137','302',42,'replication block',3,1,'replicate'),
            (3,'CML464','303',43,'replication block',3,1,'replicate'),
            (3,'ABDHL164302','304',44,'replication block',3,1,'replicate'),
            (3,'CML442','305',45,'replication block',3,1,'replicate'),
            (3,'ABDHL120918','306',46,'replication block',3,1,'replicate'),
            (3,'ABLMARSI0037','307',47,'replication block',3,1,'replicate'),
            (3,'ABDHL163943','308',48,'replication block',3,1,'replicate'),
            (3,'CML539','309',49,'replication block',3,1,'replicate'),
            (3,'CML536','310',50,'replication block',3,1,'replicate'),
            (3,'CML204','311',51,'replication block',3,1,'replicate'),
            (3,'ABDHL164672','312',52,'replication block',3,1,'replicate'),
            (3,'ABDHL120312','313',53,'replication block',3,1,'replicate'),
            (3,'ABSBL10020','314',54,'replication block',3,1,'replicate'),
            (3,'ABDHL0221','315',55,'replication block',3,1,'replicate'),
            (3,'CML494','316',56,'replication block',3,1,'replicate'),
            (3,'ABLTI0330','317',57,'replication block',3,1,'replicate'),
            (3,'ABDHL164665','318',58,'replication block',3,1,'replicate'),
            (3,'CML543','319',59,'replication block',3,1,'replicate'),
            (3,'ABSBL10060','320',60,'replication block',3,1,'replicate'),
            (4,'ABSBL10020','401',61,'replication block',4,1,'replicate'),
            (4,'CML442','402',62,'replication block',4,1,'replicate'),
            (4,'CML539','403',63,'replication block',4,1,'replicate'),
            (4,'CML444','404',64,'replication block',4,1,'replicate'),
            (4,'ABDHL164672','405',65,'replication block',4,1,'replicate'),
            (4,'ABDHL120918','406',66,'replication block',4,1,'replicate'),
            (4,'ABDHL0221','407',67,'replication block',4,1,'replicate'),
            (4,'CML204','408',68,'replication block',4,1,'replicate'),
            (4,'ABLMARSI0037','409',69,'replication block',4,1,'replicate'),
            (4,'CML494','410',70,'replication block',4,1,'replicate'),
            (4,'CML464','411',71,'replication block',4,1,'replicate'),
            (4,'CML536','412',72,'replication block',4,1,'replicate'),
            (4,'ABDHL164302','413',73,'replication block',4,1,'replicate'),
            (4,'ABSBL10060','414',74,'replication block',4,1,'replicate'),
            (4,'ABDHL164665','415',75,'replication block',4,1,'replicate'),
            (4,'CML543','416',76,'replication block',4,1,'replicate'),
            (4,'ABLTI0137','417',77,'replication block',4,1,'replicate'),
            (4,'ABLTI0330','418',78,'replication block',4,1,'replicate'),
            (4,'ABDHL163943','419',79,'replication block',4,1,'replicate'),
            (4,'ABDHL120312','420',80,'replication block',4,1,'replicate')
) AS t (design_id, entry_name, plot_code, plot_number, block_type, block_value, block_level_number, block_name)
INNER JOIN
    experiment.occurrence occ
ON
    occ.occurrence_name='KE-AYT-2021-DS-001-002'
INNER JOIN
    experiment.entry_list els
ON
    els.entry_list_name = 'KE-AYT-2021-DS Entry List'
INNER JOIN
     tenant.person psn
ON
    psn.username = 'nicola.costa';



--rollback DELETE FROM experiment.experiment_design WHERE occurrence_id IN (SELECT id FROM experiment.occurrence WHERE occurrence_name IN ('KE-AYT-2021-DS-001-001','KE-AYT-2021-DS-001-002'));