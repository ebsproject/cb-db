--liquibase formatted sql

--changeset postgres:populate_barley_germplasm_names context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.germplasm_name



-- populate barley germplasm names from their designation
INSERT INTO
    germplasm.germplasm_name (
        germplasm_id,name_value,germplasm_name_type,germplasm_name_status,germplasm_normalized_name,creator_id
    )
SELECT
    ge.id AS germplasm_id,
    ge.designation AS name_value,
    ge.germplasm_name_type,
    'standard' AS germplasm_name_status,
    ge.germplasm_normalized_name,
    1 AS creator_id
FROM
    germplasm.germplasm AS ge
    JOIN tenant.crop AS crop
        ON crop.id = ge.crop_id
WHERE
    crop.crop_code = 'BARLEY'
ORDER BY
    ge.id
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.germplasm_name AS gename
--rollback USING
--rollback     germplasm.germplasm AS ge
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     gename.germplasm_id = ge.id
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
