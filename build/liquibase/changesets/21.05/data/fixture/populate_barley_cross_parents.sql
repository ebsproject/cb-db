--liquibase formatted sql

--changeset postgres:populate_barley_cross_parents context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-331 Populate barley in germplasm.cross_parent



-- populate barley cross parents
WITH t_cross AS (
    SELECT
        DISTINCT
        crs.id,
        crs.cross_name
    FROM
        germplasm.cross AS crs,
        germplasm.germplasm AS ge
        JOIN tenant.crop AS crop
            ON crop.id = ge.crop_id
    WHERE
        crs.cross_name = ge.parentage
        AND crop.crop_code = 'BARLEY'
), t_parent AS (
    SELECT
        t.id AS cross_id,
        t.cross_name,
        regexp_split_to_table(t.cross_name, '\/') AS designation,
        unnest(ARRAY['female','male']) AS parent_role,
        unnest(ARRAY[1,2]) AS order_number
    FROM
        t_cross AS t
)
INSERT INTO
    germplasm.cross_parent (
        cross_id,germplasm_id,parent_role,order_number,seed_id,experiment_id,entry_id,creator_id
    )
SELECT
    t.cross_id,
    ge.id AS germplasm_id,
    t.parent_role,
    t.order_number,
    NULL AS seed_id,
    NULL AS experiment_id,
    NULL AS entry_id,
    1 AS creator_id
FROM
    t_parent AS t
    JOIN germplasm.germplasm AS ge
        ON ge.designation = t.designation
;



-- revert changes
--rollback DELETE FROM
--rollback     germplasm.cross_parent AS crspar
--rollback USING
--rollback     germplasm.cross AS crs
--rollback     JOIN germplasm.germplasm AS ge
--rollback         ON ge.parentage = crs.cross_name
--rollback     JOIN tenant.crop AS crop
--rollback         ON crop.id = ge.crop_id
--rollback WHERE
--rollback     crspar.cross_id = crs.id
--rollback     AND ge.parentage <> '?/?'
--rollback     AND crop.crop_code = 'BARLEY'
--rollback ;
