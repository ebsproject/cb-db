--liquibase formatted sql

--changeset postgres:add_new_wheat_germplasm_names_to_germplasm.germplasm_name context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-358 Add new wheat germplasm names to wheat germplasm.germplasm_name



INSERT INTO
    germplasm.germplasm_name
        (germplasm_id, name_value, germplasm_name_type, germplasm_name_status, germplasm_normalized_name, creator_id)
SELECT
    gm.id AS germplasm_id,
    t.name_value AS name_value,
    t.germplasm_name_type AS germplasm_name_type,
    t.germplasm_name_status AS germplasm_name_status,
    t.germplasm_normalized_name AS germplasm_normalized_name,
    1 as creator_id
FROM
    (
        VALUES
        ('CMSA09Y00383T-099B-050Y-050ZTM-0NJ-099NJ-11WGY-0B','CHEETAH','cross_name','active', platform.normalize_text('CHEETAH')),
        ('CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B','JAGUAR1','cross_name','active', platform.normalize_text('JAGUAR1')),
        ('CMSS08B00196S-099M-099NJ-099NJ-1WGY-0B','JAG1','cross_abbreviation','active', platform.normalize_text('JAG1')),
        ('CMSS11B00311S-099M-099NJ-099NJ-7RGY-0B','LYNX22','cross_name','active', platform.normalize_text('LYNX22')),
        ('CMSS11B01003T-099TOPY-099M-0SY-24M-0WGY','LION','cross_name','active', platform.normalize_text('LION')),
        ('CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B','MOUNTAINLION','cross_name','active', platform.normalize_text('MOUNTAINLION')),
        ('CMSS11Y00844T-099TOPM-099Y-099M-099NJ-099NJ-4RGY-0B','MTLION','cross_abbreviation','active', platform.normalize_text('MTLION')),
        ('CMSS12Y00651T-099TOPM-099Y-099M-0SY-18M-0WGY','TIGRESS18','cross_name','active', platform.normalize_text('TIGRESS18')),
        ('CMSS12Y00651T-099TOPM-099Y-099M-0SY-18M-0WGY','TIGR18','cross_abbreviation','active', platform.normalize_text('TIGR18')),
        ('CMSS13B01578T-099TOPY-099M-0SY-22M-0WGY','LYNX20','cross_name','active', platform.normalize_text('LYNX20'))
    ) AS t (designation,name_value,germplasm_name_type,germplasm_name_status,germplasm_normalized_name)
INNER JOIN
    germplasm.germplasm gm
ON  
    gm.designation = t.designation
;



--rollback DELETE FROM germplasm.germplasm_name WHERE name_value IN ('CHEETAH','JAGUAR1','JAG1','LYNX22','LION','MOUNTAINLION','MTLION','TIGRESS18','TIGR18','LYNX20');