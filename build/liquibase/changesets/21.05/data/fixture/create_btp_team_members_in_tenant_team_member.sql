--liquibase formatted sql

--changeset postgres:create_btp_team_members_in_tenant_team_member context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-310 Create BTP_TEAM members in tenant.team_member



-- migrate BTP_TEAM members
INSERT INTO tenant.team_member
    (team_id,person_id,person_role_id,order_number,creator_id)
SELECT
    team.id AS team_id,
    prsn.id as person_id,
    prsn.person_role_id,
    ROW_NUMBER() OVER () AS order_number,
    1 AS creator_id
FROM
    tenant.person AS prsn
    JOIN tenant.team AS team
        ON team.team_code = 'BTP_TEAM'
WHERE
    prsn.person_type = 'admin'
    AND prsn.id NOT IN (0, 1)
    AND prsn.is_void = FALSE
;

-- update sequence
SELECT SETVAL('tenant.team_member_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.team_member;



-- revert changes
--rollback DELETE FROM tenant.team_member WHERE team_id = (SELECT id FROM tenant.team WHERE team_code = 'BTP_TEAM');

--rollback SELECT SETVAL('tenant.team_member_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM tenant.team_member;
