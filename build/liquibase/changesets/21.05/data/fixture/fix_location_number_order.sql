--liquibase formatted sql

--changeset postgres:fix_location_number_order context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-427 Fix location_number order



WITH t1 AS (
    SELECT 
        location_year,
        site_id,
        season_id,
        count(1),
        array_agg(id ORDER BY id) ids
    FROM 
        experiment.location 
    GROUP BY 
        location_year,site_id,season_id
    HAVING 
        COUNT(1) > 1
), t2 AS (
    SELECT
        unnest id,
        ordinality
    FROM
        t1 t, unnest(ids) WITH ordinality
)
UPDATE 
    experiment.location el
SET
    location_number = t2.ordinality
    FROM t2
WHERE
    el.id = t2.id
;



--rollback UPDATE experiment.location SET location_number=1 WHERE id=71;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=72;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=73;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=74;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=164;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=170;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=205;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=206;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=228;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=230;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=231;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=232;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=233;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=234;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=235;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=236;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=323;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=340;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=359;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=375;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=378;
--rollback UPDATE experiment.location SET location_number=25 WHERE id=385;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=390;
--rollback UPDATE experiment.location SET location_number=26 WHERE id=421;
--rollback UPDATE experiment.location SET location_number=27 WHERE id=463;
--rollback UPDATE experiment.location SET location_number=28 WHERE id=473;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=511;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=524;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=557;
--rollback UPDATE experiment.location SET location_number=19 WHERE id=698;
--rollback UPDATE experiment.location SET location_number=20 WHERE id=700;
--rollback UPDATE experiment.location SET location_number=21 WHERE id=705;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=706;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=714;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=715;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=716;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=720;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=721;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=737;
--rollback UPDATE experiment.location SET location_number=22 WHERE id=804;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=815;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=816;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=821;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=826;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=836;
--rollback UPDATE experiment.location SET location_number=24 WHERE id=843;
--rollback UPDATE experiment.location SET location_number=21 WHERE id=885;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=893;
--rollback UPDATE experiment.location SET location_number=19 WHERE id=894;
--rollback UPDATE experiment.location SET location_number=22 WHERE id=895;
--rollback UPDATE experiment.location SET location_number=23 WHERE id=903;
--rollback UPDATE experiment.location SET location_number=24 WHERE id=907;
--rollback UPDATE experiment.location SET location_number=25 WHERE id=916;
--rollback UPDATE experiment.location SET location_number=26 WHERE id=919;
--rollback UPDATE experiment.location SET location_number=27 WHERE id=1003;
--rollback UPDATE experiment.location SET location_number=28 WHERE id=1004;
--rollback UPDATE experiment.location SET location_number=29 WHERE id=1005;
--rollback UPDATE experiment.location SET location_number=30 WHERE id=1015;
--rollback UPDATE experiment.location SET location_number=31 WHERE id=1016;
--rollback UPDATE experiment.location SET location_number=32 WHERE id=1017;
--rollback UPDATE experiment.location SET location_number=33 WHERE id=1028;
--rollback UPDATE experiment.location SET location_number=34 WHERE id=1036;
--rollback UPDATE experiment.location SET location_number=35 WHERE id=1037;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=1047;
--rollback UPDATE experiment.location SET location_number=36 WHERE id=1061;
--rollback UPDATE experiment.location SET location_number=37 WHERE id=1073;
--rollback UPDATE experiment.location SET location_number=38 WHERE id=1085;
--rollback UPDATE experiment.location SET location_number=47 WHERE id=1099;
--rollback UPDATE experiment.location SET location_number=51 WHERE id=1100;
--rollback UPDATE experiment.location SET location_number=39 WHERE id=1105;
--rollback UPDATE experiment.location SET location_number=40 WHERE id=1110;
--rollback UPDATE experiment.location SET location_number=41 WHERE id=1114;
--rollback UPDATE experiment.location SET location_number=42 WHERE id=1115;
--rollback UPDATE experiment.location SET location_number=43 WHERE id=1124;
--rollback UPDATE experiment.location SET location_number=44 WHERE id=1125;
--rollback UPDATE experiment.location SET location_number=45 WHERE id=1129;
--rollback UPDATE experiment.location SET location_number=46 WHERE id=1131;
--rollback UPDATE experiment.location SET location_number=48 WHERE id=1191;
--rollback UPDATE experiment.location SET location_number=49 WHERE id=1193;
--rollback UPDATE experiment.location SET location_number=52 WHERE id=1200;
--rollback UPDATE experiment.location SET location_number=53 WHERE id=1201;
--rollback UPDATE experiment.location SET location_number=50 WHERE id=1217;
--rollback UPDATE experiment.location SET location_number=54 WHERE id=1218;
--rollback UPDATE experiment.location SET location_number=55 WHERE id=1313;
--rollback UPDATE experiment.location SET location_number=56 WHERE id=1314;
--rollback UPDATE experiment.location SET location_number=57 WHERE id=1358;
--rollback UPDATE experiment.location SET location_number=59 WHERE id=1360;
--rollback UPDATE experiment.location SET location_number=58 WHERE id=1361;
--rollback UPDATE experiment.location SET location_number=60 WHERE id=1362;
--rollback UPDATE experiment.location SET location_number=61 WHERE id=1367;
--rollback UPDATE experiment.location SET location_number=62 WHERE id=1377;
--rollback UPDATE experiment.location SET location_number=63 WHERE id=1378;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=1380;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=1381;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=1382;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=1383;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=1384;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=1389;
--rollback UPDATE experiment.location SET location_number=20 WHERE id=1396;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=1397;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=1405;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=1408;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=1411;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=1412;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=1413;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=1414;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=1489;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=1491;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=1504;
--rollback UPDATE experiment.location SET location_number=23 WHERE id=1507;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=1508;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=1520;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=1521;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=1527;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=1540;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=1567;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=1572;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=1603;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=1621;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=1697;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=1746;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=1763;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=1764;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=1765;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=1766;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=1769;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=1771;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=1785;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=1786;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=1788;
--rollback UPDATE experiment.location SET location_number=19 WHERE id=1789;
--rollback UPDATE experiment.location SET location_number=20 WHERE id=1792;
--rollback UPDATE experiment.location SET location_number=21 WHERE id=1793;
--rollback UPDATE experiment.location SET location_number=22 WHERE id=1794;
--rollback UPDATE experiment.location SET location_number=23 WHERE id=1796;
--rollback UPDATE experiment.location SET location_number=24 WHERE id=1797;
--rollback UPDATE experiment.location SET location_number=25 WHERE id=1798;
--rollback UPDATE experiment.location SET location_number=26 WHERE id=1799;
--rollback UPDATE experiment.location SET location_number=27 WHERE id=1800;
--rollback UPDATE experiment.location SET location_number=28 WHERE id=1801;
--rollback UPDATE experiment.location SET location_number=29 WHERE id=1822;
--rollback UPDATE experiment.location SET location_number=30 WHERE id=1825;
--rollback UPDATE experiment.location SET location_number=31 WHERE id=1840;
--rollback UPDATE experiment.location SET location_number=32 WHERE id=1841;
--rollback UPDATE experiment.location SET location_number=33 WHERE id=1855;
--rollback UPDATE experiment.location SET location_number=34 WHERE id=1861;
--rollback UPDATE experiment.location SET location_number=35 WHERE id=1868;
--rollback UPDATE experiment.location SET location_number=36 WHERE id=1888;
--rollback UPDATE experiment.location SET location_number=37 WHERE id=1889;
--rollback UPDATE experiment.location SET location_number=38 WHERE id=1955;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=1956;
--rollback UPDATE experiment.location SET location_number=39 WHERE id=1981;
--rollback UPDATE experiment.location SET location_number=42 WHERE id=1982;
--rollback UPDATE experiment.location SET location_number=40 WHERE id=1988;
--rollback UPDATE experiment.location SET location_number=43 WHERE id=1989;
--rollback UPDATE experiment.location SET location_number=41 WHERE id=1990;
--rollback UPDATE experiment.location SET location_number=44 WHERE id=1991;
--rollback UPDATE experiment.location SET location_number=37 WHERE id=2016;
--rollback UPDATE experiment.location SET location_number=45 WHERE id=2026;
--rollback UPDATE experiment.location SET location_number=46 WHERE id=2059;
--rollback UPDATE experiment.location SET location_number=47 WHERE id=2125;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=2134;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=2163;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=2166;
--rollback UPDATE experiment.location SET location_number=48 WHERE id=2177;
--rollback UPDATE experiment.location SET location_number=49 WHERE id=2178;
--rollback UPDATE experiment.location SET location_number=50 WHERE id=2179;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=2181;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=2183;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=2184;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=2221;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=2223;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=2224;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=2225;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=2226;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=2227;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=2232;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=2233;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=2235;
--rollback UPDATE experiment.location SET location_number=24 WHERE id=2247;
--rollback UPDATE experiment.location SET location_number=26 WHERE id=2248;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=2249;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=2254;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=2255;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=2312;
--rollback UPDATE experiment.location SET location_number=19 WHERE id=2313;
--rollback UPDATE experiment.location SET location_number=20 WHERE id=2314;
--rollback UPDATE experiment.location SET location_number=21 WHERE id=2323;
--rollback UPDATE experiment.location SET location_number=22 WHERE id=2324;
--rollback UPDATE experiment.location SET location_number=23 WHERE id=2332;
--rollback UPDATE experiment.location SET location_number=25 WHERE id=2335;
--rollback UPDATE experiment.location SET location_number=27 WHERE id=2336;
--rollback UPDATE experiment.location SET location_number=33 WHERE id=2338;
--rollback UPDATE experiment.location SET location_number=28 WHERE id=2359;
--rollback UPDATE experiment.location SET location_number=29 WHERE id=2412;
--rollback UPDATE experiment.location SET location_number=43 WHERE id=2441;
--rollback UPDATE experiment.location SET location_number=30 WHERE id=2442;
--rollback UPDATE experiment.location SET location_number=45 WHERE id=2443;
--rollback UPDATE experiment.location SET location_number=46 WHERE id=2445;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=2446;
--rollback UPDATE experiment.location SET location_number=2 WHERE id=2449;
--rollback UPDATE experiment.location SET location_number=31 WHERE id=2465;
--rollback UPDATE experiment.location SET location_number=34 WHERE id=2466;
--rollback UPDATE experiment.location SET location_number=32 WHERE id=2467;
--rollback UPDATE experiment.location SET location_number=35 WHERE id=2468;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=2527;
--rollback UPDATE experiment.location SET location_number=36 WHERE id=2531;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=2608;
--rollback UPDATE experiment.location SET location_number=16 WHERE id=2632;
--rollback UPDATE experiment.location SET location_number=17 WHERE id=2633;
--rollback UPDATE experiment.location SET location_number=18 WHERE id=2634;
--rollback UPDATE experiment.location SET location_number=19 WHERE id=2666;
--rollback UPDATE experiment.location SET location_number=20 WHERE id=2674;
--rollback UPDATE experiment.location SET location_number=21 WHERE id=2675;
--rollback UPDATE experiment.location SET location_number=22 WHERE id=2676;
--rollback UPDATE experiment.location SET location_number=23 WHERE id=2701;
--rollback UPDATE experiment.location SET location_number=24 WHERE id=2702;
--rollback UPDATE experiment.location SET location_number=40 WHERE id=2707;
--rollback UPDATE experiment.location SET location_number=25 WHERE id=2738;
--rollback UPDATE experiment.location SET location_number=26 WHERE id=2739;
--rollback UPDATE experiment.location SET location_number=27 WHERE id=2740;
--rollback UPDATE experiment.location SET location_number=28 WHERE id=2741;
--rollback UPDATE experiment.location SET location_number=29 WHERE id=2795;
--rollback UPDATE experiment.location SET location_number=30 WHERE id=2797;
--rollback UPDATE experiment.location SET location_number=31 WHERE id=2837;
--rollback UPDATE experiment.location SET location_number=32 WHERE id=2842;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=2846;
--rollback UPDATE experiment.location SET location_number=33 WHERE id=2918;
--rollback UPDATE experiment.location SET location_number=34 WHERE id=2929;
--rollback UPDATE experiment.location SET location_number=35 WHERE id=2932;
--rollback UPDATE experiment.location SET location_number=36 WHERE id=2939;
--rollback UPDATE experiment.location SET location_number=37 WHERE id=2942;
--rollback UPDATE experiment.location SET location_number=38 WHERE id=3024;
--rollback UPDATE experiment.location SET location_number=39 WHERE id=3026;
--rollback UPDATE experiment.location SET location_number=41 WHERE id=3028;
--rollback UPDATE experiment.location SET location_number=42 WHERE id=3030;
--rollback UPDATE experiment.location SET location_number=44 WHERE id=3040;
--rollback UPDATE experiment.location SET location_number=3 WHERE id=3041;
--rollback UPDATE experiment.location SET location_number=4 WHERE id=3042;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=3043;
--rollback UPDATE experiment.location SET location_number=6 WHERE id=3044;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=3370;
--rollback UPDATE experiment.location SET location_number=10 WHERE id=3371;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=3373;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=3374;
--rollback UPDATE experiment.location SET location_number=14 WHERE id=3375;
--rollback UPDATE experiment.location SET location_number=15 WHERE id=3376;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3377;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3378;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3379;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3380;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3381;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3382;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3383;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3384;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3385;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3386;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3387;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3388;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3389;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3390;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3391;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3392;
--rollback UPDATE experiment.location SET location_number=7 WHERE id=3393;
--rollback UPDATE experiment.location SET location_number=8 WHERE id=3394;
--rollback UPDATE experiment.location SET location_number=9 WHERE id=3395;
--rollback UPDATE experiment.location SET location_number=11 WHERE id=3396;
--rollback UPDATE experiment.location SET location_number=12 WHERE id=3397;
--rollback UPDATE experiment.location SET location_number=13 WHERE id=3398;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3399;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3400;
--rollback UPDATE experiment.location SET location_number=5 WHERE id=3401;
--rollback UPDATE experiment.location SET location_number=1 WHERE id=3402;