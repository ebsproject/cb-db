--liquibase formatted sql

--changeset postgres:update_entry_type_of_experiments_with_experiment_type_breeding_trial_and_observation context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-326 Update entry_type of experiments with experiment_type Breeding Trial and Observation



-- update entry_type from 'entry' to 'test'
UPDATE
    experiment.entry
SET
    entry_type = 'test'
WHERE
    entry_list_id
IN (
        SELECT id FROM experiment.entry_list WHERE experiment_id IN (
            SELECT id FROM experiment.experiment WHERE experiment_type IN ('Breeding Trial', 'Observation')
        )
    )
AND
    entry_type = 'entry'
;



-- rollback
--rollback UPDATE
--rollback     experiment.entry
--rollback SET
--rollback     entry_type = 'entry'
--rollback WHERE
--rollback     entry_list_id
--rollback IN (
--rollback         SELECT id FROM experiment.entry_list WHERE experiment_id IN (
--rollback             SELECT id FROM experiment.experiment WHERE experiment_type IN ('Breeding Trial', 'Observation')
--rollback         )
--rollback     )
--rollback AND
--rollback     entry_type = 'test'
--rollback ;