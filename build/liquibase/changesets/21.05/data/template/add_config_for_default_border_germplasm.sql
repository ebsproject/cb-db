--liquibase formatted sql

--changeset postgres:add_config_for_default_border_germplasm context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-348 Add config for DEFAULT_BORDER_GERMPLASM



-- add config for DEFAULT_BORDER_GERMPLASM 
INSERT INTO 
    platform.config (
        abbrev, name, config_value, usage, creator_id
    )
VALUES (
    'DEFAULT_BORDER_GERMPLASM',
    'Default Configuration for Border Germplasm',
    '{
            "0": {
                "germplasm_id": "0"
            }
    }',
    'kdx',
    1
);



-- revert changes
--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev = 'DEFAULT_BORDER_GERMPLASM'
--rollback ;
