--liquibase formatted sql

--changeset postgres:update_scale_values_of_entry_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-343 Update scale values of ENTRY_TYPE variable



--add scale value border
INSERT INTO 
    master.scale_value
        (scale_id,value,order_number,description,display_name,scale_value_status,abbrev,creator_id)
SELECT
    mv.scale_id AS scale_id,
    t.value AS value,
    t.order_number AS order_number,
    t.description AS description,
    t.display_name AS display_name,
    t.scale_value_status AS scale_value_status,
    t.abbrev AS abbrev,
    1 AS creator_id
FROM
    (
        VALUES
            ('ENTRY_TYPE','border',8,'Border','Border','show','ENTRY_TYPE_BORDER')
    ) AS t(var_abbrev,value,order_number,description,display_name,scale_value_status,abbrev)
INNER JOIN
    master.variable mv
ON
    mv.abbrev = t.var_abbrev;

--update filler scale_value_status to show
UPDATE
    master.scale_value
SET
   scale_value_status = 'show'
WHERE
    abbrev='ENTRY_TYPE_FILLER';



--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback    scale_value_status = 'hide'
--rollback WHERE
--rollback     abbrev='ENTRY_TYPE_FILLER';

--rollback DELETE FROM master.scale_value WHERE abbrev='ENTRY_TYPE_BORDER';