--liquibase formatted sql

--changeset postgres:update_admin_navigation_fix context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-384 Update ADMIN navigation in platform.space (fix to ESD-391)



-- update ADMIN navigation label in platform.space
UPDATE 
    platform.space
SET 
    menu_data = 
    '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "planting-instructions-manager",
                "label": "Planting instructions manager",
                "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
            }, {
                "name": "searchs-germplasm",
                "label": "Germplasm",
                "appAbbrev": "GERMPLASM_CATALOG"
            }, {
                "name": "search-seeds",
                "label": "Seed search",
                "appAbbrev": "FIND_SEEDS"
            }, {
                "name": "search-traits",
                "label": "Traits",
                "appAbbrev": "TRAITS"
            }],
            "main_menu_items": [{
                "icon": "help_outline",
                "name": "help",
                "items": [{
                    "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
                    "icon": "headset_mic",
                    "name": "help_support-portal",
                    "label": "Support portal",
                    "tooltip": "Go to EBS Service Desk portal"
                }, {
                    "url": "https://uat-b4rapi.b4rdev.org",
                    "icon": "code",
                    "name": "help_api",
                    "label": "API",
                    "tooltip": "Go to B4R API Documentation"
                }],
                "label": "Help"
            }, {
                "icon": "folder_special",
                "name": "data-management",
                "items": [{
                    "name": "data-management_seasons",
                    "label": "Seasons",
                    "appAbbrev": "MANAGE_SEASONS"
                }],
                "label": "Data management"
            }, {
                "icon": "settings",
                "name": "administration",
                "items": [{
                    "name": "administration_users",
                    "label": "Persons",
                    "appAbbrev": "PERSONS"
                }],
                "label": "Administration"
            }]
        }
    '
WHERE
    abbrev = 'ADMIN';



-- rollback
--rollback UPDATE 
--rollback     platform.space
--rollback SET 
--rollback     menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [{
--rollback                 "name": "experiment-creation",
--rollback                 "label": "Experiment creation",
--rollback                 "appAbbrev": "EXPERIMENT_CREATION"
--rollback             }, {
--rollback                 "name": "experiment-manager",
--rollback                 "label": "Experiment manager",
--rollback                 "appAbbrev": "OCCURRENCES"
--rollback             }, {
--rollback                 "name": "planting-instructions-manager",
--rollback                 "label": "Planting instructions manager",
--rollback                 "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
--rollback             }, {
--rollback                 "name": "data-collection-qc-quality-control",
--rollback                 "label": "Data collection",
--rollback                 "appAbbrev": "QUALITY_CONTROL"
--rollback             }, {
--rollback                 "name": "seeds-harvest-manager",
--rollback                 "label": "Harvest manager",
--rollback                 "appAbbrev": "HARVEST_MANAGER"
--rollback             }, {
--rollback                 "name": "searchs-germplasm",
--rollback                 "label": "Germplasm",
--rollback                 "appAbbrev": "GERMPLASM_CATALOG"
--rollback             }, {
--rollback                 "name": "search-seeds",
--rollback                 "label": "Seed search",
--rollback                 "appAbbrev": "FIND_SEEDS"
--rollback             }, {
--rollback                 "name": "search-traits",
--rollback                 "label": "Traits",
--rollback                 "appAbbrev": "TRAITS"
--rollback             }],
--rollback             "main_menu_items": [{
--rollback                 "icon": "help_outline",
--rollback                 "name": "help",
--rollback                 "items": [{
--rollback                     "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
--rollback                     "icon": "headset_mic",
--rollback                     "name": "help_support-portal",
--rollback                     "label": "Support portal",
--rollback                     "tooltip": "Go to EBS Service Desk portal"
--rollback                 }, {
--rollback                     "url": "https://uat-b4rapi.b4rdev.org",
--rollback                     "icon": "code",
--rollback                     "name": "help_api",
--rollback                     "label": "API",
--rollback                     "tooltip": "Go to B4R API Documentation"
--rollback                 }],
--rollback                 "label": "Help"
--rollback             }]
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'ADMIN';