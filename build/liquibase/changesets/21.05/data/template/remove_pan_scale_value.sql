--liquibase formatted sql

--changeset postgres:remove_pan_scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-396 Remove pan scale value



DELETE FROM 
    master.scale_value
WHERE
   scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PACKAGE_UNIT')
;


INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PACKAGE_UNIT_PLANT', 'plant', 1, 'Plant', 'Plant', 1, 'show'),
        ('PACKAGE_UNIT_PANICLES', 'panicles', 2, 'Panicles', 'Panicles', 1, 'show'),
        ('PACKAGE_UNIT_G', 'g', 3, 'Grams', 'Grams', 1, 'show'),
        ('PACKAGE_UNIT_SEEDS', 'seeds', 4, 'Seeds', 'Seeds', 1, 'show'),
        ('PACKAGE_UNIT_KG', 'kg', 5, 'Kilograms', 'Kilograms', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PACKAGE_UNIT'
;



--rollback DELETE FROM 
--rollback     master.scale_value
--rollback WHERE
--rollback    scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PACKAGE_UNIT')
--rollback ;
--rollback 
--rollback 
--rollback INSERT INTO master.scale_value
--rollback     (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.abbrev,
--rollback     scalval.value,
--rollback     scalval.order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.creator_id,
--rollback     scalval.scale_value_status
--rollback FROM
--rollback     master.variable AS var,
--rollback     (
--rollback         VALUES
--rollback         ('PACKAGE_UNIT_PLANT', 'plant', 1, 'Plant', 'Plant', 1, 'show'),
--rollback         ('PACKAGE_UNIT_PANICLES', 'panicles', 2, 'Panicles', 'Panicles', 1, 'show'),
--rollback         ('PACKAGE_UNIT_G', 'g', 3, 'Grams', 'Grams', 1, 'show'),
--rollback         ('PACKAGE_UNIT_PAN', 'pan', 4, 'Panicles', 'Panicles', 1, 'show'),
--rollback         ('PACKAGE_UNIT_SEEDS', 'seeds', 5, 'Seeds', 'Seeds', 1, 'show'),
--rollback         ('PACKAGE_UNIT_KG', 'kg', 6, 'Kilograms', 'Kilograms', 1, 'show')
--rollback     ) AS scalval (
--rollback         abbrev, value, order_number, description, display_name, creator_id, scale_value_status
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'PACKAGE_UNIT'
--rollback ;