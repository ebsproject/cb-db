--liquibase formatted sql

--changeset postgres:update_scale_values_of_occurrence_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-445 Update scale values of OCCURRENCE_STATUS variable



INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, scale_value_status, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('OCCURRENCE_STATUS_PLANTED_DRAFT_PACKING', 'planted;draft packing', 22, 'Planted; Draft Packing', 'Planted; Draft Packing', 'show', 1),
        ('OCCURRENCE_STATUS_PLANTED_READY_FOR_PACKING', 'planted;ready for packing', 23, 'Planted; Ready for Packing', 'Planted; Ready for Packing', 'show', 1),
        ('OCCURRENCE_STATUS_PLANTED_PACKING', 'planted;packing', 24, 'Planted; Packing', 'Planted; Packing', 'show', 1),
        ('OCCURRENCE_STATUS_PLANTED_PACKED', 'planted;packed', 25, 'Planted; Packed', 'Planted; Packed', 'show', 1),
        ('OCCURRENCE_STATUS_PLANTED_PACKING_ON_HOLD', 'planted;packing on hold', 26, 'Planted; Packing on Hold', 'Planted; Packing on Hold', 'show', 1),
        ('OCCURRENCE_STATUS_PLANTED_PACKING_CANCELLED', 'planted;packing cancelled', 27, 'Planted; Packing Cancelled', 'Planted; Packing Cancelled', 'show', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, scale_value_status, creator_id
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'OCCURRENCE_STATUS_PLANTED_DRAFT_PACKING',
--rollback         'OCCURRENCE_STATUS_PLANTED_READY_FOR_PACKING',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKED',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING_ON_HOLD',
--rollback         'OCCURRENCE_STATUS_PLANTED_PACKING_CANCELLED'
--rollback     )
--rollback ;