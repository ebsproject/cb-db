--liquibase formatted sql

--changeset postgres:update_scale_unit_of_protocol_variables context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-371 Update scale unit of protocol related variables



--update unit to 'm'
WITH t1 AS (
    SELECT 
        ms.id
    FROM
        master.scale ms
    INNER JOIN
        master.variable mv
    ON
        mv.scale_id = ms.id
    WHERE
        ms.id
    IN
        (
            SELECT 
                scale_id 
            FROM 
                master.variable 
            WHERE 
                abbrev 
            IN
                (
                    'ALLEY_LENGTH',
                    'BED_WIDTH',
                    'DISTANCE_BET_PLOTS_IN_M',
                    'NO_OF_ROWS_PER_BED',
                    'PLOT_WIDTH',
                    'PLOT_WIDTH_MAIZE_BED',
                    'PLOT_WIDTH_RICE',
                    'PLOT_WIDTH_WHEAT_BED',
                    'PLOT_WIDTH_WHEAT_FLAT'
                )
        )
    AND
        ms.unit IS NULL
)
UPDATE
    master.scale AS ms1
SET
    unit='m'
FROM
    t1 AS t
WHERE 
    ms1.id = t.id;

--update unit to 'sqm'
WITH t1 AS (
    SELECT 
        ms.id
    FROM
        master.scale ms
    INNER JOIN
        master.variable mv
    ON
        mv.scale_id = ms.id
    WHERE
        ms.id
    IN
        (
            SELECT 
                scale_id 
            FROM 
                master.variable 
            WHERE 
                abbrev 
            IN
                (
                    'PLOT_AREA_1',
                    'PLOT_AREA_2',
                    'PLOT_AREA_3',
                    'PLOT_AREA_4'
                )
        )
    AND
        ms.unit IS NULL
)
UPDATE
    master.scale AS ms1
SET
    unit='sqm'
FROM
    t1 AS t
WHERE 
    ms1.id = t.id;



--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         ms.id
--rollback     FROM
--rollback         master.scale ms
--rollback     INNER JOIN
--rollback         master.variable mv
--rollback     ON
--rollback         mv.scale_id = ms.id
--rollback     WHERE
--rollback         ms.id
--rollback     IN
--rollback         (
--rollback             SELECT 
--rollback                 scale_id 
--rollback             FROM 
--rollback                 master.variable 
--rollback             WHERE 
--rollback                 abbrev 
--rollback             IN
--rollback                 (
--rollback                     'ALLEY_LENGTH',
--rollback                     'BED_WIDTH',
--rollback                     'DISTANCE_BET_PLOTS_IN_M',
--rollback                     'NO_OF_ROWS_PER_BED',
--rollback                     'PLOT_WIDTH',
--rollback                     'PLOT_WIDTH_MAIZE_BED',
--rollback                     'PLOT_WIDTH_RICE',
--rollback                     'PLOT_WIDTH_WHEAT_BED',
--rollback                     'PLOT_WIDTH_WHEAT_FLAT'
--rollback                 )
--rollback         )
--rollback     AND
--rollback         ms.unit IS NOT NULL
--rollback )
--rollback UPDATE
--rollback     master.scale AS ms1
--rollback SET
--rollback     unit=NULL
--rollback FROM
--rollback     t1 AS t
--rollback WHERE 
--rollback     ms1.id = t.id;

--rollback WITH t1 AS (
--rollback     SELECT 
--rollback         ms.id
--rollback     FROM
--rollback         master.scale ms
--rollback     INNER JOIN
--rollback         master.variable mv
--rollback     ON
--rollback         mv.scale_id = ms.id
--rollback     WHERE
--rollback         ms.id
--rollback     IN
--rollback         (
--rollback             SELECT 
--rollback                 scale_id 
--rollback             FROM 
--rollback                 master.variable 
--rollback             WHERE 
--rollback                 abbrev 
--rollback             IN
--rollback                 (
--rollback                     'PLOT_AREA_1',
--rollback                     'PLOT_AREA_2',
--rollback                     'PLOT_AREA_3',
--rollback                     'PLOT_AREA_4'
--rollback                 )
--rollback         )
--rollback     AND
--rollback         ms.unit IS NOT NULL
--rollback )
--rollback UPDATE
--rollback     master.scale AS ms1
--rollback SET
--rollback     unit=NULL
--rollback FROM
--rollback     t1 AS t
--rollback WHERE 
--rollback     ms1.id = t.id;