--liquibase formatted sql

--changeset postgres:update_scale_values_of_variables_related_to_maize_germplasm context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-398 Update scale values of variables related to maize germplasm



-- GENERATION
-- add DH scale value to GENERATION variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('GENERATION_DH', 'DH', 209, 'Double Haploid', 'DH', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id
    )
WHERE
    var.abbrev = 'GENERATION'
;


-- GERMPLASM_NAME_TYPE
-- delete line_code_1 scale value from GERMPLASM_NAME_TYPE
DELETE FROM
    master.scale_value
WHERE
    value = 'line_code_1'
AND
    abbrev = 'GERMPLASM_NAME_TYPE_LINE_CODE_1'
;


-- update ordering
UPDATE
    master.scale_value AS sv
SET
    order_number = t.new_order_number
FROM (
    VALUES
        ('GERMPLASM_NAME_TYPE_SELECTION_HISTORY',31),
        ('GERMPLASM_NAME_TYPE_UNKNOWN',32),
        ('GERMPLASM_NAME_TYPE_CROSS_ABBREVIATION',33)
    ) AS t (
            abbrev, new_order_number
    )
WHERE
    sv.abbrev  = t.abbrev
AND
    sv.scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE')
;


-- GERMPLASM_TYPE
-- update DH_line to DH
UPDATE
    master.scale_value
SET
    value = 'DH',
    description = 'Double Haploid',
    display_name = 'DH',
    abbrev = 'GERMPLASM_TYPE_DH'
WHERE
    abbrev = 'GERMPLASM_TYPE_DH_LINE'
;

-- update segregating_line to segregating
UPDATE
    master.scale_value
SET
    value = 'segregating',
    description = 'segregating',
    display_name = 'segregating',
    abbrev = 'GERMPLASM_TYPE_SEGREGATING'
WHERE
    abbrev = 'GERMPLASM_TYPE_SEGREGATING_LINE'
;



-- rollback
--rollback DELETE FROM master.scale_value WHERE abbrev = 'GENERATION_DH';
--rollback 
--rollback INSERT INTO
--rollback     master.scale_value (scale_id, value, order_number, description, display_name, abbrev)
--rollback VALUES
--rollback     (
--rollback         (SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE'),
--rollback         'line_code_1',
--rollback         31,
--rollback         'line_code_1',
--rollback         'line_code_1', 
--rollback         'GERMPLASM_NAME_TYPE_LINE_CODE_1'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value AS sv
--rollback SET
--rollback     order_number = t.new_order_number
--rollback FROM (
--rollback     VALUES
--rollback         ('GERMPLASM_NAME_TYPE_SELECTION_HISTORY',32),
--rollback         ('GERMPLASM_NAME_TYPE_UNKNOWN',33),
--rollback         ('GERMPLASM_NAME_TYPE_CROSS_ABBREVIATION',34)
--rollback     ) AS t (
--rollback             abbrev, new_order_number
--rollback     )
--rollback WHERE
--rollback     sv.abbrev  = t.abbrev
--rollback AND
--rollback     sv.scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='GERMPLASM_NAME_TYPE')
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'DH_line',
--rollback     description = 'DH_line',
--rollback     display_name = 'DH_line',
--rollback     abbrev = 'GERMPLASM_TYPE_DH_LINE'
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_TYPE_DH'
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value
--rollback SET
--rollback     value = 'segregating_line',
--rollback     description = 'segregating_line',
--rollback     display_name = 'segregating_line',
--rollback     abbrev = 'GERMPLASM_TYPE_SEGREGATING_LINE'
--rollback WHERE
--rollback     abbrev = 'GERMPLASM_TYPE_SEGREGATING'
--rollback ;