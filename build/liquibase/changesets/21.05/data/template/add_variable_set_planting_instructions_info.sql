--liquibase formatted sql

--changeset postgres:add_variable_set_planting_instructions_info context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-332 Add variable set planting instructions info



INSERT INTO
    master.variable_set (abbrev,name) 
VALUES 
    ('PLANTING_INSTRUCTION_INFO','Planting Instruction Info');



--rollback DELETE FROM master.variable_set WHERE abbrev='PLANTING_INSTRUCTION_INFO';