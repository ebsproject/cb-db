--liquibase formatted sql

--changeset postgres:add_config_packing_job_fields_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-333 Add config PACKING_JOB_FIELD to platform.config



INSERT INTO 
    platform.config 
        (abbrev, name, config_value, usage, creator_id)
VALUES
    (
        'PACKING_JOB_FIELDS', 
        'Fields configuration for packing job creation',
        '
            {
                "review": {
                    "label": "Review tab additional info",
                    "fields": [{
                        "variable_type": "identification",
                        "variable_abbrev": "PLANTING_JOB_DUE_DATE"
                    }, {
                        "variable_type": "identification",
                        "variable_abbrev": "PLANTING_JOB_INSTRUCTIONS"
                    }, {
                        "variable_abbrev": "PLANTING_JOB_FACILITY",
                        "variable_type": "identification",
                        "target_column": "facilityDbId",
                        "secondary_target_column": "facilityDbId",
                        "target_value": "facilityName",
                        "api_resource_method": "POST",
                        "api_resource_endpoint": "facilities-search",
                        "api_resource_filter": {
                            "facilityType": "building"
                        },
                        "api_resource_sort": "sort=facilityName"
                    }]
                },
                "envelopes": {
                    "label": "Envelope tab",
                    "fields": [{
                        "fixed": true,
                        "disabled": true,
                        "default": "1",
                        "required": "required",
                        "display_name": "Seeds / envelope",
                        "variable_type": "identification",
                        "variable_abbrev": "SEEDS_PER_ENVELOPE",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }, {
                        "fixed": true,
                        "disabled": true,
                        "default": "g",
                        "required": "required",
                        "display_name": "Unit",
                        "variable_type": "identification",
                        "variable_abbrev": "PACKAGE_UNIT",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }, {
                        "fixed": true,
                        "disabled": true,
                        "default": "1",
                        "required": "required",
                        "display_name": "Envelopes / plot",
                        "variable_type": "identification",
                        "variable_abbrev": "ENVELOPES_PER_PLOT",
                        "api_resource_endpoint": "planting-job-occurrences"
                    }]
                }
            }
        ',
        'planting_instructions_manager',
        1
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='PACKING_JOB_FIELDS';