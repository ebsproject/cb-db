--liquibase formatted sql

--changeset postgres:update_default_navigation context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-333 Update DEFAULT navigation



UPDATE 
    platform.space
SET 
    menu_data = 
    '
        {
            "left_menu_items": [{
                "name": "experiment-creation",
                "label": "Experiment creation",
                "appAbbrev": "EXPERIMENT_CREATION"
            }, {
                "name": "experiment-manager",
                "label": "Experiment manager",
                "appAbbrev": "OCCURRENCES"
            }, {
                "name": "planting-instructions-manager",
                "label": "Planting instructions manager",
                "appAbbrev": "PLANTING_INSTRUCTIONS_MANAGER"
            }, {
                "name": "data-collection-qc-quality-control",
                "label": "Data collection",
                "appAbbrev": "QUALITY_CONTROL"
            }, {
                "name": "seeds-harvest-manager",
                "label": "Harvest manager",
                "appAbbrev": "HARVEST_MANAGER"
            }, {
                "name": "searchs-germplasm",
                "label": "Germplasm",
                "appAbbrev": "GERMPLASM_CATALOG"
            }, {
                "name": "search-seeds",
                "label": "Seeds",
                "appAbbrev": "FIND_SEEDS"
            }, {
                "name": "search-traits",
                "label": "Traits",
                "appAbbrev": "TRAITS"
            }],
            "main_menu_items": [{
                "icon": "help_outline",
                "name": "help",
                "items": [{
                    "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
                    "icon": "headset_mic",
                    "name": "help_support-portal",
                    "label": "Support portal",
                    "tooltip": "Go to EBS Service Desk portal"
                }, {
                    "url": "https://uat-b4rapi.b4rdev.org",
                    "icon": "code",
                    "name": "help_api",
                    "label": "API",
                    "tooltip": "Go to B4R API Documentation"
                }],
                "label": "Help"
            }]
        }
    '
WHERE
    abbrev = 'DEFAULT';



--rollback UPDATE 
--rollback 	platform.space
--rollback SET 
--rollback 	menu_data = 
--rollback     '
--rollback         {
--rollback             "left_menu_items": [
--rollback                 {
--rollback                     "name": "experiment-creation",
--rollback                     "label": "Experiment creation",
--rollback                     "appAbbrev": "EXPERIMENT_CREATION"
--rollback                 },
--rollback                 {
--rollback                     "name": "experiment-manager",
--rollback                     "label": "Experiment manager",
--rollback                     "appAbbrev": "OCCURRENCES"
--rollback                 },
--rollback                 {
--rollback                     "name": "data-collection-qc-quality-control",
--rollback                     "label": "Data collection",
--rollback                     "appAbbrev": "QUALITY_CONTROL"
--rollback                 },
--rollback                 {
--rollback                     "name": "seeds-harvest-manager",
--rollback                     "label": "Harvest manager",
--rollback                     "appAbbrev": "HARVEST_MANAGER"
--rollback                 },
--rollback                 {
--rollback                     "name": "searchs-germplasm",
--rollback                     "label": "Germplasm",
--rollback                     "appAbbrev": "GERMPLASM_CATALOG"
--rollback                 },
--rollback                 {
--rollback                     "name": "search-seeds",
--rollback                     "label": "Seeds",
--rollback                     "appAbbrev": "FIND_SEEDS"
--rollback                 },
--rollback                 {
--rollback                     "name": "search-traits",
--rollback                     "label": "Traits",
--rollback                     "appAbbrev": "TRAITS"
--rollback                 }
--rollback             ],
--rollback             "main_menu_items": [
--rollback                 {
--rollback                     "icon": "help_outline",
--rollback                     "name": "help",
--rollback                     "items": [
--rollback                         {
--rollback                             "url": "https://ebsproject.atlassian.net/servicedesk/customer/portals",
--rollback                             "icon": "headset_mic",
--rollback                             "name": "help_support-portal",
--rollback                             "label": "Support portal",
--rollback                             "tooltip": "Go to EBS Service Desk portal"
--rollback                         },
--rollback                         {
--rollback                             "url": "https://uat-b4rapi.b4rdev.org",
--rollback                             "icon": "code",
--rollback                             "name": "help_api",
--rollback                             "label": "API",
--rollback                             "tooltip": "Go to B4R API Documentation"
--rollback                         }
--rollback                     ],
--rollback                     "label": "Help"
--rollback                 }
--rollback             ]
--rollback         }
--rollback     '
--rollback WHERE
--rollback 	abbrev = 'DEFAULT';