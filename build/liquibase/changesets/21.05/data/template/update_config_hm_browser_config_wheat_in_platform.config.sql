--liquibase formatted sql

--changeset postgres:update_config_hm_browser_config_wheat_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-372 Update config HM_BROWSER_CONFIG_WHEAT in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "name": "HM_BROWSER_CONFIG_WHEAT",
            "values": [
                {
                    "CROSS_METHOD_SELFING": {
                        "fixed": {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed": {
                            "harvest_method": [
                                "Bulk",
                                "Selected bulk",
                                "Single Plant Selection",
                                "Individual spike"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod",
                                "numericVar"
                            ]
                        }
                    },
                    "CROSS_METHOD_SINGLE_CROSS": {
                        "fixed": {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        },
                        "not_fixed": {
                            "harvest_method": [
                                "Bulk"
                            ],
                            "display_column": [
                                "harvestDate",
                                "harvestMethod"
                            ]
                        }
                    }
                }
            ]
        }
    ' 
WHERE 
    abbrev = 'HM_BROWSER_CONFIG_WHEAT';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "name": "HM_BROWSER_CONFIG_WHEAT",
--rollback             "values": [
--rollback                 {
--rollback                     "plot": {
--rollback                         "germplasm_state": {
--rollback                             "fixed": {
--rollback                                 "display_column": [
--rollback                                     "harvestDate",
--rollback                                     "harvestMethod"
--rollback                                 ],
--rollback                                 "harvest_method": [
--rollback                                     "Bulk"
--rollback                                 ]
--rollback                             },
--rollback                             "not_fixed": {
--rollback                                 "display_column": [
--rollback                                     "harvestDate",
--rollback                                     "harvestMethod",
--rollback                                     "numericVar"
--rollback                                 ],
--rollback                                 "harvest_method": [
--rollback                                     "Bulk",
--rollback                                     "Selected bulk",
--rollback                                     "Single Plant Selection",
--rollback                                     "Individual spike"
--rollback                                 ]
--rollback                             }
--rollback                         }
--rollback                     },
--rollback                     "cross": {
--rollback                         "cross_method": {
--rollback                             "CROSS_METHOD_SINGLE_CROSS": {
--rollback                                 "harvest_method": [
--rollback                                     "Bulk"
--rollback                                 ]
--rollback                             }
--rollback                         },
--rollback                         "display_column": [
--rollback                             "harvestDate",
--rollback                             "harvestMethod"
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             ]
--rollback         }
--rollback     ' 
--rollback WHERE 
--rollback     abbrev = 'HM_BROWSER_CONFIG_WHEAT';