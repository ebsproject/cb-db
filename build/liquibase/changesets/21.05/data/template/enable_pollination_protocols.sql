--liquibase formatted sql

--changeset postgres:enable_pollination_protocols context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-449 Enable pollination protocols



UPDATE 
    master.item 
SET 
    is_void = false 
WHERE 
    abbrev 
IN 
    (
        'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT'
    )
;

UPDATE 
    platform.module 
SET 
    is_void = false 
WHERE 
    abbrev 
IN 
    (
        'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT_MOD'
    )
;



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT'
--rollback     )
--rollback ;

--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     is_void = true 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT_MOD'
--rollback     )
--rollback ;