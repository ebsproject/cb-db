--liquibase formatted sql

--changeset postgres:update_data_type_of_variables_planting_job_instructions_and_planting_job_due_date context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-383 Update data_type of variables PLANTING_JOB_INSTRUCTIONS and PLANTING_JOB_DUE_DATE



UPDATE
    master.variable
SET
    data_type = 'text'
WHERE
    abbrev='PLANTING_JOB_INSTRUCTIONS';

UPDATE
    master.variable
SET
    data_type = 'date'
WHERE
    abbrev='PLANTING_JOB_DUE_DATE';



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_type = 'character varying'
--rollback WHERE
--rollback     abbrev='PLANTING_JOB_INSTRUCTIONS';

--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_type = 'timestamp'
--rollback WHERE
--rollback     abbrev='PLANTING_JOB_DUE_DATE';