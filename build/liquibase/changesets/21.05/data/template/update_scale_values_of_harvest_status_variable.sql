--liquibase formatted sql

--changeset postgres:update_scale_values_of_harvest_status_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-339 Update scale values of HARVEST_STATUS variable



-- delete HARVEST_STATUS scale values
DELETE FROM
    master.scale_value
WHERE
    abbrev IN (
        'HARVEST_STATUS_KEEP',
        'HARVEST_STATUS_DISCARD'
    )
;


-- update HARVEST_STATUS scale values
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, scale_value_status, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('HARVEST_STATUS_NO_HARVEST', 'NO_HARVEST', 1, 'No Harvest', 'No Harvest', 'show', 1),
        ('HARVEST_STATUS_INCOMPLETE', 'INCOMPLETE', 2, 'Incomplete', 'Incomplete', 'show', 1),
        ('HARVEST_STATUS_READY', 'READY', 3, 'Ready', 'Ready', 'show', 1),
        ('HARVEST_STATUS_COMPLETED', 'COMPLETED', 4, 'Completed', 'Completed', 'show', 1),
        ('HARVEST_STATUS_DONE', 'DONE', 5, 'Done', 'Done', 'show', 1),
        ('HARVEST_STATUS_IN_PROGRESS', 'IN_PROGRESS', 6, 'In Progress', 'In Progress', 'show', 1),
        ('HARVEST_STATUS_IN_QUEUE', 'IN_QUEUE', 7, 'In Queue', 'In Queue', 'show', 1),
        ('HARVEST_STATUS_DELETION_IN_PROGRESS', 'DELETION_IN_PROGRESS', 8, 'Deletion In Progress', 'Deletion In Progress', 'show', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, scale_value_status, creator_id
    )
WHERE
    var.abbrev = 'HARVEST_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN (
--rollback         'HARVEST_STATUS_NO_HARVEST',
--rollback         'HARVEST_STATUS_INCOMPLETE',
--rollback         'HARVEST_STATUS_READY',
--rollback         'HARVEST_STATUS_COMPLETED',
--rollback         'HARVEST_STATUS_DONE',
--rollback         'HARVEST_STATUS_IN_PROGRESS',
--rollback         'HARVEST_STATUS_IN_QUEUE',
--rollback         'HARVEST_STATUS_DELETION_IN_PROGRESS'
--rollback     )
--rollback ;
--rollback 
--rollback INSERT INTO master.scale_value
--rollback     (scale_id, abbrev, value, order_number, description, display_name, creator_id)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.abbrev,
--rollback     scalval.value,
--rollback     scalval.order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.creator_id
--rollback FROM
--rollback     master.variable AS var,
--rollback     (
--rollback         VALUES
--rollback         ('HARVEST_STATUS_KEEP', 'Keep', 1, 'Keep', 'Keep', 1),
--rollback         ('HARVEST_STATUS_DISCARD', 'Discard', 2, 'Discard', 'Discard', 1)
--rollback     ) AS scalval (
--rollback         abbrev, value, order_number, description, display_name, creator_id
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'HARVEST_STATUS'
--rollback ;
