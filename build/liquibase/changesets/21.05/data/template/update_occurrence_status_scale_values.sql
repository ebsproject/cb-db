--liquibase formatted sql

--changeset postgres:update_occurrence_status_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-382 Update OCCURRENCE_STATUS scale values



--remove old records
DELETE FROM
    master.scale_value 
WHERE
    abbrev 
IN
    (
        'OCCURRENCE_STATUS_READY_FOR_PACKING',
        'OCCURRENCE_STATUS_PACKING',
        'OCCURRENCE_STATUS_PACKED',
        'OCCURRENCE_STATUS_PACKING_ON_HOLD',
        'OCCURRENCE_STATUS_PACKING_CANCELLED'
    )
;

--add new records
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, scale_value_status, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('OCCURRENCE_STATUS_CREATED_DRAFT_PACKING', 'created;draft packing', 10, 'Created; Draft Packing', 'Created; Draft Packing', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_DRAFT_PACKING', 'mapped;draft packing', 11, 'Mapped; Draft Packing', 'Mapped; Draft Packing', 'show', 1),
        ('OCCURRENCE_STATUS_CREATED_READY_FOR_PACKING', 'created;ready for packing', 12, 'Created; Ready for Packing', 'Created; Ready for Packing', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_READY_FOR_PACKING', 'mapped;ready for packing', 13, 'Mapped; Ready for Packing', 'Mapped; Ready for Packing', 'show', 1),
        ('OCCURRENCE_STATUS_CREATED_PACKING', 'created;packing', 14, 'Created; Packing', 'Created; Packing', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_PACKING', 'mapped;packing', 15, 'Mapped; Packing', 'Mapped; Packing', 'show', 1),
        ('OCCURRENCE_STATUS_CREATED_PACKED', 'created;packed', 16, 'Created; Packed', 'Created; Packed', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_PACKED', 'mapped;packed', 17, 'Mapped; Packed', 'Mapped; Packed', 'show', 1),
        ('OCCURRENCE_STATUS_CREATED_PACKING_ON_HOLD', 'created;packing on hold', 18, 'Created; Packing on Hold', 'Created; Packing on Hold', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_PACKING_ON_HOLD', 'mapped;packing on hold', 19, 'Mapped; Packing on Hold', 'Mapped; Packing on Hold', 'show', 1),
        ('OCCURRENCE_STATUS_CREATED_PACKING_CANCELLED', 'created;packing cancelled', 20, 'Created; Packing Cancelled', 'Created; Packing Cancelled', 'show', 1),
        ('OCCURRENCE_STATUS_MAPPED_PACKING_CANCELLED', 'mapped;packing cancelled', 21, 'Mapped; Packing Cancelled', 'Mapped; Packing Cancelled', 'show', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, scale_value_status, creator_id
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;



--rollback INSERT INTO master.scale_value
--rollback     (scale_id, abbrev, value, order_number, description, display_name, scale_value_status, creator_id)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.abbrev,
--rollback     scalval.value,
--rollback     scalval.order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.scale_value_status,
--rollback     scalval.creator_id
--rollback FROM
--rollback     master.variable AS var,
--rollback     (
--rollback         VALUES
--rollback         ('OCCURRENCE_STATUS_READY_FOR_PACKING', 'ready for packing', 10, 'Ready for Packing', 'Ready for Packing', 'show', 1),
--rollback         ('OCCURRENCE_STATUS_PACKING', 'packing', 11, 'Packing', 'Packing', 'show', 1),
--rollback         ('OCCURRENCE_STATUS_PACKED', 'packed', 12, 'Packed', 'Packed', 'show', 1),
--rollback         ('OCCURRENCE_STATUS_PACKING_ON_HOLD', 'packing on hold', 13, 'Packing on Hold', 'Packing on Hold', 'show', 1),
--rollback         ('OCCURRENCE_STATUS_PACKING_CANCELLED', 'packing cancelled', 14, 'Packing Cancelled', 'Packing Cancelled', 'show', 1)
--rollback     ) AS scalval (
--rollback         abbrev, value, order_number, description, display_name, scale_value_status, creator_id
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'OCCURRENCE_STATUS'
--rollback ;

--rollback DELETE FROM
--rollback     master.scale_value 
--rollback WHERE
--rollback     abbrev 
--rollback IN
--rollback     (
--rollback         'OCCURRENCE_STATUS_CREATED_DRAFT_PACKING',
--rollback         'OCCURRENCE_STATUS_MAPPED_DRAFT_PACKING',
--rollback         'OCCURRENCE_STATUS_CREATED_READY_FOR_PACKING',
--rollback         'OCCURRENCE_STATUS_MAPPED_READY_FOR_PACKING',
--rollback         'OCCURRENCE_STATUS_CREATED_PACKING',
--rollback         'OCCURRENCE_STATUS_MAPPED_PACKING',
--rollback         'OCCURRENCE_STATUS_CREATED_PACKED',
--rollback         'OCCURRENCE_STATUS_MAPPED_PACKED',
--rollback         'OCCURRENCE_STATUS_CREATED_PACKING_ON_HOLD',
--rollback         'OCCURRENCE_STATUS_MAPPED_PACKING_ON_HOLD',
--rollback         'OCCURRENCE_STATUS_CREATED_PACKING_CANCELLED',
--rollback         'OCCURRENCE_STATUS_MAPPED_PACKING_CANCELLED'
--rollback     )
--rollback ;