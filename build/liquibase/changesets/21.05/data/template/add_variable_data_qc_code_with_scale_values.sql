--liquibase formatted sql

--changeset postgres:add_variable_data_qc_code_with_scale_values context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-340 Create DATA_QC_CODE variable with scale values



DO $$
DECLARE
    var_property_id int;
    var_method_id int;
    var_scale_id int;
    var_variable_id int;
    var_variable_set_id int;
    var_variable_set_member_order_number int;
    var_count_property_id int;
    var_count_method_id int;
    var_count_scale_id int;
    var_count_variable_set_id int;
    var_count_variable_set_member_id int;
BEGIN

    --PROPERTY

    SELECT count(id) FROM master.property WHERE ABBREV = 'DATA_QC_CODE' INTO var_count_property_id;
    IF var_count_property_id > 0 THEN
        SELECT id FROM master.property WHERE ABBREV = 'DATA_QC_CODE' INTO var_property_id;
    ELSE
        INSERT INTO
            master.property (abbrev,display_name,name) 
        VALUES 
            ('DATA_QC_CODE','Data QC Code','Data QC Code') 
        RETURNING id INTO var_property_id;
    END IF;

    --METHOD

    SELECT count(id) FROM master.method WHERE ABBREV = 'DATA_QC_CODE' INTO var_count_method_id;
    IF var_count_method_id > 0 THEN
        SELECT id FROM master.method WHERE ABBREV = 'DATA_QC_CODE' INTO var_method_id;
    ELSE
        INSERT INTO
            master.method (name,abbrev,formula,description) 
        VALUES 
            ('Data QC Code','DATA_QC_CODE',NULL,NULL) 
        RETURNING id INTO var_method_id;
    END IF;

    --SCALE

    SELECT count(id) FROM master.scale WHERE ABBREV = 'DATA_QC_CODE' INTO var_count_scale_id;
    IF var_count_scale_id > 0 THEN
        SELECT id FROM master.scale WHERE ABBREV = 'DATA_QC_CODE' INTO var_scale_id;
    ELSE
        INSERT INTO
            master.scale (abbrev,type,name,unit,level) 
        VALUES 
            ('DATA_QC_CODE','discrete','Data QC Code',NULL,'nominal') 
        RETURNING id INTO var_scale_id;
    END IF;

    --SCALE VALUE

    INSERT INTO 
        master.scale_value (scale_id, value, order_number, description, display_name, abbrev) 
    VALUES
        (var_scale_id, 'N', 1, 'New Value', 'New Value', 'DATA_QC_CODE_N'),
        (var_scale_id, 'G', 2, 'Good Value', 'Good Value', 'DATA_QC_CODE_G'),
        (var_scale_id, 'Q', 3, 'Questionable Value', 'Questionable Value', 'DATA_QC_CODE_Q'),
        (var_scale_id, 'S', 4, 'Suppressed Value', 'Suppressed Value', 'DATA_QC_CODE_S'),
        (var_scale_id, 'M', 5, 'Missing Value', 'Missing Value', 'DATA_QC_CODE_M'),
        (var_scale_id, 'B', 6, 'Bad Value', 'Bad Value', 'DATA_QC_CODE_B'),
        (var_scale_id, 'I', 7, 'Invalid Value', 'Invalid Value', 'DATA_QC_CODE_I');
        

    UPDATE master.scale SET min_value='N', max_value='I' WHERE id=var_scale_id;

    --VARIABLE

    INSERT INTO
        master.variable (status,display_name,name,data_type,description,label,not_null,abbrev,usage,type,data_level) 
    VALUES 
        ('active','Data QC Code','Data QC Code','character varying','Data QC Code','Data QC Code','True','DATA_QC_CODE','study','metadata','experiment, entry, plot, cross')
    RETURNING id INTO var_variable_id;

    --UPDATE PROPERTY_ID, METHOD_ID, SCALE_ID

    UPDATE master.variable SET property_id=var_property_id, method_id=var_method_id, scale_id=var_scale_id WHERE id=var_variable_id;

    --VARIABLE_SET_ID

    SELECT count(id) FROM master.variable_set WHERE ABBREV = 'CROSS_METADATA_OBSERVATION' INTO var_count_variable_set_id;
    IF var_count_variable_set_id > 0 THEN
        SELECT id FROM master.variable_set WHERE ABBREV = 'CROSS_METADATA_OBSERVATION' INTO var_variable_set_id;
    ELSE
        INSERT INTO
            master.variable_set (abbrev,name) 
        VALUES 
            ('CROSS_METADATA_OBSERVATION','Cross_metadata_observation') 
        RETURNING id INTO var_variable_set_id;
    END IF;

    --GET THE LAST ORDER NUMBER

    SELECT count(id) FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id INTO var_count_variable_set_member_id;
    IF var_count_variable_set_member_id > 0 THEN
        SELECT max(order_number)+1 FROM master.variable_set_member WHERE variable_set_id = var_variable_set_id GROUP BY variable_set_id INTO var_variable_set_member_order_number;
    ELSE
        var_variable_set_member_order_number = 1;
    END IF;

    --ADD VARIABLE SET MEMBER

    INSERT INTO master.variable_set_member (variable_set_id, variable_id, order_number) VALUES (var_variable_set_id, var_variable_id,var_variable_set_member_order_number  );


END;
$$



--rollback DELETE FROM master.scale_value WHERE scale_id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DATA_QC_CODE');

--rollback DELETE FROM master.scale WHERE id IN (SELECT scale_id FROM master.variable WHERE abbrev = 'DATA_QC_CODE');

--rollback DELETE FROM master."property" WHERE id IN (SELECT property_id FROM master.variable WHERE abbrev = 'DATA_QC_CODE');

--rollback DELETE FROM master.method WHERE id IN (SELECT method_id FROM master.variable WHERE abbrev = 'DATA_QC_CODE');

--rollback DELETE FROM master.variable_set_member WHERE variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'DATA_QC_CODE');

--rollback DELETE FROM master.variable WHERE abbrev = 'DATA_QC_CODE';
