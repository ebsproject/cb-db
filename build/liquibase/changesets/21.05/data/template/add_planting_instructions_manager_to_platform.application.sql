--liquibase formatted sql

--changeset postgres:add_planting_instructions_manager_to_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-333 Add PLANTING_INSTRUCTIONS_MANAGER to platform.application 



INSERT INTO 
    platform.application 
        (abbrev, label, action_label, icon, creator_id)
VALUES
	(
        'PLANTING_INSTRUCTIONS_MANAGER', 
        'Planting instructions manager', 
        'Planting instructions manager', 
        'grass',
        1
    );

INSERT INTO 
    platform.application_action 
        (application_id, module, creator_id)
SELECT 
	id,
	'plantingInstruction',
	1
FROM
	platform.application
WHERE
	abbrev = 'PLANTING_INSTRUCTIONS_MANAGER';



--rollback DELETE FROM platform.application_action WHERE application_id = (SELECT id FROM platform.application WHERE abbrev='PLANTING_INSTRUCTIONS_MANAGER');
--rollback DELETE FROM platform.application WHERE abbrev='PLANTING_INSTRUCTIONS_MANAGER';