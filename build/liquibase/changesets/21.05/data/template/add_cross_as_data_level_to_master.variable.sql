--liquibase formatted sql

--changeset postgres:add_cross_as_data_level_to_master.variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-360 Add cross as data_level to master.variable



UPDATE
    master.variable
SET
    data_level = 'plot,cross'
WHERE 
    abbrev
IN
    (
        'NO_OF_PLANTS',
        'PANNO_SEL',
        'SPECIFIC_PLANT',
        'NO_OF_EARS'
    )
;



--rollback UPDATE
--rollback     master.variable
--rollback SET
--rollback     data_level = 'plot'
--rollback WHERE 
--rollback     abbrev
--rollback IN
--rollback     (
--rollback         'NO_OF_PLANTS',
--rollback         'PANNO_SEL',
--rollback         'SPECIFIC_PLANT',
--rollback         'NO_OF_EARS'
--rollback     )
--rollback ;