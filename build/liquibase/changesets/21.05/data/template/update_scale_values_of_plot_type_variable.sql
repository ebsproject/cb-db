--liquibase formatted sql

--changeset postgres:update_scale_values_of_plot_type_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-343 Update scale values of PLOT_TYPE variable



--add scale value border
INSERT INTO 
    master.scale_value
        (scale_id,value,order_number,description,display_name,scale_value_status,abbrev,creator_id)
SELECT
    mv.scale_id AS scale_id,
    t.value AS value,
    t.order_number AS order_number,
    t.description AS description,
    t.display_name AS display_name,
    t.scale_value_status AS scale_value_status,
    t.abbrev AS abbrev,
    1 AS creator_id
FROM
    (
        VALUES
            ('PLOT_TYPE','border',58,'Border','Border','show','PLOT_TYPE_BORDER'),
            ('PLOT_TYPE','filler',59,'Filler','Filler','show','PLOT_TYPE_FILLER')
    ) AS t(var_abbrev,value,order_number,description,display_name,scale_value_status,abbrev)
INNER JOIN
    master.variable mv
ON
    mv.abbrev = t.var_abbrev;



--rollback DELETE FROM master.scale_value WHERE abbrev IN ('PLOT_TYPE_BORDER','PLOT_TYPE_FILLER');