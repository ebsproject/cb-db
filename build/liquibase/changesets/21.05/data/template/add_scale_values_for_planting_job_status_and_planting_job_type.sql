--liquibase formatted sql

--changeset postgres:add_scale_values_for_planting_job_status_and_planting_job_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-356 Add scale values for PLANTING_JOB_STATUS and PLANTING_JOB_TYPE



-- add scale values to PLANTING_JOB_STATUS variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PLANTING_JOB_STATUS_READY_FOR_PACKING', 'ready for packing', 1, 'Ready for Packing', 'Ready for Packing', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING', 'packing', 2, 'Packing', 'Packing', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKED', 'packed', 3, 'Packed', 'Packed', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING_ON_HOLD', 'packing on hold', 4, 'Packing on Hold', 'Packing on Hold', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING_CANCELLED', 'packing cancelled', 5, 'Packing Cancelled', 'Packing Cancelled', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PLANTING_JOB_STATUS'
;

-- add scale values to PLANTING_JOB_TYPE variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PLANTING_JOB_TYPE_PACKING_JOB', 'packing job', 1, 'Packing Job', 'Packing Job', 1, 'show'),
        ('PLANTING_JOB_TYPE_PLANTING_JOB', 'planting job', 2, 'Planting Job', 'Planting Job', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PLANTING_JOB_TYPE'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('PLANTING_JOB_STATUS_READY_FOR_PACKING','PLANTING_JOB_STATUS_PACKING','PLANTING_JOB_STATUS_PACKED','PLANTING_JOB_STATUS_PACKING_ON_HOLD','PLANTING_JOB_STATUS_PACKING_CANCELLED')
--rollback ;

--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('PLANTING_JOB_TYPE_PACKING_JOB','PLANTING_JOB_TYPE_PLANTING_JOB')
--rollback ;