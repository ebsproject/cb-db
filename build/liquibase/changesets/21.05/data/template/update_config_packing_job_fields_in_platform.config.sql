--liquibase formatted sql

--changeset postgres:update_config_packing_job_fields_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-388 Update config PACKING_JOB_FIELDS in platform.config



UPDATE 
    platform.config 
SET 
    config_value = 
    '
        {
            "review": {
                "label": "Review tab additional info",
                "fields": [{
                    "variable_type": "identification",
                    "variable_abbrev": "PLANTING_JOB_DUE_DATE"
                }, {
                    "variable_type": "identification",
                    "variable_abbrev": "PLANTING_JOB_INSTRUCTIONS"
                }, {
                    "variable_abbrev": "PLANTING_JOB_FACILITY",
                    "variable_type": "identification",
                    "target_column": "facilityDbId",
                    "secondary_target_column": "facilityDbId",
                    "target_value": "facilityName",
                    "api_resource_method": "POST",
                    "api_resource_endpoint": "facilities-search",
                    "api_resource_filter": {
                        "facilityType": "building"
                    },
                    "api_resource_sort": "sort=facilityName"
                }]
            },
            "envelopes": {
                "label": "Envelope tab",
                "fields": [{
                    "fixed": true,
                    "default": "1",
                    "required": "required",
                    "display_name": "Seeds / envelope",
                    "variable_type": "identification",
                    "variable_abbrev": "SEEDS_PER_ENVELOPE",
                    "api_resource_endpoint": "planting-job-occurrences"
                }, {
                    "fixed": true,
                    "default": "g",
                    "required": "required",
                    "display_name": "Unit",
                    "variable_type": "identification",
                    "variable_abbrev": "PACKAGE_UNIT",
                    "api_resource_endpoint": "planting-job-occurrences"
                }, {
                    "fixed": true,
                    "default": "1",
                    "required": "required",
                    "display_name": "Envelopes / plot",
                    "variable_type": "identification",
                    "variable_abbrev": "ENVELOPES_PER_PLOT",
                    "api_resource_endpoint": "planting-job-occurrences"
                }]
            }
        }
    ' 
WHERE 
    abbrev = 'PACKING_JOB_FIELDS';



--rollback UPDATE 
--rollback     platform.config 
--rollback SET 
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "review": {
--rollback                 "label": "Review tab additional info",
--rollback                 "fields": [
--rollback                     {
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "PLANTING_JOB_DUE_DATE"
--rollback                     },
--rollback                     {
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "PLANTING_JOB_INSTRUCTIONS"
--rollback                     },
--rollback                     {
--rollback                         "target_value": "facilityName",
--rollback                         "target_column": "facilityDbId",
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "PLANTING_JOB_FACILITY",
--rollback                         "api_resource_sort": "sort=facilityName",
--rollback                         "api_resource_filter": {
--rollback                             "facilityType": "building"
--rollback                         },
--rollback                         "api_resource_method": "POST",
--rollback                         "api_resource_endpoint": "facilities-search",
--rollback                         "secondary_target_column": "facilityDbId"
--rollback                     }
--rollback                 ]
--rollback             },
--rollback             "envelopes": {
--rollback                 "label": "Envelope tab",
--rollback                 "fields": [
--rollback                     {
--rollback                         "fixed": true,
--rollback                         "default": "1",
--rollback                         "disabled": true,
--rollback                         "required": "required",
--rollback                         "display_name": "Seeds / envelope",
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "SEEDS_PER_ENVELOPE",
--rollback                         "api_resource_endpoint": "planting-job-occurrences"
--rollback                     },
--rollback                     {
--rollback                         "fixed": true,
--rollback                         "default": "g",
--rollback                         "disabled": true,
--rollback                         "required": "required",
--rollback                         "display_name": "Unit",
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "PACKAGE_UNIT",
--rollback                         "api_resource_endpoint": "planting-job-occurrences"
--rollback                     },
--rollback                     {
--rollback                         "fixed": true,
--rollback                         "default": "1",
--rollback                         "disabled": true,
--rollback                         "required": "required",
--rollback                         "display_name": "Envelopes / plot",
--rollback                         "variable_type": "identification",
--rollback                         "variable_abbrev": "ENVELOPES_PER_PLOT",
--rollback                         "api_resource_endpoint": "planting-job-occurrences"
--rollback                     }
--rollback                 ]
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE 
--rollback     abbrev = 'PACKING_JOB_FIELDS';