--liquibase formatted sql

--changeset postgres:add_planting_job_entity context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-351 Add PLANTING_JOB entity



UPDATE
    dictionary.database
SET
    abbrev = 'CB',
    name = 'Core Breeding',
    description = 'Enterprise Breeding System - Core Breeding Database',
    comment = 'Enterprise Breeding System - Core Breeding Database'
WHERE
    abbrev = 'BIMS_0.10_PROD_OLD_VERSION'
;


INSERT INTO
    dictionary.table (database_id,schema_id,abbrev,name,comment,creator_id)
SELECT 
    t.database_id,
    t.schema_id,
    t.abbrev,
    t.name,
    t.comment,
    1
FROM 
    (
        VALUES
            (
                (SELECT id FROM dictionary.database WHERE abbrev='CB'),
                (SELECT id FROM dictionary.schema WHERE abbrev='EXPERIMENT'),
                'PLANTING_JOB',
                'Planting Job',
                'Planting Job'
            )
    ) AS t (database_id,schema_id,abbrev,name,comment)
;


INSERT INTO
    dictionary.entity (abbrev,name,description,table_id,creator_id)
SELECT 
    t.abbrev,
    t.name,
    t.description,
    t.table_id,
    1
FROM 
    (
        VALUES
            (
                'PLANTING_JOB',
                'Planting Job',
                'Planting Job',
                (SELECT id FROM dictionary.table WHERE abbrev='PLANTING_JOB')
            )
    ) AS t (abbrev,name,description,table_id)
;



--rollback DELETE FROM dictionary.entity WHERE abbrev = 'PLANTING_JOB';
--rollback DELETE FROM dictionary.table WHERE abbrev = 'PLANTING_JOB';

--rollback UPDATE
--rollback     dictionary.database
--rollback SET
--rollback     abbrev = 'BIMS_0.10_PROD_OLD_VERSION',
--rollback     name = 'bims_0.10_prod_old_version',
--rollback     description = 'BIMS: Breeding Information Management System',
--rollback     comment = 'BIMS: Breeding Information Management System'
--rollback WHERE
--rollback     abbrev = 'CB'
--rollback ;