--liquibase formatted sql

--changeset postgres:void_pollination_protocols context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-362 Void pollination protocols



UPDATE 
    master.item 
SET 
    is_void = true 
WHERE 
    abbrev 
IN 
    (
        'OBSERVATION_POLLINATION_PROTOCOL_ACT',
        'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT',
        'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT'
    )
;

UPDATE 
    platform.module 
SET 
    is_void = true 
WHERE 
    abbrev 
IN 
    (
        'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD',
        'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT_MOD',
        'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT_MOD'
    )
;



--rollback UPDATE 
--rollback     master.item 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT',
--rollback         'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT',
--rollback         'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT'
--rollback     )
--rollback ;

--rollback UPDATE 
--rollback     platform.module 
--rollback SET 
--rollback     is_void = false 
--rollback WHERE 
--rollback     abbrev 
--rollback IN 
--rollback     (
--rollback         'OBSERVATION_POLLINATION_PROTOCOL_ACT_MOD',
--rollback         'BREEDING_TRIAL_POLLINATION_PROTOCOL_ACT_MOD',
--rollback         'GENERATION_NURSERY_POLLINATION_PROTOCOL_ACT_MOD'
--rollback     )
--rollback ;