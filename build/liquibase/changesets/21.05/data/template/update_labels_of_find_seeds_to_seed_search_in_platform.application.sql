--liquibase formatted sql

--changeset postgres:update_labels_of_find_seeds_to_seed_search_in_platform.application context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-291 Update labels of Find Seeds to Seed search in platform.application



-- update label and action label of FIND_SEEDS in platform.application
UPDATE
    platform.application
SET
    label = 'Seed search',
    action_label = 'Seed search'
WHERE
    abbrev = 'FIND_SEEDS'
;
   


--rollback UPDATE
--rollback     platform.application
--rollback SET
--rollback     label = 'Seeds',
--rollback     action_label = 'Find Seeds'
--rollback WHERE
--rollback     abbrev = 'FIND_SEEDS'
--rollback ;