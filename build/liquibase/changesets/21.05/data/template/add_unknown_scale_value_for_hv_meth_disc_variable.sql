--liquibase formatted sql

--changeset postgres:add_unknown_scale_value_for_hv_meth_disc_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-45 Add Unknown scale value for HV_METH_DISC variable



-- add Unknown scale value to HV_METH_DISC variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('HV_METH_DISC_UNKNOWN', 'Unknown', 18, 'Unknown harvest method (for historical purposes)', 'Unknown', 1, 'hide')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'HV_METH_DISC'
;



-- rollback
--rollback DELETE FROM master.scale_value WHERE abbrev = 'HV_METH_DISC_UNKNOWN';