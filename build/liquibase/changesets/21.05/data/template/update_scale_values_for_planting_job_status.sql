--liquibase formatted sql

--changeset postgres:update_scale_values_for_planting_job_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-369 Update scale values for PLANTING_JOB_STATUS



--delete existing scale values
DELETE FROM 
    master.scale_value 
WHERE 
    scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PLANTING_JOB_STATUS');

-- add scale values to PLANTING_JOB_STATUS variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PLANTING_JOB_STATUS_DRAFT', 'draft', 1, 'Draft', 'Draft', 1, 'show'),
        ('PLANTING_JOB_STATUS_READY_FOR_PACKING', 'ready for packing', 2, 'Ready for Packing', 'Ready for Packing', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING', 'packing', 3, 'Packing', 'Packing', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKED', 'packed', 4, 'Packed', 'Packed', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING_ON_HOLD', 'packing on hold', 5, 'Packing on Hold', 'Packing on Hold', 1, 'show'),
        ('PLANTING_JOB_STATUS_PACKING_CANCELLED', 'packing cancelled', 6, 'Packing Cancelled', 'Packing Cancelled', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PLANTING_JOB_STATUS'
;



--rollback DELETE FROM 
--rollback     master.scale_value 
--rollback WHERE 
--rollback     scale_id = (SELECT scale_id FROM master.variable WHERE abbrev='PLANTING_JOB_STATUS');

--rollback INSERT INTO master.scale_value
--rollback     (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
--rollback SELECT
--rollback     var.scale_id,
--rollback     scalval.abbrev,
--rollback     scalval.value,
--rollback     scalval.order_number,
--rollback     scalval.description,
--rollback     scalval.display_name,
--rollback     scalval.creator_id,
--rollback     scalval.scale_value_status
--rollback FROM
--rollback     master.variable AS var,
--rollback     (
--rollback         VALUES
--rollback         ('PLANTING_JOB_STATUS_READY_FOR_PACKING', 'ready for packing', 1, 'Ready for Packing', 'Ready for Packing', 1, 'show'),
--rollback         ('PLANTING_JOB_STATUS_PACKING', 'packing', 2, 'Packing', 'Packing', 1, 'show'),
--rollback         ('PLANTING_JOB_STATUS_PACKED', 'packed', 3, 'Packed', 'Packed', 1, 'show'),
--rollback         ('PLANTING_JOB_STATUS_PACKING_ON_HOLD', 'packing on hold', 4, 'Packing on Hold', 'Packing on Hold', 1, 'show'),
--rollback         ('PLANTING_JOB_STATUS_PACKING_CANCELLED', 'packing cancelled', 5, 'Packing Cancelled', 'Packing Cancelled', 1, 'show')
--rollback     ) AS scalval (
--rollback         abbrev, value, order_number, description, display_name, creator_id, scale_value_status
--rollback     )
--rollback WHERE
--rollback     var.abbrev = 'PLANTING_JOB_STATUS'
--rollback ;