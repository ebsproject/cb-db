--liquibase formatted sql

--changeset postgres:add_scale_values_to_occurrence_status_variable_in_master.scale_value context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-334 Add scale values to OCCURRENCE_STATUS variable in master.scale_value



-- add new OCCURRENCE_STATUS scale values
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, scale_value_status, creator_id)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.scale_value_status,
    scalval.creator_id
FROM
    master.variable AS var,
    (
        VALUES
        ('OCCURRENCE_STATUS_READY_FOR_PACKING', 'ready for packing', 10, 'Ready for Packing', 'Ready for Packing', 'show', 1),
        ('OCCURRENCE_STATUS_PACKING', 'packing', 11, 'Packing', 'Packing', 'show', 1),
        ('OCCURRENCE_STATUS_PACKED', 'packed', 12, 'Packed', 'Packed', 'show', 1),
        ('OCCURRENCE_STATUS_PACKING_ON_HOLD', 'packing on hold', 13, 'Packing on Hold', 'Packing on Hold', 'show', 1),
        ('OCCURRENCE_STATUS_PACKING_CANCELLED', 'packing cancelled', 14, 'Packing Cancelled', 'Packing Cancelled', 'show', 1)
    ) AS scalval (
        abbrev, value, order_number, description, display_name, scale_value_status, creator_id
    )
WHERE
    var.abbrev = 'OCCURRENCE_STATUS'
;

-- update previous scale values
UPDATE
    master.scale_value AS scalval
SET
    description = t.description,
    display_name = t.display_name,
    scale_value_status = 'show'
FROM (
        VALUES
        ('OCCURRENCE_STATUS_DRAFT', 'Draft', 'Draft'),
        ('OCCURRENCE_STATUS_MAPPED', 'Mapped', 'Mapped'),
        ('OCCURRENCE_STATUS_CREATED', 'Created', 'Created'),
        ('OCCURRENCE_STATUS_COMMITTED', 'Committed', 'Committed'),
        ('OCCURRENCE_STATUS_PLANTED', 'Planted', 'Planted'),
        ('OCCURRENCE_STATUS_GENERATE_LOCATION_IN_PROGRESS', 'Generate Location in Progress', 'Generate Location in Progress'),
        ('OCCURRENCE_STATUS_GENERATE_LOCATION_FAILED', 'Generate Location Failed', 'Generate Location Failed'),
        ('OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_IN_PROGRESS', 'Upload Planting Arrays in Progress', 'Upload Planting Arrays in Progress'),
        ('OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_FAILED', 'Upload Planting Arrays Failed', 'Upload Planting Arrays Failed')
    ) AS t (
        abbrev, description, display_name
    )
WHERE
    scalval.abbrev = t.abbrev
;



-- rollback
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev
--rollback IN (
--rollback     'OCCURRENCE_STATUS_READY_FOR_PACKING',
--rollback     'OCCURRENCE_STATUS_PACKING',
--rollback     'OCCURRENCE_STATUS_PACKED',
--rollback     'OCCURRENCE_STATUS_PACKING_ON_HOLD',
--rollback     'OCCURRENCE_STATUS_PACKING_CANCELLED'
--rollback     )
--rollback ;
--rollback 
--rollback UPDATE
--rollback     master.scale_value AS scalval
--rollback SET
--rollback     description = t.description,
--rollback     display_name = t.display_name,
--rollback     scale_value_status = NULL
--rollback FROM (
--rollback         VALUES
--rollback         ('OCCURRENCE_STATUS_DRAFT', 'draft', 'draft'),
--rollback         ('OCCURRENCE_STATUS_MAPPED', 'mapped', 'mapped'),
--rollback         ('OCCURRENCE_STATUS_CREATED', 'created', 'created'),
--rollback         ('OCCURRENCE_STATUS_COMMITTED', 'committed', 'committed'),
--rollback         ('OCCURRENCE_STATUS_PLANTED', 'planted', 'planted'),
--rollback         ('OCCURRENCE_STATUS_GENERATE_LOCATION_IN_PROGRESS', 'generate location in progress', 'generate location in progress'),
--rollback         ('OCCURRENCE_STATUS_GENERATE_LOCATION_FAILED', 'generate location failed', 'generate location failed'),
--rollback         ('OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_IN_PROGRESS', 'upload planting arrays in progress', 'upload planting arrays in progress'),
--rollback         ('OCCURRENCE_STATUS_UPLOAD_PLANTING_ARRAYS_FAILED', 'upload planting arrays failed', 'upload planting arrays failed')
--rollback     ) AS t (
--rollback         abbrev, description, display_name
--rollback     )
--rollback WHERE
--rollback     scalval.abbrev = t.abbrev
--rollback ;
