--liquibase formatted sql

--changeset postgres:update_planting_job_status_check_constraint context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-625 Update planting_job_status check constraint



-- drop check constraint
ALTER TABLE
    experiment.planting_job
DROP CONSTRAINT
    planting_job_status_chk
;

-- update check constraint
ALTER TABLE
    experiment.planting_job
ADD CONSTRAINT
    planting_job_status_chk
CHECK (
        planting_job_status = ANY(
            ARRAY[
                'draft',
                'ready for packing',
                'packing',
                'packed',
                'packing on hold',
                'packing cancelled',
                'updating entries in progress'
            ]
        )
    )
;



-- revert changes
--rollback ALTER TABLE
--rollback     experiment.planting_job
--rollback DROP CONSTRAINT
--rollback     planting_job_status_chk
--rollback ;
--rollback 
--rollback ALTER TABLE
--rollback     experiment.planting_job
--rollback ADD CONSTRAINT
--rollback     planting_job_status_chk
--rollback CHECK (
--rollback         planting_job_status = ANY(
--rollback             ARRAY[
--rollback                 'draft',
--rollback                 'ready for packing',
--rollback                 'packing',
--rollback                 'packed',
--rollback                 'packing on hold',
--rollback                 'packing cancelled'
--rollback             ]
--rollback         )
--rollback     )
--rollback ;
