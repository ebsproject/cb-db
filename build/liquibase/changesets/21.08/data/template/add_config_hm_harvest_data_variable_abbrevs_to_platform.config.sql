--liquibase formatted sql

--changeset postgres:add_config_hm_harvest_data_variable_abbrevs_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-611 Add HM_HARVEST_DATA_VARIABLE_ABBREVS config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_HARVEST_DATA_VARIABLE_ABBREVS',
        'HM: Harvest Manager Allowed Variable Inputs',
        $$			      
            {
                "values": [
                    "HVDATE_CONT",
                    "HV_METH_DISC",
                    "NO_OF_PLANTS",
                    "PANNO_SEL",
                    "SPECIFIC_PLANT",
                    "NO_OF_EARS",
                    "NO_OF_SEED"
                ]
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-611 Add HM_HARVEST_DATA_VARIABLE_ABBREVS config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_HARVEST_DATA_VARIABLE_ABBREVS';