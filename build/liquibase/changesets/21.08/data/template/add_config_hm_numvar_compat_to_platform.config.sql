--liquibase formatted sql

--changeset postgres:add_config_hm_numvar_compat_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-613 Add HM_NUMVAR_COMPAT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NUMVAR_COMPAT',
        'HM: Numeric variable compatibility',
        $$			      
            {
                "values": {
                    "PANNO_SEL" : {
                        "description" : "Number of panicles",
                        "methods" : [
                            "Panicle Selection"
                        ],
                        "crossMethods" : [
                            "CROSS_METHOD_SELFING"
                        ],
                        "id" : "no-of-panicles-input"
                    },
                    "SPECIFIC_PLANT" : {
                        "description" : "Specific plant",
                        "methods" : [
                            "Plant-Specific and Bulk",
                            "Plant-specific",
                            "plant",
                            "Plant"],
                        "crossMethods" : [
                            "CROSS_METHOD_SELFING"
                        ],
                        "id" : "specific-plant-input"
                    },
                    "NO_OF_PLANTS" : {
                        "description" : "Number of plants",
                        "methods" : [
                            "Single Plant Selection",
                            "Single Plant Selection and Bulk",
                            "Single plant seed increase",
                            "Selected bulk",
                            "Individual spike"],
                        "crossMethods" : [
                            "CROSS_METHOD_SELFING"
                        ],
                        "id" : "no-of-plants-input"
                    },
                    "NO_OF_EARS" : {
                        "description" : "Number of ears",
                        "methods" : [
                            "Individual ear"
                        ],
                        "crossMethods" : [
                            "CROSS_METHOD_SELFING"
                        ],
                        "id" : "no-of-ears-input"
                    },
                    "NO_OF_SEED" : {
                        "description" : "Number of seeds",
                        "methods" : [
                            "Bulk",
                            "Single seed numbering"
                        ],
                        "crossMethods" : [
                            "CROSS_METHOD_SINGLE_CROSS",
                            "CROSS_METHOD_DOUBLE_CROSS",
                            "CROSS_METHOD_THREE_WAY_CROSS",
                            "CROSS_METHOD_COMPLEX_CROSS",
                            "CROSS_METHOD_BACKCROSS"
                        ],
                        "id" : "no-of-seed-input"
                    }
                }
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-613 Add HM_NUMVAR_COMPAT to platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_NUMVAR_COMPAT';