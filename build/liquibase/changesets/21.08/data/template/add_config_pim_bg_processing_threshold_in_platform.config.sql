--liquibase formatted sql

--changeset postgres:add_config_pim_bg_processing_threshold_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: CORB-1958 Add config PLANTING_INSTRUCTIONS_MANAGER_BG_PROCESSING_THRESHOLD in platform.config

INSERT INTO platform.config(
	abbrev, name, config_value, rank, usage)
	VALUES (
		'PLANTING_INSTRUCTIONS_MANAGER_BG_PROCESSING_THRESHOLD',
		'Background processing threshold values for PLANTING INSTRUCTIONS MANAGER tool features', 
		'
			{
				"updateEntries": {
					"size": "1000",
					"description": "Update entry records"
				}
			}
		'::json, 
		1, 
		'planting_instructions_manager_bg_process'
	);

	
--rollback DELETE FROM
--rollback     platform.config
--rollback WHERE
--rollback     abbrev = 'PLANTING_INSTRUCTIONS_MANAGER_BG_PROCESSING_THRESHOLD'
--rollback ;