--liquibase formatted sql

--changeset postgres:add_scale_value_to_planting_job_status_variable context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-625 Add new status in PLANTING_JOB_STATUS scale values



-- add scale value to PLANTING_JOB_STATUS variable
INSERT INTO master.scale_value
    (scale_id, abbrev, value, order_number, description, display_name, creator_id, scale_value_status)
SELECT
    var.scale_id,
    scalval.abbrev,
    scalval.value,
    scalval.order_number,
    scalval.description,
    scalval.display_name,
    scalval.creator_id,
    scalval.scale_value_status
FROM
    master.variable AS var,
    (
        VALUES
        ('PLANTING_JOB_STATUS_UPDATING_ENTRIES_IN_PROGRESS', 'updating entries in progress', 7, 'Updating Entries in Progress', 'Updating Entries in Progress', 1, 'show')
    ) AS scalval (
        abbrev, value, order_number, description, display_name, creator_id, scale_value_status
    )
WHERE
    var.abbrev = 'PLANTING_JOB_STATUS'
;



-- revert changes
--rollback DELETE FROM
--rollback     master.scale_value
--rollback WHERE
--rollback     abbrev IN ('PLANTING_JOB_STATUS_UPDATING_ENTRIES_IN_PROGRESS')
--rollback ;
