--liquibase formatted sql

--changeset postgres:update_config_plot_type_2r5m context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-647 Update PLOT_TYPE_2R5M config



-- update config
UPDATE
    platform.config
SET
    config_value = $$
        {
          "Name": "Required experiment level protocol plot type variables",
          "Values": [
            {
              "default": 2,
              "disabled": true,
              "required": "required",
              "variable_abbrev": "ROWS_PER_PLOT_CONT"
            },
            {
              "unit": "cm",
              "default": 15,
              "disabled": true,
              "required": "required",
              "variable_abbrev": "DIST_BET_ROWS"
            },
            {
              "default": 0.3,
              "disabled": true,
              "required": "required",
              "variable_abbrev": "PLOT_WIDTH"
            },
            {
              "unit": "m",
              "default": false,
              "disabled": false,
              "computed":"computed",
              "required": "required",
              "variable_abbrev": "PLOT_LN_1"
            },
            {
              "unit": "sqm",
              "default": false,
              "computed":"computed",
              "disabled": true,
              "variable_abbrev": "PLOT_AREA_2"
            },
            {
              "default": 0.8,
              "disabled": true,
              "required": "required",
              "variable_abbrev": "ALLEY_LENGTH"
            },
            {
              "unit": "m",
              "default": false,
              "disabled": false,
              "required": false,
              "allow_new_val": true,
              "variable_abbrev": "SEEDING_RATE"
            },
            {
              "default": false,
              "disabled": false,
              "variable_abbrev": "PLANTING_INSTRUCTIONS"
            }
          ]
        }
    $$
WHERE
    abbrev = 'PLOT_TYPE_2R5M'
;



-- revert changes
--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback         {
--rollback           "Name": "Required experiment level protocol plot type variables",
--rollback           "Values": [
--rollback             {
--rollback               "default": 2,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "ROWS_PER_PLOT_CONT"
--rollback             },
--rollback             {
--rollback               "unit": "m",
--rollback               "default": 1.5,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "DIST_BET_ROWS"
--rollback             },
--rollback             {
--rollback               "default": 3,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "PLOT_WIDTH"
--rollback             },
--rollback             {
--rollback               "unit": "m",
--rollback               "default": 5,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "PLOT_LN_1"
--rollback             },
--rollback             {
--rollback               "unit": "sqm",
--rollback               "default": 15,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "PLOT_AREA_2"
--rollback             },
--rollback             {
--rollback               "default": 0.8,
--rollback               "disabled": true,
--rollback               "required": "required",
--rollback               "variable_abbrev": "ALLEY_LENGTH"
--rollback             },
--rollback             {
--rollback               "unit": "m",
--rollback               "default": false,
--rollback               "disabled": false,
--rollback               "required": false,
--rollback               "allow_new_val": true,
--rollback               "variable_abbrev": "SEEDING_RATE"
--rollback             },
--rollback             {
--rollback               "default": false,
--rollback               "disabled": false,
--rollback               "variable_abbrev": "PLANTING_INSTRUCTIONS"
--rollback             }
--rollback           ]
--rollback         }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'PLOT_TYPE_2R5M'
--rollback ;
