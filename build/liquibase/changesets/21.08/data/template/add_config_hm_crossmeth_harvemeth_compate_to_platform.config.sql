--liquibase formatted sql

--changeset postgres:add_config_hm_crossmeth_harvemeth_compate_to_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-612 Add HM_CROSSMETH_HARVMETH_COMPAT to platform.config



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_CROSSMETH_HARVMETH_COMPAT',
        'HM: Cross method-harvest method compatibility',
        $$			      
            {
                "values": {
                        "CROSS_METHOD_SELFING" : [
                        "Bulk",
                        "Panicle Selection",
                        "Plant-Specific and Bulk",
                        "Plant-specific",
                        "Single Plant Selection",
                        "Single Plant Selection and Bulk",
                        "Single Seed Descent",
                        "Selected bulk",
                        "Individual spike",
                        "Individual ear"
                    ],
                    "CROSS_METHOD_TOP_CROSS" : [
                        "Bulk"
                    ],
                    "CROSS_METHOD_SINGLE_CROSS" : [
                        "Bulk",
                        "Single seed numbering"
                    ],
                    "CROSS_METHOD_DOUBLE_CROSS" : [
                        "Bulk"
                    ],
                    "CROSS_METHOD_THREE_WAY_CROSS" : [
                        "Bulk"
                    ],
                    "CROSS_METHOD_COMPLEX_CROSS" : [
                        "Bulk"
                    ],
                    "CROSS_METHOD_HYBRID_FORMATION" : [
                        "Bulk"
                    ],
                    "CROSS_METHOD_BACKCROSS" : [
                        "Bulk"
                    ]
                }
            }
        $$,
        1,
        'harvest_manager',
        1,
        'DB-612 Add HM_CROSSMETH_HARVMETH_COMPAT to platform.config'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='HM_CROSSMETH_HARVMETH_COMPAT';