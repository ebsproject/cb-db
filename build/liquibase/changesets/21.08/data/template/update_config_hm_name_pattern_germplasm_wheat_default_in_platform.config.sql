--liquibase formatted sql

--changeset postgres:update_config_hm_name_pattern_germplasm_wheat_default_in_platform.config context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-620 Update config HM_NAME_PATTERN_GERMPLASM_WHEAT_DEFAULT in platform.config



UPDATE
    platform.config
SET
    config_value = 
    '
        {
            "PLOT": {
                "default": {
                    "not_fixed": {
                        "bulk": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "free-text",
                                "value": "0",
                                "order_number": 2
                            },
                            {
                                "type": "free-text",
                                "value": "TOP",
                                "order_number": 3,
                                "special": "top"
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "originSiteCode",
                                "order_number": 4
                            }
                        ],
                        "default": [
                            {
                                "type": "free-text",
                                "value": "",
                                "order_number": 0
                            }
                        ],
                        "selected bulk": {
                            "with_no_of_plant": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "free-text",
                                    "value": "0",
                                    "order_number": 2
                                },
                                {
                                    "type": "field",
                                    "entity": "harvestData",
                                    "field_name": "numeric_variable",
                                    "order_number": 3
                                },
                                {
                                    "type": "free-text",
                                    "value": "TOP",
                                    "order_number": 4,
                                    "special": "top"
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "originSiteCode",
                                    "order_number": 5
                                }
                            ],
                            "without_no_of_plant": [
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "germplasmDesignation",
                                    "order_number": 0
                                },
                                {
                                    "type": "delimiter",
                                    "value": "-",
                                    "order_number": 1
                                },
                                {
                                    "type": "free-text",
                                    "value": "099",
                                    "order_number": 2
                                },
                                {
                                    "type": "free-text",
                                    "value": "TOP",
                                    "order_number": 3,
                                    "special": "top"
                                },
                                {
                                    "type": "field",
                                    "entity": "plot",
                                    "field_name": "originSiteCode",
                                    "order_number": 4
                                }
                            ]
                        },
                        "individual spike": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            },
                            {
                                "type": "free-text",
                                "value": "TOP",
                                "order_number": 3,
                                "special": "top"
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "originSiteCode",
                                "order_number": 4
                            }
                        ],
                        "single plant selection": [
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "germplasmDesignation",
                                "order_number": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 2
                            },
                            {
                                "type": "free-text",
                                "value": "TOP",
                                "order_number": 4,
                                "special": "top"
                            },
                            {
                                "type": "field",
                                "entity": "plot",
                                "field_name": "originSiteCode",
                                "order_number": 3
                            }
                        ]
                    }
                }
            },
            "CROSS": {
                "default": {
                    "default": {
                        "default": [
                            {
                                "type": "free-text",
                                "value": "",
                                "order_number": 0
                            }
                        ]
                    }
                }
            },
            "harvest_mode": {
                "cross_method": {
                    "germplasm_state/germplasm_type": {
                        "harvest_method": [
                            {
                                "type": "free-text",
                                "value": "ABC",
                                "order_number": 0
                            },
                            {
                                "type": "field",
                                "entity": "<entity>",
                                "field_name": "<field_name>",
                                "order_number": 1
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "order_number": 1
                            },
                            {
                                "type": "counter",
                                "order_number": 3
                            },
                            {
                                "type": "db-sequence",
                                "schema": "<schema>",
                                "order_number": 4,
                                "sequence_name": "<sequence_name>"
                            },
                            {
                                "type": "free-text-repeater",
                                "value": "ABC",
                                "minimum": "2",
                                "delimiter": "*",
                                "order_number": 5
                            }
                        ]
                    }
                }
            }
        }
    '
WHERE
    abbrev = 'HM_NAME_PATTERN_GERMPLASM_WHEAT_DEFAULT';


--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = 
--rollback     '
--rollback         {
--rollback             "PLOT": {
--rollback                 "default": {
--rollback                     "not_fixed": {
--rollback                         "bulk": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "0",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 3
--rollback                             }
--rollback                         ],
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ],
--rollback                         "selected bulk": {
--rollback                             "with_no_of_plant": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "germplasmDesignation",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "delimiter",
--rollback                                     "value": "-",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "0",
--rollback                                     "order_number": 2
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "harvestData",
--rollback                                     "field_name": "numeric_variable",
--rollback                                     "order_number": 3
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "originSiteCode",
--rollback                                     "order_number": 4
--rollback                                 }
--rollback                             ],
--rollback                             "without_no_of_plant": [
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "germplasmDesignation",
--rollback                                     "order_number": 0
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "delimiter",
--rollback                                     "value": "-",
--rollback                                     "order_number": 1
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "free-text",
--rollback                                     "value": "099",
--rollback                                     "order_number": 2
--rollback                                 },
--rollback                                 {
--rollback                                     "type": "field",
--rollback                                     "entity": "plot",
--rollback                                     "field_name": "originSiteCode",
--rollback                                     "order_number": 3
--rollback                                 }
--rollback                             ]
--rollback                         },
--rollback                         "individual spike": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 3
--rollback                             }
--rollback                         ],
--rollback                         "single plant selection": [
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "germplasmDesignation",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 2
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "plot",
--rollback                                 "field_name": "originSiteCode",
--rollback                                 "order_number": 3
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "CROSS": {
--rollback                 "default": {
--rollback                     "default": {
--rollback                         "default": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "",
--rollback                                 "order_number": 0
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             "harvest_mode": {
--rollback                 "cross_method": {
--rollback                     "germplasm_state/germplasm_type": {
--rollback                         "harvest_method": [
--rollback                             {
--rollback                                 "type": "free-text",
--rollback                                 "value": "ABC",
--rollback                                 "order_number": 0
--rollback                             },
--rollback                             {
--rollback                                 "type": "field",
--rollback                                 "entity": "<entity>",
--rollback                                 "field_name": "<field_name>",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "delimiter",
--rollback                                 "value": "-",
--rollback                                 "order_number": 1
--rollback                             },
--rollback                             {
--rollback                                 "type": "counter",
--rollback                                 "order_number": 3
--rollback                             },
--rollback                             {
--rollback                                 "type": "db-sequence",
--rollback                                 "schema": "<schema>",
--rollback                                 "order_number": 4,
--rollback                                 "sequence_name": "<sequence_name>"
--rollback                             },
--rollback                             {
--rollback                                 "type": "free-text-repeater",
--rollback                                 "value": "ABC",
--rollback                                 "minimum": "2",
--rollback                                 "delimiter": "*",
--rollback                                 "order_number": 5
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             }
--rollback         }
--rollback     '
--rollback WHERE
--rollback     abbrev = 'HM_NAME_PATTERN_GERMPLASM_WHEAT_DEFAULT';