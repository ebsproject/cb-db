--liquibase formatted sql

--changeset postgres:clean_up_saved_dashboard_filters context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-606 Clean up saved dashboard filters



DELETE FROM platform.user_dashboard_config;
DELETE FROM platform.user_data_browser_configuration WHERE name = 'occurrences-text-grid filter';



--rollback SELECT NULL;