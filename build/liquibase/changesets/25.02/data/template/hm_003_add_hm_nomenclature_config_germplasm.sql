--liquibase formatted sql

--changeset postgres:hm_003_add_hm_nomenclature_config_germplasm context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_GERMPLASM') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Germplasm



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_GERMPLASM',
        'Harvest Manager - Harvest Nomenclature Configuration for Germplasm',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": [
                        "not_fixed", "fixed"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_SEED_DESCENT",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B RGA",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION",
                        "PANICLE_SELECTION",
                        "SINGLE_PLANT_SEED_INCREASE"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": "PLANT_SPECIFIC",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "specific",
								"options": {
									"minimum": 2,
									"delimiter": "^",
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "stageCode": "TCV",
                    "state": "fixed",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "freeText",
                                "value": " R",
                                "orderNumber": 1
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "!= SELFING",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "updated",
                                "fieldName": "crossName",
                                "orderNumber": 1
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "contains F1",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "freeText",
                                "value": "(",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": ")",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
                                "type": "freeText",
                                "subType": "repeater",
                                "value": "B",
                                "options": {
                                    "minimum": 2,
									"delimiter": "*"
                                },
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "subType": "repeater",
                                "value": "B",
                                "options": {
                                    "minimum": 2,
									"delimiter": "*"
                                },
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "contains F1",
                    "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                    "config": {
                        "pattern": [
                            {
                                "type": "freeText",
                                "value": "(",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": ")",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 4
							}
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "INDIVIDUAL_EAR",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": "MAIZE",
                    "crossMethodAbbrev": "SELFING",
                    "state": "fixed",
                    "type": "doubled_haploid",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "0",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "F1TOP",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "0TOP",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "noOfPlant": "!= null",
                    "harvestMethodAbbrev": "SELECTED_BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "0",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "noOfPlant",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 4
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 5
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SELECTED_BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "099",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "F1TOP",
                    "noOfPlant": "!= null",
                    "harvestMethodAbbrev": "SELECTED_BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "0",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "noOfPlant",
                                "orderNumber": 3
                            },
                            {
                                "type": "freeText",
                                "value": "TOP",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "F1TOP",
                    "harvestMethodAbbrev": "SELECTED_BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "099TOP",
                                "orderNumber": 2
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "grainColor",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							},
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 3
                            }
                        ]
                    }
                },
                {
                    "cropCode": "WHEAT",
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "F1TOP",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION", "INDIVIDUAL_SPIKE"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							},
                            {
                                "type": "freeText",
                                "value": "TOP",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 4
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "generation": "contains F1",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "F2",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "1",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "COWPEA", "SOYBEAN"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "state": "not_fixed",
                    "harvestMethodAbbrev": "SINGLE_PLANT",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "germplasmDesignation",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Germplasm'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_GERMPLASM';