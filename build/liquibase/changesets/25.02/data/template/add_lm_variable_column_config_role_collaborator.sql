--liquibase formatted sql

--changeset postgres:add_gm_max_values_config_admin.sql context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'LM_ROLE_COLLABORATOR_VARIABLE_COLUMNS_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-4317 CB-LM:Update to configuration-based retrieval of browser columns



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'LM_ROLE_COLLABORATOR_VARIABLE_COLUMNS_CONFIG',
        'List Manager column variables for list configuration record for COLLABORATOR role',
        $$[
            {
                "label": "Germplasm Name",
                "abbrev": "GERMPLASM_NAME",
                "api_field_name": "germplasmName",
                "data_level": "germplasm",
                "description": "Germplasm Name",
                "display_name": "Germplasm Name",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed,germplasm,plot",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Germplasm Code",
                "abbrev": "GERMPLASM_CODE",
                "api_field_name": "germplasmCode",
                "data_level": "germplasm",
                "description": "Germplasm Code",
                "display_name": "Germplasm Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed,germplasm,plot",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Generation",
                "abbrev": "GENERATION",
                "api_field_name": "generation",
                "data_level": "germplasm",
                "description": "Generation",
                "display_name": "Generation",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Other Names",
                "abbrev": "OTHER_NAME",
                "api_field_name": "otherNames",
                "data_level": "germplasm",
                "description": "Other Names",
                "display_name": "Other Names",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Parentage",
                "abbrev": "PARENTAGE",
                "api_field_name": "parentage",
                "data_level": "germplasm",
                "description": "Parentage",
                "display_name": "Parentage",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm,plot",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Germplasm State",
                "abbrev": "GERMPLASM_STATE",
                "api_field_name": "germplasmState",
                "data_level": "germplasm",
                "description": "Germplasm State",
                "display_name": "Germplasm State",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm,plot",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Germplasm Type",
                "abbrev": "GERMPLASM_TYPE",
                "api_field_name": "germplasmNameType",
                "data_level": "germplasm",
                "description": "Germplasm Type",
                "display_name": "Germplasm Type",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Germplasm Normalized Name",
                "abbrev": "DESIGNATION",
                "api_field_name": "germplasmNormalizedName",
                "data_level": "germplasm",
                "description": "Germplasm Normalized Name",
                "display_name": "Germplasm Normalized Name",
                "sort": "true",
                "filter": "true",
                "list_type": "germplasm",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "germplasm": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Seed Name",
                "abbrev": "SEED_NAME",
                "api_field_name": "seedName",
                "data_level": "seed",
                "description": "Seed Name",
                "display_name": "Seed Name",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Seed Code",
                "abbrev": "SEED_CODE",
                "api_field_name": "seedCode",
                "data_level": "seed",
                "description": "Seed Code",
                "display_name": "Seed Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Source Occurrence Name",
                "abbrev": "OCCURRENCE_NAME",
                "api_field_name": "experimentOccurrence",
                "data_level": "occurrence",
                "description": "Source Occurrence Name",
                "display_name": "Source Occurrence Name",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Source Experiment Name",
                "abbrev": "EXPERIMENT_NAME",
                "api_field_name": "experimentName",
                "data_level": "experiment",
                "description": "Source Experiment Name",
                "display_name": "Source Experiment Name",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Harvest Year",
                "abbrev": "SOURCE_HARV_YEAR",
                "api_field_name": "sourceHarvestYear",
                "data_level": "seed",
                "description": "Harvest Year",
                "display_name": "Harvest Year",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "integer"
            },
            {
                "label": "Harvest Date",
                "abbrev": "HVDATE_CONT",
                "api_field_name": "harvestDate",
                "data_level": "seed",
                "description": "Harvest Date",
                "display_name": "Harvest Date",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "date"
            },
            {
                "label": "Harvest Method",
                "abbrev": "HV_METH_DISC",
                "api_field_name": "harvestMethod",
                "data_level": "seed",
                "description": "Harvest Method",
                "display_name": "Harvest Method",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Program Name",
                "abbrev": "PROGRAM_NAME",
                "api_field_name": "programName",
                "data_level": "seed",
                "description": "Program Name",
                "display_name": "Program Name",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Program Code",
                "abbrev": "PROGRAM_CODE",
                "api_field_name": "seedProgram",
                "data_level": "seed",
                "description": "Program Code",
                "display_name": "Program Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Experiment Year",
                "abbrev": "EXPERIMENT_YEAR",
                "api_field_name": "experimentYear",
                "data_level": "experiment",
                "description": "Experiment Year",
                "display_name": "Experiment Year",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "integer"
            },
            {
                "label": "Experiment Type",
                "abbrev": "EXPERIMENT_TYPE",
                "api_field_name": "experimentType",
                "data_level": "seed",
                "description": "Experiment Type",
                "display_name": "Experiment Type",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Experiment Stage",
                "abbrev": "STAGE",
                "api_field_name": "experimentStage",
                "data_level": "seed",
                "description": "Experiment Stage",
                "display_name": "Experiment Stage",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Season",
                "abbrev": "SEASON",
                "api_field_name": "sourceStudySeason",
                "data_level": "experiment",
                "description": "Season",
                "display_name": "Season",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Entry Code",
                "abbrev": "ENTRY_CODE",
                "api_field_name": "sourceEntryCode",
                "data_level": "entry",
                "description": "Entry Code",
                "display_name": "Entry Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Plot Code",
                "abbrev": "PLOT_CODE",
                "api_field_name": "seedSourcePlotCode",
                "data_level": "plot",
                "description": "Plot Code",
                "display_name": "Plot Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Entry Number",
                "abbrev": "ENTNO",
                "api_field_name": "seedSourceEntryNumber",
                "data_level": "entry",
                "description": "Entry Number",
                "display_name": "Entry Number",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "integer"
            },
            {
                "label": "Replication",
                "abbrev": "REP",
                "api_field_name": "replication",
                "data_level": "plot",
                "description": "Replication",
                "display_name": "Replication",
                "sort": "true",
                "filter": "true",
                "list_type": "package,seed,plot",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "seed":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "integer"
            },
            {
                "label": "Package Code",
                "abbrev": "PACKAGE_CODE",
                "api_field_name": "packageCode",
                "data_level": "package",
                "description": "Package Code",
                "display_name": "Package Code",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Package Label",
                "abbrev": "PACKAGE_LABEL",
                "api_field_name": "label",
                "data_level": "package",
                "description": "Package Label",
                "display_name": "Package Label",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Package Quantity",
                "abbrev": "PACKAGE_QUANTITY",
                "api_field_name": "quantity",
                "data_level": "package",
                "description": "Package Quantity",
                "display_name": "Package Quantity",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "integer"
            },
            {
                "label": "Package Unit",
                "abbrev": "PACKAGE_UNIT",
                "api_field_name": "unit",
                "data_level": "package",
                "description": "Package Unit",
                "display_name": "Package Unit",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Facility",
                "abbrev": "FACILITY",
                "api_field_name": "facility",
                "data_level": "package",
                "description": "Facility",
                "display_name": "Facility",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Sub Facility",
                "abbrev": "SUB_FACILITY",
                "api_field_name": "subFacility",
                "data_level": "package",
                "description": "Sub Facility",
                "display_name": "Sub Facility",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Container",
                "abbrev": "CONTAINER",
                "api_field_name": "container",
                "data_level": "package",
                "description": "Container",
                "display_name": "Container",
                "sort": "true",
                "filter": "true",
                "list_type": "package",
                "column_options":{
                    "default":{
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "package":{
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Abbrev",
                "abbrev": "ABBREV",
                "api_field_name": "abbrev",
                "data_level": "trait",
                "description": "Trait Abbrev",
                "display_name": "Trait Abbrev",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Label",
                "abbrev": "LABEL",
                "api_field_name": "label",
                "data_level": "package",
                "description": "Trait Label",
                "display_name": "Trait Label",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Name",
                "abbrev": "NAME",
                "api_field_name": "name",
                "data_level": "trait",
                "description": "Trait Name",
                "display_name": "Trait Name",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Display Name",
                "abbrev": "DISPLAY_NAME",
                "api_field_name": "displayName",
                "data_level": "triat",
                "description": "Trait Display Name",
                "display_name": "Trait Display Name",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Data Type",
                "abbrev": "DATA_TYPE",
                "api_field_name": "dataType",
                "data_level": "trait",
                "description": "Trait Data Type",
                "display_name": "Trait Data Type",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Data Level",
                "abbrev": "DATA_LEVEL",
                "api_field_name": "dataLevel",
                "data_level": "trait",
                "description": "Trait Data Level",
                "display_name": "Trait Data Level",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Type",
                "abbrev": "VARIABLE_TYPE",
                "api_field_name": "type",
                "data_level": "trait",
                "description": "Trait Type",
                "display_name": "Trait Type",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Trait Scale Value",
                "abbrev": "SCALE_VALUE",
                "api_field_name": "scaleValue",
                "data_level": "trait",
                "description": "Trait Scale Value",
                "display_name": "Trait Scale Value",
                "sort": "true",
                "filter": "true",
                "list_type": "trait",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "trait": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Occurrence Name",
                "abbrev": "OCCURRENCE_NAME",
                "api_field_name": "occurrenceName",
                "data_level": "plot",
                "description": "Occurrence Name",
                "display_name": "Occurrence Name",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Plot Code",
                "abbrev": "PLOT_CODE",
                "api_field_name": "plotCode",
                "data_level": "plot",
                "description": "Plot Code",
                "display_name": "Plot Code",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Plot Number",
                "abbrev": "PLOT_NUMBER",
                "api_field_name": "plotNumber",
                "data_level": "plot",
                "description": "Plot Number",
                "display_name": "Plot Number",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Entry Number",
                "abbrev": "ENTRY_NUMBER",
                "api_field_name": "entryNumber",
                "data_level": "plot",
                "description": "Entry Number",
                "display_name": "Entry Number",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Entry Code",
                "abbrev": "ENTRY_CODE",
                "api_field_name": "entryCode",
                "data_level": "plot",
                "description": "Entry Code",
                "display_name": "Entry Code",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "true",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Design_X",
                "abbrev": "DESIGN_X",
                "api_field_name": "designX",
                "data_level": "plot",
                "description": "Design_X",
                "display_name": "Design_X",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Design_Y",
                "abbrev": "DESIGN_Y",
                "api_field_name": "designY",
                "data_level": "plot",
                "description": "Design_Y",
                "display_name": "Design_Y",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "PA_X",
                "abbrev": "PA_X",
                "api_field_name": "paX",
                "data_level": "plot",
                "description": "PA_X",
                "display_name": "PA_X",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "PA_Y",
                "abbrev": "PA_Y",
                "api_field_name": "paY",
                "data_level": "plot",
                "description": "PA_Y",
                "display_name": "PA_Y",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "PA_Y",
                "abbrev": "PA_Y",
                "api_field_name": "paY",
                "data_level": "plot",
                "description": "PA_Y",
                "display_name": "PA_Y",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Block Number",
                "abbrev": "BLOCK_NO_CONT",
                "api_field_name": "blockNumber",
                "data_level": "plot",
                "description": "Block Number",
                "display_name": "Block Number",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            },
            {
                "label": "Harvest Status",
                "abbrev": "HARVEST_STATUS",
                "api_field_name": "harvestStatus",
                "data_level": "plot",
                "description": "Harvest Status",
                "display_name": "Harvest Status",
                "sort": "true",
                "filter": "true",
                "list_type": "plot",
                "column_options": {
                    "default": {
                        "browser_column": "false",
                        "visible_browser": "false",
                        "visible_export": "false"
                    },
                    "plot": {
                        "browser_column": "true",
                        "visible_browser": "false",
                        "visible_export": "true"
                    }
                },
                "data_type": "string"
            }
        ]$$,
        1,
        'List Manager variable columns confguration for lists for COLLABORATOR role',
        (
            SELECT 
                id
            FROM
                tenant.person
            WHERE 
                person_name = 'EBS, Admin'
        ),
        'BDS-4317 CB-LM:Update to configuration-based retrieval of browser columns'
    )
;



--rollback DELETE FROM platform.config WHERE abbrev='LM_ROLE_COLLABORATOR_VARIABLE_COLUMNS_CONFIG';