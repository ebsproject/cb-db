--liquibase formatted sql

--changeset postgres:hm_006_add_hm_nomenclature_config_backcross_derivative_name context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_BACKCROSS_DERIVATIVE_NAME') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Backcross Derivative Name



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_BACKCROSS_DERIVATIVE_NAME',
        'Harvest Manager - Harvest Nomenclature Configuration for Backcross Derivative Name',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": [
                        "SINGLE_PLANT_SELECTION",
                        "PANICLE_SELECTION",
                        "SINGLE_PLANT_SEED_INCREASE"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "plantedGermplasmBCDerivName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": "PLANT_SPECIFIC",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "plantedGermplasmBCDerivName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
								"type": "counter",
								"subType": "specific",
								"options": {
									"minimum": 2,
									"delimiter": "^",
									"schema": "germplasm",
									"table": "germplasm_name",
									"field": "name_value",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 2
							}
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "plantedGermplasmBCDerivName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "harvestMethodAbbrev": "SINGLE_SEED_DESCENT",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "plantedGermplasmBCDerivName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B RGA",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": [
                        "SINGLE_CROSS",
                        "DOUBLE_CROSS",
                        "THREE_WAY_CROSS",
                        "COMPLEX_CROSS"
                    ],
                    "projectCode": "!= null",
                    "crossCount": "= 1",
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "projectCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": [
                        "SINGLE_CROSS",
                        "DOUBLE_CROSS",
                        "THREE_WAY_CROSS",
                        "COMPLEX_CROSS"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "updated",
                                "fieldName": "crossName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "orderNumber": 1
                            },
                            {
                                "type": "freeText",
                                "value": "B",
                                "orderNumber": 2
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Backcross Derivative Name'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_BACKCROSS_DERIVATIVE_NAME';