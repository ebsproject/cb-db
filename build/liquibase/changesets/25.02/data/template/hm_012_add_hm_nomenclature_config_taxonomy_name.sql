--liquibase formatted sql

--changeset postgres:hm_012_add_hm_nomenclature_config_taxonomy_name context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_TAXONOMY_NAME') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-5114 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Taxonomy Name



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_TAXONOMY_NAME',
        'Harvest Manager - Harvest Nomenclature Configuration for Taxonomy Name',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "femaleGermplasmTaxonomyName",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": " x ",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "maleGermplasmTaxonomyName",
                                "orderNumber": 2
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BD-29 BD-5114 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Taxonomy Name'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_TAXONOMY_NAME';