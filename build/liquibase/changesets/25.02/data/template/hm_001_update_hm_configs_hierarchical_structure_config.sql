--liquibase formatted sql

--changeset postgres:hm_001_update_hm_configs_hierarchical_structure_config context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_CONFIGS_HIERARCHICAL_STRUCTURE') WHEN TRUE THEN 1 ELSE 0 END;
--comment:  BDS-29 BDS-4849 - CB-HM: Update HM config HM_CONFIGS_HIERARCHICAL_STRUCTURE to add new fields



UPDATE
    platform.config
SET
    config_value = $$
    {
        "entities": [
            {
                "level": 0,
                "entity": "harvestData",
                "fields": [
                    "harvestMethod",
                    "harvestMethodAbbrev",
                    "noOfPlant"
                ]
            },
            {
                "level": 1,
                "entity": "germplasm",
                "fields": [
                    "state",
                    "type",
                    "generation"
                ]
            },
            {
                "level": 2,
                "entity": "cross",
                "fields": [
                    "crossMethod",
                    "crossMethodAbbrev"
                ]
            },
            {
                "level": 3,
                "entity": "experiment",
                "fields": [
                    "stageCode",
                    "occurrenceCount",
                    "projectCode",
                    "plotCount",
                    "crossCount"
                ]
            },
            {
                "level": 3,
                "entity": "program",
                "fields": [
                    "programCode"
                ]
            },
            {
                "level": 4,
                "entity": "crop",
                "fields": [
                    "cropCode"
                ]
            }
        ]
    }
    $$
WHERE
    abbrev = 'HM_CONFIGS_HIERARCHICAL_STRUCTURE';

--rollback UPDATE
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback     {
--rollback     "entities": [
--rollback         {
--rollback             "level": 0,
--rollback             "entity": "germplasm",
--rollback             "fields": [
--rollback                 "state",
--rollback                 "type",
--rollback                 "generation"
--rollback             ]
--rollback         },
--rollback         {
--rollback             "level": 1,
--rollback             "entity": "cross",
--rollback             "fields": [
--rollback                 "crossMethod"
--rollback             ]
--rollback         },
--rollback         {
--rollback             "level": 2,
--rollback             "entity": "experiment",
--rollback             "fields": [
--rollback                 "stageCode"
--rollback             ]
--rollback         },
--rollback         {
--rollback             "level": 2,
--rollback             "entity": "program",
--rollback             "fields": [
--rollback                 "programCode"
--rollback             ]
--rollback         },
--rollback         {
--rollback             "level": 3,
--rollback             "entity": "crop",
--rollback             "fields": [
--rollback                 "cropCode"
--rollback             ]
--rollback         }
--rollback     ]
--rollback     }
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'HM_CONFIGS_HIERARCHICAL_STRUCTURE';