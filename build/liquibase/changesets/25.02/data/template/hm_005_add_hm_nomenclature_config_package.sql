--liquibase formatted sql

--changeset postgres:hm_005_add_hm_nomenclature_config_package context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_PACKAGE') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Package



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_PACKAGE',
        'Harvest Manager - Harvest Nomenclature Configuration for Package',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "SELFING",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "plotCode",
                                "orderNumber": 0
                            }
                        ]
                    }
                },
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": "!= SELFING",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "new",
                                "fieldName": "designation",
                                "orderNumber": 0
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "retrieved",
                                "fieldName": "harvestedPackagePrefix",
                                "orderNumber": 0
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 1
                            },
                            {
                                "type": "counter",
                                "subType": "autoIncrement",
                                "options": {
                                    "schema": "germplasm",
                                    "table": "package",
                                    "field": "package_label",
                                    "zeroPadded": true,
                                    "maxDigits": 4
                                },
                                "orderNumber": 2
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Package'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_PACKAGE';