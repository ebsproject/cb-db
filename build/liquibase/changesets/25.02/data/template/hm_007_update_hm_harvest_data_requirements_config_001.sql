--liquibase formatted sql

--changeset postgres:hm_007_update_hm_harvest_data_requirements_config_001 context:template splitStatements:false rollbackSplitStatements:false
--comment: BDS-29 BDS-4849 - CB-HM: Update Harvest Data Requirements Config



UPDATE 
    platform.config
SET
    config_value = $$
        [
            {
                "cropCode": "*",
                "state": "unknown",
                "config": {
                    "harvestData": [ ],
                    "harvestMethods": { }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "type": "fixed_line",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT_SEED_INCREASE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            } 
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": "*",
                "type": [ 
                    "genome_edited", "transgenic" 
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_SEED_DESCENT": [],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION_AND_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "PANICLE_SELECTION": [
                            {
                                "variableAbbrev": "PANNO_SEL",
                                "apiFieldName": "noOfPanicle",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "PLANT_SPECIFIC": [
                            {
                                "variableAbbrev": "SPECIFIC_PLANT",
                                "apiFieldName": "specificPlantNo",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "comma_sep_int"
                                }
                            }
                        ],
                        "PLANT_SPECIFIC_AND_BULK": [
                            {
                                "variableAbbrev": "SPECIFIC_PLANT",
                                "apiFieldName": "specificPlantNo",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "comma_sep_int"
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "stageCode": "TCV",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "HYB_MATERIAL_TYPE",
                            "apiFieldName": "hybridMaterialType",
                            "required": true,
                            "inputType": "select2",
                            "inputOptions": {
                                "useScaleValues": true
                            }
                        }
                    ],
                    "harvestMethods": {
                        "R_LINE_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "stageCode": "TCV",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": { }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": [
                    "SINGLE_CROSS", "BACKCROSS"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "HEAD_SINGLE_SEED_NUMBERING": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": [
                    "DOUBLE_CROSS", "THREE_WAY_CROSS",
                    "COMPLEX_CROSS"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": [
                    "TRANSGENESIS", "GENOME_EDITING"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "HEAD_SINGLE_SEED_NUMBERING": [
                            {
                                "variableAbbrev": "NO_OF_SEED",
                                "apiFieldName": "noOfSeed",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "HYBRID_FORMATION",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "H_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "CMS_MULTIPLICATION",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "A_LINE_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "RICE",
                "crossMethodAbbrev": "TEST_CROSS",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "DATE_CROSSED",
                            "apiFieldName": "crossingDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "TEST_CROSS_HARVEST": [
                            {
                                "variableAbbrev": "NO_OF_BAGS",
                                "apiFieldName": "noOfBag",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SELECTED_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "INDIVIDUAL_SPIKE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        },
                        {
                            "variableAbbrev": "GRAIN_COLOR",
                            "apiFieldName": "grainColor",
                            "required": false,
                            "inputType": "select2",
                            "inputOptions": {
                                "useScaleValues": true
                            }
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SELECTED_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "SINGLE_PLANT_SELECTION_AND_BULK": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ],
                        "INDIVIDUAL_SPIKE": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "WHEAT",
                "crossMethodAbbrev": [
                    "SINGLE_CROSS", "DOUBLE_CROSS", "TOP_CROSS", "BACKCROSS", "HYBRID_FORMATION"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": [ 
                    "fixed",
                    "not_fixed"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "type": "haploid",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "DH1_INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "type": [
                    "landrace", "composite", "synthetic", "population"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "MAINTAIN_AND_BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ],
                        "INDIVIDUAL_EAR": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": "MAIZE",
                "crossMethodAbbrev": [
                    "SINGLE_CROSS", "DOUBLE_CROSS", "THREE_WAY_CROSS", 
                    "BACKCROSS", "HYBRID_FORMATION", "MATERNAL_HAPLOID_INDUCTION"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": true,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [
                            {
                                "variableAbbrev": "NO_OF_EARS",
                                "apiFieldName": "noOfEar",
                                "required": false,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                } 
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": [
                    "contains F1", "contains F4", "contains F5", "contains F6"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "contains F2",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "SINGLE_PLANT": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "generation": "contains F3",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 1
                                }
                            }
                        ]
                    }
                }
            },
            {
                "cropCode": [
                    "COWPEA", "SOYBEAN"
                ],
                "crossMethodAbbrev": [
                    "BI_PARENTAL_CROSS", "BACKCROSS"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            },
            {
                "cropCode": [
                    "CHICKPEA", "GROUNDNUT", "SORGHUM",
                    "FINGERMILLET", "PEARLMILLET", "PIGEOPEA"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "SINGLE_PLANT": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                }
            },
            {
                "cropCode": [
                    "CHICKPEA", "GROUNDNUT", "SORGHUM",
                    "FINGERMILLET", "PEARLMILLET", "PIGEOPEA"
                ],
                "crossMethodAbbrev": "SELFING",
                "state": "not_fixed",
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": [],
                        "MAINTAIN_AND_BULK": [],
                        "SINGLE_PLANT": [
                            {
                                "variableAbbrev": "NO_OF_PLANTS",
                                "apiFieldName": "noOfPlant",
                                "required": true,
                                "inputType": "number",
                                "inputOptions": {
                                    "inputSubType": "single_int",
                                    "minimumValue": 0
                                }
                            }
                        ]
                    }
                }
            },
            {
                "cropCode": [
                    "CHICKPEA", "GROUNDNUT", "SORGHUM",
                    "FINGERMILLET", "PEARLMILLET", "PIGEOPEA"
                ],
                "crossMethodAbbrev": [
                    "SINGLE_CROSS", "THREE_WAY_CROSS", "BACKCROSS"
                ],
                "config": {
                    "harvestData": [
                        {
                            "variableAbbrev": "HVDATE_CONT",
                            "apiFieldName": "harvestDate",
                            "required": false,
                            "inputType": "datepicker",
                            "inputOptions": {}
                        }
                    ],
                    "harvestMethods": {
                        "BULK": []
                    }
                }
            }
        ]
    $$
WHERE
    abbrev = 'HM_HARVEST_DATA_REQUIREMENTS_CONFIG'
;



--rollback UPDATE 
--rollback     platform.config
--rollback SET
--rollback     config_value = $$
--rollback         [
--rollback             {
--rollback                 "cropCode": "*",
--rollback                 "state": "unknown",
--rollback                 "config": {
--rollback                     "harvestData": [ ],
--rollback                     "harvestMethods": { }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "type": "fixed_line",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_PLANT_SEED_INCREASE": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             } 
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "*",
--rollback                 "type": [ 
--rollback                     "genome_edited", "transgenic" 
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SINGLE_SEED_DESCENT": [],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "SINGLE_PLANT_SELECTION_AND_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PANICLE_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "PANNO_SEL",
--rollback                                 "apiFieldName": "noOfPanicle",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PLANT_SPECIFIC": [
--rollback                             {
--rollback                                 "variableAbbrev": "SPECIFIC_PLANT",
--rollback                                 "apiFieldName": "specificPlantNo",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "comma_sep_int"
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "PLANT_SPECIFIC_AND_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "SPECIFIC_PLANT",
--rollback                                 "apiFieldName": "specificPlantNo",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "comma_sep_int"
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "stageCode": "TCV",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "R_LINE_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "stageCode": "TCV",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": { }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "single cross", "backcross"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "HEAD_SINGLE_SEED_NUMBERING": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "double cross", "three-way cross",
--rollback                     "complex cross"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": [
--rollback                     "transgenesis", "genome editing"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "HEAD_SINGLE_SEED_NUMBERING": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_SEED",
--rollback                                 "apiFieldName": "noOfSeed",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "hybrid formation",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "H_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "CMS multiplication",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "A_LINE_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "RICE",
--rollback                 "crossMethod": "test cross",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "TEST_CROSS_HARVEST": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_BAGS",
--rollback                                 "apiFieldName": "noOfBag",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SELECTED_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": false,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "INDIVIDUAL_SPIKE": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "GRAIN_COLOR",
--rollback                             "apiFieldName": "grainColor",
--rollback                             "required": false,
--rollback                             "inputType": "select2",
--rollback                             "inputOptions": {
--rollback                                 "useScaleValues": true
--rollback                             }
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [],
--rollback                         "SELECTED_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": false,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "INDIVIDUAL_SPIKE": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "WHEAT",
--rollback                 "crossMethod": [
--rollback                     "single cross", "double cross", "top cross", "backcross", "hybrid formation"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": [ 
--rollback                     "fixed",
--rollback                     "not_fixed"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": false,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "INDIVIDUAL_EAR": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "type": "haploid",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "DH1_INDIVIDUAL_EAR": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "type": [
--rollback                     "landrace", "composite", "synthetic", "population"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": false,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "MAINTAIN_AND_BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": false,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ],
--rollback                         "INDIVIDUAL_EAR": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": "MAIZE",
--rollback                 "crossMethod": [
--rollback                     "single cross", "double cross", "three-way cross", 
--rollback                     "backcross", "hybrid formation", "maternal haploid induction"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_EARS",
--rollback                                 "apiFieldName": "noOfEar",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 0
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 } 
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "fixed",
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "generation": [
--rollback                     "contains F1", "contains F4", "contains F5", "contains F6"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethod": "selfing",
--rollback                 "state": "not_fixed",
--rollback                 "generation": [
--rollback                     "contains F2", "contains F3"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "SINGLE_PLANT_SELECTION": [
--rollback                             {
--rollback                                 "variableAbbrev": "NO_OF_PLANTS",
--rollback                                 "apiFieldName": "noOfPlant",
--rollback                                 "required": true,
--rollback                                 "inputType": "number",
--rollback                                 "inputOptions": {
--rollback                                     "inputSubType": "single_int",
--rollback                                     "minimumValue": 1
--rollback                                 }
--rollback                             }
--rollback                         ]
--rollback                     }
--rollback                 }
--rollback             },
--rollback             {
--rollback                 "cropCode": [
--rollback                     "COWPEA", "SOYBEAN"
--rollback                 ],
--rollback                 "crossMethod": [
--rollback                     "bi-parental cross", "backcross"
--rollback                 ],
--rollback                 "config": {
--rollback                     "harvestData": [
--rollback                         {
--rollback                             "variableAbbrev": "HVDATE_CONT",
--rollback                             "apiFieldName": "harvestDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         },
--rollback                         {
--rollback                             "variableAbbrev": "DATE_CROSSED",
--rollback                             "apiFieldName": "crossingDate",
--rollback                             "required": true,
--rollback                             "inputType": "datepicker",
--rollback                             "inputOptions": {}
--rollback                         }
--rollback                     ],
--rollback                     "harvestMethods": {
--rollback                         "BULK": []
--rollback                     }
--rollback                 }
--rollback             }
--rollback         ]
--rollback     $$
--rollback WHERE
--rollback     abbrev = 'HM_HARVEST_DATA_REQUIREMENTS_CONFIG'
--rollback ;