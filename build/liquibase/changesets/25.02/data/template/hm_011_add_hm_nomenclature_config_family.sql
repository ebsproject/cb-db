--liquibase formatted sql

--changeset postgres:hm_011_add_hm_nomenclature_config_family context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_FAMILY') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-5114 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Family



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_FAMILY',
        'Harvest Manager - Harvest Nomenclature Configuration for Family',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "crossMethodAbbrev": [
                        "SINGLE_CROSS",
                        "DOUBLE_CROSS",
                        "THREE_WAY_CROSS",
                        "COMPLEX_CROSS"
                    ],
                    "harvestMethodAbbrev": "BULK",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "new",
                                "fieldName": "designation",
                                "orderNumber": 0
                            }
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BD-29 BD-5114 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Family'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_FAMILY';