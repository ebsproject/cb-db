--liquibase formatted sql

--changeset postgres:hm_004_add_hm_nomenclature_config_seed context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_SEED') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Seed



INSERT INTO
    platform.config (
        abbrev,
        name,
        config_value,
        rank,
        usage,
        creator_id,
        notes
    )
VALUES
    (
        'HM_NOMENCLATURE_CONFIG_SEED',
        'Harvest Manager - Harvest Nomenclature Configuration for Seed',
        $$			
            [
                {
                    "cropCode": "RICE",
                    "config": {
                        "pattern": [
                            {
                                "type": "dbSequence",
                                "sequenceSchema": "germplasm",
                                "sequenceName": "gid_seq",
                                "orderNumber": 0
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "occurrenceCount": "=1",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "harvestYearYY",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentSeasonCode",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentName",
                                "orderNumber": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 5
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "entryCode",
                                "orderNumber": 6
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "occurrenceCount": ">1",
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "harvestYearYY",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentSeasonCode",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "occurrenceName",
                                "orderNumber": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 5
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "entryCode",
                                "orderNumber": 6
                            }
                        ]
                    }
                },
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "occurrenceCount": "=1",
                    "harvestMethodAbbrev": [
                        "INDIVIDUAL_EAR",
                        "SINGLE_PLANT_SELECTION",
                        "INDIVIDUAL_SPIKE",
                        "SINGLE_PLANT"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "harvestYearYY",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentSeasonCode",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentName",
                                "orderNumber": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 5
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "entryCode",
                                "orderNumber": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "orderNumber": 7
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "seed",
									"field": "seed_name",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 8
							}
                        ]
                    }
                },
                {
                    "cropCode": [
                        "MAIZE", "WHEAT", "COWPEA", "SOYBEAN",
                        "CHICKPEA", "GROUNDNUT", "SORGHUM",
                        "FINGERMILLET", "PEARLMILLET", "PIGEONPEA"
                    ],
                    "crossMethodAbbrev": "SELFING",
                    "occurrenceCount": ">1",
                    "harvestMethodAbbrev": [
                        "INDIVIDUAL_EAR",
                        "SINGLE_PLANT_SELECTION",
                        "INDIVIDUAL_SPIKE",
                        "SINGLE_PLANT"
                    ],
                    "config": {
                        "pattern": [
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "fieldOriginSiteCode",
                                "orderNumber": 0
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "harvestYearYY",
                                "orderNumber": 1
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "experimentSeasonCode",
                                "orderNumber": 2
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 3
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "occurrenceName",
                                "orderNumber": 4
                            },
                            {
                                "type": "delimiter",
                                "value": "-",
                                "orderNumber": 5
                            },
                            {
                                "type": "recordInfo",
                                "subType": "planted",
                                "fieldName": "entryCode",
                                "orderNumber": 6
                            },
                            {
                                "type": "delimiter",
                                "value": ":",
                                "orderNumber": 7
                            },
                            {
								"type": "counter",
								"subType": "autoIncrement",
								"options": {
									"schema": "germplasm",
									"table": "seed",
									"field": "seed_name",
									"zeroPadded": false,
									"maxDigits": null
								},
								"orderNumber": 8
							}
                        ]
                    }
                }
            ]
        $$,
        1,
        'harvest_manager',
        (SELECT id FROM tenant.person WHERE username='admin' LIMIT 1),
        'BDS-29 BDS-4849 - CB-HM: Insert new config for Harvest Nomenclature Configuration for Seed'
    )
;

--rollback DELETE FROM platform.config WHERE abbrev = 'HM_NOMENCLATURE_CONFIG_SEED';