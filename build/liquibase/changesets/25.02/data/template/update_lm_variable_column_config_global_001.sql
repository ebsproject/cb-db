--liquibase formatted sql

--changeset postgres:update_lm_variable_column_config_global_001 context:template splitStatements:false rollbackSplitStatements:false
--preconditions onFail:MARK_RAN onError:HALT
--precondition-sql-check expectedResult:1 SELECT CASE EXISTS(SELECT 1 FROM platform.config WHERE abbrev = 'LM_GLOBAL_VARIABLE_COLUMNS_CONFIG') WHEN TRUE THEN 1 ELSE 0 END;
--comment: BDS-4317 CB-LM:Update to configuration-based retrieval of browser columns



UPDATE
    platform.config
SET
    config_value = $$[
        {
            "label": "Germplasm Name",
            "abbrev": "GERMPLASM_NAME",
            "api_field_name": "germplasmName",
            "data_level": "germplasm",
            "description": "Germplasm Name",
            "display_name": "Germplasm Name",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed,germplasm,plot",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Germplasm Code",
            "abbrev": "GERMPLASM_CODE",
            "api_field_name": "germplasmCode",
            "data_level": "germplasm",
            "description": "Germplasm Code",
            "display_name": "Germplasm Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed,germplasm,plot",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Generation",
            "abbrev": "GENERATION",
            "api_field_name": "generation",
            "data_level": "germplasm",
            "description": "Generation",
            "display_name": "Generation",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Other Names",
            "abbrev": "OTHER_NAME",
            "api_field_name": "otherNames",
            "data_level": "germplasm",
            "description": "Other Names",
            "display_name": "Other Names",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Parentage",
            "abbrev": "PARENTAGE",
            "api_field_name": "parentage",
            "data_level": "germplasm",
            "description": "Parentage",
            "display_name": "Parentage",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm,plot",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Germplasm State",
            "abbrev": "GERMPLASM_STATE",
            "api_field_name": "germplasmState",
            "data_level": "germplasm",
            "description": "Germplasm State",
            "display_name": "Germplasm State",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm,plot",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Germplasm Type",
            "abbrev": "GERMPLASM_TYPE",
            "api_field_name": "germplasmNameType",
            "data_level": "germplasm",
            "description": "Germplasm Type",
            "display_name": "Germplasm Type",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Germplasm Normalized Name",
            "abbrev": "DESIGNATION",
            "api_field_name": "germplasmNormalizedName",
            "data_level": "germplasm",
            "description": "Germplasm Normalized Name",
            "display_name": "Germplasm Normalized Name",
            "sort": "true",
            "filter": "true",
            "list_type": "germplasm",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "germplasm": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Seed Name",
            "abbrev": "SEED_NAME",
            "api_field_name": "seedName",
            "data_level": "seed",
            "description": "Seed Name",
            "display_name": "Seed Name",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Seed Code",
            "abbrev": "SEED_CODE",
            "api_field_name": "seedCode",
            "data_level": "seed",
            "description": "Seed Code",
            "display_name": "Seed Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Source Occurrence Name",
            "abbrev": "OCCURRENCE_NAME",
            "api_field_name": "experimentOccurrence",
            "data_level": "occurrence",
            "description": "Source Occurrence Name",
            "display_name": "Source Occurrence Name",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Source Experiment Name",
            "abbrev": "EXPERIMENT_NAME",
            "api_field_name": "experimentName",
            "data_level": "experiment",
            "description": "Source Experiment Name",
            "display_name": "Source Experiment Name",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Harvest Year",
            "abbrev": "SOURCE_HARV_YEAR",
            "api_field_name": "sourceHarvestYear",
            "data_level": "seed",
            "description": "Harvest Year",
            "display_name": "Harvest Year",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "integer"
        },
        {
            "label": "Harvest Date",
            "abbrev": "HVDATE_CONT",
            "api_field_name": "harvestDate",
            "data_level": "seed",
            "description": "Harvest Date",
            "display_name": "Harvest Date",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "date"
        },
        {
            "label": "Harvest Method",
            "abbrev": "HV_METH_DISC",
            "api_field_name": "harvestMethod",
            "data_level": "seed",
            "description": "Harvest Method",
            "display_name": "Harvest Method",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Program Name",
            "abbrev": "PROGRAM_NAME",
            "api_field_name": "programName",
            "data_level": "seed",
            "description": "Program Name",
            "display_name": "Program Name",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Program Code",
            "abbrev": "PROGRAM_CODE",
            "api_field_name": "seedProgram",
            "data_level": "seed",
            "description": "Program Code",
            "display_name": "Program Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Experiment Year",
            "abbrev": "EXPERIMENT_YEAR",
            "api_field_name": "experimentYear",
            "data_level": "experiment",
            "description": "Experiment Year",
            "display_name": "Experiment Year",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "integer"
        },
        {
            "label": "Experiment Type",
            "abbrev": "EXPERIMENT_TYPE",
            "api_field_name": "experimentType",
            "data_level": "seed",
            "description": "Experiment Type",
            "display_name": "Experiment Type",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Experiment Stage",
            "abbrev": "STAGE",
            "api_field_name": "experimentStage",
            "data_level": "seed",
            "description": "Experiment Stage",
            "display_name": "Experiment Stage",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Season",
            "abbrev": "SEASON",
            "api_field_name": "sourceStudySeason",
            "data_level": "experiment",
            "description": "Season",
            "display_name": "Season",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Entry Code",
            "abbrev": "ENTRY_CODE",
            "api_field_name": "sourceEntryCode",
            "data_level": "entry",
            "description": "Entry Code",
            "display_name": "Entry Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Plot Code",
            "abbrev": "PLOT_CODE",
            "api_field_name": "seedSourcePlotCode",
            "data_level": "plot",
            "description": "Plot Code",
            "display_name": "Plot Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Entry Number",
            "abbrev": "ENTNO",
            "api_field_name": "seedSourceEntryNumber",
            "data_level": "entry",
            "description": "Entry Number",
            "display_name": "Entry Number",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "integer"
        },
        {
            "label": "Replication",
            "abbrev": "REP",
            "api_field_name": "replication",
            "data_level": "plot",
            "description": "Replication",
            "display_name": "Replication",
            "sort": "true",
            "filter": "true",
            "list_type": "package,seed,plot",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "seed":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "integer"
        },
        {
            "label": "Package Code",
            "abbrev": "PACKAGE_CODE",
            "api_field_name": "packageCode",
            "data_level": "package",
            "description": "Package Code",
            "display_name": "Package Code",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Package Label",
            "abbrev": "PACKAGE_LABEL",
            "api_field_name": "label",
            "data_level": "package",
            "description": "Package Label",
            "display_name": "Package Label",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Package Quantity",
            "abbrev": "PACKAGE_QUANTITY",
            "api_field_name": "quantity",
            "data_level": "package",
            "description": "Package Quantity",
            "display_name": "Package Quantity",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "integer"
        },
        {
            "label": "Package Unit",
            "abbrev": "PACKAGE_UNIT",
            "api_field_name": "unit",
            "data_level": "package",
            "description": "Package Unit",
            "display_name": "Package Unit",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Facility",
            "abbrev": "FACILITY",
            "api_field_name": "facility",
            "data_level": "package",
            "description": "Facility",
            "display_name": "Facility",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Sub Facility",
            "abbrev": "SUB_FACILITY",
            "api_field_name": "subFacility",
            "data_level": "package",
            "description": "Sub Facility",
            "display_name": "Sub Facility",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Container",
            "abbrev": "CONTAINER",
            "api_field_name": "container",
            "data_level": "package",
            "description": "Container",
            "display_name": "Container",
            "sort": "true",
            "filter": "true",
            "list_type": "package",
            "column_options":{
                "default":{
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "package":{
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Abbrev",
            "abbrev": "ABBREV",
            "api_field_name": "abbrev",
            "data_level": "trait",
            "description": "Trait Abbrev",
            "display_name": "Trait Abbrev",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Label",
            "abbrev": "LABEL",
            "api_field_name": "label",
            "data_level": "package",
            "description": "Trait Label",
            "display_name": "Trait Label",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Name",
            "abbrev": "NAME",
            "api_field_name": "name",
            "data_level": "trait",
            "description": "Trait Name",
            "display_name": "Trait Name",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Display Name",
            "abbrev": "DISPLAY_NAME",
            "api_field_name": "displayName",
            "data_level": "triat",
            "description": "Trait Display Name",
            "display_name": "Trait Display Name",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Data Type",
            "abbrev": "DATA_TYPE",
            "api_field_name": "dataType",
            "data_level": "trait",
            "description": "Trait Data Type",
            "display_name": "Trait Data Type",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Data Level",
            "abbrev": "DATA_LEVEL",
            "api_field_name": "dataLevel",
            "data_level": "trait",
            "description": "Trait Data Level",
            "display_name": "Trait Data Level",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Type",
            "abbrev": "VARIABLE_TYPE",
            "api_field_name": "type",
            "data_level": "trait",
            "description": "Trait Type",
            "display_name": "Trait Type",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Trait Scale Value",
            "abbrev": "SCALE_VALUE",
            "api_field_name": "scaleValue",
            "data_level": "trait",
            "description": "Trait Scale Value",
            "display_name": "Trait Scale Value",
            "sort": "true",
            "filter": "true",
            "list_type": "trait",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "trait": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Occurrence Name",
            "abbrev": "OCCURRENCE_NAME",
            "api_field_name": "occurrenceName",
            "data_level": "plot",
            "description": "Occurrence Name",
            "display_name": "Occurrence Name",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Plot Code",
            "abbrev": "PLOT_CODE",
            "api_field_name": "plotCode",
            "data_level": "plot",
            "description": "Plot Code",
            "display_name": "Plot Code",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Plot Number",
            "abbrev": "PLOT_NUMBER",
            "api_field_name": "plotNumber",
            "data_level": "plot",
            "description": "Plot Number",
            "display_name": "Plot Number",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Entry Number",
            "abbrev": "ENTRY_NUMBER",
            "api_field_name": "entryNumber",
            "data_level": "plot",
            "description": "Entry Number",
            "display_name": "Entry Number",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Entry Code",
            "abbrev": "ENTRY_CODE",
            "api_field_name": "entryCode",
            "data_level": "plot",
            "description": "Entry Code",
            "display_name": "Entry Code",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "true",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Design_X",
            "abbrev": "DESIGN_X",
            "api_field_name": "designX",
            "data_level": "plot",
            "description": "Design_X",
            "display_name": "Design_X",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Design_Y",
            "abbrev": "DESIGN_Y",
            "api_field_name": "designY",
            "data_level": "plot",
            "description": "Design_Y",
            "display_name": "Design_Y",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "PA_X",
            "abbrev": "PA_X",
            "api_field_name": "paX",
            "data_level": "plot",
            "description": "PA_X",
            "display_name": "PA_X",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "PA_Y",
            "abbrev": "PA_Y",
            "api_field_name": "paY",
            "data_level": "plot",
            "description": "PA_Y",
            "display_name": "PA_Y",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "PA_Y",
            "abbrev": "PA_Y",
            "api_field_name": "paY",
            "data_level": "plot",
            "description": "PA_Y",
            "display_name": "PA_Y",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Block Number",
            "abbrev": "BLOCK_NO_CONT",
            "api_field_name": "blockNumber",
            "data_level": "plot",
            "description": "Block Number",
            "display_name": "Block Number",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        },
        {
            "label": "Harvest Status",
            "abbrev": "HARVEST_STATUS",
            "api_field_name": "harvestStatus",
            "data_level": "plot",
            "description": "Harvest Status",
            "display_name": "Harvest Status",
            "sort": "true",
            "filter": "true",
            "list_type": "plot",
            "column_options": {
                "default": {
                    "browser_column": "false",
                    "visible_browser": "false",
                    "visible_export": "false"
                },
                "plot": {
                    "browser_column": "true",
                    "visible_browser": "false",
                    "visible_export": "true"
                }
            },
            "data_type": "string"
        }
    ]$$
WHERE
    abbrev = 'LM_GLOBAL_VARIABLE_COLUMNS_CONFIG';



--rollback  UPDATE
--rollback      platform.config
--rollback  SET
--rollback      config_value = $$[
--rollback          {
--rollback              "label": "Germplasm Name",
--rollback              "abbrev": "GERMPLASM_NAME",
--rollback              "api_field_name": "germplasmName",
--rollback              "data_level": "germplasm",
--rollback              "description": "Germplasm Name",
--rollback              "display_name": "Germplasm Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed,germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Germplasm Code",
--rollback              "abbrev": "GERMPLASM_CODE",
--rollback              "api_field_name": "germplasmCode",
--rollback              "data_level": "germplasm",
--rollback              "description": "Germplasm Code",
--rollback              "display_name": "Germplasm Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed,germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Generation",
--rollback              "abbrev": "GENERATION",
--rollback              "api_field_name": "generation",
--rollback              "data_level": "germplasm",
--rollback              "description": "Generation",
--rollback              "display_name": "Generation",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Other Names",
--rollback              "abbrev": "OTHER_NAME",
--rollback              "api_field_name": "otherNames",
--rollback              "data_level": "germplasm",
--rollback              "description": "Other Names",
--rollback              "display_name": "Other Names",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Parentage",
--rollback              "abbrev": "PARENTAGE",
--rollback              "api_field_name": "parentage",
--rollback              "data_level": "germplasm",
--rollback              "description": "Parentage",
--rollback              "display_name": "Parentage",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Germplasm State",
--rollback              "abbrev": "GERMPLASM_STATE",
--rollback              "api_field_name": "germplasmState",
--rollback              "data_level": "germplasm",
--rollback              "description": "Germplasm State",
--rollback              "display_name": "Germplasm State",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Germplasm Type",
--rollback              "abbrev": "GERMPLASM_TYPE",
--rollback              "api_field_name": "germplasmNameType",
--rollback              "data_level": "germplasm",
--rollback              "description": "Germplasm Type",
--rollback              "display_name": "Germplasm Type",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Germplasm Normalized Name",
--rollback              "abbrev": "DESIGNATION",
--rollback              "api_field_name": "germplasmNormalizedName",
--rollback              "data_level": "germplasm",
--rollback              "description": "Germplasm Normalized Name",
--rollback              "display_name": "Germplasm Normalized Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "germplasm",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "germplasm": {
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Seed Name",
--rollback              "abbrev": "SEED_NAME",
--rollback              "api_field_name": "seedName",
--rollback              "data_level": "seed",
--rollback              "description": "Seed Name",
--rollback              "display_name": "Seed Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Seed Code",
--rollback              "abbrev": "SEED_CODE",
--rollback              "api_field_name": "seedCode",
--rollback              "data_level": "seed",
--rollback              "description": "Seed Code",
--rollback              "display_name": "Seed Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Source Occurrence Name",
--rollback              "abbrev": "OCCURRENCE_NAME",
--rollback              "api_field_name": "experimentOccurrence",
--rollback              "data_level": "occurrence",
--rollback              "description": "Source Occurrence Name",
--rollback              "display_name": "Source Occurrence Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Source Experiment Name",
--rollback              "abbrev": "EXPERIMENT_NAME",
--rollback              "api_field_name": "experimentName",
--rollback              "data_level": "experiment",
--rollback              "description": "Source Experiment Name",
--rollback              "display_name": "Source Experiment Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Harvest Year",
--rollback              "abbrev": "SOURCE_HARV_YEAR",
--rollback              "api_field_name": "sourceHarvestYear",
--rollback              "data_level": "seed",
--rollback              "description": "Harvest Year",
--rollback              "display_name": "Harvest Year",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "integer"
--rollback          },
--rollback          {
--rollback              "label": "Harvest Date",
--rollback              "abbrev": "HVDATE_CONT",
--rollback              "api_field_name": "harvestDate",
--rollback              "data_level": "seed",
--rollback              "description": "Harvest Date",
--rollback              "display_name": "Harvest Date",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "date"
--rollback          },
--rollback          {
--rollback              "label": "Harvest Method",
--rollback              "abbrev": "HV_METH_DISC",
--rollback              "api_field_name": "harvestMethod",
--rollback              "data_level": "seed",
--rollback              "description": "Harvest Method",
--rollback              "display_name": "Harvest Method",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Program Name",
--rollback              "abbrev": "PROGRAM_NAME",
--rollback              "api_field_name": "programName",
--rollback              "data_level": "seed",
--rollback              "description": "Program Name",
--rollback              "display_name": "Program Name",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Program Code",
--rollback              "abbrev": "PROGRAM_CODE",
--rollback              "api_field_name": "seedProgram",
--rollback              "data_level": "seed",
--rollback              "description": "Program Code",
--rollback              "display_name": "Program Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Experiment Year",
--rollback              "abbrev": "EXPERIMENT_YEAR",
--rollback              "api_field_name": "experimentYear",
--rollback              "data_level": "experiment",
--rollback              "description": "Experiment Year",
--rollback              "display_name": "Experiment Year",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "integer"
--rollback          },
--rollback          {
--rollback              "label": "Experiment Type",
--rollback              "abbrev": "EXPERIMENT_TYPE",
--rollback              "api_field_name": "experimentType",
--rollback              "data_level": "seed",
--rollback              "description": "Experiment Type",
--rollback              "display_name": "Experiment Type",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Experiment Stage",
--rollback              "abbrev": "STAGE",
--rollback              "api_field_name": "experimentStage",
--rollback              "data_level": "seed",
--rollback              "description": "Experiment Stage",
--rollback              "display_name": "Experiment Stage",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Season",
--rollback              "abbrev": "SEASON",
--rollback              "api_field_name": "sourceStudySeason",
--rollback              "data_level": "experiment",
--rollback              "description": "Season",
--rollback              "display_name": "Season",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Entry Code",
--rollback              "abbrev": "ENTRY_CODE",
--rollback              "api_field_name": "sourceEntryCode",
--rollback              "data_level": "entry",
--rollback              "description": "Entry Code",
--rollback              "display_name": "Entry Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Plot Code",
--rollback              "abbrev": "PLOT_CODE",
--rollback              "api_field_name": "seedSourcePlotCode",
--rollback              "data_level": "plot",
--rollback              "description": "Plot Code",
--rollback              "display_name": "Plot Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Entry Number",
--rollback              "abbrev": "ENTNO",
--rollback              "api_field_name": "seedSourceEntryNumber",
--rollback              "data_level": "entry",
--rollback              "description": "Entry Number",
--rollback              "display_name": "Entry Number",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "integer"
--rollback          },
--rollback          {
--rollback              "label": "Replication",
--rollback              "abbrev": "REP",
--rollback              "api_field_name": "replication",
--rollback              "data_level": "plot",
--rollback              "description": "Replication",
--rollback              "display_name": "Replication",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package,seed",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  },
--rollback                  "seed":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  }
--rollback              },
--rollback              "data_type": "integer"
--rollback          },
--rollback          {
--rollback              "label": "Package Code",
--rollback              "abbrev": "PACKAGE_CODE",
--rollback              "api_field_name": "packageCode",
--rollback              "data_level": "package",
--rollback              "description": "Package Code",
--rollback              "display_name": "Package Code",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Package Label",
--rollback              "abbrev": "PACKAGE_LABEL",
--rollback              "api_field_name": "label",
--rollback              "data_level": "package",
--rollback              "description": "Package Label",
--rollback              "display_name": "Package Label",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Package Quantity",
--rollback              "abbrev": "PACKAGE_QUANTITY",
--rollback              "api_field_name": "quantity",
--rollback              "data_level": "package",
--rollback              "description": "Package Quantity",
--rollback              "display_name": "Package Quantity",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "integer"
--rollback          },
--rollback          {
--rollback              "label": "Package Unit",
--rollback              "abbrev": "PACKAGE_UNIT",
--rollback              "api_field_name": "unit",
--rollback              "data_level": "package",
--rollback              "description": "Package Unit",
--rollback              "display_name": "Package Unit",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "true",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Facility",
--rollback              "abbrev": "FACILITY",
--rollback              "api_field_name": "facility",
--rollback              "data_level": "package",
--rollback              "description": "Facility",
--rollback              "display_name": "Facility",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Sub Facility",
--rollback              "abbrev": "SUB_FACILITY",
--rollback              "api_field_name": "subFacility",
--rollback              "data_level": "package",
--rollback              "description": "Sub Facility",
--rollback              "display_name": "Sub Facility",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          },
--rollback          {
--rollback              "label": "Container",
--rollback              "abbrev": "CONTAINER",
--rollback              "api_field_name": "container",
--rollback              "data_level": "package",
--rollback              "description": "Container",
--rollback              "display_name": "Container",
--rollback              "sort": "true",
--rollback              "filter": "true",
--rollback              "list_type": "package",
--rollback              "column_options":{
--rollback                  "default":{
--rollback                      "browser_column": "false",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "false"
--rollback                  },
--rollback                  "package":{
--rollback                      "browser_column": "true",
--rollback                      "visible_browser": "false",
--rollback                      "visible_export": "true"
--rollback                  }
--rollback              },
--rollback              "data_type": "string"
--rollback          }
--rollback      ]$$
--rollback  WHERE
--rollback      abbrev = 'LM_GLOBAL_VARIABLE_COLUMNS_CONFIG';

